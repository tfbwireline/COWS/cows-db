USE COWS
GO

ALTER TABLE dbo.CPT_SRVC_TYPE NOCHECK CONSTRAINT ALL
GO
DELETE FROM LK_CPT_PLN_SRVC_TYPE WITH (TABLOCKX)
GO

INSERT INTO dbo.LK_CPT_PLN_SRVC_TYPE(CPT_PLN_SRVC_TYPE_ID,CPT_PLN_SRVC_TYPE,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(1,'IP Defender',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_CPT_PLN_SRVC_TYPE(CPT_PLN_SRVC_TYPE_ID,CPT_PLN_SRVC_TYPE,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(2,'MSS-Compucom',0,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_CPT_PLN_SRVC_TYPE(CPT_PLN_SRVC_TYPE_ID,CPT_PLN_SRVC_TYPE,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(3,'Managed Authentication',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_CPT_PLN_SRVC_TYPE(CPT_PLN_SRVC_TYPE_ID,CPT_PLN_SRVC_TYPE,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(4,'MSS Sprint',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_CPT_PLN_SRVC_TYPE(CPT_PLN_SRVC_TYPE_ID,CPT_PLN_SRVC_TYPE,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(5,'MSS Other',1,'1','1',GETDATE(),GETDATE())
GO

ALTER TABLE dbo.CPT_SRVC_TYPE CHECK CONSTRAINT ALL
GO



