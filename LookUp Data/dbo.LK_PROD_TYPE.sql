USE COWS
GO
SET NOCOUNT ON

GO
ALTER TABLE LK_XNCI_EMAIL_TYPE_MAPNG NOCHECK CONSTRAINT ALL
GO
ALTER TABLE LK_PROD_TYPE NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.IPL_ORDR NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.IPL_PROD_SRVC_CKT NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.LK_ACCS_ARNGT NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.LK_PPRT NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.ORDR_GRP NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.PLTFRM_MAPNG NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.PROD_ORDR_TYPE NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.USER_WFM NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.NCCO_ORDR NOCHECK CONSTRAINT ALL
GO
DELETE FROM LK_PROD_TYPE WITH (TABLOCKX)
GO
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (1,'IPL Canada',GETDATE(),1,'0',null, 'IPL','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (2,'IPL Mexico',GETDATE(),1,'0',null, 'IPL','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (3,'IPL Offshore',GETDATE(),1,'0',null, 'IPL','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (4,'IPL Bilateral',GETDATE(),1,'0',null, 'IPL','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (5,'IPL Leased',GETDATE(),1,'0',null, 'IPL','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (6,'IPL Resale',GETDATE(),1,'0',null, 'IPL','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (7,'IPL E2E',GETDATE(),1,'0',null, 'IPL','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (8,'IPL ICBH',GETDATE(),1,'0',null, 'IPL','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (9,'CPE',GETDATE(),2,'0','CP', 'CPE','0')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (10,'DIA Onnet',GETDATE(),2,'0','DN', 'DIA','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (11,'DIA Offnet',GETDATE(),2,'0','DO', 'DIA','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (12,'Generic Product',GETDATE(),2,'0','GP', 'Generic Product','0')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (13,'Dedicated IP',GETDATE(),2,'0','IP', 'Dedicated IP','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (14,'Managed Network Services',GETDATE(),2,'0','MN', 'MNS','0')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (15,'MPLS Onnet',GETDATE(),2,'0','MP', 'MPLS','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (16,'Managed Security Services',GETDATE(),2,'0','SE', 'MSS','0')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (17,'SLFR Onnet',GETDATE(),2,'0','SN', 'SLFR','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (18,'SLFR Offnet',GETDATE(),2,'0','SO', 'SLFR','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (19,'MPLS Offnet',GETDATE(),2,'0','MO', 'MPLS','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (20,'ATM',GETDATE(),4,'0',null, 'ATM','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (21,'GFR',GETDATE(),4,'0',null, 'GFR','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (22,'IP',GETDATE(),4,'0',null, 'IP','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (23,'SLFR',GETDATE(),4,'0',null, 'SLFR','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (24,'MPLS',GETDATE(),4,'0',null, 'MPLS','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (25,'DPL',GETDATE(),5,'0',null, 'IPL','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (26,'Half Tunnel',GETDATE(),4,'0',null, 'Half Tunnel','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (27,'CPE',GETDATE(),4,'0',null, 'CPE','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (28,'IPL',GETDATE(),4,'0',null, 'IPL Term','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (29,'MNS',GETDATE(),4,'0',null, 'MNS','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (30,'IPL Onnet',GETDATE(),6,'0','IN', 'IPL E2E','1')
INSERT INTO [COWS].[dbo].[LK_PROD_TYPE] (PROD_TYPE_ID,PROD_TYPE_DES,CREAT_DT,ORDR_CAT_ID,DMSTC_CD,FSA_PROD_TYPE_CD, PROD_NME, TRPT_CD) VALUES (31,'IPL Offnet',GETDATE(),6,'0','IO', 'IPL E2E','1')


GO
ALTER TABLE LK_PROD_TYPE CHECK CONSTRAINT ALL
GO
ALTER TABLE LK_XNCI_EMAIL_TYPE_MAPNG CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.IPL_ORDR CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.IPL_PROD_SRVC_CKT CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.LK_ACCS_ARNGT CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.LK_PPRT CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.ORDR_GRP CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.PLTFRM_MAPNG CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.PROD_ORDR_TYPE CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.USER_WFM CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.NCCO_ORDR CHECK CONSTRAINT ALL
GO