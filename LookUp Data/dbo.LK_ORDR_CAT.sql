﻿USE [COWS]
GO
SET NOCOUNT ON

GO
ALTER TABLE dbo.LK_JPRDY NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.LK_PROD_TYPE NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.ORDR NOCHECK CONSTRAINT ALL
GO
DELETE FROM [dbo].[LK_ORDR_CAT] WITH (TABLOCKX)
GO

INSERT INTO [dbo].[LK_ORDR_CAT]
           ([ORDR_CAT_ID]
           ,[ORDR_CAT_DES]
           ,[CREAT_DT]
           ,[MODFD_DT]
           ,[MODFD_BY_USER_ID]
           ,[CREAT_BY_USER_ID]
           ,[REC_STUS_ID])
     VALUES
           (1
           ,'IPL'
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1
           ,1)


INSERT INTO [dbo].[LK_ORDR_CAT]
           ([ORDR_CAT_ID]
           ,[ORDR_CAT_DES]
           ,[CREAT_DT]
           ,[MODFD_DT]
           ,[MODFD_BY_USER_ID]
           ,[CREAT_BY_USER_ID]
           ,[REC_STUS_ID])
     VALUES
           (2
           ,'FSA'
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1
           ,1)


INSERT INTO [dbo].[LK_ORDR_CAT]
           ([ORDR_CAT_ID]
           ,[ORDR_CAT_DES]
           ,[CREAT_DT]
           ,[MODFD_DT]
           ,[MODFD_BY_USER_ID]
           ,[CREAT_BY_USER_ID]
           ,[REC_STUS_ID])
     VALUES
           (3
           ,'Vendor'
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1
           ,1)


INSERT INTO [dbo].[LK_ORDR_CAT]
           ([ORDR_CAT_ID]
           ,[ORDR_CAT_DES]
           ,[CREAT_DT]
           ,[MODFD_DT]
           ,[MODFD_BY_USER_ID]
           ,[CREAT_BY_USER_ID]
           ,[REC_STUS_ID])
     VALUES
           (4
           ,'NCCO'
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1
           ,1)


INSERT INTO [dbo].[LK_ORDR_CAT]
           ([ORDR_CAT_ID]
           ,[ORDR_CAT_DES]
           ,[CREAT_DT]
           ,[MODFD_DT]
           ,[MODFD_BY_USER_ID]
           ,[CREAT_BY_USER_ID]
           ,[REC_STUS_ID])
     VALUES
           (5
           ,'DPL'
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1
           ,1)

INSERT INTO [dbo].[LK_ORDR_CAT]
           ([ORDR_CAT_ID]
           ,[ORDR_CAT_DES]
           ,[CREAT_DT]
           ,[MODFD_DT]
           ,[MODFD_BY_USER_ID]
           ,[CREAT_BY_USER_ID]
           ,[REC_STUS_ID])
     VALUES
           (6
           ,'Mach5'
           ,GETDATE()
           ,GETDATE()
           ,1
           ,1
           ,1)


GO
ALTER TABLE dbo.LK_JPRDY CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.LK_PROD_TYPE CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.ORDR CHECK CONSTRAINT ALL
GO
