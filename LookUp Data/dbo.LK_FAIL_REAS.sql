USE [COWS]
GO
SET NOCOUNT ON
GO
ALTER TABLE dbo.MDS_EVENT NOCHECK CONSTRAINT ALL 
GO
ALTER TABLE dbo.MDS_EVENT_NEW NOCHECK CONSTRAINT ALL 
GO
--ALTER TABLE arch.MDS_EVENT_NEW NOCHECK CONSTRAINT ALL 
--GO
ALTER TABLE dbo.EVENT_HIST NOCHECK CONSTRAINT ALL 
GO
--ALTER TABLE arch.EVENT_HIST NOCHECK CONSTRAINT ALL 
--GO
DELETE FROM [dbo].[LK_FAIL_REAS] WITH (TABLOCKX)
GO

INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(1,' ',0,1,1,GETDATE(),GETDATE(),0,0)
GO

INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(2,'Customer Not Ready',1,1,1,GETDATE(),GETDATE(),0,1)
GO

INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(3,'IPM Team Failure / Other',1,1,1,GETDATE(),GETDATE(),0,1)
GO

INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(4,'MDS Team Failure',1,1,1,GETDATE(),GETDATE(),0,0)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(5,'Sprint Systems',1,1,1,GETDATE(),GETDATE(),0,1)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(6,'Ckt Physical',1,1,1,GETDATE(),GETDATE(),0,1)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(7,'Ckt Logical',1,1,1,GETDATE(),GETDATE(),0,1)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(8,'Wireless',1,1,1,GETDATE(),GETDATE(),0,0)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(9,'Hardware',1,1,1,GETDATE(),GETDATE(),0,1)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(10,'CPE Late/No Show',1,1,1,GETDATE(),GETDATE(),0,0)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(11,'1mb Dial Access',1,1,1,GETDATE(),GETDATE(),0,0)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(12,'Phase Complete, Sending back for next Phase',1,1,1,GETDATE(),GETDATE(),0,0)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(13,'Return to Published',1,1,1,GETDATE(),GETDATE(),0,0)
GO
INSERT INTO [dbo].[LK_FAIL_REAS]
           ([FAIL_REAS_ID],[FAIL_REAS_DES],[REC_STUS_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT],[CREAT_DT],[MDS_FAST_TRK_CD],[NTWK_ONLY_CD])
     VALUES(14,'Other, Send to FastTrack At-Will',0,1,1,GETDATE(),GETDATE(),1,0)
GO


ALTER TABLE dbo.MDS_EVENT CHECK CONSTRAINT ALL 
GO
ALTER TABLE dbo.MDS_EVENT_NEW CHECK CONSTRAINT ALL 
GO
--ALTER TABLE arch.MDS_EVENT_NEW CHECK CONSTRAINT ALL 
--GO
ALTER TABLE dbo.EVENT_HIST CHECK CONSTRAINT ALL 
GO
--ALTER TABLE arch.EVENT_HIST CHECK CONSTRAINT ALL 
--GO

