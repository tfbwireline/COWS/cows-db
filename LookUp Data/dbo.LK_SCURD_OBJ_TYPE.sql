USE COWS
GO

ALTER TABLE dbo.CUST_SCRD_DATA NOCHECK CONSTRAINT ALL
GO
DELETE FROM LK_SCRD_OBJ_TYPE WITH (TABLOCKX)
GO

INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(1,'AD_EVENT','AD_EVENT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(2,'CPT','CPT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(3,'FSA_MDS_EVENT','FSA_MDS_EVENT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(4,'FSA_MDS_EVENT_NEW','FSA_MDS_EVENT_NEW',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(5,'FSA_ORDR_CUST','FSA_ORDR_CUST',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(6,'H5_FOLDR','H5_FOLDR',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(7,'MDS_EVENT','MDS_EVENT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(8,'MDS_EVENT_NEW','MDS_EVENT_NEW',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(9,'MPLS_EVENT','MPLS_EVENT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(10,'NCCO_ORDR','NCCO_ORDR',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(11,'NGVN_EVENT','NGVN_EVENT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(12,'ODIE_REQ','ODIE_REQ',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(13,'ODIE_RSPN','ODIE_RSPN',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(14,'ORDR_ADR','ORDR_ADR',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(15,'ORDR_CNTCT','ORDR_CNTCT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(16,'REDSGN','REDSGN',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(17,'REDSGN_CUST_BYPASS','REDSGN_CUST_BYPASS',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(18,'SIPT_EVENT','SIPT_EVENT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(19,'SPLK_EVENT','SPLK_EVENT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(20,'UCaaS_EVENT','UCaaS_EVENT',1,'1','1',GETDATE(),GETDATE())
-- INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(21,'FEDLINE_EVENT_ADR','FEDLINE_EVENT_ADR',1,'1','1',GETDATE(),GETDATE())
-- INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(22,'FEDLINE_EVENT_CNTCT','FEDLINE_EVENT_CNTCT',1,'1','1',GETDATE(),GETDATE())
-- INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(23,'FEDLINE_EVENT_TADPOLE_DATA','FEDLINE_EVENT_TADPOLE_DATA',1,'1','1',GETDATE(),GETDATE())
-- INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(24,'FEDLINE_EVENT_TADPOLE_XML','FEDLINE_EVENT_TADPOLE_XML',1,'1','1',GETDATE(),GETDATE())
-- INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(25,'FEDLINE_EVENT_USER_DATA','FEDLINE_EVENT_USER_DATA',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(26,'SDE_OPPTNTY','SDE_OPPTNTY',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(27,'SDE_OPPTNTY_NTE','SDE_OPPTNTY_NTE',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(28,'MPLS_EVENT_ACCS_TAG','MPLS_EVENT_ACCS_TAG',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(29,'NRM_CKT','NRM_CKT',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(30,'MDS_EVENT_NTWK_CUST','MDS_EVENT_NTWK_CUST',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(31,'MDS_EVENT_NTWK_TRPT','MDS_EVENT_NTWK_TRPT',1,'1','1',GETDATE(),GETDATE())



INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(101,'EMAIL_REQ','EMAIL_REQ',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(102,'SSTAT_REQ','SSTAT_REQ',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(103,'SSTAT_RSPN','SSTAT_RSPN',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(104,'M5_ORDR_MSG','M5_ORDR_MSG',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(105,'M5_EVENT_MSG','M5_EVENT_MSG',1,'1','1',GETDATE(),GETDATE())
INSERT INTO dbo.LK_SCRD_OBJ_TYPE(SCRD_OBJ_TYPE_ID,SCRD_OBJ_TYPE,SCRD_OBJ_TYPE_DES,REC_STUS_ID, CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)  VALUES(106,'M5_REDSGN_MSG','M5_REDSGN_MSG',1,'1','1',GETDATE(),GETDATE())
GO


ALTER TABLE dbo.CUST_SCRD_DATA CHECK CONSTRAINT ALL
GO