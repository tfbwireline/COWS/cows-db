USE COWS
ALTER TABLE dbo.MPLS_EVENT_ACCS_TAG NOCHECK CONSTRAINT ALL
GO
DELETE FROM LK_MNS_ROUTG_TYPE WITH (TABLOCKX)
GO

INSERT LK_MNS_ROUTG_TYPE(MNS_ROUTG_TYPE_ID,MNS_ROUTG_TYPE_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES('1','Static','1','1','1',GETDATE(),GETDATE())
INSERT LK_MNS_ROUTG_TYPE(MNS_ROUTG_TYPE_ID,MNS_ROUTG_TYPE_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES('2','Dynamic','1','1','1',GETDATE(),GETDATE())
GO
ALTER TABLE dbo.MPLS_EVENT_ACCS_TAG CHECK CONSTRAINT ALL
GO