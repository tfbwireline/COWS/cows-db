﻿USE [COWS]
GO
SET NOCOUNT ON
GO
ALTER TABLE dbo.LK_VNDR_EMAIL_TYPE NOCHECK CONSTRAINT ALL 
GO
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'Vendor Confirmation Email') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('Vendor Confirmation Email','COP-INTERNATIONAL@sprint.com',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'COP-INTERNATIONAL@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'Vendor Confirmation Email'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'Vendor Disc Confirmation Email') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('Vendor Disc Confirmation Email','COP-INTERNATIONAL@sprint.com',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'COP-INTERNATIONAL@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'Vendor Disc Confirmation Email'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'ASR Email') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('ASR Email','ITP-ASRNotification@sprint.com',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'INTLTerms@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'ASR Email'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'CPT ShortName Notification') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('CPT ShortName Notification','IPServicesVendorSupport@sprint.com, IPS-MSOnBoarding@sprint.com',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'IPServicesVendorSupport@sprint.com, IPS-MSOnBoarding@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'CPT ShortName Notification'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'CPT IP Address Notification') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('CPT IP Address Notification','MNSMgmtLink.LIST@sprint.com',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'MNSMgmtLink.LIST@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'CPT IP Address Notification'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'CPT MNSD-MNSW Notification') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('CPT MNSD-MNSW Notification','Walter.D.Bigley@sprint.com, Ronnie.2.Villa@sprint.com',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'Walter.D.Bigley@sprint.com, Ronnie.2.Villa@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'CPT MNSD-MNSW Notification'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'CPT DB Shortname Validation') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('CPT DB Shortname Validation','false',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'true',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'CPT DB Shortname Validation'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'CPT Mngd Voice Completion') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('CPT Mngd Voice Completion','IP_Services_UCaaS_COWS_Reviewers@sprint.com',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'IP_Services_UCaaS_COWS_Reviewers@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'CPT Mngd Voice Completion'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'CPT Resource Assignment') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('CPT Resource Assignment','CPTSiteAssignmentConfirmation@sprint.com',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'CPTSiteAssignmentConfirmation@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'CPT Resource Assignment'
END
GO
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'CPT Submit PDL') = 0)
BEGIN
	INSERT INTO [dbo].[LK_COWS_APP_CFG]([CFG_KEY_NME],[CFG_KEY_VALU_TXT],[CREAT_DT])
     VALUES('CPT Submit PDL','MDSOnboarding@sprint.com',GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'MDSOnboarding@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'CPT Submit PDL'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'SDE Region') = 0)
BEGIN
	INSERT INTO LK_COWS_APP_CFG (CFG_KEY_NME, CFG_KEY_VALU_TXT, CREAT_DT)
	VALUES ('SDE Region', 'Asia, Africa, Australia, Europe, Middle East, Latin America, North America, South America', GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'Asia, Africa, Australia, Europe, Middle East, Latin America, North America, South America',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'SDE Region'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'SDE Opportunity PDL') = 0)
BEGIN
	INSERT INTO LK_COWS_APP_CFG (CFG_KEY_NME, CFG_KEY_VALU_TXT, CREAT_DT)
	VALUES ('SDE Opportunity PDL', 'mss-sse.list@sprint.com', GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'mss-sse.list@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'SDE Opportunity PDL'
END
IF((SELECT COUNT(*) FROM [COWS].[dbo].[LK_COWS_APP_CFG] WHERE CFG_KEY_NME = 'RFP PDL') = 0)
BEGIN
	INSERT INTO LK_COWS_APP_CFG (CFG_KEY_NME, CFG_KEY_VALU_TXT, CREAT_DT)
	VALUES ('RFP PDL', 'RFP-IPOPSPlanning@sprint.com', GETDATE())
END
ELSE
BEGIN
	UPDATE [dbo].[LK_COWS_APP_CFG]
	SET [CFG_KEY_VALU_TXT] =  'RFP-IPOPSPlanning@sprint.com',
		[CREAT_DT] = GETDATE()
    WHERE [CFG_KEY_NME] = 'RFP PDL'
END








GO