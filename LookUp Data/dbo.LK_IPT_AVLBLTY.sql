USE COWS
GO
ALTER TABLE dbo.TRPT_ORDR NOCHECK CONSTRAINT ALL
GO
DELETE FROM dbo.LK_IPT_AVLBLTY
GO

Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(1,'1/2 Tunnel',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(2,'Add On',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(3,'ATM',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(4,'Bilateral Half',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(5,'Cabling',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(6,'Cancel',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(7,'Change Request',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(8,'Colo',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(9,'CPA',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(10,'CPE',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(11,'Disconnect',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(12,'Downgrade',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(13,'Highband',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(14,'Migration',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(15,'Move',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(16,'N/A',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(17,'No',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(18,'Other Transport',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(19,'Port Upgrade',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(20,'Rehome',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(21,'Resell',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(22,'Shared Tenant',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(23,'Upgrade',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(24,'Yes',1,1,GETDATE())
Insert into LK_IPT_AVLBLTY (IPT_AVLBLTY_ID,IPT_AVLBLTY_DES,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
Values(25,'GTS',1,1,GETDATE())

GO
ALTER TABLE dbo.TRPT_ORDR CHECK CONSTRAINT ALL
GO
