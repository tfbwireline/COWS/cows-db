USE [COWS]
GO
SET NOCOUNT ON
GO
ALTER TABLE MDS_EVENT_NEW NOCHECK CONSTRAINT ALL
GO

DELETE FROM dbo.LK_MDS_BRDG_NEED_REAS WITH (ROWLOCK)
GO

INSERT INTO LK_MDS_BRDG_NEED_REAS VALUES (0, ' ', GETDATE(), 1, GETDATE(), 1)
INSERT INTO LK_MDS_BRDG_NEED_REAS VALUES (1, 'No', GETDATE(), 1, GETDATE(), 1)
INSERT INTO LK_MDS_BRDG_NEED_REAS VALUES (2, 'Yes, Submitter', GETDATE(), 1, GETDATE(), 1)
INSERT INTO LK_MDS_BRDG_NEED_REAS VALUES (3, 'Yes, MNS', GETDATE(), 1, GETDATE(), 1)
INSERT INTO LK_MDS_BRDG_NEED_REAS VALUES (4, 'Yes, Other', GETDATE(), 1, GETDATE(), 1)

ALTER TABLE MDS_EVENT_NEW CHECK CONSTRAINT ALL
GO