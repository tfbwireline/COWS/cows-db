USE COWS
Go
SET NOCOUNT ON;
GO
ALTER TABLE dbo.ORDR_JPRDY NOCHECK CONSTRAINT ALL
GO
DELETE FROM [COWS].[dbo].[LK_JPRDY]
GO
INSERT INTO [COWS].[dbo].[LK_JPRDY]([JPRDY_CD],[JPRDY_DES],[ORDR_CAT_ID]) VALUES ('100' ,'Access contract term has not been fulfilled on disconnect' ,1)
INSERT INTO [COWS].[dbo].[LK_JPRDY]([JPRDY_CD],[JPRDY_DES],[ORDR_CAT_ID]) VALUES ('101' ,'Additional Charge Request - Early termination fees on disconnect' ,1)
INSERT INTO [COWS].[dbo].[LK_JPRDY]([JPRDY_CD],[JPRDY_DES],[ORDR_CAT_ID]) VALUES ('102' ,'GOM reject - RTS' ,1)
INSERT INTO [COWS].[dbo].[LK_JPRDY]([JPRDY_CD],[JPRDY_DES],[ORDR_CAT_ID]) VALUES ('103' ,'xNCI reject - RTS' ,1)
INSERT INTO [COWS].[dbo].[LK_JPRDY]([JPRDY_CD],[JPRDY_DES],[ORDR_CAT_ID]) VALUES ('104' ,'Additional Charge Request - Need sales support approval' ,1)
INSERT INTO [COWS].[dbo].[LK_JPRDY]([JPRDY_CD],[JPRDY_DES],[ORDR_CAT_ID]) VALUES ('105' ,'Additional Charge Request - Need sales suuport approval reminder' ,1)
INSERT INTO [COWS].[dbo].[LK_JPRDY]([JPRDY_CD],[JPRDY_DES],[ORDR_CAT_ID]) VALUES ('201' ,'FSA Reject Code' ,2)
GO
ALTER TABLE dbo.ORDR_JPRDY CHECK CONSTRAINT ALL
GO


