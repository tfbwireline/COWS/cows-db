USE COWS
Go

ALTER TABLE dbo.IPL_ORDR NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.USER_WFM NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.LK_PPRT NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.ORDR_GRP NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.PROD_ORDR_TYPE NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.NCCO_ORDR NOCHECK CONSTRAINT ALL
GO
DELETE FROM dbo.LK_ORDR_TYPE
GO

INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(1,'Install',GETDATE(),'IN','1')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(2,'Upgrade',GETDATE(),'UP','1')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(3,'Downgrade',GETDATE(),'DN','1')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(4,'Move',GETDATE(),'MV','1')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(5,'Change',GETDATE(),'CH','1')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(6,'Billing Only',GETDATE(),'BC','1')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(7,'Disconnect',GETDATE(),'DC','0.5')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(8,'Cancel',GETDATE(),'CN','1')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(9,'Add-On',GETDATE(),'AD','1')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(10,'Access Refresh',GETDATE(),'AR','1')
INSERT INTO dbo.LK_ORDR_TYPE(ORDR_TYPE_ID,ORDR_TYPE_DES,CREAT_DT,FSA_ORDR_TYPE_CD,XNCI_WFM_ORDR_WEIGHTAGE_NBR) VALUES(10,'Access Renewal',GETDATE(),'AN','1')

ALTER TABLE dbo.IPL_ORDR CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.LK_PPRT CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.ORDR_GRP CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.PROD_ORDR_TYPE CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.USER_WFM CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.NCCO_ORDR CHECK CONSTRAINT ALL
GO