USE COWS
GO

DELETE FROM dbo.LK_OPT_OUT_REAS WITH (ROWLOCK)

Insert into dbo.LK_OPT_OUT_REAS (OPT_OUT_REAS_ID,OPT_OUT_REAS_DES,REC_STUS_ID,CREAT_DT) Values (1,'Required Time is outside of FastTrack operational hours',1,GETDATE())
Insert into dbo.LK_OPT_OUT_REAS (OPT_OUT_REAS_ID,OPT_OUT_REAS_DES,REC_STUS_ID,CREAT_DT) Values (2,'Complex coordination requiring outside vendor, etc',1,GETDATE())
Insert into dbo.LK_OPT_OUT_REAS (OPT_OUT_REAS_ID,OPT_OUT_REAS_DES,REC_STUS_ID,CREAT_DT) Values (3,'Unique skill set required',1,GETDATE())
Insert into dbo.LK_OPT_OUT_REAS (OPT_OUT_REAS_ID,OPT_OUT_REAS_DES,REC_STUS_ID,CREAT_DT) Values (4,'NCR/Orange Dispatch Required',1,GETDATE())
Insert into dbo.LK_OPT_OUT_REAS (OPT_OUT_REAS_ID,OPT_OUT_REAS_DES,REC_STUS_ID,CREAT_DT) Values (5,'None of the above',1,GETDATE())
GO