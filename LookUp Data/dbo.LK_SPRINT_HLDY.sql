USE COWS
DELETE FROM LK_SPRINT_HLDY WITH (TABLOCKX)
GO

DELETE FROM dbo.LK_SPRINT_HLDY
GO
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(1,'01/17/2011','Martin L. King, Jr. Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(2,'05/30/2011','Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(3,'07/04/2011','Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(4,'09/05/2011','Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(5,'11/24/2011','Thanksgiving Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(6,'11/25/2011','Day After Thanksgiving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(7,'12/26/2011','Christmas Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(8,'01/02/2012','New Year''s Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(9,'01/16/2012','Martin Luther King Birthday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(10,'05/28/2012','Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(11,'07/04/2012','Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(12,'09/03/2012','Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(13,'11/22/2012','Thanksgiving Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(14,'11/23/2012','Day after Thanksgiving Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(15,'12/25/2012','Christmas Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(16,'01/01/2013','New Years Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(17,'01/21/2013','MLK Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(18,'05/27/2013','Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(19,'07/04/2013','Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(20,'09/02/2013','Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(21,'11/28/2013','Thanksgiving Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(23,'12/25/2013','Christmas Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(24,'01/01/2014','New Years Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(25,'01/20/2014','MLK Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(26,'05/26/2014','Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(27,'07/04/2014','Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(28,'09/01/2014','Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(29,'11/27/2014','Thanksgiving Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(30,'11/28/2014','Day after T''giving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(31,'12/25/2014','Christmas Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(32,'01/01/2015','New Years Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(33,'01/19/2015','MLK Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(34,'05/25/2015','Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(35,'07/03/2015','Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(36,'09/07/2015','Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(37,'11/26/2015','Thanksgiving Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(38,'11/27/2015','Day after Thanksgiving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(39,'12/25/2015','Christmas Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(40,'01/01/2016','New Years Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(41,'01/18/2016','MLK Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(42,'05/30/2016','Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(43,'07/04/2016','Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(44,'09/05/2016','Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(45,'11/24/2016','Thanksgiving Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(46,'11/25/2016','Day after Thanksgiving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(47,'12/26/2016','Christmas Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(48,'01/02/2017','New Years Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(49,'01/16/2017','2017 MLK',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(50,'05/29/2017','2017 Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(51,'07/04/2017','2017 Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(52,'09/04/2017','2017 Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(53,'11/23/2017','2017 Thanksgiving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(54,'11/24/2017','2017 Day After T''giving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(55,'12/25/2017','2017 Christmas Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(56,'01/01/2018','2018 New Years Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(57,'01/15/2018','2018 Martin Luther King Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(58,'05/28/2018','2018 Memorial Day ',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(59,'11/22/2018','2018 Thanksgiving Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(60,'11/23/2018','2018 Day after Thanksgiving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(61,'12/25/2018','2018 Christmas Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(62,'07/04/2018','2018 Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(63,'09/03/2018','2018 Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(64,'01/01/2019','2019 New Years Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(65,'01/21/2019','2019 Martin Luther King Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(66,'05/27/2019','2019 Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(67,'07/04/2019','2019 Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(68,'09/02/2019','2019 Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(69,'11/28/2019','2019 Thanksgiving Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(70,'11/29/2019','2019 Day after Thanksgiving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(71,'12/25/2019','2019 Christmas Holiday',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(72,'01/01/2020','New Years Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(73,'01/20/2020','MLK Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(74,'05/25/2020','Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(75,'07/03/2020','Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(76,'11/26/2020','Thanksgiving Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(77,'11/27/2020','Day after T''giving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(78,'12/25/2020','Christmas Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(79,'09/07/2020','Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(80,'11/11/2019','Veterans Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(81,'11/11/2020','Veterans Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(82,'01/01/2021','New Years Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(83,'01/18/2021','MLK Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(84,'05/31/2021','Memorial Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(85,'07/05/2021','Independence Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(86,'09/06/2021','Labor Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(87,'11/11/2021','Veterans Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(88,'11/25/2021','Thanksgiving Day',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(89,'11/26/2021','Day after T''giving',1,1,NULL,NULL,GETDATE())
INSERT INTO dbo.LK_SPRINT_HLDY(SPRINT_HLDY_ID,SPRINT_HLDY_DT,SPRINT_HLDY_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)       VALUES(90,'12/24/2021','Christmas Day',1,1,NULL,NULL,GETDATE())

GO
