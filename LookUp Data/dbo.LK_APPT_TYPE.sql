USE [COWS]
GO
SET NOCOUNT ON;
GO
ALTER TABLE dbo.LK_APPT_TYPE NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.APPT_RCURNC_DATA NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.APPT NOCHECK CONSTRAINT ALL
GO
DELETE FROM dbo.LK_APPT_TYPE WITH (TABLOCKX)
GO
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (1, 'AD Broadband', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (2, 'AD Narrowband', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (3, 'AD Government', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (4, 'MDS', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (5, 'MDS FT', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (6, 'MPLS', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (7, 'NGVN', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (8, 'SLNK', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (9, 'On Shift', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (10, 'AD Shift', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (11, 'ATM Shift', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (12, 'MDS Scheduled Shift', 1, 1, 1, GETDATE(), GETDATE())
--INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (13, 'MDS FastTrack Shift', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (14, 'NGVN Shift', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (15, 'MPLS Shift', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (16, 'SLNK Shift', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (17, 'Preparation', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (18, 'Lunch', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (19, 'Personal', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (20, 'Vacation', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (21, 'Comp', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (22, 'Meeting', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (23, 'Training', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (24, 'Project', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (25, 'Schedule Block', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (26, 'Conf. Call', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (27, 'Implementation', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (28, 'AD International', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (29, 'MPLS VAS', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (30, 'Fedline', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (31, 'Fedline Shift', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (32, 'AD TMT', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (33, 'UCaaS Shift', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (34, 'UCaaS', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (35, 'SIPT', 1, 1, 1, GETDATE(), GETDATE())
-- INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (36, 'Intl. MDS', 1, 1, 1, GETDATE(), GETDATE())
INSERT INTO dbo.LK_APPT_TYPE (APPT_TYPE_ID, APPT_TYPE_DES, REC_STUS_ID, CREAT_BY_USER_ID, MODFD_BY_USER_ID, MODFD_DT, CREAT_DT) VALUES (37, 'Network Intl', 1, 1, 1, GETDATE(), GETDATE())
GO
ALTER TABLE dbo.LK_APPT_TYPE CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.APPT_RCURNC_DATA CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.APPT CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.APPT_RCURNC_DATA CHECK CONSTRAINT ALL
GO
