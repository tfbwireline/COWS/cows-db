USE COWS
GO
ALTER TABLE dbo.QLFCTN_ENHNC_SRVC NOCHECK CONSTRAINT ALL 
GO
ALTER TABLE dbo.AD_EVENT NOCHECK CONSTRAINT ALL 
GO
DELETE FROM dbo.LK_ENHNC_SRVC WITH (TABLOCKX)
INSERT INTO dbo.LK_ENHNC_SRVC
		SELECT 1, 'STANDARD', 3, 120, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 2, 'VAS', 3, 0, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 3, 'SLNK STANDARD', 4, 60, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 4, 'NGVN STANDARD', 2, 60, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 5, 'IPSD', 5, 120, 1, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 6, 'Firewall/Security', 5, 0, 1, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 7, 'AD BROADBAND', 1, 60, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 8, 'AD NARROWBAND', 1, 60, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 9, 'AD GOVERNMENT', 1, 60, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 10, 'MSS', 5, 0, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 11, 'AD INTERNATIONAL', 1, 60, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 12, 'AD TMT', 1, 60, 0, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 13, 'UCaaS', 19, 120, 1, 1, 1, GETDATE(), GETDATE()
UNION	SELECT 14, 'VAS NETWORK', 5, 0, 1, 1, 1, GETDATE(), GETDATE()

ALTER TABLE dbo.QLFCTN_ENHNC_SRVC CHECK CONSTRAINT ALL 
GO
ALTER TABLE dbo.AD_EVENT CHECK CONSTRAINT ALL 
GO	
	
