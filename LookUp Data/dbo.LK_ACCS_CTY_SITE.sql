USE [COWS]
GO
SET NOCOUNT ON
GO
ALTER TABLE dbo.TRPT_ORDR NOCHECK CONSTRAINT ALL 
GO
DELETE FROM dbo.LK_ACCS_CTY_SITE WITH (ROWLOCK)
GO

INSERT INTO dbo.LK_ACCS_CTY_SITE(ACCS_CTY_SITE_ID,ACCS_CTY_NME_SITE_CD, CREAT_DT)
		  SELECT 200, 'Amsterdam 1022', GETDATE()
UNION ALL SELECT 201, 'Brussels 1020', GETDATE()
UNION ALL SELECT 202, 'Berlin 1073', GETDATE()
UNION ALL SELECT 203, 'Copenhagen 1025', GETDATE()
UNION ALL SELECT 204, 'Dublin 8289', GETDATE()
UNION ALL SELECT 205, 'Dusseldorf 1071', GETDATE()
UNION ALL SELECT 206, 'Frankfurt 1021', GETDATE()
UNION ALL SELECT 207, 'Frankfurt 1055', GETDATE()
UNION ALL SELECT 208, 'Hamburg 8296', GETDATE()
UNION ALL SELECT 209, 'London 1015', GETDATE()
UNION ALL SELECT 210, 'London 1019', GETDATE()
UNION ALL SELECT 211, 'Milan 8294', GETDATE()
UNION ALL SELECT 212, 'Munich 1060', GETDATE()
UNION ALL SELECT 213, 'Oslo 1066', GETDATE()
UNION ALL SELECT 214, 'Paris 1027', GETDATE()
UNION ALL SELECT 215, 'Stockholm 1028', GETDATE()
UNION ALL SELECT 216, 'Stockholm 1018', GETDATE()
UNION ALL SELECT 217, 'Stuttgart 1068', GETDATE()
UNION ALL SELECT 218, 'Vienna 1067', GETDATE()
UNION ALL SELECT 219, 'Zurich 1065', GETDATE()
UNION ALL SELECT 100, 'Buenos Aires 1079', GETDATE()
UNION ALL SELECT 101, 'Caracas 1017', GETDATE()
UNION ALL SELECT 102, 'Medell�n 1075', GETDATE()
UNION ALL SELECT 103, 'Mexico City 8872', GETDATE()
UNION ALL SELECT 104, 'Monterrey 1013', GETDATE()
UNION ALL SELECT 105, 'Rio de Janeiro 5901', GETDATE()
UNION ALL SELECT 106, 'Santiago 8868', GETDATE()
UNION ALL SELECT 107, 'Sao Paulo 8876', GETDATE()
UNION ALL SELECT 108, 'Toronto 8873', GETDATE()
UNION ALL SELECT 109, 'Vancouver 1166', GETDATE()
UNION ALL SELECT 300, 'Sydney 1032', GETDATE()
UNION ALL SELECT 301, 'Tokyo OBD 1037', GETDATE()
UNION ALL SELECT 302, 'Hong Kong 1038', GETDATE()
UNION ALL SELECT 303, 'Melbourne 1063', GETDATE()
UNION ALL SELECT 304, 'Singapore EQ 1036', GETDATE()
UNION ALL SELECT 305, 'Singapore GS 8875', GETDATE()
UNION ALL SELECT 306, 'Tokyo OTH 1069', GETDATE()
UNION ALL SELECT 307, 'Auckland 1062', GETDATE()
UNION ALL SELECT 308, 'Taipei 1064', GETDATE()
UNION ALL SELECT 309, 'Seoul 1076', GETDATE()
UNION ALL SELECT 310, 'New Delhi 1080', GETDATE()
UNION ALL SELECT 311, 'Mumbai 1081', GETDATE()
UNION ALL SELECT 312, 'Bangalore 1082', GETDATE()
UNION ALL SELECT 313, 'Manilas 1072', GETDATE()
UNION ALL SELECT 314, 'Hyderabad 1191', GETDATE()
UNION ALL SELECT 315, 'Prague 1089', GETDATE()
UNION ALL SELECT 316, 'Budapest 1088', GETDATE()
UNION ALL SELECT 317, 'Kuala Lumpur 1087', GETDATE()
UNION ALL SELECT 318, 'India  Chennai 1090', GETDATE()
UNION ALL SELECT 400, 'Amsterdam 1012', GETDATE()
UNION ALL SELECT 401, 'Amsterdam 1023', GETDATE()
UNION ALL SELECT 402, 'Anaheim 3085', GETDATE()
UNION ALL SELECT 403, 'Bangkok 1093', GETDATE()
UNION ALL SELECT 404, 'Bogota 1091', GETDATE()
UNION ALL SELECT 405, 'Budapest 1088', GETDATE()
UNION ALL SELECT 406, 'Buenos Aires 1083', GETDATE()
UNION ALL SELECT 407, 'Calgary 1049', GETDATE()
UNION ALL SELECT 408, 'Chennai 1090', GETDATE()
UNION ALL SELECT 409, 'Dubai 1099', GETDATE()
UNION ALL SELECT 410, 'Finland VPOP 91028', GETDATE()
UNION ALL SELECT 411, 'Fort Worth 2040', GETDATE()
UNION ALL SELECT 412, 'Geneva 1063', GETDATE()
UNION ALL SELECT 413, 'Guadalajara 1097', GETDATE()
UNION ALL SELECT 414, 'Guam 3001', GETDATE()
UNION ALL SELECT 415, 'Hyderabad 1191', GETDATE()
UNION ALL SELECT 416, 'Indonesia VPOP 91036', GETDATE()
UNION ALL SELECT 417, 'Israel VPOP 91015', GETDATE()
UNION ALL SELECT 418, 'Kuala Lumpur 1087', GETDATE()
UNION ALL SELECT 419, 'Madrid 1074', GETDATE()
UNION ALL SELECT 420, 'Manila TIM 1101', GETDATE()
UNION ALL SELECT 421, 'Melbourne 1086', GETDATE()
UNION ALL SELECT 422, 'Mexico City 8871', GETDATE()
UNION ALL SELECT 423, 'Miami 8308', GETDATE()
UNION ALL SELECT 424, 'Montreal 1026', GETDATE()
UNION ALL SELECT 425, 'Moscow 1092', GETDATE()
UNION ALL SELECT 426, 'New York 6520', GETDATE()
UNION ALL SELECT 427, 'Osaka 1095', GETDATE()
UNION ALL SELECT 428, 'Perth 1077', GETDATE()
UNION ALL SELECT 429, 'Phoenix 1035', GETDATE()
UNION ALL SELECT 430, 'Prague 1089', GETDATE()
UNION ALL SELECT 431, 'Slough 1085', GETDATE()
UNION ALL SELECT 432, 'Stockton 2090', GETDATE()
UNION ALL SELECT 433, 'Tacoma 1078', GETDATE()
UNION ALL SELECT 434, 'Tijuana 1059', GETDATE()
UNION ALL SELECT 435, 'Vietnam VPOP 91038', GETDATE()
UNION ALL SELECT 436, 'Warsaw 1084', GETDATE()



GO

ALTER TABLE dbo.TRPT_ORDR CHECK CONSTRAINT ALL 
GO

