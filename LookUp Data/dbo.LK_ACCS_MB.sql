
Use COWS
 GO
 --delete dbo.lk_accs_mb
 --GO
 --set identity_insert dbo.lk_accs_mb on

 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(1, '64k', getdate(),1)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(2, '128k', getdate(),2)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(3, '192k', getdate(),3)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(4, '256k', getdate(),4)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(5, '384k', getdate(),5)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(6, '512k', getdate(),6)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(7, '1024k', getdate(),7)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(8, 't1 (1536k) tdm', getdate(),8)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(9, 't1 (1544k) tdm', getdate(),9)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(10, '3 m (2xt1) tdm', getdate(),10)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(11, '4.5 m (3xt1) tdm', getdate(),11)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(12, '6 m (4xt1) tdm', getdate(),12)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(13, '9 m (6xt1) tdm', getdate(),13)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(14, 'e1 (1984k) tdm', getdate(),14)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(15, 'e1 (2048k) tdm', getdate(),15)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(16, '4 m (2xe1) tdm', getdate(),16)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(17, '6 m (3xe1) tdm', getdate(),17)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(18, '8 m (4xe1) tdm', getdate(),18)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(19, '10 m (5xe1) tdm', getdate(),19)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(20, '34 m (e3)', getdate(),20)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(21, '45 m (ds3)', getdate(),21)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(22, '155 m (stm1)', getdate(),22)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(23, '622 m (stm4)', getdate(),23)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(24, '2 m eth', getdate(),24)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(25, '4 m eth', getdate(),25)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(26, '5 m eth', getdate(),26)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(27, '6 m eth', getdate(),27)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(28, '8 m eth', getdate(),28)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(29, '10 m eth', getdate(),29)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(30, '15 m eth', getdate(),30)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(31, '20 m eth', getdate(),31)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(32, '30 m eth', getdate(),32)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(33, '40 m eth', getdate(),33)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(34, '50 m eth', getdate(),34)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(35, '60 m eth', getdate(),35)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(36, '80 m eth', getdate(),36)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(37, '100 m eth', getdate(),37)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(38, '200 m eth', getdate(),38)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(39, '400 m eth', getdate(),39)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(40, '500 m eth', getdate(),40)
 --insert into dbo.lk_accs_mb(accs_mb_id,accs_mb_des,creat_dt,sort_id) values(41, '1 gig eth ', getdate(),41)

--PJ017929 Changes
--ASR_TYPE = Aggregated
IF NOT EXISTS
	(SELECT 'X' FROM LK_ACCS_MB WHERE ASR_TYPE_ID = 1)
	BEGIN
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('1M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('2M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('4M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('5M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('6M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('8M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('10M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('20M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('30M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('40M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('50M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('60M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('70M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('80M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('100M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('200M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('300M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('400M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('500M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('600M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('700M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('800M', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('1G (GigE)', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('10G (GigE)', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('40G (GigE)', GETDATE(),null,1)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('100G (GigE)', GETDATE(),null,1)	
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('2G (GigE)', GETDATE(),null,1)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('3G (GigE)', GETDATE(),null,1)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('4G (GigE)', GETDATE(),null,1)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('5G (GigE)', GETDATE(),null,1)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('6G (GigE)', GETDATE(),null,1)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('7G (GigE)', GETDATE(),null,1)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('8G (GigE)', GETDATE(),null,1)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('9G (GigE)', GETDATE(),null,1)

	END

--ASR Type = Dedicated
IF NOT EXISTS
	(SELECT 'X' FROM LK_ACCS_MB WHERE ASR_TYPE_ID = 2)
	BEGIN
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('10M', GETDATE(),null,2)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('100M (FastE)', GETDATE(),null,2)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('1G (GigE)', GETDATE(),null,2)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('10G (GigE)', GETDATE(),null,2)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('40G (GigE)', GETDATE(),null,2)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('100G (GigE)', GETDATE(),null,2)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('2G (GigE)', GETDATE(),null,2)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('3G (GigE)', GETDATE(),null,2)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('4G (GigE)', GETDATE(),null,2)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('5G (GigE)', GETDATE(),null,2)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('6G (GigE)', GETDATE(),null,2)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('7G (GigE)', GETDATE(),null,2)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('8G (GigE)', GETDATE(),null,2)
	    Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('9G (GigE)', GETDATE(),null,2)

	END

--ASR Type = TDM
IF NOT EXISTS
	(SELECT 'X' FROM LK_ACCS_MB WHERE ASR_TYPE_ID = 3)
	BEGIN
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('T1', GETDATE(),null,3)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('E1', GETDATE(),null,3)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('NXT1', GETDATE(),null,3)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('NXE1', GETDATE(),null,3)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('E3', GETDATE(),null,3)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('DS3', GETDATE(),null,3)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('OC3 / STM1 / STS-3', GETDATE(),null,3)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('OC12 / STM4 / STS-12', GETDATE(),null,3)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('OC48 / STM16 /STS-48', GETDATE(),null,3)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('OC192 / STM64 / STS-192', GETDATE(),null,3)		
	END	




--ASR Type = IPL/IP
IF NOT EXISTS
	(SELECT 'X' FROM LK_ACCS_MB WHERE ASR_TYPE_ID = 4)
	BEGIN
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('T1', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('E1', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('NXT1', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('NXE1', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('E3', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('DS3', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('OC3 / STM1 / STS-3', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('OC12 / STM4 / STS-12', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('OC48 / STM16 /STS-48', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('OC192 / STM64 / STS-192', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('1M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('2M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('4M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('5M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('6M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('8M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('10M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('20M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('30M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('40M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('50M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('60M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('70M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('80M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('100M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('200M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('300M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('400M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('500M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('600M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('700M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('800M', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('1G (GigE)', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('10G (GigE)', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('40G (GigE)', GETDATE(),null,4)
		Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('100G (GigE)', GETDATE(),null,4)	
	END

Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('9M ETH', GETDATE(),null,null)
Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('90M ETH', GETDATE(),null,null)
Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('20G (GigE)', GETDATE(),null,null)
Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('30G (GigE)', GETDATE(),null,null)
Insert into dbo.LK_ACCS_MB(ACCS_MB_DES,CREAT_DT,SORT_ID,ASR_TYPE_ID) Values('50G (GigE)', GETDATE(),null,null)
--Select * From LK_ASR_TYPE


SET IDENTITY_INSERT dbo.LK_ACCS_MB Off

Go


