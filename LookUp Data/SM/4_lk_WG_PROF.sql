USE COWS
GO
ALTER TABLE dbo.ACT_TASK NOCHECK CONSTRAINT ALL
GO
--ALTER TABLE arch.ACT_TASK NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.WG_PROF_SM NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.WG_PROF_STUS NOCHECK CONSTRAINT ALL
GO

--ALTER TABLE arch.WG_PROF_STUS NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.WG_PTRN_SM NOCHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.WG_PTRN_STUS NOCHECK CONSTRAINT ALL
GO

--ALTER TABLE arch.WG_PTRN_STUS NOCHECK CONSTRAINT ALL
GO
DELETE FROM dbo.lk_WG_PROF
GO

--Start and End profiles
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (0,'Start',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (1,'Finish',GETDATE())

--GOM
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (100,'Pre-Submit',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (101,'Submit',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (102,'NRM/GOM Private Line',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (103,'IPL GOM Review',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (104,'GOM CCD Ready',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (105,'GOM CCD Ready/PL',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (106,'GOM Cancel Ready',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (107,'GOM Private Line',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (108,'GOM IPL+IP/NRM/PL',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (109,'GOM IPL+IP/PL',GETDATE())

--xNCI
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (200,'xNCI',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (201,'xNCI Ready/Sales Support',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (202,'xNCI CCD Ready',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (203,'xNCI Ready',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (204,'xNCI Transport Bill Clear',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (205,'xNCI Cancel Ready',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (206,'xNCI Customer Accepted/Turn up',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (207,'xNCI Disc MS',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (208,'xNCI Billing Only',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (210,'xNCI IPL Order Install Date',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (211,'xNCI Change Order Abbr Milestones',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (212,'xNCI Change Order Abbr Milestones IPL',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (213,'xNCI Vendor Confirm Disc',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (214,'xNCI Disconnect Complete',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (215,'xNCI Ready/GOM Fallout',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (216,'xNCI Customer Turnup Date',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (217,'xNCI Circuit MS Steps',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (218,'xNCI Access Renewal',GETDATE())

--MDS
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (300,'MDS',GETDATE())


--CSC
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (400,'CSC',GETDATE())


--Sales Support
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (500,'SS RTS',GETDATE())

--DCPE
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (600,'Mach5 CPE Steps',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (601,'Mach5 CPE Equip Review',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (602,'Mach5 CPE Equip Review & Mat Requisition',GETDATE())
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (603,'Mach5 CPE Equip Review w/o CPE Hold',GETDATE())

--System
INSERT INTO dbo.lk_WG_PROF (WG_PROF_ID,WG_PROF_DES,CREAT_DT)
VALUES (1000,'System',GETDATE())

GO
ALTER TABLE dbo.ACT_TASK CHECK CONSTRAINT ALL
GO

--ALTER TABLE arch.ACT_TASK CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.WG_PROF_SM CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.WG_PROF_STUS CHECK CONSTRAINT ALL
GO

--ALTER TABLE arch.WG_PROF_STUS CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.WG_PTRN_SM CHECK CONSTRAINT ALL
GO
ALTER TABLE dbo.WG_PTRN_STUS CHECK CONSTRAINT ALL
--ALTER TABLE arch.WG_PTRN_STUS CHECK CONSTRAINT ALL
GO