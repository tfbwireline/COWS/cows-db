/****** Script for SelectTopNRows command from SSMS  ******/
USE [COWS]

Truncate TABLE [dbo].[LK_M5_MSG]
  
INSERT INTO [dbo].[LK_M5_MSG]([M5_MSG_ID],[M5_MSG_DES],[CREAT_DT])
     VALUES(1,'CPE Complete',getdate())         
GO
INSERT INTO [dbo].[LK_M5_MSG]([M5_MSG_ID],[M5_MSG_DES],[CREAT_DT])
     VALUES(2,'MNS Complete',getdate())         
GO
INSERT INTO [dbo].[LK_M5_MSG]([M5_MSG_ID],[M5_MSG_DES],[CREAT_DT])
     VALUES(3,'MSS Complete',getdate())         
GO
INSERT INTO [dbo].[LK_M5_MSG]([M5_MSG_ID],[M5_MSG_DES],[CREAT_DT])
     VALUES(4,'CPE RTS',getdate()) 
GO
INSERT INTO [dbo].[LK_M5_MSG]([M5_MSG_ID],[M5_MSG_DES],[CREAT_DT])
     VALUES(5,'CPE PO# Return',getdate()) 
GO
INSERT INTO [dbo].[LK_M5_MSG]([M5_MSG_ID],[M5_MSG_DES],[CREAT_DT])
     VALUES(6,'ACCESS Complete',getdate()) 
GO
INSERT INTO [dbo].[LK_M5_MSG]([M5_MSG_ID],[M5_MSG_DES],[CREAT_DT])
     VALUES(7,'ISMV Complete',getdate()) 
         
GO

