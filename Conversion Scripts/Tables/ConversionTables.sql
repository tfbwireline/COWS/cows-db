USE [COWS]
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__AccessDel__Statu__09AB0850]') AND parent_object_id = OBJECT_ID(N'[cnv].[AccessDelivery]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessDel__Statu__09AB0850]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[AccessDelivery] DROP CONSTRAINT [DF__AccessDel__Statu__09AB0850]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__EVENT_Tmp__Statu__73BBC731]') AND parent_object_id = OBJECT_ID(N'[cnv].[EVENT_Tmp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__EVENT_Tmp__Statu__73BBC731]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[EVENT_Tmp] DROP CONSTRAINT [DF__EVENT_Tmp__Statu__73BBC731]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__MDSEvent__Status__1F9A496F]') AND parent_object_id = OBJECT_ID(N'[cnv].[MDSEvent]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MDSEvent__Status__1F9A496F]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[MDSEvent] DROP CONSTRAINT [DF__MDSEvent__Status__1F9A496F]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__MDSFastTr__Statu__75A40FA3]') AND parent_object_id = OBJECT_ID(N'[cnv].[MDSFastTrack]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MDSFastTr__Statu__75A40FA3]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[MDSFastTrack] DROP CONSTRAINT [DF__MDSFastTr__Statu__75A40FA3]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__MPLSEvent__Statu__769833DC]') AND parent_object_id = OBJECT_ID(N'[cnv].[MPLSEvent]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MPLSEvent__Statu__769833DC]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[MPLSEvent] DROP CONSTRAINT [DF__MPLSEvent__Statu__769833DC]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__NGVNEvent__Statu__778C5815]') AND parent_object_id = OBJECT_ID(N'[cnv].[NGVNEvent]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NGVNEvent__Statu__778C5815]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[NGVNEvent] DROP CONSTRAINT [DF__NGVNEvent__Statu__778C5815]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__ORDR_Tmp__Status__7974A087]') AND parent_object_id = OBJECT_ID(N'[cnv].[ORDR_Tmp]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ORDR_Tmp__Status__7974A087]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[ORDR_Tmp] DROP CONSTRAINT [DF__ORDR_Tmp__Status__7974A087]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__Qualifica__Statu__78807C4E]') AND parent_object_id = OBJECT_ID(N'[cnv].[Qualifications]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Qualifica__Statu__78807C4E]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[Qualifications] DROP CONSTRAINT [DF__Qualifica__Statu__78807C4E]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__SOWSActiv__Statu__7A68C4C0]') AND parent_object_id = OBJECT_ID(N'[cnv].[SOWSActivators]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__SOWSActiv__Statu__7A68C4C0]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[SOWSActivators] DROP CONSTRAINT [DF__SOWSActiv__Statu__7A68C4C0]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__SOWSRevie__Statu__7B5CE8F9]') AND parent_object_id = OBJECT_ID(N'[cnv].[SOWSReviewvers]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__SOWSRevie__Statu__7B5CE8F9]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[SOWSReviewvers] DROP CONSTRAINT [DF__SOWSRevie__Statu__7B5CE8F9]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__SOWSUsers__Statu__7C510D32]') AND parent_object_id = OBJECT_ID(N'[cnv].[SOWSUsers]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__SOWSUsers__Statu__7C510D32]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[SOWSUsers] DROP CONSTRAINT [DF__SOWSUsers__Statu__7C510D32]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__SprintLin__Statu__7D45316B]') AND parent_object_id = OBJECT_ID(N'[cnv].[SprintLink]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__SprintLin__Statu__7D45316B]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[SprintLink] DROP CONSTRAINT [DF__SprintLin__Statu__7D45316B]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__Subscript__Statu__7E3955A4]') AND parent_object_id = OBJECT_ID(N'[cnv].[Subscriptions]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Subscript__Statu__7E3955A4]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[Subscriptions] DROP CONSTRAINT [DF__Subscript__Statu__7E3955A4]
END


End
GO
IF  EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__TmpUsers__Status__7F2D79DD]') AND parent_object_id = OBJECT_ID(N'[cnv].[TmpUsers]'))
Begin
IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__TmpUsers__Status__7F2D79DD]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[TmpUsers] DROP CONSTRAINT [DF__TmpUsers__Status__7F2D79DD]
END


End
GO
/****** Object:  Table [cnv].[AccessDelivery]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[AccessDelivery]') AND type in (N'U'))
DROP TABLE [cnv].[AccessDelivery]
GO
/****** Object:  Table [cnv].[AD_SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[AD_SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
DROP TABLE [cnv].[AD_SAVE_UNMASKED_CUST_DATA]
GO
/****** Object:  Table [cnv].[ADAccessTable]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[ADAccessTable]') AND type in (N'U'))
DROP TABLE [cnv].[ADAccessTable]
GO
/****** Object:  Table [cnv].[EVENT_Tmp]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[EVENT_Tmp]') AND type in (N'U'))
DROP TABLE [cnv].[EVENT_Tmp]
GO
/****** Object:  Table [cnv].[EventCalender]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[EventCalender]') AND type in (N'U'))
DROP TABLE [cnv].[EventCalender]
GO
/****** Object:  Table [cnv].[EventCalender_Main]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[EventCalender_Main]') AND type in (N'U'))
DROP TABLE [cnv].[EventCalender_Main]
GO
/****** Object:  Table [cnv].[FSA_ORDR_CUST]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[FSA_ORDR_CUST]') AND type in (N'U'))
DROP TABLE [cnv].[FSA_ORDR_CUST]
GO
/****** Object:  Table [cnv].[MDSAccessTag]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSAccessTag]') AND type in (N'U'))
DROP TABLE [cnv].[MDSAccessTag]
GO
/****** Object:  Table [cnv].[MDSActivityMAC]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSActivityMAC]') AND type in (N'U'))
DROP TABLE [cnv].[MDSActivityMAC]
GO
/****** Object:  Table [cnv].[MDSCircuit]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSCircuit]') AND type in (N'U'))
DROP TABLE [cnv].[MDSCircuit]
GO
/****** Object:  Table [cnv].[MDSEvent]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSEvent]') AND type in (N'U'))
DROP TABLE [cnv].[MDSEvent]
GO
/****** Object:  Table [cnv].[MDSFastckt]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastckt]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastckt]
GO
/****** Object:  Table [cnv].[MDSFastDiscOE]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastDiscOE]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastDiscOE]
GO
/****** Object:  Table [cnv].[MDSFastDisconnectOE]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastDisconnectOE]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastDisconnectOE]
GO
/****** Object:  Table [cnv].[MDSFastMngdDev]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastMngdDev]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastMngdDev]
GO
/****** Object:  Table [cnv].[MDSFastOCT]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastOCT]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastOCT]
GO
/****** Object:  Table [cnv].[MDSFastOE]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastOE]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastOE]
GO
/****** Object:  Table [cnv].[MDSFastTrack]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastTrack]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastTrack]
GO
/****** Object:  Table [cnv].[MDSFastVirtual]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastVirtual]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastVirtual]
GO
/****** Object:  Table [cnv].[MDSFastWiredTrspt]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastWiredTrspt]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastWiredTrspt]
GO
/****** Object:  Table [cnv].[MDSFastWirelessTrpt]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastWirelessTrpt]') AND type in (N'U'))
DROP TABLE [cnv].[MDSFastWirelessTrpt]
GO
/****** Object:  Table [cnv].[MDSLoc]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSLoc]') AND type in (N'U'))
DROP TABLE [cnv].[MDSLoc]
GO
/****** Object:  Table [cnv].[MDSManagedDevices]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSManagedDevices]') AND type in (N'U'))
DROP TABLE [cnv].[MDSManagedDevices]
GO
/****** Object:  Table [cnv].[MDSOE]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSOE]') AND type in (N'U'))
DROP TABLE [cnv].[MDSOE]
GO
/****** Object:  Table [cnv].[MDSVirtual]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSVirtual]') AND type in (N'U'))
DROP TABLE [cnv].[MDSVirtual]
GO
/****** Object:  Table [cnv].[MDSWiredTrans]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSWiredTrans]') AND type in (N'U'))
DROP TABLE [cnv].[MDSWiredTrans]
GO
/****** Object:  Table [cnv].[MDSWirelessTrans]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSWirelessTrans]') AND type in (N'U'))
DROP TABLE [cnv].[MDSWirelessTrans]
GO
/****** Object:  Table [cnv].[MPLS_SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLS_SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
DROP TABLE [cnv].[MPLS_SAVE_UNMASKED_CUST_DATA]
GO
/****** Object:  Table [cnv].[MPLSAccessTag]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSAccessTag]') AND type in (N'U'))
DROP TABLE [cnv].[MPLSAccessTag]
GO
/****** Object:  Table [cnv].[MPLSCustomer]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSCustomer]') AND type in (N'U'))
DROP TABLE [cnv].[MPLSCustomer]
GO
/****** Object:  Table [cnv].[MPLSEvent]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSEvent]') AND type in (N'U'))
DROP TABLE [cnv].[MPLSEvent]
GO
/****** Object:  Table [cnv].[MPLSEventInbound]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSEventInbound]') AND type in (N'U'))
DROP TABLE [cnv].[MPLSEventInbound]
GO
/****** Object:  Table [cnv].[MPLSFirewall]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSFirewall]') AND type in (N'U'))
DROP TABLE [cnv].[MPLSFirewall]
GO
/****** Object:  Table [cnv].[MPLSOutbound]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSOutbound]') AND type in (N'U'))
DROP TABLE [cnv].[MPLSOutbound]
GO
/****** Object:  Table [cnv].[NGVN_SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[NGVN_SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
DROP TABLE [cnv].[NGVN_SAVE_UNMASKED_CUST_DATA]
GO
/****** Object:  Table [cnv].[NGVNCircuit]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[NGVNCircuit]') AND type in (N'U'))
DROP TABLE [cnv].[NGVNCircuit]
GO
/****** Object:  Table [cnv].[NGVNEvent]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[NGVNEvent]') AND type in (N'U'))
DROP TABLE [cnv].[NGVNEvent]
GO
/****** Object:  Table [cnv].[NGVNSIPTrk]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[NGVNSIPTrk]') AND type in (N'U'))
DROP TABLE [cnv].[NGVNSIPTrk]
GO
/****** Object:  Table [cnv].[ORDR_ADR]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[ORDR_ADR]') AND type in (N'U'))
DROP TABLE [cnv].[ORDR_ADR]
GO
/****** Object:  Table [cnv].[ORDR_CNTCT]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[ORDR_CNTCT]') AND type in (N'U'))
DROP TABLE [cnv].[ORDR_CNTCT]
GO
/****** Object:  Table [cnv].[ORDR_Tmp]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[ORDR_Tmp]') AND type in (N'U'))
DROP TABLE [cnv].[ORDR_Tmp]
GO
/****** Object:  Table [cnv].[Qualifications]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[Qualifications]') AND type in (N'U'))
DROP TABLE [cnv].[Qualifications]
GO
/****** Object:  Table [cnv].[SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
DROP TABLE [cnv].[SAVE_UNMASKED_CUST_DATA]
GO
/****** Object:  Table [cnv].[SLINK_SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SLINK_SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
DROP TABLE [cnv].[SLINK_SAVE_UNMASKED_CUST_DATA]
GO
/****** Object:  Table [cnv].[SOWS_FSA_ACCOUNT_TEAM]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_ACCOUNT_TEAM]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_ACCOUNT_TEAM]
GO
/****** Object:  Table [cnv].[SOWS_FSA_CPE_CHARGE]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CPE_CHARGE]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_CPE_CHARGE]
GO
/****** Object:  Table [cnv].[SOWS_FSA_CPE_GENERAL_INFO]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CPE_GENERAL_INFO]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_CPE_GENERAL_INFO]
GO
/****** Object:  Table [cnv].[SOWS_FSA_CPE_INFO]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CPE_INFO]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_CPE_INFO]
GO
/****** Object:  Table [cnv].[SOWS_FSA_CUST_PRICING_LINE_ITEM]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CUST_PRICING_LINE_ITEM]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_CUST_PRICING_LINE_ITEM]
GO
/****** Object:  Table [cnv].[SOWS_FSA_CUSTOMER_INFO]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CUSTOMER_INFO]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_CUSTOMER_INFO]
GO
/****** Object:  Table [cnv].[SOWS_FSA_DISC_CANCEL_INFO]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_DISC_CANCEL_INFO]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_DISC_CANCEL_INFO]
GO
/****** Object:  Table [cnv].[SOWS_FSA_MNS_Charge]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_MNS_Charge]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_MNS_Charge]
GO
/****** Object:  Table [cnv].[SOWS_FSA_MNS_INFO]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_MNS_INFO]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_MNS_INFO]
GO
/****** Object:  Table [cnv].[SOWS_FSA_MNS_Line_Item]    Script Date: 01/05/2012 10:26:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_MNS_Line_Item]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_MNS_Line_Item]
GO
/****** Object:  Table [cnv].[SOWS_FSA_ORDER_INFO]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_ORDER_INFO]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_ORDER_INFO]
GO
/****** Object:  Table [cnv].[SOWS_FSA_RELATED_CIRCUIT_LIST]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_RELATED_CIRCUIT_LIST]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_RELATED_CIRCUIT_LIST]
GO
/****** Object:  Table [cnv].[SOWS_FSA_RELATED_CIRCUITID_NUA_LIST]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_RELATED_CIRCUITID_NUA_LIST]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_RELATED_CIRCUITID_NUA_LIST]
GO
/****** Object:  Table [cnv].[SOWS_FSA_SERVICE_INFO]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_SERVICE_INFO]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_SERVICE_INFO]
GO
/****** Object:  Table [cnv].[SOWS_FSA_SUPPORT_INFO]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_SUPPORT_INFO]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_SUPPORT_INFO]
GO
/****** Object:  Table [cnv].[SOWS_FSA_Value_Added_Services_Info]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_Value_Added_Services_Info]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_Value_Added_Services_Info]
GO
/****** Object:  Table [cnv].[SOWS_FSA_Vas_Type_Group]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_Vas_Type_Group]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_FSA_Vas_Type_Group]
GO
/****** Object:  Table [cnv].[SOWS_Value_Added_Services_Info]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_Value_Added_Services_Info]') AND type in (N'U'))
DROP TABLE [cnv].[SOWS_Value_Added_Services_Info]
GO
/****** Object:  Table [cnv].[SOWSActivators]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWSActivators]') AND type in (N'U'))
DROP TABLE [cnv].[SOWSActivators]
GO
/****** Object:  Table [cnv].[SOWSReviewvers]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWSReviewvers]') AND type in (N'U'))
DROP TABLE [cnv].[SOWSReviewvers]
GO
/****** Object:  Table [cnv].[SOWSUsers]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWSUsers]') AND type in (N'U'))
DROP TABLE [cnv].[SOWSUsers]
GO
/****** Object:  Table [cnv].[SOWSUsers1]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWSUsers1]') AND type in (N'U'))
DROP TABLE [cnv].[SOWSUsers1]
GO
/****** Object:  Table [cnv].[SprintLink]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SprintLink]') AND type in (N'U'))
DROP TABLE [cnv].[SprintLink]
GO
/****** Object:  Table [cnv].[SprintLInkAccess]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SprintLInkAccess]') AND type in (N'U'))
DROP TABLE [cnv].[SprintLInkAccess]
GO
/****** Object:  Table [cnv].[Subscriptions]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[Subscriptions]') AND type in (N'U'))
DROP TABLE [cnv].[Subscriptions]
GO
/****** Object:  Table [cnv].[TmpUsers]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[TmpUsers]') AND type in (N'U'))
DROP TABLE [cnv].[TmpUsers]
GO
/****** Object:  Table [cnv].[WFHistory]    Script Date: 01/05/2012 10:26:16 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[WFHistory]') AND type in (N'U'))
DROP TABLE [cnv].[WFHistory]
GO
/****** Object:  Table [cnv].[WFHistory]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[WFHistory]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[WFHistory](
	[Event ID] [float] NULL,
	[Action] [nvarchar](255) NULL,
	[Performed By] [nvarchar](255) NULL,
	[Date and Time] [datetime] NULL,
	[Comment] [nvarchar](max) NULL,
	[Created] [datetime] NULL,
	[Created By] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Modified By] [nvarchar](255) NULL,
	[ID] [float] NULL,
	[Content Type] [nvarchar](255) NULL,
	[Exempt from Policy] [nvarchar](255) NULL,
	[Expiration Date] [datetime] NULL,
	[Fail Codes] [nvarchar](255) NULL,
	[Failed Activities] [nvarchar](255) NULL,
	[Item Title] [nvarchar](255) NULL,
	[Original Expiration Date] [datetime] NULL,
	[Succssful Activities] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[TmpUsers]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[TmpUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[TmpUsers](
	[Account] [nvarchar](100) NULL,
	[Name] [nvarchar](100) NULL,
	[eMail] [nvarchar](300) NULL,
	[ADID] [varchar](100) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[Subscriptions]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[Subscriptions]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[Subscriptions](
	[Conf#Bridge] [nvarchar](255) NULL,
	[Conf Bridge Pin] [nvarchar](255) NULL,
	[SOI] [nvarchar](255) NULL,
	[Created By] [nvarchar](255) NULL,
	[Modified By] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[ID] [float] NULL,
	[Item Title] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[SprintLInkAccess]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SprintLInkAccess]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SprintLInkAccess](
	[TableID] [nvarchar](255) NULL,
	[Old Private Line] [nvarchar](255) NULL,
	[Old Port Speed] [nvarchar](255) NULL,
	[Old DLCI] [nvarchar](255) NULL,
	[Old VPI VCI] [nvarchar](255) NULL,
	[New Private Line] [nvarchar](255) NULL,
	[New Port Speed] [nvarchar](255) NULL,
	[New DLCI] [nvarchar](255) NULL,
	[New VPI VCI] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[SprintLink]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SprintLink]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SprintLink](
	[Event_ID] [float] NULL,
	[H1] [nvarchar](255) NULL,
	[Customer Name] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Created By] [nvarchar](255) NULL,
	[Workflow Status] [nvarchar](255) NULL,
	[Event Status] [nvarchar](255) NULL,
	[ID] [float] NULL,
	[SprintLink WF] [nvarchar](255) NULL,
	[IP Version] [nvarchar](255) NULL,
	[AccessTableID] [nvarchar](255) NULL,
	[AccessTableTag] [nvarchar](255) NULL,
	[Activator Picker] [nvarchar](255) NULL,
	[Assigned To] [nvarchar](255) NULL,
	[Auth_Status] [nvarchar](255) NULL,
	[Bridge Number] [nvarchar](255) NULL,
	[Bridge PIN] [nvarchar](255) NULL,
	[CharsID] [nvarchar](255) NULL,
	[CNSI-Configurations Pre Built Complete] [nvarchar](255) NULL,
	[Completed E-Mail CC:] [nvarchar](255) NULL,
	[Connectivity Matrix Location/Name] [nvarchar](255) NULL,
	[Content Type] [nvarchar](255) NULL,
	[Customer Contact Cell Phone] [nvarchar](255) NULL,
	[Customer Contact Name] [nvarchar](255) NULL,
	[Customer Contact Pager] [nvarchar](255) NULL,
	[Customer Contact Pager PIN] [nvarchar](255) NULL,
	[Customer Contact Phone] [nvarchar](255) NULL,
	[Customer Email] [nvarchar](255) NULL,
	[CUSTOMER_CSG_LVL] [nvarchar](255) NULL,
	[Description and Comments] [nvarchar](max) NULL,
	[End Time] [datetime] NULL,
	[Escalation Reason] [nvarchar](255) NULL,
	[Event Duration] [float] NULL,
	[Extra Duration] [nvarchar](255) NULL,
	[Extra Duration2] [nvarchar](255) NULL,
	[FTN] [nvarchar](255) NULL,
	[H6] [nvarchar](255) NULL,
	[Is Escalation] [bit] NOT NULL,
	[Is this a Government Event?] [nvarchar](255) NULL,
	[Item Title] [nvarchar](255) NULL,
	[L2P_RECORD] [nvarchar](255) NULL,
	[L2P_VERIFIED] [nvarchar](255) NULL,
	[L2P_VERIFIED_DATETIME] [datetime] NULL,
	[MaskedContName] [nvarchar](255) NULL,
	[MaskedCustName] [nvarchar](255) NULL,
	[MaskedCustPhone] [nvarchar](255) NULL,
	[Maskedemail] [nvarchar](255) NULL,
	[MDS Managed] [bit] NOT NULL,
	[MEMBER_CSG_LVL] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Modified By] [nvarchar](255) NULL,
	[Primary Request Date] [datetime] NULL,
	[Published E-Mail CC:] [nvarchar](255) NULL,
	[Request Contact Cell Phone] [nvarchar](255) NULL,
	[Request Contact E-Mail] [nvarchar](255) NULL,
	[Request Contact Name] [nvarchar](255) NULL,
	[Request Contact Pager Phone] [nvarchar](255) NULL,
	[Request Contact Pager PIN] [nvarchar](255) NULL,
	[Request Contact Phone] [nvarchar](255) NULL,
	[Sales Engineer (SE) E-Mail] [nvarchar](255) NULL,
	[Sales Engineer (SE) Name] [nvarchar](255) NULL,
	[Sales Engineer (SE) Phone] [nvarchar](255) NULL,
	[Secondary Request Date] [datetime] NULL,
	[Short Notice] [nvarchar](255) NULL,
	[Slot Picker] [nvarchar](255) NULL,
	[SprintLink Activity Type] [nvarchar](255) NULL,
	[SprintLink Event Type] [nvarchar](255) NULL,
	[Start Time] [datetime] NULL,
	[Submit Type] [nvarchar](255) NULL,
	[Total Event Duration] [nvarchar](255) NULL,
	[UnMasked_ROW_ID] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[SOWSUsers1]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWSUsers1]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWSUsers1](
	[ID] [float] NULL,
	[Account] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Work e-mail] [nvarchar](255) NULL,
	[Is Site Admin] [bit] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[SOWSUsers]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWSUsers]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWSUsers](
	[ID] [float] NULL,
	[Account] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Work e-mail] [nvarchar](255) NULL,
	[Is Site Admin] [varchar](20) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWSReviewvers]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWSReviewvers]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWSReviewvers](
	[ID] [float] NULL,
	[Account] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Work e-mail] [nvarchar](255) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[SOWSActivators]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWSActivators]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWSActivators](
	[ID] [float] NULL,
	[Account] [nvarchar](255) NULL,
	[Name] [nvarchar](255) NULL,
	[Work e-mail] [nvarchar](255) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[SOWS_Value_Added_Services_Info]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_Value_Added_Services_Info]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_Value_Added_Services_Info](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[IP_Tunnelling_Indicator] [varchar](1) NULL,
	[Cos_Indicator] [varchar](1) NULL,
	[Noc_To_Noc_Indicator] [varchar](1) NULL,
	[EIA_Indicator] [varchar](1) NULL,
	[Data_Link_Gateway_Indicator] [varchar](1) NULL,
	[Ses_Server_IP] [varchar](60) NULL,
	[Ban_Number] [varchar](50) NULL,
	[Untrusted_IP_Block] [varchar](50) NULL,
	[Block_Subnet_Mask] [varchar](50) NULL,
	[Design_Document_Location] [varchar](100) NULL,
	[Primary_Feature_Server_IP] [varchar](30) NULL,
	[Primary_Feature_Server_Hostname] [varchar](50) NULL,
	[Secondary_Feature_Server_IP] [varchar](14) NULL,
	[Secondary_Feature_Server_Hostname] [varchar](50) NULL,
	[Time_Stamp] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_Vas_Type_Group]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_Vas_Type_Group]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_Vas_Type_Group](
	[ID] [numeric](18, 0) NOT NULL,
	[VAS_ID] [numeric](18, 0) NOT NULL,
	[Vas_Type_Cd] [varchar](10) NULL,
	[NUA] [varchar](50) NULL,
	[Quantity] [varchar](50) NULL,
	[DNS_Sec_Flag] [varchar](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_Value_Added_Services_Info]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_Value_Added_Services_Info]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_Value_Added_Services_Info](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[IP_Tunnelling_Indicator] [varchar](1) NULL,
	[Cos_Indicator] [varchar](1) NULL,
	[Noc_To_Noc_Indicator] [varchar](1) NULL,
	[EIA_Indicator] [varchar](1) NULL,
	[Data_Link_Gateway_Indicator] [varchar](1) NULL,
	[Ses_Server_IP] [varchar](60) NULL,
	[Ban_Number] [varchar](50) NULL,
	[Untrusted_IP_Block] [varchar](50) NULL,
	[Block_Subnet_Mask] [varchar](50) NULL,
	[Design_Document_Location] [varchar](100) NULL,
	[Primary_Feature_Server_IP] [varchar](30) NULL,
	[Primary_Feature_Server_Hostname] [varchar](50) NULL,
	[Secondary_Feature_Server_IP] [varchar](14) NULL,
	[Secondary_Feature_Server_Hostname] [varchar](50) NULL,
	[Time_Stamp] [datetime] NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_SUPPORT_INFO]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_SUPPORT_INFO]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_SUPPORT_INFO](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[PrimaryContactLastName] [varchar](30) NULL,
	[PrimaryContactFirstName] [varchar](30) NULL,
	[PrimaryContactPhone] [varchar](20) NULL,
	[PrimaryContactCity] [varchar](50) NULL,
	[PrimaryContactState] [varchar](2) NULL,
	[PrimaryContactZipCode] [varchar](9) NULL,
	[SecondaryContactLastName] [varchar](30) NULL,
	[SecondaryContactFirstName] [varchar](30) NULL,
	[SecondaryContactPhone] [varchar](14) NULL,
	[NetworkEngineerLastName] [varchar](30) NULL,
	[NetworkEngineerFirstName] [varchar](30) NULL,
	[NetworkEngineerPhone] [varchar](14) NULL,
	[ServiceAssuranceContactFlag] [varchar](1) NULL,
	[ServiceAssuranceContactLastName] [varchar](30) NULL,
	[ServiceAssuranceContactFirstName] [varchar](30) NULL,
	[ServiceAssuranceContactEmail] [varchar](100) NULL,
	[ServiceAssuranceContactCountryCode] [varchar](4) NULL,
	[ServiceAssuranceContactCityCode] [varchar](5) NULL,
	[ServiceAssuranceContactPhone] [varchar](14) NULL,
	[ServiceAssuranceContactPhoneExtn] [varchar](4) NULL,
	[ServiceAssuranceContactTimeZone] [varchar](50) NULL,
	[ServiceAssuranceContactAvailStartTime] [varchar](8) NULL,
	[ServiceAssuranceContactAvailEndTime] [varchar](8) NULL,
	[ServiceAssuranceContactLanguage] [varchar](15) NULL,
	[ServiceAssuranceContactFaxNumber] [varchar](14) NULL,
	[Salesperson] [varchar](50) NULL,
	[SalespersonPhone] [varchar](14) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_SERVICE_INFO]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_SERVICE_INFO]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_SERVICE_INFO](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[Access_Type] [varchar](10) NULL,
	[Speed_Of_Service] [varchar](20) NULL,
	[Link_Protocol] [varchar](255) NULL,
	[Encapsulation] [varchar](50) NULL,
	[Routing_Type] [varchar](255) NULL,
	[Mpls_Vpn_Splk_Indicator] [varchar](1) NULL,
	[Service_Type] [varchar](4) NULL,
	[Framing] [varchar](255) NULL,
	[Encoding] [varchar](255) NULL,
	[Num_Of_Timeslots] [varchar](5) NULL,
	[Starting_Timeslots] [varchar](3) NULL,
	[NetworkAddress] [varchar](30) NULL,
	[Existing_Service] [varchar](50) NULL,
	[Related_OE_Number] [varchar](50) NULL,
	[IP_Shared_Tenant_Order] [varchar](1) NULL,
	[Alternate_Access] [varchar](30) NULL,
	[Alternate_Provider] [varchar](50) NULL,
	[Fiber_Hand_Off] [varchar](255) NULL,
	[Access_Contract_Term] [varchar](30) NULL,
	[User_Type] [varchar](30) NULL,
	[Multi_Meg_Flag] [varchar](1) NULL,
	[Num_Of_Vlans] [varchar](2) NULL,
	[Ethernet_Interface_Indicator] [varchar](6) NULL,
	[Scrambling] [varchar](100) NULL,
	[Port_Type_Preference] [varchar](30) NULL,
	[Connector_Type] [varchar](30) NULL,
	[Cust_Router_Tagging] [varchar](30) NULL,
	[Cust_Router_Negotiation] [varchar](10) NULL,
	[MNS_Flag] [varchar](1) NULL,
	[DNS_Admin_Last_Name] [varchar](30) NULL,
	[DNS_Admin_First_Name] [varchar](30) NULL,
	[DNS_Admin_Phone] [varchar](20) NULL,
	[DNS_Admin_Email] [varchar](100) NULL,
	[Offnet_Product_Type] [varchar](30) NULL,
	[AOI] [varchar](512) NULL,
	[Access_Circuit_Speed] [varchar](20) NULL,
	[Primary_Hardware_Platform] [varchar](10) NULL,
	[Slvlan_Or_Slppl] [varchar](20) NULL,
	[Class_Of_NNI] [varchar](40) NULL,
	[IP_Version] [varchar](30) NULL,
	[Circuit_Type] [varchar](20) NULL,
	[Access_Provider] [varchar](50) NULL,
	[Cust_Access_Contract_Term] [varchar](30) NULL,
	[Vendor_Contract_Term] [varchar](30) NULL,
	[Rate_Type] [varchar](50) NULL,
	[Offnet_Request_Type] [varchar](50) NULL,
	[Carrier_Circuit_Number] [varchar](50) NULL,
	[Service_Port_To_CCD] [varchar](50) NULL,
	[SCA_Or_Access_Quote_Number] [varchar](50) NULL,
	[SCA_Or_Access_Quote_Exp_Date] [varchar](50) NULL,
	[Sprint_Special_Pricing_Number] [varchar](50) NULL,
	[Alaska_Special_Pricing_Number] [varchar](50) NULL,
	[Jurisdiction] [varchar](50) NULL,
	[Primary_Lec_ID] [varchar](50) NULL,
	[Carrier_Special_Pricing_Number] [varchar](100) NULL,
	[Vendor_Quote_Number] [varchar](100) NULL,
	[RFQ_Number] [varchar](100) NULL,
	[Multi_Order_Flag] [varchar](20) NULL,
	[CPE_Occupied_Date] [varchar](30) NULL,
	[Order_Seq] [varchar](30) NULL,
	[ISDN_Backup] [varchar](100) NULL,
	[Option_A_B_Flag] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_RELATED_CIRCUITID_NUA_LIST]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_RELATED_CIRCUITID_NUA_LIST]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_RELATED_CIRCUITID_NUA_LIST](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[CIRCUIT_ID_NUA] [varchar](255) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_RELATED_CIRCUIT_LIST]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_RELATED_CIRCUIT_LIST]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_RELATED_CIRCUIT_LIST](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[Related_Circuit_ID] [varchar](15) NOT NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_ORDER_INFO]    Script Date: 01/05/2012 10:26:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_ORDER_INFO]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_ORDER_INFO](
	[FTN] [int] NOT NULL,
	[Related_Order_ID] [varchar](20) NULL,
	[Associated_FTN] [varchar](20) NULL,
	[Order_Type] [varchar](30) NULL,
	[Order_Sub_Type] [varchar](30) NULL,
	[Service_Sub_Type] [varchar](20) NULL,
	[SCA_Number] [varchar](60) NULL,
	[TSP_Code] [varchar](30) NULL,
	[Carrier_Wholesale_Type] [varchar](30) NULL,
	[Network_Indicator] [varchar](20) NULL,
	[PRS_Quote_ID] [varchar](20) NULL,
	[Branch_Code] [varchar](4) NULL,
	[SOTS_ID] [varchar](30) NULL,
	[Chars_ID] [varchar](30) NULL,
	[Govt_Indicator] [varchar](5) NULL,
	[ES_FMS_Notes] [varchar](1000) NULL,
	[CPE_Special_Instructions] [varchar](1000) NULL,
	[Time_Stamp] [datetime] NULL,
	[CCD] [varchar](30) NULL,
	[CWD] [varchar](30) NULL,
	[Sign_Date] [varchar](30) NULL,
	[Cust_Order_Submit_Date] [varchar](30) NULL,
	[Expedite_Flag] [varchar](1) NULL,
	[Expedite_Type] [varchar](40) NULL,
	[Escalated_Flag] [varchar](1) NULL,
	[Company_Code] [varchar](20) NULL,
	[SOI] [varchar](255) NULL,
	[Design_Doc_Number] [varchar](50) NULL,
	[Network_Engineer_Notes] [varchar](1000) NULL,
	[Domestic_International_Flag] [varchar](1) NULL,
	[Link_ID] [int] NULL,
	[PL_Number] [varchar](10) NULL,
	[SOWS_Status] [varchar](50) NULL,
	[Assigned_To] [varchar](20) NULL,
	[Sent_To_FSA_Flag] [varchar](1) NULL,
	[Sent_To_FSA_Timestamp] [datetime] NULL,
	[Burstable_Discount_Code] [varchar](30) NULL,
	[Burstable_Usage_Type] [varchar](50) NULL,
	[Shadow_Port] [varchar](10) NULL,
	[Customer_Pricing_Currency] [varchar](30) NULL,
	[PL_Seq] [varchar](5) NULL,
	[Region] [varchar](10) NULL,
	[Bill_Clear_Date] [date] NULL,
	[Foc_Date] [date] NULL,
	[Primary_Vendor_Name] [varchar](50) NULL,
	[Vendor_Order_Ack_Date] [date] NULL,
	[SPID] [int] NULL,
	[Sub_Product_Type] [varchar](100) NULL,
	[Transport_Order_Type] [varchar](100) NULL,
	[Special_Pricing_Flag_Cd] [varchar](100) NULL,
	[SPRB_Number] [varchar](100) NULL,
	[CDM_Number] [numeric](18, 0) NULL,
	[XNCI_Transfer_Flag] [varchar](1) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_MNS_Line_Item]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_MNS_Line_Item]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_MNS_Line_Item](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[Description] [varchar](4000) NULL,
	[BIC] [varchar](100) NULL,
	[Quantity] [varchar](20) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_MNS_INFO]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_MNS_INFO]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_MNS_INFO](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[MSS_Solution_Service] [varchar](50) NULL,
	[MSS_Network_Type] [varchar](10) NULL,
	[Service_Tiers] [varchar](50) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_MNS_Charge]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_MNS_Charge]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_MNS_Charge](
	[ID] [numeric](18, 0) NOT NULL,
	[MNS_ID] [numeric](18, 0) NOT NULL,
	[Charge_Type] [varchar](10) NULL,
	[Price] [varchar](30) NULL,
	[Charge_Code] [varchar](30) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_DISC_CANCEL_INFO]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_DISC_CANCEL_INFO]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_DISC_CANCEL_INFO](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[Requestor_Email_Address] [varchar](100) NULL,
	[Requestor_Contact_Name] [varchar](30) NULL,
	[Requestor_Contact_Phone] [varchar](30) NULL,
	[Reason_Code] [varchar](30) NULL,
	[CBS_Reason] [varchar](100) NULL,
	[CBS_Reason_Detail] [varchar](1000) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_CUSTOMER_INFO]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CUSTOMER_INFO]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_CUSTOMER_INFO](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[H1_Cust_ID] [varchar](15) NOT NULL,
	[H1_Cust_Name] [varchar](50) NOT NULL,
	[H4_Billing_Address] [varchar](100) NULL,
	[H4_Billing_City] [varchar](50) NULL,
	[H4_Billing_State] [varchar](2) NULL,
	[H4_Billing_Zip_Code] [varchar](9) NULL,
	[H4_Billing_Phone] [varchar](14) NULL,
	[H4_Billing_Contact_Last_Name] [varchar](30) NULL,
	[H4_Billing_Contact_First_Name] [varchar](30) NULL,
	[H4_Prov_Muni] [varchar](30) NULL,
	[H4_Country] [varchar](30) NULL,
	[H5_H6_Cust_ID] [varchar](15) NOT NULL,
	[H5_H6_Telco_Demarc] [varchar](50) NULL,
	[H5_H6_Equip_Bld_Floor_Room] [varchar](50) NULL,
	[H5_H6_CLLI_Code] [varchar](20) NULL,
	[H5_H6_Customer_Name] [varchar](50) NOT NULL,
	[H5_H6_Contact_Last_Name] [varchar](30) NULL,
	[H5_H6_Contact_First_Name] [varchar](30) NULL,
	[H5_H6_Customer_Phone] [varchar](14) NULL,
	[H5_H6_Parent_Acct_Number] [varchar](30) NULL,
	[H5_H6_Suppres_Flag] [varchar](1) NULL,
	[H5_H6_Contact_Phone] [varchar](14) NULL,
	[H5_H6_Contact_Email] [varchar](100) NULL,
	[H5_H6_Address] [varchar](100) NULL,
	[H5_H6_City] [varchar](50) NULL,
	[H5_H6_State] [varchar](2) NULL,
	[H5_H6_Country] [varchar](30) NULL,
	[H5_H6_Prov_Muni] [varchar](30) NULL,
	[H5_H6_Zip_Code] [varchar](9) NULL,
	[Internet_Center] [varchar](50) NULL,
	[On_Site_Contact_Last_Name] [varchar](30) NULL,
	[On_Site_Contact_First_Name] [varchar](30) NULL,
	[On_Site_Phone] [varchar](14) NULL,
	[On_Site_Fax] [varchar](14) NULL,
	[On_Site_Pager] [varchar](14) NULL,
	[On_Site_Contact_Email] [varchar](100) NULL,
	[H4_Cust_Name] [varchar](50) NULL,
	[H5_H6_International_Phone] [varchar](30) NULL,
	[Alternate_On_Site_Phone] [varchar](30) NULL,
	[Sec_On_Site_Last_Name] [varchar](100) NULL,
	[Sec_On_Site_First_Name] [varchar](100) NULL,
	[Sec_On_Site_Phone] [varchar](100) NULL,
	[Site_NPAXXX] [varchar](30) NULL,
	[Nearest_Cross_St] [varchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_CUST_PRICING_LINE_ITEM]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CUST_PRICING_LINE_ITEM]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_CUST_PRICING_LINE_ITEM](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[Charge_Code] [varchar](30) NULL,
	[Price] [varchar](30) NULL,
	[Charge_Type] [varchar](10) NULL,
	[Charge_Code_Desc] [varchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_CPE_INFO]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CPE_INFO]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_CPE_INFO](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[CPE_Description] [varchar](4000) NULL,
	[Delivery_Duty_Flag] [varchar](10) NULL,
	[Contract_Type] [varchar](20) NULL,
	[SPID] [int] NULL,
	[Equipment_ID] [varchar](100) NULL,
	[Purchase_Or_Rental] [varchar](30) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_CPE_GENERAL_INFO]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CPE_GENERAL_INFO]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_CPE_GENERAL_INFO](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[Spl_Cust_Arrg_ID] [varchar](100) NULL,
	[CPE_Project_ID] [varchar](100) NULL,
	[CPE_Contract_ID] [varchar](100) NULL,
	[CPE_Order_Type] [varchar](100) NULL,
	[CPE_Provided_By] [varchar](100) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_CPE_CHARGE]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_CPE_CHARGE]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_CPE_CHARGE](
	[ID] [numeric](18, 0) NOT NULL,
	[CPE_ID] [numeric](18, 0) NOT NULL,
	[Charge_Code] [varchar](30) NULL,
	[Charge_Type] [varchar](10) NULL,
	[CPE_Price] [varchar](30) NULL,
	[Shipping_Flag] [varchar](10) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SOWS_FSA_ACCOUNT_TEAM]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SOWS_FSA_ACCOUNT_TEAM]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SOWS_FSA_ACCOUNT_TEAM](
	[ID] [numeric](18, 0) NOT NULL,
	[FTN] [int] NOT NULL,
	[Role] [varchar](255) NULL,
	[First_Name] [varchar](30) NOT NULL,
	[Last_Name] [varchar](30) NOT NULL,
	[Email_Address] [varchar](128) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[SLINK_SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SLINK_SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SLINK_SAVE_UNMASKED_CUST_DATA](
	[ROWID] [numeric](18, 0) NOT NULL,
	[H1] [nvarchar](15) NOT NULL,
	[CUSTOMERNAME] [nvarchar](150) NOT NULL,
	[CONTACTNAME] [nvarchar](150) NOT NULL,
	[CUSTOMEREMAIL] [nvarchar](150) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[SAVE_UNMASKED_CUST_DATA](
	[ROWID] [numeric](18, 0) NOT NULL,
	[H1] [nvarchar](15) NOT NULL,
	[INSTALLSITEPOC] [nvarchar](150) NOT NULL,
	[ENGINEERINGCONTACT] [nvarchar](150) NULL,
	[SERVICEASSURANCEPOC] [nvarchar](150) NOT NULL,
	[SERVICEASSURANCEPOCEMAIL] [nvarchar](150) NULL,
	[CREATED_DT] [datetime] NOT NULL,
	[CUSTOMERNAME] [nvarchar](150) NULL,
	[CUSTOMEREMAIL] [nvarchar](150) NULL,
	[CUSTADDRESS] [nvarchar](300) NULL,
	[CITY] [nvarchar](150) NULL,
	[CUSTSTATE] [nvarchar](50) NULL,
	[POSTALCODE] [nvarchar](12) NULL,
	[COUNTRY] [nvarchar](5) NULL,
	[CONTACTNAME] [nvarchar](150) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[Qualifications]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[Qualifications]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[Qualifications](
	[Item Title] [nvarchar](255) NULL,
	[Event Types] [nvarchar](255) NULL,
	[Special Projects] [nvarchar](255) NULL,
	[Enhanced Services] [nvarchar](255) NULL,
	[Devices] [nvarchar](255) NULL,
	[Vendor Model] [nvarchar](max) NULL,
	[Assigned To] [nvarchar](255) NULL,
	[Created By] [nvarchar](255) NULL,
	[Modified By] [nvarchar](255) NULL,
	[IP Version] [nvarchar](255) NULL,
	[Dedicated Customer Name] [nvarchar](255) NULL,
	[Dedicated Customers] [nvarchar](255) NULL,
	[Content Type] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[ID] [float] NULL,
	[Modified] [datetime] NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[ORDR_Tmp]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[ORDR_Tmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[ORDR_Tmp](
	[ORDR_ID] [int] IDENTITY(0,1) NOT FOR REPLICATION NOT NULL,
	[FTN] [int] NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[ORDR_CNTCT]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[ORDR_CNTCT]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[ORDR_CNTCT](
	[ORDR_CNTCT_ID] [int] NULL,
	[FRST_NME] [varchar](max) NULL,
	[LST_NME] [varchar](max) NULL,
	[EMAIL_ADR] [varchar](max) NULL,
	[CNTCT_NME] [varchar](max) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[ORDR_ADR]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[ORDR_ADR]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[ORDR_ADR](
	[ORDR_ADR_ID] [int] NULL,
	[STREET_ADR_1] [varchar](max) NULL,
	[STREET_ADR_2] [varchar](max) NULL,
	[CTY_NME] [varchar](max) NULL,
	[PRVN_NME] [varchar](max) NULL,
	[STT_CD] [varchar](max) NULL,
	[ZIP_PSTL_CD] [varchar](max) NULL,
	[BLDG_NME] [varchar](max) NULL,
	[FLR_ID] [varchar](max) NULL,
	[RM_NBR] [varchar](max) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[NGVNSIPTrk]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[NGVNSIPTrk]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[NGVNSIPTrk](
	[TableID] [nvarchar](255) NULL,
	[Trunk] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[NGVNEvent]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[NGVNEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[NGVNEvent](
	[Event Status] [nvarchar](255) NULL,
	[Event_ID] [float] NULL,
	[Is this an Escalation?] [bit] NOT NULL,
	[Assigned To] [nvarchar](255) NULL,
	[NGVN WF] [nvarchar](255) NULL,
	[Created By] [nvarchar](255) NULL,
	[AccessTableID] [nvarchar](255) NULL,
	[Activator Picker] [nvarchar](255) NULL,
	[Auth_Status] [nvarchar](255) NULL,
	[Bridge Number] [nvarchar](255) NULL,
	[Bridge PIN] [nvarchar](255) NULL,
	[Cable Add Due Date] [datetime] NULL,
	[Cable Protocol Type] [nvarchar](255) NULL,
	[CharsID] [nvarchar](255) NULL,
	[Circuit ID NUA] [nvarchar](255) NULL,
	[Completed E-Mail CC:] [nvarchar](255) NULL,
	[Content Type] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Customer Contact Name] [nvarchar](255) NULL,
	[Customer Contact Pager] [nvarchar](255) NULL,
	[Customer Contact Pager PIN] [nvarchar](255) NULL,
	[Customer Contact Phone] [nvarchar](255) NULL,
	[Customer Email] [nvarchar](255) NULL,
	[Customer Name] [nvarchar](255) NULL,
	[Description and Comments] [nvarchar](max) NULL,
	[End Time] [datetime] NULL,
	[Escalation Reason] [nvarchar](255) NULL,
	[Event Duration] [float] NULL,
	[Extra Duration] [nvarchar](255) NULL,
	[FTN] [nvarchar](255) NULL,
	[GSR Configuration] [nvarchar](max) NULL,
	[H1] [nvarchar](255) NULL,
	[H6] [nvarchar](255) NULL,
	[ID] [float] NULL,
	[Item Title] [nvarchar](255) NULL,
	[MaskedContName] [nvarchar](255) NULL,
	[MaskedCustName] [nvarchar](255) NULL,
	[MaskedCustPhone] [nvarchar](255) NULL,
	[Maskedemail] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Modified By] [nvarchar](255) NULL,
	[New SBC Pair] [bit] NOT NULL,
	[Port Mirror Description] [nvarchar](255) NULL,
	[Port Mirroring Enabled?] [bit] NOT NULL,
	[Primary Request Date] [datetime] NULL,
	[Primary Switch 6509] [nvarchar](255) NULL,
	[Product Type] [nvarchar](255) NULL,
	[Published E-Mail CC:] [nvarchar](255) NULL,
	[Request Contact Cell Phone] [nvarchar](255) NULL,
	[Request Contact E-Mail] [nvarchar](255) NULL,
	[Request Contact Name] [nvarchar](255) NULL,
	[Request Contact Pager Phone] [nvarchar](255) NULL,
	[Request Contact Pager PIN] [nvarchar](255) NULL,
	[Request Contact Phone] [nvarchar](255) NULL,
	[Sales Engineer (SE) E-Mail] [nvarchar](255) NULL,
	[Sales Engineer (SE) Name] [nvarchar](255) NULL,
	[Sales Engineer (SE) Phone] [nvarchar](255) NULL,
	[SBC Pair] [nvarchar](255) NULL,
	[Secondary Request Date] [datetime] NULL,
	[Secondary Switch 6509] [nvarchar](255) NULL,
	[SIP Trunk Table] [nvarchar](255) NULL,
	[Slot Picker] [nvarchar](255) NULL,
	[Start Time] [datetime] NULL,
	[Submit Type] [nvarchar](255) NULL,
	[Trusted HSRP IP] [nvarchar](255) NULL,
	[Trusted IP Block Netmask] [nvarchar](255) NULL,
	[Trusted SD 1st IP] [nvarchar](255) NULL,
	[Trusted SD 2nd IP] [nvarchar](255) NULL,
	[Trusted SD Virtual IP] [nvarchar](255) NULL,
	[Trusted VLAN] [nvarchar](255) NULL,
	[UnMasked_ROW_ID] [nvarchar](255) NULL,
	[Untrusted HSRP IP] [nvarchar](255) NULL,
	[Untrusted IP Block Netwmask] [nvarchar](255) NULL,
	[Untrusted SD 1st IP] [nvarchar](255) NULL,
	[Untrusted SD 2nd IP] [nvarchar](255) NULL,
	[Untrusted SD Virtual IP] [nvarchar](255) NULL,
	[Untrusted VLAN] [nvarchar](255) NULL,
	[Workflow Status] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[NGVNCircuit]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[NGVNCircuit]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[NGVNCircuit](
	[TableID] [nvarchar](255) NULL,
	[CircuitID_Nua] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[NGVN_SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[NGVN_SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[NGVN_SAVE_UNMASKED_CUST_DATA](
	[ROWID] [numeric](18, 0) NOT NULL,
	[H1] [nvarchar](15) NOT NULL,
	[CUSTOMERNAME] [nvarchar](150) NOT NULL,
	[CONTACTNAME] [nvarchar](150) NOT NULL,
	[CUSTOMEREMAIL] [nvarchar](150) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MPLSOutbound]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSOutbound]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MPLSOutbound](
	[Outbound NAT Pool] [nvarchar](255) NULL,
	[TableID] [nvarchar](255) NULL,
	[Out NAT Mask] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MPLSFirewall]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSFirewall]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MPLSFirewall](
	[Tunnel Interface IP] [nvarchar](255) NULL,
	[Routing VAS Gateway] [nvarchar](255) NULL,
	[Tunnel VAS Gateway] [nvarchar](255) NULL,
	[TableID] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MPLSEventInbound]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSEventInbound]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MPLSEventInbound](
	[Inbound NAT Pool] [nvarchar](255) NULL,
	[NAT Pool Mask] [nvarchar](255) NULL,
	[TableID] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MPLSEvent]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MPLSEvent](
	[Event_ID] [float] NULL,
	[Bridge Number] [nvarchar](255) NULL,
	[Bridge PIN] [nvarchar](255) NULL,
	[Circuit Number] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Created By] [nvarchar](255) NULL,
	[Customer Name] [nvarchar](255) NULL,
	[H1] [nvarchar](255) NULL,
	[H6] [nvarchar](255) NULL,
	[Workflow Status] [nvarchar](255) NULL,
	[Carrier Partner] [nvarchar](255) NULL,
	[OFF Net Monitoring?] [bit] NOT NULL,
	[Event Status] [nvarchar](255) NULL,
	[Start Time] [datetime] NULL,
	[SOWS_ADMIN_SCRIPT] [nvarchar](255) NULL,
	[Scheduled_Implementation_Start_WF] [nvarchar](255) NULL,
	[IP Version] [nvarchar](255) NULL,
	[Access Bandwidth] [nvarchar](255) NULL,
	[AccessTableID] [nvarchar](255) NULL,
	[AccessTableTag] [nvarchar](255) NULL,
	[Activator Picker] [nvarchar](255) NULL,
	[Activity Locale] [nvarchar](255) NULL,
	[Add End to End Monitoring] [bit] NOT NULL,
	[Assigned To] [nvarchar](255) NULL,
	[Auth_Status] [nvarchar](255) NULL,
	[CharsID] [nvarchar](255) NULL,
	[Completed E-Mail CC:] [nvarchar](255) NULL,
	[Content Type] [nvarchar](255) NULL,
	[Customer Contact Cell Phone] [nvarchar](255) NULL,
	[Customer Contact Name] [nvarchar](255) NULL,
	[Customer Contact Pager] [nvarchar](255) NULL,
	[Customer Contact Pager PIN] [nvarchar](255) NULL,
	[Customer Contact Phone] [nvarchar](255) NULL,
	[Customer Email] [nvarchar](255) NULL,
	[CUSTOMER_CSG_LVL] [nvarchar](255) NULL,
	[CustomerMapping] [nvarchar](255) NULL,
	[CustomerMappingID] [nvarchar](255) NULL,
	[Description and Comments] [nvarchar](max) NULL,
	[Design Document Approval Number] [nvarchar](255) NULL,
	[Design Document Location] [nvarchar](255) NULL,
	[End Time] [datetime] NULL,
	[Escalation Reason] [nvarchar](255) NULL,
	[Event Duration] [float] NULL,
	[Extra Duration] [nvarchar](255) NULL,
	[FirewallTable] [nvarchar](255) NULL,
	[FirewallTableID] [nvarchar](255) NULL,
	[FTN] [nvarchar](255) NULL,
	[ID] [float] NULL,
	[InboundNAT] [nvarchar](255) NULL,
	[InboundNATID] [nvarchar](255) NULL,
	[Is NDD Updated?] [bit] NOT NULL,
	[Is this a Carrier Partner?] [bit] NOT NULL,
	[Is this a Government Event?] [nvarchar](255) NULL,
	[Is this a Migration?] [bit] NOT NULL,
	[Is this a Wholesale Partner?] [bit] NOT NULL,
	[Is this an Escalation?] [bit] NOT NULL,
	[Item Title] [nvarchar](255) NULL,
	[L2P_RECORD] [nvarchar](255) NULL,
	[L2P_VERIFIED] [nvarchar](255) NULL,
	[L2P_VERIFIED_DATETIME] [datetime] NULL,
	[MaskedContName] [nvarchar](255) NULL,
	[MaskedCustName] [nvarchar](255) NULL,
	[MaskedCustPhone] [nvarchar](255) NULL,
	[Maskedemail] [nvarchar](255) NULL,
	[MDS DLCI] [nvarchar](255) NULL,
	[MDS IP Address] [nvarchar](255) NULL,
	[MDS Managed] [bit] NOT NULL,
	[MDS Static Route] [nvarchar](255) NULL,
	[MDS VRF Name] [nvarchar](255) NULL,
	[MEMBER_CSG_LVL] [nvarchar](255) NULL,
	[Migration Type] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Modified By] [nvarchar](255) NULL,
	[MPLS Activity Type] [nvarchar](255) NULL,
	[MPLS Activity Type2] [nvarchar](255) NULL,
	[MPLS Event Type] [nvarchar](255) NULL,
	[Multi VRF Requirements] [nvarchar](255) NULL,
	[NNI Design Document] [nvarchar](255) NULL,
	[ON Net Monitoring?] [bit] NOT NULL,
	[OutboundNAT] [nvarchar](255) NULL,
	[OutboundNATID] [nvarchar](255) NULL,
	[Primary Request Date] [datetime] NULL,
	[Published E-Mail CC:] [nvarchar](255) NULL,
	[Request Contact Cell Phone] [nvarchar](255) NULL,
	[Request Contact E-Mail] [nvarchar](255) NULL,
	[Request Contact Name] [nvarchar](255) NULL,
	[Request Contact Pager Phone] [nvarchar](255) NULL,
	[Request Contact Pager PIN] [nvarchar](255) NULL,
	[Request Contact Phone] [nvarchar](255) NULL,
	[Sales Engineer (SE) E-Mail] [nvarchar](255) NULL,
	[Sales Engineer (SE) Name] [nvarchar](255) NULL,
	[Sales Engineer (SE) Phone] [nvarchar](255) NULL,
	[SCRIPT_EMAIL_SENT] [nvarchar](255) NULL,
	[Secondary Request Date] [datetime] NULL,
	[Short Notice] [nvarchar](255) NULL,
	[Slot Picker] [nvarchar](255) NULL,
	[Submit Type] [nvarchar](255) NULL,
	[UnMasked_ROW_ID] [nvarchar](255) NULL,
	[VAS Types] [nvarchar](255) NULL,
	[VPN Platform Type] [nvarchar](255) NULL,
	[VRF Name] [nvarchar](255) NULL,
	[Wholesale Partner] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MPLSCustomer]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSCustomer]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MPLSCustomer](
	[Inbound Public NAT] [nvarchar](255) NULL,
	[Internal Private IP] [nvarchar](255) NULL,
	[TableID] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MPLSAccessTag]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLSAccessTag]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MPLSAccessTag](
	[TableID] [nvarchar](255) NULL,
	[LocationCity] [nvarchar](255) NULL,
	[LocationState] [nvarchar](255) NULL,
	[PrivateLineNumber] [nvarchar](255) NULL,
	[TransportNumber] [nvarchar](255) NULL,
	[VASSOL] [nvarchar](255) NULL,
	[AccessBandwidth] [nvarchar](255) NULL,
	[MNSIPAddress] [nvarchar](255) NULL,
	[MNSOffering] [nvarchar](255) NULL,
	[MNSRouting] [nvarchar](255) NULL,
	[PerformanceReporting] [nvarchar](255) NULL,
	[Number449] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Created By] [nvarchar](255) NULL,
	[Item Title] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Modified By] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MPLS_SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MPLS_SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MPLS_SAVE_UNMASKED_CUST_DATA](
	[ROWID] [numeric](18, 0) NOT NULL,
	[H1] [nvarchar](15) NOT NULL,
	[CUSTOMERNAME] [nvarchar](150) NOT NULL,
	[CONTACTNAME] [nvarchar](150) NULL,
	[CUSTOMEREMAIL] [nvarchar](150) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSWirelessTrans]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSWirelessTrans]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSWirelessTrans](
	[TableID] [nvarchar](255) NULL,
	[Primary] [nvarchar](255) NULL,
	[ESN] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSWiredTrans]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSWiredTrans]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSWiredTrans](
	[TableID] [nvarchar](255) NULL,
	[TransportType] [nvarchar](255) NULL,
	[Primary] [nvarchar](255) NULL,
	[CircuitID] [nvarchar](255) NULL,
	[PL] [nvarchar](255) NULL,
	[NUA] [nvarchar](255) NULL,
	[OldCircuit] [nvarchar](255) NULL,
	[MultilinkCircuit] [nvarchar](255) NULL,
	[Managed] [nvarchar](255) NULL,
	[Telco] [nvarchar](255) NULL,
	[FOCDate] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSVirtual]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSVirtual]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSVirtual](
	[TableID] [nvarchar](255) NULL,
	[PVC] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSOE]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSOE]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSOE](
	[TableID] [nvarchar](255) NULL,
	[MNSOEType] [nvarchar](255) NULL,
	[MNSOEDescription] [nvarchar](255) NULL,
	[MNSOENumber] [nvarchar](255) NULL,
	[ODIEDeviceName] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSManagedDevices]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSManagedDevices]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSManagedDevices](
	[TableID] [nvarchar](255) NULL,
	[VendorModel] [nvarchar](255) NULL,
	[Vendor] [nvarchar](255) NULL,
	[Model] [nvarchar](255) NULL,
	[Device] [nvarchar](255) NULL,
	[Management] [nvarchar](255) NULL,
	[ODIEDeviceName] [nvarchar](255) NULL,
	[MB] [nvarchar](255) NULL,
	[FastTrackFlag] [nvarchar](255) NULL,
	[DispatchReady] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSLoc]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSLoc]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSLoc](
	[TableID] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[LOCType] [nvarchar](255) NULL,
	[Number] [nvarchar](255) NULL,
	[RASDate] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastWirelessTrpt]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastWirelessTrpt]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastWirelessTrpt](
	[TableID] [nvarchar](255) NULL,
	[Primary] [nvarchar](255) NULL,
	[ESN] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastWiredTrspt]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastWiredTrspt]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastWiredTrspt](
	[TableID] [nvarchar](255) NULL,
	[TransportType] [nvarchar](255) NULL,
	[Primary] [nvarchar](255) NULL,
	[CircuitID] [nvarchar](255) NULL,
	[PL] [nvarchar](255) NULL,
	[NUA] [nvarchar](255) NULL,
	[OldCircuit] [nvarchar](255) NULL,
	[MultilinkCircuit] [nvarchar](255) NULL,
	[Managed] [nvarchar](255) NULL,
	[Telco] [nvarchar](255) NULL,
	[FOCDate] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastVirtual]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastVirtual]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastVirtual](
	[TableID] [nvarchar](255) NULL,
	[PVC] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastTrack]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastTrack]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastTrack](
	[Event Status] [nvarchar](255) NULL,
	[Event_ID] [float] NULL,
	[Install Site POC] [nvarchar](255) NULL,
	[Engineering Contact] [nvarchar](255) NULL,
	[Service Assurance POC] [nvarchar](255) NULL,
	[Auth_Status] [nvarchar](255) NULL,
	[Mask_MDS_ROW_ID] [nvarchar](255) NULL,
	[Description and Comments] [nvarchar](max) NULL,
	[Assigned To] [nvarchar](255) NULL,
	[Start Time] [datetime] NULL,
	[Requested Installation Date] [datetime] NULL,
	[SOWS_ADMIN_SCRIPT] [nvarchar](255) NULL,
	[MDS FT WF] [nvarchar](255) NULL,
	[Created By] [nvarchar](255) NULL,
	[AccessTableID] [nvarchar](255) NULL,
	[Activator Picker] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Any Multilink Circuits?] [bit] NOT NULL,
	[Any Virtual Connections?] [bit] NOT NULL,
	[Bridge Number] [nvarchar](255) NULL,
	[Bridge PIN] [nvarchar](255) NULL,
	[CharsID] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[Complete Entitlements] [nvarchar](255) NULL,
	[Completed E-Mail CC:] [nvarchar](255) NULL,
	[Configurator] [nvarchar](255) NULL,
	[Content Type] [nvarchar](255) NULL,
	[Country/Region] [nvarchar](255) NULL,
	[CPE Dispatch Comments] [nvarchar](255) NULL,
	[CPE Dispatch Email Address] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Customer Account Team PDL] [nvarchar](255) NULL,
	[Customer Contact Cell Phone] [nvarchar](255) NULL,
	[Customer Contact Name] [nvarchar](255) NULL,
	[Customer Contact Pager] [nvarchar](255) NULL,
	[Customer Contact Pager PIN] [nvarchar](255) NULL,
	[Customer Contact Phone] [nvarchar](255) NULL,
	[Customer Email] [nvarchar](255) NULL,
	[Customer Name] [nvarchar](255) NULL,
	[Design Document Location] [nvarchar](255) NULL,
	[Design Number] [nvarchar](255) NULL,
	[Disconnect Notify PDL] [nvarchar](255) NULL,
	[Disconnect OE Table] [nvarchar](255) NULL,
	[EmailBody] [nvarchar](max) NULL,
	[End Time] [datetime] NULL,
	[Engineering Contact Cell] [nvarchar](255) NULL,
	[Engineering Contact Phone] [nvarchar](255) NULL,
	[Escalation Reason] [nvarchar](255) NULL,
	[Event Duration] [float] NULL,
	[Extra Duration] [nvarchar](255) NULL,
	[Fast Track Type] [nvarchar](255) NULL,
	[Floor/Building] [nvarchar](255) NULL,
	[FMS Circuit Table] [nvarchar](255) NULL,
	[FTN] [nvarchar](255) NULL,
	[H1] [nvarchar](255) NULL,
	[H6] [nvarchar](255) NULL,
	[ID] [float] NULL,
	[Install Site POC Cell] [nvarchar](255) NULL,
	[Install Site POC Phone] [nvarchar](255) NULL,
	[Is Firewall Security Product?] [bit] NOT NULL,
	[Is MDS Bridge Needed?] [bit] NOT NULL,
	[Is Sprint CPE or NCR Required] [nvarchar](255) NULL,
	[Is this an Escalation?] [bit] NOT NULL,
	[Is Wired Device Transport Required?] [bit] NOT NULL,
	[Is Wireless Transport Required?] [bit] NOT NULL,
	[Item Title] [nvarchar](255) NULL,
	[LOC Table] [nvarchar](255) NULL,
	[Managed Devices] [nvarchar](255) NULL,
	[Masked_Address] [nvarchar](255) NULL,
	[MaskedCity] [nvarchar](255) NULL,
	[MaskedContName] [nvarchar](255) NULL,
	[MaskedCountry] [nvarchar](255) NULL,
	[MaskedCustName] [nvarchar](255) NULL,
	[MaskedCustPhone] [nvarchar](255) NULL,
	[Maskedemail] [nvarchar](255) NULL,
	[MaskedState] [nvarchar](255) NULL,
	[MaskedZip] [nvarchar](255) NULL,
	[MDS Activity] [nvarchar](255) NULL,
	[MDS Activity Type] [nvarchar](255) NULL,
	[MDS Install Activity] [nvarchar](255) NULL,
	[MDS MAC Activity] [nvarchar](255) NULL,
	[MNS OE Table] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Modified By] [nvarchar](255) NULL,
	[Number of Devices to Disconnect] [float] NULL,
	[OE Number] [nvarchar](255) NULL,
	[PreConfigure Completed] [nvarchar](255) NULL,
	[Primary Request Date] [datetime] NULL,
	[Published E-Mail CC:] [nvarchar](255) NULL,
	[Redesign Number] [nvarchar](255) NULL,
	[Request Contact Cell Phone] [nvarchar](255) NULL,
	[Request Contact E-Mail] [nvarchar](255) NULL,
	[Request Contact Name] [nvarchar](255) NULL,
	[Request Contact Pager Phone] [nvarchar](255) NULL,
	[Request Contact Pager PIN] [nvarchar](255) NULL,
	[Request Contact Phone] [nvarchar](255) NULL,
	[Secondary Request Date] [datetime] NULL,
	[Service Assurance POC E-Mail] [nvarchar](255) NULL,
	[Service Assurance POC Phone] [nvarchar](255) NULL,
	[Service Assurance Site Support] [nvarchar](255) NULL,
	[Service Tier] [nvarchar](255) NULL,
	[Service Tier Optional Entitlements] [nvarchar](255) NULL,
	[Short Description] [nvarchar](255) NULL,
	[Slot Picker] [nvarchar](255) NULL,
	[State/Province] [nvarchar](255) NULL,
	[Strategic Entitlements] [nvarchar](255) NULL,
	[Submit Type] [nvarchar](255) NULL,
	[Total Customer Disconnect] [bit] NOT NULL,
	[UnMasked_ROW_ID] [nvarchar](255) NULL,
	[Updated from Reviewer WF] [nvarchar](255) NULL,
	[US or International] [nvarchar](255) NULL,
	[US Timezone] [nvarchar](255) NULL,
	[Virtual Connections] [nvarchar](255) NULL,
	[Will Conference Bridge Be Needed?] [bit] NOT NULL,
	[Wired Device Transport] [nvarchar](255) NULL,
	[Wireless Transport] [nvarchar](255) NULL,
	[Workflow Status] [nvarchar](255) NULL,
	[ZIP/Postal Code] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastOE]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastOE]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastOE](
	[TableID] [nvarchar](255) NULL,
	[MNSOEType] [nvarchar](255) NULL,
	[MNSOEDescription] [nvarchar](255) NULL,
	[MNSOENumber] [nvarchar](255) NULL,
	[ODIEDeviceName] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastOCT]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastOCT]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastOCT](
	[TableID] [nvarchar](255) NULL,
	[Category] [nvarchar](255) NULL,
	[LOCType] [nvarchar](255) NULL,
	[Number] [nvarchar](255) NULL,
	[RASDate] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastMngdDev]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastMngdDev]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastMngdDev](
	[TableID] [nvarchar](255) NULL,
	[VendorModel] [nvarchar](255) NULL,
	[Vendor] [nvarchar](255) NULL,
	[Model] [nvarchar](255) NULL,
	[Device] [nvarchar](255) NULL,
	[Management] [nvarchar](255) NULL,
	[ODIEDeviceName] [nvarchar](255) NULL,
	[MB] [nvarchar](255) NULL,
	[FastTrackFlag] [nvarchar](255) NULL,
	[DispatchReady] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastDisconnectOE]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastDisconnectOE]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastDisconnectOE](
	[TableID] [nvarchar](255) NULL,
	[Vendor] [nvarchar](255) NULL,
	[ODIEDeviceName] [nvarchar](255) NULL,
	[SerialNumber] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Building] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[ZIP] [nvarchar](255) NULL,
	[MNSDisconnectOENumber] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastDiscOE]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastDiscOE]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastDiscOE](
	[TableID] [nvarchar](255) NULL,
	[Vendor] [nvarchar](255) NULL,
	[ODIEDeviceName] [nvarchar](255) NULL,
	[SerialNumber] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[Building] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[State] [nvarchar](255) NULL,
	[ZIP] [nvarchar](255) NULL,
	[MNSDisconnectOENumber] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSFastckt]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSFastckt]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSFastckt](
	[TableID] [nvarchar](255) NULL,
	[Circuit] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSEvent]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSEvent]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSEvent](
	[Event Status] [nvarchar](255) NULL,
	[Event_ID] [float] NULL,
	[Engineering Contact] [nvarchar](255) NULL,
	[Install Site POC] [nvarchar](255) NULL,
	[Service Assurance POC] [nvarchar](255) NULL,
	[Auth_Status] [nvarchar](255) NULL,
	[Description and Comments] [nvarchar](max) NULL,
	[Workflow Status] [nvarchar](255) NULL,
	[Assigned To] [nvarchar](255) NULL,
	[Start Time] [datetime] NULL,
	[SOWS_ADMIN_SCRIPT] [nvarchar](255) NULL,
	[End Time] [datetime] NULL,
	[MDS WF] [nvarchar](255) NULL,
	[Created By] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[Country/Region] [nvarchar](255) NULL,
	[Floor/Building] [nvarchar](255) NULL,
	[State/Province] [nvarchar](255) NULL,
	[AccessTableID] [nvarchar](255) NULL,
	[Activator Picker] [nvarchar](255) NULL,
	[Any Multilink Circuits?] [bit] NOT NULL,
	[Any Virtual Connections?] [bit] NOT NULL,
	[Bridge Number] [nvarchar](255) NULL,
	[Bridge PIN] [nvarchar](255) NULL,
	[CharsID] [nvarchar](255) NULL,
	[Complete Entitlements] [nvarchar](255) NULL,
	[Completed E-Mail CC:] [nvarchar](255) NULL,
	[Configurator] [nvarchar](255) NULL,
	[Content Type] [nvarchar](255) NULL,
	[CPE Dispatch Comments] [nvarchar](255) NULL,
	[CPE Dispatch Email Address] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Customer Account Team PDL] [nvarchar](255) NULL,
	[Customer Contact Cell Phone] [nvarchar](255) NULL,
	[Customer Contact Name] [nvarchar](255) NULL,
	[Customer Contact Pager] [nvarchar](255) NULL,
	[Customer Contact Pager PIN] [nvarchar](255) NULL,
	[Customer Contact Phone] [nvarchar](255) NULL,
	[Customer Email] [nvarchar](255) NULL,
	[Customer Name] [nvarchar](255) NULL,
	[Design Document Location] [nvarchar](255) NULL,
	[Design Number] [nvarchar](255) NULL,
	[EmailBody] [nvarchar](max) NULL,
	[Engineering Contact Cell] [nvarchar](255) NULL,
	[Engineering Contact Phone] [nvarchar](255) NULL,
	[Escalation Reason] [nvarchar](255) NULL,
	[Event Duration] [float] NULL,
	[Extra Duration] [nvarchar](255) NULL,
	[FMS Circuit Table] [nvarchar](255) NULL,
	[FTN] [nvarchar](255) NULL,
	[H1] [nvarchar](255) NULL,
	[H6] [nvarchar](255) NULL,
	[ID] [float] NULL,
	[Install Site POC Cell] [nvarchar](255) NULL,
	[Install Site POC Phone] [nvarchar](255) NULL,
	[Is Firewall Security Product?] [bit] NOT NULL,
	[Is MDS Bridge Needed?] [bit] NOT NULL,
	[Is Sprint CPE or NCR Required] [nvarchar](255) NULL,
	[Is this an Escalation?] [bit] NOT NULL,
	[Is Wired Device Transport Required?] [bit] NOT NULL,
	[Is Wireless Transport Required?] [bit] NOT NULL,
	[Item Title] [nvarchar](255) NULL,
	[LOC Table] [nvarchar](255) NULL,
	[Managed Devices] [nvarchar](255) NULL,
	[Masked_Address] [nvarchar](255) NULL,
	[MaskedCity] [nvarchar](255) NULL,
	[MaskedContName] [nvarchar](255) NULL,
	[MaskedCountry] [nvarchar](255) NULL,
	[MaskedCustName] [nvarchar](255) NULL,
	[MaskedCustPhone] [nvarchar](255) NULL,
	[Maskedemail] [nvarchar](255) NULL,
	[MaskedState] [nvarchar](255) NULL,
	[MaskedZip] [nvarchar](255) NULL,
	[MDS Activity] [nvarchar](255) NULL,
	[MDS Activity Type] [nvarchar](255) NULL,
	[MDS Install Activity] [nvarchar](255) NULL,
	[MDS MAC Activity] [nvarchar](255) NULL,
	[MNS OE Table] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Modified By] [nvarchar](255) NULL,
	[OE Number] [nvarchar](255) NULL,
	[Primary Request Date] [datetime] NULL,
	[Published E-Mail CC:] [nvarchar](255) NULL,
	[Redesign Number] [nvarchar](255) NULL,
	[Request Contact Cell Phone] [nvarchar](255) NULL,
	[Request Contact E-Mail] [nvarchar](255) NULL,
	[Request Contact Name] [nvarchar](255) NULL,
	[Request Contact Pager Phone] [nvarchar](255) NULL,
	[Request Contact Pager PIN] [nvarchar](255) NULL,
	[Request Contact Phone] [nvarchar](255) NULL,
	[SCRIPT_EMAIL_SENT] [nvarchar](255) NULL,
	[Secondary Request Date] [datetime] NULL,
	[Service Assurance POC E-Mail] [nvarchar](255) NULL,
	[Service Assurance POC Phone] [nvarchar](255) NULL,
	[Service Assurance Site Support] [nvarchar](255) NULL,
	[Service Tier] [nvarchar](255) NULL,
	[Service Tier Optional Entitlements] [nvarchar](255) NULL,
	[Short Description] [nvarchar](255) NULL,
	[Slot Picker] [nvarchar](255) NULL,
	[Strategic Entitlements] [nvarchar](255) NULL,
	[Submit Type] [nvarchar](255) NULL,
	[UnMasked_ROW_ID] [nvarchar](255) NULL,
	[US or International] [nvarchar](255) NULL,
	[US Timezone] [nvarchar](255) NULL,
	[Virtual Connections] [nvarchar](255) NULL,
	[Wired Device Transport] [nvarchar](255) NULL,
	[Wireless Transport] [nvarchar](255) NULL,
	[ZIP/Postal Code] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSCircuit]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSCircuit]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSCircuit](
	[TableID] [nvarchar](255) NULL,
	[Circuit] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSActivityMAC]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSActivityMAC]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSActivityMAC](
	[Item Title] [nvarchar](255) NULL,
	[Duration] [nvarchar](255) NULL,
	[Created By] [nvarchar](255) NULL,
	[Modified By] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[MDSAccessTag]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[MDSAccessTag]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[MDSAccessTag](
	[TableID] [nvarchar](255) NULL,
	[LocationCity] [nvarchar](255) NULL,
	[LocationState] [nvarchar](255) NULL,
	[PrivateLineNumber] [nvarchar](255) NULL,
	[TransportNumber] [nvarchar](255) NULL,
	[VASSOL] [nvarchar](255) NULL,
	[AccessBandwidth] [nvarchar](255) NULL,
	[MNSIPAddress] [nvarchar](255) NULL,
	[MNSOffering] [nvarchar](255) NULL,
	[MNSRouting] [nvarchar](255) NULL,
	[PerformanceReporting] [nvarchar](255) NULL,
	[Number449] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Created By] [nvarchar](255) NULL,
	[Item Title] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Modified By] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[FSA_ORDR_CUST]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[FSA_ORDR_CUST]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[FSA_ORDR_CUST](
	[ORDR_ID] [int] NULL,
	[CIS_LVL_TYPE] [varchar](2) NULL,
	[CUST_NME] [varchar](max) NULL
) ON [PRIMARY]
END
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [cnv].[EventCalender_Main]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[EventCalender_Main]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[EventCalender_Main](
	[Recurrence] [bit] NOT NULL,
	[Workspace] [nvarchar](255) NULL,
	[Location] [nvarchar](255) NULL,
	[Created By] [nvarchar](255) NULL,
	[Assigned Activators] [nvarchar](255) NULL,
	[Start Time] [datetime] NULL,
	[End Time] [datetime] NULL,
	[All Day Event] [bit] NULL,
	[Event Status] [nvarchar](255) NULL,
	[EventID] [float] NULL,
	[Event Type] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[ID] [float] NULL,
	[Calendar WF] [nvarchar](255) NULL,
	[Calendar WF (1)] [nvarchar](255) NULL,
	[Content Type] [nvarchar](255) NULL,
	[Created from Calendar] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL,
	[Item Title] [nvarchar](255) NULL,
	[Modified By] [nvarchar](255) NULL,
	[View Event] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[EventCalender]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[EventCalender]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[EventCalender](
	[Recurrence] [bit] NOT NULL,
	[Workspace] [nvarchar](255) NULL,
	[Location] [nvarchar](255) NULL,
	[Created By] [nvarchar](255) NULL,
	[Assigned Activators] [nvarchar](255) NULL,
	[Start Time] [datetime] NULL,
	[End Time] [datetime] NULL,
	[All Day Event] [nvarchar](255) NULL,
	[Event Status] [nvarchar](255) NULL,
	[EventID] [float] NULL,
	[Event Type] [nvarchar](255) NULL,
	[Product] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Modified] [datetime] NULL,
	[ID] [float] NULL,
	[Calendar WF] [nvarchar](255) NULL,
	[Calendar WF (1)] [nvarchar](255) NULL,
	[Content Type] [nvarchar](255) NULL,
	[Created from Calendar] [nvarchar](255) NULL,
	[Description] [nvarchar](max) NULL,
	[Item Title] [nvarchar](255) NULL,
	[Modified By] [nvarchar](255) NULL,
	[View Event] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[EVENT_Tmp]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[EVENT_Tmp]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[EVENT_Tmp](
	[NewEventID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[EventID] [int] NULL,
	[EventStatus] [int] NULL,
	[CreateDate] [smalldatetime] NULL,
	[EventType] [int] NOT NULL,
	[Status] [int] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[ADAccessTable]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[ADAccessTable]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[ADAccessTable](
	[Circuit] [nvarchar](255) NULL,
	[TableID] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Created By] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[AD_SAVE_UNMASKED_CUST_DATA]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[AD_SAVE_UNMASKED_CUST_DATA]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[AD_SAVE_UNMASKED_CUST_DATA](
	[ROWID] [numeric](18, 0) NOT NULL,
	[H1] [nvarchar](15) NOT NULL,
	[CUSTOMERNAME] [nvarchar](150) NOT NULL,
	[CONTACTNAME] [nvarchar](150) NULL,
	[CUSTOMEREMAIL] [nvarchar](150) NOT NULL,
	[CREATED_DT] [datetime] NOT NULL
) ON [PRIMARY]
END
GO
/****** Object:  Table [cnv].[AccessDelivery]    Script Date: 01/05/2012 10:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[cnv].[AccessDelivery]') AND type in (N'U'))
BEGIN
CREATE TABLE [cnv].[AccessDelivery](
	[Event Status] [nvarchar](255) NULL,
	[Event_ID] [float] NULL,
	[AD Event WF] [nvarchar](255) NULL,
	[Workflow Status] [nvarchar](255) NULL,
	[Created By] [nvarchar](255) NULL,
	[AccessTableID] [nvarchar](255) NULL,
	[AccessTableTag] [nvarchar](255) NULL,
	[Activator Picker] [nvarchar](255) NULL,
	[AD Type] [nvarchar](255) NULL,
	[Assigned To] [nvarchar](255) NULL,
	[Auth_Status] [nvarchar](255) NULL,
	[Bridge Number] [nvarchar](255) NULL,
	[Bridge PIN] [nvarchar](255) NULL,
	[CharsID] [nvarchar](255) NULL,
	[Circuit/Private Line ID Table] [nvarchar](255) NULL,
	[Completed E-Mail CC:] [nvarchar](255) NULL,
	[Content Type] [nvarchar](255) NULL,
	[Created] [datetime] NULL,
	[Customer Contact Cell Phone] [nvarchar](255) NULL,
	[Customer Contact Name] [nvarchar](255) NULL,
	[Customer Contact Pager] [nvarchar](255) NULL,
	[Customer Contact Pager PIN] [nvarchar](255) NULL,
	[Customer Contact Phone] [nvarchar](255) NULL,
	[Customer Email] [nvarchar](255) NULL,
	[Customer Name] [nvarchar](255) NULL,
	[CUSTOMER_CSG_LVL] [nvarchar](255) NULL,
	[Description and Comments] [nvarchar](max) NULL,
	[EmailBody] [nvarchar](255) NULL,
	[End Time] [datetime] NULL,
	[Escalation Reason] [nvarchar](255) NULL,
	[Event Duration] [float] NULL,
	[Event Type] [nvarchar](255) NULL,
	[Extra Duration] [nvarchar](255) NULL,
	[FTN] [nvarchar](255) NULL,
	[H1] [nvarchar](255) NULL,
	[H6] [nvarchar](255) NULL,
	[ID] [float] NULL,
	[Is CPE Attending?] [bit] NOT NULL,
	[Is Escalation] [bit] NOT NULL,
	[Item Title] [nvarchar](255) NULL,
	[L2P_RECORD] [nvarchar](255) NULL,
	[L2P_VERIFIED] [nvarchar](255) NULL,
	[L2P_VERIFIED_DATETIME] [datetime] NULL,
	[Links (Documents / Cut Sheets)] [nvarchar](255) NULL,
	[MaskedContName] [nvarchar](255) NULL,
	[MaskedCustName] [nvarchar](255) NULL,
	[MaskedCustPhone] [nvarchar](255) NULL,
	[Maskedemail] [nvarchar](255) NULL,
	[MEMBER_CSG_LVL] [nvarchar](255) NULL,
	[Modified] [datetime] NULL,
	[Modified By] [nvarchar](255) NULL,
	[Primary Request Date] [datetime] NULL,
	[Published E-Mail CC:] [nvarchar](255) NULL,
	[Request Contact Cell Phone] [nvarchar](255) NULL,
	[Request Contact E-Mail] [nvarchar](255) NULL,
	[Request Contact Name] [nvarchar](255) NULL,
	[Request Contact Pager Phone] [nvarchar](255) NULL,
	[Request Contact Pager PIN] [nvarchar](255) NULL,
	[Request Contact Phone] [nvarchar](255) NULL,
	[Sales Engineer (SE) E-Mail] [nvarchar](255) NULL,
	[Sales Engineer (SE) Name] [nvarchar](255) NULL,
	[Sales Engineer (SE) Phone] [nvarchar](255) NULL,
	[Secondary Request Date] [datetime] NULL,
	[Short Notice] [nvarchar](255) NULL,
	[Slot Picker] [nvarchar](255) NULL,
	[Start Time] [datetime] NULL,
	[Submit Type] [nvarchar](255) NULL,
	[Total Event Duration] [nvarchar](255) NULL,
	[UnMasked_ROW_ID] [nvarchar](255) NULL,
	[Item Type] [nvarchar](255) NULL,
	[Path] [nvarchar](255) NULL,
	[Status] [int] NULL
) ON [PRIMARY]
END
GO
/****** Object:  Default [DF__AccessDel__Statu__09AB0850]    Script Date: 01/05/2012 10:26:15 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__AccessDel__Statu__09AB0850]') AND parent_object_id = OBJECT_ID(N'[cnv].[AccessDelivery]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__AccessDel__Statu__09AB0850]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[AccessDelivery] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__EVENT_Tmp__Statu__73BBC731]    Script Date: 01/05/2012 10:26:15 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__EVENT_Tmp__Statu__73BBC731]') AND parent_object_id = OBJECT_ID(N'[cnv].[EVENT_Tmp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__EVENT_Tmp__Statu__73BBC731]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[EVENT_Tmp] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__MDSEvent__Status__1F9A496F]    Script Date: 01/05/2012 10:26:15 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__MDSEvent__Status__1F9A496F]') AND parent_object_id = OBJECT_ID(N'[cnv].[MDSEvent]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MDSEvent__Status__1F9A496F]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[MDSEvent] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__MDSFastTr__Statu__75A40FA3]    Script Date: 01/05/2012 10:26:15 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__MDSFastTr__Statu__75A40FA3]') AND parent_object_id = OBJECT_ID(N'[cnv].[MDSFastTrack]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MDSFastTr__Statu__75A40FA3]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[MDSFastTrack] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__MPLSEvent__Statu__769833DC]    Script Date: 01/05/2012 10:26:15 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__MPLSEvent__Statu__769833DC]') AND parent_object_id = OBJECT_ID(N'[cnv].[MPLSEvent]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__MPLSEvent__Statu__769833DC]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[MPLSEvent] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__NGVNEvent__Statu__778C5815]    Script Date: 01/05/2012 10:26:15 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__NGVNEvent__Statu__778C5815]') AND parent_object_id = OBJECT_ID(N'[cnv].[NGVNEvent]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__NGVNEvent__Statu__778C5815]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[NGVNEvent] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__ORDR_Tmp__Status__7974A087]    Script Date: 01/05/2012 10:26:15 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__ORDR_Tmp__Status__7974A087]') AND parent_object_id = OBJECT_ID(N'[cnv].[ORDR_Tmp]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__ORDR_Tmp__Status__7974A087]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[ORDR_Tmp] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__Qualifica__Statu__78807C4E]    Script Date: 01/05/2012 10:26:15 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__Qualifica__Statu__78807C4E]') AND parent_object_id = OBJECT_ID(N'[cnv].[Qualifications]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Qualifica__Statu__78807C4E]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[Qualifications] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__SOWSActiv__Statu__7A68C4C0]    Script Date: 01/05/2012 10:26:16 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__SOWSActiv__Statu__7A68C4C0]') AND parent_object_id = OBJECT_ID(N'[cnv].[SOWSActivators]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__SOWSActiv__Statu__7A68C4C0]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[SOWSActivators] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__SOWSRevie__Statu__7B5CE8F9]    Script Date: 01/05/2012 10:26:16 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__SOWSRevie__Statu__7B5CE8F9]') AND parent_object_id = OBJECT_ID(N'[cnv].[SOWSReviewvers]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__SOWSRevie__Statu__7B5CE8F9]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[SOWSReviewvers] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__SOWSUsers__Statu__7C510D32]    Script Date: 01/05/2012 10:26:16 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__SOWSUsers__Statu__7C510D32]') AND parent_object_id = OBJECT_ID(N'[cnv].[SOWSUsers]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__SOWSUsers__Statu__7C510D32]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[SOWSUsers] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__SprintLin__Statu__7D45316B]    Script Date: 01/05/2012 10:26:16 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__SprintLin__Statu__7D45316B]') AND parent_object_id = OBJECT_ID(N'[cnv].[SprintLink]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__SprintLin__Statu__7D45316B]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[SprintLink] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__Subscript__Statu__7E3955A4]    Script Date: 01/05/2012 10:26:16 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__Subscript__Statu__7E3955A4]') AND parent_object_id = OBJECT_ID(N'[cnv].[Subscriptions]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__Subscript__Statu__7E3955A4]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[Subscriptions] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
/****** Object:  Default [DF__TmpUsers__Status__7F2D79DD]    Script Date: 01/05/2012 10:26:16 ******/
IF Not EXISTS (SELECT * FROM sys.default_constraints WHERE object_id = OBJECT_ID(N'[cnv].[DF__TmpUsers__Status__7F2D79DD]') AND parent_object_id = OBJECT_ID(N'[cnv].[TmpUsers]'))
Begin
IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF__TmpUsers__Status__7F2D79DD]') AND type = 'D')
BEGIN
ALTER TABLE [cnv].[TmpUsers] ADD  DEFAULT ((0)) FOR [Status]
END


End
GO
