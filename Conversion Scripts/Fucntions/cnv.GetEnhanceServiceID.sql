USE [COWS] 
GO
_CreateObject 'FS','cnv','GetEnhanceServiceID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Naidu Vattigunta
-- Create date: 06/30/2011
-- Description:	Gets the Enhance ID by passing the AD Type
-- =============================================
ALTER FUNCTION [cnv].[GetEnhanceServiceID] 
(
	@ADType VARCHAR (50)
)
RETURNS TINYINT 
AS
BEGIN
	DECLARE @EnID TINYINT  
	
	IF LTRIM (RTRIM (@ADType)) =  'AD GI' 
	BEGIN
	
		SELECT @EnID = ENHNC_SRVC_ID  FROM dbo.LK_ENHNC_SRVC WITH (NOLOCK) 
		WHERE  ENHNC_SRVC_NME = 'AD Government or International'
		END
		ELSE 
		
		 	SELECT @EnID = ENHNC_SRVC_ID  FROM dbo.LK_ENHNC_SRVC WITH (NOLOCK) 
			WHERE  ENHNC_SRVC_NME = @ADType
	RETURN  @EnID
	
 
END
