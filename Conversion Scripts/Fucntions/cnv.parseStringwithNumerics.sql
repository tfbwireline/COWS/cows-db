USE [COWS]
GO
_CreateObject 'FTV','cnv','parseStringwithNumerics'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:		Naidu Vattigunta
-- Create date: 7/27/11
 -- ======================================================================

ALTER FUNCTION [cnv].[parseStringwithNumerics]
(
	@Mac varchar(max)
 
)
RETURNS @PrIDs table
 (
	IDs Varchar(1)
 )
 
AS
BEGIN
 
DECLARE @Data nVarchar(1000)
DECLARE @Len int
DECLARE @ID Varchar(1)
DECLARE @Char Varchar(10)
DECLARE @Ctr INt
SET @Ctr = 0 
 
 
SELECT @Len = LEN(@Mac)

IF @Len > 0

BEGIN
	WHILE	@Ctr <= @Len
	  BEGIN
		  SELECT @ID = SUBSTRING(@mac, @Ctr, 1)
				
				 IF ISNUMERIC(@ID) = 1 
					  BEGIN
								  
						  INSERT INTO  @PrIDs (IDs)
						  VALUES (@ID)
					   END
						    SET @Ctr = @Ctr + 1
					  
		 END 
   END
   
   RETURN 

END