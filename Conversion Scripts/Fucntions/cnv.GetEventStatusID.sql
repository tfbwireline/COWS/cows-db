USE [COWS]
GO
_CreateObject 'FS','cnv','GetEventStatusID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Naidu Vattigunta
-- Create date: 06/30/2011
-- Description:	Gets the Event Status ID by passing the Status
-- =============================================
ALTER FUNCTION [cnv].[GetEventStatusID] 
(
	@EStatus VARCHAR (20)
)
RETURNS TINYINT 
AS
BEGIN
	DECLARE @ESID TINYINT   
	SELECT @ESID =   ISNULL(EVENT_STUS_ID,0) 
		FROM dbo. LK_EVENT_STUS WITH (NOLOCK) 
		WHERE  EVENT_STUS_DES= @EStatus
		 
	RETURN ISNULL(@ESID,0)
	
 
END