USE [COWS] 
GO
_CreateObject 'FS','cnv','GetAPPTTypeID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Naidu Vattigunta
-- Create date: 06/30/2011
-- Description:	Gets the APPT Type  ID by passing the AD Type
-- =============================================
ALTER FUNCTION [cnv].[GetAPPTTypeID] 
(
	@SubmitType VARCHAR (50)
)
RETURNS TINYINT 
AS
BEGIN
	DECLARE @EnID TINYINT  
	 
	
		SELECT @EnID = APPT_TYPE_ID FROM dbo.LK_APPT_TYPE WITH (NOLOCK) 
		WHERE APPT_TYPE_DES = @SubmitType
	 
 
	RETURN  @EnID
	
 
END
