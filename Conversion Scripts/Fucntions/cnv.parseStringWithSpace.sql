USE [COWS]
GO
_CreateObject 'FTV','cnv','parseStringWithSpace'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:		Naidu Vattigunta
-- Create date: 11/15/2011
 
-- ======================================================================

ALTER FUNCTION [cnv].[parseStringWithSpace]
(
	@StringList varchar(max) 
	
)
RETURNS 
@ParsedList table
(
	StringID varchar(400)
)
AS
BEGIN
	DECLARE @ID varchar(400), @Pos int
	DECLARE @Delimiter char(1) 
 
		SET @Delimiter = ','
	
SET @StringList =	REPLACE (@StringList, ' ', @Delimiter)
	 

	SET @StringList = LTRIM(RTRIM(@StringList))+ @Delimiter
	SET @Pos = CHARINDEX(@Delimiter, @StringList, 1)

	IF REPLACE(@StringList, @Delimiter, '') <> ''
	
DECLARE @ParsedList2 table
(
	Ids Int NOT NULL IDENTITY,
	StringID varchar(400)
)
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @ID = LTRIM(RTRIM(LEFT(@StringList, @Pos - 1)))
			DECLARE	@NewLine		Char(2)
			SET		@NewLine	=	Char(13) + Char(10)
			SET		@ID			=	REPLACE(@ID, @NewLine, '')
			INSERT INTO @ParsedList2 (StringID) 
			VALUES ( @ID ) --Use Appropriate conversion
			SET @StringList = RIGHT(@StringList, LEN(@StringList) - @Pos)
			SET @Pos = CHARINDEX(@Delimiter, @StringList, 1)

		END
 
	 	INSERT  INTO @ParsedList (StringID)
       SELECT StringID FROM @ParsedList2 WHERE Ids%2 <> 0
	END	
	RETURN
END