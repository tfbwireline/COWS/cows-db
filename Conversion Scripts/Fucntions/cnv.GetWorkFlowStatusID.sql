USE [COWS] 
GO
_CreateObject 'FS','cnv','GetWorkFlowStatusID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Naidu Vattigunta
-- Create date: 06/30/2011
-- Description:	Gets the WorkFlow Status ID by passing the Status
-- =============================================
ALTER FUNCTION [cnv].[GetWorkFlowStatusID] 
(
	@Status VARCHAR (20)
)
RETURNS TINYINT 
AS
BEGIN
	DECLARE @WFID TINYINT   
	SELECT @WFID =   ISNULL(WRKFLW_STUS_ID,0) 
		FROM dbo.LK_WRKFLW_STUS WITH (NOLOCK) 
		WHERE  WRKFLW_STUS_DES = @Status
		 
	RETURN ISNULL(@WFID,0)
	
 
END