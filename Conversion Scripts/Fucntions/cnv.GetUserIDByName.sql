USE [COWS] 
GO
_CreateObject 'FS','cnv','GetUserIDByName'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Naidu Vattigunta
-- Create date: 06/30/2011
-- Description:	Gets the userID from lkUser table by passing the name
-- =============================================
ALTER FUNCTION [cnv].[GetUserIDByName] 
(
	@Name VARCHAR (100)
)
RETURNS  INT 
AS
BEGIN
	DECLARE @UID  INT   
	SELECT @UID =   ISNULL(USER_ID,0) 
		FROM  dbo.LK_USER WITH (NOLOCK) 
		WHERE  DSPL_NME = @Name
		 
	RETURN ISNULL(@UID,0)
	
 
END
