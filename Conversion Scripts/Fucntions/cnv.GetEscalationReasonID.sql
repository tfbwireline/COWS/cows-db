USE [COWS]
GO
_CreateObject 'FS','cnv','GetEscalationReasonID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Naidu Vattigunta
-- Create date: 06/30/2011
-- Description:	Gets the escalation reasonid  by passing the reason
-- =============================================
ALTER FUNCTION [cnv].[GetEscalationReasonID] 
(
	@Reason VARCHAR (100)
)
RETURNS  INT 
AS
BEGIN
	DECLARE @ERID  INT   
	SELECT @ERID =   ISNULL(ESCL_REAS_ID,0) 
		FROM  LK_ESCL_REAS WITH (NOLOCK) 
		WHERE  ESCL_REAS_DES = @Reason
		 
	RETURN ISNULL(@ERID,0)
	
 
END
