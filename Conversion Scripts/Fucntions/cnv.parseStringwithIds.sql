USE [COWS]
GO
_CreateObject 'FTV','cnv','parseStringwithIds'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:		Naidu Vattigunta
-- Create date: 7/27/11
 -- ======================================================================

ALTER FUNCTION [cnv].[parseStringwithIds]
(
	@List varchar(max)
 
)
RETURNS @Desc table
 (
	DesValue Varchar(100)
 )
 
AS
BEGIN


DECLARE @Pos INT,
@NextPos   INT,
@Len   INT
 
 
SET @List = '#'  + @List  

DECLARE @StrIds TABLE
	(Ids Int NOT NULL IDENTITY,
	Number nVarchar(100) )

	SELECT @Pos = 0, @NextPos = 1

		WHILE @NextPos > 0
				BEGIN
				SELECT @nextpos = charindex(';#', @List , @Pos +1)
				SELECT @Len = CASE WHEN @NextPos > 0
				THEN @NextPos
				ELSE len(@List) + 1
				END - @Pos - 1

	INSERT @StrIds(number)
	VALUES (convert(nvarchar(100), substring(@List , @Pos +2, @Len - 1 )))
	SELECT @Pos = @NextPos
	END
 
INSERT  INTO @Desc (DesValue)
SELECT Number FROM @StrIds WHERE Ids%2 <> 0

RETURN
 
  
END
