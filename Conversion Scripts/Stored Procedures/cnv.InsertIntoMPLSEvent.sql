USE [COWS]
GO
_CreateObject 'SP','cnv','InsertIntoMPLSEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =======================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	06/30/2011 
-- Description:	Inserts Data into MPLS Event Delivery Table
 
-- ================================================================================================


ALTER PROCEDURE [cnv].[InsertIntoMPLSEvent]
AS

BEGIN
DECLARE @EventID int		
DECLARE @ESID TinyInt		
DECLARE @FTN Int		
DECLARE @CHARSID Varchar(20)		
DECLARE @H1 Varchar(9)		
DECLARE @H6 Varchar(9)		
DECLARE @CName Varchar(100)		
DECLARE @CConName Varchar(100)		
DECLARE @CPhone Varchar(100)		
DECLARE @Email Varchar(100)		
DECLARE @CCell Varchar(100)		
DECLARE @Pager Varchar(20)		
DECLARE @Ppin Varchar(20)		
DECLARE @RqID SmallInt		
DECLARE @SaleId SmallInt		
DECLARE @PubEmail Varchar(1000)		
DECLARE @CompEmail Varchar(1000)		
DECLARE @DDLoc Varchar(1000)		
DECLARE @DDAppNum Varchar(50)
DECLARE @NDDFlag Bit		
DECLARE @DescComments Varchar(2000)
DECLARE @MPLSEvTyp TinyInt
DECLARE @VPNPltfmType TinyInt
DECLARE @IpVerID TinyInt
DECLARE @ActLocID Char(1)
DECLARE @MulVRFReqID Char(1)
DECLARE @VRFName Varchar(50)
DECLARE @MDSManaged Bit
DECLARE @ADDE2E Bit
DECLARE @MDSVRFNme Varchar(50)
DECLARE @MDSIP Varchar(50)
DECLARE @MDSDCLI Varchar(50)
DECLARE @MDSSTC Varchar(50)
DECLARE @OnNetM Bit
DECLARE @WSPrtFlag Bit
DECLARE @WSPrtID TinyInt
DECLARE @NNIDesDocName Varchar(100)
DECLARE @CXRPtnr Varchar(20)
--DECLARE @CXRPtnr Bit
DECLARE @MPLSCXRPtnr Varchar(6)
DECLARE @OffNetMon Bit
DECLARE @MigrFlag Bit
DECLARE @MPLSMigTypeID Bit	
DECLARE @EscFlag bit		
DECLARE @PReqDate SmallDatetime		
DECLARE @SReqDate SmallDatetime		
DECLARE @EscID bit		
DECLARE @StartTime SmallDateTime		
DECLARE @ExtraDuration SmallInt		
DECLARE @EndTime SmallDateTime		
DECLARE @WFSId TinyInt		
DECLARE @RECStatusID Int		
DECLARE @CreateUId Int		
DECLARE @ModUid Int		
DECLARE @ModDate SmallDateTime		
DECLARE @CDate SmallDateTime		
DECLARE @EventTitle Varchar(255)	
DECLARE @ConfBrNum Varchar(50)		
DECLARE @ConfBrPin Varchar(20)	
DECLARE @LOCCityNAme Varchar(50)
DECLARE @LOCSTTName Varchar(20)
DECLARE @PDCktNum Int
DECLARE @OEFTNNum Int
DECLARE @VASSolNum Int
DECLARE @Net499Adr Varchar(20)
DECLARE @MPBDWDId Tinyint
DECLARE @MNSSIPAdr Varchar(100)
DECLARE @OFFRtrID TinyInt
DECLARE @RoutTypeID Char(1)
DECLARE @PRRptID TinyInt
DECLARE @TagCreateDt SmallDateTime
DECLARE @CountyID SmallInt
DECLARE @OldEventID Int
DECLARE @MaxID Int
DECLARE @TableID Varchar(100)
DECLARE @MPLSActivityType nVarchar(1000)
DECLARE @SOWSEVENTID INT 
DECLARE @AuthStatus BIT
DECLARE @VASTypes Varchar(100)
DECLARE @EventDuration INT



SET @RECStatusID = 1
SET @IpVerID	 = 0
SET @EventID     = 0
--SET @FTN  = 999999999
SET @CountyID = 1

DECLARE @Cnt			INT
DECLARE @Ctr			INT

-- =============================================================================================
/* 
Event Types for cnv.EVENT_Tmp table
1	AD
2	NGVN
3	Scheduled Implementation (MPLS)
4	Sprint Link
5	MDS
  */
-- ================================================================================================
SET NOCOUNT ON


UPDATE cnv.MPLSEvent SET Status = 4 WHERE ISNUMERIC(FTN) = 0 and ISNULL(FTN,'') <> ''

 
Begin Try

SELECT @MaxID = MAX(Event_ID) from EVENT
SET @MaxID = @MaxID + 1

DBCC CHECKIDENT ( [cnv.Event_Tmp], RESEED, @maxID)


 INSERT INTO cnv.EVENT_Tmp
			 (EventID,
			 EventStatus,
			 EventType,
			 CreateDate)
 SELECT   
			 me.Event_ID,
			 [cnv].[GetEventStatusID] (me.[Event Status]),
			 3,
			 me.[Created]
			 FROM cnv.MPLSEvent me WITH (NOLOCK)
		 WHERE Status = 0 AND ISNULL(Event_ID, '') <> ''
		AND   NOT EXISTS 
		(SELECT 'X' FROM  cnv.EVENT_Tmp et WITH (NOLOCK) WHERE 
		me.Event_ID = et.EventID AND Status in(0,2,5) AND EventType = 3) Order by Event_ID asc
  -- ================================================================================================
 
 
 		SET @Cnt = 0
		SET @Ctr = 1
		SELECT 		@Cnt	=	Count(1) FROM cnv.EVENT_Tmp Where Status = 0 and EventType = 3
		
		IF @Cnt	>	0
		BEGIN

			WHILE	@Ctr <= @Cnt
				BEGIN
	
						SELECT TOP 1	@EventID =    et.NewEventID,
										@OldEventID =  et.EventID,
										@SOWSEVENTID = et.EventID,
										@AuthStatus  =   CASE WHEN me.Auth_Status = 'AUTHORISED' AND ISNULL(me.FTN,0 ) <> 0  THEN 1 ELSE 0 END					
										FROM cnv.EVENT_Tmp et INNER JOIN
										cnv.MPLSEvent me WITH (NOLOCK) ON et.EventID = me.Event_ID						
										WHERE et.Status = 0 AND EventType = 3    Order by Event_ID asc
	 
	 PRINT @EventID
	 PRINT @OldEventID
  -- ================================================================================================
 	
SET @ESID = 0		
SET @FTN = 0		
SET @CHARSID =''		
SET @H1 =''		
SET @H6 =''		
SET @CName =''		
SET @CConName =''		
SET @CPhone =''		
SET @Email =''		
SET @CCell =''		
SET @Pager =''		
SET @Ppin =''		
SET @RqID  = 0		
SET @SaleId  = 0		
SET @PubEmail =''		
SET @CompEmail =''		
SET @DDLoc =''		
SET @DDAppNum =''
SET @NDDFlag = 0		
SET @DescComments =''
SET @MPLSEvTyp = 0
SET @VPNPltfmType = 0
 
SET @ActLocID =''
SET @MulVRFReqID =''
SET @VRFName =''
SET @MDSManaged = 0
SET @ADDE2E = 0
SET @MDSVRFNme =''
SET @MDSIP =''
SET @MDSDCLI =''
SET @MDSSTC =''
SET @OnNetM = 0
SET @WSPrtFlag = 0
SET @WSPrtID = 0
SET @NNIDesDocName =''
 
SET @CXRPtnr = 0
SET @MPLSCXRPtnr = NULL
SET @OffNetMon = 0
SET @MigrFlag = 0
SET @MPLSMigTypeID = 0	
SET @EscFlag = 0		
SET @PReqDate =''		
SET @SReqDate =''		
SET @EscID = 0		
SET @StartTime =''		
SET @ExtraDuration  = 0		
SET @EndTime =''		
SET @WFSId = 0		
 	
SET @CreateUId = 0		
SET @ModUid = 0		
SET @ModDate =''		
SET @CDate =''		
SET @EventTitle =''	
SET @ConfBrNum =''		
SET @ConfBrPin =''	
SET @LOCCityNAme =''
SET @LOCSTTName =''
SET @PDCktNum = 0
SET @OEFTNNum = 0
SET @VASSolNum = 0
SET @Net499Adr =''
SET @MPBDWDId = 0
SET @MNSSIPAdr =''
SET @OFFRtrID = 0
SET @RoutTypeID =''
SET @PRRptID = 0
SET @TagCreateDt =''
SET @VASTypes = ''
 
 
SET @TableID =''
SET @MPLSActivityType =''
SET @EventDuration = 0
 
   -- ================================================================================================

    IF NOT EXISTS
    (SELECT 'X' FROM dbo.EVENT WITH (NOLOCK) WHERE EVENT_ID = @EventID)


	BEGIN
 	SET IDENTITY_INSERT DBO.EVENT ON


		 INSERT INTO DBO.EVENT
					 (	EVENT_ID,
						EVENT_TYPE_ID,
						CREAT_DT)
				  SELECT TOP 1
						  @EventID,
						 EventType,
						  CreateDate
				  FROM cnv.Event_Tmp WHERE NewEventID = @EventID AND EventType = 3 AND Status = 0 
 
	SET IDENTITY_INSERT DBO.EVENT OFF
	
	END
 -- ================================================================================================
 PRINT 'This is a new Event'
 -- ================================================================================================
 


		 SELECT TOP 1
		 --@EventID 	= 	evt.[NewEventID],
		 @ESID 		=   evt.[EventStatus],
		 @FTN 		=   SUBSTRING (mpls.FTN,1,10),
		 @CHARSID 	=	mpls.CharsID,	
		 @H1 		=	SUBSTRING (mpls.H1,1,9),
		 @H6 		=	SUBSTRING (mpls.H6,1,9),
		 @CName 	=	CASE WHEN @AuthStatus = 1 THEN 'Unknown' ELSE  mpls.[Customer Name] END,
		 @CConName 	=	CASE WHEN @AuthStatus = 1 THEN 'Unknown' ELSE mpls.[Customer Contact Name] END,
		 @CPhone 	=	CASE WHEN @AuthStatus = 1 THEN '123456' ELSE  mpls.[Customer Contact Phone] END,	
		 @Email 	=	CASE WHEN @AuthStatus = 1 THEN 'Private@sprint.com' ELSE  mpls.[Customer Email] END,	
		 @CCell 	=	CASE WHEN @AuthStatus = 1 THEN '123456' ELSE  SUBSTRING (mpls.[Customer Contact Cell Phone],1,20) END,	
		 @Pager 	=	CASE WHEN @AuthStatus = 1 THEN '123456' ELSE  mpls.[Customer Contact Pager] END,	
		 @Ppin 		=   mpls.[Customer Contact Pager PIN],
		 @RqID 		=	cnv.GetUserIDByName (mpls.[Request Contact Name]), 
		 @SaleId 	=	cnv.GetUserIDByName (mpls.[Sales Engineer (SE) Name]),	
		 @PubEmail  =	mpls.[Published E-Mail CC:], 	
		 @CompEmail =	mpls.[Completed E-Mail CC:], 		
		 @DDLoc 	=	mpls.[Design Document Location],	
		 @DDAppNum  =	mpls.[Design Document Approval Number],
		 @NDDFlag 	=	mpls.[Is NDD Updated?],
		 @DescComments =SUBSTRING(mpls.[Description and Comments],1,1000),	
		 @MPLSEvTyp	   =lkEvType.MPLS_EVENT_TYPE_ID,
		 @VPNPltfmType = LkVPNpt.VPN_PLTFRM_TYPE_ID,
		  @IpVerID  = lip.IP_VER_ID,
		 @ActLocID	= LKAcLoc.ACTY_LOCALE_ID,
		 @MulVRFReqID  = LKMVr.MULTI_VRF_REQ_ID,
		 @VRFName	= mpls.[VRF Name],
		 @MDSManaged = mpls.[MDS Managed],
		 @ADDE2E	= mpls.[Add End to End Monitoring],
		 @MDSVRFNme =mpls.[MDS VRF Name],
		 @MDSIP		= mpls.[MDS IP Address],
		 @MDSDCLI	= mpls.[MDS DLCI],
		 @MDSSTC	= mpls.[MDS Static Route],
		 @OnNetM	= mpls.[ON Net Monitoring?],
		 @WSPrtFlag = mpls.[Is this a Wholesale Partner?],
		 @WSPrtID	= lkWsp.WHLSL_PTNR_ID,
		 @NNIDesDocName = mpls.[NNI Design Document],
		 @CXRPtnr		=  CASE WHEN ISNULL(MPLS.[Is this a Carrier Partner?] ,0) = 0 THEN 0 
							ELSE MPLS.[Is this a Carrier Partner?] END,
		 --1 AS @MPLSCXRPtnr,	  
		 @OffNetMon  = mpls.[OFF Net Monitoring?],
		  @MigrFlag = ISNULL(MPLS.[Is this a Migration?] ,0), 
		 @MPLSMigTypeID =  lkMgt.MPLS_MGRTN_TYPE_ID,
		 @EscFlag =ISNULL(MPLS.[Is this an Escalation?] ,0) , 
				 @PReqDate  = mpls.[Primary Request Date],
				 @SReqDate  = mpls.[Secondary Request Date],
				 @EscID     = LkEr.ESCL_REAS_ID,
				 @StartTime = mpls.[Start Time],
				 @ExtraDuration =mpls.[Extra Duration],
				 @EndTime   = mpls.[End Time],
				 @WFSId		= ISNULL(lkWF.WRKFLW_STUS_ID, 0),
				 ----@RECStatusID
				 @CreateUId =  CASE WHEN ISNULL (cnv.GetUserIDByName (mpls.[Created By]), 0) = 0 THEN 1 ELSE cnv.GetUserIDByName (mpls.[Created By]) END ,
				 @ModUid    =  CASE WHEN ISNULL (cnv.GetUserIDByName (mpls.[Modified By]),0) = 0 THEN 1 ELSE cnv.GetUserIDByName (mpls.[Modified By]) END,
				 @ModDate   = mpls.Modified,
				 @CDate     = mpls.Created,
				 @EventTitle = mpls.[Item Title],
	 			 @ConfBrNum = mpls.[Bridge Number],
				 @ConfBrPin = mpls.[Bridge PIN],
				 @LOCCityNAme = mpat.[LocationCity],
				 @LOCSTTName  = mpat.[LocationState],
				 @PDCktNum  =  CASE WHEN ISNUMERIC(mpat.[PrivateLineNumber]) = 1 THEN SUBSTRING (mpat.[PrivateLineNumber],1,8) ELSE '' END,
				 @OEFTNNum  =  CASE WHEN ISNUMERIC(mpat.[TransportNumber]) = 1 AND RIGHT (mpat.[TransportNumber], 1) <> ',' THEN SUBSTRING (mpat.[TransportNumber],1,8) ELSE '' END ,
				 --@VASSolNum   =  SUBSTRING (mpat.[VASSOL],1,20),
				@VASSolNum   = CASE WHEN ISNUMERIC(mpat.[VASSOL])  = 1 THEN  SUBSTRING (mpat.[VASSOL],1,20) ELSE '' END,
				--@Net499Adr  = mpat.[Number449],
				@MPBDWDId  =  adwd.MPLS_ACCS_BDWD_ID,
				@MNSSIPAdr  = mpat.[MNSIPAddress],
				@OFFRtrID   =  mor.MNS_OFFRG_ROUTR_ID,
				@RoutTypeID  =mrt.MNS_ROUTG_TYPE_ID,
				@PRRptID   = mpr.MNS_PRFMC_RPT_ID,
				@TagCreateDt  = CASE WHEN ISNULL (mpat.[Created], 0) = 0 THEN GETDATE() ELSE mpat.[Created] END,
				@TableID	  = mpls.AccessTableID,
				@MPLSActivityType = mpls.[MPLS Activity Type],
				@VASTypes		 = mpls.[VAS Types],
				@EventDuration  = mpls.[Event Duration]
				-----@CountyID   This need clarif

		  
		 FROM cnv.MPLSEvent mpls WITH (NOLOCK)
		 INNER JOIN cnv.EVENT_Tmp  evt WITH (NOLOCK) ON mpls.Event_ID = evt.EventId
		 INNER JOIN dbo. LK_EVENT_STUS lkET WITH (NOLOCK) ON mpls.[Event Status] = lkET.EVENT_STUS_DES
		 LEFT JOIN dbo.LK_MPLS_EVENT_TYPE  lkEvType WITH (NOLOCK)  ON mpls.[MPLS Event Type]= lkEvType.MPLS_EVENT_TYPE_DES
		 LEFT JOIN dbo.LK_VPN_PLTFRM_TYPE  LkVPNpt WITH (NOLOCK) ON mpls.[VPN Platform Type]= LkVPNpt.VPN_PLTFRM_TYPE_DES 
		 LEFT JOIN dbo.LK_ACTY_LOCALE LKAcLoc WITH (NOLOCK) ON mpls.[Activity Locale] = LKAcLoc.ACTY_LOCALE_DES
		 LEFT JOIN dbo.LK_MULTI_VRF_REQ LKMVr WITH (NOLOCK) ON mpls.[Multi VRF Requirements] = LKMVr.MULTI_VRF_REQ_DES
		 LEFT JOIN dbo.LK_WHLSL_PTNR  lkWsp WITH (NOLOCK) ON mpls.[Wholesale Partner] = lkWsp.WHLSL_PTNR_NME
		 LEFT JOIN dbo.LK_MPLS_MGRTN_TYPE lkMgt WITH (NOLOCK) ON mpls.[Migration Type]= lkMgt.MPLS_MGRTN_TYPE_DES 
		 LEFT JOIN LK_ESCL_REAS LkEr WITH (NOLOCK)			 ON mpls.[Escalation Reason] = LkEr.ESCL_REAS_DES
		 LEFT  JOIN dbo.LK_WRKFLW_STUS LkWF WITH (NOLOCK) ON	 mpls.[Workflow Status] = lkWF.WRKFLW_STUS_DES
		 LEFT  JOIN cnv.MPLSAccessTag mpat WITH (NOLOCK) ON  mpat.[TableID] = mpls.[AccessTableID]
		 LEFT JOIN dbo.LK_MPLS_ACCS_BDWD adwd WITH (NOLOCK)  ON mpat.AccessBandwidth = adwd.MPLS_ACCS_BDWD_DES
		 LEFT JOIN dbo.LK_MNS_OFFRG_ROUTR mor WITH (NOLOCK) ON mpat.[MNSOffering] = mor.MNS_OFFRG_ROUTR_DES
		 LEFT JOIN dbo.LK_MNS_ROUTG_TYPE mrt WITH (NOLOCK) ON mpat.[MNSRouting] = mrt.MNS_ROUTG_TYPE_DES
		 LEFT JOIN dbo.LK_MNS_PRFMC_RPT mpr WITH (NOLOCK) ON mpat.[PerformanceReporting] = mpr.MNS_PRFMC_RPT_DES
		 LEFT JOIN dbo.LK_IP_VER lip WITH (NOLOCK) ON 
							 CASE WHEN mpls.[IP Version] = 'Dual Stack' THEN 'Dual Stack - MPLS'
								  WHEN mpls.[IP Version] = 'IPv6' THEN 'IPv6 - MPLS'
								  WHEN mpls.[IP Version] = 'IPv4' THEN 'IPv4 - MPLS'
								  
								  ELSE  mpls.[IP Version] END = lip.IP_VER_NME
		 WHERE
		 evt.NewEventID= @EventID
 

 
  -- ================================================================================================
  PRINT ' Select Complete '
  -- ================================================================================================
  
    IF NOT EXISTS
		(SELECT 'X' FROM dbo.MPLS_EVENT WITH (NOLOCK) WHERE EVENT_ID = @EventID)
 
		BEGIN
 
			ALTER TABLE dbo.MPLS_EVENT NOCHECK CONSTRAINT ALL 

  
				INSERT INTO dbo.MPLS_EVENT WITH (ROWLOCK) 
 
		([EVENT_ID]
      ,[EVENT_STUS_ID]
      ,[FTN]
      ,[CHARS_ID]
      ,[H1]
      ,[H6]
      ,[CUST_NME]
      ,[CUST_CNTCT_NME]
      ,[CUST_CNTCT_PHN_NBR]
      ,[CUST_EMAIL_ADR]
      ,[CUST_CNTCT_CELL_PHN_NBR]
      ,[CUST_CNTCT_PGR_NBR]
      ,[CUST_CNTCT_PGR_PIN_NBR]
      ,[REQOR_USER_ID]
      ,[SALS_USER_ID]
      ,[PUB_EMAIL_CC_TXT]
      ,[CMPLTD_EMAIL_CC_TXT]
      ,[DOC_LINK_TXT]
      ,[DD_APRVL_NBR]
      ,[NDD_UPDTD_CD]
      ,[DES_CMNT_TXT]
      ,[MPLS_EVENT_TYPE_ID]
      ,[VPN_PLTFRM_TYPE_ID]
      ,[IP_VER_ID]
      ,[ACTY_LOCALE_ID]
      ,[MULTI_VRF_REQ_ID]
      ,[VRF_NME]
      ,[MDS_MNGD_CD]
      ,[ADD_E2E_MONTRG_CD]
      ,[MDS_VRF_NME]
      ,[MDS_IP_ADR]
      ,[MDS_DLCI]
      ,[MDS_STC_RTE_DES]
      ,[ON_NET_MONTRG_CD]
      ,[WHLSL_PTNR_CD]
      ,[WHLSL_PTNR_ID]
      ,[NNI_DSGN_DOC_NME]
      ,[CXR_PTNR_CD]
      ,[MPLS_CXR_PTNR_ID]
      ,[OFF_NET_MONTRG_CD]
      ,[MGRTN_CD]
      ,[MPLS_MGRTN_TYPE_ID]
      ,[ESCL_CD]
      ,[PRIM_REQ_DT]
      ,[SCNDY_REQ_DT]
      ,[ESCL_REAS_ID]
      ,[STRT_TMST]
      ,[EXTRA_DRTN_TME_AMT]
      ,[END_TMST]
      ,[WRKFLW_STUS_ID]
      ,[REC_STUS_ID]
      ,[CREAT_BY_USER_ID]
      ,[MODFD_BY_USER_ID]
      ,[MODFD_DT]
      ,[CREAT_DT]
      ,[EVENT_TITLE_TXT]
      ,[CNFRC_BRDG_NBR]
      ,[CNFRC_PIN_NBR]
      ,[SOWS_EVENT_ID]
      ,[EVENT_DRTN_IN_MIN_QTY])
      
      
      VALUES
      ( @EventID, 		
 @ESID, 		
 CASE WHEN ISNULL(@FTN ,0) = 0 THEN -1 ELSE @FTN END,		
 @CHARSID, 		
 @H1, 		
 @H6, 		
 @CName ,		
 @CConName ,		
 @CPhone, 		
 @Email, 		
 @CCell, 		
 @Pager, 		
 @Ppin, 		
 @RqID, 		
 @SaleId, 		
 @PubEmail,  	
 @CompEmail , 		
 @DDLoc,		
 @DDAppNum ,
 @NDDFlag,	
 @DescComments ,
 @MPLSEvTyp,
 @VPNPltfmType ,
 CASE WHEN ISNULL (@IpVerID ,0) = 0 THEN 0 ELSE @IpVerID END,
 @ActLocID ,
 @MulVRFReqID ,
 @VRFName ,
 @MDSManaged ,
 @ADDE2E ,
 @MDSVRFNme ,
 @MDSIP ,
 @MDSDCLI, 
 @MDSSTC ,
 @OnNetM ,
 @WSPrtFlag ,
 @WSPrtID ,
 @NNIDesDocName ,
 @CXRPtnr ,
 @MPLSCXRPtnr ,
 @OffNetMon ,
 @MigrFlag ,
 @MPLSMigTypeID,
 @EscFlag, 		
 @PReqDate, 		
 @SReqDate, 		
 @EscID, 		
 @StartTime,
 @ExtraDuration, 		
 @EndTime, 		
 @WFSId, 		
 @RECStatusID, 		
 @CreateUId, 		
 @ModUid, 		
 @ModDate, 		
 @CDate, 		
 @EventTitle, 	
 @ConfBrNum, 		
 @ConfBrPin,
 @SOWSEVENTID,
 CASE WHEN ISNULL (@EventDuration, 0) = 0 THEN 0 ELSE @EventDuration END	
)
  
   			
		  
			ALTER TABLE dbo.MPLS_EVENT CHECK CONSTRAINT ALL 
		
  -- ================================================================================================
 PRINT ' STEP 1 MPLS Event Insert Complete'
 -- ================================================================================================
 END 
 
 
  IF EXISTS  
  (SELECT 'X' FROM cnv.MPLSAccessTag  WHERE  TableID  = @TableID  )
  
		BEGIN
			 ALTER TABLE [dbo].[MPLS_EVENT_ACCS_TAG]  NOCHECK CONSTRAINT ALL 
     
  
				INSERT INTO dbo.MPLS_EVENT_ACCS_TAG
				(  [EVENT_ID]
				  ,[LOC_CTY_NME]
				  ,[LOC_STT_NME]
				  ,[PL_DAL_CKT_NBR]
				  ,[TRNSPRT_OE_FTN_NBR]
				  ,[VAS_SOL_NBR]
				  ,[TRS_NET_449_ADR]
				  ,[MPLS_ACCS_BDWD_ID]
				  ,[MNS_SIP_ADR]
				  ,[MNS_OFFRG_ROUTR_ID]
				  ,[MNS_ROUTG_TYPE_ID]
				  ,[MNS_PRFMC_RPT_ID]
				  ,[REC_STUS_ID]
				  ,[CREAT_DT]
				  ----,[CTRY_ID]
				   )
  
  
		SELECT   DISTINCT
			    @EventID,
				mpat.[LocationCity], 
				mpat.[LocationState],
				CASE WHEN ISNUMERIC(mpat.[PrivateLineNumber]) = 1 THEN SUBSTRING (mpat.[PrivateLineNumber],1,8) ELSE '' END,
				CASE WHEN ISNUMERIC(mpat.[TransportNumber]) = 1  AND RIGHT (mpat.[TransportNumber], 1) <> ','  THEN SUBSTRING (mpat.[TransportNumber],1,8) ELSE '' END,
				CASE WHEN ISNUMERIC(mpat.[VASSOL])  = 1 THEN  SUBSTRING (mpat.[VASSOL],1,20) ELSE '' END,
				SUBSTRING (mpat.[Number449],1,20),
				adwd.MPLS_ACCS_BDWD_ID, 
				SUBSTRING (mpat.[MNSIPAddress],1,50),
				mor.MNS_OFFRG_ROUTR_ID,
				mrt.MNS_ROUTG_TYPE_ID, 
		       mpr.MNS_PRFMC_RPT_ID ,
				@RECStatusID,
				CASE WHEN ISNULL (mpat.[Created], 0) = 0 THEN GETDATE() ELSE mpat.[Created] END 
				
				from cnv.MPLSAccessTag mpat with (NOLOCK) 
		INNER JOIN cnv.MPLSEvent mpl with (NOLOCK) ON mpat.TableID = mpl.AccessTableID
		INNER JOIN cnv.EVENT_Tmp et WITH (NOLOCK) ON mpl.Event_ID = et.EventID
		LEFT JOIN dbo.LK_MPLS_ACCS_BDWD adwd WITH (NOLOCK)  ON mpat.AccessBandwidth = adwd.MPLS_ACCS_BDWD_DES
	    LEFT JOIN dbo.LK_MNS_OFFRG_ROUTR mor WITH (NOLOCK) ON mpat.[MNSOffering] = mor.MNS_OFFRG_ROUTR_DES
	    LEFT JOIN dbo.LK_MNS_ROUTG_TYPE mrt WITH (NOLOCK) ON mpat.[MNSRouting] = mrt.MNS_ROUTG_TYPE_DES
	    LEFT JOIN dbo.LK_MNS_PRFMC_RPT mpr WITH (NOLOCK) ON mpat.[PerformanceReporting] = mpr.MNS_PRFMC_RPT_DES
		 
		WHERE mpat.TableID = @TableID
		
		
		ALTER TABLE [dbo].[MPLS_EVENT_ACCS_TAG]  CHECK CONSTRAINT ALL 
    -- ================================================================================================
 PRINT ' STEP 2 MPLS Event Access Tag Complete'
 -- ================================================================================================
  END
  IF ISNULL(@MPLSActivityType, '') <> ''
  
  BEGIN
  
  INSERT INTO [dbo].[MPLS_EVENT_ACTY_TYPE]
			
			(  [EVENT_ID]
			  ,[MPLS_ACTY_TYPE_ID]
			  ,[CREAT_DT])
			  
		SELECT 
				@EventID,
				lma.MPLS_ACTY_TYPE_ID,
				GETDATE()
				
				FROM 
				
				cnv.parseStringwithIds (@MPLSActivityType) pst 				
				INNER JOIN dbo.LK_MPLS_ACTY_TYPE lma WITH (NOLOCK) ON pst.DesValue = lma.MPLS_ACTY_TYPE_DES
							 
  
  END
  
 
  
  
  
 -- ================================================================================================
 PRINT ' STEP 2(1) MPLS Event Activity Type Completed'
 -- ================================================================================================
 
   
     IF ISNULL(@VASTypes, '') <> ''
  
  BEGIN
  
  INSERT INTO  [dbo].[MPLS_EVENT_VAS_TYPE]
           ([EVENT_ID]
           ,[VAS_TYPE_ID]
           ,[CREAT_DT])
			  
		SELECT 
				@EventID,
				lma.VAS_TYPE_ID,
				GETDATE()
				
				FROM 
				
				cnv.parseStringwithIds (@VASTypes) pst 				
				INNER JOIN dbo.LK_VAS_TYPE lma WITH (NOLOCK) ON pst.DesValue = lma.VAS_TYPE_DES
							 
  
  END
   
  
  
 -- ================================================================================================
 PRINT ' STEP 2(2) MPLS Event VAS  Type Completed'
 -- ================================================================================================
    DECLARE @AssignID Varchar(100)
	 DECLARE @AuID Int
	 
	  
	SELECT @AssignID  =  [Assigned To]  FROM cnv.MPLSEvent WHERE Event_ID = @OldEventID
	IF ( ISNULL (@AssignID,'') <> '') 
	 
	BEGIN 	 
  
	
	  INSERT INTO 	dbo.EVENT_ASN_TO_USER		   
						([EVENT_ID]
					  ,[ASN_TO_USER_ID]
					  ,[CREAT_DT]
					  ,[REC_STUS_ID]) 
					  
				SELECT 
						@EventID,
						lu.[USER_ID],					 
						CASE WHEN ISNULL(ne.Created,'') ='' THEN GETDATE() ELSE ne.Created END,
						1
					 
					 FROM  dbo.LK_USER   lu WITH (NOLOCK)  
                     INNER JOIN cnv.parseStringwithIds (@AssignID) pst ON 
									 SUBSTRING(lu.DSPL_NME, 1, CHARINDEX('[',lu.DSPL_NME))
								  =  SUBSTRING(pst.DesValue, 1, CHARINDEX('[',pst.DesValue)) 
					INNER JOIN cnv.MPLSEvent  ne WITH (NOLOCK) ON ne.Event_ID = @OldEventID
					 AND NOT EXISTS
						(SELECT 'X'  FROM dbo.EVENT_ASN_TO_USER  ea WITH (NOLOCK)
						INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON ea.ASN_TO_USER_ID = lu.[USER_ID]
						WHERE 
						ea.EVENT_ID = @EventID AND ea.ASN_TO_USER_ID = CASE WHEN ISNULL (lu.[USER_ID],'') = '' THEN 1 ELSE lu.[USER_ID] END )
					
 
	 	END		
 
 

-- ================================================================================================	 	  
 PRINT 'Step 3 - Complete   Assigned User table'
-- ================================================================================================	  
 			 
	IF EXISTS 
		(SELECT 'X' FROM cnv.WFHistory  WHERE [Event ID] = @SOWSEVENTID)
		
	    BEGIN
									
			  										
			  INSERT INTO dbo.EVENT_HIST
							 
							 ([EVENT_ID]
							 ,[ACTN_ID]
							 ,[CMNT_TXT]
							 ,[CREAT_BY_USER_ID]
							 ,[MODFD_BY_USER_ID]
							 ,[MODFD_DT]
							 ,[CREAT_DT]
							 ,[PRE_CFG_CMPLT_CD]
							 ,[FAIL_REAS_ID])
							 
		      SELECT  
							 @EventID,
							 CASE WHEN ISNULL (lka.ACTN_ID, '') ='' THEN 1 ELSE lka.ACTN_ID END,
							 wfh.[Comment],
							 CASE WHEN ISNULL (cnv.GetUserIDByName (wfh.[Created BY]), '') = '' THEN 1 ELSE
								 cnv.GetUserIDByName (wfh.[Created BY]) END,
							CASE WHEN  ISNULL(cnv.GetUserIDByName (wfh.[Modified By]), '') = '' THEN 1 ELSE
							 cnv.GetUserIDByName (wfh.[Modified By]) END,
						 	 wfh.[Modified],
							 wfh.[Created], 
							 'N' AS [Pre Config Complete ID], --- This needs clarif
							  NULL AS [FAIL_REAS_ID]	 --- This needs clarif
						  
							 FROM cnv.WFHistory  wfh 
							 --LEFT JOIN	cnv.Event_Tmp evt  WITH (NOLOCK) on wfh.[EVENT ID] = evt.EVENTID
							 LEFT JOIN	dbo.LK_ACTN lka  WITH (NOLOCK) ON wfh.[Action] = lka.ACTN_DES
							--INNER JOIN	dbo.EVENT_FAIL_ACTY efa WITH (NOLOCK) on wfh.[Fail Codes] = efa.FAIL_ACTY_ID
							 WHERE wfh.[Event ID] = @OldEventID
-- ================================================================================================	 	
    PRINT 'Step 6 - Complete   Inserted WF History table'
-- ================================================================================================	 	
 
   
				   INSERT INTO dbo.EVENT_FAIL_ACTY
							  (
							  [EVENT_HIST_ID]
							 ,[FAIL_ACTY_ID]
							 ,[REC_STUS_ID]
							 ,[CREAT_DT]
							  )		
							
					  SELECT  DISTINCT
							  ehi.EVENT_HIST_ID,
							  lfa.FAIL_ACTY_ID,
							  1 AS [REC STUS ID],
							  ehi.CREAT_DT	
							  FROM    
							  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
							  cnv.Event_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
							  cnv.WFHistory wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
							  LK_FAIL_ACTY lfa WITH (NOLOCK) ON  lfa.FAIL_ACTY_DES = wfh.[Failed Activities]
							  WHERE ehi.EVENT_ID = @EventID
					  
-- ================================================================================================	 		  
	PRINT 'Step 7 - Complete   Inserted FAIL Act table'
-- ================================================================================================	 	
					INSERT INTO dbo.EVENT_SUCSS_ACTY
							([EVENT_HIST_ID]
							,[SUCSS_ACTY_ID]
							,[REC_STUS_ID]
							,[CREAT_DT]
							)
				   SELECT  DISTINCT
							  ehi.EVENT_HIST_ID,
							  lsa.SUCSS_ACTY_ID,
							  1 AS [REC STUS ID],
							  ehi.CREAT_DT	
							  FROM    
							  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
							  cnv.Event_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
							  cnv.WFHistory  wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
							  dbo.LK_SUCSS_ACTY  lsa  WITH (NOLOCK) ON  lsa.SUCSS_ACTY_DES= wfh.[Succssful Activities]
							  WHERE ehi.EVENT_ID = @EventID
-- ================================================================================================	 		  
	PRINT 'Step 8 - Complete   Inserted SUCCESS Act table'
-- ================================================================================================	 	
   	  END
 
 
  IF EXISTS 
  (SELECT 'X' FROM cnv.EventCalender WITH (NOLOCK)  WHERE EventID = @OldEventID)
   	  
	BEGIN
	
 

 	DECLARE @Id nVarchar(10)
	DECLARE @IdsoutPut nVarchar(max)
	DECLARE @IdsString nVarchar(max)
	DECLARE @outPut nVarchar(max)
	DECLARE @AssignedName nVarchar(max)
	DECLARE @STag nVarchar(100)
	DECLARE @eTag nVarchar(100)
	DECLARE @e1Tag nVarchar(100)
	DECLARE @mTag nVarchar(100)
	DECLARE @SubmitType Varchar(100)
	
	DECLARE @Counter INT
	DECLARE @Counts INT 
	
	
	    SET @Id = ''
	SET @IdsoutPut = ''
	SET @IdsString = ''
	SET @outPut = ''
	SET @AssignedName = ''
	SET @STag = ''
	SET @eTag = ''
	SET @e1Tag = ''
	SET @mTag = ''
	SET @SubmitType = ''
	
	SET @Counter = 0
	SET @Counts  = 0
	
	
	IF OBJECT_ID(N'tempdb..#TmpAssignMPLS', N'U') IS NOT NULL
				DROP TABLE #TmpAssignMPLS
				CREATE TABLE #TmpAssignMPLS 
					( AsnUser Varchar (1000),
					  xTag Varchar(200),
					  Status INT DEFAULT 0
					)

	
	 SET @STag =  '<ResourceIds> '
	 SET @eTag =  '   </ResourceIds> '
	 SET @e1Tag =  '" /> '
	 SET @mTag =  '<ResourceId Type="System.Int32" Value="'
	
		INSERT INTO #TmpAssignMPLS (AsnUser)
		SELECT DesValue FROM cnv.parseStringwithIds ( (SELECT TOP 1 [Assigned Activators] from cnv.EventCalender Where EventID = @OldEventID))
 
  IF ((SELECT TOP 1 ASNUser from #TmpAssignMPLS )<> 'SOWS ES Activators')
 
    BEGIN
       SET @Counts = 0
       SET @Counter = 1
 
        Select  @Counts =  COUNT(1) from #TmpAssignMPLS WHERE STATUS = 0
        
               
        IF (@Counts  > 0)
        BEGIN
        
		 WHILE	@Counter <= @Counts
        
			BEGIN     
			    SET @Id = ''
				SET @IdsoutPut = ''
				SET @IdsString = ''
				SET @outPut = ''
				SET @AssignedName = ''
              
				SELECT TOP 1 @AssignedName =   AsnUser from #TmpAssignMPLS WHERE Status = 0
				SELECT @Id =  [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE 
				SUBSTRING(DSPL_NME, 1, CHARINDEX('[',DSPL_NME))
				 = SUBSTRING(@AssignedName, 1, CHARINDEX('[',@AssignedName))
						  
							 
				SET @ID = CASE WHEN ISNULL(@ID, 0) = 0 THEN 1 ELSE @ID END
			
			 SELECT @IdsoutPut = @mTag  + '' + @Id + '' + @e1Tag
                  
           update #TmpAssignMPLS set Status = 1, xTag = @IdsoutPut where AsnUser = @AssignedName
      
          SET @Counter= @Counter + 1
         END
        
        END        
  
   
		SELECT @IdsString = COALESCE(@IdsString + '  ', '') + xTag
		FROM #TmpAssignMPLS		
		SELECT @outPut =   @STag  + ''+ @IdsString + ''+ @eTag
		
		END
        ELSE 
        
        BEGIN
        
        SELECT @Id = [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE DSPL_NME = 'COWS ES Activators'
             
		SET @outPut =   @STag  + ''+ @mTag  + '' + @Id + '' + @e1Tag + ''+ @eTag
		
        END    
		SELECT @SubmitType = [Submit Type] FROM cnv.MPLSEvent WHERE Event_ID = @OldEventID
		SET @SubmitType = CASE WHEN @SubmitType = 'MPLS STANDARD' THEN 'MPLS' 
						       WHEN @SubmitType = 'STANDARD'	THEN 'MPLS'	 ELSE @SubmitType END
		 	
		 	INSERT INTO dbo.APPT 
							
						  ([EVENT_ID]
						  ,[STRT_TMST]
						  ,[END_TMST]
						  ,[RCURNC_DES_TXT]
						  ,[SUBJ_TXT]
						  ,[DES]
						  ,[APPT_LOC_TXT]
						  ,[APPT_TYPE_ID]
						  ,[CREAT_BY_USER_ID]
						  ,[MODFD_BY_USER_ID]
						  ,[MODFD_DT]
						  ,[CREAT_DT]
						  ,[ASN_TO_USER_ID_LIST_TXT]
						  ,[RCURNC_CD])  
 
		  		  
				SELECT 
							@EventID,
							et.[Start Time],
							et.[End Time],
							'',
							REPLACE (et.[Item Title],@EventID,@EventID),
							et.Description,
							'',
							6,
							 --CASE WHEN ISNULL ([cnv].[GetAPPTTypeID](@SubmitType), '') = '' THEN 11 ELSE
			    	--		 [cnv].[GetAPPTTypeID] (@SubmitType) END,
							CASE WHEN ISNULL(cnv.GetUserIDByName (et.[Created By]),0 ) = 0 THEN 1  ELSE
							cnv.GetUserIDByName (et.[Created By])END,
							CASE WHEN ISNULL (cnv.GetUserIDByName (et.[Modified By]),0 ) = 0 THEN 1 
							ELSE cnv.GetUserIDByName (et.[Modified By]) END,
							et.Modified,
							et.Created,						
							@outPut,
							''
	 
							FROM cnv.EventCalender  Et WITH (NOLOCK)
							INNER JOIN cnv.MPLSEvent ng WITH (NOLOCK) ON ng.Event_ID = Et.EventID
				 			WHERE et.EventID = @OldEventID
 
  -- ================================================================================================	 		  
	PRINT 'Step 9 - Complete   Event Calender'
-- ================================================================================================	 
 
	 END
	 
	 
		UPDATE cnv.MPLSEvent SET Status = 1 WHERE Event_ID = @OldEventID
		 PRINT 'MPLS Tmp Updated'
  
		 UPDATE cnv.EVENT_Tmp SET status = 2 WHERE  NewEventID = @EventID
		 PRINT 'Event Tmp Updated'
 
  SET @Ctr = @Ctr + 1 
  PRINT 'Counter Incremented'
 END
 END
 
    
   END TRY 
  Begin Catch
	Exec [dbo].InsertErrorInfo
	  
  End Catch

  SET NOCOUNT OFF
  END
