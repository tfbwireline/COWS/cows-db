USE [COWS]
GO
_CreateObject 'SP','cnv','InsertIntoNGVNEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

-- =======================================================
-- Author  :	Naidu Vattigunta	
-- Create date:	06/30/2011 
-- Description:	Inserts Data into NGVN Event Table
 
-- ================================================================================================

ALTER PROCEDURE [cnv].[InsertIntoNGVNEvent]
AS

BEGIN

SET NOCOUNT ON
  
DECLARE @EventID INT
DECLARE @EventStatusID INT
DECLARE @NewEventID INT
DECLARE @MaxID INT
DECLARE @CEvenID INT
DECLARE @Cnt			INT
DECLARE @Ctr			INT
DECLARE @TableID  nvarchar(100)
DECLARE @AuthStatus BIT 
 
 -- =============================================================================================
/*  
1	AD
2	NGVN
3	Scheduled Implementation (MPLS)
4	Sprint Link
5	MDS
6	MDS Fast Track*/
-- ================================================================================================
 Begin Try  
 
 
UPDATE cnv.NGVNEvent SET Status = 4 WHERE ISNUMERIC(FTN) = 0 and ISNULL(FTN,'') <> ''
 
SELECT @MaxID = MAX(Event_ID) from EVENT
SET @MaxID = @MaxID + 1

DBCC CHECKIDENT ( [cnv.Event_Tmp], RESEED, @maxID)

   
 INSERT INTO cnv.EVENT_Tmp
 (EventID,
 EventStatus,
 EventType,
 CreateDate)
 SELECT   
 Event_ID,
 [cnv].[GetEventStatusID] (ne.[Event Status]),
 2,
  [Created]
 FROM cnv.NGVNEvent ne WITH (NOLOCK)
 WHERE Status = 0 AND ISNULL(Event_ID, '') <> ''
		AND   NOT EXISTS 
		(SELECT 'X' FROM  cnv.EVENT_Tmp et WITH (NOLOCK) WHERE 
		ne.Event_ID = et.EventID AND Status in(0,2,5) AND EventType = 2) Order by Event_ID asc
 
 
-- ================================================================================================	
		SET @Cnt = 0
		SET @Ctr = 1
		SELECT 	@Cnt	=	Count(1) FROM cnv.EVENT_Tmp Where Status = 0 and EventType = 2
		
		IF @Cnt	>	0
		BEGIN
			WHILE	@Ctr <= @Cnt
				BEGIN
						
						SET @EventStatusID = 0
						
 
						 SELECT  TOP 1  @EventID  = ev.EventID,
										@NewEventID = ev.NewEventID,
										@TableID  = nt.AccessTableID,
										@AuthStatus = CASE WHEN nt.Auth_Status = 'AUTHORISED'AND ISNULL(nt.FTN,0 ) <> 0 THEN 1 ELSE 0 END
								 FROM cnv.EVENT_Tmp ev WITH (NOLOCK)  
				 INNER JOIN cnv.NGVNEvent nt WITH (NOLOCK) ON ev.EventID = nt.Event_ID
                  WHERE ev.EventType = 2 and ev.[Status] = 0
      
-- ================================================================================================	
-- ================================================================================================			
 
		 IF NOT EXISTS
		(SELECT 'X' FROM dbo.EVENT WITH (NOLOCK) WHERE EVENT_ID  = @NewEventID )

					BEGIN
 					
				 SET IDENTITY_INSERT DBO.EVENT ON
					 INSERT INTO DBO.EVENT
						 (	EVENT_ID,
							EVENT_TYPE_ID,
							CREAT_DT) 
				 
	 		
			  SELECT 
						  NewEventID,
						  EventType,
						  CreateDate
						  FROM	cnv.EVENT_Tmp WHERE  NewEventID = @NewEventID    AND EventType = 2 AND [Status] = 0
 
				 SET IDENTITY_INSERT DBO.EVENT OFF
		 
				
	
			   END
			  
			 
-- ================================================================================================	  
	PRINT 'Step 1 - Complete Inserted into Event table'
-- ================================================================================================
   IF NOT EXISTS
   (SELECT 'X' FROM dbo.NGVN_EVENT  WITH (NOLOCK) WHERE EVENT_ID = @NewEventID)
	 
 
  BEGIN  
   
 ALTER TABLE [dbo].[NGVN_EVENT] NOCHECK CONSTRAINT ALL 
 
 INSERT INTO [dbo].[NGVN_EVENT]
		([EVENT_ID],
      [EVENT_STUS_ID]
      ,[NGVN_PROD_TYPE_ID]
      ,[FTN]
      ,[CHARS_ID]
      ,[H1]
      ,[H6]
      ,[CUST_NME]
      ,[CUST_CNTCT_NME]
      ,[CUST_CNTCT_PHN_NBR]
      ,[CUST_EMAIL_ADR]
      ,[CUST_CNTCT_CELL_PHN_NBR]
      ,[CUST_CNTCT_PGR_NBR]
      ,[CUST_CNTCT_PGR_PIN_NBR]
      ,[REQOR_USER_ID]
      ,[SALS_USER_ID]
      ,[PUB_EMAIL_CC_TXT]
      ,[CMPLTD_EMAIL_CC_TXT]
      ,[DSGN_CMNT_TXT]
      ,[GSR_CFGRN_DES]
      ,[CABL_ADD_DUE_DT]
      ,[CABL_PRCOL_TYPE_NME]
      ,[SBC_PAIR_DES]
      ,[ESCL_CD]
      ,[PRIM_REQ_DT]
      ,[SCNDY_REQ_DT]
      ,[ESCL_REAS_ID]
      ,[STRT_TMST]
      ,[EXTRA_DRTN_TME_AMT]
      ,[END_TMST]
      ,[WRKFLW_STUS_ID]
      ,[REC_STUS_ID]
      ,[CREAT_BY_USER_ID]
      ,[MODFD_BY_USER_ID]
      ,[MODFD_DT]
      ,[CREAT_DT]
      ,[EVENT_TITLE_TXT]
      ,[IP_VER_ID]
      ,[CNFRC_BRDG_NBR]
      ,[CNFRC_PIN_NBR]
      ,[EVENT_DRTN_IN_MIN_QTY]
      ,[SOWS_EVENT_ID])
 SELECT  DISTINCT  
				 @NewEventID,
      			 @EventStatusID,
      			CASE WHEN ISNULL(npt.NGVN_PROD_TYPE_ID, '') = '' THEN 1 ELSE npt.NGVN_PROD_TYPE_ID END,
			 	CASE WHEN ISNULL (ngvt.[FTN],'') = '' THEN -1 ELSE SUBSTRING (ngvt.[FTN],1,9) END,
				ngvt.[CHARSID],
				 SUBSTRING (ngvt.[H1], 1,9),
				 SUBSTRING (ngvt.[H6], 1,9),
				CASE WHEN @AuthStatus = 1 THEN 'Unknown' ELSE ngvt.[Customer Name] END,
				CASE WHEN @AuthStatus = 1 THEN 'Unknown' ELSE  ngvt.[Customer Contact Name] END,
				CASE WHEN @AuthStatus = 1 THEN '123456' ELSE  ngvt.[Customer Contact Phone] END,
				CASE WHEN @AuthStatus = 1 THEN 'Private@Sprint.com' ELSE  ngvt.[Customer Email] END,
				9999999999 AS [Customer Contact Cell Phone], -- This is not in NPLV Form
				CASE WHEN @AuthStatus = 1 THEN '123456' ELSE  ngvt.[Customer Contact Pager] END,
				ngvt.[Customer Contact Pager PIN],
				CASE WHEN ISNULL (cnv.GetUserIDByName (ngvt.[Request Contact Name]), '') = '' THEN 1 ELSE
							 cnv.GetUserIDByName (ngvt.[Request Contact Name]) END,
				CASE WHEN  ISNULL(cnv.GetUserIDByName (ngvt.[Sales Engineer (SE) Name]), '') = '' THEN 1 ELSE
							cnv.GetUserIDByName (ngvt.[Sales Engineer (SE) Name]) END,
				ngvt.[Published E-Mail CC:],
				ngvt.[Completed E-mail CC:],
				CASE WHEN ISNULL(ngvt.[Description and Comments], '') = '' THEN 'Converted Order' ELSE 
								 SUBSTRING (ngvt.[Description and Comments],1,2000) END,
			   CASE WHEN ISNULL(ngvt.[GSR Configuration], '') = '' THEN 'Converted Order' ELSE 
								 ngvt.[GSR Configuration] END,
			  
				CASE WHEN ISNULL(ngvt.[Cable Add Due Date],'') = ''THEN GETDATE() ELSE ngvt.[Cable Add Due Date] END,
			 	CASE WHEN ISNULL(ngvt.[Cable Protocol Type],'') = ''THEN 'ConvOrdr' ELSE ngvt.[Cable Protocol Type] END,
				CASE WHEN ISNULL(ngvt.[SBC Pair],'') = ''THEN 'Converted Order' ELSE ngvt.[SBC Pair] END,
			   	ngvt.[Is this an Escalation?],
			 	ngvt.[Primary Request Date],
				ngvt.[Secondary Request Date],
				cnv.GetEscalationReasonID (ngvt.[Escalation Reason]),
				ngvt.[Start Time],
				ngvt.[Extra Duration],
				ngvt.[End Time],
				cnv.GetWorkFlowStatusID (ngvt.[Workflow Status]),
				1, -- Record Status from  LK_REC_STUS
			 	CASE WHEN ISNULL(cnv.GetUserIDByName (ngvt.[Created BY]), '') = '' THEN 1 ELSE 
								cnv.GetUserIDByName (ngvt.[Created BY]) END,
				CASE WHEN ISNULL (cnv.GetUserIDByName (ngvt.[Modified By]), '') ='' THEN 1 ELSE 
								 cnv.GetUserIDByName (ngvt.[Modified By]) END,
				ngvt.[Modified],
				ngvt.[Created],
				ngvt.[Item Title],
				'0' AS IPVER,
				ngvt.[Bridge Number],
				ngvt.[Bridge Pin],
				ngvt.[Event Duration],
				evt.EventID
				
				From cnv.NGVNEvent ngvt With (NOLOCK) INNER JOIN
				cnv.EVENT_Tmp  evt WITH (NOLOCK) ON ngvt.Event_ID = evt.EventId LEFT JOIN
				dbo.LK_NGVN_PROD_TYPE npt WITH (NOLOCK) ON ngvt.[Product Type] = npt.NGVN_PROD_TYPE_DES
			    WHERE ngvt.Event_ID = @EventID
				
	 ALTER TABLE [dbo].[NGVN_EVENT] NOCHECK CONSTRAINT ALL 
	 END			
-- ================================================================================================	
	PRINT 'Step 2 - Complete   Inserted NGVN Event table'
-- ================================================================================================	
 
		
	IF EXISTS 
(SELECT 'X' FROM cnv.NGVNCircuit  WITH (NOLOCK) WHERE TableID = @TableID AND ISNULL(CircuitID_Nua,'') <> '')
	
		BEGIN	
		   ALTER TABLE dbo.NGVN_EVENT_CKT_ID_NUA  NOCHECK CONSTRAINT ALL 	
		   
				INSERT INTO dbo.NGVN_EVENT_CKT_ID_NUA
							(
							   [EVENT_ID]
							  ,[CKT_ID_NUA]
							  ,[CREAT_DT]
							)
						SELECT DISTINCT
						  	@NewEventID,
							SUBSTRING(nc.CircuitID_Nua,1,10),
					       CASE WHEN ISNULL(nc.Created,'') ='' THEN GETDATE() ELSE nc.Created END
				 
						FROM	cnv.NGVNCircuit nc WITH (NOLOCK)
						WHERE TableID = @TableID AND 
						NOT EXISTS 
						(SELECT 'X' FROM dbo.NGVN_EVENT_CKT_ID_NUA Nec WITH (NOLOCK)
							WHERE Nec.EVENT_ID = @NewEventID AND nec.CKT_ID_NUA = SUBSTRING(nc.CircuitID_Nua,1,10))
		   
			ALTER TABLE dbo.NGVN_EVENT_CKT_ID_NUA  CHECK CONSTRAINT ALL 
		END
-- ================================================================================================	
   PRINT 'Step 3 - Complete   Inserted NGVN Circuit NUA table'
-- ================================================================================================	
IF EXISTS
(SELECT 'X' FROM cnv.NGVNSIPTrk  WHERE TableID = @TableID AND ISNULL(Trunk,'') <> '')
		BEGIN
			ALTER TABLE dbo.NGVN_EVENT_SIP_TRNK  NOCHECK CONSTRAINT ALL 
			
 				INSERT INTO dbo.NGVN_EVENT_SIP_TRNK 
							(   [EVENT_ID]
							  ,[SIP_TRNK_NME]
							  ,[CREAT_DT]
							)
						  SELECT DISTINCT
						  	@NewEventID,
							CASE WHEN LEN (np.Trunk) > 10 THEN LEFT(np.Trunk, 10) ELSE 
							(Trunk)  END,
							CASE WHEN ISNULL(np.Created,'') ='' THEN GETDATE() ELSE np.Created END
					FROM	cnv.NGVNSIPTrk np  WHERE TableID = @TableID AND
					NOT EXISTS
					(SELECT 'X' FROM dbo.NGVN_EVENT_SIP_TRNK nsp WITH (NOLOCK) WHERE 
					nsp.EVENT_ID = @NewEventID AND nsp.SIP_TRNK_NME = CASE WHEN LEN (np.Trunk) > 10 THEN LEFT(np.Trunk, 10) ELSE 
							(Trunk)  END)
					
		 ALTER TABLE dbo.NGVN_EVENT_SIP_TRNK  CHECK CONSTRAINT ALL 
	 END
-- ================================================================================================	
 PRINT 'Step 4 - Complete   Inserted NGVN SIP TRNK table'
-- ================================================================================================	
  	 DECLARE @AssignID nVarchar(1000)
	 DECLARE @AuID Int
	 
	  
	SELECT @AssignID  =  [Assigned To]  FROM cnv.NGVNEvent WHERE Event_ID = @EventID
	IF ( ISNULL (@AssignID,'') <> '') 
	 
	BEGIN 	 
  
	
	  INSERT INTO 	dbo.EVENT_ASN_TO_USER		   
						([EVENT_ID]
					  ,[ASN_TO_USER_ID]
					  ,[CREAT_DT]
					  ,[REC_STUS_ID]) 
					  
				SELECT 
						@NewEventID,
						lu.[USER_ID],					 
						CASE WHEN ISNULL(ne.Created,'') ='' THEN GETDATE() ELSE ne.Created END,
						1
					 
					 FROM  dbo.LK_USER   lu WITH (NOLOCK)  
					  INNER JOIN cnv.parseStringwithIds (@AssignID) pst ON 
									 SUBSTRING(lu.DSPL_NME, 1, CHARINDEX('[',lu.DSPL_NME))
								  =  SUBSTRING(pst.DesValue, 1, CHARINDEX('[',pst.DesValue)) 
					INNER JOIN cnv.NGVNEvent  ne WITH (NOLOCK) ON ne.Event_ID = @EventID
					 AND NOT EXISTS
						(SELECT 'X'  FROM dbo.EVENT_ASN_TO_USER  ea WITH (NOLOCK)
						INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON ea.ASN_TO_USER_ID = lu.[USER_ID]
						WHERE 
						ea.EVENT_ID = @NewEventID AND ea.ASN_TO_USER_ID = CASE WHEN ISNULL (lu.[USER_ID],'') = '' THEN 1 ELSE lu.[USER_ID] END )
					
 
	 	END		
 

-- ================================================================================================	 	  
 PRINT 'Step 5 - Complete   Assigned User table'
-- ================================================================================================	  
  
		IF EXISTS 
		(SELECT 'X' FROM cnv.WFHistory  WHERE [Event ID] = @EventID)
		
	    BEGIN
									
			  										
			  INSERT INTO dbo.EVENT_HIST
							 
							 ([EVENT_ID]
							 ,[ACTN_ID]
							 ,[CMNT_TXT]
							 ,[CREAT_BY_USER_ID]
							 ,[MODFD_BY_USER_ID]
							 ,[MODFD_DT]
							 ,[CREAT_DT]
							 ,[PRE_CFG_CMPLT_CD]
							 ,[FAIL_REAS_ID])
							 
		      SELECT  
							 @NewEventID,
							 CASE WHEN ISNULL (lka.ACTN_ID, '') ='' THEN 1 ELSE lka.ACTN_ID END,
							 wfh.[Comment],
							 CASE WHEN ISNULL (cnv.GetUserIDByName (wfh.[Created BY]), '') = '' THEN 1 ELSE
								 cnv.GetUserIDByName (wfh.[Created BY]) END,
							CASE WHEN  ISNULL(cnv.GetUserIDByName (wfh.[Modified By]), '') = '' THEN 1 ELSE
							 cnv.GetUserIDByName (wfh.[Modified By]) END,
						 	 wfh.[Modified],
							 wfh.[Created], 
							 'N' AS [Pre Config Complete ID], --- This needs clarif
							  NULL AS [FAIL_REAS_ID]	 --- This needs clarif
						  
							 FROM cnv.WFHistory  wfh 
							 --LEFT JOIN	cnv.Event_Tmp evt  WITH (NOLOCK) on wfh.[EVENT ID] = evt.EVENTID
							 LEFT JOIN	dbo.LK_ACTN lka  WITH (NOLOCK) ON wfh.[Action] = lka.ACTN_DES
							--INNER JOIN	dbo.EVENT_FAIL_ACTY efa WITH (NOLOCK) on wfh.[Fail Codes] = efa.FAIL_ACTY_ID
							 WHERE wfh.[Event ID] = @EventID
-- ================================================================================================	 	
    PRINT 'Step 6 - Complete   Inserted WF History table'
-- ================================================================================================	 	
   
				   INSERT INTO dbo.EVENT_FAIL_ACTY
							  (
							  [EVENT_HIST_ID]
							 ,[FAIL_ACTY_ID]
							 ,[REC_STUS_ID]
							 ,[CREAT_DT]
							  )		
							
					  SELECT  
							  ehi.EVENT_HIST_ID,
							  lfa.FAIL_ACTY_ID,
							  1 AS [REC STUS ID],
							  ehi.CREAT_DT	
							  FROM    
							  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
							  cnv.Event_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
							  cnv.WFHistory wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
							  LK_FAIL_ACTY lfa WITH (NOLOCK) ON  lfa.FAIL_ACTY_DES = wfh.[Failed Activities]
							  WHERE ehi.EVENT_ID = @EventID
					  
-- ================================================================================================	 		  
	PRINT 'Step 7 - Complete   Inserted FAIL Act table'
-- ================================================================================================	 	
			INSERT INTO dbo.EVENT_SUCSS_ACTY
					([EVENT_HIST_ID]
					,[SUCSS_ACTY_ID]
					,[REC_STUS_ID]
					,[CREAT_DT]
					)
		   SELECT  
					  ehi.EVENT_HIST_ID,
					  lsa.SUCSS_ACTY_ID,
					  1 AS [REC STUS ID],
					  ehi.CREAT_DT	
					  FROM    
					  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
					  cnv.Event_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
					  cnv.WFHistory  wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
					  dbo.LK_SUCSS_ACTY  lsa  WITH (NOLOCK) ON  lsa.SUCSS_ACTY_DES= wfh.[Succssful Activities]
					  WHERE ehi.EVENT_ID = @EventID
-- ================================================================================================	 		  
	PRINT 'Step 8 - Complete   Inserted SUCCESS Act table'
-- ================================================================================================	 	
   	  END
 
   	  
   	  IF EXISTS 
   	  (SELECT 'X' FROM cnv.EventCalender WITH (NOLOCK)  WHERE EventID = @EventID)
   	  
	BEGIN
	
 

 	DECLARE @Id nVarchar(10)
	DECLARE @IdsoutPut nVarchar(max)
	DECLARE @IdsString nVarchar(max)
	DECLARE @outPut nVarchar(max)
	DECLARE @AssignedName nVarchar(max)
	DECLARE @STag nVarchar(100)
	DECLARE @eTag nVarchar(100)
	DECLARE @e1Tag nVarchar(100)
	DECLARE @mTag nVarchar(100)
	DECLARE @SubmitType Varchar(100)
	
	DECLARE @Counter INT
	DECLARE @Counts INT 
	
	    SET @Id = ''
	SET @IdsoutPut = ''
	SET @IdsString = ''
	SET @outPut = ''
	SET @AssignedName = ''
	SET @STag = ''
	SET @eTag = ''
	SET @e1Tag = ''
	SET @mTag = ''
	SET @SubmitType = ''
	
	SET @Counter = 0
	SET @Counts  = 0
	
	
	IF OBJECT_ID(N'tempdb..#TmpAssignNGVN', N'U') IS NOT NULL
				DROP TABLE #TmpAssignNGVN
				CREATE TABLE #TmpAssignNGVN 
					( AsnUser Varchar (1000),
					  xTag Varchar(200),
					  Status INT DEFAULT 0
					)

	
	 SET @STag =  '<ResourceIds> '
	 SET @eTag =  '   </ResourceIds> '
	 SET @e1Tag =  '" /> '
	 SET @mTag =  '<ResourceId Type="System.Int32" Value="'
	
		INSERT INTO #TmpAssignNGVN (AsnUser)
		SELECT DesValue FROM cnv.parseStringwithIds ( (SELECT TOP 1 [Assigned Activators] from cnv.EventCalender Where EventID = @EventID))
 
  IF ((SELECT TOP 1 ASNUser from #TmpAssignNGVN )<> 'SOWS ES Activators')
 
    BEGIN
       SET @Counts = 0
       SET @Counter = 1
 
        Select  @Counts =  COUNT(1) from #TmpAssignNGVN WHERE STATUS = 0
        
               
        IF (@Counts  > 0)
        BEGIN
        
		 WHILE	@Counter <= @Counts
         
			BEGIN     
                    SET @Id = ''
					SET @IdsoutPut = ''
					SET @IdsString = ''
					SET @outPut = ''
					SET @AssignedName = ''
	
				SELECT TOP 1 @AssignedName =   AsnUser from #TmpAssignNGVN WHERE Status = 0
				SELECT @Id =  [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE 
				SUBSTRING(DSPL_NME, 1, CHARINDEX('[',DSPL_NME))
				 = SUBSTRING(@AssignedName, 1, CHARINDEX('[',@AssignedName))
				SET @ID = CASE WHEN ISNULL(@ID, 0) = 0 THEN 1 ELSE @ID END
			
			 SELECT @IdsoutPut = @mTag  + '' + @Id + '' + @e1Tag
                  
           update #TmpAssignNGVN set Status = 1, xTag = @IdsoutPut where AsnUser = @AssignedName
      
          SET @Counter= @Counter + 1
         END
        
        END        
  
   
		SELECT @IdsString = COALESCE(@IdsString + '  ', '') + xTag
		FROM #TmpAssignNGVN		
		SELECT @outPut =   @STag  + ''+ @IdsString + ''+ @eTag
		
		END
        ELSE 
        
        BEGIN
        
        SELECT @Id = [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE DSPL_NME = 'COWS ES Activators'
             
		SET @outPut =   @STag  + ''+ @mTag  + '' + @Id + '' + @e1Tag + ''+ @eTag
		
        END    
		SELECT @SubmitType = [Submit Type] FROM cnv.NGVNEvent WHERE Event_ID = @EventID
		SET @SubmitType = CASE WHEN @SubmitType = 'NGVN STANDARD' THEN 'NGVN'  ELSE @SubmitType END
		 	
		 	INSERT INTO dbo.APPT 
							
						  ([EVENT_ID]
						  ,[STRT_TMST]
						  ,[END_TMST]
						  ,[RCURNC_DES_TXT]
						  ,[SUBJ_TXT]
						  ,[DES]
						  ,[APPT_LOC_TXT]
						  ,[APPT_TYPE_ID]
						  ,[CREAT_BY_USER_ID]
						  ,[MODFD_BY_USER_ID]
						  ,[MODFD_DT]
						  ,[CREAT_DT]
						  ,[ASN_TO_USER_ID_LIST_TXT]
						  ,[RCURNC_CD])  
 
		  		  
				SELECT 
							@NewEventID,
							et.[Start Time],
							et.[End Time],
							'',
							REPLACE (et.[Item Title],@EventID,@NewEventID),
							et.Description,
							'',
							7,
							 --CASE WHEN ISNULL ([cnv].[GetAPPTTypeID](@SubmitType), '') = '' THEN 11 ELSE
			    	--		 [cnv].[GetAPPTTypeID] (@SubmitType) END,
							CASE WHEN ISNULL(cnv.GetUserIDByName (et.[Created By]),0 ) = 0 THEN 1  ELSE
							cnv.GetUserIDByName (et.[Created By])END,
							CASE WHEN ISNULL (cnv.GetUserIDByName (et.[Modified By]),0 ) = 0 THEN 1 
							ELSE cnv.GetUserIDByName (et.[Modified By]) END,
							et.Modified,
							et.Created,						
							@outPut,
							''
	 
							FROM cnv.EventCalender  Et WITH (NOLOCK)
							INNER JOIN cnv.NGVNEvent ng WITH (NOLOCK) ON ng.Event_ID = Et.EventID
				 		 WHERE Et.EventID = @EventID
 
  -- ================================================================================================	 		  
	PRINT 'Step 9 - Complete   Event Calender'
-- ================================================================================================	 
				
	END			
 
				       	UPDATE cnv.EVENT_Tmp SET status = 2 WHERE NewEventID = @NewEventID
						UPDATE cnv.NGVNEvent SET Status = 1 WHERE Event_ID = @EventID
			 	
			 	       	
							  SET @Ctr = @Ctr + 1 
	---------------------------------------------------------------------------------------
	  
	PRINT 'Insert ALl Completed  - Counter Incremented '
	--------------------------------------------------------------------------------------
			 
	 END
	 END
		ELSE
		PRINT ' NO Events to Insert'
	
   END TRY 
  Begin Catch
	Exec [dbo].InsertErrorInfo
	  
End Catch

  SET NOCOUNT OFF
  END