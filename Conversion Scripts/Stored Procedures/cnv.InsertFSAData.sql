USE [COWS]
GO
_CreateObject 'SP','cnv','InsertIntoADEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 
-- =======================================================
-- Author:	Naidu Vattigunta	
-- Create date:	10/12/2011 
-- Description:	Inserts FSA Data
 -- ================================================================================================
ALTER PROCEDURE [cnv].[InsertFSAData]
AS
 
 
BEGIN
SET NOCOUNT ON 

DECLARE @FTN dbo.FTN
DECLARE @CHARSID Varchar(20)
DECLARE @CUSTID INT
DECLARE @H1 dbo.H1
DECLARE @H5 dbo.H5
DECLARE @H6 dbo.H6
DECLARE @CUSTIDH1 INT
DECLARE @CUSTIDH4 INT
DECLARE @CUSTIDH6 INT
DECLARE @CUSTIDH5 INT
DECLARE @CUSTIDH5_H6 INT
DECLARE @CUST_NMEH1 Varchar(100)
DECLARE @CUST_NMEH4 Varchar(100)
DECLARE @CUST_NMEH5 Varchar(100)
DECLARE @CUST_NMEH5_H6 Varchar(100)
DECLARE @FRST_NMEH1 Varchar(100)
DECLARE @FRST_NMEH4 Varchar(100)
DECLARE @FRST_NMEH5 Varchar(100)
DECLARE @FRST_NMEH6 Varchar(100)
DECLARE @H5_H6_CONTACT_NAME Varchar(100)
DECLARE @H5_H6_CONTACT_EMAIL Varchar(100) 
DECLARE @PHN_NBR  dbo.PHONE
DECLARE @PHN_NBRH1 dbo.PHONE
DECLARE @PHN_NBRH4  dbo.PHONE
DECLARE @PHN_NBRH5  dbo.PHONE
DECLARE @H5_H6_CONTACT_PHONE  dbo.PHONE
DECLARE @H5_H6_CUSTOMER_PHONE  dbo.PHONE
DECLARE @INSTL_ESCL_CD Varchar(2)
DECLARE @COUNTRY Varchar(10)
DECLARE @MNSFLAG Bit
DECLARE @SPEED_OF_SERVICE Varchar(50)
DECLARE @ESCALATED_FLAG  Varchar(2)
DECLARE @CARRIER_WHOLESALE_TYPE Varchar(50)
DECLARE @GOVT_INDICATOR  Varchar(2)
DECLARE @DESIGN_DOCUMENT_LOCATION Varchar(100)
DECLARE @H5_H6_STATE Varchar(50)
DECLARE @H5_H6_CITY  Varchar(50)
DECLARE @H5_H6_ADDRESS Varchar(100)
DECLARE @H5_H6_COUNTRY Varchar(50)
DECLARE @H5_H6_ZIP_CODE Varchar(50)
DECLARE @DESIGN_DOC_NUMBER Varchar(100)
DECLARE @PL_NUMBER Varchar(100)
DECLARE @INSTALL_SITE_CONTACT_NME Varchar(100)
DECLARE @ON_SITE_PHONE dbo.PHONE
DECLARE @NETWORKENGINEERNAME Varchar(100)
DECLARE @NETWORKENGINEERPHONE dbo.PHONE
DECLARE @SRVC_ASRN_CONT_NAME Varchar(100)
DECLARE @SRVC_ASRN_CONT_EMAIL Varchar(100)
DECLARE @SRVC_ASRN_CONT_PHONE dbo.PHONE
DECLARE @SERVICE_TIERS Varchar(20)
DECLARE @DOMESTC_INTL_FLAG INT
DECLARE @IP_VERSION Varchar(10)
DECLARE @MaxID INT
DECLARE @ORDRID INT
DECLARE @ORDR_DATE SmallDateTime
DECLARE @ORDR_CAT_ID TinyInt
DECLARE @H5_H6_PRNT_ACNO INT 
DECLARE @PROD_TYPE Varchar(50)
DECLARE @PROD_TYPE_ID INT
DECLARE @ORDR_ACTN TinyInt
DECLARE @ORDR_TYPE Varchar(50)
DECLARE @ORDR_TYPE_ID INT
DECLARE @SOI Varchar(50)
DECLARE @H5_H6_CNTCT_FIRST_NME Varchar(100)
DECLARE @H5_H6_CNTCT_LAST_NME Varchar(100)
DECLARE @ORDR_TYPE_CD Varchar(2)
DECLARE @PROD_TYPE_CD Varchar(2)
DECLARE @Cnt			INT
DECLARE @Ctr			INT


BEGIN TRY
  
 

SET @ORDR_CAT_ID = 2 
SET @ORDR_ACTN  = 2 
SET @ORDR_TYPE_ID = 0
SET @PROD_TYPE_ID = 0



-- ================================================================================================

		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;


SELECT @MaxID = MAX(ORDR_ID) FROM ORDR
SET @MaxID = @MaxID + 1

DBCC CHECKIDENT ( [cnv.ORDR_Tmp], RESEED, @maxID)

 
  INSERT INTO cnv.ORDR_Tmp
 ( FTN
 )
 SELECT  
 FTN 
 FROM cnv.SOWS_FSA_ORDER_INFO OI WITH (NOLOCK)     
  
 WHERE  NOT EXISTS 
		(SELECT 'X' FROM  cnv.ORDR_Tmp  ot WITH (NOLOCK) WHERE 
		ot.FTN = OI.FTN AND ot.Status in (0,1) )  
-- ================================================================================================	 

 -- ================================================================================================	 
		SET @Cnt = 0
		SET @Ctr = 1
		SELECT 	@Cnt	=	Count(1) FROM  cnv.ORDR_Tmp Where Status = 0  
		
		Print 		@Cnt
		
		
 
	 
		IF @Cnt	>	0
		BEGIN
			WHILE	@Ctr <= @Cnt
				BEGIN
				
 
						 SELECT  TOP 1   @ORDRID = ORDR_ID,
										 @FTN	 = FTN
								 FROM cnv.ORDR_Tmp   WITH (NOLOCK)  WHERE Status = 0
				 
	
			  
			  PRINT @ORDRID
			  PRINT @FTN
			  PRINT 'Naidu'
 
-- ================================================================================================	
 
 

			  
 

 
 	SELECT DISTINCT
			@FTN					 =		CF.FTN,
			@CUSTIDH1				 =		CF.H1_CUST_ID,
			@CUST_NMEH1				 =		ISNULL(CF.H1_CUST_NAME,''),
			@H5_H6_CONTACT_NAME	     =	 	ISNULL(CF.H5_H6_CONTACT_FIRST_NAME,'') + ' ' + ISNULL(CF.H5_H6_CONTACT_LAST_NAME,''),
			@H5_H6_CNTCT_FIRST_NME	 =		ISNULL(CF.H5_H6_CONTACT_FIRST_NAME,''), 
			@H5_H6_CNTCT_LAST_NME	 =		ISNULL(CF.H5_H6_CONTACT_LAST_NAME,''),
			@H5_H6_CONTACT_EMAIL	 =		ISNULL(CF.H5_H6_CONTACT_EMAIL,''),
			@H5_H6_CONTACT_PHONE	 =		ISNULL(CF.H5_H6_CONTACT_PHONE,'') ,
			@CUSTIDH5_H6			 =		ISNULL(CF.H5_H6_CUST_ID,'') ,
 			--@MNSFLAG				 =		ISNULL(IP.MNS_FLAG,'N') ,
			@SPEED_OF_SERVICE		 =		ISNULL(IP.SPEED_OF_SERVICE,'') ,
			@ESCALATED_FLAG			 =		ISNULL(OI.ESCALATED_FLAG,'N'), 
			@CARRIER_WHOLESALE_TYPE	 =		ISNULL(OI.CARRIER_WHOLESALE_TYPE,'') , 
			@GOVT_INDICATOR			 =		ISNULL(OI.GOVT_INDICATOR,'N'),
			@DESIGN_DOCUMENT_LOCATION=		ISNULL(VASI.DESIGN_DOCUMENT_LOCATION,''),
			@CHARSID				 =		ISNULL(OI.CHARS_ID,''),
			@CUST_NMEH5_H6			 =		ISNULL(CF.H5_H6_Customer_Name,''),
 		 	@H5_H6_STATE			 =		ISNULL(CF.H5_H6_STATE,'') ,
			@H5_H6_CITY				 =		ISNULL(CF.H5_H6_CITY,'') ,
		 	@H5_H6_CUSTOMER_PHONE	 =		ISNULL(CF.H5_H6_CUSTOMER_PHONE,'') ,
			@H5_H6_ADDRESS			 =		ISNULL(CF.H5_H6_ADDRESS,'') ,
	 		@H5_H6_COUNTRY			 =		ISNULL(CF.H5_H6_COUNTRY,''),
			@H5_H6_ZIP_CODE			 =		ISNULL(CF.H5_H6_ZIP_CODE,'') ,
			@DESIGN_DOC_NUMBER		 =		ISNULL(OI.DESIGN_DOC_NUMBER,'') ,
			@PL_NUMBER				 =		ISNULL(OI.PL_NUMBER,'') ,
			@INSTALL_SITE_CONTACT_NME=		ISNULL(CF.ON_SITE_CONTACT_LAST_NAME,'') + ' ' + ISNULL(CF.ON_SITE_CONTACT_FIRST_NAME,''),
			@ON_SITE_PHONE			 =		ISNULL(CF.ON_SITE_PHONE,'') ,
			@NETWORKENGINEERNAME	 =		ISNULL(S_INFO.NETWORKENGINEERLASTNAME,'') + ' ' + ISNULL(S_INFO.NETWORKENGINEERFIRSTNAME,''),
			@NETWORKENGINEERPHONE	 =		ISNULL(S_INFO.NETWORKENGINEERPHONE,''),
			@SRVC_ASRN_CONT_NAME	 =		ISNULL(S_INFO.SERVICEASSURANCECONTACTLASTNAME,'') + ' ' + ISNULL(S_INFO.SERVICEASSURANCECONTACTFIRSTNAME,'') ,
			@SRVC_ASRN_CONT_EMAIL	 =		ISNULL(S_INFO.SERVICEASSURANCECONTACTEMAIL,'') ,
			@SRVC_ASRN_CONT_PHONE	 =		ISNULL(S_INFO.SERVICEASSURANCECONTACTPHONE,''),
			@SERVICE_TIERS			 =		ISNULL(MI.SERVICE_TIERS,'') ,
			@IP_VERSION				 =		ISNULL(IP.IP_VERSION,''),
			@ORDR_DATE				 =		ISNULL(OI.Time_Stamp, '') ,
			@DOMESTC_INTL_FLAG		 =		ISNULL(OI.Domestic_International_Flag, ''), 
			@H5_H6_PRNT_ACNO		 =		ISNULL(CF.H5_H6_Parent_Acct_Number, ''),
			@PROD_TYPE				 =		ISNULL(OI.Service_Sub_Type, 'Managed Network Services'),
			@ORDR_TYPE				 =		ISNULL(OI.Order_Type,''),
			@SOI					 =		ISNULL(OI.SOI, '')
				
							FROM	 cnv.SOWS_FSA_CUSTOMER_INFO			CF 
				LEFT OUTER JOIN		 cnv.SOWS_FSA_SERVICE_INFO			SI		ON SI.ID = CF.ID
				LEFT OUTER JOIN		 cnv.SOWS_FSA_SERVICE_INFO			IP		ON IP.FTN = CF.FTN
				LEFT OUTER JOIN		 cnv.SOWS_FSA_ORDER_INFO			OI		ON OI.FTN = CF.FTN
				LEFT OUTER JOIN		 cnv.SOWS_FSA_SUPPORT_INFO			S_INFO	ON S_INFO.FTN = CF.FTN
				LEFT OUTER JOIN		 cnv.SOWS_FSA_MNS_INFO 				MI		ON MI.FTN = CF.FTN
				LEFT OUTER JOIN		 cnv.SOWS_Value_Added_Services_Info VASI	ON VASI.ID = CF.ID
				WHERE CF.FTN = @FTN
	
	
			
		 
	 SET @PROD_TYPE = '%' +@PROD_TYPE + '%' 
	 SELECT TOP 1 @PROD_TYPE_ID = PROD_TYPE_ID FROM LK_PROD_TYPE WHERE PROD_TYPE_DES LIKE @PROD_TYPE
	 SELECT TOP 1 @PROD_TYPE_CD = FSA_PROD_TYPE_CD FROM LK_PROD_TYPE WHERE PROD_TYPE_DES LIKE @PROD_TYPE
	 
	 SELECT @ORDR_TYPE_ID = ORDR_TYPE_ID FROM LK_ORDR_TYPE  WHERE ORDR_TYPE_DES = @ORDR_TYPE
	  SELECT @ORDR_TYPE_CD = FSA_ORDR_TYPE_CD FROM LK_ORDR_TYPE  WHERE ORDR_TYPE_DES = @ORDR_TYPE
	 
 
 
	 
	 PRINT @PROD_TYPE_ID
	 
 
	 
	 SET IDENTITY_INSERT dbo.ORDR ON
	 
 INSERT INTO dbo.ORDR
  (    ORDR_ID
	 ,[ORDR_CAT_ID]
      ,[CREAT_DT]
      ,[CREAT_BY_USER_ID]
      ,[REC_STUS_ID]
      ,[MODFD_DT]
      ,[MODFD_BY_USER_ID]
      ,[H5_FOLDR_ID]
      ,[PPRT_ID]
      ,[DMSTC_CD]
      ,[H5_H6_CUST_ID]
      ,[PRNT_ORDR_ID]
      ,[SCURD_CD]
      ,[RGN_ID]
      ,[ORDR_STUS_ID]
      ,[CHARS_ID]
 
     )
VALUES(
		@ORDRID,
		@ORDR_CAT_ID,
		@ORDR_DATE,
		1,
		1,
		GETDATE(),
		1,
		123,
		1,
		@DOMESTC_INTL_FLAG,
		@CUSTIDH5_H6,
		@ORDRID,
		0,
		1,
		2,
		@CHARSID)
		
 SET IDENTITY_INSERT dbo.ORDR OFF
		 
  INSERT INTO dbo.FSA_ORDR

			(  [ORDR_ID]
			  ,[ORDR_ACTN_ID]
			  ,[FTN]
			  ,[ORDR_TYPE_CD]
			  ,[PROD_TYPE_CD]
			  ,[PRNT_FTN]
			  ,[CUST_ORDR_SBMT_DT]
			  ,[GVRMNT_TYPE_ID]
			  ,[INSTL_ESCL_CD]
			  ,[INSTL_DSGN_DOC_NBR]
			  ,[CHARS_ID]
			)
    VALUES
				(
				@ORDRID,
				@ORDR_ACTN,
				@FTN,
				@ORDR_TYPE_CD,
				ISNULL(@PROD_TYPE_CD,'MN'),
				@H5_H6_PRNT_ACNO,
				@ORDR_DATE,
				@GOVT_INDICATOR,
				@ESCALATED_FLAG,
				@DESIGN_DOC_NUMBER,
				@CHARSID
				)
	
	IF (@CUSTIDH1 <> '')
	
			BEGIN
				
				INSERT INTO dbo.FSA_ORDR_CUST
						  ([ORDR_ID]
						  ,[CIS_LVL_TYPE]
						  ,[CUST_ID]
						  ,[CURR_BILL_CYC_CD]
						  ,[SOI_CD]
						  ,[CREAT_DT]
						  ,[CUST_NME])
				  VALUES
						  (
							@ORDRID,
							'H1',
							@CUSTIDH1,
							'AA',
							@SOI,
							@ORDR_DATE,
							 dbo.encryptString (@CUST_NMEH1)			  
						  )
				END
				
									IF (@H5_H6_COUNTRY <> 'USA')
								
										BEGIN
											
											INSERT INTO dbo.FSA_ORDR_CUST
													  ([ORDR_ID]
													  ,[CIS_LVL_TYPE]
													  ,[CUST_ID]
													  ,[CURR_BILL_CYC_CD]
													  ,[SOI_CD]
													  ,[CREAT_DT]
													  ,[CUST_NME])
											  VALUES
													  (
														@ORDRID,
														'H5',
														@CUSTIDH5_H6,
														'AA',
														@SOI,
														@ORDR_DATE,
														 dbo.encryptString (@CUST_NMEH5_H6)
													  )
											END
											
												ELSE
												
																BEGIN 
										 
																		
																		INSERT INTO dbo.FSA_ORDR_CUST
																				  ([ORDR_ID]
																				  ,[CIS_LVL_TYPE]
																				  ,[CUST_ID]
																				  ,[CURR_BILL_CYC_CD]
																				  ,[SOI_CD]
																				  ,[CREAT_DT]
																				  ,[CUST_NME])
																		  VALUES
																				  (
																					@ORDRID,
																					'H6',
																					@CUSTIDH5_H6,
																					'AA',
																					@SOI,
																					@ORDR_DATE,
																					 dbo.encryptString (@CUST_NMEH5_H6)	  		  
																				  )
																		END
 
	 
		
 	IF (@H5_H6_COUNTRY <> 'USA')	
 	
 	BEGIN				
						
		INSERT INTO dbo.ORDR_CNTCT 
		(		   
			   [ORDR_ID]
			  ,[CNTCT_TYPE_ID]
			  ,[PHN_NBR]
  			  ,[CREAT_DT]
			  ,[CREAT_BY_USER_ID]
			  ,[REC_STUS_ID]	 
			  ,[CIS_LVL_TYPE]	 
			  ,[FRST_NME]
			  ,[LST_NME]
			  ,[EMAIL_ADR] 	 
		)
		
		VALUES
			(
			@ORDRID,
			1,
			@H5_H6_CONTACT_PHONE,
			@ORDR_DATE,
			1,
			1,
			'H5',
			 dbo.encryptString (@H5_H6_CNTCT_FIRST_NME),
			 dbo.encryptString (@H5_H6_CNTCT_LAST_NME),
			  dbo.encryptString  (@H5_H6_CONTACT_EMAIL)
			)
	  END
		
		ELSE 
		
		BEGIN 
		
					INSERT INTO dbo.ORDR_CNTCT 
			(		   
				   [ORDR_ID]
				  ,[CNTCT_TYPE_ID]
				  ,[PHN_NBR]
  				  ,[CREAT_DT]
				  ,[CREAT_BY_USER_ID]
				  ,[REC_STUS_ID]	 
				  ,[CIS_LVL_TYPE]	 
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[EMAIL_ADR] 	 
			)
			
			VALUES
				(
				@ORDRID,
				1,
				@H5_H6_CONTACT_PHONE,
				@ORDR_DATE,
				1,
				1,
				'H6',
			  dbo.encryptString (@H5_H6_CNTCT_FIRST_NME  ),
			  dbo.encryptString (@H5_H6_CNTCT_LAST_NME ),
			  dbo.encryptString (@H5_H6_CONTACT_EMAIL )
				)
		  END
		  
 
		  
			IF (@H5_H6_COUNTRY <> 'USA')	
		
		BEGIN 	
		 INSERT INTO dbo.ORDR_ADR
			( 
			   [ORDR_ID]
			  ,[ADR_TYPE_ID]
			  ,[CREAT_DT]
			  ,[CREAT_BY_USER_ID]
			  ,[REC_STUS_ID]
			  ,[CIS_LVL_TYPE]
			   ,[STREET_ADR_1]
			  ,[CTY_NME]
			  ,[PRVN_NME]
	    	  ,[ZIP_PSTL_CD]
		 
	 	   )
		   
		   VALUES
		   (
		   @ORDRID,
		   3,
		   @ORDR_DATE,
		   1,
		   1,
		   'H5',
		    dbo.encryptString (@H5_H6_ADDRESS  ),
		    dbo.encryptString (@H5_H6_CITY ),
		    dbo.encryptString (@H5_H6_COUNTRY ),
		    dbo.encryptString (@H5_H6_ZIP_CODE )
		  
			)
			
			END
			
			ELSE
			
					BEGIN 	
		 INSERT INTO dbo.ORDR_ADR
			( 
			   [ORDR_ID]
			  ,[ADR_TYPE_ID]
			  ,[CREAT_DT]
			  ,[CREAT_BY_USER_ID]
			  ,[REC_STUS_ID]
			  ,[CIS_LVL_TYPE]
			   ,[STREET_ADR_1]
			  ,[CTY_NME]
			  ,[PRVN_NME]
	    	  ,[ZIP_PSTL_CD]
		 
	 	   )
		   
		   VALUES
		   (
		   @ORDRID,
		   3,
		   @ORDR_DATE,
		   1,
		   1,
		   'H6',
		    dbo.encryptString (@H5_H6_ADDRESS ),
		    dbo.encryptString (@H5_H6_CITY ),
		    dbo.encryptString (@H5_H6_COUNTRY ),
		    dbo.encryptString (@H5_H6_ZIP_CODE )
		  
			)
			
			END
		  
		UPDATE cnv.ORDR_Tmp SET Status = 1 WHERE FTN = @FTN  
		
    SET @Ctr = @Ctr + 1
		
		END  
		
		END 
		
 	END TRY 
 BEGIN CATCH 
	EXEC [dbo].InsertErrorInfo	  
 END CATCH

  SET NOCOUNT OFF
  END			
 		
 
 		
 		