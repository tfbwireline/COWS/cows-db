USE [COWS]
GO
_CreateObject 'SP','cnv','InsertSprintLinkEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

-- =======================================================
-- Author:	Naidu Vattigunta	
-- Create date:	06/30/2011 
-- Description:	Inserts Data into Access Delivery Table
 
-- ================================================================================================
ALTER PROCEDURE [cnv].[InsertSprintLinkEvent]
AS
 
 
 
BEGIN
SET NOCOUNT ON
 
DECLARE @EventID INT
DECLARE @EventStatusID INT
DECLARE @NewEventID INT
DECLARE @MaxID INT 
DECLARE @Cnt			INT
DECLARE @Ctr			INT
DECLARE @TableID  nvarchar(100)
DECLARE @AuthStaus BIT

 Begin Try  
  -- =============================================================================================
/* 
Event Types forcnv.EVENT_Tmp table
1	AD
2	NGVN
3	Scheduled Implementation (MPLS)
4	Sprint Link
5	MDS
 */
-- ================================================================================================
--DELETE FROM cnv.EVENT_Tmp where status = 0 and eventtype = 5


UPDATE cnv.SprintLink SET Status = 4 WHERE ISNUMERIC(FTN) = 0 and ISNULL(FTN,'') <> ''
SELECT @MaxID = MAX(Event_ID) from EVENT
SET @MaxID = @MaxID + 1

DBCC CHECKIDENT ( [cnv.Event_Tmp], RESEED, @maxID)
    
 INSERT INTO cnv.EVENT_Tmp
 (EventID,
 EventStatus,
 EventType,
 CreateDate)
 SELECT  
 sl.Event_ID,
 [cnv].[GetEventStatusID] (sl.[Event Status]),
 4,
  sl.[Created]
 FROM cnv.SprintLink sl WITH (NOLOCK)
 WHERE Status = 0 AND ISNULL(Event_ID, '') <> ''  
 AND  NOT EXISTS 
		(SELECT 'X' FROM  cnv.EVENT_Tmp et WITH (NOLOCK) WHERE 
		sl.Event_ID = et.EventID AND Status in(0,2,5) AND EventType = 4) Order by Event_ID asc
 
 -- ================================================================================================	
		SET @Cnt = 0
		SET @Ctr = 1
		SELECT 	@Cnt	=	Count(1) FROM cnv.EVENT_Tmp Where Status = 0 and EventType = 4
		
		IF @Cnt	>	0
		BEGIN
			WHILE	@Ctr <= @Cnt
				BEGIN
				
 
						 SELECT  TOP 1  @EventID  = ev.EventID,
										@NewEventID = ev.NewEventID,
										@TableID  = st.AccessTableID,
										@AuthStaus = CASE WHEN st.Auth_Status = 'AUTHORISED' AND ISNULL(st.FTN,0 ) <> 0  THEN 1 ELSE 0 END,
										@EventStatusID = ev.EventStatus
								 FROM cnv.EVENT_Tmp ev WITH (NOLOCK)  
				 INNER JOIN cnv.SprintLink st WITH (NOLOCK) ON ev.EventID = st.Event_ID
               WHERE ev.EventType = 4 and ev.Status = 0 Order by Event_ID asc
            
   
       
				
-- ================================================================================================	
-- ================================================================================================	
 

    
   	 IF NOT EXISTS
		(SELECT 'X' FROM dbo.EVENT WITH (NOLOCK) WHERE EVENT_ID  = @NewEventID)
	BEGIN
 
			SET IDENTITY_INSERT DBO.EVENT ON
 
				 INSERT INTO dbo.EVENT
						 ( EVENT_ID,
						  EVENT_TYPE_ID,
						  CREAT_DT)
				  SELECT 
						  @NewEventID,
						  Eventstatus,
						  CreateDate
				  FROM cnv.EVENT_Tmp WHERE EventID = @EventID
			     AND EventType = 4 AND [Status] = 0

  
		SET IDENTITY_INSERT DBO.EVENT OFF
	
	
  END 
  
-- ================================================================================================	  
	PRINT 'Step 1 - Complete Inserted into Event table'
-- ================================================================================================
   
 
     IF NOT EXISTS
		(SELECT 'X' FROM  dbo.SPLK_EVENT  WITH (NOLOCK) WHERE EVENT_ID = @EventID )
  
  BEGIN
   
   
	 ALTER TABLE dbo.SPLK_EVENT NOCHECK CONSTRAINT ALL 
 
	 INSERT INTO dbo.SPLK_EVENT
					(
					   [EVENT_ID]
					  ,[EVENT_STUS_ID]
					  ,[FTN]
					  ,[CHARS_ID]
					  ,[H1]
					  ,[H6]
					  ,[CUST_NME]
					  ,[CUST_CNTCT_NME]
					  ,[CUST_CNTCT_PHN_NBR]
					  ,[CUST_EMAIL_ADR]
					  ,[CUST_CNTCT_CELL_PHN_NBR]
					  ,[CUST_CNTCT_PGR_NBR]
					  ,[CUST_CNTCT_PGR_PIN_NBR]
					  ,[REQOR_USER_ID]
					  ,[SALS_USER_ID]
					  ,[PUB_EMAIL_CC_TXT]
					  ,[CMPLTD_EMAIL_CC_TXT]
					  ,[DSGN_CMNT_TXT]
					  ,[SPLK_EVENT_TYPE_ID]
					  ,[SPLK_ACTY_TYPE_ID]
					  ,[IP_VER_ID]
					  ,[MDS_MNGD_CD]
					  ,[ESCL_CD]
					  ,[PRIM_REQ_DT]
					  ,[SCNDY_REQ_DT]
					  ,[ESCL_REAS_ID]
					  ,[STRT_TMST]
					  ,[EXTRA_DRTN_TME_AMT]
					  ,[END_TMST]
					  ,[WRKFLW_STUS_ID]
					  ,[REC_STUS_ID]
					  ,[CREAT_BY_USER_ID]
					  ,[MODFD_BY_USER_ID]
					  ,[MODFD_DT]
					  ,[CREAT_DT]
					  ,[EVENT_TITLE_TXT]
					  ,[CNFRC_BRDG_NBR]
					  ,[CNFRC_PIN_NBR]
					  ,[EVENT_DRTN_IN_MIN_QTY]
					  ,[SOWS_EVENT_ID]
				 
					 )
	SELECT TOP 1		
						@NewEventID,
						@EventStatusID,
						CASE WHEN ISNULL(spl.[FTN], '') = '' THEN -1 ELSE SUBSTRING (spl.FTN,1,10) END AS FTN,
						spl.[CHARSID], 
						SUBSTRING( spl.[H1], 1,9),
						SUBSTRING( spl.[H6],1,9),
						 CASE WHEN @AuthStaus = 1 THEN 'Unknown' ELSE spl.[Customer Name] END,
						 CASE WHEN @AuthStaus = 1 THEN 'Unknown' ELSE spl.[Customer Contact Name] END,
						 CASE WHEN @AuthStaus = 1 THEN '123456' ELSE spl.[Customer Contact Phone] END,
						 CASE WHEN @AuthStaus = 1 THEN 'Private@Sprint.com' ELSE spl.[Customer Email] END,
						 CASE WHEN @AuthStaus = 1 THEN '123456' ELSE SUBSTRING (spl.[Customer Contact Cell Phone],1,10)END,
						 CASE WHEN @AuthStaus = 1 THEN '123456' ELSE spl.[Customer Contact Pager] END,
						spl.[Customer Contact Pager PIN],
						CASE WHEN ISNULL (cnv.GetUserIDByName (spl.[Request Contact Name]), '') = '' THEN 1 ELSE
							 cnv.GetUserIDByName (spl.[Request Contact Name]) END,
						CASE WHEN  ISNULL(cnv.GetUserIDByName (spl.[Sales Engineer (SE) Name]), '') = '' THEN 1 ELSE
							cnv.GetUserIDByName (spl.[Sales Engineer (SE) Name]) END,
						spl.[Published E-Mail CC:],
						spl.[Completed E-mail CC:],
						SUBSTRING (spl.[Description and Comments],1,1000),
						1 AS [SprintLink Event Type], -- This needs clarification 
					    sat.SPLK_ACTY_TYPE_ID,
						 lip.IP_VER_ID,	
						spl.[MDS Managed],			
						spl.[Is Escalation],
						spl.[Primary Request Date],
						spl.[Secondary Request Date],
						cnv.GetEscalationReasonID (spl.[Escalation Reason]),
						spl.[Start Time],	
						spl.[Extra Duration],
						spl.[End Time],
						 cnv.GetWorkFlowStatusID (spl.[Workflow Status]),
						1, -- Record Status from  LK_REC_STUS
						CASE WHEN ISNULL(cnv.GetUserIDByName (spl.[Created BY]), '') = '' THEN 1 ELSE 
								cnv.GetUserIDByName (spl.[Created BY]) END,
						CASE WHEN ISNULL (cnv.GetUserIDByName (spl.[Modified By]), '') ='' THEN 1 ELSE 
								 cnv.GetUserIDByName (spl.[Modified By]) END,
						spl.[Modified],
						spl.[Created],
						spl.[Item Title], 			
						spl.[Bridge Number],
						spl.[Bridge Pin],
						spl.[Event Duration],
						evt.EventID
					
					 FROM cnv.SprintLink   spl WITH (NOLOCK)
		INNER JOIN	cnv.EVENT_Tmp  evt WITH (NOLOCK) ON spl.Event_ID = evt.EventId 
		INNER JOIN	LK_SPLK_ACTY_TYPE sat WITH (NOLOCK) ON spl.[SprintLink Activity Type] = sat.SPLK_ACTY_TYPE_DES
	 LEFT JOIN dbo.LK_IP_VER lip WITH (NOLOCK) ON 
							 CASE WHEN spl.[IP Version] = 'Dual Stack' THEN 'Dual Stack - SprintLink'
								  WHEN spl.[IP Version] = 'IPv6' THEN 'IPv6 - SprintLink'
								  WHEN spl.[IP Version] = 'IPv4' THEN 'IPv4 - SprintLink'
								  
								  ELSE  spl.[IP Version] END = lip.IP_VER_NME
			         WHERE spl.Event_ID = @EventID
		
		 ALTER TABLE dbo.SPLK_EVENT CHECK CONSTRAINT ALL 
	END	
-- ================================================================================================	
	PRINT 'Step 2 - Complete   Inserted Sprint Link Event table'
-- ================================================================================================	
	
		IF EXISTS 
	(SELECT 'X' FROM cnv.SprintLInkAccess  WITH (NOLOCK) WHERE TableID = @TableID)
	
	BEGIN
	
			ALTER TABLE  dbo.SPLK_EVENT_ACCS  NOCHECK CONSTRAINT ALL 	
			
			INSERT INTO	   dbo.SPLK_EVENT_ACCS 
							([EVENT_ID],
							[OLD_PL_NBR],
							[OLD_PORT_SPEED_DES],
							[NEW_PL_NBR],
							[NEW_PORT_SPEED_DES],
							[CREAT_DT],
							[OLD_VPI_VCI_DLCI_NME],
							[NEW_VPI_VCI_DLCI_NME])
			SELECT DISTINCT
						   @NewEventID,
						   slk.[Old Private Line],
						   slk.[Old Port Speed],
						   slk.[New Private Line],
						   slk.[New Port Speed],
						   GETDATE(), --- This needs to be changed from View
						   slk.[Old VPI VCI],
						   slk.[New VPI VCI]
							 
						FROM cnv.SprintLInkAccess  slk WITH (NOLOCK)  WHERE TableID = @TableID
		 
			ALTER TABLE  dbo.SPLK_EVENT_ACCS  CHECK CONSTRAINT ALL 	
		END
		
-- ================================================================================================	
	 PRINT 'Step 3 - Complete   Inserted Sprint Link Access table'
-- ================================================================================================	
 	 DECLARE @AssignID Varchar(100)
	 DECLARE @AuID Int
	 
	SELECT @AssignID  =  [Assigned To]  FROM cnv.SprintLink WHERE Event_ID = @EventID
	IF ( ISNULL (@AssignID,'') <> '') 
	
	BEGIN 	 
  
	
		INSERT INTO 	dbo.EVENT_ASN_TO_USER		   
						([EVENT_ID]
					  ,[ASN_TO_USER_ID]
					  ,[CREAT_DT]
					  ,[REC_STUS_ID]) 
					  
				SELECT 
						@NewEventID,
						lu.[USER_ID],					 
						CASE WHEN ISNULL(ne.Created,'') ='' THEN GETDATE() ELSE ne.Created END,
						1
					 
					 FROM  dbo.LK_USER   lu WITH (NOLOCK)  
					  INNER JOIN cnv.parseStringwithIds (@AssignID) pst ON 
									 SUBSTRING(lu.DSPL_NME, 1, CHARINDEX('[',lu.DSPL_NME))
								  =  SUBSTRING(pst.DesValue, 1, CHARINDEX('[',pst.DesValue)) 
					INNER JOIN cnv.SprintLink  ne WITH (NOLOCK) ON ne.Event_ID = @EventID
					 AND NOT EXISTS
						(SELECT 'X'  FROM dbo.EVENT_ASN_TO_USER  ea WITH (NOLOCK)
						INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON ea.ASN_TO_USER_ID = lu.[USER_ID]
						WHERE 
						ea.EVENT_ID = @NewEventID AND ea.ASN_TO_USER_ID = CASE WHEN ISNULL (lu.[USER_ID],'') = '' THEN 1 ELSE lu.[USER_ID] END )
					
 
	 	END		
 

 

-- ================================================================================================	 	  
 PRINT 'Step 3 -(2) - Complete   Assigned User table'
-- ================================================================================================	 
		IF EXISTS 
		(SELECT 'X' FROM cnv.WFHistory  WHERE [Event ID] = @EventID )
		
	    BEGIN
							
			  										
			  INSERT INTO dbo.EVENT_HIST
							 
							 ([EVENT_ID]
							 ,[ACTN_ID]
							 ,[CMNT_TXT]
							 ,[CREAT_BY_USER_ID]
							 ,[MODFD_BY_USER_ID]
							 ,[MODFD_DT]
							 ,[CREAT_DT]
							 ,[PRE_CFG_CMPLT_CD]
							 ,[FAIL_REAS_ID])
							 
		      SELECT  
							 @NewEventID,
							 CASE WHEN ISNULL (lka.ACTN_ID, '') ='' THEN 1 ELSE lka.ACTN_ID END,
							 wfh.[Comment],
							 CASE WHEN ISNULL (cnv.GetUserIDByName (wfh.[Created BY]), '') = '' THEN 1 ELSE
											   cnv.GetUserIDByName (wfh.[Created BY]) END,
							 CASE WHEN ISNULL (cnv.GetUserIDByName (wfh.[Modified By]), '') = '' THEN 1 ELSE
											   cnv.GetUserIDByName (wfh.[Modified By]) END,
							 wfh.[Modified],
							 wfh.[Created], 
							 'N' AS [Pre Config Complete ID], --- This needs clarif
							  NULL AS [FAIL_REAS_ID]	 --- This needs clarif
						  
							 FROM cnv.WFHistory wfh 							 
				 LEFT JOIN	dbo.LK_ACTN lka  WITH (NOLOCK) ON wfh.[Action] = lka.ACTN_DES
							--INNER JOIN	dbo.EVENT_FAIL_ACTY efa WITH (NOLOCK) on wfh.[Fail Codes] = efa.FAIL_ACTY_ID
							 WHERE wfh.[Event ID] = @EventID
-- ================================================================================================		  
    PRINT 'Step 4 - Complete   Inserted WF History table'
-- ================================================================================================	

				   INSERT INTO dbo.EVENT_FAIL_ACTY
							  (
							  [EVENT_HIST_ID]
							 ,[FAIL_ACTY_ID]
							 ,[REC_STUS_ID]
							 ,[CREAT_DT]
							  )		
							
					  SELECT  DISTINCT
							  ehi.EVENT_HIST_ID,
							  lfa.FAIL_ACTY_ID,
							  1 AS [REC STUS ID],
							  ehi.CREAT_DT	
							  FROM    
							  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
							  cnv.EVENT_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
							  cnv.WFHistory wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
							  LK_FAIL_ACTY lfa WITH (NOLOCK) ON  lfa.FAIL_ACTY_DES = wfh.[Failed Activities]
							  WHERE ehi.EVENT_ID = @EventID
			  
-- ================================================================================================	  
	PRINT 'Step 5 - Complete   Inserted FAIL Act table'
-- ================================================================================================	
					
				INSERT INTO dbo.EVENT_SUCSS_ACTY
							([EVENT_HIST_ID]
							,[SUCSS_ACTY_ID]
							,[REC_STUS_ID]
							,[CREAT_DT]
							)
				   SELECT  DISTINCT
							  ehi.EVENT_HIST_ID,
							  lsa.SUCSS_ACTY_ID,
							  1 AS [REC STUS ID],
							  ehi.CREAT_DT	
							  FROM    
							  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
							  cnv.EVENT_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
							  cnv.WFHistory wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
							  dbo.LK_SUCSS_ACTY  lsa  WITH (NOLOCK) ON  lsa.SUCSS_ACTY_DES= wfh.[Succssful Activities]
								WHERE ehi.EVENT_ID = @EventID
-- ================================================================================================		  
	PRINT 'Step 6 - Complete   Inserted SUCCESS Act table'
-- ================================================================================================	
 
	 END
	  
   	  
   	  IF EXISTS 
   	  (SELECT 'X' FROM cnv.EventCalender WITH (NOLOCK)  WHERE EventID = @EventID)
   	  
	BEGIN
	
 

 	DECLARE @Id nVarchar(10)
	DECLARE @IdsoutPut nVarchar(max)
	DECLARE @IdsString nVarchar(max)
	DECLARE @outPut nVarchar(max)
	DECLARE @AssignedName nVarchar(max)
	DECLARE @STag nVarchar(100)
	DECLARE @eTag nVarchar(100)
	DECLARE @e1Tag nVarchar(100)
	DECLARE @mTag nVarchar(100)
	DECLARE @SubmitType Varchar(100)
	
	DECLARE @Counter INT
	DECLARE @Counts INT 
	
    SET @Id = ''
	SET @IdsoutPut = ''
	SET @IdsString = ''
	SET @outPut = ''
	SET @AssignedName = ''
	SET @STag = ''
	SET @eTag = ''
	SET @e1Tag = ''
	SET @mTag = ''
	SET @SubmitType = ''
	
	SET @Counter = 0
	SET @Counts  = 0
	
	
	IF OBJECT_ID(N'tempdb..#TmpAssignSPLK', N'U') IS NOT NULL
				DROP TABLE #TmpAssignSPLK
				CREATE TABLE #TmpAssignSPLK 
					( AsnUser Varchar (1000),
					  xTag Varchar(200),
					  Status INT DEFAULT 0
					)

	
	 SET @STag =  '<ResourceIds> '
	 SET @eTag =  '   </ResourceIds> '
	 SET @e1Tag =  '" /> '
	 SET @mTag =  '<ResourceId Type="System.Int32" Value="'
	
		INSERT INTO #TmpAssignSPLK (AsnUser)
		SELECT DesValue FROM cnv.parseStringwithIds ( (SELECT TOP 1 [Assigned Activators] from cnv.EventCalender Where EventID = @EventID))
 
 IF ((SELECT TOP 1 ASNUser from #TmpAssignSPLK )<> 'SOWS ES Activators')
 
    BEGIN
       SET @Counts = 0
       SET @Counter = 1
 
        Select  @Counts =  COUNT(1) from #TmpAssignSPLK WHERE STATUS = 0
        
               
        IF (@Counts  > 0)
        BEGIN
        
		 WHILE	@Counter <= @Counts
        
			BEGIN     
			
			    SET @Id = ''
				SET @IdsoutPut = ''
				SET @IdsString = ''
				SET @outPut = ''
				SET @AssignedName = ''
              
				SELECT TOP 1 @AssignedName =   AsnUser from #TmpAssignSPLK WHERE Status = 0
				SELECT @Id =  [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE 
				SUBSTRING(DSPL_NME, 1, CHARINDEX('[',DSPL_NME))
				 = SUBSTRING(@AssignedName, 1, CHARINDEX('[',@AssignedName))
				SET @ID = CASE WHEN ISNULL(@ID, 0) = 0 THEN 1 ELSE @ID END
			
			 SELECT @IdsoutPut = @mTag  + '' + @Id + '' + @e1Tag
                  
           update #TmpAssignSPLK set Status = 1, xTag = @IdsoutPut where AsnUser = @AssignedName
      
          SET @Counter= @Counter + 1
         END
        
        END        
  
   
		SELECT @IdsString = COALESCE(@IdsString + '  ', '') + xTag
		FROM #TmpAssignSPLK		
		SELECT @outPut =   @STag  + ''+ @IdsString + ''+ @eTag
		
		END
        ELSE 
        
        BEGIN
        
        SELECT @Id = [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE DSPL_NME = 'COWS ES Activators'
             
		SET @outPut =   @STag  + ''+ @mTag  + '' + @Id + '' + @e1Tag + ''+ @eTag
		
        END     
		SELECT @SubmitType = [Submit Type] FROM cnv.SprintLink WHERE Event_ID = @EventID
 
		 	
		 	INSERT INTO dbo.APPT 
							
						  ([EVENT_ID]
						  ,[STRT_TMST]
						  ,[END_TMST]
						  ,[RCURNC_DES_TXT]
						  ,[SUBJ_TXT]
						  ,[DES]
						  ,[APPT_LOC_TXT]
						  ,[APPT_TYPE_ID]
						  ,[CREAT_BY_USER_ID]
						  ,[MODFD_BY_USER_ID]
						  ,[MODFD_DT]
						  ,[CREAT_DT]
						  ,[ASN_TO_USER_ID_LIST_TXT]
						  ,[RCURNC_CD])  
 
		  		  
				SELECT 
							@NewEventID,
							et.[Start Time],
							et.[End Time],
							'',
							REPLACE (et.[Item Title],@EventID,@NewEventID),
							et.Description,
							'',
							8,
							 --CASE WHEN ISNULL ([cnv].[GetAPPTTypeID](@SubmitType), '') = '' THEN 11 ELSE
			    	--		 [cnv].[GetAPPTTypeID] (@SubmitType) END,
							CASE WHEN ISNULL (cnv.GetUserIDByName (et.[Created By]), '') = '' THEN 1 ELSE
											  cnv.GetUserIDByName (et.[Created By]) END,
							CASE WHEN ISNULL (cnv.GetUserIDByName (et.[Modified By]),'') = '' THEN 1 ELSE
											cnv.GetUserIDByName (et.[Modified By]) END,
							et.Modified,
							et.Created,						
							@outPut,
							''
	 
							FROM cnv.EventCalender  Et WITH (NOLOCK)
							INNER JOIN cnv.SprintLink at  WITH (NOLOCK) ON at.Event_ID = Et.EventID
				 		   WHERE et.EventID = @EventID
 	  
   	  
  -- ================================================================================================	 		  
	PRINT 'Step 7 - Complete   Event Calender'
-- ================================================================================================	 
	 END
	 
						
						UPDATE cnv.SprintLink   SET Status = 1 WHERE Event_ID = @EventID
						--Update dbo.AccessDelivery_Tmp SET Status = 1 WHERE Event_ID = @CEvenID
				       	UPDATE cnv.EVENT_Tmp SET status = 2  Where EventID = @EventID
			   SET @Ctr = @Ctr + 1 
-- ================================================================================================		  
	PRINT 'Step 8 - Completed and Counter Incremented'
-- ================================================================================================	
  
 		  END 
    	END
END TRY 
 BEGIN CATCH 
	EXEC [dbo].InsertErrorInfo	  
 END CATCH

  SET NOCOUNT OFF
  END