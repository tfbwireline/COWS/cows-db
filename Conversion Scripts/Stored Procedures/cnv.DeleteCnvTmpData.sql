USE [COWS]
GO
_CreateObject 'SP','cnv','DeleteCnvTmpData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 
-- =======================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	10/18/2011 
-- Description:	Delete Data before Load
 -- ================================================================================================


 
ALTER  PROCEDURE [cnv].[DeleteCnvTmpData]
AS
BEGIN

DECLARE @Cnt INT

BEGIN TRY



Delete  from  cnv.EVENT_Tmp
Delete  from  cnv.EventCalender
Delete  from  cnv.ADAccessTable
Delete  from  cnv.MDSAccessTag
Delete  from  cnv.MDSActivityMAC
Delete  from  cnv.MDSCircuit
Delete  from  cnv.MDSEvent
Delete  from  cnv.MDSFastckt
Delete  from  cnv.MDSFastDiscOE
Delete  from  cnv.MDSFastDisconnectOE
Delete  from  cnv.MDSFastMngdDev
Delete  from  cnv.MDSFastOCT
Delete  from  cnv.MDSFastOE
Delete  from  cnv.MDSFastTrack
Delete  from  cnv.MDSFastVirtual
Delete  from  cnv.MDSFastWiredTrspt
Delete  from  cnv.MDSFastWirelessTrpt
Delete  from  cnv.MDSLoc
Delete  from  cnv.MDSManagedDevices
Delete  from  cnv.MDSOE
Delete  from  cnv.MDSVirtual
Delete  from  cnv.MDSWiredTrans
Delete  from  cnv.MDSWirelessTrans
Delete  from  cnv.MPLSAccessTag
Delete  from  cnv.MPLSCustomer
Delete  from  cnv.MPLSEvent
Delete  from  cnv.MPLSEventInbound
Delete  from  cnv.MPLSFirewall
Delete  from  cnv.MPLSOutbound
Delete  from  cnv.NGVNCircuit
Delete  from  cnv.NGVNEvent
Delete  from  cnv.NGVNSIPTrk
 Delete  from  cnv.ORDR_Tmp
Delete  from  cnv.SprintLink
Delete  from  cnv.SprintLInkAccess
SET @Cnt = 0


 
 
	 
	 update cnv.MPLSAccessTag set [VASSOL] = '' where [VASSOL]  = ',,'
	 
	 update cnv.MPLSAccessTag set [VASSOL] = '' where [VASSOL]  = '.'
	  
	 update cnv.MPLSAccessTag set [TransportNumber] = '' where [TransportNumber]  = '.'
	 
	 update cnv.MPLSAccessTag set [TransportNumber] = '' where [TransportNumber]  = ',,'
	  
 
	 Delete from cnv.NGVNSIPTrk where ISNULL(Trunk,'') = '' 
	 
	 
	 SELECT @Cnt =  COUNT(1) FROM LK_MPLS_EVENT_TYPE WHERE MPLS_EVENT_TYPE_DES = 'IPv6/Dual Stack Implementation'
	 	 	 
	 IF (@Cnt = 0)
	 
	 BEGIN
	 
	 	 
	 Insert into LK_MPLS_EVENT_TYPE (MPLS_EVENT_TYPE_ID, MPLS_EVENT_TYPE_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)
	 VALUES (4, 'IPv6/Dual Stack Implementation',0,1,1,getdate(),getdate())
	 	 
	END

 
 SELECT @Cnt =  COUNT(1) FROM LK_MPLS_EVENT_TYPE WHERE MPLS_EVENT_TYPE_DES = 'IPv6/Dual Stack Pre-configuration'
 	 IF (@Cnt = 0)
	 
	 BEGIN
	 
 
	 	 	 Insert into LK_MPLS_EVENT_TYPE (MPLS_EVENT_TYPE_ID, MPLS_EVENT_TYPE_DES,REC_STUS_ID,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)
	 VALUES (5, 'IPv6/Dual Stack Pre-configuration',0,1,1,getdate(),getdate())
	 
	END
	
	
	
	

Select * from LK_DEV_MANF 
Select * from LK_DEV_MODEL

DECLARE @ManfID INT
DECLARE @DevID INT
 
Select @ManfID = MAX(MANF_ID) FROM LK_DEV_MANF
SET @ManfID = @ManfID + 1
SELECT @DevID = MAX(DEV_MODEL_ID) FROM LK_DEV_MODEL

SET @DevID = @DevID + 1 

IF NOT EXISTS
( SELECT 'X' FROM  LK_DEV_MANF  WITH (NOLOCK) WHERE MANF_NME = 'Converted Order')
BEGIN

  INSERT INTO LK_DEV_MANF 
       (MANF_ID
       ,[MANF_NME]
      ,[REC_STUS_ID]
      ,[CREAT_BY_USER_ID]
      ,[CREAT_DT])
    VALUES
    (@ManfID,
    'ConvertedOrder',
      1,
      1,
      GETDATE()
    )

END

 

IF NOT EXISTS
( SELECT 'X' FROM LK_DEV_MODEL WITH (NOLOCK) WHERE DEV_MODEL_NME = 'Converted Order')
BEGIN
		INSERT INTO LK_DEV_MODEL
		(DEV_MODEL_ID
		,[DEV_MODEL_NME]
      ,[MIN_DRTN_TME_REQR_AMT]
      ,[REC_STUS_ID]
      ,[CREAT_BY_USER_ID]
      ,[MODFD_BY_USER_ID]
      ,[MODFD_DT]
      ,[CREAT_DT]
      ,[MANF_ID])
      
      VALUES
      (@DevID,
      'Converted Order',
		120,
		1,
		1,
		1,
		GETDATE(),
		GETDATE(),
		@ManfID) 

END

update LK_USER set DSPL_NME = 'COWS ES Activators' WHERE USER_ID = 5001

Select  DSPL_NME, * from LK_USER where CHARINDEX('[',DSPL_NME) =  0

	


 
  
 
END TRY
 BEGIN CATCH 
	EXEC [dbo].InsertErrorInfo	  
 END CATCH

  SET NOCOUNT OFF
  END