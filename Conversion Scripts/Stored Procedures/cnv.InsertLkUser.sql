USE [COWS]
GO
_CreateObject 'SP','cnv','InsertLkUser'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =======================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	07/25/2011 
-- Description:	Inserts Data into LKUser Table Table and child tables
 -- ================================================================================================

ALTER PROCEDURE [cnv].[InsertLkUser]
AS
BEGIN
SET NOCOUNT ON 
 
 
 DECLARE @UserID Varchar(25)
 DECLARE @AD Varchar(2)
 DECLARE @MaxID Int
 DECLARE @Account Varchar(20)
 DECLARE @Name Varchar(100)
 DECLARE @email Varchar(100)
 DECLARE @Cnt			INT
 DECLARE @Ctr			INT
 DECLARE @lkUserID   INT
 
 BEGIN TRY
 
 
 
 
 IF EXISTS
	 (SELECT 'X' FROM cnv.SOWSUsers  WHERE  ( SUBSTRING([Account], 0, 3) <> 'AD') OR LEN([Account]) > 12)
	 
	  UPDATE  cnv.SOWSUsers SET Status = 3 WHERE  (   SUBSTRING([Account], 0, 3) <> 'AD' OR LEN([Account]) > 12)
	  
	  
	  
 IF EXISTS
	 (SELECT 'X' FROM cnv.SOWSReviewvers  WHERE  ( SUBSTRING([Account], 0, 3) <> 'AD') OR LEN([Account]) > 12)
	 
	  UPDATE  cnv.SOWSReviewvers SET Status = 3 WHERE  (   SUBSTRING([Account], 0, 3) <> 'AD' OR LEN([Account]) > 12)
	  
	  
 IF EXISTS
	 (SELECT 'X' FROM cnv.SOWSActivators  WHERE  ( SUBSTRING([Account], 0, 3) <> 'AD') OR LEN([Account]) > 12)
	 
	  UPDATE  cnv.SOWSActivators SET Status = 3 WHERE  (   SUBSTRING([Account], 0, 3) <> 'AD' OR LEN([Account]) > 12)
 
 
 BEGIN
 
 
 DECLARE @TmpUsers TABLE
 (Account nVarchar (100),
 Name	  nVarchar(100),
 eMail	  nVarchar(300),
 Status INT Default 0)
 
 INSERT INTO @TmpUsers
 (Account,Name,eMail)
 
 SELECT	 DISTINCT 
								 [Account],
								 Name,
								 [Work e-mail] 		
						FROM cnv.SOWSUsers  WITH (NOLOCK) WHERE Status = 0 AND  ISNULL([Account], '') <> ''
						
UNION ALL
 SELECT	 DISTINCT 
								 [Account],
								 Name,
								  [Work e-mail] 		
						FROM cnv.SOWSReviewvers  WITH (NOLOCK) WHERE Status = 0 AND  ISNULL([Account], '') <> ''
						
UNION ALL

 SELECT	 DISTINCT 
								  [Account],
								  Name,
								 [Work e-mail] 		
						FROM cnv.SOWSActivators  WITH (NOLOCK) WHERE Status = 0 AND  ISNULL([Account], '') <> ''
		  
 -- ================================================================================================	 
		SET @Cnt = 0
		SET @Ctr = 1
		SELECT 	@Cnt	=	Count(1) FROM @TmpUsers Where Status = 0  
		 
		IF @Cnt	>	0
		BEGIN
			WHILE	@Ctr <= @Cnt
				BEGIN
				  
						SELECT	TOP 1 
								@Account = [Account],
								@Name   = Name,
								@email  = [Work e-mail] 		
						FROM cnv.SOWSUsers  WITH (NOLOCK) WHERE Status = 0 AND  ISNULL([Account], '') <> ''
  
  
  						 SELECT @MaxID = MAX(USER_ID) from dbo.LK_USER
	 					 SET @MaxID = @MaxID + 1
	  
						SET @UserID =  Substring(@Account,charindex('\',@Account)+1,LEN(@Account)-charindex('\',@Account)) 
	
	--PRINT @Account
	--PRINT @Name
	--PRINT @eMail 
	--PRINT @UserID
	
	--RETURN

 -- ================================================================================================	 
	IF NOT EXISTS
	(SELECT 'X' FROM dbo.LK_USER WITH(NOLOCK) WHERE USER_ADID  = @UserID)
  
		BEGIN
  
				 INSERT INTO dbo.LK_USER
						([USER_ID],
						[USER_ADID]
					  ,[USER_ACF2_ID]
					  ,[FULL_NME]
					  ,[PHN_NBR]
					  ,[EMAIL_ADR]
					  ,[REC_STUS_ID]
					  ,[CREAT_BY_USER_ID]
					  ,[MODFD_BY_USER_ID]
					  ,[MODFD_DT]
					  ,[CREAT_DT]
					  ,[CELL_PHN_NBR]
					  ,[PGR_NBR]
					  ,[PGR_PIN_NBR]
					  ,[DSPL_NME]) 
				VALUES(
						@MaxID,
						@UserID,
						@UserID,
						@Name,
						9999999999,
						@email,
						1,
						29,
						29,
						GETDATE(),
						GETDATE(),
						9999999999,
						'',
						'',       
						 @Name )
	 	END
	 	
	 					SET @Ctr = @Ctr + 1 
   -- ================================================================================================	

	 		 

END
  END
  
  END

 
		END TRY 
 BEGIN CATCH 
	EXEC [dbo].InsertErrorInfo	  
 END CATCH

  SET NOCOUNT OFF
  END		
 		