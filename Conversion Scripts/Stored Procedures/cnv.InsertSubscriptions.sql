USE [COWS]
GO
_CreateObject 'SP','cnv','InsertSubscriptions'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =======================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	07/02/2011 
-- Description:	Inserts Data into Conference Bridgr Table tables
 -- ================================================================================================

ALTER PROCEDURE [cnv].[InsertSubscriptions]
AS
 
BEGIN
DECLARE @MaxID Int
DECLARE @Cnt Int
DECLARE @Ctr Int
DECLARE @ConfBrNum	Varchar(100)

SET @Cnt = 0
SET @Ctr = 1

UPDATE cnv.Subscriptions SET Status = 3 WHERE Status = 0 AND ISNULL (Conf#Bridge, '')  = ''

SELECT @Cnt = COUNT(1) FROM cnv.Subscriptions WHERE Status = 0 
 

BEGIN TRY

IF (@Cnt > 1)
  BEGIN
	WHILE @Ctr < @Cnt
		 BEGIN
			SELECT TOP 1 @ConfBrNum = Conf#Bridge FROM cnv.Subscriptions WITH (NOLOCK)
		    WHERE Status = 0 AND ISNULL (Conf#Bridge, '') <> ''
	 
IF NOT EXISTS
(SELECT 'X' FROM LK_CNFRC_BRDG WITH (NOLOCK) WHERE CNFRC_BRDG_NBR = @ConfBrNum)



BEGIN

SELECT @MaxID = MAX([CNFRC_BRDG_ID])  FROM [dbo].[LK_CNFRC_BRDG]
 
SET @MaxID = @MaxID + 1

 		INSERT INTO  [dbo].[LK_CNFRC_BRDG]
			 ([CNFRC_BRDG_ID]
			  ,[CNFRC_BRDG_NBR]
			  ,[CNFRC_PIN_NBR]
			  ,[SOI]
			  ,[REC_STUS_ID]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_BY_USER_ID]
			  ,[MODFD_DT]
			  ,[CREAT_DT]) 
      
         SELECT TOP 1 
				@MaxID ,
				@ConfBrNum,
				[Conf Bridge Pin],
				SOI,
				1,
				CASE WHEN ISNULL( cnv.GetUserIDByName ([Created By]), '') = '' THEN	1 ELSE
								 cnv.GetUserIDByName ([Created By]) END,
				CASE WHEN ISNULL( cnv.GetUserIDByName ([Modified By]), '') = '' THEN	1 ELSE
								 cnv.GetUserIDByName ([Modified By]) END,
				Modified,
				Created
				FROM cnv.Subscriptions WITH (NOLOCK) WHERE Conf#Bridge = @ConfBrNum and Status = 0
				
	 
 -- ================================================================================================
		PRINT ' Inserted Conf Bridge Number' + '  ' + @ConfBrNum
 -- ================================================================================================
		UPDATE cnv.Subscriptions SET Status = 2 WHERE Conf#Bridge = @ConfBrNum
		END
		
		UPDATE cnv.Subscriptions SET Status = 2 WHERE Conf#Bridge = @ConfBrNum
	
		SET @Ctr = @Ctr + 1
END
	
END
ELSE 

PRINT ' NO records to Insert'
  
 
END TRY
 BEGIN CATCH 
	EXEC [dbo].InsertErrorInfo	  
 END CATCH

  SET NOCOUNT OFF
  END