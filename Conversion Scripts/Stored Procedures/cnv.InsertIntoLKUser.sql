USE [COWS]
GO
_CreateObject 'SP','cnv','InsertIntoLKUser'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

-- =======================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	07/25/2011 
-- Description:	Inserts Data into LKUser Table Table and child tables
 -- ================================================================================================

ALTER PROCEDURE [cnv].[InsertIntoLKUser]
AS
BEGIN
SET NOCOUNT ON 
 
 
 DECLARE @UserID Varchar(25)
 DECLARE @AD Varchar(2)
 DECLARE @MaxID Int
 DECLARE @Account Varchar(20)
 DECLARE @Name Varchar(100)
 DECLARE @email Varchar(100)
 DECLARE @Cnt			INT
 DECLARE @Ctr			INT
 DECLARE @lkUserID   INT
 
 BEGIN TRY
 
 
 
 
 IF EXISTS
	 (SELECT 'X' FROM cnv.SOWSUsers  WHERE  ( SUBSTRING([Account], 0, 3) <> 'AD') OR LEN([Account]) > 12)
	 
	  UPDATE  cnv.SOWSUsers SET Status = 3 WHERE  (   SUBSTRING([Account], 0, 3) <> 'AD' OR LEN([Account]) > 12)
 
 
 BEGIN
		  
 -- ================================================================================================	 
		SET @Cnt = 0
		SET @Ctr = 1
		SELECT 	@Cnt	=	Count(1) FROM cnv.SOWSUsers Where Status = 0 AND  ISNULL([Account], '') <> ''
		 
		IF @Cnt	>	0
		BEGIN
			WHILE	@Ctr <= @Cnt
				BEGIN
				  
						SELECT	TOP 1 
								@Account = [Account],
								@Name   = Name,
								@email  = [Work e-mail] 		
						FROM cnv.SOWSUsers  WITH (NOLOCK) WHERE Status = 0 AND  ISNULL([Account], '') <> ''
  
  
  						 SELECT @MaxID = MAX(USER_ID) from dbo.LK_USER
	 					 SET @MaxID = @MaxID + 1
	  
						SET @UserID =  Substring(@Account,charindex('\',@Account)+1,LEN(@Account)-charindex('\',@Account)) 
	
	--PRINT @Account
	--PRINT @Name
	--PRINT @eMail 
	--PRINT @UserID
	
	--RETURN

 -- ================================================================================================	 
	IF NOT EXISTS
	(SELECT 'X' FROM dbo.LK_USER WITH(NOLOCK) WHERE USER_ADID  = @UserID)
  
		BEGIN
  
				 INSERT INTO dbo.LK_USER
						([USER_ID],
						[USER_ADID]
					  ,[USER_ACF2_ID]
					  ,[FULL_NME]
					  ,[PHN_NBR]
					  ,[EMAIL_ADR]
					  ,[REC_STUS_ID]
					  ,[CREAT_BY_USER_ID]
					  ,[MODFD_BY_USER_ID]
					  ,[MODFD_DT]
					  ,[CREAT_DT]
					  ,[CELL_PHN_NBR]
					  ,[PGR_NBR]
					  ,[PGR_PIN_NBR]
					  ,[DSPL_NME]) 
				VALUES(
						@MaxID,
						@UserID,
						@UserID,
						@Name,
						9999999999,
						@email,
						1,
						29,
						29,
						GETDATE(),
						GETDATE(),
						9999999999,
						'',
						'',       
						 @Name )
	 	END
  -- ================================================================================================
    PRINT 'Inserted in LK USer Table'	 
  -- ================================================================================================	 
		 		
 	SELECT @lkUserID = USER_ID FROM dbo.LK_USER WHERE USER_ADID = @UserID
	
		
			IF NOT EXISTS
			(SELECT 'X' FROM dbo.USER_GRP_ROLE  WITH (NOLOCK) WHERE USER_ID = @lkUserID)
		BEGIN
		 
	
			INSERT INTO dbo.USER_GRP_ROLE
						(  [USER_ID]
						  ,[GRP_ID]
						  ,[ROLE_ID]
						  ,[REC_STUS_ID]
						  ,[CREAT_BY_USER_ID]
						  ,[MODFD_BY_USER_ID]
						  ,[CREAT_DT]
						  ,[MODFD_DT] )
				VALUES (
							@lkUserID,
							6,
							21,
							1,
							29,
							29,
							GETDATE(),
							GETDATE() 
						
					   ),
					    (
							@lkUserID,
							10,
							21,
							1,
							29,
							29,
							GETDATE(),
							GETDATE() 
						
					   )
  -- ================================================================================================
    PRINT 'Inserted  ES  Group   '	 
  -- ================================================================================================
	END
	
	IF EXISTS
	
	(SELECT 'X' FROM cnv.SOWSActivators WHERE Status = 0 and Account = @Account)  
	
		BEGIN
		
		IF NOT EXISTS
		(SELECT 'X' FROM dbo.USER_GRP_ROLE  WITH (NOLOCK) WHERE USER_ID = @lkUserID)
		
		BEGIN
 
	
			INSERT INTO dbo.USER_GRP_ROLE
						(  [USER_ID]
						  ,[GRP_ID]
						  ,[ROLE_ID]
						  ,[REC_STUS_ID]
						  ,[CREAT_BY_USER_ID]
						  ,[MODFD_BY_USER_ID]
						  ,[CREAT_DT]
						  ,[MODFD_DT] )
				VALUES (
							@lkUserID,
							9,
							23,
							1,
							29,
							29,
							GETDATE(),
							GETDATE() 
						
					   )
  -- ================================================================================================
    PRINT 'Inserted  Activator Group   '	 
  -- ================================================================================================		
	
		END
		END
	 	
		IF EXISTS
	
		(SELECT 'X' FROM cnv.SOWSReviewvers  WHERE Status = 0 and Account = @Account)
	
		BEGIN
	
		IF NOT EXISTS
		(SELECT 'X' FROM dbo.USER_GRP_ROLE  WITH (NOLOCK) WHERE USER_ID = @lkUserID)
		
		BEGIN
 
	
			INSERT INTO dbo.USER_GRP_ROLE
						(  [USER_ID]
						  ,[GRP_ID]
						  ,[ROLE_ID]
						  ,[REC_STUS_ID]
						  ,[CREAT_BY_USER_ID]
						  ,[MODFD_BY_USER_ID]
						  ,[CREAT_DT]
						  ,[MODFD_DT] )
				VALUES (
							@lkUserID,
							2,
							22,
							1,
							29,
							29,
							GETDATE(),
							GETDATE() 
						
					   ),
					   (
							@lkUserID,
							6,
							22,
							1,
							29,
							29,
							GETDATE(),
							GETDATE() 
						
					   ),
					   (
							@lkUserID,
							10,
							22,
							1,
							29,
							29,
							GETDATE(),
							GETDATE() 
						
					   )
  -- ================================================================================================
    PRINT 'Inserted  Reviewver Group   '	 
  -- ================================================================================================		
	
		END
		
		END
	
	   -- ================================================================================================	 
		 		 
				UPDATE cnv.SOWSUsers  SET Status = 1 WHERE [Account] = @Account
				UPDATE cnv.SOWSActivators SET Status = 1  WHERE [Account] = @Account
				UPDATE cnv.SOWSReviewvers SET Status = 1  WHERE [Account] = @Account
				SET @Ctr = @Ctr + 1 
   -- ================================================================================================	

	 		 

END
  END
  
  END

 
		END TRY 
 BEGIN CATCH 
	EXEC [dbo].InsertErrorInfo	  
 END CATCH

  SET NOCOUNT OFF
  END		
 		
 
 
 		
 		
 		
 		
 		
 		
 
 
 		
 		
 		
 		
 