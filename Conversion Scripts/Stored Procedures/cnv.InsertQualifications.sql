USE [COWS]
GO
_CreateObject 'SP','cnv','InsertQualifications'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =======================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	07/25/2011 
-- Description:	Inserts Qualifications Table Table and child tables
 -- ================================================================================================

ALTER PROCEDURE [cnv].[InsertQualifications]
AS
 
 
BEGIN

SET NOCOUNT ON

DECLARE @AssignUser nVarchar(1000)
DECLARE @AssignUserID Int
DECLARE @DedicatedCustID Int
DECLARE @EHNCId		Int
DECLARE @RECStusID  Int
DECLARE @CreatedID  Int
DECLARE @ModifiedID Int
DECLARE @CreateDt   SmallDateTime
DECLARE @ModifiedDt	SmallDateTIme
DECLARE @EventTypeId  Int
DECLARE @IPVerID    Int
DECLARE @SpcProjID	Int
DECLARE @VendorModelID Int
DECLARE @QualifID	 Int
DECLARE @CustID		 Int
DECLARE	@DevID		 Int
DECLARE @UserID		Int
DECLARE @CreatedBy Varchar(100)
DECLARE @ModifiedBy Varchar(100)
DECLARE @QAssigned	nVarchar(100)
DECLARE @Cnt		 Int
DECLARE @Ctr		 Int
DECLARE @DedCustNme	nVarchar(max)
DECLARE @Devices nVarchar(max)
DECLARE @EnServices nVarchar(max)
DECLARE @EventType nVarchar(max)
DECLARE @IPVer	nVarchar(max)
DECLARE @SpcProj	nVarchar(max)
DECLARE @VendModel  nVarchar(max)
DECLARE @EnId Int


BEGIN TRY

		SET @Cnt = 0
		SET @Ctr = 1
		SELECT 	@Cnt	=	Count(1) FROM  cnv.Qualifications WHERE  Status = 0 and ISNULL([Assigned To], '') <> ''
		 
				 
		IF @Cnt	>	0
		BEGIN
			WHILE	@Ctr <= @Cnt
				BEGIN
				  
				SELECT TOP 1 @AssignUser = [Assigned To], 
							 @CreatedID  =cnv.GetUserIDByName ([Created By]),
							 @ModifiedID = cnv.GetUserIDByName ([Modified By]),
							 @DedCustNme = [Dedicated Customer Name],
							 @Devices      = Devices ,
							 @EnServices    =[Enhanced Services],
							 @EventType		= [Event Types],
							 @IPVer			= [IP Version],
							 @SpcProj		=[Special Projects],
							 @VendModel		= [Vendor Model]							 
						 FROM cnv.Qualifications WHERE Status = 0 AND ISNULL([Assigned To], '') <> ''

SELECT  @AssignUserID = cnv.GetUserIDByName (SUBSTRING (@AssignUser, 1, CHARINDEX (';', @AssignUser) - 1))
 
IF ( ISNULL(@AssignUserID, '') <> '')
BEGIN

SELECT @QAssigned =  DSPL_NME  FROM LK_USER WITH (NOLOCK) WHERE USER_ID = @AssignUserID 
 
SET		@RECStusID = 1
SET		@CreateDt = GETDATE()
SET		@ModifiedDt = GETDATE()

  -- ================================================================================================

IF NOT EXISTS
(SELECT 'X' FROM DBO.LK_QLFCTN  WITH (NOLOCK) WHERE ASN_TO_USER_ID = @AssignUserID)
     BEGIN

 				INSERT INTO DBO.LK_QLFCTN 
							( [ASN_TO_USER_ID]
							  ,[REC_STUS_ID]
							  ,[CREAT_BY_USER_ID]
							  ,[MODFD_BY_USER_ID]
							  ,[MODFD_DT]
							  ,[CREAT_DT]
							)
							
						VALUES
						 (
							CASE WHEN ISNULL (@AssignUserID,'') = '' THEN 1 ELSE @AssignUserID END,
							@RECStusID,
							CASE WHEN ISNULL (@CreatedID,'') = '' THEN 1 ELSE @CreatedID END,
							CASE WHEN ISNULL (@ModifiedID,'') = '' THEN 1 ELSE @ModifiedID END,								
							@ModifiedDt,
							@CreateDt
							
						 )

 
 -- ================================================================================================
print 'Entered Main Table'
-- ================================================================================================
	  
		END

 
 	SELECT @QualifID = QLFCTN_ID FROM LK_QLFCTN WHERE ASN_TO_USER_ID = @AssignUserID
 
 	
 	 
 --PRINT @DedicatedCustID
 ----PRINT @AssignUserID
 --PRINT @QualifID
 --RETURN
 
 -- ================================================================================================
	 
	 
  					
  IF ( ISNULL (@DedCustNme, '') <> '')
     BEGIN
		 INSERT INTO [dbo].[QLFCTN_DEDCTD_CUST]
				([QLFCTN_ID]
			  ,[CUST_ID] 
			  ,[CREAT_DT] )
		  SELECT DISTINCT
			   @QualifID,
			    lc.CUST_ID,  
			   @CreateDt  
		        FROM     LK_DEDCTD_CUST lc WITH (NOLOCK) INNER JOIN
		     cnv.parseStringwithIds (@DedCustNme) pst  ON  pst.DesValue = lc.CUST_NME 
		     WHERE NOT EXISTS 
		     (SELECT 'X' FROM dbo.QLFCTN_DEDCTD_CUST  qdc WHERE 
		     qdc.QLFCTN_ID = @QualifID AND qdc.CUST_ID = lc.CUST_ID )  
	
	
	
-- ================================================================================================
  PRINT ' Step 2 -- Inserted [dbo].[QLFCTN_DEDCTD_CUST]'
 -- ================================================================================================
  END 
  
 
	SELECT   
			@DevID =  IDs FROM  cnv.parseStringwithNumerics (@Devices)
  		
  
  IF ( ISNULL (@Devices, '') <> '')
	BEGIN 
	INSERT INTO  dbo.QLFCTN_DEV
				  ([QLFCTN_ID]
				  ,[DEV_ID]
				  ,[CREAT_DT] )
			SELECT DISTINCT
					@QualifID,
					ld.DEV_ID,
  					@CreateDt
			
					FROM dbo.LK_DEV ld WITH (NOLOCK) 
		INNER JOIN cnv.parseStringwithIds (@Devices) pst ON pst.DesValue = ld.DEV_NME 
		WHERE NOT EXISTS 
		( SELECT 'X' FROM dbo.QLFCTN_DEV qd WITH (NOLOCK) WHERE 
            qd.QLFCTN_ID = @QualifID AND qd.DEV_ID = ld.DEV_ID)
  

	
 -- ================================================================================================
  PRINT '  Step 3 Inserted   dbo.QLFCTN_DEV'
 -- ================================================================================================
  END
  
		
 IF (ISNULL (@EnServices, '') <> '') 
 
 BEGIN
  
 
 UPDATE dbo.LK_ENHNC_SRVC SET ENHNC_SRVC_NME = 'AD GI' WHERE ENHNC_SRVC_NME = 'AD Government or International'
 
 INSERT INTO dbo.QLFCTN_ENHNC_SRVC
				( [QLFCTN_ID]
				  ,[ENHNC_SRVC_ID] 
				  ,[CREAT_DT]
				)
		SELECT DISTINCT
				@QualifID,
				 les.ENHNC_SRVC_ID,
				@CreateDt
				 
				
				FROM LK_ENHNC_SRVC les WITH (NOLOCK)
		INNER JOIN  cnv.parseStringwithIds (@EnServices) cpt ON cpt.DesValue = les.ENHNC_SRVC_NME
		WHERE NOT EXISTS
				(SELECT 'X' FROM dbo.QLFCTN_ENHNC_SRVC qes WITH (NOLOCK) 
				 WHERE qes.QLFCTN_ID = @QualifID AND qes.ENHNC_SRVC_ID = les.ENHNC_SRVC_ID)
		
	
 UPDATE dbo.LK_ENHNC_SRVC SET ENHNC_SRVC_NME = 'AD Government or International' WHERE ENHNC_SRVC_NME = 'AD GI'			
 
  -- ================================================================================================
  PRINT '  Step 4 Inserted   dbo.QLFCTN_ENHNC_SRVC'
 -- ================================================================================================
 END
   
 		IF(ISNULL(@EventType, '') <> '') 
 		BEGIN
	 
	INSERT INTO dbo.QLFCTN_EVENT_TYPE
				( [EVENT_TYPE_ID]
				  ,[QLFCTN_ID]
	 			  ,[CREAT_DT]
				)
		SELECT DISTINCT
				    lke.EVENT_TYPE_ID,
				    @QualifID,	 
				   @CreateDt
				FROM dbo.LK_EVENT_TYPE lke WITH (NOLOCK) 
		INNER JOIN  cnv.parseStringwithIds (@EventType) psi on psi.DesValue = lke.EVENT_TYPE_NME
		WHERE NOT EXISTS
		      (SELECT 'X' FROM dbo.QLFCTN_EVENT_TYPE qet WITH (NOLOCK)
		       WHERE qet.QLFCTN_ID = @QualifID AND qet.EVENT_TYPE_ID = lke.EVENT_TYPE_ID)
				
	  
  -- ================================================================================================
  PRINT '  Step 5 Inserted   dbo.QLFCTN_EVENT_TYPE '
 -- =====================================================================================================
	 END
	 
			
 	IF(ISNULL(@IPVer, '') <> '') 
 	
 	BEGIN
	 INSERT INTO dbo.QLFCTN_IP_VER
				( [QLFCTN_ID]
				  ,[IP_VER_ID]
 				  ,[CREAT_DT])
		SELECT DISTINCT
				   @QualifID,
				  liv.IP_VER_ID,	 
				   @CreateDt
			FROM LK_IP_VER liv WITH (NOLOCK) 
	INNER JOIN cnv.parseStringwithIds (@IPVer) cpi ON cpi.DesValue = liv.IP_VER_NME 
	WHERE NOT EXISTS
		(SELECT 'X' FROM dbo.QLFCTN_IP_VER  qiv WITH (NOLOCK)
		  WHERE qiv.QLFCTN_ID = @QualifID AND qiv.IP_VER_ID = liv.IP_VER_ID) 
	 
  -- ================================================================================================
  PRINT '  Step 6 Inserted   dbo.QLFCTN_EVENT_TYPE '
 -- =====================================================================================================
END	 
	
  	IF(ISNULL(@SpcProj, '') <> '') 
  	
  	BEGIN 
 		
	 INSERT INTO dbo.QLFCTN_SPCL_PROJ
				  ([QLFCTN_ID]
				  ,[SPCL_PROJ_ID] 
				  ,[CREAT_DT])
			SELECT DISTINCT
				   
				    @QualifID,
				    lsp.SPCL_PROJ_ID,
				    @CreateDt
				   
				   FROM LK_SPCL_PROJ lsp WITH (NOLOCK) 
			INNER  JOIN cnv.parseStringwithIds (@SpcProj) cpi on cpi.DesValue = lsp.SPCL_PROJ_NME
			WHERE NOT EXISTS
			(SELECT 'X' FROM dbo.QLFCTN_SPCL_PROJ qsp WITH (NOLOCK) 
			 WHERE qsp.QLFCTN_ID = @QualifID AND qsp.SPCL_PROJ_ID = lsp.SPCL_PROJ_ID)

	 
  -- ================================================================================================
  PRINT '  Step 7 Inserted    dbo.QLFCTN_SPCL_PROJ'
 -- =====================================================================================================
 END
 
	SELECT   
	
		 @VendorModelID =  IDs FROM cnv.parseStringwithNumerics (@VendModel)
	 	
	 	IF(ISNULL(@VendorModelID, '') <> '') 
  	
  	BEGIN 
	 
	 INSERT INTO dbo.QLFCTN_VNDR_MODEL
				(
					[QLFCTN_ID]
				  ,[DEV_MODEL_ID] 
				  ,[CREAT_DT]
				) 
		SELECT DISTINCT 
					@QualifID,
					ldm.DEV_MODEL_ID,	 
				    @CreateDt
				
				FROM LK_DEV_MODEL ldm WITH (NOLOCK) 
		INNER JOIN cnv.parseStringwithIds (@VendModel) cpi ON cpi.DesValue = ldm.DEV_MODEL_NME
		WHERE NOT EXISTS
		(SELECT 'X' FROM dbo.QLFCTN_VNDR_MODEL  qvm WITH (NOLOCK) 
		  WHERE qvm.QLFCTN_ID = @QualifID AND qvm.DEV_MODEL_ID = ldm.DEV_MODEL_ID)
		  
	 
	   -- ================================================================================================
  PRINT '  Step 8 Inserted    dbo.QLFCTN_VNDR_MODEL'
 -- =====================================================================================================
		
		END
			UPDATE  cnv.Qualifications SET Status = 1 WHERE [Assigned To] = @AssignUser
			
	print 'Updated qualifications'
			SET @Ctr = @Ctr + 1 
	print 'Counter incremented'
	 
	 
	 -- ================================================================================================
	END 

ELSE
 	
 	UPDATE  cnv.Qualifications SET Status = 2 WHERE [Assigned To] = @AssignUser
 	
 	PRINT ' User ID does not exist in lk user table'
END
END
    	ELSE 
    	
    	PRINT ' No Ids to Insert'
END TRY

 BEGIN CATCH 
	EXEC [dbo].InsertErrorInfo	  
 END CATCH

  SET NOCOUNT OFF
  END
