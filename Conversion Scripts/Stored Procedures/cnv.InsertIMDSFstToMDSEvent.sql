USE [COWS]
GO
_CreateObject 'SP','cnv','InsertIMDSFstToMDSEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

-- =======================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	11/14/2011 
-- Description:	Inserts Data into MDS Event Table 
-- Event Types 
-- ================================================================================================

 

ALTER PROCEDURE [cnv].[InsertIMDSFstToMDSEvent]
AS

BEGIN
SET NOCOUNT ON



DECLARE @EventID		INT
DECLARE @EVENTSTATUSID	INT
DECLARE @NewEventID		INT
DECLARE @MaxID			INT
DECLARE @CEventID		INT
DECLARE @TableID	nvarchar(100)
DECLARE @Cnt			INT
DECLARE @Ctr			INT
DECLARE @FastTrackFlag  Bit
DECLARE @DescComments Varchar(5000)
DECLARE @IntFlag Varchar(1)
DECLARE @MacActivity Varchar(1000)
DECLARE @AuthStatus BIT 
DECLARE @DesignNumber Varchar(100)

SET		@Cnt			= 0
SET		@Ctr			= 1 
SET @EventID			= 0
SET @FastTrackFlag       = 1
Begin Try  
-- =============================================================================================
/* 
Event Types for cnv.EVENT_Tmp table
   1 - Ad Event  
   2 - MDS
   3 - MPLS
   4 - NGVN
   5 - Sprint Link 
 */
-- ================================================================================================



		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
		
		
UPDATE cnv.MDSFastTrack SET Status = 4 WHERE ISNUMERIC(FTN) = 0 and ISNULL(FTN,'') <> ''


UPDATE cnv.MDSFastTrack SET Status = 8 WHERE  ISNUMERIC(H6) = 0 AND ISNULL(H6,'') <> ''

SELECT @MaxID = MAX(Event_ID) from EVENT
SET @MaxID = @MaxID + 1

DBCC CHECKIDENT ( [cnv.Event_Tmp], RESEED, @maxID)

   
 INSERT INTO cnv.EVENT_Tmp
 (EventID,
 EventStatus,
 EventType,
 CreateDate)
 SELECT   
 Event_ID,
 [cnv].[GetEventStatusID] (ne.[Event Status]),
 5,
  [Created]
 FROM cnv.MDSFastTrack ne WITH (NOLOCK)
 WHERE Status = 0 AND ISNULL(Event_ID, '') <> '' 
		AND   NOT EXISTS 
		(SELECT 'X' FROM  cnv.EVENT_Tmp et WITH (NOLOCK) WHERE 
		ne.Event_ID = et.EventID  AND Status in(0,2,5)  AND EventType = 5) Order by Event_ID asc
 
 -- ================================================================================================
 		SET @Cnt = 0
		SET @Ctr = 1
		SELECT 		@Cnt	=	Count(1) FROM cnv.EVENT_Tmp Where Status = 0 and EventType = 5
		
					IF @Cnt	>	0
					BEGIN

						WHILE	@Ctr <= @Cnt
							BEGIN
							
							
									SET @EventID = 0
									SET	@NewEventID  =0
									SET	@TableID  = ''
									SET	@DescComments =''
									SET	@MacActivity =''
									SET	@IntFlag = 0
									SET	@AuthStatus = ''
									SET @EVENTSTATUSID = 0
									SET @DesignNumber = ''
				
						 SELECT  TOP  1 @EventID  = ev.EventID,
										@NewEventID = ev.NewEventID,
										@TableID  = md.AccessTableID,
										@DescComments = md.[Description and Comments],
										@MacActivity   = md.[MDS MAC Activity],
										@IntFlag	   = CASE WHEN  ISNULL(md.[US or International],'')  = 'International' THEN 'I' ELSE 'D' END,
										@AuthStatus    = CASE WHEN md.Auth_Status ='AUTHORISED' THEN 1 ELSE 0 END,
										@EVENTSTATUSID = ev.EventStatus,
										@DesignNumber  = md.[Design Number]

								   FROM cnv.EVENT_Tmp ev WITH (NOLOCK)  
							 INNER JOIN cnv.MDSFastTrack md WITH (NOLOCK) ON ev.EventID = md.Event_ID
								   WHERE ev.EventType = 5 and ev.Status = 0   
    
    
    PRINT @EventID
    PRINT @NewEventID
    PRINT @TableID
  
    
   -- ================================================================================================

		 IF NOT EXISTS
		(SELECT 'X' FROM dbo.EVENT WITH (NOLOCK) WHERE EVENT_ID  = @NewEventID)

					BEGIN
 					
				 SET IDENTITY_INSERT DBO.EVENT ON
					 INSERT INTO DBO.EVENT
						 (	EVENT_ID,
							EVENT_TYPE_ID,
							CREAT_DT) 
				 
	 		
			  SELECT 
						  NewEventID,
						  EventType,
						  CreateDate
						  FROM	cnv.EVENT_Tmp WHERE  NewEventID = @NewEventID    AND EventType = 5 AND [Status] = 0 
 
				 SET IDENTITY_INSERT DBO.EVENT OFF
		 
  -- ================================================================================================
   	PRINT ' Step 1 Inserting   Event Complete'  
  -- ================================================================================================
 		   END
  
 
   
     IF NOT EXISTS
		(SELECT 'X' FROM [dbo].[MDS_EVENT]  WITH (NOLOCK) WHERE EVENT_ID = @NewEventID)
	 
  
  BEGIN
      
  ALTER TABLE  [dbo].[MDS_EVENT] NOCHECK CONSTRAINT ALL 
 
  
 INSERT INTO	dbo.MDS_EVENT
			  ([EVENT_ID]
			  ,[MDS_FAST_TRK_CD]
			  ,[MDS_FAST_TRK_TYPE_ID]
			  ,[EVENT_STUS_ID]
			  ,[FTN]
			  ,[CHARS_ID]
			  ,[H1]
			  ,[H6]
			  ,[CUST_NME]
			  ,[CUST_EMAIL_ADR]
	 		  ,[MDS_ACTY_TYPE_ID]
			  ,[MDS_INSTL_ACTY_TYPE_ID]
			  ,[FULL_CUST_DISC_CD]
			  ,[DEV_CNT]
			  ,[FRWL_SCTY_PROD_CD]
			  ,[DSGN_DOC_LOC_TXT]
			  ,[RDSGN_NBR]
			  ,[MDS_BRDG_NEED_CD]
 			  ,[DES_CMNT_TXT]
			  ,[SHRT_DES]
 			  ,[REQOR_USER_ID]
			  ,[PUB_EMAIL_CC_TXT]
			  ,[CMPLTD_EMAIL_CC_TXT]
			  ,[DISC_NTFY_PDL_NME]
			  ,[CUST_ACCT_TEAM_PDL_NME]
			  ,[ESCL_CD]
			  ,[PRIM_REQ_DT]
			  ,[SCNDY_REQ_DT]
			  ,[ESCL_REAS_ID]
			  ,[STRT_TMST]
			  ,[EXTRA_DRTN_TME_AMT]
			  ,[END_TMST]
			  ,[WRKFLW_STUS_ID]
			  ,[REC_STUS_ID]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_BY_USER_ID]
			  ,[MODFD_DT]
			  ,[CREAT_DT]
			  ,[EVENT_TITLE_TXT]
			  ,[IP_VER_ID]
			  ,[CNFRC_BRDG_NBR]
			  ,[CNFRC_PIN_NBR]
			  ,[SOWS_EVENT_ID]
			  ,[SCURD_CD]
			  ,[EVENT_DRTN_IN_MIN_QTY]
			  )   
      
SELECT  TOP 1 
				 
@NewEventID,
@FastTrackFlag,
SUBSTRING (LTRIM(RTRIM(mds.[Fast Track Type])), 1,1),
@EVENTSTATUSID, 
CASE WHEN ISNULL(mds.[FTN], '') = '' THEN -1 ELSE SUBSTRING(mds.FTN, 1,9) END AS FTN,
LTRIM(RTRIM(mds.[CharsID])),
SUBSTRING (LTRIM(RTRIM(mds.[H1])),1,9),
SUBSTRING (LTRIM(RTRIM(mds.[H6])),1,9),
CAST ( dbo.encryptString (mds.[Customer Name]) AS VARBINARY(MAX)),
CAST ( dbo.encryptString (mds.[Customer Account Team PDL]) AS VARBINARY(MAX)),
LTRIM(RTRIM(mdt.[MDS_ACTY_TYPE_ID])),
LTRIM(RTRIM(mdc.[MDS_INSTL_ACTY_TYPE_ID])), 
 CASE WHEN ISNULL(LTRIM(RTRIM(mds.[Total Customer Disconnect])), '') = '' THEN 1
 ELSE mds.[Total Customer Disconnect] END , 
LTRIM(RTRIM(mds.[Number of Devices to Disconnect])),  
LTRIM(RTRIM(mds.[Is Firewall Security Product?])),
CASE WHEN ISNULL( LTRIM(RTRIM(mds.[Design Document Location])),'') = '' THEN 'Converted Order' ELSE mds.[Design Document Location] END,
CASE WHEN ISNULL(LTRIM(RTRIM(mds.[Design Number])), '') = '' THEN 'Converted Order' ELSE SUBSTRING( mds.[Design Number],1,50) END,
ISNULL(mds.[Will Conference Bridge Be Needed?], 0),
CASE WHEN ISNULL(LTRIM(RTRIM(mds.[Description and Comments])), '') = '' THEN ' Converted Order'
	ELSE SUBSTRING (mds.[Description and Comments], 1,2000)END,
LTRIM(RTRIM(mds.[Short Description])), 
cnv.GetUserIDByName (mds.[Request Contact Name]), 
LTRIM(RTRIM(mds.[Published E-Mail CC:])),
LTRIM(RTRIM(mds.[Completed E-Mail CC:])),
LTRIM(RTRIM(mds.[Disconnect Notify PDL])), 
LTRIM(RTRIM(mds.[Customer Account Team PDL])),
LTRIM(RTRIM(mds.[Is this an Escalation?])),
LTRIM(RTRIM(mds.[Primary Request Date])),
LTRIM(RTRIM(mds.[Secondary Request Date])),
cnv.GetEscalationReasonID (mds.[Escalation Reason]), 
LTRIM(RTRIM(mds.[Requested Installation Date])),
LTRIM(RTRIM(mds.[Extra Duration])),
LTRIM(RTRIM(mds.[Requested Installation Date])),
cnv.GetWorkFlowStatusID (mds.[Workflow Status]),
1, -- Record Status from  LK_REC_STUS  
CASE WHEN ISNULL (cnv.GetUserIDByName (mds.[Created By]), 0)  = 0 THEN 1 ELSE cnv.GetUserIDByName (mds.[Created By]) END ,
CASE WHEN ISNULL (cnv.GetUserIDByName (mds.[Modified By]), 0)  = 0 THEN 1 ELSE cnv.GetUserIDByName (mds.[Modified By]) END,
LTRIM(RTRIM(mds.[Modified])),
LTRIM(RTRIM(mds.[Created])),
dbo.encryptString (mds.[Item Title]),
NULL AS IPVER,
SUBSTRING (mds.[Bridge Number], 1,50),
SUBSTRING (mds.[Bridge Pin], 1,20),
@EventID,
@AuthStatus,
CASE WHEN ISNULL (mds.[Event Duration], 0) = 0 THEN 0 ELSE mds.[Event Duration] END
						 FROM cnv.MDSFastTrack mds with (NOLOCK)	
		INNER JOIN 		 cnv.EVENT_Tmp  evt ON mds.Event_ID = evt.EventId	
    	LEFT JOIN		 dbo.LK_MDS_SRVC_TIER svt with (NOLOCK)	ON mds.[Service Tier] = svt.MDS_SRVC_TIER_DES 
		LEFT JOIN		 dbo.LK_MDS_ACTY_TYPE mdt with (NOLOCK)	 ON mds.[MDS Activity Type] = mdt.MDS_ACTY_TYPE_DES 
		LEFT JOIN		 dbo.LK_MDS_INSTL_ACTY_TYPE mdc with (NOLOCK)	 ON mds.[MDS Activity Type] = mdc.MDS_INSTL_ACTY_TYPE_DES 
		LEFT JOIN		 dbo.LK_SRVC_ASSRN_SITE_SUPP sas with (NOLOCK)	 ON mds.[Service Assurance Site Support] = sas.SRVC_ASSRN_SITE_SUPP_DES 
		LEFT JOIN		 dbo.LK_SPRINT_CPE_NCR scn with (NOLOCK)	 ON mds.[Is Sprint CPE or NCR Required] = scn.SPRINT_CPE_NCR_DES  
	 
	 
		Where mds. Event_ID = @EventID
 
	
		ALTER TABLE [dbo].[MDS_EVENT] CHECK CONSTRAINT ALL 
		
		
	 
 -- ================================================================================================
 	PRINT ' Step 2 Inserting to MDS Event Complete'
 -- ================================================================================================
 END

    
     IF NOT EXISTS
		(SELECT 'X' FROM [dbo].[FSA_MDS_EVENT]  WITH (NOLOCK) WHERE EVENT_ID = @NewEventID)
 BEGIN
 
  INSERT INTO	dbo.FSA_MDS_EVENT
	( [EVENT_ID]
      ,[TAB_NME]  
      ,[ORDR_EVENT_STUS_ID]
      ,[ADR]
      ,[FLR_BLDG_NME]
      ,[CTY_NME]
      ,[STT_PRVN_NME]
      ,[ZIP_CD]
      ,[US_INTL_ID]
      ,[CTRY_RGN_NME]
      ,[TME_ZONE_ID]
      ,[INSTL_SITE_POC_NME]
      ,[INSTL_SITE_POC_PHN_NBR]
      ,[INSTL_SITE_POC_CELL_PHN_NBR]
      ,[SRVC_ASSRN_POC_NME]
      ,[SRVC_ASSRN_POC_PHN_NBR]
      ,[SRVC_ASSRN_POC_EMAIL_ADR]
      ,[WIRED_DEV_TRNSPRT_REQR_CD]
      ,[WRLS_TRNSPRT_REQR_CD]
      ,[VRTL_CNCTN_CD]
      ,[ESCL_CD]
      ,[CREAT_BY_USER_ID]
      ,[MODFD_BY_USER_ID]
      ,[MODFD_DT]
      ,[CREAT_DT]
      ,[SPRINT_CPE_NCR_ID]
      ,[SRVC_ASSRN_SITE_SUPP_ID]
      ,[CPE_DSPCH_EMAIL_ADR]
      ,[CPE_DSPCH_CMNT_TXT]
      ,[MULTI_LINK_CKT_CD]
      ,[SRVC_ASSRN_POC_CELL_NBR]
      ,[EVENT_SCHED_NTE_TXT]
      ,[TAB_SEQ_NBR]
      ,[H5_H6_ID]
      
      ,[CMPLTD_CD]

      )
      
SELECT  TOP 1 
@NewEventID,
CASE WHEN ISNULL(mds.H6,'') = '' THEN 123 ELSE SUBSTRING (mds.H6, 1,9) END AS TABNAME,  --'abc' AS TABNAME,
@EVENTSTATUSID,
 CASE WHEN ISNULL(mds.[Address],'') = '' THEN  ( dbo.encryptString ('Unknown'))
	ELSE
  dbo.encryptString (mds.[Address])  END,
--ISNULL (CAST ( dbo.encryptString (mds.[Address]) AS VARBINARY(MAX)), 'Unknown...'),
CAST ( dbo.encryptString (mds.[Floor/Building]) AS VARBINARY(MAX)),
CAST ( dbo.encryptString (mds.[City]) AS VARBINARY(MAX)),
CAST ( dbo.encryptString (mds.[State/Province]) AS VARBINARY(MAX)),
CAST ( dbo.encryptString (mds.[ZIP/Postal Code]) AS VARBINARY(MAX)),
 dbo.encryptString (@IntFlag) , 
CAST ( dbo.encryptString (mds.[Country/Region]) AS VARBINARY(MAX)),
ltz.TME_ZONE_ID,
 dbo.encryptString (mds.[Install Site POC]),
LTRIM(RTRIM(mds.[Install Site POC Phone])),
SUBSTRING (LTRIM(RTRIM(mds.[Install Site POC Cell])), 1,16),
dbo.encryptString (mds.[Service Assurance POC]),
LTRIM(RTRIM(mds.[Service Assurance POC Phone])),
LTRIM(RTRIM(mds.[Service Assurance POC E-Mail])),
ISNULL(mds.[Is Wired Device Transport Required?], 0),
ISNULL(mds.[Is Wireless Transport Required?],0),
ISNULL(mds.[Any Virtual Connections?],0),
LTRIM(RTRIM(mds.[Is this an Escalation?])), 
CASE WHEN ISNULL (cnv.GetUserIDByName (mds.[Created By]), 0)  = 0 THEN 1 ELSE cnv.GetUserIDByName (mds.[Created By]) END ,
CASE WHEN ISNULL (cnv.GetUserIDByName (mds.[Modified By]), 0)  = 0 THEN 1 ELSE cnv.GetUserIDByName (mds.[Modified By]) END,
LTRIM(RTRIM(mds.[Modified])),
LTRIM(RTRIM(mds.[Created])),
CASE WHEN ISNULL(LTRIM(RTRIM(scn.SPRINT_CPE_NCR_ID)),0)= 0 THEN 2 ELSE scn.SPRINT_CPE_NCR_ID END,
LTRIM(RTRIM(sas.SRVC_ASSRN_SITE_SUPP_ID)),
LTRIM(RTRIM(MDS.[CPE Dispatch Email Address])), 
LTRIM(RTRIM(mds.[CPE Dispatch Comments])),
LTRIM(RTRIM(mds.[Any Multilink Circuits?])),
NULL AS [SRVC_ASSRN_POC_CELL_NBR],
LTRIM(RTRIM(mds.[Item Title])), 
 1  AS [TAB_SEQ_NBR],
CASE WHEN ISNULL(mds.H6,'') = '' THEN 123 ELSE SUBSTRING (mds.H6, 1,9) END AS [H5_H6_ID],
 
CASE WHEN mds.[Event Status] = 'COMPLETED' THEN 1 ELSE 0 END AS [CMPLTD_CD]




 	  FROM cnv.MDSFastTrack mds with (NOLOCK)	
		INNER JOIN 		 cnv.EVENT_Tmp  evt ON mds.Event_ID = evt.EventId		 									
 		LEFT JOIN		 dbo.LK_MDS_SRVC_TIER svt with (NOLOCK)	ON mds.[Service Tier] = svt.MDS_SRVC_TIER_DES 
		LEFT JOIN		 dbo.LK_MDS_ACTY_TYPE mdt with (NOLOCK)	 ON mds.[MDS Activity Type] = mdt.MDS_ACTY_TYPE_DES 
		LEFT JOIN		 dbo.LK_MDS_INSTL_ACTY_TYPE mdc with (NOLOCK)	 ON mds.[MDS Activity Type] = mdc.MDS_INSTL_ACTY_TYPE_DES 
		LEFT JOIN		 dbo.LK_SRVC_ASSRN_SITE_SUPP sas with (NOLOCK)	 ON mds.[Service Assurance Site Support] = sas.SRVC_ASSRN_SITE_SUPP_DES 
		LEFT JOIN		 dbo.LK_SPRINT_CPE_NCR scn with (NOLOCK)	 ON mds.[Is Sprint CPE or NCR Required] = scn.SPRINT_CPE_NCR_DES  
 	 		LEFT JOIN		dbo.LK_TME_ZONE ltz WITH (NOLOCK)	ON 
						  CASE	WHEN mds.[US Timezone] = 'US/Mountain' THEN 'MT'
								WHEN mds.[US Timezone] = 'US/Eastern' THEN 'ET'
								WHEN mds.[US Timezone] = 'US/Central' THEN 'CT'
								WHEN mds.[US Timezone] = 'US/Pacific' THEN 'PT' ELSE NULL END = ltz.TME_ZONE_NME
 
		Where mds. Event_ID = @EventID
  
 -- ================================================================================================
 	PRINT ' Step 3 Inserting to FSA MDS Event Complete'
 -- ================================================================================================
 END
   
 
 DECLARE @FSAMDSEventID INT
 SET @FSAMDSEventID = 0
 SELECT @FSAMDSEventID = FSA_MDS_EVENT_ID from dbo.FSA_MDS_EVENT WHERE EVENT_ID = @NewEventID
 
 
  -- ================================================================================================
 	PRINT ' Step 4 FSA_MDS_EVENT_ID is'  
 	PRINT  @FSAMDSEventID
 -- ================================================================================================
 
  
 IF EXISTS
 (SELECT 'X' FROM cnv.MDSFastWiredTrspt    WITH (NOLOCK) WHERE  TableID = @TableID) 
 
 
  DECLARE @FOCDate SmallDateTime
 SET @FOCDate  =''
 
 BEGIN TRY
					
								
SELECT @FOCDate = CASE WHEN  RIGHT (FOCDate, 1) = '.' THEN 
		CAST (SUBSTRING(FOCDate, 1, (LEN(FOCDATE) -1)) AS SMALLDATETIME)
		WHEN ISNULL(FOCDATE, '') = '' THEN CAST(0 AS SMALLDATETIME)
		WHEN FOCDate like '%th%' THEN CAST( REPLACE(FOCDATE, 'th', '') AS SMALLDATETIME)
		--WHEN FOCDate like '%st%' THEN CAST( REPLACE(FOCDATE, 'st', '') AS SMALLDATETIME)
		WHEN SUBSTRING(FOCDATE, 1, 4 ) = '.' THEN  CAST( REPLACE(FOCDATE, '.', '') AS SMALLDATETIME)
		WHEN FOCDate like '%.%'THEN  CAST( REPLACE(FOCDATE, '.', ',') AS SMALLDATETIME)
		WHEN SUBSTRING(FOCDATE, 3, 3 ) = 'st' THEN  CAST( REPLACE(FOCDATE, 'st', '') AS SMALLDATETIME)
		WHEN FOCDate like '%NN%'THEN CAST(0 AS SMALLDATETIME)
		WHEN FOCDate ='7/2008' THEN CAST(0 AS SMALLDATETIME)
		WHEN FOCDate ='6/232005' THEN CAST(0 AS SMALLDATETIME)
		WHEN SUBSTRING(FOCDATE, 3, 3 ) = 'nd' THEN  CAST( REPLACE(FOCDATE, 'nd', '') AS SMALLDATETIME)
		ELSE  CAST (FOCDATE AS SmallDatetime) END from cnv.MDSFastWiredTrspt WHERE TableID = @TableID
		
 
 END TRY
 BEGIN CATCH
 
 SELECT @FOCDate = CAST( 0 AS SMallDateTime)
 END CATCH
 
 
	BEGIN 
	
		INSERT INTO  dbo.MDS_EVENT_WIRED_TRNSPRT_DEV   
					( [FSA_MDS_EVENT_ID]
				  ,[MDS_TRNSPRT_TYPE_ID]
				  ,[TELCO_ID]
				  ,[PRIM_BKUP_CD]
				  ,[FMS_CKT_ID]
				  ,[PL_NBR]
				  ,[NUA_449_ADR]
				  ,[OLD_CKT_ID]
				  ,[MULTI_LINK_CKT_CD]
				  ,[SPRINT_MNGD_CD]
				  ,[FOC_DT]
				  ,[CREAT_DT]
				  ,[NEW_NUA_449_ADR]
				  ,[NEW_FMS_CKT_ID]
				  ,[TRPT_FTN])
	 	  SELECT  DISTINCT
				@FSAMDSEventID,
				tt.MDS_TRNSPRT_TYPE_ID,
				lt.TELCO_ID,
				--CASE WHEN ISNUMERIC(mw.Telco) = 1 THEN mw.Telco ELSE '' END,
				SUBSTRING (mw.[Primary],1,1),			

				SUBSTRING(mw.CircuitID,1,100),
			    ISNULL (SUBSTRING (mw.PL,1,20),'123'),
				CASE WHEN ISNULL(mw.NUA,'')= '' THEN 'Converted Order' ELSE SUBSTRING (mw.NUA,1,25) END,
				CASE WHEN mw.OldCircuit = 'Click to edit' THEN '' ELSE SUBSTRING (mw.OldCircuit,1,30) END,
			    CASE WHEN SUBSTRING (mw.MultilinkCircuit,1,1)= 'Y' THEN 1 ELSE 0 END,
				SUBSTRING (mw.Managed,1,1),
				@FOCDate,	 
				-- GETDATE(), 
				GETDATE(),
				'',
				'',
				NULL
 
		FROM 	cnv.MDSFastWiredTrspt  mw   WITH (NOLOCK) INNER JOIN
			LK_MDS_TRNSPRT_TYPE  tt WITH (NOLOCK) ON mw.TransportType = tt.MDS_TRNSPRT_TYPE_DES	
			LEFT JOIN dbo.LK_TELCO lt WITH (NOLOCK) ON mw.Telco = (lt.TELCO_NME + ': '+ lt.TELCO_CNTCT_PHN_NBR)
	
		WHERE 	TableID = @TableID  
 
 
  -- ================================================================================================
 	PRINT ' Step 5 Inserting to dbo.MDS_Wired Trans 1  Complete'
 -- ================================================================================================
	END

 IF EXISTS
 (SELECT 'X' FROM cnv.MDSFastckt    WITH (NOLOCK) WHERE  TableID = @TableID) 
 
	BEGIN 
	
			DECLARE @Circuit nVarchar(1000)
		 	Set @Circuit = ''
			DECLARE @TmpCk TABLE
			(
			CircuitID Varchar(1000)
			)
 	 
			SELECT @Circuit = COALESCE (@Circuit,'', '') + '  ' + Circuit  from cnv.MDSFastckt  WHERE 	TableID = @TableID 
							 AND Circuit <> 'Click to edit' AND Circuit <> 'N/A' AND Circuit <> 'test'
			
			
			SELECT @Circuit = REPLACE (@Circuit, '-', ' ')
			SELECT @Circuit = REPLACE (@Circuit, ',', ' ')
			SELECT @Circuit = REPLACE (@Circuit, '.', ' ')
			 
			 
  
			INSERT INTO @TmpCk (CircuitID)
			Select StringID from [cnv].[parseStringWithSpace](@Circuit)

	
		INSERT INTO  dbo.MDS_EVENT_WIRED_TRNSPRT_DEV   
					( [FSA_MDS_EVENT_ID]
				  ,[MDS_TRNSPRT_TYPE_ID]
				  ,[TELCO_ID]
				  ,[PRIM_BKUP_CD]
				  ,[FMS_CKT_ID]
				  ,[PL_NBR]
				  ,[NUA_449_ADR]
				  ,[OLD_CKT_ID]
				  ,[MULTI_LINK_CKT_CD]
				  ,[SPRINT_MNGD_CD]
				  ,[FOC_DT]
				  ,[CREAT_DT]
				  ,[NEW_NUA_449_ADR]
				  ,[NEW_FMS_CKT_ID])
		  SELECT DISTINCT
				@FSAMDSEventID,
				1,
				NULL, 
				'Z',
				CircuitID,
				123,
				123,
				'',
				1,
				'Y',
				GetDate(),
				GETDATE(),
				'',
				''
 
		FROM 	@TmpCk WHERE ISNULL (CircuitID, '') <> ''
	 

  -- ================================================================================================
 	PRINT ' Step 6 Inserting to dbo.MDS_Wired Trans 2 Complete'
 	-- ================================================================================================
 	END
 	
  IF EXISTS
 (SELECT 'X' FROM cnv.MDSFastDiscOE    WITH (NOLOCK) WHERE  TableID  = @TableID)
 
 BEGIN
 
 
 INSERT INTO dbo.MDS_EVENT_DISC_OE 
			( [FTN]
			  ,[VNDR_CD]
			  ,[H5_H6_CUST_ID]
			  ,[SERIAL_NBR]
			  ,[ADR]
			  ,[BLDG_NME]
			  ,[CTY_NME]
			  ,[STT_NME]
			  ,[ZIP_CD]
			  ,[CREAT_DT]
			  ,[DEV_MODEL_NME]
			  ,[ODIE_DEV_NME]
			  ,[EVENT_ID]
			  ,[MANF_ID]
			  ,[DEV_MODEL_ID]
			  ,[FSA_MDS_EVENT_ID]
			)
				SELECT DISTINCT
				mft.FTN,			  
				NULL, 
				mft.H6,			
				mdd. [SerialNumber],
				mdd. [Address],
				mdd. [Building],
				mdd. [City],
				mdd. [State],
				SUBSTRING (mdd. [ZIP],1,10),
				GETDATE(),
				'',
				mdd. [ODIEDeviceName],
				@NewEventID,
				NULL,
				NULL,
				@FSAMDSEventID
 
						
			FROM  cnv.MDSFastDiscOE   mdd WITH (NOLOCK) 
	INNER JOIN cnv.MDSFastTrack mft WITH (NOLOCK) ON mdd.TableID = mft.AccessTableID
	--INNER JOIN  LK_VNDR   lvm WITH (NOLOCK) ON  mdd.[Vendor] = lvm.VNDR_NME
	WHERE  mdd.TableID = @TableID
 

--   ================================================================================================
 	PRINT ' Step 6 - 1 Inserting to dbo.MDS_EVENT_DISC_OE  Complete'
---  ================================================================================================	
	END
	
	 DECLARE @AssignID Varchar(100)
	 DECLARE @AuID Int
	 
	SELECT @AssignID  =  [Assigned To]  FROM cnv.MDSFastTrack WHERE Event_ID = @EventID
	IF ( ISNULL (@AssignID,'') <> '') 
	
	BEGIN 
	 		 
  
	
	  INSERT INTO 	dbo.EVENT_ASN_TO_USER		   
						([EVENT_ID]
					  ,[ASN_TO_USER_ID]
					  ,[CREAT_DT]
					  ,[REC_STUS_ID]) 
					  
				SELECT 
						@NewEventID,
						lu.[USER_ID],					 
						CASE WHEN ISNULL(ne.Created,'') ='' THEN GETDATE() ELSE ne.Created END,
						1
					 
					 FROM  dbo.LK_USER   lu WITH (NOLOCK)  
				     INNER JOIN cnv.parseStringwithIds (@AssignID) pst ON 
									 SUBSTRING(lu.DSPL_NME, 1, CHARINDEX('[',lu.DSPL_NME))
								  =  SUBSTRING(pst.DesValue, 1, CHARINDEX('[',pst.DesValue)) 
					INNER JOIN cnv.MDSFastTrack  ne WITH (NOLOCK) ON ne.Event_ID = @EventID
					 AND NOT EXISTS
						(SELECT 'X'  FROM dbo.EVENT_ASN_TO_USER  ea WITH (NOLOCK)
						INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON ea.ASN_TO_USER_ID = lu.[USER_ID]
						WHERE 
						ea.EVENT_ID = @NewEventID AND ea.ASN_TO_USER_ID = CASE WHEN ISNULL (lu.[USER_ID],'') = '' THEN 1 ELSE lu.[USER_ID] END )
					
	 

-- ================================================================================================	 	  
 PRINT 'Step 7 - Complete   Assigned User table'
-- ================================================================================================
 
  
 END
 
  IF EXISTS
 (SELECT 'X' FROM cnv.MDSFastOCT  WITH (NOLOCK) WHERE  TableID  = @TableID)
 BEGIN
 
 DECLARE @RasDate SmallDateTIme
 SET @RasDate = ''
 
		BEGIN TRY
				 SELECT @RasDate =
				CASE WHEN  RIGHT (Rasdate, 1) = '.' THEN 
						CAST (SUBSTRING(Rasdate, 1, (LEN(Rasdate) -1)) AS SMALLDATETIME)
						WHEN ISNULL(Rasdate, '') = '' THEN CAST(0 AS SMALLDATETIME)
						WHEN Rasdate like '%th%' THEN CAST( REPLACE(Rasdate, 'th', '') AS SMALLDATETIME)
						--WHEN Rasdate like '%st%' THEN CAST( REPLACE(Rasdate, 'st', '') AS SMALLDATETIME)						 
						WHEN SUBSTRING(Rasdate, 1, 4 ) = '.' THEN  CAST( REPLACE(Rasdate, '.', '') AS SMALLDATETIME)
						WHEN Rasdate like '%.%'THEN  CAST( REPLACE(Rasdate, '.', ',') AS SMALLDATETIME)
						WHEN SUBSTRING(Rasdate, 3, 3 ) = 'st' THEN  CAST( REPLACE(Rasdate, 'st', '') AS SMALLDATETIME)
						WHEN Rasdate like '%NN%'THEN CAST(0 AS SMALLDATETIME)
						WHEN Rasdate ='7/2008' THEN CAST(0 AS SMALLDATETIME)
						WHEN Rasdate ='6/232005' THEN CAST(0 AS SMALLDATETIME)
						WHEN SUBSTRING(Rasdate, 3, 3 ) = 'nd' THEN  CAST( REPLACE(Rasdate, 'nd', '') AS SMALLDATETIME)
						ELSE  CAST (Rasdate AS SmallDatetime) END from cnv.MDSFastOCT WHERE TableID = @TableID
						
						END TRY
						BEGIN CATCH
					 SELECT @RasDate =	CAST (0 AS SmallDatetime) 
						END CATCH
 
 
INSERT INTO dbo.MDS_EVENT_LOC   
			(
			  [FSA_MDS_EVENT_ID]
			 ,[MDS_LOC_CAT_ID]
			,[MDS_LOC_TYPE_ID]
			,[MDS_LOC_NBR]
			,[MDS_LOC_RAS_DT]
			,[CREAT_DT]
			) 
	SELECT  DISTINCT
			@FSAMDSEventID,
			CASE WHEN ISNULL (lmloc.MDS_LOC_CAT_ID,'') = '' THEN 1 ELSE lmloc.MDS_LOC_CAT_ID END,
			lmlt.MDS_LOC_TYPE_ID,
			CASE WHEN ISNULL (Mloc.Number,'') ='' THEN 'ConvertedOrder' ELSE Mloc.Number END,
		    @RasDate,----GETDATE(),--CAST (Mloc.RASDate AS SmallDateTime), ---we need to make sure this is in dateformat
			GETDATE()---Mloc.Created This needs to be added to the tmp table
		 		 
			 FROM cnv.MDSFastOCT  Mloc WITH (NOLOCK) 
INNER JOIN  dbo.LK_MDS_LOC_CAT lmloc WITH (NOLOCK) ON Mloc.Category = lmloc.MDS_LOC_CAT_DES
INNER JOIN  dbo.LK_MDS_LOC_TYPE lmlt WITH (NOLOCK) ON Mloc.LOCType  = lmlt.MDS_LOC_TYPE_DES
WHERE Mloc.TableID = @TableID
	   
 	 
  -- ================================================================================================
 	PRINT ' Step 8 Inserting to dbo.MDS_EVENT_LOC Complete'
 -- ================================================================================================
 END
 IF EXISTS
 
 (SELECT 'X' FROM  cnv.MDSFastMngdDev    WITH (NOLOCK) WHERE  TableID  = @TableID)
 
	BEGIN
 
			 
		
			INSERT INTO dbo.MDS_EVENT_MNGD_DEV
					 ( 
						  [FSA_MDS_EVENT_ID]
						  ,[FTN]
						  ,[DEV_NME]
						  ,[EQPT_DES]
						  ,[EQPT_VNDR_NME]
						  ,[MANF_ID]
						  ,[DEV_MODEL_ID]
						  ,[CREAT_DT]
						  ,[ORDR_ID])
			SELECT DISTINCT
						
						   @FSAMDSEventID,
						   md.FTN,
						   SUBSTRING (mdt.ODIEDeviceName,1,100),
						   NULL,
						   CASE WHEN ISNULL(mdt.VendorModel,'') = 'Click to edit' THEN '' ELSE SUBSTRING (mdt.VendorModel,1,50) END,
						    ldm.MANF_ID,
						   dmo.DEV_MODEL_ID,	 
						   GETDATE(),
						   fsa.ORDR_ID
						   
						 
				   FROM cnv.MDSFastMngdDev   mdt WITH (NOLOCK) 
		    INNER JOIN cnv.MDSFastTrack md with (NOLOCK) ON mdt.TableID = md.AccessTableID
		    LEFT JOIN dbo.FSA_ORDR fsa WITH (NOLOCK) ON md.FTN		= fsa.FTN
		    LEFT JOIN dbo.LK_DEV ld WITH (NOLOCK) ON mdt.Device  = ld.DEV_NME
		    INNER JOIN dbo.LK_DEV_MANF ldm WITH (NOLOCK) ON ldm.MANF_NME = 'ConvertedOrder'
		    INNER JOIN dbo.LK_DEV_MODEL dmo WITH (NOLOCK) ON dmo.DEV_MODEL_NME ='Converted Order'
		 
			WHERE mdt.TableID = @TableID
		 
  -- ================================================================================================
 	PRINT ' Step 9 Inserting to dbo.MDSManagedDevices_Tmp Complete'
 -- ================================================================================================
  END
  IF EXISTS 
 (SELECT 'X' FROM cnv.MDSFastOE WITH (NOLOCK) WHERE  TableID  = @TableID AND LTRIM(RTRIM(MNSOEType))  <> 'No OE Needed')
 
		 BEGIN
		 
				INSERT INTO dbo.MDS_EVENT_MNS_OE
							( [FSA_MDS_EVENT_ID]
							  ,[FTN]
							  ,[MNS_OE_TYPE_ID]
							  ,[MNS_OE_ID]
							  ,[MDS_SRVC_TIER_ID]
							  ,[MDS_ENTLMNT_ID]
							  ,[CREAT_DT]
							  ,[ORDR_ID])
				SELECT DISTINCT
							 @FSAMDSEventID,
							 ISNULL(md.FTN, 0),							 
							ISNULL (lmt.MNS_OE_TYPE_ID, 5),
							lme.MNS_OE_ID,
							mst.MDS_SRVC_TIER_ID,
							lmen.MDS_ENTLMNT_ID,  
							GETDATE(),
							fsa.ORDR_ID  			
												
						FROM cnv.MDSFastOE  Det WITH (NOLOCK) 
			 INNER JOIN cnv.MDSFastTrack md with (NOLOCK) ON Det.TableID = md.AccessTableID
		    LEFT JOIN dbo.FSA_ORDR fsa WITH (NOLOCK) ON md.FTN		= fsa.FTN 
			INNER JOIN dbo.LK_MNS_OE_TYPE lmt WITH (NOLOCK) ON Det.MNSOEType = lmt.MNS_OE_TYPE_DES
			INNER JOIN dbo.LK_MNS_OE lme WITH (NOLOCK)  ON det.MNSOEDescription = lme.MNS_OE_DES
			INNER JOIN dbo.LK_MDS_SRVC_TIER mst  WITH (NOLOCK) ON md.[Service Tier] = mst.MDS_SRVC_TIER_DES
		    INNER JOIN dbo.LK_MDS_ENTLMNT lmen WITH (NOLOCK) ON 
			CASE WHEN mst.MDS_SRVC_TIER_ID NOT IN (1, 2) THEN 'N/A'
				WHEN mst.MDS_SRVC_TIER_ID IN (1,2) AND     md.[Strategic Entitlements] like '%Firewall%' THEN  'FireWall'

			 ELSE ' ' END = lmen.MDS_ENTLMNT_DES
						AND mst.MDS_SRVC_TIER_ID = lmen.MDS_SRVC_TIER_ID	
						WHERE 
						det.TableID = @TableID
			END		 
		  	ELSE
			
			BEGIN
			
					 
				INSERT INTO dbo.MDS_EVENT_MNS_OE
							( [FSA_MDS_EVENT_ID]
							  ,[FTN]
							  ,[MNS_OE_TYPE_ID]
							  ,[MNS_OE_ID]
							  ,[MDS_SRVC_TIER_ID]
							  ,[MDS_ENTLMNT_ID]
							  ,[CREAT_DT]
							  ,[ORDR_ID])
			VALUES
						(	 @FSAMDSEventID,					 
							0,							 
							  5,
							0,
							NULL,
							NULL,  
							GETDATE(),
							NULL  	)		 
							
		 
		 
  -- ================================================================================================
 	PRINT ' Step 10 Inserting to  dbo.MDS_EVENT_MNS_OE Complete'
 -- ================================================================================================
    END
 IF EXISTS 
 (SELECT 'X' FROM cnv.MDSFastVirtual   WITH (NOLOCK) WHERE  TableID  = @TableID)
 
		 BEGIN
		 
				INSERT INTO dbo.MDS_EVENT_PVC 
						   ( [FSA_MDS_EVENT_ID]
							,[DLCI_ID]
						    ,[CREAT_DT]
							)
					SELECT DISTINCT
					       @FSAMDSEventID,
					       SUBSTRING(PVC,1,25),
					       GETDATE() -- Created This is needed from tmp
					  FROM cnv.MDSFastVirtual WITH (NOLOCK) WHERE TableID = @TableID

 
 -- ================================================================================================
 	PRINT ' Step 11 Inserting to  dbo.MDS_EVENT_PVC  Complete'
 -- ================================================================================================
 		END
 
  IF EXISTS 
 (SELECT 'X' FROM cnv.MDSFastWirelessTrpt  WITH (NOLOCK) WHERE  TableID  = @TableID)
	
	BEGIN
			INSERT INTO dbo.MDS_EVENT_WRLS_TRNSPRT_DEV
						(
						   [FSA_MDS_EVENT_ID]
						  ,[PRIM_BKUP_CD]
						  ,[ESN]
						  ,[CREAT_DT]
						)
			    SELECT   DISTINCT
						@FSAMDSEventID, 
						SUBSTRING([Primary],1,1),  
						SUBSTRING (ESN,1,8),
						GETDATE()		 
					FROM cnv.MDSFastWirelessTrpt  WITH (NOLOCK)
						WHERE TableID = @TableID
				
								    

 
  -- ================================================================================================
 	PRINT ' Step 12 Inserting to   dbo.MDS_EVENT_WRLS_TRNSPRT_DEV Complete'
 -- ================================================================================================
  	END
  	
  	
  	IF ( ISNULL(@MacActivity, '') <> '')
  	 
 
	
	BEGIN
			INSERT INTO [dbo].[MDS_EVENT_MAC_ACTY]
						(
						   [EVENT_ID]
						  ,[MDS_MAC_ACTY_ID]
						  ,[CREAT_DT]
						)
			    SELECT  DISTINCT
						@NewEventID, 
						lma.MDS_MAC_ACTY_ID,
						GETDATE()		 
					FROM  LK_MDS_MAC_ACTY lma WITH (NOLOCK)
				INNER JOIN 	cnv.parseStringwithIds (@MacActivity)  pst ON lma.MDS_MAC_ACTY_NME = pst.DesValue
	 
 
  -- ================================================================================================
 	PRINT ' Step 12 - 1 Inserting to   MDS_EVENT_MAC_ACTY Complete'
 -- ================================================================================================
  	END
  	
  	 IF EXISTS
 
 (SELECT 'X' FROM  cnv.MDSFastMngdDev   WITH (NOLOCK) WHERE  TableID  = @TableID)	
 BEGIN
DECLARE @REQID INT
SET @REQID = 0
			INSERT INTO [dbo].[ODIE_REQ]
						(
						   [ORDR_ID]
						  ,[ODIE_MSG_ID]
						  ,[STUS_MOD_DT]
						  ,[H1_CUST_ID]
						  ,[CREAT_DT]
						  ,[STUS_ID]
						  ,[MDS_EVENT_ID]
						  ,[TAB_SEQ_NBR]
						  ,[CUST_NME]
						)
			    SELECT  DISTINCT
			    
			    NULL,
			    2,
			    GETDATE(),
			    NULL,  -- H1
			    GETDATE(),
			    21,
			    @NewEventID,  
			    1,
			    NULL  

SELECT @REQID = SCOPE_IDENTITY()	
						 
 
  -- ================================================================================================
 	PRINT ' Step 12 - 1 Inserting to   ODIE REQ Complete'
 -- ================================================================================================
  	END
  	
 
	
	BEGIN
			INSERT INTO [dbo].[ODIE_RSPN_INFO]
						(
						 
						   [REQ_ID]
						  ,[MODEL_ID]
						  ,[DEV_ID]
						  ,[SERIAL_NO]
						  ,[RDSN_NBR]
						  ,[FAST_TRK_CD]
						  ,[DSPCH_RDY_CD]
						  ,[RSPN_INFO_DT]
						  ,[SLCTD_CD]
						  ,[CREAT_DT]
						  ,[MANF_ID]
						  ,[OPT_OUT_CD]
						  ,[OPT_OUT_REAS_TXT]
						  ,[BUS_JUSTN_TXT]
						  ,[MGT_TXT]
						  ,[ONE_MB_TXT]
						  ,[FRWL_PROD_CD]
						  ,[FSA_MDS_EVENT_ID]
						  ,[EVENT_ID]
						)
			    SELECT  DISTINCT
							
							@REQID,
							NULL,
							mdt.ODIEDeviceName,
							NULL,
							mf.[Redesign Number],
							1,
							CASE WHEN SUBSTRING (mdt.DispatchReady,1,1) = 'Y' THEN 1 ELSE 0 END,
							GETDATE(),
							1,
							GETDATE(),
							
							SUBSTRING(mdt.VendorModel, 1, CHARINDEX (' ', mdt.VendorModel)),
							
							'N',
							NULL,
							NULL,
							SUBSTRING(mdt.Management,1,200),
							SUBSTRING (mdt.MB,1,200),
							CASE WHEN mf.[Is Firewall Security Product?] = 0 THEN 'N' ELSE 'Y' END,
							NULL,--@FSAMDSEventID,
							@NewEventID
							
						  FROM cnv.MDSFastMngdDev   mdt 
						INNER JOIN cnv.MDSFastTrack  mf WITH (NOLOCK) ON mdt.TableID = mf.AccessTableID  
						  
					--LEFT JOIN  cnv.parseStringWithSpace ((mdt.VendorModel,'') = 'Click to edit' THEN '' ELSE SUBSTRING (mdt.VendorModel,1,50) END
					
						  WHERE TableID = @TableID 
						
							
  -- ================================================================================================
 	PRINT ' Step 12 - 2 Inserting to  [ODIE_RSPN_INFO] Complete'
 -- ================================================================================================
  	END
 

		IF EXISTS 
		(SELECT 'X' FROM cnv.WFHistory  WHERE [Event ID] = @EventID)
		
	    BEGIN
									
			  										
			  INSERT INTO dbo.EVENT_HIST
							 
							 ([EVENT_ID]
							 ,[ACTN_ID]
							 ,[CMNT_TXT]
							 ,[CREAT_BY_USER_ID]
							 ,[MODFD_BY_USER_ID]
							 ,[MODFD_DT]
							 ,[CREAT_DT]
							 ,[PRE_CFG_CMPLT_CD]
							 ,[FAIL_REAS_ID])
							 
		      SELECT  DISTINCT
							 @NewEventID,
							 CASE WHEN ISNULL (lka.ACTN_ID, '') ='' THEN 1 ELSE lka.ACTN_ID END,
							 wfh.[Comment],
							 CASE WHEN ISNULL (cnv.GetUserIDByName (wfh.[Created BY]), '') = '' THEN 1 ELSE
								 cnv.GetUserIDByName (wfh.[Created BY]) END,
							CASE WHEN  ISNULL(cnv.GetUserIDByName (wfh.[Modified By]), '') = '' THEN 1 ELSE
							 cnv.GetUserIDByName (wfh.[Modified By]) END,
						 	 wfh.[Modified],
							 wfh.[Created], 
							 'N' AS [Pre Config Complete ID], --- This needs clarif
							  NULL AS [FAIL_REAS_ID]	 --- This needs clarif
						  
							 FROM cnv.WFHistory  wfh 
							 --LEFT JOIN	cnv.Event_Tmp evt  WITH (NOLOCK) on wfh.[EVENT ID] = evt.EVENTID
							 LEFT JOIN	dbo.LK_ACTN lka  WITH (NOLOCK) ON wfh.[Action] = lka.ACTN_DES
							--INNER JOIN	dbo.EVENT_FAIL_ACTY efa WITH (NOLOCK) on wfh.[Fail Codes] = efa.FAIL_ACTY_ID
							 WHERE wfh.[Event ID] = @EventID
-- ================================================================================================	 	
    PRINT 'Step 14 - Complete   Inserted WF History table'
-- ================================================================================================	 	
   
				   INSERT INTO dbo.EVENT_FAIL_ACTY
							  (
							  [EVENT_HIST_ID]
							 ,[FAIL_ACTY_ID]
							 ,[REC_STUS_ID]
							 ,[CREAT_DT]
							  )		
							
					  SELECT  DISTINCT
							  ehi.EVENT_HIST_ID,
							  lfa.FAIL_ACTY_ID,
							  1 AS [REC STUS ID],
							  ehi.CREAT_DT	
							  FROM    
							  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
							  cnv.Event_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
							  cnv.WFHistory wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
							  LK_FAIL_ACTY lfa WITH (NOLOCK) ON  lfa.FAIL_ACTY_DES = wfh.[Failed Activities]
							  WHERE ehi.EVENT_ID = @EventID
					  
-- ================================================================================================	 		  
	PRINT 'Step 15 - Complete   Inserted FAIL Act table'
-- ================================================================================================	 	
			INSERT INTO dbo.EVENT_SUCSS_ACTY
					([EVENT_HIST_ID]
					,[SUCSS_ACTY_ID]
					,[REC_STUS_ID]
					,[CREAT_DT]
					)
		   SELECT  DISTINCT
					  ehi.EVENT_HIST_ID,
					  lsa.SUCSS_ACTY_ID,
					  1 AS [REC STUS ID],
					  ehi.CREAT_DT	
					  FROM    
					  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
					  cnv.Event_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
					  cnv.WFHistory  wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
					  dbo.LK_SUCSS_ACTY  lsa  WITH (NOLOCK) ON  lsa.SUCSS_ACTY_DES= wfh.[Succssful Activities]
					  WHERE ehi.EVENT_ID = @EventID
-- ================================================================================================	 		  
	PRINT 'Step 16 - Complete   Inserted SUCCESS Act table'
-- ================================================================================================	 	
   	  END
   	   
		IF  (@DescComments <> '')
		
		SET @DescComments = 'Description and Comments : ' + @DescComments
		
	    BEGIN
									
			  										
			  INSERT INTO dbo.EVENT_HIST
							 
							 ([EVENT_ID]
							 ,[ACTN_ID]
							 ,[CMNT_TXT]
							 ,[CREAT_BY_USER_ID]
							 ,[MODFD_BY_USER_ID]
							 ,[MODFD_DT]
							 ,[CREAT_DT]
							 ,[PRE_CFG_CMPLT_CD]
							 ,[FAIL_REAS_ID])
							 
		      SELECT  
							 @NewEventID,
							  1,
							 @DescComments,
							 1,
							 1,
						 	 wfh.[Modified],
							 wfh.[Created], 
							 'N' AS [Pre Config Complete ID], --- This needs clarif
							  NULL AS [FAIL_REAS_ID]	 --- This needs clarif
						  
							 FROM cnv.MDSFastTrack wfh
							 --LEFT JOIN	cnv.Event_Tmp evt  WITH (NOLOCK) on wfh.[EVENT ID] = evt.EVENTID
						 --INNER JOIN	dbo.EVENT_FAIL_ACTY efa WITH (NOLOCK) on wfh.[Fail Codes] = efa.FAIL_ACTY_ID
							 WHERE wfh.Event_ID = @EventID
		
-- ================================================================================================	 	
    PRINT 'Step 13 - Complete   Inserted WF History for comments table'
-- ================================================================================================	   
 	END
 	
 	
  	IF  (Select TOP 1 MNSOENumber from cnv.MDSFastOE where ISNUMERIC(MNSOENumber) = 1 AND TableID = @TableID) <> ''
		
	    BEGIN
	    
	    DECLARE  @OEString Varchar(100)
	    SET @OEString = ''
 
		SELECT @OEString = COALESCE(@OEString + '  ', '') +   MNSOENumber from cnv.MDSFastOE where ISNUMERIC(MNSOENumber) = 1 AND TableID = @TableID
															 
 			
		SET @OEString = 'OE Numbers are :' + @OEString

	    
									
			  										
			  INSERT INTO dbo.EVENT_HIST
							 
							 ([EVENT_ID]
							 ,[ACTN_ID]
							 ,[CMNT_TXT]
							 ,[CREAT_BY_USER_ID]
							 ,[MODFD_BY_USER_ID]
							 ,[MODFD_DT]
							 ,[CREAT_DT]
							 ,[PRE_CFG_CMPLT_CD]
							 ,[FAIL_REAS_ID])
							 
		      VALUES  
							( @NewEventID,
							  1,
							 @OEString,
							 1,
							 1,
						 	 GETDATE(),
							 GETDATE(), 
							 'N' ,  
							  NULL   )
 
		
-- ================================================================================================	 	
    PRINT 'Step 13(2) - Complete   Inserted WF History for   OE NUMBER table'
-- ================================================================================================	   
 	END
 		IF  (@DesignNumber <> '')
		
		SET @DesignNumber = 'Design Number : ' + @DesignNumber
		
	    BEGIN
									
			  										
			  INSERT INTO dbo.EVENT_HIST
							 
							 ([EVENT_ID]
							 ,[ACTN_ID]
							 ,[CMNT_TXT]
							 ,[CREAT_BY_USER_ID]
							 ,[MODFD_BY_USER_ID]
							 ,[MODFD_DT]
							 ,[CREAT_DT]
							 ,[PRE_CFG_CMPLT_CD]
							 ,[FAIL_REAS_ID])
							 
		      SELECT  
							 @NewEventID,
							  1,
							 @DesignNumber,
							 1,
							 1,
						 	 wfh.[Modified],
							 wfh.[Created], 
							 'N' AS [Pre Config Complete ID], --- This needs clarif
							  NULL AS [FAIL_REAS_ID]	 --- This needs clarif
						  
							 FROM cnv.MDSFastTrack wfh
							 --LEFT JOIN	cnv.Event_Tmp evt  WITH (NOLOCK) on wfh.[EVENT ID] = evt.EVENTID
						 --INNER JOIN	dbo.EVENT_FAIL_ACTY efa WITH (NOLOCK) on wfh.[Fail Codes] = efa.FAIL_ACTY_ID
							 WHERE wfh.Event_ID = @EventID
		
-- ================================================================================================	 	
    PRINT 'Step 13 (3)- Complete   Inserted WF History for Design Number '
-- ================================================================================================	   
 	END
   	  
   	  IF EXISTS 
   	  (SELECT 'X' FROM cnv.EventCalender WITH (NOLOCK)  WHERE EventID = @EventID)
   	  
	BEGIN
	
 
	
 	DECLARE @Id nVarchar(10)
	DECLARE @IdsoutPut nVarchar(max)
	DECLARE @IdsString nVarchar(max)
	DECLARE @outPut nVarchar(max)
	DECLARE @AssignedName nVarchar(max)
	DECLARE @STag nVarchar(100)
	DECLARE @eTag nVarchar(100)
	DECLARE @e1Tag nVarchar(100)
	DECLARE @mTag nVarchar(100)
	DECLARE @SubmitType Varchar(100)
	
	DECLARE @Counter INT
	DECLARE @Counts INT 
	
	
    SET @Id = ''
	SET @IdsoutPut = ''
	SET @IdsString = ''
	SET @outPut = ''
	SET @AssignedName = ''
	SET @STag = ''
	SET @eTag = ''
	SET @e1Tag = ''
	SET @mTag = ''
	SET @SubmitType = ''
	
	SET @Counter = 0
	SET @Counts  = 0
	
			IF OBJECT_ID(N'tempdb..#TmpAssign', N'U') IS NOT NULL
				DROP TABLE #TmpAssign
				CREATE TABLE #TmpAssign 
					( AsnUser Varchar (1000),
					  xTag Varchar(200),
					  Status INT DEFAULT 0
					)

	
	 SET @STag =  '<ResourceIds> '
	 SET @eTag =  '   </ResourceIds> '
	 SET @e1Tag =  '" /> '
	 SET @mTag =  '<ResourceId Type="System.Int32" Value="'
	
		INSERT INTO #TmpAssign (AsnUser)
		SELECT DesValue FROM cnv.parseStringwithIds ( (SELECT TOP 1 [Assigned Activators] from cnv.EventCalender Where EventID = @EventID))
 
 IF ((SELECT TOP 1 ASNUser from #TmpAssign )<> 'SOWS ES Activators')
 
    BEGIN
       SET @Counts = 0
       SET @Counter = 1
 
        Select  @Counts =  COUNT(1) from #TmpAssign WHERE STATUS = 0
        
               
        IF (@Counts  > 0)
        BEGIN
        
		 WHILE	@Counter <= @Counts
        
			BEGIN   
	SET @Id = ''
	SET @IdsoutPut = ''
	SET @IdsString = ''
	SET @outPut = ''
	SET @AssignedName = ''  
              
				SELECT TOP 1 @AssignedName =   AsnUser from #TmpAssign WHERE Status = 0
				SELECT @Id =  [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE 
				SUBSTRING(DSPL_NME, 1, CHARINDEX('[',DSPL_NME))
				 = SUBSTRING(@AssignedName, 1, CHARINDEX('[',@AssignedName))
				SET @ID = CASE WHEN ISNULL(@ID, 0) = 0 THEN 1 ELSE @ID END
			
			 SELECT @IdsoutPut = @mTag  + '' + @Id + '' + @e1Tag
                  
           update #TmpAssign set Status = 1, xTag = @IdsoutPut where AsnUser = @AssignedName
      
          SET @Counter= @Counter + 1
         END
        
        END        
  
   
		SELECT @IdsString = COALESCE(@IdsString + '  ', '') + xTag
		FROM #TmpAssign		
		SELECT @outPut =   @STag  + ''+ @IdsString + ''+ @eTag
		
		END
        ELSE 
        
        BEGIN
        
        SELECT @Id = [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE DSPL_NME = 'COWS ES Activators'
             
		SET @outPut =   @STag  + ''+ @mTag  + '' + @Id + '' + @e1Tag + ''+ @eTag
		
        END     
		SELECT @SubmitType = [Submit Type] FROM cnv.MDSFastTrack WHERE Event_ID = @EventID
		SET @SubmitType = CASE WHEN @SubmitType = 'MDS STANDARD' THEN 'MDS'  ELSE @SubmitType END
		 	
		 	INSERT INTO dbo.APPT 
							
						  ([EVENT_ID]
						  ,[STRT_TMST]
						  ,[END_TMST]
						  ,[RCURNC_DES_TXT]
						  ,[SUBJ_TXT]
						  ,[DES]
						  ,[APPT_LOC_TXT]
						  ,[APPT_TYPE_ID]
						  ,[CREAT_BY_USER_ID]
						  ,[MODFD_BY_USER_ID]
						  ,[MODFD_DT]
						  ,[CREAT_DT]
						  ,[ASN_TO_USER_ID_LIST_TXT]
						  ,[RCURNC_CD])  
 
		  		  
				SELECT  DISTINCT
							@NewEventID,
							et.[Start Time],
							et.[End Time],
							'',
							REPLACE (et.[Item Title],@EventID,@NewEventID),
							SUBSTRING (et.Description,1,1000),
							'',
							5,
							 --CASE WHEN ISNULL ([cnv].[GetAPPTTypeID](@SubmitType), '') = '' THEN 11 ELSE
			    	--		 [cnv].[GetAPPTTypeID] (@SubmitType) END,
							CASE WHEN ISNULL (cnv.GetUserIDByName (et.[Created By]),0 ) = 0 THEN 1 ELSE
							cnv.GetUserIDByName (et.[Created By]) END,							
							CASE WHEN ISNULL (cnv.GetUserIDByName (et.[Modified By]),0 ) = 0 THEN 1 ELSE
							cnv.GetUserIDByName (et.[Modified By]) END,
							et.Modified,
							et.Created,						
							@outPut,
							''
	 
							FROM cnv.EventCalender  Et WITH (NOLOCK)
							INNER JOIN cnv.MDSFastTrack ng WITH (NOLOCK) ON ng.Event_ID = Et.EventID
				 		 WHERE EventID = @EventID
 
  -- ================================================================================================	 		  
	PRINT 'Step 17 - Complete   Event Calender'
-- ================================================================================================	 
				
	END			

			
						Update  cnv.MDSFastTrack SET Status = 1 WHERE Event_ID = @EventID
								
			        	Update cnv.EVENT_Tmp set status = 2 where NewEventID =  @NewEventID and EventType = 5
			        	
			        				        	
			        	--Update dbo.MDS_EVENT_WIRED_TRNSPRT_DEV  SET MULTI_LINK_CKT_CD = 1  WHERE MULTI_LINK_CKT_CD = 'Y'
			        	--Update dbo.MDS_EVENT_WIRED_TRNSPRT_DEV  SET MULTI_LINK_CKT_CD = 0  WHERE MULTI_LINK_CKT_CD = 'N'
			 
	
	SET @Ctr = @Ctr + 1
	-- ================================================================================================		  
	PRINT 'Step 18 - Completed and Counter Incremented' 
-- ================================================================================================	
  
	END
	
	END
	
 
			 
 END TRY 
  Begin Catch
	Exec [dbo].InsertErrorInfo
	  
End Catch

  SET NOCOUNT OFF
  END
