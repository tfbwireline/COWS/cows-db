USE [COWS]
GO
_CreateObject 'SP','cnv','InsertIntoADEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

 

-- =======================================================
-- Author:	Naidu Vattigunta	
-- Create date:	06/30/2011 
-- Description:	Inserts Data into Access Delivery Table and child tables
 -- ================================================================================================
ALTER PROCEDURE [cnv].[InsertIntoADEvent]
AS
 
 
BEGIN
SET NOCOUNT ON 
 
DECLARE @EventID INT
DECLARE @EventStatusID INT
DECLARE @NewEventID INT
DECLARE @MaxID INT 
DECLARE @Cnt			INT
DECLARE @Ctr			INT
DECLARE @TableID  nvarchar(100)
DECLARE @AuthStatus BIT 
 

Begin Try  
  -- =============================================================================================
/* 
Event Types for cnv.Event_Tmp table
   1 - Ad Event  
   2 - MDS
   3 - MPLS
   4 - NGVN
   5 - Sprint Link 
 */
-- ================================================================================================

UPDATE cnv.AccessDelivery SET Status = 4 WHERE ISNUMERIC(FTN) = 0 and ISNULL(FTN,'') <> ''


 --UPDATE cnv.AccessDelivery SET [Assigned To] =  REPLACE([Assigned To], 'HCL', 'ERICSSON_HCL') WHERE [Assigned To] IN 
 --(Select [Assigned To]   from cnv.AccessDelivery where [Assigned To]   like  '%HCL Contractor for Sprint%'
 --AND [Assigned To]  NOT lIKE '%ERICSSON_HCL Contractor for Sprint%')

--------------------------------------------------
SELECT @MaxID = MAX(Event_ID) from EVENT

IF ISNULL(@MaxID, 0) = 0 
BEGIN
	SET @MaxID = 0001
	
	END
SET @MaxID = @MaxID + 1

DBCC CHECKIDENT ( [cnv.Event_Tmp], RESEED, @maxID)

 --Delete from cnv.Event_Tmp where EventType = 1 and Status = 0
  INSERT INTO cnv.Event_Tmp
 (EventID,
 EventStatus,
 EventType,
 CreateDate)
 SELECT   
 ad.Event_ID,
 [cnv].[GetEventStatusID] (ad.[Event Status]),
 1,
  ad.[Created]
 FROM cnv.AccessDelivery  ad WITH (NOLOCK)    
 WHERE Status = 0 AND ISNULL(Event_ID, '') <> ''  
 AND  NOT EXISTS 
		(SELECT 'X' FROM  cnv.EVENT_Tmp et WITH (NOLOCK) WHERE 
		ad.Event_ID = et.EventID AND Status = 0 AND EventType = 1) Order by Event_ID asc
-- ================================================================================================	 
		SET @Cnt = 0
		SET @Ctr = 1
		SELECT 	@Cnt	=	Count(1) FROM  cnv.Event_Tmp Where Status = 0 and EventType =1
		
		
		
		IF @Cnt	>	0
		BEGIN
			WHILE	@Ctr <= @Cnt
				BEGIN
				
 
						 SELECT  TOP 1  @EventID  = ev.EventID,
										@NewEventID = ev.NewEventID,
										@TableID  = adt.AccessTableID,
										@AuthStatus = CASE WHEN adt.Auth_Status = 'AUTHORISED' AND ISNULL(adt.FTN,0 ) <> 0 THEN 1 ELSE 0 END,
										@EventStatusID = ev.EventStatus
								 FROM cnv.Event_Tmp ev WITH (NOLOCK)  
				 INNER JOIN  cnv.AccessDelivery  adt WITH (NOLOCK) ON ev.EventID = adt.Event_ID
								 WHERE ev.EventType = 1 and ev.Status = 0
	


-- ================================================================================================	
-- ================================================================================================	
 
  
   	 IF NOT EXISTS
		(SELECT 'X' FROM dbo.EVENT WITH (NOLOCK) WHERE EVENT_ID  = @NewEventID)
	BEGIN
 
			SET IDENTITY_INSERT DBO.EVENT ON
 
			  INSERT INTO dbo.EVENT
						 ( EVENT_ID,
						  EVENT_TYPE_ID,
						  CREAT_DT) 
				  SELECT 
						  @NewEventID,
						 EventType,
						  CreateDate
				  FROM cnv.Event_Tmp WHERE EventID = @EventID AND EventType = 1 AND Status = 0 

  
		SET IDENTITY_INSERT DBO.EVENT OFF
	
	
   END 
    
 
-- ================================================================================================	 

	PRINT @EventID
	PRINT @NewEventID 
	PRINT 'Step 1 - Complete Inserted into Event table'
 
-- ================================================================================================
	 
  
   IF NOT EXISTS
		(SELECT 'X' FROM dbo.AD_EVENT WITH (NOLOCK) WHERE EVENT_ID = @NewEventID)
  
   BEGIN
 
    ALTER TABLE [dbo].[AD_EVENT] NOCHECK CONSTRAINT ALL 
 
  
	INSERT INTO	 dbo.AD_EVENT
			  ([EVENT_ID]
			  ,[EVENT_STUS_ID]
			  ,[FTN]
			  ,[CHARS_ID]
			  ,[H1]
			  ,[H6]
			  ,[CUST_NME]
			  ,[CUST_CNTCT_NME]
			  ,[CUST_CNTCT_PHN_NBR]
			  ,[CUST_EMAIL_ADR]
			  ,[CUST_CNTCT_CELL_PHN_NBR]
			  ,[CUST_CNTCT_PGR_NBR]
			  ,[CUST_CNTCT_PGR_PIN_NBR]
			  ,[ENHNC_SRVC_ID]
			  ,[REQOR_USER_ID]
			  ,[SALS_USER_ID]
			  ,[PUB_EMAIL_CC_TXT]
			  ,[CMPLTD_EMAIL_CC_TXT]
			  ,[DES_CMNT_TXT]
			  ,[CPE_ATND_CD]
			  ,[DOC_LINK_TXT]
			  ,[ESCL_CD]
			  ,[PRIM_REQ_DT]
			  ,[SCNDY_REQ_DT]
			  ,[ESCL_REAS_ID]
			  ,[STRT_TMST]
			  ,[EXTRA_DRTN_TME_AMT]
			  ,[END_TMST]
			  ,[WRKFLW_STUS_ID]
			  ,[REC_STUS_ID]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_BY_USER_ID]
			  ,[MODFD_DT]
			  ,[CREAT_DT]
			  ,[EVENT_TITLE_TXT]
			  ,[IP_VER_ID]
			  ,[CNFRC_BRDG_NBR]
			  ,[CNFRC_PIN_NBR]
			  ,[EVENT_DRTN_IN_MIN_QTY]
			  ,[SOWS_EVENT_ID]) 
			  
			 
			  
		 
      SELECT TOP 1

				@NewEventID,
				@EventStatusID,
				CASE WHEN ISNULL (adt.[FTN],'') = '' THEN -1 ELSE adt.[FTN] END,
				LTRIM(RTRIM(adt.[CHARSID])),
				SUBSTRING(LTRIM(RTRIM(adt.[H1])),1,9),
				SUBSTRING(LTRIM(RTRIM(adt.[H6])),1,9),
				CASE WHEN @AuthStatus = 1 THEN 'Unknown' ELSE  SUBSTRING( LTRIM(RTRIM(adt.[Customer Name])),1,100) END,
				CASE WHEN @AuthStatus = 1 THEN 'Unknown' ELSE SUBSTRING (LTRIM(RTRIM(adt.[Customer Contact Name])),1,100) END,
				CASE WHEN @AuthStatus = 1 THEN '123456' ELSE LTRIM(RTRIM(adt.[Customer Contact Phone]))END,
				CASE WHEN @AuthStatus = 1 THEN 'Private@sprint.com' ELSE SUBSTRING (LTRIM(RTRIM(adt.[Customer Email])),1,99)END,
				CASE WHEN @AuthStatus = 1 THEN '123456' ELSE LTRIM(RTRIM(adt.[Customer Contact Cell Phone])) END,
				LTRIM(RTRIM(adt.[Customer Contact Pager])),
				LTRIM(RTRIM(adt.[Customer Contact Pager PIN])),
			    CASE WHEN ISNULL (cnv.GetEnhanceServiceID (adt.[Submit Type]), '') = '' THEN 11 ELSE
			    			 [cnv].[GetEnhanceServiceID] (adt.[Submit Type]) END,
				CASE WHEN ISNULL (cnv.GetUserIDByName (adt.[Request Contact Name]), '') = '' THEN 1 ELSE
							 cnv.GetUserIDByName (adt.[Request Contact Name]) END,
				CASE WHEN  ISNULL(cnv.GetUserIDByName (adt.[Sales Engineer (SE) Name]), '') = '' THEN 1 ELSE
							cnv.GetUserIDByName (adt.[Sales Engineer (SE) Name]) END,
				SUBSTRING(LTRIM(RTRIM(adt.[Published E-Mail CC:])),1,200),
				SUBSTRING( LTRIM(RTRIM(adt.[Completed E-mail CC:])),1,200),
			    LTRIM(RTRIM(adt.[Description and Comments])), 
				LTRIM(RTRIM(adt.[IS CPE Attending?])),
				LTRIM(RTRIM(adt.[Links (Documents / Cut Sheets)])),
				LTRIM(RTRIM(adt.[Is Escalation])),
				LTRIM(RTRIM(adt.[Primary Request Date])),
				LTRIM(RTRIM(adt.[Secondary Request Date])),
				cnv.GetEscalationReasonID (adt.[Escalation Reason]),
				LTRIM(RTRIM(adt.[Start Time])),
				LTRIM(RTRIM(adt.[Extra Duration])),
				LTRIM(RTRIM(adt.[End Time])),
				cnv.GetWorkFlowStatusID (adt.[Workflow Status]),
				1, -- Record Status from  LK_REC_STUS
				CASE WHEN ISNULL(cnv.GetUserIDByName (adt.[Created BY]), '') = '' THEN 1 ELSE 
								cnv.GetUserIDByName (adt.[Created BY]) END,
				CASE WHEN ISNULL (cnv.GetUserIDByName (adt.[Modified By]), '') ='' THEN 1 ELSE 
								 cnv.GetUserIDByName (adt.[Modified By]) END,
				LTRIM(RTRIM([Modified])),
				LTRIM(RTRIM([Created])),
				LTRIM(RTRIM(adt.[Item Title])), 
				'0' AS IPVER,
				LTRIM(RTRIM(adt.[Bridge Number])),
				LTRIM(RTRIM(adt.[Bridge Pin])),
				LTRIM(RTRIM(adt.[Event Duration])),
				evt.EventID
				
				
				FROM cnv.AccessDelivery  adt WITH (NOLOCK) INNER JOIN
				cnv.Event_Tmp  evt WITH (NOLOCK) ON adt.Event_ID = evt.EventId
				WHERE evt.NewEventID = @NewEventID  
	 
		 ALTER TABLE [dbo].[AD_EVENT] CHECK CONSTRAINT ALL 
		 
	END  
 
 
 
-- ================================================================================================	  
 PRINT 'Step 2 - Complete   Inserted AD Event table'
-- ================================================================================================
			   
	 IF EXISTS 
	(SELECT 'X' FROM cnv.ADAccessTable  WITH (NOLOCK) WHERE TableID = @TableID)
			
		BEGIN 
			ALTER TABLE [dbo].[AD_EVENT_ACCS_TAG] NOCHECK CONSTRAINT ALL 
			INSERT INTO dbo.AD_EVENT_ACCS_TAG 
							(EVENT_ID,
							CKT_ID,
							CREAT_DT
						    )  
						    
						 
							SELECT  DISTINCT  @NewEventID,
							SUBSTRING (addt.[Circuit], 1,9),
							GETDATE()--addt.[Created]
						FROM	cnv.ADAccessTable  addt WITH (NOLOCK) 
						WHERE 	addt.TableID = @TableID	  
						  --and not exists (select 'x' from dbo.AD_EVENT_ACCS_TAG at with (nolock) where at.EVENT_ID = @NewEventID and addt.Circuit = at.CKT_ID)  

						 
						  
			ALTER TABLE [dbo].[AD_EVENT_ACCS_TAG]  CHECK CONSTRAINT ALL 	
	 	END			

-- ================================================================================================	 	  
 PRINT 'Step 3 - Complete   Inserted AD Access table'
-- ================================================================================================	
	 DECLARE @AssignID Varchar(100)
	 DECLARE @AuID Int
	 
	SELECT @AssignID  =  [Assigned To]  FROM cnv.AccessDelivery WHERE Event_ID = @EventID
	IF ( ISNULL (@AssignID,'') <> '') 
	
	BEGIN 	 
  
	
	  INSERT INTO 	dbo.EVENT_ASN_TO_USER		   
						([EVENT_ID]
					  ,[ASN_TO_USER_ID]
					  ,[CREAT_DT]
					  ,[REC_STUS_ID]) 
					  
				SELECT 
						@NewEventID,
						lu.[USER_ID],					 
						CASE WHEN ISNULL(ne.Created,'') ='' THEN GETDATE() ELSE ne.Created END,
						1
					 
					 FROM  dbo.LK_USER   lu WITH (NOLOCK)  
				     INNER JOIN cnv.parseStringwithIds (@AssignID) pst ON 
									 SUBSTRING(lu.DSPL_NME, 1, CHARINDEX('[',lu.DSPL_NME))
								  =  SUBSTRING(pst.DesValue, 1, CHARINDEX('[',pst.DesValue)) 
					INNER JOIN cnv.AccessDelivery  ne WITH (NOLOCK) ON ne.Event_ID = @EventID
					 AND NOT EXISTS
						(SELECT 'X'  FROM dbo.EVENT_ASN_TO_USER  ea WITH (NOLOCK)
						INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON ea.ASN_TO_USER_ID = lu.[USER_ID]
						WHERE 
						ea.EVENT_ID = @NewEventID AND ea.ASN_TO_USER_ID = CASE WHEN ISNULL (lu.[USER_ID],'') = '' THEN 1 ELSE lu.[USER_ID] END )
					
 
	 	END		
 

-- ================================================================================================	 	  
 PRINT 'Step 4 - Complete   Assigned User table'
-- ================================================================================================	  
		IF EXISTS 
		(SELECT 'X' FROM cnv.WFHistory  WHERE [Event ID] = @EventID)
		
	    BEGIN
									
			  										
			  INSERT INTO dbo.EVENT_HIST
							 
							 ([EVENT_ID]
							 ,[ACTN_ID]
							 ,[CMNT_TXT]
							 ,[CREAT_BY_USER_ID]
							 ,[MODFD_BY_USER_ID]
							 ,[MODFD_DT]
							 ,[CREAT_DT]
							 ,[PRE_CFG_CMPLT_CD]
							 ,[FAIL_REAS_ID])
							 
		      SELECT  
							 @NewEventID,
							 CASE WHEN ISNULL (lka.ACTN_ID, '') ='' THEN 1 ELSE lka.ACTN_ID END,
							 wfh.[Comment],
							 CASE WHEN ISNULL (cnv.GetUserIDByName (wfh.[Created BY]), '') = '' THEN 1 ELSE
								 cnv.GetUserIDByName (wfh.[Created BY]) END,
							CASE WHEN  ISNULL(cnv.GetUserIDByName (wfh.[Modified By]), '') = '' THEN 1 ELSE
							 cnv.GetUserIDByName (wfh.[Modified By]) END,
						 	 wfh.[Modified],
							 wfh.[Created], 
							 'N' AS [Pre Config Complete ID], --- This needs clarif
							  NULL AS [FAIL_REAS_ID]	 --- This needs clarif
						  
							 FROM cnv.WFHistory  wfh 
							 --LEFT JOIN	cnv.Event_Tmp evt  WITH (NOLOCK) on wfh.[EVENT ID] = evt.EVENTID
							 LEFT JOIN	dbo.LK_ACTN lka  WITH (NOLOCK) ON wfh.[Action] = lka.ACTN_DES
							--INNER JOIN	dbo.EVENT_FAIL_ACTY efa WITH (NOLOCK) on wfh.[Fail Codes] = efa.FAIL_ACTY_ID
							 WHERE wfh.[Event ID] = @EventID
-- ================================================================================================	 	
    PRINT 'Step 4 - Complete   Inserted WF History table'
-- ================================================================================================	 	
   
				   INSERT INTO dbo.EVENT_FAIL_ACTY
							  (
							  [EVENT_HIST_ID]
							 ,[FAIL_ACTY_ID]
							 ,[REC_STUS_ID]
							 ,[CREAT_DT]
							  )		
							
					  SELECT  
							  ehi.EVENT_HIST_ID,
							  lfa.FAIL_ACTY_ID,
							  1 AS [REC STUS ID],
							  ehi.CREAT_DT	
							  FROM    
							  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
							  cnv.Event_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
							  cnv.WFHistory wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
							  LK_FAIL_ACTY lfa WITH (NOLOCK) ON  lfa.FAIL_ACTY_DES = wfh.[Failed Activities]
							  WHERE ehi.EVENT_ID = @EventID
					  
-- ================================================================================================	 		  
	PRINT 'Step 5 - Complete   Inserted FAIL Act table'
-- ================================================================================================	 	
			INSERT INTO dbo.EVENT_SUCSS_ACTY
					([EVENT_HIST_ID]
					,[SUCSS_ACTY_ID]
					,[REC_STUS_ID]
					,[CREAT_DT]
					)
		   SELECT  
					  ehi.EVENT_HIST_ID,
					  lsa.SUCSS_ACTY_ID,
					  1 AS [REC STUS ID],
					  ehi.CREAT_DT	
					  FROM    
					  dbo.EVENT_HIST ehi WITH (NOLOCK)  INNER JOIN
					  cnv.Event_Tmp evt WITH (NOLOCK) ON evt.NewEventID = ehi.EVENT_ID INNER JOIN
					  cnv.WFHistory  wfh WITH (NOLOCK) ON wfh.[event ID] = evt.EventID INNER JOIN
					  dbo.LK_SUCSS_ACTY  lsa  WITH (NOLOCK) ON  lsa.SUCSS_ACTY_DES= wfh.[Succssful Activities]
					  WHERE ehi.EVENT_ID = @EventID
-- ================================================================================================	 		  
	PRINT 'Step 6 - Complete   Inserted SUCCESS Act table'
-- ================================================================================================	 	
   	  END
 
   	  
   	  IF EXISTS 
   	  (SELECT 'X' FROM cnv.EventCalender WITH (NOLOCK)  WHERE EventID = @EventID)
   	  
	BEGIN
	
 	DECLARE @Id nVarchar(10)
	DECLARE @IdsoutPut nVarchar(max)
	DECLARE @IdsString nVarchar(max)
	DECLARE @outPut nVarchar(max)
	DECLARE @AssignedName nVarchar(max)
	DECLARE @STag nVarchar(100)
	DECLARE @eTag nVarchar(100)
	DECLARE @e1Tag nVarchar(100)
	DECLARE @mTag nVarchar(100)
	DECLARE @SubmitType Varchar(100)
	
	DECLARE @Counter INT
	DECLARE @Counts INT 
	
		
    SET @Id = ''
	SET @IdsoutPut = ''
	SET @IdsString = ''
	SET @outPut = ''
	SET @AssignedName = ''
	SET @STag = ''
	SET @eTag = ''
	SET @e1Tag = ''
	SET @mTag = ''
	SET @SubmitType = ''
	
	SET @Counter = 0
	SET @Counts  = 0
	
	IF OBJECT_ID(N'tempdb..#TmpAssignAD', N'U') IS NOT NULL
				DROP TABLE #TmpAssignAD
				CREATE TABLE #TmpAssignAD 
					( AsnUser Varchar (1000),
					  xTag Varchar(200),
					  Status INT DEFAULT 0
					)

	
	 SET @STag =  '<ResourceIds> '
	 SET @eTag =  '   </ResourceIds> '
	 SET @e1Tag =  '" /> '
	 SET @mTag =  '<ResourceId Type="System.Int32" Value="'
	
		INSERT INTO #TmpAssignAD (AsnUser)
		SELECT DesValue FROM cnv.parseStringwithIds ( (SELECT TOP 1 [Assigned Activators] from cnv.EventCalender Where EventID = @EventID))
 
  IF ((SELECT TOP 1 ASNUser from #TmpAssignAD )<> 'SOWS ES Activators')
 
    BEGIN
       SET @Counts = 0
       SET @Counter = 1
 
        Select  @Counts =  COUNT(1) from #TmpAssignAD WHERE STATUS = 0
        IF (@Counts  > 0)
        BEGIN
        
		 WHILE	@Counter <= @Counts
        
			BEGIN     
			
    SET @Id = ''
	SET @IdsoutPut = ''
	SET @IdsString = ''
	SET @outPut = ''
	SET @AssignedName = ''
              
				SELECT TOP 1 @AssignedName =   AsnUser from #TmpAssignAD WHERE Status = 0
				SELECT @Id =  [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE 
				SUBSTRING(DSPL_NME, 1, CHARINDEX('[',DSPL_NME))
				 = SUBSTRING(@AssignedName, 1, CHARINDEX('[',@AssignedName))
				SET @ID = CASE WHEN ISNULL(@ID, 0) = 0 THEN 1 ELSE @ID END
			
			 SELECT @IdsoutPut = @mTag  + '' + @Id + '' + @e1Tag
                  
           update #TmpAssignAD set Status = 1, xTag = @IdsoutPut where AsnUser = @AssignedName
      
          SET @Counter= @Counter + 1
         END
        
        END       
   
		SELECT @IdsString = COALESCE(@IdsString + '  ', '') + xTag
		FROM #TmpAssignAD		
		SELECT @outPut =   @STag  + ''+ @IdsString + ''+ @eTag
				END
        ELSE 
        
        BEGIN
        
        SELECT @Id = [USER_ID] FROM LK_USER WITH (NOLOCK) WHERE DSPL_NME = 'COWS ES Activators'
             
		SET @outPut =   @STag  + ''+ @mTag  + '' + @Id + '' + @e1Tag + ''+ @eTag
		
        END   
		SELECT @SubmitType = [Submit Type] FROM cnv.AccessDelivery WHERE Event_ID = @EventID
 
		 	
		 	INSERT INTO dbo.APPT 
							
						  ([EVENT_ID]
						  ,[STRT_TMST]
						  ,[END_TMST]
						  ,[RCURNC_DES_TXT]
						  ,[SUBJ_TXT]
						  ,[DES]
						  ,[APPT_LOC_TXT]
						  ,[APPT_TYPE_ID]
						  ,[CREAT_BY_USER_ID]
						  ,[MODFD_BY_USER_ID]
						  ,[MODFD_DT]
						  ,[CREAT_DT]
						  ,[ASN_TO_USER_ID_LIST_TXT]
						  ,[RCURNC_CD])  
 
		  		  
				SELECT 
							@NewEventID,
							et.[Start Time],
							et.[End Time],
							'',
							REPLACE (et.[Item Title],@EventID,@NewEventID),
							et.Description,
							'',
							 CASE WHEN ISNULL ([cnv].[GetAPPTTypeID](@SubmitType), '') = '' THEN 11 ELSE
			    			 [cnv].[GetAPPTTypeID] (@SubmitType) END,
							CASE WHEN ISNULL(cnv.GetUserIDByName (et.[Created By]),0 ) = 0 THEN 1  ELSE
							cnv.GetUserIDByName (et.[Created By])END,
							CASE WHEN ISNULL (cnv.GetUserIDByName (et.[Modified By]),0 ) = 0 THEN 1 
							ELSE cnv.GetUserIDByName (et.[Modified By]) END,
							et.Modified,
							et.Created,						
							@outPut,
							''
	 
							FROM cnv.EventCalender  Et WITH (NOLOCK)
							INNER JOIN cnv.AccessDelivery at  WITH (NOLOCK) ON at.Event_ID = Et.EventID
				 		 WHERE EventID = @EventID
 	  
   	  
  -- ================================================================================================	 		  
	PRINT 'Step 7 - Complete   Event Calender'
-- ================================================================================================	 
				
	END			
						UPDATE cnv.AccessDelivery  SET Status = 1 WHERE Event_ID = @EventID
						--Update dbo.AccessDelivery_Tmp SET Status = 1 WHERE Event_ID = @CEvenID
				       	UPDATE cnv.Event_Tmp SET status = 2  where EventID = @EventID 
			 	SET @Ctr = @Ctr + 1
-- ================================================================================================		  
	PRINT 'Step 8 - Completed and Counter Incremented'
-- ================================================================================================	
  
 		  END 
    	END
    	
    	ELSE 
    	
    	PRINT ' NO Events to Insert'
		END TRY 
 BEGIN CATCH 
	EXEC [dbo].InsertErrorInfo	  
 END CATCH

  SET NOCOUNT OFF
  END
