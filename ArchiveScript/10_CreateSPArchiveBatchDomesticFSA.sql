USE [COWS]
GO

/****** Object:  StoredProcedure [tmp].[DELETEBatchEvents]    Script Date: 12/09/2014 13:51:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---- =============================================
---- Author:		Md. Monir
---- Create date:   12/06/2017
---- Description:	This SP is used to Multiple Order and delete Base on the OrdrID  secured code 1
---- Description:	removed   SET @TOk=1
---- =============================================
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[arch].[DELETEBatcEventOrderDomesticFSA]') AND type in (N'P', N'PC'))
DROP PROCEDURE [arch].[DELETEBatcEventOrderDomesticFSA]
GO
CREATE PROCEDURE [arch].[DELETEBatcEventOrderDomesticFSA](
	@Date   DateTime
)
AS
/* 
[arch].[spCOWS_Archive]  Will be called by passing either ODER_ID or EVENT_ID and will delete 1 by 1
Calling  PROCEDURE [arch].[spCOWS_Archive]  @Mood Must Reflect this:
------------------------------------------------------------------------------------------------
@Mood=???
		@Mood=0=ORDR Only No Eevents Table  , @Event_type_ID = 1=AD, @Event_type_ID = 2=NGVN 
		@Event_type_ID = 3=MPLS, @Event_type_ID = 4= SPLK 
		@Event_type_ID = 5=MDS tables  and both ORDR Table As well, 
		@Mood = 6 = MDS EVENTS only Tables  No Order no other events also. 
------------------------------------------------------------------------------------------------
*/
BEGIN
		SET NOCOUNT ON;
		SET	DEADLOCK_PRIORITY	10;
		BEGIN TRANSACTION;
			BEGIN TRY
				
				
				DECLARE		@CurID INT,@MaxID INT, @LOOP INT,@MaxID1 INT, @LOOP1 INT, @Mood INT, @FKMood int
				DECLARE		@RefNumber	int
				declare		@TOk int
				
				/*
				CREATE    TABLE [arch].[DeletedOrderEvent]   (
				ID			INT IDENTITY(1,1),
				ORDR_ID		VARCHAR(MAX)	,
				EVENT_ID	VARCHAR(MAX),
				Processed   VARCHAR(1),
				ProcessedTIME dateTime,
				Mood INT	)
				*/
				
				DECLARE     @Order   TABLE(
				ID			INT IDENTITY(1,1),
				ORDR_ID		Int	,
				Mood		int default(0),
				Processed   int default(0) )
				DECLARE     @Event   TABLE(
				ID			INT IDENTITY(1,1),
				EVENT_ID	Int,
				Mood		int default(0),
				Processed   int default(0)	)
				------------------------------------------------------------------------------------------
				------------------------------------------------------------------------------------------
				--This select is to capture all mds events and related orders, since it is many-many relation, 
				--have 4 conditions in where clause to make sure there is no overlap

				declare @OrderTable table (id int identity(1,1), orderid int, eventid int, flag bit)
				declare @orderids varchar(max)
				declare @eventids varchar(max)
				declare @orderid int
				 
				set @orderids = ''
				set @eventids = ''
				set @FKMood   = 0
				SET @CurID= 0
				INSERT INTO @OrderTable
				SELECT distinct od.ORDR_ID, ISNULL(ev.EVENT_ID,0) ,0        /*-------------------1-------------------*/
				FROM dbo.[ORDR] od WITH (NOLOCK) LEFT JOIN
					dbo.FSA_MDS_EVENT_ORDR fme WITH (NOLOCK) ON od.ORDR_ID = fme.ORDR_ID LEFT JOIN
					dbo.ACT_TASK at WITH (NOLOCK) ON at.ORDR_ID = od.ORDR_ID LEFT JOIN
					dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID = fme.EVENT_ID LEFT JOIN
					dbo.EVENT_HIST eh WITH (NOLOCK) ON eh.EVENT_ID = ev.EVENT_ID
				WHERE 
					od.DMSTC_CD=0 
					AND od.ORDR_CAT_ID IN (2,6)  
					AND Od.CREAT_DT <=@Date
					AND ISNULL(ev.CREAT_DT,@Date) <=@Date
					/*AND od.SCURD_CD=1*/  /*Can not use this   this is for secured customer*/
					
					--events with pending orders
					and not exists (select 'x' from dbo.FSA_MDS_EVENT_ORDR fme with (nolock) inner join
                           dbo.act_task act with (nolock) on act.ordr_id=fme.ordr_id
                           where act.task_id <> 1001 and act.stus_id=0 and fme.CMPLTD_CD=1 and fme.ordr_id=Od.ordr_id)
					and not exists (select 'x' from dbo.event_cpe_dev ecd with (nolock)
                           inner join dbo.fsa_ordr fo with (nolock) on fo.FTN=ecd.CPE_ORDR_NBR inner join
                           dbo.act_task act with (nolock) on act.ordr_id=fo.ordr_id
                           where act.task_id <> 1001 and act.stus_id=0 and ecd.REC_STUS_ID=1 and fo.ordr_id=Od.ordr_id)
					and not exists (select 'x' from dbo.EVENT_DEV_SRVC_MGMT eds with (nolock)
                           inner join dbo.fsa_ordr fo with (nolock) on fo.FTN=eds.MNS_ORDR_NBR inner join
                           dbo.act_task act with (nolock) on act.ordr_id=fo.ordr_id
                           where act.task_id <> 1001 and act.stus_id=0 and eds.REC_STUS_ID=1 and fo.ordr_id=Od.ordr_id)
					and not exists (select 'x' from dbo.MDS_EVENT_PORT_BNDWD mep with (nolock)
                           inner join dbo.fsa_ordr fo with (nolock) on fo.FTN=mep.M5_ORDR_NBR inner join
                           dbo.act_task act with (nolock) on act.ordr_id=fo.ordr_id
                           where act.task_id <> 1001 and act.stus_id=0 and mep.REC_STUS_ID=1 and fo.ordr_id=Od.ordr_id)
					
				ORDER BY od.ORDR_ID ASC


				IF (select COUNT(1) from @OrderTable)>0
				BEGIN
					EXEC [arch].[spEnableDisableFK] 2   --0 before
					Set @FKMood=1
				END
				 
				while ((SELECT COUNT(1) FROM @OrderTable WHERE flag=0) > 0)
				begin
					   select top 1 @orderid = orderid
					   from @OrderTable
					   where flag=0
					   order by id
				 
					   while (((@eventids = '') AND (@orderids = '')) OR (((select count(1) from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
									 < (select count(distinct eventid) from @OrderTable where orderid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))))
									 OR ((select count(1) from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
									 < (select count(distinct orderid) from @OrderTable where eventid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))))))
					   begin
							  --print 'in '+ @eventids
							  --print 'in '+ @orderids
							  if ((@eventids = '') AND (@orderids = ''))
							  begin
									 --print 'in-1'
									 select @eventids = CASE @eventids WHEN '' THEN convert(varchar,eventid) ELSE COALESCE(@eventids + ', ' + convert(varchar,eventid), convert(varchar,eventid)) END
									 from @OrderTable
									 where orderid = @orderid
				 
									 set @orderids = convert(varchar,@orderid)
							  end
							  else if ((select count(1) from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
									 < (select count(distinct eventid) from @OrderTable where orderid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))))
							  begin
							  --print 'in-2'
									 select @eventids = COALESCE(@eventids + ', ' + convert(varchar,eventid), convert(varchar,eventid))
									 from @OrderTable
									 where orderid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
									 and eventid not in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
							  end
							  else if ((select count(1) from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
									 < (select count(distinct orderid) from @OrderTable where eventid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))))
							  begin
							  --print 'in-3'
									 select @orderids = COALESCE(@orderids + ', ' + convert(varchar,orderid), convert(varchar,orderid))
									 from @OrderTable
									 where eventid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
									 and orderid not in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
							  end
					   end
				 
					   print '----------'
					   print 'orderids - ' + @orderids
					   print 'eventids - ' + @eventids
					   /*
					   Call SP here
					   select * FROM web.ParseCommaSeparatedIntegers('62421, 64264, 68102, 69544',',') 
					   SP will use this to bring back the comma seperated value to Table form
					   */
					    RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
						EXEC @TOk=[tmp].[CheckDeleteTime]     /*  Check Allowed Delete Time From this table Copy setup from CAPTAIN*/
						--SET @TOk=1   /* Temporary Code Remove when Time setup done For COWS */
						IF(@TOk>0)
						BEGIN
						   SET @CurID=@CurID+1
						   --RAISERROR ('TO BE DELETED ORDR_ID & EVENT_ID: ', 0, 1) WITH NOWAIT
						   RAISERROR ('orderids -  %s.  Will be ARCHIVED Now', 0, 1,@orderids) WITH NOWAIT 
						   RAISERROR ('eventids -  %s.  Will be ARCHIVED Now', 0, 1,@eventids) WITH NOWAIT 
						   RAISERROR ('Calling [arch].[spCOWS_Archive]  %d.  times', 0, 1,@CurID) WITH NOWAIT 
						   EXEC  [arch].[spCOWS_Archive] @orderids,@eventids, 5,@Date  /* @Mood=5=MDS=ORDR & EVENTS*/
						   INSERT INTO [arch].[DeletedOrderEvent] (ORDR_ID,EVENT_ID,Processed,ProcessedTIME,Mood)
					       SELECT @orderids ,@eventids,'Y',GETDATE(),5
						   --RAISERROR ('DELETED ORDR_ID & EVENT_ID: ', 0, 1) WITH NOWAIT
						   RAISERROR ('orderids -  %s. Has Been ARCHIVED', 0, 1,@orderids) WITH NOWAIT 
						   RAISERROR ('eventids -  %s. Has Been ARCHIVED', 0, 1,@eventids) WITH NOWAIT 
						   --print '----------'
					   END
					   ELSE
					   BEGIN
						   RAISERROR ('TIME OVER CANT DO ARCHIVE', 0, 1) WITH NOWAIT
						   BREAK   /*While Loop*/
					   END
					   update @OrderTable set flag=1 where orderid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
					   update @OrderTable set flag=1 where eventid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
					   set @orderids = ''
					   set @eventids = ''
					   set @orderid = -1

				END
				RAISERROR ( '*********************************************************************************', 0, 1) WITH NOWAIT
				if(@FKMood=1)
				BEGIN
					EXEC [arch].[spEnableDisableFK] 3 --Before 1
					SET @FKMood=0
				END
				RAISERROR ('Processed Events/Orders', 0, 1) WITH NOWAIT
				--Select * from [arch].[DeletedOrderEvent] WHERE cast(ProcessedTIME as date) =cast(GETDATE() as date)
				Select * from [arch].[DeletedOrderEvent] WHERE cast(ProcessedTIME as date) =cast(GETDATE() as date) and Action IS NULL ORDER BY ProcessedTIME DESC
		END TRY
		BEGIN CATCH
			IF (XACT_STATE()) <> 0 
			BEGIN
				ROLLBACK TRANSACTION;
				RAISERROR ('Error: Transaction RollBack No Operation Perform', 0, 1) WITH NOWAIT
				RAISERROR ('Check: Select * FROM dbo.SQL_ERROR WITH (NOLOCK)  order by SQL_Error_ID DESC', 0, 1) WITH NOWAIT
			END
			SET DEADLOCK_PRIORITY -5; 
			EXEC [dbo].[insertErrorInfo]
		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;

END



GO



