USE [COWS]
GO

/****** Object:  Table [arch].[DeletedOrderEvent]    Script Date: 02/26/2015 6:22:23 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[arch].[DeletedOrderEvent]') AND type in (N'U'))
DROP TABLE [arch].[DeletedOrderEvent]
GO
CREATE TABLE [arch].[DeletedOrderEvent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ORDR_ID] [varchar](max) NULL,
	[EVENT_ID] [varchar](max) NULL,
	[Processed] [varchar](1) NULL,
	[ProcessedTIME] [datetime] NULL,
	[Mood] [int] NULL,
	[Action]  [varchar](300) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


