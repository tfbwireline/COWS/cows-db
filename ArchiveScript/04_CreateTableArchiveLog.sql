USE [COWS]
GO

/****** Object:  Table [arch].[ArchiveLog]    Script Date: 02/26/2015 6:21:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING OFF
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[arch].[ArchiveLog]') AND type in (N'U'))
DROP TABLE [arch].[ArchiveLog]
GO
CREATE TABLE [arch].[ArchiveLog](
	[TableName] [varchar](200) NULL,
	[NumberofRows] [int] NULL,
	[CREAT_DT] [smalldatetime] NULL
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


