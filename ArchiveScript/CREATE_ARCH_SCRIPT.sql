if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_AVAL_USER' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_AVAL_USER]
   Print 'Delete Table [arch].[EVENT_AVAL_USER]'
END
GO

CREATE TABLE [arch].[EVENT_AVAL_USER](
	[EVENT_AVAL_USER_ID] [int] NOT NULL,
	[TME_SLOT_ID] [tinyint] NOT NULL,
	[USER_ID] [int] NOT NULL,
	[TME_SLOT_STRT_TME] [time](7) NOT NULL,
	[TME_SLOT_END_TME] [time](7) NOT NULL,
	[TME_SLOT_DRTN_IN_MIN_QTY] [int] NOT NULL,
	[DEL_CD] [bit] NOT NULL,
	[CREAT_DT] [datetime] NOT NULL
)

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_DEV_CMPLT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_DEV_CMPLT]
   Print 'Delete Table [arch].[EVENT_DEV_CMPLT]'
END
GO


CREATE TABLE [arch].[EVENT_DEV_CMPLT](
	[EVENT_DEV_CMPLT_ID] [int] NOT NULL,
	[EVENT_ID] [int] NOT NULL,
	[ODIE_DEV_NME] [varchar](200) NOT NULL,
	[H6] [varchar](9)  NOT NULL,
	[CMPLTD_CD] [bit] NOT NULL,
	[CREAT_DT] [datetime] NOT NULL,
	[REDSGN_DEV_ID] [int] NOT NULL,
	[REC_STUS_ID] [tinyint] NOT NULL,
	[ODIE_SENT_DT] [datetime]
)

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='M5_EVENT_MSG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[M5_EVENT_MSG]
   Print 'Delete Table [arch].[M5_EVENT_MSG]'
END
GO

CREATE TABLE [arch].[M5_EVENT_MSG](
	[M5_EVENT_MSG_ID] [int]  NOT NULL,
	[M5_ORDR_ID] [int] NOT NULL,
	[M5_ORDR_NBR] [varchar](50) NOT NULL,
	[M5_MSG_ID] [int] NOT NULL,
	[DEV_ID] [varchar](50) NULL,
	[CMPNT_ID] [int] NULL,
	[STUS_ID] [smallint] NOT NULL,
	[CREAT_DT] [smalldatetime] NOT NULL,
	[SENT_DT] [smalldatetime] NULL,
	[EVENT_ID] [int] NULL
	)
Go

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='M5_REDSGN_MSG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[M5_REDSGN_MSG]
   Print 'Delete Table [arch].[M5_REDSGN_MSG]'
END
GO
CREATE TABLE [arch].[M5_REDSGN_MSG](
	[M5_REDSGN_MSG_ID] [int] NOT NULL,
	[REDSGN_DEV_ID] [int] NOT NULL,
	[CHARGE_AMT] [decimal](10, 2) NOT NULL,
	[SITE] [varchar](255) NULL,
	[STUS_ID] [smallint] NOT NULL,
	[CREAT_DT] [smalldatetime] NOT NULL,
	[SENT_DT] [smalldatetime] NULL,
	[EVENT_ID] [int] NULL
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_DSL_SBIC_CUST_TRPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_DSL_SBIC_CUST_TRPT]
   Print 'Delete Table [arch].[MDS_EVENT_DSL_SBIC_CUST_TRPT]'
END
GO

CREATE TABLE [arch].[MDS_EVENT_DSL_SBIC_CUST_TRPT](
	[DSL_SBIC_CUST_TRPT_ID] [int] NOT NULL,
	[EVENT_ID] [int] NOT NULL,
	[PRIM_BKUP_CD] [char](1) NOT NULL,
	[VNDR_PRVDR_TRPT_CKT_ID] [varchar](100) NULL,
	[VNDR_PRVDR_NME] [varchar](100) NULL,
	[IS_WIRELESS_CD] [char](1) NOT NULL,
	[BW_ESN_MEID] [varchar](50) NULL,
	[SCA_NBR] [varchar](50) NULL,
	[IP_ADR] [varchar](50) NULL,
	[SUBNET_MASK_ADR] [varchar](50) NULL,
	[NXTHOP_GTWY_ADR] [varchar](50) NULL,
	[SPRINT_MNGD_CD] [char](1) NOT NULL,
	[ODIE_DEV_NME] [varchar](200) NOT NULL,
	[CREAT_DT] [datetime] NOT NULL,
	[MDS_TRNSPRT_TYPE] [varchar](200) NOT NULL,
	[REC_STUS_ID] [tinyint] NOT NULL,
	[MODFD_DT] [datetime] NULL,
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_NTWK_ACTY' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_NTWK_ACTY]
   Print 'Delete Table [arch].[MDS_EVENT_NTWK_ACTY]'
END
GO

CREATE TABLE [arch].[MDS_EVENT_NTWK_ACTY](
	[ID] [bigint] NOT NULL,
	[EVENT_ID] [int] NOT NULL,
	[NTWK_ACTY_TYPE_ID] [tinyint] NOT NULL,
	[CREAT_DT] [smalldatetime] NOT NULL,
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_NTWK_CUST' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_NTWK_CUST]
   Print 'Delete Table [arch].[MDS_EVENT_NTWK_CUST]'
END
GO

CREATE TABLE [arch].[MDS_EVENT_NTWK_CUST](
	[MDS_EVENT_NTWK_CUST_ID] [int] NOT NULL,
	[EVENT_ID] [int] NOT NULL,
	[INSTL_SITE_POC_NME] [varchar](200) NULL,
	[INSTL_SITE_POC_PHN] [varchar](200) NULL,
	[INSTL_SITE_POC_EMAIL] [varchar](200) NULL,
	[ROLE_NME] [varchar](200) NOT NULL,
	[CREAT_DT] [datetime] NOT NULL,
	[MODFD_DT] [datetime] NULL,
	[ASSOC_H6] [varchar](9) NOT NULL,
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_NTWK_TRPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_NTWK_TRPT]
   Print 'Delete Table [arch].[MDS_EVENT_NTWK_TRPT]'
END
GO

CREATE TABLE [arch].[MDS_EVENT_NTWK_TRPT](
	[MDS_EVENT_NTWK_TRPT_ID] [int] NOT NULL,
	[EVENT_ID] [int] NOT NULL,
	[MDS_TRNSPRT_TYPE] [varchar](100) NOT NULL,
	[LEC_PRVDR_NME] [varchar](200) NULL,
	[LEC_CNTCT_INFO] [varchar](1000) NULL,
	[MACH5_ORDR_NBR] [varchar](50) NULL,
	[IP_NUA_ADR] [varchar](max) NULL,
	[MPLS_ACCS_BDWD] [varchar](50) NULL,
	[SCA_NBR] [varchar](100) NULL,
	[TAGGD_CD] [bit] NULL,
	[VLAN_NBR] [varchar](50) NULL,
	[MULTI_VRF_REQ] [varchar](255) NULL,
	[IP_VER] [varchar](50) NULL,
	[LOC_CITY] [varchar](200) NULL,
	[LOC_STT_PRVN] [varchar](200) NULL,
	[LOC_CTRY] [varchar](200) NULL,
	[ASSOC_H6] [char](9) NULL,
	[CREAT_DT] [datetime] NOT NULL,
	[REC_STUS_ID] [tinyint] NOT NULL,
	[MODFD_DT] [datetime] NULL,
	[DD_APRVL_NBR] [varchar](200) NULL,
	[VAS_TYPE] [varchar](100) NULL,
	[CE_SRVC_ID] [varchar](500) NULL,
	[BDWD_NME] [varchar](20) NULL,
)

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='REDSGN' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[REDSGN]
   Print 'Delete Table [arch].[REDSGN]'
END
GO
CREATE TABLE [arch].[REDSGN](
	[REDSGN_ID] [int] ,
	[REDSGN_NBR] [varchar](20) NOT NULL,
	[H1_CD] [varchar](9) NOT NULL,
	[NTE_ASSIGNED] [varchar](100) NULL,
	[PM_ASSIGNED] [varchar](100) NULL,
	[REDSGN_TYPE_ID] [tinyint] NOT NULL,
	[STUS_ID] [smallint] NOT NULL,
	[ENTIRE_NW_CKD_ID] [bit] NOT NULL,
	[REDSGN_DEV_STUS_ID] [smallint] NULL,
	[CRETD_BY_CD] [int] NOT NULL,
	[CRETD_DT] [smalldatetime] NOT NULL,
	[MODFD_BY_CD] [int] NULL,
	[MODFD_DT] [smalldatetime] NULL,
	[SUBMIT_DT] [smalldatetime] NULL,
	[MOD_SUBMIT_DT] [smalldatetime] NULL,
	[SLA_DT] [smalldatetime] NULL,
	[ODIE_CUST_ID] [varchar](10) NULL,
	[APPROV_NBR] [varchar](50) NULL,
	[COST_AMT] [decimal](10, 2) NULL,
	[EXPRTN_DT] [smalldatetime] NULL,
	[CSG_LVL_ID] [tinyint] NOT NULL,
	[EXT_XPIRN_FLG_CD] [bit] NULL,
	[REDSGN_CAT_ID] [tinyint] NULL,
	[DSGN_DOC_LOC_TXT] [nvarchar](2000) NULL,
	[SDE_ASN_NME] [varchar](100) NULL,
	[NTE_LVL_EFFORT_AMT] [decimal](10, 2) NULL,
	[PM_LVL_EFFORT_AMT] [decimal](10, 2) NULL,
	[NE_LVL_EFFORT_AMT] [decimal](10, 2) NULL,
	[NTE_OT_LVL_EFFORT_AMT] [decimal](10, 2) NULL,
	[PM_OT_LVL_EFFORT_AMT] [decimal](10, 2) NULL,
	[NE_OT_LVL_EFFORT_AMT] [decimal](10, 2) NULL,
	[SPS_LVL_EFFORT_AMT] [decimal](10, 2) NULL,
	[NE_ASN_NME] [varchar](100) NULL,
	[SOWS_FOLDR_PATH_NME] [varchar](300) NULL,
	[SPS_OT_LVL_EFFORT_AMT] [decimal](10, 2) NULL,
	[BILL_OVRRDN_CD] [bit] NOT NULL,
	[BILL_OVRRDN_AMT] [decimal](10, 2) NULL,
	[IS_SDWAN] [bit] NOT NULL,
	[CUST_NME] [varchar](1000) NULL,
	[CUST_EMAIL_ADR] [varchar](500) NULL,
	[DSGN_DOC_NBR_BPM] [varchar](26) NULL,
	[CISC_SMRT_LIC_CD] [bit] NULL,
	[SMRT_ACCNT_DMN] [varchar](100) NULL,
	[MSS_IMPL_EST_AMT] [decimal](10, 2) NULL,
	[VRTL_ACCNT] [varchar](100) NULL,
	[BPM_REDSGN_NBR] [varchar](50) NULL,
	[ADDL_DOC_TXT] [varchar](max) NULL,
)

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='REDSGN_CUST_BYPASS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[REDSGN_CUST_BYPASS]
   Print 'Delete Table [arch].[REDSGN_CUST_BYPASS]'
END
GO
CREATE TABLE [arch].[REDSGN_CUST_BYPASS](
	[ID] [int] NOT NULL,
	[H1_CD] [varchar](9) NOT NULL,
	[REC_STUS_ID] [bit] NOT NULL,
	[CREAT_DT] [smalldatetime] NOT NULL,
	[CREAT_BY_USER_ID] [int] NOT NULL,
	[MODFD_DT] [smalldatetime] NULL,
	[MODFD_BY_USER_ID] [int] NULL,
	[CSG_LVL_ID] [tinyint] NOT NULL,
	[CUST_NME] [varchar](1000) NULL,
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='REDSGN_DEV_EVENT_HIST' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[REDSGN_DEV_EVENT_HIST]
   Print 'Delete Table [arch].[REDSGN_DEV_EVENT_HIST]'
END
GO
CREATE TABLE [arch].[REDSGN_DEV_EVENT_HIST](
	[REDSGN_DEV_EVENT_HIST_ID] [int] NOT NULL,
	[REDSGN_DEV_ID] [int] NOT NULL,
	[EVENT_ID] [int] NOT NULL,
	[CMPLTD_DT] [datetime] NOT NULL
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='REDSGN_DEVICES_INFO' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[REDSGN_DEVICES_INFO]
   Print 'Delete Table [arch].[REDSGN_DEVICES_INFO]'
END
GO
CREATE TABLE [arch].[REDSGN_DEVICES_INFO](
	[REDSGN_DEV_ID] [int] NOT NULL,
	[REDSGN_ID] [int] NOT NULL,
	[SEQ_NBR] [smallint] NOT NULL,
	[DEV_NME] [varchar](50) NOT NULL,
	[FAST_TRK_CD] [int],
	[DSPCH_RDY_CD] [bit],
	[REC_STUS_ID] [bit] NOT NULL,
	[CRETD_BY_CD] [int] NOT NULL,
	[CRETD_DT] [smalldatetime] NOT NULL,
	[MODFD_BY_CD] [int],
	[MODFD_DT] [smalldatetime],
	[NTE_CHRG_CD] [bit] NOT NULL,
	[DEV_BILL_DT] [datetime],
	[DEV_CMPLTN_CD] [bit],
	[H6_CUST_ID] [nvarchar](9) ,
	[EVENT_CMPLTN_DT] [smalldatetime],
	[SC_CD] [char](1)
)

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='REDSGN_DOC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[REDSGN_DOC]
   Print 'Delete Table [arch].[REDSGN_DOC]'
END
GO
CREATE TABLE [arch].[REDSGN_DOC](
	[REDSGN_DOC_ID] [int] NOT NULL,
	[REDSGN_ID] [int] NOT NULL,
	[FILE_NME] [varchar](100) NOT NULL,
	[FILE_CNTNT] [varchar](max) NOT NULL,
	[FILE_SIZE_QTY] [int] NOT NULL,
	[CREAT_DT] [smalldatetime] NOT NULL,
	[CREAT_BY_USER_ID] [int] NOT NULL,
	[MODFD_BY_USER_ID] [int],
	[REC_STUS_ID] [tinyint] NOT NULL,
	[REDSGN_DEV_STUS_ID] [smallint],
	[MODFD_DT] [smalldatetime]
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='REDSGN_EMAIL_NTFCTN' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[REDSGN_EMAIL_NTFCTN]
   Print 'Delete Table [arch].[REDSGN_EMAIL_NTFCTN]'
END
GO
CREATE TABLE [arch].[REDSGN_EMAIL_NTFCTN](
	[REDSGN_EMAIL_NTFCTN_ID] [int] NOT NULL,
	[REDSGN_ID] [int] NOT NULL,
	[USER_IDNTFCTN_EMAIL] [varchar](max) NOT NULL,
	[REC_STUS_ID] [bit] NOT NULL,
	[CRETD_USER_ID] [int] NOT NULL,
	[CRETD_DT] [smalldatetime] NOT NULL
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='REDSGN_EMAIL_NTFCTN' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[REDSGN_H1_MRC_VALUES]
   Print 'Delete Table [arch].[REDSGN_H1_MRC_VALUES]'
END
GO
CREATE TABLE [arch].[REDSGN_H1_MRC_VALUES](
	[H1_ID] [varchar](10) NOT NULL,
	[REDSGN_CAT_ID] [tinyint] NOT NULL,
	[MRC_RATE_VS_NRC] [decimal] NOT NULL,
	[REC_STUS_ID] [bit] NOT NULL
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='REDSGN_NOTES' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[REDSGN_NOTES]
   Print 'Delete Table [arch].[REDSGN_NOTES]'
END
GO
CREATE TABLE [arch].[REDSGN_NOTES](
	[REDSGN_NOTES_ID] [int] NOT NULL,
	[REDSGN_ID] [int] NOT NULL,
	[REDSGN_NTE_TYPE_ID] [tinyint] NOT NULL,
	[NOTES] [varchar](max)  NOT NULL,
	[CRETD_BY_CD] [int] NOT NULL,
	[CRETD_DT] [smalldatetime] NOT NULL
)

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='REDSGN_WRKFLW' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[REDSGN_WRKFLW]
   Print 'Delete Table [arch].[REDSGN_WRKFLW]'
END
GO
CREATE TABLE [arch].[REDSGN_WRKFLW](
	[REDSGN_WRKFLW_ID] [int] NOT NULL,
	[PRE_WRKFLW_STUS_ID] [smallint] NOT NULL,
	[DESRD_WRKFLW_STUS_ID] [smallint] NOT NULL,
	[REC_STUS_ID] [tinyint] NOT NULL,
	[CRETD_DT] [smalldatetime] NOT NULL
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SIPT_EVENT_ACTY' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SIPT_EVENT_ACTY]
   Print 'Delete Table [arch].[SIPT_EVENT_ACTY]'
END
GO
CREATE TABLE [arch].[SIPT_EVENT_ACTY](
	[EVENT_ID] [int] NOT NULL,
	[SIPT_ACTY_TYPE_ID] [smallint] NOT NULL,
	[CREAT_DT] [datetime] NOT NULL
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SIPT_EVENT_DOC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SIPT_EVENT_DOC]
   Print 'Delete Table [arch].[SIPT_EVENT_DOC]'
END
GO
CREATE TABLE [arch].[SIPT_EVENT_DOC](
	[SIPT_EVENT_DOC_ID] [int] NOT NULL,
	[EVENT_ID] [int] NOT NULL,
	[FILE_NME] [varchar](4000) NOT NULL,
	[FILE_CNTNT] [varchar](max)  NOT NULL,
	[FILE_SIZE_QTY] [int] NOT NULL,
	[REC_STUS_ID] [tinyint] NOT NULL,
	[CREAT_BY_USER_ID] [int] NOT NULL,
	[CREAT_DT] [datetime] NOT NULL
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SIPT_EVENT_TOLL_TYPE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SIPT_EVENT_TOLL_TYPE]
   Print 'Delete Table [arch].[SIPT_EVENT_TOLL_TYPE]'
END
GO
CREATE TABLE [arch].[SIPT_EVENT_TOLL_TYPE](
	[EVENT_ID] [int] NOT NULL,
	[SIPT_TOLL_TYPE_ID] [tinyint] NOT NULL,
	[CREAT_DT] [datetime] NOT NULL
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SIPT_EVENT_TOLL_TYPE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SIPT_RELTD_ORDR]
   Print 'Delete Table [arch].[SIPT_EVENT_TOLL_TYPE]'
END
GO
CREATE TABLE [arch].[SIPT_RELTD_ORDR](
	[SIPT_RELTD_ORDR_ID] [int] NOT NULL,
	[EVENT_ID] [int] NOT NULL,
	[M5_ORDR_NBR] [varchar](50) NOT NULL,
	[CREAT_DT] [datetime] NOT NULL
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SPLK_EVENT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SPLK_EVENT]
   Print 'Delete Table [arch].[SPLK_EVENT]'
END
GO
CREATE TABLE [arch].[SPLK_EVENT](
	[EVENT_ID] [int] NOT NULL,
	[EVENT_STUS_ID] [tinyint] NOT NULL,
	[FTN] [varchar](20) NULL,
	[CHARS_ID] [varchar](20) NULL,
	[H1] [dbo].[H1] NOT NULL,
	[H6] [dbo].[H6] NULL,
	[REQOR_USER_ID] [int] NULL,
	[SALS_USER_ID] [int] NULL,
	[PUB_EMAIL_CC_TXT] [varchar](200) NULL,
	[CMPLTD_EMAIL_CC_TXT] [varchar](200) NULL,
	[DSGN_CMNT_TXT] [varchar](max) NOT NULL,
	[SPLK_EVENT_TYPE_ID] [tinyint] NULL,
	[SPLK_ACTY_TYPE_ID] [tinyint] NOT NULL,
	[IP_VER_ID] [tinyint] NULL,
	[MDS_MNGD_CD] [bit] NOT NULL,
	[ESCL_CD] [bit] NOT NULL,
	[PRIM_REQ_DT] [smalldatetime] NULL,
	[SCNDY_REQ_DT] [smalldatetime] NULL,
	[ESCL_REAS_ID] [tinyint] NULL,
	[STRT_TMST] [smalldatetime] NOT NULL,
	[EXTRA_DRTN_TME_AMT] [smallint] NOT NULL,
	[END_TMST] [smalldatetime] NOT NULL,
	[WRKFLW_STUS_ID] [tinyint] NULL,
	[REC_STUS_ID] [tinyint] NOT NULL,
	[CREAT_BY_USER_ID] [int] NOT NULL,
	[MODFD_BY_USER_ID] [int] NULL,
	[MODFD_DT] [smalldatetime] NULL,
	[CREAT_DT] [smalldatetime] NOT NULL,
	[CNFRC_BRDG_NBR] [varchar](50) NULL,
	[CNFRC_PIN_NBR] [varchar](20) NULL,
	[EVENT_DRTN_IN_MIN_QTY] [smallint] NOT NULL,
	[SOWS_EVENT_ID] [int] NULL,
	[RELTD_CMPS_NCR_CD] [bit] NOT NULL,
	[RELTD_CMPS_NCR_NME] [varchar](12) NULL,
	[CUST_NME] [varchar](1000) NULL,
	[CUST_CNTCT_NME] [varchar](500) NULL,
	[CUST_CNTCT_PHN_NBR] [varchar](200) NULL,
	[CUST_EMAIL_ADR] [varchar](500) NULL,
	[CUST_CNTCT_CELL_PHN_NBR] [varchar](200) NULL,
	[CUST_CNTCT_PGR_NBR] [varchar](200) NULL,
	[CUST_CNTCT_PGR_PIN_NBR] [varchar](200) NULL,
	[EVENT_TITLE_TXT] [varchar](2000) NULL,
	[EVENT_DES] [varchar](max) NULL,
)

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='UCaaS_EVENT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[UCaaS_EVENT]
   Print 'Delete Table [arch].[UCaaS_EVENT]'
END
GO
CREATE TABLE [arch].[UCaaS_EVENT](
	[EVENT_ID] [int],
	[EVENT_STUS_ID] [tinyint],
	[H1] [varchar](9) NOT NULL,
	[CHARS_ID] [varchar](20),
	[CUST_ACCT_TEAM_PDL_NME] [varchar](200) ,
	[CUST_SOW_LOC_TXT] [nvarchar](1000),
	[UCaaS_PROD_TYPE_ID] [smallint],
	[UCaaS_PLAN_TYPE_ID] [smallint],
	[UCaaS_ACTY_TYPE_ID] [smallint],
	[SHRT_DES] [nvarchar](2500),
	[H6] [nvarchar](9),
	[CCD] [smalldatetime],
	[SITE_ID] [varchar](200),
	[INSTL_SITE_POC_INTL_PHN_CD] [varchar](50),
	[INSTL_SITE_POC_INTL_CELL_PHN_CD] [varchar](16),
	[SRVC_ASSRN_POC_INTL_PHN_CD] [varchar](50),
	[SRVC_ASSRN_POC_INTL_CELL_PHN_CD] [varchar](20),
	[US_INTL_CD] [char](1) ,
	[SPRINT_CPE_NCR_ID] [tinyint] ,
	[CPE_DSPCH_EMAIL_ADR] [varchar](200),
	[CPE_DSPCH_CMNT_TXT] [varchar](1000),
	[ESCL_REAS_ID] [tinyint],
	[PRIM_REQ_DT] [smalldatetime],
	[SCNDY_REQ_DT] [smalldatetime],
	[BUS_JUSTN_TXT] [varchar](1000),
	[CNFRC_BRDG_ID] [tinyint],
	[CNFRC_BRDG_NBR] [varchar](50),
	[CNFRC_PIN_NBR] [varchar](20),
	[ONLINE_MEETING_ADR] [varchar](1000),
	[PUB_EMAIL_CC_TXT] [varchar](200),
	[CMPLTD_EMAIL_CC_TXT] [varchar](200),
	[REQOR_USER_ID] [int] NOT NULL,
	[REQOR_USER_CELL_PHN_NBR] [varchar](50),
	[CMNT_TXT] [varchar](max),
	[STRT_TMST] [smalldatetime],
	[END_TMST] [smalldatetime],
	[EXTRA_DRTN_TME_AMT] [smallint],
	[EVENT_DRTN_IN_MIN_QTY] [smallint],
	[WRKFLW_STUS_ID] [tinyint],
	[OLD_EVENT_STUS_ID] [tinyint],
	[FAIL_REAS_ID] [smallint],
	[CREAT_BY_USER_ID] [int],
	[MODFD_BY_USER_ID] [int],
	[MODFD_DT] [datetime],
	[CREAT_DT] [datetime],
	[REC_STUS_ID] [tinyint] NOT NULL,
	[DISC_MGMT_CD] [char](1),
	[FULL_CUST_DISC_CD] [bit],
	[FULL_CUST_DISC_REAS_TXT] [varchar](2500),
	[UCaaS_DESGN_DOC] [varchar](max),
	[MNS_PM_ID] [varchar](10),
	[STREET_ADR] [varchar](500),
	[EVENT_TITLE_TXT] [varchar](2000),
	[CUST_NME] [varchar](1000),
	[INSTL_SITE_POC_NME] [varchar](500),
	[INSTL_SITE_POC_PHN_NBR] [varchar](200),
	[INSTL_SITE_POC_CELL_PHN_NBR] [varchar](200),
	[SRVC_ASSRN_POC_NME] [varchar](500),
	[SRVC_ASSRN_POC_PHN_NBR] [varchar](200),
	[SRVC_ASSRN_POC_CELL_PHN_NBR] [varchar](200),
	[FLR_BLDG_NME] [varchar](200),
	[CTY_NME] [varchar](500),
	[STT_PRVN_NME] [varchar](500),
	[CTRY_RGN_NME] [varchar](500),
	[ZIP_CD] [varchar](200)
)


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='UCaaS_EVENT_BILLING' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[UCaaS_EVENT_BILLING]
   Print 'Delete Table [arch].[UCaaS_EVENT_BILLING]'
END
GO
CREATE TABLE [arch].[UCaaS_EVENT_BILLING](
	[UCaaS_EVENT_BILLING_ID] [int],
	[EVENT_ID] [int],
	[UCaaS_BILL_ACTY_ID] [smallint],
	[CREAT_DT] [datetime],
	[INC_QTY] [smallint],
	[DCR_QTY] [smallint]
)



if exists (select 'x' from information_schema.tables where  TABLE_NAME ='UCaaS_EVENT_ODIE_DEV' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[UCaaS_EVENT_ODIE_DEV]
   Print 'Delete Table [arch].[UCaaS_EVENT_ODIE_DEV]'
END
GO

CREATE TABLE [arch].[UCaaS_EVENT_ODIE_DEV](
	[UCaaS_EVENT_ODIE_DEV_ID] [int] NOT NULL,
	[EVENT_ID] [int] NOT NULL,
	[RDSN_NBR] [varchar](50) NOT NULL,
	[RDSN_EXP_DT] [smalldatetime] NOT NULL,
	[ODIE_DEV_NME] [varchar](200) NOT NULL,
	[DEV_MODEL_ID] [smallint] NOT NULL,
	[MANF_ID] [smallint] NOT NULL,
	[CREAT_DT] [datetime] NOT NULL
)


