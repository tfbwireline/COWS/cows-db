USE [COWS]
GO

DECLARE @SqlStatement VARCHAR(MAX)
SELECT @SqlStatement = 
    COALESCE(@SqlStatement, '') + 'DROP TABLE [arch].' + QUOTENAME(TABLE_NAME) + ';' + CHAR(13)
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'arch'

PRINT @SqlStatement


--DECLARE @SqlStatement VARCHAR(MAX)
SELECT @SqlStatement = 
    COALESCE(@SqlStatement, '') + 'ALTER TABLE  [arch].' + QUOTENAME(TABLE_NAME) + '     NOCHECK CONSTRAINT ALL'+ ';' + CHAR(13)
FROM INFORMATION_SCHEMA.TABLES
WHERE TABLE_SCHEMA = 'arch'

PRINT @SqlStatement
----1
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SQL_ERROR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SQL_ERROR]
   Print 'Delete Table [arch].[SQL_ERROR]'
END

----2 
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ArchiveLog' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ArchiveLog]
   Print 'Delete Table [arch].[ArchiveLog]'
END
----3
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='WG_PTRN_STUS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[WG_PTRN_STUS]
   Print 'Delete Table [arch].[WG_PTRN_STUS]'
END
---4
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='WG_PROF_STUS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[WG_PROF_STUS]
   Print 'Delete Table [arch].[WG_PROF_STUS]'
END

---5
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='VNDR_ORDR_MS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[VNDR_ORDR_MS]
   Print 'Delete Table [arch].[VNDR_ORDR_MS]'
END
---6
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='VNDR_ORDR_FORM' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[VNDR_ORDR_FORM]
   Print 'Delete Table [arch].[VNDR_ORDR_FORM]'
END
---7
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='VNDR_ORDR_EMAIL_ATCHMT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[VNDR_ORDR_EMAIL_ATCHMT]
   Print 'Delete Table [arch].[VNDR_ORDR_EMAIL_ATCHMT]'
END
---8
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='VNDR_ORDR_EMAIL' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[VNDR_ORDR_EMAIL]
   Print 'Delete Table [arch].[VNDR_ORDR_EMAIL]'
END
---9
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='VNDR_ORDR_EMAIL' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[VNDR_ORDR_EMAIL]
   Print 'Delete Table [arch].[VNDR_ORDR_EMAIL]'
END
---10
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='USER_WFM_ASMT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[USER_WFM_ASMT]
   Print 'Delete Table [arch].[USER_WFM_ASMT]'
END
---11
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='TRPT_ORDR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[TRPT_ORDR]
   Print 'Delete Table [arch].[TRPT_ORDR]'
END
---12
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SPLK_EVENT_ACCS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SPLK_EVENT_ACCS]
   Print 'Delete Table [arch].[SPLK_EVENT_ACCS]'
END
---13
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SPLK_EVENT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SPLK_EVENT]
   Print 'Delete Table [arch].[SPLK_EVENT]'
END
---14
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='PROD_ORDR_TYPE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[PROD_ORDR_TYPE]
   Print 'Delete Table [arch].[PROD_ORDR_TYPE]'
END
----15
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_VLAN' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_VLAN]
   Print 'Delete Table [arch].[ORDR_VLAN]'
END

-----16
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_STDI_HIST' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_STDI_HIST]
   Print 'Delete Table [arch].[ORDR_STDI_HIST]'
END
---17
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_REC_LOCK' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_REC_LOCK]
   Print 'Delete Table [arch].[ORDR_REC_LOCK]'
END
---18
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_MS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_MS]
   Print 'Delete Table [arch].[ORDR_MS]'
END
----19
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_JPRDY' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_JPRDY]
   Print 'Delete Table [arch].[ORDR_JPRDY]'
END
----20
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_HOLD_MS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_HOLD_MS]
   Print 'Delete Table [arch].[ORDR_HOLD_MS]'
END
----21
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_H5_DOC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_H5_DOC]
   Print 'Delete Table [arch].[ORDR_H5_DOC]'
END
----22
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_GRP' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_GRP]
   Print 'Delete Table [arch].[ORDR_GRP]'
END
-----23
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_EXP' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_EXP]
   Print 'Delete Table [arch].[ORDR_EXP]'
END
---24
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_CNTCT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_CNTCT]
   Print 'Delete Table [arch].[ORDR_CNTCT]'
END
---25
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_CKT_CHG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_CKT_CHG]
   Print 'Delete Table [arch].[ORDR_CKT_CHG]'
END
---26
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_ADR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_ADR]
   Print 'Delete Table [arch].[ORDR_ADR]'
END
---27
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ODIE_RSPN_INFO' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ODIE_RSPN_INFO]
   Print 'Delete Table [arch].[ODIE_RSPN_INFO]'
END
----28
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ODIE_RSPN' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ODIE_RSPN]
   Print 'Delete Table [arch].[ODIE_RSPN]'
END
---29
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ODIE_DISC_REQ' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ODIE_DISC_REQ]
   Print 'Delete Table [arch].[ODIE_DISC_REQ]'
END
---30
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='NRM_VNDR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[NRM_VNDR]
   Print 'Delete Table [arch].[NRM_VNDR]'
END
---31
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='NRM_SRVC_INSTC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[NRM_SRVC_INSTC]
   Print 'Delete Table [arch].[NRM_SRVC_INSTC]'
END
---32
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='NRM_CKT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[NRM_CKT]
   Print 'Delete Table [arch].[NRM_CKT]'
END
----33
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='NGVN_EVENT_SIP_TRNK' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[NGVN_EVENT_SIP_TRNK]
   Print 'Delete Table [arch].[NGVN_EVENT_SIP_TRNK]'
END
---34
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='NGVN_EVENT_CKT_ID_NUA' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[NGVN_EVENT_CKT_ID_NUA]
   Print 'Delete Table [arch].[NGVN_EVENT_CKT_ID_NUA]'
END
----35
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='NGVN_EVENT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[NGVN_EVENT]
   Print 'Delete Table [arch].[NGVN_EVENT]'
END
---36
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='NCCO_ORDR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[NCCO_ORDR]
   Print 'Delete Table [arch].[NCCO_ORDR]'
END
---37
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MPLS_EVENT_VAS_TYPE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MPLS_EVENT_VAS_TYPE]
   Print 'Delete Table [arch].[MPLS_EVENT_VAS_TYPE]'
END
----38
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MPLS_EVENT_VAS_TYPE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MPLS_EVENT_VAS_TYPE]
   Print 'Delete Table [arch].[MPLS_EVENT_VAS_TYPE]'
END
----39
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MPLS_EVENT_ACTY_TYPE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MPLS_EVENT_ACTY_TYPE]
   Print 'Delete Table [arch].[MPLS_EVENT_ACTY_TYPE]'
END
---40
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MPLS_EVENT_ACCS_TAG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MPLS_EVENT_ACCS_TAG]
   Print 'Delete Table [arch].[MPLS_EVENT_ACCS_TAG]'
END
----41
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MPLS_EVENT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MPLS_EVENT]
   Print 'Delete Table [arch].[MPLS_EVENT]'
END
----42
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MPLS_ACCS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MPLS_ACCS]
   Print 'Delete Table [arch].[MPLS_ACCS]'
END
----43
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SIP_TRNK_GRP' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SIP_TRNK_GRP]
   Print 'Delete Table [arch].[SIP_TRNK_GRP]'
END
----44
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_MNGD_ACT_NEW' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_MNGD_ACT_NEW]
   Print 'Delete Table [arch].[MDS_MNGD_ACT_NEW]'
END
---45
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_MNGD_ACT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_MNGD_ACT]
   Print 'Delete Table [arch].[MDS_MNGD_ACT]'
END
----46
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_WRLS_TRPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_WRLS_TRPT]
   Print 'Delete Table [arch].[MDS_EVENT_WRLS_TRPT]'
END
---47
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_WRLS_TRNSPRT_DEV' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_WRLS_TRNSPRT_DEV]
   Print 'Delete Table [arch].[MDS_EVENT_WRLS_TRNSPRT_DEV]'
END
---48
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_WIRED_TRPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_WIRED_TRPT]
   Print 'Delete Table [arch].[MDS_EVENT_WIRED_TRPT]'
END
----49
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_WIRED_TRNSPRT_DEV' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_WIRED_TRNSPRT_DEV]
   Print 'Delete Table [arch].[MDS_EVENT_WIRED_TRNSPRT_DEV]'
END
----50
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_VRTL_CNCTN' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_VRTL_CNCTN]
   Print 'Delete Table [arch].[MDS_EVENT_VRTL_CNCTN]'
END
----51
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_VLAN' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_VLAN]
   Print 'Delete Table [arch].[MDS_EVENT_VLAN]'
END
----52
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_SPRINT_LINK_ATM_TRPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_SPRINT_LINK_ATM_TRPT]
   Print 'Delete Table [arch].[MDS_EVENT_SPRINT_LINK_ATM_TRPT]'
END
---53
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_SPAE_TRPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_SPAE_TRPT]
   Print 'Delete Table [arch].[MDS_EVENT_SPAE_TRPT]'
END
---54
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_PVC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_PVC]
   Print 'Delete Table [arch].[MDS_EVENT_PVC]'
END
----55
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_ODIE_DEV' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_ODIE_DEV]
   Print 'Delete Table [arch].[MDS_EVENT_ODIE_DEV]'
END
---56
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_ODIE_DEV_NME' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_ODIE_DEV_NME]
   Print 'Delete Table [arch].[MDS_EVENT_ODIE_DEV_NME]'
END
---57
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_MNS_OE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_MNS_OE]
   Print 'Delete Table [arch].[MDS_EVENT_MNS_OE]'
END
----58
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_MNS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_MNS]
   Print 'Delete Table [arch].[MDS_EVENT_MNS]'
END
---59
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_MNGD_DEV' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_MNGD_DEV]
   Print 'Delete Table [arch].[MDS_EVENT_MNGD_DEV]'
END
---60
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_MAC_ACTY' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_MAC_ACTY]
   Print 'Delete Table [arch].[MDS_EVENT_MAC_ACTY]'
END
---61
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_LOC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_LOC]
   Print 'Delete Table [arch].[MDS_EVENT_LOC]'
END
---62
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_FORM_CHNG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_FORM_CHNG]
   Print 'Delete Table [arch].[MDS_EVENT_FORM_CHNG]'
END
---63
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_FMS_CKT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_FMS_CKT]
   Print 'Delete Table [arch].[MDS_EVENT_FMS_CKT]'
END
---64
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_DSL_TRPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_DSL_TRPT]
   Print 'Delete Table [arch].[MDS_EVENT_DSL_TRPT]'
END
---65
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_DISCO' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_DISCO]
   Print 'Delete Table [arch].[MDS_EVENT_DISCO]'
END
-----66
--if exists (select 'x' from information_schema.tables where  TABLE_NAME ='H5_FOLDR' and TABLE_SCHEMA='arch')
--BEGIN
--   DROP TABLE [arch].[ORDR]
--   Print 'Delete Table [arch].[H5_FOLDR]'
--END
---67
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_ACCS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_ACCS]
   Print 'Delete Table [arch].[MDS_EVENT_ACCS]'
END
---68
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='L2P_LOG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[L2P_LOG]
   Print 'Delete Table [arch].[L2P_LOG]'
END
---69
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='IPL_ORDR_EQPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[IPL_ORDR_EQPT]
   Print 'Delete Table [arch].[IPL_ORDR_EQPT]'
END
---70
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='IPL_ORDR_CKT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[IPL_ORDR_CKT]
   Print 'Delete Table [arch].[IPL_ORDR_CKT]'
END

------71
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='IPL_ORDR_ACCS_INFO' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[IPL_ORDR_ACCS_INFO]
   Print 'Delete Table [arch].[IPL_ORDR_ACCS_INFO]'
END
----72
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='IPL_ORDR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[IPL_ORDR]
   Print 'Delete Table [arch].[IPL_ORDR]'
END
---73
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='H5_DOC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[H5_DOC]
   Print 'Delete Table [arch].[H5_DOC]'
END
---74
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_ORDR_RELTD_ORDR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_ORDR_RELTD_ORDR]
   Print 'Delete Table [arch].[FSA_ORDR_RELTD_ORDR]'
END
----75
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_ORDR_MSG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_ORDR_MSG]
   Print 'Delete Table [arch].[FSA_ORDR_MSG]'
END
----76
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_ORDR_GOM_XNCI' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_ORDR_GOM_XNCI]
   Print 'Delete Table [arch].[FSA_ORDR_GOM_XNCI]'
END
---77
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_ORDR_CUST' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_ORDR_CUST]
   Print 'Delete Table [arch].[FSA_ORDR_CUST]'
END
--78
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_ORDR_CSC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_ORDR_CSC]
   Print 'Delete Table [arch].[FSA_ORDR_CSC]'
END
--79
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_ORDR_BILL_LINE_ITEM' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_ORDR_BILL_LINE_ITEM]
   Print 'Delete Table [arch].[FSA_ORDR_BILL_LINE_ITEM]'
END
---80
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_ORDR_CPE_LINE_ITEM' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_ORDR_CPE_LINE_ITEM]
   Print 'Delete Table [arch].[FSA_ORDR_CPE_LINE_ITEM]'
END
---81
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_ORDR_VAS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_ORDR_VAS]
   Print 'Delete Table [arch].[FSA_ORDR_VAS]'
END
----82
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_MDS_EVENT_ORDR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_MDS_EVENT_ORDR]
   Print 'Delete Table [arch].[FSA_MDS_EVENT_ORDR]'
END
---83
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ODIE_REQ' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ODIE_REQ]
   Print 'Delete Table [arch].[ODIE_REQ]'
END
----84
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_CPE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_CPE]
   Print 'Delete Table [arch].[MDS_EVENT_CPE]'
END
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_MDS_EVENT_NEW' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_MDS_EVENT_NEW]
   Print 'Delete Table [arch].[FSA_MDS_EVENT_NEW]'
END
----85
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_NEW' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_NEW]
   Print 'Delete Table [arch].[MDS_EVENT_NEW]'
END
---86
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_IP_ADR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_IP_ADR]
   Print 'Delete Table [arch].[FSA_IP_ADR]'
END
----87
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_ORDR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_ORDR]
   Print 'Delete Table [arch].[FSA_ORDR]'
END
---88
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_SUCSS_ACTY' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_SUCSS_ACTY]
   Print 'Delete Table [arch].[EVENT_SUCSS_ACTY]'
END
----89
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_REC_LOCK' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_REC_LOCK]
   Print 'Delete Table [arch].[EVENT_REC_LOCK]'
END
----90
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_NVLD_TME_SLOT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_NVLD_TME_SLOT]
   Print 'Delete Table [arch].[EVENT_NVLD_TME_SLOT]'
END
----91
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_FAIL_ACTY' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_FAIL_ACTY]
   Print 'Delete Table [arch].[EVENT_FAIL_ACTY]'
END
----92
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_HIST' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_HIST]
   Print 'Delete Table [arch].[EVENT_HIST]'
END
---93


if exists (select 'x' from information_schema.tables where  TABLE_NAME ='FSA_MDS_EVENT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[FSA_MDS_EVENT]
   Print 'Delete Table [arch].[FSA_MDS_EVENT]'
END
----94
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT]
   Print 'Delete Table [arch].[MDS_EVENT]'
END
---95
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_ASN_TO_USER' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_ASN_TO_USER]
   Print 'Delete Table [arch].[EVENT_ASN_TO_USER]'
END
----96
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EMAIL_REQ' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EMAIL_REQ]
   Print 'Delete Table [arch].[EMAIL_REQ]'
END
---97

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='CKT_MS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[CKT_MS]
   Print 'Delete Table [arch].[CKT_MS]'
END
----98
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='CKT_COST' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[CKT_COST]
   Print 'Delete Table [arch].[CKT_COST]'
END
---99
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='CKT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[CKT]
   Print 'Delete Table [arch].[CKT]'
END

----100
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='VNDR_ORDR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[VNDR_ORDR]
   Print 'Delete Table [VNDR_ORDR].[ORDR]'
END
----101
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='CD_HIST_REAS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[CCD_HIST_REAS]
   Print 'Delete Table [CD_HIST_REAS].[ORDR]'
END

-----102
--if exists (select 'x' from information_schema.tables where  TABLE_NAME ='CCD_HIST' and TABLE_SCHEMA='arch')
--BEGIN
--   DROP TABLE [arch].[CCD_HIST]
--   Print 'Delete Table [CCD_HIST].[ORDR]'
--END
----103
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='APPT_RCURNC_TRGR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[APPT_RCURNC_TRGR]
   Print 'Delete Table [arch].[APPT_RCURNC_TRGR]'
END
----104
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='APPT_RCURNC_DATA' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[APPT_RCURNC_DATA]
   Print 'Delete Table [arch].[APPT_RCURNC_DATA]'
END
----105
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='APPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[APPT]
   Print 'Delete Table [arch].[APPT]'
END
----106
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='AD_EVENT_ACCS_TAG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[AD_EVENT_ACCS_TAG]
   Print 'Delete Table [AD_EVENT_ACCS_TAG]'
END
----107
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='AD_EVENT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[AD_EVENT]
   Print 'Delete Table [arch].[AD_EVENT]'
END


----108
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT]
   Print 'Delete Table [arch].[EVENT]'
END
----109
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ACT_TASK' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ACT_TASK]
   Print 'Delete Table [arch].[ACT_TASK]'
END
------110
--if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_NTE' and TABLE_SCHEMA='arch')
--BEGIN
--   DROP TABLE [arch].[ORDR_NTE]
--   Print 'Delete Table [arch].[ORDR_NTE]'
--END
---102
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='CCD_HIST_REAS' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[CCD_HIST_REAS]
   Print 'Delete Table [arch].[CCD_HIST_REAS]'
END
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='CCD_HIST' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[CCD_HIST]
   Print 'Delete Table [arch].[CCD_HIST]'
END
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ORDR_NTE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR_NTE]
   Print 'Delete Table [arch].[ORDR_NTE]'
END
-----66
--if exists (select 'x' from information_schema.tables where  TABLE_NAME ='H5_FOLDR' and TABLE_SCHEMA='arch')
--BEGIN
--   DROP TABLE [arch].[H5_FOLDR]
--   Print 'Delete Table [arch].[H5_FOLDR]'
--END
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='M5_ORDR_MSG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[M5_ORDR_MSG]
   Print 'Delete Table [arch].[M5_ORDR_MSG]'
END
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='PS_RCPT_QUEUE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[PS_RCPT_QUEUE]
   Print 'Delete Table [arch].[PS_RCPT_QUEUE]'
END

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='PS_REQ_HDR_QUEUE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[PS_REQ_HDR_QUEUE]
   Print 'Delete Table [arch].[PS_REQ_HDR_QUEUE]'
END

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='PS_REQ_LINE_ITM_QUEUE' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[PS_REQ_LINE_ITM_QUEUE]
   Print 'Delete Table [arch].[PS_REQ_LINE_ITM_QUEUE]'
END

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SSTAT_REQ' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SSTAT_REQ]
   Print 'Delete Table [arch].[SSTAT_REQ]'
END

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='SSTAT_RSPN' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[SSTAT_RSPN]
   Print 'Delete Table [arch].[SSTAT_RSPN]'
END



if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_PORT_BNDWD' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_PORT_BNDWD]
   Print 'Delete Table [arch].[MDS_EVENT_PORT_BNDWD]'
END

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_SLNK_WIRED_TRPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_SLNK_WIRED_TRPT]
   Print 'Delete Table [arch].[MDS_EVENT_SLNK_WIRED_TRPT]'
END
GO
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_SRVC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_SRVC]
   Print 'Delete Table [arch].[MDS_EVENT_SRVC]'
END

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_SITE_SRVC' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[MDS_EVENT_SITE_SRVC]
   Print 'Delete Table [arch].[MDS_EVENT_SITE_SRVC]'
END

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_CPE_DEV' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_CPE_DEV]
   Print 'Delete Table [arch].[EVENT_CPE_DEV]'
END
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_DEV_SRVC_MGMT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_DEV_SRVC_MGMT]
   Print 'Delete Table [arch].[EVENT_DEV_SRVC_MGMT]'
END
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='EVENT_DISCO_DEV' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[EVENT_DISCO_DEV]
   Print 'Delete Table [arch].[EVENT_DISCO_DEV]'
END

----111
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='ordr' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[ORDR]
   Print 'Delete Table [arch].[ORDR]'
END
---66
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='H5_FOLDR' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[H5_FOLDR]
   Print 'Delete Table [arch].[H5_FOLDR]'
END
---112
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='CUST_SCRD_DATA' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[CUST_SCRD_DATA]
   Print 'Delete Table [arch].[CUST_SCRD_DATA]'
END

if exists (select 'x' from information_schema.tables where  TABLE_NAME ='M5_EVENT_MSG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[M5_EVENT_MSG]
   Print 'Delete Table [arch].[M5_EVENT_MSG]'
END
/*[M5_REDSGN_MSG] Added July 03 2018*/
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='M5_REDSGN_MSG' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[M5_REDSGN_MSG]
   Print 'Delete Table [arch].[M5_REDSGN_MSG]'
END

GO
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_NTWK_CUST' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[M5_REDSGN_MSG]
   Print 'Delete Table [arch].[MDS_EVENT_NTWK_CUST]'
END
GO
if exists (select 'x' from information_schema.tables where  TABLE_NAME ='MDS_EVENT_NTWK_TRPT' and TABLE_SCHEMA='arch')
BEGIN
   DROP TABLE [arch].[M5_REDSGN_MSG]
   Print 'Delete Table [arch].[MDS_EVENT_NTWK_TRPT]'
END



GO
				