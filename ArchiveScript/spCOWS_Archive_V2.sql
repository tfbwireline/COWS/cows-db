USE [COWS]
GO

/****** Object:  StoredProcedure [arch].[spCOWS_Archive_V2]     ******/

GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---- =============================================
---- Author:		John Caranay
---- Create date:   04/19/2021
---- Description:	

---- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[arch].[spCOWS_Archive_V2]') AND type in (N'P', N'PC'))
DROP PROCEDURE [arch].[spCOWS_Archive_V2]
GO
CREATE PROCEDURE [arch].[spCOWS_Archive_V2]( 
	 @Date			  DateTime
	)
AS
--DECLARE @Date Datetime
--SET @Date = '2011-03-03'
BEGIN

	--[EVENT_DEV_CMPLT] > [REDSGN_DEVICES_INFO]
	RAISERROR ('started [COWS].[arch].[spCOWS_Archive_V2] SP', 0, 1) WITH NOWAIT


	RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
	RAISERROR ('****************         BEGIN ARCHIVE PROCESS                  *****************', 0, 1) WITH NOWAIT
	RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT

	DECLARE @Time varchar(30)
	--SET @Time= cast(GetDate() as varchar)  RAISERROR ('Begin Defining Variables at: %s. ', 0, 1,@Time) WITH NOWAIT

	Declare @RowCounts  Int
	SET @RowCounts=0

	DECLARE     @Event_Table   TABLE(
				ID			INT IDENTITY(1,1),
				EVENT_ID		Int
	)

	DECLARE     @Redsgn_Table TABLE(
				 ID			INT IDENTITY(1,1),
				 REDSGN_ID	INT
	)


	BEGIN TRY
		BEGIN TRANSACTION

		-- EVENT_AVAL_USER
		RAISERROR ('****************         EVENT_AVAL_USER                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EVENT_DEV_CMPLT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[EVENT_AVAL_USER](
			[EVENT_AVAL_USER_ID]   ,
			[TME_SLOT_ID]  ,
			[USER_ID]  ,
			[TME_SLOT_STRT_TME]  ,
			[TME_SLOT_END_TME]  ,
			[TME_SLOT_DRTN_IN_MIN_QTY]  ,
			[DEL_CD]  ,
			[CREAT_DT]  )
		SELECT [EVENT_AVAL_USER_ID]   ,
			[TME_SLOT_ID]  ,
			[USER_ID]  ,
			[TME_SLOT_STRT_TME]  ,
			[TME_SLOT_END_TME]  ,
			[TME_SLOT_DRTN_IN_MIN_QTY]  ,
			[DEL_CD]  ,
			[CREAT_DT]
			
		FROM  [dbo].[EVENT_AVAL_USER]	Where [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_AVAL_USER] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_AVAL_USER] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM  [dbo].[EVENT_AVAL_USER]	Where [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_AVAL_USER] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_AVAL_USER]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 	






		-- EVENT_DEV_CMPLT
		RAISERROR ('****************         EVENT_DEV_CMPLT                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EVENT_DEV_CMPLT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[EVENT_DEV_CMPLT](
			   [EVENT_DEV_CMPLT_ID]
			  ,[EVENT_ID]
			  ,[ODIE_DEV_NME]
			  ,[H6]
			  ,[CMPLTD_CD]
			  ,[CREAT_DT]
			  ,[REDSGN_DEV_ID]
			  ,[REC_STUS_ID]
			  ,[ODIE_SENT_DT] )
		SELECT [EVENT_DEV_CMPLT_ID]
			  ,[EVENT_ID]
			  ,[ODIE_DEV_NME]
			  ,[H6]
			  ,[CMPLTD_CD]
			  ,[CREAT_DT]
			  ,[REDSGN_DEV_ID]
			  ,[REC_STUS_ID]
			  ,[ODIE_SENT_DT]
		FROM	[COWS].[dbo].[EVENT_DEV_CMPLT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_DEV_CMPLT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_DEV_CMPLT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[EVENT_DEV_CMPLT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)  AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_DEV_CMPLT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_DEV_CMPLT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[M5_EVENT_MSG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		-- INSERT INTO [COWS].[arch].[M5_EVENT_MSG]
			   -- ([M5_EVENT_MSG_ID]
			  -- ,[M5_ORDR_ID]
			  -- ,[M5_ORDR_NBR]
			  -- ,[M5_MSG_ID]
			  -- ,[DEV_ID]
			  -- ,[CMPNT_ID]
			  -- ,[STUS_ID]
			  -- ,[CREAT_DT]
			  -- ,[SENT_DT]
			  -- ,[EVENT_ID])
		-- SELECT [M5_EVENT_MSG_ID]
			  -- ,[M5_ORDR_ID]
			  -- ,[M5_ORDR_NBR]
			  -- ,[M5_MSG_ID]
			  -- ,[DEV_ID]
			  -- ,[CMPNT_ID]
			  -- ,[STUS_ID]
			  -- ,[CREAT_DT]
			  -- ,[SENT_DT]
			  -- ,[EVENT_ID]
		-- FROM [COWS].[dbo].[M5_EVENT_MSG] WITH(NOLOCK)	WHERE M5_ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
		-- Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[M5_EVENT_MSG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		-- Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[M5_EVENT_MSG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		-- DELETE FROM	[COWS].[dbo].[M5_EVENT_MSG] WITH(TABLOCKX)	WHERE M5_ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
		-- Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[M5_EVENT_MSG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		-- INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[M5_EVENT_MSG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- M5_REDSGN_MSG
		RAISERROR ('****************         M5_REDSGN_MSG                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[M5_REDSGN_MSG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [COWS].[arch].[M5_REDSGN_MSG]
			   ([M5_REDSGN_MSG_ID]
			  ,[REDSGN_DEV_ID]
			  ,[CHARGE_AMT]
			  ,[SITE]
			  ,[STUS_ID]
			  ,[CREAT_DT]
			  ,[SENT_DT]
			  ,[EVENT_ID])
		SELECT [M5_REDSGN_MSG_ID]
			  ,[REDSGN_DEV_ID]
			  ,[CHARGE_AMT]
			  ,[SITE]
			  ,[STUS_ID]
			  ,[CREAT_DT]
			  ,[SENT_DT]
			  ,[EVENT_ID]
		FROM	[COWS].[dbo].[M5_REDSGN_MSG] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[M5_REDSGN_MSG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[M5_REDSGN_MSG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[M5_REDSGN_MSG] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)  AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[M5_REDSGN_MSG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[M5_REDSGN_MSG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 	




		-- MDS_EVENT_DSL_SBIC_CUST_TRPT
		RAISERROR ('****************         MDS_EVENT_DSL_SBIC_CUST_TRPT                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_DSL_SBIC_CUST_TRPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[MDS_EVENT_DSL_SBIC_CUST_TRPT](
			   [DSL_SBIC_CUST_TRPT_ID]
			  ,[EVENT_ID]
			  ,[PRIM_BKUP_CD]
			  ,[VNDR_PRVDR_TRPT_CKT_ID]
			  ,[VNDR_PRVDR_NME]
			  ,[IS_WIRELESS_CD]
			  ,[BW_ESN_MEID]
			  ,[SCA_NBR]
			  ,[IP_ADR] 
			  ,[SUBNET_MASK_ADR]
			  ,[NXTHOP_GTWY_ADR]
			  ,[SPRINT_MNGD_CD]
			  ,[ODIE_DEV_NME]
			  ,[CREAT_DT]
			  ,[MDS_TRNSPRT_TYPE]
			  ,[REC_STUS_ID]
			  ,[MODFD_DT] )
		SELECT [DSL_SBIC_CUST_TRPT_ID]
			  ,[EVENT_ID]
			  ,[PRIM_BKUP_CD]
			  ,[VNDR_PRVDR_TRPT_CKT_ID]
			  ,[VNDR_PRVDR_NME]
			  ,[IS_WIRELESS_CD]
			  ,[BW_ESN_MEID]
			  ,[SCA_NBR]
			  ,[IP_ADR] 
			  ,[SUBNET_MASK_ADR]
			  ,[NXTHOP_GTWY_ADR]
			  ,[SPRINT_MNGD_CD]
			  ,[ODIE_DEV_NME]
			  ,[CREAT_DT]
			  ,[MDS_TRNSPRT_TYPE]
			  ,[REC_STUS_ID]
			  ,[MODFD_DT] 
		FROM	[COWS].[dbo].[MDS_EVENT_DSL_SBIC_CUST_TRPT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_DSL_SBIC_CUST_TRPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_DSL_SBIC_CUST_TRPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[MDS_EVENT_DSL_SBIC_CUST_TRPT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)  AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_DSL_SBIC_CUST_TRPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_DSL_SBIC_CUST_TRPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 	



		-- MDS_EVENT_NTWK_ACTY
		RAISERROR ('****************         MDS_EVENT_NTWK_ACTY                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_NTWK_ACTY] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[MDS_EVENT_NTWK_ACTY](
			   [ID]
			  ,[EVENT_ID]
			  ,[NTWK_ACTY_TYPE_ID]
			  ,[CREAT_DT] )
		SELECT [ID]
			  ,[EVENT_ID]
			  ,[NTWK_ACTY_TYPE_ID]
			  ,[CREAT_DT]
		FROM	[COWS].[dbo].[MDS_EVENT_NTWK_ACTY] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_NTWK_ACTY] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_NTWK_ACTY] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[MDS_EVENT_NTWK_ACTY] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)  AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_NTWK_ACTY] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_NTWK_ACTY]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 	


		-- MDS_EVENT_NTWK_CUST
		RAISERROR ('****************         MDS_EVENT_NTWK_CUST                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_NTWK_CUST] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[MDS_EVENT_NTWK_CUST](
			   [MDS_EVENT_NTWK_CUST_ID]
			  ,[EVENT_ID]
			  ,[INSTL_SITE_POC_NME]
			  ,[INSTL_SITE_POC_PHN]
			  ,[INSTL_SITE_POC_EMAIL]
			  ,[ROLE_NME]
			  ,[CREAT_DT]
			  ,[MODFD_DT]
			  ,[ASSOC_H6] )
		SELECT [MDS_EVENT_NTWK_CUST_ID]
			  ,[EVENT_ID]
			  ,[INSTL_SITE_POC_NME]
			  ,[INSTL_SITE_POC_PHN]
			  ,[INSTL_SITE_POC_EMAIL]
			  ,[ROLE_NME]
			  ,[CREAT_DT]
			  ,[MODFD_DT]
			  ,[ASSOC_H6]
		FROM	[COWS].[dbo].[MDS_EVENT_NTWK_CUST] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_NTWK_CUST] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_NTWK_CUST] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[MDS_EVENT_NTWK_CUST] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)  AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_NTWK_CUST] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_NTWK_CUST]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 	



		-- MDS_EVENT_NTWK_TRPT
		RAISERROR ('****************         MDS_EVENT_NTWK_TRPT                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_NTWK_TRPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[MDS_EVENT_NTWK_TRPT](
			   [MDS_EVENT_NTWK_TRPT_ID]
			  ,[EVENT_ID]
			  ,[MDS_TRNSPRT_TYPE]
			  ,[LEC_PRVDR_NME]
			  ,[LEC_CNTCT_INFO]
			  ,[MACH5_ORDR_NBR]
			  ,[IP_NUA_ADR]
			  ,[MPLS_ACCS_BDWD]
			  ,[SCA_NBR] 
			  ,[TAGGD_CD]
			  ,[VLAN_NBR]
			  ,[MULTI_VRF_REQ]
			  ,[IP_VER]
			  ,[LOC_CITY]
			  ,[LOC_STT_PRVN]
			  ,[LOC_CTRY]
			  ,[ASSOC_H6]
			  ,[CREAT_DT]
			  ,[REC_STUS_ID]
			  ,[MODFD_DT]
			  ,[DD_APRVL_NBR]
			  ,[VAS_TYPE]
			  ,[CE_SRVC_ID]
			  ,[BDWD_NME] )
		SELECT [MDS_EVENT_NTWK_TRPT_ID]
			  ,[EVENT_ID]
			  ,[MDS_TRNSPRT_TYPE]
			  ,[LEC_PRVDR_NME]
			  ,[LEC_CNTCT_INFO]
			  ,[MACH5_ORDR_NBR]
			  ,[IP_NUA_ADR]
			  ,[MPLS_ACCS_BDWD]
			  ,[SCA_NBR] 
			  ,[TAGGD_CD]
			  ,[VLAN_NBR]
			  ,[MULTI_VRF_REQ]
			  ,[IP_VER]
			  ,[LOC_CITY]
			  ,[LOC_STT_PRVN]
			  ,[LOC_CTRY]
			  ,[ASSOC_H6]
			  ,[CREAT_DT]
			  ,[REC_STUS_ID]
			  ,[MODFD_DT]
			  ,[DD_APRVL_NBR]
			  ,[VAS_TYPE]
			  ,[CE_SRVC_ID]
			  ,[BDWD_NME]
		FROM	[COWS].[dbo].[MDS_EVENT_NTWK_TRPT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_NTWK_TRPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_NTWK_TRPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[MDS_EVENT_NTWK_TRPT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)  AND [CREAT_DT]<=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_NTWK_TRPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_NTWK_TRPT]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 	



		-- REDSGN
		RAISERROR ('****************         REDSGN                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[REDSGN] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[REDSGN](
			   [REDSGN_ID]
			  ,[REDSGN_NBR]
			  ,[H1_CD]
			  ,[NTE_ASSIGNED]
			  ,[PM_ASSIGNED]
			  ,[REDSGN_TYPE_ID]
			  ,[STUS_ID]
			  ,[REDSGN_DEV_STUS_ID]
			  ,[CRETD_BY_CD] 
			  ,[CRETD_DT]
			  ,[MODFD_BY_CD]
			  ,[MODFD_DT]
			  ,[SUBMIT_DT]
			  ,[MOD_SUBMIT_DT]
			  ,[SLA_DT]
			  ,[ODIE_CUST_ID]
			  ,[APPROV_NBR]
			  ,[COST_AMT]
			  ,[EXPRTN_DT]
			  ,[CSG_LVL_ID]
			  ,[EXT_XPIRN_FLG_CD]
			  ,[REDSGN_CAT_ID]
			  ,[DSGN_DOC_LOC_TXT]
			  ,[SDE_ASN_NME]
			  ,[NTE_LVL_EFFORT_AMT]
			  ,[PM_LVL_EFFORT_AMT]
			  ,[NE_LVL_EFFORT_AMT]
			  ,[NTE_OT_LVL_EFFORT_AMT] 
			  ,[PM_OT_LVL_EFFORT_AMT]
			  ,[NE_OT_LVL_EFFORT_AMT]
			  ,[SPS_LVL_EFFORT_AMT]
			  ,[NE_ASN_NME]
			  ,[SOWS_FOLDR_PATH_NME]
			  ,[SPS_OT_LVL_EFFORT_AMT]
			  ,[BILL_OVRRDN_CD]
			  ,[BILL_OVRRDN_AMT]
			  ,[IS_SDWAN]
			  ,[CUST_NME]
			  ,[CUST_EMAIL_ADR]
			  ,[DSGN_DOC_NBR_BPM]
			  ,[CISC_SMRT_LIC_CD]
			  ,[SMRT_ACCNT_DMN]
			  ,[MSS_IMPL_EST_AMT]
			  ,[VRTL_ACCNT]		  )
		SELECT [REDSGN_ID]
			  ,[REDSGN_NBR]
			  ,[H1_CD]
			  ,[NTE_ASSIGNED]
			  ,[PM_ASSIGNED]
			  ,[REDSGN_TYPE_ID]
			  ,[STUS_ID]
			  ,[REDSGN_DEV_STUS_ID]
			  ,[CRETD_BY_CD] 
			  ,[CRETD_DT]
			  ,[MODFD_BY_CD]
			  ,[MODFD_DT]
			  ,[SUBMIT_DT]
			  ,[MOD_SUBMIT_DT]
			  ,[SLA_DT]
			  ,[ODIE_CUST_ID]
			  ,[APPROV_NBR]
			  ,[COST_AMT]
			  ,[EXPRTN_DT]
			  ,[CSG_LVL_ID]
			  ,[EXT_XPIRN_FLG_CD]
			  ,[REDSGN_CAT_ID]
			  ,[DSGN_DOC_LOC_TXT]
			  ,[SDE_ASN_NME]
			  ,[NTE_LVL_EFFORT_AMT]
			  ,[PM_LVL_EFFORT_AMT]
			  ,[NE_LVL_EFFORT_AMT]
			  ,[NTE_OT_LVL_EFFORT_AMT] 
			  ,[PM_OT_LVL_EFFORT_AMT]
			  ,[NE_OT_LVL_EFFORT_AMT]
			  ,[SPS_LVL_EFFORT_AMT]
			  ,[NE_ASN_NME]
			  ,[SOWS_FOLDR_PATH_NME]
			  ,[SPS_OT_LVL_EFFORT_AMT]
			  ,[BILL_OVRRDN_CD]
			  ,[BILL_OVRRDN_AMT]
			  ,[IS_SDWAN]
			  ,[CUST_NME]
			  ,[CUST_EMAIL_ADR]
			  ,[DSGN_DOC_NBR_BPM]
			  ,[CISC_SMRT_LIC_CD]
			  ,[SMRT_ACCNT_DMN]
			  ,[MSS_IMPL_EST_AMT]
			  ,[VRTL_ACCNT]
		FROM	[COWS].[dbo].[REDSGN] WITH(NOLOCK)	WHERE [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[REDSGN] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[REDSGN] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[REDSGN] WITH(TABLOCKX)	WHERE  [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[REDSGN] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[REDSGN]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 



		-- REDSGN_CUST_BYPASS
		RAISERROR ('****************         REDSGN_CUST_BYPASS                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[REDSGN_CUST_BYPASS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[REDSGN_CUST_BYPASS](
			   [ID]
			  ,[H1_CD]
			  ,[REC_STUS_ID]
			  ,[CREAT_DT]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_DT]
			  ,[MODFD_BY_USER_ID]
			  ,[CSG_LVL_ID]
			  ,[CUST_NME] )
		SELECT [ID]
			  ,[H1_CD]
			  ,[REC_STUS_ID]
			  ,[CREAT_DT]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_DT]
			  ,[MODFD_BY_USER_ID]
			  ,[CSG_LVL_ID]
			  ,[CUST_NME]

		FROM	[COWS].[dbo].[REDSGN_CUST_BYPASS] WITH(NOLOCK)	WHERE [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[REDSGN_CUST_BYPASS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[REDSGN_CUST_BYPASS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[REDSGN_CUST_BYPASS] WITH(TABLOCKX)	WHERE  [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[REDSGN_CUST_BYPASS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[REDSGN_CUST_BYPASS]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- REDSGN_DEV_EVENT_HIST
		RAISERROR ('****************         REDSGN_DEV_EVENT_HIST                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[REDSGN_DEV_EVENT_HIST] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[REDSGN_DEV_EVENT_HIST](
			   [REDSGN_DEV_EVENT_HIST_ID]
			  ,[REDSGN_DEV_ID]
			  ,[EVENT_ID]
			  ,[CMPLTD_DT] )
		SELECT [REDSGN_DEV_EVENT_HIST_ID]
			  ,[REDSGN_DEV_ID]
			  ,[EVENT_ID]
			  ,[CMPLTD_DT]
		FROM	[COWS].[dbo].[REDSGN_DEV_EVENT_HIST] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CMPLTD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[REDSGN_DEV_EVENT_HIST] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[REDSGN_DEV_EVENT_HIST] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[REDSGN_DEV_EVENT_HIST] WITH(TABLOCKX)	WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CMPLTD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[REDSGN_DEV_EVENT_HIST] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[REDSGN_DEV_EVENT_HIST]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- REDSGN_DEVICES_INFO
		RAISERROR ('****************         REDSGN_DEVICES_INFO                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[REDSGN_DEVICES_INFO] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[REDSGN_DEVICES_INFO](
			   [REDSGN_DEV_ID]
			  ,[REDSGN_ID]
			  ,[SEQ_NBR]
			  ,[DEV_NME]
			  ,[FAST_TRK_CD]
			  ,[DSPCH_RDY_CD]
			  ,[REC_STUS_ID]
			  ,[CRETD_BY_CD]
			  ,[CRETD_DT]
			  ,[MODFD_BY_CD]
			  ,[MODFD_DT]
			  ,[NTE_CHRG_CD]
			  ,[DEV_BILL_DT]
			  ,[DEV_CMPLTN_CD]
			  ,[H6_CUST_ID]
			  ,[EVENT_CMPLTN_DT]
			  ,[SC_CD]		  )
		SELECT [REDSGN_DEV_ID]
			  ,[REDSGN_ID]
			  ,[SEQ_NBR]
			  ,[DEV_NME]
			  ,[FAST_TRK_CD]
			  ,[DSPCH_RDY_CD]
			  ,[REC_STUS_ID]
			  ,[CRETD_BY_CD]
			  ,[CRETD_DT]
			  ,[MODFD_BY_CD]
			  ,[MODFD_DT]
			  ,[NTE_CHRG_CD]
			  ,[DEV_BILL_DT]
			  ,[DEV_CMPLTN_CD]
			  ,[H6_CUST_ID]
			  ,[EVENT_CMPLTN_DT]
			  ,[SC_CD]
		FROM	[COWS].[dbo].[REDSGN_DEVICES_INFO] WITH(NOLOCK)	WHERE REDSGN_ID	IN	(SELECT REDSGN_ID FROM	@Redsgn_Table) AND [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[REDSGN_DEVICES_INFO] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[REDSGN_DEVICES_INFO] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[REDSGN_DEVICES_INFO] WITH(TABLOCKX)	WHERE  REDSGN_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[REDSGN_DEVICES_INFO] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[REDSGN_DEVICES_INFO]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 




		-- REDSGN_DOC
		RAISERROR ('****************         REDSGN_DOC                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[REDSGN_DOC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[REDSGN_DOC](
			   [REDSGN_DOC_ID]
			  ,[REDSGN_ID]
			  ,[FILE_NME]
			  ,[FILE_CNTNT]
			  ,[FILE_SIZE_QTY]
			  ,[CREAT_DT]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_BY_USER_ID]
			  ,[REC_STUS_ID]
			  ,[REDSGN_DEV_STUS_ID]
			  ,[MODFD_DT]	)
		SELECT [REDSGN_DOC_ID]
			  ,[REDSGN_ID]
			  ,[FILE_NME]
			  ,[FILE_CNTNT]
			  ,[FILE_SIZE_QTY]
			  ,[CREAT_DT]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_BY_USER_ID]
			  ,[REC_STUS_ID]
			  ,[REDSGN_DEV_STUS_ID]
			  ,[MODFD_DT]
		FROM	[COWS].[dbo].[REDSGN_DOC] WITH(NOLOCK)	WHERE REDSGN_ID	IN	(SELECT REDSGN_ID FROM	@Redsgn_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[REDSGN_DOC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[REDSGN_DOC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[REDSGN_DOC] WITH(TABLOCKX)	WHERE  REDSGN_ID	IN	(SELECT REDSGN_ID FROM	@Redsgn_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[REDSGN_DOC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[REDSGN_DOC]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 



		-- REDSGN_EMAIL_NTFCTN
		RAISERROR ('****************         REDSGN_EMAIL_NTFCTN                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[REDSGN_EMAIL_NTFCTN] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[REDSGN_EMAIL_NTFCTN](
			   [REDSGN_EMAIL_NTFCTN_ID]
			  ,[REDSGN_ID]
			  ,[USER_IDNTFCTN_EMAIL]
			  ,[REC_STUS_ID]
			  ,[CRETD_USER_ID]
			  ,[CRETD_DT]	)
		SELECT [REDSGN_EMAIL_NTFCTN_ID]
			  ,[REDSGN_ID]
			  ,[USER_IDNTFCTN_EMAIL]
			  ,[REC_STUS_ID]
			  ,[CRETD_USER_ID]
			  ,[CRETD_DT]
		FROM	[COWS].[dbo].[REDSGN_EMAIL_NTFCTN] WITH(NOLOCK)	WHERE REDSGN_ID	IN	(SELECT REDSGN_ID FROM	@Redsgn_Table) AND [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[REDSGN_EMAIL_NTFCTN] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[REDSGN_EMAIL_NTFCTN] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[REDSGN_EMAIL_NTFCTN] WITH(TABLOCKX)	WHERE  REDSGN_ID	IN	(SELECT REDSGN_ID FROM	@Redsgn_Table) AND [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[REDSGN_EMAIL_NTFCTN] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[REDSGN_EMAIL_NTFCTN]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- REDSGN_H1_MRC_VALUES
		RAISERROR ('****************         REDSGN_H1_MRC_VALUES                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[REDSGN_H1_MRC_VALUES] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[REDSGN_H1_MRC_VALUES](
			   [H1_ID]
			  ,[REDSGN_CAT_ID]
			  ,[MRC_RATE_VS_NRC]
			  ,[REC_STUS_ID]	)
		SELECT [H1_ID]
			  ,[REDSGN_CAT_ID]
			  ,[MRC_RATE_VS_NRC]
			  ,[REC_STUS_ID]
		FROM	[COWS].[dbo].[REDSGN_H1_MRC_VALUES] WITH(NOLOCK) WHERE REDSGN_CAT_ID IN	(SELECT REDSGN_CAT_ID FROM	LK_REDSGN_CAT)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[REDSGN_H1_MRC_VALUES] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[REDSGN_H1_MRC_VALUES] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[REDSGN_H1_MRC_VALUES] WITH(TABLOCKX) WHERE REDSGN_CAT_ID	IN	(SELECT REDSGN_CAT_ID FROM	LK_REDSGN_CAT)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[REDSGN_H1_MRC_VALUES] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[REDSGN_H1_MRC_VALUES]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 



		-- REDSGN_NOTES
		RAISERROR ('****************         REDSGN_NOTES                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[REDSGN_NOTES] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[REDSGN_NOTES](
			   [REDSGN_NOTES_ID]
			  ,[REDSGN_ID]
			  ,[REDSGN_NTE_TYPE_ID]
			  ,[NOTES]
			  ,[CRETD_BY_CD]
			  ,[CRETD_DT]			  )
		SELECT [REDSGN_NOTES_ID]
			  ,[REDSGN_ID]
			  ,[REDSGN_NTE_TYPE_ID]
			  ,[NOTES]
			  ,[CRETD_BY_CD]
			  ,[CRETD_DT]	
		FROM	[COWS].[dbo].[REDSGN_NOTES] WITH(NOLOCK) WHERE REDSGN_ID	IN	(SELECT REDSGN_ID FROM	@Redsgn_Table) AND [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[REDSGN_NOTES] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[REDSGN_NOTES] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[REDSGN_NOTES] WITH(TABLOCKX) WHERE REDSGN_ID	IN	(SELECT REDSGN_ID FROM	@Redsgn_Table) AND [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[REDSGN_NOTES] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[REDSGN_NOTES]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- REDSGN_WRKFLW
		RAISERROR ('****************         REDSGN_WRKFLW                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[REDSGN_WRKFLW] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[REDSGN_WRKFLW](
			   [REDSGN_WRKFLW_ID]
			  ,[PRE_WRKFLW_STUS_ID]
			  ,[DESRD_WRKFLW_STUS_ID]
			  ,[REC_STUS_ID]
			  ,[CRETD_DT]		  )
		SELECT [REDSGN_WRKFLW_ID]
			  ,[PRE_WRKFLW_STUS_ID]
			  ,[DESRD_WRKFLW_STUS_ID]
			  ,[REC_STUS_ID]
			  ,[CRETD_DT]		
		FROM	[COWS].[dbo].[REDSGN_WRKFLW] WITH(NOLOCK) WHERE  [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[REDSGN_WRKFLW] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[REDSGN_WRKFLW] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[REDSGN_WRKFLW] WITH(TABLOCKX) WHERE [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[REDSGN_WRKFLW] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[REDSGN_WRKFLW]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 



		-- SIPT_EVENT_ACTY
		RAISERROR ('****************         SIPT_EVENT_ACTY                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SIPT_EVENT_ACTY] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[SIPT_EVENT_ACTY](
			   [EVENT_ID]
			  ,[SIPT_ACTY_TYPE_ID]
			  ,[CREAT_DT]		  )
		SELECT [EVENT_ID]
			  ,[SIPT_ACTY_TYPE_ID]
			  ,[CREAT_DT]		
		FROM	[COWS].[dbo].[SIPT_EVENT_ACTY] WITH(NOLOCK) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SIPT_EVENT_ACTY] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SIPT_EVENT_ACTY] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[SIPT_EVENT_ACTY] WITH(TABLOCKX) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SIPT_EVENT_ACTY] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SIPT_EVENT_ACTY]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 



		-- SIPT_EVENT_DOC
		RAISERROR ('****************         SIPT_EVENT_DOC                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SIPT_EVENT_DOC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[SIPT_EVENT_DOC](
			   [SIPT_EVENT_DOC_ID]
			  ,[EVENT_ID]
			  ,[FILE_NME]
			  ,[FILE_CNTNT]
			  ,[FILE_SIZE_QTY]
			  ,[REC_STUS_ID]
			  ,[CREAT_BY_USER_ID]
			  ,[CREAT_DT]		  )
		SELECT [SIPT_EVENT_DOC_ID]
			  ,[EVENT_ID]
			  ,[FILE_NME]
			  ,[FILE_CNTNT]
			  ,[FILE_SIZE_QTY]
			  ,[REC_STUS_ID]
			  ,[CREAT_BY_USER_ID]
			  ,[CREAT_DT]		
		FROM	[COWS].[dbo].[SIPT_EVENT_DOC] WITH(NOLOCK) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SIPT_EVENT_DOC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SIPT_EVENT_DOC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[SIPT_EVENT_DOC] WITH(TABLOCKX) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SIPT_EVENT_DOC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SIPT_EVENT_DOC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- SIPT_EVENT_TOLL_TYPE
		RAISERROR ('****************         SIPT_EVENT_TOLL_TYPE                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SIPT_EVENT_TOLL_TYPE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[SIPT_EVENT_TOLL_TYPE](
			   [EVENT_ID]
			  ,[M5_ORDR_NBR]
			  ,[CRETD_DT]			  )
		SELECT [EVENT_ID]
			  ,[M5_ORDR_NBR]
			  ,[CRETD_DT]		
		FROM	[COWS].[dbo].[SIPT_EVENT_TOLL_TYPE] WITH(NOLOCK) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SIPT_EVENT_TOLL_TYPE] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SIPT_EVENT_TOLL_TYPE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[SIPT_EVENT_TOLL_TYPE] WITH(TABLOCKX) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CRETD_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SIPT_EVENT_TOLL_TYPE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SIPT_EVENT_TOLL_TYPE]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- SIPT_RELTD_ORDR
		RAISERROR ('****************         SIPT_RELTD_ORDR                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SIPT_RELTD_ORDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[SIPT_RELTD_ORDR](
			   [SIPT_RELTD_ORDR_ID]
			  ,[EVENT_ID]
			  ,[M5_ORDR_NBR]
			  ,[CREAT_DT]			  )
		SELECT [SIPT_RELTD_ORDR_ID]
			  ,[EVENT_ID]
			  ,[M5_ORDR_NBR]
			  ,[CREAT_DT]		
		FROM	[COWS].[dbo].[SIPT_RELTD_ORDR] WITH(NOLOCK) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SIPT_RELTD_ORDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SIPT_RELTD_ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[SIPT_RELTD_ORDR] WITH(TABLOCKX) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SIPT_RELTD_ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SIPT_RELTD_ORDR]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- SPLK_EVENT_ACCS
		RAISERROR ('****************         SPLK_EVENT_ACCS                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SPLK_EVENT_ACCS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO  [arch].[SPLK_EVENT_ACCS](
			[SPLK_EVENT_ACCS_ID]   ,
			[EVENT_ID]  ,
			[OLD_PL_NBR]  ,
			[OLD_PORT_SPEED_DES]  ,
			[NEW_PL_NBR]  ,
			[NEW_PORT_SPEED_DES]  ,
			[CREAT_DT]  ,
			[OLD_VPI_VCI_DLCI_NME]  ,
			[NEW_VPI_VCI_DLCI_NME]  )
			
		SELECT  [SPLK_EVENT_ACCS_ID]   ,
			[EVENT_ID]  ,
			[OLD_PL_NBR]  ,
			[OLD_PORT_SPEED_DES]  ,
			[NEW_PL_NBR]  ,
			[NEW_PORT_SPEED_DES]  ,
			[CREAT_DT]  ,
			[OLD_VPI_VCI_DLCI_NME]  ,
			[NEW_VPI_VCI_DLCI_NME] 
		FROM	[COWS].[dbo].[SPLK_EVENT_ACCS] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table ) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SPLK_EVENT_ACCS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SPLK_EVENT_ACCS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[SPLK_EVENT_ACCS] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SPLK_EVENT_ACCS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SPLK_EVENT_ACCS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- SPLK_EVENT
		RAISERROR ('****************         SPLK_EVENT                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SPLK_EVENT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[SPLK_EVENT](
			   [EVENT_ID]
			  ,[EVENT_STUS_ID]
			  ,[FTN]
			  ,[CHARS_ID]
			  ,[H1]
			  ,[H6]
			  ,[REQOR_USER_ID]
			  ,[SALS_USER_ID]
			  ,[PUB_EMAIL_CC_TXT]
			  ,[CMPLTD_EMAIL_CC_TXT]
			  ,[DSGN_CMNT_TXT]
			  ,[SPLK_EVENT_TYPE_ID]
			  ,[SPLK_ACTY_TYPE_ID]
			  ,[IP_VER_ID]
			  ,[MDS_MNGD_CD]
			  ,[ESCL_CD]
			  ,[PRIM_REQ_DT]
			  ,[SCNDY_REQ_DT]
			  ,[ESCL_REAS_ID]
			  ,[STRT_TMST]
			  ,[EXTRA_DRTN_TME_AMT]
			  ,[END_TMST]
			  ,[WRKFLW_STUS_ID]
			  ,[REC_STUS_ID]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_BY_USER_ID]
			  ,[MODFD_DT]
			  ,[CREAT_DT]
			  ,[RELTD_CMPS_NCR_NME]
			  ,[CNFRC_BRDG_NBR]
			  ,[CNFRC_PIN_NBR]
			  ,[EVENT_DRTN_IN_MIN_QTY]
			  ,[SOWS_EVENT_ID]
			  ,[RELTD_CMPS_NCR_CD]
			  ,[CUST_NME]
			  ,[CUST_CNTCT_NME]
			  ,[CUST_CNTCT_PHN_NBR]
			  ,[CUST_EMAIL_ADR]
			  ,[CUST_CNTCT_CELL_PHN_NBR]
			  ,[CUST_CNTCT_PGR_NBR]
			  ,[CUST_CNTCT_PGR_PIN_NBR]
			  ,[EVENT_TITLE_TXT]
			  ,[EVENT_DES]			)
		SELECT [EVENT_ID]
			  ,[EVENT_STUS_ID]
			  ,[FTN]
			  ,[CHARS_ID]
			  ,[H1]
			  ,[H6]
			  ,[REQOR_USER_ID]
			  ,[SALS_USER_ID]
			  ,[PUB_EMAIL_CC_TXT]
			  ,[CMPLTD_EMAIL_CC_TXT]
			  ,[DSGN_CMNT_TXT]
			  ,[SPLK_EVENT_TYPE_ID]
			  ,[SPLK_ACTY_TYPE_ID]
			  ,[IP_VER_ID]
			  ,[MDS_MNGD_CD]
			  ,[ESCL_CD]
			  ,[PRIM_REQ_DT]
			  ,[SCNDY_REQ_DT]
			  ,[ESCL_REAS_ID]
			  ,[STRT_TMST]
			  ,[EXTRA_DRTN_TME_AMT]
			  ,[END_TMST]
			  ,[WRKFLW_STUS_ID]
			  ,[REC_STUS_ID]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_BY_USER_ID]
			  ,[MODFD_DT]
			  ,[CREAT_DT]
			  ,[CNFRC_BRDG_NBR]
			  ,[CNFRC_PIN_NBR]
			  ,[EVENT_DRTN_IN_MIN_QTY]
			  ,[SOWS_EVENT_ID]
			  ,[RELTD_CMPS_NCR_CD]
			  ,[RELTD_CMPS_NCR_NME]
			  ,[CUST_NME]
			  ,[CUST_CNTCT_NME]
			  ,[CUST_CNTCT_PHN_NBR]
			  ,[CUST_EMAIL_ADR]
			  ,[CUST_CNTCT_CELL_PHN_NBR]
			  ,[CUST_CNTCT_PGR_NBR]
			  ,[CUST_CNTCT_PGR_PIN_NBR]
			  ,[EVENT_TITLE_TXT]
			  ,[EVENT_DES]		
		FROM	[COWS].[dbo].[SPLK_EVENT] WITH(NOLOCK) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SPLK_EVENT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SPLK_EVENT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[SPLK_EVENT] WITH(TABLOCKX) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SPLK_EVENT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SPLK_EVENT]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 




		-- UCaaS_EVENT
		RAISERROR ('****************         UCaaS_EVENT                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[UCaaS_EVENT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[UCaaS_EVENT](
			   [EVENT_ID]
			  ,[EVENT_STUS_ID]
			  ,[H1]
			  ,[CHARS_ID]
			  ,[CUST_ACCT_TEAM_PDL_NME]
			  ,[CUST_SOW_LOC_TXT]
			  ,[UCaaS_PROD_TYPE_ID]
			  ,[UCaaS_PLAN_TYPE_ID]
			  ,[UCaaS_ACTY_TYPE_ID]
			  ,[SHRT_DES]
			  ,[H6]
			  ,[CCD]
			  ,[SITE_ID]
			  ,[INSTL_SITE_POC_INTL_PHN_CD]
			  ,[INSTL_SITE_POC_INTL_CELL_PHN_CD]
			  ,[SRVC_ASSRN_POC_INTL_PHN_CD]
			  ,[SRVC_ASSRN_POC_INTL_CELL_PHN_CD]
			  ,[US_INTL_CD]
			  ,[SPRINT_CPE_NCR_ID]
			  ,[CPE_DSPCH_EMAIL_ADR]
			  ,[CPE_DSPCH_CMNT_TXT]
			  ,[ESCL_REAS_ID]
			  ,[PRIM_REQ_DT]
			  ,[SCNDY_REQ_DT]
			  ,[BUS_JUSTN_TXT]
			  ,[CNFRC_BRDG_ID]
			  ,[CNFRC_BRDG_NBR]
			  ,[CNFRC_PIN_NBR]
			  ,[ONLINE_MEETING_ADR]
			  ,[PUB_EMAIL_CC_TXT]
			  ,[CMPLTD_EMAIL_CC_TXT]
			  ,[REQOR_USER_ID]
			  ,[REQOR_USER_CELL_PHN_NBR]
			  ,[CMNT_TXT]
			  ,[STRT_TMST]
			  ,[END_TMST]
			  ,[EXTRA_DRTN_TME_AMT]
			  ,[EVENT_DRTN_IN_MIN_QTY]
			  ,[WRKFLW_STUS_ID]
			  ,[OLD_EVENT_STUS_ID]
			  ,[FAIL_REAS_ID]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_BY_USER_ID]
			  ,[MODFD_DT]
			  ,[CREAT_DT]	
			  ,[REC_STUS_ID]
			  ,[DISC_MGMT_CD]
			  ,[FULL_CUST_DISC_CD]
			  ,[FULL_CUST_DISC_REAS_TXT]	
			  ,[UCaaS_DESGN_DOC]
			  ,[MNS_PM_ID]
			  ,[STREET_ADR]
			  ,[EVENT_TITLE_TXT]	
			  ,[CUST_NME]
			  ,[INSTL_SITE_POC_NME]
			  ,[INSTL_SITE_POC_PHN_NBR]
			  ,[INSTL_SITE_POC_CELL_PHN_NBR]	
			  ,[SRVC_ASSRN_POC_NME]
			  ,[SRVC_ASSRN_POC_PHN_NBR]
			  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
			  ,[FLR_BLDG_NME]
			  ,[CTY_NME]
			  ,[STT_PRVN_NME]
			  ,[CTRY_RGN_NME]
			  ,[ZIP_CD]			  )
		SELECT [EVENT_ID]
			  ,[EVENT_STUS_ID]
			  ,[H1]
			  ,[CHARS_ID]
			  ,[CUST_ACCT_TEAM_PDL_NME]
			  ,[CUST_SOW_LOC_TXT]
			  ,[UCaaS_PROD_TYPE_ID]
			  ,[UCaaS_PLAN_TYPE_ID]
			  ,[UCaaS_ACTY_TYPE_ID]
			  ,[SHRT_DES]
			  ,[H6]
			  ,[CCD]
			  ,[SITE_ID]
			  ,[INSTL_SITE_POC_INTL_PHN_CD]
			  ,[INSTL_SITE_POC_INTL_CELL_PHN_CD]
			  ,[SRVC_ASSRN_POC_INTL_PHN_CD]
			  ,[SRVC_ASSRN_POC_INTL_CELL_PHN_CD]
			  ,[US_INTL_CD]
			  ,[SPRINT_CPE_NCR_ID]
			  ,[CPE_DSPCH_EMAIL_ADR]
			  ,[CPE_DSPCH_CMNT_TXT]
			  ,[CCD]
			  ,[ESCL_REAS_ID]
			  ,[PRIM_REQ_DT]
			  ,[SCNDY_REQ_DT]
			  ,[BUS_JUSTN_TXT]
			  ,[CNFRC_BRDG_ID]
			  ,[CNFRC_BRDG_NBR]
			  ,[CNFRC_PIN_NBR]
			  ,[ONLINE_MEETING_ADR]
			  ,[PUB_EMAIL_CC_TXT]
			  ,[CMPLTD_EMAIL_CC_TXT]
			  ,[REQOR_USER_ID]
			  ,[REQOR_USER_CELL_PHN_NBR]
			  ,[CMNT_TXT]
			  ,[STRT_TMST]
			  ,[END_TMST]
			  ,[EXTRA_DRTN_TME_AMT]
			  ,[EVENT_DRTN_IN_MIN_QTY]
			  ,[WRKFLW_STUS_ID]
			  ,[OLD_EVENT_STUS_ID]
			  ,[FAIL_REAS_ID]
			  ,[CREAT_BY_USER_ID]
			  ,[MODFD_BY_USER_ID]
			  ,[MODFD_DT]
			  ,[CREAT_DT]	
			  ,[REC_STUS_ID]
			  ,[DISC_MGMT_CD]
			  ,[FULL_CUST_DISC_CD]
			  ,[FULL_CUST_DISC_REAS_TXT]	
			  ,[UCaaS_DESGN_DOC]
			  ,[MNS_PM_ID]
			  ,[STREET_ADR]
			  ,[EVENT_TITLE_TXT]	
			  ,[CUST_NME]
			  ,[INSTL_SITE_POC_NME]
			  ,[INSTL_SITE_POC_PHN_NBR]
			  ,[INSTL_SITE_POC_CELL_PHN_NBR]	
			  ,[SRVC_ASSRN_POC_NME]
			  ,[SRVC_ASSRN_POC_PHN_NBR]
			  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
			  ,[FLR_BLDG_NME]
			  ,[CTY_NME]
			  ,[STT_PRVN_NME]
			  ,[CTRY_RGN_NME]
			  ,[ZIP_CD]		
		FROM	[COWS].[dbo].[UCaaS_EVENT] WITH(NOLOCK) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[UCaaS_EVENT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[UCaaS_EVENT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[UCaaS_EVENT] WITH(TABLOCKX) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[UCaaS_EVENT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[UCaaS_EVENT]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- UCaaS_EVENT_BILLING
		RAISERROR ('****************         UCaaS_EVENT_BILLING                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[UCaaS_EVENT_BILLING] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[UCaaS_EVENT_BILLING](
			   [UCaaS_EVENT_BILLING_ID]
			  ,[EVENT_ID]
			  ,[UCaaS_BILL_ACTY_ID]	
			  ,[CREAT_DT]	
			  ,[INC_QTY]	
			  ,[DCR_QTY]			  )
		SELECT [UCaaS_EVENT_BILLING_ID]
			  ,[EVENT_ID]
			  ,[UCaaS_BILL_ACTY_ID]	
			  ,[CREAT_DT]	
			  ,[INC_QTY]	
			  ,[DCR_QTY]		
		FROM	[COWS].[dbo].[UCaaS_EVENT_BILLING] WITH(NOLOCK) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[UCaaS_EVENT_BILLING] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[UCaaS_EVENT_BILLING] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[UCaaS_EVENT_BILLING] WITH(TABLOCKX) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[UCaaS_EVENT_BILLING] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[UCaaS_EVENT_BILLING]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 


		-- UCaaS_EVENT_ODIE_DEV
		RAISERROR ('****************         UCaaS_EVENT_ODIE_DEV                 *****************', 0, 1) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[UCaaS_EVENT_ODIE_DEV] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO [arch].[UCaaS_EVENT_ODIE_DEV](
			   [UCaaS_EVENT_ODIE_DEV_ID]
			  ,[EVENT_ID]
			  ,[RDSN_NBR]	
			  ,[RDSN_EXP_DT]	
			  ,[ODIE_DEV_NME]	
			  ,[DEV_MODEL_ID]	
			  ,[MANF_ID]	
			  ,[CREAT_DT]		  )
		SELECT [UCaaS_EVENT_ODIE_DEV_ID]
			  ,[EVENT_ID]
			  ,[RDSN_NBR]	
			  ,[RDSN_EXP_DT]	
			  ,[ODIE_DEV_NME]	
			  ,[DEV_MODEL_ID]	
			  ,[MANF_ID]	
			  ,[CREAT_DT]		
		FROM	[COWS].[dbo].[UCaaS_EVENT_ODIE_DEV] WITH(NOLOCK) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[UCaaS_EVENT_ODIE_DEV] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[UCaaS_EVENT_ODIE_DEV] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
		DELETE  FROM	[COWS].[dbo].[UCaaS_EVENT_ODIE_DEV] WITH(TABLOCKX) WHERE  EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [CREAT_DT] <=CONVERT(datetime,@Date)
		Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[UCaaS_EVENT_ODIE_DEV] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[UCaaS_EVENT_ODIE_DEV]',@RowCounts,CONVERT(VARCHAR(30), GETDATE(), 9) 


		COMMIT TRAN -- Transaction Success!
	END TRY
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK TRAN --RollBack in case of Error

		-- <EDIT>: From SQL2008 on, you must raise error messages as follows:
		DECLARE @ErrorMessage NVARCHAR(4000);  
		DECLARE @ErrorSeverity INT;  
		DECLARE @ErrorState INT;  

		SELECT   
		   @ErrorMessage = ERROR_MESSAGE(),  
		   @ErrorSeverity = ERROR_SEVERITY(),  
		   @ErrorState = ERROR_STATE();  

		RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);  
		-- </EDIT>
	END CATCH

END	