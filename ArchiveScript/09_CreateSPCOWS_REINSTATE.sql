USE [COWS]
GO

/****** Object:  StoredProcedure [arch].[COWS_Archive]    Script Date: 12/24/2014 13:51:36 ******/

GO

SET QUOTED_IDENTIFIER ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---- =============================================
---- Author:		Md. Mahmud Monir
---- Create date:   12/09/2014
---- Description:	This SP is used to archive.
----				ALL  DBO table record has to be arcived  not just  secured Delete  tables
---- Corrected date:   02/07/2015
---- Description:	Modified to adjust to EventType and Ordr
---- Corrected date:   02/10/2015
---- Description:	Modified to use RaiseError instead print
---- =============================================

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[arch].[spCOWS_REINSTATE]') AND type in (N'P', N'PC'))
DROP PROCEDURE [arch].[spCOWS_REINSTATE]
GO
CREATE PROCEDURE [arch].[spCOWS_REINSTATE](
	 @RefORDNumber    VARCHAR(MAX) , 
	 @RefEVNTNumber   VARCHAR(MAX) , 
	 @Mood			  int,   
	 @Date			  DateTime
	)
AS
--DECLARE @Date Datetime
--SET @Date = '2011-03-03'
BEGIN



/*
---- =============================================
---- Author:		Md. Mahmud Monir
----				
---- Create date:   12/24/2014
---- Description:	This SP is used to Create archive Tables in the COWS Database.
---- UPDATE date:   02/06/2015
---- Description:	Archive/Affect only  Related table per loop
---- Corrected date:   04/10/2018
---- Description:	Added Change done by CUST_SCRD_OBJ
---- =============================================
*/

/*Find All FK contraint of all tables in DB*/
/*
SELECT 
    f.name AS ForeignKey,
    OBJECT_NAME(f.parent_object_id) AS TableName,
    COL_NAME(fc.parent_object_id,
    fc.parent_column_id) AS ColumnName,
    OBJECT_NAME (f.referenced_object_id) AS ReferenceTableName,
    COL_NAME(fc.referenced_object_id,
    fc.referenced_column_id) AS ReferenceColumnName
FROM 
    sys.foreign_keys AS f
    INNER JOIN sys.foreign_key_columns AS fc ON f.OBJECT_ID = fc.constraint_object_id
------------------------------------------------------------------------------------------------
@Mood=???
		@Mood=0=ORDR Only No Eevents Table  , @Event_type_ID = 1=AD, @Event_type_ID = 2=NGVN 
		@Event_type_ID = 3=MPLS, @Event_type_ID = 4= SPLK 
		@Event_type_ID = 5=MDS tables  and both ORDR Table As well, 
		@Mood = 6 = MDS EVENTS only Tables  No Order no other events also. 
------------------------------------------------------------------------------------------------
																	Mood	ORDER	EVENTS
------------------------------------------------------------------------------------------------
@Mood=0=ORDR Only No Eevents Table   								0		Y		N
@Event_type_ID = 1=AD 												1		N		Y (AD)
@Event_type_ID = 2=NGVN 											2		N		Y (NGVN)
@Event_type_ID = 3=MPLS 											3		N		Y (MPLS)
@Event_type_ID = 4= SPLK 											4		N		Y (SPLK)
@Event_type_ID = 5=MDS tables  and both ORDR Table As well	 		5		Y		Y (MDS)
@Mood = 6 = MDS EVENTS only Tables  No Order no other events also 	6		N		Y (MDS)

------------------------------------------------------------------------------------------------
*/

	SET NOCOUNT ON;
	SET	DEADLOCK_PRIORITY	10;
	DECLARE @Time nvarchar(30)=GetDate()
	RAISERROR ('started [COWS].[arch].[spCOWS_REINSTATE]  SP', 0, 1) WITH NOWAIT


	RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
	RAISERROR ('****************         BEGIN REINSTATE PROCESS                  *****************', 0, 1) WITH NOWAIT
	RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT

	--PRINT ''
	Set @Time=GetDate()   RAISERROR ('Begin Defining Variables at: %s. ', 0, 1,@Time) WITH NOWAIT

	
	Declare @RowCounts  Int
	SET @RowCounts=0
	
	DECLARE     @Order_Table   TABLE(
				ID			INT IDENTITY(1,1),
				ORDR_ID		Int
	)
	DECLARE	@RECID	TABLE (
	         ID			INT IDENTITY(1,1),
			 REQ_ID	INT)
	DECLARE	@CKTID	TABLE (
	         ID			INT IDENTITY(1,1),
			 CKT_ID	VARCHAR(15))
	DECLARE @VNDRORDRID TABLE (
			 ID			INT IDENTITY(1,1),
			 VNDR_ORDR_ID	INT)
	DECLARE	@FTN	TABLE (
	         ID			INT IDENTITY(1,1),
			 FTN	VARCHAR(50))		 
	
	DECLARE     @Event_Table   TABLE(
				ID			INT IDENTITY(1,1),
				EVENT_ID		Int
	)
	DECLARE     @FSA_MDS_EVENT_ID  TABLE(
		 ID			INT IDENTITY(1,1),
		 FSA_MDS_EVENT_ID	INT)


	-------------------------------------------------------------------------------------------------------------
	----------------------------ORDER
	-------------------------------------------------------------------------------------------------------------
	IF(@Mood=0 OR @Mood=5)
	BEGIN
		Set @Time=GetDate()   RAISERROR ('Start Loading @Order_Table at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO @Order_Table     (ORDR_ID)
		select * FROM web.ParseCommaSeparatedIntegers(@RefORDNumber,',')
		
		--Select * from 	@Order_Table

		Set @Time=GetDate()   RAISERROR ('End Defining Variables at: %s. ', 0, 1,@Time) WITH NOWAIT
		Set @Time=GetDate()   RAISERROR ('Start Loading @VNDRORDRID at: %s. ', 0, 1,@Time) WITH NOWAIT
		
		INSERT INTO	@VNDRORDRID (VNDR_ORDR_ID)
		SELECT	DISTINCT VNDR_ORDR_ID
		FROM	COWS.arch.VNDR_ORDR WITH(NOLOCK) 
		WHERE	ORDR_ID IN(SELECT DISTINCT ORDR_ID FROM @Order_Table )

		Set @Time=GetDate()   RAISERROR ('Start Loading @CKTID at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO	@CKTID (CKT_ID)
		SELECT	DISTINCT RTRIM(LTRIM(CKT_ID))
		FROM	COWS.arch.CKT WITH(NOLOCK) 
		WHERE	ORDR_ID IN(SELECT DISTINCT ORDR_ID FROM @Order_Table )
		UNION
		SELECT	DISTINCT RTRIM(LTRIM(CKT_ID))
		FROM	COWS.arch.CKT WITH(NOLOCK) 
		WHERE	VNDR_ORDR_ID IN(SELECT DISTINCT VNDR_ORDR_ID FROM @VNDRORDRID )
		Set @RowCounts=@@RowCount 
		Set @Time=GetDate() RAISERROR ('Finished @CKTID INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		
		Set @Time=GetDate()   RAISERROR ('Start Loading @FTN at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO	@FTN (FTN)
		SELECT FTN FROM arch.FSA_ORDR WITH (NOLOCK)
		WHERE ORDR_ID IN (SELECT ORDR_ID FROM @Order_Table)
		Set @RowCounts=@@RowCount 
		Set @Time=GetDate() RAISERROR ('Finished @FTN INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT

		Set @Time=GetDate()   RAISERROR ('Start Loading @RECID at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO	@RECID (REQ_ID)
		SELECT	DISTINCT REQ_ID
		FROM	COWS.arch.ODIE_REQ WITH(NOLOCK) 
		WHERE	ORDR_ID IN(SELECT DISTINCT ORDR_ID FROM @Order_Table )
		Set @RowCounts=@@RowCount 
		Set @Time=GetDate() RAISERROR ('Finished @RECID INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
	END
	-------------------------------------------------------------------------------------------------------------
	----------------------------EVENT
	-------------------------------------------------------------------------------------------------------------
	
	
	IF(@Mood=1 OR @Mood=2 OR @Mood=3 OR @Mood=4 OR @Mood=5 OR @Mood=6)
	BEGIN	 
		Set @Time=GetDate()   RAISERROR ('Start Loading @Event_Table at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO @Event_Table     (EVENT_ID)	
		select * FROM web.ParseCommaSeparatedIntegers(@RefEVNTNumber,',')
		Set @RowCounts=@@RowCount 
		Set @Time=GetDate() RAISERROR ('Finished @Event_Table INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		Set @Time=GetDate()   RAISERROR ('End Defining Variables at: %s. ', 0, 1,@Time) WITH NOWAIT


		Set @Time=GetDate()   RAISERROR ('Start Loading @FSA_MDS_EVENT_ID at: %s. ', 0, 1,@Time) WITH NOWAIT
		INSERT INTO	@FSA_MDS_EVENT_ID (FSA_MDS_EVENT_ID)	
		SELECT  [FSA_MDS_EVENT_ID] 
		FROM	[COWS].[arch].[FSA_MDS_EVENT_NEW] WITH(NOLOCK)	
		WHERE   EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
		Set @RowCounts=@@RowCount 
		Set @Time=GetDate() RAISERROR ('Finished @FSA_MDS_EVENT_ID INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
		
		Set @Time=GetDate()   RAISERROR ('Start Loading @RECID at: %s. ', 0, 1,@Time) WITH NOWAIT

		INSERT INTO	@RECID (REQ_ID)
		SELECT	DISTINCT [REQ_ID] 
		FROM	[COWS].[arch].[ODIE_REQ] WITH(NOLOCK)	
		WHERE	[MDS_EVENT_ID]	IN	(SELECT EVENT_ID FROM	@Event_Table)
		Set @RowCounts=@@RowCount 
		Set @Time=GetDate() RAISERROR ('Finished @RECID INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
	END 

	DECLARE @Ctr1	INT
	SET		@Ctr1	= 0
	SELECT  @CTr1	= COUNT(1) FROM @Order_Table
	
	DECLARE @Ctr2	INT
	SET		@Ctr2	= 0
	SELECT  @CTr2	= COUNT(1) FROM @Event_Table
	
	Set @Time=GetDate()   RAISERROR ('End Taking Temp Data into Variables at: %s. ', 0, 1,@Time) WITH NOWAIT
	--PRINT ''
	BEGIN TRANSACTION
	
		BEGIN TRY	
		IF	(@Ctr1 > 0 OR @Ctr2 > 0)
		BEGIN 
			RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
			RAISERROR ('****************         BEGIN REINSTATE PROCESS COWS       *********************', 0, 1) WITH NOWAIT
			RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
			Set @Time=GetDate()   RAISERROR ('Begin REINSTATE the records at: %s. ', 0, 1,@Time)     WITH NOWAIT
			RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
			RAISERROR ('ORDER_IDs Choose for REINSTATE in this run are:  %s. ', 0, 1,@RefORDNumber)  WITH NOWAIT --PRINT @RefORDNumber
			RAISERROR ('EVENT_IDs Choose for REINSTATE in this run are:  %s. ', 0, 1,@RefEVNTNumber) WITH NOWAIT --PRINT @RefEVNTNumber
			RAISERROR ('Event Type/ Mood of this Run:  %d. ', 0, 1,@Mood) WITH NOWAIT
			RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
			
			
			
			/*
			--------------------------------------------ORDR-----------------------------------------------------------
			*/
			
			----------------------------------------[CUST_SCRD_DATA]---------ORDR------------------------------------------------
			/*
				05=FSA_ORDR_CUST
				06=H5_Foldr
				10=NCCO_ORDR
				12=ODIE_REQ  ------Not Reinstate
				13=ODIE_RSPN ------Not Reinstate
				14=ORDR_ADR
				15=ORDR_CNTCT
				
				----------------------------------------------------------------------------------------------------------------------
				*/
				
				
				---05=FSA_ORDR_CUST
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] For FSA_ORDR_CUST/ NCCO_ORDR/ORDR_ADR/ORDR_CNTCT INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[CUST_SCRD_DATA] (
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
					)
				SELECT 
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				FROM [COWS].[arch].[CUST_SCRD_DATA] csd with(NoLOCK) WHERE csd.SCRD_OBJ_ID IN	
				(SELECT DISTINCT FSA_ORDR_CUST_ID FROM COWS.arch.FSA_ORDR_CUST with(NoLOCK) WHERE ORDR_ID IN(SELECT ORDR_ID FROM @Order_Table)) 
				AND SCRD_OBJ_TYPE_ID =5  
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CUST_SCRD_DATA]  WITH(TABLOCKX)	WHERE SCRD_OBJ_ID IN	
				(SELECT DISTINCT FSA_ORDR_CUST_ID FROM COWS.arch.FSA_ORDR_CUST with(NoLOCK) WHERE ORDR_ID IN(SELECT ORDR_ID FROM @Order_Table)) 
				AND SCRD_OBJ_TYPE_ID =5   
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CUST_SCRD_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				--06=H5_Foldr
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] for [H5_FOLDR_ID] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[CUST_SCRD_DATA] (
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
					)
				SELECT 
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				FROM [COWS].[arch].[CUST_SCRD_DATA] csd with(NoLOCK) WHERE csd.SCRD_OBJ_ID IN	
				(SELECT DISTINCT [H5_FOLDR_ID] FROM  [COWS].[dbo].ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))  
				AND csd.SCRD_OBJ_TYPE_ID in(6) 
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CUST_SCRD_DATA] WITH(TABLOCKX)	WHERE SCRD_OBJ_ID IN	(SELECT DISTINCT [H5_FOLDR_ID] FROM  [COWS].[dbo].ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))  
				AND SCRD_OBJ_TYPE_ID in(6)  
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CUST_SCRD_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				---10=NCCO_ORDR
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] For FSA_ORDR_CUST/ NCCO_ORDR/ORDR_ADR/ORDR_CNTCT INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[CUST_SCRD_DATA] (
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
					)
				SELECT 
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				FROM [COWS].[arch].[CUST_SCRD_DATA] csd with(NoLOCK) WHERE csd.SCRD_OBJ_ID IN	
				(SELECT ORDR_ID FROM	@Order_Table)
				 AND SCRD_OBJ_TYPE_ID =10 
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CUST_SCRD_DATA]  WITH(TABLOCKX)	WHERE SCRD_OBJ_ID IN	
				(SELECT ORDR_ID FROM	@Order_Table)
				 AND SCRD_OBJ_TYPE_ID =10   
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CUST_SCRD_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				---14=ORDR_ADR
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] For FSA_ORDR_CUST/ NCCO_ORDR/ORDR_ADR/ORDR_CNTCT INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[CUST_SCRD_DATA] (
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
					)
				SELECT 
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				FROM [COWS].[arch].[CUST_SCRD_DATA] csd with(NoLOCK) WHERE csd.SCRD_OBJ_ID IN	
				(SELECT DISTINCT ORDR_ADR_ID FROM COWS.arch.ORDR_ADR with(NoLOCK) WHERE ORDR_ID IN(SELECT ORDR_ID FROM @Order_Table)) 
				AND SCRD_OBJ_TYPE_ID =14  
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CUST_SCRD_DATA]  WITH(TABLOCKX)	WHERE SCRD_OBJ_ID IN	
				(SELECT DISTINCT ORDR_ADR_ID FROM COWS.arch.ORDR_ADR with(NoLOCK) WHERE ORDR_ID IN(SELECT ORDR_ID FROM @Order_Table)) 
				AND SCRD_OBJ_TYPE_ID =14   
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CUST_SCRD_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				---15=ORDR_CNTCT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] For FSA_ORDR_CUST/ NCCO_ORDR/ORDR_ADR/ORDR_CNTCT INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[CUST_SCRD_DATA] (
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
					)
				SELECT 
					[ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				FROM [COWS].[arch].[CUST_SCRD_DATA] csd with(NoLOCK) WHERE csd.SCRD_OBJ_ID IN	
				(SELECT DISTINCT ORDR_CNTCT_ID FROM COWS.arch.ORDR_CNTCT with(NoLOCK) WHERE ORDR_ID IN(SELECT ORDR_ID FROM @Order_Table)) 
				AND SCRD_OBJ_TYPE_ID =15 
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[CUST_SCRD_DATA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CUST_SCRD_DATA]  WITH(TABLOCKX)	WHERE SCRD_OBJ_ID IN	
				(SELECT DISTINCT ORDR_CNTCT_ID FROM COWS.arch.ORDR_CNTCT with(NoLOCK) WHERE ORDR_ID IN(SELECT ORDR_ID FROM @Order_Table)) 
				AND SCRD_OBJ_TYPE_ID =15   
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CUST_SCRD_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			
			/*
			--------------------------------------------ORDR-----------------------------------------------------------
			*/
			
			
			
			/*
			--------------------------------------------EVENT-----------------------------------------------------------
			*/
			
			/*
				----------------------------------------------------------------------------------------------------------------------
				----------------------------------------[CUST_SCRD_DATA]---------EVENT------------------------------------------------
				1= AD_EVENT  
				9= MPLS_EVENT  
				11=NGVN_EVENT  
				19=SPLK_EVENT  
				03=FSA_MDS_EVENT  
				04=FSA_MDS_EVENT_NEW  
				07=MDS_EVENT  
				08=MDS_EVENT_NEW  
				18=SIPT_EVENT  
				20=UCaaS_Event 
				----------------------------------------------------------------------------------------------------------------------
				*/
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[CUST_SCRD_DATA] For Event Tables INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT  
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     ON
				INSERT INTO [COWS].[dbo].[CUST_SCRD_DATA] (
				    [ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
					)
				SELECT 
				    [ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				FROM [COWS].[arch].[CUST_SCRD_DATA] csd with(NoLOCK) WHERE csd.SCRD_OBJ_ID IN 
				(SELECT  DISTINCT EVENT_ID FROM @Event_Table)  AND csd.SCRD_OBJ_TYPE_ID in(1,7,8,9,11,18,19,20) 
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[CUST_SCRD_DATA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CUST_SCRD_DATA]  WITH(TABLOCKX)	WHERE SCRD_OBJ_ID IN 
				(SELECT  DISTINCT EVENT_ID FROM @Event_Table)  AND SCRD_OBJ_TYPE_ID in(1,7,8,9,11,18,19,20)  
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[arch].[CUST_SCRD_DATA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CUST_SCRD_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*  
				----------------------------------------------------------------------------------------------------------------------  
				----------------------------------------[CUST_SCRD_DATA]---------EVENT------------------------------------------------  
				03=FSA_MDS_EVENT  
				----------------------------------------------------------------------------------------------------------------------  
				*/
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[CUST_SCRD_DATA] For FSA_MDS_EVENT INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     ON
				INSERT INTO [COWS].[dbo].[CUST_SCRD_DATA] (
				    [ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
					)
				SELECT 
				    [ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				FROM [COWS].[arch].[CUST_SCRD_DATA] csd with(NoLOCK) WHERE csd.SCRD_OBJ_ID IN 
				(SELECT  DISTINCT fme.FSA_MDS_EVENT_ID FROM arch.FSA_MDS_EVENT fme INNER JOIN @Event_Table et ON et.EVENT_ID=fme.EVENT_ID)  
				AND csd.SCRD_OBJ_TYPE_ID in(3)    
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[CUST_SCRD_DATA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CUST_SCRD_DATA]  WITH(TABLOCKX)	WHERE SCRD_OBJ_ID IN 
				(SELECT  DISTINCT fme.FSA_MDS_EVENT_ID FROM arch.FSA_MDS_EVENT fme INNER JOIN @Event_Table et ON et.EVENT_ID=fme.EVENT_ID)  
				AND SCRD_OBJ_TYPE_ID in(3)     
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[arch].[CUST_SCRD_DATA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CUST_SCRD_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				/*  
				----------------------------------------------------------------------------------------------------------------------  
				----------------------------------------[CUST_SCRD_DATA]---------EVENT------------------------------------------------  
				04=FSA_MDS_EVENT_NEW  
				----------------------------------------------------------------------------------------------------------------------  
				*/
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[CUST_SCRD_DATA] For FSA_MDS_EVENT_NEW INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     ON
				INSERT INTO [COWS].[dbo].[CUST_SCRD_DATA] (
				    [ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
					)
				SELECT 
				    [ID]
				  ,[SCRD_OBJ_ID]
				  ,[SCRD_OBJ_TYPE_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[EVENT_DES]
				  ,[CUST_NME]
				  ,[CUST_EMAIL_ADR]
				  ,[CUST_CNTCT_NME]
				  ,[CUST_CNTCT_PHN_NBR]
				  ,[CUST_CNTCT_CELL_PHN_NBR]
				  ,[CUST_CNTCT_PGR_NBR]
				  ,[CUST_CNTCT_PGR_PIN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[BLDG_NME]
				  ,[CTY_NME]
				  ,[FLR_ID]
				  ,[STT_PRVN_NME]
				  ,[RM_NBR]
				  ,[SITE_ADR]
				  ,[STREET_ADR_1]
				  ,[STREET_ADR_2]
				  ,[STREET_ADR_3]
				  ,[STT_CD]
				  ,[ZIP_PSTL_CD]
				  ,[CTRY_RGN_NME]
				  ,[FRST_NME]
				  ,[LST_NME]
				  ,[NTE_TXT]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				FROM [COWS].[arch].[CUST_SCRD_DATA] csd with(NoLOCK) WHERE csd.SCRD_OBJ_ID IN 
			    (SELECT  DISTINCT fme.FSA_MDS_EVENT_ID FROM arch.FSA_MDS_EVENT_NEW fme INNER JOIN @Event_Table et ON et.EVENT_ID=fme.EVENT_ID)  
				AND csd.SCRD_OBJ_TYPE_ID in(4)    
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[dbo].[CUST_SCRD_DATA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. [CUST_SCRD_DATA]     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[CUST_SCRD_DATA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CUST_SCRD_DATA]  WITH(TABLOCKX)	WHERE SCRD_OBJ_ID IN 
				(SELECT  DISTINCT fme.FSA_MDS_EVENT_ID FROM arch.FSA_MDS_EVENT_NEW fme INNER JOIN @Event_Table et ON et.EVENT_ID=fme.EVENT_ID)  
				AND SCRD_OBJ_TYPE_ID in(4)     
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [COWS].[arch].[CUST_SCRD_DATA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CUST_SCRD_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
			/*
			--------------------------------------------EVENT-----------------------------------------------------------
			*/
			
			
			IF(@Mood=0)   /* ORDER ONLY*/
			BEGIN
				/*
				[arch].[ACT_TASK]

				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME		REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_ACT_TASK	ACT_TASK	ORDR_ID			ORDR				ORDR_ID
				FK02_ACT_TASK	ACT_TASK	WG_PROF_ID		LK_WG_PROF			WG_PROF_ID
				FK03_ACT_TASK	ACT_TASK	TASK_ID			LK_TASK				TASK_ID
				FK04_ACT_TASK	ACT_TASK	STUS_ID			LK_STUS				STUS_ID
				-------------------------------------------ok-------------------------ORDER----------------------------

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. ACT_TASK     ON
				Set @Time=GetDate()    RAISERROR ('Starting to execute [dbo].[ACT_TASK] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ACT_TASK](
						[ACT_TASK_ID]  ,
						[ORDR_ID]  ,
						[TASK_ID]  ,
						[STUS_ID]  ,
						[WG_PROF_ID]  ,
						[CREAT_DT],
						[MODFD_DT]  )
				SELECT  DISTINCT
						[ACT_TASK_ID]  ,
						A.[ORDR_ID]  ,
						[TASK_ID]  ,
						[STUS_ID]  ,
						[WG_PROF_ID]  ,
						[CREAT_DT] ,
						[MODFD_DT]
				FROM	[COWS].[arch].[ACT_TASK] A WITH(NOLOCK) INNER JOIN @Order_Table B ON A.ORDR_ID=B.ORDR_ID
				Set @RowCounts=@@RowCount Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ACT_TASK] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. ACT_TASK     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ACT_TASK] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				--DELETE  FROM	[COWS].[dbo].[ACT_TASK] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				DELETE  A FROM	[COWS].[arch].[ACT_TASK] A WITH(TABLOCKX) INNER JOIN @Order_Table B ON A.ORDR_ID=B.ORDR_ID  
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ACT_TASK] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ACT_TASK]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
							
			END
			IF(@Mood=1)  /* AD EVENT Only */
			BEGIN
				/*
				[AD_EVENT_ACCS_TAG]
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_AD_EVENT_ACCS_TAG	AD_EVENT_ACCS_TAG	EVENT_ID	AD_EVENT	EVENT_ID
				-------------------------------------------ok---------------------------EVENT------ORDER-->>-CKTID--------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[AD_EVENT_ACCS_TAG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO[dbo].[AD_EVENT_ACCS_TAG](
						[EVENT_ID]  ,
						[CKT_ID]  ,
						[CREAT_DT]  ,
						[OLD_CKT_ID] )
				SELECT  DISTINCT
						A.[EVENT_ID]  ,
						[CKT_ID]  ,
						[CREAT_DT]  ,
						[OLD_CKT_ID]	
				FROM	[COWS].[arch].[AD_EVENT_ACCS_TAG] A WITH(NOLOCK) INNER JOIN @Event_Table B ON A.[EVENT_ID]=B.[EVENT_ID]  
				UNION
				SELECT	DISTINCT
						[EVENT_ID]  ,
						A.[CKT_ID]  ,
						[CREAT_DT]  ,
						[OLD_CKT_ID]
				FROM	[COWS].[arch].[AD_EVENT_ACCS_TAG] A WITH(NOLOCK) INNER JOIN @CKTID B ON CONVERT(VARCHAR(15),A.CKT_ID)=B.CKT_ID  
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[AD_EVENT_ACCS_TAG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[AD_EVENT_ACCS_TAG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				 Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[AD_EVENT_ACCS_TAG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  A FROM	[COWS].[arch].[AD_EVENT_ACCS_TAG] A WITH(TABLOCKX) INNER JOIN @Event_Table B ON A.EVENT_ID=B.EVENT_ID 
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[AD_EVENT_ACCS_TAG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[AD_EVENT_ACCS_TAG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  A FROM	[COWS].[arch].[AD_EVENT_ACCS_TAG] A WITH(TABLOCKX) INNER JOIN @CKTID B ON CONVERT(VARCHAR(15),A.CKT_ID)=B.CKT_ID  
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[AD_EVENT_ACCS_TAG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				
				/*
				AD_EVENT


				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_AD_EVENT	AD_EVENT	WRKFLW_STUS_ID		LK_WRKFLW_STUS	WRKFLW_STUS_ID
				FK02_AD_EVENT	AD_EVENT	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID
				FK03_AD_EVENT	AD_EVENT	EVENT_STUS_ID		LK_EVENT_STUS	EVENT_STUS_ID
				FK04_AD_EVENT	AD_EVENT	REQOR_USER_ID		LK_USER	USER_ID
				FK05_AD_EVENT	AD_EVENT	SALS_USER_ID		LK_USER	USER_ID
				FK06_AD_EVENT	AD_EVENT	CREAT_BY_USER_ID	LK_USER	USER_ID
				FK07_AD_EVENT	AD_EVENT	MODFD_BY_USER_ID	LK_USER	USER_ID
				FK08_AD_EVENT	AD_EVENT	ESCL_REAS_ID		LK_ESCL_REAS	ESCL_REAS_ID
				FK09_AD_EVENT	AD_EVENT	ENHNC_SRVC_ID		LK_ENHNC_SRVC	ENHNC_SRVC_ID
				FK10_AD_EVENT	AD_EVENT	EVENT_ID			EVENT	EVENT_ID
				FK12_AD_EVENT	AD_EVENT	IP_VER_ID			LK_IP_VER	IP_VER_ID
				------------------------------------------ok----------------EVENT--------------------
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_AD_EVENT_ACCS_TAG	AD_EVENT_ACCS_TAG	EVENT_ID	AD_EVENT	EVENT_ID

				*/

				Set @Time=GetDate() RAISERROR ('Starting to execute [adbo].[AD_EVENT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[AD_EVENT](
						[EVENT_ID] ,
						[EVENT_STUS_ID]  ,
						[FTN]  ,
						[CHARS_ID] ,
						[H1]  ,
						[H6] ,
						[ENHNC_SRVC_ID]  ,
						[REQOR_USER_ID]  ,
						[SALS_USER_ID]  ,
						[PUB_EMAIL_CC_TXT] ,
						[CMPLTD_EMAIL_CC_TXT] ,
						[DES_CMNT_TXT]  ,
						[CPE_ATND_CD]  ,
						[DOC_LINK_TXT] ,
						[ESCL_CD]  ,
						[PRIM_REQ_DT] ,
						[SCNDY_REQ_DT] ,
						[ESCL_REAS_ID]  ,
						[STRT_TMST]  ,
						[EXTRA_DRTN_TME_AMT]  ,
						[END_TMST]  ,
						[WRKFLW_STUS_ID]  ,
						[REC_STUS_ID]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_BY_USER_ID] ,
						[MODFD_DT] ,
						[CREAT_DT] ,
						[IP_VER_ID]  ,
						[CNFRC_BRDG_NBR] ,
						[CNFRC_PIN_NBR] ,
						[EVENT_DRTN_IN_MIN_QTY]  ,
						[SOWS_EVENT_ID] ,
						[CUST_NME]  ,
						[CUST_CNTCT_NME]  ,
						[CUST_CNTCT_PHN_NBR] ,
						[CUST_EMAIL_ADR] ,
						[CUST_CNTCT_CELL_PHN_NBR] ,
						[CUST_CNTCT_PGR_NBR] ,
						[CUST_CNTCT_PGR_PIN_NBR] ,
						[EVENT_TITLE_TXT],
						[EVENT_DES] )
				SELECT 	[EVENT_ID] ,
						[EVENT_STUS_ID]  ,
						[FTN]  ,
						[CHARS_ID] ,
						[H1]  ,
						[H6] ,
						[ENHNC_SRVC_ID]  ,
						[REQOR_USER_ID]  ,
						[SALS_USER_ID]  ,
						[PUB_EMAIL_CC_TXT] ,
						[CMPLTD_EMAIL_CC_TXT] ,
						[DES_CMNT_TXT]  ,
						[CPE_ATND_CD]  ,
						[DOC_LINK_TXT] ,
						[ESCL_CD]  ,
						[PRIM_REQ_DT] ,
						[SCNDY_REQ_DT] ,
						[ESCL_REAS_ID]  ,
						[STRT_TMST]  ,
						[EXTRA_DRTN_TME_AMT]  ,
						[END_TMST]  ,
						[WRKFLW_STUS_ID]  ,
						[REC_STUS_ID]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_BY_USER_ID] ,
						[MODFD_DT] ,
						[CREAT_DT] ,
						[IP_VER_ID]  ,
						[CNFRC_BRDG_NBR] ,
						[CNFRC_PIN_NBR] ,
						[EVENT_DRTN_IN_MIN_QTY]  ,
						[SOWS_EVENT_ID] ,
						[CUST_NME]  ,
						[CUST_CNTCT_NME]  ,
						[CUST_CNTCT_PHN_NBR] ,
						[CUST_EMAIL_ADR] ,
						[CUST_CNTCT_CELL_PHN_NBR] ,
						[CUST_CNTCT_PGR_NBR] ,
						[CUST_CNTCT_PGR_PIN_NBR] ,
						[EVENT_TITLE_TXT],
						[EVENT_DES]
				FROM	[COWS].[arch].[AD_EVENT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[AD_EVENT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[AD_EVENT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[AD_EVENT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[AD_EVENT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[AD_EVENT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			END
			/*
			[ADHOC_RPT_ACTY]
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_ADHOC_RPT_ACTY	ADHOC_RPT_ACTY	REQ_BY_USER_ID	LK_USER	USER_ID

			*/

			--PRINT 'Starting to execute [arch].[ADHOC_RPT_ACTY] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[ADHOC_RPT_ACTY](
			--	[ADHOC_RPT_ACTY_ID]   ,
			--	[REQ_BY_USER_ID]  ,
			--	[ADHOC_RPT_QUERY_TXT]  ,
			--	[ADHOC_RPT_SCHEDULE]  ,
			--	[CREAT_DT]  )


			--SELECT [ADHOC_RPT_ACTY_ID]   ,
			--	[REQ_BY_USER_ID]  ,
			--	[ADHOC_RPT_QUERY_TXT]  ,
			--	[ADHOC_RPT_SCHEDULE]  ,
			--	[CREAT_DT]
			--FROM COWS.dbo.[ADHOC_RPT_ACTY] WHERE [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [arch].[ADHOC_RPT_ACTY] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[ADHOC_RPT_ACTY] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE   FROM COWS.dbo.[ADHOC_RPT_ACTY] WHERE [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [dbo].[ADHOC_RPT_ACTY] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ADHOC_RPT_ACTY]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			IF(@Mood <> 0 )  /* Related to EVENTS ONLY All Event TYpe */
			BEGIN
				/*
				[APPT_RCURNC_DATA]
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_APPT_RCURNC_DATA	APPT_RCURNC_DATA	APPT_ID				APPT				APPT_ID
				FK02_APPT_RCURNC_DATA	APPT_RCURNC_DATA	APPT_TYPE_ID		LK_APPT_TYPE		APPT_TYPE_ID
				FK03_APPT_RCURNC_DATA	APPT_RCURNC_DATA	ASN_USER_ID			LK_USER				USER_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. APPT_RCURNC_DATA     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[APPT_RCURNC_DATA] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[APPT_RCURNC_DATA](
					[APPT_RCURNC_DATA_ID]   ,
					[APPT_ID]  ,
					[STRT_TMST]  ,
					[END_TMST]  ,
					[APPT_TYPE_ID]  ,
					[ASN_USER_ID]  ,
					[CREAT_DT] )
				SELECT [APPT_RCURNC_DATA_ID]   ,
					[APPT_ID]  ,
					[STRT_TMST]  ,
					[END_TMST]  ,
					[APPT_TYPE_ID]  ,
					[ASN_USER_ID]  ,
					[CREAT_DT]
				FROM  [arch].[APPT_RCURNC_DATA] WHERE [APPT_ID] IN (SELECT DISTINCT [APPT_ID] FROM [COWS].[arch].[APPT] WITH (NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[APPT_RCURNC_DATA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. APPT_RCURNC_DATA     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[APPT_RCURNC_DATA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM  [arch].[APPT_RCURNC_DATA] WHERE [APPT_ID] IN (SELECT DISTINCT [APPT_ID] FROM [COWS].[arch].[APPT] WITH (NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[APPT_RCURNC_DATA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[APPT_RCURNC_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				[APPT_RCURNC_TRGR]
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_APPT_RCURNC_TRGR	APPT_RCURNC_TRGR	APPT_ID			APPT				APPT_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. APPT_RCURNC_TRGR     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[APPT_RCURNC_TRGR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[APPT_RCURNC_TRGR](
					[APPT_ID]  ,
					[STUS_ID]  ,
					[CREAT_DT]  ,
					[APPT_RCURNC_TRGR_ID],
					[MODFD_DT]   )
				SELECT [APPT_ID]  ,
					[STUS_ID]  ,
					[CREAT_DT]  ,
					[APPT_RCURNC_TRGR_ID],
					[MODFD_DT]
				FROM  [arch].[APPT_RCURNC_TRGR] 
				WHERE [APPT_ID] IN (SELECT DISTINCT [APPT_ID] FROM [COWS].[arch].[APPT] WITH (NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))	
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[APPT_RCURNC_TRGR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. APPT_RCURNC_TRGR     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[APPT_RCURNC_TRGR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM  [arch].[APPT_RCURNC_TRGR] 
				WHERE [APPT_ID] IN (SELECT DISTINCT [APPT_ID] FROM [COWS].[arch].[APPT] WITH (NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))	
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[APPT_RCURNC_TRGR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[APPT_RCURNC_TRGR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*

				APPT

				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_APPT	APPT	CREAT_BY_USER_ID	LK_USER	USER_ID
				FK02_APPT	APPT	MODFD_BY_USER_ID	LK_USER	USER_ID
				FK03_APPT	APPT	EVENT_ID			EVENT	EVENT_ID
				FK04_APPT	APPT	APPT_TYPE_ID		LK_APPT_TYPE	APPT_TYPE_ID
				FK06_APPT	APPT	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID
				--------------------------------------ok-----------------------EVENT-----------------
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_APPT_RCURNC_DATA	APPT_RCURNC_DATA	APPT_ID	APPT	APPT_ID
				FK01_APPT_RCURNC_TRGR	APPT_RCURNC_TRGR	APPT_ID	APPT	APPT_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. APPT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[APPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[APPT](
						[APPT_ID]   ,
						[EVENT_ID] ,
						[STRT_TMST]  ,
						[END_TMST] ,
						[RCURNC_DES_TXT] ,
						[SUBJ_TXT]  ,
						[DES]  ,
						[APPT_LOC_TXT] ,
						[APPT_TYPE_ID]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_BY_USER_ID] ,
						[MODFD_DT] ,
						[CREAT_DT]  ,
						[ASN_TO_USER_ID_LIST_TXT] ,
						[RCURNC_CD]  ,
						[ACTY_TYPE_ID] ,
						[REC_STUS_ID] )
				SELECT  [APPT_ID]   ,
						[EVENT_ID] ,
						[STRT_TMST]  ,
						[END_TMST] ,
						[RCURNC_DES_TXT] ,
						[SUBJ_TXT]  ,
						[DES]  ,
						[APPT_LOC_TXT] ,
						[APPT_TYPE_ID]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_BY_USER_ID] ,
						[MODFD_DT] ,
						[CREAT_DT]  ,
						[ASN_TO_USER_ID_LIST_TXT] ,
						[RCURNC_CD]  ,
						[ACTY_TYPE_ID] ,
						[REC_STUS_ID]
				FROM	[COWS].[arch].[APPT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[APPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. APPT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[APPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[APPT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[APPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[APPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			END
			
			IF(@Mood=0 )  /* ORDER ONLY */
			BEGIN
				/*

				[CCD_HIST_REAS]
				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_CCD_HIST_REAS	CCD_HIST_REAS	CCD_HIST_ID			CCD_HIST			CCD_HIST_ID
				FK02_CCD_HIST_REAS	CCD_HIST_REAS	CCD_MISSD_REAS_ID	LK_CCD_MISSD_REAS	CCD_MISSD_REAS_ID
				-------------------------------------------ok----------------------------ORDER-------------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[CCD_HIST_REAS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[CCD_HIST_REAS](
						[CCD_HIST_ID]  ,
						[CCD_MISSD_REAS_ID]  ,
						[CREAT_DT]  )
				SELECT
						[CCD_HIST_ID]  ,
						[CCD_MISSD_REAS_ID]  ,
						[CREAT_DT]
				FROM  [arch].[CCD_HIST_REAS]
				WHERE CCD_HIST_ID IN (SELECT CCD_HIST_ID FROM [COWS].[arch].CCD_HIST WITH(NOLOCK) WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)  )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[CCD_HIST_REAS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[CCD_HIST_REAS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM  [arch].[CCD_HIST_REAS]
				WHERE CCD_HIST_ID IN (SELECT CCD_HIST_ID FROM [COWS].[arch].CCD_HIST WITH(NOLOCK) WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)  )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[CCD_HIST_REAS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CCD_HIST_REAS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				[CCD_HIST]

				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_CCD_HIST	CCD_HIST	NTE_ID				ORDR_NTE			NTE_ID
				FK02_CCD_HIST	CCD_HIST	ORDR_ID				ORDR				ORDR_ID
				FK03_CCD_HIST	CCD_HIST	CREAT_BY_USER_ID	LK_USER				USER_ID
				-------------------------------------------ok----------------------ORDER-------------------------------
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_CCD_HIST_REAS	CCD_HIST_REAS	CCD_HIST_ID	CCD_HIST	CCD_HIST_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. CCD_HIST     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[CCD_HIST] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[CCD_HIST](
						[CCD_HIST_ID]   ,
						[ORDR_ID]  ,
						[OLD_CCD_DT]  ,
						[NEW_CCD_DT]  ,
						[NTE_ID] ,
						[SENT_TO_VNDR_DT] ,
						[ACK_BY_VNDR_DT] ,
						[CREAT_BY_USER_ID]  ,
						[CREAT_DT]  )
				SELECT 	[CCD_HIST_ID]   ,
						[ORDR_ID]  ,
						[OLD_CCD_DT]  ,
						[NEW_CCD_DT]  ,
						[NTE_ID] ,
						[SENT_TO_VNDR_DT] ,
						[ACK_BY_VNDR_DT] ,
						[CREAT_BY_USER_ID]  ,
						[CREAT_DT] 	
				FROM	[COWS].[arch].[CCD_HIST] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[CCD_HIST] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. CCD_HIST     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[CCD_HIST] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CCD_HIST] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[CCD_HIST] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CCD_HIST]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_VNDR_TMPLT	VNDR_TMPLT	VNDR_FOLDR_ID		VNDR_FOLDR			VNDR_FOLDR_ID
				FK02_VNDR_TMPLT	VNDR_TMPLT	CREAT_BY_USER_ID	LK_USER				USER_ID
				FK03_VNDR_TMPLT	VNDR_TMPLT	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID

				*/
				/*
				PRINT 'Starting to execute [arch].[VNDR_TMPLT] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[VNDR_TMPLT](
					[VNDR_TMPLT_ID]   ,
					[VNDR_FOLDR_ID]  ,
					[FILE_NME]  ,
					[FILE_CNTNT]  ,
					[FILE_SIZE_QTY]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  )
				SELECT [VNDR_TMPLT_ID]   ,
					[VNDR_FOLDR_ID]  ,
					[FILE_NME]  ,
					[FILE_CNTNT]  ,
					[FILE_SIZE_QTY]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]
				FROM [COWS].[dbo].[VNDR_TMPLT]  WHERE [VNDR_FOLDR_ID] IN( SELECT [VNDR_FOLDR_ID] FROM	[COWS].[dbo].[VNDR_FOLDR] WITH(NOLOCK)	WHERE [VNDR_FOLDR_ID]	IN	
						(SELECT DISTINCT [VNDR_FOLDR_ID] FROM  [COWS].[dbo].VNDR_ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)))
				PRINT 'Finished executing [arch].[VNDR_TMPLT] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				
				PRINT 'Starting to execute [dbo].[VNDR_TMPLT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM [COWS].[dbo].[VNDR_TMPLT]  WHERE [VNDR_FOLDR_ID] IN( SELECT [VNDR_FOLDR_ID] FROM	[COWS].[dbo].[VNDR_FOLDR] WITH(NOLOCK)	WHERE [VNDR_FOLDR_ID]	IN	
						(SELECT DISTINCT [VNDR_FOLDR_ID] FROM  [COWS].[dbo].VNDR_ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)))
				PRINT 'Finished executing [dbo].[VNDR_TMPLT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'	
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[VNDR_TMPLT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/

				
				
				
				/*

				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_CKT_COST	CKT_COST	CKT_ID				CKT					CKT_ID
				FK02_CKT_COST	CKT_COST	ACCS_CUST_CUR_ID	LK_CUR				CUR_ID
				FK03_CKT_COST	CKT_COST	ASR_CUR_ID			LK_CUR				CUR_ID
				FK04_CKT_COST	CKT_COST	VNDR_CUR_ID			LK_CUR				CUR_ID
				FK05_CKT_COST	CKT_COST	CREAT_BY_USER_ID	LK_USER				USER_ID
				-------------------------------------------ok--------------------------ORDER---------------------------

				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[CKT_COST] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[CKT_COST](
					[CKT_ID]  ,
					[VER_ID]  ,
					[QOT_NBR]  ,
					[TAX_RT_PCT_QTY]  ,
					[VNDR_CUR_ID]  ,
					[VNDR_MRC_AMT]  ,
					[VNDR_MRC_IN_USD_AMT]  ,
					[VNDR_NRC_AMT]  ,
					[VNDR_NRC_IN_USD_AMT]  ,
					[ASR_CUR_ID]  ,
					[ASR_MRC_ATY]  ,
					[ASR_MRC_IN_USD_AMT]  ,
					[ASR_NRC_AMT]  ,
					[ASR_NRC_IN_USD_AMT]  ,
					[ACCS_CUST_CUR_ID]  ,
					[ACCS_CUST_MRC_AMT]  ,
					[ACCS_CUST_MRC_IN_USD_AMT]  ,
					[ACCS_CUST_NRC_AMT]  ,
					[ACCS_CUST_NRC_IN_USD_AMT]  ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[BNDL_CD]  ,
					[VNDR_ORDR_NBR] )
				SELECT
					[CKT_ID]  ,
					[VER_ID]  ,
					[QOT_NBR]  ,
					[TAX_RT_PCT_QTY]  ,
					[VNDR_CUR_ID]  ,
					[VNDR_MRC_AMT]  ,
					[VNDR_MRC_IN_USD_AMT]  ,
					[VNDR_NRC_AMT]  ,
					[VNDR_NRC_IN_USD_AMT]  ,
					[ASR_CUR_ID]  ,
					[ASR_MRC_ATY]  ,
					[ASR_MRC_IN_USD_AMT]  ,
					[ASR_NRC_AMT]  ,
					[ASR_NRC_IN_USD_AMT]  ,
					[ACCS_CUST_CUR_ID]  ,
					[ACCS_CUST_MRC_AMT]  ,
					[ACCS_CUST_MRC_IN_USD_AMT]  ,
					[ACCS_CUST_NRC_AMT]  ,
					[ACCS_CUST_NRC_IN_USD_AMT]  ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[BNDL_CD]  ,
					[VNDR_ORDR_NBR]
				FROM	[COWS].[arch].[CKT_COST] WITH(NOLOCK)	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[CKT_COST] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[CKT_COST] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CKT_COST] WITH(TABLOCKX)	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[CKT_COST] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CKT_COST]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
								
				/*

				[CKT_MS]

				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_CKT_MS	CKT_MS		CKT_ID				CKT					CKT_ID
				FK02_CKT_MS	CKT_MS		CREAT_BY_USER_ID	LK_USER				USER_ID
				FK03_CKT_MS	CKT_MS		CNTRC_TERM_ID		LK_CUST_CNTRC_LGTH	CUST_CNTRC_LGTH_ID
				-------------------------------------------ok------------------ORDER-->>CKTID-----------------------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[CKT_MS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[CKT_MS](
					[CKT_ID]  ,
					[VER_ID]  ,
					[TRGT_DLVRY_DT_RECV_DT] ,
					[TRGT_DLVRY_DT] ,
					[ACCS_DLVRY_DT] ,
					[ACCS_ACPTC_DT] ,
					[CNTRC_STRT_DT] ,
					[CNTRC_END_DT] ,
					[RNL_DT] ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[CNTRC_TERM_ID] ,
					[VNDR_CNFRM_DSCNCT_DT])
				SELECT  [CKT_ID]  ,
					[VER_ID]  ,
					[TRGT_DLVRY_DT_RECV_DT] ,
					[TRGT_DLVRY_DT] ,
					[ACCS_DLVRY_DT] ,
					[ACCS_ACPTC_DT] ,
					[CNTRC_STRT_DT] ,
					[CNTRC_END_DT] ,
					[RNL_DT] ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[CNTRC_TERM_ID] ,
					[VNDR_CNFRM_DSCNCT_DT]
				FROM	[COWS].[arch].[CKT_MS] WITH(NOLOCK)	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[CKT_MS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[CKT_MS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CKT_MS] WITH(TABLOCKX)	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[CKT_MS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CKT_MS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				[CKT]
				FOREIGN_KEY	TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_CKT	CKT					CREAT_BY_USER_ID	LK_USER				USER_ID
				FK02_CKT	CKT					ORDR_ID				ORDR				ORDR_ID
				FK03_CKT	CKT					VNDR_ORDR_ID		VNDR_ORDR			VNDR_ORDR_ID
				--------------------------------------ok------------------ORDER-->CKTID------------------------
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_CKT_COST	CKT_COST	CKT_ID	CKT	CKT_ID
				FK01_CKT_MS		CKT_MS		CKT_ID	CKT	CKT_ID

				
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. CKT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[CKT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[CKT](
					[CKT_ID]   ,
					[ORDR_ID] ,
					[VNDR_CKT_ID]  ,
					[LEC_ID]  ,
					[VNDR_CNTRC_TERM_END_DT] ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[VNDR_ORDR_ID] ,
					[PL_SEQ_NBR]  ,
					[TRMTG_CD]  )
				SELECT
					[CKT_ID]   ,
					[ORDR_ID] ,
					[VNDR_CKT_ID]  ,
					[LEC_ID]  ,
					[VNDR_CNTRC_TERM_END_DT] ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[VNDR_ORDR_ID] ,
					[PL_SEQ_NBR]  ,
					[TRMTG_CD]
				FROM	[COWS].[arch].[CKT] WITH(NOLOCK)	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[CKT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. CKT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[CKT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[CKT] WITH(TABLOCKX)	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[CKT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[CKT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			END
			/*
			FOREIGN_KEY				TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE		REFERANCE_COLUMN_NAME
			FK01_DSPL_EVENT_MAPNG	DSPL_EVENT_MAPNG	DSPL_VW_ID		DSPL_VW				DSPL_VW_ID
			FK02_DSPL_EVENT_MAPNG	DSPL_EVENT_MAPNG	EVENT_STUS_ID	LK_EVENT_STUS		EVENT_STUS_ID


			*/
			--PRINT 'Starting to execute [arch].[DSPL_EVENT_MAPNG] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[DSPL_EVENT_MAPNG](
			--	[DSPL_VW_ID]  ,
			--	[EVENT_STUS_ID]  ,
			--	[DSPL_RANK_ID]  ,
			--	[CREAT_DT] )
			--SELECT [DSPL_VW_ID]  ,
			--	[EVENT_STUS_ID]  ,
			--	[DSPL_RANK_ID]  ,
			--	[CREAT_DT]	
			--FROM  [dbo].[DSPL_EVENT_MAPNG]
			--WHERE [DSPL_VW_ID] IN (SELECT DISTINCT[DSPL_VW_ID] FROM [arch].[DSPL_VW] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [arch].[DSPL_EVENT_MAPNG] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[DSPL_EVENT_MAPNG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM  [dbo].[DSPL_EVENT_MAPNG]
			--WHERE [DSPL_VW_ID] IN (SELECT DISTINCT[DSPL_VW_ID] FROM [arch].[DSPL_VW] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [dbo].[DSPL_EVENT_MAPNG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[DSPL_EVENT_MAPNG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			[DSPL_VW_COL]
			FOREIGN_KEY			TABLE_NAME		COLUMN_NAME		REFERANCE_TABLE		REFERANCE_COLUMN_NAME
			FK01_DSPL_VW_COL	DSPL_VW_COL		DSPL_VW_ID		DSPL_VW				DSPL_VW_ID
			FK02_DSPL_VW_COL	DSPL_VW_COL		DSPL_COL_ID		LK_DSPL_COL			DSPL_COL_ID


			*/
			--PRINT 'Starting to execute [arch].[DSPL_VW_COL] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[DSPL_VW_COL](
			--	[DSPL_VW_ID]  ,
			--	[DSPL_COL_ID]  ,
			--	[DSPL_POS_FROM_LEFT_NBR]  ,
			--	[CREAT_DT] )
			--SELECT [DSPL_VW_ID]  ,
			--	[DSPL_COL_ID]  ,
			--	[DSPL_POS_FROM_LEFT_NBR]  ,
			--	[CREAT_DT]
			--FROM  [dbo].[DSPL_VW_COL]	
			--WHERE [DSPL_VW_ID] IN (SELECT DISTINCT[DSPL_VW_ID] FROM [arch].[DSPL_VW] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [arch].[DSPL_VW_COL] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			
			--PRINT 'Starting to execute [dbo].[DSPL_VW_COL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM  [dbo].[DSPL_VW_COL]	
			--WHERE [DSPL_VW_ID] IN (SELECT DISTINCT[DSPL_VW_ID] FROM [arch].[DSPL_VW] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [dbo].[DSPL_VW_COL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[DSPL_VW_COL]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			FOREIGN_KEY		TABLE_NAME					COLUMN_NAME				REFERANCE_TABLE		REFERANCE_COLUMN_NAME
			FK01_DSPL_VW	DSPL_VW						MODFD_BY_USER_ID		LK_USER				USER_ID
			FK02_DSPL_VW	DSPL_VW						CREAT_BY_USER_ID		LK_USER				USER_ID
			FK03_DSPL_VW	DSPL_VW						USER_ID					LK_USER				USER_ID
			FK04_DSPL_VW	DSPL_VW						SITE_CNTNT_ID			LK_SITE_CNTNT		SITE_CNTNT_ID
			FK05_DSPL_VW	DSPL_VW						FILTR_COL_1_OPR_ID		LK_FILTR_OPR		FILTR_OPR_ID
			FK06_DSPL_VW	DSPL_VW						FILTR_COL_2_OPR_ID		LK_FILTR_OPR		FILTR_OPR_ID
			FK07_DSPL_VW	DSPL_VW						FILTR_COL_2_ID			LK_DSPL_COL			DSPL_COL_ID
			FK08_DSPL_VW	DSPL_VW						SORT_BY_COL_1_ID		LK_DSPL_COL			DSPL_COL_ID
			FK09_DSPL_VW	DSPL_VW						SORT_BY_COL_2_ID		LK_DSPL_COL			DSPL_COL_ID
			FK10_DSPL_VW	DSPL_VW						GRP_BY_COL_2_ID			LK_DSPL_COL			DSPL_COL_ID
			FK11_DSPL_VW	DSPL_VW						GRP_BY_COL_1_ID			LK_DSPL_COL			DSPL_COL_ID
			FK12_DSPL_VW	DSPL_VW						FILTR_COL_1_ID			LK_DSPL_COL			DSPL_COL_ID
			FK13_DSPL_VW	DSPL_VW						USER_ID					LK_USER				USER_ID
			FK14_DSPL_VW	DSPL_VW						FILTR_LOGIC_OPR_ID		LK_FILTR_OPR		FILTR_OPR_ID
			-------------------------------------------------------------------------------------------------------
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_DSPL_EVENT_MAPNG	DSPL_EVENT_MAPNG	DSPL_VW_ID	DSPL_VW	DSPL_VW_ID
			FK01_DSPL_VW_COL		DSPL_VW_COL			DSPL_VW_ID	DSPL_VW	DSPL_VW_ID


			*/
			--PRINT 'Starting to execute [arch].[DSPL_VW] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[DSPL_VW](
			--	[DSPL_VW_ID]   ,
			--	[USER_ID]  ,
			--	[DSPL_VW_NME]  ,
			--	[SITE_CNTNT_ID]  ,
			--	[SITE_CNTNT_COL_VW_CD]  ,
			--	[DSPL_VW_DES]  ,
			--	[DSPL_VW_WEB_ADR] ,
			--	[DFLT_VW_CD]  ,
			--	[PBLC_VW_CD]  ,
			--	[FILTR_COL_1_OPR_ID]  ,
			--	[FILTR_COL_2_OPR_ID]  ,
			--	[FILTR_COL_1_ID] ,
			--	[FILTR_COL_2_ID] ,
			--	[FILTR_COL_1_VALU_TXT]  ,
			--	[FILTR_COL_2_VALU_TXT]  ,
			--	[FILTR_ON_CD]  ,
			--	[SORT_BY_COL_1_ID] ,
			--	[SORT_BY_COL_2_ID] ,
			--	[GRP_BY_COL_1_ID] ,
			--	[GRP_BY_COL_2_ID] ,
			--	[CREAT_BY_USER_ID]  ,
			--	[MODFD_BY_USER_ID] ,
			--	[MODFD_DT] ,
			--	[CREAT_DT]  ,
			--	[FILTR_LOGIC_OPR_ID]  ,
			--	[SORT_BY_COL_1_ASC_ORDR_CD]  ,
			--	[SORT_BY_COL_2_ASC_ORDR_CD]  ,
			--	[SEQ_NBR] )
				
			--SELECT [DSPL_VW_ID]   ,
			--	[USER_ID]  ,
			--	[DSPL_VW_NME]  ,
			--	[SITE_CNTNT_ID]  ,
			--	[SITE_CNTNT_COL_VW_CD]  ,
			--	[DSPL_VW_DES]  ,
			--	[DSPL_VW_WEB_ADR] ,
			--	[DFLT_VW_CD]  ,
			--	[PBLC_VW_CD]  ,
			--	[FILTR_COL_1_OPR_ID]  ,
			--	[FILTR_COL_2_OPR_ID]  ,
			--	[FILTR_COL_1_ID] ,
			--	[FILTR_COL_2_ID] ,
			--	[FILTR_COL_1_VALU_TXT]  ,
			--	[FILTR_COL_2_VALU_TXT]  ,
			--	[FILTR_ON_CD]  ,
			--	[SORT_BY_COL_1_ID] ,
			--	[SORT_BY_COL_2_ID] ,
			--	[GRP_BY_COL_1_ID] ,
			--	[GRP_BY_COL_2_ID] ,
			--	[CREAT_BY_USER_ID]  ,
			--	[MODFD_BY_USER_ID] ,
			--	[MODFD_DT] ,
			--	[CREAT_DT]  ,
			--	[FILTR_LOGIC_OPR_ID]  ,
			--	[SORT_BY_COL_1_ASC_ORDR_CD]  ,
			--	[SORT_BY_COL_2_ASC_ORDR_CD]  ,
			--	[SEQ_NBR]
			--FROM  [dbo].[DSPL_VW] 	Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [arch].[DSPL_VW] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[DSPL_VW] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM  [dbo].[DSPL_VW] 	Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [dbo].[DSPL_VW] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[DSPL_VW]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			
			/*
			[EMAIL_REQ]
			FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
			FK01_EMAIL_REQ	EMAIL_REQ	ORDR_ID				ORDR				ORDR_ID
			FK02_EMAIL_REQ	EMAIL_REQ	STUS_ID				LK_STUS				STUS_ID
			FK03_EMAIL_REQ	EMAIL_REQ	EMAIL_REQ_TYPE_ID	LK_EMAIL_REQ_TYPE	EMAIL_REQ_TYPE_ID
			FK05_EMAIL_REQ	EMAIL_REQ	EVENT_ID			EVENT				EVENT_ID
			------------------------------------------------------ok------------------EVENT----ORDER-->>[ORDR_ID]-------------
			*/ 
			------------------------------------------------------------------------------------------------------------------
			---------------------[EMAIL_REQ]---------------------NO NEED TO BRING THIS TO DBO---------------------------------
			------------------------------------------------------------------------------------------------------------------
			/*
			Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EMAIL_REQ] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
			INSERT INTO [dbo].[EMAIL_REQ](
				[EMAIL_REQ_ID]   ,
				[ORDR_ID] ,
				[EMAIL_REQ_TYPE_ID]  ,
				[STUS_ID]  ,
				[CREAT_DT]  ,
				[EVENT_ID] ,
				[EMAIL_LIST_TXT] ,
				[EMAIL_CC_TXT]  ,
				[EMAIL_SUBJ_TXT]  ,
				[EMAIL_BODY_TXT] ,
				[SENT_DT],
				[REDSGN_ID],
				[CPT_ID] )
			SELECT [EMAIL_REQ_ID]   ,
				[ORDR_ID] ,
				[EMAIL_REQ_TYPE_ID]  ,
				[STUS_ID]  ,
				[CREAT_DT]  ,
				[EVENT_ID] ,
				[EMAIL_LIST_TXT] ,
				[EMAIL_CC_TXT]  ,
				[EMAIL_SUBJ_TXT]  ,
				[EMAIL_BODY_TXT] ,
				[SENT_DT] ,
				[REDSGN_ID],
				[CPT_ID]
			FROM	[COWS].[arch].[EMAIL_REQ] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
			UNION
			SELECT [EMAIL_REQ_ID]   ,
				[ORDR_ID] ,
				[EMAIL_REQ_TYPE_ID]  ,
				[STUS_ID]  ,
				[CREAT_DT]  ,
				[EVENT_ID] ,
				[EMAIL_LIST_TXT] ,
				[EMAIL_CC_TXT]  ,
				[EMAIL_SUBJ_TXT]  ,
				[EMAIL_BODY_TXT] ,
				[SENT_DT] ,
				[REDSGN_ID],
				[CPT_ID]
			FROM	[COWS].[arch].[EMAIL_REQ] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
			Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EMAIL_REQ] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EMAIL_REQ]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9)
			Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EMAIL_REQ] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			DELETE  FROM	[COWS].[arch].[EMAIL_REQ] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
			Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EMAIL_REQ] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EMAIL_REQ] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			DELETE  FROM	[COWS].[arch].[EMAIL_REQ] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
			Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EMAIL_REQ] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			*/
			------------------------------------------------------------------------------------------------------------------
			
			
			
			--IF(@Mood<>0)  /*  EVENTS ONLY ALL EVENTS */
			IF(@Mood=1 OR @Mood=2 OR @Mood=3 OR @Mood=4 OR @Mood=5 OR @Mood=6)
			BEGIN
				/*
				[EVENT_ASN_TO_USER]
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_EVENT_ASN_TO_USER	EVENT_ASN_TO_USER	ASN_TO_USER_ID	LK_USER			USER_ID
				FK02_EVENT_ASN_TO_USER	EVENT_ASN_TO_USER	EVENT_ID		EVENT			EVENT_ID
				FK03_EVENT_ASN_TO_USER	EVENT_ASN_TO_USER	REC_STUS_ID		LK_REC_STUS		REC_STUS_ID
				--------------------------------------------ok---------------------------EVENT------------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_ASN_TO_USER] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[EVENT_ASN_TO_USER](
					[EVENT_ID]  ,
					[ASN_TO_USER_ID]  ,
					[CREAT_DT]  ,
					[REC_STUS_ID],
					[ROLE_ID],
					[MODFD_DT] 
					)
				SELECT 
					[EVENT_ID]  ,
					[ASN_TO_USER_ID]  ,
					[CREAT_DT]  ,
					[REC_STUS_ID],
					[ROLE_ID],
					[MODFD_DT]
				FROM	[COWS].[arch].[EVENT_ASN_TO_USER] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_ASN_TO_USER] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EVENT_ASN_TO_USER] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT_ASN_TO_USER] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_ASN_TO_USER] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_ASN_TO_USER]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				SET IDENTITY_INSERT  [COWS].[dbo].[EVENT_CPE_DEV]     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EVENT_CPE_DEV] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[EVENT_CPE_DEV]
				   ([EVENT_CPE_DEV_ID]
				   ,[EVENT_ID]
				   ,[CPE_ORDR_ID]
				   ,[CPE_ORDR_NBR]
				   ,[DEVICE_ID]
				   ,[ASSOC_H6]
				   ,[MAINT_VNDR_PO]
				   ,[REQSTN_NBR]
				   ,[CCD]
				   ,[ODIE_DEV_NME]
				   ,[ODIE_CD]
				   ,[ORDR_STUS]
				   ,[RCPT_STUS]
				   ,[CREAT_DT]
				   ,[REC_STUS_ID]
				   ,[MODFD_DT])
				SELECT 
					[EVENT_CPE_DEV_ID]
				   ,[EVENT_ID]
				   ,[CPE_ORDR_ID]
				   ,[CPE_ORDR_NBR]
				   ,[DEVICE_ID]
				   ,[ASSOC_H6]
				   ,[MAINT_VNDR_PO]
				   ,[REQSTN_NBR]
				   ,[CCD]
				   ,[ODIE_DEV_NME]
				   ,[ODIE_CD]
				   ,[ORDR_STUS]
				   ,[RCPT_STUS]
				   ,[CREAT_DT]
				   ,[REC_STUS_ID]
				   ,[MODFD_DT]
				FROM	[COWS].[arch].[EVENT_CPE_DEV] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_CPE_DEV] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[EVENT_CPE_DEV]     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_CPE_DEV] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT_CPE_DEV] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_CPE_DEV] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_CPE_DEV]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				
				
				SET IDENTITY_INSERT  [COWS].[dbo].[MDS_EVENT_SITE_SRVC]     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_SITE_SRVC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[MDS_EVENT_SITE_SRVC]
				   ([MDS_EVENT_SITE_SRVC_ID]
				  ,[EVENT_ID]
				  ,[MACH5_SRVC_ORDR_ID]
				  ,[SRVC_TYPE_ID]
				  ,[THRD_PARTY_VNDR_ID]
				  ,[THRD_PARTY_ID]
				  ,[THRD_PARTY_SRVC_LVL_ID]
				  ,[ACTV_DT]
				  ,[CMNT_TXT]
				  ,[EMAIL_CD]
				  ,[ODIE_DEV_NME]
				  ,[CREAT_DT]
				  ,[M5_ORDR_NBR])
				SELECT 
				   [MDS_EVENT_SITE_SRVC_ID]
				  ,[EVENT_ID]
				  ,[MACH5_SRVC_ORDR_ID]
				  ,[SRVC_TYPE_ID]
				  ,[THRD_PARTY_VNDR_ID]
				  ,[THRD_PARTY_ID]
				  ,[THRD_PARTY_SRVC_LVL_ID]
				  ,[ACTV_DT]
				  ,[CMNT_TXT]
				  ,[EMAIL_CD]
				  ,[ODIE_DEV_NME]
				  ,[CREAT_DT]
				  ,[M5_ORDR_NBR]
				FROM	[COWS].[arch].[MDS_EVENT_SITE_SRVC] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_SITE_SRVC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[MDS_EVENT_SITE_SRVC]     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_SITE_SRVC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_SITE_SRVC] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_SITE_SRVC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_SITE_SRVC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				
				SET IDENTITY_INSERT  [COWS].[dbo].[EVENT_DISCO_DEV]     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_DISCO_DEV] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[EVENT_DISCO_DEV]
				   ([EVENT_DISCO_DEV_ID]
				  ,[EVENT_ID]
				  ,[ODIE_DEV_NME]
				  ,[H5_H6]
				  ,[SITE_ID]
				  ,[DEVICE_ID]
				  ,[DEV_MODEL_ID]
				  ,[MANF_ID]
				  ,[SERIAL_NBR]
				  ,[THRD_PARTY_CTRCT]
				  ,[CREAT_DT]
				  ,[READY_BEGIN_CD]
				  ,[OPT_IN_CKT_CD]
				  ,[OPT_IN_H_CD]
				  ,[REC_STUS_ID]
				  ,[MODFD_DT])
				SELECT 
				   [EVENT_DISCO_DEV_ID]
				  ,[EVENT_ID]
				  ,[ODIE_DEV_NME]
				  ,[H5_H6]
				  ,[SITE_ID]
				  ,[DEVICE_ID]
				  ,[DEV_MODEL_ID]
				  ,[MANF_ID]
				  ,[SERIAL_NBR]
				  ,[THRD_PARTY_CTRCT]
				  ,[CREAT_DT]
				  ,[READY_BEGIN_CD]
				  ,[OPT_IN_CKT_CD]
				  ,[OPT_IN_H_CD]
				  ,[REC_STUS_ID]
				  ,[MODFD_DT]
				FROM	[COWS].[arch].[EVENT_DISCO_DEV] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_DISCO_DEV] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[EVENT_DISCO_DEV]     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_DISCO_DEV] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT_DISCO_DEV] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_DISCO_DEV] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_DISCO_DEV]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				SET IDENTITY_INSERT  [COWS].[dbo].[EVENT_DEV_SRVC_MGMT]     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_DEV_SRVC_MGMT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[EVENT_DEV_SRVC_MGMT]
				   ([EVENT_DEV_SRVC_MGMT_ID]
				  ,[EVENT_ID]
				  ,[MNS_ORDR_NBR]
				  ,[M5_ORDR_CMPNT_ID]
				  ,[DEVICE_ID]
				  ,[MNS_SRVC_TIER_ID]
				  ,[ODIE_DEV_NME]
				  ,[CMPNT_STUS]
				  ,[CMPNT_TYPE_CD]
				  ,[CMPNT_NME]
				  ,[ODIE_CD]
				  ,[CREAT_DT]
				  ,[REC_STUS_ID]
				  ,[MODFD_DT])
				SELECT 
				   [EVENT_DEV_SRVC_MGMT_ID]
				  ,[EVENT_ID]
				  ,[MNS_ORDR_NBR]
				  ,[M5_ORDR_CMPNT_ID]
				  ,[DEVICE_ID]
				  ,[MNS_SRVC_TIER_ID]
				  ,[ODIE_DEV_NME]
				  ,[CMPNT_STUS]
				  ,[CMPNT_TYPE_CD]
				  ,[CMPNT_NME]
				  ,[ODIE_CD]
				  ,[CREAT_DT]
				  ,[REC_STUS_ID]
				  ,[MODFD_DT]
				FROM	[COWS].[arch].[EVENT_DEV_SRVC_MGMT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_DEV_SRVC_MGMT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[EVENT_DEV_SRVC_MGMT]     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_DEV_SRVC_MGMT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT_DEV_SRVC_MGMT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_DEV_SRVC_MGMT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_DEV_SRVC_MGMT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				SET IDENTITY_INSERT  [COWS].[dbo].[MDS_EVENT_PORT_BNDWD]     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_PORT_BNDWD] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[MDS_EVENT_PORT_BNDWD]
				   ([PORT_BNDWD_ID]
				  ,[EVENT_ID]
				  ,[ODIE_DEV_NME]
				  ,[M5_ORDR_NBR]
				  ,[M5_ORDR_CMPNT_ID]
				  ,[NUA]
				  ,[PORT_BNDWD]
				  ,[CREAT_DT]
				  ,[REC_STUS_ID]
				  ,[MODFD_DT])
				SELECT 
				   [PORT_BNDWD_ID]
				  ,[EVENT_ID]
				  ,[ODIE_DEV_NME]
				  ,[M5_ORDR_NBR]
				  ,[M5_ORDR_CMPNT_ID]
				  ,[NUA]
				  ,[PORT_BNDWD]
				  ,[CREAT_DT]
				  ,[REC_STUS_ID]
				  ,[MODFD_DT]
				FROM	[COWS].[arch].[MDS_EVENT_PORT_BNDWD] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_PORT_BNDWD] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[MDS_EVENT_PORT_BNDWD]     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_PORT_BNDWD] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_PORT_BNDWD] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_PORT_BNDWD] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_PORT_BNDWD]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				SET IDENTITY_INSERT  [COWS].[dbo].[MDS_EVENT_SLNK_WIRED_TRPT]     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_SLNK_WIRED_TRPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[MDS_EVENT_SLNK_WIRED_TRPT]
				   ([SLNK_WIRED_TRPT_ID]
				  ,[EVENT_ID]
				  ,[MDS_TRNSPRT_TYPE]
				  ,[PRIM_BKUP_CD]
				  ,[VNDR_PRVDR_TRPT_CKT_ID]
				  ,[VNDR_PRVDR_NME]
				  ,[BDWD_CHNL_NME]
				  ,[PL_NBR]
				  ,[IP_NUA_ADR]
				  ,[OLD_CKT_ID]
				  ,[MULTI_LINK_CKT_CD]
				  ,[DED_ACCS_CD]
				  ,[FMS_NBR]
				  ,[DLCI_VPI_VCI]
				  ,[VLAN_NBR]
				  ,[SPA_NUA]
				  ,[SPRINT_MNGD_CD]
				  ,[ODIE_DEV_NME]
				  ,[CREAT_DT]
				  ,[REC_STUS_ID]
				  ,[MODFD_DT]
				  ,[READY_BEGIN_CD]
				  ,[OPT_IN_CKT_CD]
				  ,[OPT_IN_H_CD])
				SELECT 
				   [SLNK_WIRED_TRPT_ID]
				  ,[EVENT_ID]
				  ,[MDS_TRNSPRT_TYPE]
				  ,[PRIM_BKUP_CD]
				  ,[VNDR_PRVDR_TRPT_CKT_ID]
				  ,[VNDR_PRVDR_NME]
				  ,[BDWD_CHNL_NME]
				  ,[PL_NBR]
				  ,[IP_NUA_ADR]
				  ,[OLD_CKT_ID]
				  ,[MULTI_LINK_CKT_CD]
				  ,[DED_ACCS_CD]
				  ,[FMS_NBR]
				  ,[DLCI_VPI_VCI]
				  ,[VLAN_NBR]
				  ,[SPA_NUA]
				  ,[SPRINT_MNGD_CD]
				  ,[ODIE_DEV_NME]
				  ,[CREAT_DT]
				  ,[REC_STUS_ID]
				  ,[MODFD_DT]
				  ,[READY_BEGIN_CD]
				  ,[OPT_IN_CKT_CD]
				  ,[OPT_IN_H_CD]
				FROM	[COWS].[arch].[MDS_EVENT_SLNK_WIRED_TRPT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_SLNK_WIRED_TRPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[MDS_EVENT_SLNK_WIRED_TRPT]     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_SLNK_WIRED_TRPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_SLNK_WIRED_TRPT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_SLNK_WIRED_TRPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_SLNK_WIRED_TRPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				
				/*
				[EVENT_AVAL_USER]
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_EVENT_AVAL_USER	EVENT_AVAL_USER		USER_ID			LK_USER			USER_ID

				*/
				--PRINT 'Starting to execute [arch].[EVENT_AVAL_USER] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				--INSERT INTO [arch].[EVENT_AVAL_USER](
				--	[EVENT_AVAL_USER_ID]   ,
				--	[TME_SLOT_ID]  ,
				--	[USER_ID]  ,
				--	[TME_SLOT_STRT_TME]  ,
				--	[TME_SLOT_END_TME]  ,
				--	[TME_SLOT_DRTN_IN_MIN_QTY]  ,
				--	[DEL_CD]  ,
				--	[CREAT_DT]  )
				--SELECT [EVENT_AVAL_USER_ID]   ,
				--	[TME_SLOT_ID]  ,
				--	[USER_ID]  ,
				--	[TME_SLOT_STRT_TME]  ,
				--	[TME_SLOT_END_TME]  ,
				--	[TME_SLOT_DRTN_IN_MIN_QTY]  ,
				--	[DEL_CD]  ,
				--	[CREAT_DT]
				--FROM  [dbo].[EVENT_AVAL_USER]	Where [CREAT_DT]<=CONVERT(datetime,@Date)
				--PRINT 'Finished executing [arch].[EVENT_AVAL_USER] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				--PRINT 'Starting to execute [dbo].[EVENT_AVAL_USER] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				--DELETE  FROM  [dbo].[EVENT_AVAL_USER]	Where [CREAT_DT]<=CONVERT(datetime,@Date)
				--PRINT 'Finished executing [dbo].[EVENT_AVAL_USER] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_AVAL_USER]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				
				
				/*
				FOREIGN_KEY				TABLE_NAME		COLUMN_NAME		REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_EVENT_FAIL_ACTY	EVENT_FAIL_ACTY	REC_STUS_ID		LK_REC_STUS			REC_STUS_ID
				FK02_EVENT_FAIL_ACTY	EVENT_FAIL_ACTY	EVENT_HIST_ID	EVENT_HIST			EVENT_HIST_ID
				FK03_EVENT_FAIL_ACTY	EVENT_FAIL_ACTY	FAIL_ACTY_ID	LK_FAIL_ACTY		FAIL_ACTY_ID
				---------------------------------------------ok-----------------------------EVENT------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_FAIL_ACTY] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[EVENT_FAIL_ACTY](
					[EVENT_HIST_ID]  ,
					[FAIL_ACTY_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_DT] )
				SELECT [EVENT_HIST_ID]  ,
					[FAIL_ACTY_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[EVENT_FAIL_ACTY] WITH(NOLOCK) WHERE EVENT_HIST_ID IN ( SELECT EVENT_HIST_ID FROM  [COWS].[arch].[EVENT_HIST] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_FAIL_ACTY] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EVENT_FAIL_ACTY] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT_FAIL_ACTY] WITH(TABLOCKX) WHERE EVENT_HIST_ID IN ( SELECT EVENT_HIST_ID FROM  [COWS].[dbo].[EVENT_HIST] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_FAIL_ACTY] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_FAIL_ACTY]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*

				FOREIGN_KEY					TABLE_NAME			COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_EVENT_NVLD_TME_SLOT	EVENT_NVLD_TME_SLOT	EVENT_ID	EVENT			EVENT_ID

				---------------------------------------------ok-----------------------EVENT------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_NVLD_TME_SLOT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO[dbo].[EVENT_NVLD_TME_SLOT](
					[EVENT_ID]  ,
					[CREAT_DT]  )
				SELECT [EVENT_ID]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[EVENT_NVLD_TME_SLOT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_NVLD_TME_SLOT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EVENT_NVLD_TME_SLOT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT_NVLD_TME_SLOT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_NVLD_TME_SLOT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT 
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_NVLD_TME_SLOT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*

				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_EVENT_REC_LOCK	EVENT_REC_LOCK	LOCK_BY_USER_ID	LK_USER			USER_ID
				FK02_EVENT_REC_LOCK	EVENT_REC_LOCK	EVENT_ID		EVENT			EVENT_ID
				-------------------------------------------------ok------------------------EVENT----------------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_REC_LOCK] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[EVENT_REC_LOCK](
					[EVENT_ID]  ,
					[STRT_REC_LOCK_TMST]  ,
					[LOCK_BY_USER_ID]  ,
					[CREAT_DT] )
				SELECT [EVENT_ID]  ,
					[STRT_REC_LOCK_TMST]  ,
					[LOCK_BY_USER_ID]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[EVENT_REC_LOCK] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_REC_LOCK] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EVENT_REC_LOCK] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT_REC_LOCK] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_REC_LOCK] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_REC_LOCK]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*

				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_EVENT_SUCSS_ACTY	EVENT_SUCSS_ACTY	REC_STUS_ID		LK_REC_STUS		REC_STUS_ID
				FK02_EVENT_SUCSS_ACTY	EVENT_SUCSS_ACTY	EVENT_HIST_ID	EVENT_HIST		EVENT_HIST_ID
				FK03_EVENT_SUCSS_ACTY	EVENT_SUCSS_ACTY	SUCSS_ACTY_ID	LK_SUCSS_ACTY	SUCSS_ACTY_ID
				--------------------------------------------------ok---------------------EVENT---------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_SUCSS_ACTY] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[EVENT_SUCSS_ACTY](
					[EVENT_HIST_ID]  ,
					[SUCSS_ACTY_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_DT]  )
				SELECT 	[EVENT_HIST_ID]  ,
					[SUCSS_ACTY_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[EVENT_SUCSS_ACTY] WITH(NOLOCK) WHERE EVENT_HIST_ID IN ( SELECT EVENT_HIST_ID FROM  [COWS].[arch].[EVENT_HIST] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_SUCSS_ACTY] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EVENT_SUCSS_ACTY] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT_SUCSS_ACTY] WITH(TABLOCKX) WHERE EVENT_HIST_ID IN ( SELECT EVENT_HIST_ID FROM  [COWS].[arch].[EVENT_HIST] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_SUCSS_ACTY] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_SUCSS_ACTY]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY						TABLE_NAME				COLUMN_NAME			REFERANCE_TABLE			REFERANCE_COLUMN_NAME
				FK01_EVENT_TYPE_RSRC_AVLBLTY	EVENT_TYPE_RSRC_AVLBLTY	MODFD_BY_USER_ID	LK_USER					USER_ID
				FK02_EVENT_TYPE_RSRC_AVLBLTY	EVENT_TYPE_RSRC_AVLBLTY	EVENT_TYPE_ID		LK_EVENT_TYPE			EVENT_TYPE_ID
				FK03_EVENT_TYPE_RSRC_AVLBLTY	EVENT_TYPE_RSRC_AVLBLTY	EVENT_TYPE_ID		LK_EVENT_TYPE_TME_SLOT	EVENT_TYPE_ID
				FK03_EVENT_TYPE_RSRC_AVLBLTY	EVENT_TYPE_RSRC_AVLBLTY	TME_SLOT_ID			LK_EVENT_TYPE_TME_SLOT	TME_SLOT_ID

				*/
				--PRINT 'Starting to execute [arch].[EVENT_TYPE_RSRC_AVLBLTY] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				--INSERT INTO [arch].[EVENT_TYPE_RSRC_AVLBLTY](
				--	[EVENT_TYPE_RSRC_AVLBLTY_ID]  ,
				--	[EVENT_TYPE_ID]  ,
				--	[DAY_DT]  ,
				--	[TME_SLOT_ID]  ,
				--	[AVAL_TME_SLOT_CNT]  ,
				--	[MAX_AVAL_TME_SLOT_CAP_CNT]  ,
				--	[CURR_AVAL_TME_SLOT_CNT]  ,
				--	[MODFD_BY_USER_ID] ,
				--	[MODFD_DT] ,
				--	[CREAT_DT]  ,
				--	[EVENT_ID_LIST_TXT] )
					
				--SELECT [EVENT_TYPE_RSRC_AVLBLTY_ID]  ,
				--	[EVENT_TYPE_ID]  ,
				--	[DAY_DT]  ,
				--	[TME_SLOT_ID]  ,
				--	[AVAL_TME_SLOT_CNT]  ,
				--	[MAX_AVAL_TME_SLOT_CAP_CNT]  ,
				--	[CURR_AVAL_TME_SLOT_CNT]  ,
				--	[MODFD_BY_USER_ID] ,
				--	[MODFD_DT] ,
				--	[CREAT_DT]  ,
				--	[EVENT_ID_LIST_TXT] 
				--FROM  [dbo].[EVENT_TYPE_RSRC_AVLBLTY] Where [CREAT_DT]<=CONVERT(datetime,@Date)	
				--PRINT 'Finished executing [arch].[EVENT_TYPE_RSRC_AVLBLTY] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				--PRINT 'Starting to execute [dbo].[EVENT_TYPE_RSRC_AVLBLTY] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				--DELETE  FROM  [dbo].[EVENT_TYPE_RSRC_AVLBLTY] Where [CREAT_DT]<=CONVERT(datetime,@Date)
				--PRINT 'Finished executing [dbo].[EVENT_TYPE_RSRC_AVLBLTY] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_TYPE_RSRC_AVLBLTY]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*

				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_FEDLINE_EVENT_ADR	FEDLINE_EVENT_ADR	FEDLINE_EVENT_ID	FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_ID
				FK02_FEDLINE_EVENT_ADR	FEDLINE_EVENT_ADR	ADR_TYPE_ID			LK_ADR_TYPE	ADR_TYPE_ID
				-----------------------------------------ok-----------------------EVENT--------------------------------

				*/
				/*
				PRINT 'Starting to execute [arch].[FEDLINE_EVENT_ADR] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[FEDLINE_EVENT_ADR](
					[FEDLINE_EVENT_ADR_ID]   ,
					[FEDLINE_EVENT_ID]  ,
					[ADR_TYPE_ID]  ,
					[STREET_1_ADR] ,
					[STREET_2_ADR] ,
					[CITY_NME] ,
					[STT_CD] ,
					[ZIP_CD] ,
					[PRVN_NME] ,
					[CTRY_CD] ,
					[CREAT_DT]  )
				SELECT [FEDLINE_EVENT_ADR_ID]   ,
					[FEDLINE_EVENT_ID]  ,
					[ADR_TYPE_ID]  ,
					[STREET_1_ADR] ,
					[STREET_2_ADR] ,
					[CITY_NME] ,
					[STT_CD] ,
					[ZIP_CD] ,
					[PRVN_NME] ,
					[CTRY_CD] ,
					[CREAT_DT]
				FROM	[COWS].[dbo].[FEDLINE_EVENT_ADR] WITH(NOLOCK) WHERE [FEDLINE_EVENT_ID] IN (
						SELECT [FEDLINE_EVENT_ADR_ID] FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				PRINT 'Finished executing [arch].[FEDLINE_EVENT_ADR] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[FEDLINE_EVENT_ADR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[FEDLINE_EVENT_ADR] WITH(NOLOCK) WHERE [FEDLINE_EVENT_ID] IN (
						SELECT [FEDLINE_EVENT_ADR_ID] FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				PRINT 'Finished executing [dbo].[FEDLINE_EVENT_ADR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FEDLINE_EVENT_ADR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY						TABLE_NAME					COLUMN_NAME			REFERANCE_TABLE				REFERANCE_COLUMN_NAME
				FK01_FEDLINE_EVENT_CFGRN_DATA	FEDLINE_EVENT_CFGRN_DATA	FEDLINE_EVENT_ID	FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_ID
				--------------------------------------------------ok---------------------------EVENT-----------
				*/
				/*
				PRINT 'Starting to execute [arch].[FEDLINE_EVENT_CFGRN_DATA] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[FEDLINE_EVENT_CFGRN_DATA](
					[FEDLINE_EVENT_CFGRN_ID]   ,
					[FEDLINE_EVENT_ID]  ,
					[PORT_NME]  ,
					[IP_ADR]  ,
					[SUBNET_MASK_ADR]  ,
					[DFLT_GTWY_ADR]  ,
					[SPEED_NME]  ,
					[DX_NME]  ,
					[MAC_ADR]  )
				SELECT [FEDLINE_EVENT_CFGRN_ID]   ,
					[FEDLINE_EVENT_ID]  ,
					[PORT_NME]  ,
					[IP_ADR]  ,
					[SUBNET_MASK_ADR]  ,
					[DFLT_GTWY_ADR]  ,
					[SPEED_NME]  ,
					[DX_NME]  ,
					[MAC_ADR]
				FROM	[COWS].[dbo].[FEDLINE_EVENT_CFGRN_DATA] WITH(NOLOCK) WHERE [FEDLINE_EVENT_ID] IN (
						SELECT [FEDLINE_EVENT_ADR_ID] FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				PRINT 'Finished executing [arch].[FEDLINE_EVENT_CFGRN_DATA] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[FEDLINE_EVENT_CFGRN_DATA] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[FEDLINE_EVENT_CFGRN_DATA] WITH(NOLOCK) WHERE [FEDLINE_EVENT_ID] IN (
						SELECT [FEDLINE_EVENT_ADR_ID] FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				PRINT 'Finished executing [dbo].[FEDLINE_EVENT_CFGRN_DATA] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FEDLINE_EVENT_CFGRN_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY					TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE				REFERANCE_COLUMN_NAME
				FK01_FEDLINE_EVENT_CNTCT	FEDLINE_EVENT_CNTCT	FEDLINE_EVENT_ID	FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_ID
				FK02_FEDLINE_EVENT_CNTCT	FEDLINE_EVENT_CNTCT	ROLE_ID				LK_ROLE						ROLE_ID
				FK03_FEDLINE_EVENT_CNTCT	FEDLINE_EVENT_CNTCT	CNTCT_TYPE_ID		LK_CNTCT_TYPE				CNTCT_TYPE_ID
				---------------------------------------------------------ok----------------------EVENT------------------------
				*/
				/*
				PRINT 'Starting to execute [arch].[FEDLINE_EVENT_CNTCT] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[FEDLINE_EVENT_CNTCT](
					[FEDLINE_EVENT_CNTCT_ID]   ,
					[FEDLINE_EVENT_ID]  ,
					[ROLE_ID]  ,
					[CNTCT_TYPE_ID]  ,
					[CNTCT_NME] ,
					[PHN_NBR] ,
					[EMAIL_ADR] ,
					[CREAT_DT]  ,
					[PHN_EXT_NBR]  )
				SELECT  [FEDLINE_EVENT_CNTCT_ID]   ,
					[FEDLINE_EVENT_ID]  ,
					[ROLE_ID]  ,
					[CNTCT_TYPE_ID]  ,
					[CNTCT_NME] ,
					[PHN_NBR] ,
					[EMAIL_ADR] ,
					[CREAT_DT]  ,
					[PHN_EXT_NBR]	
				FROM	[COWS].[dbo].[FEDLINE_EVENT_CNTCT] WITH(NOLOCK) WHERE [FEDLINE_EVENT_ID] IN (
						SELECT [FEDLINE_EVENT_ADR_ID] FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				PRINT 'Finished executing [arch].[FEDLINE_EVENT_CNTCT] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[FEDLINE_EVENT_CNTCT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[FEDLINE_EVENT_CNTCT] WITH(NOLOCK) WHERE [FEDLINE_EVENT_ID] IN (
						SELECT [FEDLINE_EVENT_ADR_ID] FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				PRINT 'Finished executing [dbo].[FEDLINE_EVENT_CNTCT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FEDLINE_EVENT_CNTCT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE				REFERANCE_COLUMN_NAME
				FK01_FEDLINE_EVENT_MSG	FEDLINE_EVENT_MSG	FEDLINE_EVENT_ID	FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_ID
				FK02_FEDLINE_EVENT_MSG	FEDLINE_EVENT_MSG	STUS_ID				LK_STUS						STUS_ID
				--------------------------------------------------ok-------------------------EVENT-------------
				*/
				/*
				PRINT 'Starting to execute [arch].[FEDLINE_EVENT_MSG] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[FEDLINE_EVENT_MSG](
					[FEDLINE_EVENT_MSG_ID]   ,
					[FEDLINE_EVENT_ID]  ,
					[FEDLINE_EVENT_MSG_STUS_NME]  ,
					[RSPN_DES]  ,
					[STUS_ID]  ,
					[CREAT_DT]  ,
					[SENT_DT] )
				SELECT  [FEDLINE_EVENT_MSG_ID]   ,
					[FEDLINE_EVENT_ID]  ,
					[FEDLINE_EVENT_MSG_STUS_NME]  ,
					[RSPN_DES]  ,
					[STUS_ID]  ,
					[CREAT_DT]  ,
					[SENT_DT]
				FROM	[COWS].[dbo].[FEDLINE_EVENT_MSG] WITH(NOLOCK) WHERE [FEDLINE_EVENT_ID] IN (
						SELECT [FEDLINE_EVENT_ADR_ID] FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				PRINT 'Finished executing [arch].[FEDLINE_EVENT_MSG] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				
				PRINT 'Starting to execute [dbo].[FEDLINE_EVENT_MSG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[FEDLINE_EVENT_MSG] WITH(NOLOCK) WHERE [FEDLINE_EVENT_ID] IN (
						SELECT [FEDLINE_EVENT_ADR_ID] FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table))
				PRINT 'Finished executing [dbo].[FEDLINE_EVENT_MSG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FEDLINE_EVENT_MSG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY						TABLE_NAME					COLUMN_NAME			REFERANCE_TABLE			REFERANCE_COLUMN_NAME
				FK01_FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_TADPOLE_DATA	EVENT_ID			EVENT					EVENT_ID
				FK02_FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_TADPOLE_DATA	ORDR_TYPE_CD		LK_FEDLINE_ORDR_TYPE	ORDR_TYPE_CD
				FK03_FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_TADPOLE_DATA	REC_STUS_ID			LK_REC_STUS				REC_STUS_ID
				FK04_FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_TADPOLE_DATA	FRB_ORDR_TYPE_CD	LK_FEDLINE_ORDR_TYPE	ORDR_TYPE_CD
				---------------------------------------------ok---------------------------EVENT--------------
				FOREIGN_KEY						TABLE_NAME					COLUMN_NAME			REFERANCE_TABLE				REFERANCE_COLUMN_NAME
				FK01_FEDLINE_EVENT_ADR			FEDLINE_EVENT_ADR			FEDLINE_EVENT_ID	FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_ID
				FK01_FEDLINE_EVENT_CFGRN_DATA	FEDLINE_EVENT_CFGRN_DATA	FEDLINE_EVENT_ID	FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_ID
				FK01_FEDLINE_EVENT_CNTCT		FEDLINE_EVENT_CNTCT			FEDLINE_EVENT_ID	FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_ID
				FK01_FEDLINE_EVENT_MSG			FEDLINE_EVENT_MSG			FEDLINE_EVENT_ID	FEDLINE_EVENT_TADPOLE_DATA	FEDLINE_EVENT_ID

				
				*/
				/*
				PRINT 'Starting to execute [arch].[FEDLINE_EVENT_TADPOLE_DATA] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[FEDLINE_EVENT_TADPOLE_DATA](
					[FEDLINE_EVENT_ID]   ,
					[EVENT_ID]  ,
					[FRB_REQ_ID]  ,
					[SEQ_NBR]  ,
					[ORDR_TYPE_CD]  ,
					[DSGN_TYPE_CD]  ,
					[STRT_TME] ,
					[END_TME] ,
					[DEV_NME]  ,
					[DEV_SERIAL_NBR]  ,
					[DEV_MODEL_NBR]  ,
					[DEV_VNDR_NME]  ,
					[CREAT_DT]  ,
					[REC_STUS_ID]  ,
					[FRB_ORDR_TYPE_CD]  ,
					[IS_MOVE_CD]  ,
					[XST_DEV_NME]  ,
					[IS_EXPEDITE_CD]  ,
					[REQ_RECV_DT] ,
					[ORG_ID]  ,
					[FRB_INSTL_TYPE_NME]  ,
					[ORDR_SUB_TYPE_NME]  ,
					[REPLMT_REAS_NME]  ,
					[CUST_NME]  )
				SELECT  [FEDLINE_EVENT_ID]   ,
					[EVENT_ID]  ,
					[FRB_REQ_ID]  ,
					[SEQ_NBR]  ,
					[ORDR_TYPE_CD]  ,
					[DSGN_TYPE_CD]  ,
					[STRT_TME] ,
					[END_TME] ,
					[DEV_NME]  ,
					[DEV_SERIAL_NBR]  ,
					[DEV_MODEL_NBR]  ,
					[DEV_VNDR_NME]  ,
					[CREAT_DT]  ,
					[REC_STUS_ID]  ,
					[FRB_ORDR_TYPE_CD]  ,
					[IS_MOVE_CD]  ,
					[XST_DEV_NME]  ,
					[IS_EXPEDITE_CD]  ,
					[REQ_RECV_DT] ,
					[ORG_ID]  ,
					[FRB_INSTL_TYPE_NME]  ,
					[ORDR_SUB_TYPE_NME]  ,
					[REPLMT_REAS_NME]  ,
					[CUST_NME]
				FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				PRINT 'Finished executing [arch].[FEDLINE_EVENT_TADPOLE_DATA] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[FEDLINE_EVENT_TADPOLE_DATA] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				PRINT 'Finished executing [dbo].[FEDLINE_EVENT_TADPOLE_DATA] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FEDLINE_EVENT_TADPOLE_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*

				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_FEDLINE_EVENT_PRCS	FEDLINE_EVENT_PRCS	EVENT_ID			EVENT				EVENT_ID
				FK02_FEDLINE_EVENT_PRCS	FEDLINE_EVENT_PRCS	EVENT_STUS_ID		LK_EVENT_STUS		EVENT_STUS_ID
				FK03_FEDLINE_EVENT_PRCS	FEDLINE_EVENT_PRCS	EVENT_END_STUS_ID	LK_EVENT_STUS		EVENT_STUS_ID
				------------------------------------------------ok-------------------------------EVENT---------
				*/
				/*
				PRINT 'Starting to execute [arch].[FEDLINE_EVENT_PRCS] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[FEDLINE_EVENT_PRCS](
					[FEDLINE_EVENT_PRCS_ID]   ,
					[EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[STRT_TME] ,
					[END_TME] ,
					[CREAT_DT] ,
					[EVENT_END_STUS_ID] )

				SELECT [FEDLINE_EVENT_PRCS_ID]   ,
					[EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[STRT_TME] ,
					[END_TME] ,
					[CREAT_DT] ,
					[EVENT_END_STUS_ID]
				FROM	[COWS].[dbo].[FEDLINE_EVENT_PRCS] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				PRINT 'Finished executing [arch].[FEDLINE_EVENT_PRCS] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				
				PRINT 'Starting to execute [dbo].[FEDLINE_EVENT_PRCS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[FEDLINE_EVENT_PRCS] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				PRINT 'Finished executing [dbo].[FEDLINE_EVENT_PRCS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FEDLINE_EVENT_PRCS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY						TABLE_NAME				COLUMN_NAME			REFERANCE_TABLE				REFERANCE_COLUMN_NAME
				FK01_FEDLINE_EVENT_USER_DATA	FEDLINE_EVENT_USER_DATA	EVENT_ID			EVENT						EVENT_ID
				FK02_FEDLINE_EVENT_USER_DATA	FEDLINE_EVENT_USER_DATA	EVENT_STUS_ID		LK_EVENT_STUS				EVENT_STUS_ID
				FK03_FEDLINE_EVENT_USER_DATA	FEDLINE_EVENT_USER_DATA	ENGR_USER_ID		LK_USER						USER_ID
				FK04_FEDLINE_EVENT_USER_DATA	FEDLINE_EVENT_USER_DATA	ACTV_USER_ID		LK_USER						USER_ID
				FK05_FEDLINE_EVENT_USER_DATA	FEDLINE_EVENT_USER_DATA	MODFD_BY_USER_ID	LK_USER						USER_ID
				FK06_FEDLINE_EVENT_USER_DATA	FEDLINE_EVENT_USER_DATA	FAIL_CD_ID			LK_FEDLINE_EVENT_FAIL_CD	FAIL_CD_ID
				------------------------------------------------------------ok----------------------EVENT---------------------

				*/
				/*
				PRINT 'Starting to execute [arch].[FEDLINE_EVENT_USER_DATA] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[FEDLINE_EVENT_USER_DATA](
					[EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[EVENT_TITLE_TXT]  ,
					[CMPLT_EMAIL_CC_ADR]  ,
					[ENGR_USER_ID] ,
					[SHIPPING_CXR_NME]  ,
					[TRK_NBR]  ,
					[ARRIVAL_DT] [date] ,
					[PRE_STG_CMTLT_CD]  ,
					[ACTV_USER_ID] ,
					[FAIL_CD_ID] ,
					[MODFD_DT] ,
					[MODFD_BY_USER_ID] ,
					[DEV_TYPE_NME]  ,
					[DOC_LINK_TXT]  ,
					[RDSN_NBR]  ,
					[TNNL_CD] ,
					[SLA_CD] )
					
				SELECT [EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[EVENT_TITLE_TXT]  ,
					[CMPLT_EMAIL_CC_ADR]  ,
					[ENGR_USER_ID] ,
					[SHIPPING_CXR_NME]  ,
					[TRK_NBR]  ,
					[ARRIVAL_DT] [date] ,
					[PRE_STG_CMTLT_CD]  ,
					[ACTV_USER_ID] ,
					[FAIL_CD_ID] ,
					[MODFD_DT] ,
					[MODFD_BY_USER_ID] ,
					[DEV_TYPE_NME]  ,
					[DOC_LINK_TXT]  ,
					[RDSN_NBR]  ,
					[TNNL_CD] ,
					[SLA_CD]
				FROM	[COWS].[dbo].[FEDLINE_EVENT_USER_DATA] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				PRINT 'Finished executing [arch].[FEDLINE_EVENT_USER_DATA] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[FEDLINE_EVENT_USER_DATA] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[FEDLINE_EVENT_USER_DATA] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				PRINT 'Finished executing [dbo].[FEDLINE_EVENT_USER_DATA] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FEDLINE_EVENT_USER_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				----------------------------------
				*/
				/*
				PRINT 'Starting to execute [arch].[FEDLINE_EVENT_TADPOLE_XML] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[FEDLINE_EVENT_TADPOLE_XML](
					[MSG_XML_ID]   ,
					[MSG_XML_TXT]  ,
					[ERROR_MSG_TXT] ,
					[CREAT_DT]  ,
					[IS_ERROR_CD]  ,
					[IS_REJECT_CD]  ,
					[FRB_REQ_ID] )
				SELECT  [MSG_XML_ID]   ,
					[MSG_XML_TXT]  ,
					[ERROR_MSG_TXT] ,
					[CREAT_DT]  ,
					[IS_ERROR_CD]  ,
					[IS_REJECT_CD]  ,
					[FRB_REQ_ID]
				FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_XML] WITH(NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [arch].[FEDLINE_EVENT_TADPOLE_XML] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				
				PRINT 'Starting to execute [dbo].[FEDLINE_EVENT_TADPOLE_XML] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_XML] WITH(NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [dbo].[FEDLINE_EVENT_TADPOLE_XML] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FEDLINE_EVENT_TADPOLE_XML]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY					TABLE_NAME				COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_FEDLINE_REQ_ACT_HIST	FEDLINE_REQ_ACT_HIST	EVENT_ID		EVENT			EVENT_ID
				FK02_FEDLINE_REQ_ACT_HIST	FEDLINE_REQ_ACT_HIST	EVENT_HIST_ID	EVENT_HIST		EVENT_HIST_ID
				-------------------------------------------------ok----------------------EVENT--------------------
				*/
				/*
				PRINT 'Starting to execute [arch].[FEDLINE_REQ_ACT_HIST] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[FEDLINE_REQ_ACT_HIST](
					[REQ_ACT_HIST_ID]   ,
					[EVENT_ID]  ,
					[OLD_DT] ,
					[NEW_DT] ,
					[FAIL_CD_ID] ,
					[CREAT_DT] ,
					[EVENT_HIST_ID] )
				SELECT  [REQ_ACT_HIST_ID]   ,
					[EVENT_ID]  ,
					[OLD_DT] ,
					[NEW_DT] ,
					[FAIL_CD_ID] ,
					[CREAT_DT] ,
					[EVENT_HIST_ID]
				FROM	[COWS].[dbo].[FEDLINE_REQ_ACT_HIST] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				PRINT 'Finished executing [arch].[FEDLINE_REQ_ACT_HIST] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[FEDLINE_REQ_ACT_HIST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[FEDLINE_REQ_ACT_HIST] WITH(NOLOCK) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				PRINT 'Finished executing [dbo].[FEDLINE_REQ_ACT_HIST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FEDLINE_REQ_ACT_HIST]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_EVENT_HIST	EVENT_HIST	MODFD_BY_USER_ID	LK_USER			USER_ID
				FK02_EVENT_HIST	EVENT_HIST	CREAT_BY_USER_ID	LK_USER			USER_ID
				FK03_EVENT_HIST	EVENT_HIST	EVENT_ID			EVENT			EVENT_ID
				FK04_EVENT_HIST	EVENT_HIST	ACTN_ID				LK_ACTN			ACTN_ID
				FK05_EVENT_HIST	EVENT_HIST	FAIL_REAS_ID		LK_FAIL_REAS	FAIL_REAS_ID
				FK06_EVENT_HIST	EVENT_HIST	FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				----------------------------------------------ok------------------------EVENT-----------------
				FOREIGN_KEY					TABLE_NAME				COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK02_EVENT_FAIL_ACTY		EVENT_FAIL_ACTY			EVENT_HIST_ID	EVENT_HIST		EVENT_HIST_ID
				FK02_EVENT_SUCSS_ACTY		EVENT_SUCSS_ACTY		EVENT_HIST_ID	EVENT_HIST		EVENT_HIST_ID
				FK02_FEDLINE_REQ_ACT_HIST	FEDLINE_REQ_ACT_HIST	EVENT_HIST_ID	EVENT_HIST		EVENT_HIST_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. EVENT_HIST     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[EVENT_HIST] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[EVENT_HIST](
					[EVENT_HIST_ID]   ,
					[EVENT_ID]  ,
					[ACTN_ID]  ,
					[CMNT_TXT] ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[PRE_CFG_CMPLT_CD]  ,
					[FAIL_REAS_ID] ,
					[FSA_MDS_EVENT_ID] ,
					[EVENT_STRT_TMST] ,
					[EVENT_END_TMST] )
				SELECT [EVENT_HIST_ID]   ,
					[EVENT_ID]  ,
					[ACTN_ID]  ,
					[CMNT_TXT] ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[PRE_CFG_CMPLT_CD]  ,
					[FAIL_REAS_ID] ,
					[FSA_MDS_EVENT_ID] ,
					[EVENT_STRT_TMST] ,
					[EVENT_END_TMST]
				FROM	[COWS].[arch].[EVENT_HIST] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT_HIST] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. EVENT_HIST     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[EVENT_HIST] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT_HIST] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT_HIST] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT_HIST]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				----[M5_REDSGN_MSG] Added July 03 2018
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[M5_REDSGN_MSG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[M5_REDSGN_MSG]     ON
				INSERT INTO [COWS].[dbo].[M5_REDSGN_MSG]
					   ([M5_REDSGN_MSG_ID]
					  ,[REDSGN_DEV_ID]
					  ,[CHARGE_AMT]
					  ,[SITE]
					  ,[STUS_ID]
					  ,[CREAT_DT]
					  ,[SENT_DT]
					  ,[EVENT_ID])
				SELECT [M5_REDSGN_MSG_ID]
					  ,[REDSGN_DEV_ID]
					  ,[CHARGE_AMT]
					  ,[SITE]
					  ,[STUS_ID]
					  ,[CREAT_DT]
					  ,[SENT_DT]
					  ,[EVENT_ID]
				FROM [COWS].[arch].[M5_REDSGN_MSG] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[M5_REDSGN_MSG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[M5_REDSGN_MSG]      OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[M5_EVENT_MSG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE FROM	[COWS].[arch].[M5_REDSGN_MSG] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[M5_REDSGN_MSG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[M5_REDSGN_MSG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			
			END
			
			
			IF(@Mood=0 )  /* ORDER ONLY  */
			BEGIN
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_FSA_IP_ADR	FSA_IP_ADR	ORDR_ID		FSA_ORDR		ORDR_ID
				-------------------------------------------------ok---------------ORDER-->ORDERID--------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_IP_ADR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[FSA_IP_ADR](
					[ORDR_ID]  ,
					[IP_ADR_TYPE_CD]  ,
					[IP_ADR]  ,
					[CREAT_DT]  )
				SELECT [ORDR_ID]  ,
					[IP_ADR_TYPE_CD]  ,
					[IP_ADR]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[FSA_IP_ADR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_IP_ADR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_IP_ADR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[FSA_IP_ADR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_IP_ADR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_IP_ADR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			END
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_FSA_LAYOUT_COL	FSA_LAYOUT_COL	LAYOUT_ID	LK_FSA_LAYOUT	LAYOUT_ID
			FK02_FSA_LAYOUT_COL	FSA_LAYOUT_COL	LVL_ID		LK_FSA_LVL	LVL_ID
			FK04_FSA_LAYOUT_COL	FSA_LAYOUT_COL	LVL_ID		LK_FSA_LVL_COL	LVL_ID
			FK04_FSA_LAYOUT_COL	FSA_LAYOUT_COL	DB_COL_NME	LK_FSA_LVL_COL	DB_COL_NME

			*/
			--PRINT 'Starting to execute [arch].[FSA_LAYOUT_COL] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[FSA_LAYOUT_COL](
			--	[LAYOUT_ID]  ,
			--	[LVL_ID]  ,
			--	[DB_COL_NME]  ,
			--	[UI_NME]  ,
			--	[SRC_TBL_NME]  ,
			--	[CREAT_DT]  ,
			--	[COL_POS_NBR] ,
			--	[RPET_SECT_CD]  )
			--SELECT [LAYOUT_ID]  ,
			--	[LVL_ID]  ,
			--	[DB_COL_NME]  ,
			--	[UI_NME]  ,
			--	[SRC_TBL_NME]  ,
			--	[CREAT_DT]  ,
			--	[COL_POS_NBR] ,
			--	[RPET_SECT_CD]
			--FROM  [dbo].[FSA_LAYOUT_COL] Where [CREAT_DT]<=CONVERT(datetime,@Date)	
			--PRINT 'Finished executing [arch].[FSA_LAYOUT_COL] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'	
			
			--PRINT 'Starting to execute [dbo].[FSA_LAYOUT_COL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE FROM  [dbo].[FSA_LAYOUT_COL] Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [dbo].[FSA_LAYOUT_COL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.' 
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_LAYOUT_COL]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_FSA_LAYOUT_PROD_ORDR_TYPE	FSA_LAYOUT_PROD_ORDR_TYPE	LAYOUT_ID		LK_FSA_LAYOUT	LAYOUT_ID
			FK02_FSA_LAYOUT_PROD_ORDR_TYPE	FSA_LAYOUT_PROD_ORDR_TYPE	PROD_TYPE_CD	LK_FSA_PROD_TYPE	FSA_PROD_TYPE_CD
			FK03_FSA_LAYOUT_PROD_ORDR_TYPE	FSA_LAYOUT_PROD_ORDR_TYPE	ORDR_TYPE_CD	LK_FSA_ORDR_TYPE	FSA_ORDR_TYPE_CD

			*/
			--PRINT 'Starting to execute [arch].[FSA_LAYOUT_PROD_ORDR_TYPE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[FSA_LAYOUT_PROD_ORDR_TYPE](
			--	[LAYOUT_ID]  ,
			--	[PROD_TYPE_CD]  ,
			--	[ORDR_TYPE_CD]  ,
			--	[CREAT_DT]  ,
			--	[INTL_CD]  )
			--SELECT [LAYOUT_ID]  ,
			--	[PROD_TYPE_CD]  ,
			--	[ORDR_TYPE_CD]  ,
			--	[CREAT_DT]  ,
			--	[INTL_CD]
			--FROM   [dbo].[FSA_LAYOUT_PROD_ORDR_TYPE]  Where [CREAT_DT]<=CONVERT(datetime,@Date)	
			--PRINT 'Finished executing [arch].[FSA_LAYOUT_PROD_ORDR_TYPE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			
			--PRINT 'Starting to execute [dbo].[FSA_LAYOUT_PROD_ORDR_TYPE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE FROM   [dbo].[FSA_LAYOUT_PROD_ORDR_TYPE]  Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [dbo].[FSA_LAYOUT_PROD_ORDR_TYPE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_LAYOUT_PROD_ORDR_TYPE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_FSA_LVL_HIER	FSA_LVL_HIER	LAYOUT_ID	LK_FSA_LAYOUT	LAYOUT_ID
			FK02_FSA_LVL_HIER	FSA_LVL_HIER	LVL_ID	LK_FSA_LVL	LVL_ID
			FK03_FSA_LVL_HIER	FSA_LVL_HIER	SUB_LVL_ID	LK_FSA_LVL	LVL_ID

			*/
			--PRINT 'Starting to execute [arch].[FSA_LVL_HIER] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[FSA_LVL_HIER](
			--	[LAYOUT_ID]  ,
			--	[LVL_ID]  ,
			--	[SUB_LVL_ID]  ,
			--	[CREAT_DT] )
			--SELECT 	[LAYOUT_ID]  ,
			--	[LVL_ID]  ,
			--	[SUB_LVL_ID]  ,
			--	[CREAT_DT]
			--FROM  [dbo].[FSA_LVL_HIER] Where [CREAT_DT]<=CONVERT(datetime,@Date)	
			--PRINT 'Finished executing [arch].[FSA_LVL_HIER] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[FSA_LVL_HIER] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM  [dbo].[FSA_LVL_HIER] Where [CREAT_DT]<=CONVERT(datetime,@Date)	
			--PRINT 'Finished executing [dbo].[FSA_LVL_HIER] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_LVL_HIER]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			/*
			FOREIGN_KEY				TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
			FK01_MDS_FAST_TRK_TYPE	MDS_FAST_TRK_TYPE	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID
			FK02_MDS_FAST_TRK_TYPE	MDS_FAST_TRK_TYPE	CREAT_BY_USER_ID	LK_USER				USER_ID
			FK03_MDS_FAST_TRK_TYPE	MDS_FAST_TRK_TYPE	MODFD_BY_USER_ID	LK_USER				USER_ID
			----------------------------------------------------------------------------------------------------------
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK14_MDS_EVENT		MDS_EVENT		MDS_FAST_TRK_TYPE_ID	MDS_FAST_TRK_TYPE	MDS_FAST_TRK_TYPE_ID
			FK02_MDS_EVENT_NEW	MDS_EVENT_NEW	MDS_FAST_TRK_TYPE_ID	MDS_FAST_TRK_TYPE	MDS_FAST_TRK_TYPE_ID

			*/
			
			--PRINT 'Starting to execute [arch].[MDS_FAST_TRK_TYPE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[MDS_FAST_TRK_TYPE](
			--	[MDS_FAST_TRK_TYPE_ID]  ,
			--	[MDS_FAST_TRK_TYPE_DES]  ,
			--	[REC_STUS_ID]  ,
			--	[CREAT_BY_USER_ID]  ,
			--	[MODFD_BY_USER_ID] ,
			--	[MODFD_DT] ,
			--	[CREAT_DT]  )
			--SELECT [MDS_FAST_TRK_TYPE_ID]  ,
			--	[MDS_FAST_TRK_TYPE_DES]  ,
			--	[REC_STUS_ID]  ,
			--	[CREAT_BY_USER_ID]  ,
			--	[MODFD_BY_USER_ID] ,
			--	[MODFD_DT] ,
			--	[CREAT_DT]
			--FROM  [dbo].[MDS_FAST_TRK_TYPE] Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [arch].[MDS_FAST_TRK_TYPE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[MDS_FAST_TRK_TYPE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM  [dbo].[MDS_FAST_TRK_TYPE] Where [CREAT_DT]<=CONVERT(datetime,@Date)
			--PRINT 'Finished executing [dbo].[MDS_FAST_TRK_TYPE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_FAST_TRK_TYPE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			IF(@Mood=5)  /* MDS EVENT ONLY */
			BEGIN
				/*
				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME				REFERANCE_TABLE			REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_NEW	MDS_EVENT_NEW	EVENT_ID				EVENT					EVENT_ID
				FK02_MDS_EVENT_NEW	MDS_EVENT_NEW	MDS_FAST_TRK_TYPE_ID	MDS_FAST_TRK_TYPE		MDS_FAST_TRK_TYPE_ID
				FK03_MDS_EVENT_NEW	MDS_EVENT_NEW	EVENT_STUS_ID			LK_EVENT_STUS			EVENT_STUS_ID
				FK04_MDS_EVENT_NEW	MDS_EVENT_NEW	OLD_EVENT_STUS_ID		LK_EVENT_STUS			EVENT_STUS_ID
				FK05_MDS_EVENT_NEW	MDS_EVENT_NEW	WRKFLW_STUS_ID			LK_WRKFLW_STUS			WRKFLW_STUS_ID
				FK07_MDS_EVENT_NEW	MDS_EVENT_NEW	FAIL_REAS_ID			LK_FAIL_REAS			FAIL_REAS_ID
				FK08_MDS_EVENT_NEW	MDS_EVENT_NEW	CREAT_BY_USER_ID		LK_USER					USER_ID
				FK09_MDS_EVENT_NEW	MDS_EVENT_NEW	MODFD_BY_USER_ID		LK_USER					USER_ID
				FK10_MDS_EVENT_NEW	MDS_EVENT_NEW	REC_STUS_ID				LK_REC_STUS				REC_STUS_ID
				FK11_MDS_EVENT_NEW	MDS_EVENT_NEW	TME_SLOT_ID				LK_EVENT_TYPE_TME_SLOT	TME_SLOT_ID
				FK11_MDS_EVENT_NEW	MDS_EVENT_NEW	EVENT_TYPE_ID			LK_EVENT_TYPE_TME_SLOT	EVENT_TYPE_ID
				FK12_MDS_EVENT_NEW	MDS_EVENT_NEW	ESCL_REAS_ID			LK_ESCL_REAS			ESCL_REAS_ID
				FK13_MDS_EVENT_NEW	MDS_EVENT_NEW	REQOR_USER_ID			LK_USER					USER_ID
				FK14_MDS_EVENT_NEW	MDS_EVENT_NEW	MDS_BRDG_NEED_ID		LK_MDS_BRDG_NEED_REAS	MDS_BRDG_NEED_ID
				-----------------------------------OK----------------------------EVENT---------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_NEW] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_NEW](
					[EVENT_ID]  ,
					[MDS_FAST_TRK_CD]  ,
					[MDS_FAST_TRK_TYPE_ID]  ,
					[EVENT_STUS_ID]  ,
					[OLD_EVENT_STUS_ID]  ,
					[WRKFLW_STUS_ID]  ,
					[CHARS_ID] ,
					[H1_ID]  ,
					[MDS_ACTY_TYPE_ID]  ,
					[EVENT_TITLE_TXT] ,
					[CUST_NME]  ,
					[CUST_EMAIL_ADR] ,
					[CUST_ACCT_TEAM_PDL_NME]  ,
					[SHRT_DES]  ,
					[SOWS_EVENT_ID] ,
					[FAIL_REAS_ID] ,
					[PRE_CFG_CMPLT_CD]  ,
					[CREAT_BY_USER_ID] ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[FULL_CUST_DISC_CD]  ,
					[DISCO_DEV_CNT] ,
					[DSGN_DOC_LOC_TXT]  ,
					[DISC_NTFY_PDL_NME]  ,
					[CNFRC_BRDG_NBR]  ,
					[CNFRC_PIN_NBR] ,
					[ALT_CNFRC_BRG_NME]  ,
					[STRT_TMST]  ,
					[EXTRA_DRTN_TME_AMT]  ,
					[END_TMST]  ,
					[REC_STUS_ID]  ,
					[EVENT_DRTN_IN_MIN_QTY]  ,
					[TME_SLOT_ID]  ,
					[EVENT_TYPE_ID] ,
					[SPRINT_CPE_NCR_ID]  ,
					[SRVC_ASSRN_SITE_SUPP_ID]  ,
					[CPE_DSPCH_EMAIL_ADR]  ,
					[CPE_DSPCH_CMNT_TXT]  ,
					[ESCL_CD]  ,
					[ESCL_REAS_ID]  ,
					[PRIM_REQ_DT] ,
					[SCNDY_REQ_DT] ,
					[BUS_JUSTN_TXT]  ,
					[REQOR_USER_ID] ,
					[REQOR_CELL_PHN_NBR]  ,
					[PUB_EMAIL_CC_TXT]  ,
					[CMPLTD_EMAIL_CC_TXT]  ,
					[MNS_PM_ID]  ,
					[ODIE_CUST_ID]  ,
					[SPRS_EMAIL_ON_PUB_CD]  ,
					[OPT_OUT_REAS_TXT]  ,
					[DEV_CNT] ,
					--[CSG_LVL_CD] ,
					[BUS_JUSTN_CMNT_TXT]  ,
					[MDS_BRDG_NEED_ID]  ,
					[ESCL_BUS_JUST_TXT] ,
					[ONLINE_MEETING_ADR]  ,
					[FULFILLED_DT] ,
					[SHIPPED_DT] ,
					[SHIP_TRK_REFR_NBR]  ,
					[SHIP_CUST_NME]  ,
					[SHIP_CUST_EMAIL_ADR]  ,
					[SHIP_CUST_PHN_NBR]  ,
					[WOOB_IP_ADR]  ,
					[DEV_SERIAL_NBR],
					[MDS_BRDG_NEED_CD]  )
				SELECT [EVENT_ID]  ,
					[MDS_FAST_TRK_CD]  ,
					[MDS_FAST_TRK_TYPE_ID]  ,
					[EVENT_STUS_ID]  ,
					[OLD_EVENT_STUS_ID]  ,
					[WRKFLW_STUS_ID]  ,
					[CHARS_ID] ,
					[H1_ID]  ,
					[MDS_ACTY_TYPE_ID]  ,
					[EVENT_TITLE_TXT] ,
					[CUST_NME]  ,
					[CUST_EMAIL_ADR] ,
					[CUST_ACCT_TEAM_PDL_NME]  ,
					[SHRT_DES]  ,
					[SOWS_EVENT_ID] ,
					[FAIL_REAS_ID] ,
					[PRE_CFG_CMPLT_CD]  ,
					[CREAT_BY_USER_ID] ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[FULL_CUST_DISC_CD]  ,
					[DISCO_DEV_CNT] ,
					[DSGN_DOC_LOC_TXT]  ,
					[DISC_NTFY_PDL_NME]  ,
					[CNFRC_BRDG_NBR]  ,
					[CNFRC_PIN_NBR] ,
					[ALT_CNFRC_BRG_NME]  ,
					[STRT_TMST]  ,
					[EXTRA_DRTN_TME_AMT]  ,
					[END_TMST]  ,
					[REC_STUS_ID]  ,
					[EVENT_DRTN_IN_MIN_QTY]  ,
					[TME_SLOT_ID]  ,
					[EVENT_TYPE_ID] ,
					[SPRINT_CPE_NCR_ID]  ,
					[SRVC_ASSRN_SITE_SUPP_ID]  ,
					[CPE_DSPCH_EMAIL_ADR]  ,
					[CPE_DSPCH_CMNT_TXT]  ,
					[ESCL_CD]  ,
					[ESCL_REAS_ID]  ,
					[PRIM_REQ_DT] ,
					[SCNDY_REQ_DT] ,
					[BUS_JUSTN_TXT]  ,
					[REQOR_USER_ID] ,
					[REQOR_CELL_PHN_NBR]  ,
					[PUB_EMAIL_CC_TXT]  ,
					[CMPLTD_EMAIL_CC_TXT]  ,
					[MNS_PM_ID]  ,
					[ODIE_CUST_ID]  ,
					[SPRS_EMAIL_ON_PUB_CD]  ,
					[OPT_OUT_REAS_TXT]  ,
					[DEV_CNT] ,
					--[CSG_LVL_CD] ,
					[BUS_JUSTN_CMNT_TXT]  ,
					[MDS_BRDG_NEED_ID]  ,
					[ESCL_BUS_JUST_TXT] ,
					[ONLINE_MEETING_ADR]  ,
					[FULFILLED_DT] ,
					[SHIPPED_DT] ,
					[SHIP_TRK_REFR_NBR]  ,
					[SHIP_CUST_NME]  ,
					[SHIP_CUST_EMAIL_ADR]  ,
					[SHIP_CUST_PHN_NBR]  ,
					[WOOB_IP_ADR]  ,
					[DEV_SERIAL_NBR],
					[MDS_BRDG_NEED_CD]
				FROM	[COWS].[arch].[MDS_EVENT_NEW] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_NEW] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_NEW] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_NEW] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_NEW] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_NEW]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_NEW	EVENT_ID			MDS_EVENT_NEW		EVENT_ID
				FK02_FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_NEW	ORDR_EVENT_STUS_ID	LK_EVENT_STUS		EVENT_STUS_ID
				FK03_FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_NEW	CREAT_BY_USER_ID	LK_USER				USER_ID
				FK04_FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_NEW	MODFD_BY_USER_ID	LK_USER				USER_ID
				--------------------------------------------OK----------------------------EVENT------------------
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_NEW	EVENT_ID			MDS_EVENT_NEW	EVENT_ID
				FK02_FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_NEW	ORDR_EVENT_STUS_ID	LK_EVENT_STUS	EVENT_STUS_ID
				FK03_FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_NEW	CREAT_BY_USER_ID	LK_USER			USER_ID
				FK04_FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_NEW	MODFD_BY_USER_ID	LK_USER			USER_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. FSA_MDS_EVENT_NEW     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_MDS_EVENT_NEW] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[FSA_MDS_EVENT_NEW](
					[FSA_MDS_EVENT_ID]   ,
					[EVENT_ID]  ,
					[TAB_NME]  ,
					[ORDR_EVENT_STUS_ID]  ,
					[TAB_SEQ_NBR]  ,
					[H5_H6_CUST_ID]  ,
					[CMPLTD_CD]  ,
					[TAB_DLTD_CD]  ,
					[INSTL_SITE_POC_NME] ,
					[INSTL_SITE_POC_INTL_PHN_CD]  ,
					[INSTL_SITE_POC_PHN_NBR]  ,
					[INSTL_SITE_POC_INTL_CELL_PHN_CD]  ,
					[INSTL_SITE_POC_CELL_PHN_NBR]  ,
					[SRVC_ASSRN_POC_NME] ,
					[SRVC_ASSRN_POC_INTL_PHN_CD]  ,
					[SRVC_ASSRN_POC_PHN_NBR]  ,
					[SRVC_ASSRN_POC_INTL_CELL_PHN_CD] ,
					[SRVC_ASSRN_POC_CELL_NBR] ,
					[US_INTL_ID]  ,
					[SITE_ADR]  ,
					[FLR_BLDG_NME] ,
					[CTY_NME] ,
					[STT_PRVN_NME]  ,
					[CTRY_RGN_NME]  ,
					[ZIP_CD]  ,
					[WIRED_DEV_TRPT_REQR_CD]  ,
					[WRLS_TRPT_REQR_CD]  ,
					[VRTL_CNCTN_CD]  ,
					[CREAT_BY_USER_ID] ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[EVENT_SCHED_NTE_TXT]  ,
					[SPRF_DEV_TRNSPRT_REQR_CD]  ,
					[SPAE_TRNSPRT_REQR_CD]  ,
					[SA_POC_HR_NME]  ,
					[DSL_TRNSPRT_REQR_CD]  ,
					[TME_ZONE_ID],  
					[MACH5_SRVC_REQR_CD]  )
					
				SELECT  [FSA_MDS_EVENT_ID]   ,
					[EVENT_ID]  ,
					[TAB_NME]  ,
					[ORDR_EVENT_STUS_ID]  ,
					[TAB_SEQ_NBR]  ,
					[H5_H6_CUST_ID]  ,
					[CMPLTD_CD]  ,
					[TAB_DLTD_CD]  ,
					[INSTL_SITE_POC_NME] ,
					[INSTL_SITE_POC_INTL_PHN_CD]  ,
					[INSTL_SITE_POC_PHN_NBR]  ,
					[INSTL_SITE_POC_INTL_CELL_PHN_CD]  ,
					[INSTL_SITE_POC_CELL_PHN_NBR]  ,
					[SRVC_ASSRN_POC_NME] ,
					[SRVC_ASSRN_POC_INTL_PHN_CD]  ,
					[SRVC_ASSRN_POC_PHN_NBR]  ,
					[SRVC_ASSRN_POC_INTL_CELL_PHN_CD] ,
					[SRVC_ASSRN_POC_CELL_NBR] ,
					[US_INTL_ID]  ,
					[SITE_ADR]  ,
					[FLR_BLDG_NME] ,
					[CTY_NME] ,
					[STT_PRVN_NME]  ,
					[CTRY_RGN_NME]  ,
					[ZIP_CD]  ,
					[WIRED_DEV_TRPT_REQR_CD]  ,
					[WRLS_TRPT_REQR_CD]  ,
					[VRTL_CNCTN_CD]  ,
					[CREAT_BY_USER_ID] ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[EVENT_SCHED_NTE_TXT]  ,
					[SPRF_DEV_TRNSPRT_REQR_CD]  ,
					[SPAE_TRNSPRT_REQR_CD]  ,
					[SA_POC_HR_NME]  ,
					[DSL_TRNSPRT_REQR_CD]  ,
					[TME_ZONE_ID],  
					[MACH5_SRVC_REQR_CD]
				FROM	[COWS].[arch].[FSA_MDS_EVENT_NEW] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_MDS_EVENT_NEW] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. FSA_MDS_EVENT_NEW     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_MDS_EVENT_NEW] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[FSA_MDS_EVENT_NEW] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_MDS_EVENT_NEW] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_MDS_EVENT_NEW]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				/*
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_FSA_MDS_EVENT_ORDR	FSA_MDS_EVENT_ORDR	ORDR_ID			FSA_ORDR		ORDR_ID
				FK02_FSA_MDS_EVENT_ORDR	FSA_MDS_EVENT_ORDR	EVENT_ID		EVENT			EVENT_ID
				FK03_FSA_MDS_EVENT_ORDR	FSA_MDS_EVENT_ORDR	REQ_ID			ODIE_REQ		REQ_ID
				--------------------------------------------------ok--------EVENT-------------ORDER--------------------------
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK02_FSA_MDS_EVENT_ORDR	FSA_MDS_EVENT_ORDR	EVENT_ID	EVENT		EVENT_ID
				FK03_FSA_MDS_EVENT_ORDR	FSA_MDS_EVENT_ORDR	REQ_ID		ODIE_REQ	REQ_ID
				FK01_FSA_MDS_EVENT_ORDR	FSA_MDS_EVENT_ORDR	ORDR_ID		SA_ORDR		ORDR_ID

				*/
				---------------------------------------------------------------------------------------------------------------------------
				-----------------------------------------REMOVED [FSA_MDS_EVENT_ORDR] for Renstate No MDS order relation-------------------
				
				---------------------------------------------------------------------------------------------------------------------------
				
				/*
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_MDS_EVENT_ORDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[FSA_MDS_EVENT_ORDR](
					[ORDR_ID]  ,
					[EVENT_ID]  ,
					[TAB_SEQ_NBR]  ,
					[CMPLTD_CD]  ,
					[CREAT_DT]  ,
					[REQ_ID] )
				SELECT [ORDR_ID]  ,
					[EVENT_ID]  ,
					[TAB_SEQ_NBR]  ,
					[CMPLTD_CD]  ,
					[CREAT_DT]  ,
					[REQ_ID]	
				FROM	[COWS].[arch].[FSA_MDS_EVENT_ORDR] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				UNION
				SELECT [ORDR_ID]  ,
					[EVENT_ID]  ,
					[TAB_SEQ_NBR]  ,
					[CMPLTD_CD]  ,
					[CREAT_DT]  ,
					[REQ_ID]
				FROM	[COWS].[arch].[FSA_MDS_EVENT_ORDR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)	
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_MDS_EVENT_ORDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_MDS_EVENT_ORDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_MDS_EVENT_ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[FSA_MDS_EVENT_ORDR] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_MDS_EVENT_ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_MDS_EVENT_ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[FSA_MDS_EVENT_ORDR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_MDS_EVENT_ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_MDS_EVENT_ORDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				*/
				
				END
				
				IF( @Mood =0)  /* ORDERS ONLY */
				BEGIN
					/*
					FOREIGN_KEY			TABLE_NAME		COLUMN_NAME					REFERANCE_TABLE			REFERANCE_COLUMN_NAME
					FK01_FSA_ORDR_VAS	FSA_ORDR_VAS	ORDR_ID						FSA_ORDR				ORDR_ID
					FK02_FSA_ORDR_VAS	FSA_ORDR_VAS	FSA_ORDR_BILL_LINE_ITEM_ID	FSA_ORDR_BILL_LINE_ITEM	FSA_ORDR_BILL_LINE_ITEM_ID
					----------------------------------------------ok------------------ORDER---------------------------
					
					*/
					SET IDENTITY_INSERT  [COWS].[dbo]. FSA_ORDR_VAS     ON
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_ORDR_VAS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[FSA_ORDR_VAS](
						[FSA_ORDR_VAS_ID]   ,
						[ORDR_ID]  ,
						[VAS_TYPE_CD]  ,
						[SRVC_LVL_ID]  ,
						[NW_ADR]  ,
						[VAS_QTY]  ,
						[FSA_ORDR_BILL_LINE_ITEM_ID] ,
						[CREAT_DT])  
					SELECT [FSA_ORDR_VAS_ID]   ,
						[ORDR_ID]  ,
						[VAS_TYPE_CD]  ,
						[SRVC_LVL_ID]  ,
						[NW_ADR]  ,
						[VAS_QTY]  ,
						[FSA_ORDR_BILL_LINE_ITEM_ID] ,
						[CREAT_DT]
					FROM	[COWS].[arch].[FSA_ORDR_VAS] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_ORDR_VAS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					SET IDENTITY_INSERT  [COWS].[dbo]. FSA_ORDR_VAS     OFF
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_ORDR_VAS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[COWS].[arch].[FSA_ORDR_VAS] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_ORDR_VAS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_ORDR_VAS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 


					/*
					FOREIGN_KEY						TABLE_NAME				COLUMN_NAME				REFERANCE_TABLE			REFERANCE_COLUMN_NAME
					FK01_FSA_ORDR_BILL_LINE_ITEM	FSA_ORDR_BILL_LINE_ITEM	ORDR_ID					FSA_ORDR				ORDR_ID
					FK02_FSA_ORDR_BILL_LINE_ITEM	FSA_ORDR_BILL_LINE_ITEM	BILL_ITEM_TYPE_ID		LK_BILL_ITEM_TYPE		BILL_ITEM_TYPE_ID
					FK03_FSA_ORDR_BILL_LINE_ITEM	FSA_ORDR_BILL_LINE_ITEM	FSA_CPE_LINE_ITEM_ID	FSA_ORDR_CPE_LINE_ITEM	FSA_CPE_LINE_ITEM_ID
					-----------------------------------------------ok----------------------ORDER-->>ORDERID----------------------
					*/
					SET IDENTITY_INSERT  [COWS].[dbo]. FSA_ORDR_BILL_LINE_ITEM     ON
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_ORDR_BILL_LINE_ITEM] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[FSA_ORDR_BILL_LINE_ITEM](
						[FSA_ORDR_BILL_LINE_ITEM_ID]   ,
						[ORDR_ID]  ,
						[BILL_ITEM_TYPE_ID]  ,
						[BIC_CD]  ,
						[LINE_ITEM_DES]  ,
						[LINE_ITEM_QTY]  ,
						[ITEM_CD]  ,
						[ITEM_DES] ,
						[MRC_CHG_AMT] ,
						[NRC_CHG_AMT] ,
						[CREAT_DT]  ,
						[ACTN_CD]  ,
						[CHG_CD]  ,
						[FSA_CPE_LINE_ITEM_ID] )
					SELECT  [FSA_ORDR_BILL_LINE_ITEM_ID]   ,
						[ORDR_ID]  ,
						[BILL_ITEM_TYPE_ID]  ,
						[BIC_CD]  ,
						[LINE_ITEM_DES]  ,
						[LINE_ITEM_QTY]  ,
						[ITEM_CD]  ,
						[ITEM_DES] ,
						[MRC_CHG_AMT] ,
						[NRC_CHG_AMT] ,
						[CREAT_DT]  ,
						[ACTN_CD]  ,
						[CHG_CD]  ,
						[FSA_CPE_LINE_ITEM_ID]
					FROM	[COWS].[arch].[FSA_ORDR_BILL_LINE_ITEM] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_ORDR_BILL_LINE_ITEM] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					SET IDENTITY_INSERT  [COWS].[dbo]. FSA_ORDR_BILL_LINE_ITEM     OFF
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_ORDR_BILL_LINE_ITEM] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[COWS].[arch].[FSA_ORDR_BILL_LINE_ITEM] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_ORDR_BILL_LINE_ITEM] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_ORDR_BILL_LINE_ITEM]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
						

					/*
					FOREIGN_KEY					TABLE_NAME				COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
					FK01_FSA_ORDR_CPE_LINE_ITEM	FSA_ORDR_CPE_LINE_ITEM	ORDR_ID				FSA_ORDR			ORDR_ID
					FK02_FSA_ORDR_CPE_LINE_ITEM	FSA_ORDR_CPE_LINE_ITEM	FSA_ORDR_VAS_ID		FSA_ORDR_VAS		FSA_ORDR_VAS_ID
					FK03_FSA_ORDR_CPE_LINE_ITEM	FSA_ORDR_CPE_LINE_ITEM	LINE_ITEM_CD		LK_FSA_LINE_ITEM	LINE_ITEM_CD
					--------------------------------------------ok------------------ORDER-->>ORDERID------------------------------------
					*/
					SET IDENTITY_INSERT  [COWS].[dbo]. FSA_ORDR_CPE_LINE_ITEM     ON
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_ORDR_CPE_LINE_ITEM] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[FSA_ORDR_CPE_LINE_ITEM](
						[FSA_CPE_LINE_ITEM_ID]
					,[ORDR_ID]
					,[SRVC_LINE_ITEM_CD]
					,[EQPT_TYPE_ID]
					,[EQPT_ID]
					,[MFR_NME]
					,[CNTRC_TYPE_ID]
					,[CNTRC_TERM_ID]
					,[INSTLN_CD]
					,[MNTC_CD]
					,[CREAT_DT]
					,[DISC_PL_NBR]
					,[DISC_FMS_CKT_NBR]
					,[DISC_NW_ADR]
					,[MDS_DES]
					,[CMPNT_ID]
					,[LINE_ITEM_CD]
					,[FSA_ORGNL_INSTL_ORDR_ID]
					,[CXR_SRVC_ID]
					,[CXR_CKT_ID]
					,[TTRPT_ACCS_TYPE_CD]
					,[TTRPT_SPD_OF_SRVC_BDWD_DES]
					,[TTRPT_ACCS_TYPE_DES]
					,[FSA_ORDR_VAS_ID]
					,[PLSFT_RQSTN_NBR]
					,[RQSTN_DT]
					,[PRCH_ORDR_NBR]
					,[EQPT_ITM_RCVD_DT]
					,[MATL_CD]
					,[UNIT_MSR]
					,[MANF_PART_CD]
					,[VNDR_CD]
					,[ORDR_QTY]
					,[UNIT_PRICE]
					,[MANF_DISCNT_CD]
					,[FMS_CKT_NBR]
					,[EQPT_RCVD_BY_ADID]
					,[CMPL_DT]
					,[PO_LN_NBR]
					,[RCVD_QTY]
					,[PS_RCVD_STUS]
					,[PID]
					,[DROP_SHP]
					,[DEVICE_ID]
					,[SUPPLIER]
					,[ORDR_CMPNT_ID]
					,[ITM_STUS]
					,[CMPNT_FMLY]
					,[RLTD_CMPNT_ID]
					,[STUS_CD]
					,[MFR_PS_ID]
					,[CPE_REUSE_CD] 
					,[TTRPT_ACCS_ARNGT_CD] 
					,[TTRPT_ENCAP_CD] 
					,[TTRPT_FBR_HAND_OFF_CD] 
					,[CXR_ACCS_CD] 
					,[CKT_ID] 
					,[TTRPT_QOT_XPIRN_DT] 
					,[TTRPT_ACCS_TERM_DES] 
					,[TTRPT_ACCS_BDWD_TYPE] 
					,[PL_NBR] 
					,[PORT_RT_TYPE_CD] 
					,[TPORT_IP_VER_TYPE_CD] 
					,[TPORT_IPV4_ADR_PRVDR_CD] 
					,[TPORT_IPV4_ADR_QTY] 
					,[TPORT_IPV6_ADR_PRVDR_CD] 
					,[TPORT_IPV6_ADR_QTY] 
					,[TPORT_VLAN_QTY] 
					,[TPORT_ETHRNT_NRFC_INDCR] 
					,[TPORT_CNCTR_TYPE_ID] 
					,[TPORT_CUST_ROUTR_TAG_TXT] 
					,[TPORT_CUST_ROUTR_AUTO_NEGOT_CD] 
					,[SPA_ACCS_INDCR_DES] 
					,[TPORT_DIA_NOC_TO_NOC_TXT] 
					,[TPORT_PROJ_DES] 
					,[TPORT_VNDR_QUOTE_ID] 
					,[TPORT_ETH_ACCS_TYPE_CD]					  
					  )
					SELECT  
					[FSA_CPE_LINE_ITEM_ID]
					,[ORDR_ID]
					,[SRVC_LINE_ITEM_CD]
					,[EQPT_TYPE_ID]
					,[EQPT_ID]
					,[MFR_NME]
					,[CNTRC_TYPE_ID]
					,[CNTRC_TERM_ID]
					,[INSTLN_CD]
					,[MNTC_CD]
					,[CREAT_DT]
					,[DISC_PL_NBR]
					,[DISC_FMS_CKT_NBR]
					,[DISC_NW_ADR]
					,[MDS_DES]
					,[CMPNT_ID]
					,[LINE_ITEM_CD]
					,[FSA_ORGNL_INSTL_ORDR_ID]
					,[CXR_SRVC_ID]
					,[CXR_CKT_ID]
					,[TTRPT_ACCS_TYPE_CD]
					,[TTRPT_SPD_OF_SRVC_BDWD_DES]
					,[TTRPT_ACCS_TYPE_DES]
					,[FSA_ORDR_VAS_ID]
					,[PLSFT_RQSTN_NBR]
					,[RQSTN_DT]
					,[PRCH_ORDR_NBR]
					,[EQPT_ITM_RCVD_DT]
					,[MATL_CD]
					,[UNIT_MSR]
					,[MANF_PART_CD]
					,[VNDR_CD]
					,[ORDR_QTY]
					,[UNIT_PRICE]
					,[MANF_DISCNT_CD]
					,[FMS_CKT_NBR]
					,[EQPT_RCVD_BY_ADID]
					,[CMPL_DT]
					,[PO_LN_NBR]
					,[RCVD_QTY]
					,[PS_RCVD_STUS]
					,[PID]
					,[DROP_SHP]
					,[DEVICE_ID]
					,[SUPPLIER]
					,[ORDR_CMPNT_ID]
					,[ITM_STUS]
					,[CMPNT_FMLY]
					,[RLTD_CMPNT_ID]
					,[STUS_CD]
					,[MFR_PS_ID]
					,[CPE_REUSE_CD]
					,[TTRPT_ACCS_ARNGT_CD] 
					,[TTRPT_ENCAP_CD] 
					,[TTRPT_FBR_HAND_OFF_CD] 
					,[CXR_ACCS_CD] 
					,[CKT_ID] 
					,[TTRPT_QOT_XPIRN_DT] 
					,[TTRPT_ACCS_TERM_DES] 
					,[TTRPT_ACCS_BDWD_TYPE] 
					,[PL_NBR] 
					,[PORT_RT_TYPE_CD] 
					,[TPORT_IP_VER_TYPE_CD] 
					,[TPORT_IPV4_ADR_PRVDR_CD] 
					,[TPORT_IPV4_ADR_QTY] 
					,[TPORT_IPV6_ADR_PRVDR_CD] 
					,[TPORT_IPV6_ADR_QTY] 
					,[TPORT_VLAN_QTY] 
					,[TPORT_ETHRNT_NRFC_INDCR] 
					,[TPORT_CNCTR_TYPE_ID] 
					,[TPORT_CUST_ROUTR_TAG_TXT] 
					,[TPORT_CUST_ROUTR_AUTO_NEGOT_CD] 
					,[SPA_ACCS_INDCR_DES] 
					,[TPORT_DIA_NOC_TO_NOC_TXT] 
					,[TPORT_PROJ_DES] 
					,[TPORT_VNDR_QUOTE_ID] 
					,[TPORT_ETH_ACCS_TYPE_CD]
					FROM  [COWS].[arch].[FSA_ORDR_CPE_LINE_ITEM] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_ORDR_CPE_LINE_ITEM] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					SET IDENTITY_INSERT  [COWS].[dbo]. FSA_ORDR_CPE_LINE_ITEM     OFF
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_ORDR_CPE_LINE_ITEM] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[COWS].[arch].[FSA_ORDR_CPE_LINE_ITEM] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_ORDR_CPE_LINE_ITEM] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_ORDR_CPE_LINE_ITEM]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					
						
					
					/*
					FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
					FK01_FSA_ORDER_CSC	FSA_ORDR_CSC	ORDR_ID				FSA_ORDR		ORDR_ID
					FK02_FSA_ORDR_CSC	FSA_ORDR_CSC	CREAT_BY_USER_ID	LK_USER			USER_ID
					FK03_FSA_ORDR_CSC	FSA_ORDR_CSC	MODFD_BY_USER_ID	LK_USER			USER_ID
					--------------------------------------------------ok-------------ORDER----------------------------------
					*/
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_ORDR_CSC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[FSA_ORDR_CSC](
						[ORDR_ID]  ,
						[SERIAL_NBR]  ,
						[MTRL_RQSTN_TXT]  ,
						[PRCH_ORDR_TXT]  ,
						[INTL_MNTC_SCHRG_TXT]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_BY_USER_ID] ,
						[MODFD_DT] ,
						[CREAT_DT]  )
					SELECT  [ORDR_ID]  ,
						[SERIAL_NBR]  ,
						[MTRL_RQSTN_TXT]  ,
						[PRCH_ORDR_TXT]  ,
						[INTL_MNTC_SCHRG_TXT]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_BY_USER_ID] ,
						[MODFD_DT] ,
						[CREAT_DT] 
					FROM	[COWS].[arch].[FSA_ORDR_CSC] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_ORDR_CSC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_ORDR_CSC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[COWS].[arch].[FSA_ORDR_CSC] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_ORDR_CSC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_ORDR_CSC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					/*
					FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
					FK01_FSA_ORDR_CUST	FSA_ORDR_CUST	ORDR_ID	FSA_ORDR	ORDR_ID
					----------------------------------------ok-------------------ORDER-------------------
					*/
					SET IDENTITY_INSERT  [COWS].[dbo]. [FSA_ORDR_CUST]     ON
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_ORDR_CUST] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[FSA_ORDR_CUST](
						[ORDR_ID]  ,
						[CIS_LVL_TYPE]  ,
						[CUST_ID]  ,
						[BR_CD] ,
						[CURR_BILL_CYC_CD]  ,
						[FUT_BILL_CYC_CD] ,
						[SRVC_SUB_TYPE_ID]  ,
						[SOI_CD]  ,
						[SALS_PERSN_PRIM_CID]  ,
						[SALS_PERSN_SCNDY_CID]  ,
						[CLLI_CD]  ,
						[CREAT_DT]  ,
						[CUST_NME],
						[TAX_XMPT_CD] ,
						[CHARS_CUST_ID],
						[SITE_ID],
						[FSA_ORDR_CUST_ID]  )
					SELECT  [ORDR_ID]  ,
						[CIS_LVL_TYPE]  ,
						[CUST_ID]  ,
						[BR_CD] ,
						[CURR_BILL_CYC_CD]  ,
						[FUT_BILL_CYC_CD] ,
						[SRVC_SUB_TYPE_ID]  ,
						[SOI_CD]  ,
						[SALS_PERSN_PRIM_CID]  ,
						[SALS_PERSN_SCNDY_CID]  ,
						[CLLI_CD]  ,
						[CREAT_DT]  ,
						[CUST_NME],
						[TAX_XMPT_CD] ,
						[CHARS_CUST_ID],
						[SITE_ID],
						[FSA_ORDR_CUST_ID]
					FROM	[COWS].[arch].[FSA_ORDR_CUST] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_ORDR_CUST] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_ORDR_CUST] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					SET IDENTITY_INSERT  [COWS].[dbo]. [FSA_ORDR_CUST]     OFF
					DELETE  FROM	[COWS].[arch].[FSA_ORDR_CUST] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_ORDR_CUST] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_ORDR_CUST]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					/*
					FOREIGN_KEY				TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
					FK01_FSA_ORDR_GOM_XNCI	FSA_ORDR_GOM_XNCI	CREAT_BY_USER_ID	LK_USER			USER_ID
					FK02_FSA_ORDR_GOM_XNCI	FSA_ORDR_GOM_XNCI	MODFD_BY_USER_ID	LK_USER			USER_ID
					FK03_FSA_ORDR_GOM_XNCI	FSA_ORDR_GOM_XNCI	GRP_ID				LK_GRP			GRP_ID
					FK04_FSA_ORDR_GOM_XNCI	FSA_ORDR_GOM_XNCI	ORDR_ID				FSA_ORDR		ORDR_ID
					FK05_FSA_ORDR_GOM_XNCI	FSA_ORDR_GOM_XNCI	CPE_STUS_ID			LK_STUS		STUS_ID
					------------------------------------------ok--------------ORDER--------------------------------
					*/
					SET IDENTITY_INSERT  [COWS].[dbo]. [FSA_ORDR_GOM_XNCI]     ON
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_ORDR_GOM_XNCI] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[FSA_ORDR_GOM_XNCI](
						[ORDR_ID]  ,
						[GRP_ID]  ,
						[VNDR_SHIP_DT] ,
						[DRTBN_SHIP_DT] ,
						[CUST_DLVRY_DT] ,
						[CSTM_DT] ,
						[SHPMT_TRK_NBR]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_BY_USER_ID] ,
						[MODFD_DT] ,
						[CREAT_DT]  ,
						[HDWR_DLVRD_DT] ,
						[EQPT_INSTL_DT] ,
						[CPE_STUS_ID]  ,
						[CPE_CMPLT_DT] ,
						[ATLAS_WRK_ORDR_NBR]  ,
						[PLSFT_RQSTN_NBR]  ,
						[RQSTN_AMT]  ,
						[RQSTN_DT] ,
						[PRCH_ORDR_NBR]  ,
						[PRCH_ORDR_BACK_ORDR_SHIP_DT] ,
						[VNDR_RECV_FROM_DT] ,
						[VNDR_COURIER_TRK_NBR]  ,
						[PLSFT_RELS_DT] ,
						[PLSFT_RELS_ID]  ,
						[PLSFT_RCPT_ID]  ,
						[COURIER_TRK_NBR]  ,
						[CPE_EQPT_TYPE_TXT]  ,
						[CPE_VNDR_NME]  ,
						[SHPMT_SIGN_BY_NME]  ,
						[PLN_NME]  ,
						[BILL_FTN]  ,
						[CUST_PRCH_CD]  ,
						[CUST_RNTL_CD]  ,
						[CUST_RNTL_TERM_NBR] ,
						[LOGICALIS_MNTH_LEASE_RT_AMT]  ,
						[TERM_STRT_DT] ,
						[TERM_END_DT] ,
						[TERM_60_DAY_NTFCTN_DT] ,
						[CUST_RNL_CD]  ,
						[CUST_RNL_TERM_NBR] ,
						[LOGICALIS_RNL_MNTH_LEASE_RT_AMT]  ,
						[CUST_DSCNCT_CD]  ,
						[CUST_DSCNCT_DT],
						[FSA_CPE_LINE_ITEM_ID] ,
						[ID] 
						)
					SELECT  
						[ORDR_ID]  ,
						[GRP_ID]  ,
						[VNDR_SHIP_DT] ,
						[DRTBN_SHIP_DT] ,
						[CUST_DLVRY_DT] ,
						[CSTM_DT] ,
						[SHPMT_TRK_NBR]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_BY_USER_ID] ,
						[MODFD_DT] ,
						[CREAT_DT]  ,
						[HDWR_DLVRD_DT] ,
						[EQPT_INSTL_DT] ,
						[CPE_STUS_ID]  ,
						[CPE_CMPLT_DT] ,
						[ATLAS_WRK_ORDR_NBR]  ,
						[PLSFT_RQSTN_NBR]  ,
						[RQSTN_AMT]  ,
						[RQSTN_DT] ,
						[PRCH_ORDR_NBR]  ,
						[PRCH_ORDR_BACK_ORDR_SHIP_DT] ,
						[VNDR_RECV_FROM_DT] ,
						[VNDR_COURIER_TRK_NBR]  ,
						[PLSFT_RELS_DT] ,
						[PLSFT_RELS_ID]  ,
						[PLSFT_RCPT_ID]  ,
						[COURIER_TRK_NBR]  ,
						[CPE_EQPT_TYPE_TXT]  ,
						[CPE_VNDR_NME]  ,
						[SHPMT_SIGN_BY_NME]  ,
						[PLN_NME]  ,
						[BILL_FTN]  ,
						[CUST_PRCH_CD]  ,
						[CUST_RNTL_CD]  ,
						[CUST_RNTL_TERM_NBR] ,
						[LOGICALIS_MNTH_LEASE_RT_AMT]  ,
						[TERM_STRT_DT] ,
						[TERM_END_DT] ,
						[TERM_60_DAY_NTFCTN_DT] ,
						[CUST_RNL_CD]  ,
						[CUST_RNL_TERM_NBR] ,
						[LOGICALIS_RNL_MNTH_LEASE_RT_AMT]  ,
						[CUST_DSCNCT_CD]  ,
						[CUST_DSCNCT_DT],
						[FSA_CPE_LINE_ITEM_ID] ,
						[ID] 
					FROM	[COWS].[arch].[FSA_ORDR_GOM_XNCI] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_ORDR_GOM_XNCI] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_ORDR_GOM_XNCI] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					SET IDENTITY_INSERT  [COWS].[dbo]. [FSA_ORDR_GOM_XNCI]     OFF
					DELETE  FROM	[COWS].[arch].[FSA_ORDR_GOM_XNCI] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_ORDR_GOM_XNCI] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_ORDR_GOM_XNCI]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					/*
					FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
					FK01_FSA_ORDR_MSG	FSA_ORDR_MSG	ORDR_ID		FSA_ORDR	ORDR_ID
					FK02_FSA_ORDR_MSG	FSA_ORDR_MSG	FSA_MSG_ID	LK_FSA_MSG	FSA_MSG_ID
					FK03_FSA_ORDR_MSG	FSA_ORDR_MSG	STUS_ID		LK_STUS	STUS_ID
					-------------------------------------------ok--------------ORDER------------------
					*/
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_ORDR_MSG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[FSA_ORDR_MSG](
						[ORDR_ID]  ,
						[FSA_MSG_ID]  ,
						[STUS_ID]  ,
						[CREAT_DT] )
					SELECT [ORDR_ID]  ,
						[FSA_MSG_ID]  ,
						[STUS_ID]  ,
						[CREAT_DT]
					FROM	[COWS].[arch].[FSA_ORDR_MSG] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_ORDR_MSG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_ORDR_MSG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[COWS].[arch].[FSA_ORDR_MSG] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_ORDR_MSG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_ORDR_MSG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					/*
					FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
					FK01_FSA_ORDR_RELTD_ORDR	FSA_ORDR_RELTD_ORDR	RELTD_ORDR_ID		FSA_ORDR	ORDR_ID
					FK02_FSA_ORDR_RELTD_ORDR	FSA_ORDR_RELTD_ORDR	ORDR_ID				FSA_ORDR	ORDR_ID
					---------------------------------------------------ok---------------ORDER---------------------
					NOW
					*/
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_ORDR_RELTD_ORDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[FSA_ORDR_RELTD_ORDR](
						[ORDR_ID]  ,
						[RELTD_ORDR_ID]  ,
						[CREAT_DT]  )
					SELECT [ORDR_ID]  ,
						[RELTD_ORDR_ID]  ,
						[CREAT_DT] 
					FROM	[COWS].[arch].[FSA_ORDR_RELTD_ORDR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_ORDR_RELTD_ORDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_ORDR_RELTD_ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[COWS].[arch].[FSA_ORDR_RELTD_ORDR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_ORDR_RELTD_ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_ORDR_RELTD_ORDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					
					
					
					/*
					FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
					FK01_GRP_SITE_CNTNT	GRP_SITE_CNTNT	CREAT_BY_USER_ID	LK_USER	USER_ID
					FK02_GRP_SITE_CNTNT	GRP_SITE_CNTNT	MODFD_BY_USER_ID	LK_USER	USER_ID
					FK03_GRP_SITE_CNTNT	GRP_SITE_CNTNT	SITE_CNTNT_ID		LK_SITE_CNTNT	SITE_CNTNT_ID
					FK04_GRP_SITE_CNTNT	GRP_SITE_CNTNT	GRP_ID				LK_GRP	GRP_ID

					*/
					--PRINT 'Starting to execute [arch].[GRP_SITE_CNTNT] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
					--INSERT INTO [arch].[GRP_SITE_CNTNT](
					--	[GRP_ID]  ,
					--	[SITE_CNTNT_ID]  ,
					--	[CREAT_BY_USER_ID]  ,
					--	[MODFD_BY_USER_ID] ,
					--	[MODFD_DT] ,
					--	[CREAT_DT]  )
					--SELECT [GRP_ID]  ,
					--	[SITE_CNTNT_ID]  ,
					--	[CREAT_BY_USER_ID]  ,
					--	[MODFD_BY_USER_ID] ,
					--	[MODFD_DT] ,
					--	[CREAT_DT]
					--FROM  [dbo].[GRP_SITE_CNTNT] Where [CREAT_DT]<=CONVERT(datetime,@Date)
					--PRINT 'Finished executing [arch].[GRP_SITE_CNTNT] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
					--PRINT 'Starting to execute [dbo].[GRP_SITE_CNTNT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
					--DELETE  FROM  [dbo].[GRP_SITE_CNTNT] Where [CREAT_DT]<=CONVERT(datetime,@Date)
					--PRINT 'Finished executing [dbo].[GRP_SITE_CNTNT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
					--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[GRP_SITE_CNTNT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					
					/*
					FOREIGN_KEY			TABLE_NAME	COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
					FK01_ORDR_H5_DOC	ORDR_H5_DOC	ORDR_ID			ORDR			ORDR_ID
					FK02_ORDR_H5_DOC	ORDR_H5_DOC	H5_DOC_ID		H5_DOC			H5_DOC_ID
					------------------------------------------ok---------------------ORDER------------------
					*/
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_H5_DOC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[ORDR_H5_DOC](
						[ORDR_ID]  ,
						[H5_DOC_ID]  ,
						[CREAT_DT] )
					SELECT  [ORDR_ID]  ,
						[H5_DOC_ID]  ,
						[CREAT_DT]	
					FROM	[COWS].[arch].[ORDR_H5_DOC] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					UNION 
					SELECT  [ORDR_ID]  ,
						[H5_DOC_ID]  ,
						[CREAT_DT]	
					FROM	[COWS].[arch].[ORDR_H5_DOC] WITH(NOLOCK) WHERE [H5_DOC_ID] IN ( SELECT DISTINCT [H5_DOC_ID]
					FROM	[COWS].[arch].[H5_DOC] WITH(NOLOCK)	WHERE [H5_FOLDR_ID]	IN	
					(SELECT DISTINCT [H5_FOLDR_ID] FROM  [COWS].[arch].ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)	) )
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_H5_DOC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_H5_DOC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_H5_DOC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[COWS].[arch].[ORDR_H5_DOC] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_H5_DOC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					DELETE  FROM	[COWS].[arch].[ORDR_H5_DOC] WITH(TABLOCKX) WHERE [H5_DOC_ID] IN ( SELECT DISTINCT [H5_DOC_ID]
					FROM	[COWS].[arch].[H5_DOC] WITH(NOLOCK)	WHERE [H5_FOLDR_ID]	IN	
					(SELECT DISTINCT [H5_FOLDR_ID] FROM  [COWS].[arch].ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)	) )
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_H5_DOC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_H5_DOC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					
					
					/*
					FOREIGN_KEY	TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
					FK01_H5_DOC	H5_DOC		H5_FOLDR_ID			H5_FOLDR		H5_FOLDR_ID
					FK03_H5_DOC	H5_DOC		CREAT_BY_USER_ID	LK_USER			USER_ID
					FK04_H5_DOC	H5_DOC		MODFD_BY_USER_ID	LK_USER			USER_ID
					FK05_H5_DOC	H5_DOC		REC_STUS_ID			LK_REC_STUS		REC_STUS_ID

					*/
					SET IDENTITY_INSERT  [COWS].[dbo]. H5_DOC     ON
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[H5_DOC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[H5_DOC](
						[H5_DOC_ID]   ,
						[H5_FOLDR_ID]  ,
						[FILE_NME]  ,
						[FILE_CNTNT]  ,
						[FILE_SIZE_QTY]  ,
						[CREAT_DT]  ,
						[MODFD_BY_USER_ID] ,
						[CREAT_BY_USER_ID]  ,
						[REC_STUS_ID]  ,
						[MODFD_DT] ,
						[CMNT_TXT]  )
					SELECT  [H5_DOC_ID]   ,
						[H5_FOLDR_ID]  ,
						[FILE_NME]  ,
						[FILE_CNTNT]  ,
						[FILE_SIZE_QTY]  ,
						[CREAT_DT]  ,
						[MODFD_BY_USER_ID] ,
						[CREAT_BY_USER_ID]  ,
						[REC_STUS_ID]  ,
						[MODFD_DT] ,
						[CMNT_TXT]
					FROM	[COWS].[arch].[H5_DOC] WITH(NOLOCK)	WHERE [H5_FOLDR_ID]	IN	
					(SELECT DISTINCT [H5_FOLDR_ID] FROM  [COWS].[arch].ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)	)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[H5_DOC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					SET IDENTITY_INSERT  [COWS].[dbo]. H5_DOC     OFF
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[H5_DOC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE FROM	[COWS].[arch].[H5_DOC] WITH(TABLOCKX)	WHERE [H5_FOLDR_ID]	IN	
					(SELECT DISTINCT [H5_FOLDR_ID] FROM  [COWS].[arch].ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[H5_DOC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT 
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[H5_DOC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					/*
					FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
					FK01_IPL_PROD_SRVC_CKT	IPL_PROD_SRVC_CKT	PROD_TYPE_ID		LK_PROD_TYPE	PROD_TYPE_ID
					FK02_IPL_PROD_SRVC_CKT	IPL_PROD_SRVC_CKT	CREAT_BY_USER_ID	LK_USER	USER_ID
					FK03_IPL_PROD_SRVC_CKT	IPL_PROD_SRVC_CKT	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID

					*/
					/*
					PRINT 'Starting to execute [arch].[IPL_PROD_SRVC_CKT] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
					INSERT INTO [arch].[IPL_PROD_SRVC_CKT](
							[PROD_TYPE_ID]  ,
							[SRVC_TYPE_CD]  ,
							[CKT_TYPE_CD]  ,
							[SRVC_CKT_DES]  ,
							[CREAT_DT]  ,
							[CREAT_BY_USER_ID]  ,
							[REC_STUS_ID]  )
					SELECT	[PROD_TYPE_ID]  ,
							[SRVC_TYPE_CD]  ,
							[CKT_TYPE_CD]  ,
							[SRVC_CKT_DES]  ,
							[CREAT_DT]  ,
							[CREAT_BY_USER_ID]  ,
							[REC_STUS_ID] 
					FROM	[dbo].[IPL_PROD_SRVC_CKT] WITH(NOLOCK) 
					WHERE   LTRIM(RTRIM([PROD_TYPE_ID]))+LTRIM(RTRIM([SRVC_TYPE_CD]))+LTRIM(RTRIM([CKT_TYPE_CD]) NOT IN(
							SELECT  LTRIM(RTRIM([PROD_TYPE_ID]))+LTRIM(RTRIM([SRVC_TYPE_CD]))+LTRIM(RTRIM([CKT_TYPE_CD]) 
							FROM [arch].[IPL_PROD_SRVC_CKT] WITH(NOLOCK))
					PRINT 'Finished executing [arch].[IPL_PROD_SRVC_CKT] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'		
					
					PRINT 'Starting to execute [dbo].[IPL_PROD_SRVC_CKT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
					DELETE  FROM	[dbo].[IPL_PROD_SRVC_CKT] WITH(NOLOCK) 
					WHERE   LTRIM(RTRIM([PROD_TYPE_ID]))+LTRIM(RTRIM([SRVC_TYPE_CD]))+LTRIM(RTRIM([CKT_TYPE_CD]) NOT IN(
							SELECT  LTRIM(RTRIM([PROD_TYPE_ID]))+LTRIM(RTRIM([SRVC_TYPE_CD]))+LTRIM(RTRIM([CKT_TYPE_CD]) 
							FROM [arch].[IPL_PROD_SRVC_CKT] WITH(NOLOCK))
					PRINT 'Finished executing [dbo].[IPL_PROD_SRVC_CKT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[IPL_PROD_SRVC_CKT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					*/
					/*
					FOREIGN_KEY				TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE			REFERANCE_COLUMN_NAME
					FK01_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	LINE_CODNG_TYPE_ID	LK_LINE_CODNG_TYPE	LINE_CODNG_TYPE_ID
					FK02_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	CXR_CD				LK_FRGN_CXR	CXR_CD
					FK03_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	ORDR_ID				IPL_ORDR	ORDR_ID
					FK04_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	OSS_OPT_ID			LK_OSS_OPT	OSS_OPT_ID
					FK05_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	FRMNG_ID			LK_FRMNG	FRMNG_ID
					FK06_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	ACCS_ARNGT_ID		LK_ACCS_ARNGT	ACCS_ARNGT_ID
					FK07_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	NRFC_TYPE_ID		LK_NRFC_TYPE	NRFC_TYPE_ID
					FK08_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	ACCS_TYPE_ID		LK_ACCS_TYPE	ACCS_TYPE_ID
					FK09_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	OSS_CXR_INDCR		LK_OSS_CXR_INDCR	OSS_CXR_INDCR
					FK10_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID
					FK13_IPL_ORDR_ACCS_INFO	IPL_ORDR_ACCS_INFO	LOCAL_ACCS_TYPE_ID	LK_LOCAL_ACCS_TYPE	LOCAL_ACCS_TYPE_ID
					-------------------------------------------------ok-------------------ORDER--------------------
					*/
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[IPL_ORDR_ACCS_INFO] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[IPL_ORDR_ACCS_INFO](
						[ORDR_ID]  ,
						[ACCS_TYPE_ID]  ,
						[FRMNG_ID] ,
						[NRFC_TYPE_ID] ,
						[ACCS_ARNGT_ID] ,
						[LINE_CODNG_TYPE_ID]  ,
						[LOCAL_LOOP_XST_CD]  ,
						[T1_E1_ID_NBR]  ,
						[PRMS_ID]  ,
						[REQ_CHNL_ASMT_ID] ,
						[TIMING_SRC_SPRINT_CD]  ,
						[OSS_OPT_ID]  ,
						[OSS_CXR_INDCR] ,
						[CXR_CD] ,
						[CREAT_DT]  ,
						[REC_STUS_ID]  ,
						[LOCAL_ACCS_TYPE_ID]  ,
						[LOCAL_ACCS_QTY] ,
						[TME_SLOT_QTY] ,
						[CPA_VNDR_NME]  ,
						[CPA_PHN_NBR] ,
						[JSDC_NME] ,
						[LATA_NME] ,
						[DMSTC_CXR_CD]  ,
						[DMSTC_CXR_NME]  ,
						[MULTI_PNT_CD]  ,
						[SCHRG_XMPT_CD]  ,
						[MULTI_DROPBOX_CD]  )
					SELECT [ORDR_ID]  ,
						[ACCS_TYPE_ID]  ,
						[FRMNG_ID] ,
						[NRFC_TYPE_ID] ,
						[ACCS_ARNGT_ID] ,
						[LINE_CODNG_TYPE_ID]  ,
						[LOCAL_LOOP_XST_CD]  ,
						[T1_E1_ID_NBR]  ,
						[PRMS_ID]  ,
						[REQ_CHNL_ASMT_ID] ,
						[TIMING_SRC_SPRINT_CD]  ,
						[OSS_OPT_ID]  ,
						[OSS_CXR_INDCR] ,
						[CXR_CD] ,
						[CREAT_DT]  ,
						[REC_STUS_ID]  ,
						[LOCAL_ACCS_TYPE_ID]  ,
						[LOCAL_ACCS_QTY] ,
						[TME_SLOT_QTY] ,
						[CPA_VNDR_NME]  ,
						[CPA_PHN_NBR] ,
						[JSDC_NME] ,
						[LATA_NME] ,
						[DMSTC_CXR_CD]  ,
						[DMSTC_CXR_NME]  ,
						[MULTI_PNT_CD]  ,
						[SCHRG_XMPT_CD]  ,
						[MULTI_DROPBOX_CD]	
					FROM	[arch].[IPL_ORDR_ACCS_INFO] WITH(NOLOCK) WHERE ORDR_ID IN (SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[IPL_ORDR_ACCS_INFO] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[IPL_ORDR_ACCS_INFO] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[arch].[IPL_ORDR_ACCS_INFO] WITH(TABLOCKX) WHERE ORDR_ID IN (SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[IPL_ORDR_ACCS_INFO] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[IPL_ORDR_ACCS_INFO]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					/*
					FOREIGN_KEY			TABLE_NAME		COLUMN_NAME		REFERANCE_TABLE		REFERANCE_COLUMN_NAME
					FK01_IPL_ORDR_CKT	IPL_ORDR_CKT	ORDR_ID			IPL_ORDR			ORDR_ID
					FK02_IPL_ORDR_CKT	IPL_ORDR_CKT	ACCS_TYPE_ID	LK_ACCS_TYPE		ACCS_TYPE_ID
					-------------------------------------------ok----------------ORDER----------------------
					*/
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[IPL_ORDR_CKT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[IPL_ORDR_CKT](
						[ORDR_ID]  ,
						[ACCS_TYPE_ID]  ,
						[CKT_WIRING_TRMTN_NME]  ,
						[CKT_SUPERVISORY_SGNLG_NME] ,
						[CKT_CKT_ADR_SGNLG_NME] ,
						[CKT_CKT_STRT_DIAL_CD] )
					SELECT [ORDR_ID]  ,
						[ACCS_TYPE_ID]  ,
						[CKT_WIRING_TRMTN_NME]  ,
						[CKT_SUPERVISORY_SGNLG_NME] ,
						[CKT_CKT_ADR_SGNLG_NME] ,
						[CKT_CKT_STRT_DIAL_CD] 
					FROM	[arch].[IPL_ORDR_CKT] WITH(NOLOCK) WHERE ORDR_ID IN 	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[IPL_ORDR_CKT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[IPL_ORDR_CKT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[arch].[IPL_ORDR_CKT] WITH(TABLOCKX) WHERE ORDR_ID IN 	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[IPL_ORDR_CKT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[IPL_ORDR_CKT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					
					/*
					FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
					FK01_IPL_ORDR_EQPT	IPL_ORDR_EQPT	ORDR_ID				IPL_ORDR			ORDR_ID
					FK02_IPL_ORDR_EQPT	IPL_ORDR_EQPT	ACCS_TYPE_ID		LK_ACCS_TYPE		ACCS_TYPE_ID
					---------------------------------------------ok-----------------------ORDER-------------------
					*/
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[IPL_ORDR_EQPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[IPL_ORDR_EQPT](
						[ORDR_ID]  ,
						[ACCS_TYPE_ID]  ,
						[RAD_BOX_NEED_CD]  ,
						[CSU_DSU_CD]  ,
						[PRE_XST_CLLI_CD]  ,
						[CPE_DISC_PCT_QTY]  ,
						[CPE_CMNT_TXT]  ,
						[CPE_CMPNT_ID]  ,
						[CPE_PROD_NBR]  ,
						[EQPT_PRVD_TYPE_NME] ,
						[EQPT_TERM_LGTH_NME] )
					SELECT [ORDR_ID]  ,
						[ACCS_TYPE_ID]  ,
						[RAD_BOX_NEED_CD]  ,
						[CSU_DSU_CD]  ,
						[PRE_XST_CLLI_CD]  ,
						[CPE_DISC_PCT_QTY]  ,
						[CPE_CMNT_TXT]  ,
						[CPE_CMPNT_ID]  ,
						[CPE_PROD_NBR]  ,
						[EQPT_PRVD_TYPE_NME] ,
						[EQPT_TERM_LGTH_NME]	
					FROM	[arch].[IPL_ORDR_EQPT] WITH(NOLOCK) WHERE ORDR_ID IN (SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[IPL_ORDR_EQPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[IPL_ORDR_EQPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[arch].[IPL_ORDR_EQPT] WITH(TABLOCKX) WHERE ORDR_ID IN (SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[IPL_ORDR_EQPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[IPL_ORDR_EQPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
					
					/*
					FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
					FK01_IPL_ORDR	IPL_ORDR	ORDR_ID				ORDR				ORDR_ID
					FK02_IPL_ORDR	IPL_ORDR	DSCNCT_REAS_CD		LK_DSCNCT_REAS		DSCNCT_REAS_CD
					FK03_IPL_ORDR	IPL_ORDR	PROD_TYPE_ID		LK_PROD_TYPE		PROD_TYPE_ID
					FK04_IPL_ORDR	IPL_ORDR	ORDR_TYPE_ID		LK_ORDR_TYPE		ORDR_TYPE_ID
					FK05_IPL_ORDR	IPL_ORDR	ORDR_STUS_ID		LK_STUS				STUS_ID
					FK06_IPL_ORDR	IPL_ORDR	PROD_TYPE_ID		IPL_PROD_SRVC_CKT	PROD_TYPE_ID
					FK06_IPL_ORDR	IPL_ORDR	SRVC_TYPE_CD		IPL_PROD_SRVC_CKT	SRVC_TYPE_CD
					FK06_IPL_ORDR	IPL_ORDR	CKT_TYPE_CD			IPL_PROD_SRVC_CKT	CKT_TYPE_CD
					FK08_IPL_ORDR	IPL_ORDR	ORDR_ID				ORDR				ORDR_ID
					FK09_IPL_ORDR	IPL_ORDR	CUST_CNTRC_LGTH_ID	LK_CUST_CNTRC_LGTH	CUST_CNTRC_LGTH_ID
					FK10_IPL_ORDR	IPL_ORDR	CREAT_BY_USER_ID	LK_USER				USER_ID
					FK11_IPL_ORDR	IPL_ORDR	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID
					FK12_IPL_ORDR	IPL_ORDR	MODFD_BY_USER_ID	LK_USER				USER_ID
					FK13_IPL_ORDR	IPL_ORDR	PREV_ORDR_TYPE_ID	LK_ORDR_TYPE		ORDR_TYPE_ID
					----------------------------------------------ok--------------ORDER---------------------------
					*/
					Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[IPL_ORDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
					INSERT INTO [dbo].[IPL_ORDR](
						[ORDR_ID]  ,
						[RE_SBMSN_CD]  ,
						[CUST_CNTRC_SIGN_DT] ,
						[CUST_ORDR_SBMT_DT] ,
						[CUST_WANT_DT] ,
						[CUST_CMMT_DT] ,
						[CUST_ACPT_ERLY_SRVC_CD]  ,
						[NEW_CNSTRCTN_CD]  ,
						[UNMAN_SITE_CD]  ,
						[DSCNCT_REAS_CD] ,
						[TSP_CD]  ,
						[SCA_NBR]  ,
						[GVRMNT_CUST_CD]  ,
						[PRE_XST_PL_NBR]  ,
						[H5_ES_ACCT_TO_BE_DSCNCT_CD]  ,
						[PRS_PRICE_QOT_NBR]  ,
						[GCS_PRICE_QOT_NBR]  ,
						[CUST_CNTRC_LGTH_ID] ,
						[SALS_PERSN_NME]  ,
						[CLCM_NME]  ,
						[CLCM_PHN_NBR]  ,
						[SOI]  ,
						[SALS_PERSN_CID]  ,
						[SALS_BR_NME]  ,
						[ORDR_STUS_ID]  ,
						[CREAT_DT]  ,
						[CREAT_BY_USER_ID]  ,
						[REC_STUS_ID]  ,
						[MODFD_DT] ,
						[MODFD_BY_USER_ID] ,
						[PROD_TYPE_ID]  ,
						[SRVC_TYPE_CD]  ,
						[CKT_TYPE_CD]  ,
						[ORDR_TYPE_ID]  ,
						[H5_CUST_ACCT_NBR]  ,
						[ORGTNG_CHARS_ID] ,
						[TRMTG_CHARS_ID] ,
						[ORGTNG_CO_ID]  ,
						[TRMTG_CO_CD]  ,
						[OEQPT_RAD_BOX_NEED_CD]  ,
						[OEQPT_CSU_DSU_CD]  ,
						[OEQPT_PRE_XST_CLLI_CD]  ,
						[OEQPT_CPE_DISC_PCT]  ,
						[OEQPT_CPE_CMNT_TXT]  ,
						[CAL_BEF_DSPCH_HR_TXT]  ,
						[XTNL_MOVE_CD]  ,
						[CKT_SPD_QTY] ,
						[ORDR_CMNT_TXT] ,
						[SALS_PERSN_CO_CD] ,
						[PREV_ORDR_TYPE_ID]  ,
						[RELTD_ORDR_ID] ,
						[BASE_PLAN_TYPE_NME]  ,
						[PRMT_CD]  ,
						[ORGTNG_CMNT_TXT]  ,
						[TRMTG_CMNT_TXT]  ,
						[PL_CAT_TXT]  )

					SELECT [ORDR_ID]  ,
						[RE_SBMSN_CD]  ,
						[CUST_CNTRC_SIGN_DT] ,
						[CUST_ORDR_SBMT_DT] ,
						[CUST_WANT_DT] ,
						[CUST_CMMT_DT] ,
						[CUST_ACPT_ERLY_SRVC_CD]  ,
						[NEW_CNSTRCTN_CD]  ,
						[UNMAN_SITE_CD]  ,
						[DSCNCT_REAS_CD] ,
						[TSP_CD]  ,
						[SCA_NBR]  ,
						[GVRMNT_CUST_CD]  ,
						[PRE_XST_PL_NBR]  ,
						[H5_ES_ACCT_TO_BE_DSCNCT_CD]  ,
						[PRS_PRICE_QOT_NBR]  ,
						[GCS_PRICE_QOT_NBR]  ,
						[CUST_CNTRC_LGTH_ID] ,
						[SALS_PERSN_NME]  ,
						[CLCM_NME]  ,
						[CLCM_PHN_NBR]  ,
						[SOI]  ,
						[SALS_PERSN_CID]  ,
						[SALS_BR_NME]  ,
						[ORDR_STUS_ID]  ,
						[CREAT_DT]  ,
						[CREAT_BY_USER_ID]  ,
						[REC_STUS_ID]  ,
						[MODFD_DT] ,
						[MODFD_BY_USER_ID] ,
						[PROD_TYPE_ID]  ,
						[SRVC_TYPE_CD]  ,
						[CKT_TYPE_CD]  ,
						[ORDR_TYPE_ID]  ,
						[H5_CUST_ACCT_NBR] ,
						[ORGTNG_CHARS_ID] ,
						[TRMTG_CHARS_ID] ,
						[ORGTNG_CO_ID]  ,
						[TRMTG_CO_CD]  ,
						[OEQPT_RAD_BOX_NEED_CD]  ,
						[OEQPT_CSU_DSU_CD]  ,
						[OEQPT_PRE_XST_CLLI_CD]  ,
						[OEQPT_CPE_DISC_PCT]  ,
						[OEQPT_CPE_CMNT_TXT]  ,
						[CAL_BEF_DSPCH_HR_TXT]  ,
						[XTNL_MOVE_CD]  ,
						[CKT_SPD_QTY] ,
						[ORDR_CMNT_TXT] ,
						[SALS_PERSN_CO_CD] ,
						[PREV_ORDR_TYPE_ID]  ,
						[RELTD_ORDR_ID] ,
						[BASE_PLAN_TYPE_NME]  ,
						[PRMT_CD]  ,
						[ORGTNG_CMNT_TXT]  ,
						[TRMTG_CMNT_TXT]  ,
						[PL_CAT_TXT]
					FROM	[COWS].[arch].[IPL_ORDR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[IPL_ORDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[IPL_ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
					DELETE  FROM	[COWS].[arch].[IPL_ORDR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
					Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[IPL_ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
					
					INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[IPL_ORDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			END
			/*  ----------------------------L2P_LOG   NO NEED TO BRING BACK ----------------------------*/
			----IF(@Mood<>0)
			--IF(@Mood=1 OR @Mood=2 OR @Mood=3 OR @Mood=4 OR @Mood=5 OR @Mood=6)
			--BEGIN
			--	/*
			--	FOREIGN_KEY		TABLE_NAME		COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			--	FK01_L2P_LOG	L2P_LOG			ORDR_ID			ORDR			ORDR_ID
			--	FK02_L2P_LOG	L2P_LOG			EVENT_ID		EVENT			EVENT_ID
			--	-------------------------------------------------ok--------EVENT--------ORDER-->>--------------------
			--	*/
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[L2P_LOG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	INSERT INTO [dbo].[L2P_LOG](
			--		[L2P_LOG_ID]   ,
			--		[H1]  ,
			--		[ORDR_ID] ,
			--		[L2P_UPDT_DT] ,
			--		[ERROR_MSG] ,
			--		[CREAT_DT]  ,
			--		[EVENT_ID] )
			--	SELECT [L2P_LOG_ID]   ,
			--		[H1]  ,
			--		[ORDR_ID] ,
			--		[L2P_UPDT_DT] ,
			--		[ERROR_MSG] ,
			--		[CREAT_DT]  ,
			--		[EVENT_ID]	
			--	FROM	[COWS].[arch].[L2P_LOG] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) AND [L2P_LOG_ID] NOT IN
			--			(SELECT [L2P_LOG_ID]  FROM [dbo].[L2P_LOG] WITH(NOLOCK))
			--	UNION
			--	SELECT [L2P_LOG_ID]   ,
			--		[H1]  ,
			--		[ORDR_ID] ,
			--		[L2P_UPDT_DT] ,
			--		[ERROR_MSG] ,
			--		[CREAT_DT]  ,
			--		[EVENT_ID]
			--	FROM	[COWS].[arch].[L2P_LOG] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table) AND [L2P_LOG_ID] NOT IN
			--			(SELECT [L2P_LOG_ID]  FROM [dbo].[L2P_LOG] WITH(NOLOCK))
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[L2P_LOG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[L2P_LOG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[L2P_LOG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	DELETE  FROM	[COWS].[arch].[L2P_LOG] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table) /*AND [L2P_LOG_ID] NOT IN
			--			(SELECT [L2P_LOG_ID]  FROM [arch].[L2P_LOG] WITH(NOLOCK))*/
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[L2P_LOG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[L2P_LOG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	DELETE  FROM	[COWS].[arch].[L2P_LOG] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table) /*AND [L2P_LOG_ID] NOT IN
			--			(SELECT [L2P_LOG_ID]  FROM [arch].[L2P_LOG] WITH(NOLOCK) )*/
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[L2P_LOG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT		
			--	--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[L2P_LOG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			--END
			/*
			FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_MAP_GRP_TASK	MAP_GRP_TASK	MODFD_BY_USER_ID	LK_USER			USER_ID
			FK02_MAP_GRP_TASK	MAP_GRP_TASK	TASK_ID				LK_TASK			TASK_ID
			FK03_MAP_GRP_TASK	MAP_GRP_TASK	GRP_ID				LK_GRP			GRP_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[MAP_GRP_TASK] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[MAP_GRP_TASK](
				[MAP_GRP_TASK_ID]   ,
				[TASK_ID]  ,
				[GRP_ID]  ,
				[MODFD_BY_USER_ID]  ,
				[MODFD_DT]  ,
				[CREAT_DT] )
			SELECT [MAP_GRP_TASK_ID]   ,
				[TASK_ID]  ,
				[GRP_ID]  ,
				[MODFD_BY_USER_ID]  ,
				[MODFD_DT]  ,
				[CREAT_DT]
			FROM  [dbo].[MAP_GRP_TASK] Where [CREAT_DT]<=CONVERT(datetime,@Date)	
			PRINT 'Finished executing [arch].[MAP_GRP_TASK] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[MAP_GRP_TASK] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM  [dbo].[MAP_GRP_TASK] Where [CREAT_DT]<=CONVERT(datetime,@Date)		
			PRINT 'Finished executing [dbo].[MAP_GRP_TASK] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MAP_GRP_TASK]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_MAP_RPT_GRP	MAP_RPT_GRP		GRP_ID				LK_GRP			GRP_ID
			FK02_MAP_RPT_GRP	MAP_RPT_GRP		CREAT_BY_USER_ID	LK_USER			USER_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[MAP_RPT_GRP] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[MAP_RPT_GRP](
				[MAP_RPT_GRP_ID]  ,
				[GRP_ID]  ,
				[RPT_GRP_ID]  ,
				[CREAT_DT]  ,
				[CREAT_BY_USER_ID] )
			SELECT [MAP_RPT_GRP_ID]  ,
				[GRP_ID]  ,
				[RPT_GRP_ID]  ,
				[CREAT_DT]  ,
				[CREAT_BY_USER_ID]	
			FROM  [dbo].[MAP_RPT_GRP] Where [CREAT_DT]<=CONVERT(datetime,@Date)		
			PRINT 'Finished executing [arch].[MAP_RPT_GRP] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[MAP_RPT_GRP] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM  [dbo].[MAP_RPT_GRP] Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[MAP_RPT_GRP] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'	
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MAP_RPT_GRP]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
			FK01_MDS_EVENT_ACCS	MDS_EVENT_ACCS	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
			------------------------------------ok-----------------------------EVENT------------ORDER--------------------

			*/
			IF(@Mood=5)  /* ---------------------MDS EVENT ONLY--------------------- */
			BEGIN
			
				
				--------FSA_MDS_EVENT_ID
				SET IDENTITY_INSERT  [COWS].[dbo].[MDS_EVENT_SRVC]     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_SRVC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[MDS_EVENT_SRVC]
				   ([MDS_EVENT_SRVC_TYPE_ID]
				  ,[FSA_MDS_EVENT_ID]
				  ,[MACH5_H6_SRVC_ID]
				  ,[SRVC_TYPE_ID]
				  ,[THRD_PARTY_VNDR_ID]
				  ,[THRD_PARTY_ID]
				  ,[THRD_PARTY_SRVC_LVL_ID]
				  ,[ACTV_DT]
				  ,[CMNT_TXT]
				  ,[EMAIL_CD]
				  ,[TAB_SEQ_NBR])
				SELECT 
				   [MDS_EVENT_SRVC_TYPE_ID]
				  ,[FSA_MDS_EVENT_ID]
				  ,[MACH5_H6_SRVC_ID]
				  ,[SRVC_TYPE_ID]
				  ,[THRD_PARTY_VNDR_ID]
				  ,[THRD_PARTY_ID]
				  ,[THRD_PARTY_SRVC_LVL_ID]
				  ,[ACTV_DT]
				  ,[CMNT_TXT]
				  ,[EMAIL_CD]
				  ,[TAB_SEQ_NBR]
				FROM	[COWS].[arch].[MDS_EVENT_SRVC] WITH(NOLOCK)	WHERE FSA_MDS_EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_SRVC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[MDS_EVENT_SRVC]     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_SRVC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_SRVC] WITH(TABLOCKX)	WHERE FSA_MDS_EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_SRVC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_SRVC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_ACCS     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_ACCS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_ACCS](
					[MDS_EVENT_ACCS_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[ACCS_FTN_NBR]  ,
					[ORDR_ID] ,
					[TAB_SEQ_NBR]  )
				SELECT [MDS_EVENT_ACCS_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[ACCS_FTN_NBR]  ,
					[ORDR_ID] ,
					[TAB_SEQ_NBR]
				FROM	[COWS].[arch].[MDS_EVENT_ACCS] WITH(NOLOCK)	WHERE [FSA_MDS_EVENT_ID]	IN	( SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_ACCS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_ACCS     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_ACCS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_ACCS] WITH(TABLOCKX)	WHERE [FSA_MDS_EVENT_ID]	IN	( SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_ACCS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_ACCS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_CPE	MDS_EVENT_CPE	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				------------------------------------------------ok--------------EVENT---------------ORDER----------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_CPE     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_CPE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_CPE](
					[MDS_EVENT_CPE_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[CPE_FTN_NBR]  ,
					[EQPT_DES]  ,
					[EQPT_VNDR_NME]  ,
					[ORDR_ID] ,
					[TAB_SEQ_NBR]  )
				SELECT [MDS_EVENT_CPE_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[CPE_FTN_NBR]  ,
					[EQPT_DES]  ,
					[EQPT_VNDR_NME]  ,
					[ORDR_ID] ,
					[TAB_SEQ_NBR]
				FROM [arch].[MDS_EVENT_CPE] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				UNION
				SELECT [MDS_EVENT_CPE_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[CPE_FTN_NBR]  ,
					[EQPT_DES]  ,
					[EQPT_VNDR_NME]  ,
					[ORDR_ID] ,
					[TAB_SEQ_NBR]
				FROM	[COWS].[arch].[MDS_EVENT_CPE] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)	
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_CPE] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_CPE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_CPE     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_CPE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_CPE] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_CPE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_CPE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_CPE] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_CPE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_CPE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				------------------------------------------------------------------------------------------------------------------------
				-----------------------------------OK-------------------------EVENT-----------------------------------------------------
				------------------------------------------------------------------------------------------------------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_DISCO     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_DISCO] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_DISCO](
					[MDS_EVENT_DISC_OE_ID]   ,
					[H5_H6_CUST_ID]  ,
					[SERIAL_NBR]  ,
					[CREAT_DT]  ,
					[DEV_MODEL_NME]  ,
					[ODIE_DEV_NME]  ,
					[EVENT_ID] ,
					[VNDR_NME]  ,
					[MODEL_NME]  ,
					[INTL_CTRY_CD]  ,
					[PHN_NBR] )
				SELECT  [MDS_EVENT_DISC_OE_ID]   ,
					[H5_H6_CUST_ID]  ,
					[SERIAL_NBR]  ,
					[CREAT_DT]  ,
					[DEV_MODEL_NME]  ,
					[ODIE_DEV_NME]  ,
					[EVENT_ID] ,
					[VNDR_NME]  ,
					[MODEL_NME]  ,
					[INTL_CTRY_CD]  ,
					[PHN_NBR]
				FROM	[COWS].[arch].[MDS_EVENT_DISCO] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_DISCO] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_DISCO     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_DISCO] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_DISCO] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_DISCO] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_DISCO]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_DSL_TRPT	MDS_EVENT_DSL_TRPT	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				FK02_MDS_EVENT_DSL_TRPT	MDS_EVENT_DSL_TRPT	TELCO_ID			LK_TELCO			TELCO_ID
				--------------------------------------ok-----EVENT-----------------------------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_DSL_TRPT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_DSL_TRPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_DSL_TRPT](
					[MDS_EVENT_DSL_TRPT_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[PRIM_BKUP_CD]  ,
					[IP_ADR]  ,
					[SUBNET_MASK_ADR]  ,
					[NXTHOP_GTWY_ADR]  ,
					[SPRINT_MNGD_CD]  ,
					[PRVDR_CKT_ID]  ,
					[TELCO_ID]  ,
					[SPRINT_CUST_CD]  ,
					[TAB_SEQ_NBR]  )
				SELECT 	[MDS_EVENT_DSL_TRPT_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[PRIM_BKUP_CD]  ,
					[IP_ADR]  ,
					[SUBNET_MASK_ADR]  ,
					[NXTHOP_GTWY_ADR]  ,
					[SPRINT_MNGD_CD]  ,
					[PRVDR_CKT_ID]  ,
					[TELCO_ID]  ,
					[SPRINT_CUST_CD]  ,
					[TAB_SEQ_NBR]
				FROM	[COWS].[arch].[MDS_EVENT_DSL_TRPT] WITH(NOLOCK)	WHERE [FSA_MDS_EVENT_ID] IN	(SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_DSL_TRPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_DSL_TRPT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_DSL_TRPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_DSL_TRPT] WITH(TABLOCKX)	WHERE [FSA_MDS_EVENT_ID] IN	( SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_DSL_TRPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_DSL_TRPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				[MDS_EVENT]
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME				REFERANCE_TABLE			REFERANCE_COLUMN_NAME
				FK02_MDS_EVENT	MDS_EVENT	REC_STUS_ID				LK_REC_STUS				REC_STUS_ID
				FK03_MDS_EVENT	MDS_EVENT	EVENT_STUS_ID			LK_EVENT_STUS			EVENT_STUS_ID
				FK05_MDS_EVENT	MDS_EVENT	CREAT_BY_USER_ID		LK_USER					USER_ID
				FK06_MDS_EVENT	MDS_EVENT	MODFD_BY_USER_ID		LK_USER					USER_ID
				FK07_MDS_EVENT	MDS_EVENT	EVENT_ID				EVENT					EVENT_ID
				FK10_MDS_EVENT	MDS_EVENT	MDS_ACTY_TYPE_ID		LK_MDS_ACTY_TYPE		MDS_ACTY_TYPE_ID
				FK13_MDS_EVENT	MDS_EVENT	MDS_INSTL_ACTY_TYPE_ID	LK_MDS_INSTL_ACTY_TYPE	MDS_INSTL_ACTY_TYPE_ID
				FK14_MDS_EVENT	MDS_EVENT	MDS_FAST_TRK_TYPE_ID	MDS_FAST_TRK_TYPE		MDS_FAST_TRK_TYPE_ID
				FK16_MDS_EVENT	MDS_EVENT	IP_VER_ID				LK_IP_VER				IP_VER_ID
				FK17_MDS_EVENT	MDS_EVENT	REQOR_USER_ID			LK_USER					USER_ID
				FK18_MDS_EVENT	MDS_EVENT	EVENT_TYPE_ID			LK_EVENT_TYPE_TME_SLOT	EVENT_TYPE_ID
				FK18_MDS_EVENT	MDS_EVENT	TME_SLOT_ID				LK_EVENT_TYPE_TME_SLOT	TME_SLOT_ID
				FK19_MDS_EVENT	MDS_EVENT	EVENT_TYPE_ID			LK_EVENT_TYPE			EVENT_TYPE_ID
				FK20_MDS_EVENT	MDS_EVENT	FAIL_REAS_ID			LK_FAIL_REAS			FAIL_REAS_ID
				FK21_MDS_EVENT	MDS_EVENT	WRKFLW_STUS_ID			LK_WRKFLW_STUS			WRKFLW_STUS_ID
				FK22_MDS_EVENT	MDS_EVENT	ESCL_REAS_ID			LK_ESCL_REAS			ESCL_REAS_ID
				FK23_MDS_EVENT	MDS_EVENT	OLD_EVENT_STUS_ID		LK_EVENT_STUS			EVENT_STUS_ID
				FK24_MDS_EVENT	MDS_EVENT	OPT_OUT_REAS_ID			LK_OPT_OUT_REAS			OPT_OUT_REAS_ID
				FK25_MDS_EVENT	MDS_EVENT	CSG_LVL_CD				LK_CSG_LVL				CSG_LVL_CD
				--------------------------------------OK-----------------------EVENT-----------------------------------
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_FSA_MDS_EVENT	FSA_MDS_EVENT	EVENT_ID	MDS_EVENT	EVENT_ID

				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT](
					[EVENT_ID]
				  ,[EVENT_STUS_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[H1]
				  ,[CUST_NME]
				  ,[CHARS_ID]
				  ,[CUST_ACCT_TEAM_PDL_NME]
				  ,[CUST_SOW_LOC_TXT]
				  ,[MDS_ACTY_TYPE_ID]
				  ,[SHRT_DES]
				  ,[H6]
				  ,[CCD]
				  ,[OPT_OUT_REAS_ID]
				  ,[OPT_OUT_BUS_JUSTN_TXT]
				  ,[SITE_ID_TXT]
				  ,[INSTL_SITE_POC_NME]
				  ,[INSTL_SITE_POC_INTL_PHN_CD]
				  ,[INSTL_SITE_POC_PHN_NBR]
				  ,[INSTL_SITE_POC_INTL_CELL_PHN_CD]
				  ,[INSTL_SITE_POC_CELL_PHN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_INTL_PHN_CD]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_INTL_CELL_PHN_CD]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[SRVC_TME_ZN_CD]
				  ,[SRVC_AVLBLTY_HRS]
				  ,[US_INTL_CD]
				  --,[SITE_ADR]
				  ,[FLR_BLDG_NME]
				  ,[CTY_NME]
				  ,[STT_PRVN_NME]
				  ,[CTRY_RGN_NME]
				  ,[ZIP_CD]
				  ,[SPRINT_CPE_NCR_ID]
				  ,[CPE_DSPCH_EMAIL_ADR]
				  ,[CPE_DSPCH_CMNT_TXT]
				  ,[ESCL_CD]
				  ,[ESCL_REAS_ID]
				  ,[PRIM_REQ_DT]
				  ,[SCNDY_REQ_DT]
				  ,[ESCL_BUS_JUSTN_TXT]
				  ,[CNFRC_BRDG_ID]
				  ,[CNFRC_BRDG_NBR]
				  ,[CNFRC_PIN_NBR]
				  ,[ONLINE_MEETING_ADR]
				  ,[PUB_EMAIL_CC_TXT]
				  ,[CMPLTD_EMAIL_CC_TXT]
				  ,[REQOR_USER_ID]
				  ,[REQOR_USER_CELL_PHN_NBR]
				  ,[CMNT_TXT]
				  ,[MDS_FAST_TRK_TYPE_ID]
				  ,[TME_SLOT_ID]
				  ,[PRE_CFG_CMPLT_CD]
				  ,[DISC_MGMT_CD]
				  ,[FULL_CUST_DISC_CD]
				  ,[FULL_CUST_DISC_REAS_TXT]
				  ,[STRT_TMST]
				  ,[END_TMST]
				  ,[EXTRA_DRTN_TME_AMT]
				  ,[EVENT_DRTN_IN_MIN_QTY]
				  ,[WRKFLW_STUS_ID]
				  ,[OLD_EVENT_STUS_ID]
				  ,[FAIL_REAS_ID]
				  ,[WOOB_IP_ADR]
				  ,[SHIPPED_DT]
				  ,[SHIP_TRK_REFR_NBR]
				  ,[SHIP_CUST_EMAIL_ADR]
				  ,[SHIP_DEV_SERIAL_NBR]
				  ,[SHIP_CXR_NME]
				  ,[CPE_ORDR_CD]
				  ,[MNS_ORDR_CD]
				  ,[ESCL_POLICY_LOC_TXT]
				  ,[MNS_PM_ID]
				  ,[CREAT_BY_USER_ID]
				  ,[MODFD_BY_USER_ID]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				  ,[REC_STUS_ID]
				  ,[CUST_TRPT_REQR_CD]
				  ,[WRLES_TRPT_REQR_CD]
				  ,[BUS_JUSTN_TXT]
				  ,[CRDLEPNT_CD]
				  ,[NTWK_EVENT_TYPE_ID]
				  ,[VPN_PLTFRM_TYPE_ID]
				  ,[DD_APRVL_NBR] 
				  ,[SALS_ENGR_PHN]
				  ,[SALS_ENGR_EMAIL]
				  ,[NTWK_CUST_NME]
				  ,[NTWK_H6]
				  ,[NTWK_H1]  )
				SELECT 
				   [EVENT_ID]
				  ,[EVENT_STUS_ID]
				  ,[EVENT_TITLE_TXT]
				  ,[H1]
				  ,[CUST_NME]
				  ,[CHARS_ID]
				  ,[CUST_ACCT_TEAM_PDL_NME]
				  ,[CUST_SOW_LOC_TXT]
				  ,[MDS_ACTY_TYPE_ID]
				  ,[SHRT_DES]
				  ,[H6]
				  ,[CCD]
				  ,[OPT_OUT_REAS_ID]
				  ,[OPT_OUT_BUS_JUSTN_TXT]
				  ,[SITE_ID_TXT]
				  ,[INSTL_SITE_POC_NME]
				  ,[INSTL_SITE_POC_INTL_PHN_CD]
				  ,[INSTL_SITE_POC_PHN_NBR]
				  ,[INSTL_SITE_POC_INTL_CELL_PHN_CD]
				  ,[INSTL_SITE_POC_CELL_PHN_NBR]
				  ,[SRVC_ASSRN_POC_NME]
				  ,[SRVC_ASSRN_POC_INTL_PHN_CD]
				  ,[SRVC_ASSRN_POC_PHN_NBR]
				  ,[SRVC_ASSRN_POC_INTL_CELL_PHN_CD]
				  ,[SRVC_ASSRN_POC_CELL_PHN_NBR]
				  ,[SRVC_TME_ZN_CD]
				  ,[SRVC_AVLBLTY_HRS]
				  ,[US_INTL_CD]
				  --,[SITE_ADR]
				  ,[FLR_BLDG_NME]
				  ,[CTY_NME]
				  ,[STT_PRVN_NME]
				  ,[CTRY_RGN_NME]
				  ,[ZIP_CD]
				  ,[SPRINT_CPE_NCR_ID]
				  ,[CPE_DSPCH_EMAIL_ADR]
				  ,[CPE_DSPCH_CMNT_TXT]
				  ,[ESCL_CD]
				  ,[ESCL_REAS_ID]
				  ,[PRIM_REQ_DT]
				  ,[SCNDY_REQ_DT]
				  ,[ESCL_BUS_JUSTN_TXT]
				  ,[CNFRC_BRDG_ID]
				  ,[CNFRC_BRDG_NBR]
				  ,[CNFRC_PIN_NBR]
				  ,[ONLINE_MEETING_ADR]
				  ,[PUB_EMAIL_CC_TXT]
				  ,[CMPLTD_EMAIL_CC_TXT]
				  ,[REQOR_USER_ID]
				  ,[REQOR_USER_CELL_PHN_NBR]
				  ,[CMNT_TXT]
				  ,[MDS_FAST_TRK_TYPE_ID]
				  ,[TME_SLOT_ID]
				  ,[PRE_CFG_CMPLT_CD]
				  ,[DISC_MGMT_CD]
				  ,[FULL_CUST_DISC_CD]
				  ,[FULL_CUST_DISC_REAS_TXT]
				  ,[STRT_TMST]
				  ,[END_TMST]
				  ,[EXTRA_DRTN_TME_AMT]
				  ,[EVENT_DRTN_IN_MIN_QTY]
				  ,[WRKFLW_STUS_ID]
				  ,[OLD_EVENT_STUS_ID]
				  ,[FAIL_REAS_ID]
				  ,[WOOB_IP_ADR]
				  ,[SHIPPED_DT]
				  ,[SHIP_TRK_REFR_NBR]
				  ,[SHIP_CUST_EMAIL_ADR]
				  ,[SHIP_DEV_SERIAL_NBR]
				  ,[SHIP_CXR_NME]
				  ,[CPE_ORDR_CD]
				  ,[MNS_ORDR_CD]
				  ,[ESCL_POLICY_LOC_TXT]
				  ,[MNS_PM_ID]
				  ,[CREAT_BY_USER_ID]
				  ,[MODFD_BY_USER_ID]
				  ,[MODFD_DT]
				  ,[CREAT_DT]
				  ,[REC_STUS_ID]
				  ,[CUST_TRPT_REQR_CD]
				  ,[WRLES_TRPT_REQR_CD]
				  ,[BUS_JUSTN_TXT]
				  ,[CRDLEPNT_CD]
				  ,[NTWK_EVENT_TYPE_ID]
				  ,[VPN_PLTFRM_TYPE_ID]
				  ,[DD_APRVL_NBR] 
				  ,[SALS_ENGR_PHN]
				  ,[SALS_ENGR_EMAIL]
				  ,[NTWK_CUST_NME]
				  ,[NTWK_H6]
				  ,[NTWK_H1]
				FROM	[COWS].[arch].[MDS_EVENT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				/*
				MDS_EVENT_NTWK_TRPT
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_NTWK_TRPT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_NTWK_TRPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_NTWK_TRPT](
					 [MDS_EVENT_NTWK_CUST_ID] 
					,[EVENT_ID] 
					,[INSTL_SITE_POC_NME] 
					,[INSTL_SITE_POC_PHN] 
					,[INSTL_SITE_POC_EMAIL]
					,[ROLE_NME] 
					,[CREAT_DT] 
					,[REC_STUS_ID] 
					,[MODFD_DT]
					  )
				SELECT 
				     [MDS_EVENT_NTWK_CUST_ID] 
					,[EVENT_ID] 
					,[INSTL_SITE_POC_NME] 
					,[INSTL_SITE_POC_PHN] 
					,[INSTL_SITE_POC_EMAIL]
					,[ROLE_NME] 
					,[CREAT_DT] 
					,[REC_STUS_ID] 
					,[MODFD_DT]
				FROM	[COWS].[arch].[MDS_EVENT_NTWK_TRPT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_NTWK_TRPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_NTWK_TRPT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_NTWK_TRPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_NTWK_TRPT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_NTWK_TRPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_NTWK_TRPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				/*
				MDS_EVENT_NTWK_TRPT
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_NTWK_CUST     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_NTWK_CUST] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_NTWK_CUST](
					 [MDS_EVENT_NTWK_TRPT_ID] 
					,[EVENT_ID] 
					,[MDS_TRNSPRT_TYPE] 
					,[LEC_PRVDR_NME] 
					,[LEC_CNTCT_INFO] 
					,[MACH5_ORDR_NBR] 
					,[IP_NUA_ADR] 
					,[MPLS_ACCS_BDWD] 
					,[SCA_NBR] 
					,[TAGGD_CD] 
					,[VLAN_NBR] 
					,[MULTI_VRF_REQ] 
					,[IP_VER] 
					,[LOC_CITY] 
					,[LOC_STT_PRVN] 
					,[LOC_CTRY] 
					,[ASSOC_H6] 
					,[CREAT_DT] 
					,[REC_STUS_ID] 
					,[MODFD_DT]
					  )
				SELECT 
				     [MDS_EVENT_NTWK_TRPT_ID] 
					,[EVENT_ID] 
					,[MDS_TRNSPRT_TYPE] 
					,[LEC_PRVDR_NME] 
					,[LEC_CNTCT_INFO] 
					,[MACH5_ORDR_NBR] 
					,[IP_NUA_ADR] 
					,[MPLS_ACCS_BDWD] 
					,[SCA_NBR] 
					,[TAGGD_CD] 
					,[VLAN_NBR] 
					,[MULTI_VRF_REQ] 
					,[IP_VER] 
					,[LOC_CITY] 
					,[LOC_STT_PRVN] 
					,[LOC_CTRY] 
					,[ASSOC_H6] 
					,[CREAT_DT] 
					,[REC_STUS_ID] 
					,[MODFD_DT]
				FROM	[COWS].[arch].[MMDS_EVENT_NTWK_CUST] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_NTWK_CUST] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_NTWK_CUST     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_NTWK_CUST] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_NTWK_TRPT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_NTWK_CUST] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_NTWK_CUST]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				/*

				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME				REFERANCE_TABLE			REFERANCE_COLUMN_NAME
				FK01_FSA_MDS_EVENT	FSA_MDS_EVENT	EVENT_ID				MDS_EVENT				EVENT_ID
				FK02_FSA_MDS_EVENT	FSA_MDS_EVENT	ORDR_EVENT_STUS_ID		LK_EVENT_STUS			EVENT_STUS_ID
				FK05_FSA_MDS_EVENT	FSA_MDS_EVENT	CREAT_BY_USER_ID		LK_USER					USER_ID
				FK06_FSA_MDS_EVENT	FSA_MDS_EVENT	MODFD_BY_USER_ID		LK_USER					USER_ID
				FK08_FSA_MDS_EVENT	FSA_MDS_EVENT	TME_ZONE_ID				LK_TME_ZONE				TME_ZONE_ID
				FK10_FSA_MDS_EVENT	FSA_MDS_EVENT	SRVC_ASSRN_SITE_SUPP_ID	LK_SRVC_ASSRN_SITE_SUPP	SRVC_ASSRN_SITE_SUPP_ID
				FK11_FSA_MDS_EVENT	FSA_MDS_EVENT	SPRINT_CPE_NCR_ID		LK_SPRINT_CPE_NCR		SPRINT_CPE_NCR_ID
				--------------------------------OK-----------------------------EVENT--------------------------------
				FOREIGN_KEY							TABLE_NAME					COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK06_EVENT_HIST						EVENT_HIST					FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				FK01_MDS_EVENT_FMS_CKT				MDS_EVENT_FMS_CKT			FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				FK01_MDS_EVENT_MNGD_DEV				MDS_EVENT_MNGD_DEV			FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				FK05_MDS_EVENT_MNS_OE				MDS_EVENT_MNS_OE			FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				FK01_MDS_EVENT_PVC					MDS_EVENT_PVC				FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				FK03_MDS_EVENT_WIRED_TRNSPRT_DEV	MDS_EVENT_WIRED_TRNSPRT_DEV	FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				FK01_MDS_EVENT_WRLS_TRNSPRT_DEV		MDS_EVENT_WRLS_TRNSPRT_DEV	FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				FK02_ODIE_RSPN_INFO					ODIE_RSPN_INFO				FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. FSA_MDS_EVENT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_MDS_EVENT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[FSA_MDS_EVENT](
					[FSA_MDS_EVENT_ID]   ,
					[EVENT_ID]  ,
					[TAB_NME]  ,
					[ORDR_EVENT_STUS_ID]  ,
					[TME_ZONE_ID]  ,
					[INSTL_SITE_POC_PHN_NBR]  ,
					[INSTL_SITE_POC_CELL_PHN_NBR]  ,
					[SRVC_ASSRN_POC_PHN_NBR]  ,
					[SRVC_ASSRN_POC_EMAIL_ADR]  ,
					[WIRED_DEV_TRNSPRT_REQR_CD]  ,
					[WRLS_TRNSPRT_REQR_CD]  ,
					[VRTL_CNCTN_CD]  ,
					[ESCL_CD]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[SPRINT_CPE_NCR_ID]  ,
					[SRVC_ASSRN_SITE_SUPP_ID]  ,
					[CPE_DSPCH_EMAIL_ADR]  ,
					[CPE_DSPCH_CMNT_TXT]  ,
					[MULTI_LINK_CKT_CD]  ,
					[SRVC_ASSRN_POC_CELL_NBR] ,
					[EVENT_SCHED_NTE_TXT]  ,
					[TAB_SEQ_NBR]  ,
					[H5_H6_ID]  ,
					[CMPLTD_CD]  ,
					[ADR]  ,
					[FLR_BLDG_NME] ,
					[CTY_NME] ,
					[STT_PRVN_NME]  ,
					[ZIP_CD]  ,
					[US_INTL_ID]  ,
					[CTRY_RGN_NME]  ,
					[H5_H6_CUST_ID]  ,
					[SRVC_ASSRN_POC_NME] ,
					[INSTL_SITE_POC_NME] )
				SELECT  [FSA_MDS_EVENT_ID]   ,
					[EVENT_ID]  ,
					[TAB_NME]  ,
					[ORDR_EVENT_STUS_ID]  ,
					[TME_ZONE_ID]  ,
					[INSTL_SITE_POC_PHN_NBR]  ,
					[INSTL_SITE_POC_CELL_PHN_NBR]  ,
					[SRVC_ASSRN_POC_PHN_NBR]  ,
					[SRVC_ASSRN_POC_EMAIL_ADR]  ,
					[WIRED_DEV_TRNSPRT_REQR_CD]  ,
					[WRLS_TRNSPRT_REQR_CD]  ,
					[VRTL_CNCTN_CD]  ,
					[ESCL_CD]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[SPRINT_CPE_NCR_ID]  ,
					[SRVC_ASSRN_SITE_SUPP_ID]  ,
					[CPE_DSPCH_EMAIL_ADR]  ,
					[CPE_DSPCH_CMNT_TXT]  ,
					[MULTI_LINK_CKT_CD]  ,
					[SRVC_ASSRN_POC_CELL_NBR] ,
					[EVENT_SCHED_NTE_TXT]  ,
					[TAB_SEQ_NBR]  ,
					[H5_H6_ID]  ,
					[CMPLTD_CD]  ,
					[ADR]  ,
					[FLR_BLDG_NME] ,
					[CTY_NME] ,
					[STT_PRVN_NME]  ,
					[ZIP_CD]  ,
					[US_INTL_ID]  ,
					[CTRY_RGN_NME]  ,
					[H5_H6_CUST_ID]  ,
					[SRVC_ASSRN_POC_NME] ,
					[INSTL_SITE_POC_NME]
				FROM	[COWS].[arch].[FSA_MDS_EVENT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_MDS_EVENT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. FSA_MDS_EVENT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_MDS_EVENT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[FSA_MDS_EVENT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_MDS_EVENT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_MDS_EVENT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_FMS_CKT	MDS_EVENT_FMS_CKT	FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				-------------------------------------------ok----------EVENT----------ORDER-->>CKTID---------------------------------

				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_FMS_CKT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_FMS_CKT](
					[FSA_MDS_EVENT_ID]  ,
					[CKT_ID]  ,
					[CREAT_DT]  )
					
				SELECT [FSA_MDS_EVENT_ID]  ,
					[CKT_ID]  ,
					[CREAT_DT]
				FROM [arch].[MDS_EVENT_FMS_CKT] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				UNION
				SELECT [FSA_MDS_EVENT_ID]  ,
					[CKT_ID]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[MDS_EVENT_FMS_CKT] WITH(NOLOCK)	WHERE CKT_ID	IN	(SELECT CONVERT(varchar(10), CKT_ID) FROM	@CKTID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_FMS_CKT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_FMS_CKT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_FMS_CKT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_FMS_CKT] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_FMS_CKT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_FMS_CKT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_FMS_CKT] WITH(TABLOCKX)	WHERE CKT_ID	IN	(SELECT CONVERT(varchar(10), CKT_ID) FROM	@CKTID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_FMS_CKT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_FMS_CKT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY					TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_FORM_HIST	MDS_EVENT_FORM_CHNG	MDS_EVENT_ID		EVENT			EVENT_ID
				FK02_MDS_EVENT_FORM_HIST	MDS_EVENT_FORM_CHNG	CREAT_BY_USER_ID	LK_USER			USER_ID
				----------------------------------------------OK-------------------------------EVENT---------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_FORM_CHNG     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_FORM_CHNG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_FORM_CHNG](
					[MDS_EVENT_FORM_CHNG_ID]   ,
					[MDS_EVENT_ID]  ,
					[CHNG_TYPE_NME]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID] )
				SELECT  [MDS_EVENT_FORM_CHNG_ID]   ,
					[MDS_EVENT_ID]  ,
					[CHNG_TYPE_NME]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]
				FROM	[COWS].[arch].[MDS_EVENT_FORM_CHNG] WITH(NOLOCK)	WHERE [MDS_EVENT_ID]	IN	(SELECT EVENT_ID FROM	@Event_Table )	
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_FORM_CHNG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_FORM_CHNG     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_FORM_CHNG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_FORM_CHNG] WITH(TABLOCKX)	WHERE [MDS_EVENT_ID]	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_FORM_CHNG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT	
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_FORM_CHNG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK02_MDS_EVENT_LOC	MDS_EVENT_LOC	MDS_LOC_TYPE_ID	LK_MDS_LOC_TYPE	MDS_LOC_TYPE_ID
				FK03_MDS_EVENT_LOC	MDS_EVENT_LOC	MDS_LOC_CAT_ID	LK_MDS_LOC_CAT	MDS_LOC_CAT_ID
				FK04_MDS_EVENT_LOC	MDS_EVENT_LOC	EVENT_ID		EVENT			EVENT_ID
				--------------------------------------------ok---------------------event--------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_LOC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_LOC](
					[MDS_LOC_CAT_ID]  ,
					[MDS_LOC_TYPE_ID]  ,
					[MDS_LOC_NBR]  ,
					[MDS_LOC_RAS_DT]  ,
					[CREAT_DT]  ,
					[EVENT_ID] )
				SELECT  [MDS_LOC_CAT_ID]  ,
					[MDS_LOC_TYPE_ID]  ,
					[MDS_LOC_NBR]  ,
					[MDS_LOC_RAS_DT]  ,
					[CREAT_DT]  ,
					[EVENT_ID]
				FROM	[COWS].[arch].[MDS_EVENT_LOC] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_LOC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_LOC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_LOC] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_LOC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_LOC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				-------------------------OK-----------------------EVENT----------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_MAC_ACTY] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_MAC_ACTY](
					[EVENT_ID]  ,
					[MDS_MAC_ACTY_ID]  ,
					[CREAT_DT]  )
				SELECT [EVENT_ID]  ,
					[MDS_MAC_ACTY_ID]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[MDS_EVENT_MAC_ACTY] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_MAC_ACTY] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_MAC_ACTY] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_MAC_ACTY] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_MAC_ACTY] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_MAC_ACTY]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_MNGD_DEV	MDS_EVENT_MNGD_DEV	FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				FK02_MDS_EVENT_MNGD_DEV	MDS_EVENT_MNGD_DEV	DEV_MODEL_ID		LK_DEV_MODEL	DEV_MODEL_ID
				FK03_MDS_EVENT_MNGD_DEV	MDS_EVENT_MNGD_DEV	MANF_ID				LK_DEV_MANF		MANF_ID
				FK04_MDS_EVENT_MNGD_DEV	MDS_EVENT_MNGD_DEV	ORDR_ID				ORDR			ORDR_ID
				-------------------------------------------ok--------EVENT----------------ORDER-----------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_MNGD_DEV     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_MNGD_DEV] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_MNGD_DEV](
					[MDS_EVENT_MNGD_DEV_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[FTN]  ,
					[DEV_NME]  ,
					[EQPT_DES]  ,
					[EQPT_VNDR_NME]  ,
					[MANF_ID] ,
					[DEV_MODEL_ID] ,
					[CREAT_DT]  ,
					[ORDR_ID])
				SELECT [MDS_EVENT_MNGD_DEV_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[FTN]  ,
					[DEV_NME]  ,
					[EQPT_DES]  ,
					[EQPT_VNDR_NME]  ,
					[MANF_ID] ,
					[DEV_MODEL_ID] ,
					[CREAT_DT]  ,
					[ORDR_ID]	
				FROM [arch].[MDS_EVENT_MNGD_DEV] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				UNION
				SELECT [MDS_EVENT_MNGD_DEV_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[FTN]  ,
					[DEV_NME]  ,
					[EQPT_DES]  ,
					[EQPT_VNDR_NME]  ,
					[MANF_ID] ,
					[DEV_MODEL_ID] ,
					[CREAT_DT]  ,
					[ORDR_ID]
				FROM	[COWS].[arch].[MDS_EVENT_MNGD_DEV] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_MNGD_DEV] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_MNGD_DEV]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_MNGD_DEV     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_MNGD_DEV] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_MNGD_DEV] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_MNGD_DEV] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_MNGD_DEV] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_MNGD_DEV] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_MNGD_DEV] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_MNGD_DEV]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_MNS	MDS_EVENT_MNS	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				FK02_MDS_EVENT_MNS	MDS_EVENT_MNS	MDS_SRVC_TIER_ID	LK_MDS_SRVC_TIER	MDS_SRVC_TIER_ID
				----------------------------------ok--------------EVENT------------ORDER--------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_MNS     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_MNS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_MNS](
					[MDS_EVENT_MNS_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MNS_FTN_NBR]  ,
					[MDS_SRVC_TIER_ID]  ,
					[MDS_ENTLMNT_ID]  ,
					[ORDR_ID] ,
					[TAB_SEQ_NBR] )
				SELECT [MDS_EVENT_MNS_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MNS_FTN_NBR]  ,
					[MDS_SRVC_TIER_ID]  ,
					[MDS_ENTLMNT_ID]  ,
					[ORDR_ID] ,
					[TAB_SEQ_NBR]
				FROM [arch].[MDS_EVENT_MNS] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				UNION
				SELECT [MDS_EVENT_MNS_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MNS_FTN_NBR]  ,
					[MDS_SRVC_TIER_ID]  ,
					[MDS_ENTLMNT_ID]  ,
					[ORDR_ID] ,
					[TAB_SEQ_NBR]
				FROM	[COWS].[arch].[MDS_EVENT_MNS] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_MNS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_MNS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_MNS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_MNS     OFF
				DELETE  FROM [arch].[MDS_EVENT_MNS] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_MNS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_MNS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_MNS] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_MNS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_MNS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_MNS_OE	MDS_EVENT_MNS_OE	MDS_SRVC_TIER_ID	LK_MDS_SRVC_TIER	MDS_SRVC_TIER_ID
				FK02_MDS_EVENT_MNS_OE	MDS_EVENT_MNS_OE	MNS_OE_ID			LK_MNS_OE		MNS_OE_ID
				FK03_MDS_EVENT_MNS_OE	MDS_EVENT_MNS_OE	MNS_OE_TYPE_ID		LK_MNS_OE_TYPE	MNS_OE_TYPE_ID
				FK05_MDS_EVENT_MNS_OE	MDS_EVENT_MNS_OE	FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				FK06_MDS_EVENT_MNS_OE	MDS_EVENT_MNS_OE	ORDR_ID				ORDR			ORDR_ID
				----------------------------------------------------ok---------EVENT-------------ORDER-----------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_MNS_OE     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_MNS_OE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_MNS_OE](
					[MDS_EVENT_MNS_OE_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[FTN]  ,
					[MNS_OE_TYPE_ID]  ,
					[MNS_OE_ID]  ,
					[MDS_SRVC_TIER_ID]  ,
					[MDS_ENTLMNT_ID]  ,
					[CREAT_DT]  ,
					[ORDR_ID] ,
					[MDS_ENTLMNT_OE_ID] )
				SELECT [MDS_EVENT_MNS_OE_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[FTN]  ,
					[MNS_OE_TYPE_ID]  ,
					[MNS_OE_ID]  ,
					[MDS_SRVC_TIER_ID]  ,
					[MDS_ENTLMNT_ID]  ,
					[CREAT_DT]  ,
					[ORDR_ID] ,
					[MDS_ENTLMNT_OE_ID]	
				FROM [arch].[MDS_EVENT_MNS_OE] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				UNION
				SELECT [MDS_EVENT_MNS_OE_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[FTN]  ,
					[MNS_OE_TYPE_ID]  ,
					[MNS_OE_ID]  ,
					[MDS_SRVC_TIER_ID]  ,
					[MDS_ENTLMNT_ID]  ,
					[CREAT_DT]  ,
					[ORDR_ID] ,
					[MDS_ENTLMNT_OE_ID]
				FROM	[COWS].[arch].[MDS_EVENT_MNS_OE] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_MNS_OE] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_MNS_OE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_MNS_OE     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_MNS_OE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_MNS_OE] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_MNS_OE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_MNS_OE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_EVENT_MNS_OE] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_MNGD_DEV] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_MNS_OE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 

				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_ODIE_DEV_NME	MDS_EVENT_ODIE_DEV_NME	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				FK02_MDS_EVENT_ODIE_DEV_NME	MDS_EVENT_ODIE_DEV_NME	MANF_ID				LK_DEV_MANF			MANF_ID
				FK03_MDS_EVENT_ODIE_DEV_NME	MDS_EVENT_ODIE_DEV_NME	DEV_MODEL_ID		LK_DEV_MODEL		DEV_MODEL_ID
				----------------------------------ok----------------Event-------------------------------------
				*/
				
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_ODIE_DEV] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_ODIE_DEV](
				   [MDS_EVENT_ODIE_DEV_ID]
				  ,[EVENT_ID]
				  ,[RDSN_NBR]
				  ,[RDSN_EXP_DT]
				  ,[ODIE_DEV_NME]
				  ,[DEV_MODEL_ID]
				  ,[MANF_ID]
				  ,[FRWL_PROD_CD]
				  ,[INTL_CTRY_CD]
				  ,[PHN_NBR]
				  ,[FAST_TRK_CD]
				  ,[WOOB_CD]
				  ,[OPTOUT_CD]
				  ,[SRVC_ASSRN_SITE_SUPP_ID]
				  ,[CREAT_DT]
				  ,[SYS_CD] )
				SELECT  
				   [MDS_EVENT_ODIE_DEV_ID]
				  ,[EVENT_ID]
				  ,[RDSN_NBR]
				  ,[RDSN_EXP_DT]
				  ,[ODIE_DEV_NME]
				  ,[DEV_MODEL_ID]
				  ,[MANF_ID]
				  ,[FRWL_PROD_CD]
				  ,[INTL_CTRY_CD]
				  ,[PHN_NBR]
				  ,[FAST_TRK_CD]
				  ,[WOOB_CD]
				  ,[OPTOUT_CD]
				  ,[SRVC_ASSRN_SITE_SUPP_ID]
				  ,[CREAT_DT]
				  ,[SYS_CD]
				FROM [arch].[MDS_EVENT_ODIE_DEV] WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_ODIE_DEV] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_ODIE_DEV_NME] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [dbo].[MDS_EVENT_ODIE_DEV] WITH(TABLOCKX) WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_ODIE_DEV] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_ODIE_DEV]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_ODIE_DEV_NME     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_ODIE_DEV_NME] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_ODIE_DEV_NME](
					[MDS_EVENT_ODIE_DEV_NME_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[ODIE_DEV_NME]  ,
					[MANF_ID] ,
					[DEV_MODEL_ID] ,
					[TAB_SEQ_NBR] )
				SELECT  [MDS_EVENT_ODIE_DEV_NME_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[ODIE_DEV_NME]  ,
					[MANF_ID] ,
					[DEV_MODEL_ID] ,
					[TAB_SEQ_NBR]
				FROM [arch].[MDS_EVENT_ODIE_DEV_NME] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_ODIE_DEV_NME] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_ODIE_DEV_NME     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_ODIE_DEV_NME] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_ODIE_DEV_NME] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_ODIE_DEV_NME] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_ODIE_DEV_NME]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_PVC	MDS_EVENT_PVC	FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				-------------------------------------ok--------------EBENT------------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_PVC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_PVC](
					[FSA_MDS_EVENT_ID]  ,
					[DLCI_ID]  ,
					[CREAT_DT]  )
				SELECT [FSA_MDS_EVENT_ID]  ,
					[DLCI_ID]  ,
					[CREAT_DT]
				FROM [arch].[MDS_EVENT_PVC] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_PVC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_PVC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_PVC] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_PVC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_PVC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY					TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_SPAE_TRPT	MDS_EVENT_SPAE_TRPT	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				FK02_MDS_EVENT_SPAE_TRPT	MDS_EVENT_SPAE_TRPT	MDS_TRPT_TYPE_ID	LK_MDS_TRNSPRT_TYPE	MDS_TRNSPRT_TYPE_ID
				FK03_MDS_EVENT_SPAE_TRPT	MDS_EVENT_SPAE_TRPT	TELCO_ID			LK_TELCO			TELCO_ID
				-------------------------------------------ok-----------------EVENT-----------------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_SPAE_TRPT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_SPAE_TRPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_SPAE_TRPT](
					[MDS_EVENT_SPAE_TRPT_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MDS_TRPT_TYPE_ID]  ,
					[PRIM_BKUP_CD]  ,
					[BDWD_CHNL_NME]  ,
					[ECCKT_ID]  ,
					[UNI_NUA_ADR]  ,
					[OLD_CKT_ID]  ,
					[SPRINT_MNGD_CD]  ,
					[TELCO_ID]  ,
					[FOC_DT]  ,
					[TAB_SEQ_NBR] )
				SELECT [MDS_EVENT_SPAE_TRPT_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MDS_TRPT_TYPE_ID]  ,
					[PRIM_BKUP_CD]  ,
					[BDWD_CHNL_NME]  ,
					[ECCKT_ID]  ,
					[UNI_NUA_ADR]  ,
					[OLD_CKT_ID]  ,
					[SPRINT_MNGD_CD]  ,
					[TELCO_ID]  ,
					[FOC_DT]  ,
					[TAB_SEQ_NBR]
				FROM [arch].[MDS_EVENT_SPAE_TRPT] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_SPAE_TRPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_SPAE_TRPT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_SPAE_TRPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_SPAE_TRPT] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_SPAE_TRPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_SPAE_TRPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY							TABLE_NAME						COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_SPRINT_LINK_ATM_TRPT	MDS_EVENT_SPRINT_LINK_ATM_TRPT	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				FK02_MDS_EVENT_SPRINT_LINK_ATM_TRPT	MDS_EVENT_SPRINT_LINK_ATM_TRPT	MDS_TRPT_TYPE_ID	LK_MDS_TRNSPRT_TYPE	MDS_TRNSPRT_TYPE_ID
				FK03_MDS_EVENT_SPRINT_LINK_ATM_TRPT	MDS_EVENT_SPRINT_LINK_ATM_TRPT	TELCO_ID			LK_TELCO			TELCO_ID
				---------------------------------------ok---------------------------EVENT-----------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_SPRINT_LINK_ATM_TRPT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_SPRINT_LINK_ATM_TRPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_SPRINT_LINK_ATM_TRPT](
					[MDS_EVENT_SPRINT_LINK_ATM_TRPT_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MDS_TRPT_TYPE_ID]  ,
					[PRIM_BKUP_CD]  ,
					[BDWD_CHNL_NME]  ,
					[FMS_CKT_ID]  ,
					[PL_NBR]  ,
					[IP_NUA_ADR]  ,
					[OLD_CKT_ID]  ,
					[MULTI_LINK_CKT_CD]  ,
					[SPRINT_MNGD_CD]  ,
					[TELCO_ID]  ,
					[FOC_DT]  ,
					[TAB_SEQ_NBR]  )
				SELECT [MDS_EVENT_SPRINT_LINK_ATM_TRPT_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MDS_TRPT_TYPE_ID]  ,
					[PRIM_BKUP_CD]  ,
					[BDWD_CHNL_NME]  ,
					[FMS_CKT_ID]  ,
					[PL_NBR]  ,
					[IP_NUA_ADR]  ,
					[OLD_CKT_ID]  ,
					[MULTI_LINK_CKT_CD]  ,
					[SPRINT_MNGD_CD]  ,
					[TELCO_ID]  ,
					[FOC_DT]  ,
					[TAB_SEQ_NBR]
				FROM [arch].[MDS_EVENT_SPRINT_LINK_ATM_TRPT] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_SPRINT_LINK_ATM_TRPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_SPRINT_LINK_ATM_TRPT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_SPRINT_LINK_ATM_TRPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_SPRINT_LINK_ATM_TRPT] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_SPRINT_LINK_ATM_TRPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_SPRINT_LINK_ATM_TRPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_VLAN	MDS_EVENT_VLAN	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				----------------------------------------------ok-------------------EVENT-------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_VLAN     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_VLAN] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_VLAN](
					[MDS_EVENT_VLAN_ID]   ,
					[FSA_MDS_EVENT_ID] ,
					[VLAN_NBR]  ,
					[SPA_NUA_ADR] ,
					[TAB_SEQ_NBR]  )
				SELECT [MDS_EVENT_VLAN_ID]   ,
					[FSA_MDS_EVENT_ID] ,
					[VLAN_NBR]  ,
					[SPA_NUA_ADR] ,
					[TAB_SEQ_NBR]	
				FROM [arch].[MDS_EVENT_VLAN] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_VLAN] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_VLAN     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_VLAN] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_VLAN] WITH(TABLOCKX) WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_VLAN] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_VLAN]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_VRTL_CNCTN	MDS_EVENT_VRTL_CNCTN	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				------------------------------------ok---------EVENT--------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_VRTL_CNCTN     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_VRTL_CNCTN] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_VRTL_CNCTN](
					[MDS_EVENT_VRTL_CNCTN_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[FMS_NME]  ,
					[DLCI_VPI_CD]  ,
					[TAB_SEQ_NBR] )
				SELECT 	[MDS_EVENT_VRTL_CNCTN_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[FMS_NME]  ,
					[DLCI_VPI_CD]  ,
					[TAB_SEQ_NBR]
				FROM [arch].[MDS_EVENT_VRTL_CNCTN] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_VRTL_CNCTN] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_VRTL_CNCTN     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_VRTL_CNCTN] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_VRTL_CNCTN] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_VRTL_CNCTN] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_VRTL_CNCTN]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY							TABLE_NAME					COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_WIRED_TRNSPRT_DEV	MDS_EVENT_WIRED_TRNSPRT_DEV	MDS_TRNSPRT_TYPE_ID	LK_MDS_TRNSPRT_TYPE	MDS_TRNSPRT_TYPE_ID
				FK02_MDS_EVENT_WIRED_TRNSPRT_DEV	MDS_EVENT_WIRED_TRNSPRT_DEV	TELCO_ID			LK_TELCO			TELCO_ID
				FK03_MDS_EVENT_WIRED_TRNSPRT_DEV	MDS_EVENT_WIRED_TRNSPRT_DEV	FSA_MDS_EVENT_ID	FSA_MDS_EVENT		FSA_MDS_EVENT_ID
				--------------------------------------------ok------------EVENT------------------------------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_WIRED_TRNSPRT_DEV     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_WIRED_TRNSPRT_DEV] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_WIRED_TRNSPRT_DEV](
					[MDS_EVENT_WIRED_TRNSPRT_DEV_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MDS_TRNSPRT_TYPE_ID]  ,
					[TELCO_ID]  ,
					[PRIM_BKUP_CD]  ,
					[FMS_CKT_ID]  ,
					[PL_NBR]  ,
					[NUA_449_ADR]  ,
					[OLD_CKT_ID]  ,
					[MULTI_LINK_CKT_CD]  ,
					[SPRINT_MNGD_CD]  ,
					[FOC_DT]  ,
					[CREAT_DT]  ,
					[NEW_NUA_449_ADR]  ,
					[NEW_FMS_CKT_ID]  ,
					[TRPT_FTN] )
				SELECT [MDS_EVENT_WIRED_TRNSPRT_DEV_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MDS_TRNSPRT_TYPE_ID]  ,
					[TELCO_ID]  ,
					[PRIM_BKUP_CD]  ,
					[FMS_CKT_ID]  ,
					[PL_NBR]  ,
					[NUA_449_ADR]  ,
					[OLD_CKT_ID]  ,
					[MULTI_LINK_CKT_CD]  ,
					[SPRINT_MNGD_CD]  ,
					[FOC_DT]  ,
					[CREAT_DT]  ,
					[NEW_NUA_449_ADR]  ,
					[NEW_FMS_CKT_ID]  ,
					[TRPT_FTN]	
				FROM [arch].[MDS_EVENT_WIRED_TRNSPRT_DEV] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_WIRED_TRNSPRT_DEV] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_WIRED_TRNSPRT_DEV     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_WIRED_TRNSPRT_DEV] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_WIRED_TRNSPRT_DEV] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_WIRED_TRNSPRT_DEV] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_WIRED_TRNSPRT_DEV]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY						TABLE_NAME				COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_WIRED_DEV_TRPT	MDS_EVENT_WIRED_TRPT	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				FK02_MDS_EVENT_WIRED_TRPT		MDS_EVENT_WIRED_TRPT	MDS_TRPT_TYPE_ID	LK_MDS_TRNSPRT_TYPE	MDS_TRNSPRT_TYPE_ID
				FK03_MDS_EVENT_WIRED_TRPT		MDS_EVENT_WIRED_TRPT	TELCO_ID			LK_TELCO			TELCO_ID
				------------------------------------OK-------------------EVENT----------------------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_WIRED_TRPT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_WIRED_TRPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_WIRED_TRPT](
					[MDS_EVENT_WIRED_TRPT_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MDS_TRPT_TYPE_ID]  ,
					[PRIM_BKUP_CD]  ,
					[BDWD_CHNL_NME]  ,
					[FMS_CKT_ID]  ,
					[PL_NBR]  ,
					[IP_NUA_ADR]  ,
					[OLD_CKT_ID]  ,
					[MULTI_LINK_CKT_CD]  ,
					[SPRINT_MNGD_CD]  ,
					[TELCO_ID]  ,
					[FOC_DT]  ,
					[TAB_SEQ_NBR]  )
				SELECT [MDS_EVENT_WIRED_TRPT_ID]   ,
					[FSA_MDS_EVENT_ID]  ,
					[MDS_TRPT_TYPE_ID]  ,
					[PRIM_BKUP_CD]  ,
					[BDWD_CHNL_NME]  ,
					[FMS_CKT_ID]  ,
					[PL_NBR]  ,
					[IP_NUA_ADR]  ,
					[OLD_CKT_ID]  ,
					[MULTI_LINK_CKT_CD]  ,
					[SPRINT_MNGD_CD]  ,
					[TELCO_ID]  ,
					[FOC_DT]  ,
					[TAB_SEQ_NBR]	
				FROM [arch].[MDS_EVENT_WIRED_TRPT] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_WIRED_TRPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_WIRED_TRPT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_WIRED_TRPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_WIRED_TRPT] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_WIRED_TRPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_WIRED_TRPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY						TABLE_NAME					COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_WRLS_TRNSPRT_DEV	MDS_EVENT_WRLS_TRNSPRT_DEV	FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
				-------------------------------------OK------------------EVENT--------------------------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_WRLS_TRNSPRT_DEV] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_WRLS_TRNSPRT_DEV](
					[FSA_MDS_EVENT_ID]  ,
					[PRIM_BKUP_CD]  ,
					[ESN]  ,
					[CREAT_DT]  )
				SELECT [FSA_MDS_EVENT_ID]  ,
					[PRIM_BKUP_CD]  ,
					[ESN]  ,
					[CREAT_DT]
				FROM [arch].[MDS_EVENT_WRLS_TRNSPRT_DEV] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_WRLS_TRNSPRT_DEV] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_WRLS_TRNSPRT_DEV] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_WRLS_TRNSPRT_DEV] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_WRLS_TRNSPRT_DEV] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_WRLS_TRNSPRT_DEV]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY					TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_MDS_EVENT_WRLS_TRPT	MDS_EVENT_WRLS_TRPT	FSA_MDS_EVENT_ID	FSA_MDS_EVENT_NEW	FSA_MDS_EVENT_ID
				-----------------------------------------------OK-------------------------EVENT----------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_WRLS_TRPT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_EVENT_WRLS_TRPT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_EVENT_WRLS_TRPT](
					   [MDS_EVENT_WRLS_TRPT_ID]
					  ,[FSA_MDS_EVENT_ID]
					  ,[PRIM_BKUP_CD]
					  ,[ESN_MAC_ID]
					  ,[TAB_SEQ_NBR]
					  ,[EVENT_ID]
					  ,[WRLS_TYPE_CD]
					  ,[BILL_ACCT_NME]
					  ,[BILL_ACCT_NBR]
					  ,[ACCT_PIN]
					  ,[SALS_CD]
					  ,[SOC]
					  ,[STATIC_IP_ADR]
					  ,[IMEI]
					  ,[UICC_ID_NBR]
					  ,[PRICE_PLN]
					  ,[ODIE_DEV_NME]
					  ,[CREAT_DT] )
				SELECT [MDS_EVENT_WRLS_TRPT_ID]
					  ,[FSA_MDS_EVENT_ID]
					  ,[PRIM_BKUP_CD]
					  ,[ESN_MAC_ID]
					  ,[TAB_SEQ_NBR]
					  ,[EVENT_ID]
					  ,[WRLS_TYPE_CD]
					  ,[BILL_ACCT_NME]
					  ,[BILL_ACCT_NBR]
					  ,[ACCT_PIN]
					  ,[SALS_CD]
					  ,[SOC]
					  ,[STATIC_IP_ADR]
					  ,[IMEI]
					  ,[UICC_ID_NBR]
					  ,[PRICE_PLN]
					  ,[ODIE_DEV_NME]
					  ,[CREAT_DT]
				FROM [arch].[MDS_EVENT_WRLS_TRPT] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_EVENT_WRLS_TRPT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_EVENT_WRLS_TRPT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_EVENT_WRLS_TRPT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM [arch].[MDS_EVENT_WRLS_TRPT] WHERE [FSA_MDS_EVENT_ID] IN (SELECT  [FSA_MDS_EVENT_ID]  FROM  @FSA_MDS_EVENT_ID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_EVENT_WRLS_TRPT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_EVENT_WRLS_TRPT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_MNGD_ACT	MDS_MNGD_ACT	REQ_ID		ODIE_REQ		REQ_ID
				FK02_MDS_MNGD_ACT	MDS_MNGD_ACT	EVENT_ID	EVENT			EVENT_ID
				------------------------------------------ok-------------EVENT--------ORDER-->>REQ_ID-----------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_MNGD_ACT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_MNGD_ACT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_MNGD_ACT](
					[MDS_MNGD_ACT_ID]   ,
					[REQ_ID] ,
					[MODEL_ID]  ,
					[DEV_ID]  ,
					[SERIAL_NO]  ,
					[RDSN_NBR]  ,
					[FAST_TRK_CD]  ,
					[DSPCH_RDY_CD]  ,
					[RSPN_INFO_DT]  ,
					[SLCTD_CD]  ,
					[CREAT_DT]  ,
					[MANF_ID] ,
					[OPT_OUT_CD]  ,
					[OPT_OUT_REAS_TXT]  ,
					[BUS_JUSTN_TXT]  ,
					[MGT_TXT]  ,
					[ONE_MB_TXT]  ,
					[FRWL_PROD_CD]  ,
					[EVENT_ID] )
				SELECT [MDS_MNGD_ACT_ID]   ,
					[REQ_ID] ,
					[MODEL_ID]  ,
					[DEV_ID]  ,
					[SERIAL_NO]  ,
					[RDSN_NBR]  ,
					[FAST_TRK_CD]  ,
					[DSPCH_RDY_CD]  ,
					[RSPN_INFO_DT]  ,
					[SLCTD_CD]  ,
					[CREAT_DT]  ,
					[MANF_ID] ,
					[OPT_OUT_CD]  ,
					[OPT_OUT_REAS_TXT]  ,
					[BUS_JUSTN_TXT]  ,
					[MGT_TXT]  ,
					[ONE_MB_TXT]  ,
					[FRWL_PROD_CD]  ,
					[EVENT_ID]
				FROM	[COWS].[arch].[MDS_MNGD_ACT] WITH(NOLOCK) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
				UNION
				SELECT [MDS_MNGD_ACT_ID]   ,
					[REQ_ID] ,
					[MODEL_ID]  ,
					[DEV_ID]  ,
					[SERIAL_NO]  ,
					[RDSN_NBR]  ,
					[FAST_TRK_CD]  ,
					[DSPCH_RDY_CD]  ,
					[RSPN_INFO_DT]  ,
					[SLCTD_CD]  ,
					[CREAT_DT]  ,
					[MANF_ID] ,
					[OPT_OUT_CD]  ,
					[OPT_OUT_REAS_TXT]  ,
					[BUS_JUSTN_TXT]  ,
					[MGT_TXT]  ,
					[ONE_MB_TXT]  ,
					[FRWL_PROD_CD]  ,
					[EVENT_ID]
				FROM	[COWS].[arch].[MDS_MNGD_ACT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_MNGD_ACT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_MNGD_ACT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_MNGD_ACT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_MNGD_ACT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_MNGD_ACT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_MNGD_ACT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_MNGD_ACT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_MNGD_ACT] WITH(TABLOCKX) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_MNGD_ACT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_MNGD_ACT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MDS_MNGD_ACT_NEW	MDS_MNGD_ACT_NEW	REQ_ID	    ODIE_REQ	REQ_ID
				FK02_MDS_MNGD_ACT_NEW	MDS_MNGD_ACT_NEW	EVENT_ID	EVENT	    EVENT_ID
				--------------------------------------------------ok---------EVENT-------ORDER-->REQ_ID----------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_MNGD_ACT_NEW     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MDS_MNGD_ACT_NEW] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MDS_MNGD_ACT_NEW](
					[REQ_ID] ,
					[EVENT_ID] ,
					[SLCTD_CD]  ,
					[RDSN_NBR]  ,
					[ODIE_DEV_NME]  ,
					[DEV_MODEL_ID]  ,
					[MANF_ID]  ,
					[FRWL_PROD_CD]  ,
					[INTL_CTRY_CD]  ,
					[PHN_NBR] ,
					[FAST_TRK_CD]  ,
					[OPT_OUT_CD]  ,
					[OPT_OUT_REAS_TXT]  ,
					[BUS_JUSTN_TXT]  ,
					[SRVC_ASSRN_SITE_SUPP_ID]  ,
					[CREAT_DT]  ,
					[RSPN_INFO_DT]  ,
					[MDS_MNGD_ACT_ID]   ,
					[WOOB_CD] )
				SELECT [REQ_ID] ,
					[EVENT_ID] ,
					[SLCTD_CD]  ,
					[RDSN_NBR]  ,
					[ODIE_DEV_NME]  ,
					[DEV_MODEL_ID]  ,
					[MANF_ID]  ,
					[FRWL_PROD_CD]  ,
					[INTL_CTRY_CD]  ,
					[PHN_NBR] ,
					[FAST_TRK_CD]  ,
					[OPT_OUT_CD]  ,
					[OPT_OUT_REAS_TXT]  ,
					[BUS_JUSTN_TXT]  ,
					[SRVC_ASSRN_SITE_SUPP_ID]  ,
					[CREAT_DT]  ,
					[RSPN_INFO_DT]  ,
					[MDS_MNGD_ACT_ID]   ,
					[WOOB_CD]	
				FROM	[COWS].[arch].[MDS_MNGD_ACT_NEW] WITH(NOLOCK) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
				UNION
				SELECT [REQ_ID] ,
					[EVENT_ID] ,
					[SLCTD_CD]  ,
					[RDSN_NBR]  ,
					[ODIE_DEV_NME]  ,
					[DEV_MODEL_ID]  ,
					[MANF_ID]  ,
					[FRWL_PROD_CD]  ,
					[INTL_CTRY_CD]  ,
					[PHN_NBR] ,
					[FAST_TRK_CD]  ,
					[OPT_OUT_CD]  ,
					[OPT_OUT_REAS_TXT]  ,
					[BUS_JUSTN_TXT]  ,
					[SRVC_ASSRN_SITE_SUPP_ID]  ,
					[CREAT_DT]  ,
					[RSPN_INFO_DT]  ,
					[MDS_MNGD_ACT_ID]   ,
					[WOOB_CD]
				FROM	[COWS].[arch].[MDS_MNGD_ACT_NEW] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MDS_MNGD_ACT_NEW] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_MNGD_ACT_NEW]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				SET IDENTITY_INSERT  [COWS].[dbo]. MDS_MNGD_ACT_NEW     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_MNGD_ACT_NEW] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_MNGD_ACT_NEW] WITH(TABLOCKX) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_MNGD_ACT_NEW] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MDS_MNGD_ACT_NEW] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MDS_MNGD_ACT_NEW] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MDS_MNGD_ACT_NEW] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDS_MNGD_ACT_NEW]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			END
			
			IF(@Mood=0)  /*  ORDR Only  and if MPLS_ACCS  has anything to do with MPLS_EVENT  the MPLS EVENT*/
			BEGIN
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_SIP_TRNK_GRP	SIP_TRNK_GRP	ORDR_ID	FSA_ORDR	ORDR_ID
				--------------------------------------------ok--------------ORDER---------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. SIP_TRNK_GRP     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SIP_TRNK_GRP] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[SIP_TRNK_GRP](
					[SIP_TRNK_GRP_ID]   ,
					[ORDR_ID]  ,
					[CMPNT_ID]  ,
					[IS_INSTL_CD]  ,
					[SIP_TRNK_QTY]  ,
					[NW_USER_ADR]  )
				SELECT  [SIP_TRNK_GRP_ID]   ,
					[ORDR_ID]  ,
					[CMPNT_ID]  ,
					[IS_INSTL_CD]  ,
					[SIP_TRNK_QTY]  ,
					[NW_USER_ADR]
				FROM	[COWS].[arch].[SIP_TRNK_GRP] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SIP_TRNK_GRP] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. SIP_TRNK_GRP     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SIP_TRNK_GRP] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[SIP_TRNK_GRP] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SIP_TRNK_GRP] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SIP_TRNK_GRP]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				---------This [dbo].[MPLS_ACCS]  NEVER USED ignore this table
				
				--/*
				--FOREIGN_KEY		TABLE_NAME	COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				--FK01_MPLS_ACCS	MPLS_ACCS	SIP_TRNK_GRP_ID	SIP_TRNK_GRP	SIP_TRNK_GRP_ID
				-------------------------------------------ok------------------ORDER-->>[CKT_ID]------------------------------
				--*/
				--Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MPLS_ACCS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				--INSERT INTO [dbo].[MPLS_ACCS](
				--	[MPLS_ACCS_ID]   ,
				--	[SIP_TRNK_GRP_ID]  ,
				--	[CKT_ID]  ,
				--	[NW_USER_ADR]  )
				--SELECT [MPLS_ACCS_ID]   ,
				--	[SIP_TRNK_GRP_ID]  ,
				--	[CKT_ID]  ,
				--	[NW_USER_ADR]	
				--FROM	[COWS].[arch].[MPLS_ACCS] WITH(NOLOCK)	WHERE CKT_ID	IN	(SELECT  CKT_ID  FROM	@CKTID)
				--Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MPLS_ACCS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				--Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MPLS_ACCS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				--DELETE  FROM	[COWS].[arch].[MPLS_ACCS] WITH(TABLOCKX)	WHERE CKT_ID	IN	(SELECT  CKT_ID  FROM	@CKTID)
				--Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MPLS_ACCS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MPLS_ACCS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			END
			IF(@Mood=3)
			BEGIN
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MPLS_EVENT	MPLS_EVENT	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID
				FK02_MPLS_EVENT	MPLS_EVENT	SALS_USER_ID		LK_USER	USER_ID
				FK03_MPLS_EVENT	MPLS_EVENT	CREAT_BY_USER_ID	LK_USER	USER_ID
				FK04_MPLS_EVENT	MPLS_EVENT	REQOR_USER_ID		LK_USER	USER_ID
				FK05_MPLS_EVENT	MPLS_EVENT	MODFD_BY_USER_ID	LK_USER	USER_ID
				FK06_MPLS_EVENT	MPLS_EVENT	WRKFLW_STUS_ID		LK_WRKFLW_STUS	WRKFLW_STUS_ID
				FK07_MPLS_EVENT	MPLS_EVENT	EVENT_STUS_ID		LK_EVENT_STUS	EVENT_STUS_ID
				FK08_MPLS_EVENT	MPLS_EVENT	EVENT_ID			EVENT	EVENT_ID
				FK09_MPLS_EVENT	MPLS_EVENT	IP_VER_ID			LK_IP_VER	IP_VER_ID
				FK10_MPLS_EVENT	MPLS_EVENT	ESCL_REAS_ID		LK_ESCL_REAS	ESCL_REAS_ID
				FK11_MPLS_EVENT	MPLS_EVENT	MPLS_CXR_PTNR_ID	LK_VNDR	VNDR_CD
				FK12_MPLS_EVENT	MPLS_EVENT	ACTY_LOCALE_ID		LK_ACTY_LOCALE	ACTY_LOCALE_ID
				FK13_MPLS_EVENT	MPLS_EVENT	WHLSL_PTNR_ID		LK_WHLSL_PTNR	WHLSL_PTNR_ID
				FK14_MPLS_EVENT	MPLS_EVENT	MPLS_EVENT_TYPE_ID	LK_MPLS_EVENT_TYPE	MPLS_EVENT_TYPE_ID
				FK15_MPLS_EVENT	MPLS_EVENT	MPLS_MGRTN_TYPE_ID	LK_MPLS_MGRTN_TYPE	MPLS_MGRTN_TYPE_ID
				FK17_MPLS_EVENT	MPLS_EVENT	VPN_PLTFRM_TYPE_ID	LK_VPN_PLTFRM_TYPE	VPN_PLTFRM_TYPE_ID
				FK18_MPLS_EVENT	MPLS_EVENT	MULTI_VRF_REQ_ID	LK_MULTI_VRF_REQ	MULTI_VRF_REQ_ID

				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MPLS_EVENT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MPLS_EVENT](
					[EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[FTN]  ,
					[CHARS_ID] ,
					[H1]  ,
					[H6]  ,
					[REQOR_USER_ID]  ,
					[SALS_USER_ID]  ,
					[PUB_EMAIL_CC_TXT]  ,
					[CMPLTD_EMAIL_CC_TXT]  ,
					[DOC_LINK_TXT]  ,
					[DD_APRVL_NBR]  ,
					[NDD_UPDTD_CD]  ,
					[DES_CMNT_TXT]  ,
					[MPLS_EVENT_TYPE_ID]  ,
					[VPN_PLTFRM_TYPE_ID]  ,
					[IP_VER_ID]  ,
					[ACTY_LOCALE_ID]  ,
					[MULTI_VRF_REQ_ID]  ,
					[VRF_NME]  ,
					[MDS_MNGD_CD]  ,
					[ADD_E2E_MONTRG_CD]  ,
					[MDS_VRF_NME]  ,
					[MDS_IP_ADR]  ,
					[MDS_DLCI]  ,
					[MDS_STC_RTE_DES]  ,
					[ON_NET_MONTRG_CD]  ,
					[WHLSL_PTNR_CD]  ,
					[WHLSL_PTNR_ID]  ,
					[NNI_DSGN_DOC_NME]  ,
					[CXR_PTNR_CD]  ,
					[MPLS_CXR_PTNR_ID]  ,
					[OFF_NET_MONTRG_CD]  ,
					[MGRTN_CD]  ,
					[MPLS_MGRTN_TYPE_ID]  ,
					[ESCL_CD]  ,
					[PRIM_REQ_DT] ,
					[SCNDY_REQ_DT] ,
					[ESCL_REAS_ID]  ,
					[STRT_TMST]  ,
					[EXTRA_DRTN_TME_AMT]  ,
					[END_TMST]  ,
					[WRKFLW_STUS_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[CNFRC_BRDG_NBR]  ,
					[CNFRC_PIN_NBR] ,
					[EVENT_DRTN_IN_MIN_QTY]  ,
					[SOWS_EVENT_ID] ,
					[RELTD_CMPS_NCR_CD]  ,
					[RELTD_CMPS_NCR_NME]  ,
					[CUST_NME]  ,
					[CUST_CNTCT_NME]  ,
					[CUST_CNTCT_PHN_NBR] ,
					[CUST_EMAIL_ADR] ,
					[CUST_CNTCT_CELL_PHN_NBR] ,
					[CUST_CNTCT_PGR_NBR] ,
					[CUST_CNTCT_PGR_PIN_NBR] ,
					[EVENT_TITLE_TXT],
					[EVENT_DES] )
					
				SELECT [EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[FTN]  ,
					[CHARS_ID] ,
					[H1]  ,
					[H6]  ,
					[REQOR_USER_ID]  ,
					[SALS_USER_ID]  ,
					[PUB_EMAIL_CC_TXT]  ,
					[CMPLTD_EMAIL_CC_TXT]  ,
					[DOC_LINK_TXT]  ,
					[DD_APRVL_NBR]  ,
					[NDD_UPDTD_CD]  ,
					[DES_CMNT_TXT]  ,
					[MPLS_EVENT_TYPE_ID]  ,
					[VPN_PLTFRM_TYPE_ID]  ,
					[IP_VER_ID]  ,
					[ACTY_LOCALE_ID]  ,
					[MULTI_VRF_REQ_ID]  ,
					[VRF_NME]  ,
					[MDS_MNGD_CD]  ,
					[ADD_E2E_MONTRG_CD]  ,
					[MDS_VRF_NME]  ,
					[MDS_IP_ADR]  ,
					[MDS_DLCI]  ,
					[MDS_STC_RTE_DES]  ,
					[ON_NET_MONTRG_CD]  ,
					[WHLSL_PTNR_CD]  ,
					[WHLSL_PTNR_ID]  ,
					[NNI_DSGN_DOC_NME]  ,
					[CXR_PTNR_CD]  ,
					[MPLS_CXR_PTNR_ID]  ,
					[OFF_NET_MONTRG_CD]  ,
					[MGRTN_CD]  ,
					[MPLS_MGRTN_TYPE_ID]  ,
					[ESCL_CD]  ,
					[PRIM_REQ_DT] ,
					[SCNDY_REQ_DT] ,
					[ESCL_REAS_ID]  ,
					[STRT_TMST]  ,
					[EXTRA_DRTN_TME_AMT]  ,
					[END_TMST]  ,
					[WRKFLW_STUS_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[CNFRC_BRDG_NBR]  ,
					[CNFRC_PIN_NBR] ,
					[EVENT_DRTN_IN_MIN_QTY]  ,
					[SOWS_EVENT_ID] ,
					[RELTD_CMPS_NCR_CD]  ,
					[RELTD_CMPS_NCR_NME]  ,
					[CUST_NME]  ,
					[CUST_CNTCT_NME]  ,
					[CUST_CNTCT_PHN_NBR] ,
					[CUST_EMAIL_ADR] ,
					[CUST_CNTCT_CELL_PHN_NBR] ,
					[CUST_CNTCT_PGR_NBR] ,
					[CUST_CNTCT_PGR_PIN_NBR] ,
					[EVENT_TITLE_TXT],
					[EVENT_DES]
				FROM	[COWS].[arch].[MPLS_EVENT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MPLS_EVENT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MPLS_EVENT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MPLS_EVENT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MPLS_EVENT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MPLS_EVENT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY					TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MPLS_EVENT_ACCS_TAG	MPLS_EVENT_ACCS_TAG	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID
				FK04_MPLS_EVENT_ACCS_TAG	MPLS_EVENT_ACCS_TAG	EVENT_ID			MPLS_EVENT	EVENT_ID
				FK05_MPLS_EVENT_ACCS_TAG	MPLS_EVENT_ACCS_TAG	CTRY_CD				LK_CTRY	CTRY_CD
				FK06_MPLS_EVENT_ACCS_TAG	MPLS_EVENT_ACCS_TAG	MNS_OFFRG_ROUTR_ID	LK_MNS_OFFRG_ROUTR	MNS_OFFRG_ROUTR_ID
				FK07_MPLS_EVENT_ACCS_TAG	MPLS_EVENT_ACCS_TAG	MNS_PRFMC_RPT_ID	LK_MNS_PRFMC_RPT	MNS_PRFMC_RPT_ID
				FK08_MPLS_EVENT_ACCS_TAG	MPLS_EVENT_ACCS_TAG	MNS_ROUTG_TYPE_ID	LK_MNS_ROUTG_TYPE	MNS_ROUTG_TYPE_ID
				FK09_MPLS_EVENT_ACCS_TAG	MPLS_EVENT_ACCS_TAG	MPLS_ACCS_BDWD_ID	LK_MPLS_ACCS_BDWD	MPLS_ACCS_BDWD_ID
				-----------------------------------------OK-----------------------------EVENT-----------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. MPLS_EVENT_ACCS_TAG     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MPLS_EVENT_ACCS_TAG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MPLS_EVENT_ACCS_TAG](
					[MPLS_EVENT_ACCS_TAG_ID]   ,
					[EVENT_ID]  ,
					[LOC_CTY_NME]  ,
					[LOC_STT_NME] ,
					[VAS_SOL_NBR]  ,
					[TRS_NET_449_ADR]  ,
					[MPLS_ACCS_BDWD_ID]  ,
					[MNS_SIP_ADR]  ,
					[MNS_OFFRG_ROUTR_ID]  ,
					[MNS_ROUTG_TYPE_ID]  ,
					[MNS_PRFMC_RPT_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_DT]  ,
					[CTRY_CD] ,
					[PL_DAL_CKT_NBR]  ,
					[TRNSPRT_OE_FTN_NBR]  )
				SELECT [MPLS_EVENT_ACCS_TAG_ID]   ,
					[EVENT_ID]  ,
					[LOC_CTY_NME]  ,
					[LOC_STT_NME] ,
					[VAS_SOL_NBR]  ,
					[TRS_NET_449_ADR]  ,
					[MPLS_ACCS_BDWD_ID]  ,
					[MNS_SIP_ADR]  ,
					[MNS_OFFRG_ROUTR_ID]  ,
					[MNS_ROUTG_TYPE_ID]  ,
					[MNS_PRFMC_RPT_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_DT]  ,
					[CTRY_CD] ,
					[PL_DAL_CKT_NBR]  ,
					[TRNSPRT_OE_FTN_NBR]	
				FROM	[COWS].[arch].[MPLS_EVENT_ACCS_TAG] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MPLS_EVENT_ACCS_TAG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. MPLS_EVENT_ACCS_TAG     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MPLS_EVENT_ACCS_TAG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MPLS_EVENT_ACCS_TAG] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MPLS_EVENT_ACCS_TAG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MPLS_EVENT_ACCS_TAG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MPLS_EVENT_ACTY_TYPE	MPLS_EVENT_ACTY_TYPE	EVENT_ID	MPLS_EVENT	EVENT_ID
				FK02_MPLS_EVENT_ACTY_TYPE	MPLS_EVENT_ACTY_TYPE	MPLS_ACTY_TYPE_ID	LK_MPLS_ACTY_TYPE	MPLS_ACTY_TYPE_ID
				---------------------------OK------------------------EVENT------------------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MPLS_EVENT_ACTY_TYPE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MPLS_EVENT_ACTY_TYPE](
					[EVENT_ID]  ,
					[MPLS_ACTY_TYPE_ID]  ,
					[CREAT_DT] )
				SELECT  [EVENT_ID]  ,
					[MPLS_ACTY_TYPE_ID]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[MPLS_EVENT_ACTY_TYPE] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MPLS_EVENT_ACTY_TYPE] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MPLS_EVENT_ACTY_TYPE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MPLS_EVENT_ACTY_TYPE] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MPLS_EVENT_ACTY_TYPE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MPLS_EVENT_ACTY_TYPE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY					TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_MPLS_EVENT_VAS_TYPE	MPLS_EVENT_VAS_TYPE	EVENT_ID		MPLS_EVENT		EVENT_ID
				FK02_MPLS_EVENT_VAS_TYPE	MPLS_EVENT_VAS_TYPE	VAS_TYPE_ID		LK_VAS_TYPE		VAS_TYPE_ID
				------------------------------------OK--------------------EVENT---------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[MPLS_EVENT_VAS_TYPE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[MPLS_EVENT_VAS_TYPE](
					[EVENT_ID]  ,
					[VAS_TYPE_ID]  ,
					[CREAT_DT]  )
				SELECT [EVENT_ID]  ,
					[VAS_TYPE_ID]  ,
					[CREAT_DT]	
				FROM	[COWS].[arch].[MPLS_EVENT_VAS_TYPE] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[MPLS_EVENT_VAS_TYPE] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[MPLS_EVENT_VAS_TYPE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[MPLS_EVENT_VAS_TYPE] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[MPLS_EVENT_VAS_TYPE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MPLS_EVENT_VAS_TYPE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				---------------------------------
				*/
				/*
				PRINT 'Starting to execute [arch].[MPLS_SAVE_UNMASKED_CUST_DATA] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[MPLS_SAVE_UNMASKED_CUST_DATA](
					[ROWID]  ,
					[H1]  ,
					[CUSTOMERNAME]  ,
					[CONTACTNAME]  ,
					[CUSTOMEREMAIL]  ,
					[CREATED_DT]  
				) 
				SELECT [ROWID]  ,
					[H1]  ,
					[CUSTOMERNAME]  ,
					[CONTACTNAME]  ,
					[CUSTOMEREMAIL]  ,
					[CREATED_DT]
				FROM COWS.dbo.[MPLS_SAVE_UNMASKED_CUST_DATA] WITH (NOLOCK) Where [CREATED_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [arch].[MPLS_SAVE_UNMASKED_CUST_DATA] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[MPLS_SAVE_UNMASKED_CUST_DATA] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM COWS.dbo.[MPLS_SAVE_UNMASKED_CUST_DATA] WITH (ROWLOCK) Where [CREATED_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [dbo].[MPLS_SAVE_UNMASKED_CUST_DATA] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MPLS_SAVE_UNMASKED_CUST_DATA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
			
			END
			/*
			------------------------------------
			*/
			/*
			GO
			CREATE TABLE [arch].[MultiThreadedTest](
				[ID]   ,
				[ThreadId] ,
				[MyValue] ,
			PRIMARY KEY CLUSTERED 
			(
				[ID] ASC
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			) ON [PRIMARY]
			GO
			*/
			
			IF(@Mood=0 )  /*  ORDR ONLY */
			BEGIN
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_NCCO_ORDR	NCCO_ORDR	ORDR_ID				ORDR			ORDR_ID
				FK02_NCCO_ORDR	NCCO_ORDR	CTRY_CD				LK_CTRY			CTRY_CD
				FK03_NCCO_ORDR	NCCO_ORDR	PROD_TYPE_ID		LK_PROD_TYPE	PROD_TYPE_ID
				FK04_NCCO_ORDR	NCCO_ORDR	ORDR_TYPE_ID		LK_ORDR_TYPE	ORDR_TYPE_ID
				FK05_NCCO_ORDR	NCCO_ORDR	CREAT_BY_USER_ID	LK_USER			USER_ID
				FK06_NCCO_ORDR	NCCO_ORDR	VNDR_CD				LK_VNDR			VNDR_CD
				-------------------------------------------ok----------------------ORDER-------------------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[NCCO_ORDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[NCCO_ORDR](
					[ORDR_ID]  ,
					[SITE_CITY_NME]  ,
					[CTRY_CD] ,
					[H5_ACCT_NBR] ,
					[SOL_NME]  ,
					[OE_NME]  ,
					[SOTS_NBR]  ,
					[BILL_CYC_NBR]  ,
					[PROD_TYPE_ID]  ,
					[ORDR_TYPE_ID]  ,
					[CWD_DT] ,
					[CCS_DT] ,
					[EMAIL_ADR] ,
					[CMNT_TXT]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[VNDR_CD]  ,
					[PL_NBR] ,
					[ORDR_BY_LASSIE_CD]  ,
					[CUST_NME] )
				SELECT 	[ORDR_ID]  ,
					[SITE_CITY_NME]  ,
					[CTRY_CD] ,
					[H5_ACCT_NBR] ,
					[SOL_NME]  ,
					[OE_NME]  ,
					[SOTS_NBR]  ,
					[BILL_CYC_NBR]  ,
					[PROD_TYPE_ID]  ,
					[ORDR_TYPE_ID]  ,
					[CWD_DT] ,
					[CCS_DT] ,
					[EMAIL_ADR] ,
					[CMNT_TXT]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[VNDR_CD]  ,
					[PL_NBR] ,
					[ORDR_BY_LASSIE_CD]  ,
					[CUST_NME]
				FROM	[COWS].[arch].[NCCO_ORDR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[NCCO_ORDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[NCCO_ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[NCCO_ORDR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[NCCO_ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[NCCO_ORDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			END
			IF(@Mood=2)  /* NGVN EVENT ONLY */
			BEGIN
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_NGVN_EVENT	NGVN_EVENT	WRKFLW_STUS_ID		LK_WRKFLW_STUS		WRKFLW_STUS_ID
				FK02_NGVN_EVENT	NGVN_EVENT	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID
				FK03_NGVN_EVENT	NGVN_EVENT	EVENT_STUS_ID		LK_EVENT_STUS		EVENT_STUS_ID
				FK04_NGVN_EVENT	NGVN_EVENT	CREAT_BY_USER_ID	LK_USER				USER_ID
				FK05_NGVN_EVENT	NGVN_EVENT	SALS_USER_ID		LK_USER				USER_ID
				FK06_NGVN_EVENT	NGVN_EVENT	ESCL_REAS_ID		LK_ESCL_REAS		ESCL_REAS_ID
				FK07_NGVN_EVENT	NGVN_EVENT	MODFD_BY_USER_ID	LK_USER				USER_ID
				FK08_NGVN_EVENT	NGVN_EVENT	REQOR_USER_ID		LK_USER				USER_ID
				FK09_NGVN_EVENT	NGVN_EVENT	EVENT_ID			EVENT				EVENT_ID
				FK10_NGVN_EVENT	NGVN_EVENT	NGVN_PROD_TYPE_ID	LK_NGVN_PROD_TYPE	NGVN_PROD_TYPE_ID
				FK12_NGVN_EVENT	NGVN_EVENT	IP_VER_ID			LK_IP_VER			IP_VER_ID
				------------------------------------OK--------------------------EVENT-----------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[NGVN_EVENT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[NGVN_EVENT](
					[EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[NGVN_PROD_TYPE_ID]  ,
					[FTN]  ,
					[CHARS_ID] ,
					[H1]  ,
					[H6]  ,
					[REQOR_USER_ID] ,
					[SALS_USER_ID] ,
					[PUB_EMAIL_CC_TXT]  ,
					[CMPLTD_EMAIL_CC_TXT]  ,
					[DSGN_CMNT_TXT]  ,
					[GSR_CFGRN_DES]  ,
					[CABL_ADD_DUE_DT]  ,
					[CABL_PRCOL_TYPE_NME]  ,
					[SBC_PAIR_DES]  ,
					[ESCL_CD]  ,
					[PRIM_REQ_DT] ,
					[SCNDY_REQ_DT] ,
					[ESCL_REAS_ID]  ,
					[STRT_TMST]  ,
					[EXTRA_DRTN_TME_AMT]  ,
					[END_TMST]  ,
					[WRKFLW_STUS_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[IP_VER_ID]  ,
					[CNFRC_BRDG_NBR]  ,
					[CNFRC_PIN_NBR] ,
					[EVENT_DRTN_IN_MIN_QTY]  ,
					[SOWS_EVENT_ID] ,
					[RELTD_CMPS_NCR_CD]  ,
					[RELTD_CMPS_NCR_NME]  ,
					[CUST_NME]  ,
					[CUST_CNTCT_NME]  ,
					[CUST_CNTCT_PHN_NBR] ,
					[CUST_EMAIL_ADR] ,
					[CUST_CNTCT_CELL_PHN_NBR] ,
					[CUST_CNTCT_PGR_NBR] ,
					[CUST_CNTCT_PGR_PIN_NBR] ,
					[EVENT_TITLE_TXT],
					[EVENT_DES] )
				SELECT  [EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[NGVN_PROD_TYPE_ID]  ,
					[FTN]  ,
					[CHARS_ID] ,
					[H1]  ,
					[H6]  ,
					[REQOR_USER_ID] ,
					[SALS_USER_ID] ,
					[PUB_EMAIL_CC_TXT]  ,
					[CMPLTD_EMAIL_CC_TXT]  ,
					[DSGN_CMNT_TXT]  ,
					[GSR_CFGRN_DES]  ,
					[CABL_ADD_DUE_DT]  ,
					[CABL_PRCOL_TYPE_NME]  ,
					[SBC_PAIR_DES]  ,
					[ESCL_CD]  ,
					[PRIM_REQ_DT] ,
					[SCNDY_REQ_DT] ,
					[ESCL_REAS_ID]  ,
					[STRT_TMST]  ,
					[EXTRA_DRTN_TME_AMT]  ,
					[END_TMST]  ,
					[WRKFLW_STUS_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[IP_VER_ID]  ,
					[CNFRC_BRDG_NBR]  ,
					[CNFRC_PIN_NBR] ,
					[EVENT_DRTN_IN_MIN_QTY]  ,
					[SOWS_EVENT_ID] ,
					[RELTD_CMPS_NCR_CD]  ,
					[RELTD_CMPS_NCR_NME]  ,
					[CUST_NME]  ,
					[CUST_CNTCT_NME]  ,
					[CUST_CNTCT_PHN_NBR] ,
					[CUST_EMAIL_ADR] ,
					[CUST_CNTCT_CELL_PHN_NBR] ,
					[CUST_CNTCT_PGR_NBR] ,
					[CUST_CNTCT_PGR_PIN_NBR] ,
					[EVENT_TITLE_TXT],
					[EVENT_DES]	
				FROM	[COWS].[arch].[NGVN_EVENT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )	
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[NGVN_EVENT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[NGVN_EVENT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[NGVN_EVENT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[NGVN_EVENT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[NGVN_EVENT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY					TABLE_NAME				COLUMN_NAME		REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_NGVN_EVENT_CKT_ID_NUA	NGVN_EVENT_CKT_ID_NUA	EVENT_ID		NGVN_EVENT			EVENT_ID
				------------------------------------OK---------------------EVENT-------------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[NGVN_EVENT_CKT_ID_NUA] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[NGVN_EVENT_CKT_ID_NUA](
					[EVENT_ID]  ,
					[CKT_ID_NUA]  ,
					[CREAT_DT]  )
				SELECT  [EVENT_ID]  ,
					[CKT_ID_NUA]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[NGVN_EVENT_CKT_ID_NUA] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[NGVN_EVENT_CKT_ID_NUA] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[NGVN_EVENT_CKT_ID_NUA] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[NGVN_EVENT_CKT_ID_NUA] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[NGVN_EVENT_CKT_ID_NUA] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[NGVN_EVENT_CKT_ID_NUA]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_NGVN_EVENT_SIP_TRNK	NGVN_EVENT_SIP_TRNK	EVENT_ID	NGVN_EVENT	EVENT_ID
				-----------------------------------------OK----------------------EVENT---------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[NGVN_EVENT_SIP_TRNK] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[NGVN_EVENT_SIP_TRNK](
					[EVENT_ID]  ,
					[SIP_TRNK_NME]  ,
					[CREAT_DT]  )
				SELECT  [EVENT_ID]  ,
					[SIP_TRNK_NME]  ,
					[CREAT_DT]	
				FROM	[COWS].[arch].[NGVN_EVENT_SIP_TRNK] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[NGVN_EVENT_SIP_TRNK] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[NGVN_EVENT_SIP_TRNK] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[NGVN_EVENT_SIP_TRNK] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[NGVN_EVENT_SIP_TRNK] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[NGVN_EVENT_SIP_TRNK]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			END
			IF(@Mood=0 )   /* ORDR ONLY */  
			BEGIN
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_NRM_CKT	NRM_CKT	REC_STUS_ID	LK_REC_STUS	REC_STUS_ID
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. NRM_CKT     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[NRM_CKT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[NRM_CKT](
					[NRM_CKT_ID]   ,
					[SRVC_INSTC_OBJ_ID]  ,
					[SRVC_INSTC_NME]  ,
					[FTN]  ,
					[CKT_OBJ_ID]  ,
					[CKT_NME]  ,
					[FMS_CKT_ID]  ,
					[NUA_ADR]  ,
					[PLN_NME]  ,
					[PLN_SEQ_NBR]  ,
					[TOC_ADR]  ,
					[PORT_ASMT_DES]  ,
					[TOC_NME]  ,
					[FMS_SITE_ID]  ,
					[CREAT_DT]  ,
					[TOC_ADR_1]  ,
					[TOC_ADR_2]  ,
					[TOC_CTY_NME]  ,
					[TOC_DMSTC_STT_NME]  ,
					[TOC_ZIP_CD]  ,
					[TOC_CTRY_CD]  ,
					[TOC_CTRY_NME]  ,
					[REC_STUS_ID]  ,
					[MODFD_DT]  )
					
				SELECT 	DISTINCT 
					[NRM_CKT_ID]   ,
					[SRVC_INSTC_OBJ_ID]  ,
					[SRVC_INSTC_NME]  ,
					A.[FTN]  ,
					[CKT_OBJ_ID]  ,
					[CKT_NME]  ,
					[FMS_CKT_ID]  ,
					[NUA_ADR]  ,
					[PLN_NME]  ,
					[PLN_SEQ_NBR]  ,
					[TOC_ADR]  ,
					[PORT_ASMT_DES]  ,
					[TOC_NME]  ,
					[FMS_SITE_ID]  ,
					[CREAT_DT]  ,
					[TOC_ADR_1]  ,
					[TOC_ADR_2]  ,
					[TOC_CTY_NME]  ,
					[TOC_DMSTC_STT_NME]  ,
					[TOC_ZIP_CD]  ,
					[TOC_CTRY_CD]  ,
					[TOC_CTRY_NME]  ,
					[REC_STUS_ID]  ,
					[MODFD_DT]
				FROM    COWS.arch.[NRM_CKT] A WITH (NOLOCK) INNER JOIN @FTN B  ON A.[FTN]=B.[FTN]	
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[NRM_CKT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. NRM_CKT     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[NRM_CKT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE A FROM    COWS.arch.[NRM_CKT] A WITH(TABLOCKX) INNER JOIN @FTN B  ON A.[FTN]=B.[FTN]
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[NRM_CKT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[NRM_CKT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_NRM_SRVC_INSTC	NRM_SRVC_INSTC	REC_STUS_ID	LK_STUS	STUS_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. NRM_SRVC_INSTC     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[NRM_SRVC_INSTC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[NRM_SRVC_INSTC](
					[NRM_SRVC_INSTC_ID]   ,
					[SRVC_INSTC_OBJ_ID]  ,
					[SRVC_INSTC_NME]  ,
					[LOOP_ACPT_DT] ,
					[ORDR_STUS_DES]  ,
					[DSCNCT_DT] ,
					[ACTV_DT] ,
					[ORDR_CMNT_TXT]  ,
					[MDS_CD]  ,
					[H5_H6_ID]  ,
					[FTN]  ,
					[NUA_ADR]  ,
					[MODFD_DT]  ,
					[CREAT_DT]  ,
					[BILL_CLEAR_DT] ,
					[REC_STUS_ID]  )
				SELECT  DISTINCT  [NRM_SRVC_INSTC_ID]   ,
					[SRVC_INSTC_OBJ_ID]  ,
					[SRVC_INSTC_NME]  ,
					[LOOP_ACPT_DT] ,
					[ORDR_STUS_DES]  ,
					[DSCNCT_DT] ,
					[ACTV_DT] ,
					[ORDR_CMNT_TXT]  ,
					[MDS_CD]  ,
					[H5_H6_ID]  ,
					A.[FTN]  ,
					[NUA_ADR]  ,
					[MODFD_DT]  ,
					[CREAT_DT]  ,
					[BILL_CLEAR_DT] ,
					[REC_STUS_ID] 	
				FROM    COWS.arch.[NRM_SRVC_INSTC] A WITH (NOLOCK) INNER JOIN @FTN B  ON A.[FTN]=B.[FTN]
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[NRM_SRVC_INSTC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. NRM_SRVC_INSTC     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[NRM_SRVC_INSTC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  A FROM    COWS.arch.[NRM_SRVC_INSTC]  A WITH(TABLOCKX) INNER JOIN @FTN B  ON A.[FTN]=B.[FTN]
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[NRM_SRVC_INSTC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[NRM_SRVC_INSTC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_NRM_VNDR	NRM_VNDR	REC_STUS_ID	LK_REC_STUS	REC_STUS_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. NRM_VNDR     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[NRM_SRVC_INSTC] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[NRM_VNDR](
					[NMS_VNDR_ID]   ,
					[SRVC_INSTC_OBJ_ID]  ,
					[FTN]  ,
					[VNDR_NME]  ,
					[VNDR_TYPE_ID]  ,
					[LEC_ID]  ,
					[VNDR_CKT_ID]  ,
					[SEG_TYPE_ID]  ,
					[H6_H5_ID]  ,
					[CREAT_DT]  ,
					[REC_STUS_ID]  ,
					[MODFD_DT] )
				SELECT  DISTINCT [NMS_VNDR_ID]   ,
					[SRVC_INSTC_OBJ_ID]  ,
					A.[FTN]  ,
					[VNDR_NME]  ,
					[VNDR_TYPE_ID]  ,
					[LEC_ID]  ,
					[VNDR_CKT_ID]  ,
					[SEG_TYPE_ID]  ,
					[H6_H5_ID]  ,
					[CREAT_DT]  ,
					[REC_STUS_ID]  ,
					[MODFD_DT] 	
				FROM    COWS.arch.[NRM_VNDR] A WITH (NOLOCK) INNER JOIN @FTN B  ON A.[FTN]=B.[FTN]
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[NRM_SRVC_INSTC] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. NRM_VNDR     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[NRM_SRVC_INSTC] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  A FROM    COWS.arch.[NRM_SRVC_INSTC]  A WITH(TABLOCKX) INNER JOIN @FTN B  ON A.[FTN]=B.[FTN]
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[NRM_SRVC_INSTC] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[NRM_VNDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			END
			------------------------------ODIE_  Tables are for MDS Events only ------------------------------------------------
			/*
				jagan  02252015
				it is mainly used by mds event and since some mds events r related to orders, we insert ordr_id n event_id in thr
				i would say it is only mds event, since mdsevent has ordr relation, we put ordrid n eventid in thr

				u might see som records with ordr_id but no event_id, tht is because before the user creates a event, he sends a 
				request to odie from within mds event form. so we have only ordr_id populated for tht request and eventid is null 
				becasue mds event is not fully created yet. u can leave tht table too for reinstatin


			*/
			--IF(@Mood=0 OR @Mood=1 OR @Mood=2 OR @Mood=3 OR @Mood=4 OR @Mood=5 OR @Mood=6)
			--BEGIN
			--	/*
			--	FOREIGN_KEY			TABLE_NAME		COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			--	FK01_ODIE_DISC_REQ	ODIE_DISC_REQ	REQ_ID		ODIE_REQ		REQ_ID
			--	FK02_ODIE_DISC_REQ	ODIE_DISC_REQ	STUS_ID		LK_STUS			STUS_ID
			--	----------------------------------------------ok----------ORDER-->>REQ_ID--------pink asshole
			--	*/
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ODIE_DISC_REQ] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	INSERT INTO [dbo].[ODIE_DISC_REQ](
			--		[ODIE_DISC_REQ_ID]   ,
			--		[REQ_ID] ,
			--		[DEV_NME]  ,
			--		[H5_H6_CUST_ID] ,
			--		[VNDR_NME]  ,
			--		[MODEL_NME]  ,
			--		[REDSGN_NBR]  ,
			--		[SERIAL_NBR]  ,
			--		[STUS_ID],
			--		[SITE_ID] ,
			--		[CTRCT_NBR] )
			--	SELECT 	[ODIE_DISC_REQ_ID]   ,
			--		[REQ_ID] ,
			--		[DEV_NME]  ,
			--		[H5_H6_CUST_ID] ,
			--		[VNDR_NME]  ,
			--		[MODEL_NME]  ,
			--		[REDSGN_NBR]  ,
			--		[SERIAL_NBR]  ,
			--		[STUS_ID],
			--		[SITE_ID] ,
			--		[CTRCT_NBR]
			--	FROM	[COWS].[arch].[ODIE_DISC_REQ] WITH(NOLOCK) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ODIE_DISC_REQ] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ODIE_DISC_REQ] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	DELETE  FROM	[COWS].[arch].[ODIE_DISC_REQ] WITH(TABLOCKX) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ODIE_DISC_REQ] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ODIE_DISC_REQ]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			--	/*
			--	FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			--	FK01_ODIE_RSPN	ODIE_RSPN	REQ_ID	ODIE_REQ	REQ_ID
			--	------------------------------------ok-----------------------ORDER-->>[REQ_ID]-----------------
			--	*/
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ODIE_RSPN] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	INSERT INTO [dbo].[ODIE_RSPN](
			--		[RSPN_ID]   ,
			--		[REQ_ID]  ,
			--		[RSPN_DT]  ,
			--		[CUST_TEAM_PDL]  ,
			--		[MNSPM_ID]  ,
			--		[RSPN_ERROR_TXT]  ,
			--		[CREAT_DT]  ,
			--		[ACK_CD]  ,
			--		[SOWS_FOLDR_PATH_NME]  ,
			--		[DOC_URL_ADR]  ,
			--		[ACT_CD]  ,
			--		[CUST_NME] ,
			--		[ODIE_CUST_ID],
			--		[NTE_ID] ,
			--		[SDE_Assigned]  )
			--	SELECT  [RSPN_ID]   ,
			--		[REQ_ID]  ,
			--		[RSPN_DT]  ,
			--		[CUST_TEAM_PDL]  ,
			--		[MNSPM_ID]  ,
			--		[RSPN_ERROR_TXT]  ,
			--		[CREAT_DT]  ,
			--		[ACK_CD]  ,
			--		[SOWS_FOLDR_PATH_NME]  ,
			--		[DOC_URL_ADR]  ,
			--		[ACT_CD]  ,
			--		[CUST_NME] ,
			--		[ODIE_CUST_ID],
			--		[NTE_ID] ,
			--		[SDE_Assigned]	
			--	FROM	[COWS].[arch].[ODIE_RSPN] WITH(NOLOCK) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
			--	UNION
			--	SELECT  [RSPN_ID]   ,
			--		[REQ_ID]  ,
			--		[RSPN_DT]  ,
			--		[CUST_TEAM_PDL]  ,
			--		[MNSPM_ID]  ,
			--		[RSPN_ERROR_TXT]  ,
			--		[CREAT_DT]  ,
			--		[ACK_CD]  ,
			--		[SOWS_FOLDR_PATH_NME]  ,
			--		[DOC_URL_ADR]  ,
			--		[ACT_CD]  ,
			--		[CUST_NME] ,
			--		[ODIE_CUST_ID]	,
			--		[NTE_ID] ,
			--		[SDE_Assigned]
			--	FROM	[COWS].[arch].[ODIE_RSPN] WITH(NOLOCK) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ODIE_RSPN] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ODIE_RSPN] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	DELETE  FROM	[COWS].[arch].[ODIE_RSPN] WITH(TABLOCKX) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ODIE_RSPN] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ODIE_RSPN]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			--	/*
			--	FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			--	FK01_ODIE_RSPN_INFO	ODIE_RSPN_INFO	REQ_ID				ODIE_REQ		REQ_ID
			--	FK02_ODIE_RSPN_INFO	ODIE_RSPN_INFO	FSA_MDS_EVENT_ID	FSA_MDS_EVENT	FSA_MDS_EVENT_ID
			--	FK03_ODIE_RSPN_INFO	ODIE_RSPN_INFO	EVENT_ID			EVENT			EVENT_ID
			--	-------------------------------------------------ok--------EVENT--------ORDER-->>[REQ_ID]----------------
			--	*/
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ODIE_RSPN_INFO] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	INSERT INTO [dbo].[ODIE_RSPN_INFO](
			--		[RSPN_INFO_ID]   ,
			--,[REQ_ID]
			--		  ,[MODEL_ID]
			--		  ,[DEV_ID]
			--		  ,[SERIAL_NO]
			--		  ,[RDSN_NBR]
			--		  ,[FAST_TRK_CD]
			--		  ,[DSPCH_RDY_CD]
			--		  ,[RSPN_INFO_DT]
			--		  ,[SLCTD_CD]
			--		  ,[CREAT_DT]
			--		  ,[MANF_ID]
			--		  ,[OPT_OUT_CD]
			--		  ,[OPT_OUT_REAS_TXT]
			--		  ,[BUS_JUSTN_TXT]
			--		  ,[MGT_TXT]
			--		  ,[ONE_MB_TXT]
			--		  ,[FRWL_PROD_CD]
			--		  ,[FSA_MDS_EVENT_ID]
			--		  ,[EVENT_ID]
			--		  ,[H6_CUST_ID] )
			----	SELECT  [RSPN_INFO_ID]   ,
			--,[REQ_ID]
			--		  ,[MODEL_ID]
			--		  ,[DEV_ID]
			--		  ,[SERIAL_NO]
			--		  ,[RDSN_NBR]
			--		  ,[FAST_TRK_CD]
			--		  ,[DSPCH_RDY_CD]
			--		  ,[RSPN_INFO_DT]
			--		  ,[SLCTD_CD]
			--		  ,[CREAT_DT]
			--		  ,[MANF_ID]
			--		  ,[OPT_OUT_CD]
			--		  ,[OPT_OUT_REAS_TXT]
			--		  ,[BUS_JUSTN_TXT]
			--		  ,[MGT_TXT]
			--		  ,[ONE_MB_TXT]
			--		  ,[FRWL_PROD_CD]
			--		  ,[FSA_MDS_EVENT_ID]
			--		  ,[EVENT_ID]
			--		  ,[H6_CUST_ID]	
			----	FROM	[COWS].[arch].[ODIE_RSPN_INFO] WITH(NOLOCK) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
			----	UNION
			----	SELECT  [RSPN_INFO_ID]   ,
			--,[REQ_ID]
			--		  ,[MODEL_ID]
			--		  ,[DEV_ID]
			--		  ,[SERIAL_NO]
			--		  ,[RDSN_NBR]
			--		  ,[FAST_TRK_CD]
			--		  ,[DSPCH_RDY_CD]
			--		  ,[RSPN_INFO_DT]
			--		  ,[SLCTD_CD]
			--		  ,[CREAT_DT]
			--		  ,[MANF_ID]
			--		  ,[OPT_OUT_CD]
			--		  ,[OPT_OUT_REAS_TXT]
			--		  ,[BUS_JUSTN_TXT]
			--		  ,[MGT_TXT]
			--		  ,[ONE_MB_TXT]
			--		  ,[FRWL_PROD_CD]
			--		  ,[FSA_MDS_EVENT_ID]
			--		  ,[EVENT_ID]
			--		  ,[H6_CUST_ID]
			--	FROM	[COWS].[arch].[ODIE_RSPN_INFO] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ODIE_RSPN_INFO] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ODIE_RSPN_INFO]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ODIE_RSPN_INFO] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	DELETE  FROM	[COWS].[arch].[ODIE_RSPN_INFO] WITH(TABLOCKX) WHERE REQ_ID IN (SELECT	REQ_ID 	FROM	@RECID)
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ODIE_RSPN_INFO] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ODIE_RSPN_INFO] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	DELETE  FROM	[COWS].[arch].[ODIE_RSPN_INFO] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ODIE_RSPN_INFO] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ODIE_RSPN_INFO]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			--	/*
			--	FOREIGN_KEY		TABLE_NAME	COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			--	FK01_ODIE_REQ	ODIE_REQ	ODIE_MSG_ID		LK_ODIE_MSG		ODIE_MSG_ID
			--	FK02_ODIE_REQ	ODIE_REQ	ORDR_ID			FSA_ORDR		ORDR_ID
			--	FK03_ODIE_REQ	ODIE_REQ	STUS_ID			LK_STUS			STUS_ID
			--	FK04_ODIE_REQ	ODIE_REQ	MDS_EVENT_ID	EVENT			EVENT_ID
			--	------------------------------------------ok------------------EVENT-------ORDER-->>[REQ_ID]--------------
			--	FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			--	FK02_ODIE_REQ	ODIE_REQ	ORDR_ID			FSA_ORDR		ORDR_ID
			--	FK01_ODIE_REQ	ODIE_REQ	ODIE_MSG_ID		LK_ODIE_MSG		ODIE_MSG_ID
			--	FK03_ODIE_REQ	ODIE_REQ	STUS_ID			LK_STUS			STUS_ID
			--	FK04_ODIE_REQ	ODIE_REQ	MDS_EVENT_ID	EVENT			EVENT_ID

			--	*/
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ODIE_REQ] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	INSERT INTO [dbo].[ODIE_REQ](
			--		[REQ_ID]   ,
			--		[ORDR_ID] ,
			--		[ODIE_MSG_ID]  ,
			--		[STUS_MOD_DT]  ,
			--		[H1_CUST_ID]  ,
			--		[CREAT_DT]  ,
			--		[STUS_ID]  ,
			--		[MDS_EVENT_ID] ,
			--		[TAB_SEQ_NBR]  ,
			--		[CUST_NME] ,
			--		[ODIE_CUST_ID],
			--		[CPT_ID] ,
			--		[ODIE_CUST_INFO_REQ_CAT_TYPE_ID] ,
			--		[DEV_FLTR],[CSG_LVL_ID]  )
			--	SELECT  [REQ_ID]   ,
			--		[ORDR_ID] ,
			--		[ODIE_MSG_ID]  ,
			--		[STUS_MOD_DT]  ,
			--		[H1_CUST_ID]  ,
			--		[CREAT_DT]  ,
			--		[STUS_ID]  ,
			--		[MDS_EVENT_ID] ,
			--		[TAB_SEQ_NBR]  ,
			--		[CUST_NME] ,
			--		[ODIE_CUST_ID],
			--		[CPT_ID] ,
			--		[ODIE_CUST_INFO_REQ_CAT_TYPE_ID] ,
			--		[DEV_FLTR],[CSG_LVL_ID]
			--	FROM	[COWS].[arch].[ODIE_REQ] WITH(NOLOCK)	WHERE MDS_EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
			--	UNION
			--	SELECT  [REQ_ID]   ,
			--		[ORDR_ID] ,
			--		[ODIE_MSG_ID]  ,
			--		[STUS_MOD_DT]  ,
			--		[H1_CUST_ID]  ,
			--		[CREAT_DT]  ,
			--		[STUS_ID]  ,
			--		[MDS_EVENT_ID] ,
			--		[TAB_SEQ_NBR]  ,
			--		[CUST_NME] ,
			--		[ODIE_CUST_ID],
			--		[CPT_ID] ,
			--		[ODIE_CUST_INFO_REQ_CAT_TYPE_ID] ,
			--		[DEV_FLTR],[CSG_LVL_ID]
			--	FROM	[COWS].[arch].[ODIE_REQ] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ODIE_REQ] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ODIE_REQ]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ODIE_REQ] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	DELETE  FROM	[COWS].[arch].[ODIE_REQ] WITH(TABLOCKX)	WHERE MDS_EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ODIE_REQ] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ODIE_REQ] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			--	DELETE  FROM	[COWS].[arch].[ODIE_REQ] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
			--	Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ODIE_REQ] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			--	--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ODIE_REQ]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			--END
			IF(@Mood=0 )  /* ORDR ONLY */
			BEGIN
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK14_FSA_ORDR	FSA_ORDR	INSTL_SRVC_TIER_CD	LK_MDS_SRVC_TIER	MDS_SRVC_TIER_ID
				FK16_FSA_ORDR	FSA_ORDR	ORDR_ACTN_ID		LK_ORDR_ACTN	ORDR_ACTN_ID
				FK18_FSA_ORDR	FSA_ORDR	ORDR_TYPE_CD		LK_FSA_ORDR_TYPE	FSA_ORDR_TYPE_CD
				FK20_FSA_ORDR	FSA_ORDR	PROD_TYPE_CD		LK_FSA_PROD_TYPE	FSA_PROD_TYPE_CD
				FK25_FSA_ORDR	FSA_ORDR	ORDR_ID				ORDR	ORDR_ID
				-------------------------------------------ok----------ORDER-->>CKTID-------------------------------------------
				FOREIGN_KEY						TABLE_NAME				COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_SIP_TRNK_GRP				SIP_TRNK_GRP			ORDR_ID				FSA_ORDR			ORDR_ID
				FK02_ODIE_REQ					ODIE_REQ				ORDR_ID				FSA_ORDR			ORDR_ID
				FK01_FSA_ORDR_VAS				FSA_ORDR_VAS			ORDR_ID				FSA_ORDR			ORDR_ID
				FK01_FSA_ORDR_RELTD_ORDR		FSA_ORDR_RELTD_ORDR		RELTD_ORDR_ID		FSA_ORDR			ORDR_ID
				FK02_FSA_ORDR_RELTD_ORDR		FSA_ORDR_RELTD_ORDR		ORDR_ID				FSA_ORDR			ORDR_ID
				FK01_FSA_ORDR_MSG				FSA_ORDR_MSG			ORDR_ID				FSA_ORDR			ORDR_ID
				FK04_FSA_ORDR_GOM_XNCI			FSA_ORDR_GOM_XNCI		ORDR_ID				FSA_ORDR			ORDR_ID
				FK01_FSA_ORDR_CUST				FSA_ORDR_CUST			ORDR_ID				FSA_ORDR			ORDR_ID
				FK01_FSA_ORDER_CSC				FSA_ORDR_CSC			ORDR_ID				FSA_ORDR			ORDR_ID
				FK01_FSA_ORDR_CPE_LINE_ITEM		FSA_ORDR_CPE_LINE_ITEM	ORDR_ID				FSA_ORDR			ORDR_ID
				FK01_FSA_ORDR_BILL_LINE_ITEM	FSA_ORDR_BILL_LINE_ITEM	ORDR_ID				FSA_ORDR			ORDR_ID
				FK01_FSA_MDS_EVENT_ORDR			FSA_MDS_EVENT_ORDR		ORDR_ID				FSA_ORDR			ORDR_ID
				FK01_FSA_IP_ADR					FSA_IP_ADR				ORDR_ID				FSA_ORDR			ORDR_ID

				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[FSA_ORDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[FSA_ORDR](
					[ORDR_ID]
				  ,[ORDR_ACTN_ID]
				  ,[FTN]
				  ,[ORDR_TYPE_CD]
				  ,[ORDR_SUB_TYPE_CD]
				  ,[PROD_TYPE_CD]
				  ,[PRNT_FTN]
				  ,[RELTD_FTN]
				  ,[SCA_NBR]
				  ,[TSP_CD]
				  ,[CPW_TYPE]
				  ,[CUST_CMMT_DT]
				  ,[CUST_WANT_DT]
				  ,[CUST_ORDR_SBMT_DT]
				  ,[CUST_SIGNED_DT]
				  ,[ORDR_SBMT_DT]
				  ,[CUST_PRMS_OCPY_CD]
				  ,[CUST_ACPT_ERLY_SRVC_CD]
				  ,[MULT_CUST_ORDR_CD]
				  ,[GVRMNT_TYPE_ID]
				  ,[INSTL_ESCL_CD]
				  --,[INSTL_DSGN_DOC_NBR]
				  ,[INSTL_VNDR_CD]
				  ,[INSTL_SOLU_SRVC_DES]
				  ,[INSTL_NW_TYPE_CD]
				  ,[INSTL_TRNSPRT_TYPE_CD]
				  ,[INSTL_SRVC_TIER_CD]
				  ,[CPE_CPE_ORDR_TYPE_CD]
				  ,[CPE_EQPT_ONLY_CD]
				  ,[CPE_ACCS_PRVDR_CD]
				  ,[CPE_ECCKT_ID]
				  ,[CPE_MSCP_CD]
				  ,[CPE_DLVRY_DUTY_ID]
				  ,[CPE_DLVRY_DUTY_AMT]
				  ,[CPE_SHIP_CHG_AMT]
				  ,[TSUP_TELCO_DEMARC_BLDG_NME]
				  ,[TSUP_TELCO_DEMARC_FLR_ID]
				  ,[TSUP_TELCO_DEMARC_RM_NBR]
				  ,[TSUP_NEAREST_CROSS_STREET_NME]
				  ,[TSUP_PRS_QOT_NBR]
				  ,[TSUP_GCS_NBR]
				  ,[TSUP_RFQ_NBR]
				  ,[TTRPT_ACCS_TYPE_CD]
				  ,[TTRPT_ACCS_PRVDR_CD]
				  --,[TTRPT_ACCS_ARNGT_CD]
				  ,[TTRPT_ACCS_CNTRC_TERM_CD]
				  ,[TTRPT_SPD_OF_SRVC_BDWD_DES]
				  ,[TTRPT_LINK_PRCOL_CD]
				  --,[TTRPT_ENCAP_CD]
				  ,[TTRPT_ROUTG_TYPE_CD]
				  ,[TTRPT_MPLS_VPN_OVR_SPLK_CD]
				  ,[TTRPT_SRVC_TYPE_ID]
				  ,[TTRPT_FRMNG_DES]
				  ,[TTRPT_NCDE_DES]
				  ,[TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD]
				  ,[TTRPT_JACK_NRFC_TYPE_CD]
				  ,[TTRPT_STRATUM_LVL_CD]
				  ,[TTRPT_CLOCK_SRC_CD]
				  ,[TTRPT_IS_OCHRONOUS_CD]
				  ,[TTRPT_PRLEL_PRCS_TYPE_CD]
				  ,[TTRPT_PRLEL_PRCS_CKT_ID]
				  ,[TTRPT_RELTD_FCTY_REUSE_CD]
				  ,[TTRPT_RELTD_FCTY_CKT_ID]
				  ,[TTRPT_TME_SOLT_CNT]
				  ,[TTRPT_STRT_TME_SLOT_NBR]
				  ,[TTRPT_NW_ADR]
				  ,[TTRPT_SHRD_TNANT_CD]
				  ,[TTRPT_ALT_ACCS_CD]
				  ,[TTRPT_ALT_ACCS_PRVDR_CD]
				  --,[TTRPT_FBR_HAND_OFF_CD]
				  ,[TTRPT_MNGD_DATA_SRVC_CD]
				  ,[TTRPT_OSS_CD]
				  ,[TTRPT_MPLS_BACKHAUL_CD]
				  --,[TPORT_IP_VER_TYPE_CD]
				  --,[TPORT_IPV4_ADR_PRVDR_CD]
				  --,[TPORT_IPV4_ADR_QTY]
				  --,[TPORT_IPV6_ADR_PRVDR_CD]
				  --,[TPORT_IPV6_ADR_QTY]
				  ,[TPORT_USER_TYPE_ID]
				  ,[TPORT_MULTI_MEG_CD]
				  --,[TPORT_VLAN_QTY]
				  --,[TPORT_ETHRNT_NRFC_INDCR]
				  ,[TPORT_SCRMB_TXT]
				  ,[TPORT_PORT_TYPE_ID]
				  --,[TPORT_CNCTR_TYPE_ID]
				  --,[TPORT_CUST_ROUTR_TAG_TXT]
				  --,[TPORT_CUST_ROUTR_AUTO_NEGOT_CD]
				  ,[TPORT_CUST_PRVDD_IP_ADR_INDCR]
				  ,[TPORT_CUST_IP_ADR]
				  ,[TPORT_XST_IP_CNCTN_CD]
				  ,[TPORT_CURR_INET_PRVDR_CD]
				  ,[TPORT_BRSTBL_PRICE_DISC_CD]
				  ,[TPORT_BRSTBL_USAGE_TYPE_CD]
				  ,[DISC_REAS_CD]
				  ,[DISC_CNCL_BEF_STRT_REAS_CD]
				  ,[DISC_CNCL_BEF_STRT_REAS_DETL_TXT]
				  ,[CREAT_DT]
				  ,[FSA_EXP_TYPE_CD]
				  ,[CPE_PHN_NBR]
				  ,[CPE_PHN_NBR_TYPE_CD]
				  ,[INTL_PL_SRVC_TYPE_CD]
				  ,[INTL_PL_CKT_TYPE_CD]
				  ,[CHARS_ID]
				  ,[SOTS_ID]
				  --,[CXR_ACCS_CD]
				  ,[INTL_PL_PLUS_IP_CD]
				  ,[INTL_PL_CAT_ID]
				  ,[OSS_OPT_CD]
				  ,[MULT_ORDR_INDEX_NBR]
				  ,[MULT_ORDR_TOT_CNT]
				  ,[VNDR_VPN_CD]
				  ,[ORDR_CHNG_DES]
				  ,[CXR_SRVC_ID]
				  ,[CXR_CKT_ID]
				  ,[FSA_ORGNL_INSTL_ORDR_ID]
				  ,[ORANGE_OFFR_CD]
				  ,[CKT_ID]
				  ,[VNDO_CNTRC_TERM_ID]
				  ,[ADDL_IP_ADR_CD]
				  ,[DOMN_NME]
				  ,[NRFC_PRCOL_CD]
				  ,[CUST_FRWL_CD]
				  ,[IPV4_SUBNET_MASK_ADR]
				  --,[SPA_ACCS_INDCR_DES]
				  ,[NW_USER_ADR]
				  --,[PORT_RT_TYPE_CD]
				  ,[TTRPT_ACCS_TYPE_DES]
				  ,[PRE_SBMT_CREAT_DT]
				  ,[TPORT_PPLVLS_CD]
				  ,[CPE_REC_ONLY_CD]
				  --,[TTRPT_ACCS_TERM_DES]
				  --,[TTRPT_QOT_XPIRN_DT]
				  ,[TPORT_SPCL_INSTRTN_TXT]
				  ,[TPORT_DIA_CLASS_OF_SRVC_TXT]
				  --,[TPORT_DIA_NOC_TO_NOC_TXT]
				  ,[TPORT_DUAL_STK_ADR_PRVDR_TXT]
				  ,[TPORT_LINK_MGT_PRCOL_TXT]
				  --,[TPORT_PROJ_DES]
				  ,[TPORT_VNDR_RAW_MRC_IN_CUR_AMT]
				  ,[TPORT_VNDR_RAW_MRC_QOT_CUR_AMT]
				  ,[TPORT_VNDR_RAW_NRC_QOT_CUR_AMT]
				  ,[TPORT_CNVRSN_RT_QTY]
				  ,[TPORT_MARKUP_PCT_QTY]
				  ,[TPORT_TAX_PCT_QTY]
				  ,[TPORT_BEARER_CHG_AMT]
				  ,[TPORT_ADDL_MRC_AMT]
				  ,[TPORT_CALC_RT_MRC_USD_AMT]
				  ,[TPORT_CALC_RT_NRC_USD_AMT]
				  ,[TPORT_CUR_NME]
				  --,[TTRPT_ACCS_BDWD_TYPE]
				  ,[PRE_QUAL_NBR]
				  --,[TPORT_VNDR_QUOTE_ID]
				  ,[TPORT_PREQUAL_SITE_ID]
				  --,[TPORT_ETH_ACCS_TYPE_CD]
				  ,[TPORT_PREQ_LINE_ITEM_ID_XPIRN_DT]
				  ,[TPORT_PREQUAL_LINE_ITEM_ID]
				  ,[TPORT_DVSTY_TYPE_NME]
				  ,[TPORT_CALC_RT_NRC_IN_CUR_AMT]
				  ,[TPORT_CALC_RT_MRC_IN_CUR_AMT]
				  ,[DDR]
				  ,[DDU]
				  ,[CPE_TST_TN_NBR]
				  ,[SITE_ID]
				  ,[CPE_VNDR]
				  --,[PL_NBR]
				  ,[NUA]
				  ,[CKT] )

				SELECT  
				[ORDR_ID]
				  ,[ORDR_ACTN_ID]
				  ,[FTN]
				  ,[ORDR_TYPE_CD]
				  ,[ORDR_SUB_TYPE_CD]
				  ,[PROD_TYPE_CD]
				  ,[PRNT_FTN]
				  ,[RELTD_FTN]
				  ,[SCA_NBR]
				  ,[TSP_CD]
				  ,[CPW_TYPE]
				  ,[CUST_CMMT_DT]
				  ,[CUST_WANT_DT]
				  ,[CUST_ORDR_SBMT_DT]
				  ,[CUST_SIGNED_DT]
				  ,[ORDR_SBMT_DT]
				  ,[CUST_PRMS_OCPY_CD]
				  ,[CUST_ACPT_ERLY_SRVC_CD]
				  ,[MULT_CUST_ORDR_CD]
				  ,[GVRMNT_TYPE_ID]
				  ,[INSTL_ESCL_CD]
				  --,[INSTL_DSGN_DOC_NBR]
				  ,[INSTL_VNDR_CD]
				  ,[INSTL_SOLU_SRVC_DES]
				  ,[INSTL_NW_TYPE_CD]
				  ,[INSTL_TRNSPRT_TYPE_CD]
				  ,[INSTL_SRVC_TIER_CD]
				  ,[CPE_CPE_ORDR_TYPE_CD]
				  ,[CPE_EQPT_ONLY_CD]
				  ,[CPE_ACCS_PRVDR_CD]
				  ,[CPE_ECCKT_ID]
				  ,[CPE_MSCP_CD]
				  ,[CPE_DLVRY_DUTY_ID]
				  ,[CPE_DLVRY_DUTY_AMT]
				  ,[CPE_SHIP_CHG_AMT]
				  ,[TSUP_TELCO_DEMARC_BLDG_NME]
				  ,[TSUP_TELCO_DEMARC_FLR_ID]
				  ,[TSUP_TELCO_DEMARC_RM_NBR]
				  ,[TSUP_NEAREST_CROSS_STREET_NME]
				  ,[TSUP_PRS_QOT_NBR]
				  ,[TSUP_GCS_NBR]
				  ,[TSUP_RFQ_NBR]
				  ,[TTRPT_ACCS_TYPE_CD]
				  ,[TTRPT_ACCS_PRVDR_CD]
				  --,[TTRPT_ACCS_ARNGT_CD]
				  ,[TTRPT_ACCS_CNTRC_TERM_CD]
				  ,[TTRPT_SPD_OF_SRVC_BDWD_DES]
				  ,[TTRPT_LINK_PRCOL_CD]
				  --,[TTRPT_ENCAP_CD]
				  ,[TTRPT_ROUTG_TYPE_CD]
				  ,[TTRPT_MPLS_VPN_OVR_SPLK_CD]
				  ,[TTRPT_SRVC_TYPE_ID]
				  ,[TTRPT_FRMNG_DES]
				  ,[TTRPT_NCDE_DES]
				  ,[TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD]
				  ,[TTRPT_JACK_NRFC_TYPE_CD]
				  ,[TTRPT_STRATUM_LVL_CD]
				  ,[TTRPT_CLOCK_SRC_CD]
				  ,[TTRPT_IS_OCHRONOUS_CD]
				  ,[TTRPT_PRLEL_PRCS_TYPE_CD]
				  ,[TTRPT_PRLEL_PRCS_CKT_ID]
				  ,[TTRPT_RELTD_FCTY_REUSE_CD]
				  ,[TTRPT_RELTD_FCTY_CKT_ID]
				  ,[TTRPT_TME_SOLT_CNT]
				  ,[TTRPT_STRT_TME_SLOT_NBR]
				  ,[TTRPT_NW_ADR]
				  ,[TTRPT_SHRD_TNANT_CD]
				  ,[TTRPT_ALT_ACCS_CD]
				  ,[TTRPT_ALT_ACCS_PRVDR_CD]
				  --,[TTRPT_FBR_HAND_OFF_CD]
				  ,[TTRPT_MNGD_DATA_SRVC_CD]
				  ,[TTRPT_OSS_CD]
				  ,[TTRPT_MPLS_BACKHAUL_CD]
				  --,[TPORT_IP_VER_TYPE_CD]
				  --,[TPORT_IPV4_ADR_PRVDR_CD]
				  --,[TPORT_IPV4_ADR_QTY]
				  --,[TPORT_IPV6_ADR_PRVDR_CD]
				  --,[TPORT_IPV6_ADR_QTY]
				  ,[TPORT_USER_TYPE_ID]
				  ,[TPORT_MULTI_MEG_CD]
				  --,[TPORT_VLAN_QTY]
				  --,[TPORT_ETHRNT_NRFC_INDCR]
				  ,[TPORT_SCRMB_TXT]
				  ,[TPORT_PORT_TYPE_ID]
				  --,[TPORT_CNCTR_TYPE_ID]
				  --,[TPORT_CUST_ROUTR_TAG_TXT]
				  --,[TPORT_CUST_ROUTR_AUTO_NEGOT_CD]
				  ,[TPORT_CUST_PRVDD_IP_ADR_INDCR]
				  ,[TPORT_CUST_IP_ADR]
				  ,[TPORT_XST_IP_CNCTN_CD]
				  ,[TPORT_CURR_INET_PRVDR_CD]
				  ,[TPORT_BRSTBL_PRICE_DISC_CD]
				  ,[TPORT_BRSTBL_USAGE_TYPE_CD]
				  ,[DISC_REAS_CD]
				  ,[DISC_CNCL_BEF_STRT_REAS_CD]
				  ,[DISC_CNCL_BEF_STRT_REAS_DETL_TXT]
				  ,[CREAT_DT]
				  ,[FSA_EXP_TYPE_CD]
				  ,[CPE_PHN_NBR]
				  ,[CPE_PHN_NBR_TYPE_CD]
				  ,[INTL_PL_SRVC_TYPE_CD]
				  ,[INTL_PL_CKT_TYPE_CD]
				  ,[CHARS_ID]
				  ,[SOTS_ID]
				  --,[CXR_ACCS_CD]
				  ,[INTL_PL_PLUS_IP_CD]
				  ,[INTL_PL_CAT_ID]
				  ,[OSS_OPT_CD]
				  ,[MULT_ORDR_INDEX_NBR]
				  ,[MULT_ORDR_TOT_CNT]
				  ,[VNDR_VPN_CD]
				  ,[ORDR_CHNG_DES]
				  ,[CXR_SRVC_ID]
				  ,[CXR_CKT_ID]
				  ,[FSA_ORGNL_INSTL_ORDR_ID]
				  ,[ORANGE_OFFR_CD]
				  ,[CKT_ID]
				  ,[VNDO_CNTRC_TERM_ID]
				  ,[ADDL_IP_ADR_CD]
				  ,[DOMN_NME]
				  ,[NRFC_PRCOL_CD]
				  ,[CUST_FRWL_CD]
				  ,[IPV4_SUBNET_MASK_ADR]
				  --,[SPA_ACCS_INDCR_DES]
				  ,[NW_USER_ADR]
				  --,[PORT_RT_TYPE_CD]
				  ,[TTRPT_ACCS_TYPE_DES]
				  ,[PRE_SBMT_CREAT_DT]
				  ,[TPORT_PPLVLS_CD]
				  ,[CPE_REC_ONLY_CD]
				  --,[TTRPT_ACCS_TERM_DES]
				  --,[TTRPT_QOT_XPIRN_DT]
				  ,[TPORT_SPCL_INSTRTN_TXT]
				  ,[TPORT_DIA_CLASS_OF_SRVC_TXT]
				  --,[TPORT_DIA_NOC_TO_NOC_TXT]
				  ,[TPORT_DUAL_STK_ADR_PRVDR_TXT]
				  ,[TPORT_LINK_MGT_PRCOL_TXT]
				  --,[TPORT_PROJ_DES]
				  ,[TPORT_VNDR_RAW_MRC_IN_CUR_AMT]
				  ,[TPORT_VNDR_RAW_MRC_QOT_CUR_AMT]
				  ,[TPORT_VNDR_RAW_NRC_QOT_CUR_AMT]
				  ,[TPORT_CNVRSN_RT_QTY]
				  ,[TPORT_MARKUP_PCT_QTY]
				  ,[TPORT_TAX_PCT_QTY]
				  ,[TPORT_BEARER_CHG_AMT]
				  ,[TPORT_ADDL_MRC_AMT]
				  ,[TPORT_CALC_RT_MRC_USD_AMT]
				  ,[TPORT_CALC_RT_NRC_USD_AMT]
				  ,[TPORT_CUR_NME]
				  --,[TTRPT_ACCS_BDWD_TYPE]
				  ,[PRE_QUAL_NBR]
				  --,[TPORT_VNDR_QUOTE_ID]
				  ,[TPORT_PREQUAL_SITE_ID]
				  --,[TPORT_ETH_ACCS_TYPE_CD]
				  ,[TPORT_PREQ_LINE_ITEM_ID_XPIRN_DT]
				  ,[TPORT_PREQUAL_LINE_ITEM_ID]
				  ,[TPORT_DVSTY_TYPE_NME]
				  ,[TPORT_CALC_RT_NRC_IN_CUR_AMT]
				  ,[TPORT_CALC_RT_MRC_IN_CUR_AMT]
				  ,[DDR]
				  ,[DDU]
				  ,[CPE_TST_TN_NBR]
				  ,[SITE_ID]
				  ,[CPE_VNDR]
				  --,[PL_NBR]
				  ,[NUA]
				  ,[CKT]
				FROM	[COWS].[arch].[FSA_ORDR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[FSA_ORDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[FSA_ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[FSA_ORDR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[FSA_ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[FSA_ORDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_ADR	ORDR_ADR	ADR_TYPE_ID			LK_ADR_TYPE	ADR_TYPE_ID
				FK02_ORDR_ADR	ORDR_ADR	CTRY_CD				LK_CTRY	CTRY_CD
				FK04_ORDR_ADR	ORDR_ADR	ORDR_ID				ORDR	ORDR_ID
				FK05_ORDR_ADR	ORDR_ADR	CREAT_BY_USER_ID	LK_USER	USER_ID
				FK06_ORDR_ADR	ORDR_ADR	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID
				-------------------------------------------------ok-----------ORDER------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_ADR     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_ADR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_ADR](
					[ORDR_ADR_ID]   ,
					[ORDR_ID]  ,
					[ADR_TYPE_ID]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[CTRY_CD] ,
					[CIS_LVL_TYPE] ,
					[FSA_MDUL_ID]  ,
					[STREET_ADR_1] ,
					[STREET_ADR_2] ,
					[CTY_NME] ,
					[PRVN_NME] ,
					[STT_CD] ,
					[ZIP_PSTL_CD] ,
					[BLDG_NME] ,
					[FLR_ID] ,
					[RM_NBR] ,
					[HIER_LVL_CD],
					[STREET_ADR_3] --,[STE_TXT] 
					)
				SELECT  [ORDR_ADR_ID]   ,
					[ORDR_ID]  ,
					[ADR_TYPE_ID]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[CTRY_CD] ,
					[CIS_LVL_TYPE] ,
					[FSA_MDUL_ID]  ,
					[STREET_ADR_1] ,
					[STREET_ADR_2] ,
					[CTY_NME] ,
					[PRVN_NME] ,
					[STT_CD] ,
					[ZIP_PSTL_CD] ,
					[BLDG_NME] ,
					[FLR_ID] ,
					[RM_NBR] ,
					[HIER_LVL_CD],
					[STREET_ADR_3] --,[STE_TXT]
				FROM	[COWS].[arch].[ORDR_ADR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_ADR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_ADR     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_ADR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_ADR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_ADR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_ADR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_CKT_CHG	ORDR_CKT_CHG	ORDR_ID				ORDR	ORDR_ID
				FK02_ORDR_CKT_CHG	ORDR_CKT_CHG	CREAT_BY_USER_ID	LK_USER	USER_ID
				FK03_ORDR_CKT_CHG	ORDR_CKT_CHG	CHG_CUR_ID			LK_CUR	CUR_ID
				FK04_ORDR_CKT_CHG	ORDR_CKT_CHG	CKT_CHG_TYPE_ID		LK_CKT_CHG_TYPE	CKT_CHG_TYPE_ID
				FK05_ORDR_CKT_CHG	ORDR_CKT_CHG	SALS_STUS_ID		LK_STUS	STUS_ID
				------------------------------------ok--------------------ORDER------------------------
				*/
				RAISERROR ('Starting to execute [dbo].[ORDR_CKT_CHG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_CKT_CHG](
					[ORDR_ID]  ,
					[CKT_CHG_TYPE_ID]  ,
					[VER_ID]  ,
					[CHG_CUR_ID]  ,
					[CHG_NRC_AMT]  ,
					[CHG_NRC_IN_USD_AMT]  ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[TAX_RT_PCT_QTY]  ,
					[NTE_TXT] ,
					[SALS_STUS_ID]  ,
					[REQR_BILL_ORDR_CD]  ,
					[XPIRN_DT] ,
					[TRMTG_CD]  )
				SELECT  [ORDR_ID]  ,
					[CKT_CHG_TYPE_ID]  ,
					[VER_ID]  ,
					[CHG_CUR_ID]  ,
					[CHG_NRC_AMT]  ,
					[CHG_NRC_IN_USD_AMT]  ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[TAX_RT_PCT_QTY]  ,
					[NTE_TXT] ,
					[SALS_STUS_ID]  ,
					[REQR_BILL_ORDR_CD]  ,
					[XPIRN_DT] ,
					[TRMTG_CD]
				FROM	[COWS].[arch].[ORDR_CKT_CHG] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_CKT_CHG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate()RAISERROR ('Starting to execute [arch].[ORDR_CKT_CHG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_CKT_CHG] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_CKT_CHG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_CKT_CHG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_CNTCT	ORDR_CNTCT	CNTCT_TYPE_ID			LK_CNTCT_TYPE	CNTCT_TYPE_ID
				FK02_ORDR_CNTCT	ORDR_CNTCT	ORDR_ID					ORDR			ORDR_ID
				FK03_ORDR_CNTCT	ORDR_CNTCT	TME_ZONE_ID				LK_TME_ZONE		TME_ZONE_ID
				FK04_ORDR_CNTCT	ORDR_CNTCT	CREAT_BY_USER_ID		LK_USER			USER_ID
				FK05_ORDR_CNTCT	ORDR_CNTCT	REC_STUS_ID				LK_REC_STUS		REC_STUS_ID
				FK07_ORDR_CNTCT	ORDR_CNTCT	ROLE_ID					LK_ROLE			ROLE_ID
				-------------------------------------------ok--------------ORDER-------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_CNTCT     ON
				Set @Time=GetDate()RAISERROR ('Starting to execute [dbo].[ORDR_CNTCT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_CNTCT](
					[ORDR_CNTCT_ID]   ,
					[ORDR_ID]  ,
					[CNTCT_TYPE_ID]  ,
					[PHN_NBR]  ,
					[TME_ZONE_ID]  ,
					[INTPRTR_CD]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[FAX_NBR]  ,
					[ROLE_ID]  ,
					[CIS_LVL_TYPE] ,
					[FSA_MDUL_ID]  ,
					[FRST_NME] ,
					[LST_NME] ,
					[EMAIL_ADR] ,
					[CNTCT_HR_TXT]  ,
					[NPA]  ,
					[NXX]  ,
					[STN_NBR]  ,
					[CTY_CD] ,
					[ISD_CD]  ,
					[CNTCT_NME] ,
					[PHN_EXT_NBR]  ,
					[SUPPD_LANG_NME]  ,
					[FSA_TME_ZONE_CD]  )
				SELECT DISTINCT  [ORDR_CNTCT_ID]   ,
					A.[ORDR_ID]  ,
					[CNTCT_TYPE_ID]  ,
					[PHN_NBR]  ,
					[TME_ZONE_ID]  ,
					[INTPRTR_CD]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[FAX_NBR]  ,
					[ROLE_ID]  ,
					[CIS_LVL_TYPE] ,
					[FSA_MDUL_ID]  ,
					[FRST_NME] ,
					[LST_NME] ,
					[EMAIL_ADR] ,
					[CNTCT_HR_TXT]  ,
					[NPA]  ,
					[NXX]  ,
					[STN_NBR]  ,
					[CTY_CD] ,
					[ISD_CD]  ,
					[CNTCT_NME] ,
					[PHN_EXT_NBR]  ,
					[SUPPD_LANG_NME]  ,
					[FSA_TME_ZONE_CD] 
				FROM	[COWS].[arch].[ORDR_CNTCT] A WITH(NOLOCK) INNER JOIN @Order_Table B ON A.ORDR_ID=B.ORDR_ID
				--WHERE   A.ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_CNTCT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_CNTCT     OFF
				Set @Time=GetDate()RAISERROR ('Starting to execute [arch].[ORDR_CNTCT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				--DELETE  FROM	[COWS].[dbo].[ORDR_CNTCT] WITH(TABLOCKX) WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table) 
				DELETE  A FROM		[COWS].[arch].[ORDR_CNTCT] A WITH(TABLOCKX) INNER JOIN @Order_Table B ON A.ORDR_ID=B.ORDR_ID
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_CNTCT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_CNTCT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_EXP	ORDR_EXP	EXP_TYPE_ID			LK_EXP_TYPE	EXP_TYPE_ID
				FK02_ORDR_EXP	ORDR_EXP	ORDR_ID				ORDR		ORDR_ID
				FK04_ORDR_EXP	ORDR_EXP	CREAT_BY_USER_ID	LK_USER		USER_ID
				FK05_ORDR_EXP	ORDR_EXP	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID
				--------------------------------ok----------------------ORDER---------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_EXP     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_EXP] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_EXP](
					[ORDR_EXP_ID]   ,
					[ORDR_ID]  ,
					[EXP_TYPE_ID]  ,
					[AUTHRZR_FRST_NME]  ,
					[AUTHRZR_MDLE_INITL_TXT]  ,
					[AUTHRZR_LST_NME]  ,
					[AUTHRZR_FULL_NME]  ,
					[AUTHRZR_TITLE_ID] ,
					[AUTHRZR_PHN_NBR]  ,
					[JUSTN_TXT]  ,
					[AUTHN_DT] ,
					[BILL_TO_COST_CTR_CD]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[ESCALAT_ORDR_CD]  )
				SELECT  [ORDR_EXP_ID]   ,
					[ORDR_ID]  ,
					[EXP_TYPE_ID]  ,
					[AUTHRZR_FRST_NME]  ,
					[AUTHRZR_MDLE_INITL_TXT]  ,
					[AUTHRZR_LST_NME]  ,
					[AUTHRZR_FULL_NME]  ,
					[AUTHRZR_TITLE_ID] ,
					[AUTHRZR_PHN_NBR]  ,
					[JUSTN_TXT]  ,
					[AUTHN_DT] ,
					[BILL_TO_COST_CTR_CD]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[ESCALAT_ORDR_CD]
				FROM	[COWS].[arch].[ORDR_EXP] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_EXP] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_EXP     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_EXP] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_EXP] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_EXP] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_EXP]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_GRP	ORDR_GRP	ORDR_TYPE_ID	LK_ORDR_TYPE	ORDR_TYPE_ID
				FK02_ORDR_GRP	ORDR_GRP	PROD_TYPE_ID	LK_PROD_TYPE	PROD_TYPE_ID
				FK03_ORDR_GRP	ORDR_GRP	ORDR_ID			ORDR			ORDR_ID
				---------------------------------------ok--------------------ORDER---------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_GRP] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_GRP](
					[ORDR_ID]  ,
					[H5_FOLDR_CUST_ID] ,
					[ORDR_TYPE_ID]  ,
					[PROD_TYPE_ID]  ,
					[CUST_CMMT_DT] ,
					[CREAT_DT] )
				SELECT  [ORDR_ID]  ,
					[H5_FOLDR_CUST_ID] ,
					[ORDR_TYPE_ID]  ,
					[PROD_TYPE_ID]  ,
					[CUST_CMMT_DT] ,
					[CREAT_DT]
				FROM	[COWS].[arch].[ORDR_GRP] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_GRP] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_GRP] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_GRP] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_GRP] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_GRP]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME		REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_ORDR_HOLD_MS	ORDR_HOLD_MS	ORDR_ID			ORDR				ORDR_ID
				FK02_ORDR_HOLD_MS	ORDR_HOLD_MS	RELS_BY_USER_ID	LK_USER				USER_ID
				FK03_ORDR_HOLD_MS	ORDR_HOLD_MS	HOLD_BY_USER_ID	LK_USER				USER_ID
				FK04_ORDR_HOLD_MS	ORDR_HOLD_MS	HOLD_REAS_ID	LK_ORDR_HOLD_REAS	ORDR_HOLD_REAS_ID
				--------------------------------------------------ok----------------ORDER-----------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_HOLD_MS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_HOLD_MS](
					[ORDR_ID]  ,
					[VER_ID]  ,
					[HOLD_DT]  ,
					[HOLD_REAS_ID]  ,
					[HOLD_REAS_CMNT_TXT]  ,
					[HOLD_BY_USER_ID] ,
					[RELS_DT] ,
					[RELS_BY_USER_ID] ,
					[CREAT_DT]  )
				SELECT  [ORDR_ID]  ,
					[VER_ID]  ,
					[HOLD_DT]  ,
					[HOLD_REAS_ID]  ,
					[HOLD_REAS_CMNT_TXT]  ,
					[HOLD_BY_USER_ID] ,
					[RELS_DT] ,
					[RELS_BY_USER_ID] ,
					[CREAT_DT]
				FROM	[COWS].[arch].[ORDR_HOLD_MS] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_HOLD_MS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_HOLD_MS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_HOLD_MS] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_HOLD_MS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_HOLD_MS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_JPRDY	ORDR_JPRDY	JPRDY_CD	LK_JPRDY	JPRDY_CD
				FK02_ORDR_JPRDY	ORDR_JPRDY	ORDR_ID		ORDR		ORDR_ID
				FK03_ORDR_JPRDY	ORDR_JPRDY	NTE_ID		ORDR_NTE	NTE_ID
				------------------------------------------------ok------------ORDER---------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_JPRDY] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_JPRDY](
					[ORDR_ID]  ,
					[JPRDY_CD]  ,
					[NTE_ID]  ,
					[CREAT_DT] )
				SELECT  [ORDR_ID]  ,
					[JPRDY_CD]  ,
					[NTE_ID]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[ORDR_JPRDY] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_JPRDY] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_JPRDY] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_JPRDY] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_JPRDY] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_JPRDY]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_ORDR_NTE	ORDR_NTE	NTE_TYPE_ID			LK_NTE_TYPE			NTE_TYPE_ID
				FK02_ORDR_NTE	ORDR_NTE	ORDR_ID				ORDR				ORDR_ID
				FK03_ORDR_NTE	ORDR_NTE	CREAT_BY_USER_ID	LK_USER				USER_ID
				FK04_ORDR_NTE	ORDR_NTE	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID
				FK05_ORDR_NTE	ORDR_NTE	MODFD_BY_USER_ID	LK_USER				USER_ID
				--------------------------------------ok---------------------ORDER---------------------------
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_CCD_HIST	CCD_HIST	NTE_ID	ORDR_NTE	NTE_ID
				FK03_ORDR_JPRDY	ORDR_JPRDY	NTE_ID	ORDR_NTE	NTE_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_NTE     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_NTE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_NTE](
					[NTE_ID]   ,
					[NTE_TYPE_ID]  ,
					[ORDR_ID]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[NTE_TXT]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] 
					)
				SELECT 
					[NTE_ID]   ,
					[NTE_TYPE_ID]  ,
					[ORDR_ID]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[NTE_TXT]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT]
				FROM	[COWS].[arch].[ORDR_NTE] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_NTE] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_NTE     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_NTE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE FROM	[COWS].[arch].[ORDR_NTE] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_NTE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_NTE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_MS	ORDR_MS	CREAT_BY_USER_ID	LK_USER	USER_ID
				FK02_ORDR_MS	ORDR_MS	ORDR_ID				ORDR	ORDR_ID
				---------------------------------------------ok-----------ORDER------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_MS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_MS](
					[ORDR_ID]  ,
					[VER_ID]  ,
					[PRE_SBMT_DT] ,
					[SBMT_DT] ,
					[VLDTD_DT] ,
					[ORDR_BILL_CLEAR_INSTL_DT] ,
					[ORDR_DSCNCT_DT] ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[VNDR_CNCLN_DT] ,
					[CUST_ACPTC_TURNUP_DT] ,
					[XNCI_CNFRM_RNL_DT] ,
					[RNL_DT_REQR_CD]  )
				SELECT  [ORDR_ID]  ,
					[VER_ID]  ,
					[PRE_SBMT_DT] ,
					[SBMT_DT] ,
					[VLDTD_DT] ,
					[ORDR_BILL_CLEAR_INSTL_DT] ,
					[ORDR_DSCNCT_DT] ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[VNDR_CNCLN_DT] ,
					[CUST_ACPTC_TURNUP_DT] ,
					[XNCI_CNFRM_RNL_DT] ,
					[RNL_DT_REQR_CD]
				FROM	[COWS].[arch].[ORDR_MS] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_MS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_MS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_MS] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_MS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_MS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_REC_LOCK	ORDR_REC_LOCK	ORDR_ID			ORDR	ORDR_ID
				FK02_ORDR_REC_LOCK	ORDR_REC_LOCK	LOCK_BY_USER_ID	LK_USER	USER_ID
				----------------------------------------ok------------ORDER-----------------------
				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_REC_LOCK] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_REC_LOCK](
					[ORDR_ID]  ,
					[STRT_REC_LOCK_TMST]  ,
					[LOCK_BY_USER_ID]  ,
					[CREAT_DT]  )
				SELECT  [ORDR_ID]  ,
					[STRT_REC_LOCK_TMST]  ,
					[LOCK_BY_USER_ID]  ,
					[CREAT_DT]
				FROM	[COWS].[arch].[ORDR_REC_LOCK] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_REC_LOCK] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_REC_LOCK] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_REC_LOCK] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_REC_LOCK] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_REC_LOCK]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_STDI_HIST	ORDR_STDI_HIST	ORDR_ID				ORDR			ORDR_ID
				FK02_ORDR_STDI_HIST	ORDR_STDI_HIST	STDI_REAS_ID		LK_STDI_REAS	STDI_REAS_ID
				FK03_ORDR_STDI_HIST	ORDR_STDI_HIST	CREAT_BY_USER_ID	LK_USER			USER_ID
				FK04_ORDR_STDI_HIST	ORDR_STDI_HIST	MODFD_BY_USER_ID	LK_USER			USER_ID
				-----------------------------------------------ok--------------ORDER--------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_STDI_HIST     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_STDI_HIST] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_STDI_HIST](
					[ORDR_STDI_ID]   ,
					[ORDR_ID]  ,
					[STDI_REAS_ID]  ,
					[CMNT_TXT] ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[CREAT_DT]  ,
					[MODFD_DT] ,
					[STDI_DT]  )
				SELECT  [ORDR_STDI_ID]   ,
					[ORDR_ID]  ,
					[STDI_REAS_ID]  ,
					[CMNT_TXT] ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[CREAT_DT]  ,
					[MODFD_DT] ,
					[STDI_DT]
				FROM	[COWS].[arch].[ORDR_STDI_HIST] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_STDI_HIST] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_STDI_HIST     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_STDI_HIST] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_STDI_HIST] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_STDI_HIST] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_STDI_HIST]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_ORDR_VLAN	ORDR_VLAN	ORDR_ID				ORDR		ORDR_ID
				FK02_ORDR_VLAN	ORDR_VLAN	VLAN_SRC_NME		LK_APP_SRC	APP_SRC_NME
				FK03_ORDR_VLAN	ORDR_VLAN	CREAT_BY_USER_ID	LK_USER		USER_ID
				-------------------------------------------ok---------------ORDER------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_VLAN     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[ORDR_VLAN] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR_VLAN](
					[ORDR_VLAN_ID]   ,
					[ORDR_ID]  ,
					[VLAN_ID]  ,
					[VLAN_SRC_NME] ,
					[VLAN_PCT_QTY]  ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[TRMTG_CD] )
				SELECT  [ORDR_VLAN_ID]   ,
					[ORDR_ID]  ,
					[VLAN_ID]  ,
					[VLAN_SRC_NME] ,
					[VLAN_PCT_QTY]  ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  ,
					[TRMTG_CD]
				FROM	[COWS].[arch].[ORDR_VLAN] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR_VLAN] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR_VLAN     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[ORDR_VLAN] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[ORDR_VLAN] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR_VLAN] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_VLAN]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
				
				
				
				SET IDENTITY_INSERT  [COWS].[dbo].SSTAT_REQ     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SSTAT_REQ] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[SSTAT_REQ](
					   [TRAN_ID]
					  ,[ORDR_ID]
					  ,[SSTAT_MSG_ID]
					  ,[STUS_MOD_DT]
					  ,[CREAT_DT]
					  ,[STUS_ID]
					  ,[FTN] )
				SELECT [TRAN_ID]
					  ,[ORDR_ID]
					  ,[SSTAT_MSG_ID]
					  ,[STUS_MOD_DT]
					  ,[CREAT_DT]
					  ,[STUS_ID]
					  ,[FTN]
				FROM	[COWS].[arch].[SSTAT_REQ] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SSTAT_REQ] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].SSTAT_REQ     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SSTAT_REQ] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[SSTAT_REQ] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SSTAT_REQ] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SSTAT_REQ]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
				
				SET IDENTITY_INSERT  [COWS].[dbo].SSTAT_RSPN     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SSTAT_RSPN] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[SSTAT_RSPN](
					   [SSTAT_RSPN_ID]
					  ,[TRAN_ID]
					  ,[ORDR_ID]
					  ,[DEVICE_ID]
					  ,[ORDER_ACTION]
					  ,[COMPLETION_DATE]
					  ,[NOTE]
					  ,[ERROR_MSG]
					  ,[CREAT_DT]
					  ,[ACK_CD]
					  ,[ACT_CD] )
				SELECT [SSTAT_RSPN_ID]
					  ,[TRAN_ID]
					  ,[ORDR_ID]
					  ,[DEVICE_ID]
					  ,[ORDER_ACTION]
					  ,[COMPLETION_DATE]
					  ,[NOTE]
					  ,[ERROR_MSG]
					  ,[CREAT_DT]
					  ,[ACK_CD]
					  ,[ACT_CD]
				FROM	[COWS].[arch].[SSTAT_RSPN] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT FTN FROM	@FTN)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SSTAT_RSPN] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].SSTAT_RSPN     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SSTAT_RSPN] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[SSTAT_RSPN] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT FTN FROM	@FTN)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SSTAT_RSPN] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SSTAT_RSPN]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			
			
				SET IDENTITY_INSERT  [COWS].[dbo].PS_RCPT_QUEUE     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[PS_RCPT_QUEUE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[PS_RCPT_QUEUE](
					   [PO_RCVD_ID]
					  ,[PRCH_ORDR_NBR]
					  ,[ORDR_ID]
					  ,[REQSTN_NBR]
					  ,[FSA_CPE_LINE_ITEM_ID]
					  ,[EQPT_RCVD_BY_ADID]
					  ,[RCVD_QTY]
					  ,[PS_RCVD_STUS]
					  ,[PS_SENT_DT]
					  ,[CREAT_DT]
					  ,[RCVD_DT]
					  ,[BATCH_SEQ] )
				SELECT [PO_RCVD_ID]
					  ,[PRCH_ORDR_NBR]
					  ,[ORDR_ID]
					  ,[REQSTN_NBR]
					  ,[FSA_CPE_LINE_ITEM_ID]
					  ,[EQPT_RCVD_BY_ADID]
					  ,[RCVD_QTY]
					  ,[PS_RCVD_STUS]
					  ,[PS_SENT_DT]
					  ,[CREAT_DT]
					  ,[RCVD_DT]
					  ,[BATCH_SEQ]
				FROM	[COWS].[arch].[PS_RCPT_QUEUE] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[PS_RCPT_QUEUE] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].PS_RCPT_QUEUE     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[PS_RCPT_QUEUE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[PS_RCPT_QUEUE] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[PS_RCPT_QUEUE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[PS_RCPT_QUEUE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			
				
				SET IDENTITY_INSERT  [COWS].[dbo].PS_REQ_HDR_QUEUE     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[PS_REQ_HDR_QUEUE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[PS_REQ_HDR_QUEUE](
					   [REQ_HDR_ID]
					  ,[ORDR_ID]
					  ,[REQSTN_NBR]
					  ,[CUST_ELID]
					  ,[DLVY_CLLI]
					  ,[DLVY_NME]
					  ,[DLVY_ADDR1]
					  ,[DLVY_ADDR2]
					  ,[DLVY_ADDR3]
					  ,[DLVY_ADDR4]
					  ,[DLVY_CTY]
					  ,[DLVY_CNTY]
					  ,[DLVY_ST]
					  ,[DLVY_ZIP]
					  ,[DLVY_PHN_NBR]
					  ,[INST_CLLI]
					  ,[MRK_PKG]
					  ,[REQSTN_DT]
					  ,[REF_NBR]
					  ,[SHIP_CMMTS]
					  ,[RFQ_INDCTR]
					  ,[REC_STUS_ID]
					  ,[CREAT_DT]
					  ,[SENT_DT]
					  ,[DLVY_BLDG]
					  ,[DLVY_FLR]
					  ,[DLVY_RM]
					  ,[CSG_LVL]
					  ,[BATCH_SEQ] )
				SELECT [REQ_HDR_ID]
					  ,[ORDR_ID]
					  ,[REQSTN_NBR]
					  ,[CUST_ELID]
					  ,[DLVY_CLLI]
					  ,[DLVY_NME]
					  ,[DLVY_ADDR1]
					  ,[DLVY_ADDR2]
					  ,[DLVY_ADDR3]
					  ,[DLVY_ADDR4]
					  ,[DLVY_CTY]
					  ,[DLVY_CNTY]
					  ,[DLVY_ST]
					  ,[DLVY_ZIP]
					  ,[DLVY_PHN_NBR]
					  ,[INST_CLLI]
					  ,[MRK_PKG]
					  ,[REQSTN_DT]
					  ,[REF_NBR]
					  ,[SHIP_CMMTS]
					  ,[RFQ_INDCTR]
					  ,[REC_STUS_ID]
					  ,[CREAT_DT]
					  ,[SENT_DT]
					  ,[DLVY_BLDG]
					  ,[DLVY_FLR]
					  ,[DLVY_RM]
					  ,[CSG_LVL]
					  ,[BATCH_SEQ]
				FROM	[COWS].[arch].[PS_REQ_HDR_QUEUE] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[PS_REQ_HDR_QUEUE] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].PS_REQ_HDR_QUEUE     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[PS_REQ_HDR_QUEUE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[PS_REQ_HDR_QUEUE] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[PS_REQ_HDR_QUEUE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[PS_REQ_HDR_QUEUE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			
				
				SET IDENTITY_INSERT  [COWS].[dbo].PS_REQ_LINE_ITM_QUEUE     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[PS_REQ_LINE_ITM_QUEUE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[PS_REQ_LINE_ITM_QUEUE](
					   [REQ_LINE_ITM_ID]
					  ,[ORDR_ID]
					  ,[CMPNT_ID]
					  ,[REQSTN_NBR]
					  ,[MAT_CD]
					  ,[DLVY_CLLI]
					  ,[ITM_DES]
					  ,[MANF_PART_NBR]
					  ,[MANF_ID]
					  ,[ORDR_QTY]
					  ,[UNT_MSR]
					  ,[UNT_PRICE]
					  ,[RAS_DT]
					  ,[VNDR_NME]
					  ,[BUS_UNT_GL]
					  ,[ACCT]
					  ,[COST_CNTR]
					  ,[PRODCT]
					  ,[MRKT]
					  ,[AFFLT]
					  ,[REGN]
					  ,[PROJ_ID]
					  ,[BUS_UNT_PC]
					  ,[ACTVY]
					  ,[SOURCE_TYP]
					  ,[RSRC_CAT]
					  ,[RSRC_SUB]
					  ,[CNTRCT_ID]
					  ,[CNTRCT_LN_NBR]
					  ,[AXLRY_ID]
					  ,[INST_CD]
					  ,[REC_STUS_ID]
					  ,[CREAT_DT]
					  ,[SENT_DT]
					  ,[FSA_CPE_LINE_ITEM_ID]
					  ,[EQPT_TYPE_ID]
					  ,[COMMENTS]
					  ,[MANF_DISCNT_CD]
					  ,[REQ_LINE_NBR])
				SELECT [REQ_LINE_ITM_ID]
					  ,[ORDR_ID]
					  ,[CMPNT_ID]
					  ,[REQSTN_NBR]
					  ,[MAT_CD]
					  ,[DLVY_CLLI]
					  ,[ITM_DES]
					  ,[MANF_PART_NBR]
					  ,[MANF_ID]
					  ,[ORDR_QTY]
					  ,[UNT_MSR]
					  ,[UNT_PRICE]
					  ,[RAS_DT]
					  ,[VNDR_NME]
					  ,[BUS_UNT_GL]
					  ,[ACCT]
					  ,[COST_CNTR]
					  ,[PRODCT]
					  ,[MRKT]
					  ,[AFFLT]
					  ,[REGN]
					  ,[PROJ_ID]
					  ,[BUS_UNT_PC]
					  ,[ACTVY]
					  ,[SOURCE_TYP]
					  ,[RSRC_CAT]
					  ,[RSRC_SUB]
					  ,[CNTRCT_ID]
					  ,[CNTRCT_LN_NBR]
					  ,[AXLRY_ID]
					  ,[INST_CD]
					  ,[REC_STUS_ID]
					  ,[CREAT_DT]
					  ,[SENT_DT]
					  ,[FSA_CPE_LINE_ITEM_ID]
					  ,[EQPT_TYPE_ID]
					  ,[COMMENTS]
					  ,[MANF_DISCNT_CD]
					  ,[REQ_LINE_NBR]
				FROM	[COWS].[arch].[PS_REQ_LINE_ITM_QUEUE] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[PS_REQ_LINE_ITM_QUEUE] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].PS_REQ_LINE_ITM_QUEUE     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[PS_REQ_LINE_ITM_QUEUE] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[PS_REQ_LINE_ITM_QUEUE] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[PS_REQ_LINE_ITM_QUEUE] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[PS_REQ_LINE_ITM_QUEUE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			
			END	
			/*
			[owssvr$]
			------------------------------------------------OK------------------EEVNT--------------------------
			*/
			/*
			PRINT 'Starting to execute [arch].[owssvr$] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[owssvr$](
				[Item Title]  ,
				[Event_ID]  ,
				[Bridge Number]  ,
				[Bridge PIN]  ,
				[Circuit Number]  ,
				[Created] ,
				[Created By]  ,
				[Customer Name]  ,
				[H1]  ,
				[H6]  ,
				[Workflow Status]  ,
				[Carrier Partner]  ,
				[OFF Net Monitoring?]  ,
				[Event Status]  ,
				[Start Time] ,
				[SOWS_ADMIN_SCRIPT]  ,
				[Scheduled_Implementation_Start_WF]  ,
				[Item Type]  ,
				[Path] )
			SELECT  [Item Title]  ,
				[Event_ID]  ,
				[Bridge Number]  ,
				[Bridge PIN]  ,
				[Circuit Number]  ,
				[Created] ,
				[Created By]  ,
				[Customer Name]  ,
				[H1]  ,
				[H6]  ,
				[Workflow Status]  ,
				[Carrier Partner]  ,
				[OFF Net Monitoring?]  ,
				[Event Status]  ,
				[Start Time] ,
				[SOWS_ADMIN_SCRIPT]  ,
				[Scheduled_Implementation_Start_WF]  ,
				[Item Type]  ,
				[Path]	
			FROM	[COWS].[dbo].[owssvr$] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			PRINT 'Finished executing [arch].[owssvr$] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[owssvr$] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM	[COWS].[dbo].[owssvr$] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			PRINT 'Finished executing [dbo].[owssvr$] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[owssvr$]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_PLTFRM_MAPNG	PLTFRM_MAPNG	PLTFRM_CD			LK_PLTFRM			PLTFRM_CD
			FK02_PLTFRM_MAPNG	PLTFRM_MAPNG	PROD_NME			LK_PROD				PROD_NME
			FK03_PLTFRM_MAPNG	PLTFRM_MAPNG	FSA_PROD_TYPE_CD	LK_FSA_PROD_TYPE	FSA_PROD_TYPE_CD
			rollback
			*/
			/*
			PRINT 'Starting to execute [arch].[PLTFRM_MAPNG] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[PLTFRM_MAPNG](
				[PLTFRM_MAPNG_ID]   ,
				[PLTFRM_CD] ,
				[PROD_NME]  ,
				[FSA_PROD_TYPE_CD] ,
				[CXR_PTNR_AVAL_CD]  ,
				[CREAT_DT]  )
			SELECT [PLTFRM_MAPNG_ID]   ,
				[PLTFRM_CD] ,
				[PROD_NME]  ,
				[FSA_PROD_TYPE_CD] ,
				[CXR_PTNR_AVAL_CD]  ,
				[CREAT_DT]
			FROM   COWS.dbo.[PLTFRM_MAPNG] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[PLTFRM_MAPNG] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[PLTFRM_MAPNG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[PLTFRM_MAPNG] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[PLTFRM_MAPNG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[PLTFRM_MAPNG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE		REFERANCE_COLUMN_NAME
			FK01_PROD_ORDR_TYPE	PROD_ORDR_TYPE	PROD_TYPE_ID		LK_PROD_TYPE	PROD_TYPE_ID
			FK02_PROD_ORDR_TYPE	PROD_ORDR_TYPE	ORDR_TYPE_ID		LK_ORDR_TYPE	ORDR_TYPE_ID
			FK03_PROD_ORDR_TYPE	PROD_ORDR_TYPE	CREAT_BY_USER_ID	LK_USER			USER_ID
			FK04_PROD_ORDR_TYPE	PROD_ORDR_TYPE	REC_STUS_ID			LK_REC_STUS		REC_STUS_ID
			
			*/
			/*
			PRINT 'Starting to execute [arch].[PROD_ORDR_TYPE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[PROD_ORDR_TYPE](
				[PROD_TYPE_ID]  ,
				[ORDR_TYPE_ID]  ,
				[CREAT_DT]  ,
				[CREAT_BY_USER_ID]  ,
				[REC_STUS_ID]  )
			SELECT [PROD_TYPE_ID]  ,
				[ORDR_TYPE_ID]  ,
				[CREAT_DT]  ,
				[CREAT_BY_USER_ID]  ,
				[REC_STUS_ID]	
			FROM   COWS.dbo.[PROD_ORDR_TYPE] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[PROD_ORDR_TYPE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[PROD_ORDR_TYPE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[PROD_ORDR_TYPE] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			Set @RowCounts=@@RowCount PRINT 'Finished executing [dbo].[PROD_ORDR_TYPE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@ROWCOUNTS) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[PROD_ORDR_TYPE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY				TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK02_QLFCTN_DEDCTD_CUST	QLFCTN_DEDCTD_CUST	QLFCTN_ID		LK_QLFCTN		QLFCTN_ID
			FK05_QLFCTN_DEDCTD_CUST	QLFCTN_DEDCTD_CUST	CUST_ID			LK_DEDCTD_CUST	CUST_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[QLFCTN_DEDCTD_CUST] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[QLFCTN_DEDCTD_CUST](
				[QLFCTN_ID]  ,
				[CUST_ID]  ,
				[CREAT_DT]  )
			SELECT [QLFCTN_ID]  ,
				[CUST_ID]  ,
				[CREAT_DT]
			FROM   COWS.dbo.[QLFCTN_DEDCTD_CUST] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[QLFCTN_DEDCTD_CUST] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[QLFCTN_DEDCTD_CUST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[QLFCTN_DEDCTD_CUST] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[QLFCTN_DEDCTD_CUST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[QLFCTN_DEDCTD_CUST]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK04_QLFCTN_DEV	QLFCTN_DEV	QLFCTN_ID	LK_QLFCTN	QLFCTN_ID
			FK05_QLFCTN_DEV	QLFCTN_DEV	DEV_ID		LK_DEV		DEV_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[QLFCTN_DEV] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO  [arch].[QLFCTN_DEV](
				[QLFCTN_ID]  ,
				[DEV_ID]  ,
				[CREAT_DT]  )
			SELECT [QLFCTN_ID]  ,
				[DEV_ID]  ,
				[CREAT_DT]	
			FROM   COWS.dbo.[QLFCTN_DEV] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[QLFCTN_DEV] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[QLFCTN_DEV] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[QLFCTN_DEV] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[QLFCTN_DEV] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[QLFCTN_DEV]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK04_QLFCTN_ENHNC_SRVC	QLFCTN_ENHNC_SRVC	QLFCTN_ID		LK_QLFCTN		QLFCTN_ID
			FK05_QLFCTN_ENHNC_SRVC	QLFCTN_ENHNC_SRVC	ENHNC_SRVC_ID	LK_ENHNC_SRVC	ENHNC_SRVC_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[QLFCTN_ENHNC_SRVC] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[QLFCTN_ENHNC_SRVC](
				[QLFCTN_ID]  ,
				[ENHNC_SRVC_ID]  ,
				[CREAT_DT]  )
			SELECT  [QLFCTN_ID]  ,
				[ENHNC_SRVC_ID]  ,
				[CREAT_DT]
			FROM   COWS.dbo.[QLFCTN_ENHNC_SRVC] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[QLFCTN_ENHNC_SRVC] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[QLFCTN_ENHNC_SRVC] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[QLFCTN_ENHNC_SRVC] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[QLFCTN_ENHNC_SRVC] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[QLFCTN_ENHNC_SRVC]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK04_QLFCTN_EVENT_TYPE	QLFCTN_EVENT_TYPE	QLFCTN_ID		LK_QLFCTN		QLFCTN_ID
			FK05_QLFCTN_EVENT_TYPE	QLFCTN_EVENT_TYPE	EVENT_TYPE_ID	LK_EVENT_TYPE	EVENT_TYPE_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[QLFCTN_EVENT_TYPE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[QLFCTN_EVENT_TYPE](
				[EVENT_TYPE_ID]  ,
				[QLFCTN_ID]  ,
				[CREAT_DT] )
			SELECT [EVENT_TYPE_ID]  ,
				[QLFCTN_ID]  ,
				[CREAT_DT]	
			FROM   COWS.dbo.[QLFCTN_EVENT_TYPE] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[QLFCTN_EVENT_TYPE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[QLFCTN_EVENT_TYPE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[QLFCTN_EVENT_TYPE] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[QLFCTN_EVENT_TYPE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[QLFCTN_EVENT_TYPE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			[arch].[QLFCTN_IP_VER]
			*/
			/*
			PRINT 'Starting to execute [arch].[QLFCTN_IP_VER] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[QLFCTN_IP_VER](
				[QLFCTN_ID]  ,
				[IP_VER_ID]  ,
				[CREAT_DT]  )
			SELECT [QLFCTN_ID]  ,
				[IP_VER_ID]  ,
				[CREAT_DT]	
			FROM   COWS.dbo.[QLFCTN_IP_VER] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[QLFCTN_IP_VER] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[QLFCTN_IP_VER] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[QLFCTN_IP_VER] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[QLFCTN_IP_VER] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[QLFCTN_IP_VER]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK04_QLFCTN_SPCL_PROJ	QLFCTN_SPCL_PROJ	SPCL_PROJ_ID	LK_SPCL_PROJ	SPCL_PROJ_ID
			FK05_QLFCTN_SPCL_PROJ	QLFCTN_SPCL_PROJ	QLFCTN_ID		LK_QLFCTN		QLFCTN_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[QLFCTN_SPCL_PROJ] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[QLFCTN_SPCL_PROJ](
				[QLFCTN_ID]  ,
				[SPCL_PROJ_ID]  ,
				[CREAT_DT]  )
			SELECT [QLFCTN_ID]  ,
				[SPCL_PROJ_ID]  ,
				[CREAT_DT]	
			FROM   COWS.dbo.[QLFCTN_SPCL_PROJ] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[QLFCTN_SPCL_PROJ] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[QLFCTN_SPCL_PROJ] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[QLFCTN_SPCL_PROJ] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[QLFCTN_SPCL_PROJ] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[QLFCTN_SPCL_PROJ]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK04_QLFCTN_VNDR_MODEL	QLFCTN_VNDR_MODEL	QLFCTN_ID		LK_QLFCTN		QLFCTN_ID
			FK05_QLFCTN_VNDR_MODEL	QLFCTN_VNDR_MODEL	DEV_MODEL_ID	LK_DEV_MODEL	DEV_MODEL_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[QLFCTN_VNDR_MODEL] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO [arch].[QLFCTN_VNDR_MODEL](
				[QLFCTN_ID]  ,
				[DEV_MODEL_ID]  ,
				[CREAT_DT]   )
			SELECT [QLFCTN_ID]  ,
				[DEV_MODEL_ID]  ,
				[CREAT_DT]	
			FROM   COWS.dbo.[QLFCTN_VNDR_MODEL] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[QLFCTN_VNDR_MODEL] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[QLFCTN_VNDR_MODEL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[QLFCTN_VNDR_MODEL] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[QLFCTN_VNDR_MODEL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[QLFCTN_VNDR_MODEL]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			FOREIGN_KEY	TABLE_NAME		COLUMN_NAME				REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_SM		SM				PRE_REQST_WG_PTRN_ID	LK_WG_PTRN		WG_PTRN_ID
			FK02_SM		SM				DESRD_WG_PTRN_ID		LK_WG_PTRN		WG_PTRN_ID
			FK03_SM		SM				CREAT_BY_USER_ID		LK_USER			USER_ID
			FK04_SM		SM				SM_ID					LK_SM			SM_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[SM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			INSERT INTO  [arch].[SM](
				[SM_ID]  ,
				[PRE_REQST_WG_PTRN_ID]  ,
				[DESRD_WG_PTRN_ID]  ,
				[CREAT_BY_USER_ID]  ,
				[CREAT_DT] )
			SELECT [SM_ID]  ,
				[PRE_REQST_WG_PTRN_ID]  ,
				[DESRD_WG_PTRN_ID]  ,
				[CREAT_BY_USER_ID]  ,
				[CREAT_DT] 	
			FROM   COWS.dbo.[SM] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[SM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[SM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[SM] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[SM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SM]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			
			IF(@Mood=4)  /* SPLK EVNT ONLY */
			BEGIN
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_SPLK_EVENT_ACCS	SPLK_EVENT_ACCS	EVENT_ID	SPLK_EVENT	EVENT_ID
				------------------------------------------------------------OK----------------------EVENT----------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. SPLK_EVENT_ACCS     ON
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SPLK_EVENT_ACCS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO  [dbo].[SPLK_EVENT_ACCS](
					[SPLK_EVENT_ACCS_ID]   ,
					[EVENT_ID]  ,
					[OLD_PL_NBR]  ,
					[OLD_PORT_SPEED_DES]  ,
					[NEW_PL_NBR]  ,
					[NEW_PORT_SPEED_DES]  ,
					[CREAT_DT]  ,
					[OLD_VPI_VCI_DLCI_NME]  ,
					[NEW_VPI_VCI_DLCI_NME]  )
					
				SELECT  [SPLK_EVENT_ACCS_ID]   ,
					[EVENT_ID]  ,
					[OLD_PL_NBR]  ,
					[OLD_PORT_SPEED_DES]  ,
					[NEW_PL_NBR]  ,
					[NEW_PORT_SPEED_DES]  ,
					[CREAT_DT]  ,
					[OLD_VPI_VCI_DLCI_NME]  ,
					[NEW_VPI_VCI_DLCI_NME] 
				FROM	[COWS].[arch].[SPLK_EVENT_ACCS] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SPLK_EVENT_ACCS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. SPLK_EVENT_ACCS     OFF
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SPLK_EVENT_ACCS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[SPLK_EVENT_ACCS] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SPLK_EVENT_ACCS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SPLK_EVENT_ACCS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_SPLK_EVENT	SPLK_EVENT	WRKFLW_STUS_ID		LK_WRKFLW_STUS	WRKFLW_STUS_ID
				FK02_SPLK_EVENT	SPLK_EVENT	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID
				FK03_SPLK_EVENT	SPLK_EVENT	EVENT_STUS_ID		LK_EVENT_STUS	EVENT_STUS_ID
				FK04_SPLK_EVENT	SPLK_EVENT	CREAT_BY_USER_ID	LK_USER	USER_ID
				FK05_SPLK_EVENT	SPLK_EVENT	SALS_USER_ID		LK_USER	USER_ID
				FK06_SPLK_EVENT	SPLK_EVENT	REQOR_USER_ID		LK_USER	USER_ID
				FK07_SPLK_EVENT	SPLK_EVENT	MODFD_BY_USER_ID	LK_USER	USER_ID
				FK08_SPLK_EVENT	SPLK_EVENT	EVENT_ID			EVENT	EVENT_ID
				FK09_SPLK_EVENT	SPLK_EVENT	IP_VER_ID			LK_IP_VER	IP_VER_ID
				FK10_SPLK_EVENT	SPLK_EVENT	SPLK_EVENT_TYPE_ID	LK_SPLK_EVENT_TYPE	SPLK_EVENT_TYPE_ID
				FK11_SPLK_EVENT	SPLK_EVENT	SPLK_ACTY_TYPE_ID	LK_SPLK_ACTY_TYPE	SPLK_ACTY_TYPE_ID
				FK12_SPLK_EVENT	SPLK_EVENT	ESCL_REAS_ID		LK_ESCL_REAS	ESCL_REAS_ID
				--------------------------------------------OK----------------------------EVENT----------------

				*/
				Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SPLK_EVENT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[SPLK_EVENT](
					[EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[FTN]  ,
					[CHARS_ID] ,
					[H1]  ,
					[H6]  ,
					[REQOR_USER_ID] ,
					[SALS_USER_ID] ,
					[PUB_EMAIL_CC_TXT]  ,
					[CMPLTD_EMAIL_CC_TXT]  ,
					[DSGN_CMNT_TXT]  ,
					[SPLK_EVENT_TYPE_ID]  ,
					[SPLK_ACTY_TYPE_ID]  ,
					[IP_VER_ID]  ,
					[MDS_MNGD_CD]  ,
					[ESCL_CD]  ,
					[PRIM_REQ_DT] ,
					[SCNDY_REQ_DT] ,
					[ESCL_REAS_ID]  ,
					[STRT_TMST]  ,
					[EXTRA_DRTN_TME_AMT]  ,
					[END_TMST]  ,
					[WRKFLW_STUS_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[CNFRC_BRDG_NBR]  ,
					[CNFRC_PIN_NBR] ,
					[EVENT_DRTN_IN_MIN_QTY]  ,
					[SOWS_EVENT_ID] ,
					[RELTD_CMPS_NCR_CD]  ,
					[RELTD_CMPS_NCR_NME]  ,
					[CUST_NME]  ,
					[CUST_CNTCT_NME]  ,
					[CUST_CNTCT_PHN_NBR] ,
					[CUST_EMAIL_ADR] ,
					[CUST_CNTCT_CELL_PHN_NBR] ,
					[CUST_CNTCT_PGR_NBR] ,
					[CUST_CNTCT_PGR_PIN_NBR] ,
					[EVENT_TITLE_TXT],
					[EVENT_DES] )
				SELECT	[EVENT_ID]  ,
					[EVENT_STUS_ID]  ,
					[FTN]  ,
					[CHARS_ID] ,
					[H1]  ,
					[H6]  ,
					[REQOR_USER_ID] ,
					[SALS_USER_ID] ,
					[PUB_EMAIL_CC_TXT]  ,
					[CMPLTD_EMAIL_CC_TXT]  ,
					[DSGN_CMNT_TXT]  ,
					[SPLK_EVENT_TYPE_ID]  ,
					[SPLK_ACTY_TYPE_ID]  ,
					[IP_VER_ID]  ,
					[MDS_MNGD_CD]  ,
					[ESCL_CD]  ,
					[PRIM_REQ_DT] ,
					[SCNDY_REQ_DT] ,
					[ESCL_REAS_ID]  ,
					[STRT_TMST]  ,
					[EXTRA_DRTN_TME_AMT]  ,
					[END_TMST]  ,
					[WRKFLW_STUS_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[CNFRC_BRDG_NBR]  ,
					[CNFRC_PIN_NBR] ,
					[EVENT_DRTN_IN_MIN_QTY]  ,
					[SOWS_EVENT_ID] ,
					[RELTD_CMPS_NCR_CD]  ,
					[RELTD_CMPS_NCR_NME]  ,
					[CUST_NME]  ,
					[CUST_CNTCT_NME]  ,
					[CUST_CNTCT_PHN_NBR] ,
					[CUST_EMAIL_ADR] ,
					[CUST_CNTCT_CELL_PHN_NBR] ,
					[CUST_CNTCT_PGR_NBR] ,
					[CUST_CNTCT_PGR_PIN_NBR] ,
					[EVENT_TITLE_TXT],
					[EVENT_DES]
				FROM	[COWS].[arch].[SPLK_EVENT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SPLK_EVENT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate() RAISERROR ('Starting to execute [arch].[SPLK_EVENT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[SPLK_EVENT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SPLK_EVENT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SPLK_EVENT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			END	
			--IF(@Mood<>0)  /* FOR ALL EVENT ONLY  NO ORDR */
			IF(@Mood=1 OR @Mood=2 OR @Mood=3 OR @Mood=4 OR @Mood=5 OR @Mood=6)
			BEGIN
				/*
				EVENT
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_EVENT	EVENT	EVENT_TYPE_ID	LK_EVENT_TYPE	EVENT_TYPE_ID
				---------------------------------------------OK--------------------EVENT---------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. EVENT     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[EVENT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[EVENT](
						[EVENT_ID]   ,
						[EVENT_TYPE_ID]  ,
						[CREAT_DT]  ,
						--[SCURD_CD]  ,
						[MODFD_DT],[CSG_LVL_ID] )
				SELECT  [EVENT_ID]   ,
						[EVENT_TYPE_ID]  ,
						[CREAT_DT]  ,
						--[SCURD_CD]  ,
						[MODFD_DT],[CSG_LVL_ID]
				FROM	[COWS].[arch].[EVENT] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[EVENT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. EVENT     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[EVENT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[EVENT] WITH(TABLOCKX)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[EVENT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[EVENT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
			END
			/*
			FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
			FK01_SRC_CUR_FILE	SRC_CUR_FILE	CREAT_BY_USER_ID	LK_USER		USER_ID
			FK02_SRC_CUR_FILE	SRC_CUR_FILE	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID

			*/
			/*
			PRINT 'Starting to execute [arch].[SRC_CUR_FILE] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
			INSERT INTO [arch].[SRC_CUR_FILE](
				[SRC_CUR_FILE_ID]   ,
				[FILE_NME]  ,
				[FILE_CNTNT_BTSM]  ,
				[PRSD_CUR_LIST_TXT]  ,
				[PRCSD_DT] ,
				[REC_STUS_ID]  ,
				[CREAT_BY_USER_ID]  ,
				[CREAT_DT]    ,
				[PRE_REQST_WG_PTRN_ID]  ,
				[DESRD_WG_PTRN_ID]  ,
				[CREAT_BY_USER_ID]  ,
				[CREAT_DT] )
				
			SELECT [SRC_CUR_FILE_ID]   ,
				[FILE_NME]  ,
				[FILE_CNTNT_BTSM]  ,
				[PRSD_CUR_LIST_TXT]  ,
				[PRCSD_DT] ,
				[REC_STUS_ID]  ,
				[CREAT_BY_USER_ID]  ,
				[CREAT_DT]    ,
				[PRE_REQST_WG_PTRN_ID]  ,
				[DESRD_WG_PTRN_ID]  ,
				[CREAT_BY_USER_ID]  ,
				[CREAT_DT]		
			FROM   COWS.dbo.[SRC_CUR_FILE] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [arch].[SRC_CUR_FILE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [dbo].[SRC_CUR_FILE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE  FROM   COWS.dbo.[SRC_CUR_FILE] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
			PRINT 'Finished executing [dbo].[SRC_CUR_FILE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SRC_CUR_FILE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/
			/*
			[TestCase]
			*/
			/*
			GO
			CREATE TABLE [arch].[TestCase](
				[ID]   ,
				[Field1] ,
				[Field2]  ,
				[Fi�ld3]  ,
				[Fi�ld4] [char](10) ,
				[Field5] ,
				[Field6] [image] ,
			PRIMARY KEY CLUSTERED 
			(
				[ID] ASC
			)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
			) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
			GO
			SET ANSI_PADDING OFF
			GO
			*/
			IF(@Mood=0 )  /* ORDR ONLY */
			BEGIN
				/*
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME						REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_TRPT_ORDR	TRPT_ORDR	CCD_MISSD_REAS_ID				LK_CCD_MISSD_REAS	CCD_MISSD_REAS_ID
				FK02_TRPT_ORDR	TRPT_ORDR	CUST_BILL_CUR_ID				LK_CUR				CUR_ID
				FK03_TRPT_ORDR	TRPT_ORDR	IPT_AVLBLTY_ID					LK_IPT_AVLBLTY		IPT_AVLBLTY_ID
				FK04_TRPT_ORDR	TRPT_ORDR	REC_STUS_ID						LK_REC_STUS			REC_STUS_ID
				FK05_TRPT_ORDR	TRPT_ORDR	A_END_RGN_ID					LK_XNCI_RGN			RGN_ID
				FK06_TRPT_ORDR	TRPT_ORDR	SALS_CHNL_ID					LK_SALS_CHNL		SALS_CHNL_ID
				FK07_TRPT_ORDR	TRPT_ORDR	XNCI_ASN_MGR_ID					LK_USER				USER_ID
				FK08_TRPT_ORDR	TRPT_ORDR	VNDR_TYPE_ID					LK_VNDR_TYPE		VNDR_TYPE_ID
				FK09_TRPT_ORDR	TRPT_ORDR	ORDR_ID							ORDR				ORDR_ID
				FK10_TRPT_ORDR	TRPT_ORDR	CREAT_BY_USER_ID				LK_USER				USER_ID
				FK11_TRPT_ORDR	TRPT_ORDR	MODFD_BY_USER_ID				K_USER				USER_ID
				FK12_TRPT_ORDR	TRPT_ORDR	VNDR_LOCAL_ACCS_PRVDR_GRP_NME	LK_VNDR_LOCAL_ACCS_PRVDR_GRP	VNDR_LOCAL_ACCS_PRVDR_GRP_NME
				FK13_TRPT_ORDR	TRPT_ORDR	ACCS_CTY_SITE_ID				LK_ACCS_CTY_SITE	ACCS_CTY_SITE_ID
				FK14_TRPT_ORDR	TRPT_ORDR	STDI_REAS_ID					LK_STDI_REAS	S	TDI_REAS_ID
				--------------------------------------------ok---------------------ORDER--------------------
				*/
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[TRPT_ORDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[TRPT_ORDR](
					[ORDR_ID]  ,
					[REC_STUS_ID]  ,
					[CMNT_TXT] ,
					[SALS_CHNL_ID] ,
					[XNCI_ASN_MGR_ID] ,
					[CCD_MISSD_REAS_ID] ,
					[ORDR_ASN_VW_DT] ,
					[CUST_ACPTD_DT] ,
					[BILL_CLEAR_DT] ,
					[CUST_CNTRC_XPIR_DT] ,
					[CKT_LGTH_IN_KMS_QTY] ,
					[ACCS_IN_MB_VALU_QTY] ,
					[A_END_SPCL_CUST_ACCS_DETL_TXT]  ,
					[A_END_NEW_CKT_CD]  ,
					[A_END_RGN_ID] ,
					[A_END_REGION] ,
					[B_END_SPCL_CUST_ACCS_DETL_TXT]  ,
					[B_END_NEW_CKT_CD]  ,
					[VNDR_LOCAL_ACCS_PRVDR_GRP_NME]  ,
					[VNDR_TYPE_ID] ,
					[IPT_AVLBLTY_ID] ,
					[CUST_BILL_CUR_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[VNDR_ORDR_ID]  ,
					[TOT_HOLD_DRTN_TME_TXT] ,
					[ACCS_CTY_SITE_ID] ,
					[NRM_UPDT_CD]  ,
					[CNFRM_RNL_DT] ,
					[ACCS_MB_DES]  ,
					[STDI_DT] ,
					[STDI_REAS_ID]  )
				SELECT  [ORDR_ID]  ,
					[REC_STUS_ID]  ,
					[CMNT_TXT] ,
					[SALS_CHNL_ID] ,
					[XNCI_ASN_MGR_ID] ,
					[CCD_MISSD_REAS_ID] ,
					[ORDR_ASN_VW_DT] ,
					[CUST_ACPTD_DT] ,
					[BILL_CLEAR_DT] ,
					[CUST_CNTRC_XPIR_DT] ,
					[CKT_LGTH_IN_KMS_QTY] ,
					[ACCS_IN_MB_VALU_QTY] ,
					[A_END_SPCL_CUST_ACCS_DETL_TXT]  ,
					[A_END_NEW_CKT_CD]  ,
					[A_END_RGN_ID] ,
					[A_END_REGION] ,
					[B_END_SPCL_CUST_ACCS_DETL_TXT]  ,
					[B_END_NEW_CKT_CD]  ,
					[VNDR_LOCAL_ACCS_PRVDR_GRP_NME]  ,
					[VNDR_TYPE_ID] ,
					[IPT_AVLBLTY_ID] ,
					[CUST_BILL_CUR_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  ,
					[VNDR_ORDR_ID]  ,
					[TOT_HOLD_DRTN_TME_TXT] ,
					[ACCS_CTY_SITE_ID] ,
					[NRM_UPDT_CD]  ,
					[CNFRM_RNL_DT] ,
					[ACCS_MB_DES]  ,
					[STDI_DT] ,
					[STDI_REAS_ID]	
				FROM	[COWS].[arch].[TRPT_ORDR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[TRPT_ORDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[TRPT_ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[TRPT_ORDR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[TRPT_ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[TRPT_ORDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY				TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_USER_CSG_LVL		USER_CSG_LVL	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID
				FK02_USER_CSG_LVL		USER_CSG_LVL	USER_ID				LK_USER				USER_ID
				FK03_USER_CSG_LVL		USER_CSG_LVL	CREAT_BY_USER_ID	LK_USER				USER_ID
				FK04_USER_CSG_LVL		USER_CSG_LVL	MODFD_BY_USER_ID	LK_USER				USER_ID
				FK05_USER_CSG_LVL		USER_CSG_LVL	CSG_LVL_ID			LK_CSG_LVL			CSG_LVL_ID

				*/
				/*
				PRINT 'Starting to execute [arch].[USER_CSG_LVL] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[USER_CSG_LVL](
					[USER_ID]  ,
					[CSG_LVL_ID]  ,
					[EMPL_ACT_CD]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]  )
				SELECT [USER_ID]  ,
					[CSG_LVL_ID]  ,
					[EMPL_ACT_CD]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]

				FROM COWS.dbo.[USER_CSG_LVL] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [arch].[USER_CSG_LVL] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[USER_CSG_LVL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM COWS.dbo.[USER_CSG_LVL] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [dbo].[USER_CSG_LVL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[USER_CSG_LVL]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_USER_GRP_ROLE	USER_GRP_ROLE	USER_ID				LK_USER	USER_ID
				FK02_USER_GRP_ROLE	USER_GRP_ROLE	GRP_ID				LK_GRP	GRP_ID
				FK03_USER_GRP_ROLE	USER_GRP_ROLE	ROLE_ID				LK_ROLE	ROLE_ID
				FK04_USER_GRP_ROLE	USER_GRP_ROLE	REC_STUS_ID			LK_REC_STUS	REC_STUS_ID
				FK05_USER_GRP_ROLE	USER_GRP_ROLE	CREAT_BY_USER_ID	LK_USER	USER_ID
				FK06_USER_GRP_ROLE	USER_GRP_ROLE	MODFD_BY_USER_ID	LK_USER	USER_ID

				*/
				/*
				PRINT 'Starting to execute [arch].[USER_GRP_ROLE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO  [arch].[USER_GRP_ROLE](
					[USER_ID]  ,
					[GRP_ID]  ,
					[ROLE_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[CREAT_DT]  ,
					[MODFD_DT] )
				SELECT [USER_ID]  ,
					[USER_ID]  ,
					[GRP_ID]  ,
					[ROLE_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[CREAT_DT]  ,
					[MODFD_DT]

				FROM COWS.dbo.[USER_GRP_ROLE] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [arch].[USER_GRP_ROLE] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[USER_GRP_ROLE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM COWS.dbo.[USER_GRP_ROLE] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [dbo].[USER_GRP_ROLE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[USER_GRP_ROLE]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY			TABLE_NAME				COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_USER_H5_WFM	USER_H5_WFM				USER_ID		LK_USER			USER_ID
				FK02_USER_H5_WFM	USER_H5_WFM				REC_STUS_ID	LK_REC_STUS		REC_STUS_ID

				*/
				/*
				PRINT 'Starting to execute [arch].[USER_H5_WFM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[USER_H5_WFM](
					[USER_H5_ID]   ,
					[USER_ID]  ,
					[CUST_ID]  ,
					[SEG_NME]  ,
					[MNS_CD] ,
					[ISIP_CD] ,
					[REC_STUS_ID]  ,
					[CREAT_DT]  ,
					[MODFD_DT] )
				SELECT [USER_H5_ID]   ,
					[USER_ID]  ,
					[CUST_ID]  ,
					[SEG_NME]  ,
					[MNS_CD] ,
					[ISIP_CD] ,
					[REC_STUS_ID]  ,
					[CREAT_DT]  ,
					[MODFD_DT]

				FROM COWS.dbo.[USER_H5_WFM] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [arch].[USER_H5_WFM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[USER_H5_WFM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM COWS.dbo.[USER_H5_WFM] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [dbo].[USER_H5_WFM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[USER_H5_WFM]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_USER_WFM	USER_WFM	VNDR_CD				LK_VNDR			VNDR_CD
				FK02_USER_WFM	USER_WFM	CREAT_BY_USER_ID	LK_USER			USER_ID
				FK03_USER_WFM	USER_WFM	MODFD_BY_USER_ID	LK_USER			USER_ID
				FK04_USER_WFM	USER_WFM	USER_ID				LK_USER			USER_ID
				FK05_USER_WFM	USER_WFM	ROLE_ID				LK_ROLE			ROLE_ID
				FK06_USER_WFM	USER_WFM	REC_STUS_ID			LK_REC_STUS		REC_STUS_ID
				FK07_USER_WFM	USER_WFM	PROD_TYPE_ID		LK_PROD_TYPE	PROD_TYPE_ID
				FK08_USER_WFM	USER_WFM	PLTFRM_CD			LK_PLTFRM		PLTFRM_CD
				FK09_USER_WFM	USER_WFM	ORDR_TYPE_ID		LK_ORDR_TYPE	ORDR_TYPE_ID
				FK10_USER_WFM	USER_WFM	ORDR_ACTN_ID		LK_ORDR_ACTN	ORDR_ACTN_ID
				FK11_USER_WFM	USER_WFM	GRP_ID				LK_GRP			GRP_ID
				FK12_USER_WFM	USER_WFM	IPL_TRMTG_CTRY_CD	LK_CTRY			CTRY_CD
				FK13_USER_WFM	USER_WFM	ORGTNG_CTRY_CD		LK_CTRY			CTRY_CD

				*/
				/*
				PRINT 'Starting to execute [arch].[USER_WFM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO  [arch].[USER_WFM](
					[WFM_ID]   ,
					[USER_ID]  ,
					[GRP_ID]  ,
					[ROLE_ID]  ,
					[ORDR_TYPE_ID]  ,
					[PROD_TYPE_ID]  ,
					[PLTFRM_CD] ,
					[VNDR_CD]  ,
					[ORGTNG_CTRY_CD] ,
					[IPL_TRMTG_CTRY_CD] ,
					[ORDR_ACTN_ID]  ,
					[WFM_ASMT_LVL_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]   )
				SELECT [WFM_ID]   ,
					[USER_ID]  ,
					[GRP_ID]  ,
					[ROLE_ID]  ,
					[ORDR_TYPE_ID]  ,
					[PROD_TYPE_ID]  ,
					[PLTFRM_CD] ,
					[VNDR_CD]  ,
					[ORGTNG_CTRY_CD] ,
					[IPL_TRMTG_CTRY_CD] ,
					[ORDR_ACTN_ID]  ,
					[WFM_ASMT_LVL_ID]  ,
					[REC_STUS_ID]  ,
					[CREAT_BY_USER_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[CREAT_DT]

				FROM COWS.dbo.[USER_WFM] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [arch].[USER_WFM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[USER_WFM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM COWS.dbo.[USER_WFM] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [dbo].[USER_WFM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[USER_WFM]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_USER_WFM_ASMT	USER_WFM_ASMT	ORDR_ID			ORDR	ORDR_ID
				FK02_USER_WFM_ASMT	USER_WFM_ASMT	ASN_BY_USER_ID	LK_USER	USER_ID
				FK03_USER_WFM_ASMT	USER_WFM_ASMT	ASN_USER_ID		LK_USER	USER_ID
				FK04_USER_WFM_ASMT	USER_WFM_ASMT	GRP_ID			LK_GRP	GRP_ID
				---------------------------------------------ok-------------ORDER----------------------------------
				*/
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[USER_WFM_ASMT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO  [dbo].[USER_WFM_ASMT](
					[ORDR_ID]  ,
					[GRP_ID]  ,
					[ASN_USER_ID]  ,
					[ASN_BY_USER_ID]  ,
					[ASMT_DT]  ,
					[CREAT_DT]  ,
					[ORDR_HIGHLIGHT_CD]  ,
					[ORDR_HIGHLIGHT_MODFD_DT] ,
					[INRTE_TM] ,
					[ONSITE_TM] ,
					[CMPLT_TM] ,
					[EVENT_ID] )
				SELECT  [ORDR_ID]  ,
					[GRP_ID]  ,
					[ASN_USER_ID]  ,
					[ASN_BY_USER_ID]  ,
					[ASMT_DT]  ,
					[CREAT_DT]  ,
					[ORDR_HIGHLIGHT_CD]  ,
					[ORDR_HIGHLIGHT_MODFD_DT] ,
					[INRTE_TM] ,
					[ONSITE_TM] ,
					[CMPLT_TM] ,
					[EVENT_ID]
				FROM	[COWS].[arch].[USER_WFM_ASMT] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[USER_WFM_ASMT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[USER_WFM_ASMT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[USER_WFM_ASMT] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[USER_WFM_ASMT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[USER_WFM_ASMT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY				TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_VNDR_ORDR_MS		VNDR_ORDR_MS	VNDR_ORDR_EMAIL_ID	VNDR_ORDR_EMAIL		VNDR_ORDR_EMAIL_ID
				FK02_VNDR_ORDR_MS		VNDR_ORDR_MS	VNDR_ORDR_ID		VNDR_ORDR			VNDR_ORDR_ID
				FK03_VNDR_ORDR_MS		VNDR_ORDR_MS	CREAT_BY_USER_ID	LK_USER				USER_ID
				----------------------------------------ok-----------ORDER-->>VNDR_ORDR_EMAIL_ID------------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. VNDR_ORDR_MS     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[VNDR_ORDR_MS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [COWS].[dbo].[VNDR_ORDR_MS](
					[VNDR_ORDR_MS_ID]   ,
					[VNDR_ORDR_ID]  ,
					[VER_ID]  ,
					[VNDR_ORDR_EMAIL_ID] ,
					[SENT_TO_VNDR_DT] ,
					[ACK_BY_VNDR_DT] ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT]  )
					
				SELECT  [VNDR_ORDR_MS_ID]   ,
					[VNDR_ORDR_ID]  ,
					[VER_ID]  ,
					[VNDR_ORDR_EMAIL_ID] ,
					[SENT_TO_VNDR_DT] ,
					[ACK_BY_VNDR_DT] ,
					[CREAT_BY_USER_ID]  ,
					[CREAT_DT] 
				FROM	[COWS].[arch].[VNDR_ORDR_MS] WITH(NOLOCK) WHERE [VNDR_ORDR_EMAIL_ID] IN ( SELECT [VNDR_ORDR_EMAIL_ID] FROM 
				  [COWS].[arch].[VNDR_ORDR_EMAIL] WITH(NOLOCK) WHERE [VNDR_ORDR_ID] IN ( SELECT [VNDR_ORDR_ID] FROM [COWS].[arch].[VNDR_ORDR] WITH(NOLOCK) 	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)) )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[VNDR_ORDR_MS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. VNDR_ORDR_MS     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[VNDR_ORDR_MS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE FROM	[COWS].[arch].[VNDR_ORDR_MS] WITH(TABLOCKX) WHERE [VNDR_ORDR_EMAIL_ID] IN ( SELECT [VNDR_ORDR_EMAIL_ID] FROM 
				  [COWS].[arch].[VNDR_ORDR_EMAIL] WITH(NOLOCK) WHERE [VNDR_ORDR_ID] IN ( SELECT [VNDR_ORDR_ID] FROM [COWS].[arch].[VNDR_ORDR] WITH(NOLOCK) 	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)) )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[VNDR_ORDR_MS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT	
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[VNDR_ORDR_MS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY					TABLE_NAME				COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_VNDR_ORDR_EMAIL_ATCHMT	VNDR_ORDR_EMAIL_ATCHMT	VNDR_ORDR_EMAIL_ID	VNDR_ORDR_EMAIL		VNDR_ORDR_EMAIL_ID
				FK02_VNDR_ORDR_EMAIL_ATCHMT	VNDR_ORDR_EMAIL_ATCHMT	CREAT_BY_USER_ID	LK_USER				USER_ID
				FK03_VNDR_ORDR_EMAIL_ATCHMT	VNDR_ORDR_EMAIL_ATCHMT	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID

				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. VNDR_ORDR_EMAIL_ATCHMT     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[VNDR_ORDR_EMAIL_ATCHMT] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[VNDR_ORDR_EMAIL_ATCHMT](
					[VNDR_ORDR_EMAIL_ATCHMT_ID]   ,
					[VNDR_ORDR_EMAIL_ID]  ,
					[FILE_NME]  ,
					[FILE_CNTNT]  ,
					[FILE_SIZE_QTY]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  )
				SELECT  [VNDR_ORDR_EMAIL_ATCHMT_ID]   ,
					[VNDR_ORDR_EMAIL_ID]  ,
					[FILE_NME]  ,
					[FILE_CNTNT]  ,
					[FILE_SIZE_QTY]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID] 
				FROM	[COWS].[arch].[VNDR_ORDR_EMAIL_ATCHMT] WITH(NOLOCK) WHERE [VNDR_ORDR_EMAIL_ID] IN ( SELECT [VNDR_ORDR_EMAIL_ID] FROM 
				  [COWS].[arch].[VNDR_ORDR_EMAIL] WITH(NOLOCK) WHERE [VNDR_ORDR_ID] IN ( SELECT [VNDR_ORDR_ID] FROM [COWS].[arch].[VNDR_ORDR] WITH(NOLOCK) 	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)) )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[VNDR_ORDR_EMAIL_ATCHMT] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. VNDR_ORDR_EMAIL_ATCHMT     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[VNDR_ORDR_EMAIL_ATCHMT] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE FROM	[COWS].[arch].[VNDR_ORDR_EMAIL_ATCHMT] WITH(TABLOCKX) WHERE [VNDR_ORDR_EMAIL_ID] IN ( SELECT [VNDR_ORDR_EMAIL_ID] FROM 
				  [COWS].[arch].[VNDR_ORDR_EMAIL] WITH(TABLOCKX) WHERE [VNDR_ORDR_ID] IN ( SELECT [VNDR_ORDR_ID] FROM [COWS].[arch].[VNDR_ORDR] WITH(NOLOCK) 	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)) )
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[VNDR_ORDR_EMAIL_ATCHMT] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[VNDR_ORDR_EMAIL_ATCHMT]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				FOREIGN_KEY				TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_VNDR_ORDR_EMAIL	VNDR_ORDR_EMAIL	EMAIL_STUS_ID		LK_STUS				STUS_ID
				FK02_VNDR_ORDR_EMAIL	VNDR_ORDR_EMAIL	VNDR_ORDR_ID		VNDR_ORDR			VNDR_ORDR_ID
				FK03_VNDR_ORDR_EMAIL	VNDR_ORDR_EMAIL	CREAT_BY_USER_ID	LK_USER				USER_ID
				FK04_VNDR_ORDR_EMAIL	VNDR_ORDR_EMAIL	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID
				FK05_VNDR_ORDR_EMAIL	VNDR_ORDR_EMAIL	MODFD_BY_USER_ID	LK_USER				USER_ID
				FK06_VNDR_ORDR_EMAIL	VNDR_ORDR_EMAIL	VNDR_EMAIL_TYPE_ID	LK_VNDR_EMAIL_TYPE	VNDR_EMAIL_TYPE_ID
				----------------------------------------------------ok--------ORDER-->>VNDR_ORDR_ID--------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. VNDR_ORDR_EMAIL     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[VNDR_ORDR_EMAIL] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO  [dbo].[VNDR_ORDR_EMAIL](
					[VNDR_ORDR_EMAIL_ID]   ,
					[VNDR_ORDR_ID]  ,
					[VER_NBR]  ,
					[FROM_EMAIL_ADR]  ,
					[TO_EMAIL_ADR]  ,
					[CC_EMAIL_ADR]  ,
					[EMAIL_BODY] ,
					[EMAIL_STUS_ID]  ,
					[SENT_DT] ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[EMAIL_SUBJ_TXT] ,
					[VNDR_EMAIL_TYPE_ID] )
				SELECT  [VNDR_ORDR_EMAIL_ID]   ,
					[VNDR_ORDR_ID]  ,
					[VER_NBR]  ,
					[FROM_EMAIL_ADR]  ,
					[TO_EMAIL_ADR]  ,
					[CC_EMAIL_ADR]  ,
					[EMAIL_BODY] ,
					[EMAIL_STUS_ID]  ,
					[SENT_DT] ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[MODFD_BY_USER_ID] ,
					[MODFD_DT] ,
					[EMAIL_SUBJ_TXT] ,
					[VNDR_EMAIL_TYPE_ID]
				FROM	[COWS].[arch].[VNDR_ORDR_EMAIL] WITH(NOLOCK) WHERE [VNDR_ORDR_ID] IN ( SELECT [VNDR_ORDR_ID] FROM [COWS].[arch].[VNDR_ORDR] WITH(NOLOCK) 	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[VNDR_ORDR_EMAIL] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. VNDR_ORDR_EMAIL     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[VNDR_ORDR_EMAIL] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE FROM	[COWS].[arch].[VNDR_ORDR_EMAIL] WITH(TABLOCKX) WHERE [VNDR_ORDR_ID] IN ( SELECT [VNDR_ORDR_ID] FROM [COWS].[arch].[VNDR_ORDR] WITH(NOLOCK) 	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[VNDR_ORDR_EMAIL] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[VNDR_ORDR_EMAIL]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				/*
				FOREIGN_KEY			TABLE_NAME		COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_VNDR_ORDR_FORM	VNDR_ORDR_FORM	VNDR_ORDR_ID		VNDR_ORDR			VNDR_ORDR_ID
				FK02_VNDR_ORDR_FORM	VNDR_ORDR_FORM	CREAT_BY_USER_ID	LK_USER				USER_ID
				FK03_VNDR_ORDR_FORM	VNDR_ORDR_FORM	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID
				FK04_VNDR_ORDR_FORM	VNDR_ORDR_FORM	TMPLT_ID			VNDR_ORDR_FORM		VNDR_ORDR_FORM_ID
				-----------------------------------------------------ok--------------ORDER->>[VNDR_ORDR_ID]-------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. VNDR_ORDR_FORM     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[VNDR_ORDR_FORM] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[VNDR_ORDR_FORM](
					[VNDR_ORDR_FORM_ID]   ,
					[VNDR_ORDR_ID]  ,
					[FILE_NME]  ,
					[FILE_CNTNT]  ,
					[FILE_SIZE_QTY]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[TMPLT_CD]  ,
					[TMPLT_ID]  )
				SELECT  [VNDR_ORDR_FORM_ID]   ,
					[VNDR_ORDR_ID]  ,
					[FILE_NME]  ,
					[FILE_CNTNT]  ,
					[FILE_SIZE_QTY]  ,
					[CREAT_DT]  ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[TMPLT_CD]  ,
					[TMPLT_ID]
				FROM	[COWS].[arch].[VNDR_ORDR_FORM] WITH(NOLOCK) WHERE [VNDR_ORDR_ID] IN ( SELECT [VNDR_ORDR_ID] FROM [COWS].[arch].[VNDR_ORDR] WITH(NOLOCK) 	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[VNDR_ORDR_FORM] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. VNDR_ORDR_FORM     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[VNDR_ORDR_FORM] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE FROM	[COWS].[arch].[VNDR_ORDR_FORM] WITH(TABLOCKX) WHERE [VNDR_ORDR_ID] IN ( SELECT [VNDR_ORDR_ID] FROM [COWS].[arch].[VNDR_ORDR] WITH(NOLOCK) 	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[VNDR_ORDR_FORM] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT	
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[VNDR_ORDR_FORM]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				VNDR_FOLDR
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE			REFERANCE_COLUMN_NAME
				FK01_VNDR_FOLDR	VNDR_FOLDR	CTRY_CD				LK_CTRY					CTRY_CD
				FK02_VNDR_FOLDR	VNDR_FOLDR	VNDR_CD				LK_VNDR					VNDR_CD
				FK03_VNDR_FOLDR	VNDR_FOLDR	STT_CD				LK_US_STT				US_STATE_ID
				FK04_VNDR_FOLDR	VNDR_FOLDR	CREAT_BY_USER_ID	LK_USER					USER_ID
				FK05_VNDR_FOLDR	VNDR_FOLDR	MODFD_BY_USER_ID	LK_USER					USER_ID
				FK06_VNDR_FOLDR	VNDR_FOLDR	REC_STUS_ID			LK_REC_STUS				REC_STUS_ID
				-----------------------------------------------------------------------------------------------
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_VNDR_FOLDR_CNTCT	VNDR_FOLDR_CNTCT	VNDR_FOLDR_ID	VNDR_FOLDR		VNDR_FOLDR_ID
				FK01_VNDR_ORDR			VNDR_ORDR			VNDR_FOLDR_ID	VNDR_FOLDR		VNDR_FOLDR_ID
				FK01_VNDR_TMPLT			VNDR_TMPLT			VNDR_FOLDR_ID	VNDR_FOLDR		VNDR_FOLDR_ID

				*/
				/*
				PRINT 'Starting to execute [arch].[VNDR_FOLDR] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[VNDR_FOLDR](
					[VNDR_FOLDR_ID]   ,
					[VNDR_CD]  ,
					[STREET_ADR_1]  ,
					[STREET_ADR_2]  ,
					[CTY_NME]  ,
					[PRVN_NME] ,
					[ZIP_PSTL_CD] ,
					[STT_CD]  ,
					[CMNT_TXT] ,
					[VNDR_EMAIL_LIST_TXT]  ,
					[CREAT_DT]  ,
					[MODFD_DT] ,
					[MODFD_BY_USER_ID] ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[CTRY_CD] ,
					[BLDG_NME]  ,
					[FLR_ID]  ,
					[RM_NBR]  )
				SELECT 
					[VNDR_FOLDR_ID]   ,
					[VNDR_CD]  ,
					[STREET_ADR_1]  ,
					[STREET_ADR_2]  ,
					[CTY_NME]  ,
					[PRVN_NME] ,
					[ZIP_PSTL_CD] ,
					[STT_CD]  ,
					[CMNT_TXT] ,
					[VNDR_EMAIL_LIST_TXT]  ,
					[CREAT_DT]  ,
					[MODFD_DT] ,
					[MODFD_BY_USER_ID] ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[CTRY_CD] ,
					[BLDG_NME]  ,
					[FLR_ID]  ,
					[RM_NBR]
				FROM	[COWS].[dbo].[VNDR_FOLDR] WITH(NOLOCK)	WHERE [VNDR_FOLDR_ID]	IN	
						(SELECT DISTINCT [VNDR_FOLDR_ID] FROM  [COWS].[dbo].VNDR_ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))
				PRINT 'Finished executing [arch].[VNDR_FOLDR] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				
				PRINT 'Starting to execute [dbo].[VNDR_FOLDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM	[COWS].[dbo].[VNDR_FOLDR] WITH(TABLOCKX)	WHERE [VNDR_FOLDR_ID]	IN	
						(SELECT DISTINCT [VNDR_FOLDR_ID] FROM  [COWS].[dbo].VNDR_ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))
				PRINT 'Finished executing [dbo].[VNDR_FOLDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[VNDR_FOLDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_WG_PROF_SM	WG_PROF_SM	WG_PROF_ID			LK_WG_PROF	WG_PROF_ID
				FK02_WG_PROF_SM	WG_PROF_SM	CREAT_BY_USER_ID	LK_USER		USER_ID
				FK03_WG_PROF_SM	WG_PROF_SM	PRE_REQST_TASK_ID	LK_TASK		TASK_ID
				FK04_WG_PROF_SM	WG_PROF_SM	DESRD_TASK_ID		LK_TASK		TASK_ID
				rollback
				*/
				/*
				PRINT 'Starting to execute [arch].[WG_PROF_SM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[WG_PROF_SM](
					[WG_PROF_ID]  ,
					[PRE_REQST_TASK_ID]  ,
					[DESRD_TASK_ID]  ,
					[OPT_ID]  ,
					[CREAT_BY_USER_ID] ,
					[CREAT_DT]  ,
					[PRE_REQST_TASK_GRP_REQR_CD]  )
				SELECT [WG_PROF_ID]  ,
					[PRE_REQST_TASK_ID]  ,
					[DESRD_TASK_ID]  ,
					[OPT_ID]  ,
					[CREAT_BY_USER_ID] ,
					[CREAT_DT]  ,
					[PRE_REQST_TASK_GRP_REQR_CD]	
				FROM   COWS.dbo.[WG_PROF_SM] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [arch].[WG_PROF_SM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[WG_PROF_SM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE FROM   COWS.dbo.[WG_PROF_SM] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				Set @RowCounts=@@RowCount PRINT 'Finished executing [dbo].[WG_PROF_SM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@RowCounts) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[WG_PROF_SM]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_WG_PROF_STUS	WG_PROF_STUS	ORDR_ID		ORDR		ORDR_ID
				FK02_WG_PROF_STUS	WG_PROF_STUS	WG_PTRN_ID	LK_WG_PTRN	WG_PTRN_ID
				FK03_WG_PROF_STUS	WG_PROF_STUS	WG_PROF_ID	LK_WG_PROF	WG_PROF_ID
				FK04_WG_PROF_STUS	WG_PROF_STUS	STUS_ID		LK_STUS		STUS_ID
				-----------------------------------ok---------------------ORDER--------------
				*/
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[WG_PROF_STUS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[WG_PROF_STUS](
					[ORDR_ID]  ,
					[WG_PROF_ID]  ,
					[WG_PTRN_ID]  ,
					[STUS_ID]  ,
					[CREAT_DT]  )
				SELECT [ORDR_ID]  ,
					[WG_PROF_ID]  ,
					[WG_PTRN_ID]  ,
					[STUS_ID]  ,
					[CREAT_DT]	
				FROM	[COWS].[arch].[WG_PROF_STUS] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[WG_PROF_STUS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[WG_PROF_STUS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[WG_PROF_STUS] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[WG_PROF_STUS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[WG_PROF_STUS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_WG_PTRN_SM	WG_PTRN_SM	WG_PTRN_ID				LK_WG_PTRN	WG_PTRN_ID
				FK02_WG_PTRN_SM	WG_PTRN_SM	PRE_REQST_WG_PROF_ID	LK_WG_PROF	WG_PROF_ID
				FK03_WG_PTRN_SM	WG_PTRN_SM	DESRD_WG_PROF_ID		LK_WG_PROF	WG_PROF_ID

				*/
				/*
				PRINT 'Starting to execute [arch].[WG_PTRN_SM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				INSERT INTO [arch].[WG_PTRN_SM](
					[WG_PTRN_ID]  ,
					[PRE_REQST_WG_PROF_ID]  ,
					[DESRD_WG_PROF_ID]  ,
					[CREAT_DT]  )

				SELECT [WG_PTRN_ID]  ,
					[PRE_REQST_WG_PROF_ID]  ,
					[DESRD_WG_PROF_ID]  ,
					[CREAT_DT]	
				FROM   COWS.dbo.[WG_PTRN_SM] WITH (NOLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [arch].[WG_PTRN_SM] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				PRINT 'Starting to execute [dbo].[WG_PTRN_SM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
				DELETE  FROM   COWS.dbo.[WG_PTRN_SM] WITH (ROWLOCK) Where [CREAT_DT]<=CONVERT(datetime,@Date)
				PRINT 'Finished executing [dbo].[WG_PTRN_SM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[WG_PTRN_SM]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				*/
				/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_WG_PTRN_STUS	WG_PTRN_STUS	ORDR_ID			ORDR		ORDR_ID
				FK02_WG_PTRN_STUS	WG_PTRN_STUS	WG_PTRN_ID		LK_WG_PTRN	WG_PTRN_ID
				FK03_WG_PTRN_STUS	WG_PTRN_STUS	STUS_ID			LK_STUS		STUS_ID
				-------------------------------------------ok-----------------ORDER---------------
				*/
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[WG_PTRN_STUS] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[WG_PTRN_STUS](
					[ORDR_ID]  ,
					[WG_PTRN_ID]  ,
					[STUS_ID]  ,
					[CREAT_DT]  )

				SELECT [ORDR_ID]  ,
					[WG_PTRN_ID]  ,
					[STUS_ID]  ,
					[CREAT_DT]	
				FROM   COWS.arch.[WG_PTRN_STUS] WITH (NOLOCK) WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[WG_PTRN_STUS] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[WG_PTRN_STUS] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM   COWS.arch.[WG_PTRN_STUS] WITH (ROWLOCK) WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[WG_PTRN_STUS] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[WG_PTRN_STUS]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				/*
				[VNDR_ORDR]
				FOREIGN_KEY		TABLE_NAME	COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_VNDR_ORDR	VNDR_ORDR	VNDR_FOLDR_ID		VNDR_FOLDR			VNDR_FOLDR_ID
				FK02_VNDR_ORDR	VNDR_ORDR	ORDR_ID				ORDR				ORDR_ID
				FK03_VNDR_ORDR	VNDR_ORDR	CREAT_BY_USER_ID	LK_USER				USER_ID
				FK04_VNDR_ORDR	VNDR_ORDR	REC_STUS_ID			LK_REC_STUS			REC_STUS_ID
				FK05_VNDR_ORDR	VNDR_ORDR	MODFD_BY_USER_ID	LK_USER				USER_ID
				FK06_VNDR_ORDR	VNDR_ORDR	VNDR_ORDR_ID		ORDR				ORDR_ID
				FK07_VNDR_ORDR	VNDR_ORDR	VNDR_ORDR_TYPE_ID	LK_VNDR_ORDR_TYPE	VNDR_ORDR_TYPE_ID
				FK08_VNDR_ORDR	VNDR_ORDR	PREV_ORDR_ID		ORDR				ORDR_ID
				----------------------------------------ok--------------------------------ORDER-----------
				FOREIGN_KEY				TABLE_NAME			COLUMN_NAME		REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK03_CKT				CKT					VNDR_ORDR_ID	VNDR_ORDR		VNDR_ORDR_ID
				FK02_VNDR_ORDR_EMAIL	VNDR_ORDR_EMAIL		VNDR_ORDR_ID	VNDR_ORDR		VNDR_ORDR_ID
				FK01_VNDR_ORDR_FORM		VNDR_ORDR_FORM		VNDR_ORDR_ID	VNDR_ORDR		VNDR_ORDR_ID
				FK02_VNDR_ORDR_MS		VNDR_ORDR_MS		VNDR_ORDR_ID	VNDR_ORDR		VNDR_ORDR_ID
				*/
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[VNDR_ORDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[VNDR_ORDR](
						[VNDR_ORDR_ID]  ,
						[VNDR_FOLDR_ID]  ,
						[ORDR_ID] ,
						[CREAT_DT]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_DT] ,
						[REC_STUS_ID]  ,
						[MODFD_BY_USER_ID] ,
						[VNDR_ORDR_TYPE_ID]  ,
						[TRMTG_CD]  ,
						[PREV_ORDR_ID] ,
						[BYPAS_VNDR_ORDR_MS_CD]  )	
				SELECT
						[VNDR_ORDR_ID]  ,
						[VNDR_FOLDR_ID]  ,
						[ORDR_ID] ,
						[CREAT_DT]  ,
						[CREAT_BY_USER_ID]  ,
						[MODFD_DT] ,
						[REC_STUS_ID]  ,
						[MODFD_BY_USER_ID] ,
						[VNDR_ORDR_TYPE_ID]  ,
						[TRMTG_CD]  ,
						[PREV_ORDR_ID] ,
						[BYPAS_VNDR_ORDR_MS_CD]
				FROM	[COWS].[arch].[VNDR_ORDR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[VNDR_ORDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[VNDR_ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[VNDR_ORDR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[VNDR_ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[VNDR_ORDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				/*
				[arch].[ORDR]
				FOREIGN_KEY	TABLE_NAME			COLUMN_NAME			REFERANCE_TABLE		REFERANCE_COLUMN_NAME
				FK01_ORDR	ORDR				ORDR_CAT_ID			LK_ORDR_CAT			ORDR_CAT_ID
				FK02_ORDR	ORDR				CREAT_BY_USER_ID	LK_USER				USER_ID
				FK03_ORDR	ORDR				REC_STUS_ID			LK_REC_STUS			REC_STUS_ID
				FK04_ORDR	ORDR				MODFD_BY_USER_ID	LK_USER				USER_ID
				FK05_ORDR	ORDR				H5_FOLDR_ID			H5_FOLDR			H5_FOLDR_ID
				FK06_ORDR	ORDR				PPRT_ID				LK_PPRT				PPRT_ID
				FK07_ORDR	ORDR				PRNT_ORDR_ID		ORDR				ORDR_ID
				FK08_ORDR	ORDR				CSG_LVL_CD			LK_CSG_LVL			CSG_LVL_CD
				FK09_ORDR	ORDR				RGN_ID				LK_XNCI_RGN			RGN_ID
				FK10_ORDR	ORDR				PLTFRM_CD			LK_PLTFRM			PLTFRM_CD
				FK11_ORDR	ORDR				ORDR_STUS_ID		LK_ORDR_STUS		ORDR_STUS_ID
				--------------------------------------------ok-----------------------------ORDER------------------------
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[ORDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[ORDR](
						[ORDR_ID] ,
						[ORDR_CAT_ID] ,
						[CREAT_DT] ,
						[CREAT_BY_USER_ID] ,
						[REC_STUS_ID]  ,
						[MODFD_DT]  ,
						[MODFD_BY_USER_ID] ,
						[H5_FOLDR_ID] ,
						[PPRT_ID] ,
						[DMSTC_CD]  ,
						[H5_H6_CUST_ID] ,
						[PRNT_ORDR_ID] ,
						--[SCURD_CD]  ,
						[CSG_LVL_ID] ,
						[RGN_ID] ,
						[PLTFRM_CD] ,
						[CCD_UPDT_CD]  ,
						[ORDR_STUS_ID]  ,
						[ORDR_CMPLTN_DRTN_TME_TXT] ,
						[CHARS_ID] ,
						[SLA_VLTD_CD]  ,
						[CUST_CMMT_DT] ,
						[INSTL_ESCL_CD] ,
						[FSA_EXP_TYPE_CD],
						[SMR_NMR],
						[RAS_DT],
						[DLVY_CLLI],
						[PROD_ID],
						[CPE_CLLI] 
						)
				SELECT 
						[ORDR_ID] ,
						[ORDR_CAT_ID] ,
						[CREAT_DT] ,
						[CREAT_BY_USER_ID] ,
						[REC_STUS_ID]  ,
						[MODFD_DT]  ,
						[MODFD_BY_USER_ID] ,
						[H5_FOLDR_ID] ,
						[PPRT_ID] ,
						[DMSTC_CD]  ,
						[H5_H6_CUST_ID] ,
						[PRNT_ORDR_ID] ,
						--[SCURD_CD]  ,
						[CSG_LVL_ID] ,
						[RGN_ID] ,
						[PLTFRM_CD] ,
						[CCD_UPDT_CD]  ,
						[ORDR_STUS_ID]  ,
						[ORDR_CMPLTN_DRTN_TME_TXT] ,
						[CHARS_ID] ,
						[SLA_VLTD_CD]  ,
						[CUST_CMMT_DT] ,
						[INSTL_ESCL_CD] ,
						[FSA_EXP_TYPE_CD],
						[SMR_NMR],
						[RAS_DT],
						[DLVY_CLLI],
						[PROD_ID],
						[CPE_CLLI]
				FROM	[COWS].[arch].[ORDR] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[ORDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. ORDR     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[ORDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE FROM	[COWS].[arch].[ORDR] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[ORDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
				
				
				
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[M5_ORDR_MSG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[M5_ORDR_MSG]     ON
				INSERT INTO [COWS].[dbo].[M5_ORDR_MSG]
					   ([M5_TRAN_ID]
					   ,[ORDR_ID]
					   ,[M5_MSG_ID]
					   ,[NTE]
					   ,[MSG]
					   ,[DEVICE]
					   ,[CMPNT_ID]
					   ,[STUS_ID]
					   ,[CREAT_DT]
					   ,[SENT_DT]
					   ,[PRCH_ORDR_NBR])
				SELECT [M5_TRAN_ID]
					  ,[ORDR_ID]
					  ,[M5_MSG_ID]
					  ,[NTE]
					  ,[MSG]
					  ,[DEVICE]
					  ,[CMPNT_ID]
					  ,[STUS_ID]
					  ,[CREAT_DT]
					  ,[SENT_DT]
					  ,[PRCH_ORDR_NBR]
				FROM [COWS].[arch].[M5_ORDR_MSG] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[M5_ORDR_MSG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[M5_ORDR_MSG]      OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[M5_ORDR_MSG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE FROM	[COWS].[arch].[M5_ORDR_MSG] WITH(TABLOCKX)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[M5_ORDR_MSG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[M5_ORDR_MSG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
				
				
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[M5_EVENT_MSG] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[M5_EVENT_MSG]     ON
				INSERT INTO [COWS].[dbo].[M5_EVENT_MSG]
					   ([M5_EVENT_MSG_ID]
					  ,[M5_ORDR_ID]
					  ,[M5_ORDR_NBR]
					  ,[M5_MSG_ID]
					  ,[DEV_ID]
					  ,[CMPNT_ID]
					  ,[STUS_ID]
					  ,[CREAT_DT]
					  ,[SENT_DT]
					  ,[EVENT_ID])
				SELECT [M5_EVENT_MSG_ID]
					  ,[M5_ORDR_ID]
					  ,[M5_ORDR_NBR]
					  ,[M5_MSG_ID]
					  ,[DEV_ID]
					  ,[CMPNT_ID]
					  ,[STUS_ID]
					  ,[CREAT_DT]
					  ,[SENT_DT]
					  ,[EVENT_ID]
				FROM [COWS].[arch].[M5_EVENT_MSG] WITH(NOLOCK)	WHERE M5_ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[M5_EVENT_MSG] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo].[M5_EVENT_MSG]      OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[M5_EVENT_MSG] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE FROM	[COWS].[arch].[M5_EVENT_MSG] WITH(TABLOCKX)	WHERE M5_ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[M5_EVENT_MSG] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [COWS].[arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[M5_EVENT_MSG]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			
			
				
			
			
			/*
				FOREIGN_KEY	TABLE_NAME	COLUMN_NAME	REFERANCE_TABLE	REFERANCE_COLUMN_NAME
				FK01_H5_FOLDR	H5_FOLDR	CTRY_CD				LK_CTRY			CTRY_CD
				FK03_H5_FOLDR	H5_FOLDR	CREAT_BY_USER_ID	LK_USER			USER_ID
				FK04_H5_FOLDR	H5_FOLDR	MODFD_BY_USER_ID	LK_USER			USER_ID
				FK05_H5_FOLDR	H5_FOLDR	REC_STUS_ID			LK_REC_STUS		REC_STUS_ID
				*/
				SET IDENTITY_INSERT  [COWS].[dbo]. H5_FOLDR     ON
				Set @Time=GetDate()   RAISERROR ('Starting to execute [dbo].[H5_FOLDR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
				INSERT INTO [dbo].[H5_FOLDR](
					[H5_FOLDR_ID]   ,
					[CUST_ID]  ,
					[CUST_CTY_NME]  ,
					[CREAT_DT]  ,
					[MODFD_DT] ,
					[MODFD_BY_USER_ID] ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[CTRY_CD] ,
					--[SCURD_CD]  ,
					[CSG_LVL_ID] ,
					[CUST_NME] )
				SELECT  [H5_FOLDR_ID]   ,
					[CUST_ID]  ,
					[CUST_CTY_NME]  ,
					[CREAT_DT]  ,
					[MODFD_DT] ,
					[MODFD_BY_USER_ID] ,
					[CREAT_BY_USER_ID]  ,
					[REC_STUS_ID]  ,
					[CTRY_CD] ,
					--[SCURD_CD]  ,
					[CSG_LVL_ID] ,
					[CUST_NME]
				FROM	[COWS].[arch].[H5_FOLDR] WITH(NOLOCK)	WHERE [H5_FOLDR_ID]	IN	
				(SELECT DISTINCT [H5_FOLDR_ID] FROM  [COWS].[arch].ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[H5_FOLDR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				SET IDENTITY_INSERT  [COWS].[dbo]. H5_FOLDR     OFF
				Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[H5_FOLDR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
				DELETE  FROM	[COWS].[arch].[H5_FOLDR] WITH(TABLOCKX)	WHERE [H5_FOLDR_ID]	IN	
				(SELECT DISTINCT [H5_FOLDR_ID] FROM  [COWS].[arch].ORDR WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table))
				Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[H5_FOLDR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
				INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[H5_FOLDR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			END	
			/*
			[AccessDelivery]
			------------------------------------------ok------------------EVENT---------------------
			*/
			--PRINT 'Starting to execute [arch].[AccessDelivery] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[AccessDelivery](
			--	[Event Status]  ,
			--	[Event_ID]  ,
			--	[AD Event WF]  ,
			--	[Workflow Status]  ,
			--	[Created By]  ,
			--	[AccessTableID]  ,
			--	[AccessTableTag]  ,
			--	[Activator Picker]  ,
			--	[AD Type]  ,
			--	[Assigned To]  ,
			--	[Auth_Status]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[CharsID]  ,
			--	[Circuit/Private Line ID Table]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Content Type]  ,
			--	[Created] ,
			--	[Customer Contact Cell Phone]  ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[Customer Name]  ,
			--	[CUSTOMER_CSG_LVL]  ,
			--	[Description and Comments]  ,
			--	[EmailBody]  ,
			--	[End Time] ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Event Type]  ,
			--	[Extra Duration]  ,
			--	[FTN]  ,
			--	[H1]  ,
			--	[H6]  ,
			--	[ID]  ,
			--	[Is CPE Attending?]  ,
			--	[Is Escalation]  ,
			--	[Item Title]  ,
			--	[L2P_RECORD]  ,
			--	[L2P_VERIFIED]  ,
			--	[L2P_VERIFIED_DATETIME] ,
			--	[Links (Documents / Cut Sheets)]  ,
			--	[MaskedContName]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[MEMBER_CSG_LVL]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[Primary Request Date] ,
			--	[Published E-Mail CC:]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[Sales Engineer (SE) E-Mail]  ,
			--	[Sales Engineer (SE) Name]  ,
			--	[Sales Engineer (SE) Phone]  ,
			--	[Secondary Request Date] ,
			--	[Short Notice]  ,
			--	[Slot Picker]  ,
			--	[Start Time] ,
			--	[Submit Type]  ,
			--	[Total Event Duration]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status] )
			--SELECT  [Event Status]  ,
			--	[Event_ID]  ,
			--	[AD Event WF]  ,
			--	[Workflow Status]  ,
			--	[Created By]  ,
			--	[AccessTableID]  ,
			--	[AccessTableTag]  ,
			--	[Activator Picker]  ,
			--	[AD Type]  ,
			--	[Assigned To]  ,
			--	[Auth_Status]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[CharsID]  ,
			--	[Circuit/Private Line ID Table]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Content Type]  ,
			--	[Created] ,
			--	[Customer Contact Cell Phone]  ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[Customer Name]  ,
			--	[CUSTOMER_CSG_LVL]  ,
			--	[Description and Comments]  ,
			--	[EmailBody]  ,
			--	[End Time] ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Event Type]  ,
			--	[Extra Duration]  ,
			--	[FTN]  ,
			--	[H1]  ,
			--	[H6]  ,
			--	[ID]  ,
			--	[Is CPE Attending?]  ,
			--	[Is Escalation]  ,
			--	[Item Title]  ,
			--	[L2P_RECORD]  ,
			--	[L2P_VERIFIED]  ,
			--	[L2P_VERIFIED_DATETIME] ,
			--	[Links (Documents / Cut Sheets)]  ,
			--	[MaskedContName]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[MEMBER_CSG_LVL]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[Primary Request Date] ,
			--	[Published E-Mail CC:]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[Sales Engineer (SE) E-Mail]  ,
			--	[Sales Engineer (SE) Name]  ,
			--	[Sales Engineer (SE) Phone]  ,
			--	[Secondary Request Date] ,
			--	[Short Notice]  ,
			--	[Slot Picker]  ,
			--	[Start Time] ,
			--	[Submit Type]  ,
			--	[Total Event Duration]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status]
			--FROM	[COWS].[dbo].[AccessDelivery] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [arch].[AccessDelivery] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[AccessDelivery] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM	[COWS].[dbo].[AccessDelivery] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [dbo].[AccessDelivery] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[AccessDelivery]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			[MDSEvent]
			---------------------------------------------OK----------------------------EVENT--------------------
			*/
			--PRINT 'Starting to execute [arch].[MDSEvent] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[MDSEvent](
			--	[Event Status]  ,
			--	[Event_ID]  ,
			--	[Engineering Contact]  ,
			--	[Install Site POC]  ,
			--	[Service Assurance POC]  ,
			--	[Auth_Status]  ,
			--	[Description and Comments]  ,
			--	[Workflow Status]  ,
			--	[Assigned To]  ,
			--	[Start Time] ,
			--	[SOWS_ADMIN_SCRIPT]  ,
			--	[End Time] ,
			--	[MDS WF]  ,
			--	[Created By]  ,
			--	[Address]  ,
			--	[City]  ,
			--	[Country/Region]  ,
			--	[Floor/Building]  ,
			--	[State/Province]  ,
			--	[AccessTableID]  ,
			--	[Activator Picker]  ,
			--	[Any Multilink Circuits?]  ,
			--	[Any Virtual Connections?]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[CharsID]  ,
			--	[Complete Entitlements]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Configurator]  ,
			--	[Content Type]  ,
			--	[CPE Dispatch Comments]  ,
			--	[CPE Dispatch Email Address]  ,
			--	[Created] ,
			--	[Customer Account Team PDL]  ,
			--	[Customer Contact Cell Phone]  ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[Customer Name]  ,
			--	[Design Document Location]  ,
			--	[Design Number]  ,
			--	[EmailBody]  ,
			--	[Engineering Contact Cell]  ,
			--	[Engineering Contact Phone]  ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Extra Duration]  ,
			--	[FMS Circuit Table]  ,
			--	[FTN]  ,
			--	[H1]  ,
			--	[H6]  ,
			--	[ID]  ,
			--	[Install Site POC Cell]  ,
			--	[Install Site POC Phone]  ,
			--	[Is Firewall Security Product?]  ,
			--	[Is MDS Bridge Needed?]  ,
			--	[Is Sprint CPE or NCR Required]  ,
			--	[Is this an Escalation?]  ,
			--	[Is Wired Device Transport Required?]  ,
			--	[Is Wireless Transport Required?]  ,
			--	[Item Title]  ,
			--	[LOC Table]  ,
			--	[Managed Devices]  ,
			--	[Masked_Address]  ,
			--	[MaskedCity]  ,
			--	[MaskedContName]  ,
			--	[MaskedCountry]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[MaskedState]  ,
			--	[MaskedZip]  ,
			--	[MDS Activity]  ,
			--	[MDS Activity Type]  ,
			--	[MDS Install Activity]  ,
			--	[MDS MAC Activity]  ,
			--	[MNS OE Table]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[OE Number]  ,
			--	[Primary Request Date] ,
			--	[Published E-Mail CC:]  ,
			--	[Redesign Number]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[SCRIPT_EMAIL_SENT]  ,
			--	[Secondary Request Date] ,
			--	[Service Assurance POC E-Mail]  ,
			--	[Service Assurance POC Phone]  ,
			--	[Service Assurance Site Support]  ,
			--	[Service Tier]  ,
			--	[Service Tier Optional Entitlements]  ,
			--	[Short Description]  ,
			--	[Slot Picker]  ,
			--	[Strategic Entitlements]  ,
			--	[Submit Type]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[US or International]  ,
			--	[US Timezone]  ,
			--	[Virtual Connections]  ,
			--	[Wired Device Transport]  ,
			--	[Wireless Transport]  ,
			--	[ZIP/Postal Code]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status]  )
			--SELECT  [Event Status]  ,
			--	[Event_ID]  ,
			--	[Engineering Contact]  ,
			--	[Install Site POC]  ,
			--	[Service Assurance POC]  ,
			--	[Auth_Status]  ,
			--	[Description and Comments]  ,
			--	[Workflow Status]  ,
			--	[Assigned To]  ,
			--	[Start Time] ,
			--	[SOWS_ADMIN_SCRIPT]  ,
			--	[End Time] ,
			--	[MDS WF]  ,
			--	[Created By]  ,
			--	[Address]  ,
			--	[City]  ,
			--	[Country/Region]  ,
			--	[Floor/Building]  ,
			--	[State/Province]  ,
			--	[AccessTableID]  ,
			--	[Activator Picker]  ,
			--	[Any Multilink Circuits?]  ,
			--	[Any Virtual Connections?]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[CharsID]  ,
			--	[Complete Entitlements]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Configurator]  ,
			--	[Content Type]  ,
			--	[CPE Dispatch Comments]  ,
			--	[CPE Dispatch Email Address]  ,
			--	[Created] ,
			--	[Customer Account Team PDL]  ,
			--	[Customer Contact Cell Phone]  ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[Customer Name]  ,
			--	[Design Document Location]  ,
			--	[Design Number]  ,
			--	[EmailBody]  ,
			--	[Engineering Contact Cell]  ,
			--	[Engineering Contact Phone]  ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Extra Duration]  ,
			--	[FMS Circuit Table]  ,
			--	[FTN]  ,
			--	[H1]  ,
			--	[H6]  ,
			--	[ID]  ,
			--	[Install Site POC Cell]  ,
			--	[Install Site POC Phone]  ,
			--	[Is Firewall Security Product?]  ,
			--	[Is MDS Bridge Needed?]  ,
			--	[Is Sprint CPE or NCR Required]  ,
			--	[Is this an Escalation?]  ,
			--	[Is Wired Device Transport Required?]  ,
			--	[Is Wireless Transport Required?]  ,
			--	[Item Title]  ,
			--	[LOC Table]  ,
			--	[Managed Devices]  ,
			--	[Masked_Address]  ,
			--	[MaskedCity]  ,
			--	[MaskedContName]  ,
			--	[MaskedCountry]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[MaskedState]  ,
			--	[MaskedZip]  ,
			--	[MDS Activity]  ,
			--	[MDS Activity Type]  ,
			--	[MDS Install Activity]  ,
			--	[MDS MAC Activity]  ,
			--	[MNS OE Table]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[OE Number]  ,
			--	[Primary Request Date] ,
			--	[Published E-Mail CC:]  ,
			--	[Redesign Number]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[SCRIPT_EMAIL_SENT]  ,
			--	[Secondary Request Date] ,
			--	[Service Assurance POC E-Mail]  ,
			--	[Service Assurance POC Phone]  ,
			--	[Service Assurance Site Support]  ,
			--	[Service Tier]  ,
			--	[Service Tier Optional Entitlements]  ,
			--	[Short Description]  ,
			--	[Slot Picker]  ,
			--	[Strategic Entitlements]  ,
			--	[Submit Type]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[US or International]  ,
			--	[US Timezone]  ,
			--	[Virtual Connections]  ,
			--	[Wired Device Transport]  ,
			--	[Wireless Transport]  ,
			--	[ZIP/Postal Code]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status]
			--FROM	[COWS].[dbo].[MDSEvent] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [arch].[MDSEvent] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[MDSEvent] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM	[COWS].[dbo].[MDSEvent] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [dbo].[MDSEvent] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDSEvent]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			[MDSFastckt]
			---------------------------------------------OK----------------------------EVENT--------------------
			*/
			--PRINT 'Starting to execute [arch].[MDSFastckt] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[MDSFastckt](
			--	[TableID]  ,
			--	[Circuit]  ,
			--	[Item Type]  ,
			--	[Path])
			--SELECT  [TableID]  ,
			--	[Circuit]  ,
			--	[Item Type]  ,
			--	[Path]
			--FROM	[COWS].[dbo].[MDSFastckt] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [arch].[MDSFastckt] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[MDSFastckt] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM	[COWS].[dbo].[MDSFastckt] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [dbo].[MDSFastckt] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MDSFastckt]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			[MPLSEvent]
			---------------------------------------------OK----------------------------EVENT--------------------
			*/
			--PRINT 'Starting to execute [arch].[MPLSEvent] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[MPLSEvent](
			--	[Event_ID]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[Circuit Number]  ,
			--	[Created] ,
			--	[Created By]  ,
			--	[Customer Name]  ,
			--	[H1]  ,
			--	[H6]  ,
			--	[Workflow Status]  ,
			--	[Carrier Partner]  ,
			--	[OFF Net Monitoring?]  ,
			--	[Event Status]  ,
			--	[Start Time] ,
			--	[SOWS_ADMIN_SCRIPT]  ,
			--	[Scheduled_Implementation_Start_WF]  ,
			--	[IP Version]  ,
			--	[Access Bandwidth]  ,
			--	[AccessTableID]  ,
			--	[AccessTableTag]  ,
			--	[Activator Picker]  ,
			--	[Activity Locale]  ,
			--	[Add End to End Monitoring]  ,
			--	[Assigned To]  ,
			--	[Auth_Status]  ,
			--	[CharsID]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Content Type]  ,
			--	[Customer Contact Cell Phone]  ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[CUSTOMER_CSG_LVL]  ,
			--	[CustomerMapping]  ,
			--	[CustomerMappingID]  ,
			--	[Description and Comments]  ,
			--	[Design Document Approval Number]  ,
			--	[Design Document Location]  ,
			--	[End Time] ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Extra Duration]  ,
			--	[FirewallTable]  ,
			--	[FirewallTableID]  ,
			--	[FTN]  ,
			--	[ID]  ,
			--	[InboundNAT]  ,
			--	[InboundNATID]  ,
			--	[Is NDD Updated?]  ,
			--	[Is this a Carrier Partner?]  ,
			--	[Is this a Government Event?]  ,
			--	[Is this a Migration?]  ,
			--	[Is this a Wholesale Partner?]  ,
			--	[Is this an Escalation?]  ,
			--	[Item Title]  ,
			--	[L2P_RECORD]  ,
			--	[L2P_VERIFIED]  ,
			--	[L2P_VERIFIED_DATETIME] ,
			--	[MaskedContName]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[MDS DLCI]  ,
			--	[MDS IP Address]  ,
			--	[MDS Managed]  ,
			--	[MDS Static Route]  ,
			--	[MDS VRF Name]  ,
			--	[MEMBER_CSG_LVL]  ,
			--	[Migration Type]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[MPLS Activity Type]  ,
			--	[MPLS Activity Type2]  ,
			--	[MPLS Event Type]  ,
			--	[Multi VRF Requirements]  ,
			--	[NNI Design Document]  ,
			--	[ON Net Monitoring?]  ,
			--	[OutboundNAT]  ,
			--	[OutboundNATID]  ,
			--	[Primary Request Date] ,
			--	[Published E-Mail CC:]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[Sales Engineer (SE) E-Mail]  ,
			--	[Sales Engineer (SE) Name]  ,
			--	[Sales Engineer (SE) Phone]  ,
			--	[SCRIPT_EMAIL_SENT]  ,
			--	[Secondary Request Date] ,
			--	[Short Notice]  ,
			--	[Slot Picker]  ,
			--	[Submit Type]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[VAS Types]  ,
			--	[VPN Platform Type]  ,
			--	[VRF Name]  ,
			--	[Wholesale Partner]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status]  )
			--SELECT  [Event_ID]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[Circuit Number]  ,
			--	[Created] ,
			--	[Created By]  ,
			--	[Customer Name]  ,
			--	[H1]  ,
			--	[H6]  ,
			--	[Workflow Status]  ,
			--	[Carrier Partner]  ,
			--	[OFF Net Monitoring?]  ,
			--	[Event Status]  ,
			--	[Start Time] ,
			--	[SOWS_ADMIN_SCRIPT]  ,
			--	[Scheduled_Implementation_Start_WF]  ,
			--	[IP Version]  ,
			--	[Access Bandwidth]  ,
			--	[AccessTableID]  ,
			--	[AccessTableTag]  ,
			--	[Activator Picker]  ,
			--	[Activity Locale]  ,
			--	[Add End to End Monitoring]  ,
			--	[Assigned To]  ,
			--	[Auth_Status]  ,
			--	[CharsID]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Content Type]  ,
			--	[Customer Contact Cell Phone]  ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[CUSTOMER_CSG_LVL]  ,
			--	[CustomerMapping]  ,
			--	[CustomerMappingID]  ,
			--	[Description and Comments]  ,
			--	[Design Document Approval Number]  ,
			--	[Design Document Location]  ,
			--	[End Time] ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Extra Duration]  ,
			--	[FirewallTable]  ,
			--	[FirewallTableID]  ,
			--	[FTN]  ,
			--	[ID]  ,
			--	[InboundNAT]  ,
			--	[InboundNATID]  ,
			--	[Is NDD Updated?]  ,
			--	[Is this a Carrier Partner?]  ,
			--	[Is this a Government Event?]  ,
			--	[Is this a Migration?]  ,
			--	[Is this a Wholesale Partner?]  ,
			--	[Is this an Escalation?]  ,
			--	[Item Title]  ,
			--	[L2P_RECORD]  ,
			--	[L2P_VERIFIED]  ,
			--	[L2P_VERIFIED_DATETIME] ,
			--	[MaskedContName]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[MDS DLCI]  ,
			--	[MDS IP Address]  ,
			--	[MDS Managed]  ,
			--	[MDS Static Route]  ,
			--	[MDS VRF Name]  ,
			--	[MEMBER_CSG_LVL]  ,
			--	[Migration Type]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[MPLS Activity Type]  ,
			--	[MPLS Activity Type2]  ,
			--	[MPLS Event Type]  ,
			--	[Multi VRF Requirements]  ,
			--	[NNI Design Document]  ,
			--	[ON Net Monitoring?]  ,
			--	[OutboundNAT]  ,
			--	[OutboundNATID]  ,
			--	[Primary Request Date] ,
			--	[Published E-Mail CC:]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[Sales Engineer (SE) E-Mail]  ,
			--	[Sales Engineer (SE) Name]  ,
			--	[Sales Engineer (SE) Phone]  ,
			--	[SCRIPT_EMAIL_SENT]  ,
			--	[Secondary Request Date] ,
			--	[Short Notice]  ,
			--	[Slot Picker]  ,
			--	[Submit Type]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[VAS Types]  ,
			--	[VPN Platform Type]  ,
			--	[VRF Name]  ,
			--	[Wholesale Partner]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status] 
			--FROM	[COWS].[dbo].[MPLSEvent] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [arch].[MPLSEvent] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[MPLSEvent] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM	[COWS].[dbo].[MPLSEvent] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [dbo].[MPLSEvent] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[MPLSEvent]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			[NGVNEvent]
			--------------------------------------------OK-------------------------------EBENT--------------------
			*/
			--PRINT 'Starting to execute [arch].[NGVNEvent] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[NGVNEvent](
			--	[Event Status]  ,
			--	[Event_ID]  ,
			--	[Is this an Escalation?]  ,
			--	[Assigned To]  ,
			--	[NGVN WF]  ,
			--	[Created By]  ,
			--	[AccessTableID]  ,
			--	[Activator Picker]  ,
			--	[Auth_Status]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[Cable Add Due Date] ,
			--	[Cable Protocol Type]  ,
			--	[CharsID]  ,
			--	[Circuit ID NUA]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Content Type]  ,
			--	[Created] ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[Customer Name]  ,
			--	[Description and Comments]  ,
			--	[End Time] ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Extra Duration]  ,
			--	[FTN]  ,
			--	[GSR Configuration]  ,
			--	[H1]  ,
			--	[H6]  ,
			--	[ID]  ,
			--	[Item Title]  ,
			--	[MaskedContName]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[New SBC Pair]  ,
			--	[Port Mirror Description]  ,
			--	[Port Mirroring Enabled?]  ,
			--	[Primary Request Date] ,
			--	[Primary Switch 6509]  ,
			--	[Product Type]  ,
			--	[Published E-Mail CC:]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[Sales Engineer (SE) E-Mail]  ,
			--	[Sales Engineer (SE) Name]  ,
			--	[Sales Engineer (SE) Phone]  ,
			--	[SBC Pair]  ,
			--	[Secondary Request Date] ,
			--	[Secondary Switch 6509]  ,
			--	[SIP Trunk Table]  ,
			--	[Slot Picker]  ,
			--	[Start Time] ,
			--	[Submit Type]  ,
			--	[Trusted HSRP IP]  ,
			--	[Trusted IP Block Netmask]  ,
			--	[Trusted SD 1st IP]  ,
			--	[Trusted SD 2nd IP]  ,
			--	[Trusted SD Virtual IP]  ,
			--	[Trusted VLAN]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[Untrusted HSRP IP]  ,
			--	[Untrusted IP Block Netwmask]  ,
			--	[Untrusted SD 1st IP]  ,
			--	[Untrusted SD 2nd IP]  ,
			--	[Untrusted SD Virtual IP]  ,
			--	[Untrusted VLAN]  ,
			--	[Workflow Status]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status]   )
			--SELECT  [Event Status]  ,
			--	[Event_ID]  ,
			--	[Is this an Escalation?]  ,
			--	[Assigned To]  ,
			--	[NGVN WF]  ,
			--	[Created By]  ,
			--	[AccessTableID]  ,
			--	[Activator Picker]  ,
			--	[Auth_Status]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[Cable Add Due Date] ,
			--	[Cable Protocol Type]  ,
			--	[CharsID]  ,
			--	[Circuit ID NUA]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Content Type]  ,
			--	[Created] ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[Customer Name]  ,
			--	[Description and Comments]  ,
			--	[End Time] ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Extra Duration]  ,
			--	[FTN]  ,
			--	[GSR Configuration]  ,
			--	[H1]  ,
			--	[H6]  ,
			--	[ID]  ,
			--	[Item Title]  ,
			--	[MaskedContName]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[New SBC Pair]  ,
			--	[Port Mirror Description]  ,
			--	[Port Mirroring Enabled?]  ,
			--	[Primary Request Date] ,
			--	[Primary Switch 6509]  ,
			--	[Product Type]  ,
			--	[Published E-Mail CC:]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[Sales Engineer (SE) E-Mail]  ,
			--	[Sales Engineer (SE) Name]  ,
			--	[Sales Engineer (SE) Phone]  ,
			--	[SBC Pair]  ,
			--	[Secondary Request Date] ,
			--	[Secondary Switch 6509]  ,
			--	[SIP Trunk Table]  ,
			--	[Slot Picker]  ,
			--	[Start Time] ,
			--	[Submit Type]  ,
			--	[Trusted HSRP IP]  ,
			--	[Trusted IP Block Netmask]  ,
			--	[Trusted SD 1st IP]  ,
			--	[Trusted SD 2nd IP]  ,
			--	[Trusted SD Virtual IP]  ,
			--	[Trusted VLAN]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[Untrusted HSRP IP]  ,
			--	[Untrusted IP Block Netwmask]  ,
			--	[Untrusted SD 1st IP]  ,
			--	[Untrusted SD 2nd IP]  ,
			--	[Untrusted SD Virtual IP]  ,
			--	[Untrusted VLAN]  ,
			--	[Workflow Status]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status]
			--FROM	[COWS].[dbo].[NGVNEvent] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [arch].[NGVNEvent] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[NGVNEvent] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM	[COWS].[dbo].[NGVNEvent] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [dbo].[NGVNEvent] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[NGVNEvent]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			[SprintLink]
			--------------------------------------------OK-------------------------------EBENT--------------------
			*/
			--PRINT 'Starting to execute [arch].[NGVNEvent] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[SprintLink](
			--	[Event_ID]  ,
			--	[H1]  ,
			--	[Customer Name]  ,
			--	[Created] ,
			--	[Created By]  ,
			--	[Workflow Status]  ,
			--	[Event Status]  ,
			--	[ID]  ,
			--	[SprintLink WF]  ,
			--	[IP Version]  ,
			--	[AccessTableID]  ,
			--	[AccessTableTag]  ,
			--	[Activator Picker]  ,
			--	[Assigned To]  ,
			--	[Auth_Status]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[CharsID]  ,
			--	[CNSI-Configurations Pre Built Complete]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Connectivity Matrix Location/Name]  ,
			--	[Content Type]  ,
			--	[Customer Contact Cell Phone]  ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[CUSTOMER_CSG_LVL]  ,
			--	[Description and Comments]  ,
			--	[End Time] ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Extra Duration]  ,
			--	[Extra Duration2]  ,
			--	[FTN]  ,
			--	[H6]  ,
			--	[Is Escalation]  ,
			--	[Is this a Government Event?]  ,
			--	[Item Title]  ,
			--	[L2P_RECORD]  ,
			--	[L2P_VERIFIED]  ,
			--	[L2P_VERIFIED_DATETIME] ,
			--	[MaskedContName]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[MDS Managed]  ,
			--	[MEMBER_CSG_LVL]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[Primary Request Date] ,
			--	[Published E-Mail CC:]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[Sales Engineer (SE) E-Mail]  ,
			--	[Sales Engineer (SE) Name]  ,
			--	[Sales Engineer (SE) Phone]  ,
			--	[Secondary Request Date] ,
			--	[Short Notice]  ,
			--	[Slot Picker]  ,
			--	[SprintLink Activity Type]  ,
			--	[SprintLink Event Type]  ,
			--	[Start Time] ,
			--	[Submit Type]  ,
			--	[Total Event Duration]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status]   )
			--SELECT  [Event_ID]  ,
			--	[H1]  ,
			--	[Customer Name]  ,
			--	[Created] ,
			--	[Created By]  ,
			--	[Workflow Status]  ,
			--	[Event Status]  ,
			--	[ID]  ,
			--	[SprintLink WF]  ,
			--	[IP Version]  ,
			--	[AccessTableID]  ,
			--	[AccessTableTag]  ,
			--	[Activator Picker]  ,
			--	[Assigned To]  ,
			--	[Auth_Status]  ,
			--	[Bridge Number]  ,
			--	[Bridge PIN]  ,
			--	[CharsID]  ,
			--	[CNSI-Configurations Pre Built Complete]  ,
			--	[Completed E-Mail CC:]  ,
			--	[Connectivity Matrix Location/Name]  ,
			--	[Content Type]  ,
			--	[Customer Contact Cell Phone]  ,
			--	[Customer Contact Name]  ,
			--	[Customer Contact Pager]  ,
			--	[Customer Contact Pager PIN]  ,
			--	[Customer Contact Phone]  ,
			--	[Customer Email]  ,
			--	[CUSTOMER_CSG_LVL]  ,
			--	[Description and Comments]  ,
			--	[End Time] ,
			--	[Escalation Reason]  ,
			--	[Event Duration]  ,
			--	[Extra Duration]  ,
			--	[Extra Duration2]  ,
			--	[FTN]  ,
			--	[H6]  ,
			--	[Is Escalation]  ,
			--	[Is this a Government Event?]  ,
			--	[Item Title]  ,
			--	[L2P_RECORD]  ,
			--	[L2P_VERIFIED]  ,
			--	[L2P_VERIFIED_DATETIME] ,
			--	[MaskedContName]  ,
			--	[MaskedCustName]  ,
			--	[MaskedCustPhone]  ,
			--	[Maskedemail]  ,
			--	[MDS Managed]  ,
			--	[MEMBER_CSG_LVL]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[Primary Request Date] ,
			--	[Published E-Mail CC:]  ,
			--	[Request Contact Cell Phone]  ,
			--	[Request Contact E-Mail]  ,
			--	[Request Contact Name]  ,
			--	[Request Contact Pager Phone]  ,
			--	[Request Contact Pager PIN]  ,
			--	[Request Contact Phone]  ,
			--	[Sales Engineer (SE) E-Mail]  ,
			--	[Sales Engineer (SE) Name]  ,
			--	[Sales Engineer (SE) Phone]  ,
			--	[Secondary Request Date] ,
			--	[Short Notice]  ,
			--	[Slot Picker]  ,
			--	[SprintLink Activity Type]  ,
			--	[SprintLink Event Type]  ,
			--	[Start Time] ,
			--	[Submit Type]  ,
			--	[Total Event Duration]  ,
			--	[UnMasked_ROW_ID]  ,
			--	[Item Type]  ,
			--	[Path]  ,
			--	[Status]
			--FROM	[COWS].[dbo].[NGVNEvent] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [arch].[NGVNEvent] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[NGVNEvent] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM	[COWS].[dbo].[NGVNEvent] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [dbo].[NGVNEvent] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SprintLink]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			[WFHistory]
			-------------------------------------------OK----------------------EVENT---------------------------
			*/
			--PRINT 'Starting to execute [arch].[WFHistory] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[WFHistory](
			--	[Event ID]  ,
			--	[Action]  ,
			--	[Performed By]  ,
			--	[Date and Time] ,
			--	[Comment]  ,
			--	[Created] ,
			--	[Created By]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[ID]  ,
			--	[Content Type]  ,
			--	[Exempt from Policy]  ,
			--	[Expiration Date] ,
			--	[Fail Codes]  ,
			--	[Failed Activities]  ,
			--	[Item Title]  ,
			--	[Original Expiration Date] ,
			--	[Succssful Activities]  ,
			--	[Item Type]  ,
			--	[Path])
			--SELECT  [Event ID]  ,
			--	[Action]  ,
			--	[Performed By]  ,
			--	[Date and Time] ,
			--	[Comment]  ,
			--	[Created] ,
			--	[Created By]  ,
			--	[Modified] ,
			--	[Modified By]  ,
			--	[ID]  ,
			--	[Content Type]  ,
			--	[Exempt from Policy]  ,
			--	[Expiration Date] ,
			--	[Fail Codes]  ,
			--	[Failed Activities]  ,
			--	[Item Title]  ,
			--	[Original Expiration Date] ,
			--	[Succssful Activities]  ,
			--	[Item Type]  ,
			--	[Path]
			--FROM	[COWS].[dbo].[WFHistory] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [arch].[WFHistory] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[WFHistory] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM	[COWS].[dbo].[WFHistory] WITH(NOLOCK)	WHERE EVENT_ID	IN	(SELECT EVENT_ID FROM	@Event_Table )
			--PRINT 'Finished executing [dbo].[WFHistory] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[WFHistory]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			/*
			[ORDR_Tmp]
			-------------------------------------ORDER-------------------------------------

			*/
			--PRINT 'Starting to execute [arch].[ORDR_Tmp] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--INSERT INTO [arch].[ORDR_Tmp](
			--	[ORDR_ID]   ,
			--	[FTN] ,
			--	[Status]   )
			--SELECT  [ORDR_ID]   ,
			--	[FTN] ,
			--	[Status]	
			--FROM	[COWS].[dbo].[ORDR_Tmp] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
			--PRINT 'Finished executing [arch].[ORDR_Tmp] INSERT at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [dbo].[ORDR_Tmp] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE  FROM	[COWS].[dbo].[ORDR_Tmp] WITH(NOLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@Order_Table)
			--PRINT 'Finished executing [dbo].[ORDR_Tmp] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[ORDR_Tmp]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			-------------------------------------------------------------------------------------------------------------------------
			----------------------------------------------------------SQL ERROR----------------------------------------------------------
			-------------------------------------------------------------------------------------------------------------------------
			/*
			Set @Time=GetDate() RAISERROR ('Starting to execute [dbo].[SQL_ERROR] INSERT at: %s. ', 0, 1,@Time) WITH NOWAIT
			INSERT INTO [dbo].[SQL_ERROR](
						[SQL_ERROR_ID],
						[ERROR_NBR],
						[ERROR_SVRTY_CD] ,
						[ERROR_STATE_CD] ,
						[ERROR_LINE_NBR] ,
						[ERROR_PRC_NAME] ,
						[ERROR_MSG_TXT] ,
						[HOST_NAME] ,
						[TRAN_INFO_TXT] ,
						[CREAT_DT] ,
						[MODFD_DT] ,
						[MODFD_BY_USER_ID])
			SELECT		[SQL_ERROR_ID],
						[ERROR_NBR] ,
						[ERROR_SVRTY_CD] ,
						[ERROR_STATE_CD] ,
						[ERROR_LINE_NBR] ,
						[ERROR_PRC_NAME] ,
						[ERROR_MSG_TXT] ,
						[HOST_NAME] ,
						[TRAN_INFO_TXT] ,
						[CREAT_DT] ,
						[MODFD_DT] ,
						[MODFD_BY_USER_ID]	
			FROM [COWS].[arch].[SQL_ERROR] WHERE [CREAT_DT]<=CONVERT(datetime,@Date)
			Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [dbo].[SQL_ERROR] INSERT at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			Set @Time=GetDate()   RAISERROR ('Starting to execute [arch].[SQL_ERROR] DELETE at: %s. ', 0, 1,@Time) WITH NOWAIT
			DELETE   FROM [COWS].[arch].[SQL_ERROR] WHERE [CREAT_DT]<=CONVERT(datetime,@Date)
			Set @RowCounts=@@RowCount  Set @Time=GetDate() RAISERROR ('Finished executing [arch].[SQL_ERROR] DELETE at:  %s. Impacted Rows %d', 0, 1,@Time, @RowCounts) WITH NOWAIT
			INSERT INTO [arch].[ArchiveLog]( TableName , NumberofRows ,CREAT_DT )SELECT '[SQL_ERROR]',@ROWCOUNTS,CONVERT(VARCHAR(30), GETDATE(), 9) 
			*/

			
			
			--PRINT ''
			Set @Time=GetDate()   RAISERROR ('End REINSTATE the records at: %s. ', 0, 1,@Time) WITH NOWAIT
			--PRINT ''

			RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
			RAISERROR ('****************         END REINSTATE PROCESS COWS            ********************', 0, 1) WITH NOWAIT
			RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT

		
			--PRINT ''
			Set @Time=GetDate()   RAISERROR ('End REINSTATE the records at: %s. ', 0, 1,@Time) WITH NOWAIT
			--PRINT ''

		END

		RAISERROR ('Total number of ORDER Records REINSTATE =  %d. ', 0, 1,@Ctr1) WITH NOWAIT --+ CONVERT(VARCHAR(5), @Ctr1)
		--PRINT ''
		RAISERROR ('Total number of EVENT Records REINSTATE =  %d. ', 0, 1,@Ctr2) WITH NOWAIT --+ CONVERT(VARCHAR(5), @Ctr2)
		--PRINT ''
		

		RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
		RAISERROR ('****************         END REINSTATE PROCESS                  ******************', 0, 1) WITH NOWAIT
		RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT

	END TRY
	BEGIN CATCH
		print XACT_STATE()
		print @@TRANCOUNT
		IF (XACT_STATE()) <> 0 
		BEGIN
			ROLLBACK TRANSACTION;
			RAISERROR ('Error: Transaction RollBack No Operation Perform', 0, 1) WITH NOWAIT
			RAISERROR ('Check Error Select TOP 10 * FROM dbo.SQL_ERROR WITH (NOLOCK)  order by SQL_Error_ID DESC', 0, 1) WITH NOWAIT
		END
		SET DEADLOCK_PRIORITY -5; 
		EXEC [dbo].[insertErrorInfo]
	END CATCH;

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION;

END