ALTER TABLE [arch].[CKT] ADD 
	[RTNG_PRCOL] [varchar](500) NULL, 
	[IP_ADR_QTY] [varchar](20)  NULL,
	[CUST_IP_ADR] [varchar](2000) NULL,
	[SCA_DET] [varchar](1000) NULL,
	[RLTD_VNDR_ORDR_ID] [varchar](100) NULL
	
	
ALTER TABLE [arch].[CKT] ADD 
	[REDSGN_ID] [int] NULL
	
	
ALTER TABLE [arch].[MDS_EVENT] ADD 
	[NID_HOST_NME] [varchar](100) NULL,
	[NID_SERIAL_NBR] [varchar](100) NULL,
	[IPM_DES] [varchar](500) NULL,
	[RLTD_CMPS_NCR_CD] [bit] NULL,
	[RLTD_CMPS_NCR_NME] [varchar](200) NULL,


ALTER TABLE [arch].[FSA_ORDR] ADD 
	[TTRPT_VNDR_TERM] [varchar](12) NULL,
	[TPORT_VNDR_RAW_MRC_AMT] [varchar](25) NULL,
	[TPORT_VNDR_RAW_NRC_AMT] [varchar](25) NULL,
	[VNDR_TERM] [varchar](12) NULL,
	[TPORT_VNDR_RVSD_QOT_CUR] [varchar](25) NULL,	
	
	
ALTER TABLE [arch].[FSA_ORDR] ADD 
	[TTRPT_VNDR_TERM] [varchar](12) NULL,
	[TPORT_VNDR_RAW_MRC_AMT] [varchar](25) NULL,
	[TPORT_VNDR_RAW_NRC_AMT] [varchar](25) NULL,
	[VNDR_TERM] [varchar](12) NULL,
	[TPORT_VNDR_RVSD_QOT_CUR] [varchar](25) NULL,		
	
	
ALTER TABLE [arch].[FSA_ORDR_CPE_LINE_ITEM] ADD 
	[SMRT_ACCT_DOMN_TXT] [varchar](100) NULL,
	[VRTL_ACCT_DOMN_TXT] [varchar](100) NULL,
	[NID_SERIAL_NBR] [varchar](100) NULL,
	[SPRINT_MNTD_FLG] [char](1) NULL,
	[CPE_CUST_PRVD_SERIAL_NBR] [varchar](200) NULL,
	
ALTER TABLE [arch].[FSA_ORDR_CPE_LINE_ITEM] ADD 
	[CUST_NME] [varchar](1000) NULL,


ALTER TABLE [arch].[FSA_ORDR_GOM_XNCI] ADD 
	[USR_PRF_ID] [smallint] NULL,
	
ALTER TABLE [arch].[MDS_EVENT_ODIE_DEV] ADD 
	[SC_CD] [char](1) NULL,
	
	
ALTER TABLE [arch].[ORDR_CNTCT] ADD 
	[PHN_NBR] [varchar](50) NULL,
	[FAX_NBR] [varchar](50) NULL,	