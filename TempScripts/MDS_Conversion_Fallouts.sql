USE COWS
GO

-- To update MDS Bridge Needed = No; for conference bridge numbers populated either empty, null or NA and workflow status not in complete,Reject and Delete
--Update nmds
--	SET		nmds.MDS_BRDG_NEED_ID = 1
--	From	dbo.MDS_EVENT mds WITH (NOLOCK)
--Inner Join	dbo.MDS_EVENT_NEW nmds WITH (ROWLOCK) ON nmds.EVENT_ID = mds.EVENT_ID
--	Where	mds.MDS_BRDG_NEED_CD  = 0
--		AND	ISNULL(mds.CNFRC_BRDG_NBR,'') IN ('null','','NA')
--		AND mds.WRKFLW_STUS_ID NOT IN (7,9,11)

-- To update MDS Bridge Needed = Yes, Other;  for conference bridge numbers populated and MDS bridge is not checked and workflow status not in complete,Reject and Delete
Update nmds
	SET		nmds.MDS_BRDG_NEED_ID = 4
	From	dbo.MDS_EVENT mds WITH (NOLOCK)
Inner Join	dbo.MDS_EVENT_NEW nmds WITH (ROWLOCK) ON nmds.EVENT_ID = mds.EVENT_ID
	Where	mds.MDS_BRDG_NEED_CD  = 0
		AND	ISNULL(mds.CNFRC_BRDG_NBR,'') NOT IN ('null','','NA')
		AND mds.WRKFLW_STUS_ID NOT IN (7,9,11)
		
-- To update MDS Bridge Needed = Yes, MNS;  for conference bridge numbers populated and MDS bridge is not checked and workflow status not in complete,Reject and Delete
Update nmds
	SET		nmds.MDS_BRDG_NEED_ID = 3
	From	dbo.MDS_EVENT mds WITH (NOLOCK)
Inner Join	dbo.MDS_EVENT_NEW nmds WITH (ROWLOCK) ON nmds.EVENT_ID = mds.EVENT_ID
	Where	mds.MDS_BRDG_NEED_CD  = 1
		AND	ISNULL(mds.CNFRC_BRDG_NBR,'') NOT IN ('null','','NA')
		AND mds.WRKFLW_STUS_ID NOT IN (7,9,11)

-- To update MDS Bridge Needed = No; for conference bridge numbers populated either empty, null or NA, MDS bridge needed is checked and workflow status not in complete,Reject,Visible,Retract,Reschedule,Return and Delete
Update nmds
	SET		nmds.MDS_BRDG_NEED_ID = 1
	From	dbo.MDS_EVENT mds WITH (NOLOCK)
Inner Join	dbo.MDS_EVENT_NEW nmds WITH (ROWLOCK) ON nmds.EVENT_ID = mds.EVENT_ID
	Where	mds.MDS_BRDG_NEED_CD  = 1
		AND	ISNULL(mds.CNFRC_BRDG_NBR,'') IN ('null','','NA')
		AND mds.WRKFLW_STUS_ID NOT IN (7,9,11,1,3,8,5)

--To update Business Justication text field when Event is escalted and workflow status not in complete,Reject and Delete
Update nmds
	SET		nmds.BUS_JUSTN_CMNT_TXT = 'Converted Order'
	From	dbo.MDS_EVENT mds WITH (NOLOCK)
Inner Join	dbo.MDS_EVENT_NEW nmds WITH (ROWLOCK) ON nmds.EVENT_ID = mds.EVENT_ID
	Where	mds.ESCL_CD = 1
		AND mds.WRKFLW_STUS_ID NOT IN (7,9,11)
		AND ISNULL(nmds.BUS_JUSTN_CMNT_TXT,'') = ''

-- To update Escalation Reason ID = 'Other' when event is escalated and doesn't have escalated reason ID populated
Update nmds
	Set	nmds.ESCL_REAS_ID = 6
	From	dbo.MDS_EVENT mds WITH (NOLOCK)
Inner Join	dbo.MDS_EVENT_NEW nmds WITH (ROWLOCK) ON nmds.EVENT_ID = mds.EVENT_ID
	Where	mds.ESCL_CD = 1
		AND mds.WRKFLW_STUS_ID NOT IN (7,9,11)
		AND	ISNULL(mds.ESCL_REAS_ID,0) = 0

--To update secondary request date (increment by 5 minutes) for escalated events and secondary and primary date/time match
Update nmds
	SET nmds.SCNDY_REQ_DT = DATEADD(MINUTE,5,nmds.SCNDY_REQ_DT)
	From	dbo.MDS_EVENT mds WITH (NOLOCK)
Inner Join	dbo.MDS_EVENT_NEW nmds WITH (NOLOCK) ON nmds.EVENT_ID = mds.EVENT_ID
	Where	mds.ESCL_CD = 1
		AND mds.WRKFLW_STUS_ID NOT IN (7,9,11)
		AND mds.PRIM_REQ_DT = mds.SCNDY_REQ_DT

-- to update entitlement to No Entitlement when service tier = complete and entitlement is empty
Update mns
	SET		mns.MDS_ENTLMNT_ID = 11
	From	dbo.FSA_MDS_EVENT_NEW nfmds WITH (NOLOCK) 
Inner Join	dbo.MDS_EVENT_MNS mns WITH (ROWLOCK) ON mns.FSA_MDS_EVENT_ID = nfmds.FSA_MDS_EVENT_ID
Inner Join	dbo.MDS_EVENT_NEW nmds WITH (NOLOCK) ON nmds.EVENT_ID = nfmds.EVENT_ID
	Where	mns.MDS_SRVC_TIER_ID = 2
		AND mns.MDS_ENTLMNT_ID = 10
		AND nmds.WRKFLW_STUS_ID NOT IN (7,9,11)

-- update OPT OUT REASON Code 
Update actn
	SET		actn.OPT_OUT_REAS_TXT = act.OPT_OUT_REAS_TXT,
			actn.BUS_JUSTN_TXT = act.BUS_JUSTN_TXT
	FROM	dbo.MDS_MNGD_ACT_NEW actn with (nolock)
Inner Join	dbo.MDS_MNGD_ACT act WITH (NOLOCK) ON act.EVENT_ID = actn.EVENT_ID
	WHERE act.OPT_OUT_CD = 'Y'
		AND actn.OPT_OUT_CD = 'Y'
		AND actn.ODIE_DEV_NME = act.DEV_ID
		AND ISNULL(act.OPT_OUT_REAS_TXT,'') != ''

Update mds
	SET		mds.OPT_OUT_REAS_TXT = act.OPT_OUT_REAS_TXT,
			mds.BUS_JUSTN_TXT = act.BUS_JUSTN_TXT
	FROM	dbo.MDS_MNGD_ACT_NEW actn with (nolock)
Inner Join	dbo.MDS_MNGD_ACT act WITH (NOLOCK) ON act.EVENT_ID = actn.EVENT_ID
Inner Join	dbo.MDS_EVENT_NEW mds ON mds.EVENT_ID = actn.EVENT_ID
	WHERE act.OPT_OUT_CD = 'Y'
		AND actn.OPT_OUT_CD = 'Y'
		AND actn.ODIE_DEV_NME = act.DEV_ID
		AND ISNULL(act.OPT_OUT_REAS_TXT,'') != ''

/*
--Select nmds.EVENT_ID,nmds.ESCL_CD,nmds.ESCL_REAS_ID,nmds.ESCL_BUS_JUST_TXT,nmds.PRIM_REQ_DT,nmds.SCNDY_REQ_DT,DATEADD(MINUTE,5,nmds.SCNDY_REQ_DT) AS MODIFIED_DATE,mds.ESCL_REAS_ID,mds.PRIM_REQ_DT,mds.SCNDY_REQ_DT
--	From	dbo.MDS_EVENT mds WITH (NOLOCK)
--Inner Join	dbo.MDS_EVENT_NEW nmds WITH (NOLOCK) ON nmds.EVENT_ID = mds.EVENT_ID
--	Where	mds.ESCL_CD = 1
--		AND mds.WRKFLW_STUS_ID NOT IN (7,9,11)
--		AND mds.PRIM_REQ_DT = mds.SCNDY_REQ_DT

Select DISTINCT  nfmds.FSA_MDS_EVENT_ID,nfmds.EVENT_ID
	From	
--	dbo.FSA_MDS_EVENT fmds WITH (NOLOCK)
--Inner Join	
	dbo.FSA_MDS_EVENT_NEW nfmds WITH (NOLOCK) 
--Inner Join	dbo.MDS_EVENT_MNS_OE mnsoe WITH (NOLOCK) ON fmds.FSA_MDS_EVENT_ID = mnsoe.FSA_MDS_EVENT_ID
Inner Join	dbo.MDS_EVENT_MNS mns WITH (NOLOCK) ON mns.FSA_MDS_EVENT_ID = nfmds.FSA_MDS_EVENT_ID
Inner Join	dbo.MDS_EVENT_NEW nmds WITH (NOLOCK) ON nmds.EVENT_ID = nfmds.EVENT_ID
	Where	mns.MDS_SRVC_TIER_ID = 2
		AND mns.MDS_ENTLMNT_ID = 10
		AND nmds.WRKFLW_STUS_ID NOT IN (7,9,11)
	ORDER BY nfmds.EVENT_ID

Select DISTINCT fmds.FSA_MDS_EVENT_ID,fmds.EVENT_ID
	From	
	dbo.FSA_MDS_EVENT fmds WITH (NOLOCK)
Inner Join	dbo.MDS_EVENT_MNS_OE mnsoe WITH (NOLOCK) ON fmds.FSA_MDS_EVENT_ID = mnsoe.FSA_MDS_EVENT_ID
Inner Join	dbo.MDS_EVENT mds WITH (NOLOCK) ON mds.EVENT_ID = fmds.EVENT_ID
	Where	mnsoe.MDS_SRVC_TIER_ID = 2
		AND mnsoe.MDS_ENTLMNT_ID = 10
		AND mds.WRKFLW_STUS_ID NOT IN (7,9,11)
	ORDER BY fmds.EVENT_ID
		
Select * From dbo.LK_WRKFLW_STUS
 */