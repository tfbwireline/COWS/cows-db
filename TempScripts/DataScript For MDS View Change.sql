USE COWS
GO

UPDATE DVC
SET DVC.DSPL_COL_ID=111
FROM DBO.DSPL_VW_COL DVC INNER JOIN
DBO.DSPL_VW DV WITH (NOLOCK) ON DV.DSPL_VW_ID = DVC.DSPL_VW_ID
WHERE DV.SITE_CNTNT_ID=5
AND DVC.DSPL_COL_ID = 3

UPDATE DV
SET DV.SORT_BY_COL_1_ID=111
FROM 
DBO.DSPL_VW DV
WHERE DV.SITE_CNTNT_ID=5
AND DV.SORT_BY_COL_1_ID = 3

UPDATE DV
SET DV.SORT_BY_COL_2_ID=111
FROM 
DBO.DSPL_VW DV
WHERE DV.SITE_CNTNT_ID=5
AND DV.SORT_BY_COL_2_ID = 3