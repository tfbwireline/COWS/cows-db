USE [COWS]
GO

/****** Object:  StoredProcedure [tmp].[DELETEBatchEvents]    Script Date: 12/09/2014 13:51:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---- =============================================
---- Author:		Md. Monir
---- Create date:   12/09/2014
---- Description:	This SP is used to Single DELETE the Events by Event_ID ..
---- =============================================


ALTER PROCEDURE [tmp].[DELETEBatchevents](
	@NoOfOrdrDlt   int
)
AS
/* 
SP  DELETEBatchEvents
All The Events will be deleted base on criteria COWS.dbo.Event WITH (NOLOCK) Where SCURD_CD=1
The parameter is there for you to set How many records you want to delete per batch Example Records
In table is 500   and if your parameter is 100  it means it will delete 100 record per loop
This SP Calls
 SP [CheckDeleteTime]  to check the Time Constraint You set in the  COWS Table
SELECT *  FROM [COWS].[dbo].[LK_SYS_CFG] WITH (NOLOCK) WHERE cfg_id in (63,64)
It Will Also Call 
SP  [OrdersEvents_DisableEnabledCONSTRAINTS]   to Disable and Enable Constraints
On Related Tables so that we can execute delete.
It Will Eventually Call 
SP   DELETEEventsByEVENT_ID  for actual Operation. Which Deletes data as a whole batch  base on the parameter value  it receives
SP uses Proper Error Trapping and  Transactions So that for any failure it will either Rollback
Transaction

*/
BEGIN
		SET NOCOUNT ON;
		SET	DEADLOCK_PRIORITY	10;
		BEGIN TRANSACTION;
			BEGIN TRY
				If(@NoOfOrdrDlt Is NULL) SET @NoOfOrdrDlt=1000   /* If Not Supplied How many TNs Must be deleted  than choose 1000*/			
				DECLARE		@CurID INT,@MaxID INT, @LOOP INT
				DECLARE		@TN	VARCHAR(10)
				declare		@TOk int
				PRINT 'Checking time............'
				EXEC @TOk=[tmp].[CheckDeleteTime]     /*  Check Allowed Delete Time From this table Copy setup from CAPTAIN*/
				--SET @TOk=1   /* Temporary Code Remove when Time setup done For COWS */
				PRINT @TOk
				IF(@TOk>0)
				BEGIN
					PRINT 'OK time............'
					PRINT 'Checking Events Table with SCURD_CD=1............'
					SELECT @MaxID=COUNT(DISTINCT EVENT_ID) FROM COWS.dbo.EVENT WITH (NOLOCK) Where SCURD_CD=1
					
					IF    (@MaxID > 0)
					BEGIN
						IF(@MaxID > @NoOfOrdrDlt)
						BEGIN
							SET @LOOP = ROUND(	(CONVERT(numeric(10,2),@MaxID)   /
							 CONVERT(numeric(10,2),@NoOfOrdrDlt)+1
							),0)
						END
						ELSE
						BEGIN
							SET @LOOP=1
						END
						PRINT 'OK Event Table with SCURD_CD 1............'
						PRINT 'Start executing [tmp].[OrdersEvents_DisableEnabledCONSTRAINTS] at:' + CONVERT(VARCHAR(30), GETDATE(), 9)
						EXEC [tmp].[OrdersEvents_DisableEnabledCONSTRAINTS] 0
						PRINT 'Finished executing [tmp].[OrdersEvents_DisableEnabledCONSTRAINTS] at:' + CONVERT(VARCHAR(30), GETDATE(), 9)
						PRINT ''
						PRINT '*********************************************************************************'
									
						EXEC @TOk=[tmp].[CheckDeleteTime]     /*  Check Allowed Delete Time From this table Copy setup from CAPTAIN*/
						--SET @TOk=1   /* Temporary Code Remove when Time setup done For COWS */
						IF(@TOk>0)
						BEGIN 
							set @CurID = 1
							PRINT 'Procedure [tmp].[DELETEEventsByEVENT_ID]  will be called times: '
							PRINT @LOOP
							WHILE(@LOOP>0)
							BEGIN
								PRINT 'Calling [tmp].[DELETEEventsByEVENT_ID] ' +' '+CAST(@CurID as VARCHAR(10)) + ' Time'
								EXEC  [tmp].[DELETEEventsByEVENT_ID] @NoOfOrdrDlt
								PRINT @NoOfOrdrDlt 
								PRINT 'No of Order DELETED'
								/* Reset Alternative Cursor */
								set @CurID = @CurID +1
								set @LOOP  = @LOOP - 1
								IF (@LOOP<1) BREAK
								EXEC @TOk=[tmp].[CheckDeleteTime]     /*  Check Allowed Delete Time From this table Copy setup from CAPTAIN*/
								--SET @TOk=1   /* Temporary Code Remove when Time setup done For COWS */
								IF(@TOk<1) BREAK
							END
						END
						ELSE
						BEGIN
							Print   'TIME OVER'
						END
						PRINT '*********************************************************************************'
						PRINT 'Start executing [tmp].[OrdersEvents_DisableEnabledCONSTRAINTS] at:' + CONVERT(VARCHAR(30), GETDATE(), 9)
						EXEC [tmp].[OrdersEvents_DisableEnabledCONSTRAINTS] 1
						PRINT 'Finished executing [tmp].[OrdersEvents_DisableEnabledCONSTRAINTS] at:' + CONVERT(VARCHAR(30), GETDATE(), 9)
						PRINT ''
						PRINT '*********************************************************************************'
								
					END
					ELSE
					BEGIN
						Print   'NO Event TO DELETE'
					END
				END
				ELSE  PRINT 'TIME OVER CANT BE DELETED'

		END TRY
		BEGIN CATCH
			IF (XACT_STATE()) <> 0 
			BEGIN
				ROLLBACK TRANSACTION;
				Print 'Error: Transaction RollBack No Operation Perform'
				Print 'Check: Select * FROM dbo.SQL_ERROR WITH (NOLOCK)  order by SQL_Error_ID DESC'
			END
			SET DEADLOCK_PRIORITY -5; 
			EXEC [dbo].[insertErrorInfo]
		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;

END



GO


