USE [COWS]
GO

/****** Object:  StoredProcedure [tmp].[DELETEOrdersByORDR_ID]    Script Date: 12/09/2014 13:51:36 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

---- =============================================
---- Author:		Md. Mahmud Monir
---- Create date:   12/09/2011
---- Description:	This SP is used to Single DELETE the Orders by ORDR_ID ..
---- =============================================


ALTER PROCEDURE [tmp].[DELETEOrdersByORDR_ID](
	 @NoOfOrdrDlt   int
	)
AS
--DECLARE @NoOfOrdrDlt   int
--SET     @NoOfOrdrDlt=1
BEGIN
/* 
SP  DELETEBatchOrders
All The Orders will be deleted base on criteria COWS.dbo.ORDR WITH (NOLOCK) Where SCURD_CD=1
The parameter is there for you to set How many records you want to delete per batch Example Records
In table is 500   and if your parameter is 100  it means it will delete 100 record per loop
This SP Calls
 SP [CheckDeleteTime]  to check the Time Constraint You set in the  COWS Table
SELECT *  FROM [COWS].[dbo].[LK_SYS_CFG] WITH (NOLOCK) WHERE cfg_id in (63,64)
It Will Also Call 
SP  [OrdersEvents_DisableEnabledCONSTRAINTS]   to Disable and Enable Constraints
On Related Tables so that we can execute delete.
It Will Eventually Call 
SP   DELETEOrdersByORDR_ID  for actual Operation. Which Deletes data as a whole batch  base on the parameter value  it receives
SP uses Proper Error Trapping and  Transactions So that for any failure it will either Rollback
Transaction

*/


	
	SET	DEADLOCK_PRIORITY	10;
	PRINT 'started [COWS].[DELETEOrdersByORDR_ID] SP'


	PRINT '*********************************************************************************'
	PRINT '****************         BEGIN DELETE PROCESS                  ******************'
	PRINT '*********************************************************************************'

	PRINT ''
	PRINT 'Begin Defining Variables at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)

		
			
	DECLARE     @XMNCPONTableBATCH   TABLE(
				ID			INT IDENTITY(1,1),
				ORDR_ID		Int
	)
	DECLARE	@RECID	TABLE (
	         ID			INT IDENTITY(1,1),
			 REQ_ID	INT)
	DECLARE	@CKTID	TABLE (
	         ID			INT IDENTITY(1,1),
			 CKT_ID	INT)
	DECLARE @VNDRORDRID TABLE (
			 ID			INT IDENTITY(1,1),
			 VNDR_ORDR_ID	INT)

	PRINT 'Start Loading @XMNCPONTableBATCH at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
	INSERT INTO @XMNCPONTableBATCH     (ORDR_ID)	
	EXEC ('Select Top '+@NoOfOrdrDlt+' ORDR_ID FROM COWS.dbo.ORDR WITH (NOLOCK) Where SCURD_CD=1 ORDER BY ORDR_ID ASC')
	PRINT 'End Defining Variables at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
	PRINT ''
	PRINT 'Start Loading @RECID/@CKTID/@VNDRORDRID at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
	INSERT INTO	@RECID (REQ_ID)
	SELECT	DISTINCT REQ_ID
	FROM	COWS.dbo.ODIE_REQ WITH(NOLOCK) 
	WHERE	ORDR_ID IN(SELECT DISTINCT ORDR_ID FROM @XMNCPONTableBATCH )

	INSERT INTO	@VNDRORDRID (VNDR_ORDR_ID)
	SELECT	DISTINCT VNDR_ORDR_ID
	FROM	COWS.dbo.VNDR_ORDR WITH(NOLOCK) 
	WHERE	ORDR_ID IN(SELECT DISTINCT ORDR_ID FROM @XMNCPONTableBATCH )

	INSERT INTO	@CKTID (CKT_ID)
	SELECT	DISTINCT CKT_ID
	FROM	COWS.dbo.CKT WITH(NOLOCK) 
	WHERE	ORDR_ID IN(SELECT DISTINCT ORDR_ID FROM @XMNCPONTableBATCH )

	INSERT INTO	@CKTID (CKT_ID)
	SELECT	DISTINCT CKT_ID
	FROM	COWS.dbo.CKT WITH(NOLOCK) 
	WHERE	VNDR_ORDR_ID IN(SELECT DISTINCT VNDR_ORDR_ID FROM @VNDRORDRID )

	
	PRINT 'Finished Loading @RECID/@CKTID/@VNDRORDRID at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)	
		
	PRINT 'Begin Taking Temp Data into Variables at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
		

		
	DECLARE @Ctr	INT
	SET		@Ctr	= 0
	SELECT  @CTr	= COUNT(1) FROM @XMNCPONTableBATCH

	PRINT 'End Taking Temp Data into Variables at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
	PRINT ''
	BEGIN TRANSACTION;
		SET NOCOUNT ON;
		BEGIN TRY
		IF	@Ctr > 0
		BEGIN 
			PRINT '*********************************************************************************'
			PRINT '****************         BEGIN DELETE PROCESS COWS          *********************'
			PRINT '*********************************************************************************'
			PRINT 'Begin Deleting the records at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
			/*
			PRINT 'Starting to execute frmLSR UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			UPDATE [COWS].[dbo].[NCCO_ORDR]						WITH(ROWLOCK)	SET ORDR_ID	= NULL	WHERE ORDR_ID IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing frmLSR UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			
			*/

			PRINT 'Starting to execute [NCCO_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].[NCCO_ORDR]			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [NCCO_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ACT_TASK] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ACT_TASK				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ACT_TASK] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [CCD_HIST_REAS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].CCD_HIST_REAS WITH(ROWLOCK) WHERE CCD_HIST_ID IN (SELECT CCD_HIST_ID FROM [COWS].[dbo].CCD_HIST WITH(NOLOCK) WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)  )
			PRINT 'Finished executing [CCD_HIST_REAS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [CCD_HIST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].CCD_HIST				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [CCD_HIST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [AD_EVENT_ACCS_TAG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].AD_EVENT_ACCS_TAG		WITH(ROWLOCK)	WHERE CKT_ID	IN	(SELECT CONVERT(varchar(10), CKT_ID) FROM	@CKTID)
			PRINT 'Finished executing [AD_EVENT_ACCS_TAG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [CKT_COST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].CKT_COST				WITH(ROWLOCK)	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
			PRINT 'Finished executing [CKT_COST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [CKT_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].CKT_MS					WITH(ROWLOCK)	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
			PRINT 'Finished executing [CKT_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR] UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			UPDATE  [COWS].[dbo].FSA_ORDR WITH(ROWLOCK)  SET 	CKT_ID=NULL				WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
			PRINT 'Finished executing [FSA_ORDR] UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [MDS_EVENT_FMS_CKT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].MDS_EVENT_FMS_CKT		WITH(ROWLOCK)	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
			PRINT 'Finished executing [MDS_EVENT_FMS_CKT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [MPLS_ACCS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			UPDATE  [COWS].[dbo].MPLS_ACCS				WITH(ROWLOCK) SET 	CKT_ID=NULL	WHERE CKT_ID	IN	(SELECT CKT_ID FROM	@CKTID)
			PRINT 'Finished executing [MPLS_ACCS] UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [CKT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].CKT					WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [CKT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [EMAIL_REQ] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].EMAIL_REQ				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [EMAIL_REQ] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_IP_ADR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_IP_ADR				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_IP_ADR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_MDS_EVENT_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_MDS_EVENT_ORDR		WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_MDS_EVENT_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR_BILL_LINE_ITEM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_ORDR_BILL_LINE_ITEM    WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_ORDR_BILL_LINE_ITEM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR_CPE_LINE_ITEM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_ORDR_CPE_LINE_ITEM    WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_ORDR_CPE_LINE_ITEM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR_CSC] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_ORDR_CSC			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_ORDR_CSC] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			
			--PRINT 'Starting to execute [FSA_ORDR_CUST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE FROM [COWS].[dbo].FSA_ORDR_CUST			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			--PRINT 'Finished executing [FSA_ORDR_CUST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR_CUST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_ORDR_CUST			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_ORDR_CUST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR_GOM_XNCI] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_ORDR_GOM_XNCI		WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_ORDR_GOM_XNCI] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR_MSG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_ORDR_MSG			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_ORDR_MSG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR_RELTD_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_ORDR_RELTD_ORDR    WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_ORDR_RELTD_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR_VAS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_ORDR_VAS			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_ORDR_VAS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_ORDR				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [FSA_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			
			PRINT 'Starting to execute [IPL_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].IPL_ORDR				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [IPL_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [IPL_ORDR_ACCS_INFO] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].IPL_ORDR_ACCS_INFO		WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [IPL_ORDR_ACCS_INFO] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [IPL_ORDR_CKT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].IPL_ORDR_CKT			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [IPL_ORDR_CKT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [IPL_ORDR_EQPT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].IPL_ORDR_EQPT			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [IPL_ORDR_EQPT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [L2P_LOG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].L2P_LOG				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [L2P_LOG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [MDS_EVENT_ACCS] UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			UPDATE [COWS].[dbo].MDS_EVENT_ACCS	WITH(ROWLOCK) SET ORDR_ID=NULL	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [MDS_EVENT_ACCS] UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [MDS_EVENT_CPE] UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			UPDATE [COWS].[dbo].MDS_EVENT_CPE WITH(ROWLOCK) SET ORDR_ID=NULL WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [MDS_EVENT_CPE] UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [MDS_EVENT_MNGD_DEV] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].MDS_EVENT_MNGD_DEV		WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [MDS_EVENT_MNGD_DEV] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [MDS_EVENT_MNS] UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			UPDATE [COWS].[dbo].MDS_EVENT_MNS	WITH(ROWLOCK) SET ORDR_ID=NULL	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [MDS_EVENT_MNS] UPDATE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [MDS_EVENT_MNS_OE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].MDS_EVENT_MNS_OE		WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [MDS_EVENT_MNS_OE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [NCCO_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].NCCO_ORDR				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [NCCO_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [FSA_MDS_EVENT_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].FSA_MDS_EVENT_ORDR     WITH(ROWLOCK)	WHERE REQ_ID	IN	(SELECT REQ_ID FROM	@RECID)
			PRINT 'Finished executing [FSA_MDS_EVENT_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [MDS_MNGD_ACT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].MDS_MNGD_ACT		    WITH(ROWLOCK)	WHERE REQ_ID	IN	(SELECT REQ_ID FROM	@RECID)
			PRINT 'Finished executing [MDS_MNGD_ACT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [MDS_MNGD_ACT_NEW] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].MDS_MNGD_ACT_NEW       WITH(ROWLOCK)	WHERE REQ_ID	IN	(SELECT REQ_ID FROM	@RECID)
			PRINT 'Finished executing [MDS_MNGD_ACT_NEW] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ODIE_DISC_REQ] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ODIE_DISC_REQ          WITH(ROWLOCK)	WHERE REQ_ID	IN	(SELECT REQ_ID FROM	@RECID)
			PRINT 'Finished executing [ODIE_DISC_REQ] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ODIE_RSPN] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ODIE_RSPN              WITH(ROWLOCK)	WHERE REQ_ID	IN	(SELECT REQ_ID FROM	@RECID)
			PRINT 'Finished executing [ODIE_RSPN] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ODIE_RSPN_INFO] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ODIE_RSPN_INFO         WITH(ROWLOCK)	WHERE REQ_ID	IN	(SELECT REQ_ID FROM	@RECID)
			PRINT 'Finished executing [ODIE_RSPN_INFO] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ODIE_REQ] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ODIE_REQ				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ODIE_REQ] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			--PRINT 'Starting to execute [ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE FROM [COWS].[dbo].ORDR					WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			--PRINT 'Finished executing [ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_ADR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_ADR				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_ADR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_CKT_CHG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_CKT_CHG			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_CKT_CHG] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_CNTCT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_CNTCT				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_CNTCT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_EXP] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_EXP				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_EXP] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_GRP] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_GRP				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_GRP] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_H5_DOC] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_H5_DOC			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_H5_DOC] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_HOLD_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_HOLD_MS			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_HOLD_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_JPRDY] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_JPRDY				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_JPRDY] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_MS				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_NTE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_NTE				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_NTE] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_REC_LOCK] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_REC_LOCK			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_REC_LOCK] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_STDI_HIST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_STDI_HIST			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_STDI_HIST] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_Tmp] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[cnv].ORDR_Tmp				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_Tmp] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [ORDR_VLAN] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR_VLAN				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR_VLAN] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [SIP_TRNK_GRP] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].SIP_TRNK_GRP			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [SIP_TRNK_GRP] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [TRPT_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			UPDATE TRPT_ORDR   WITH(ROWLOCK) SET VNDR_ORDR_ID=NULL	WHERE VNDR_ORDR_ID	IN	(SELECT LTRIM(CONVERT(VARCHAR(30),VNDR_ORDR_ID)) FROM	@VNDRORDRID) OR ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)                   
			DELETE FROM [COWS].[dbo].TRPT_ORDR				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [TRPT_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [USER_WFM_ASMT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].USER_WFM_ASMT			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [USER_WFM_ASMT] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			
			--PRINT 'Starting to execute [VNDR_ORDR_EMAIL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			--DELETE FROM [COWS].[dbo].VNDR_ORDR_EMAIL WITH(ROWLOCK)   WHERE VNDR_ORDR_EMAIL_ID	IN (SELECT DISTINCT VNDR_ORDR_EMAIL_ID FROM [COWS].[dbo].VNDR_ORDR_EMAIL WITH(NOLOCK) WHERE VNDR_ORDR_ID	IN	(SELECT VNDR_ORDR_ID FROM	@VNDRORDRID)) 
			--PRINT 'Finished executing [VNDR_ORDR_EMAIL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [VNDR_ORDR_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].VNDR_ORDR_MS    WITH(ROWLOCK)   WHERE VNDR_ORDR_EMAIL_ID	IN (SELECT DISTINCT VNDR_ORDR_EMAIL_ID FROM [COWS].[dbo].VNDR_ORDR_EMAIL WITH(NOLOCK) WHERE VNDR_ORDR_ID	IN	(SELECT VNDR_ORDR_ID FROM	@VNDRORDRID)) 
			PRINT 'Finished executing [VNDR_ORDR_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.' 
			PRINT 'Starting to execute [VNDR_ORDR_EMAIL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].VNDR_ORDR_EMAIL WITH(ROWLOCK) 	WHERE VNDR_ORDR_ID	IN	(SELECT VNDR_ORDR_ID FROM	@VNDRORDRID)
			PRINT 'Finished executing [VNDR_ORDR_EMAIL] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.' 
			PRINT 'Starting to execute [VNDR_ORDR_FORM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)   
			DELETE FROM [COWS].[dbo].VNDR_ORDR_FORM  WITH(ROWLOCK) 	WHERE VNDR_ORDR_ID	IN	(SELECT VNDR_ORDR_ID FROM	@VNDRORDRID)
			PRINT 'Finished executing [VNDR_ORDR_FORM] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [VNDR_ORDR_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)   
			DELETE FROM [COWS].[dbo].VNDR_ORDR_MS    WITH(ROWLOCK) 	WHERE VNDR_ORDR_ID	IN	(SELECT VNDR_ORDR_ID FROM	@VNDRORDRID) 
			PRINT 'Finished executing [VNDR_ORDR_MS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'      
			PRINT 'Starting to execute [VNDR_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].VNDR_ORDR				WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [VNDR_ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [WG_PROF_STUS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].WG_PROF_STUS			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [WG_PROF_STUS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			PRINT 'Starting to execute [WG_PTRN_STUS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].WG_PTRN_STUS			WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [WG_PTRN_STUS] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			
			PRINT 'Starting to execute [ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9)
			DELETE FROM [COWS].[dbo].ORDR					WITH(ROWLOCK)	WHERE ORDR_ID	IN	(SELECT ORDR_ID FROM	@XMNCPONTableBATCH)
			PRINT 'Finished executing [ORDR] DELETE at: ' +  CONVERT(VARCHAR(30), GETDATE(), 9) + '. Impacted Rows: ' + CONVERT(VARCHAR(50),@@ROWCOUNT) + '.'
			
			
			PRINT ''
			PRINT 'End Deleting the records at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
			PRINT ''

			PRINT '*********************************************************************************'
			PRINT '****************         END DELETE PROCESS COWS            ******************'
			PRINT '*********************************************************************************'

		
			PRINT ''
			PRINT 'End Deleting the records at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
			PRINT ''

		
		END

		PRINT 'Total number of ORDR Records removed = ' + CONVERT(VARCHAR(5), @Ctr)
		PRINT ''
		
		

		PRINT '*********************************************************************************'
		PRINT '****************         END DELETE PROCESS                    ******************'
		PRINT '*********************************************************************************'

	--END TRY

	--BEGIN CATCH
	--	EXEC [dbo].[insertErrorInfo]
	--	IF (XACT_STATE()) <> 0 
	--		ROLLBACK
	--SET DEADLOCK_PRIORITY -5; 
	--END CATCH

	END TRY
	BEGIN CATCH
		IF (XACT_STATE()) <> 0 
		BEGIN
			ROLLBACK TRANSACTION;
			Print 'Error: Transaction RollBack No Operation Perform'
			Print 'Check: Select * FROM dbo.SQL_ERROR WITH (NOLOCK)  order by SQL_Error_ID DESC'
		END
		SET DEADLOCK_PRIORITY -5; 
		EXEC [dbo].[insertErrorInfo]
	END CATCH;

	IF @@TRANCOUNT > 0
		COMMIT TRANSACTION;
END




GO


