USE [COWS]
GO

/****** Object:  StoredProcedure [tmp].[CheckDeleteTime]    Script Date: 12/10/2014 08:02:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [tmp].[CheckDeleteTime](
	@Valid int =null   OUTPUT
  )
  AS
  BEGIN
	Declare @HourNow	 int,@MintNow	 int,@HourStartH  int,@HourEndH    int,@HourStartM  int,@HourEndM    int
	SELECT @HourStartH=SUBSTRING([PRMTR_VALU_TXT], 1, 2)  FROM [COWS].[dbo].[LK_SYS_CFG] WITH (NOLOCK) WHERE [PRMTR_NME] = 'Delete Start Time'
	SELECT @HourEndH  =SUBSTRING([PRMTR_VALU_TXT], 1, 2)  FROM [COWS].[dbo].[LK_SYS_CFG] WITH (NOLOCK) WHERE [PRMTR_NME] = 'Delete End Time'
	SELECT @HourStartM=SUBSTRING([PRMTR_VALU_TXT], 4, 2)  FROM [COWS].[dbo].[LK_SYS_CFG] WITH (NOLOCK) WHERE [PRMTR_NME] = 'Delete Start Time'
	SELECT @HourEndM  =SUBSTRING([PRMTR_VALU_TXT], 4, 2)  FROM [COWS].[dbo].[LK_SYS_CFG] WITH (NOLOCK) WHERE [PRMTR_NME] = 'Delete End Time'
	SELECT  @HourNow   = DatePart(hh,getDate()) 
	SELECT  @MintNow   = DatePart(mi,getDate())
	Set @Valid = 0
	IF(@HourEndH > @HourStartH )
	BEGIN
		if (@HourNow>= @HourStartH AND @HourNow<@HourEndH )
		BEGIN
			Set @Valid = 1
		END
	END
	IF(@HourEndH < @HourStartH )
	BEGIN
		if ((@HourNow>=@HourStartH AND @HourNow < 24 )or (@HourNow>=0 AND @HourNow<@HourEndH))
		BEGIN
			Set @Valid = 1
		END
	END
	RETURN @Valid
  END
GO


