USE [COWS]
GO

/****** Object:  Table [arch].[DeletedOrderEvent]    Script Date: 03/09/2015 11:49:16 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [arch].[DeletedOrderEvent](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ORDR_ID] [varchar](max) NULL,
	[EVENT_ID] [varchar](max) NULL,
	[Processed] [varchar](1) NULL,
	[ProcessedTIME] [datetime] NULL,
	[Mood] [int] NULL,
	[Action] [varchar](20) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


