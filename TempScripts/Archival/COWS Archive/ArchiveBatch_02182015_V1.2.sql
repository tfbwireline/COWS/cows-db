USE [COWS]
GO

/****** Object:  StoredProcedure [tmp].[DELETEBatchEvents]    Script Date: 12/09/2014 13:51:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---- =============================================
---- Author:		Md. Monir
---- Create date:   12/09/2014
---- Description:	This SP is used to Single DELETE the Events by Event_ID ..
---- Corrected date:   02/07/2015
---- Description:	Modified to adjust to EventType and Ordr
---- Corrected date:   02/16/2015
---- Description:	Modified to use raise Error and save Order/Events that are being archived.
---- Corrected date:   03/06/2015
---- Description:	removed   SET @TOk=1
---- =============================================

ALTER PROCEDURE [arch].[DELETEBatcEventOrder](
	@Date   DateTime
)
AS
/* 
[arch].[spCOWS_Archive]  Will be called by passing either ODER_ID or EVENT_ID and will delete 1 by 1
Calling  PROCEDURE [arch].[spCOWS_Archive]  @Mood Must Reflect this:
------------------------------------------------------------------------------------------------
@Mood=???
		@Mood=0=ORDR Only No Eevents Table  , @Event_type_ID = 1=AD, @Event_type_ID = 2=NGVN 
		@Event_type_ID = 3=MPLS, @Event_type_ID = 4= SPLK 
		@Event_type_ID = 5=MDS tables  and both ORDR Table As well, 
		@Mood = 6 = MDS EVENTS only Tables  No Order no other events also. 
------------------------------------------------------------------------------------------------
*/
BEGIN
		SET NOCOUNT ON;
		SET	DEADLOCK_PRIORITY	10;
		BEGIN TRANSACTION;
			BEGIN TRY
				
				
				DECLARE		@CurID INT,@MaxID INT, @LOOP INT,@MaxID1 INT, @LOOP1 INT, @Mood INT, @FKMood int
				DECLARE		@RefNumber	int
				declare		@TOk int
				
				/*
				CREATE    TABLE [arch].[DeletedOrderEvent]   (
				ID			INT IDENTITY(1,1),
				ORDR_ID		VARCHAR(MAX)	,
				EVENT_ID	VARCHAR(MAX),
				Processed   VARCHAR(1),
				ProcessedTIME dateTime,
				Mood INT	)
				*/
				
				DECLARE     @Order   TABLE(
				ID			INT IDENTITY(1,1),
				ORDR_ID		Int	,
				Mood		int default(0),
				Processed   int default(0) )
				DECLARE     @Event   TABLE(
				ID			INT IDENTITY(1,1),
				EVENT_ID	Int,
				Mood		int default(0),
				Processed   int default(0)	)
				------------------------------------------------------------------------------------------
				------------------------------------------------------------------------------------------
				--This select is to capture all mds events and related orders, since it is many-many relation, 
				--have 4 conditions in where clause to make sure there is no overlap

				declare @OrderTable table (id int identity(1,1), orderid int, eventid int, flag bit)
				declare @orderids varchar(max)
				declare @eventids varchar(max)
				declare @orderid int
				 
				set @orderids = ''
				set @eventids = ''
				set @FKMood   = 0
				SET @CurID= 0
				INSERT INTO @OrderTable
				SELECT distinct od.ORDR_ID, ev.EVENT_ID ,0        /*-------------------1-------------------*/
				FROM dbo.[ORDR] od WITH (NOLOCK) INNER JOIN
					dbo.FSA_MDS_EVENT_ORDR fme WITH (NOLOCK) ON od.ORDR_ID = fme.ORDR_ID INNER JOIN
					dbo.ACT_TASK at WITH (NOLOCK) ON at.ORDR_ID = od.ORDR_ID INNER JOIN
					dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID = fme.EVENT_ID INNER JOIN
					dbo.EVENT_HIST eh WITH (NOLOCK) ON eh.EVENT_ID = ev.EVENT_ID
				WHERE ev.EVENT_TYPE_ID=5
					AND at.TASK_ID = 1001
					AND at.CREAT_DT <= CONVERT(datetime,@Date)
					AND eh.ACTN_ID=17
					AND eh.CREAT_DT <= CONVERT(datetime,@Date)
					AND NOT EXISTS (SELECT 'x'
										   FROM dbo.[EVENT] ev2 WITH (NOLOCK) INNER JOIN
												dbo.FSA_MDS_EVENT_ORDR fme2 WITH (NOLOCK) ON fme2.EVENT_ID = ev2.EVENT_ID INNER JOIN
												  dbo.ACT_TASK at2 WITH (NOLOCK) ON at2.ORDR_ID = fme2.ORDR_ID
										   WHERE ev2.EVENT_ID = ev.EVENT_ID
											 AND at2.TASK_ID = 1001
											 AND at2.CREAT_DT > CONVERT(datetime,@Date)) -- No Events with other orders which have later completion date 
					AND NOT EXISTS (SELECT 'x'
											   FROM dbo.[EVENT] ev3 WITH (NOLOCK) INNER JOIN
													dbo.FSA_MDS_EVENT_ORDR fme3 WITH (NOLOCK) ON fme3.EVENT_ID = ev3.EVENT_ID
											   WHERE ev3.EVENT_ID = ev.EVENT_ID
												 AND NOT EXISTS (SELECT 'x' FROM dbo.ACT_TASK at3 WITH (NOLOCK)
																			 WHERE at3.ORDR_ID = fme3.ORDR_ID
																			   AND at3.TASK_ID = 1001)) -- No Events with other orders which are still pending
					AND NOT EXISTS (SELECT 'x'
											   FROM dbo.FSA_MDS_EVENT_ORDR fme4 WITH (NOLOCK) INNER JOIN
											   dbo.EVENT ev4 WITH (NOLOCK) ON ev4.EVENT_ID = fme4.EVENT_ID INNER JOIN 
											   dbo.EVENT_HIST eh4 WITH (NOLOCK) ON eh4.EVENT_ID = ev4.EVENT_ID
											   WHERE fme4.ORDR_ID = od.ORDR_ID
												 AND eh4.ACTN_ID=17
												 AND eh4.CREAT_DT > CONVERT(datetime,@Date)) -- No orders with other events which have later completion date
					AND NOT EXISTS (SELECT 'x'
											   FROM dbo.FSA_MDS_EVENT_ORDR fme5 WITH (NOLOCK) INNER JOIN
											   dbo.EVENT ev5 WITH (NOLOCK) ON ev5.EVENT_ID = fme5.EVENT_ID
											   WHERE fme5.ORDR_ID = od.ORDR_ID
												 AND NOT EXISTS (SELECT 'x'
																			 FROM dbo.EVENT_HIST eh5 WITH (NOLOCK)
																			 WHERE eh5.EVENT_ID = ev5.EVENT_ID
																			   AND eh5.ACTN_ID = 17)) -- No orders with other events which are still pending
					ORDER BY od.ORDR_ID ASC


				IF (select COUNT(1) from @OrderTable)>0
				BEGIN
					EXEC [arch].[spEnableDisableFK] 2   --0 before
					Set @FKMood=1
				END
				 
				while ((SELECT COUNT(1) FROM @OrderTable WHERE flag=0) > 0)
				begin
					   select top 1 @orderid = orderid
					   from @OrderTable
					   where flag=0
					   order by id
				 
					   while (((@eventids = '') AND (@orderids = '')) OR (((select count(1) from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
									 < (select count(distinct eventid) from @OrderTable where orderid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))))
									 OR ((select count(1) from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
									 < (select count(distinct orderid) from @OrderTable where eventid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))))))
					   begin
							  --print 'in '+ @eventids
							  --print 'in '+ @orderids
							  if ((@eventids = '') AND (@orderids = ''))
							  begin
									 --print 'in-1'
									 select @eventids = CASE @eventids WHEN '' THEN convert(varchar,eventid) ELSE COALESCE(@eventids + ', ' + convert(varchar,eventid), convert(varchar,eventid)) END
									 from @OrderTable
									 where orderid = @orderid
				 
									 set @orderids = convert(varchar,@orderid)
							  end
							  else if ((select count(1) from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
									 < (select count(distinct eventid) from @OrderTable where orderid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))))
							  begin
							  --print 'in-2'
									 select @eventids = COALESCE(@eventids + ', ' + convert(varchar,eventid), convert(varchar,eventid))
									 from @OrderTable
									 where orderid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
									 and eventid not in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
							  end
							  else if ((select count(1) from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
									 < (select count(distinct orderid) from @OrderTable where eventid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))))
							  begin
							  --print 'in-3'
									 select @orderids = COALESCE(@orderids + ', ' + convert(varchar,orderid), convert(varchar,orderid))
									 from @OrderTable
									 where eventid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
									 and orderid not in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
							  end
					   end
				 
					   --print '----------'
					   --print 'orderids - ' + @orderids
					   --print 'eventids - ' + @eventids
					   /*
					   Call SP here
					   select * FROM web.ParseCommaSeparatedIntegers('62421, 64264, 68102, 69544',',') 
					   SP will use this to bring back the comma seperated value to Table form
					   */
					    RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
						EXEC @TOk=[tmp].[CheckDeleteTime]     /*  Check Allowed Delete Time From this table Copy setup from CAPTAIN*/
						--SET @TOk=1   /* Temporary Code Remove when Time setup done For COWS */
						IF(@TOk>0)
						BEGIN
						   SET @CurID=@CurID+1
						   --RAISERROR ('TO BE DELETED ORDR_ID & EVENT_ID: ', 0, 1) WITH NOWAIT
						   RAISERROR ('orderids -  %s.  Will be ARCHIVED Now', 0, 1,@orderids) WITH NOWAIT 
						   RAISERROR ('eventids -  %s.  Will be ARCHIVED Now', 0, 1,@eventids) WITH NOWAIT 
						   RAISERROR ('Calling [arch].[spCOWS_Archive]  %d.  times', 0, 1,@CurID) WITH NOWAIT 
						   EXEC  [arch].[spCOWS_Archive] @orderids,@eventids, 5,@Date  /* @Mood=5=MDS=ORDR & EVENTS*/
						   INSERT INTO [arch].[DeletedOrderEvent] (ORDR_ID,EVENT_ID,Processed,ProcessedTIME,Mood)
					       SELECT @orderids ,@eventids,'Y',GETDATE(),5
						   --RAISERROR ('DELETED ORDR_ID & EVENT_ID: ', 0, 1) WITH NOWAIT
						   RAISERROR ('orderids -  %s. Has Been ARCHIVED', 0, 1,@orderids) WITH NOWAIT 
						   RAISERROR ('eventids -  %s. Has Been ARCHIVED', 0, 1,@eventids) WITH NOWAIT 
						   --print '----------'
					   END
					   ELSE
					   BEGIN
						   RAISERROR ('TIME OVER CANT DO ARCHIVE', 0, 1) WITH NOWAIT
						   BREAK   /*While Loop*/
					   END
					   update @OrderTable set flag=1 where orderid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@orderids, ','))
					   update @OrderTable set flag=1 where eventid in (select IntegerID from [web].[ParseCommaSeparatedIntegers] (@eventids, ','))
					   set @orderids = ''
					   set @eventids = ''
					   set @orderid = -1

				END
				RAISERROR ( '*********************************************************************************', 0, 1) WITH NOWAIT
				SET @CurID=0
				EXEC @TOk=[tmp].[CheckDeleteTime]     /*  Check Allowed Delete Time From this table Copy setup from CAPTAIN*/
				--SET @TOk=1   /* Temporary Code Remove when Time setup done For COWS */
				IF(@TOk>0)
				BEGIN
					------------------------------------------------------------------------------------------
					------------------------------------------------------------------------------------------
					INSERT INTO @Order     (ORDR_ID,Mood)
					-- All orders which are not related to Events   /*-------------------2-------------------*/
					SELECT DISTINCT od.ORDR_ID,0   /* @Mood=0=ORDR ONLY no EVENTS*/
					FROM dbo.[ORDR] od WITH (NOLOCK) INNER JOIN
					dbo.ACT_TASK at WITH (NOLOCK) ON od.ORDR_ID = at.ORDR_ID
					WHERE at.TASK_ID = 1001
					AND at.CREAT_DT <= CONVERT(datetime,@Date)
					AND od.ORDR_ID NOT IN (SELECT orderid FROM @OrderTable)
					AND NOT EXISTS (SELECT 'x'
											   FROM dbo.FSA_MDS_EVENT_ORDR fmeo WITH (NOLOCK)
											   WHERE fmeo.ORDR_ID = od.ORDR_ID)
					------------------------------------------------------------------------------------------
					------------------------------------------------------------------------------------------
					INSERT INTO @Event     (EVENT_ID,Mood)
					-- All MDS Events which are not related to Orders/*-------------------3-------------------*/
					SELECT DISTINCT ev.EVENT_ID, 6   /* @Mood=6=MDS= NO ORDR Involve*/
					FROM dbo.[EVENT] ev WITH (NOLOCK) INNER JOIN
					dbo.EVENT_HIST eh WITH (NOLOCK) ON eh.EVENT_ID = ev.EVENT_ID
					WHERE ev.EVENT_TYPE_ID = 5
					AND eh.ACTN_ID=17
					AND eh.CREAT_DT <= CONVERT(datetime,@Date)
					AND NOT EXISTS (SELECT 'x'
											   FROM dbo.FSA_MDS_EVENT_ORDR fmeo WITH (NOLOCK)
											   WHERE fmeo.EVENT_ID = ev.EVENT_ID)
					UNION
					-- All Non-MDS Events							/*-------------------4-------------------*/
					SELECT DISTINCT ev.EVENT_ID, ev.EVENT_TYPE_ID
					FROM dbo.[EVENT] ev WITH (NOLOCK) INNER JOIN
					dbo.EVENT_HIST eh WITH (NOLOCK) ON eh.EVENT_ID = ev.EVENT_ID
					WHERE ev.EVENT_TYPE_ID IN (1,2,3,4)  /* Mood=@Event_type_ID = 1=AD, @Event_type_ID = 2=NGVN 	@Event_type_ID = 3=MPLS, @Event_type_ID = 4= SPLK */
					AND eh.ACTN_ID=17
					AND eh.CREAT_DT <= CONVERT(datetime,@Date)
					------------------------------------------------------------------------------------------
					------------------------------------------------------------------------------------------
					RAISERROR ('OK time............', 0, 1) WITH NOWAIT
					RAISERROR ('Checking @Order Table Total Number of ORDR to be deleted............', 0, 1) WITH NOWAIT
					SELECT @MaxID=COUNT(DISTINCT ORDR_ID) FROM @Order 
					PRINT  @MaxID
					RAISERROR ('Checking @Event Table Total Number of EVENT to be deleted............', 0, 1) WITH NOWAIT
					SELECT @MaxID1=COUNT(DISTINCT EVENT_ID) FROM @Event
					PRINT  @MaxID1 
					IF    (@MaxID > 0 or @MaxID1 > 0 )
					BEGIN
						SET @LOOP=@MaxID
						SET @LOOP1=@MaxID1
						RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
						EXEC @TOk=[tmp].[CheckDeleteTime]     /*  Check Allowed Delete Time From this table Copy setup from CAPTAIN*/
						--SET @TOk=1   /* Temporary Code Remove when Time setup done For COWS */
						IF(@TOk>0)
						BEGIN 
							if(@FKMood=0)
							BEGIN
								EXEC [arch].[spEnableDisableFK] 2  -- before 0
								SET @FKMood=1
							END
							RAISERROR ('START ARCHIVING ORDER DATA----------------: ', 0, 1) WITH NOWAIT
							set @CurID = 0
							RAISERROR ('Procedure [arch].[spCOWS_Archive]  will be called %d. times: ', 0, 1,@LOOP) WITH NOWAIT
							--PRINT @LOOP
							While (Select Count(*) From @Order Where Processed = 0) > 0
							BEGIN
								Select Top 1 @CurID = Id, @RefNumber=ORDR_ID, @Mood=Mood From @Order Where Processed = 0
								RAISERROR ('ORDR_ID: %d. Will be ARCHIVED NOW', 0, 1,@RefNumber) WITH NOWAIT
								--PRINT @RefNumber
								RAISERROR ('Calling [arch].[spCOWS_Archive]  %d times', 0, 1,@CurID) WITH NOWAIT 
								EXEC  [arch].[spCOWS_Archive] @RefNumber,'', @Mood,@Date
								RAISERROR ('ORDR_ID: %d. has been ARCHIVED', 0, 1,@RefNumber) WITH NOWAIT
								-------------------------------------------------------------------------
								INSERT INTO [arch].[DeletedOrderEvent] (ORDR_ID,EVENT_ID,Processed,ProcessedTIME,Mood)
								SELECT  @RefNumber,'','Y',GETDATE(),@Mood --FROM @Order
								-------------------------------------------------------------------------
								--PRINT @RefNumber 
								/* Reset Alternative Cursor */
								Update @Order Set Processed = 1 Where Id = @CurID
								 
							END
							RAISERROR ('FINISHED ARCHIVING ORDER DATA----------------: ', 0, 1) WITH NOWAIT
							RAISERROR ('START ARCHIVING EVENT DATA----------------: ', 0, 1) WITH NOWAIT
							set @CurID = 0
							RAISERROR ('Procedure [arch].[spCOWS_Archive]  will be called %d. times: ', 0, 1,@LOOP1) WITH NOWAIT
							--PRINT @LOOP1
							While (Select Count(*) From @Event Where Processed = 0) > 0
							BEGIN
								Select Top 1 @CurID = Id, @RefNumber=EVENT_ID, @Mood=Mood From @Event Where Processed = 0
								RAISERROR ('EVENT_ID: %d.  Will be ARCHIVED NOW', 0, 1,@RefNumber) WITH NOWAIT
								--PRINT @RefNumber 
								RAISERROR ('Calling [arch].[spCOWS_Archive]  %d times', 0, 1,@CurID) WITH NOWAIT 
								EXEC  [arch].[spCOWS_Archive] '',@RefNumber, @Mood,@Date
								RAISERROR ('EVENT_ID: %d. has been ARCHIVED', 0, 1,@RefNumber) WITH NOWAIT
								-------------------------------------------------------------------------
								INSERT INTO [arch].[DeletedOrderEvent] (ORDR_ID,EVENT_ID,Processed,ProcessedTIME,Mood)
								SELECT  '',@RefNumber,'Y',GETDATE(),@Mood --FROM @Event
								-------------------------------------------------------------------------
								--PRINT @RefNumber 
								/* Reset Alternative Cursor */
								Update @Event Set Processed = 1 Where Id = @CurID
								 
							END
							RAISERROR ('FINISHED ARCHIVING EVENT DATA----------------: ', 0, 1) WITH NOWAIT
						END
						ELSE
						BEGIN
							RAISERROR ('TIME OVER', 0, 1) WITH NOWAIT
						END
								
					END
					ELSE
					BEGIN
						RAISERROR ('NO Event/Order TO DELETE', 0, 1) WITH NOWAIT
					END
					RAISERROR ('FINISHED ARCHIVING PROCESS----------------: ', 0, 1) WITH NOWAIT
				END
				ELSE  RAISERROR ('TIME OVER CANT BE DELETED', 0, 1) WITH NOWAIT
				if(@FKMood=1)
				BEGIN
					EXEC [arch].[spEnableDisableFK] 3 --Before 1
					SET @FKMood=0
				END
				RAISERROR ('Processed Events/Orders', 0, 1) WITH NOWAIT
				--Select * from [arch].[DeletedOrderEvent] WHERE cast(ProcessedTIME as date) =cast(GETDATE() as date)
				Select * from [arch].[DeletedOrderEvent] WHERE cast(ProcessedTIME as date) =cast(GETDATE() as date) and Action IS NULL ORDER BY ProcessedTIME DESC
		END TRY
		BEGIN CATCH
			IF (XACT_STATE()) <> 0 
			BEGIN
				ROLLBACK TRANSACTION;
				RAISERROR ('Error: Transaction RollBack No Operation Perform', 0, 1) WITH NOWAIT
				RAISERROR ('Check: Select * FROM dbo.SQL_ERROR WITH (NOLOCK)  order by SQL_Error_ID DESC', 0, 1) WITH NOWAIT
			END
			SET DEADLOCK_PRIORITY -5; 
			EXEC [dbo].[insertErrorInfo]
		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;

END



GO



