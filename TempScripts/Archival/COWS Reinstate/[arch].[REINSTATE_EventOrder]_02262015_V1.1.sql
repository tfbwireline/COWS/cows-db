USE [COWS]
GO

/****** Object:  StoredProcedure [tmp].[DELETEBatchEvents]    Script Date: 12/09/2014 13:51:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---- =============================================
---- Author:		Md. Monir
---- Create date:   12/09/2014
---- Description:	This SP is used to Single DELETE the Events by Event_ID ..
---- Corrected date:   02/07/2015
---- Description:	Modified to adjust to EventType and Ordr
---- Corrected date:   02/16/2015
---- Description:	Modified to use raise Error and save Order/Events that are being archived.
---- Corrected date:   03/06/2015
---- Description:	removed   SET @TOk=1
---- =============================================

ALTER PROCEDURE [arch].[REINSTATE_EventOrder](
	@ID   INT=NULL,
	@TYPE INT=NULL /* ORDER =0 EVENT=1 FTN=3 */
)
AS
/* 
[arch].[spCOWS_Archive]  Will be called by passing either ODER_ID or EVENT_ID and will delete 1 by 1
Calling  PROCEDURE [arch].[spCOWS_Archive]  @Mood Must Reflect this:
------------------------------------------------------------------------------------------------
@Mood=???
		@Mood=0=ORDR Only No Eevents Table  , @Event_type_ID = 1=AD, @Event_type_ID = 2=NGVN 
		@Event_type_ID = 3=MPLS, @Event_type_ID = 4= SPLK 
		@Event_type_ID = 5=MDS tables  and both ORDR Table As well, 
		@Mood = 6 = MDS EVENTS only Tables  No Order no other events also. 
------------------------------------------------------------------------------------------------
*/
BEGIN
		SET NOCOUNT ON;
		SET	DEADLOCK_PRIORITY	10;
		BEGIN TRANSACTION;
			BEGIN TRY
				
				
				DECLARE		@TOk int
				
				------------------------------------------------------------------------------------------
				------------------------------------------------------------------------------------------
				DECLARE @Mood INT, @ORDR_ID INT=NULL, @FTN INT=NULL, @EVENT_ID INT=NULL,  @FKMood int, @Date			  DateTime
				SET @FKMood   = 0
				SET @Date=getdate()
				IF(@ID IS NOT NULL AND  @TYPE=2)  /* FTN  @Mood=0 */
				BEGIN
					SELECT DISTINCT @ORDR_ID=ORDR_ID, @Mood=0 FROM [arch].[FSA_ORDR] WITH (NOLOCK) WHERE FTN =@ID
				END
				IF(@ID IS NOT NULL AND @TYPE=1) /*  EVENT */
				BEGIN
					SELECT DISTINCT @EVENT_ID=ev.EVENT_ID, @Mood=ev.EVENT_TYPE_ID FROM [arch].[EVENT] ev WITH (NOLOCK) WHERE  ev.EVENT_ID=@ID
				END
				IF(@ID IS NOT NULL AND @TYPE=0) /*  ORDER */
				BEGIN
					SET @ORDR_ID=@ID
					SET @Mood=0
				END
				------------------------------------------------------------------------------------------
				------------------------------------------------------------------------------------------
				IF    (@ORDR_ID IS NOT NULL )
				BEGIN
					RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
					EXEC @TOk=[tmp].[CheckDeleteTime]     /*  Check Allowed Delete Time From this table Copy setup from CAPTAIN*/
					--SET @TOk=1   /* Temporary Code Remove when Time setup done For COWS */
					IF(@TOk>0)
					BEGIN 
						if(@FKMood=0)
						BEGIN
							EXEC [arch].[spEnableDisableFK] 2
							SET @FKMood=1
						END
						RAISERROR ('START ARCHIVING ORDER DATA----------------: ', 0, 1) WITH NOWAIT
						RAISERROR ('ORDR_ID: %d. Will be REINSTATE NOW', 0, 1,@ORDR_ID) WITH NOWAIT
						EXEC  [arch].[spCOWS_REINSTATE] @ORDR_ID,'', @Mood,@Date
						RAISERROR ('ORDR_ID: %d. has been REINSTATED', 0, 1,@ORDR_ID) WITH NOWAIT
						-------------------------------------------------------------------------
						INSERT INTO [arch].[DeletedOrderEvent] (ORDR_ID,EVENT_ID,Processed,ProcessedTIME,Mood,action)
						SELECT  @ORDR_ID,'','Y',GETDATE(),@Mood,'Reinstate'  --FROM @Order
						-------------------------------------------------------------------------
					END
					ELSE
					BEGIN
						RAISERROR ('TIME OVER', 0, 1) WITH NOWAIT
					END
				END	
				IF    (@EVENT_ID IS NOT NULL )
				BEGIN
					RAISERROR ('*********************************************************************************', 0, 1) WITH NOWAIT
					EXEC @TOk=[tmp].[CheckDeleteTime]     /*  Check Allowed Delete Time From this table Copy setup from CAPTAIN*/
					--SET @TOk=1   /* Temporary Code Remove when Time setup done For COWS */
					IF(@TOk>0)
					BEGIN 
						if(@FKMood=0)
						BEGIN
							EXEC [arch].[spEnableDisableFK] 2
							SET @FKMood=1
						END
						RAISERROR ('START REINSTATE EVENT DATA----------------: ', 0, 1) WITH NOWAIT
						RAISERROR ('EVENT_ID: %d. Will be REINSTATE NOW', 0, 1,@EVENT_ID) WITH NOWAIT
						EXEC  [arch].[spCOWS_REINSTATE] '',@EVENT_ID, @Mood,@Date
						RAISERROR ('EVENT_ID: %d. has been REINSTATED', 0, 1,@EVENT_ID) WITH NOWAIT
						-------------------------------------------------------------------------
						INSERT INTO [arch].[DeletedOrderEvent] (ORDR_ID,EVENT_ID,Processed,ProcessedTIME,Mood,action)
						SELECT  '',@EVENT_ID,'Y',GETDATE(),@Mood,'Reinstate' --1/2/3/4/5
						-------------------------------------------------------------------------
					END
					ELSE
					BEGIN
						RAISERROR ('TIME OVER', 0, 1) WITH NOWAIT
					END
				END		 
				ELSE
				BEGIN
					RAISERROR ('NO Event/Order TO REINSTATE', 0, 1) WITH NOWAIT
				END
				RAISERROR ('FINISHED REINSTATE PROCESS----------------: ', 0, 1) WITH NOWAIT
				if(@FKMood=1)
				BEGIN
					EXEC [arch].[spEnableDisableFK] 3
					SET @FKMood=0
				END
				RAISERROR ('Processed Events/Orders', 0, 1) WITH NOWAIT
				Select * from [arch].[DeletedOrderEvent] WHERE cast(ProcessedTIME as date) =cast(GETDATE() as date) and Action='Reinstate' ORDER BY ProcessedTIME DESC

		END TRY
		BEGIN CATCH
			IF (XACT_STATE()) <> 0 
			BEGIN
				ROLLBACK TRANSACTION;
				RAISERROR ('Error: Transaction RollBack No Operation Perform', 0, 1) WITH NOWAIT
				RAISERROR ('Check: Select * FROM dbo.SQL_ERROR WITH (NOLOCK)  order by SQL_Error_ID DESC', 0, 1) WITH NOWAIT
			END
			SET DEADLOCK_PRIORITY -5; 
			EXEC [dbo].[insertErrorInfo]
		END CATCH;

		IF @@TRANCOUNT > 0
			COMMIT TRANSACTION;

END



GO



