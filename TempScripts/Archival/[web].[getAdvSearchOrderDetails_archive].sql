USE [COWS]
GO

/****** Object:  StoredProcedure [web].[getAdvSearchOrderDetails_archive]    Script Date: 03/05/2015 12:37:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	08/10/2011 
-- Description:	Advanced Search
 -- ================================================================================================
 
 -- ================================================================================================
-- Modified By:	Naidu Vattigunta	
-- Modified Date:	09/27/2011
-- Description:	Advanced Search for IPL
 -- ================================================================================================
 
-- ================================================================================================
-- Modified By:	Md MOnir	
-- Modified Date:	01/30/2015
-- Description:	Added Archive Modification
-- ================================================================================================

 --Kyle: Backed out changes to search due to slowness, will revisit later.
-- EXEC [web].[getAdvSearchOrderDetails] '11334675','','','','','','','','','','0','0','0','','','0','Orange'
-- EXEC [web].[getAdvSearchOrderDetails_archive] '11334675','','','','','','','','','','0','0','0','','','0','Orange',1
CREATE PROCEDURE [web].[getAdvSearchOrderDetails_archive] 
  @FTNs Varchar(1000) = '',
  @Names Varchar(1000) = '',
  @H5names Varchar(1000) = '',
  @OrderTypes Varchar(50) = '',
  @OrderSubType Varchar(50) = '',
  @CCDs DateTime = NULL,
  @H5_H6 Varchar(20) = '',
  @H1 Varchar(20)= '',
  @PRSQuote Varchar(20) ='',
  @SOI Varchar(20)='',
  @WorkGroup Varchar(2) = '0',
  @ProductType Varchar(5) = '0',
  @PlatformType Varchar(5) ='0',
  @PrivateLine Varchar(100) = '',
  @VendorCircuit Varchar(100) ='',
  @Region	 Varchar(100) = '0',
  @VendorName Varchar(100) ='',
  @Status Varchar(1)		= '',
  @NUA    VARCHAR(100)      = '',
  @ParentFTN VARCHAR(50)	= '',
  @RelFTN VARCHAR(10)	= '',
  @PRCH_ORDR_NBR VARCHAR(15) = '',
  @ATLAS_WRK_ORDR_NBR VARCHAR(15) = '',
  @PLSFT_RQSTN_NBR	VARCHAR(15) = '',
  @CCDOps	Varchar(2)		= '0',
  @CCDOption Varchar(2)		=	'0',
  @REQ_BY_USR_ID	 INT			=	1,
  @RptSchedule	CHAR(1)			=	'D',
  @AdhocEmailChk  VARCHAR(10)		=	'FALSE',
  @Mode int			=0   /* 0=No Archive   1=With Archive */
AS 
BEGIN	

--SET NOCOUNT ON;
DECLARE @Sort				nVarchar(max)  
DECLARE @OrderTypeFilter	Int = 0	 
DECLARE @CCD				Varchar(10)
DECLARE @sql_stmt			nVarchar(max)      
DECLARE @sql_Insert			nVARCHAR(max)
DECLARE @sql_filter			nVARCHAR(max)
DECLARE @sql_union			nVARCHAR(max)
DECLARE @finalsql			nVARCHAR(max)
DECLARE @finalsql1			nVARCHAR(max)
DECLARE @finalsql2			nVARCHAR(max)
DECLARE @finalsql3			nVARCHAR(max)
DECLARE @SQLStr1			nVARCHAR(max)
DECLARE @SQLStr2			nVARCHAR(max)
DECLARE @SQLStr3			nVARCHAR(max)
DECLARE @tmp				nVarchar(max)   
DECLARE @SQLStr				nVARCHAR(max)
DECLARE @tempInsert			nVARCHAR(max)
DECLARE @SQLTemp			nVARCHAR(max)
DECLARE @GCSQuoteID			Varchar(100)
DECLARE @VendorQuoteID		Varchar(100)
DECLARE @SCANumber			Varchar(100)
DECLARE @CustomerName		Varchar(100) 


BEGIN TRY



	SET @SQLStr			=	''  
	SET @SQLStr1		=	''   
	SET @SQLStr2		=	''
	SET @SQLStr3		=	''        
	SET @finalsql		=	''
	SET @sql_filter		=	''
 	SET @sql_Insert		=	''
	SET @sql_stmt		=	''
	SET @sql_union		=	''
	SET @tempInsert		=	''
	SET @SQLTemp		=	''
	SET	@CCD			=	''
 
 
 
---- TRIM ALL THE PARAM's
SET @FTNs = LTRIM(RTRIM(@FTNs))
SET @OrderTypes = LTRIM(RTRIM(@OrderTypes))
SET @OrderSubType = LTRIM(RTRIM(@OrderSubType))
SET @H1 = LTRIM(RTRIM(@H1))
SET @H5_H6 = LTRIM(RTRIM(@H5_H6))
SET @PRSQuote = LTRIM(RTRIM(@PRSQuote))
SET @SOI	  = LTRIM(RTRIM(@SOI))
SET @Names  = LTRIM(RTRIM(@Names))
SET @H5names  = LTRIM(RTRIM(@H5names))

 SET @ProductType = LTRIM(RTRIM(@ProductType))
 SET @PlatformType = LTRIM(RTRIM(@PlatformType))
 SET @PrivateLine = LTRIM(RTRIM(@PrivateLine))
 SET @VendorCircuit = LTRIM(RTRIM(@VendorCircuit))
 SET @Region		= LTRIM(RTRIM(@Region))
 SET @CustomerName		= LTRIM(RTRIM(@CustomerName))
 SET @NUA		= LTRIM(RTRIM(@NUA))

----------------------------------------------------------------------------------------------------

OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;	

 	 IF (ISNULL(@Names, '') <> '')
		BEGIN
			SET @Names = REPLACE(@Names, '*', '%') 
			SET @Names = '%' + @Names + '%'
		END
	 
	 IF (ISNULL(@H5names, '') <> '')
		BEGIN
			SET @H5names = REPLACE(@H5names, '*', '%') 
			SET @H5names = '%' + @H5names + '%'
		END
	IF OBJECT_ID(N'tempdb..#CCDOrders', N'U') IS NOT NULL         
				DROP TABLE #CCDOrders   
			    CREATE TABLE #CCDOrders         
				(RowID INT Identity(1,1), OrderID	Varchar(100)) 
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
DECLARE @e_sqlEvent NVARCHAR(MAX)
DECLARE @e_where	NVARCHAR(max)	 = ''
DECLARE @e_finalSQL NVARCHAR(MAX)
----------------------------------------------------------------------------------------------------
IF(@Mode=0)
BEGIN
		IF @CCDs IS NOT NULL OR @CCDOption != '0'
			BEGIN
				--DECLARE @e_sqlEvent NVARCHAR(MAX)
				--DECLARE @e_where	NVARCHAR(max)	 = ''
				--DECLARE @e_finalSQL NVARCHAR(MAX)
				SET @e_sqlEvent = 'INSERT INTO #CCDOrders (OrderID)        
										SELECT DISTINCT odr.ORDR_ID
											FROM  dbo.ORDR odr WITH(NOLOCK) 
										Left Join dbo.FSA_ORDR fsa WITH (NOLOCK) ON fsa.ORDR_ID = odr.ORDR_ID
										Left Join dbo.IPL_ORDR ipl WITH (NOLOCK) ON ipl.ORDR_ID = odr.ORDR_ID
										Left Join dbo.NCCO_ORDR ncco WITH (NOLOCK) ON ncco.ORDR_ID = odr.ORDR_ID
											WHERE 1 = 1 '

  				IF @CCDs IS NOT NULL
  					BEGIN
						IF @CCDOps != '0'
							BEGIN
								IF @CCDOps = '1'
									BEGIN
										SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= ''' + CONVERT(varchar,@CCDs) + '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <=''' + CONVERT(Varchar,DATEADD(day,180,@CCDs)) + ''''  
									END
								ELSE IF @CCDOps = '2'
									BEGIN
										SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= ''' + CONVERT(varchar,DATEADD(day,-180,@CCDs)) + '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <=''' + CONVERT(Varchar,@CCDs) + ''''  
									END
								ELSE IF @CCDOps = '3'
									BEGIN
										SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) = ''' + CONVERT(Varchar,@CCDs) + ''''  
									END
							END
						ELSE
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) = ''' + CONVERT(Varchar,@CCDs) + ''''  
							END

						--SET @CCD =  CONVERT(Varchar(10), @CCDs, 101)
					END
				ELSE IF @CCDOption != '0'
					BEGIN
						IF @CCDOption = '1'
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= '''  + CONVERT(varchar,(GETDATE() - 30)) +  '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 
							END
						ELSE IF @CCDOption = '2'
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= '''  + CONVERT(varchar,(GETDATE() - 60)) +  '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 
							END
						ELSE IF @CCDOption = '3'
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= '''  + CONVERT(varchar,(GETDATE() - 90)) +  '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 
							END
						ELSE IF @CCDOption = '4'
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= '''  + CONVERT(varchar,(GETDATE() - 180)) +  '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 
							END
					END
		
				SET @e_finalSQL = @e_sqlEvent + @e_where
				IF @e_finalSQL != ''
					EXEC sp_executesql @e_finalSQL
			END

		
			
		SET @FTNs = REPLACE(@FTNs, '*', '%')
		SET @FTNs = REPLACE(@FTNs, '\r\n', '') 
		
		IF OBJECT_ID(N'tempdb..#FTNs', N'U') IS NOT NULL         
					DROP TABLE #FTNs   
					CREATE TABLE #FTNs         
					(RowID INT Identity(1,1), FTN	Varchar(10))

		IF (@FTNs <> '')
			BEGIN			 

				IF CHARINDEX('%',@FTNs) > 0 
					BEGIN 
						INSERT INTO #FTNs (FTN)        
								SELECT DISTINCT TOP 100 FTN AS FTN 
								FROM dbo.FSA_ORDR WITH(NOLOCK) 
								WHERE FTN LIKE(REPLACE(@FTNs, ',', '')) 
					END           
				ELSE           
					BEGIN          
					INSERT INTO #FTNs (FTN)       
								SELECT DISTINCT SubString(stringid, 1, 10) AS FTN 
								FROM web.ParseCommaSeparatedStrings(@FTNs)        
					END 
			END

				---------------------------------------------------------------
				--	RELATED FTN Filter 
				---------------------------------------------------------------
				IF ((@RelFTN <> '') AND (@RelFTN = 'TRUE'))
				BEGIN
					SET @FTNs = REPLACE(@FTNs, '*', '%')
					SET @FTNs = REPLACE(@FTNs, '\r\n', ',')
					--IF CHARINDEX('%',@FTNs) <= 0
					--	SET @FTNs = '%'+@FTNs+'%'

					INSERT INTO #FTNs (FTN)
					SELECT  CAST(FO2.FTN AS VARCHAR(10))
					FROM dbo.FSA_ORDR FO1 WITH (NOLOCK) INNER JOIN
					dbo.ORDR OD WITH (NOLOCK) ON OD.PRNT_ORDR_ID = FO1.ORDR_ID INNER JOIN
					dbo.FSA_ORDR FO2 WITH (NOLOCK) ON OD.ORDR_ID = FO2.ORDR_ID
					WHERE FO1.FTN LIKE  @FTNs
				END
	             
				---------------------------------------------------------------
				--	PARENT FTN Filter 
				---------------------------------------------------------------
				IF (@ParentFTN <> '')
				BEGIN
					SET @ParentFTN = REPLACE(@ParentFTN, '*', '%')
					SET @ParentFTN = REPLACE(@ParentFTN, '\r\n', ',')
					--IF CHARINDEX('%',@ParentFTN) <= 0
					--	SET @ParentFTN = '%'+@ParentFTN+'%'

					INSERT INTO #FTNs (FTN)
					SELECT CAST(FTN AS VARCHAR(10))
					FROM dbo.FSA_ORDR WITH (NOLOCK)
					WHERE PRNT_FTN LIKE  @ParentFTN
					UNION ALL
					SELECT REPLACE(@ParentFTN,'%','')
				END  

		IF(@Names <> '')
			BEGIN
				IF OBJECT_ID(N'tempdb..#Names', N'U') IS NOT NULL
					DROP TABLE #Names
					CREATE TABLE #Names
						(RowID INT Identity(1,1), Name Varchar(1000))
												IF CHARINDEX ('%', @Names) > 0
							
								BEGIN
								  INSERT INTO #Names (Name)
							  		SELECT DISTINCT TOP 100 dbo.decryptBinaryData(CUST_NME) AS Name 
									FROM	dbo.FSA_ORDR_CUST WITH(NOLOCK) 
									WHERE	dbo.decryptBinaryData(CUST_NME) LIKE(REPLACE(@Names, ',', '')) 
									AND		CIS_LVL_TYPE = 'H1'
								END
							ELSE
								BEGIN
								   INSERT INTO #Names(Name)
								   SELECT DISTINCT TOP 100 dbo.decryptBinaryData(CUST_NME) AS Name 
									FROM	dbo.FSA_ORDR_CUST WITH(NOLOCK) 
									WHERE	dbo.decryptBinaryData(CUST_NME) IN (@Names)
									AND		CIS_LVL_TYPE = 'H1'
								END
			END

		IF(@H5names <> '')
			BEGIN
				IF OBJECT_ID(N'tempdb..#H5names', N'U') IS NOT NULL
					DROP TABLE #H5names
					CREATE TABLE #H5names
						(RowID INT Identity(1,1), Name Varchar(1000))
												IF CHARINDEX ('%', @H5names) > 0
							
								BEGIN
								  INSERT INTO #H5names (Name)
							  		SELECT DISTINCT TOP 100 dbo.decryptBinaryData(CUST_NME) AS Name 
									FROM	dbo.FSA_ORDR_CUST WITH(NOLOCK) 
									WHERE	dbo.decryptBinaryData(CUST_NME) LIKE(REPLACE(@H5names, ',', '')) 
									AND		(CIS_LVL_TYPE = 'H5' or CIS_LVL_TYPE = 'H6'or CIS_LVL_TYPE = 'ES')
								END
							ELSE
								BEGIN
								   INSERT INTO #H5names(Name)
								   SELECT DISTINCT TOP 100 dbo.decryptBinaryData(CUST_NME) AS Name 
									FROM	dbo.FSA_ORDR_CUST WITH(NOLOCK) 
									WHERE	dbo.decryptBinaryData(CUST_NME) IN (@H5names)
									AND		(CIS_LVL_TYPE = 'H5' or CIS_LVL_TYPE = 'H6'or CIS_LVL_TYPE = 'ES')
								END
			END
		
				
		IF(@OrderSubType <> ''AND @OrderSubType <>'0')		
			BEGIN
				IF OBJECT_ID(N'tempdb..#OrderSubType', N'U') IS NOT NULL
					DROP TABLE #OrderSubType
					CREATE TABLE #OrderSubType
						(RowID INT Identity(1,1), OrderSubType Varchar(50))
					 
					 			BEGIN
								   INSERT INTO #OrderSubType(OrderSubType)
							   		SELECT DISTINCT SubString(stringid, 1, 10) AS Name 
									FROM web.ParseCommaSeparatedStrings(@OrderSubType) 
								END
			
			END
			
		IF(@OrderTypes <> '' AND @OrderTypes <>'0' )		
			BEGIN
				IF OBJECT_ID(N'tempdb..#OrderTypes', N'U') IS NOT NULL
					DROP TABLE #OrderTypes
				CREATE TABLE #OrderTypes
					(RowID INT Identity(1,1), OrderTyp tinyint)
						
				IF OBJECT_ID(N'tempdb..#iplOrderTypes', N'U') IS NOT NULL
					DROP TABLE #iplOrderTypes
				CREATE TABLE #iplOrderTypes
					(RowID INT Identity(1,1), iplOrderTyp tinyint)
					
				IF	ISNUMERIC(@OrderTypes)	=	0
					BEGIN	
						INSERT INTO #OrderTypes (OrderTyp) 
							SELECT		ORDR_TYPE_ID AS OrderType 
								FROM	dbo.LK_ORDR_TYPE WITH (NOLOCK)
								WHERE	ORDR_TYPE_DES	LIKE '%' + @OrderTypes + '%'
								
						INSERT INTO #iplOrderTypes (iplOrderTyp) 
							SELECT		ORDR_TYPE_ID AS iplOrderTyp 
								FROM	dbo.LK_ORDR_TYPE WITH (NOLOCK) 
								WHERE	ORDR_TYPE_DES	LIKE	'%' + @OrderTypes + '%'			
					END
				ELSE
					BEGIN
						INSERT INTO #OrderTypes (OrderTyp)		VALUES	(CONVERT(TINYINT, @OrderTypes))
						INSERT INTO #iplOrderTypes (iplOrderTyp)VALUES	(CONVERT(TINYINT, @OrderTypes))
					END
			END
		
		IF(@H5_H6 <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#H5_H6', N'U') IS NOT NULL
					DROP TABLE #H5_H6
					CREATE TABLE #H5_H6
						(RowID INT Identity(1,1), H5_H6 Varchar(20))
					 
					 			BEGIN
								   INSERT INTO #H5_H6 (H5_H6)
								   VALUES(@H5_H6)
							 			 
								END	
			END

		IF(@H1 <> '' )	
			BEGIN
				IF OBJECT_ID(N'tempdb..#TmpH1', N'U') IS NOT NULL
					DROP TABLE #TmpH1
					CREATE TABLE #TmpH1
						(RowID INT Identity(1,1), H1 Varchar(20))
					 
					 			BEGIN
								   INSERT INTO #TmpH1 (H1)
									VALUES(@H1)
							 			 
								END	
			END
			
		IF(@PRSQuote <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#PRS', N'U') IS NOT NULL
					DROP TABLE #PRS
					CREATE TABLE #PRS
						(RowID INT Identity(1,1), PRS Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #PRS (PRS)
									VALUES(@PRSQuote)
							 			 
								END	
			END
		 
		IF(@SOI <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#SOI', N'U') IS NOT NULL
					DROP TABLE #SOI
					CREATE TABLE #SOI
						(RowID INT Identity(1,1), SOI Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #SOI (SOI)
									VALUES(@SOI)
							 			 
								END	
			END	
		
		IF(@PrivateLine <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#PrivateLine ', N'U') IS NOT NULL
					DROP TABLE #PrivateLine
					CREATE TABLE #PrivateLine
						(RowID INT Identity(1,1), PrivateLine Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #PrivateLine (PrivateLine)
									VALUES(@PrivateLine)
							 			 
								END	
			END
			
		IF(@GCSQuoteID  <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#GCSQuoteID', N'U') IS NOT NULL
					DROP TABLE #GCSQuoteID
					CREATE TABLE #GCSQuoteID
						(RowID INT Identity(1,1), GCSQuoteID Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #GCSQuoteID (GCSQuoteID)
									VALUES(@GCSQuoteID)
							 			 
								END	
			END
		
		IF(@VendorQuoteID   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#VendorQuoteID ', N'U') IS NOT NULL
					DROP TABLE #VendorQuoteID 
					CREATE TABLE #VendorQuoteID
						(RowID INT Identity(1,1), VendorQuoteID Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #VendorQuoteID (VendorQuoteID)
									VALUES(@VendorQuoteID)
							 			 
								END	
			END
			
		IF(@VendorName   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#VendorName', N'U') IS NOT NULL
					DROP TABLE #VendorName
					CREATE TABLE #VendorName
						(RowID INT Identity(1,1), VendorName Varchar(100))
					 
					 
					 			BEGIN
					 			 SET @VendorName = '%' +@VendorName+ '%'
								   INSERT INTO #VendorName (VendorName)
								   ( SELECT CXR_NME FROM dbo.LK_FRGN_CXR WITH (NOLOCK) WHERE CXR_NME LIKE (@VendorName))
								   
								   INSERT INTO #VendorName (VendorName)
								   ( SELECT VNDR_NME FROM dbo.LK_VNDR WITH (NOLOCK) WHERE VNDR_NME LIKE (@VendorName))
							 			 
								END	
			END
			
		IF(@CustomerName   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#CustomerName', N'U') IS NOT NULL
					DROP TABLE #CustomerName
					CREATE TABLE #CustomerName
						(RowID INT Identity(1,1), CustomerName Varchar(100))
					 
					 
					 			BEGIN
					 			 SET @CustomerName = '%' +@CustomerName+ '%'
								   INSERT INTO #CustomerName (CustomerName)
								   (SELECT	dbo.decryptBinaryData(CUST_NME)
										FROM	dbo.FSA_ORDR_CUST WITH(NOLOCK) 
										WHERE	dbo.decryptBinaryData(CUST_NME) LIKE(@CustomerName) 
										AND		CIS_LVL_TYPE = 'H1')
								END	
			END

		IF(@SCANumber   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#SCANumber ', N'U') IS NOT NULL
					DROP TABLE #SCANumber 
					CREATE TABLE #SCANumber
						(RowID INT Identity(1,1), SCANumber Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #SCANumber (SCANumber)
									VALUES(@SCANumber)
							 			 
								END	
			END
			
		IF(@VendorCircuit    <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#VendorCircuit  ', N'U') IS NOT NULL
					DROP TABLE #VendorCircuit  
					CREATE TABLE #VendorCircuit
						(RowID INT Identity(1,1), VendorCircuit Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #VendorCircuit (VendorCircuit)
									VALUES(@VendorCircuit)
							 			 
								END	
			END
			
		IF(@ProductType    <> '' AND @ProductType    <> '0' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#ProductType  ', N'U') IS NOT NULL
					DROP TABLE #ProductType
					CREATE TABLE #ProductType
						(RowID INT Identity(1,1), ProductType Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #ProductType(ProductType)
									VALUES(@ProductType)
							 			 
								END	
			END
			
			IF(@NUA   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#NUA ', N'U') IS NOT NULL
					DROP TABLE #NUA 
					CREATE TABLE #NUA
						(RowID INT Identity(1,1), NUA Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #NUA (NUA)
									VALUES(@NUA)
							 			 
								END	
			END
			
 		IF OBJECT_ID(N'tempdb..#OrderTmp', N'U') IS NOT NULL
					DROP TABLE #OrderTmp
					CREATE TABLE #OrderTmp 
					(
						H1 INT,
						OrderID INT,
						CTRY_NME Varchar(50),
						PRE_XST_PL_NBR Varchar(100),
						H5 Varchar(50),
						GCS_PRICE_QOT_NBR Varchar(100),
						ORDR_ACTN_DES Varchar(50),
						SCA_NBR  Varchar(50),
						QOT_NBR Varchar(50),
						VNDR_CKT_ID Varchar(50), 
						PROD_TYPE_DES Varchar(50),
						PLTFRM_NME Varchar(50),
						ORDR_SUB_TYPE_CD Varchar(50),
						CTRY_CD Varchar(50),
						H5_H6_CUST_ID Varchar(50),
						ORDR_TYPE_DES Varchar(50),
						TSUP_PRS_QOT_NBR Varchar(50),
						CXR_NME  Varchar(50),
						TRGT_DLVRY_DT SmallDateTime,
						SOI Varchar(50)
					)					 

  		IF OBJECT_ID(N'tempdb..#OrderList', N'U') IS NOT NULL
					DROP TABLE #OrderList
					CREATE TABLE #OrderList
					(
						OrderID Int,	
						FTN Int,
						H1 Int,
						CountryName Varchar(50),
						PrivateLine Varchar(50),
			  
						CustomerName Varchar(100),
						GCSQuoteID Varchar(100),
						CCD DateTime,			 
						OrderType Varchar(50),
						SCANumber Varchar(50),
						VendorQuote Varchar(50),
						VendorCircuitID Varchar(50),
						ProductType Varchar(50),
						PlatformType Varchar(50),
						OrderSubType Varchar(50),			
						OrderTypeDesc Varchar(50),			 
						[H5/H6] Varchar(100),
						TargerDeliveryDate SmallDateTime,
						DomesticFlag Varchar(50),
						PRSQuote Varchar(50),
						SOI Varchar(50),
						VendorName Varchar(50),
						Region Varchar(100),
						WorkGroup Varchar(50),
						CurrentTask Varchar(50),
						OrderStatus Varchar(50),
						WGID INT,
						TASKID INT,
						CAT_ID INT,
						CSG_LVL_CD INT,
						SCURD_CD Bit,
						H5_FOLDR_ID INT,
						NUA  Varchar(100)	 				 		
					)
	  
		SET @SQLStr2 = ' INSERT INTO #OrderList'
		
		SET @SQLStr =	'SELECT  DISTINCT TOP 500 
							lo.ORDR_ID,
							fsa.FTN,
							foc.CUST_ID										AS H1,
							NULL											AS [Country Name],
							NULL											AS [Private Line ], 
							Case	lo.SCURD_CD
								When	1	Then	''Private Customer''
								Else	CASE lo.DMSTC_CD
											WHEN 1 THEN dbo.decryptBinaryData(foc5.CUST_NME)
											ELSE dbo.decryptBinaryData(foc6.CUST_NME)
											END
								End	AS CUST_NME,
							NULL											AS [GCS Quote ID ],
							ISNULL(lo.CUST_CMMT_DT, fsa.CUST_CMMT_DT),
							loa.ORDR_ACTN_DES,
							NULL											AS [SCA Number],
							NULL											AS [Vendor Quote ID],
							NULL											AS [Vendor Circuit ID],
							lpt.PROD_TYPE_DES								AS [Product Type],
							lp.PLTFRM_NME									AS [Platform Type],
							fsa.ORDR_SUB_TYPE_CD,
							CASE 
								WHEN lpprt.ORDR_TYPE_ID = 8 AND fsa.ORDR_TYPE_CD != ''CN'' THEN lot.ORDR_TYPE_DES + '' Cancel''
								ELSE lot.ORDR_TYPE_DES
								END AS	OrderTypeDesc,
							lo.H5_H6_CUST_ID,
							NULL											AS TRGT_DLVRY_DT,
							CASE lo.DMSTC_CD 
								WHEN 0 THEN '' Domestic''  
								WHEN 1 THEN '' International'' 
							END												AS DomesticFlag,
							ISNULL(fsa.TSUP_PRS_QOT_NBR, '''')				AS [PRS Quote Number],
							CASE
								WHEN foc5.SOI_CD <> ''''         THEN foc5.SOI_CD
								WHEN foc6.SOI_CD <> ''''         THEN foc6.SOI_CD
								ELSE NULL
							END                                         	AS SOI, 
							--web.getVendorName(lo.ORDR_ID) AS [Vendor Name],  
							CASE lo.PLTFRM_CD
									WHEN ''SF''	THEN ISNULL(sf.VNDR_NME,ISNULL(rsf.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									WHEN ''CP''	THEN ISNULL(cp.VNDR_NME,ISNULL(rcp.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									WHEN ''RS''	THEN ISNULL(rs.VNDR_NME,ISNULL(rrs.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									ELSE ''''
									END AS [Vendor Name],
							CASE 
								WHEN lo.DMSTC_CD = 1 THEN xr.RGN_DES
								WHEN lo.DMSTC_CD = 0 THEN ''''
							END												AS Region,
							CASE 
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 1 THEN ''ENCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 1 THEN ''ANCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 THEN ISNULL(lg.GRP_NME,0)
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS GRP_NME,
							ISNULL(lt.TASK_NME, '''')						AS TASK_NME,
							Case	ISNULL(lt.TASK_NME, '''') 
								When	''Bill Activated''	Then	ts.ORDR_STUS_DES
								When	''''				Then	ts.ORDR_STUS_DES
								Else	ls.STUS_DES 
							End												AS STUS_DES, 							
							CASE 
								WHEN lg.GRP_ID in (5,6,7)		AND lo.RGN_ID = 1	THEN 5
								WHEN lg.GRP_ID in (5,6,7)		AND lo.RGN_ID = 3	THEN 6
								WHEN lg.GRP_ID in (5,6,7)		AND lo.RGN_ID = 2	THEN 7
								WHEN lg.GRP_ID in (5,6,7)		AND IsNull(lo.RGN_ID,0) = 0 THEN 5
								WHEN lg.GRP_ID NOT IN (5,6,7)	AND IsNull(lg.GRP_ID,0) <> 0 THEN lg.GRP_ID
								WHEN lg.GRP_ID NOT IN (5,6,7)	AND lg.GRP_ID <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS  GRP_ID,
							lt.TASK_ID,
							lo.ORDR_CAT_ID, 
							lo.CSG_LVL_CD, 
							lo.SCURD_CD, 
							lo.H5_FOLDR_ID,
							nsi.NUA_ADR                                      AS NUA 
						FROM			dbo.ORDR			lo	WITH (NOLOCK) 
							INNER JOIN	dbo.FSA_ORDR		fsa WITH (NOLOCK) ON	lo.ORDR_ID =	fsa.ORDR_ID
							INNER JOIN  dbo.LK_ORDR_ACTN	loa WITH (NOLOCK) ON	fsa.ORDR_ACTN_ID =	loa.ORDR_ACTN_ID
							INNER JOIN	dbo.LK_ORDR_TYPE	lot WITH (NOLOCK) ON	fsa.ORDR_TYPE_CD =	lot.FSA_ORDR_TYPE_CD 
							INNER JOIN	dbo.LK_XNCI_RGN		xr	WITH (NOLOCK) ON	ISNULL(lo.RGN_ID, 1) =	xr.RGN_ID
							INNER JOIN	dbo.LK_ORDR_STUS	ts	WITH (NOLOCK) ON 	lo.ORDR_STUS_ID	=	ts.ORDR_STUS_ID
							INNER JOIN  dbo.LK_PROD_TYPE    lpt WITH (NOLOCK) ON	fsa.PROD_TYPE_CD =	lpt.FSA_PROD_TYPE_CD
							LEFT JOIN   dbo.LK_PLTFRM		lp  WITH (NOLOCK) ON	lo.PLTFRM_CD =	lp.PLTFRM_CD
							LEFT JOIN	dbo.FSA_ORDR_CUST	foc WITH (NOLOCK) ON	lo.ORDR_ID =	foc.ORDR_ID
																				AND	foc.CIS_LVL_TYPE		=	''H1''
							LEFT JOIN	dbo.FSA_ORDR_CUST	foc5 WITH (NOLOCK) ON	lo.ORDR_ID =	foc5.ORDR_ID
																				AND	foc5.CIS_LVL_TYPE		=	''H5''
							LEFT JOIN	dbo.FSA_ORDR_CUST	foc6 WITH (NOLOCK) ON	lo.ORDR_ID =	foc6.ORDR_ID
																				AND	foc6.CIS_LVL_TYPE		=	''H6''
							LEFT JOIN	dbo.ACT_TASK		at	WITH (NOLOCK) ON	lo.ORDR_ID				=	at.ORDR_ID 
																				AND	at.STUS_ID				IN	(0, 3)
							LEFT JOIN	LK_TASK				lt WITH (NOLOCK) ON lt.TASK_ID				=	at.TASK_ID 
							LEFT JOIN	dbo.MAP_GRP_TASK	gt WITH (NOLOCK) ON gt.TASK_ID				=	at.TASK_ID
							LEFT JOIN	dbo.LK_GRP			lg WITH (NOLOCK) ON gt.GRP_ID				=	lg.GRP_ID
							LEFT JOIN	dbo.LK_STUS			ls WITH (NOLOCK) ON at.STUS_ID				=	ls.STUS_ID
							LEFT JOIN	dbo.CKT				ct WITH (NOLOCK) ON ct.ORDR_ID				=	lo.ORDR_ID
							LEFT JOIN dbo.NRM_CKT			nc WITH (NOLOCK) ON nc.FTN = fsa.FTN
							LEFT JOIN dbo.NRM_SRVC_INSTC			nsi WITH (NOLOCK) ON nsi.FTN = fsa.FTN
							LEFT JOIN	dbo.LK_PPRT			lpprt	WITH (NOLOCK)	ON	lpprt.PPRT_ID			=	lo.PPRT_ID
							LEFT JOIN	#OrderTmp			lca WITH (NOLOCK)  ON lca.OrderID = lo.ORDR_ID
							LEFT JOIN	dbo.FSA_ORDR_GOM_XNCI fogx WITH (NOLOCK) ON		fogx.ORDR_ID	=	lo.ORDR_ID
																					AND	fogx.GRP_ID		=	1
							LEFT JOIN	dbo.LK_VNDR		sf	WITH (NOLOCK)			ON	sf.VNDR_CD		=	fsa.CXR_ACCS_CD
							LEFT JOIN	dbo.LK_VNDR		cp	WITH (NOLOCK)			ON	cp.VNDR_CD		=	fsa.VNDR_VPN_CD	
							LEFT JOIN	dbo.LK_VNDR		rs	WITH (NOLOCK)			ON	rs.VNDR_CD		=	fsa.INSTL_VNDR_CD																
							LEFT JOIN	dbo.FSA_ORDR	rfs	WITH (NOLOCK)			ON	rfs.FTN		=	fsa.RELTD_FTN 
							LEFT JOIN	dbo.LK_VNDR		rsf	WITH (NOLOCK)			ON	rsf.VNDR_CD		=	rfs.CXR_ACCS_CD
							LEFT JOIN	dbo.LK_VNDR		rcp	WITH (NOLOCK)			ON	rcp.VNDR_CD		=	rfs.VNDR_VPN_CD	
							LEFT JOIN	dbo.LK_VNDR		rrs	WITH (NOLOCK)			ON	rrs.VNDR_CD		=	rfs.INSTL_VNDR_CD
							LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)			ON	vo.ORDR_ID		=	lo.ORDR_ID
							LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)			ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
							LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)			ON	lvo.VNDR_CD = vf.VNDR_CD
						WHERE	lo.ORDR_STUS_ID <> 6	'
					
					
	 SET @SQLStr1 =		'SELECT  DISTINCT TOP 500 
							lo.ORDR_ID,
							lo.ORDR_ID										AS FTN,
							NULL											AS H1,
							lc.CTRY_NME										AS [Country Name],
							ipl.PRE_XST_PL_NBR								AS [Private Line ],
							Case	lo.SCURD_CD
								When	1	Then	''Private Customer''
								Else	dbo.decryptBinaryData(hf.CUST_NME)
							End	AS CUST_NME,
							ipl.GCS_PRICE_QOT_NBR							AS [GCS Quote ID ],
							ISNULL(lo.CUST_CMMT_DT, ipl.CUST_CMMT_DT),
							NULL											AS ORDR_ACTN_DES,
							ipl.SCA_NBR										AS [SCA Number],
							NULL											AS [Vendor Quote ID],
							NULL											AS [Vendor Circuit ID], 
							pt.PROD_TYPE_DES								AS [Product Type],
							pl.PLTFRM_NME									AS [Platform Type],
							NULL											AS ORDR_SUB_TYPE_CD,
							CASE ipl.ORDR_TYPE_ID
								WHEN 8 THEN lt2.ORDR_TYPE_DES + '' '' + lot.ORDR_TYPE_DES
								ELSE lot.ORDR_TYPE_DES
							END												AS ORDR_TYPE_DES,
							hf.CUST_ID										AS H5_H6_CUST_ID,
							cm.TRGT_DLVRY_DT,
							CASE lo.DMSTC_CD 
								WHEN 0 THEN '' Domestic''    
								WHEN 1 THEN ''International'' 
							END												AS DomesticFlag,
							NULL											AS [PRS Quote Number],
							ipl.SOI											AS SOI,
							--web.getVendorName1(lo.ORDR_ID)					AS [Vendor Name],
							ISNULL(lv.CXR_NME,ISNULL(lvo.VNDR_NME,'''')) AS [Vendor Name],
							CASE 
								WHEN lo.DMSTC_CD = 1 THEN xr.RGN_DES
								WHEN lo.DMSTC_CD = 0 THEN ''''
							END												AS Region,
							CASE 
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 1 THEN ''ENCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 1 THEN ''ANCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 THEN ISNULL(lg.GRP_NME,0)
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS GRP_NME,
							ISNULL(lt.TASK_NME, '''')						AS TASK_NME,
							Case	ISNULL(lt.TASK_NME, '''')
								When	''Bill Activated''	Then	ts.ORDR_STUS_DES
								When	''''				Then	ts.ORDR_STUS_DES
								Else	ls.STUS_DES 
							End												AS STUS_DES, 		
							CASE 
								WHEN lg.GRP_ID in (5,6,7) and lo.RGN_ID = 1 THEN 5
								WHEN lg.GRP_ID in (5,6,7) and lo.RGN_ID = 3 THEN 6
								WHEN lg.GRP_ID in (5,6,7)and lo.RGN_ID = 2 THEN 7
								WHEN lg.GRP_ID in (5,6,7)and IsNull(lo.RGN_ID,0) = 0 THEN 5
								WHEN lg.GRP_ID NOT IN (5,6,7) and IsNull(lg.GRP_ID,0) <> 0 THEN lg.GRP_ID
								WHEN lg.GRP_ID NOT IN (5,6,7) and lg.GRP_ID <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS  GRP_ID,
							lt.TASK_ID,
							lo.ORDR_CAT_ID, 
							lo.CSG_LVL_CD, 
							lo.SCURD_CD,  
							lo.H5_FOLDR_ID,
							ISNULL(nsi.NUA_ADR,'''')                         AS NUA
						FROM			ORDR					lo WITH (NOLOCK) 
							INNER JOIN	IPL_ORDR				ipl WITH (NOLOCK)	ON	lo.ORDR_ID				=	ipl.ORDR_ID
							INNER JOIN	LK_PROD_TYPE			pt WITH (NOLOCK)	ON	ipl.PROD_TYPE_ID =	pt.PROD_TYPE_ID
							INNER JOIN	H5_FOLDR				hf WITH (NOLOCK)	ON	lo.H5_FOLDR_ID =	hf.H5_FOLDR_ID
							INNER JOIN	dbo.LK_ORDR_TYPE		lot	WITH (NOLOCK)	ON	ipl.ORDR_TYPE_ID =	lot.ORDR_TYPE_ID
							LEFT JOIN	dbo.LK_ORDR_TYPE		lt2 WITH (NOLOCK)	ON	ipl.PREV_ORDR_TYPE_ID = lt2.ORDR_TYPE_ID
							LEFT JOIN	CKT						ct	WITH (NOLOCK)	ON	lo.ORDR_ID =	ct.ORDR_ID 
							LEFT JOIN	CKT_COST				ctc	WITH (NOLOCK)	ON	ct.CKT_ID =	ctc.CKT_ID
							LEFT JOIN	CKT_MS					cm	WITH (NOLOCK)	ON	ct.CKT_ID =	cm.CKT_ID
							LEFT JOIN	LK_PLTFRM				pl	WITH (NOLOCK)	ON	lo.PLTFRM_CD =	pl.PLTFRM_CD
							LEFT JOIN	ORDR_ADR				oa	WITH (NOLOCK)	ON	lo.ORDR_ID =	oa.ORDR_ID
																						AND	oa.ADR_TYPE_ID =	11
							LEFT JOIN	LK_CTRY					lc	WITH (NOLOCK)	ON	oa.CTRY_CD =	lc.CTRY_CD  
							INNER JOIN	dbo.LK_XNCI_RGN			xr	WITH (NOLOCK)	ON	ISNULL(lo.RGN_ID, 1) =	xr.RGN_ID
							INNER JOIN	dbo.LK_ORDR_STUS		ts	WITH (NOLOCK)	ON 	lo.ORDR_STUS_ID	=	ts.ORDR_STUS_ID
							LEFT JOIN	dbo.ACT_TASK			at	WITH (NOLOCK)	ON	lo.ORDR_ID	=	at.ORDR_ID 
																						AND	at.STUS_ID				IN	(0, 3)
							LEFT JOIN	LK_TASK					lt	WITH (NOLOCK)	ON lt.TASK_ID = at.TASK_ID 
							LEFT JOIN	dbo.MAP_GRP_TASK		gt	WITH (NOLOCK)	ON gt.TASK_ID = at.TASK_ID
							LEFT JOIN	dbo.LK_GRP				lg	WITH (NOLOCK)	ON gt.GRP_ID = lg.GRP_ID
							LEFT JOIN	dbo.LK_STUS				ls	WITH (NOLOCK)	ON at.STUS_ID = ls.STUS_ID
							LEFT JOIN   dbo.NRM_CKT             nc  WITH (NOLOCK)   on nc.FTN = lo.ORDR_ID
							LEFT JOIN dbo.NRM_SRVC_INSTC			nsi WITH (NOLOCK) ON nsi.FTN = lo.ORDR_ID
							LEFT JOIN	#OrderTmp				lca WITH (NOLOCK)	 ON lca.OrderID = lo.ORDR_ID
							LEFT JOIN	dbo.FSA_ORDR_GOM_XNCI fogx WITH (NOLOCK) ON		fogx.ORDR_ID	=	lo.ORDR_ID
																					AND	fogx.GRP_ID		=	1
							LEFT JOIN	dbo.IPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	lo.ORDR_ID		=	la.ORDR_ID
																AND	la.ACCS_TYPE_ID	=	''Terminating''
							LEFT JOIN	dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD	
							LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)				ON	vo.ORDR_ID		=	lo.ORDR_ID
							LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)				ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
							LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)				ON	lvo.VNDR_CD = vf.VNDR_CD
							WHERE (lo.ORDR_CAT_ID IN (1,5)) AND ipl.ORDR_STUS_ID <> 20 '
							
		SET @SQLStr3 = ' SELECT  DISTINCT TOP 500 
							lo.ORDR_ID,
							lo.ORDR_ID										AS FTN,
							0												AS H1,
							lc.CTRY_NME										AS [Country Name],
							ISNULL(nco.PL_NBR,'''')								AS [Private Line ],
							Case	lo.SCURD_CD
								When	1	Then	''Private Customer''
								Else	dbo.decryptBinaryData(nco.CUST_NME)
							End	AS CUST_NME,
							''''											AS [GCS Quote ID ],
							ISNULL(lo.CUST_CMMT_DT, ISNULL(nco.CCS_DT,'''')),
							''Submit''										AS ORDR_ACTN_DES,
							''''											AS [SCA Number],
							NULL											AS [Vendor Quote ID],
							NULL											AS [Vendor Circuit ID], 
							pt.PROD_TYPE_DES								AS [Product Type],
							pl.PLTFRM_NME									AS [Platform Type],
							NULL											AS ORDR_SUB_TYPE_CD,
							lot.ORDR_TYPE_DES								AS ORDR_TYPE_DES,
							hf.CUST_ID										AS H5_H6_CUST_ID,
							'''',
							CASE lo.DMSTC_CD 
								WHEN 0 THEN '' Domestic''    
								WHEN 1 THEN ''International'' 
							END												AS DomesticFlag,
							NULL											AS [PRS Quote Number],
							''''											AS SOI,
							--web.getVendorName1(lo.ORDR_ID)					AS [Vendor Name],
							ISNULL(lv.VNDR_NME,ISNULL(lvo.VNDR_NME,'''')) AS [Vendor Name],
							CASE 
								WHEN lo.DMSTC_CD = 1 THEN xr.RGN_DES
								WHEN lo.DMSTC_CD = 0 THEN ''''
							END												AS Region,
							CASE 
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 1 THEN ''ENCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 1 THEN ''ANCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 THEN ISNULL(lg.GRP_NME,0)
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS GRP_NME,
							ISNULL(lt.TASK_NME, '''')						AS TASK_NME,
							Case	ISNULL(lt.TASK_NME, '''')
								When	''Bill Activated''	Then	ts.ORDR_STUS_DES
								When	''''				Then	ts.ORDR_STUS_DES
								Else	ls.STUS_DES 
							End												AS STUS_DES, 		
							CASE 
								WHEN lg.GRP_ID in (5,6,7) and lo.RGN_ID = 1 THEN 5
								WHEN lg.GRP_ID in (5,6,7) and lo.RGN_ID = 3 THEN 6
								WHEN lg.GRP_ID in (5,6,7)and lo.RGN_ID = 2 THEN 7
								WHEN lg.GRP_ID in (5,6,7)and IsNull(lo.RGN_ID,0) = 0 THEN 5
								WHEN lg.GRP_ID NOT IN (5,6,7) and IsNull(lg.GRP_ID,0) <> 0 THEN lg.GRP_ID
								WHEN lg.GRP_ID NOT IN (5,6,7) and lg.GRP_ID <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS  GRP_ID,
							lt.TASK_ID,
							lo.ORDR_CAT_ID, 
							lo.CSG_LVL_CD, 
							lo.SCURD_CD,  
							lo.H5_FOLDR_ID,
							ISNULL(nsi.NUA_ADR,'''')                         AS NUA
						FROM			ORDR					lo	WITH (NOLOCK) 
							INNER JOIN	NCCO_ORDR				nco WITH (NOLOCK)	ON	lo.ORDR_ID				=	nco.ORDR_ID
							INNER JOIN	LK_PROD_TYPE			pt	WITH (NOLOCK)	ON	nco.PROD_TYPE_ID =	pt.PROD_TYPE_ID
							LEFT JOIN	H5_FOLDR				hf	WITH (NOLOCK)	ON	lo.H5_FOLDR_ID =	hf.H5_FOLDR_ID
							INNER JOIN	dbo.LK_ORDR_TYPE		lot	WITH (NOLOCK)	ON	nco.ORDR_TYPE_ID =	lot.ORDR_TYPE_ID
							LEFT JOIN	LK_PLTFRM				pl	WITH (NOLOCK)	ON	lo.PLTFRM_CD =	pl.PLTFRM_CD
							LEFT JOIN	LK_CTRY					lc	WITH (NOLOCK)	ON	nco.CTRY_CD =	lc.CTRY_CD  
							INNER JOIN	dbo.LK_XNCI_RGN			xr	WITH (NOLOCK)	ON	ISNULL(lo.RGN_ID, 1) =	xr.RGN_ID
							INNER JOIN	dbo.LK_ORDR_STUS		ts	WITH (NOLOCK)	ON 	lo.ORDR_STUS_ID	=	ts.ORDR_STUS_ID
							LEFT JOIN	dbo.ACT_TASK			at	WITH (NOLOCK)	ON	lo.ORDR_ID	=	at.ORDR_ID 
																						AND	at.STUS_ID				IN	(0, 3)
							LEFT JOIN	LK_TASK					lt	WITH (NOLOCK)	ON lt.TASK_ID = at.TASK_ID 
							LEFT JOIN	dbo.MAP_GRP_TASK		gt	WITH (NOLOCK)	ON gt.TASK_ID = at.TASK_ID
							LEFT JOIN	dbo.LK_GRP				lg	WITH (NOLOCK)	ON gt.GRP_ID = lg.GRP_ID
							LEFT JOIN	dbo.LK_STUS				ls	WITH (NOLOCK)	ON at.STUS_ID = ls.STUS_ID
							LEFT JOIN   dbo.NRM_CKT             nc  WITH (NOLOCK)   on nc.FTN = lo.ORDR_ID
							LEFT JOIN	dbo.NRM_SRVC_INSTC		nsi WITH (NOLOCK)	ON nsi.FTN = lo.ORDR_ID
							LEFT JOIN	#OrderTmp				lca WITH (NOLOCK)	 ON lca.OrderID = lo.ORDR_ID
							LEFT JOIN	dbo.FSA_ORDR_GOM_XNCI fogx WITH (NOLOCK) ON		fogx.ORDR_ID	=	lo.ORDR_ID
																					AND	fogx.GRP_ID		=	1
							LEFT JOIN	dbo.LK_VNDR		lv	 WITH (NOLOCK)			ON	nco.VNDR_CD	=	lv.VNDR_CD																	
							LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)				ON	vo.ORDR_ID		=	lo.ORDR_ID
							LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)				ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
							LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)				ON	lvo.VNDR_CD = vf.VNDR_CD														
							WHERE lo.ORDR_CAT_ID = 4 '						
	   
		IF ((@FTNs <>  '') OR (@ParentFTN <> '') OR ((@RelFTN <> '') AND (@RelFTN = 'TRUE'))) 
			BEGIN   
				SET @SQLStr  =  @SQLStr + 'AND fsa.FTN IN ( SELECT DISTINCT FTN FROM #FTNs WITH (NOLOCK))'
				SET @SQLStr1  =  @SQLStr1 + 'AND lo.ORDR_ID IN ( SELECT DISTINCT FTN FROM #FTNs WITH (NOLOCK))'
				SET @SQLStr3  =  @SQLStr3 + 'AND lo.ORDR_ID IN ( SELECT DISTINCT FTN FROM #FTNs WITH (NOLOCK))'
			END  		
		
		IF (@Names <>  '')
			BEGIN
				SET @SQLStr  = @SQLStr + 'AND dbo.decryptBinaryData(foc.CUST_NME) IN ( SELECT DISTINCT Name FROM #Names WITH (NOLOCK))'
				SET @SQLStr1  = @SQLStr1 + 'AND dbo.decryptBinaryData(hf.CUST_NME) IN ( SELECT DISTINCT Name FROM #Names WITH (NOLOCK))'
				SET @SQLStr3  = @SQLStr3 + 'AND dbo.decryptBinaryData(nco.CUST_NME) IN ( SELECT DISTINCT Name FROM #Names WITH (NOLOCK))'
			END

		IF (@H5names <>  '')
			BEGIN
				SET @SQLStr  = @SQLStr + 'AND (dbo.decryptBinaryData(foc5.CUST_NME) IN ( SELECT DISTINCT Name FROM #H5names WITH (NOLOCK)) OR
											dbo.decryptBinaryData(foc6.CUST_NME) IN ( SELECT DISTINCT Name FROM #H5names WITH (NOLOCK)))'
				SET @SQLStr1  = @SQLStr1 + 'AND dbo.decryptBinaryData(hf.CUST_NME) IN ( SELECT DISTINCT Name FROM #H5names WITH (NOLOCK))'
				SET @SQLStr3  = @SQLStr3 + 'AND dbo.decryptBinaryData(nco.CUST_NME) IN ( SELECT DISTINCT Name FROM #H5names WITH (NOLOCK))'
			END

		IF(@OrderSubType <> ''AND @OrderSubType <>'0')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND fsa.ORDR_SUB_TYPE_CD in ( SELECT DISTINCT OrderSubType  FROM #OrderSubType WITH (NOLOCK))'
				SET @SQLStr1 = @SQLStr1 + ' AND lca.ORDR_SUB_TYPE_CD in ( SELECT DISTINCT OrderSubType  FROM #OrderSubType WITH (NOLOCK))'
				SET @SQLStr3 = @SQLStr3 + ' AND lca.ORDR_SUB_TYPE_CD in ( SELECT DISTINCT OrderSubType  FROM #OrderSubType WITH (NOLOCK))'
			END
		
		IF ((Select Count(1) From #CCDOrders) > 0 )
			BEGIN
				SET @SQLStr  =  @SQLStr + ' AND fsa.ORDR_ID IN (SELECT OrderID FROM #CCDOrders WITH (NOLOCK))' 
				SET @SQLStr1  =  @SQLStr1 + ' AND ipl.ORDR_ID IN (SELECT OrderID FROM #CCDOrders WITH (NOLOCK))' 
				SET @SQLStr3  =  @SQLStr3 + ' AND nco.ORDR_ID IN (SELECT OrderID FROM #CCDOrders WITH (NOLOCK))'
			END

		IF(  @OrderTypes <>'0' AND  @OrderTypes <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lot.ORDR_TYPE_ID in (SELECT  OrderTyp from #OrderTypes WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (lot.ORDR_TYPE_ID in (SELECT  iplOrderTyp from #iplOrderTypes WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lot.ORDR_TYPE_ID in (SELECT  iplOrderTyp from #iplOrderTypes WITH (NOLOCK)))'
			END
		
		IF(@H5_H6 <> '' )
			BEGIN
				SET @SQLStr  =  @SQLStr + 'AND lo.H5_H6_CUST_ID in( SELECT DISTINCT H5_H6  from #H5_H6 WITH (NOLOCK))'
				SET @SQLStr1  =  @SQLStr1 + 'AND (lo.H5_H6_CUST_ID in( SELECT DISTINCT H5_H6  from #H5_H6 WITH (NOLOCK)) OR (hf.CUST_ID in( SELECT DISTINCT H5_H6  from #H5_H6 WITH (NOLOCK))))'
				SET @SQLStr3  =  @SQLStr3 + 'AND lo.H5_H6_CUST_ID in( SELECT DISTINCT H5_H6  from #H5_H6 WITH (NOLOCK))'
			END
		
		IF(  @H1 <> '' )
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (foc.CUST_ID in (SELECT DISTINCT H1 from #TmpH1 WITH (NOLOCK)) )'
				SET @SQLStr1 = @SQLStr1 + ' AND (lca.H1 in (SELECT DISTINCT H1 from #TmpH1 WITH (NOLOCK)) )'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.H1 in (SELECT DISTINCT H1 from #TmpH1 WITH (NOLOCK)) )'
			END
		
		IF (@SOI <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (ISNULL(foc5.SOI_CD,foc6.SOI_CD) in (SELECT SOI from #SOI WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.SOI in ( SELECT SOI from #SOI WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.SOI in ( SELECT SOI from #SOI WITH (NOLOCK)))'
			END
		
		IF (@PRSQuote <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (fsa.TSUP_PRS_QOT_NBR in ( SELECT PRS from #PRS WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (lca.TSUP_PRS_QOT_NBR in ( SELECT PRS from #PRS WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.TSUP_PRS_QOT_NBR in ( SELECT PRS from #PRS WITH (NOLOCK)))'
			END
		
		IF(ISNULL (@WorkGroup, 0) <> '0' AND @WorkGroup <>'')
		
				BEGIN
				IF @WorkGroup in (5,6,7)
					BEGIN
						SET @SQLStr = @SQLStr + ' AND ( xr.GRP_ID = '' ' + @WorkGroup + ''' )'
												+ ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
						SET @SQLStr1 = @SQLStr1 + ' AND ( xr.GRP_ID = '' ' + @WorkGroup + ''' )'
												+ ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
						SET @SQLStr3 = @SQLStr3 + ' AND ( xr.GRP_ID = '' ' + @WorkGroup + ''' )'
												+ ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'						
					END
				ELSE
					BEGIN
						SET @SQLStr = @SQLStr + ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
						SET @SQLStr1 = @SQLStr1 + ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
						SET @SQLStr3 = @SQLStr3 + ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
					END
			END
			
		IF (@PrivateLine  <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (nc.PLN_NME   in ( SELECT PrivateLine from #PrivateLine WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.PRE_XST_PL_NBR in ( SELECT PrivateLine from #PrivateLine WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (nco.PL_NBR in ( SELECT PrivateLine from #PrivateLine WITH (NOLOCK)))'
			END
			
 		IF (@GCSQuoteID   <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lca.GCS_PRICE_QOT_NBR  in ( SELECT GCSQuoteID from #GCSQuoteID WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.GCS_PRICE_QOT_NBR in ( SELECT GCSQuoteID from #GCSQuoteID WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.GCS_PRICE_QOT_NBR in ( SELECT GCSQuoteID from #GCSQuoteID WITH (NOLOCK)))'
			END
			
		IF (@VendorQuoteID    <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lca.QOT_NBR  in ( SELECT VendorQuoteID from #VendorQuoteID WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ctc.QOT_NBR in ( SELECT VendorQuoteID from #VendorQuoteID WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.QOT_NBR in ( SELECT VendorQuoteID from #VendorQuoteID WITH (NOLOCK)))'
			END
			
		IF (@SCANumber	    <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lca.SCA_NBR  in ( SELECT SCANumber from #SCANumber WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.SCA_NBR  in ( SELECT SCANumber from #SCANumber WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.SCA_NBR  in ( SELECT SCANumber from #SCANumber WITH (NOLOCK)))'
			END
	 
 		IF (@VendorCircuit     <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (ct.VNDR_CKT_ID in ( SELECT VendorCircuit from #VendorCircuit WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ct.VNDR_CKT_ID in ( SELECT VendorCircuit from #VendorCircuit WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.VNDR_CKT_ID in ( SELECT VendorCircuit from #VendorCircuit WITH (NOLOCK)))'
			END
			   
 		IF (@VendorName     <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND CASE lo.PLTFRM_CD
									WHEN ''SF''	THEN ISNULL(sf.VNDR_NME,ISNULL(rsf.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									WHEN ''CP''	THEN ISNULL(cp.VNDR_NME,ISNULL(rcp.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									WHEN ''RS''	THEN ISNULL(rs.VNDR_NME,ISNULL(rrs.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									ELSE ''''
									END in ( SELECT VendorName  from #VendorName  WITH (NOLOCK))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ISNULL(lv.CXR_NME,ISNULL(lvo.VNDR_NME,'''')) in ( SELECT VendorName  from #VendorName  WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (ISNULL(lv.VNDR_NME,ISNULL(lvo.VNDR_NME,'''')) in ( SELECT VendorName  from #VendorName  WITH (NOLOCK)))'
			END

		IF(@Region    <> '' AND @Region    <> '0' )
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lo.RGN_ID  = '' ' + @Region + ''')'
				SET @SQLStr1 = @SQLStr1 + ' AND (lo.RGN_ID  = '' ' + @Region + ''')'
				SET @SQLStr3 = @SQLStr3 + ' AND (lo.RGN_ID  = '' ' + @Region + ''')'
			END


			
		IF(@ProductType    <> '' AND @ProductType    <> '0' )
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (fsa.PROD_TYPE_CD IN (SELECT  FSA_PROD_TYPE_CD FROM dbo.LK_PROD_TYPE WITH (NOLOCK) WHERE PROD_TYPE_ID =  '' ' + @ProductType + '''  ))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.PROD_TYPE_ID  =   '' ' + @ProductType + ''' )'
				SET @SQLStr3 = @SQLStr3 + ' AND (nco.PROD_TYPE_ID  =   '' ' + @ProductType + ''' )'
			END
			
				
	 
		IF(@PlatformType    <> '' AND @PlatformType <> '0' )
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lo.PLTFRM_CD   = ''' + @PlatformType +''')'
				SET @SQLStr1 = @SQLStr1 + ' AND (lo.PLTFRM_CD    = ''' + @PlatformType+''')'
				SET @SQLStr3 = @SQLStr3 + ' AND (lo.PLTFRM_CD    = ''' + @PlatformType+''')'
			END
			
		IF(@CustomerName   <> '' )
		    
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lo.CustomerName in ( SELECT CustomerName  from #CustomerName  WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (lo.CustomerName in ( SELECT CustomerName  from #CustomerName  WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lo.CustomerName in ( SELECT CustomerName  from #CustomerName  WITH (NOLOCK)))'
			END

		IF @Status <> ''
					BEGIN
						IF (@Status = '1')
						BEGIN
							SET @SQLStr = @SQLStr + ' AND (lo.ORDR_STUS_ID in (2,5)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
							SET @SQLStr1 = @SQLStr1 + ' AND (lo.ORDR_STUS_ID in (2,5)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
							SET @SQLStr3 = @SQLStr3 + ' AND (lo.ORDR_STUS_ID in (2,5)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						END
						ELSE
						BEGIN
							SET @SQLStr = @SQLStr + ' AND (lo.ORDR_STUS_ID IN (0, 1)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
							SET @SQLStr1 = @SQLStr1 + ' AND (lo.ORDR_STUS_ID IN (0, 1)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
							SET @SQLStr3 = @SQLStr3 + ' AND (lo.ORDR_STUS_ID IN (0, 1)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						END
					END
	  		IF (@NUA <> '')
				BEGIN
					SET @SQLStr = @SQLStr + ' AND ((nsi.NUA_ADR in (SELECT NUA from #NUA WITH (NOLOCK))) OR (nc.NUA_ADR in (SELECT NUA from #NUA WITH (NOLOCK))))'
					SET @SQLStr1 = @SQLStr1 + ' AND ((nsi.NUA_ADR in (SELECT NUA from #NUA WITH (NOLOCK))) OR (nc.NUA_ADR in (SELECT NUA from #NUA WITH (NOLOCK))))'
					SET @SQLStr3 = @SQLStr3 + ' AND ((nsi.NUA_ADR in (SELECT NUA from #NUA WITH (NOLOCK))) OR (nc.NUA_ADR in (SELECT NUA from #NUA WITH (NOLOCK))))'
				END 
		
		IF (@PRCH_ORDR_NBR <> '' OR  @ATLAS_WRK_ORDR_NBR <> '' OR @PLSFT_RQSTN_NBR	<> '')
		   BEGIN
				IF @PRCH_ORDR_NBR <> ''
					BEGIN
						SET @SQLStr = @SQLStr + ' AND (fogx.PRCH_ORDR_NBR   = ''' + @PRCH_ORDR_NBR +''')'
						SET @SQLStr1 = @SQLStr1 + ' AND (fogx.PRCH_ORDR_NBR   = ''' + @PRCH_ORDR_NBR +''')'
						SET @SQLStr3 = @SQLStr3 + ' AND (fogx.PRCH_ORDR_NBR   = ''' + @PRCH_ORDR_NBR +''')'
					END
					
				IF @ATLAS_WRK_ORDR_NBR <> ''
					BEGIN
						SET @SQLStr = @SQLStr + ' AND (fogx.ATLAS_WRK_ORDR_NBR   = ''' + @ATLAS_WRK_ORDR_NBR +''')'
						SET @SQLStr1 = @SQLStr1 + ' AND (fogx.ATLAS_WRK_ORDR_NBR   = ''' + @ATLAS_WRK_ORDR_NBR +''')'
						SET @SQLStr3 = @SQLStr3 + ' AND (fogx.ATLAS_WRK_ORDR_NBR   = ''' + @ATLAS_WRK_ORDR_NBR +''')'
					END
					
				IF @PLSFT_RQSTN_NBR <> ''
					BEGIN
						SET @SQLStr = @SQLStr + ' AND (fogx.PLSFT_RQSTN_NBR   = ''' + @PLSFT_RQSTN_NBR +''')'
						SET @SQLStr1 = @SQLStr1 + ' AND (fogx.PLSFT_RQSTN_NBR   = ''' + @PLSFT_RQSTN_NBR +''')'
						SET @SQLStr3 = @SQLStr3 + ' AND (fogx.PLSFT_RQSTN_NBR   = ''' + @PLSFT_RQSTN_NBR +''')'
					END
					
		   END
		   SET @sql_stmt = 'SELECT DISTINCT TOP 500  
						OrderID									AS ORDR_ID,
						FTN,
						H1,
						CountryName,
						PrivateLine,
						CustomerName,
						CCD,
						OrderType,  
						VendorCircuitID,
						ProductType,
						PlatformType,
						OrderSubType,
						[H5/H6],
						DomesticFlag,
						PRSQuote,
						SOI,
						VendorName,
						Region,
						OrderTypeDesc,
						WorkGroup,
						Case	CurrentTask
							When	''Bill Activated''	Then	OrderStatus 
							WHEN	''xNCI Milestones Complete'' THEN ''xNCI Milestones Complete''
							When	''''				Then	OrderStatus 
							Else	CurrentTask	+ ''-'' + OrderStatus
						End										AS	OrderStatus, 
						ISNULL(WGID,0)							AS WGID,
						ISNULL(TASKID,0)						AS TASK_ID,
						ISNULL(CAT_ID,0)						AS CAT_ID,
						ISNULL(CSG_LVL_CD,0)					AS CSG_LVL_CD,
						ISNULL(SCURD_CD, 0)						AS SCURD_CD,
						ISNULL(H5_FOLDR_ID,0)					AS H5_FOLDR_ID,
						ProductType								AS [PROD_TYPE],
						Case	CurrentTask
							When	''Bill Activated''	Then	OrderStatus 
							WHEN	''xNCI Milestones Complete'' THEN ''xNCI Milestones Complete''
							When	''''				Then	OrderStatus 
							Else	CurrentTask	+ ''-'' + OrderStatus
						End										AS [ORDR_STUS],
						OrderTypeDesc							AS [ORDR_TYPE],
						PlatformType							AS [PLTFRM_NME]
					FROM #orderlist' 
	 
  
	END  
------------------------------------------------------------------------------------------------------------------------------ 
-----------------------@Mode=0 END-------------------------------------------------------------------------------------------- 
------------------------------------------------------------------------------------------------------------------------------  	   
IF(@Mode=1)
BEGIN
		IF @CCDs IS NOT NULL OR @CCDOption != '0'
			BEGIN
				--DECLARE @e_sqlEvent NVARCHAR(MAX)
				--DECLARE @e_where	NVARCHAR(max)	 = ''
				--DECLARE @e_finalSQL NVARCHAR(MAX)
				SET @e_sqlEvent = 'INSERT INTO #CCDOrders (OrderID)        
										SELECT DISTINCT odr.ORDR_ID
											FROM  dbo.vORDR odr WITH(NOLOCK) 
										Left Join dbo.vFSA_ORDR fsa WITH (NOLOCK) ON fsa.ORDR_ID = odr.ORDR_ID
										Left Join dbo.vIPL_ORDR ipl WITH (NOLOCK) ON ipl.ORDR_ID = odr.ORDR_ID
										Left Join dbo.vNCCO_ORDR ncco WITH (NOLOCK) ON ncco.ORDR_ID = odr.ORDR_ID
											WHERE 1 = 1 '

  				IF @CCDs IS NOT NULL
  					BEGIN
						IF @CCDOps != '0'
							BEGIN
								IF @CCDOps = '1'
									BEGIN
										SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= ''' + CONVERT(varchar,@CCDs) + '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <=''' + CONVERT(Varchar,DATEADD(day,180,@CCDs)) + ''''  
									END
								ELSE IF @CCDOps = '2'
									BEGIN
										SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= ''' + CONVERT(varchar,DATEADD(day,-180,@CCDs)) + '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <=''' + CONVERT(Varchar,@CCDs) + ''''  
									END
								ELSE IF @CCDOps = '3'
									BEGIN
										SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) = ''' + CONVERT(Varchar,@CCDs) + ''''  
									END
							END
						ELSE
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) = ''' + CONVERT(Varchar,@CCDs) + ''''  
							END

						--SET @CCD =  CONVERT(Varchar(10), @CCDs, 101)
					END
				ELSE IF @CCDOption != '0'
					BEGIN
						IF @CCDOption = '1'
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= '''  + CONVERT(varchar,(GETDATE() - 30)) +  '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 
							END
						ELSE IF @CCDOption = '2'
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= '''  + CONVERT(varchar,(GETDATE() - 60)) +  '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 
							END
						ELSE IF @CCDOption = '3'
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= '''  + CONVERT(varchar,(GETDATE() - 90)) +  '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 
							END
						ELSE IF @CCDOption = '4'
							BEGIN
								SET @e_where += ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) >= '''  + CONVERT(varchar,(GETDATE() - 180)) +  '''' + ' AND ISNULL(odr.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT ,ISNULL(ipl.CUST_CMMT_DT, ncco.CCS_DT))) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 
							END
					END
		
				SET @e_finalSQL = @e_sqlEvent + @e_where
				IF @e_finalSQL != ''
					EXEC sp_executesql @e_finalSQL
			END

		
			
		SET @FTNs = REPLACE(@FTNs, '*', '%')
		SET @FTNs = REPLACE(@FTNs, '\r\n', '') 
		
		IF OBJECT_ID(N'tempdb..#FTNs_wAchive', N'U') IS NOT NULL         
					DROP TABLE #FTNs_wAchive   
					CREATE TABLE #FTNs_wAchive         
					(RowID INT Identity(1,1), FTN	Varchar(10))

		IF (@FTNs <> '')
			BEGIN			 

				IF CHARINDEX('%',@FTNs) > 0 
					BEGIN 
						INSERT INTO #FTNs_wAchive (FTN)        
								SELECT DISTINCT TOP 100 FTN AS FTN 
								FROM dbo.vFSA_ORDR WITH(NOLOCK) 
								WHERE FTN LIKE(REPLACE(@FTNs, ',', '')) 
					END           
				ELSE           
					BEGIN          
					INSERT INTO #FTNs_wAchive (FTN)       
								SELECT DISTINCT SubString(stringid, 1, 10) AS FTN 
								FROM web.ParseCommaSeparatedStrings(@FTNs)        
					END 
			END

				---------------------------------------------------------------
				--	RELATED FTN Filter 
				---------------------------------------------------------------
				IF ((@RelFTN <> '') AND (@RelFTN = 'TRUE'))
				BEGIN
					SET @FTNs = REPLACE(@FTNs, '*', '%')
					SET @FTNs = REPLACE(@FTNs, '\r\n', ',')
					--IF CHARINDEX('%',@FTNs) <= 0
					--	SET @FTNs = '%'+@FTNs+'%'

					INSERT INTO #FTNs_wAchive (FTN)
					SELECT  CAST(FO2.FTN AS VARCHAR(10))
					FROM dbo.vFSA_ORDR FO1 WITH (NOLOCK) INNER JOIN
					dbo.vORDR OD WITH (NOLOCK) ON OD.PRNT_ORDR_ID = FO1.ORDR_ID INNER JOIN
					dbo.vFSA_ORDR FO2 WITH (NOLOCK) ON OD.ORDR_ID = FO2.ORDR_ID
					WHERE FO1.FTN LIKE  @FTNs
				END
	             
				---------------------------------------------------------------
				--	PARENT FTN Filter 
				---------------------------------------------------------------
				IF (@ParentFTN <> '')
				BEGIN
					SET @ParentFTN = REPLACE(@ParentFTN, '*', '%')
					SET @ParentFTN = REPLACE(@ParentFTN, '\r\n', ',')
					--IF CHARINDEX('%',@ParentFTN) <= 0
					--	SET @ParentFTN = '%'+@ParentFTN+'%'

					INSERT INTO #FTNs_wAchive (FTN)
					SELECT CAST(FTN AS VARCHAR(10))
					FROM dbo.vFSA_ORDR WITH (NOLOCK)
					WHERE PRNT_FTN LIKE  @ParentFTN
					UNION ALL
					SELECT REPLACE(@ParentFTN,'%','')
				END  

		IF(@Names <> '')
			BEGIN
				IF OBJECT_ID(N'tempdb..#Names_wAchive', N'U') IS NOT NULL
					DROP TABLE #Names_wAchive
					CREATE TABLE #Names_wAchive
						(RowID INT Identity(1,1), Name Varchar(1000))
												IF CHARINDEX ('%', @Names) > 0
							
								BEGIN
								  INSERT INTO #Names_wAchive (Name)
							  		SELECT DISTINCT TOP 100 dbo.decryptBinaryData(CUST_NME) AS Name 
									FROM	dbo.vFSA_ORDR_CUST WITH(NOLOCK) 
									WHERE	dbo.decryptBinaryData(CUST_NME) LIKE(REPLACE(@Names, ',', '')) 
									AND		CIS_LVL_TYPE = 'H1'
								END
							ELSE
								BEGIN
								   INSERT INTO #Names_wAchive(Name)
								   SELECT DISTINCT TOP 100 dbo.decryptBinaryData(CUST_NME) AS Name 
									FROM	dbo.vFSA_ORDR_CUST WITH(NOLOCK) 
									WHERE	dbo.decryptBinaryData(CUST_NME) IN (@Names)
									AND		CIS_LVL_TYPE = 'H1'
								END
			END

		IF(@H5names <> '')
			BEGIN
				IF OBJECT_ID(N'tempdb..#H5names_wAchive', N'U') IS NOT NULL
					DROP TABLE #H5names_wAchive
					CREATE TABLE #H5names_wAchive
						(RowID INT Identity(1,1), Name Varchar(1000))
												IF CHARINDEX ('%', @H5names) > 0
							
								BEGIN
								  INSERT INTO #H5names_wAchive (Name)
							  		SELECT DISTINCT TOP 100 dbo.decryptBinaryData(CUST_NME) AS Name 
									FROM	dbo.vFSA_ORDR_CUST WITH(NOLOCK) 
									WHERE	dbo.decryptBinaryData(CUST_NME) LIKE(REPLACE(@H5names, ',', '')) 
									AND		(CIS_LVL_TYPE = 'H5' or CIS_LVL_TYPE = 'H6'or CIS_LVL_TYPE = 'ES')
								END
							ELSE
								BEGIN
								   INSERT INTO #H5names_wAchive(Name)
								   SELECT DISTINCT TOP 100 dbo.decryptBinaryData(CUST_NME) AS Name 
									FROM	dbo.vFSA_ORDR_CUST WITH(NOLOCK) 
									WHERE	dbo.decryptBinaryData(CUST_NME) IN (@H5names)
									AND		(CIS_LVL_TYPE = 'H5' or CIS_LVL_TYPE = 'H6'or CIS_LVL_TYPE = 'ES')
								END
			END
		
				
		IF(@OrderSubType <> ''AND @OrderSubType <>'0')		
			BEGIN
				IF OBJECT_ID(N'tempdb..#OrderSubType_wAchive', N'U') IS NOT NULL
					DROP TABLE #OrderSubType_wAchive
					CREATE TABLE #OrderSubType_wAchive
						(RowID INT Identity(1,1), OrderSubType Varchar(50))
					 
					 			BEGIN
								   INSERT INTO #OrderSubType_wAchive(OrderSubType)
							   		SELECT DISTINCT SubString(stringid, 1, 10) AS Name 
									FROM web.ParseCommaSeparatedStrings(@OrderSubType) 
								END
			
			END
			
		IF(@OrderTypes <> '' AND @OrderTypes <>'0' )		
			BEGIN
				IF OBJECT_ID(N'tempdb..#OrderTypes_wAchive', N'U') IS NOT NULL
					DROP TABLE #OrderTypes_wAchive
				CREATE TABLE #OrderTypes_wAchive
					(RowID INT Identity(1,1), OrderTyp tinyint)
						
				IF OBJECT_ID(N'tempdb..#iplOrderTypes_wAchive', N'U') IS NOT NULL
					DROP TABLE #iplOrderTypes_wAchive
				CREATE TABLE #iplOrderTypes_wAchive
					(RowID INT Identity(1,1), iplOrderTyp tinyint)
					
				IF	ISNUMERIC(@OrderTypes)	=	0
					BEGIN	
						INSERT INTO #OrderTypes_wAchive (OrderTyp) 
							SELECT		ORDR_TYPE_ID AS OrderType 
								FROM	dbo.LK_ORDR_TYPE WITH (NOLOCK)
								WHERE	ORDR_TYPE_DES	LIKE '%' + @OrderTypes + '%'
								
						INSERT INTO #iplOrderTypes_wAchive (iplOrderTyp) 
							SELECT		ORDR_TYPE_ID AS iplOrderTyp 
								FROM	dbo.LK_ORDR_TYPE WITH (NOLOCK) 
								WHERE	ORDR_TYPE_DES	LIKE	'%' + @OrderTypes + '%'			
					END
				ELSE
					BEGIN
						INSERT INTO #OrderTypes_wAchive (OrderTyp)		VALUES	(CONVERT(TINYINT, @OrderTypes))
						INSERT INTO #iplOrderTypes_wAchive (iplOrderTyp)VALUES	(CONVERT(TINYINT, @OrderTypes))
					END
			END
		
		IF(@H5_H6 <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#H5_H6_wAchive', N'U') IS NOT NULL
					DROP TABLE #H5_H6_wAchive
					CREATE TABLE #H5_H6_wAchive
						(RowID INT Identity(1,1), H5_H6 Varchar(20))
					 
					 			BEGIN
								   INSERT INTO #H5_H6_wAchive (H5_H6)
								   VALUES(@H5_H6)
							 			 
								END	
			END

		IF(@H1 <> '' )	
			BEGIN
				IF OBJECT_ID(N'tempdb..#TmpH1_wAchive', N'U') IS NOT NULL
					DROP TABLE #TmpH1_wAchive
					CREATE TABLE #TmpH1_wAchive
						(RowID INT Identity(1,1), H1 Varchar(20))
					 
					 			BEGIN
								   INSERT INTO #TmpH1_wAchive (H1)
									VALUES(@H1)
							 			 
								END	
			END
			
		IF(@PRSQuote <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#PRS_wAchive', N'U') IS NOT NULL
					DROP TABLE #PRS_wAchive
					CREATE TABLE #PRS_wAchive
						(RowID INT Identity(1,1), PRS Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #PRS_wAchive (PRS)
									VALUES(@PRSQuote)
							 			 
								END	
			END
		 
		IF(@SOI <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#SOI_wAchive', N'U') IS NOT NULL
					DROP TABLE #SOI_wAchive
					CREATE TABLE #SOI_wAchive
						(RowID INT Identity(1,1), SOI Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #SOI_wAchive (SOI)
									VALUES(@SOI)
							 			 
								END	
			END	
		
		IF(@PrivateLine <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#PrivateLine_wAchive ', N'U') IS NOT NULL
					DROP TABLE #PrivateLine_wAchive
					CREATE TABLE #PrivateLine_wAchive
						(RowID INT Identity(1,1), PrivateLine Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #PrivateLine_wAchive (PrivateLine)
									VALUES(@PrivateLine)
							 			 
								END	
			END
			
		IF(@GCSQuoteID  <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#GCSQuoteID_wAchive', N'U') IS NOT NULL
					DROP TABLE #GCSQuoteID_wAchive
					CREATE TABLE #GCSQuoteID_wAchive
						(RowID INT Identity(1,1), GCSQuoteID Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #GCSQuoteID_wAchive (GCSQuoteID)
									VALUES(@GCSQuoteID)
							 			 
								END	
			END
		
		IF(@VendorQuoteID   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#VendorQuoteID_wAchive ', N'U') IS NOT NULL
					DROP TABLE #VendorQuoteID_wAchive 
					CREATE TABLE #VendorQuoteID_wAchive
						(RowID INT Identity(1,1), VendorQuoteID Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #VendorQuoteID_wAchive (VendorQuoteID)
									VALUES(@VendorQuoteID)
							 			 
								END	
			END
			
		IF(@VendorName   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#VendorName_wAchive', N'U') IS NOT NULL
					DROP TABLE #VendorName_wAchive
					CREATE TABLE #VendorName_wAchive
						(RowID INT Identity(1,1), VendorName Varchar(100))
					 
					 
					 			BEGIN
					 			 SET @VendorName = '%' +@VendorName+ '%'
								   INSERT INTO #VendorName_wAchive (VendorName)
								   ( SELECT CXR_NME FROM dbo.LK_FRGN_CXR WITH (NOLOCK) WHERE CXR_NME LIKE (@VendorName))
								   
								   INSERT INTO #VendorName_wAchive (VendorName)
								   ( SELECT VNDR_NME FROM dbo.LK_VNDR WITH (NOLOCK) WHERE VNDR_NME LIKE (@VendorName))
							 			 
								END	
			END
			
		IF(@CustomerName   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#CustomerName_wAchive', N'U') IS NOT NULL
					DROP TABLE #CustomerName_wAchive
					CREATE TABLE #CustomerName_wAchive
						(RowID INT Identity(1,1), CustomerName Varchar(100))
					 
					 
					 			BEGIN
					 			 SET @CustomerName = '%' +@CustomerName+ '%'
								   INSERT INTO #CustomerName_wAchive (CustomerName)
								   (SELECT	dbo.decryptBinaryData(CUST_NME)
										FROM	dbo.vFSA_ORDR_CUST WITH(NOLOCK) 
										WHERE	dbo.decryptBinaryData(CUST_NME) LIKE(@CustomerName) 
										AND		CIS_LVL_TYPE = 'H1')
								END	
			END

		IF(@SCANumber   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#SCANumber_wAchive ', N'U') IS NOT NULL
					DROP TABLE #SCANumber_wAchive 
					CREATE TABLE #SCANumber_wAchive
						(RowID INT Identity(1,1), SCANumber Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #SCANumber_wAchive (SCANumber)
									VALUES(@SCANumber)
							 			 
								END	
			END
			
		IF(@VendorCircuit    <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#VendorCircuit_wAchive  ', N'U') IS NOT NULL
					DROP TABLE #VendorCircuit_wAchive  
					CREATE TABLE #VendorCircuit_wAchive
						(RowID INT Identity(1,1), VendorCircuit Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #VendorCircuit_wAchive (VendorCircuit)
									VALUES(@VendorCircuit)
							 			 
								END	
			END
			
		IF(@ProductType    <> '' AND @ProductType    <> '0' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#ProductType_wAchive  ', N'U') IS NOT NULL
					DROP TABLE #ProductType_wAchive
					CREATE TABLE #ProductType_wAchive
						(RowID INT Identity(1,1), ProductType Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #ProductType_wAchive(ProductType)
									VALUES(@ProductType)
							 			 
								END	
			END
			
			IF(@NUA   <> '' )
			
			BEGIN
				IF OBJECT_ID(N'tempdb..#NUA_wAchive ', N'U') IS NOT NULL
					DROP TABLE #NUA_wAchive 
					CREATE TABLE #NUA_wAchive
						(RowID INT Identity(1,1), NUA Varchar(40))
					 
					 			BEGIN
								   INSERT INTO #NUA_wAchive (NUA)
									VALUES(@NUA)
							 			 
								END	
			END
			
 		IF OBJECT_ID(N'tempdb..#OrderTmp_wAchive', N'U') IS NOT NULL
					DROP TABLE #OrderTmp_wAchive
					CREATE TABLE #OrderTmp_wAchive 
					(
						H1 INT,
						OrderID INT,
						CTRY_NME Varchar(50),
						PRE_XST_PL_NBR Varchar(100),
						H5 Varchar(50),
						GCS_PRICE_QOT_NBR Varchar(100),
						ORDR_ACTN_DES Varchar(50),
						SCA_NBR  Varchar(50),
						QOT_NBR Varchar(50),
						VNDR_CKT_ID Varchar(50), 
						PROD_TYPE_DES Varchar(50),
						PLTFRM_NME Varchar(50),
						ORDR_SUB_TYPE_CD Varchar(50),
						CTRY_CD Varchar(50),
						H5_H6_CUST_ID Varchar(50),
						ORDR_TYPE_DES Varchar(50),
						TSUP_PRS_QOT_NBR Varchar(50),
						CXR_NME  Varchar(50),
						TRGT_DLVRY_DT SmallDateTime,
						SOI Varchar(50)
					)					 

  		IF OBJECT_ID(N'tempdb..#OrderList_wAchive', N'U') IS NOT NULL
					DROP TABLE #OrderList_wAchive
					CREATE TABLE #OrderList_wAchive
					(
						OrderID Int,	
						FTN Int,
						H1 Int,
						CountryName Varchar(50),
						PrivateLine Varchar(50),
			  
						CustomerName Varchar(100),
						GCSQuoteID Varchar(100),
						CCD DateTime,			 
						OrderType Varchar(50),
						SCANumber Varchar(50),
						VendorQuote Varchar(50),
						VendorCircuitID Varchar(50),
						ProductType Varchar(50),
						PlatformType Varchar(50),
						OrderSubType Varchar(50),			
						OrderTypeDesc Varchar(50),			 
						[H5/H6] Varchar(100),
						TargerDeliveryDate SmallDateTime,
						DomesticFlag Varchar(50),
						PRSQuote Varchar(50),
						SOI Varchar(50),
						VendorName Varchar(50),
						Region Varchar(100),
						WorkGroup Varchar(50),
						CurrentTask Varchar(50),
						OrderStatus Varchar(50),
						WGID INT,
						TASKID INT,
						CAT_ID INT,
						CSG_LVL_CD INT,
						SCURD_CD Bit,
						H5_FOLDR_ID INT,
						NUA  Varchar(100)	 				 		
					)
	  
		SET @SQLStr2 = ' INSERT INTO #OrderList_wAchive'
		
		SET @SQLStr =	'SELECT  DISTINCT TOP 500 
							lo.ORDR_ID,
							fsa.FTN,
							foc.CUST_ID										AS H1,
							NULL											AS [Country Name],
							NULL											AS [Private Line ], 
							Case	lo.SCURD_CD
								When	1	Then	''Private Customer''
								Else	CASE lo.DMSTC_CD
											WHEN 1 THEN dbo.decryptBinaryData(foc5.CUST_NME)
											ELSE dbo.decryptBinaryData(foc6.CUST_NME)
											END
								End	AS CUST_NME,
							NULL											AS [GCS Quote ID ],
							ISNULL(lo.CUST_CMMT_DT, fsa.CUST_CMMT_DT),
							loa.ORDR_ACTN_DES,
							NULL											AS [SCA Number],
							NULL											AS [Vendor Quote ID],
							NULL											AS [Vendor Circuit ID],
							lpt.PROD_TYPE_DES								AS [Product Type],
							lp.PLTFRM_NME									AS [Platform Type],
							fsa.ORDR_SUB_TYPE_CD,
							CASE 
								WHEN lpprt.ORDR_TYPE_ID = 8 AND fsa.ORDR_TYPE_CD != ''CN'' THEN lot.ORDR_TYPE_DES + '' Cancel''
								ELSE lot.ORDR_TYPE_DES
								END AS	OrderTypeDesc,
							lo.H5_H6_CUST_ID,
							NULL											AS TRGT_DLVRY_DT,
							CASE lo.DMSTC_CD 
								WHEN 0 THEN '' Domestic''  
								WHEN 1 THEN '' International'' 
							END												AS DomesticFlag,
							ISNULL(fsa.TSUP_PRS_QOT_NBR, '''')				AS [PRS Quote Number],
							CASE
								WHEN foc5.SOI_CD <> ''''         THEN foc5.SOI_CD
								WHEN foc6.SOI_CD <> ''''         THEN foc6.SOI_CD
								ELSE NULL
							END                                         	AS SOI, 
							--web.getVendorName(lo.ORDR_ID) AS [Vendor Name],  
							CASE lo.PLTFRM_CD
									WHEN ''SF''	THEN ISNULL(sf.VNDR_NME,ISNULL(rsf.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									WHEN ''CP''	THEN ISNULL(cp.VNDR_NME,ISNULL(rcp.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									WHEN ''RS''	THEN ISNULL(rs.VNDR_NME,ISNULL(rrs.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									ELSE ''''
									END AS [Vendor Name],
							CASE 
								WHEN lo.DMSTC_CD = 1 THEN xr.RGN_DES
								WHEN lo.DMSTC_CD = 0 THEN ''''
							END												AS Region,
							CASE 
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 1 THEN ''ENCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 1 THEN ''ANCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 THEN ISNULL(lg.GRP_NME,0)
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS GRP_NME,
							ISNULL(lt.TASK_NME, '''')						AS TASK_NME,
							Case	ISNULL(lt.TASK_NME, '''') 
								When	''Bill Activated''	Then	ts.ORDR_STUS_DES
								When	''''				Then	ts.ORDR_STUS_DES
								Else	ls.STUS_DES 
							End												AS STUS_DES, 							
							CASE 
								WHEN lg.GRP_ID in (5,6,7)		AND lo.RGN_ID = 1	THEN 5
								WHEN lg.GRP_ID in (5,6,7)		AND lo.RGN_ID = 3	THEN 6
								WHEN lg.GRP_ID in (5,6,7)		AND lo.RGN_ID = 2	THEN 7
								WHEN lg.GRP_ID in (5,6,7)		AND IsNull(lo.RGN_ID,0) = 0 THEN 5
								WHEN lg.GRP_ID NOT IN (5,6,7)	AND IsNull(lg.GRP_ID,0) <> 0 THEN lg.GRP_ID
								WHEN lg.GRP_ID NOT IN (5,6,7)	AND lg.GRP_ID <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS  GRP_ID,
							lt.TASK_ID,
							lo.ORDR_CAT_ID, 
							lo.CSG_LVL_CD, 
							lo.SCURD_CD, 
							lo.H5_FOLDR_ID,
							nsi.NUA_ADR                                      AS NUA 
						FROM			dbo.vORDR			lo	WITH (NOLOCK) 
							INNER JOIN	dbo.vFSA_ORDR		fsa WITH (NOLOCK) ON	lo.ORDR_ID =	fsa.ORDR_ID
							INNER JOIN  dbo.LK_ORDR_ACTN	loa WITH (NOLOCK) ON	fsa.ORDR_ACTN_ID =	loa.ORDR_ACTN_ID
							INNER JOIN	dbo.LK_ORDR_TYPE	lot WITH (NOLOCK) ON	fsa.ORDR_TYPE_CD =	lot.FSA_ORDR_TYPE_CD 
							INNER JOIN	dbo.LK_XNCI_RGN		xr	WITH (NOLOCK) ON	ISNULL(lo.RGN_ID, 1) =	xr.RGN_ID
							INNER JOIN	dbo.LK_ORDR_STUS	ts	WITH (NOLOCK) ON 	lo.ORDR_STUS_ID	=	ts.ORDR_STUS_ID
							INNER JOIN  dbo.LK_PROD_TYPE    lpt WITH (NOLOCK) ON	fsa.PROD_TYPE_CD =	lpt.FSA_PROD_TYPE_CD
							LEFT JOIN   dbo.LK_PLTFRM		lp  WITH (NOLOCK) ON	lo.PLTFRM_CD =	lp.PLTFRM_CD
							LEFT JOIN	dbo.vFSA_ORDR_CUST	foc WITH (NOLOCK) ON	lo.ORDR_ID =	foc.ORDR_ID
																				AND	foc.CIS_LVL_TYPE		=	''H1''
							LEFT JOIN	dbo.vFSA_ORDR_CUST	foc5 WITH (NOLOCK) ON	lo.ORDR_ID =	foc5.ORDR_ID
																				AND	foc5.CIS_LVL_TYPE		=	''H5''
							LEFT JOIN	dbo.vFSA_ORDR_CUST	foc6 WITH (NOLOCK) ON	lo.ORDR_ID =	foc6.ORDR_ID
																				AND	foc6.CIS_LVL_TYPE		=	''H6''
							LEFT JOIN	dbo.vACT_TASK		at	WITH (NOLOCK) ON	lo.ORDR_ID				=	at.ORDR_ID 
																				AND	at.STUS_ID				IN	(0, 3)
							LEFT JOIN	LK_TASK				lt WITH (NOLOCK) ON lt.TASK_ID				=	at.TASK_ID 
							LEFT JOIN	dbo.MAP_GRP_TASK	gt WITH (NOLOCK) ON gt.TASK_ID				=	at.TASK_ID
							LEFT JOIN	dbo.LK_GRP			lg WITH (NOLOCK) ON gt.GRP_ID				=	lg.GRP_ID
							LEFT JOIN	dbo.LK_STUS			ls WITH (NOLOCK) ON at.STUS_ID				=	ls.STUS_ID
							LEFT JOIN	dbo.vCKT				ct WITH (NOLOCK) ON ct.ORDR_ID				=	lo.ORDR_ID
							LEFT JOIN   dbo.vNRM_CKT			nc WITH (NOLOCK) ON nc.FTN = fsa.FTN
							LEFT JOIN   dbo.vNRM_SRVC_INSTC			nsi WITH (NOLOCK) ON nsi.FTN = fsa.FTN
							LEFT JOIN	dbo.LK_PPRT			lpprt	WITH (NOLOCK)	ON	lpprt.PPRT_ID			=	lo.PPRT_ID
							LEFT JOIN	#OrderTmp_wAchive			lca WITH (NOLOCK)  ON lca.OrderID = lo.ORDR_ID
							LEFT JOIN	dbo.vFSA_ORDR_GOM_XNCI fogx WITH (NOLOCK) ON		fogx.ORDR_ID	=	lo.ORDR_ID
																					AND	fogx.GRP_ID		=	1
							LEFT JOIN	dbo.LK_VNDR		sf	WITH (NOLOCK)			ON	sf.VNDR_CD		=	fsa.CXR_ACCS_CD
							LEFT JOIN	dbo.LK_VNDR		cp	WITH (NOLOCK)			ON	cp.VNDR_CD		=	fsa.VNDR_VPN_CD	
							LEFT JOIN	dbo.LK_VNDR		rs	WITH (NOLOCK)			ON	rs.VNDR_CD		=	fsa.INSTL_VNDR_CD																
							LEFT JOIN	dbo.vFSA_ORDR	rfs	WITH (NOLOCK)			ON	rfs.FTN		=	fsa.RELTD_FTN 
							LEFT JOIN	dbo.LK_VNDR		rsf	WITH (NOLOCK)			ON	rsf.VNDR_CD		=	rfs.CXR_ACCS_CD
							LEFT JOIN	dbo.LK_VNDR		rcp	WITH (NOLOCK)			ON	rcp.VNDR_CD		=	rfs.VNDR_VPN_CD	
							LEFT JOIN	dbo.LK_VNDR		rrs	WITH (NOLOCK)			ON	rrs.VNDR_CD		=	rfs.INSTL_VNDR_CD
							LEFT JOIN	dbo.vVNDR_ORDR	vo	WITH (NOLOCK)			ON	vo.ORDR_ID		=	lo.ORDR_ID
							LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)			ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
							LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)			ON	lvo.VNDR_CD = vf.VNDR_CD
						WHERE	lo.ORDR_STUS_ID <> 6	'
					
					
	 SET @SQLStr1 =		'SELECT  DISTINCT TOP 500 
							lo.ORDR_ID,
							lo.ORDR_ID										AS FTN,
							NULL											AS H1,
							lc.CTRY_NME										AS [Country Name],
							ipl.PRE_XST_PL_NBR								AS [Private Line ],
							Case	lo.SCURD_CD
								When	1	Then	''Private Customer''
								Else	dbo.decryptBinaryData(hf.CUST_NME)
							End	AS CUST_NME,
							ipl.GCS_PRICE_QOT_NBR							AS [GCS Quote ID ],
							ISNULL(lo.CUST_CMMT_DT, ipl.CUST_CMMT_DT),
							NULL											AS ORDR_ACTN_DES,
							ipl.SCA_NBR										AS [SCA Number],
							NULL											AS [Vendor Quote ID],
							NULL											AS [Vendor Circuit ID], 
							pt.PROD_TYPE_DES								AS [Product Type],
							pl.PLTFRM_NME									AS [Platform Type],
							NULL											AS ORDR_SUB_TYPE_CD,
							CASE ipl.ORDR_TYPE_ID
								WHEN 8 THEN lt2.ORDR_TYPE_DES + '' '' + lot.ORDR_TYPE_DES
								ELSE lot.ORDR_TYPE_DES
							END												AS ORDR_TYPE_DES,
							hf.CUST_ID										AS H5_H6_CUST_ID,
							cm.TRGT_DLVRY_DT,
							CASE lo.DMSTC_CD 
								WHEN 0 THEN '' Domestic''    
								WHEN 1 THEN ''International'' 
							END												AS DomesticFlag,
							NULL											AS [PRS Quote Number],
							ipl.SOI											AS SOI,
							--web.getVendorName1(lo.ORDR_ID)					AS [Vendor Name],
							ISNULL(lv.CXR_NME,ISNULL(lvo.VNDR_NME,'''')) AS [Vendor Name],
							CASE 
								WHEN lo.DMSTC_CD = 1 THEN xr.RGN_DES
								WHEN lo.DMSTC_CD = 0 THEN ''''
							END												AS Region,
							CASE 
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 1 THEN ''ENCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 1 THEN ''ANCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 THEN ISNULL(lg.GRP_NME,0)
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS GRP_NME,
							ISNULL(lt.TASK_NME, '''')						AS TASK_NME,
							Case	ISNULL(lt.TASK_NME, '''')
								When	''Bill Activated''	Then	ts.ORDR_STUS_DES
								When	''''				Then	ts.ORDR_STUS_DES
								Else	ls.STUS_DES 
							End												AS STUS_DES, 		
							CASE 
								WHEN lg.GRP_ID in (5,6,7) and lo.RGN_ID = 1 THEN 5
								WHEN lg.GRP_ID in (5,6,7) and lo.RGN_ID = 3 THEN 6
								WHEN lg.GRP_ID in (5,6,7)and lo.RGN_ID = 2 THEN 7
								WHEN lg.GRP_ID in (5,6,7)and IsNull(lo.RGN_ID,0) = 0 THEN 5
								WHEN lg.GRP_ID NOT IN (5,6,7) and IsNull(lg.GRP_ID,0) <> 0 THEN lg.GRP_ID
								WHEN lg.GRP_ID NOT IN (5,6,7) and lg.GRP_ID <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS  GRP_ID,
							lt.TASK_ID,
							lo.ORDR_CAT_ID, 
							lo.CSG_LVL_CD, 
							lo.SCURD_CD,  
							lo.H5_FOLDR_ID,
							ISNULL(nsi.NUA_ADR,'''')                         AS NUA
						FROM			ORDR					lo WITH (NOLOCK) 
							INNER JOIN	dbo.vIPL_ORDR				ipl WITH (NOLOCK)	ON	lo.ORDR_ID				=	ipl.ORDR_ID
							INNER JOIN	LK_PROD_TYPE			pt WITH (NOLOCK)	ON	ipl.PROD_TYPE_ID =	pt.PROD_TYPE_ID
							INNER JOIN	dbo.vH5_FOLDR				hf WITH (NOLOCK)	ON	lo.H5_FOLDR_ID =	hf.H5_FOLDR_ID
							INNER JOIN	dbo.LK_ORDR_TYPE		lot	WITH (NOLOCK)	ON	ipl.ORDR_TYPE_ID =	lot.ORDR_TYPE_ID
							LEFT JOIN	dbo.LK_ORDR_TYPE		lt2 WITH (NOLOCK)	ON	ipl.PREV_ORDR_TYPE_ID = lt2.ORDR_TYPE_ID
							LEFT JOIN	dbo.vCKT						ct	WITH (NOLOCK)	ON	lo.ORDR_ID =	ct.ORDR_ID 
							LEFT JOIN	dbo.vCKT_COST				ctc	WITH (NOLOCK)	ON	ct.CKT_ID =	ctc.CKT_ID
							LEFT JOIN	dbo.vCKT_MS					cm	WITH (NOLOCK)	ON	ct.CKT_ID =	cm.CKT_ID
							LEFT JOIN	LK_PLTFRM				pl	WITH (NOLOCK)	ON	lo.PLTFRM_CD =	pl.PLTFRM_CD
							LEFT JOIN	dbo.vORDR_ADR				oa	WITH (NOLOCK)	ON	lo.ORDR_ID =	oa.ORDR_ID
																						AND	oa.ADR_TYPE_ID =	11
							LEFT JOIN	LK_CTRY					lc	WITH (NOLOCK)	ON	oa.CTRY_CD =	lc.CTRY_CD  
							INNER JOIN	dbo.LK_XNCI_RGN			xr	WITH (NOLOCK)	ON	ISNULL(lo.RGN_ID, 1) =	xr.RGN_ID
							INNER JOIN	dbo.LK_ORDR_STUS		ts	WITH (NOLOCK)	ON 	lo.ORDR_STUS_ID	=	ts.ORDR_STUS_ID
							LEFT JOIN	dbo.vACT_TASK			at	WITH (NOLOCK)	ON	lo.ORDR_ID	=	at.ORDR_ID 
																						AND	at.STUS_ID				IN	(0, 3)
							LEFT JOIN	LK_TASK					lt	WITH (NOLOCK)	ON lt.TASK_ID = at.TASK_ID 
							LEFT JOIN	dbo.MAP_GRP_TASK		gt	WITH (NOLOCK)	ON gt.TASK_ID = at.TASK_ID
							LEFT JOIN	dbo.LK_GRP				lg	WITH (NOLOCK)	ON gt.GRP_ID = lg.GRP_ID
							LEFT JOIN	dbo.LK_STUS				ls	WITH (NOLOCK)	ON at.STUS_ID = ls.STUS_ID
							LEFT JOIN   dbo.vNRM_CKT            nc  WITH (NOLOCK)   on nc.FTN = lo.ORDR_ID
							LEFT JOIN dbo.vNRM_SRVC_INSTC		nsi WITH (NOLOCK) ON nsi.FTN = lo.ORDR_ID
							LEFT JOIN	#OrderTmp_wAchive				lca WITH (NOLOCK)	 ON lca.OrderID = lo.ORDR_ID
							LEFT JOIN	dbo.vFSA_ORDR_GOM_XNCI fogx WITH (NOLOCK) ON		fogx.ORDR_ID	=	lo.ORDR_ID
																					AND	fogx.GRP_ID		=	1
							LEFT JOIN	dbo.vIPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	lo.ORDR_ID		=	la.ORDR_ID
																AND	la.ACCS_TYPE_ID	=	''Terminating''
							LEFT JOIN	dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD	
							LEFT JOIN	dbo.vVNDR_ORDR	vo	WITH (NOLOCK)				ON	vo.ORDR_ID		=	lo.ORDR_ID
							LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)				ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
							LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)				ON	lvo.VNDR_CD = vf.VNDR_CD
							WHERE (lo.ORDR_CAT_ID IN (1,5)) AND ipl.ORDR_STUS_ID <> 20 '
							
		SET @SQLStr3 = ' SELECT  DISTINCT TOP 500 
							lo.ORDR_ID,
							lo.ORDR_ID										AS FTN,
							0												AS H1,
							lc.CTRY_NME										AS [Country Name],
							ISNULL(nco.PL_NBR,'''')								AS [Private Line ],
							Case	lo.SCURD_CD
								When	1	Then	''Private Customer''
								Else	dbo.decryptBinaryData(nco.CUST_NME)
							End	AS CUST_NME,
							''''											AS [GCS Quote ID ],
							ISNULL(lo.CUST_CMMT_DT, ISNULL(nco.CCS_DT,'''')),
							''Submit''										AS ORDR_ACTN_DES,
							''''											AS [SCA Number],
							NULL											AS [Vendor Quote ID],
							NULL											AS [Vendor Circuit ID], 
							pt.PROD_TYPE_DES								AS [Product Type],
							pl.PLTFRM_NME									AS [Platform Type],
							NULL											AS ORDR_SUB_TYPE_CD,
							lot.ORDR_TYPE_DES								AS ORDR_TYPE_DES,
							hf.CUST_ID										AS H5_H6_CUST_ID,
							'''',
							CASE lo.DMSTC_CD 
								WHEN 0 THEN '' Domestic''    
								WHEN 1 THEN ''International'' 
							END												AS DomesticFlag,
							NULL											AS [PRS Quote Number],
							''''											AS SOI,
							--web.getVendorName1(lo.ORDR_ID)					AS [Vendor Name],
							ISNULL(lv.VNDR_NME,ISNULL(lvo.VNDR_NME,'''')) AS [Vendor Name],
							CASE 
								WHEN lo.DMSTC_CD = 1 THEN xr.RGN_DES
								WHEN lo.DMSTC_CD = 0 THEN ''''
							END												AS Region,
							CASE 
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 1 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 1 THEN ''ENCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 2 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 1 THEN ''ANCI''
								WHEN gt.GRP_ID IN (5,6,7) AND lo.RGN_ID = 3 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 1 THEN ''AMNCI''
								WHEN gt.GRP_ID IN (5,6,7) AND IsNull(lo.RGN_ID,0) = 0 AND lo.DMSTC_CD = 0 THEN ''System''
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 THEN ISNULL(lg.GRP_NME,0)
								WHEN gt.GRP_ID NOT IN (5,6,7) AND IsNull(gt.GRP_ID,0) <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS GRP_NME,
							ISNULL(lt.TASK_NME, '''')						AS TASK_NME,
							Case	ISNULL(lt.TASK_NME, '''')
								When	''Bill Activated''	Then	ts.ORDR_STUS_DES
								When	''''				Then	ts.ORDR_STUS_DES
								Else	ls.STUS_DES 
							End												AS STUS_DES, 		
							CASE 
								WHEN lg.GRP_ID in (5,6,7) and lo.RGN_ID = 1 THEN 5
								WHEN lg.GRP_ID in (5,6,7) and lo.RGN_ID = 3 THEN 6
								WHEN lg.GRP_ID in (5,6,7)and lo.RGN_ID = 2 THEN 7
								WHEN lg.GRP_ID in (5,6,7)and IsNull(lo.RGN_ID,0) = 0 THEN 5
								WHEN lg.GRP_ID NOT IN (5,6,7) and IsNull(lg.GRP_ID,0) <> 0 THEN lg.GRP_ID
								WHEN lg.GRP_ID NOT IN (5,6,7) and lg.GRP_ID <> 0 and IsNull(at.TASK_ID,0) = 0 THEN ''''
							END												AS  GRP_ID,
							lt.TASK_ID,
							lo.ORDR_CAT_ID, 
							lo.CSG_LVL_CD, 
							lo.SCURD_CD,  
							lo.H5_FOLDR_ID,
							ISNULL(nsi.NUA_ADR,'''')                         AS NUA
						FROM			vORDR					lo	WITH (NOLOCK) 
							INNER JOIN	dbo.vNCCO_ORDR				nco WITH (NOLOCK)	ON	lo.ORDR_ID				=	nco.ORDR_ID
							INNER JOIN	LK_PROD_TYPE			pt	WITH (NOLOCK)	ON	nco.PROD_TYPE_ID =	pt.PROD_TYPE_ID
							LEFT JOIN	dbo.vH5_FOLDR				hf	WITH (NOLOCK)	ON	lo.H5_FOLDR_ID =	hf.H5_FOLDR_ID
							INNER JOIN	dbo.LK_ORDR_TYPE		lot	WITH (NOLOCK)	ON	nco.ORDR_TYPE_ID =	lot.ORDR_TYPE_ID
							LEFT JOIN	LK_PLTFRM				pl	WITH (NOLOCK)	ON	lo.PLTFRM_CD =	pl.PLTFRM_CD
							LEFT JOIN	LK_CTRY					lc	WITH (NOLOCK)	ON	nco.CTRY_CD =	lc.CTRY_CD  
							INNER JOIN	dbo.LK_XNCI_RGN			xr	WITH (NOLOCK)	ON	ISNULL(lo.RGN_ID, 1) =	xr.RGN_ID
							INNER JOIN	dbo.LK_ORDR_STUS		ts	WITH (NOLOCK)	ON 	lo.ORDR_STUS_ID	=	ts.ORDR_STUS_ID
							LEFT JOIN	dbo.vACT_TASK			at	WITH (NOLOCK)	ON	lo.ORDR_ID	=	at.ORDR_ID 
																						AND	at.STUS_ID				IN	(0, 3)
							LEFT JOIN	LK_TASK					lt	WITH (NOLOCK)	ON lt.TASK_ID = at.TASK_ID 
							LEFT JOIN	dbo.MAP_GRP_TASK		gt	WITH (NOLOCK)	ON gt.TASK_ID = at.TASK_ID
							LEFT JOIN	dbo.LK_GRP				lg	WITH (NOLOCK)	ON gt.GRP_ID = lg.GRP_ID
							LEFT JOIN	dbo.LK_STUS				ls	WITH (NOLOCK)	ON at.STUS_ID = ls.STUS_ID
							LEFT JOIN   dbo.vNRM_CKT             nc  WITH (NOLOCK)   on nc.FTN = lo.ORDR_ID
							LEFT JOIN	dbo.vNRM_SRVC_INSTC		nsi WITH (NOLOCK)	ON nsi.FTN = lo.ORDR_ID
							LEFT JOIN	#OrderTmp_wAchive				lca WITH (NOLOCK)	 ON lca.OrderID = lo.ORDR_ID
							LEFT JOIN	dbo.vFSA_ORDR_GOM_XNCI fogx WITH (NOLOCK) ON		fogx.ORDR_ID	=	lo.ORDR_ID
																					AND	fogx.GRP_ID		=	1
							LEFT JOIN	dbo.LK_VNDR		lv	 WITH (NOLOCK)			ON	nco.VNDR_CD	=	lv.VNDR_CD																	
							LEFT JOIN	dbo.vVNDR_ORDR	vo	WITH (NOLOCK)				ON	vo.ORDR_ID		=	lo.ORDR_ID
							LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)				ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
							LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)				ON	lvo.VNDR_CD = vf.VNDR_CD														
							WHERE lo.ORDR_CAT_ID = 4 '						
	   
		IF ((@FTNs <>  '') OR (@ParentFTN <> '') OR ((@RelFTN <> '') AND (@RelFTN = 'TRUE'))) 
			BEGIN   
				SET @SQLStr  =  @SQLStr + 'AND fsa.FTN IN ( SELECT DISTINCT FTN FROM #FTNs_wAchive WITH (NOLOCK))'
				SET @SQLStr1  =  @SQLStr1 + 'AND lo.ORDR_ID IN ( SELECT DISTINCT FTN FROM #FTNs_wAchive WITH (NOLOCK))'
				SET @SQLStr3  =  @SQLStr3 + 'AND lo.ORDR_ID IN ( SELECT DISTINCT FTN FROM #FTNs_wAchive WITH (NOLOCK))'
			END  		
		
		IF (@Names <>  '')
			BEGIN
				SET @SQLStr  = @SQLStr + 'AND dbo.decryptBinaryData(foc.CUST_NME) IN ( SELECT DISTINCT Name FROM #Names_wAchive WITH (NOLOCK))'
				SET @SQLStr1  = @SQLStr1 + 'AND dbo.decryptBinaryData(hf.CUST_NME) IN ( SELECT DISTINCT Name FROM #Names_wAchive WITH (NOLOCK))'
				SET @SQLStr3  = @SQLStr3 + 'AND dbo.decryptBinaryData(nco.CUST_NME) IN ( SELECT DISTINCT Name FROM #Names_wAchive WITH (NOLOCK))'
			END

		IF (@H5names <>  '')
			BEGIN
				SET @SQLStr  = @SQLStr + 'AND (dbo.decryptBinaryData(foc5.CUST_NME) IN ( SELECT DISTINCT Name FROM #H5names_wAchive WITH (NOLOCK)) OR
											dbo.decryptBinaryData(foc6.CUST_NME) IN ( SELECT DISTINCT Name FROM #H5names_wAchive WITH (NOLOCK)))'
				SET @SQLStr1  = @SQLStr1 + 'AND dbo.decryptBinaryData(hf.CUST_NME) IN ( SELECT DISTINCT Name FROM #H5names_wAchive WITH (NOLOCK))'
				SET @SQLStr3  = @SQLStr3 + 'AND dbo.decryptBinaryData(nco.CUST_NME) IN ( SELECT DISTINCT Name FROM #H5names_wAchive WITH (NOLOCK))'
			END

		IF(@OrderSubType <> ''AND @OrderSubType <>'0')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND fsa.ORDR_SUB_TYPE_CD in ( SELECT DISTINCT OrderSubType  FROM #OrderSubType_wAchive WITH (NOLOCK))'
				SET @SQLStr1 = @SQLStr1 + ' AND lca.ORDR_SUB_TYPE_CD in ( SELECT DISTINCT OrderSubType  FROM #OrderSubType_wAchive WITH (NOLOCK))'
				SET @SQLStr3 = @SQLStr3 + ' AND lca.ORDR_SUB_TYPE_CD in ( SELECT DISTINCT OrderSubType  FROM #OrderSubType_wAchive WITH (NOLOCK))'
			END
		
		IF ((Select Count(1) From #CCDOrders) > 0 )
			BEGIN
				SET @SQLStr  =  @SQLStr + ' AND fsa.ORDR_ID IN (SELECT OrderID FROM #CCDOrders WITH (NOLOCK))' 
				SET @SQLStr1  =  @SQLStr1 + ' AND ipl.ORDR_ID IN (SELECT OrderID FROM #CCDOrders WITH (NOLOCK))' 
				SET @SQLStr3  =  @SQLStr3 + ' AND nco.ORDR_ID IN (SELECT OrderID FROM #CCDOrders WITH (NOLOCK))'
			END

		IF(  @OrderTypes <>'0' AND  @OrderTypes <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lot.ORDR_TYPE_ID in (SELECT  OrderTyp from #OrderTypes_wAchive WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (lot.ORDR_TYPE_ID in (SELECT  iplOrderTyp from #iplOrderTypes_wAchive WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lot.ORDR_TYPE_ID in (SELECT  iplOrderTyp from #iplOrderTypes_wAchive WITH (NOLOCK)))'
			END
		
		IF(@H5_H6 <> '' )
			BEGIN
				SET @SQLStr  =  @SQLStr + 'AND lo.H5_H6_CUST_ID in( SELECT DISTINCT H5_H6  from #H5_H6_wAchive WITH (NOLOCK))'
				SET @SQLStr1  =  @SQLStr1 + 'AND (lo.H5_H6_CUST_ID in( SELECT DISTINCT H5_H6  from #H5_H6_wAchive WITH (NOLOCK)) OR (hf.CUST_ID in( SELECT DISTINCT H5_H6  from #H5_H6_wAchive WITH (NOLOCK))))'
				SET @SQLStr3  =  @SQLStr3 + 'AND lo.H5_H6_CUST_ID in( SELECT DISTINCT H5_H6  from #H5_H6_wAchive WITH (NOLOCK))'
			END
		
		IF(  @H1 <> '' )
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (foc.CUST_ID in (SELECT DISTINCT H1 from #TmpH1_wAchive WITH (NOLOCK)) )'
				SET @SQLStr1 = @SQLStr1 + ' AND (lca.H1 in (SELECT DISTINCT H1 from #TmpH1_wAchive WITH (NOLOCK)) )'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.H1 in (SELECT DISTINCT H1 from #TmpH1_wAchive WITH (NOLOCK)) )'
			END
		
		IF (@SOI <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (ISNULL(foc5.SOI_CD,foc6.SOI_CD) in (SELECT SOI from #SOI_wAchive WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.SOI in ( SELECT SOI from #SOI_wAchive WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.SOI in ( SELECT SOI from #SOI_wAchive WITH (NOLOCK)))'
			END
		
		IF (@PRSQuote <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (fsa.TSUP_PRS_QOT_NBR in ( SELECT PRS from #PRS_wAchive WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (lca.TSUP_PRS_QOT_NBR in ( SELECT PRS from #PRS_wAchive WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.TSUP_PRS_QOT_NBR in ( SELECT PRS from #PRS_wAchive WITH (NOLOCK)))'
			END
		
		IF(ISNULL (@WorkGroup, 0) <> '0' AND @WorkGroup <>'')
		
				BEGIN
				IF @WorkGroup in (5,6,7)
					BEGIN
						SET @SQLStr = @SQLStr + ' AND ( xr.GRP_ID = '' ' + @WorkGroup + ''' )'
												+ ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
						SET @SQLStr1 = @SQLStr1 + ' AND ( xr.GRP_ID = '' ' + @WorkGroup + ''' )'
												+ ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
						SET @SQLStr3 = @SQLStr3 + ' AND ( xr.GRP_ID = '' ' + @WorkGroup + ''' )'
												+ ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'						
					END
				ELSE
					BEGIN
						SET @SQLStr = @SQLStr + ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
						SET @SQLStr1 = @SQLStr1 + ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
						SET @SQLStr3 = @SQLStr3 + ' AND ( lg.GRP_ID = '' ' + @WorkGroup + ''' )'
					END
			END
			
		IF (@PrivateLine  <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (nc.PLN_NME   in ( SELECT PrivateLine from #PrivateLine_wAchive WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.PRE_XST_PL_NBR in ( SELECT PrivateLine from #PrivateLine_wAchive WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (nco.PL_NBR in ( SELECT PrivateLine from #PrivateLine_wAchive WITH (NOLOCK)))'
			END
			
 		IF (@GCSQuoteID   <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lca.GCS_PRICE_QOT_NBR  in ( SELECT GCSQuoteID from #GCSQuoteID_wAchive WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.GCS_PRICE_QOT_NBR in ( SELECT GCSQuoteID from #GCSQuoteID_wAchive WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.GCS_PRICE_QOT_NBR in ( SELECT GCSQuoteID from #GCSQuoteID_wAchive WITH (NOLOCK)))'
			END
			
		IF (@VendorQuoteID    <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lca.QOT_NBR  in ( SELECT VendorQuoteID from #VendorQuoteID_wAchive WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ctc.QOT_NBR in ( SELECT VendorQuoteID from #VendorQuoteID_wAchive WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.QOT_NBR in ( SELECT VendorQuoteID from #VendorQuoteID_wAchive WITH (NOLOCK)))'
			END
			
		IF (@SCANumber	    <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lca.SCA_NBR  in ( SELECT SCANumber from #SCANumber_wAchive WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.SCA_NBR  in ( SELECT SCANumber from #SCANumber_wAchive WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.SCA_NBR  in ( SELECT SCANumber from #SCANumber_wAchive WITH (NOLOCK)))'
			END
	 
 		IF (@VendorCircuit     <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (ct.VNDR_CKT_ID in ( SELECT VendorCircuit from #VendorCircuit_wAchive WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ct.VNDR_CKT_ID in ( SELECT VendorCircuit from #VendorCircuit_wAchive WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lca.VNDR_CKT_ID in ( SELECT VendorCircuit from #VendorCircuit_wAchive WITH (NOLOCK)))'
			END
			   
 		IF (@VendorName     <> '')
			BEGIN
				SET @SQLStr = @SQLStr + ' AND CASE lo.PLTFRM_CD
									WHEN ''SF''	THEN ISNULL(sf.VNDR_NME,ISNULL(rsf.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									WHEN ''CP''	THEN ISNULL(cp.VNDR_NME,ISNULL(rcp.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									WHEN ''RS''	THEN ISNULL(rs.VNDR_NME,ISNULL(rrs.VNDR_NME,ISNULL(web.getVendorName(lo.ORDR_ID),lvo.VNDR_NME)))
									ELSE ''''
									END in ( SELECT VendorName  from #VendorName_wAchive  WITH (NOLOCK))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ISNULL(lv.CXR_NME,ISNULL(lvo.VNDR_NME,'''')) in ( SELECT VendorName  from #VendorName_wAchive  WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (ISNULL(lv.VNDR_NME,ISNULL(lvo.VNDR_NME,'''')) in ( SELECT VendorName  from #VendorName_wAchive  WITH (NOLOCK)))'
			END

		IF(@Region    <> '' AND @Region    <> '0' )
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lo.RGN_ID  = '' ' + @Region + ''')'
				SET @SQLStr1 = @SQLStr1 + ' AND (lo.RGN_ID  = '' ' + @Region + ''')'
				SET @SQLStr3 = @SQLStr3 + ' AND (lo.RGN_ID  = '' ' + @Region + ''')'
			END


			
		IF(@ProductType    <> '' AND @ProductType    <> '0' )
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (fsa.PROD_TYPE_CD IN (SELECT  FSA_PROD_TYPE_CD FROM dbo.LK_PROD_TYPE WITH (NOLOCK) WHERE PROD_TYPE_ID =  '' ' + @ProductType + '''  ))'
				SET @SQLStr1 = @SQLStr1 + ' AND (ipl.PROD_TYPE_ID  =   '' ' + @ProductType + ''' )'
				SET @SQLStr3 = @SQLStr3 + ' AND (nco.PROD_TYPE_ID  =   '' ' + @ProductType + ''' )'
			END
			
				
	 
		IF(@PlatformType    <> '' AND @PlatformType <> '0' )
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lo.PLTFRM_CD   = ''' + @PlatformType +''')'
				SET @SQLStr1 = @SQLStr1 + ' AND (lo.PLTFRM_CD    = ''' + @PlatformType+''')'
				SET @SQLStr3 = @SQLStr3 + ' AND (lo.PLTFRM_CD    = ''' + @PlatformType+''')'
			END
			
		IF(@CustomerName   <> '' )
		    
			BEGIN
				SET @SQLStr = @SQLStr + ' AND (lo.CustomerName in ( SELECT CustomerName  from #CustomerName_wAchive  WITH (NOLOCK)))'
				SET @SQLStr1 = @SQLStr1 + ' AND (lo.CustomerName in ( SELECT CustomerName  from #CustomerName_wAchive  WITH (NOLOCK)))'
				SET @SQLStr3 = @SQLStr3 + ' AND (lo.CustomerName in ( SELECT CustomerName  from #CustomerName_wAchive  WITH (NOLOCK)))'
			END

		IF @Status <> ''
					BEGIN
						IF (@Status = '1')
						BEGIN
							SET @SQLStr = @SQLStr + ' AND (lo.ORDR_STUS_ID in (2,5)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
							SET @SQLStr1 = @SQLStr1 + ' AND (lo.ORDR_STUS_ID in (2,5)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
							SET @SQLStr3 = @SQLStr3 + ' AND (lo.ORDR_STUS_ID in (2,5)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						END
						ELSE
						BEGIN
							SET @SQLStr = @SQLStr + ' AND (lo.ORDR_STUS_ID IN (0, 1)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
							SET @SQLStr1 = @SQLStr1 + ' AND (lo.ORDR_STUS_ID IN (0, 1)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
							SET @SQLStr3 = @SQLStr3 + ' AND (lo.ORDR_STUS_ID IN (0, 1)) AND (lo.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						END
					END
	  		IF (@NUA <> '')
				BEGIN
					SET @SQLStr = @SQLStr + ' AND ((nsi.NUA_ADR in (SELECT NUA from #NUA_wAchive WITH (NOLOCK))) OR (nc.NUA_ADR in (SELECT NUA from #NUA_wAchive WITH (NOLOCK))))'
					SET @SQLStr1 = @SQLStr1 + ' AND ((nsi.NUA_ADR in (SELECT NUA from #NUA_wAchive WITH (NOLOCK))) OR (nc.NUA_ADR in (SELECT NUA from #NUA_wAchive WITH (NOLOCK))))'
					SET @SQLStr3 = @SQLStr3 + ' AND ((nsi.NUA_ADR in (SELECT NUA from #NUA_wAchive WITH (NOLOCK))) OR (nc.NUA_ADR in (SELECT NUA from #NUA_wAchive WITH (NOLOCK))))'
				END 
		
		IF (@PRCH_ORDR_NBR <> '' OR  @ATLAS_WRK_ORDR_NBR <> '' OR @PLSFT_RQSTN_NBR	<> '')
		   BEGIN
				IF @PRCH_ORDR_NBR <> ''
					BEGIN
						SET @SQLStr = @SQLStr + ' AND (fogx.PRCH_ORDR_NBR   = ''' + @PRCH_ORDR_NBR +''')'
						SET @SQLStr1 = @SQLStr1 + ' AND (fogx.PRCH_ORDR_NBR   = ''' + @PRCH_ORDR_NBR +''')'
						SET @SQLStr3 = @SQLStr3 + ' AND (fogx.PRCH_ORDR_NBR   = ''' + @PRCH_ORDR_NBR +''')'
					END
					
				IF @ATLAS_WRK_ORDR_NBR <> ''
					BEGIN
						SET @SQLStr = @SQLStr + ' AND (fogx.ATLAS_WRK_ORDR_NBR   = ''' + @ATLAS_WRK_ORDR_NBR +''')'
						SET @SQLStr1 = @SQLStr1 + ' AND (fogx.ATLAS_WRK_ORDR_NBR   = ''' + @ATLAS_WRK_ORDR_NBR +''')'
						SET @SQLStr3 = @SQLStr3 + ' AND (fogx.ATLAS_WRK_ORDR_NBR   = ''' + @ATLAS_WRK_ORDR_NBR +''')'
					END
					
				IF @PLSFT_RQSTN_NBR <> ''
					BEGIN
						SET @SQLStr = @SQLStr + ' AND (fogx.PLSFT_RQSTN_NBR   = ''' + @PLSFT_RQSTN_NBR +''')'
						SET @SQLStr1 = @SQLStr1 + ' AND (fogx.PLSFT_RQSTN_NBR   = ''' + @PLSFT_RQSTN_NBR +''')'
						SET @SQLStr3 = @SQLStr3 + ' AND (fogx.PLSFT_RQSTN_NBR   = ''' + @PLSFT_RQSTN_NBR +''')'
					END
					
		   END
		   SET @sql_stmt = 'SELECT DISTINCT TOP 500  
						OrderID									AS ORDR_ID,
						FTN,
						H1,
						CountryName,
						PrivateLine,
						CustomerName,
						CCD,
						OrderType,  
						VendorCircuitID,
						ProductType,
						PlatformType,
						OrderSubType,
						[H5/H6],
						DomesticFlag,
						PRSQuote,
						SOI,
						VendorName,
						Region,
						OrderTypeDesc,
						WorkGroup,
						Case	CurrentTask
							When	''Bill Activated''	Then	OrderStatus 
							WHEN	''xNCI Milestones Complete'' THEN ''xNCI Milestones Complete''
							When	''''				Then	OrderStatus 
							Else	CurrentTask	+ ''-'' + OrderStatus
						End										AS	OrderStatus, 
						ISNULL(WGID,0)							AS WGID,
						ISNULL(TASKID,0)						AS TASK_ID,
						ISNULL(CAT_ID,0)						AS CAT_ID,
						ISNULL(CSG_LVL_CD,0)					AS CSG_LVL_CD,
						ISNULL(SCURD_CD, 0)						AS SCURD_CD,
						ISNULL(H5_FOLDR_ID,0)					AS H5_FOLDR_ID,
						ProductType								AS [PROD_TYPE],
						Case	CurrentTask
							When	''Bill Activated''	Then	OrderStatus 
							WHEN	''xNCI Milestones Complete'' THEN ''xNCI Milestones Complete''
							When	''''				Then	OrderStatus 
							Else	CurrentTask	+ ''-'' + OrderStatus
						End										AS [ORDR_STUS],
						OrderTypeDesc							AS [ORDR_TYPE],
						PlatformType							AS [PLTFRM_NME]
					FROM #orderlist_wAchive' 
	 
   
	END    
------------------------------------------------------------------------------------------------------------------------------	
-----------------------@Mode=1-END--------------------------------------------------------------------------------------------  
------------------------------------------------------------------------------------------------------------------------------	   
	   
	   
	
	 
 			 
 SET @Sort = ' CCD ASC'
 SET @Sort = 'ORDER BY' + @Sort 
 SET @sql_union = 'UNION ALL'
 
 
 
 SET @finalsql = @SQLStr2 + ' ' + @SQLStr + ' ' + @sql_union + '  ' + @SQLStr1 + ' ' + @sql_union + '  ' + @SQLStr3 + ' ' +  @sql_stmt + ' ' + @Sort


--print @finalsql

 EXEC sp_executesql @finalsql
 RETURN @@ROWCOUNT
 

		IF ((@@ROWCOUNT > 0) AND (@AdhocEmailChk = 'TRUE'))
			BEGIN
				DECLARE @EMAIL_LIST_TXT VARCHAR(500)
				DECLARE @AdhcRptID  INT
				DECLARE @EmailBody  VARCHAR(1000)
				DECLARE @ReqUsr VARCHAR(200)

				SELECT TOP 1 @EMAIL_LIST_TXT = PRMTR_VALU_TXT
				FROM dbo.LK_SYS_CFG WITH (NOLOCK)
				WHERE PRMTR_NME = 'AdhocRptNotify'
				ORDER BY CFG_ID DESC

				SELECT @ReqUsr = FULL_NME
				FROM dbo.LK_USER WITH (NOLOCK)
				WHERE [USER_ID] = @REQ_BY_USR_ID
				
				INSERT INTO dbo.ADHOC_RPT_ACTY WITH (ROWLOCK)
				(REQ_BY_USER_ID,
				 ADHOC_RPT_QUERY_TXT,
				 ADHOC_RPT_SCHEDULE)
				 VALUES (
						 @REQ_BY_USR_ID,
						 @finalsql,
						 CASE @RptSchedule WHEN '' THEN 'D' ELSE @RptSchedule END
						)

				SELECT @AdhcRptID = SCOPE_IDENTITY()

				SELECT @EmailBody = '<AdhocRpt><ID>'+CONVERT(VARCHAR,@AdhcRptID)+'</ID><ReqUsr>'+@ReqUsr+'</ReqUsr><CreatDt>'+CONVERT(VARCHAR,GETDATE(), 121)+'</CreatDt></AdhocRpt>'

				INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
				(EMAIL_REQ_TYPE_ID
				,STUS_ID
				,EMAIL_LIST_TXT
				,EMAIL_SUBJ_TXT
				,EMAIL_BODY_TXT)
		VALUES	(15
				,10
				--,'Pramod.B.Fernandez@sprint.com,Jagannath.r.gangi@sprint.com,Ramesh.Ragi@sprint.com,Alessandra.R.Antolini@sprint.com,David.L2.Phillips@sprint.com,Frank.Luna@sprint.com')
				,@EMAIL_LIST_TXT
				,'Adhoc Report Notification'
				,@EmailBody)

			END
 	 
END TRY
BEGIN CATCH         
		EXEC [dbo].[insertErrorInfo]          
END CATCH    

SET NOCOUNT OFF;

END


GO


