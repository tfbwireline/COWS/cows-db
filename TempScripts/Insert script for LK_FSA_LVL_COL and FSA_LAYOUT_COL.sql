



INSERT INTO [COWS].[dbo].[LK_FSA_LVL_COL]
           ([LVL_ID]
           ,[DB_COL_NME]
           ,[CREAT_DT])
     VALUES
           (27
           ,'TTR_PL_NBR'
           ,CAST(GETDATE() as SMALLDATETIME))
GO



INSERT INTO [COWS].[dbo].[FSA_LAYOUT_COL]
           ([LAYOUT_ID]
           ,[LVL_ID]
           ,[DB_COL_NME]
           ,[UI_NME]
           ,[SRC_TBL_NME]
           ,[CREAT_DT]
           ,[COL_POS_NBR]
           ,[RPET_SECT_CD])
     VALUES
		   (1
           ,27
           ,'TTR_PL_NBR'
           ,'Private Line Number'
           ,'FSA_ORDR'
           ,CAST(GETDATE() as SMALLDATETIME)
           ,41
           ,0),
           (2
           ,27
           ,'TTR_PL_NBR'
           ,'Private Line Number'
           ,'FSA_ORDR'
           ,CAST(GETDATE() as SMALLDATETIME)
           ,44
           ,0)
GO



