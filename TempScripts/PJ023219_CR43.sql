USE COWS
GO

INSERT INTO dbo.NID_ACTY (NID_SERIAL_NBR, FSA_CPE_LINE_ITEM_ID, M5_ORDR_NBR, H6, DD_APRVL_NBR, EVENT_ID, NID_HOST_NME, REC_STUS_ID, CREAT_BY_USER_ID, CREAT_DT)
SELECT distinct fcl.NID_SERIAL_NBR, fcl.FSA_CPE_LINE_ITEM_ID, fo.FTN, od.H5_H6_CUST_ID, (select top 1 ment.DD_APRVL_NBR from dbo.MDS_EVENT_NTWK_TRPT ment with (nolock) inner join dbo.MDS_EVENT me with (nolock) on ment.event_id=me.event_id and ment.rec_stus_id=1 inner join dbo.event_hist eh with (nolock) on eh.event_id=me.event_id where me.NID_SERIAL_NBR = fcl.NID_SERIAL_NBR and eh.actn_id=17 order by eh.CREAT_DT DESC), (select top 1 me.EVENT_ID from dbo.MDS_EVENT me with (nolock) inner join dbo.event_hist eh with (nolock) on eh.event_id=me.event_id where me.NID_SERIAL_NBR = fcl.NID_SERIAL_NBR and eh.actn_id=17 order by eh.CREAT_DT DESC),
(select top 1 me.NID_HOST_NME from dbo.MDS_EVENT me with (nolock) inner join dbo.event_hist eh with (nolock) on eh.event_id=me.event_id where me.NID_SERIAL_NBR = fcl.NID_SERIAL_NBR and eh.actn_id=17 order by eh.CREAT_DT DESC), 252, isnull((select top 1 ont.CREAT_BY_USER_ID from dbo.ordr_nte ont with (nolock) where ont.ordr_id=fo.ordr_id and ont.nte_txt like 'NID Serial Number%' order by ont.CREAT_DT DESC),
isnull((select top 1 ont.CREAT_BY_USER_ID from dbo.ordr_nte ont with (nolock) where ont.ordr_id=fo.ordr_id and ont.nte_txt like '%Material Requisitioning Completed%' order by ont.CREAT_DT DESC),1)),
isnull((select top 1 ont.CREAT_DT from dbo.ordr_nte ont with (nolock) where ont.ordr_id=fo.ordr_id and ont.nte_txt like 'NID Serial Number%' order by ont.CREAT_DT DESC),
isnull((select top 1 ont.CREAT_DT from dbo.ordr_nte ont with (nolock) where ont.ordr_id=fo.ordr_id and ont.nte_txt like '%Material Requisitioning Completed%' order by ont.CREAT_DT DESC),getdate()))
from dbo.fsa_ordr fo with (nolock) inner join
dbo.FSA_ORDR_CPE_LINE_ITEM fcl with (nolock) on fcl.ORDR_ID=fo.ordr_id inner join
dbo.ordr od with (nolock) on od.ordr_id=fo.ordr_id
where isnull(fcl.NID_SERIAL_NBR,'') != ''

UPDATE na
SET REC_STUS_ID = 251
FROM dbo.NID_ACTY na WITH (NOLOCK) INNER JOIN
(select distinct fcl.NID_SERIAL_NBR, fo.FTN
from dbo.fsa_ordr fo with (nolock) inner join
dbo.FSA_ORDR_CPE_LINE_ITEM fcl with (nolock) on fcl.ORDR_ID=fo.ordr_id
where isnull(fcl.NID_SERIAL_NBR,'') != '') as x on x.FTN=na.M5_ORDR_NBR
WHERE na.REC_STUS_ID=252

-- UPDATE na
-- SET EVENT_ID = 
-- FROM dbo.NID_ACTY na WITH (NOLOCK) INNER JOIN
-- ((select distinct fcl.NID_SERIAL_NBR, (select top 1 ont.ordr_id from dbo.MDS_EVENT me with (nolock) inner join dbo.event_hist eh with (nolock) on eh.event_id=me.event_id where me.NID_SERIAL_NBR = fcl.NID_SERIAL_NBR and eh.actn_id=17 order by ont.CREAT_DT DESC) as ORDR_ID
-- from dbo.fsa_ordr fo with (nolock) inner join
-- dbo.FSA_ORDR_CPE_LINE_ITEM fcl with (nolock) on fcl.ORDR_ID=fo.ordr_id
-- where isnull(fcl.NID_SERIAL_NBR,'') != '')) as x on x.ORDR_ID=na.ORDR_ID



-- select fo.*
-- from dbo.fsa_ordr fo with (nolock) inner join
-- dbo.FSA_ORDR_CPE_LINE_ITEM fcl with (nolock) on fcl.ORDR_ID=fo.ordr_id
-- where isnull(fcl.NID_SERIAL_NBR,'') != ''
-- and fo.ordr_id in (13964,
-- 13980)


-- select distinct fcl.NID_SERIAL_NBR, len(fcl.NID_SERIAL_NBR), (select top 1 ont.ordr_id from dbo.ordr_nte ont with (nolock) where ont.nte_txt like '%' + fcl.NID_SERIAL_NBR + '%' order by ont.CREAT_DT DESC)
-- from dbo.fsa_ordr fo with (nolock) inner join
-- dbo.FSA_ORDR_CPE_LINE_ITEM fcl with (nolock) on fcl.ORDR_ID=fo.ordr_id
-- where isnull(fcl.NID_SERIAL_NBR,'') != ''
-- order by fcl.NID_SERIAL_NBR desc
