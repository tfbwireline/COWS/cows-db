/*
	Enter FTN if its from FSA/Mach5 or 
		Order ID if order was generated from cows.
*/
Declare @ftn Varchar(20) = '427759' -- Enter FTN here 
Declare @ordr_id Int = 0, @vndr_ordr_id Int = 0 
Select @ordr_id = isnull(ordr_id,0)
	From dbo.FSA_ORDR 
	Where FTN = @ftn	

IF @ordr_id != 0	
	Begin
		Select @vndr_ordr_id = ISNULL(VNDR_ORDR_ID,0)
			From dbo.VNDR_ORDR 
			Where ORDR_ID = @ordr_id
			
		IF @vndr_ordr_id != 0
			BEGIN
				UPDATE dbo.VNDR_ORDR
					SET ORDR_ID = null
					Where VNDR_ORDR_ID = @vndr_ordr_id
					
				IF EXISTS
					(
						SELECT 'X'
							FROM dbo.CKT WITH (NOLOCK)
							Where VNDR_ORDR_ID = @vndr_ordr_id
								AND ORDR_ID = @ordr_id
					)
					BEGIN
						UPDATE dbo.CKT 
							SET VNDR_ORDR_ID = null
							WHERE ORDR_ID = @ordr_id
					END
			END	
	End
Else
	Begin
		Select @vndr_ordr_id = ISNULL(VNDR_ORDR_ID,0)
			From dbo.VNDR_ORDR 
			Where ORDR_ID = @ftn
		
		SET @ordr_id = @ftn
			
		IF @vndr_ordr_id != 0
			BEGIN
				UPDATE dbo.VNDR_ORDR
					SET ORDR_ID = null
					Where VNDR_ORDR_ID = @vndr_ordr_id
					
				IF EXISTS
					(
						SELECT 'X'
							FROM dbo.CKT WITH (NOLOCK)
							Where VNDR_ORDR_ID = @vndr_ordr_id
								AND ORDR_ID = @ordr_id
					)
					BEGIN
						UPDATE dbo.CKT 
							SET VNDR_ORDR_ID = null
							WHERE ORDR_ID = @ordr_id
					END
			END	
	End

Select o.ORDR_ID,f.FTN,f.ORDR_ID,v.ORDR_ID as 'VendorOrderID',c.VNDR_ORDR_ID as 'CktVendorOrderID'
	From dbo.ORDR o with (nolock)
	left join dbo.FSA_ORDR f with (nolock) on f.ORDR_ID = o.ORDR_ID
	left join dbo.VNDR_ORDR v with (nolock) on f.ORDR_ID = v.ORDR_ID
	left join dbo.CKT c with (nolock) on c.ORDR_ID = f.ORDR_ID
	Where (
				f.FTN = @ftn
				OR
				Convert(Varchar,o.ORDR_ID) = @ftn
			)
	
--Update VNDR_ORDR set ORDR_ID = 427793 Where VNDR_ORDR_ID = 428339		