USE COWS
GO
/*
	This script manually fetches order based on order number from Mach5 
	system bypassing systematic process thru SQL Job
	Pass in order number(ORDER_NBR) or change transaction number (CHNG_TXN_NBR).
	
*/

DECLARE @ORDER_NBR Varchar(20) = 'CH3G8P005815' --'IN3G8M005753' --'CH3G8P005815' --Enter FTN here


IF NOT EXISTS
	(
		SELECT 'X'
			FROM dbo.FSA_ORDR WITH (NOLOCK)
			WHERE FTN = @ORDER_NBR
	)
	BEGIN
		DECLARE @SQL nVarchar(max) = ''
		DECLARE @ORDR_ID INT,
				@ORDR_TYPE_CD Varchar(5),
				@M5_RELT_ORDR_ID INT

		DECLARE @M5Table Table (ORDR_ID INT,ORDR_TYPE_CD Varchar(5),RLTD_ORDR_ID INT)
		SET @SQL = 'SELECT	ORDR_ID,  
							ORDR_TYPE_CD,
							ISNULL(RLTD_ORDR_ID,0) RLTD_ORDR_ID
						FROM OpenQuery(M5,''Select ORDR_ID,ORDR_TYPE_CD,RLTD_ORDR_ID From Mach5.V_V5U_ORDR WHERE ORDER_NBR = ''''' + @ORDER_NBR + ''''''')'
										
		--Select @SQL
		INSERT INTO @M5Table 
		Exec sp_executeSQL @SQL

		IF EXISTS
			(
				SELECT 'X' 
					FROM @M5Table
			)
			BEGIN
				Select 
					@ORDR_ID = ORDR_ID,
					@ORDR_TYPE_CD = ORDR_TYPE_CD, 
					@M5_RELT_ORDR_ID = RLTD_ORDR_ID
					FROM @M5Table

				--Select * From @M5Table
				--Select @ORDR_ID 'OrderID',@ORDR_TYPE_CD 'OrderType', @M5_RELT_ORDR_ID 'RelatedOrderID'			
				
				EXEC dbo.insertMach5OrderDetails @ORDR_ID,@M5_RELT_ORDR_ID,@ORDR_TYPE_CD,'',''
			END
		ELSE
			BEGIN
				SET @SQL = 'SELECT	CHNG_TXN_ID,  
									TYPE_CD,
									ISNULL(ORDR_ID,0) ORDR_ID
								FROM	OpenQuery(M5,''Select CHNG_TXN_ID,TYPE_CD,ORDR_ID 
															From	Mach5.V_V5U_CHNG_TXN 
															WHERE	CHNG_TXN_NBR = ''''' + @ORDER_NBR + ''''''')'
				--Select @SQL															
				INSERT INTO @M5Table 
				Exec sp_executeSQL @SQL													
				
				IF EXISTS
					(
						SELECT 'X' 
							FROM @M5Table
					)
					BEGIN
						Select 
							@ORDR_ID = ORDR_ID,
							@ORDR_TYPE_CD = ORDR_TYPE_CD, 
							@M5_RELT_ORDR_ID = RLTD_ORDR_ID
							FROM @M5Table
						
						--Select * From @M5Table
						--Select @ORDR_ID 'OrderID',@ORDR_TYPE_CD 'OrderType', @M5_RELT_ORDR_ID 'RelatedOrderID'			
						
						EXEC dbo.insertMach5OrderDetails @ORDR_ID,@M5_RELT_ORDR_ID,@ORDR_TYPE_CD,'',''
					END
				ELSE
					BEGIN
						SELECT 'Please check your order number and try again. The order does not exist in order and transaction Mach5 view.'
					END
			END
		
		
	END
ELSE
	BEGIN
		Select 'The order already exists in COWS.'
	END
	
--Select *
--	From dbo.FSA_ORDR WITH (NOLOCK)
--	WHERE FTN = @ORDER_NBR




--Exec dbo.insertMach5OrderDetails @M5_ORDR_ID,@M5_RELT_ORDR_ID,@ORDR_TYPE_CD,@QDA_ORDR_ID,@DEVICE_ID
--DECLARE @QDA_ORDR_ID Varchar(100) = '', @DEVICE_ID Varchar(50) = ''
--DECLARE @ORDR_ID INT,
--		@ORDR_TYPE_CD Varchar(5)
--Exec dbo.insertMach5OrderDetails @ORDR_ID,0,@ORDR_TYPE_CD,@DEVICE_ID
