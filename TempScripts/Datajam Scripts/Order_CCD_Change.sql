/*
	To update CCD on an order
	Enter FTN if its from FSA/Mach5 or 
		Order ID if order was generated from cows.
*/
USE COWS
GO
Declare @ftn Varchar(20) = 'DC3GBU007137' -- Enter FTN here 
DECLARE @newCCD DateTime = '12/27/2016' -- Enter new CCD  here
DECLARE @nte_txt Varchar(100) = 'CCD changed as per user request' --Enter comments here
Declare @ordr_id Int = 0

Select @ordr_id = isnull(ordr_id,0)
	From dbo.FSA_ORDR 
	Where FTN = @ftn

IF @ordr_id = 0
	begin
		Select @ordr_id = ORDR_ID
			From dbo.ORDR 
			Where CONVERT(Varchar,ORDR_ID) = @ftn
		
	end	

IF @ordr_id != 0
	BEGIN
		Update dbo.ORDR SET CUST_CMMT_DT = @newCCD WHERE ORDR_ID = @ordr_id
		INSERT INTO dbo.ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
		Select 9,@ordr_id,GETDATE(),1,1,@nte_txt
			
	END
ELSE
	BEGIN
		Select 'Invalid FTN/OrderID'
	END	