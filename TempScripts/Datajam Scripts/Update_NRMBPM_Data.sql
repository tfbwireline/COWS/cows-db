USE COWS
GO
/*
	Update NRMBPM data for an order. 
*/
DECLARE @FTN Varchar(20) = 'CN3GEK008115' --Enter FTN here
DECLARE @QDA_V_BPMF_COWS_ORDR Table (M5_ORDR_ID Varchar(20))
Declare @SQL nVarchar(max) = ''
Set @sql = 'Select *
				From OPENQUERY (NRMBPM,''Select M5_ORDR_ID
								From BPMF_OWNER.V_BPMF_COWS_ORDR 
								WHERE M5_ORDR_ID IN ('''''  + @FTN  + ''''')'')'

INSERT INTO @QDA_V_BPMF_COWS_ORDR
Exec sp_executeSQL @sql

--Select * From @QDA_V_BPMF_COWS_ORDR		

DECLARE @QDA_ORDR_ID INT
Select @QDA_ORDR_ID = ISNULL(ORDR_ID,0)
	FROM dbo.FSA_ORDR
	WHERE FTN = @FTN

--Select @QDA_ORDR_ID

IF (@QDA_ORDR_ID != 0 
	AND ((SELECT COUNT(1) FROM @QDA_V_BPMF_COWS_ORDR) > 0))
		BEGIN
			EXEC [dbo].[updateNRMBPMData] @QDA_ORDR_ID
			Select 'NRMBPM Data updated for this FTN.'
		END
ELSE
	IF ISNULL(@QDA_ORDR_ID,0)  = 0	
		BEGIN
			SELECT 'Invalid FTN'
		END	
	ELSE IF ((SELECT COUNT(1) FROM @QDA_V_BPMF_COWS_ORDR) = 0)
		BEGIN
			SELECT 'No data exists for this FTN in NRMBPM'
		END
	