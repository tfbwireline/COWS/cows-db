USE COWS
GO

DELETE 
FROM dbo.APPT WITH (ROWLOCK)
WHERE (RCURNC_CD = 1)
  AND (LEN(RCURNC_DES_TXT) > 10)
  and (ASN_TO_USER_ID_LIST_TXT IS NULL)


INSERT INTO dbo.APPT_RCURNC_TRGR
SELECT APPT_ID, 0, GETDATE()
FROM dbo.APPT WITH (NOLOCK)
WHERE (RCURNC_CD = 1)
  AND (LEN(RCURNC_DES_TXT) > 10)
  AND ((REC_STUS_ID = 1) OR (REC_STUS_ID IS NULL))
  and (ASN_TO_USER_ID_LIST_TXT IS NOT NULL)