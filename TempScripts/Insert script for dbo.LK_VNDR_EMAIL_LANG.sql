DECLARE @ID INT
IF((SELECT COUNT(*) FROM dbo.LK_VNDR_EMAIL_LANG WHERE VNDR_CD = '10107') = 0)
BEGIN
	SELECT @ID = MAX(ISNULL(VNDR_EMAIL_LANG_ID, 0)) + 1
	FROM dbo.LK_VNDR_EMAIL_LANG

	INSERT INTO dbo.LK_VNDR_EMAIL_LANG (VNDR_EMAIL_LANG_ID, VNDR_CD, LANG_NME,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
	VALUES(@ID, '10107', 'This Order is governed by the Terms & Conditions of  the Amended & Restated Distribution Agreement between Sprint Communications Company L.P. and Equant Inc. operating under the name of Orange Business Services.',1,1,GETDATE())
END
IF((SELECT COUNT(*) FROM dbo.LK_VNDR_EMAIL_LANG WHERE VNDR_CD = 'RES-Orange') = 0)
BEGIN
	SELECT @ID = MAX(ISNULL(VNDR_EMAIL_LANG_ID, 0)) + 1
	FROM dbo.LK_VNDR_EMAIL_LANG

	INSERT INTO dbo.LK_VNDR_EMAIL_LANG (VNDR_EMAIL_LANG_ID, VNDR_CD, LANG_NME,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
	VALUES(@ID, 'RES-Orange', 'This Order is governed by the Terms & Conditions of  the Amended & Restated Distribution Agreement between Sprint Communications Company L.P. and Equant Inc. operating under the name of Orange Business Services.',1,1,GETDATE())
END
IF((SELECT COUNT(*) FROM dbo.LK_VNDR_EMAIL_LANG WHERE VNDR_CD = '40106') = 0)
BEGIN
	SELECT @ID = MAX(ISNULL(VNDR_EMAIL_LANG_ID, 0)) + 1
	FROM dbo.LK_VNDR_EMAIL_LANG

	INSERT INTO dbo.LK_VNDR_EMAIL_LANG (VNDR_EMAIL_LANG_ID, VNDR_CD, LANG_NME,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
	VALUES(@ID, '40106',	'This Order is governed by the Terms & Conditions of the Reciprocal NNI & Wholesale Services Agreement between Sprint Communications Company L.P. and Rogers Telecom Inc.',1,1,GETDATE())
END
IF((SELECT COUNT(*) FROM dbo.LK_VNDR_EMAIL_LANG WHERE VNDR_CD = 'RES-Rogers') = 0)
BEGIN
	SELECT @ID = MAX(ISNULL(VNDR_EMAIL_LANG_ID, 0)) + 1
	FROM dbo.LK_VNDR_EMAIL_LANG

	INSERT INTO dbo.LK_VNDR_EMAIL_LANG (VNDR_EMAIL_LANG_ID, VNDR_CD, LANG_NME,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
	VALUES(@ID, 'RES-Rogers', 'This Order is governed by the Terms & Conditions of the Reciprocal NNI & Wholesale Services Agreement between Sprint Communications Company L.P. and Rogers Telecom Inc.',1,1,GETDATE())
END
IF((SELECT COUNT(*) FROM dbo.LK_VNDR_EMAIL_LANG WHERE VNDR_CD = '110107') = 0)
BEGIN
	SELECT @ID = MAX(ISNULL(VNDR_EMAIL_LANG_ID, 0)) + 1
	FROM dbo.LK_VNDR_EMAIL_LANG

	INSERT INTO dbo.LK_VNDR_EMAIL_LANG (VNDR_EMAIL_LANG_ID, VNDR_CD, LANG_NME,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT)
	VALUES(@ID, '110107', 'This Order is governed by the Master Services Agreement between Sprint Hong Kong Limited and China Telecom Hong Kong International (CTHKI).',1,1,GETDATE())
END