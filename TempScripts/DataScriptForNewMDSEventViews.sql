USE COWS
GO

if not exists (select 'x'
				from dbo.LK_DSPL_COL with (nolock)
				where DSPL_COL_NME = 'H1_ID')
begin
insert into dbo.LK_DSPL_COL
select 1, 'H1_ID', 'H1_ID', 'H1_ID', 1, NULL, NULL, GETDATE()
end

declare @col_id int
select @col_id = DSPL_COL_ID
from dbo.LK_DSPL_COL with (nolock)
where DSPL_COL_NME = 'H1_ID'

update dvc
set dvc.DSPL_COL_ID = @col_id
from dbo.DSPL_VW_COL dvc with (rowlock) inner join
dbo.DSPL_VW dv with (nolock) on dv.DSPL_VW_ID = dvc.DSPL_VW_ID
where dv.SITE_CNTNT_ID = 5
  and dvc.DSPL_COL_ID = 3
  
update dv
set dv.SORT_BY_COL_1_ID = @col_id
from dbo.DSPL_VW dv with (rowlock)
where dv.SITE_CNTNT_ID = 5
  and dv.SORT_BY_COL_1_ID=3
  
update dv
set dv.SORT_BY_COL_2_ID = @col_id
from dbo.DSPL_VW dv with (rowlock)
where dv.SITE_CNTNT_ID = 5
  and dv.SORT_BY_COL_2_ID=3
  
update dv
set dv.GRP_BY_COL_1_ID = @col_id
from dbo.DSPL_VW dv with (rowlock)
where dv.SITE_CNTNT_ID = 5
  and dv.GRP_BY_COL_1_ID=3
  
update dv
set dv.GRP_BY_COL_2_ID = @col_id
from dbo.DSPL_VW dv with (rowlock)
where dv.SITE_CNTNT_ID = 5
  and dv.GRP_BY_COL_2_ID=3