USE [COWS]
GO

--To handle interoperability between sql server 2017 and prior versions because of old versions using SHA1 and 2017 using SHA2
DBCC TRACEON(4631, -1); 
GO

IF EXISTS (SELECT * FROM sys.symmetric_keys WHERE name = 'FS@K3y')
BEGIN
	if exists(select 1 
              from sys.openkeys 
              where key_name = 'FS@K3y' and database_name = db_name()
              )
    BEGIN
		CLOSE SYMMETRIC KEY FS@K3y;
	END
	DROP SYMMETRIC KEY FS@K3y;
END

IF EXISTS
	(SELECT * FROM sys.certificates WHERE name='S3cFS@CustInf0')
BEGIN
	DROP CERTIFICATE S3cFS@CustInf0
END


IF EXISTS 
    (SELECT * FROM sys.symmetric_keys WHERE name LIKE '%MasterKey##')
BEGIN
	DROP MASTER KEY 
END
GO

CREATE MASTER KEY ENCRYPTION BY 
	PASSWORD = 'S8r3s7R@m9s7J@g@nS8m@nN@1d8&*(&%^tu*B#######897By^&*b&*678IH()_)'
GO

CREATE CERTIFICATE S3cFS@CustInf0
   WITH SUBJECT = 'Secured FSA Customer Info';
GO

CREATE SYMMETRIC KEY FS@K3y
WITH KEY_SOURCE = 'S8r3s7R@m9s7J@g@nS8m@nN@1d8',
    IDENTITY_VALUE = 'S8r3s7R@m9s7J@g@nS8m@nN@1d8',
    ALGORITHM = AES_256
    ENCRYPTION BY CERTIFICATE S3cFS@CustInf0;
GO

IF EXISTS (select 'X' from sys.sysusers where name='AD\QDA-App-Admins')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\QDA-App-Admins]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\QDA-App-Admins]
EXEC sp_addrolemember 'db_procexecuter', 'AD\QDA-App-Admins';
EXEC sp_addrolemember 'db_datawriter', 'AD\QDA-App-Admins';
EXEC sp_addrolemember 'db_datareader', 'AD\QDA-App-Admins';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\QDA-App-Developers')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\QDA-App-Developers]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\QDA-App-Developers]
EXEC sp_addrolemember 'db_procexecuter', 'AD\QDA-App-Developers';
EXEC sp_addrolemember 'db_datawriter', 'AD\QDA-App-Developers';
EXEC sp_addrolemember 'db_datareader', 'AD\QDA-App-Developers';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\QDA-SQL-Admins')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\QDA-SQL-Admins]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\QDA-SQL-Admins]
EXEC sp_addrolemember 'db_procexecuter', 'AD\QDA-SQL-Admins';
EXEC sp_addrolemember 'db_datawriter', 'AD\QDA-SQL-Admins';
EXEC sp_addrolemember 'db_datareader', 'AD\QDA-SQL-Admins';
END

IF EXISTS (select 'X' from sys.sysusers where name='cows_rpt_usr')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [cows_rpt_usr]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [cows_rpt_usr]
EXEC sp_addrolemember 'db_procexecuter', 'cows_rpt_usr';
EXEC sp_addrolemember 'db_datawriter', 'cows_rpt_usr';
EXEC sp_addrolemember 'db_datareader', 'cows_rpt_usr';
END

IF EXISTS (select 'X' from sys.sysusers where name='APP_V5U_User')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [APP_V5U_User]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [APP_V5U_User]
EXEC sp_addrolemember 'db_procexecuter', 'APP_V5U_User';
EXEC sp_addrolemember 'db_datawriter', 'APP_V5U_User';
EXEC sp_addrolemember 'db_datareader', 'APP_V5U_User';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\DVMXD823$')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\DVMXD823$]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\DVMXD823$]
EXEC sp_addrolemember 'db_procexecuter', 'AD\DVMXD823$';
EXEC sp_addrolemember 'db_datawriter', 'AD\DVMXD823$';
EXEC sp_addrolemember 'db_datareader', 'AD\DVMXD823$';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\DVMXD824$')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\DVMXD824$]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\DVMXD824$]
EXEC sp_addrolemember 'db_procexecuter', 'AD\DVMXD824$';
EXEC sp_addrolemember 'db_datawriter', 'AD\DVMXD824$';
EXEC sp_addrolemember 'db_datareader', 'AD\DVMXD824$';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\TVMXD967$')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\TVMXD967$]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\TVMXD967$]
EXEC sp_addrolemember 'db_procexecuter', 'AD\TVMXD967$';
EXEC sp_addrolemember 'db_datawriter', 'AD\TVMXD967$';
EXEC sp_addrolemember 'db_datareader', 'AD\TVMXD967$';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\TVMXD968$')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\TVMXD968$]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\TVMXD968$]
EXEC sp_addrolemember 'db_procexecuter', 'AD\TVMXD968$';
EXEC sp_addrolemember 'db_datawriter', 'AD\TVMXD968$';
EXEC sp_addrolemember 'db_datareader', 'AD\TVMXD968$';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\TVMXD970$')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\TVMXD970$]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\TVMXD970$]
EXEC sp_addrolemember 'db_procexecuter', 'AD\TVMXD970$';
EXEC sp_addrolemember 'db_datawriter', 'AD\TVMXD970$';
EXEC sp_addrolemember 'db_datareader', 'AD\TVMXD970$';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\TVMXD971$')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\TVMXD971$]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\TVMXD971$]
EXEC sp_addrolemember 'db_procexecuter', 'AD\TVMXD971$';
EXEC sp_addrolemember 'db_datawriter', 'AD\TVMXD971$';
EXEC sp_addrolemember 'db_datareader', 'AD\TVMXD971$';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\TVMXE172$')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\TVMXE172$]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\TVMXE172$]
EXEC sp_addrolemember 'db_procexecuter', 'AD\TVMXE172$';
EXEC sp_addrolemember 'db_datawriter', 'AD\TVMXE172$';
EXEC sp_addrolemember 'db_datareader', 'AD\TVMXE172$';
END

IF EXISTS (select 'X' from sys.sysusers where name='AD\TVMXE173$')
BEGIN
GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\TVMXE173$]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\TVMXE173$]
EXEC sp_addrolemember 'db_procexecuter', 'AD\TVMXE173$';
EXEC sp_addrolemember 'db_datawriter', 'AD\TVMXE173$';
EXEC sp_addrolemember 'db_datareader', 'AD\TVMXE173$';
END

DBCC TRACEOFF(4631, -1); 
GO