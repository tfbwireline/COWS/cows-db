USE COWS
GO

Select * FROM dbo.ORDR_MS WITH (NOLOCK) WHERE VNDR_CNFRM_RNL_DT IS NOT NULL

UPDATE trptO
	SET	trptO.CNFRM_RNL_DT = oMS.VNDR_CNFRM_RNL_DT
	FROM	dbo.TRPT_ORDR trptO WITH (ROWLOCK)
INNER JOIN	dbo.ORDR_MS oMS WITH (NOLOCK) ON trptO.ORDR_ID = oMS.ORDR_ID
	WHERE	oMS.VNDR_CNFRM_RNL_DT	IS NOT NULL

INSERT INTO dbo.TRPT_ORDR (ORDR_ID,CNFRM_RNL_DT,CREAT_BY_USER_ID)
SELECT oMS.ORDR_ID,oMS.VNDR_CNFRM_RNL_DT,1
	FROM	dbo.ORDR_MS oMS WITH (NOLOCK)
	WHERE	oMS.VNDR_CNFRM_RNL_DT	IS NOT NULL
		AND NOT EXISTS
			(
				SELECT 'X'
					FROM	dbo.TRPT_ORDR trptO WITH (NOLOCK)
					WHERE	trptO.ORDR_ID = oMS.ORDR_ID
			)
			

--Select * From dbo.ORDR_MS WITH (NOLOCK) WHERE VNDR_CNFRM_RNL_DT IS NOT NULL
--Select * FROM dbo.TRPT_ORDR WITH (NOLOCK) WHERE VNDR_CNFRM_RNL_DT IS NOT NULL
			