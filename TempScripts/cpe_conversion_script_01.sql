USE COWS
GO


BEGIN TRY

IF OBJECT_ID(N'tempdb..#CPE_ORDR',N'U') IS NOT NULL 
				DROP TABLE #CPE_ORDR
CREATE TABLE #CPE_ORDR (FTN Varchar(20),M5_ORDR_ID INT,ORDR_TYPE_CD Varchar(5),EXST_FSA_ORDR_ID INT,NEW_FSA_ORDR_ID Varchar(max),Flag Bit)
IF NOT EXISTS
	(
		Select 'X' 
			From dbo.CPE_ORDR WITH (NOLOCK)	
	)
	BEGIN
		INSERT INTO dbo.CPE_ORDR (FTN)	VALUES ('11763074')
		INSERT INTO dbo.CPE_ORDR (FTN)	VALUES ('11763077')
		INSERT INTO dbo.CPE_ORDR (FTN)	VALUES ('11763075')
		INSERT INTO dbo.CPE_ORDR (FTN)	VALUES ('11763076')
		INSERT INTO dbo.CPE_ORDR (FTN)	VALUES ('11712219')
		INSERT INTO dbo.CPE_ORDR (FTN)	VALUES ('11712145')
		INSERT INTO #CPE_ORDR (FTN,Flag)
			SELECT FTN,0
				FROM dbo.CPE_ORDR
	END
ELSE
	BEGIN
		INSERT INTO #CPE_ORDR (FTN,Flag)
			SELECT FTN,0
				FROM dbo.CPE_ORDR
	END

Update c
	SET c.EXST_FSA_ORDR_ID = f.ORDR_ID
	FROM #CPE_ORDR c WITH (ROWLOCK)
LEFT JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON c.ftn = f.FTN

--Select c.FTN,f.ORDR_ID 
--	From @CPE_ORDR c
--	LEFT JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON c.ftn = f.FTN
DECLARE @Cnt Int = 0, @Ctr Int = 0, @SQL nVarchar(max) = '',@FTN varchar(20)= '' 
Select @Cnt = count(DISTINCT FTN) From #CPE_ORDR Where EXST_FSA_ORDR_ID IS NOT NULL
Select @Cnt
DECLARE @EXST_FSA_ORDR_ID INT = 0, @NEW_FSA_ORDR_IDS Varchar(max) = '', @M5_ORDR_ID INT = 0, @ORDR_TYPE_CD Varchar(5) = '', @M5_RELT_ORDR_ID INT = 0
Declare @ordr_nte_status Varchar(200), @ordr_nte_cnvr Varchar(200)
WHILE (@Ctr < @Cnt)
	BEGIN
		SELECT TOP 1 @EXST_FSA_ORDR_ID = EXST_FSA_ORDR_ID
					,@FTN = FTN
			FROM #CPE_ORDR
			WHERE Flag = 0
				AND ISNULL(EXST_FSA_ORDR_ID,0) != 0
		
		Select @ordr_nte_status = 'This FTN: ' + f.FTN + ' status was updated from ' + ls.ORDR_STUS_DES + ' to cancelled status.',
			   @ordr_nte_cnvr = 'FTN changed from ' + f.FTN  + ' to ' + f.FTN	+ '_C'
			From dbo.FSA_ORDR f WITH (NOLOCK)
			Inner Join dbo.ORDR o with (nolock) on f.ordr_id = o.ordr_id
			Left Join dbo.LK_ORDR_STUS ls with (nolock) on ls.ORDR_STUS_ID = o.ORDR_STUS_ID
			Where f.ORDR_ID = @EXST_FSA_ORDR_ID
		
		Select @ordr_nte_status,@ordr_nte_cnvr
		
		SET @SQL = ' UPDATE #CPE_ORDR
						SET M5_ORDR_ID = A.ORDR_ID,
							ORDR_TYPE_CD = A.ORDR_TYPE_CD
						FROM (SELECT *
			FROM OPENQUERY(M5,''SELECT ORDR_ID, ORDR_TYPE_CD 
										FROM MACH5.V_V5U_ORDR  
										WHERE ORDER_NBR = ''''' + CONVERT(VARCHAR,@FTN) + '''''''))A
						WHERE FTN = ' + @FTN
										
		PRINT @SQL
		Exec sp_executeSQL @SQL
		
		Select @M5_ORDR_ID = M5_ORDR_ID
				,@ORDR_TYPE_CD = ORDR_TYPE_CD
			From #CPE_ORDR
			WHERE FTN = @FTN
		
		IF @M5_ORDR_ID != 0 AND @ORDR_TYPE_CD != ''
			BEGIN
				--Select 'Call dbo.insertMach5OrderDetails ' + Convert(varchar,@M5_ORDR_ID) + ',' + Convert(varchar,@M5_RELT_ORDR_ID) + ',' + @ORDR_TYPE_CD + ',' + @NEW_FSA_ORDR_IDS ' OUTPUT' 
				EXEC dbo.insertMach5OrderDetails @M5_ORDR_ID, @M5_RELT_ORDR_ID, @ORDR_TYPE_CD, @NEW_FSA_ORDR_IDS OUTPUT, ''
			END
		
		UPDATE dbo.ORDR WITH (ROWLOCK)
			SET ORDR_STUS_ID = 4
			WHERE ORDR_ID = @EXST_FSA_ORDR_ID
			
		INSERT INTO ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
			SELECT 20,@EXST_FSA_ORDR_ID,GETDATE(),1,1,@ordr_nte_status
				
		UPDATE dbo.FSA_ORDR	
			SET FTN = FTN + '_C'
			WHERE ORDR_ID = @EXST_FSA_ORDR_ID
			
		INSERT INTO ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
			SELECT 20,@EXST_FSA_ORDR_ID,GETDATE(),1,1,@ordr_nte_cnvr
		
		
		IF @NEW_FSA_ORDR_IDS != ''
			BEGIN
				DECLARE @QDA_ORDR TABLE (ORDR_ID varchar(20), Flag bit)
				DECLARE @FSA_ORDR_ID INT, @Ctr_new Int, @Cnt_new Int
				INSERT INTO @QDA_ORDR (ORDR_ID, Flag)
				Select *,0 From web.ParseCommaSeparatedStrings(@NEW_FSA_ORDR_IDS)
				
				Select @Cnt_new = COUNT(DISTINCT ORDR_ID) FROM @QDA_ORDR
				
				IF @Cnt_new > 0
					BEGIN
						WHILE (@Ctr_new < @Cnt_new)
							BEGIN
								SELECT TOP 1 @FSA_ORDR_ID = CONVERT(INT,ORDR_ID)
									FROM @QDA_ORDR 
									WHERE ISNULL(ORDR_ID,0) NOT IN ('',0)	
										AND Flag = 0
										
								INSERT INTO ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
								SELECT 20,@FSA_ORDR_ID,GETDATE(),1,1,'This order was part of conversion script.'
								
								
								Update @QDA_ORDR SET Flag = 1 WHERE Flag = 0 AND ORDR_ID = Convert(Varchar,@FSA_ORDR_ID) 
								Set @Ctr_new = @Ctr_new + 1	
								
							END
					END
				
			END
		
		Update #CPE_ORDR
			SET Flag = 1
				,NEW_FSA_ORDR_ID = @NEW_FSA_ORDR_IDS
			Where Flag = 0
				AND EXST_FSA_ORDR_ID = @EXST_FSA_ORDR_ID
		
		SET @Ctr = @Ctr + 1
						
	END
	Select * From #CPE_ORDR
END TRY

BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH

--Truncate table dbo.CPE_ORDR



