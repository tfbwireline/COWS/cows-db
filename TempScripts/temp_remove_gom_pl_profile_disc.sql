/*
	Script to remove GOM profile (GOM PL) from Worflow tables
*/
USE COWS
GO
DECLARE @tbl Table (ordr_id int)

Insert into @tbl (ordr_id)
Select DISTINCT o.ORDR_ID--COUNT(DISTINCT o.ORDR_ID)--DISTINCT fo.FTN,ISNULL(o.CUST_CMMT_DT,fo.CUST_CMMT_DT) AS CCD
	From	dbo.ORDR o WITH (NOLOCK)
Inner Join	dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = o.PPRT_ID
Inner Join	dbo.FSA_ORDR fo with (nolock) on fo.ORDR_ID = o.ORDR_ID
--Inner Join	dbo.ACT_TASK at WITH (NOLOCK) on at.ORDR_ID = o.ORDR_ID 
	Where	lp.SM_ID in (22)
	AND o.ORDR_STUS_ID IN (0,1)
	AND  NOT EXISTS
		(
			SELECT 'X'
				FROM	dbo.ACT_TASK at WITH (NOLOCK)
				WHERE	at.ORDR_ID = o.ORDR_ID
					AND	at.TASK_ID = 1000
					AND at.STUS_ID = 0
		)
	--AND	at.STUS_ID = 0
	--AND at.TASK_ID  NOT IN (1000)
	--AND  EXISTS
	--	(
	--		SELECT 'X'
	--			FROM	dbo.ORDR_NTE otn WITH (NOLOCK)
	--			WHERE	otn.ORDR_ID = o.ORDR_ID
	--				AND	otn.NTE_TXT = 'Move the order out from GOM Private Line - Pending to Complete state.'
	--	)
		
Select f.FTN,t.ordr_id
	From	@tbl t
Inner Join	dbo.FSA_ORDR f with (nolock) ON t.ordr_id = f.ORDR_ID
Inner join	dbo.ORDR o with (nolock) on f.ORDR_ID = o.ORDR_ID
--Inner join	dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = o.PPRT_ID
	
	
				
		
--Select * 
--	From	@tbl t
--Inner Join	dbo.ACT_TASK at WITH (NOLOCK) 	ON t.ordr_id = at.ORDR_ID 
--	Where	at.WG_PROF_ID = 107
--	order by t.ordr_id
		

--Select * 
--	From	@tbl t
--Inner Join	dbo.WG_PROF_STUS at WITH (NOLOCK) 	ON t.ordr_id = at.ORDR_ID 
--	Where	at.WG_PROF_ID = 107
--	order by t.ordr_id


--Select * 
--	From	@tbl t
--Inner Join	dbo.USER_WFM_ASMT at WITH (NOLOCK) 	ON t.ordr_id = at.ORDR_ID 
--	Where at.GRP_ID = 1
	


Delete at
	From	@tbl t
Inner Join	dbo.ACT_TASK at WITH (ROWLOCK) 	ON t.ordr_id = at.ORDR_ID 
	Where	at.WG_PROF_ID = 107


Delete at
	From	@tbl t
Inner Join	dbo.WG_PROF_STUS at WITH (ROWLOCK) 	ON t.ordr_id = at.ORDR_ID 
	Where	at.WG_PROF_ID = 107
	
