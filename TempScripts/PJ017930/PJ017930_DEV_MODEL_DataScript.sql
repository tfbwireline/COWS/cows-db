USE COWS
GO

	declare @manf int
	declare @maxmanf int
	declare @hcsmodel int
	declare @maxmodel int
	declare @miptmodel int
	SELECT @manf = MANF_ID FROM dbo.LK_DEV_MANF WITH (NOLOCK) WHERE MANF_NME = 'Sprint' 
	
	IF (@manf IS NULL)
	BEGIN
		select @maxmanf = max(manf_id) from dbo.LK_DEV_MANF
		set @maxmanf = @maxmanf + 1
		INSERT INTO dbo.LK_DEV_MANF (MANF_ID, MANF_NME, REC_STUS_ID, CREAT_BY_USER_ID) VALUES (@maxmanf, 'Sprint', 1, 1)

		SELECT @hcsmodel = DEV_MODEL_ID FROM dbo.LK_DEV_MODEL WITH (NOLOCK) WHERE DEV_MODEL_NME = 'HCS' 

		IF (@hcsmodel IS NULL)
		BEGIN
			select @maxmodel = max(DEV_MODEL_ID) from dbo.LK_DEV_MODEL
			set @maxmodel = @maxmodel + 1
			INSERT INTO [COWS].[dbo].[LK_DEV_MODEL] ([DEV_MODEL_id],[manF_id],[dev_model_nme],[min_drtn_tme_reqr_amt], [rec_stus_id],[creat_dt],[creat_by_user_id],[modfd_dt],[modfd_by_user_id]) values (@maxmodel,@maxmanf,'HCS',60,1,getdate(),1,null, null)
		END

		SELECT @miptmodel = DEV_MODEL_ID FROM dbo.LK_DEV_MODEL WITH (NOLOCK) WHERE DEV_MODEL_NME = 'MIPT' 

		IF (@miptmodel IS NULL)
		BEGIN
			select @maxmodel = max(DEV_MODEL_ID) from dbo.LK_DEV_MODEL
			set @maxmodel = @maxmodel + 1
			INSERT INTO [COWS].[dbo].[LK_DEV_MODEL] ([DEV_MODEL_id],[manF_id],[dev_model_nme],[min_drtn_tme_reqr_amt], [rec_stus_id],[creat_dt],[creat_by_user_id],[modfd_dt],[modfd_by_user_id]) values (@maxmodel,@maxmanf,'MIPT',60,1,getdate(),1,null, null)
		END
	END
	ELSE
	BEGIN
		SELECT @hcsmodel = DEV_MODEL_ID FROM dbo.LK_DEV_MODEL WITH (NOLOCK) WHERE DEV_MODEL_NME = 'HCS' 

		IF (@hcsmodel IS NULL)
		BEGIN
			select @maxmodel = max(DEV_MODEL_ID) from dbo.LK_DEV_MODEL
			set @maxmodel = @maxmodel + 1
			INSERT INTO [COWS].[dbo].[LK_DEV_MODEL] ([DEV_MODEL_id],[manF_id],[dev_model_nme],[min_drtn_tme_reqr_amt], [rec_stus_id],[creat_dt],[creat_by_user_id],[modfd_dt],[modfd_by_user_id]) values (@maxmodel,@manf,'HCS',60,1,getdate(),1,null, null)
		END

		SELECT @miptmodel = DEV_MODEL_ID FROM dbo.LK_DEV_MODEL WITH (NOLOCK) WHERE DEV_MODEL_NME = 'MIPT' 

		IF (@miptmodel IS NULL)
		BEGIN
			select @maxmodel = max(DEV_MODEL_ID) from dbo.LK_DEV_MODEL
			set @maxmodel = @maxmodel + 1
			INSERT INTO [COWS].[dbo].[LK_DEV_MODEL] ([DEV_MODEL_id],[manF_id],[dev_model_nme],[min_drtn_tme_reqr_amt], [rec_stus_id],[creat_dt],[creat_by_user_id],[modfd_dt],[modfd_by_user_id]) values (@maxmodel,@manf,'MIPT',60,1,getdate(),1,null, null)
		END
	END