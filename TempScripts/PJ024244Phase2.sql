USE COWS
GO

--Default all simple/complex codes to 'S'
UPDATE dbo.REDSGN_DEVICES_INFO SET SC_CD='S' WHERE FAST_TRK_CD=1 and SC_CD IS NULL
GO
UPDATE dbo.REDSGN_DEVICES_INFO SET SC_CD='C' WHERE ((FAST_TRK_CD != 1) OR (FAST_TRK_CD IS NULL)) and SC_CD IS NULL
GO

update me set me.SC_CD=rdi.SC_CD
from dbo.mds_event_odie_dev me inner join
dbo.REDSGN rd with (nolock) on me.RDSN_NBR = rd.REDSGN_NBR inner join
dbo.REDSGN_DEVICES_INFO rdi with (nolock) on rd.REDSGN_ID=rdi.REDSGN_ID and rdi.DEV_NME=me.ODIE_DEV_NME
where isnull(me.SC_CD,'')=''
and isnull(rdi.SC_CD,'')!=''

--select me.*
--from dbo.mds_event_odie_dev me with (nolock) inner join
--dbo.REDSGN rd with (nolock) on me.RDSN_NBR = rd.REDSGN_NBR inner join
--dbo.REDSGN_DEVICES_INFO rdi with (nolock) on rd.REDSGN_ID=rdi.REDSGN_ID and rdi.DEV_NME=me.ODIE_DEV_NME
--where isnull(me.SC_CD,'')=''
--and isnull(rdi.SC_CD,'')!=''

--Move all pending mds_events to rework

SELECT me.EVENT_ID, lu.DSPL_NME
FROM dbo.MDS_EVENT me WITH (NOLOCK) INNER JOIN
dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID]=me.CREAT_BY_USER_ID
WHERE me.EVENT_STUS_ID=2
ORDER BY me.EVENT_ID DESC

INSERT INTO dbo.EVENT_HIST (EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT)
SELECT EVENT_ID, 20, 'Event moved to Rework/Retract status as part of FT Decommissioning effort', 1, GETDATE()
FROM dbo.MDS_EVENT WITH (NOLOCK)
WHERE EVENT_STUS_ID=2
GO

UPDATE dbo.MDS_EVENT 
SET EVENT_STUS_ID=3,
WRKFLW_STUS_ID=3
WHERE EVENT_STUS_ID=2
GO

--Delete 'FT Preconfig Needed' view
delete from DSPL_VW_COL where DSPL_VW_ID in (select DSPL_VW_ID from DSPL_VW where DSPL_VW_NME='FT Preconfig Needed')
delete from DSPL_VW where DSPL_VW_NME='FT Preconfig Needed'