USE [COWS]
GO

OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

UPDATE foc 
SET foc.CUST_NME = dbo.encryptString(RTRIM(LTRIM(COALESCE(tfoc.CUST_NME,'')))) 
FROM 
dbo.FSA_ORDR_CUST foc WITH (TABLOCKX) INNER JOIN
cnv.FSA_ORDR_CUST tfoc WITH (NOLOCK) ON tfoc.ORDR_ID = foc.ORDR_ID AND tfoc.CIS_LVL_TYPE = foc.CIS_LVL_TYPE


UPDATE ocn 
SET ocn.FRST_NME = dbo.encryptString(RTRIM(LTRIM(COALESCE(tocn.FRST_NME,'')))),
ocn.LST_NME = dbo.encryptString(RTRIM(LTRIM(COALESCE(tocn.LST_NME,'')))),
ocn.EMAIL_ADR = dbo.encryptString(RTRIM(LTRIM(COALESCE(tocn.EMAIL_ADR,'')))),
ocn.CNTCT_NME = dbo.encryptString(RTRIM(LTRIM(COALESCE(tocn.CNTCT_NME,''))))
FROM 
dbo.ORDR_CNTCT ocn WITH (TABLOCKX) INNER JOIN
cnv.ORDR_CNTCT tocn WITH (NOLOCK) ON tocn.ORDR_CNTCT_ID = ocn.ORDR_CNTCT_ID


UPDATE oa
SET oa.STREET_ADR_1 = dbo.encryptString(RTRIM(LTRIM(COALESCE(toa.STREET_ADR_1,'')))),
oa.STREET_ADR_2 = dbo.encryptString(RTRIM(LTRIM(COALESCE(toa.STREET_ADR_2,'')))),
oa.CTY_NME = dbo.encryptString(RTRIM(LTRIM(COALESCE(toa.CTY_NME,'')))),
oa.PRVN_NME = dbo.encryptString(RTRIM(LTRIM(COALESCE(toa.PRVN_NME,'')))),
oa.STT_CD = dbo.encryptString(RTRIM(LTRIM(COALESCE(toa.STT_CD,'')))),
oa.ZIP_PSTL_CD = dbo.encryptString(RTRIM(LTRIM(COALESCE(toa.ZIP_PSTL_CD,'')))),
oa.BLDG_NME = dbo.encryptString(RTRIM(LTRIM(COALESCE(toa.BLDG_NME,'')))),
oa.FLR_ID = dbo.encryptString(RTRIM(LTRIM(COALESCE(toa.FLR_ID,'')))),
oa.RM_NBR = dbo.encryptString(RTRIM(LTRIM(COALESCE(toa.RM_NBR,''))))
FROM 
dbo.ORDR_ADR oa WITH (TABLOCKX) INNER JOIN
cnv.ORDR_ADR toa WITH (NOLOCK) ON toa.ORDR_ADR_ID = oa.ORDR_ADR_ID

