use COWS
go

Declare @ipl_disc table (ordr_id int)


			
--Scenario 3: (5477)	 		
-- disconnect pending order in xNCI milestones pending status
-- update profile from 207 to 213 in act_task and wg_prof_stus, update sm_id = 22, wg_ptrn_id = 609
insert into @ipl_disc (ordr_id)
Select distinct o.ORDR_ID
	From	dbo.FSA_ORDR ipl WITH (NOLOCK) 
inner join	dbo.ORDR o with (nolock) on ipl.ORDR_ID = o.ORDR_ID	
inner join	dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = o.PPRT_ID
	WHERE	lp.ORDR_TYPE_ID = 7
		and o.ORDR_STUS_ID != 5
		and	lp.SM_ID	IN	(22,28)
		and exists
			(
				select 'x'
					from	dbo.ACT_TASK at with (nolock)
					where	at.ORDR_ID = o.ORDR_ID
						and at.WG_PROF_ID = 107
						and	at.TASK_ID in (103)						
						and at.STUS_ID = 0	
			)
			
Select * From @ipl_disc

insert into dbo.WG_Prof_STUS (ORDR_ID,WG_PROF_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
select distinct ordr_id,214,609,0,GETDATE()
	from @ipl_disc ipld
	where not exists
		(
			select 'x'
				from WG_Prof_STUS wpts 
				where wpts.ORDR_ID = ipld.ordr_id
					and wpts.WG_PTRN_ID = 609
					and	wpts.WG_PROF_ID = 214
		)
		
insert into dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
Select distinct ordr_id,215,0,214,GETDATE()
	from @ipl_disc ipld
	Where not exists
		(
			Select 'x'
				From dbo.ACT_TASK at WITH (NOLOCK)
				Where	at.ORDR_ID		=	ipld.ordr_id
					and at.WG_PROF_ID	=	214
					and	at.TASK_ID		=	215
		)


	