USE COWS
GO

DECLARE @DEV_MODEL_ID INT
DECLARE @DEV_Sprint INT
SELECT @DEV_MODEL_ID = MAX(DEV_MODEL_ID)+1 FROM dbo.LK_DEV_MODEL with (nolock)
SELECT @DEV_Sprint = MANF_ID FROM dbo.LK_DEV_MANF with (nolock) where MANF_NME='Sprint'
IF NOT EXISTS(SELECT 'X' FROM [COWS].[dbo].[LK_DEV_MODEL] WITH (NOLOCK) WHERE dev_model_nme = 'SDV')
BEGIN
INSERT INTO [COWS].[dbo].[LK_DEV_MODEL] ([DEV_MODEL_id],[manF_id],[dev_model_nme],[min_drtn_tme_reqr_amt], [rec_stus_id],[creat_dt],[creat_by_user_id],[modfd_dt],[modfd_by_user_id]) values (@DEV_MODEL_ID,@DEV_Sprint,'SDV',60,1,getdate(),1,null, null)
END