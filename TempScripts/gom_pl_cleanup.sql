use cows
go

DECLARE @GOMPLTable Table (ORDR_ID Int)
Insert into @GOMPLTable (ORDR_ID)
--Select DISTINCT odr.ORDR_ID,odr.ORDR_CAT_ID,fsa.FTN,fsa.PROD_TYPE_CD, fsa.ORDR_TYPE_CD,lp.ORDR_TYPE_ID,lp.PROD_TYPE_ID
Select DISTINCT odr.ORDR_ID
	From	dbo.ORDR odr WITH (NOLOCK)
Inner Join	dbo.LK_PPRT lp with (nolock) ON lp.PPRT_ID = odr.PPRT_ID
Inner Join	dbo.WG_PTRN_STUS wpt with (nolock) on wpt.ORDR_ID  = odr.ORDR_ID
Inner Join	dbo.WG_PROF_STUS wpf with (nolock) on wpf.ORDR_ID = odr.ORDR_ID
Left Join	dbo.FSA_ORDR fsa with (nolock) on fsa.ordr_id = odr.ORDR_ID
	Where	wpt.WG_PTRN_ID = 609
		and wpt.STUS_ID = 0
		and odr.ORDR_STUS_ID IN (0,1)
		and not exists
			(
				select 'x'
					from dbo.ACT_TASK at with (nolock)
					where	at.ordr_id = odr.ORDR_ID
						and at.TASK_ID = 103
			)

--Insert GOM PL WG Profile ID
Insert into dbo.WG_PROF_STUS 
Select ORDR_ID, 107,609,0,GETDATE()
	From @GOMPLTable

--Insert GOM PL Task 
Insert into dbo.ACT_TASK 
Select ORDR_ID,103,5,107,GETDATE()
	From @GOMPLTable 

Update at
	set STUS_ID = 0
	From @GOMPLTable t 
	Inner Join dbo.ACT_TASK at with (rowlock) on t.ORDR_ID = at.ORDR_ID
	Where	at.TASK_ID = 103
		and at.STUS_ID = 5
		and exists
			(
				Select 'x'
					from dbo.ACT_TASK at1 with (nolock)
					where at1.ORDR_ID = at.ORDR_ID
						and at1.TASK_ID = 214
						and at1.STUS_ID = 0
			)
		and not exists
			(
				select 'x'
					from dbo.vndr_ordr vo with (nolock)
					where	vo.ordr_id = at.ORDR_ID
						and	isnull(BYPAS_VNDR_ORDR_MS_CD,'') = '1'
			)
		and not exists
			(
				select 'x'
					from dbo.ORDR_NTE ont with (nolock)
					where	ont.ORDR_ID = at.ORDR_ID 
						and (ont.NTE_TXT = 'bypassing xNCI Order Sent to Vendor Date milestone.' or ont.NTE_TXT = 'bypassing xNCI Order Acknowledged by Vendor Date milestone.')
			)





