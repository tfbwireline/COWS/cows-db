USE [COWS]
GO

UPDATE dbo.AD_EVENT WITH (TABLOCKX)
SET SCURD_CD = CASE WHEN (LEN(H1) > 0) 
						THEN (CASE WHEN ((SELECT TOP 1 ISNULL(ordr.CSG_LVL_CD,0)
										   FROM FSA_ORDR_CUST foc    
												INNER JOIN ORDR ordr ON ordr.ORDR_ID = foc.ORDR_ID    
										   WHERE foc.CIS_LVL_TYPE = 'H1'     
												AND foc.CUST_ID = H1    
										   ORDER BY ordr.ORDR_ID DESC)   = '00002')
									THEN 1 ELSE (CASE WHEN (FTN > 0)
										 THEN (CASE WHEN 
										 ((SELECT TOP 1 SCURD_CD
											 FROM dbo.ORDR od WITH (NOLOCK) INNER JOIN
												  dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID
											 WHERE fo.FTN = FTN
											 ORDER BY od.ORDR_ID DESC) = 1) 
										THEN 1 ELSE 0 END) ELSE 0 END) END)
					ELSE 0 END
WHERE ISNUMERIC(H1) = 1


UPDATE dbo.MPLS_EVENT WITH (TABLOCKX)
SET SCURD_CD = CASE WHEN (LEN(H1) > 0) 
						THEN (CASE WHEN ((SELECT TOP 1 ISNULL(ordr.CSG_LVL_CD,0)
										   FROM FSA_ORDR_CUST foc    
												INNER JOIN ORDR ordr ON ordr.ORDR_ID = foc.ORDR_ID    
										   WHERE foc.CIS_LVL_TYPE = 'H1'     
												AND foc.CUST_ID = H1    
										   ORDER BY ordr.ORDR_ID DESC)   = '00002')
									THEN 1 ELSE (CASE WHEN (FTN > 0)
										 THEN (CASE WHEN 
										 ((SELECT TOP 1 SCURD_CD
											 FROM dbo.ORDR od WITH (NOLOCK) INNER JOIN
												  dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID
											 WHERE fo.FTN = FTN
											 ORDER BY od.ORDR_ID DESC) = 1) 
										THEN 1 ELSE 0 END) ELSE 0 END) END)
					ELSE 0 END
WHERE ISNUMERIC(H1) = 1
					
UPDATE dbo.NGVN_EVENT WITH (TABLOCKX)
SET SCURD_CD = CASE WHEN (LEN(H1) > 0) 
						THEN (CASE WHEN ((SELECT TOP 1 ISNULL(ordr.CSG_LVL_CD,0)
										   FROM FSA_ORDR_CUST foc    
												INNER JOIN ORDR ordr ON ordr.ORDR_ID = foc.ORDR_ID    
										   WHERE foc.CIS_LVL_TYPE = 'H1'     
												AND foc.CUST_ID = H1    
										   ORDER BY ordr.ORDR_ID DESC)   = '00002')
									THEN 1 ELSE (CASE WHEN (FTN > 0)
										 THEN (CASE WHEN 
										 ((SELECT TOP 1 SCURD_CD
											 FROM dbo.ORDR od WITH (NOLOCK) INNER JOIN
												  dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID
											 WHERE fo.FTN = FTN
											 ORDER BY od.ORDR_ID DESC) = 1) 
										THEN 1 ELSE 0 END) ELSE 0 END) END)
					ELSE 0 END
WHERE ISNUMERIC(H1) = 1
					
UPDATE dbo.SPLK_EVENT WITH (TABLOCKX)
SET SCURD_CD = CASE WHEN (LEN(H1) > 0) 
						THEN (CASE WHEN ((SELECT TOP 1 ISNULL(ordr.CSG_LVL_CD,0)
										   FROM FSA_ORDR_CUST foc    
												INNER JOIN ORDR ordr ON ordr.ORDR_ID = foc.ORDR_ID    
										   WHERE foc.CIS_LVL_TYPE = 'H1'     
												AND foc.CUST_ID = H1    
										   ORDER BY ordr.ORDR_ID DESC)   = '00002')
									THEN 1 ELSE (CASE WHEN (FTN > 0)
										 THEN (CASE WHEN 
										 ((SELECT TOP 1 SCURD_CD
											 FROM dbo.ORDR od WITH (NOLOCK) INNER JOIN
												  dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID
											 WHERE fo.FTN = FTN
											 ORDER BY od.ORDR_ID DESC) = 1) 
										THEN 1 ELSE 0 END) ELSE 0 END) END)
					ELSE 0 END
WHERE ISNUMERIC(H1) = 1