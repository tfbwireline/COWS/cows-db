DECLARE @TBL table (ORDR_ID INT, CCD_DT DateTime, ORDR_CAT_ID INT)
INSERT INTO @TBL 
	SELECT odr.ORDR_ID,
				CASE odr.ORDR_CAT_ID
					WHEN 1 THEN ipl.CUST_CMMT_DT
					WHEN 2 THEN fsa.CUST_CMMT_DT
					WHEN 4 THEN ncco.CCS_DT
					END, odr.ORDR_CAT_ID
		FROM	dbo.ORDR odr WITH (NOLOCK)
			INNER JOIN dbo.LK_PPRT lp WITH (NOLOCK) ON lp.PPRT_ID = odr.PPRT_ID
			INNER JOIN dbo.LK_PROD_TYPE lpt WITH (NOLOCK) ON lpt.PROD_TYPE_ID = lp.PROD_TYPE_ID	
			LEFT JOIN dbo.FSA_ORDR fsa WITH (NOLOCK) ON fsa.ORDR_ID = odr.ORDR_ID
			LEFT JOIN dbo.IPL_ORDR ipl WITH (NOLOCK) ON ipl.ORDR_ID = odr.ORDR_ID
			LEFT JOIN dbo.NCCO_ORDR ncco WITH (NOLOCK) ON ncco.ORDR_ID = odr.ORDR_ID
			LEFT JOIN dbo.TRPT_ORDR tt WITH (NOLOCK) ON tt.ORDR_ID = odr.ORDR_ID
		WHERE	odr.DMSTC_CD = 1
			AND lpt.TRPT_CD = 1
			AND 
			(tt.ORDR_ID IS NULL
			OR EXISTS
				(
					SELECT 'X'
						FROM dbo.TRPT_ORDR trpt WITH (NOLOCK)
						WHERE	trpt.ORDR_ID = odr.ORDR_ID
							AND trpt.STDI_DT is NULL	
				)
			)
		ORDER BY odr.ORDR_ID 
			

UPDATE trpt
	SET trpt.STDI_DT = t.CCD_DT,
		trpt.STDI_REAS_ID = 1
		FROM dbo.TRPT_ORDR trpt WITH (ROWLOCK)
INNER JOIN @TBL t ON t.ORDR_ID = trpt.ORDR_ID	
		WHERE t.CCD_DT IS NOT NULL

INSERT INTO dbo.TRPT_ORDR (ORDR_ID,REC_STUS_ID,CREAT_BY_USER_ID,CREAT_DT,STDI_DT,STDI_REAS_ID)
SELECT t.ORDR_ID,1,1,GETDATE(),t.CCD_DT,1
	FROM @TBL t
	WHERE t.CCD_DT IS NOT NULL
		AND NOT EXISTS
			(
				SELECT 'X'
					FROM	dbo.TRPT_ORDR trpt WITH (NOLOCK)
					WHERE	trpt.ORDR_ID = t.ORDR_ID
						AND trpt.STDI_DT = t.CCD_DT
			)

INSERT INTO dbo.ORDR_STDI_HIST (ORDR_ID,STDI_REAS_ID,CREAT_BY_USER_ID,CREAT_DT,STDI_DT)
SELECT t.ORDR_ID,1,1,GETDATE(),t.CCD_DT
	FROM @TBL t
	WHERE t.CCD_DT IS NOT NULL
		AND NOT EXISTS
			(
				SELECT 'X'
					FROM	dbo.ORDR_STDI_HIST osh WITH (NOLOCK)
					WHERE	osh.ORDR_ID = t.ORDR_ID
						AND osh.STDI_DT = t.CCD_DT
			)



	