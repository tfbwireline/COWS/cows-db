USE COWS
GO

SET NOCOUNT ON;

ALTER TABLE dbo.DSPL_VW_COL NOCHECK CONSTRAINT ALL 
GO

declare @dsplvw table (vwid int, flag bit)

insert into @dsplvw
select DISTINCT DSPL_VW_ID, 0 FROM dbo.DSPL_VW WHERE CREAT_BY_USER_ID <> 1

declare @ctr int = 0
declare @cnt int = 0
declare @vwid int
declare @nvwid int

select @cnt = count(1) from @dsplvw

while (@ctr < @cnt)
begin

select top 1 @vwid = vwid
from @dsplvw
where flag = 0

set @nvwid = @vwid + 5000

update DSPL_VW_COL set DSPL_VW_ID = @nvwid where DSPL_VW_ID = @vwid
update DSPL_VW set DSPL_VW_ID = @nvwid where DSPL_VW_ID = @vwid

update @dsplvw set flag=1 where vwid = @vwid
set @ctr = @ctr + 1
end

ALTER TABLE dbo.DSPL_VW_COL CHECK CONSTRAINT ALL 
GO
