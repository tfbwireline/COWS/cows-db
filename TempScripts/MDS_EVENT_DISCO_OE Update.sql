-- Changes to the DISC_OE table based on the disconnect table rewrite

--Update VNDR_NME
UPDATE md 
SET md.VNDR_NME = ldm.MANF_NME
FROM MDS_EVENT_DISC_OE md 
	 INNER JOIN LK_DEV_MANF ldm on ldm.MANF_ID = md.MANF_ID

--Update Model_NME	 
update MDS_EVENT_DISC_OE set MODEL_NME = DEV_MODEL_NME
