USE COWS
GO

OPEN SYMMETRIC KEY FS@K3y       
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;      

UPDATE	fmds
	SET	fmds.US_INTL_ID = dbo.encryptString('D')
	From	dbo.MDS_EVENT mds WITH (NOLOCK)
Inner Join	dbo.FSA_MDS_EVENT fmds WITH (ROWLOCK) ON fmds.EVENT_ID = mds.EVENT_ID
	Where	mds.EVENT_STUS_ID NOT IN (6)
		AND dbo.decryptBinaryData(fmds.CTRY_RGN_NME) IN ('usaa','USA/West','USA/SW','USA/Pacific','usa/north east','USA/NE','usa/eastern','USA/East Coast','USA/EAST',
								'usa/ sw','USA/ North East','usa/ america','USA West','USA /East coast','USA /East','USA / West','USA / Eastern / Central / Central',
								'USA / Eastern','USA / East','USA / DOMESTIC','USA / Central','USA - Northwest','USA & International','USA',
								'US1`','US/SE','US/PST','US/Pacific','us/ne','US/mountain','US/Easternb','US/Eastern','US/East','US/domestic',
								'US/Central','US/ North East','US / Pacific','US / Central','US','Unities States','United States of Georgia',
								'United States of America','UNITED STATES','United Staes','U.S.A.','U.S.','U*SA','U S','Eastern USA')
		AND dbo.decryptBinaryData(fmds.US_INTL_ID) = 'I'



