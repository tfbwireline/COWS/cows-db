use COWS
go

declare @tbl table (ordr_id int)

insert into @tbl (ordr_id)
Select distinct at.ORDR_ID
	From	dbo.ACT_TASK at WITH (NOLOCK)
Inner Join	dbo.ORDR o WITH (NOLOCK) ON o.ORDR_ID = at.ORDR_ID
Inner Join	dbo.lk_pprt lp with (nolock) on lp.pprt_id = o.pprt_id
Inner join	dbo.fsa_ordr f with (nolock) on f.ordr_id = at.ordr_id
	Where	lp.Prod_type_id in (Select prod_type_id from dbo.lk_prod_type where FSA_PROD_TYPE_CD IN ('DN','MP','SN'))
		and lp.ordr_type_id in (select ordr_type_id from dbo.lk_ordr_type where FSA_ORDR_TYPE_CD = 'DC')
		and at.stus_id = 0
		and at.wg_prof_id = 207
		and at.TASK_ID = 215
		and exists
			(
				select 'x' from dbo.ORDR_NTE otn with (nolock)  where otn.ORDR_ID = o.ORDR_ID and otn.NTE_TXT = 'Move the order out from GOM Private Line - Pending to Complete state.'
			)
	


select * from @tbl

--update prodfile id from 207 to 214
update	at
	set at.WG_PROF_ID = 214
	from	dbo.act_task at with (rowlock)
inner join	@tbl t on t.ordr_id = at.ORDR_ID
	where	at.stus_id = 0
		and at.wg_prof_id = 207
		and at.TASK_ID = 215
		

update	at
	set at.WG_PROF_ID = 214
	from	dbo.WG_PROF_STUS at with (rowlock)
inner join	@tbl t on t.ordr_id = at.ORDR_ID
	where	at.stus_id = 0
		and at.wg_prof_id = 207
		and at.WG_PTRN_ID in (609,612) 
		
delete from @tbl

insert into @tbl (ordr_id)
Select distinct at.ORDR_ID
	From	dbo.ACT_TASK at WITH (NOLOCK)
Inner Join	dbo.ORDR o WITH (NOLOCK) ON o.ORDR_ID = at.ORDR_ID
Inner Join	dbo.lk_pprt lp with (nolock) on lp.pprt_id = o.pprt_id
Inner join	dbo.fsa_ordr f with (nolock) on f.ordr_id = at.ordr_id
	Where	lp.Prod_type_id in (Select prod_type_id from dbo.lk_prod_type where FSA_PROD_TYPE_CD IN ('DN','MP','SN'))
		and lp.ordr_type_id in (select ordr_type_id from dbo.lk_ordr_type where FSA_ORDR_TYPE_CD = 'DC')
		and at.stus_id = 0
		and at.wg_prof_id = 207
		and at.TASK_ID = 215
		and not exists
			(
				select 'x' from dbo.ORDR_NTE otn with (nolock)  where otn.ORDR_ID = o.ORDR_ID and otn.NTE_TXT = 'Move the order out from GOM Private Line - Pending to Complete state.'
			)
	
	
if((Select COUNT(1) from @tbl) > 0)
	begin
		delete	at
			from	dbo.act_task at with (rowlock)
		inner join	@tbl t on t.ordr_id = at.ORDR_ID
			where	at.stus_id = 0
				and at.wg_prof_id = 207
				and at.TASK_ID = 215
		
		update	at
			set at.WG_PROF_ID = 213
			from	dbo.ACT_TASK at with (rowlock)
		inner join	@tbl t on t.ordr_id = at.ORDR_ID
			where	at.wg_prof_id = 207
				and at.STUS_ID = 2
				
				
		update	at
			set at.WG_PROF_ID = 213
				,at.STUS_ID = 2	
			from	dbo.WG_PROF_STUS at with (rowlock)
		inner join	@tbl t on t.ordr_id = at.ORDR_ID
			where	at.stus_id = 0
				and at.wg_prof_id = 207
				and at.WG_PTRN_ID in (609,612) 
		
		insert into dbo.WG_PROF_STUS (ORDR_ID,WG_PROF_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
		select	Distinct t.ordr_id,107,609,0,GETDATE()
			from	@tbl t 
		inner join	dbo.WG_PROF_STUS wpf with (nolock) on	t.ordr_id = wpf.ORDR_ID
														and wpf.WG_PTRN_ID = 609
			where not exists
					(
						select 'x'
							from dbo.WG_PROF_STUS wpf1 with (nolock)
							where	wpf1.ORDR_ID	=	wpf.ORDR_ID
								and	wpf1.WG_PROF_ID =	107
					)											
			
		union all
		
		select	Distinct t.ordr_id,107,612,0,GETDATE()
			from	@tbl t 
		inner join	dbo.WG_PROF_STUS wpf with (nolock) on	t.ordr_id = wpf.ORDR_ID
														and wpf.WG_PTRN_ID = 612				
			where not exists
					(
						select 'x'
							from dbo.WG_PROF_STUS wpf1 with (nolock)
							where	wpf1.ORDR_ID	=	wpf.ORDR_ID
								and	wpf1.WG_PROF_ID =	107
					)											
		
		insert into dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
		select	Distinct t.ordr_id,103,0,107,GETDATE()
			from	@tbl t 
			where not exists
					(
						select 'x'
							from dbo.ACT_TASK at with (nolock)
							where	at.ORDR_ID	=	t.ORDR_ID
								and	at.WG_PROF_ID =	107
					)											
	end

delete from @tbl	

insert into @tbl
Select distinct at.ORDR_ID
	From	dbo.ACT_TASK at WITH (NOLOCK)
Inner Join	dbo.ORDR o WITH (NOLOCK) ON o.ORDR_ID = at.ORDR_ID
Inner Join	dbo.lk_pprt lp with (nolock) on lp.pprt_id = o.pprt_id
Inner join	dbo.fsa_ordr f with (nolock) on f.ordr_id = at.ordr_id
	Where	lp.Prod_type_id in (Select prod_type_id from dbo.lk_prod_type where FSA_PROD_TYPE_CD IN ('DN','MP','SN'))
		and lp.ordr_type_id in (select ordr_type_id from dbo.lk_ordr_type where FSA_ORDR_TYPE_CD = 'DC')
		and at.stus_id = 0
		and at.wg_prof_id = 207
		and at.TASK_ID between 200 and 214
		and not exists
			(
				select 'x' from dbo.ORDR_NTE otn with (nolock)  where otn.ORDR_ID = o.ORDR_ID and otn.NTE_TXT = 'Move the order out from GOM Private Line - Pending to Complete state.'
			)
	


if((select COUNT(1) from @tbl) > 0)
	begin
		update	wpf
			set		wpf.WG_PROF_ID = 213
			from	dbo.WG_PROF_STUS wpf with (rowlock)
		inner join	@tbl t on wpf.ORDR_ID = t.ordr_id
			where	wpf.WG_PROF_ID = 207
				and wpf.WG_PTRN_ID in (609,612)
				and wpf.STUS_ID = 0
		
		update	at
			set		at.WG_PROF_ID = 213
			from	dbo.ACT_TASK at with (rowlock)
		inner join	@tbl t on at.ORDR_ID = t.ordr_id
			where	at.WG_PROF_ID = 207
	end	
		
		