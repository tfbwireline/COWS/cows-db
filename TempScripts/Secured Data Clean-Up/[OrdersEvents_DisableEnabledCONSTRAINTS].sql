USE [COWS]
GO

/****** Object:  StoredProcedure [tmp].[OrdersEvents_DisableEnabledCONSTRAINTS]    Script Date: 12/09/2014 13:50:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

---- =============================================
---- Author:		Md. Monir
---- Create date:   12/09/2014
---- Description:	This SP is used to Single DELETE the Orders by PON ..
---- =============================================

--drop  PROCEDURE  [tmp].[OrdersEvents_DisableEnabledCONSTRAINTS]
ALTER PROCEDURE [tmp].[OrdersEvents_DisableEnabledCONSTRAINTS](
	 @Check   int
	)
AS

--DECLARE @Check   int
--SET   @Check = 2
BEGIN

/*
   0=Event Tables Disabale Constraints
   1=Event Tables Enabale Constraints
   2=Order Tables Disabale Constraints
   3=Order Tables Enabale Constraints
*/
/* 
SP  DELETEBatchOrders
All The Orders will be deleted base on criteria COWS.dbo.ORDR WITH (NOLOCK) Where SCURD_CD=1
The parameter is there for you to set How many records you want to delete per batch Example Records
In table is 500   and if your parameter is 100  it means it will delete 100 record per loop
This SP Calls
 SP [CheckDeleteTime]  to check the Time Constraint You set in the  COWS Table
SELECT *  FROM [COWS].[dbo].[LK_SYS_CFG] WITH (NOLOCK) WHERE cfg_id in (63,64)
It Will Also Call 
SP  [OrdersEvents_DisableEnabledCONSTRAINTS]   to Disable and Enable Constraints
On Related Tables so that we can execute delete.
It Will Eventually Call 
SP   DELETEOrdersByORDR_ID  for actual Operation. Which Deletes data as a whole batch  base on the parameter value  it receives
SP uses Proper Error Trapping and  Transactions So that for any failure it will either Rollback
Transaction

*/




BEGIN TRY

	SET NOCOUNT ON;
	SET	DEADLOCK_PRIORITY 10;

	IF	@Check = 0
		BEGIN
		PRINT 'Begin disabling Foreign Keys for the Tables in the script at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
	
		--ALTER TABLE [COWS].[dbo].ACT_TASK					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].AccessDelivery				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].AD_EVENT					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].AD_EVENT_ACCS_TAG			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].APPT						NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EMAIL_REQ					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT 						NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT_ASN_TO_USER			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT_HIST					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT_NVLD_TME_SLOT		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT_REC_LOCK				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_CNTCT		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_ADR			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_MSG			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_CFGRN_DATA	NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_PRCS			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_TADPOLE_DATA	NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_USER_DATA	NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_REQ_ACT_HIST		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT_NEW			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT_ORDR			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].L2P_LOG					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_ACCS				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_CPE				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_DSL_TRPT			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_FMS_CKT			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNGD_DEV			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNS				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNS_OE			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_ODIE_DEV_NME		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_PVC				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_SPAE_TRPT		NOCHECK CONSTRAINT ALL 
		ALTER TABLE [COWS].[dbo].MDS_EVENT_SPRINT_LINK_ATM_TRPT NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_VLAN				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_VRTL_CNCTN		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_WIRED_TRNSPRT_DEV	NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_WIRED_TRPT		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_WRLS_TRNSPRT_DEV NOCHECK CONSTRAINT ALL	
		ALTER TABLE [COWS].[dbo].MDS_EVENT_WRLS_TRPT		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_DISCO			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_LOC				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MAC_ACTY			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_NEW				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_MNGD_ACT				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_MNGD_ACT_NEW			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].MDSEvent					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].MDSFastTrack				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MPLS_EVENT					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MPLS_EVENT_ACCS_TAG		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MPLS_EVENT_ACTY_TYPE		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MPLS_EVENT_VAS_TYPE		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].MPLSEvent					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].NGVN_EVENT					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].NGVN_EVENT_CKT_ID_NUA		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].NGVN_EVENT_SIP_TRNK		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].NGVNEvent					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_RSPN_INFO				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].owssvr$					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].SPLK_EVENT					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].SPLK_EVENT_ACCS			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].SprintLink					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].WFHistory					NOCHECK CONSTRAINT ALL
		PRINT 'Finished disabling Foreign Keys for the Tables in the script at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
		END
	IF	@Check = 1
		BEGIN
		PRINT 'Begin Enabling Foreign Keys for the Tables in the script at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
	
		--ALTER TABLE [COWS].[dbo].ACT_TASK					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].AccessDelivery				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].AD_EVENT					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].AD_EVENT_ACCS_TAG			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].APPT						CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EMAIL_REQ					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT 						CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT_ASN_TO_USER			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT_HIST					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT_NVLD_TME_SLOT		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EVENT_REC_LOCK				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_CNTCT		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_ADR			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_MSG			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_CFGRN_DATA	CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_PRCS			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_TADPOLE_DATA	CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_EVENT_USER_DATA	CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FEDLINE_REQ_ACT_HIST		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT_NEW			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT_ORDR			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].L2P_LOG					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_ACCS				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_CPE				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_DSL_TRPT			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_FMS_CKT			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNGD_DEV			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNS				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNS_OE			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_ODIE_DEV_NME		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_PVC				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_SPAE_TRPT		CHECK CONSTRAINT ALL 
		ALTER TABLE [COWS].[dbo].MDS_EVENT_SPRINT_LINK_ATM_TRPT CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_VLAN				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_VRTL_CNCTN		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_WIRED_TRNSPRT_DEV	CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_WIRED_TRPT		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_WRLS_TRNSPRT_DEV CHECK CONSTRAINT ALL	
		ALTER TABLE [COWS].[dbo].MDS_EVENT_WRLS_TRPT		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].L2P_LOG					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_DISCO			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_LOC				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MAC_ACTY			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_NEW				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_MNGD_ACT				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_MNGD_ACT_NEW			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].MDSEvent					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].MDSFastTrack				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MPLS_EVENT					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MPLS_EVENT_ACCS_TAG		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MPLS_EVENT_ACTY_TYPE		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MPLS_EVENT_VAS_TYPE		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].MPLSEvent					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].NGVN_EVENT					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].NGVN_EVENT_CKT_ID_NUA		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].NGVN_EVENT_SIP_TRNK		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].NGVNEvent					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_RSPN_INFO				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].owssvr$					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].SPLK_EVENT					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].SPLK_EVENT_ACCS			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].SprintLink					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].WFHistory					CHECK CONSTRAINT ALL
		PRINT 'Finished enabling Foreign Keys for the Tables in the script at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
		END
	IF	@Check = 2
		BEGIN

		PRINT 'Begin disabling Foreign Keys for the Tables in the script at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
	
		ALTER TABLE [COWS].[dbo].ACT_TASK					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CCD_HIST					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CCD_HIST_REAS              NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CKT						NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].AD_EVENT_ACCS_TAG			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CKT_COST					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CKT_MS						NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_FMS_CKT			NOCHECK CONSTRAINT ALL	
		ALTER TABLE [COWS].[dbo].MPLS_ACCS					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EMAIL_REQ					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_IP_ADR					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT_ORDR			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_BILL_LINE_ITEM	NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_CPE_LINE_ITEM		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_CSC				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_CUST				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_CUST				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_GOM_XNCI			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_MSG				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_RELTD_ORDR		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_VAS				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].IPL_ORDR					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].IPL_ORDR_ACCS_INFO			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].IPL_ORDR_CKT				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].IPL_ORDR_EQPT				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].L2P_LOG					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_ACCS				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_CPE				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNGD_DEV			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNS				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNS_OE			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].NCCO_ORDR					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_REQ					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT_ORDR			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_MNGD_ACT				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_MNGD_ACT_NEW			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_DISC_REQ				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_RSPN					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_RSPN_INFO				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR						NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_ADR					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_CKT_CHG				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_CNTCT					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_EXP					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_GRP					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_H5_DOC				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_HOLD_MS				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_JPRDY					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_MS					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_NTE					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_REC_LOCK				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_STDI_HIST				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].ORDR_Tmp					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_VLAN					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].SIP_TRNK_GRP				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].TRPT_ORDR					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].USER_WFM_ASMT				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR					NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_EMAIL			NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_EMAIL_ATCHMT		NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_MS				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_FORM				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_MS				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].WG_PROF_STUS				NOCHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].WG_PTRN_STUS				NOCHECK CONSTRAINT ALL
		PRINT 'Finished disabling Foreign Keys for the Tables in the script at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
		END
	IF	@Check = 3
		BEGIN

		PRINT 'Begin enabling Foreign Keys for the Tables in the script at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
	
		ALTER TABLE [COWS].[dbo].ACT_TASK					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CCD_HIST					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CCD_HIST_REAS              CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CKT						CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].AD_EVENT_ACCS_TAG			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CKT_COST					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].CKT_MS						CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_FMS_CKT			CHECK CONSTRAINT ALL	
		ALTER TABLE [COWS].[dbo].MPLS_ACCS					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].EMAIL_REQ					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_IP_ADR					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT_ORDR			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_BILL_LINE_ITEM	CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_CPE_LINE_ITEM		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_CSC				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_CUST				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_CUST				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_GOM_XNCI			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_MSG				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_RELTD_ORDR		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_ORDR_VAS				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].IPL_ORDR					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].IPL_ORDR_ACCS_INFO			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].IPL_ORDR_CKT				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].IPL_ORDR_EQPT				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].L2P_LOG					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_ACCS				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_CPE				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNGD_DEV			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNS				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_EVENT_MNS_OE			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].NCCO_ORDR					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_REQ					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].FSA_MDS_EVENT_ORDR			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_MNGD_ACT				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].MDS_MNGD_ACT_NEW			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_DISC_REQ				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_RSPN					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ODIE_RSPN_INFO				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR						CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_ADR					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_CKT_CHG				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_CNTCT					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_EXP					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_GRP					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_H5_DOC				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_HOLD_MS				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_JPRDY					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_MS					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_NTE					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_REC_LOCK				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_STDI_HIST				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[cnv].ORDR_Tmp					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].ORDR_VLAN					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].SIP_TRNK_GRP				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].TRPT_ORDR					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].USER_WFM_ASMT				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR					CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_EMAIL			CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_EMAIL_ATCHMT		CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_MS				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_FORM				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].VNDR_ORDR_MS				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].WG_PROF_STUS				CHECK CONSTRAINT ALL
		ALTER TABLE [COWS].[dbo].WG_PTRN_STUS				CHECK CONSTRAINT ALL
		PRINT 'Finished enabling Foreign Keys for the Tables in the script at: ' + CONVERT(VARCHAR(30), GETDATE(), 9)
		END
	ELSE
		BEGIN
		PRINT 'Do Nothing : ' + CONVERT(VARCHAR(30), GETDATE(), 9)
		END
	
	SET	DEADLOCK_PRIORITY 0;

END TRY

BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
	SET DEADLOCK_PRIORITY 0; 
END CATCH

END



GO


