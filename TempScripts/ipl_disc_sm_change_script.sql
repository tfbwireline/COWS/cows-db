use COWS
go

Declare @ipl_disc table (ordr_id int)

----Scenario 1: (5476)
---- disconnect pending order in GOM approval pending status
---- delete this profile and load profile 213, update sm_id = 22, wg_ptrn_id = 609
--insert into @ipl_disc (ordr_id)
--Select distinct o.ORDR_ID
--	From	dbo.IPL_ORDR ipl WITH (NOLOCK) 
--inner join	dbo.ORDR o with (nolock) on ipl.ORDR_ID = o.ORDR_ID	
--inner join	dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = o.PPRT_ID
--	WHERE	ipl.ORDR_TYPE_ID = 7
--		and o.ORDR_STUS_ID != 5
--		and exists
--			(
--				select 'x'
--					from	dbo.ACT_TASK at with (nolock)
--					where	at.ORDR_ID = o.ORDR_ID
--						and at.WG_PROF_ID = 103
--						and at.STUS_ID = 0	
--			)

--delete wpts
--	from	dbo.WG_PTRN_STUS wpts with (rowlock)
--inner join	@ipl_disc ipld on ipld.ordr_id = wpts.ORDR_ID
--	where	wpts.WG_PTRN_ID = 103

--insert into dbo.WG_PTRN_STUS (ORDR_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
--select ordr_id,609,0,GETDATE()
--	from @ipl_disc


--delete wpfs
--	from	dbo.WG_Prof_STUS wpfs with (rowlock)
--inner join	@ipl_disc ipld on ipld.ordr_id = wpfs.ORDR_ID
--	where	wpfs.WG_Prof_ID = 103

--insert into dbo.WG_Prof_STUS (ORDR_ID,WG_PROF_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
--select ordr_id,213,609,0,GETDATE()
--	from @ipl_disc
				

--delete act
--	from	dbo.ACT_TASK act with (rowlock)
--inner join	@ipl_disc ipld on ipld.ordr_id = act.ORDR_ID
--	where	act.WG_Prof_ID = 103

--insert into dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
--select ordr_id,200,0,213,GETDATE()
--	from @ipl_disc


--delete from @ipl_disc	


----Scenario 2: (5320)
---- disconnect pending order in xNCI approval pending status
---- delete this profile and load profile 213, update sm_id = 22, wg_ptrn_id = 609
--insert into @ipl_disc (ordr_id)
--Select distinct o.ORDR_ID
--	From	dbo.IPL_ORDR ipl WITH (NOLOCK) 
--inner join	dbo.ORDR o with (nolock) on ipl.ORDR_ID = o.ORDR_ID	
--inner join	dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = o.PPRT_ID
--	WHERE	ipl.ORDR_TYPE_ID = 7
--		and o.ORDR_STUS_ID != 5
--		and exists
--			(
--				select 'x'
--					from	dbo.ACT_TASK at with (nolock)
--					where	at.ORDR_ID = o.ORDR_ID
--						and at.WG_PROF_ID = 201
--						and at.STUS_ID = 0	
--			)
			
--delete wpts
--	from	dbo.WG_PTRN_STUS wpts with (rowlock)
--inner join	@ipl_disc ipld on ipld.ordr_id = wpts.ORDR_ID
--	where	wpts.WG_PTRN_ID in (201,103)

--insert into dbo.WG_PTRN_STUS (ORDR_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
--select ordr_id,609,0,GETDATE()
--	from @ipl_disc ipld
--	where not exists
--		(
--			select 'x'
--				from WG_PTRN_STUS wpts 
--				where wpts.ORDR_ID = ipld.ordr_id
--					and wpts.WG_PTRN_ID = 609
--		)


--delete wpfs
--	from	dbo.WG_Prof_STUS wpfs with (rowlock)
--inner join	@ipl_disc ipld on ipld.ordr_id = wpfs.ORDR_ID
--	where	wpfs.WG_Prof_ID in (103,201)

--insert into dbo.WG_Prof_STUS (ORDR_ID,WG_PROF_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
--select ordr_id,213,609,0,GETDATE()
--	from @ipl_disc ipld
--	where not exists
--		(
--			select 'x'
--				from WG_Prof_STUS wpts 
--				where wpts.ORDR_ID = ipld.ordr_id
--					and wpts.WG_PTRN_ID = 609
--		)
				

--delete act
--	from	dbo.ACT_TASK act with (rowlock)
--inner join	@ipl_disc ipld on ipld.ordr_id = act.ORDR_ID
--	where	act.WG_Prof_ID in (103,201)

--insert into dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
--select ordr_id,200,0,213,GETDATE()
--	from @ipl_disc ipld
--	where not exists
--		(
--			select 'x'
--				from ACT_TASK act
--				where act.ORDR_ID = ipld.ordr_id
--					and act.WG_Prof_ID = 213
--		)	

--delete from @ipl_disc
			
--Scenario 3: (5477)	 		
-- disconnect pending order in xNCI milestones pending status
-- update profile from 207 to 213 in act_task and wg_prof_stus, update sm_id = 22, wg_ptrn_id = 609
insert into @ipl_disc (ordr_id)
Select distinct o.ORDR_ID
	From	dbo.IPL_ORDR ipl WITH (NOLOCK) 
inner join	dbo.ORDR o with (nolock) on ipl.ORDR_ID = o.ORDR_ID	
inner join	dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = o.PPRT_ID
	WHERE	ipl.ORDR_TYPE_ID = 7
		and o.ORDR_STUS_ID != 5
		and exists
			(
				select 'x'
					from	dbo.ACT_TASK at with (nolock)
					where	at.ORDR_ID = o.ORDR_ID
						and at.WG_PROF_ID = 207
						and	at.TASK_ID in (200,201,202,214)						
						and at.STUS_ID = 0	
			)
			
delete wpts
	from	dbo.WG_PTRN_STUS wpts with (rowlock)
inner join	@ipl_disc ipld on ipld.ordr_id = wpts.ORDR_ID
	where	wpts.WG_PTRN_ID in (207)

insert into dbo.WG_PTRN_STUS (ORDR_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
select distinct ordr_id,609,0,GETDATE()
	from @ipl_disc ipld
	where not exists
		(
			select 'x'
				from WG_PTRN_STUS wpts 
				where wpts.ORDR_ID = ipld.ordr_id
					and wpts.WG_PTRN_ID = 609
		)


delete wpfs
	from	dbo.WG_Prof_STUS wpfs with (rowlock)
inner join	@ipl_disc ipld on ipld.ordr_id = wpfs.ORDR_ID
	where	wpfs.WG_Prof_ID in (207)

insert into dbo.WG_Prof_STUS (ORDR_ID,WG_PROF_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
select distinct ordr_id,213,609,0,GETDATE()
	from @ipl_disc ipld
	where not exists
		(
			select 'x'
				from WG_Prof_STUS wpts 
				where wpts.ORDR_ID = ipld.ordr_id
					and wpts.WG_PTRN_ID = 609
		)

--delete act
--	from	dbo.ACT_TASK act with (rowlock)
--inner join	@ipl_disc ipld on ipld.ordr_id = act.ORDR_ID
--	where	act.WG_Prof_ID in (103,201)


update 	act
	set act.WG_PROF_ID = 213
	from	dbo.ACT_TASK act with (rowlock)
inner join	@ipl_disc ipld on ipld.ordr_id = act.ORDR_ID
	where	act.WG_Prof_ID = 207


INSERT INTO ORDR_NTE WITH (ROWLOCK)(NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
SELECT distinct 9,ordr_id,GETDATE(),38,1,'Modified SM'
	From @ipl_disc

delete from @ipl_disc
			

--Scenario 4: (5479)
--disconnect pending order in xNCI disconnected pending status
-- delete act_task with this pending task, update sm_id = 22, wg_ptrn_id = 609, load wg_prof_id = 107 in wg_ptrn_stus and act_task table
insert into @ipl_disc (ordr_id)
Select distinct o.ORDR_ID
	From	dbo.IPL_ORDR ipl WITH (NOLOCK) 
inner join	dbo.ORDR o with (nolock) on ipl.ORDR_ID = o.ORDR_ID	
inner join	dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = o.PPRT_ID
	WHERE	ipl.ORDR_TYPE_ID = 7
		and o.ORDR_STUS_ID != 5
		and exists
			(
				select 'x'
					from	dbo.ACT_TASK at with (nolock)
					where	at.ORDR_ID = o.ORDR_ID
						and at.WG_PROF_ID = 207
						and	at.TASK_ID = 215						
						and at.STUS_ID = 0	
			)


Update wpts
	Set wpts.WG_PTRN_ID = 609
	from	dbo.WG_PTRN_STUS wpts with (rowlock)
inner join	@ipl_disc ipld on ipld.ordr_id = wpts.ORDR_ID
	where	wpts.WG_PTRN_ID in (207)

--insert into dbo.WG_PTRN_STUS (ORDR_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
--select ordr_id,609,0,GETDATE()
--	from @ipl_disc ipld
--	where not exists
--		(
--			select 'x'
--				from WG_PTRN_STUS wpts 
--				where wpts.ORDR_ID = ipld.ordr_id
--					and wpts.WG_PTRN_ID = 609
--		)


delete wpfs
	from	dbo.WG_Prof_STUS wpfs with (rowlock)
inner join	@ipl_disc ipld on ipld.ordr_id = wpfs.ORDR_ID
	where	wpfs.WG_Prof_ID in (207)

insert into dbo.WG_Prof_STUS (ORDR_ID,WG_PROF_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
select distinct ordr_id,213,609,2,GETDATE()
	from @ipl_disc ipld
	where not exists
		(
			select 'x'
				from WG_Prof_STUS wpts 
				where wpts.ORDR_ID = ipld.ordr_id
					and wpts.WG_PTRN_ID = 609
					and wpts.WG_PROF_ID = 213
		)

insert into dbo.WG_Prof_STUS (ORDR_ID,WG_PROF_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
select distinct ordr_id,214,609,0,GETDATE()
	from @ipl_disc ipld
	where not exists
		(
			select 'x'
				from WG_Prof_STUS wpts 
				where wpts.ORDR_ID = ipld.ordr_id
					and wpts.WG_PTRN_ID = 609
					and wpts.WG_PROF_ID = 214
		)


insert into dbo.WG_Prof_STUS (ORDR_ID,WG_PROF_ID,WG_PTRN_ID,STUS_ID,CREAT_DT)
select distinct ordr_id,107,609,0,GETDATE()
	from @ipl_disc ipld
	where not exists
		(
			select 'x'
				from WG_Prof_STUS wpts 
				where wpts.ORDR_ID = ipld.ordr_id
					and wpts.WG_PTRN_ID = 609
					and wpts.WG_PROF_ID = 107
		)
		

--delete act
--	from	dbo.ACT_TASK act with (rowlock)
--inner join	@ipl_disc ipld on ipld.ordr_id = act.ORDR_ID
--	where	act.WG_Prof_ID in (103,201)		

Update act
	Set	act.WG_PROF_ID = 214
	from	dbo.ACT_TASK	act with (rowlock)
inner join	@ipl_disc	ipld on ipld.ordr_id = act.ORDR_ID
	where	act.WG_Prof_ID = 207
		and act.TASK_ID = 215

	
update 	act
	set act.WG_PROF_ID = 213
	from	dbo.ACT_TASK act with (rowlock)
inner join	@ipl_disc ipld on ipld.ordr_id = act.ORDR_ID
	where	act.WG_Prof_ID = 207
		AND act.TASK_ID != 215
	

insert into dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
select distinct ordr_id,103,0,107,GETDATE()
	from @ipl_disc ipld
	where not exists
		(
			select 'x'
				from ACT_TASK act with (nolock)
				where act.ORDR_ID = ipld.ordr_id
					and act.TASK_ID = 103
		)
	
INSERT INTO ORDR_NTE WITH (ROWLOCK)(NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
SELECT distinct 9,ordr_id,GETDATE(),38,1,'Modified SM'
	From @ipl_disc