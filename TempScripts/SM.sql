use cows
go

select lp.PPRT_ID, lp.PPRT_NME, ls.SM_ID, ls.SM_DES, count(1) as cnt
from dbo.LK_PPRT lp with (nolock) inner join
dbo.LK_SM ls with (nolock) on ls.SM_ID=lp.SM_ID inner join
dbo.ordr ord with (nolock) on ord.PPRT_ID=lp.PPRT_ID
where ord.ORDR_CAT_ID!=2
group by lp.PPRT_ID, lp.PPRT_NME, ls.SM_ID, ls.SM_DES
order by count(1)

--select top 100 fo.*
--from dbo.fsa_ordr fo with (nolock) inner join
--dbo.ordr ord with (nolock) on ord.ordr_id=fo.ordr_id
--where ord.ORDR_CAT_ID=6
--and ord.PPRT_ID=627
--and ord.ORDR_STUS_ID=2
--order by 1 desc

select * from dbo.sm with (nolock) where sm_id=52--pprt_id=627
select * from dbo.WG_PTRN_SM with (nolock) where WG_PTRN_ID in (200,215,1000)--pprt_id=627
select * from dbo.WG_PROF_SM with (nolock) where WG_PROF_ID in (200,204,216,1000)--pprt_id=627

declare @smidtbl table (id int identity(1,1), smid smallint, pptrnid smallint, dptrnid smallint, flag bit)
declare @smtbl table (id int identity(1,1), smid smallint, profid smallint, ptaskid smallint, dtaskid smallint, optid int, flag bit)
declare @ptrntbl table (id int identity(1,1), smid smallint, ptrnid smallint, pprofid smallint, dprofid smallint, flag bit)
declare @endtask table (smid smallint, profid smallint, ptaskid smallint, dtaskid smallint, optid smallint)
declare @smid smallint, @prptrnid smallint, @dptrnid smallint, @prprdid smallint, @dprfid smallint, @cnt1 int, @ctr1 int=1, @cnt2 int, @ctr2 int=1

insert into @smidtbl (smid, pptrnid, dptrnid, flag)
select distinct sm_id, PRE_REQST_WG_PTRN_ID, DESRD_WG_PTRN_ID, 0 from dbo.sm with (nolock) where SM_ID=60
select @cnt1 = count(1) from @smidtbl
while (@ctr1 <= @cnt1)
begin
	select top 1 @smid = smid,
	@prptrnid = pptrnid,
	@dptrnid = dptrnid
	from @smidtbl where id=@ctr1 and flag=0

	if (@prptrnid = 0)
	begin
		insert into @smtbl (smid, profid, ptaskid, dtaskid, optid, flag)
		select distinct @smid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
		from dbo.sm with (nolock) inner join
		dbo.WG_PTRN_SM wps1 with (nolock) on wps1.WG_PTRN_ID = sm.DESRD_WG_PTRN_ID inner join
		dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = wps1.DESRD_WG_PROF_ID
		where sm_id=@smid
		and sm.PRE_REQST_WG_PTRN_ID=0
		and sm.DESRD_WG_PTRN_ID = @dptrnid
		and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)
	end
	--select @dptrnid
	if (@dptrnid != 1)
	begin
		insert into @ptrntbl (smid, ptrnid, pprofid, dprofid, flag)
		select distinct sm.SM_ID, @dptrnid, wps1.PRE_REQST_WG_PROF_ID, wps1.DESRD_WG_PROF_ID, 0
		from dbo.sm sm with (nolock) inner join
			 dbo.WG_PTRN_SM wps1 with (nolock) on sm.DESRD_WG_PTRN_ID=wps1.WG_PTRN_ID 
		where sm.PRE_REQST_WG_PTRN_ID=@dptrnid
		  and sm.SM_ID=@smid

		select @cnt2 = count(1) from @ptrntbl
		while (@ctr2<=@cnt2)
		begin

		select top 1 @prprdid=pprofid, @dprfid = dprofid
		from @ptrntbl
		where flag=0 and id=@ctr2
		--select @prprdid, @dprfid
		if (@prprdid = 0)
		begin
			delete from @endtask
			insert into @endtask (smid, profid, ptaskid, dtaskid, optid)
			select distinct smid, profid, ptaskid, 1, optid
						  from @smtbl
						  where dtaskid=1

			--assign a desired taskid to the ending taskid from prior pattern/profile
			update st
			set dtaskid = x.DESRD_TASK_ID
			from @smtbl st inner join
			(select top 1 wpr1.desrd_task_id, wpr1.OPT_ID, pt.smid
			from @ptrntbl pt inner join
				dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = pt.dprofid
			where pt.pprofid=0
			  and wpr1.PRE_REQST_TASK_ID=0) as x
			on st.dtaskid=1 and st.optid=x.OPT_ID and st.smid=x.smid

			--load all prereq/starting tasks for that desired pattern
			insert into @smtbl (smid, ptaskid, dtaskid, optid, flag)
			select distinct @smid, et.ptaskid, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
			from @endtask et inner join
				 @ptrntbl pt on et.smid=pt.smid inner join
				dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = pt.dprofid
			where pt.pprofid=0
			  and wpr1.PRE_REQST_TASK_ID=0
			  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=et.ptaskid and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)
		end

		if (@dprfid != 1)
		begin
			update st
			set dtaskid = x.DESRD_TASK_ID
			from @smtbl st inner join
			(select top 1 wpr1.desrd_task_id, wpr1.OPT_ID, pt.smid
			from @ptrntbl pt inner join
				dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = pt.dprofid
			where pt.pprofid=@dprfid
			  and wpr1.PRE_REQST_TASK_ID=0) as x
			on st.dtaskid=1 and st.optid=x.OPT_ID and st.smid=x.smid

			select * from @ptrntbl where pprofid=@dprfid

			--load all non-prereq/starting tasks for that desired pattern
			insert into @smtbl (smid, profid, ptaskid, dtaskid, optid, flag)
			select distinct @smid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
			from @ptrntbl pt inner join
				 dbo.WG_PROF_SM wpr1 with (nolock) on pt.dprofid=wpr1.WG_PROF_ID
			where pt.pprofid=@dprfid
			  and wpr1.PRE_REQST_TASK_ID!=0
			  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)

			--delete from @endtask
			--insert into @endtask (smid, profid, ptaskid, dtaskid, optid)
			--select distinct smid, profid, ptaskid, 1, optid
			--			  from @smtbl
			--			  where dtaskid=1

			----non-prereq and non-desired/end profiles
			--insert into @smtbl (smid, profid, ptaskid, dtaskid, optid, flag)
			--select distinct @smid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
			--from dbo.sm sm with (nolock) inner join
			--	 dbo.WG_PTRN_SM wps1 with (nolock) on sm.DESRD_WG_PTRN_ID=wps1.WG_PTRN_ID inner join
			--	 dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = wps1.PRE_REQST_WG_PROF_ID inner join
			--	 dbo.WG_PROF_SM wpr2 with (nolock) on wpr2.WG_PROF_ID = wps1.DESRD_WG_PROF_ID
			--where sm.PRE_REQST_WG_PTRN_ID=@dptrnid
			--  and sm.SM_ID=@smid
			--  and wps1.PRE_REQST_WG_PROF_ID=0
			--  and wpr1.PRE_REQST_TASK_ID=0
			--  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)

			end

			if (@dprfid=1)
			begin
				insert into @smtbl (smid, profid, ptaskid, dtaskid, optid, flag)
				select distinct @smid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
				from @ptrntbl pt inner join
					 dbo.WG_PROF_SM wpr1 with (nolock) on pt.pprofid=wpr1.WG_PROF_ID
				where pt.dprofid=@dprfid
				  and wpr1.PRE_REQST_TASK_ID!=0
				  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)
			end

		  update @ptrntbl set flag=1 where id=@ctr2 and flag=0
		  set @ctr2 = @ctr2 + 1
		end
	end
	update @smidtbl set flag=1 where id=@ctr1 and flag=0
	set @ctr1 = @ctr1 + 1
end
select * from @smtbl

declare @smid smallint=60
--get initial tasks
select distinct wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID
from dbo.sm with (nolock) inner join
dbo.WG_PTRN_SM wps1 with (nolock) on wps1.WG_PTRN_ID = sm.DESRD_WG_PTRN_ID inner join
dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = wps1.DESRD_WG_PROF_ID
where sm_id=@smid
and sm.PRE_REQST_WG_PTRN_ID=0
union--get intermediate tasks
select distinct wpr2.PRE_REQST_TASK_ID, wpr2.DESRD_TASK_ID, wpr2.OPT_ID
from dbo.sm with (nolock) inner join
dbo.WG_PTRN_SM wps1 with (nolock) on wps1.WG_PTRN_ID = sm.PRE_REQST_WG_PTRN_ID inner join
dbo.WG_PTRN_SM wps2 with (nolock) on wps2.WG_PTRN_ID = sm.DESRD_WG_PTRN_ID inner join
dbo.WG_PROF_SM wpr2 with (nolock) on wpr2.WG_PROF_ID = wps1.DESRD_WG_PROF_ID
where sm_id=60
and sm.PRE_REQST_WG_PTRN_ID!=0 and sm.DESRD_WG_PTRN_ID!=1
union
select distinct wpr3.PRE_REQST_TASK_ID, wpr3.DESRD_TASK_ID, wpr3.OPT_ID
from dbo.sm with (nolock) inner join
dbo.WG_PTRN_SM wps1 with (nolock) on wps1.WG_PTRN_ID = sm.PRE_REQST_WG_PTRN_ID inner join
dbo.WG_PTRN_SM wps2 with (nolock) on wps2.WG_PTRN_ID = sm.DESRD_WG_PTRN_ID inner join
dbo.WG_PROF_SM wpr3 with (nolock) on wpr3.WG_PROF_ID = wps2.PRE_REQST_WG_PROF_ID
where sm_id=60
and sm.PRE_REQST_WG_PTRN_ID!=0 and sm.DESRD_WG_PTRN_ID!=1
union
select distinct wpr4.PRE_REQST_TASK_ID, wpr4.DESRD_TASK_ID, wpr4.OPT_ID
from dbo.sm with (nolock) inner join
dbo.WG_PTRN_SM wps1 with (nolock) on wps1.WG_PTRN_ID = sm.PRE_REQST_WG_PTRN_ID inner join
dbo.WG_PTRN_SM wps2 with (nolock) on wps2.WG_PTRN_ID = sm.DESRD_WG_PTRN_ID inner join
dbo.WG_PROF_SM wpr4 with (nolock) on wpr4.WG_PROF_ID = wps2.DESRD_WG_PROF_ID
where sm_id=60
and sm.PRE_REQST_WG_PTRN_ID!=0 and sm.DESRD_WG_PTRN_ID=1

---------------
--Final
---------------
declare @smidtbl table (id int identity(1,1), smid smallint, pptrnid smallint, dptrnid smallint, flag bit)
declare @smtbl table (id int identity(1,1), smid smallint, ptrnid smallint, profid smallint, ptaskid smallint, dtaskid smallint, optid int, flag bit)
declare @ptrntbl table (id int identity(1,1), smid smallint, ptrnid smallint, pprofid smallint, dprofid smallint, flag bit)
--declare @prftbl table (id int identity(1,1), smid smallint, prfid smallint, ptskid smallint, dtskid smallint, optid int, flag bit)
declare @endtask table (smid smallint, ptrnid smallint, profid smallint, ptaskid smallint, dtaskid smallint, optid smallint)
declare @smid smallint, @ptrnid smallint, @prptrnid smallint, @dptrnid smallint, @prprdid smallint, @dprfid smallint, @cnt1 int, @ctr1 int=1, @cnt2 int, @ctr2 int=1

insert into @smidtbl (smid, pptrnid, dptrnid, flag)
select distinct sm_id, PRE_REQST_WG_PTRN_ID, DESRD_WG_PTRN_ID, 0 from dbo.sm with (nolock) where sm_id=64
--where SM_ID in (54,
--65,
--64,
--67,
--66,
--52,
--63,55,60)
select @cnt1 = count(1) from @smidtbl
while (@ctr1 <= @cnt1)
begin
	select top 1 @smid = smid,
	@prptrnid = pptrnid,
	@dptrnid = dptrnid
	from @smidtbl where id=@ctr1 and flag=0

	if (@prptrnid = 0)
	begin
		insert into @smtbl (smid, ptrnid, profid, ptaskid, dtaskid, optid, flag)
		select distinct @smid, @dptrnid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
		from dbo.sm with (nolock) inner join
		dbo.WG_PTRN_SM wps1 with (nolock) on wps1.WG_PTRN_ID = sm.DESRD_WG_PTRN_ID inner join
		dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = wps1.DESRD_WG_PROF_ID
		where sm_id=@smid
		and sm.PRE_REQST_WG_PTRN_ID=0
		and sm.DESRD_WG_PTRN_ID = @dptrnid
		and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)
	end
	
	if (@dptrnid != 1)
	begin
		--select @dptrnid
		delete from @ptrntbl
		insert into @ptrntbl (smid, ptrnid, pprofid, dprofid, flag)
		select distinct sm.SM_ID, sm.DESRD_WG_PTRN_ID, wps1.PRE_REQST_WG_PROF_ID, wps1.DESRD_WG_PROF_ID, 0
		from dbo.sm sm with (nolock) inner join
			 dbo.WG_PTRN_SM wps1 with (nolock) on sm.DESRD_WG_PTRN_ID=wps1.WG_PTRN_ID 
		where sm.PRE_REQST_WG_PTRN_ID=@dptrnid
		  and sm.SM_ID=@smid
		  and (@dptrnid != 1)
		--union
		--select distinct sm.SM_ID, @prptrnid, wps1.PRE_REQST_WG_PROF_ID, wps1.DESRD_WG_PROF_ID, 0
		--from dbo.sm sm with (nolock) inner join
		--	 dbo.WG_PTRN_SM wps1 with (nolock) on sm.PRE_REQST_WG_PTRN_ID=wps1.WG_PTRN_ID 
		--where sm.PRE_REQST_WG_PTRN_ID=@prptrnid
		--  and sm.SM_ID=@smid
		--  and (@dptrnid = 1)

		  --select * from @ptrntbl
		  declare @inptrnid smallint=0
		Select @ctr2 = min(id) from @ptrntbl
		select @cnt2 = count(1) from @ptrntbl
		if (@ctr2 != 1)
		 set @cnt2 = @cnt2 + @ctr2 - 1
		while (@ctr2<=@cnt2)
		begin

		select top 1 @ptrnid=ptrnid, @prprdid=pprofid, @dprfid = dprofid
		from @ptrntbl
		where flag=0 and id=@ctr2
		--select @smid, @prprdid, @dprfid
		--select @inptrnid, @ptrnid
		if ((@prprdid = 0) and (@inptrnid != @ptrnid))
		begin
			set @inptrnid = @ptrnid
			delete from @endtask
			insert into @endtask (smid, ptrnid, profid, ptaskid, dtaskid, optid)
			select distinct smid, ptrnid, profid, ptaskid, 1, optid
						  from @smtbl
						  where dtaskid=1
						    and smid=@smid

			--assign a desired taskid to the ending taskid from prior pattern/profile
			update st
			set dtaskid = x.DESRD_TASK_ID
			from @smtbl st inner join
			(select top 1 wpr1.WG_PROF_ID, wpr1.desrd_task_id, wpr1.OPT_ID, pt.smid
			from @ptrntbl pt inner join
				dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = pt.dprofid
			where pt.pprofid=0
			  and pt.smid=@smid
			  and wpr1.PRE_REQST_TASK_ID=0) as x
			on st.dtaskid=1
			--and st.optid=x.OPT_ID
			and st.ptaskid != x.DESRD_TASK_ID
			and st.smid=x.smid


			--load all prereq/starting tasks for that desired pattern
			insert into @smtbl (smid, ptrnid, ptaskid, dtaskid, optid, flag)
			select distinct @smid, et.ptrnid, et.ptaskid, wpr1.DESRD_TASK_ID, et.optid, 0
			from @endtask et inner join
				 @ptrntbl pt on et.smid=pt.smid inner join
				dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = pt.dprofid
			where pt.pprofid=0
			  and wpr1.PRE_REQST_TASK_ID=0
			  and et.smid=@smid
			  and pt.smid=@smid
			  and et.ptaskid != wpr1.DESRD_TASK_ID
			  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=et.ptaskid and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)
		end

		if (@dprfid != 1)
		begin
			--if(@dprfid in (203,1000))
			--begin
			--select @prprdid, @dprfid
			--select st.*
			----set dtaskid = x.DESRD_TASK_ID
			--from @smtbl st where st.dtaskid=1 			and st.smid=64
			--select st.*
			----set dtaskid = x.DESRD_TASK_ID
			--from @smtbl st inner join
			--(select top 1 wpr1.desrd_task_id, wpr1.OPT_ID, pt.smid
			--from @ptrntbl pt inner join
			--	dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = pt.dprofid
			--where pt.pprofid=@dprfid
			--  and pt.smid=@smid
			--  and wpr1.PRE_REQST_TASK_ID=0) as x
			--on st.dtaskid=1 
			----and st.optid=x.OPT_ID
			--and st.smid=x.smid
			--select top 1 wpr1.desrd_task_id, wpr1.OPT_ID, pt.smid
			--from @ptrntbl pt inner join
			--	dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = pt.dprofid
			--where pt.pprofid=@dprfid
			--  and pt.smid=@smid
			--  and wpr1.PRE_REQST_TASK_ID=0
			--  end

			update st
			set dtaskid = x.DESRD_TASK_ID
			from @smtbl st inner join
			(select top 1 wpr1.desrd_task_id, wpr1.OPT_ID, pt.smid
			from @ptrntbl pt inner join
				dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = pt.dprofid
			where pt.pprofid=@dprfid
			  and pt.smid=@smid
			  and wpr1.PRE_REQST_TASK_ID=0) as x
			on st.dtaskid=1 
			--and st.optid=x.OPT_ID
			and st.smid=x.smid
			and st.ptaskid != x.DESRD_TASK_ID
			and not exists (select 'x' from dbo.sm sm2 with (nolock) inner join 
			dbo.WG_PTRN_SM wps2 with (nolock) on sm2.PRE_REQST_WG_PTRN_ID=wps2.WG_PTRN_ID and wps2.DESRD_WG_PROF_ID=1 inner join 
			dbo.WG_PROF_SM wpr2 with (nolock) on wps2.PRE_REQST_WG_PROF_ID=wpr2.WG_PROF_ID and wpr2.desrd_task_id=1
			where st.smid=sm2.SM_ID and sm2.PRE_REQST_WG_PTRN_ID=st.ptrnid  and sm2.DESRD_WG_PTRN_ID=1 and wpr2.PRE_REQST_TASK_ID=st.ptaskid)

			--select * from dbo.sm sm2 with (nolock) inner join 
			--dbo.WG_PTRN_SM wps2 with (nolock) on sm2.PRE_REQST_WG_PTRN_ID=wps2.WG_PTRN_ID and wps2.DESRD_WG_PROF_ID=1 inner join 
			--dbo.WG_PROF_SM wpr2 with (nolock) on wps2.PRE_REQST_WG_PROF_ID=wpr2.WG_PROF_ID and wpr2.desrd_task_id=1
			--where 14=sm2.SM_ID and sm2.PRE_REQST_WG_PTRN_ID=601  and sm2.DESRD_WG_PTRN_ID=1 and wpr2.PRE_REQST_TASK_ID=1001

			--select distinct @smid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
			--from @ptrntbl pt inner join
			--	 dbo.WG_PROF_SM wpr1 with (nolock) on pt.pprofid=wpr1.WG_PROF_ID
			--where pt.pprofid=@dprfid
			--  and pt.smid=@smid
			--  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)

			----select * from @ptrntbl where pprofid=@dprfid
			--select distinct @smid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
			--from @ptrntbl pt inner join
			--	 dbo.WG_PROF_SM wpr1 with (nolock) on pt.dprofid=wpr1.WG_PROF_ID
			--where pt.pprofid=@dprfid
			--  and wpr1.PRE_REQST_TASK_ID!=0
			--  and pt.smid=@smid
			--  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)

			--load all non-prereq/starting tasks for that desired pattern
			insert into @smtbl (smid, ptrnid, profid, ptaskid, dtaskid, optid, flag)
			select distinct @smid, pt.ptrnid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
			from @ptrntbl pt inner join
				 dbo.WG_PROF_SM wpr1 with (nolock) on pt.dprofid=wpr1.WG_PROF_ID
			where pt.pprofid=@dprfid
			  and wpr1.PRE_REQST_TASK_ID!=0
			  and pt.smid=@smid
			  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)

			  if ((@prprdid!=1) and (@dprfid!=1))
			  begin
				--select @inptrnid, @ptrnid
			 -- select distinct @smid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
				--from @ptrntbl pt inner join
				-- dbo.WG_PROF_SM wpr1 with (nolock) on pt.pprofid=wpr1.WG_PROF_ID
				--where pt.pprofid=@prprdid
				--  and wpr1.PRE_REQST_TASK_ID!=0
				--  and pt.smid=@smid
				--  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)
				insert into @smtbl (smid, ptrnid, profid, ptaskid, dtaskid, optid, flag)
				select distinct @smid, pt.ptrnid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
				from @ptrntbl pt inner join
				 dbo.WG_PROF_SM wpr1 with (nolock) on pt.pprofid=wpr1.WG_PROF_ID
				where pt.pprofid=@prprdid
				  and wpr1.PRE_REQST_TASK_ID!=0
				  and pt.smid=@smid
				  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)

				  update st
					set dtaskid = x.DESRD_TASK_ID
					from @smtbl st inner join
					(select top 1 wpr1.WG_PROF_ID, wpr1.desrd_task_id, wpr1.OPT_ID, pt.smid
					from @ptrntbl pt inner join
						dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = pt.pprofid
					where pt.pprofid=@dprfid
					  and pt.smid=@smid
					  and wpr1.PRE_REQST_TASK_ID=0) as x
					on st.dtaskid=1 
					and st.profid!=x.WG_PROF_ID
					and st.ptaskid != x.DESRD_TASK_ID
					and st.ptaskid in (select desrd_task_id from dbo.WG_PROF_SM wpr2 with (nolock) where wpr2.WG_PROF_ID=@prprdid or wpr2.WG_PROF_ID=@dprfid )
					and st.smid=x.smid 

				  insert into @smtbl (smid, ptrnid, profid, ptaskid, dtaskid, optid, flag)
				select distinct @smid,pt.ptrnid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
				from @ptrntbl pt inner join
				 dbo.WG_PROF_SM wpr1 with (nolock) on pt.pprofid=wpr1.WG_PROF_ID
				where pt.pprofid=@dprfid
				  and wpr1.PRE_REQST_TASK_ID!=0
				  and wpr1.DESRD_TASK_ID!=1
				  and pt.smid=@smid
				  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)
			  end

			--delete from @endtask
			--insert into @endtask (smid, profid, ptaskid, dtaskid, optid)
			--select distinct smid, profid, ptaskid, 1, optid
			--			  from @smtbl
			--			  where dtaskid=1

			----non-prereq and non-desired/end profiles
			--insert into @smtbl (smid, profid, ptaskid, dtaskid, optid, flag)
			--select distinct @smid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
			--from dbo.sm sm with (nolock) inner join
			--	 dbo.WG_PTRN_SM wps1 with (nolock) on sm.DESRD_WG_PTRN_ID=wps1.WG_PTRN_ID inner join
			--	 dbo.WG_PROF_SM wpr1 with (nolock) on wpr1.WG_PROF_ID = wps1.PRE_REQST_WG_PROF_ID inner join
			--	 dbo.WG_PROF_SM wpr2 with (nolock) on wpr2.WG_PROF_ID = wps1.DESRD_WG_PROF_ID
			--where sm.PRE_REQST_WG_PTRN_ID=@dptrnid
			--  and sm.SM_ID=@smid
			--  and wps1.PRE_REQST_WG_PROF_ID=0
			--  and wpr1.PRE_REQST_TASK_ID=0
			--  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)

			end

			if (@dprfid=1)
			begin				
				--select distinct @smid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
				--from @ptrntbl pt inner join
				--	 dbo.WG_PROF_SM wpr1 with (nolock) on pt.pprofid=wpr1.WG_PROF_ID
				--where pt.pprofid=@prprdid
				--  and pt.smid=@smid
				--  and wpr1.PRE_REQST_TASK_ID!=0
				--  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)

				insert into @smtbl (smid, ptrnid, profid, ptaskid, dtaskid, optid, flag)
				select distinct @smid, pt.ptrnid, wpr1.WG_PROF_ID, wpr1.PRE_REQST_TASK_ID, wpr1.DESRD_TASK_ID, wpr1.OPT_ID, 0
				from @ptrntbl pt inner join
					 dbo.WG_PROF_SM wpr1 with (nolock) on pt.pprofid=wpr1.WG_PROF_ID
				where pt.pprofid=@prprdid
				  and pt.smid=@smid
				  and wpr1.PRE_REQST_TASK_ID!=0
				  and not exists (select 'x' from @smtbl sm2 where sm2.smid=@smid and ptaskid=wpr1.PRE_REQST_TASK_ID and dtaskid=wpr1.DESRD_TASK_ID and optid=wpr1.OPT_ID)
			end
		  
		  update @ptrntbl set flag=1 where id=@ctr2 and flag=0
		  set @ctr2 = @ctr2 + 1
		end
	end

	--if  (@dptrnid=1)
	--begin
	--	select @prptrnid
	--	select * from @smidtbl
	--	select * from @smtbl
	--end
	update @smidtbl set flag=1 where id=@ctr1 and flag=0
	set @ctr1 = @ctr1 + 1
end
--select distinct smid, ptaskid, dtaskid, optid from @smtbl order by smid, ptaskid, optid desc
select * from @smtbl order by smid, ptaskid, optid desc


--100, 102, 2
--100, 210, 2
--100, 300, 2
--100, 400, 2
--101, 102, 3
--101, 210, 3
--101, 300, 3
--101, 400, 3