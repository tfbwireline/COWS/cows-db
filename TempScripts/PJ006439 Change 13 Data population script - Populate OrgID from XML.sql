USE COWS
GO

----------------------------------
--Click the minus sign next to Begin
--Ignore everything until the End.
----------------------------------
IF 1=1
Begin
IF OBJECT_ID(N'tempdb..#XMLTable', N'U') IS NOT NULL   
	DROP TABLE #XMLTable

Create Table #XMLTable
(
	FRBID		int,
	requestType	varchar(50),
	MSG_XML_ID	int,
	is_error	bit,
	is_reject	bit,
	error		varchar(max),
	tadpoleXML	varchar(max),
	idStart		int,
	idEnd		int,
	typeStart	int,
	typeEnd		int,
	OrgStart	int,
	OrgEnd		int,
	OrgID		varchar(50),
	receiveTime	DateTime
)

OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

INSERT INTO #XMLTable (MSG_XML_ID, is_error, is_reject, error, tadpoleXML, receiveTime)
SELECT MSG_XML_ID, IS_ERROR_CD, IS_REJECT_CD, ERROR_MSG_TXT, dbo.decryptBinaryData(msg_xml_txt), CREAT_DT from FEDLINE_EVENT_TADPOLE_XML order by MSG_XML_ID desc

UPDATE t
SET	idStart	=	CHARINDEX('<requestID', tadpoleXML)+11,
	idEnd	=	CHARINDEX('</requestID', tadpoleXML),
	typeStart	=	CHARINDEX('<responseDescription', tadpoleXML)+21,
	typeEnd		=	CHARINDEX('</responseDescription', tadpoleXML),
	OrgStart	=	CHARINDEX('<organizationID', tadpoleXML)+16,
	OrgEnd		=	CHARINDEX('</organizationID', tadpoleXML)
FROM #XMLTable	t

UPDATE t
SET	FRBID		=	SUBSTRING(tadpoleXML, idStart, idEnd - idStart)
FROM #XMLTable	t
	WHERE idStart <> 0 AND idEnd <> 0

UPDATE t
SET	requestType	=	REPLACE(SUBSTRING(tadpoleXML, typeStart, typeEnd - typeStart),'xmlns="">','')
FROM #XMLTable	t
	WHERE typeStart <> 0 AND typeEnd <> 0
	
UPDATE t
SET	OrgID	=	REPLACE(SUBSTRING(tadpoleXML, OrgStart, OrgEnd - OrgStart),'xmlns="">','')
FROM #XMLTable	t
	WHERE OrgStart <> 0 AND OrgEnd <> 0
End
----------------------------------
--Ignore above here.  Add any search
--conditions below 
----------------------------------

--select td.FEDLINE_EVENT_ID, td.ORG_ID, t.OrgID, FRBID, requestType, MSG_XML_ID, is_error, is_reject, error, tadpoleXML, receiveTime 
--from FEDLINE_EVENT_TADPOLE_DATA td 
--left join #XMLTable t on td.FRB_REQ_ID = t.FRBID
	
--Where	t.FRBID			=	t.FRBID
--	AND	t.MSG_XML_ID	=	t.MSG_XML_ID
--	AND	t.is_error		=	0
--	AND	t.is_reject		=	0
--	AND	t.OrgID			is not null
--Order By MSG_XML_ID desc

Update	td
Set		td.ORG_ID	=	t.OrgID
FROM FEDLINE_EVENT_TADPOLE_DATA td 
inner join #XMLTable t on td.FRB_REQ_ID = t.FRBID
Where	t.FRBID			=	t.FRBID
	AND	t.MSG_XML_ID	=	t.MSG_XML_ID
	AND	t.is_error		=	0
	AND	t.is_reject		=	0
	AND	t.OrgID			is not null
	
Update FEDLINE_EVENT_TADPOLE_DATA Set ORG_ID = '' Where ORG_ID IS NULL
