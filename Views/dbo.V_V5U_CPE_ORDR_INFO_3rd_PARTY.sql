USE [COWS]
GO

/****** Object:  View [dbo].[V_V5U_CPE_ORDR_INFO_3rd_PARTY]    Script Date: 05/09/2018 09:31:34 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[V_V5U_CPE_ORDR_INFO_3rd_PARTY]
AS 

	SELECT  DISTINCT fo.ORDR_ID
				,boa.Device_ID + '-' + fo.FTN
							+ '-' + ISNULL(cli.PRCH_ORDR_NBR,'')	   AS DEVICE_ID_FTN
				, boa.Device_ID				           AS DEVICE_ID
				,fo.FTN									   AS FTN
				,boa.CUST_NME 
                ,case
                   when    fo.ORDR_TYPE_CD = 'IN' then 'INST'
                   when    fo.ORDR_TYPE_CD = 'DC' then 'DISC'
                   WHEN    fo.ORDR_TYPE_CD = 'CN' then 'CAN'
                  END					       AS ACT
                ,CASE
					WHEN act.STUS_ID = 156 THEN 'HOLD--' + lt.TASK_NME
					ELSE lt.TASK_NME
					END							   AS STUS
                ,ISNULL(ord.CUST_CMMT_DT,fo.CUST_CMMT_DT)  AS CUST_CMMT_DT
                ,ISNULL(lu.FULL_NME,'Unassigned')          AS ASN_USER
                ,boa.CUST_ID					  
				,ISNULL(ord.DLVY_CLLI,'')				   AS CLLI_CD
				,boa.STREET_ADR_1
				,boa.STREET_ADR_2
				,boa.STREET_ADR_3
                 ,boa. BLDG_NME
                 ,boa.FLR_ID
                 ,boa.RM_NBR
                 ,boa.CTY_NME
				 ,boa.STT_CD
				 ,boa.ZIP_PSTL_CD
				 ,boa.CTRY_CD
                 ,boa.PRVN_NME
				 ,ISNULL(fo.CPE_EQPT_ONLY_CD,'N')		AS CPE_EQPT_ONLY_CD
				 ,ISNULL(fo.CPE_ACCS_PRVDR_CD,'')		AS CPE_ACCS_PRVDR_CD
				 ,ISNULL (fo.CPE_ECCKT_ID,'')           AS CPE_ECCKT_ID
				 ,ISNULL(fo.CPE_PHN_NBR,'')             AS CPE_PHN_NBR
				 ,ISNULL(fo.CPE_REC_ONLY_CD,'N')		AS CPE_REC_ONLY_CD
			----**************************************************************************	
				  ,boa.SHIP_STREET_ADR_1
				  ,boa.SHIP_STREET_ADR_2
				  ,boa.SHIP_STREET_ADR_3
				  ,boa.SHIP_BLDG_NME
                  ,boa.SHIP_FLR_ID
				  ,boa.SHIP_RM_NBR
                  ,boa.SHIP_CTY_NME
				  ,boa.SHIP_STT_CD		
				  ,boa.SHIP_ZIP_PSTL_CD		  
				  ,boa.SHIP_CTRY_CD							
				 ,boa.SHIP_PRVN_NME
				 , ISNULL(lu.USER_ID,'')                AS ASN_USER_ID
				 , ISNULL(lu.USER_ADID,'')              AS ASN_ADID
				 , ord.DMSTC_CD                         AS DMSTC_CD
				 , CASE
					WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN 0
					ELSE 1
					END
					                        AS SCURD_CD
				 ,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_CD,'')
						FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
						     dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
						WHERE oj.ORDR_ID = fo.ORDR_ID
							ORDER BY oj.CREAT_DT DESC) AS JPRDY_CD
				,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_DES,'')
						FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
						     dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
						WHERE oj.ORDR_ID = fo.ORDR_ID
							ORDER BY oj.CREAT_DT DESC) AS JPRDY_DES
				 --, ISNULL(lj.CPE_JPRDY_CD,'')           AS JPRDY_CD
				 --, ISNULL(lj.CPE_JPRDY_DES,'')				AS JPRDY_DES
				 ,(SELECT DATEDIFF(dd,at1.CREAT_DT,GETDATE()) 
						FROM dbo.ACT_TASK at1 WITH (NOLOCK)
						WHERE at1.TASK_ID IN (602,1000)
						  AND at1.STUS_ID = 0
						  AND at1.ORDR_ID = fo.ORDR_ID) AS INBOX
				 , fo.INSTL_ESCL_CD                     AS EXPEDITE
				 , fo.CREAT_DT                          AS CREAT_DT -- GOM received order.
				 , ord.RAS_DT                           AS RAS_DT 
				 , '1234567890'                         AS ELID   -- space holder for later
				 ,ISNULL(mns.MNTC_CD,'N')				AS MNS_ORDR -- 
				 ,ISNULL(fo.DDR,'')                     AS DDR
				 ,ISNULL(fo.DDU,'')                     AS DDU
				 ,ISNULL(ord.SMR_NMR,'')                AS SMR_NMR
				 ,boa.SITE_ID
				 ,ISNULL(ord.CPE_CLLI,'')               AS CPE_CLLI
				 ,CASE
					WHEN act.STUS_ID = 156 THEN 'Y'
					ELSE 'N'
					END								    AS HOLD
				 ,'Y'                                   AS THIRD_PARTY_SITE
				 ,fo.PRNT_FTN                           AS RELTD_FTN
				 ,fo.SCA_NBR							AS SCA_NBR
	--INTO #Cpe3rdPrty
	FROM         dbo.FSA_ORDR fo			WITH (NOLOCK)
	INNER JOIN   dbo.ORDR ord				WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
	INNER JOIN dbo.ACT_TASK act				WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID
	INNER JOIN   dbo.LK_TASK lt				WITH (NOLOCK) ON act.TASK_ID = lt.TASK_ID
	INNER JOIN BPM_ORDR_ADR boa				WITH (NOLOCK) ON ord.ORDR_ID = boa.ORDR_ID
	LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK)
						ON cli.ORDR_ID = fo.ORDR_ID
	LEFT OUTER JOIN dbo.USER_WFM_ASMT uwa	WITH (NOLOCK) ON uwa.ORDR_ID = fo.ORDR_ID
												AND uwa.GRP_ID in (13,1,14)
	LEFT OUTER JOIN dbo.LK_USER lu			WITH (NOLOCK) ON lu.USER_ID = uwa.ASN_USER_ID
	LEFT OUTER JOIN (SELECT MNTC_CD, DEVICE_ID, ORDR_ID FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
						WHERE MNTC_CD = 'Y')mns ON mns.ORDR_ID = fo.ORDR_ID AND cli.DEVICE_ID = mns.DEVICE_ID
	WHERE  fo.PROD_TYPE_CD = 'CP'
		AND ord.DMSTC_CD = 0
		AND ((act.TASK_ID IN (602,604,1000) AND act.STUS_ID = 0) 
					AND ord.DMSTC_CD = 0 AND  ord.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM'))
		AND ord.ORDR_CAT_ID = 6
		AND ISNULL(cli.DEVICE_ID,'') <> ''
		AND ISNULL(fo.CPE_VNDR,'') = 'Goodman'


GO


