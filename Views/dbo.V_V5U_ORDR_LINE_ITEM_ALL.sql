USE [COWS]
GO

/****** Object:  View [dbo].[V_V5U_ORDR_LINE_ITEM_ALL]    Script Date: 11/28/2016 13:47:12 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[V_V5U_ORDR_LINE_ITEM_ALL]
AS
 
	Select * from openquery(local, '[COWS].[dbo].[getCpeLineItemsAll_V5U]')




GO


