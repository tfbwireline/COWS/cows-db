USE [COWS]
GO

/****** Object:  View [dbo].[V_V5U_ORDR_NTE]    Script Date: 11/28/2016 13:47:59 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





CREATE VIEW [dbo].[V_V5U_ORDR_NTE](NTE_ID, FTN, ORDR_ID, NTE_TXT,USER_NME,CREAT_DT)  
AS 

	SELECT		 nte.NTE_ID
				,fo.FTN                          AS FTN
				,fo.ORDR_ID                      AS ORDR_ID												   
				,NTE_TXT
                ,lu.DSPL_NME                     AS USER_NME
                ,nte.CREAT_DT			

	FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
	INNER JOIN   dbo.ORDR_NTE nte WITH (NOLOCK) ON nte.ORDR_ID = fo.ORDR_ID
	LEFT OUTER JOIN dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = nte.CREAT_BY_USER_ID

	WHERE  fo.PROD_TYPE_CD = 'CP'





GO


