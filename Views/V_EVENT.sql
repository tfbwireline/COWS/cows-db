USE [COWS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
ALTER VIEW [dbo].[V_EVENT](EVENT_ID,EVENT_TYPE_ID,EVENT_TYPE_NME,EVENT_STUS_ID,EVENT_STUS_DES,ASN_TO_USER_ID,ASN_TO_USER_NME,FTN_NBR,H1_NBR,H5_H6_NBR,CHARS_ID,SOWS_EVENT_ID,REQOR_USER_ID,REQOR_USER_NME,MDS_FAST_TRK_CD,CUST_NME,FMS_CKT_ID
, STRT_TMST, END_TMST, ENG_USER_ID, ENG_USER_NME, ACTV_USER_ID, ACTV_USER_NME)  
AS   
SELECT    
                      ev.EVENT_ID, ev.EVENT_TYPE_ID, CASE ae.ENHNC_SRVC_ID WHEN 7 THEN 'BROADBAND' WHEN 8 THEN 'NARROWBAND'  
                                    WHEN 9 THEN 'GOVERNMENT' WHEN 10 THEN 'INTERNATIONAL' END AS EVENT_TYPE_NME, ae.EVENT_STUS_ID, evs.EVENT_STUS_DES, esu.ASN_TO_USER_ID,   
                      lu.DSPL_NME, ae.FTN, ae.H1, ae.H6, ae.CHARS_ID, ae.SOWS_EVENT_ID, ae.REQOR_USER_ID, ru.DSPL_NME, 0,   
                      CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE ae.CUST_NME END AS CUST_NME, eat.CKT_ID  AS FMS_CKT_ID  
					  ,ae.STRT_TMST		AS	STRT_TMST
                      ,ae.END_TMST		AS	END_TMST
                      ,0				AS	ENG_USER_ID   
					  ,''				AS	ENG_USER_NME					  
                      ,0				AS	ACTV_USER_ID   
					  ,''				AS	ACTV_USER_NME
FROM         dbo.EVENT ev WITH (NOLOCK) INNER JOIN  
                      LK_EVENT_TYPE evt WITH (NOLOCK) ON ev.EVENT_TYPE_ID = evt.EVENT_TYPE_ID INNER JOIN  
                      dbo.AD_EVENT ae WITH (NOLOCK) ON ev.EVENT_ID = ae.EVENT_ID LEFT JOIN  
                      dbo.AD_EVENT_ACCS_TAG eat WITH (NOLOCK) ON ev.EVENT_ID = eat.EVENT_ID INNER JOIN  
                      LK_EVENT_STUS evs WITH (NOLOCK) ON ae.EVENT_STUS_ID = evs.EVENT_STUS_ID LEFT JOIN  
                      dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN = ae.FTN LEFT JOIN  
                      dbo.ORDR o WITH (NOLOCK) ON fo.ORDR_ID = o.ORDR_ID LEFT JOIN  
                      dbo.EVENT_ASN_TO_USER esu WITH (NOLOCK) ON ev.EVENT_ID = esu.EVENT_ID AND esu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = esu.ASN_TO_USER_ID AND lu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER ru WITH (NOLOCK) ON ae.REQOR_USER_ID = ru.USER_ID AND ru.REC_STUS_ID = 1  
UNION ALL    
SELECT     
                      ev.EVENT_ID, ev.EVENT_TYPE_ID, evt.EVENT_TYPE_NME, mde.EVENT_STUS_ID, evs.EVENT_STUS_DES, esu.ASN_TO_USER_ID,   
                      lu.DSPL_NME AS ASN_TO_USER_NME, '' AS FTN, mde.H1_ID,   
                      f.H5_H6_CUST_ID AS H5_H6, mde.CHARS_ID, mde.SOWS_EVENT_ID, mde.REQOR_USER_ID, ru.DSPL_NME AS REQOR_USER_NME,   
                      mde.MDS_FAST_TRK_CD AS MDSFastTrack, CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE mde.CUST_NME
                      END AS CUST_NME, wtd.FMS_CKT_ID AS FMS_Ckt_ID  
					  ,mde.STRT_TMST	AS	STRT_TMST
                      ,mde.END_TMST		AS	END_TMST
                      ,0				AS	ENG_USER_ID   
					  ,''				AS	ENG_USER_NME					  
                      ,0				AS	ACTV_USER_ID   
					  ,''				AS	ACTV_USER_NME
FROM         dbo.EVENT ev WITH (NOLOCK) INNER JOIN  
                      LK_EVENT_TYPE evt WITH (NOLOCK) ON ev.EVENT_TYPE_ID = evt.EVENT_TYPE_ID INNER JOIN  
                      dbo.MDS_EVENT_NEW mde WITH (NOLOCK) ON ev.EVENT_ID = mde.EVENT_ID INNER JOIN  
                      dbo.FSA_MDS_EVENT_NEW f WITH (NOLOCK) ON mde.EVENT_ID = f.EVENT_ID  LEFT JOIN  
                      dbo.MDS_EVENT_WIRED_TRPT wtd WITH (NOLOCK) ON wtd.FSA_MDS_EVENT_ID = f.FSA_MDS_EVENT_ID  INNER JOIN  
                      dbo.LK_EVENT_STUS evs WITH (NOLOCK) ON mde.EVENT_STUS_ID = evs.EVENT_STUS_ID LEFT JOIN  
                      dbo.EVENT_ASN_TO_USER esu WITH (NOLOCK) ON ev.EVENT_ID = esu.EVENT_ID AND esu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = esu.ASN_TO_USER_ID AND lu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER ru WITH (NOLOCK) ON mde.REQOR_USER_ID = ru.USER_ID AND ru.REC_STUS_ID = 1  
WHERE		mde.MDS_ACTY_TYPE_ID!=3
UNION ALL  
SELECT     
                      ev.EVENT_ID, ev.EVENT_TYPE_ID, evt.EVENT_TYPE_NME, mde.EVENT_STUS_ID, evs.EVENT_STUS_DES, esu.ASN_TO_USER_ID,   
                      lu.DSPL_NME AS ASN_TO_USER_NME, '' AS FTN, mde.H1_ID,   
                      CONVERT(nVARCHAR(9), md.H5_H6_CUST_ID) AS H5_H6, mde.CHARS_ID, mde.SOWS_EVENT_ID, mde.REQOR_USER_ID,   
                      ru.DSPL_NME AS REQOR_USER_NME, mde.MDS_FAST_TRK_CD AS MDSFastTrack,   
             CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE mde.CUST_NME END AS CUST_NME, '' AS FMS_Ckt_ID  
					  ,mde.STRT_TMST	AS	STRT_TMST
                      ,mde.END_TMST		AS	END_TMST
                      ,0				AS	ENG_USER_ID   
					  ,''				AS	ENG_USER_NME					  
                      ,0				AS	ACTV_USER_ID   
					  ,''				AS	ACTV_USER_NME	
FROM         dbo.[EVENT] ev WITH (NOLOCK) INNER JOIN  
                      LK_EVENT_TYPE evt WITH (NOLOCK) ON ev.EVENT_TYPE_ID = evt.EVENT_TYPE_ID INNER JOIN  
                      dbo.MDS_EVENT_NEW mde WITH (NOLOCK) ON ev.EVENT_ID = mde.EVENT_ID INNER JOIN  
                      [dbo].[MDS_EVENT_DISCO] md WITH (NOLOCK) ON md.EVENT_ID= mde.EVENT_ID  LEFT JOIN   
                      dbo.LK_EVENT_STUS evs WITH (NOLOCK) ON mde.EVENT_STUS_ID = evs.EVENT_STUS_ID LEFT JOIN  
                      dbo.EVENT_ASN_TO_USER esu WITH (NOLOCK) ON ev.EVENT_ID = esu.EVENT_ID AND esu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = esu.ASN_TO_USER_ID AND lu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER ru WITH (NOLOCK) ON mde.REQOR_USER_ID = ru.USER_ID AND ru.REC_STUS_ID = 1 
WHERE		mde.MDS_ACTY_TYPE_ID=3
UNION ALL
SELECT     
                      ev.EVENT_ID, ev.EVENT_TYPE_ID, evt.EVENT_TYPE_NME, mde.EVENT_STUS_ID, evs.EVENT_STUS_DES, esu.ASN_TO_USER_ID,   
                      lu.DSPL_NME AS ASN_TO_USER_NME, '' AS FTN, mde.H1 as H1_ID,   
                      mde.H6 AS H5_H6, mde.CHARS_ID, -1 As SOWS_EVENT_ID, mde.REQOR_USER_ID, ru.DSPL_NME AS REQOR_USER_NME,   
                      ISNULL(mde.MDS_FAST_TRK_TYPE_ID,'') AS MDSFastTrack, CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE mde.CUST_NME
                      END AS CUST_NME, wtd.FMS_NBR AS FMS_Ckt_ID  
					  ,mde.STRT_TMST	AS	STRT_TMST
                      ,mde.END_TMST		AS	END_TMST
                      ,0				AS	ENG_USER_ID   
					  ,''				AS	ENG_USER_NME					  
                      ,0				AS	ACTV_USER_ID   
					  ,''				AS	ACTV_USER_NME
FROM         dbo.EVENT ev WITH (NOLOCK) INNER JOIN  
                      LK_EVENT_TYPE evt WITH (NOLOCK) ON ev.EVENT_TYPE_ID = evt.EVENT_TYPE_ID INNER JOIN  
                      dbo.MDS_EVENT mde WITH (NOLOCK) ON ev.EVENT_ID = mde.EVENT_ID INNER JOIN  
                      dbo.MDS_EVENT_SLNK_WIRED_TRPT wtd WITH (NOLOCK) ON wtd.EVENT_ID = mde.EVENT_ID AND wtd.REC_STUS_ID=1  INNER JOIN  
                      dbo.LK_EVENT_STUS evs WITH (NOLOCK) ON mde.EVENT_STUS_ID = evs.EVENT_STUS_ID LEFT JOIN  
                      dbo.EVENT_ASN_TO_USER esu WITH (NOLOCK) ON ev.EVENT_ID = esu.EVENT_ID AND esu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = esu.ASN_TO_USER_ID AND lu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER ru WITH (NOLOCK) ON mde.REQOR_USER_ID = ru.USER_ID AND ru.REC_STUS_ID = 1  
WHERE		mde.MDS_ACTY_TYPE_ID!=3
UNION ALL  
SELECT     
                      ev.EVENT_ID, ev.EVENT_TYPE_ID, evt.EVENT_TYPE_NME, mde.EVENT_STUS_ID, evs.EVENT_STUS_DES, esu.ASN_TO_USER_ID,   
                      lu.DSPL_NME AS ASN_TO_USER_NME, '' AS FTN, mde.H1 as H1_ID,   
                      CONVERT(nVARCHAR(9), md.H5_H6) AS H5_H6, mde.CHARS_ID, -1 AS SOWS_EVENT_ID, mde.REQOR_USER_ID,   
                      ru.DSPL_NME AS REQOR_USER_NME, ISNULL(mde.MDS_FAST_TRK_TYPE_ID,'') AS MDSFastTrack,   
             CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE mde.CUST_NME END AS CUST_NME, '' AS FMS_Ckt_ID  
					  ,mde.STRT_TMST	AS	STRT_TMST
                      ,mde.END_TMST		AS	END_TMST
                      ,0				AS	ENG_USER_ID   
					  ,''				AS	ENG_USER_NME					  
                      ,0				AS	ACTV_USER_ID   
					  ,''				AS	ACTV_USER_NME	
FROM         dbo.[EVENT] ev WITH (NOLOCK) INNER JOIN  
                      LK_EVENT_TYPE evt WITH (NOLOCK) ON ev.EVENT_TYPE_ID = evt.EVENT_TYPE_ID INNER JOIN  
                      dbo.MDS_EVENT mde WITH (NOLOCK) ON ev.EVENT_ID = mde.EVENT_ID INNER JOIN  
                      [dbo].EVENT_DISCO_DEV md WITH (NOLOCK) ON md.EVENT_ID= mde.EVENT_ID  LEFT JOIN   
                      dbo.LK_EVENT_STUS evs WITH (NOLOCK) ON mde.EVENT_STUS_ID = evs.EVENT_STUS_ID LEFT JOIN  
                      dbo.EVENT_ASN_TO_USER esu WITH (NOLOCK) ON ev.EVENT_ID = esu.EVENT_ID AND esu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = esu.ASN_TO_USER_ID AND lu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER ru WITH (NOLOCK) ON mde.REQOR_USER_ID = ru.USER_ID AND ru.REC_STUS_ID = 1 
WHERE		mde.MDS_ACTY_TYPE_ID=3
UNION ALL  
SELECT     
                      ev.EVENT_ID, ev.EVENT_TYPE_ID, evt.EVENT_TYPE_NME, mpe.EVENT_STUS_ID, evs.EVENT_STUS_DES, esu.ASN_TO_USER_ID,   
                      lu.DSPL_NME AS ASN_TO_USER_NME, mpe.FTN, mpe.H1, mpe.H6 AS H5_H6, mpe.CHARS_ID, mpe.SOWS_EVENT_ID, mpe.REQOR_USER_ID,   
                      ru.DSPL_NME AS REQOR_USER_NME, 0 AS MDSFastTrack,   
                      CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE mpe.CUST_NME END AS CUST_NME, mea.PL_DAL_CKT_NBR AS FMS_Ckt_ID  
					  ,mpe.STRT_TMST	AS	STRT_TMST
                      ,mpe.END_TMST		AS	END_TMST
                      ,0				AS	ENG_USER_ID   
					  ,''				AS	ENG_USER_NME					  
                      ,0				AS	ACTV_USER_ID   
					  ,''				AS	ACTV_USER_NME
FROM                  dbo.EVENT ev WITH (NOLOCK) INNER JOIN  
                      LK_EVENT_TYPE evt WITH (NOLOCK) ON ev.EVENT_TYPE_ID = evt.EVENT_TYPE_ID LEFT JOIN  
                      dbo.MPLS_EVENT_ACCS_TAG mea WITH (NOLOCK) on mea.EVENT_ID = ev.EVENT_ID INNER JOIN  
                      dbo.MPLS_EVENT mpe WITH (NOLOCK) ON ev.EVENT_ID = mpe.EVENT_ID INNER JOIN  
                      LK_EVENT_STUS evs WITH (NOLOCK) ON mpe.EVENT_STUS_ID = evs.EVENT_STUS_ID LEFT JOIN  
                      dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN = mpe.FTN LEFT JOIN  
                      dbo.ORDR o WITH (NOLOCK) ON fo.ORDR_ID = o.ORDR_ID LEFT JOIN  
                      dbo.EVENT_ASN_TO_USER esu WITH (NOLOCK) ON ev.EVENT_ID = esu.EVENT_ID AND esu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = esu.ASN_TO_USER_ID AND lu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER ru WITH (NOLOCK) ON mpe.REQOR_USER_ID = ru.USER_ID AND ru.REC_STUS_ID = 1  
UNION ALL  
SELECT     
                      ev.EVENT_ID, ev.EVENT_TYPE_ID, evt.EVENT_TYPE_NME, ne.EVENT_STUS_ID, evs.EVENT_STUS_DES, esu.ASN_TO_USER_ID,   
                      lu.DSPL_NME AS ASN_TO_USER_NME, ne.FTN, ne.H1, ne.H6 AS H5_H6, ne.CHARS_ID, ne.SOWS_EVENT_ID, ne.REQOR_USER_ID,   
                      ru.DSPL_NME AS REQOR_USER_NME, 0 AS MDSFastTrack,   
                      CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE ne.CUST_NME END AS CUST_NME, Null AS FMS_Ckt_ID  
					  ,ne.STRT_TMST		AS	STRT_TMST
                      ,ne.END_TMST		AS	END_TMST
                      ,0				AS	ENG_USER_ID   
					  ,''				AS	ENG_USER_NME					  
                      ,0				AS	ACTV_USER_ID   
					  ,''				AS	ACTV_USER_NME
FROM         dbo.EVENT ev WITH (NOLOCK) INNER JOIN  
                      LK_EVENT_TYPE evt WITH (NOLOCK) ON ev.EVENT_TYPE_ID = evt.EVENT_TYPE_ID INNER JOIN  
                      dbo.NGVN_EVENT ne WITH (NOLOCK) ON ev.EVENT_ID = ne.EVENT_ID INNER JOIN  
                      LK_EVENT_STUS evs WITH (NOLOCK) ON ne.EVENT_STUS_ID = evs.EVENT_STUS_ID LEFT JOIN  
                      dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN = ne.FTN LEFT JOIN  
                      dbo.ORDR o WITH (NOLOCK) ON fo.ORDR_ID = o.ORDR_ID LEFT JOIN  
                      dbo.EVENT_ASN_TO_USER esu WITH (NOLOCK) ON ev.EVENT_ID = esu.EVENT_ID AND esu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = esu.ASN_TO_USER_ID AND lu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER ru WITH (NOLOCK) ON ne.REQOR_USER_ID = ru.USER_ID AND ru.REC_STUS_ID = 1  
UNION ALL  
SELECT     
                      ev.EVENT_ID, ev.EVENT_TYPE_ID, evt.EVENT_TYPE_NME, sle.EVENT_STUS_ID, evs.EVENT_STUS_DES, esu.ASN_TO_USER_ID,   
                      lu.DSPL_NME AS ASN_TO_USER_NME, sle.FTN, sle.H1, sle.H6 AS H5_H6, sle.CHARS_ID, sle.SOWS_EVENT_ID, sle.REQOR_USER_ID,   
                      ru.DSPL_NME AS REQOR_USER_NME, 0 AS MDSFastTrack,   
                      CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE sle.CUST_NME END AS CUST_NME, Null AS FMS_Ckt_ID 
					  ,sle.STRT_TMST	AS	STRT_TMST
                      ,sle.END_TMST		AS	END_TMST					  
                      ,0				AS	ENG_USER_ID   
					  ,''				AS	ENG_USER_NME					  
                      ,0				AS	ACTV_USER_ID   
					  ,''				AS	ACTV_USER_NME
FROM         dbo.EVENT ev WITH (NOLOCK) INNER JOIN  
                      LK_EVENT_TYPE evt WITH (NOLOCK) ON ev.EVENT_TYPE_ID = evt.EVENT_TYPE_ID INNER JOIN  
                      dbo.SPLK_EVENT sle WITH (NOLOCK) ON ev.EVENT_ID = sle.EVENT_ID INNER JOIN  
                      LK_EVENT_STUS evs WITH (NOLOCK) ON sle.EVENT_STUS_ID = evs.EVENT_STUS_ID LEFT JOIN  
                      dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN = sle.FTN LEFT JOIN  
                      dbo.ORDR o WITH (NOLOCK) ON fo.ORDR_ID = o.ORDR_ID LEFT JOIN  
                      dbo.EVENT_ASN_TO_USER esu WITH (NOLOCK) ON ev.EVENT_ID = esu.EVENT_ID AND esu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = esu.ASN_TO_USER_ID AND lu.REC_STUS_ID = 1 LEFT JOIN  
                      dbo.LK_USER ru WITH (NOLOCK) ON sle.REQOR_USER_ID = ru.USER_ID AND ru.REC_STUS_ID = 1    
  
UNION ALL  
SELECT DISTINCT	ev.EVENT_ID 
				,ev.EVENT_TYPE_ID 
				,evt.EVENT_TYPE_NME 
				,fu.EVENT_STUS_ID
				,evs.EVENT_STUS_DES 
				,0							AS	ASN_TO_USER_ID   
                ,'COWS Fedline Activators'	AS	ASN_TO_USER_NME 
                ,ft.FRB_REQ_ID		AS	FTN 
                ,''					AS	H1 
                ,''					AS	H5_H6 
                ,''					AS	CHARS_ID 
                ,NULL				AS	SOWS_EVENT_ID 
                ,0					AS	REQOR_USER_ID   
                ,'FRM'				AS	REQOR_USER_NME 
                ,0					AS	MDSFastTrack   
                ,'Private Customer'	AS	CUST_NME
                ,NULL				AS	FMS_Ckt_ID
                ,ft.STRT_TME		AS	STRT_TMST
                ,ft.END_TME			AS	END_TMST
                ,fu.ENGR_USER_ID	AS	ENG_USER_ID   
                ,Eng.DSPL_NME		AS	ENG_USER_NME					  
                ,fu.ACTV_USER_ID	AS	ACTV_USER_ID   
				,Act.DSPL_NME		AS	ACTV_USER_NME
	FROM		dbo.EVENT						ev	WITH (NOLOCK)
	INNER JOIN	dbo.LK_EVENT_TYPE				evt	WITH (NOLOCK)	ON	ev.EVENT_TYPE_ID	=	evt.EVENT_TYPE_ID	
	INNER JOIN	dbo.FEDLINE_EVENT_USER_DATA		fu	WITH (NOLOCK)	ON	ev.EVENT_ID			=	fu.EVENT_ID
	INNER JOIN	dbo.FEDLINE_EVENT_TADPOLE_DATA	ft	WITH (NOLOCK)	ON	ev.EVENT_ID			=	ft.EVENT_ID
	INNER JOIN  dbo.LK_EVENT_STUS				evs	WITH (NOLOCK)	ON	evs.EVENT_STUS_ID	=	fu.EVENT_STUS_ID
	LEFT JOIN   dbo.LK_USER						Eng WITH (NOLOCK)	ON  Eng.USER_ID			=	fu.ENGR_USER_ID
	LEFT JOIN   dbo.LK_USER						Act WITH (NOLOCK)   ON	Act.USER_ID			=   fu.ACTV_USER_ID
	WHERE		ft.REC_STUS_ID				=	1	
GO