USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[GetCustomerBypass]    Script Date: 10/25/2017 9:57:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		km967761
-- Create date: 10/20/2017
-- Description:	Get Customer Bypass
-- =============================================
ALTER PROCEDURE [dbo].[GetCustomerBypass] -- exec dbo.GetCustomerBypass
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;	

	SELECT
		ID,
		H1_CD AS [H1],
		CASE WHEN (CSG_LVL_ID > 0) THEN NULL ELSE ISNULL(CUST_NME,'') END  AS [CUSTOMER NAME],
		--CONVERT(varchar, DecryptByKey(CUST_NME)) AS [CUSTOMER NAME],
		CASE
			WHEN REC_STUS_ID = 0 THEN 'Inactive'
			ELSE 'Active'
		END [STATUS]
	FROM REDSGN_CUST_BYPASS WITH (NOLOCK)
	ORDER BY ID DESC
END
