USE [COWS]
GO
_CreateObject 'SP','dbo','getRetractEvents'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jrg7298
-- Create date: 09/1/2011
-- Description:	Get all Events to be retracted as part of nightly job.
-- =============================================
ALTER PROCEDURE [dbo].[getRetractEvents] 
AS
BEGIN
SET NOCOUNT ON
Begin Try


	DECLARE @Events TABLE (EVENT_ID INT, EVENT_TYPE_ID TINYINT, IsNewMDS TINYINT)

	INSERT INTO @Events (EVENT_ID, EVENT_TYPE_ID, IsNewMDS)
	SELECT	DISTINCT	me.EVENT_ID,5,1
		FROM	dbo.MDS_EVENT me	WITH (NOLOCK) INNER JOIN
				dbo.EVENT_HIST eh WITH (NOLOCK) ON eh.EVENT_ID=me.EVENT_ID
		WHERE	me.EVENT_STUS_ID	= 3
		 AND DATEDIFF(Hour, eh.CREAT_DT, GETDATE()) >= 24 
		 AND me.WRKFLW_STUS_ID IN (3,8)
	UNION ALL	
	SELECT		me.EVENT_ID,5,1
		FROM	dbo.MDS_EVENT me	WITH (NOLOCK)
		WHERE	EVENT_STUS_ID	= 7
		 AND DATEDIFF(DD, STRT_TMST, GETDATE()) >= 1
		 AND STRT_TMST IS NOT NULL

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END