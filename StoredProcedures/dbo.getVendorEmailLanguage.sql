﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getVendorEmailLanguage'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 21st Sep 2011
-- Description:	Returns Vendor Email Language ID
-- ================================================================

ALTER PROCEDURE [dbo].[getVendorEmailLanguage]
	@VendorID VARCHAR(30),
	@CountryID VARCHAR(2),
	@City VARCHAR(50)
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	DECLARE @LanguageID INT
	IF (SELECT COUNT(*) FROM dbo.LK_VNDR_EMAIL_LANG WITH (NOLOCK)
				WHERE VNDR_CD = @VendorID
				AND CTRY_CD = @CountryID
				AND CTY_NME = @City)	> 0
	BEGIN
		SELECT TOP 1 @LanguageID = ISNULL(VNDR_EMAIL_LANG_ID,0)
		FROM dbo.LK_VNDR_EMAIL_LANG WITH (NOLOCK)
		WHERE VNDR_CD = @VendorID
		AND CTRY_CD = @CountryID
		AND CTY_NME = @City
	END
	ELSE	 
	BEGIN
		IF (SELECT COUNT(*) FROM dbo.LK_VNDR_EMAIL_LANG WITH (NOLOCK)
				WHERE VNDR_CD = @VendorID
				AND CTRY_CD = @CountryID) > 0
		BEGIN
			SELECT TOP 1 @LanguageID = ISNULL(VNDR_EMAIL_LANG_ID,0) FROM dbo.LK_VNDR_EMAIL_LANG WITH (NOLOCK)
				WHERE VNDR_CD = @VendorID
				AND CTRY_CD = @CountryID
				AND CTY_NME IS NULL
		END	
		ELSE
		BEGIN
			SELECT TOP 1 @LanguageID = ISNULL(VNDR_EMAIL_LANG_ID,0) FROM dbo.LK_VNDR_EMAIL_LANG WITH (NOLOCK)
				WHERE VNDR_CD = @VendorID				
				AND CTRY_CD IS NULL
				AND CTY_NME IS NULL
		END
	END					
	SELECT @LanguageID
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
