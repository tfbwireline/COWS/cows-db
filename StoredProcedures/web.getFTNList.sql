USE [COWS]
GO
_CreateObject 'SP','web','getFTNList'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/11/2011
-- Description:	Check the Parent FTN and then gets the related info.
-- =========================================================
ALTER PROCEDURE [web].[getFTNList]
	@ORDR_ID		Int,
	@GRP_ID			SmallInt = 0
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	DECLARE	@Order	TABLE
		(ORDR_ID	Int)

	INSERT INTO @Order	(ORDR_ID)
		VALUES	(@ORDR_ID)
		
	INSERT INTO @Order	(ORDR_ID)
		SELECT			fp.ORDR_ID
			FROM		dbo.FSA_ORDR	fs	WITH (NOLOCK)	
			INNER JOIN	dbo.FSA_ORDR	fp	WITH (NOLOCK)	ON	fs.RELTD_FTN	=	fp.FTN
			WHERE		fs.ORDR_ID	=	@ORDR_ID
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
			
	IF	OBJECT_ID(N'tempdb..#TempFTNResults', N'U') IS NOT NULL 
		DROP TABLE #TempFTNResults
				
	SELECT DISTINCT fs.ORDR_ID					AS	ORDR_ID
					,fs.ORDR_ACTN_ID			AS	ORDR_ACTN_ID
                    ,fs.FTN						AS	FTN
                    --,lp.FSA_PROD_TYPE_DES		AS	PROD_TYPE
					,CASE WHEN (od.PROD_ID='CETH') THEN 'Carrier Ethernet'
							  WHEN (od.PROD_ID='CEPL') THEN 'Carrier Ethernet Private Line'
							  WHEN (od.PROD_ID='CEVPL') THEN 'Carrier Ethernet Virtual Private Line'
							  WHEN (od.PROD_ID='CENNI') THEN 'Carrier Ethernet Network to Network Interface'
							  ELSE lp.FSA_PROD_TYPE_DES		END				AS	PROD_TYPE
                    ,lo.FSA_ORDR_TYPE_DES		AS	ORDR_TYPE
                    ,ISNULL(at.TASK_ID, 0)		AS	TASK_ID
                    ,ISNULL(lu.USER_ADID, '')	AS	USER_ADID
                    ,ISNULL(uw.GRP_ID, 0)		AS	GRP_ID
                    ,Case	
						When	mt.GRP_ID	IN	(5, 6, 7)	Then	lr.GRP_ID
						Else	mt.GRP_ID
					ENd							AS	WG_ID
					,fs.PROD_TYPE_CD			AS	PROD_TYPE_CD
                    ,od.DMSTC_CD				AS	DMSTC_CD
                    ,os.ORDR_STUS_DES			AS	ORDR_STUS_DES
					,lst.ORDR_SUB_TYPE_DES		AS	ORDR_SUB_TYPE
					,dbo.GetOrderTasksByGroup(fs.ORDR_ID, @GRP_ID)	AS ACT_TASK
					,Case	
							When od.CSG_LVL_ID > 0 AND SUBSTRING(dbo.decryptBinaryData(csdfc5.CUST_NME),1,2) = 'O-'	
									Then	'O-Private Customer'
							When od.CSG_LVL_ID > 0 AND SUBSTRING(dbo.decryptBinaryData(csdfc5.CUST_NME),1,2) = 'T-'	
									Then	'T-Private Customer'
							When od.CSG_LVL_ID > 0	Then	'Private Customer'
							Else	fc5.CUST_NME
						End											AS	CUST_NME
		INTO 	#TempFTNResults		
        FROM		@Order					vt
        INNER JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	vt.ORDR_ID			=	od.ORDR_ID
        INNER JOIN	dbo.ORDR				rs	WITH (NOLOCK)	ON	od.PRNT_ORDR_ID		=	rs.ORDR_ID
        INNER JOIN	dbo.ORDR				sd	WITH (NOLOCK)	ON	rs.ORDR_ID			=	sd.PRNT_ORDR_ID
        INNER JOIN	dbo.FSA_ORDR			fs	WITH (NOLOCK)	ON	sd.ORDR_ID			=	fs.ORDR_ID
        INNER JOIN	dbo.LK_FSA_PROD_TYPE	lp	WITH (NOLOCK)	ON	fs.PROD_TYPE_CD		=	lp.FSA_PROD_TYPE_CD
        INNER JOIN	dbo.LK_FSA_ORDR_TYPE	lo	WITH (NOLOCK)	ON	fs.ORDR_TYPE_CD		=	lo.FSA_ORDR_TYPE_CD
        INNER JOIN	dbo.LK_ORDR_STUS		os	WITH (NOLOCK)	ON	sd.ORDR_STUS_ID		=	os.ORDR_STUS_ID
        LEFT JOIN	dbo.ACT_TASK			at	WITH (NOLOCK)	ON	fs.ORDR_ID			=	at.ORDR_ID
																AND	at.STUS_ID			IN	(0, 156)
		INNER JOIN	dbo.LK_XNCI_RGN			lr	WITH (NOLOCK)	ON	lr.RGN_ID			=	ISNULL(od.RGN_ID, 1)
		LEFT JOIN	dbo.MAP_GRP_TASK		mt	WITH (NOLOCK)	ON	at.TASK_ID			=	mt.TASK_ID				
		LEFT JOIN	dbo.USER_WFM_ASMT		uw	WITH (NOLOCK)	ON	fs.ORDR_ID			=	uw.ORDR_ID	
		LEFT JOIN	dbo.LK_USER				lu	WITH (NOLOCK)	ON	uw.ASN_USER_ID		=	lu.USER_ID
		LEFT JOIN	dbo.LK_ORDR_SUB_TYPE	lst	WITH (NOLOCK)	ON	fs.ORDR_SUB_TYPE_CD	=	lst.ORDR_SUB_TYPE_CD
		LEFT JOIN	dbo.FSA_ORDR_CUST		fc5	WITH (NOLOCK)	ON	fs.ORDR_ID		=	fc5.ORDR_ID
																	AND	fc5.CIS_LVL_TYPE	IN	('H5', 'H6')
		LEFT JOIN   dbo.CUST_SCRD_DATA  csdfc5 WITH (NOLOCK) ON csdfc5.SCRD_OBJ_ID=fc5.FSA_ORDR_CUST_ID AND csdfc5.SCRD_OBJ_TYPE_ID=5

	UPDATE t
		SET FTN = CASE WHEN ISNULL(fcpe.DEVICE_ID,'') = '' THEN t.FTN 
						ELSE FTN + '-' + fcpe.DEVICE_ID
					END
		FROM #TempFTNResults t WITH (NOLOCK)
	INNER JOIN dbo.ORDR odr WITH (NOLOCK) ON odr.ORDR_ID = t.ORDR_ID
	INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcpe WITH (NOLOCK) ON fcpe.ORDR_ID = t.ORDR_ID
		WHERE odr.ORDR_CAT_ID = 6
			AND t.PROD_TYPE	=	'CPE' OR (t.PROD_TYPE LIKE 'Carrier Ethernet%')
			AND odr.DMSTC_CD = 0
				
	SELECT * FROM #TempFTNResults WITH (NOLOCK)
			
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END