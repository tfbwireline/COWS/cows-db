USE [COWS]
GO
_CreateObject 'SP','dbo','validateFSAOrder'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/19/2011
-- Description:	Validates an FSA Order and then Rejects it accordingly.
-- =========================================================
ALTER PROCEDURE [dbo].[validateFSAOrder]
	@OrderID	Int
	,@PrntFTN	Varchar(20)
	,@RelFTN	Varchar(20)
	,@RetVal	Bit	OUTPUT
AS
BEGIN
SET NOCOUNT ON;

DECLARE	@RejDes			Varchar(255)
DECLARE	@OrderStatus	SMALLINT
SET	@RejDes			=	''
SET	@RetVal			=	0
SET	@OrderStatus	=	0
	
Begin Try
	IF	@PrntFTN	!=	''
		BEGIN
			SELECT			@OrderStatus	=	ORDR_STUS_ID
				FROM		dbo.FSA_ORDR	fs	WITH (NOLOCK)
				INNER JOIN	dbo.ORDR		od	WITH (NOLOCK)	ON	fs.ORDR_ID	=	od.ORDR_ID
					WHERE	fs.FTN		=	@PrntFTN
			-------------------------------------------------------------------------------
			--	Reject the Move orders if the Parent FTN is NOT complete.
			-------------------------------------------------------------------------------
			IF	@RelFTN	!=	''	AND	@OrderStatus	!=	0
				BEGIN
					SET	@RejDes	=	'Move order received while the Related FTN is still in processing.'
				END		
			-------------------------------------------------------------------------------
			--	Reject the Cancels if the Parent FTN is complete.
			-------------------------------------------------------------------------------
			ELSE IF	@RelFTN	=	''	AND	@OrderStatus	=	0
				BEGIN
					SET	@RejDes	=	'Cancel order cannot be received after the Parent FTN is Bill Activated.'
				END			
		END

	IF	@RejDes	!=	''
		BEGIN
			SEt	@RetVal	=	1
			-----------------------------------------------------------
			--	Delete the Tasks for this order.
			-----------------------------------------------------------
			DELETE FROM dbo.WG_PTRN_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @OrderID
			DELETE FROM dbo.WG_PROF_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @OrderID
			DELETE FROM dbo.ACT_TASK		WITH (ROWLOCK) WHERE ORDR_ID = @OrderID
		
			-----------------------------------------------------------
			--	Load FSA Reject Messages and Populate the Notes. 
			-----------------------------------------------------------
			INSERT INTO dbo.FSA_ORDR_MSG	WITH (ROWLOCK)
						(ORDR_ID	,FSA_MSG_ID,	STUS_ID)
				VALUES	(@OrderID	,4				,10)

			EXEC	dbo.insertOrderNotes
					@OrderID	,9	,@RejDes	,	1
					
			SET @RetVal	=	1		
		END
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO