USE COWS
GO
_CreateObject 'SP','web','getWebUserInfoFromUserName'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jagannath Gangi
-- Create date: 04-20-2011
-- Description:	get all info using username
-- =============================================
ALTER PROCEDURE [web].[getWebUserInfoFromUserName]
@sUserName	varchar (10)
AS

BEGIN

SET NOCOUNT ON ;

BEGIN TRY
	SELECT		[USER_ID] as WebUserID,
				[USER_ADID] as UserName,
				[USER_ACF2_ID] as Acf2Id,
				[FULL_NME] as FullName,
				[PHN_NBR] as Phone,
				[EMAIL_ADR] as Email
		FROM	dbo.LK_USER	WITH (NOLOCK) 
		WHERE	USER_ADID	=	@sUserName 
END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH
SET NOCOUNT OFF
	
END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
