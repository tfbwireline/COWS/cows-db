USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[ValidateUserCSG]    Script Date: 09/24/2021 12:12:05 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Author : Jagannath Gangi
Created On : 4-1-2011
This SP cleans up lk_user, user_csg_lvl table data by checking ldap active/inactive records
Modified: km967761 - 06/23/2021 - OneId changes; Replace USER_ADID -> OLD_USER_ADID
Modified: jrg7298 - 7/21/21 - Modified to query ldap by name/email and location
*/
ALTER PROCEDURE [dbo].[ValidateUserCSG] (
	@REQUESTORUSERIDENTIFIER AS VARCHAR(10)
	,@UserID AS INT
	)
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		DECLARE @InputTxt VARCHAR(1000) = ''

		SET @InputTxt = @REQUESTORUSERIDENTIFIER + '|' + CONVERT(VARCHAR, @UserID)

		DECLARE @COMMANDSTRING AS NVARCHAR(MAX)
		DECLARE @Str AS NVARCHAR(MAX)
		DECLARE @Str1 AS NVARCHAR(MAX)
		DECLARE @Str2 AS NVARCHAR(MAX)
		DECLARE @Str3 AS NVARCHAR(MAX)
		DECLARE @Str_gen AS NVARCHAR(MAX)
		DECLARE @StringUnion AS NVARCHAR(MAX)

		SET @COMMANDSTRING = ''
		SET @Str = ''
		SET @Str1 = ''
		SET @Str2 = ''
		SET @Str3 = ''
		SET @Str_gen = ''
		SET @StringUnion = ' UNION ALL '

		DECLARE @USER_ADID NVARCHAR(10);
		DECLARE @OLD_USER_ADID NVARCHAR(10);
		DECLARE @ActionFlag VARCHAR(2)
		DECLARE @CSGLevel INT
		DECLARE @lastname VARCHAR(100)
		DECLARE @firstname VARCHAR(100)
		DECLARE @middlename VARCHAR(100)
		DECLARE @sttcd VARCHAR(50)
		DECLARE @city VARCHAR(100)
		DECLARE @EMAIL VARCHAR(200)
		DECLARE @EMAIL_TMO VARCHAR(200)
		DECLARE @EMAIL_SPRINT VARCHAR(200)

		IF OBJECT_ID(N'tempdb..#CSGLevel', N'U') IS NOT NULL
			DROP TABLE #CSGLevel

		CREATE TABLE #CSGLevel (
			[USER_ID] VARCHAR(20)
			,[CSG_LVL_ID] VARCHAR(10)
			,[EMPL_ACT_CD] VARCHAR(2)
			,[EMAIL_ADR] VARCHAR(100)
			,[DSPL_NME] VARCHAR(100)
			,[FIRST_NME] VARCHAR(100)
			,[MIDDLE_NME] VARCHAR(100)
			,[LAST_NME] VARCHAR(100)
			,[PHN_NBR] VARCHAR(50)
			,[CELL_PHN_NBR] VARCHAR(20)
			,[CTY_NME] VARCHAR(100)
			,[STT_CD] VARCHAR(50)
			,[FullName] VARCHAR(100)
			,[CREAT_BY_USER_ID] INT
			,[CREAT_DT] SMALLDATETIME
			)

		--Split the fullname into a cte and load into variables
		DECLARE @userData TABLE (
			RowN INT IDENTITY(1, 1)
			,NamePart VARCHAR(100)
			)

		INSERT INTO @userData (NamePart)
		SELECT value
		FROM dbo.lk_user WITH (NOLOCK)
		CROSS APPLY STRING_SPLIT(full_nme, ' ')
		WHERE user_adid = @REQUESTORUSERIDENTIFIER

		IF (
				(
					(
						SELECT COUNT(1)
						FROM @userData
						) > 3
					)
				OR (
					(
						SELECT COUNT(1)
						FROM @userData
						) <= 1
					)
				) --Name could not be parsed so email & location code needs to be used
		BEGIN
			SELECT @EMAIL = EMAIL_ADR
				,@sttcd = STT_CD
				,@city = CTY_NME
			FROM lk_user WITH (NOLOCK)
			WHERE USER_ADID = @REQUESTORUSERIDENTIFIER

			SET @EMAIL_TMO = REPLACE(@EMAIL, '@sprint.com', '@t-mobile.com')
			SET @EMAIL_SPRINT = REPLACE(@EMAIL, '@t-mobile.com', '@sprint.com')

			IF (LEN(@EMAIL) > 0)
			BEGIN
				SET @Str = ' INSERT INTO #CSGLevel(  [CREAT_BY_USER_ID],
													 [CREAT_DT],
													 [CSG_LVL_ID],
													 [USER_ID],
													 [EMPL_ACT_CD],
													 [DSPL_NME],
													 [FIRST_NME],
													 [MIDDLE_NME],
													 [LAST_NME],											 
													 [EMAIL_ADR],											 
													 [PHN_NBR],
													 [CELL_PHN_NBR],
													 [CTY_NME],
													 [STT_CD]) '
													 
				SET @Str1 = 'SELECT 1, GETDATE(), ''00001'', *
								FROM OPENQUERY(ADAM,''<LDAP://IDMData.corp.sprint.com:389/OU=People,O=SPrint,C=US>;(&(fonConSecGrp=00001)'

				IF (LEN(@sttcd) > 0)
					SET @Str1 = @Str1 + '(st='+@sttcd+')'
				IF (LEN(@city) > 0)
					SET @Str1 = @Str1 + '(l='+@city+')'

				SET @Str1 = @Str1 + '(|(mail='+@EMAIL_SPRINT+')(mail='+@EMAIL_TMO+'))(objectCategory=person));
										sAMAccountName,  fonEmpStatus,  displayName,  givenName,  
										middleName,   	sn, 	mail,	telephonenumber, 
										mobile, l, st'') ' 

				SET @Str2 = 'SELECT 1, GETDATE(), ''00002'', *
								FROM OPENQUERY(ADAM,''<LDAP://IDMData.corp.sprint.com:389/OU=People,O=SPrint,C=US>;(&(fonConSecGrp=00002)'

				IF (LEN(@sttcd) > 0)
					SET @Str2 = @Str2 + '(st='+@sttcd+')'
				IF (LEN(@city) > 0)
					SET @Str2 = @Str2 + '(l='+@city+')'

				SET @Str2 = @Str2 + '(|(mail='+@EMAIL_SPRINT+')(mail='+@EMAIL_TMO+'))(objectCategory=person));
										sAMAccountName,  fonEmpStatus,  displayName,  givenName,  
										middleName,   	sn, 	mail,	telephonenumber, 
										mobile, l, st'') ' 

				SET @Str3 = 'SELECT 1, GETDATE(), ''00003'', *
								FROM OPENQUERY(ADAM,''<LDAP://IDMData.corp.sprint.com:389/OU=People,O=SPrint,C=US>;(&(fonConSecGrp=00003)'

				IF (LEN(@sttcd) > 0)
					SET @Str3 = @Str3 + '(st='+@sttcd+')'
				IF (LEN(@city) > 0)
					SET @Str3 = @Str3 + '(l='+@city+')'
				SET @Str3 = @Str3 + '(|(mail='+@EMAIL_SPRINT+')(mail='+@EMAIL_TMO+'))(objectCategory=person));
										sAMAccountName,  fonEmpStatus,  displayName,  givenName,  
										middleName,   	sn, 	mail,	telephonenumber, 
										mobile, l, st'') ' 
				SET @COMMANDSTRING = @Str + '' + @Str1 + '' + @StringUnion + '' + @Str2 + '' + @StringUnion + '' + @Str3 + ''

				EXEC sp_executesql @COMMANDSTRING


				DELETE ucl
				FROM dbo.USER_CSG_LVL ucl WITH (ROWLOCK)
				WHERE ucl.[USER_ID] = @UserID

				IF (
						SELECT COUNT(1)
						FROM #CSGLevel
						) = 0
				BEGIN
					SET @Str_gen = 'SELECT 1, GETDATE(), '''', *
								FROM OPENQUERY(ADAM,''<LDAP://IDMData.corp.sprint.com:389/OU=People,O=SPrint,C=US>;(&'

					IF (LEN(@sttcd) > 0)
						SET @Str_gen = @Str_gen + '(st='+@sttcd+')'
					IF (LEN(@city) > 0)
						SET @Str_gen = @Str_gen + '(l='+@city+')'
					IF (LEN(@sttcd) > 0) OR (LEN(@city) > 0)
						SET @Str_gen = @Str_gen + ' ('
					SET @Str_gen = @Str_gen + '(|(mail='+@EMAIL_SPRINT+')(mail='+@EMAIL_TMO+'))'

					IF (LEN(@sttcd) > 0) OR (LEN(@city) > 0)
						SET @Str_gen = @Str_gen + ')'
					SET @Str_gen = @Str_gen + '(objectCategory=person));
										sAMAccountName,  fonEmpStatus,  displayName,  givenName,  
										middleName,   	sn, 	mail,	telephonenumber, 
										mobile, l, st'') ' 
					SET @COMMANDSTRING = @Str + '' + @Str_gen

					EXEC sp_executesql @COMMANDSTRING
				END

				SET @ActionFlag = ''

				SELECT TOP 1 @ActionFlag = EMPL_ACT_CD
					,@CSGLevel = CSG_LVL_ID
				FROM #CSGLevel WITH (NOLOCK)
				WHERE (
						EMAIL_ADR = @EMAIL_SPRINT
						OR EMAIL_ADR = @EMAIL_TMO
						)

				IF (
						SELECT COUNT(1)
						FROM #CSGLevel
						) > 0
				BEGIN
					UPDATE lu
					SET EMAIL_ADR = COALESCE(cs.EMAIL_ADR, '')
						,PHN_NBR = CASE 
							WHEN ISNULL(lu.PHN_NBR, '') = ''
								THEN COALESCE(cs.PHN_NBR, '')
							ELSE lu.PHN_NBR
							END
						,[CELL_PHN_NBR] = CASE 
							WHEN ISNULL(lu.[CELL_PHN_NBR], '') = ''
								THEN COALESCE(cs.CELL_PHN_NBR, '')
							ELSE lu.[CELL_PHN_NBR]
							END
						,DSPL_NME = REPLACE(COALESCE(cs.DSPL_NME, ''), '''', ' ')
						,FULL_NME = REPLACE(CASE LEN(COALESCE(cs.MIDDLE_NME, ''))
								WHEN 0
									THEN COALESCE(cs.FIRST_NME, '') + ' ' + COALESCE(cs.LAST_NME, '')
								ELSE COALESCE(cs.FIRST_NME, '') + ' ' + COALESCE(cs.MIDDLE_NME, '') + ' ' + COALESCE(cs.LAST_NME, '')
								END, '''', ' ')
						,[CTY_NME] = COALESCE(cs.CTY_NME, '')
						,[STT_CD] = COALESCE(cs.STT_CD, '')
						,[REC_STUS_ID] = CASE 
							WHEN @ActionFlag IN (
									'A'
									,'P'
									)
								THEN lu.[REC_STUS_ID]
							ELSE 0
							END
						,MODFD_BY_USER_ID = 1
						,MODFD_DT = GETDATE()
					FROM dbo.LK_USER lu WITH (ROWLOCK)
					INNER JOIN #CSGLevel cs WITH (NOLOCK) ON cs.STT_CD = lu.STT_CD
						AND (
							REPLACE(cs.EMAIL_ADR, '@t-mobile.com', '@sprint.com') = lu.EMAIL_ADR
							OR REPLACE(cs.EMAIL_ADR, '@sprint.com', '@t-mobile.com') = lu.EMAIL_ADR
							)
					WHERE lu.[USER_ADID] = @REQUESTORUSERIDENTIFIER

					-- DEACTIVATE ACTIVE MAP_USR_PRF FOR INACTIVE LK_USER
					UPDATE mup
					SET REC_STUS_ID = 0
						,MODFD_BY_USER_ID = 1
						,MODFD_DT = GETDATE()
					FROM dbo.LK_USER lu WITH (NOLOCK)
					INNER JOIN dbo.MAP_USR_PRF mup WITH (ROWLOCK) ON mup.[USER_ID] = lu.[USER_ID]
					WHERE lu.REC_STUS_ID = 0
						AND lu.[USER_ADID] = @REQUESTORUSERIDENTIFIER
						AND mup.[REC_STUS_ID] = 1

					SELECT TOP 1 @USER_ADID = TRIM([USER_ID])
						,@EMAIL = EMAIL_ADR
					FROM #CSGLevel
					WHERE CHARINDEX('@t-mobile.com', @EMAIL) > 0

					-- Check whether ADID or NTID
					IF CHARINDEX('@t-mobile.com', @EMAIL) > 0
					BEGIN
						SELECT @OLD_USER_ADID = OLD_USER_ADID
						FROM dbo.LK_USER WITH (NOLOCK)
						WHERE USER_ADID = @REQUESTORUSERIDENTIFIER;

						IF @OLD_USER_ADID IS NULL
						BEGIN
							INSERT INTO dbo.SYS_LOG (
								Notes
								,Process
								,CreatDt
								)
							VALUES (
								'USER_ID:' + CONVERT(VARCHAR, @UserID) + ',@USER_ADID:' + @USER_ADID + ';@EMAIL:' + @EMAIL
								,'ValidateUserCSG'
								,GETDATE()
								)

							UPDATE lu
							SET OLD_USER_ADID = TRIM(lu.USER_ADID)
								,USER_ADID = TRIM(@USER_ADID)
								,EMAIL_ADR = @EMAIL
								,MODFD_BY_USER_ID = 1
								,MODFD_DT = GETDATE()
							FROM dbo.LK_USER lu WITH (ROWLOCK)
							WHERE lu.[USER_ID] = @UserID
								AND OLD_USER_ADID IS NULL

							SELECT @USER_ADID = USER_ADID
								,@OLD_USER_ADID = OLD_USER_ADID
							FROM dbo.LK_USER WITH (NOLOCK)
							WHERE OLD_USER_ADID = @REQUESTORUSERIDENTIFIER;

							-- UPDATE RELATED ADID's	
							-- MDS_EVENT
							UPDATE MDS_EVENT
							SET MNS_PM_ID = @USER_ADID
							WHERE MNS_PM_ID = @OLD_USER_ADID

							-- UCaaS_EVENT
							UPDATE UCaaS_EVENT
							SET MNS_PM_ID = @USER_ADID
							WHERE MNS_PM_ID = @OLD_USER_ADID

							-- REDSGN - PM_ASSIGNED
							UPDATE REDSGN
							SET PM_ASSIGNED = @USER_ADID
							WHERE PM_ASSIGNED = @OLD_USER_ADID

							-- REDSGN - PM_ASSIGNED
							UPDATE REDSGN
							SET NTE_ASSIGNED = @USER_ADID
							WHERE NTE_ASSIGNED = @OLD_USER_ADID

							-- REDSGN - SDE_ASN_NME
							UPDATE REDSGN
							SET SDE_ASN_NME = @USER_ADID
							WHERE SDE_ASN_NME = @OLD_USER_ADID

							-- REDSGN - NE_ASN_NME
							UPDATE REDSGN
							SET NE_ASN_NME = @USER_ADID
							WHERE NE_ASN_NME = @OLD_USER_ADID
						END
					END
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT 'x' FROM dbo.LK_USER WITH (NOLOCK) WHERE [USER_ADID] = @REQUESTORUSERIDENTIFIER AND REC_STUS_ID=1)
					BEGIN
						INSERT INTO dbo.SYS_LOG (
								Notes
								,Process
								,CreatDt
								)
							VALUES (
								'Setting to Inactive, USER_ID:' + CONVERT(VARCHAR, @UserID) + ',@USER_ADID:' + @USER_ADID + ';@EMAIL:' + @EMAIL
								,'ValidateUserCSG'
								,GETDATE()
								)
					END
					UPDATE dbo.LK_USER
					WITH (ROWLOCK)
					SET REC_STUS_ID = 0
						,MODFD_BY_USER_ID = 1
						,MODFD_DT = GETDATE()
					WHERE [USER_ADID] = @REQUESTORUSERIDENTIFIER

					-- DEACTIVATE ACTIVE MAP_USR_PRF FOR INACTIVE LK_USER
					UPDATE mup
					SET REC_STUS_ID = 0
						,MODFD_BY_USER_ID = 1
						,MODFD_DT = GETDATE()
					FROM dbo.LK_USER lu WITH (NOLOCK)
					INNER JOIN dbo.MAP_USR_PRF mup WITH (ROWLOCK) ON mup.[USER_ID] = lu.[USER_ID]
					WHERE lu.REC_STUS_ID = 0
						AND lu.[USER_ADID] = @REQUESTORUSERIDENTIFIER
						AND mup.[REC_STUS_ID] = 1
				END
			END
		END
		ELSE
		BEGIN
			IF (
					SELECT COUNT(1)
					FROM @userData
					) = 2
			BEGIN
				SELECT @firstname = REPLACE(NamePart, '''', '''''''''')
				FROM @userData
				WHERE RowN = 1

				SELECT @lastname = REPLACE(NamePart, '''', '''''''''')
				FROM @userData
				WHERE RowN = 2
			END
			ELSE IF (
					SELECT COUNT(1)
					FROM @userData
					) = 3
			BEGIN
				SELECT @firstname = REPLACE(NamePart, '''', '''''''''')
				FROM @userData
				WHERE RowN = 1

				SELECT @middlename = REPLACE(NamePart, '''', '''''''''')
				FROM @userData
				WHERE RowN = 2

				SELECT @lastname = REPLACE(NamePart, '''', '''''''''')
				FROM @userData
				WHERE RowN = 3
			END

			SELECT @EMAIL = EMAIL_ADR
				,@sttcd = STT_CD
				,@city = CTY_NME
			FROM dbo.lk_user WITH (NOLOCK)
			WHERE USER_ADID = @REQUESTORUSERIDENTIFIER

			SET @Str = ' INSERT INTO #CSGLevel(  [CREAT_BY_USER_ID],
													 [CREAT_DT],
													 [CSG_LVL_ID],
													 [USER_ID],
													 [EMPL_ACT_CD],
													 [DSPL_NME],
													 [FIRST_NME],
													 [MIDDLE_NME],
													 [LAST_NME],											 
													 [EMAIL_ADR],											 
													 [PHN_NBR],
													 [CELL_PHN_NBR],
													 [CTY_NME],
													 [STT_CD]) '
			SET @Str1 = 'SELECT 1, GETDATE(), ''00001'', *
								FROM OPENQUERY(ADAM,''<LDAP://IDMData.corp.sprint.com:389/OU=People,O=SPrint,C=US>;(&(fonConSecGrp=00001)'
			IF (LEN(@sttcd) > 0)
				SET @Str1 = @Str1 + '(st='+@sttcd+')'
			IF (LEN(@city) > 0)
				SET @Str1 = @Str1 + '(l='+@city+')'
			IF (LEN(@middlename) > 0)
				BEGIN
					-- Details : Condtions works like this OR(Condition1, Condition2, Condition3)
					-- Condition 1: FNAME && MNAME && LNAME
					-- Condition 2: FNAME MNAME && LNAME
					-- Condition 3: FNAME && MNAME LNAME
					-- Condition 4: FNAME && MNAME'LNAME | Added apostrophe
					SET @Str1 = @Str1 + '(|(&(givenname='+@firstname+')(middlename='+@middlename+')(sn='+@lastname+'))(&(givenname='+@firstname+' '+@middlename+')(sn='+@lastname+ '))(&(givenname='+@firstname+')(sn='+@middlename+' '+@lastname+'))(&(givenname='+@firstname+')(sn='+@middlename+''''''+@lastname+')))'
				END
			ELSE
				BEGIN
					SET @Str1 = @Str1 + '(givenname='+@firstname+')'
					SET @Str1 = @Str1 + '(sn='+@lastname+')'
				END
			SET @Str1 = @Str1 + '(objectCategory=person));
									sAMAccountName,  fonEmpStatus,  displayName,  givenName,  
									middleName,   	sn, 	mail,	telephonenumber, 
									mobile, l, st'') '
									 

			SET @Str2 = 'SELECT 1, GETDATE(), ''00002'', *
								FROM OPENQUERY(ADAM,''<LDAP://IDMData.corp.sprint.com:389/OU=People,O=SPrint,C=US>;(&(fonConSecGrp=00002)'
			IF (LEN(@sttcd) > 0)
				SET @Str2 = @Str2 + '(st='+@sttcd+')'
			IF (LEN(@city) > 0)
				SET @Str2 = @Str2 + '(l='+@city+')'
			IF (LEN(@middlename) > 0)
				BEGIN
					-- Details : Condtions works like this OR(Condition1, Condition2, Condition3)
					-- Condition 1: FNAME && MNAME && LNAME
					-- Condition 2: FNAME MNAME && LNAME
					-- Condition 3: FNAME && MNAME LNAME
					-- Condition 4: FNAME && MNAME'LNAME | Added apostrophe
					SET @Str2 = @Str2 + '(|(&(givenname='+@firstname+')(middlename='+@middlename+')(sn='+@lastname+'))(&(givenname='+@firstname+' '+@middlename+')(sn='+@lastname+ '))(&(givenname='+@firstname+')(sn='+@middlename+' '+@lastname+'))(&(givenname='+@firstname+')(sn='+@middlename+''''''+@lastname+')))'
				END
			ELSE
				BEGIN
					SET @Str2 = @Str2 + '(givenname='+@firstname+')'
					SET @Str2 = @Str2 + '(sn='+@lastname+')'
				END
			SET @Str2 = @Str2 + '(objectCategory=person));
									sAMAccountName,  fonEmpStatus,  displayName,  givenName,  
									middleName,   	sn, 	mail,	telephonenumber, 
									mobile, l, st'') '


			SET @Str3 = 'SELECT 1, GETDATE(), ''00003'', *
								FROM OPENQUERY(ADAM,''<LDAP://IDMData.corp.sprint.com:389/OU=People,O=SPrint,C=US>;(&(fonConSecGrp=00003)'

			IF (LEN(@sttcd) > 0)
				SET @Str3 = @Str3 + '(st='+@sttcd+')'
			IF (LEN(@city) > 0)
				SET @Str3 = @Str3 + '(l='+@city+')'
			IF (LEN(@middlename) > 0)
				BEGIN
					-- Details : Condtions works like this OR(Condition1, Condition2, Condition3)
					-- Condition 1: FNAME && MNAME && LNAME
					-- Condition 2: FNAME MNAME && LNAME
					-- Condition 3: FNAME && MNAME LNAME
					-- Condition 4: FNAME && MNAME'LNAME | Added apostrophe
					SET @Str3 = @Str3 + '(|(&(givenname='+@firstname+')(middlename='+@middlename+')(sn='+@lastname+'))(&(givenname='+@firstname+' '+@middlename+')(sn='+@lastname+ '))(&(givenname='+@firstname+')(sn='+@middlename+' '+@lastname+'))(&(givenname='+@firstname+')(sn='+@middlename+''''''+@lastname+')))'
				END
			ELSE
				BEGIN
					SET @Str3 = @Str3 + '(givenname='+@firstname+')'
					SET @Str3 = @Str3 + '(sn='+@lastname+')'
				END
			SET @Str3 = @Str3 + '(objectCategory=person));
									sAMAccountName,  fonEmpStatus,  displayName,  givenName,  
									middleName,   	sn, 	mail,	telephonenumber, 
									mobile, l, st'') '
			SET @COMMANDSTRING = @Str + '' + @Str1 + '' + @StringUnion + '' + @Str2 + '' + @StringUnion + '' + @Str3 + ''

			EXEC sp_executesql @COMMANDSTRING

			DELETE ucl
			FROM dbo.USER_CSG_LVL ucl WITH (ROWLOCK)
			WHERE ucl.[USER_ID] = @UserID

			IF (
					SELECT COUNT(1)
					FROM #CSGLevel
					) = 0
			BEGIN
				SET @Str_gen = 'SELECT 1, GETDATE(), '''', *
								FROM OPENQUERY(ADAM,''<LDAP://IDMData.corp.sprint.com:389/OU=People,O=SPrint,C=US>;(&'


				IF (LEN(@sttcd) > 0)
					SET @Str_gen = @Str_gen + '(st='+@sttcd+')'
				IF (LEN(@city) > 0)
					SET @Str_gen = @Str_gen + '(l='+@city+')'
				IF (LEN(@middlename) > 0)
					BEGIN
						-- Details : Condtions works like this OR(Condition1, Condition2, Condition3)
						-- Condition 1: FNAME && MNAME && LNAME
						-- Condition 2: FNAME MNAME && LNAME
						-- Condition 3: FNAME && MNAME LNAME
						-- Condition 4: FNAME && MNAME'LNAME | Added apostrophe
					SET @Str_gen = @Str_gen + '(|(&(givenname='+@firstname+')(middlename='+@middlename+')(sn='+@lastname+'))(&(givenname='+@firstname+' '+@middlename+')(sn='+@lastname+ '))(&(givenname='+@firstname+')(sn='+@middlename+' '+@lastname+'))(&(givenname='+@firstname+')(sn='+@middlename+''''''+@lastname+')))'
					END
				ELSE
					BEGIN
						SET @Str_gen = @Str_gen + '(givenname='+@firstname+')'
						SET @Str_gen = @Str_gen + '(sn='+@lastname+')'
					END
				SET @Str_gen = @Str_gen + '(objectCategory=person));
										sAMAccountName,  fonEmpStatus,  displayName,  givenName,  
										middleName,   	sn, 	mail,	telephonenumber, 
										mobile, l, st'') '
				SET @COMMANDSTRING = @Str + '' + @Str_gen

				EXEC sp_executesql @COMMANDSTRING
			END

			SET @ActionFlag = ''

			SELECT TOP 1 @ActionFlag = EMPL_ACT_CD
				,@CSGLevel = CSG_LVL_ID
			FROM #CSGLevel WITH (NOLOCK)

			IF (
					SELECT COUNT(1)
					FROM #CSGLevel
					) > 0
			BEGIN
				UPDATE lu
				SET EMAIL_ADR = COALESCE(cs.EMAIL_ADR, '')
					,PHN_NBR = CASE 
						WHEN ISNULL(lu.PHN_NBR, '') = ''
							THEN COALESCE(cs.PHN_NBR, '')
						ELSE lu.PHN_NBR
						END
					,[CELL_PHN_NBR] = CASE 
						WHEN ISNULL(lu.[CELL_PHN_NBR], '') = ''
							THEN COALESCE(cs.CELL_PHN_NBR, '')
						ELSE lu.[CELL_PHN_NBR]
						END
					,DSPL_NME = REPLACE(COALESCE(cs.DSPL_NME, ''), '''', ' ')
					,FULL_NME = REPLACE(CASE LEN(COALESCE(cs.MIDDLE_NME, ''))
							WHEN 0
								THEN COALESCE(cs.FIRST_NME, '') + ' ' + COALESCE(cs.LAST_NME, '')
							ELSE COALESCE(cs.FIRST_NME, '') + ' ' + COALESCE(cs.MIDDLE_NME, '') + ' ' + COALESCE(cs.LAST_NME, '')
							END, '''', ' ')
					,[CTY_NME] = COALESCE(cs.CTY_NME, '')
					,[STT_CD] = COALESCE(cs.STT_CD, '')
					,[REC_STUS_ID] = CASE 
						WHEN @ActionFlag IN (
								'A'
								,'P'
								)
							THEN lu.[REC_STUS_ID]
						ELSE 0
						END
					,MODFD_DT = GETDATE()
					,MODFD_BY_USER_ID = 1
				FROM dbo.LK_USER lu WITH (ROWLOCK)
				INNER JOIN #CSGLevel cs WITH (NOLOCK) ON cs.FullName = lu.FULL_NME
				WHERE lu.[USER_ADID] = @REQUESTORUSERIDENTIFIER

				-- DEACTIVATE ACTIVE MAP_USR_PRF FOR INACTIVE LK_USER
				UPDATE mup
				SET REC_STUS_ID = 0
					,MODFD_BY_USER_ID = 1
					,MODFD_DT = GETDATE()
				FROM dbo.LK_USER lu WITH (NOLOCK)
				INNER JOIN dbo.MAP_USR_PRF mup WITH (ROWLOCK) ON mup.[USER_ID] = lu.[USER_ID]
				WHERE lu.REC_STUS_ID = 0
					AND lu.[USER_ADID] = @REQUESTORUSERIDENTIFIER
					AND mup.[REC_STUS_ID] = 1

				SELECT TOP 1 @USER_ADID = TRIM([USER_ID])
					,@EMAIL = EMAIL_ADR
				FROM #CSGLevel
				WHERE CHARINDEX('@t-mobile.com', EMAIL_ADR) > 0

				-- Check whether ADID or NTID
				IF CHARINDEX('@t-mobile.com', @EMAIL) > 0
				BEGIN
					SELECT @OLD_USER_ADID = OLD_USER_ADID
					FROM dbo.LK_USER WITH (NOLOCK)
					WHERE USER_ADID = @REQUESTORUSERIDENTIFIER;

					IF @OLD_USER_ADID IS NULL
					BEGIN
						INSERT INTO dbo.SYS_LOG (
							Notes
							,Process
							,CreatDt
							)
						VALUES (
							'USER_ID:' + CONVERT(VARCHAR, @UserID) + ',@USER_ADID:' + @USER_ADID + ';@EMAIL:' + @EMAIL
							,'ValidateUserCSG'
							,GETDATE()
							)

						UPDATE lu
						SET OLD_USER_ADID = TRIM(lu.USER_ADID)
							,USER_ADID = TRIM(@USER_ADID)
							,EMAIL_ADR = @EMAIL
							,MODFD_BY_USER_ID = 1
							,MODFD_DT = GETDATE()
						FROM dbo.LK_USER lu WITH (ROWLOCK)
						WHERE lu.[USER_ID] = @UserID
							AND OLD_USER_ADID IS NULL

						SELECT @USER_ADID = USER_ADID
							,@OLD_USER_ADID = OLD_USER_ADID
						FROM dbo.LK_USER WITH (NOLOCK)
						WHERE OLD_USER_ADID = @REQUESTORUSERIDENTIFIER;

						-- UPDATE RELATED ADID's	
						-- MDS_EVENT
						UPDATE MDS_EVENT
						SET MNS_PM_ID = @USER_ADID
						WHERE MNS_PM_ID = @OLD_USER_ADID

						-- UCaaS_EVENT
						UPDATE UCaaS_EVENT
						SET MNS_PM_ID = @USER_ADID
						WHERE MNS_PM_ID = @OLD_USER_ADID

						-- REDSGN - PM_ASSIGNED
						UPDATE REDSGN
						SET PM_ASSIGNED = @USER_ADID
						WHERE PM_ASSIGNED = @OLD_USER_ADID

						-- REDSGN - PM_ASSIGNED
						UPDATE REDSGN
						SET NTE_ASSIGNED = @USER_ADID
						WHERE NTE_ASSIGNED = @OLD_USER_ADID

						-- REDSGN - SDE_ASN_NME
						UPDATE REDSGN
						SET SDE_ASN_NME = @USER_ADID
						WHERE SDE_ASN_NME = @OLD_USER_ADID

						-- REDSGN - NE_ASN_NME
						UPDATE REDSGN
						SET NE_ASN_NME = @USER_ADID
						WHERE NE_ASN_NME = @OLD_USER_ADID
					END
				END
			END
			ELSE
			BEGIN
				IF EXISTS (SELECT 'x' FROM dbo.LK_USER WITH (NOLOCK) WHERE [USER_ADID] = @REQUESTORUSERIDENTIFIER AND REC_STUS_ID=1)
					BEGIN
						INSERT INTO dbo.SYS_LOG (
								Notes
								,Process
								,CreatDt
								)
							VALUES (
								'Setting to Inactive, USER_ID:' + CONVERT(VARCHAR, @UserID) + ',@USER_ADID:' + @REQUESTORUSERIDENTIFIER + ';@EMAIL:' + @EMAIL
								,'ValidateUserCSG'
								,GETDATE()
								)
					END

				UPDATE dbo.LK_USER
				WITH (ROWLOCK)
				SET REC_STUS_ID = 0
					,MODFD_BY_USER_ID = 1
					,MODFD_DT = GETDATE()
				WHERE [USER_ADID] = @REQUESTORUSERIDENTIFIER

				-- DEACTIVATE ACTIVE MAP_USR_PRF FOR INACTIVE LK_USER
				UPDATE mup
				SET REC_STUS_ID = 0
					,MODFD_BY_USER_ID = 1
					,MODFD_DT = GETDATE()
				FROM dbo.LK_USER lu WITH (NOLOCK)
				INNER JOIN dbo.MAP_USR_PRF mup WITH (ROWLOCK) ON mup.[USER_ID] = lu.[USER_ID]
				WHERE lu.REC_STUS_ID = 0
					AND lu.[USER_ADID] = @REQUESTORUSERIDENTIFIER
					AND mup.[REC_STUS_ID] = 1
			END
		END --Name, Location Code

		IF (
				@ActionFlag IN (
					'A'
					,'P'
					)
				AND ISNULL(@CSGLevel, '') <> ''
				)
		BEGIN
			INSERT INTO USER_CSG_LVL (
				[USER_ID]
				,[CSG_LVL_ID]
				,[EMPL_ACT_CD]
				,[REC_STUS_ID]
				,[CREAT_BY_USER_ID]
				,[MODFD_BY_USER_ID]
				,[MODFD_DT]
				,[CREAT_DT]
				)
			SELECT DISTINCT lu.[USER_ID]
				,cl.CSG_LVL_CD
				,cs.[EMPL_ACT_CD]
				,1
				,1
				,1
				,GETDATE()
				,GETDATE()
			FROM #CSGLevel cs WITH (NOLOCK)
			INNER JOIN LK_USER lu WITH (NOLOCK) ON lu.USER_ADID = cs.[USER_ID]
			INNER JOIN LK_CSG_LVL cl WITH (NOLOCK) ON cs.CSG_LVL_ID = cl.CSG_LVL_ID
			WHERE ISNULL(cs.[CSG_LVL_ID], '') <> ''
			AND NOT EXISTS (SELECT 'X' FROM dbo.USER_CSG_LVL ucl2 WITH (NOLOCK) WHERE ucl2.[USER_ID]=lu.[USER_ID] AND ucl2.[CSG_LVL_ID]=cl.CSG_LVL_CD)
		END
	END TRY

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo] @InputTxt
	END CATCH
END
