USE [COWS]
GO
_CreateObject 'SP','dbo','unlockTimedOutOrdersEvents'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 08/26/2011
-- Description:	Locks/Unlocks the Orders and Events.
-- =========================================================
ALTER PROCEDURE [dbo].[unlockTimedOutOrdersEvents]
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	DELETE FROM dbo.EVENT_REC_LOCK	WITH (ROWLOCK)
		WHERE	DateDiff(minute, STRT_REC_LOCK_TMST, getDate())	>	60
		
	DELETE FROM dbo.ORDR_REC_LOCK	WITH (ROWLOCK)
		WHERE	DateDiff(minute, STRT_REC_LOCK_TMST, getDate())	>	60

	DELETE FROM dbo.REDSGN_REC_LOCK	WITH (ROWLOCK)
		WHERE	DateDiff(minute, STRT_REC_LOCK_TMST, getDate())	>	60

	DELETE FROM dbo.CPT_REC_LOCK	WITH (ROWLOCK)
		WHERE	DateDiff(minute, STRT_REC_LOCK_TMST, getDate())	>	60

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END