USE [COWS]
GO
_CreateObject 'SP','dbo','ResetUserWFMAssignment'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Jagan Gangi
-- Create date: 06/15/2012
-- Description:	Reset entries in dbo.USER_WFM
-- ================================================================

ALTER PROCEDURE [dbo].[ResetUserWFMAssignment]
	@USERID	INT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		--IF NOT EXISTS (SELECT 'X' FROM dbo.USER_WFM WITH (NOLOCK) WHERE [USER_ID] = @userID AND [REC_STUS_ID] = 1)
		--BEGIN
		
			--UPDATE dbo.USER_WFM WITH (ROWLOCK) SET REC_STUS_ID=1 WHERE [USER_ID] = @userID
			
			DECLARE @UserListwCnt TABLE (UserID INT, Cnt INT)

			INSERT INTO @UserListwCnt
				select x.[USER_ID], SUM(x.Cnt)
				FROM (
				select uw.[USER_ID], COUNT(*) as Cnt
				from dbo.USER_WFM uw with (nolock)
				inner join dbo.LK_USER lu with (nolock) on lu.USER_ID = uw.USER_ID 
				where	uw.REC_STUS_ID	=	1
					AND	lu.REC_STUS_ID	=	1
				group by uw.[USER_ID], uw.PROD_TYPE_ID, uw.PLTFRM_CD, uw.ORGTNG_CTRY_CD, uw.ORDR_ACTN_ID) as x
				group by x.[USER_ID]
				order by x.[USER_ID] desc

		    DECLARE @CrntUserTbl TABLE (UserID INT, Cnt INT)

			INSERT INTO @CrntUserTbl
			SELECT @userID, COUNT(1)
			FROM (SELECT	distinct PROD_TYPE_ID,	PLTFRM_CD,	ORGTNG_CTRY_CD, ORDR_ACTN_ID
					FROM	dbo.USER_WFM  uw WITH (NOLOCK) 
				inner join	dbo.LK_USER lu with (nolock) on lu.USER_ID = uw.USER_ID 
					WHERE uw.[USER_ID] = @userID AND uw.REC_STUS_ID = 1 and lu.REC_STUS_ID = 1
					) as y

			 DELETE from @UserListwCnt
			 WHERE Cnt <> (SELECT Cnt FROM @CrntUserTbl)
			 
			 DELETE FROM @UserListwCnt
			 WHERE UserID in (SELECT uw.[USER_ID] 
							  FROM dbo.USER_WFM uw WITH (NOLOCK) inner join	
								   @UserListwCnt uc on uc.[UserID] = uw.[USER_ID]
							  WHERE uw.REC_STUS_ID = 1
							    and ((uw.PROD_TYPE_ID 
									not in (SELECT uw2.PROD_TYPE_ID
											FROM dbo.USER_WFM uw2 WITH (NOLOCK)
										    WHERE uw2.[USER_ID] = @userID
										      AND uw2.REC_STUS_ID = 1))
								or (uw.PLTFRM_CD 
									not in (SELECT uw2.PLTFRM_CD
											FROM dbo.USER_WFM uw2 WITH (NOLOCK)
										    WHERE uw2.[USER_ID] = @userID
											  AND uw2.REC_STUS_ID = 1))
								or (uw.ORGTNG_CTRY_CD 
									not in (SELECT uw2.ORGTNG_CTRY_CD
											FROM dbo.USER_WFM uw2 WITH (NOLOCK)
										    WHERE uw2.[USER_ID] = @userID
											  AND uw2.REC_STUS_ID = 1))
								or (uw.ORDR_ACTN_ID 
									not in (SELECT uw2.ORDR_ACTN_ID
											FROM dbo.USER_WFM uw2 WITH (NOLOCK)
										    WHERE uw2.[USER_ID] = @userID
											  AND uw2.REC_STUS_ID = 1))))

			DECLARE @OrderCnt DECIMAL(5,2)
			SET @OrderCnt = 0.00

			SELECT @OrderCnt = MIN(USER_ORDR_CNT)
			FROM dbo.LK_USER lu WITH (NOLOCK) INNER JOIN
				 @UserListwCnt ul ON ul.UserID = lu.[USER_ID] INNER JOIN
				 @CrntUserTbl cu ON ul.Cnt = cu.Cnt
			WHERE ul.UserID <> (SELECT [UserID] FROM @CrntUserTbl)
			  AND lu.USER_ORDR_CNT <> 0

			UPDATE dbo.LK_USER WITH (ROWLOCK) 
			SET USER_ORDR_CNT=CASE WHEN (COALESCE(@OrderCnt, 0) <= 0) THEN 0
									ELSE @OrderCnt END 
									WHERE [USER_ID] = @userID

		--END

	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	
END
