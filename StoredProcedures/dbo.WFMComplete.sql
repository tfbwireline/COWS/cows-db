USE [COWS]
GO
_CreateObject 'SP','dbo','WFMComplete'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO	

-- =============================================
-- Author:		David Phillips
-- Create date: 08/012/2011
-- Description:	This SP is used to remove WFM assignments.
-- =============================================
-- Alter:		Kyle Wichert
-- Modify date: 01-24-2012
-- Description: When an NCI order hits complete, it may still have a NCI task.  Clear the assignments if:
--				1.  Task 1001 exists AND
--				2.  Task 213 is not pending
-- sbg9814	02/07/2012	If TaskID = 1, then delete the WFM User assignments.
-- =============================================

ALTER  PROCEDURE [dbo].[WFMComplete]
	@OrderID INT, 
	@TaskId SMALLINT, 
	@TaskStatus TINYINT, 
	@Comments VARCHAR(1000) = NULL
AS
BEGIN
Begin Try
	
	DECLARE	@Continue	BIT
	SET		@Continue	=	0

	--Task rule called on task 1001, so check if 213 is pending
	IF @TaskId IN	(1, 1001)
	BEGIN	
		IF	@TaskId	=	1
		OR	NOT EXISTS (SELECT 1 FROM dbo.ACT_TASK WITH (NOLOCK) WHERE ORDR_ID	=	@OrderID	AND	TASK_ID	=	213		AND	STUS_ID	=	0)
		BEGIN
			SET @Continue = 1
		END
	END

	IF	@TaskId	=	213
	BEGIN
		SELECT @Continue = 1 FROM dbo.ACT_TASK WITH (NOLOCK) WHERE ORDR_ID	=	@OrderID	AND	TASK_ID	=	1001
	END

	IF @Continue = 1
	BEGIN
		DECLARE @ORDR_CAT_ID INT = 0
		SELECT @ORDR_CAT_ID = ORDR_CAT_ID
			FROM dbo.ORDR WITH (NOLOCK)
			WHERE ORDR_ID = @OrderID
			
		DECLARE @User	TABLE
			(UserID	Int,ORDR_Weightage Decimal(5,2),GRP_ID INT)
	
		INSERT INTO @User (UserID,ORDR_Weightage,GRP_ID)
		SELECT		ISNULL(ASN_USER_ID, 0), CASE WHEN ((ISNULL(u.GRP_ID,0) IN (5,6,7)) OR (u.USR_PRF_ID IN (14,28,112))) THEN ISNULL(lot.XNCI_WFM_ORDR_WEIGHTAGE_NBR,1)
												ELSE 1
											END, u.GRP_ID
			FROM		dbo.USER_WFM_ASMT	u WITH (NOLOCK)	
			INNER JOIN	dbo.ORDR odr WITH (NOLOCK)  ON u.ORDR_ID = odr.ORDR_ID
			LEFT JOIN	dbo.LK_PPRT lp WITH (NOLOCK) ON lp.PPRT_ID = odr.PPRT_ID
			LEFT JOIN	dbo.LK_ORDR_TYPE lot WITH (NOLOCK) ON lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
			WHERE	u.ORDR_ID		=	@OrderID
				AND	u.GRP_ID		=	u.GRP_ID
										
		
		UPDATE	dbo.ACT_TASK WITH (ROWLOCK)
		SET		STUS_ID = 2, MODFD_DT = GETDATE()
		WHERE	TASK_ID	IN (106, 211)
			AND	ORDR_ID	=	@OrderID

		IF @ORDR_CAT_ID = 2
			BEGIN
				IF @TaskId = 1
					BEGIN
						UPDATE			lu
							SET			lu.USER_ORDR_CNT	=	lu.USER_ORDR_CNT	-	vt.ORDR_Weightage
							FROM		dbo.LK_USER		lu	WITH (ROWLOCK)
							INNER JOIN	@User			vt	ON	lu.USER_ID	=	vt.UserID
							WHERE		vt.UserID			!=	0
								AND		lu.USER_ORDR_CNT	>	0
					END
				ELSE
					BEGIN
						UPDATE			lu
							SET			lu.USER_ORDR_CNT	=	lu.USER_ORDR_CNT	-	vt.ORDR_Weightage
							FROM		dbo.LK_USER		lu	WITH (ROWLOCK)
							INNER JOIN	@User			vt	ON	lu.USER_ID	=	vt.UserID
							WHERE		vt.UserID			!=	0
								AND		lu.USER_ORDR_CNT	>	0
								AND		vt.GRP_ID			!=	1
								
					END
			END
		--ELSE  -- Commented out March2015 because only incrementing count for FSA orders.
		--	BEGIN
		--		UPDATE			lu
		--			SET			lu.USER_ORDR_CNT	=	lu.USER_ORDR_CNT	-	vt.ORDR_Weightage
		--			FROM		dbo.LK_USER		lu	WITH (ROWLOCK)
		--			INNER JOIN	@User			vt	ON	lu.USER_ID	=	vt.UserID
		--			WHERE		vt.UserID			!=	0
		--				AND		lu.USER_ORDR_CNT	>	0
		--	END
		
	END		
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
		
End
