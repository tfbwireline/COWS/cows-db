USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getTechAssignment_V5U]    Script Date: 11/15/2016 11:55:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 11/14/2016
-- Description:	Gets Tech assigned to 3rd Party order.
-- =========================================================
ALTER PROCEDURE [dbo].[getTechAssignment_V5U]
			@ORDR_ID int        	
        
AS
BEGIN
SET NOCOUNT ON;


BEGIN TRY

	SELECT  tec.USER_CPE_TECH_NME, uwa.DSPTCH_TM, uwa.EVENT_ID, 
			uwa.INRTE_TM, uwa.ONSITE_TM, uwa.CMPLT_TM, usr.PHN_NBR, usr.EMAIL_ADR
	 FROM  dbo.USER_WFM_ASMT uwa WITH (NOLOCK)
		INNER JOIN dbo.LK_USER usr WITH (NOLOCK) ON usr.USER_ID = uwa.ASN_USER_ID
		INNER JOIN dbo.USER_CPE_TECH tec WITH (NOLOCK) ON usr.USER_ADID = tec.USER_CPE_TECH_ADID
	 WHERE ((uwa.GRP_ID = 15) OR (uwa.USR_PRF_ID=56)) AND uwa.ORDR_ID = @ORDR_ID	
	

END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH
END

