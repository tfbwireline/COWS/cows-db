USE [COWS]
GO
_CreateObject 'SP','dbo','WfmGOMTaskRule'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		David Phillips
-- Create date: 08/012/2011
-- Description:	This SP is used to assign GOM users to orders
-- Updated By:	Md M MOnir / Vn370313
-- Create date: 08/012/2011
-- Description:	Lk_user table order count  added mach5  submit Orders
-- =============================================

ALTER  PROCEDURE [dbo].[WfmGOMTaskRule]
	@OrderID INT, 
	@TaskId SMALLINT, 
	@TaskStatus TINYINT, 
	@Comments VARCHAR(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET DEADLOCK_PRIORITY 3

	DECLARE @Ordr_cat		SMALLINT  -- 1 = PLS, 2 = FSA, 3 = Vendor 4 = NCCO
    DECLARE @Ctr			INT	-- counter
	DECLARE @Prod_type_id	TINYINT
	DECLARE @Pltfrm_cd		VARCHAR(2)
	DECLARE @Ordr_actn_id   TINYINT
	DECLARE @User_id		INT = 0
	DECLARE @RelativeOrder  INT = 0
	DECLARE @UserName       VARCHAR(100)
	DECLARE @Note           VARCHAR(1000)
	DECLARE @DMSTC_CD		BIT

	SET @Ctr = 0

	
	BEGIN TRY

		SELECT @DMSTC_CD = DMSTC_CD,
		@Ordr_cat = ORDR_CAT_ID
		FROM dbo.ORDR WITH (NOLOCK)
		WHERE ORDR_ID = @OrderID

		IF ((@DMSTC_CD=1) OR (@Ordr_cat IN (1,5))) --Do GOM assignment only for Intl' orders or IPL/DPL
		BEGIN

		--Check to see if order has already been assigned to someone in the GOM group.
	
		SELECT @Ctr = COUNT(ufa.ORDR_ID)
			FROM dbo.USER_WFM_ASMT ufa  WITH (NOLOCK)
			WHERE ufa.ORDR_ID = @OrderID
				AND ((ufa.GRP_ID = 1) OR (ufa.USR_PRF_ID = 126)) -- 1 = GOM group

		--Validation & Assignment for move orders and Regular Submit orders
		IF @Ctr = 0 
			BEGIN
				--This would be Handled in dbo.CompleteFSAInitialTask SP for TaskID = 100
				IF @TaskID = 100
					BEGIN
						--Submit orders with existing presubmits should not be re-assigned. We copy information from presubmits
						-- and this logic is implemented in dbo.CompleteFSAInitialTask SP
						SELECT @Ctr = Count(1)
							FROM	dbo.FSA_ORDR fsa WITH (NOLOCK)
						Inner Join 	dbo.FSA_ORDR fp WITH (NOLOCK) ON fsa.FTN = fp.FTN
							WHERE	fsa.ORDR_ACTN_ID	=	2
								AND	fp.ORDR_ACTN_ID		=	1
								AND	fsa.ORDR_ID			=	@OrderID
								AND	EXISTS
								(
									SELECT 'X'
										FROM dbo.USER_WFM_ASMT uwa WITH (NOLOCK)
										WHERE uwa.ORDR_ID = fp.ORDR_ID
								)
						
						--Submit orders on Moves with existing presubmit orders in system should not be re-assigned.
						-- We copy information from presubmits and this logic is implemented in dbo.CompleteFSAInitialTask SP		
						IF @Ctr = 0
							BEGIN
								SELECT @Ctr = Count(1)
								FROM	dbo.FSA_ORDR fsa WITH (NOLOCK)
							Inner Join 	dbo.FSA_ORDR fp WITH (NOLOCK) ON fsa.PRNT_FTN = fp.FTN
								WHERE	fsa.ORDR_ACTN_ID	=	2
									AND	fp.ORDR_ACTN_ID		=	1
									AND	fsa.ORDR_ID			=	@OrderID
									AND	EXISTS
									(
										SELECT 'X'
											FROM dbo.USER_WFM_ASMT uwa WITH (NOLOCK)
											WHERE uwa.ORDR_ID = fp.ORDR_ID
									)
							END
							
								
						IF @Ctr = 0
							BEGIN
								DECLARE @RelOrderID INT = 0 
								
								SELECT @RelOrderID = ISNULL(fsar.ORDR_ID,0)
								FROM	dbo.FSA_ORDR fsa WITH (NOLOCK)
							INNER JOIN	dbo.FSA_ORDR fsar WITH (NOLOCK) ON fsa.FTN = fsar.RELTD_FTN
								WHERE	fsa.ORDR_ACTN_ID	=	2
									AND fsar.ORDR_ACTN_ID 	=	2
									AND	fsa.ORDR_ID			=	@OrderID
									AND	EXISTS
										(
											SELECT 'X'
												FROM	dbo.USER_WFM_ASMT uwa WITH (NOLOCK)
												WHERE	uwa.ORDR_ID = fsar.ORDR_ID
													AND ((uwa.GRP_ID = 1) OR (uwa.USR_PRF_ID = 126)) -- 1 = GOM group
										)
										
								IF @RelOrderID != 0	
									BEGIN
										SET @Ctr = 1 
										INSERT INTO dbo.USER_WFM_ASMT	
											(ORDR_ID
											,GRP_ID
											,ASN_USER_ID
											,ASN_BY_USER_ID
											,ASMT_DT
											,CREAT_DT,USR_PRF_ID)
										SELECT	DISTINCT	@OrderID
											,GRP_ID
											,ASN_USER_ID
											,ASN_BY_USER_ID
											,GETDATE()
											,ASMT_DT,126
										FROM	dbo.USER_WFM_ASMT WITH (NOLOCK)
										WHERE	ORDR_ID = @RelOrderID
											AND ((GRP_ID = 1) OR (USR_PRF_ID = 126)) -- 1 = GOM group
									END
							END
						
						
					END
			END
	
		IF @Ctr = 0
			Begin
	
				SELECT @Ordr_cat = ord.ORDR_CAT_ID		   
					FROM	dbo.ORDR ord	WITH (NOLOCK)			
					WHERE	ord.ORDR_ID		=	@OrderID
						AND ord.DMSTC_CD = 1
							
			
				IF @Ordr_cat in (2,6) -- FSA Order
					Begin
						Select @Pltfrm_cd	= IsNull(ord.PLTFRM_CD,0)
							   ,@Prod_type_id	= pt.PROD_TYPE_ID
							  , @Ordr_actn_id  = fo.ORDR_ACTN_ID
							  , @RelativeOrder = IsNull(fr.ORDR_ID,0)			
								
						FROM dbo.FSA_ORDR fo	 WITH (NOLOCK)
						JOIN dbo.ORDR ord		 WITH (NOLOCK)
							ON fo.ORDR_ID = ord.ORDR_ID
						JOIN dbo.LK_PROD_TYPE pt WITH (NOLOCK)
							ON pt.FSA_PROD_TYPE_CD = fo.PROD_TYPE_CD
						LEFT OUTER JOIN dbo.FSA_ORDR fr WITH (NOLOCK) on fr.FTN = fo.RELTD_FTN	
						WHERE @OrderID = fo.ORDR_ID
					End					
				ELSE 
				IF (@Ordr_cat IN (1,5)) -- IPL Order
					Begin
						Select @Prod_type_id	= ip.PROD_TYPE_ID
							   ,@Pltfrm_cd	 	= IsNull(ord.PLTFRM_CD,0)
						FROM dbo.IPL_ORDR ip	WITH (NOLOCK)
						JOIN dbo.ORDR ord       WITH (NOLOCK)
							ON ip.ORDR_ID = ord.ORDR_ID
						WHERE @OrderID = ip.ORDR_ID 
						
						Set @Ordr_actn_id  = 2
						
					End
				
				If @RelativeOrder > 0
				Begin

					SELECT TOP 1 @User_id	= uwfm.ASN_USER_ID
						FROM dbo.USER_WFM_ASMT uwfm	WITH (NOLOCK) INNER JOIN
							 dbo.LK_USER lu WITH (NOLOCK) ON uwfm.ASN_USER_ID = lu.[USER_ID]
						WHERE ((uwfm.GRP_ID = 1) OR (uwfm.USR_PRF_ID = 126)) -- 1 = GOM group
				    		AND uwfm.ORDR_ID	= @RelativeOrder
							AND lu.REC_STUS_ID		= 1
				End

				IF @Ordr_cat = 4 --For NCCO orders, assign user who created the order if s/he has WFM profile to work NCCO orders.
					BEGIN
						SELECT @User_id = CREAT_BY_USER_ID
							FROM	dbo.NCCO_ORDR ncco WITH (NOLOCK)
							WHERE	ORDR_ID = @OrderID
						
						IF NOT EXISTS
							(	
								SELECT 'X'
									FROM	 dbo.USER_WFM uwfm	WITH (NOLOCK)
										JOIN dbo.LK_USER lu	WITH (NOLOCK) ON lu.USER_ID = uwfm.USER_ID
									WHERE	uwfm.USER_ID	=	@User_id		
										AND ((uwfm.GRP_ID = 1) OR (uwfm.USR_PRF_ID = 126)) -- 1 = GOM group
		    							AND uwfm.PROD_TYPE_ID	IN (SELECT PROD_TYPE_ID FROM dbo.LK_PROD_TYPE WHERE ORDR_CAT_ID = 4)
										AND uwfm.REC_STUS_ID	= 1
										AND lu.REC_STUS_ID		= 1
							)
							BEGIN
								SELECT TOP 1 @User_id	= uwfm.USER_ID
									FROM dbo.USER_WFM uwfm	WITH (NOLOCK)
										JOIN dbo.LK_USER lu	WITH (NOLOCK)
										ON lu.USER_ID = uwfm.USER_ID
									WHERE ((uwfm.GRP_ID = 1) OR (uwfm.USR_PRF_ID = 126)) -- 1 = GOM group
				    					AND uwfm.PROD_TYPE_ID	IN (SELECT PROD_TYPE_ID FROM dbo.LK_PROD_TYPE WHERE ORDR_CAT_ID = 4)
										AND uwfm.REC_STUS_ID	= 1
										AND lu.REC_STUS_ID		= 1
									ORDER BY lu.USER_ORDR_CNT	
							END
					END

				IF @User_id = 0
					Begin
						SELECT TOP 1 @User_id	= uwfm.USER_ID
							FROM dbo.USER_WFM uwfm	WITH (NOLOCK)
								JOIN dbo.LK_USER lu	WITH (NOLOCK)
								ON lu.USER_ID = uwfm.USER_ID
							WHERE ((uwfm.GRP_ID = 1) OR (uwfm.USR_PRF_ID = 126)) -- 1 = GOM group
				    			AND uwfm.PROD_TYPE_ID	= @Prod_type_id
								AND IsNull(uwfm.PLTFRM_CD,0) = @Pltfrm_cd
								AND uwfm.ORDR_ACTN_ID	= @Ordr_actn_id
								AND uwfm.REC_STUS_ID	= 1
								AND lu.REC_STUS_ID		= 1
							ORDER BY lu.USER_ORDR_CNT	
					END
	

				IF @User_id <> 0
					Begin	
						Select @UserName = FULL_NME from dbo.LK_USER WITH (NOLOCK) where USER_ID = @User_id

						Select @Note = 'Order auto-assigned to GOM User ' + @UserName

						INSERT INTO dbo.ORDR_NTE	(NTE_TYPE_ID,	ORDR_ID,	CREAT_DT,	CREAT_BY_USER_ID,	REC_STUS_ID,	NTE_TXT)
						VALUES						(10,			@OrderID,	GETDATE(),	1,					1,				@Note)

						INSERT INTO dbo.USER_WFM_ASMT
							(ORDR_ID,GRP_ID,ASN_USER_ID,ASN_BY_USER_ID,ASMT_DT,CREAT_DT,USR_PRF_ID)
						 VALUES
							(@OrderID,1,@User_id,1,GETDATE(),GETDATE(),126)
					
						-- March2015  --IF ((@Ordr_cat != 2) OR (@Ordr_cat = 2 AND @Ordr_actn_id = 1))
						IF  (@Ordr_cat = 2 AND @Ordr_actn_id = 1)	
							BEGIN 
								UPDATE dbo.LK_USER WITH (ROWLOCK)
									SET USER_ORDR_CNT = ISNULL(USER_ORDR_CNT,0) + 1
									WHERE USER_ID = @User_id	
							END
						/* User: vn370313  Added  Mach5  Submit Order as well  03222017 */
						IF  (@Ordr_cat = 6 AND @Ordr_actn_id = 2)	
							BEGIN 
								UPDATE dbo.LK_USER WITH (ROWLOCK)
									SET USER_ORDR_CNT = ISNULL(USER_ORDR_CNT,0) + 1
									WHERE USER_ID = @User_id	
							END
					 End
					 Else
					 Begin
						INSERT INTO dbo.ORDR_NTE	(NTE_TYPE_ID,	ORDR_ID,	CREAT_DT,	CREAT_BY_USER_ID,	REC_STUS_ID,	NTE_TXT)
						VALUES						(10,			@OrderID,	GETDATE(),	1,					1,				'bypassing GOM as no GOM workgroup involvement.')
					 End
			End
		END			
	END TRY

	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	SET NOCOUNT OFF
END