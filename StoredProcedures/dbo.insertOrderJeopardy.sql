USE [COWS]
GO
_CreateObject 'SP','dbo','insertOrderJeopardy'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================  
-- Author:		kaw4664  
-- Create date: 01/26/2012  
-- Description: Inserts jeopardy reason into the ORDR_JPRDY table and notes into ORDR_NTE table.  
-- =========================================================  
ALTER PROCEDURE [dbo].[insertOrderJeopardy]  
	@ORDR_ID		Int,
	@JPRDY_CD		Varchar(3),
	@Notes			Varchar(500),
	@NoteTypeID		Tinyint,
	@Created_By		Int  
AS  
BEGIN  
	SET NOCOUNT ON;  
	BEGIN TRY
	
		DECLARE @NoteID INT   
		SET @NoteID=0  
	   
		INSERT INTO dbo.ORDR_NTE	(NTE_TYPE_ID,	ORDR_ID,	CREAT_DT,	CREAT_BY_USER_ID,	REC_STUS_ID,	NTE_TXT	)  
		VALUES						(@NoteTypeID,	@ORDR_ID,	GETDATE(),	@Created_By,		1,				@Notes	)  
	       
		SET @NoteID = SCOPE_IDENTITY()   
	   
		IF ISNULL(@NoteID, 0) <> 0 
		BEGIN  
			INSERT INTO	dbo.ORDR_JPRDY	(ORDR_ID,	JPRDY_CD,	NTE_ID,		CREAT_DT	)  
			VALUES						(@ORDR_ID,	@JPRDY_CD,	@NoteID,	GETDATE()	)  
		END  
		
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH      
END