USE [COWS]
GO
_CreateObject 'SP'
	,'dbo'
	,'updateSSTATRequest'
GO
/****** Object:  StoredProcedure [dbo].[updateSSTATRequest]    Script Date: 11/11/2015 08:53:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 08/02/2011
-- Description:	Update the status of a given SSTAT Request
-- km967761 - 08/23/2021 - added condition for SSTAT CPE Dispatch Emails
-- km967761 - 09/27/2021 - revert condition for SSTAT CPE Dispatch Emails
-- =========================================================
ALTER PROCEDURE [dbo].[updateSSTATRequest]
						@TRAN_ID	Int
					   ,@STUS_ID	Int
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	
	UPDATE		dbo.SSTAT_REQ	WITH (ROWLOCK)
		SET		STUS_ID		=	@STUS_ID
				,STUS_MOD_DT =	GETDATE()
	WHERE	TRAN_ID		=	@TRAN_ID
	    	AND	STUS_ID		!=	21

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
