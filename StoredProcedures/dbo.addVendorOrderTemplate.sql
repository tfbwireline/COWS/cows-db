﻿USE [COWS]
GO
_CreateObject 'SP','dbo','addVendorOrderTemplate'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 12th July 2011
-- Description:	Creates Vendor Order Form from Vendor Template
-- ================================================================

ALTER PROCEDURE [dbo].[addVendorOrderTemplate]
	@VendorOrderID INT,
	@VendorTemplateIDs VARCHAR(1000)	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	INSERT INTO [dbo].[VNDR_ORDR_FORM]
			   ([VNDR_ORDR_ID]
			   ,[FILE_NME]
			   ,[FILE_CNTNT]
			   ,[FILE_SIZE_QTY]
			   ,[TMPLT_CD]
			   ,[CREAT_DT]
			   ,[CREAT_BY_USER_ID]
			   ,[REC_STUS_ID])     
	SELECT 
			@VendorOrderID      
		  ,[FILE_NME]
		  ,[FILE_CNTNT]
		  ,[FILE_SIZE_QTY]
		  ,1
		  ,[CREAT_DT]
		  ,[CREAT_BY_USER_ID]
		  ,[REC_STUS_ID]
	  FROM [dbo].[VNDR_TMPLT] WITH (NOLOCK)
	  WHERE VNDR_TMPLT_ID IN (SELECT IntegerID FROM web.ParseCommaSeparatedIntegers(@VendorTemplateIDs,','))

	  RETURN SCOPE_IDENTITY()
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
