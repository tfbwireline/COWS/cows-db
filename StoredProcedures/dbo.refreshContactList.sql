USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[RefreshContactList]    Script Date: 1/18/2022 2:36:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  Jbolano15            
-- Create date: 12/02/2021 
-- Description: Refresh Contact List based from ODIE/M5; To be called via Job every 30mins
-- jbolano15 - 12/02/2021 - Add ODIE_CUST_ID column
-- jbolano15 - 12/15/2021 - Alter ODIE_CUST_ID column to NULLABLE
-- =============================================            
Create PROCEDURE [dbo].[RefreshContactList] -- exec [dbo].[RefreshContactList]
AS
BEGIN
	BEGIN TRY
		IF OBJECT_ID(N'tempdb..#NewContactList', N'U') IS NOT NULL         
			DROP TABLE #NewContactList

		CREATE TABLE #NewContactList (
			ROW_NO INT identity(1,1) NOT NULL
			,OBJ_ID INT NOT NULL
			,OBJ_TYP_CD VARCHAR(1) NOT NULL
			,HIER_ID VARCHAR(9) NOT NULL
			,HIER_LVL_CD VARCHAR(2) NOT NULL
			,ODIE_CUST_ID VARCHAR(10) NULL)

		INSERT INTO #NewContactList (OBJ_ID, OBJ_TYP_CD, HIER_ID, HIER_LVL_CD, ODIE_CUST_ID) 
		SELECT Distinct
				a.OBJ_ID, a.OBJ_TYP_CD, a.HIER_ID, a.HIER_LVL_CD, b.ODIE_CUST_ID
			FROM CNTCT_DETL a WITH (NOLOCK)
				INNER JOIN REDSGN b WITH (NOLOCK) ON b.REDSGN_ID = a.OBJ_ID
			WHERE
				a.REC_STUS_ID = 1
				AND a.HIER_LVL_CD = 'H1'
				AND a.OBJ_TYP_CD = 'R'
				AND a.CREAT_BY_USER_ID = 1
				AND b.STUS_ID NOT IN (226,227,228,229)
			UNION All
			SELECT distinct
				a.OBJ_ID, a.OBJ_TYP_CD, a.HIER_ID, a.HIER_LVL_CD, b.ODIE_CUST_ID
			FROM CNTCT_DETL a WITH (NOLOCK)
				INNER JOIN MDS_EVENT b WITH (NOLOCK) ON b.EVENT_ID = a.OBJ_ID
			WHERE
				a.REC_STUS_ID = 1
				AND a.HIER_LVL_CD = 'H1'
				AND a.OBJ_TYP_CD = 'E'
				AND a.CREAT_BY_USER_ID = 1
				AND b.EVENT_STUS_ID NOT IN (6,8,13)
			UNION All
			SELECT distinct
				a.OBJ_ID, a.OBJ_TYP_CD, a.HIER_ID, a.HIER_LVL_CD, b.CUST_SHRT_NME AS ODIE_CUST_ID
			FROM CNTCT_DETL a WITH (NOLOCK)
				INNER JOIN CPT b WITH (NOLOCK) ON b.CPT_ID = a.OBJ_ID
			WHERE
				a.REC_STUS_ID = 1
				AND a.HIER_LVL_CD = 'H1'
				AND a.OBJ_TYP_CD = 'C'
				AND a.CREAT_BY_USER_ID = 1
				AND b.CPT_STUS_ID NOT IN (308,309,311)

		CREATE NONCLUSTERED INDEX ix_NewContactList_Temp ON #NewContactList ([OBJ_ID]);
		
		DECLARE @ITERATOR INT = 1
		DECLARE @ROW_COUNT BIGINT = 0

		-- get a count of total rows to process 
		SELECT @ROW_COUNT = COUNT(1) FROM #NewContactList

		WHILE @ITERATOR <= @ROW_COUNT
		BEGIN
			DECLARE @OBJ_ID INT
			DECLARE @OBJ_TYP_CD VARCHAR(1)
			DECLARE @HIER_ID VARCHAR(9)
			DECLARE @HIER_LVL_CD VARCHAR(2)
			DECLARE @ODIE_CUST_ID VARCHAR(10)

			SELECT
				@OBJ_ID = OBJ_ID,
				@OBJ_TYP_CD = OBJ_TYP_CD,
				@HIER_ID = HIER_ID,
				@HIER_LVL_CD = HIER_LVL_CD,
				@ODIE_CUST_ID = ODIE_CUST_ID
				--*
			FROM #NewContactList
			WHERE
				ROW_NO = @ITERATOR

			EXEC web.getContactDetails @OBJ_ID, @HIER_ID, @HIER_LVL_CD, @OBJ_TYP_CD, @ODIE_CUST_ID, 1, 1
    
		   SET @ITERATOR = @ITERATOR + 1 
		   
		END
	END TRY

	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END