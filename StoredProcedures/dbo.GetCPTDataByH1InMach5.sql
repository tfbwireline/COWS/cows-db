USE [COWS]
GO
_CreateObject 'SP','dbo','GetCPTDataByH1InMach5'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================
-- Author:	Sarah Sandoval
-- Create date: 02/15/2016
-- Description:	To Get H1 Address Info from mac5d101 for CPT Data
-- ===============================================================
ALTER PROCEDURE [dbo].[GetCPTDataByH1InMach5]
	@H1	VARCHAR(9) = ''
AS
BEGIN

BEGIN TRY

DECLARE @SQL nVarchar(max)
	SET @SQL = ''
	SET @SQL = 'SELECT *
					FROM OPENQUERY (mac5d101,		
				''SELECT *  
					FROM v_v5u_cust_acct '
	IF @H1 <> ''
		SET @SQL = @SQL + ' WHERE CIS_CUST_ID = ' + @H1

	SET @SQL = @SQL + '	'') '

	EXEC sp_executesql @SQL

	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
END
