USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[AccReqCmplt]    Script Date: 05/21/2019 9:42:34 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By:		David Phillips
-- Create date: 02/07/2017
-- Description:	This SP is used to complete tasks not required for Access Request Changes.
-- =============================================
ALTER PROCEDURE [dbo].[AccReqCmplt]
@OrderID INT, 
@TaskId SMALLINT, 
@TaskStatus TINYINT, 
@Comments VARCHAR(1000) = NULL

AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRY
	
	IF EXISTS (SELECT * FROM FSA_ORDR WITH (NOLOCK) 
		WHERE ORDR_ID = @OrderID AND ORDR_SUB_TYPE_CD in ( 'AR','AN'))
			BEGIN
				EXEC [dbo].[CompleteActiveTask] @OrderID,@TaskId,2,'Systematically Completed task not required.',1,1
			END	
	END TRY
	BEGIN CATCH
		exec [dbo].[insertErrorInfo]
	END CATCH
END

