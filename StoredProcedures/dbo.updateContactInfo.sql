USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[updateContactInfo]    Script Date: 10/30/2013 13:45:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <08/22/2011>
-- Description:	<To Insert or Update Customer and account team contact information>
-- =============================================
-- Modified by: <David Phillips>
-- Mod Date: <10/28/2013>
-- Description: <Changed email encription to use dbo.ecryptString because the field was expanded to 40 bytes from 30.>
-- =============================================
ALTER PROCEDURE [dbo].[updateContactInfo]
@ORDR_CNTCT_ID		Int,
@ORDR_ID			Int,
@CNTCT_TYPE_ID		TinyInt,
@CREAT_BY_USER_ID	Int,
@REC_STUS_ID		TinyInt,
@ROLE_ID			TinyInt = NULL, 
@FRST_NME			Varchar(100),
@LST_NME			Varchar(100),
@EMAIL_ADR			Varchar(100),
@PHN_NBR			Varchar(20)
AS
BEGIN
	BEGIN TRY
		OPEN SYMMETRIC KEY FS@K3y 
			DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
			
		IF ISNULL(@ORDR_CNTCT_ID,0) = 0 OR ISNULL(@ORDR_CNTCT_ID,0) = -1
			BEGIN
				INSERT INTO dbo.ORDR_CNTCT (ORDR_ID, CNTCT_TYPE_ID, CREAT_BY_USER_ID, REC_STUS_ID, ROLE_ID, FRST_NME, LST_NME, EMAIL_ADR,PHN_NBR)
				VALUES (@ORDR_ID, @CNTCT_TYPE_ID, @CREAT_BY_USER_ID, @REC_STUS_ID, @ROLE_ID 
												,EncryptByKey(Key_GUID('FS@K3y'), ISNULL(@FRST_NME,''))
												,EncryptByKey(Key_GUID('FS@K3y'), ISNULL(@LST_NME,'')) 
												,dbo.encryptString(ISNULL(@EMAIL_ADR,''))
												,@PHN_NBR
						)
						
				SET @ORDR_CNTCT_ID = SCOPE_IDENTITY()
			END	
		ELSE
			BEGIN
				UPDATE	dbo.ORDR_CNTCT	WITH (ROWLOCK)
					SET	CNTCT_TYPE_ID		=	@CNTCT_TYPE_ID,
						CREAT_BY_USER_ID	=	@CREAT_BY_USER_ID,
						ROLE_ID				=	@ROLE_ID,
						FRST_NME			=	EncryptByKey(Key_GUID('FS@K3y'), ISNULL(@FRST_NME,'')),
						LST_NME				=	EncryptByKey(Key_GUID('FS@K3y'), ISNULL(@LST_NME,'')) ,
						EMAIL_ADR			=	dbo.encryptString(ISNULL(@EMAIL_ADR,'')),
						PHN_NBR				=	@PHN_NBR		
					WHERE
						ORDR_CNTCT_ID		=	@ORDR_CNTCT_ID
			END
			
		SELECT @ORDR_CNTCT_ID
		
	END TRY
	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
END
