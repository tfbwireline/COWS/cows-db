USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getReqNbr_V5U]    Script Date: 07/01/2019 10:44:32 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 07/01/2019
-- Description:	Gets the Shipping (PS Header) Notes.
-- =========================================================
CREATE PROCEDURE [dbo].[getPeopleSoftHdrNte] --12790
  @ORDR_ID int 
           
AS
BEGIN
SET NOCOUNT ON;

Declare @NTE varchar (350) = null

BEGIN TRY

	IF EXISTS
			(
				SELECT 'X'
					FROM	dbo.ORDR_NTE WITH (NOLOCK)
					WHERE	ORDR_ID =	@ORDR_ID AND NTE_TYPE_ID = 27
					
			)
			BEGIN
				SELECT TOP 1 @NTE =  [NTE_TXT]
					FROM dbo.ORDR_NTE with (nolock) 
						WHERE ORDR_ID = @ORDR_ID AND NTE_TYPE_ID = 27 
						ORDER BY CREAT_DT DESC
									
			END
		

	SELECT @NTE as [Note]

END TRY

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch

END

