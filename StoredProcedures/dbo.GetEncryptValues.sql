USE [COWS]
GO
_CreateObject 'SP','dbo','GetEncryptValues'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Jagan Gangi
-- Create date: 7/6/12
-- Get a table of encrypted values using a delimited string
 -- ======================================================================

ALTER PROCEDURE [dbo].[GetEncryptValues]  (@s VARCHAR(MAX))
AS
BEGIN
BEGIN TRY
    
		OPEN SYMMETRIC KEY FS@K3y 
			DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
			
		IF (PATINDEX('%&amp;%', @s) <= 0)
		SET @s = REPLACE(@s, '&', '&amp;')

        DECLARE @xml XML
        SET @XML = N'<root><r>' + REPLACE(@s, '^', '</r><r>') + '</r></root>'

        SELECT EncryptByKey(Key_GUID('FS@K3y'), r.value('.','VARCHAR(max)')) as Item
        FROM @xml.nodes('//root/r') AS RECORDS(r)

   
END TRY
BEGIN CATCH
EXEC dbo.insertErrorInfo
END CATCH
END