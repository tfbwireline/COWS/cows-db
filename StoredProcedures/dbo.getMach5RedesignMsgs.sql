USE [COWS]
GO
_CreateObject 'SP','dbo','getMach5RedesignMsgs'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		jrg7298
-- Create date: 10/27/2016
-- Description:	Get all Redesign charges that needs to be sent to MACH5
-- =========================================================
ALTER PROCEDURE [dbo].[getMach5RedesignMsgs]
AS
BEGIN
SET NOCOUNT ON;
Begin Try
		
	SELECT DISTINCT mrm.M5_REDSGN_MSG_ID as TRAN_ID,
					rd.REDSGN_NBR,
					CASE WHEN (ue.EVENT_ID IS NOT NULL) THEN 'Special Request Changes' ELSE lrt.REDSGN_TYPE_NME END AS REDSGN_TYPE,
					rdi.H6_CUST_ID AS H6,
					rdi.DEV_BILL_DT AS ACTV_DT,
					mrm.CHARGE_AMT AS CHARGE,
					mrm.[SITE],
					CASE WHEN (ue.EVENT_ID IS NOT NULL) THEN ISNULL((SELECT TOP 1 ISNULL(PRMTR_VALU_TXT,'200011847') FROM dbo.LK_SYS_CFG WITH (NOLOCK) WHERE PRMTR_NME='M5SDVBEC'), '200011847') 
					ELSE ISNULL((SELECT TOP 1 ISNULL(PRMTR_VALU_TXT,'100033596') FROM dbo.LK_SYS_CFG WITH (NOLOCK) WHERE PRMTR_NME='M5RedesignBEC'), '100033596') END AS BEC
	FROM dbo.M5_REDSGN_MSG mrm WITH (NOLOCK) INNER JOIN
		 dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK) ON rdi.REDSGN_DEV_ID = mrm.REDSGN_DEV_ID INNER JOIN
		 dbo.REDSGN rd WITH (NOLOCK) ON rd.REDSGN_ID = rdi.REDSGN_ID INNER JOIN
		 dbo.LK_REDSGN_TYPE lrt WITH (NOLOCK) ON lrt.REDSGN_TYPE_ID = rd.REDSGN_TYPE_ID	LEFT JOIN
		 dbo.UCaaS_EVENT ue WITH (NOLOCK) ON ue.EVENT_ID = mrm.EVENT_ID AND ue.UCaaS_PROD_TYPE_ID=6
	WHERE mrm.SENT_DT IS NULL
	  AND mrm.STUS_ID=10


End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

