USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[processBillActivation]    Script Date: 12/01/2020 9:03:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 12/1/2020
-- Description:	Updates Bill Clear date on the pending M5_Bill_Clear staging table to send complete
--             messages back to M5 on Access Refresh (AR) and Access Renewal (AN) orders.  On AR/AN orders
--             the message is sent on Bill Clear date entered by the users.
-- =========================================================
ALTER PROCEDURE [dbo].[updateBillClearDt] 
	@ORDR_ID		Int,
	@DATE			smalldatetime 
AS
BEGIN
SET NOCOUNT ON;
Begin Try
			DECLARE @oldDT smalldatetime,
					@NTE_TXT VARCHAR(MAX)

			IF EXISTS (SELECT 'x' FROM dbo.M5_BILL_CLEAR WITH (NOLOCK) 
							where ORDR_ID = @ORDR_ID AND STUS_ID = 1 AND ISNULL(SENT_DT,'') = '')
				BEGIN	 
						SELECT @oldDT = BILL_CLEAR_DT FROM dbo.M5_BILL_CLEAR WITH (NOLOCK) 
											WHERE ORDR_ID = @ORDR_ID AND STUS_ID = 1

						IF @DATE > getdate()
							SET @NTE_TXT = 'Bill Clear Date updated from ' + convert(varchar(12),@oldDT) + ' to ' + convert(varchar(12),@DATE) + '. Complete message will be sent to M5 on '+ convert(varchar(12),@DATE) + '.'
						ELSE
							SET @NTE_TXT = 'Bill Clear Date updated from ' + convert(varchar(12),@oldDT) + ' to ' + convert(varchar(12),@DATE) + '. Complete message will be sent to M5 today.'
				
						INSERT INTO dbo.ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
							VALUES (9,@ORDR_ID,GETDATE(),1,1,@NTE_TXT)

						UPDATE dbo.M5_BILL_CLEAR
							set BILL_CLEAR_DT = @DATE
							where ORDR_ID = @ORDR_ID					
				END


			ELSE IF EXISTS (SELECT 'x' FROM dbo.M5_BILL_CLEAR WITH (NOLOCK) 
							where ORDR_ID = @ORDR_ID AND STUS_ID = 0 )

				BEGIN
					
						IF EXISTS (SELECT 'x' from dbo.M5_ORDR_MSG WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID)
							BEGIN
								Declare @m5Dt smalldatetime,
										@origDt smalldatetime

								SELECT @m5Dt = SENT_DT FROM dbo.M5_ORDR_MSG WITH (NOLOCK)
														WHERE ORDR_ID = @ORDR_ID

								SELECT @oldDT = SENT_DT, @origDt = BILL_CLEAR_DT FROM dbo.M5_BILL_CLEAR WITH (NOLOCK)
												WHERE ORDR_ID = @ORDR_ID

								SET @NTE_TXT = 'Bill Clear Date updated from ' + convert(varchar(12),@oldDT) + ' to ' 
												+ convert(varchar(12),@DATE) + '. Complete message has already been sent to M5 on '+
										convert(varchar(12),@m5Dt) + ' per user entering the Bill Clear date of ' + convert(varchar(12),@origDt)
											 + ' which was met to trigger message to M5.'
				
								INSERT INTO dbo.ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
									VALUES (9,@ORDR_ID,GETDATE(),1,1,@NTE_TXT)
							END
				END
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
