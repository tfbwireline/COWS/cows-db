USE [COWS]
GO
_CreateObject 'SP','sis','GetADAMData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Jagannath Gangi
-- Create date: 2011-4-14
-- Description:	This SP is used to get ADAM data
-- =========================================================
-- =========================================================
-- Modified:	Naidu Vattigunta
-- Create date: 08/29/2011
-- Description:	 We should look in LK_USER table insted of CSG Level
-- =========================================================


ALTER PROCEDURE [sis].[GetADAMData]
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		DECLARE @Cnt SMALLINT
		DECLARE @Ctr SMALLINT
		DECLARE @UserTbl TABLE (Ident SMALLINT IDENTITY(1,1), UserADID dbo.ADID, UserID INT)
		DECLARE @UserADID dbo.ADID
		DECLARE @UserID INT
		
		SET		@Cnt			= 0
		SET		@Ctr			= 1
		
		INSERT INTO @UserTbl (UserADID, UserID)
		(SELECT DISTINCT   USER_ADID, [USER_ID] 
		 FROM    dbo.LK_USER 	WITH (NOLOCK) WHERE ISNULL(USER_ADID, '') <> '' AND USER_ADID NOT IN ('System', '$Premise', 'NTEPool', 'nte_pool', '$b5psa','INT-OPS1','INT-OPS2','INT-OPS3') )
		
		
		SELECT 		@Cnt	=	Count(1) FROM @UserTbl

		IF @Cnt	>	0
			BEGIN
				WHILE	@Ctr <= @Cnt
					BEGIN
					SELECT	@UserADID	=	UserADID,
							@UserID		=	UserID
					FROM	@UserTbl
					WHERE	Ident = @Ctr
					
					EXEC [dbo].[ValidateUserCSG] @UserADID, @UserID
							
					SET @Ctr = @Ctr + 1
					END
			END

	END TRY
	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
END
