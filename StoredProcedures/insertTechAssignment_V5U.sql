USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertODIEReq_V5U]    Script Date: 11/14/2016 09:06:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 11/14/2016
-- Description:	Assignes the 3rd Party Tech to the order.
-- =========================================================
CREATE PROCEDURE [dbo].[insertTechAssignment_V5U]
			@ORDR_ID int        
			,@TECH varchar(100)
			,@DSPTCH_TM  datetime
			,@EVENT_ID  int
			,@ASN_BY_USER_ADID  VARCHAR(10)
        
AS
BEGIN
SET NOCOUNT ON;


DECLARE	@Tech_USER_ID varchar(10), @ASN_BY_USER_ID int

BEGIN TRY

	SELECT @Tech_USER_ID = USER_ID FROM LK_USER WITH (NOLOCK)
			WHERE FULL_NME = @TECH
	
	SELECT @ASN_BY_USER_ID = USER_ID FROM LK_USER WITH (NOLOCK)
			WHERE USER_ADID = @ASN_BY_USER_ADID  
			


	INSERT INTO [COWS].[dbo].[USER_WFM_ASMT]
			   ([ORDR_ID],[GRP_ID],[ASN_USER_ID],[ASN_BY_USER_ID],[ASMT_DT]
			   ,[CREAT_DT],[ORDR_HIGHLIGHT_CD],[ORDR_HIGHLIGHT_MODFD_DT],[DSPTCH_TM]
			   ,[INRTE_TM],[ONSITE_TM],[CMPLT_TM],[EVENT_ID])
		 VALUES
			   (@ORDR_ID,15,@Tech_USER_ID,@ASN_BY_USER_ID,GETDATE(),GETDATE()
			   ,null,null,@DSPTCH_TM,null,null,null,@EVENT_ID)

END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH
END

