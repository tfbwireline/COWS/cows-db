USE COWS
GO
_CreateObject 'SP','dbo','getOrderMilestoneDataForSpecificVersion'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <09/06/2011>
-- Description:	<Retrieve Order Milestone Data for specific version of an order>
-- =============================================
ALTER PROCEDURE dbo.getOrderMilestoneDataForSpecificVersion 
@ORDR_ID INT,
@VER_ID  SMALLINT	
AS
BEGIN
	BEGIN TRY
		SELECT	o.ORDR_ID AS OrderID
				,ISNULL(o.VER_ID,1) AS VersionID
				,ISNULL(dbo.convertOrderMilestonedates(o.PRE_SBMT_DT),'') AS PreSubmitDate
				,ISNULL(dbo.convertOrderMilestonedates(o.SBMT_DT),'') AS SubmitDate
				,ISNULL(dbo.convertOrderMilestonedates(o.VLDTD_DT),'') AS ValidatedDate
				--,ISNULL(dbo.convertOrderMilestonedates(o.SENT_TO_VNDR_DT),'') AS VendorSentDate
				--,ISNULL(dbo.convertOrderMilestonedates(o.ACK_BY_VNDR_DT),'') AS VendorAckDate
				,ISNULL(dbo.convertOrderMilestonedates(o.ORDR_BILL_CLEAR_INSTL_DT),'') AS BillClearDate
				--,ISNULL(dbo.convertOrderMilestonedates(o.VNDR_CNFRM_DSCNCT_DT),'') AS VendorConfirmDiscDate
				,ISNULL(dbo.convertOrderMilestonedates(o.ORDR_DSCNCT_DT),'') AS OrdrDiscDate
				,ISNULL(dbo.convertOrderMilestonedates(o.VNDR_CNCLN_DT),'') AS VendorCancellationDate
				,ISNULL(dbo.convertOrderMilestonedates(o.CUST_ACPTC_TURNUP_DT),'') AS CUST_ACPTC_TURNUP_DT
				,o.CREAT_BY_USER_ID AS CreatedUserID
				,o.CREAT_DT AS CreatedDate
		FROM	dbo.ORDR_MS		o	WITH (NOLOCK)	
		WHERE	o.ORDR_ID	=	@ORDR_ID
			AND	o.VER_ID	=	@VER_ID	
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo	
	END CATCH	
END
GO
