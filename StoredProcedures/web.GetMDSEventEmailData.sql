USE [COWS]
GO
_CreateObject 'SP','web','GetMDSEventEmailData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [web].[GetMDSEventEmailData] --6524
	@iEventID BIGINT        
AS      
BEGIN      
SET NOCOUNT ON;      

BEGIN TRY      
		OPEN SYMMETRIC KEY FS@K3y     
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;    

	IF OBJECT_ID (N'tempdb..#FSA_MDS_EVENT',N'U') IS NOT NULL    
		DROP TABLE #FSA_MDS_EVENT     

	DECLARE @CREAT_DT DATETIME  

	DECLARE @NewMDS BIT = 0
	
	IF EXISTS(SELECT 'X' FROM dbo.MDS_EVENT WITH (NOLOCK) WHERE EVENT_ID=@iEventID)
		SET @NewMDS = 1
	
	IF (@NewMDS = 0)
	BEGIN
	SELECT			MDS_EVENT_NEW.EVENT_ID AS EVENT_ID, 
					FSA_MDS_EVENT_NEW.FSA_MDS_EVENT_ID   
		INTO		#FSA_MDS_EVENT   
		FROM		dbo.MDS_EVENT_NEW WITH (NOLOCK)  
		LEFT JOIN	dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) ON FSA_MDS_EVENT_NEW.EVENT_ID = MDS_EVENT_NEW.EVENT_ID  
		WHERE		MDS_EVENT_NEW.EVENT_ID = @iEventID  

	SELECT DISTINCT mp.EVENT_ID,    
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.EVENT_TITLE_TXT ELSE ISNULL(dbo.decryptBinaryData(csd.EVENT_TITLE_TXT),'') END AS EVENT_TITLE_TXT,    
					CASE mp.MDS_FAST_TRK_CD    
					WHEN 1 THEN 'FastTrack'    
					WHEN 0 THEN 'Non-Fast Track'    
					ELSE 'Non-FAST TRACK'    
					END AS MDS_FAST_TRK_CD,    
					ftt.MDS_FAST_TRK_TYPE_DES,    
					mp.H1_ID as H1,    
					mp.CHARS_ID,    
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.CUST_NME ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'') END AS CUST_NME,    
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.CUST_EMAIL_ADR ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_EMAIL_ADR),'') END AS CUST_EMAIL_ADR,
					mac.MDS_ACTY_TYPE_DES,    
					mp.MDS_ACTY_TYPE_ID,    
					'' AS MDS_INSTL_ACTY_TYPE_DES,    
					--mp.MDS_INSTL_ACTY_TYPE_ID,    
					mp.DSGN_DOC_LOC_TXT,    
					mp.SHRT_DES,    
					CASE mp.FULL_CUST_DISC_CD    
					WHEN 1 THEN 'Full Disconnect'    
					WHEN 0 THEN 'Partial Disconnect'    
					ELSE 'Partial Disconnect'    
					END AS FULL_CUST_DISC_CD,    
					CASE WHEN mp.DEV_CNT < 0 THEN 0     
					ELSE mp.DEV_CNT      
					END AS DEV_CNT,    
					mp.DISC_NTFY_PDL_NME,    
					CASE mp.MDS_BRDG_NEED_ID    
					WHEN 1 THEN 'Yes'    
					WHEN 0 THEN 'No'    
					ELSE 'No'    
					END AS MDS_BRDG_NEED_CD,    
					mp.CNFRC_BRDG_NBR,    
					mp.CNFRC_PIN_NBR,
					mp.ONLINE_MEETING_ADR,    
					mp.STRT_TMST,    
					mp.END_TMST,    
					usrc.FULL_NME AS CREAT_BY_USER_ID,    
					usrc.EMAIL_ADR AS CREAT_BY_EMAIL,    
					usrc.USER_ADID AS CREAT_BY_ADID,    
					mp.EXTRA_DRTN_TME_AMT,    
					mp.EVENT_DRTN_IN_MIN_QTY,    
					'' AS MANL_CD,    
					CASE mp.TME_SLOT_ID    
					WHEN NULL THEN 'SLOTS N/A'    
					WHEN 1 THEN '06:00 AM CT-10:00 AM CT'    
					WHEN 2 THEN '10:00 AM CT-02:00 PM CT'    
					WHEN 3 THEN '02:00 PM CT-06:00 PM CT'    
					WHEN 4 THEN '06:00 PM CT-10:00 PM CT'
					WHEN 21 THEN '10:00 PM CT-02:00 AM CT'
					WHEN 22 THEN '02:00 AM CT-06:00 AM CT'    
					ELSE 'SLOTS N/A'    
					END AS TME_SLOT_ID,    
					les.EVENT_STUS_DES AS EVENT_STUS_DES,    
					usrm.FULL_NME AS MODFD_BY_USER_ID,    
					mp.MODFD_DT,    
					mp.CREAT_DT,    
					mp.PUB_EMAIL_CC_TXT,    
					mp.CMPLTD_EMAIL_CC_TXT,    
					lws.WRKFLW_STUS_DES AS WRKFLW_STUS_DES,         
					CASE mp.FAIL_REAS_ID    
					WHEN 0 THEN ' '    
					WHEN NULL THEN ' '    
					ELSE fr.FAIL_REAS_DES    
					END FAIL_REAS_DES,    
					CASE mp.ESCL_REAS_ID    
					WHEN NULL THEN ' '     
					WHEN 0 THEN ' '    
					ELSE er.ESCL_REAS_DES    
					END ESCL_REAS_DES,    
					mp.PRIM_REQ_DT,    
					mp.SCNDY_REQ_DT,    
					CASE mp.REQOR_USER_ID    
					WHEN NULL THEN ' '    
					WHEN 0 THEN ' '    
					ELSE usrr.FULL_NME    
					END REQOR_USER,    
					CASE mp.REQOR_USER_ID    
					WHEN NULL THEN ' '    
					WHEN 0 THEN ' '    
					ELSE usrr.PHN_NBR    
					END REQOR_USER_PHN,
					CASE mp.REQOR_USER_ID    
					WHEN NULL THEN ' '    
					WHEN 0 THEN ' '    
					ELSE usrr.EMAIL_ADR    
					END REQOR_USER_EMAIL,    
					CASE mp.ESCL_CD    
					WHEN 0 THEN 'NOT ESCALATED'    
					WHEN 1 THEN 'ESCALATED'    
					ELSE 'NOT ESCALATED'    
					END AS ESCL_CD,    
					mp.CUST_ACCT_TEAM_PDL_NME,    
					les.EVENT_STUS_DES,    
					lws.WRKFLW_STUS_DES,    
					leso.EVENT_STUS_DES AS OLD_EVENT_STUS_DES,    
					CASE mp.PRE_CFG_CMPLT_CD    
					WHEN 1 THEN 'Yes'    
					WHEN 0 THEN 'No'    
					ELSE 'No'    
					END PRE_CFG_CMPLT_CD,  
					ev.CSG_LVL_ID,  
					mp.SOWS_EVENT_ID,
					--mp.FULFILLED_DT,
					mp.SHIPPED_DT,
					--mp.SHIP_CUST_NME,
					mp.SHIP_CUST_EMAIL_ADR,
					--mp.SHIP_CUST_PHN_NBR,
					mp.SHIP_TRK_REFR_NBR,
					mp.DEV_SERIAL_NBR,
					mp.WOOB_IP_ADR, CASE mp.MDS_BRDG_NEED_CD WHEN NULL THEN 0 ELSE mp.MDS_BRDG_NEED_CD END AS MDS_BRDG_NEED_CD
		FROM		dbo.MDS_EVENT_NEW mp WITH (NOLOCK)
		INNER JOIN	dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID = mp.EVENT_ID
		LEFT JOIN	#FSA_MDS_EVENT fe WITH (NOLOCK) ON mp.EVENT_ID = fe.EVENT_ID    
		INNER JOIN	dbo.LK_EVENT_STUS les WITH (NOLOCK) ON les.EVENT_STUS_ID = mp.EVENT_STUS_ID    
		INNER JOIN	dbo.LK_WRKFLW_STUS lws WITH (NOLOCK) ON lws.WRKFLW_STUS_ID = mp.WRKFLW_STUS_ID    
		LEFT JOIN	dbo.LK_EVENT_STUS leso WITH (NOLOCK) ON leso.EVENT_STUS_ID = mp.OLD_EVENT_STUS_ID    
		--LEFT JOIN	dbo.LK_MDS_INSTL_ACTY_TYPE mat WITH (NOLOCK) ON  mat.MDS_INSTL_ACTY_TYPE_ID  = mp.MDS_INSTL_ACTY_TYPE_ID    
		LEFT JOIN	dbo.LK_MDS_ACTY_TYPE mac WITH(NOLOCK) ON mac.MDS_ACTY_TYPE_ID = mp.MDS_ACTY_TYPE_ID    
		LEFT JOIN	dbo.MDS_FAST_TRK_TYPE ftt WITH(NOLOCK) ON ftt.MDS_FAST_TRK_TYPE_ID = mp.MDS_FAST_TRK_TYPE_ID    
		LEFT JOIN	dbo.LK_EVENT_TYPE_TME_SLOT ett WITH (NOLOCK) ON ett.TME_SLOT_ID = mp.TME_SLOT_ID     
		LEFT JOIN	dbo.LK_FAIL_REAS fr WITH (NOLOCK) ON mp.FAIL_REAS_ID = fr.FAIL_REAS_ID    
		LEFT JOIN	dbo.LK_ESCL_REAS er WITH (NOLOCK) ON er.ESCL_REAS_ID = mp.ESCL_REAS_ID    
		LEFT JOIN	dbo.LK_USER usrc WITH(NOLOCK) ON usrc.[user_id] = mp.CREAT_BY_USER_ID    
		LEFT JOIN	dbo.LK_USER usrm WITH(NOLOCK) ON usrm.[user_id] = mp.MODFD_BY_USER_ID    
		LEFT JOIN	dbo.LK_USER usrr WITH(NOLOCK) ON usrr.[user_id] = mp.REQOR_USER_ID  
		LEFT JOIN	dbo.LK_MDS_BRDG_NEED_REAS mbn  WITH (NOLOCK) ON mbn.MDS_BRDG_NEED_ID = mp.MDS_BRDG_NEED_ID
		LEFT JOIN   dbo.CUST_SCRD_DATA  csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mp.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=8
		WHERE	mp.EVENT_ID = @iEventID
		
	SELECT		FSA_MDS_EVENT_ID, 
				TAB_SEQ_NBR, 
				TAB_NME, 
				EVENT_ID     
		FROM	dbo.FSA_MDS_EVENT_NEW  WITH (NOLOCK)    
		WHERE	EVENT_ID = @iEventID    
	ORDER BY TAB_SEQ_NBR ASC
	END
	ELSE
	BEGIN
		SELECT DISTINCT top 1 mp.EVENT_ID,    
					ev.CSG_LVL_ID,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.EVENT_TITLE_TXT ELSE ISNULL(dbo.decryptBinaryData(csd.EVENT_TITLE_TXT),'') END AS EVENT_TITLE_TXT,    
					CASE WHEN ISNULL(mp.MDS_FAST_TRK_TYPE_ID,'') <> ''
					THEN 'FastTrack'
					ELSE 'Non-FAST TRACK'    
					END AS MDS_FAST_TRK_CD,    
					ftt.MDS_FAST_TRK_TYPE_DES,    
					mp.H1,    
					mp.CHARS_ID,    
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.CUST_NME ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'') END AS CUST_NME,    
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.CUST_ACCT_TEAM_PDL_NME ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_EMAIL_ADR),'') END AS CUST_EMAIL_ADR,
					mac.MDS_ACTY_TYPE_DES,    
					mp.MDS_ACTY_TYPE_ID,    
					'' AS MDS_INSTL_ACTY_TYPE_DES,    
					--mp.MDS_INSTL_ACTY_TYPE_ID,    
					mp.CUST_SOW_LOC_TXT,    
					mp.SHRT_DES,    
					CASE mp.FULL_CUST_DISC_CD    
					WHEN 1 THEN 'Full Disconnect'    
					WHEN 0 THEN 'Partial Disconnect'    
					ELSE 'Partial Disconnect'    
					END AS FULL_CUST_DISC_CD,
					mp.FULL_CUST_DISC_REAS_TXT,
					CASE mp.CNFRC_BRDG_ID    
					WHEN 1 THEN 'Yes'    
					WHEN 0 THEN 'No'    
					ELSE 'No'    
					END AS MDS_BRDG_NEED_CD,    
					mp.CNFRC_BRDG_NBR,    
					mp.CNFRC_PIN_NBR,
					mp.ONLINE_MEETING_ADR,    
					mp.STRT_TMST,    
					mp.END_TMST,    
					usrc.FULL_NME AS CREAT_BY_USER_ID,    
					usrc.EMAIL_ADR AS CREAT_BY_EMAIL,    
					usrc.USER_ADID AS CREAT_BY_ADID,    
					mp.EXTRA_DRTN_TME_AMT,    
					mp.EVENT_DRTN_IN_MIN_QTY,    
					'' AS MANL_CD,    
					CASE mp.TME_SLOT_ID    
					WHEN NULL THEN 'SLOTS N/A'    
					WHEN 1 THEN '06:00 AM CT-10:00 AM CT'    
					WHEN 2 THEN '10:00 AM CT-02:00 PM CT'    
					WHEN 3 THEN '02:00 PM CT-06:00 PM CT'    
					WHEN 4 THEN '06:00 PM CT-10:00 PM CT'
					WHEN 21 THEN '10:00 PM CT-02:00 AM CT'
					WHEN 22 THEN '02:00 AM CT-06:00 AM CT'    
					ELSE 'SLOTS N/A'    
					END AS TME_SLOT_ID,    
					les.EVENT_STUS_DES AS EVENT_STUS_DES,    
					usrm.FULL_NME AS MODFD_BY_USER_ID,    
					mp.MODFD_DT,    
					mp.CREAT_DT,    
					mp.PUB_EMAIL_CC_TXT,    
					mp.CMPLTD_EMAIL_CC_TXT,    
					lws.WRKFLW_STUS_DES AS WRKFLW_STUS_DES,         
					CASE mp.FAIL_REAS_ID    
					WHEN 0 THEN ' '    
					WHEN NULL THEN ' '    
					ELSE fr.FAIL_REAS_DES    
					END FAIL_REAS_DES,    
					CASE mp.ESCL_REAS_ID    
					WHEN NULL THEN ' '     
					WHEN 0 THEN ' '    
					ELSE er.ESCL_REAS_DES    
					END ESCL_REAS_DES,    
					mp.PRIM_REQ_DT,    
					mp.SCNDY_REQ_DT,    
					CASE mp.REQOR_USER_ID    
					WHEN NULL THEN ' '    
					WHEN 0 THEN ' '    
					ELSE usrr.FULL_NME    
					END REQOR_USER,    
					CASE mp.REQOR_USER_ID    
					WHEN NULL THEN ' '    
					WHEN 0 THEN ' '    
					ELSE usrr.PHN_NBR    
					END REQOR_USER_PHN,
					CASE mp.REQOR_USER_ID    
					WHEN NULL THEN ' '    
					WHEN 0 THEN ' '    
					ELSE usrr.EMAIL_ADR    
					END REQOR_USER_EMAIL,    
					CASE mp.ESCL_CD    
					WHEN 0 THEN 'NOT ESCALATED'    
					WHEN 1 THEN 'ESCALATED'    
					ELSE 'NOT ESCALATED'    
					END AS ESCL_CD,    
					mp.CUST_ACCT_TEAM_PDL_NME,    
					les.EVENT_STUS_DES,    
					lws.WRKFLW_STUS_DES,    
					leso.EVENT_STUS_DES AS OLD_EVENT_STUS_DES,    
					CASE mp.PRE_CFG_CMPLT_CD    
					WHEN 1 THEN 'Yes'    
					WHEN 0 THEN 'No'    
					ELSE 'No'    
					END PRE_CFG_CMPLT_CD,  
					mp.SHIPPED_DT,
					mp.SHIP_CUST_EMAIL_ADR,
					mp.SHIP_TRK_REFR_NBR,
					mp.WOOB_IP_ADR, 
					CASE mp.CNFRC_BRDG_ID WHEN NULL THEN 0 ELSE mp.CNFRC_BRDG_ID END AS MDS_BRDG_NEED_CD,
					mp.CMNT_TXT,
					mp.DISC_MGMT_CD,
					mp.US_INTL_CD,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.INSTL_SITE_POC_NME ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_CNTCT_NME),'') END AS INSTL_SITE_POC_NME,
					mp.INSTL_SITE_POC_INTL_PHN_CD,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.INSTL_SITE_POC_PHN_NBR ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_CNTCT_PHN_NBR),'') END AS INSTL_SITE_POC_PHN_NBR,
					mp.INSTL_SITE_POC_INTL_CELL_PHN_CD,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.INSTL_SITE_POC_CELL_PHN_NBR ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_CNTCT_CELL_PHN_NBR),'') END AS INSTL_SITE_POC_CELL_PHN_NBR,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.SRVC_ASSRN_POC_NME ELSE ISNULL(dbo.decryptBinaryData(csd.SRVC_ASSRN_POC_NME),'') END AS SRVC_ASSRN_POC_NME,
					mp.SRVC_ASSRN_POC_INTL_PHN_CD,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.SRVC_ASSRN_POC_PHN_NBR ELSE ISNULL(dbo.decryptBinaryData(csd.SRVC_ASSRN_POC_PHN_NBR),'') END AS SRVC_ASSRN_POC_PHN_NBR,
					mp.SRVC_ASSRN_POC_INTL_CELL_PHN_CD,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.SRVC_ASSRN_POC_CELL_PHN_NBR ELSE ISNULL(dbo.decryptBinaryData(csd.SRVC_ASSRN_POC_CELL_PHN_NBR),'') END AS SRVC_ASSRN_POC_CELL_PHN_NBR,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.STREET_ADR ELSE ISNULL(dbo.decryptBinaryData(csd.STREET_ADR_1),'') END AS STREET_ADR,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.FLR_BLDG_NME ELSE ISNULL(dbo.decryptBinaryData(csd.FLR_ID),'') END AS FLR_BLDG_NME,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.CTY_NME ELSE ISNULL(dbo.decryptBinaryData(csd.CTY_NME),'') END AS CTY_NME,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.STT_PRVN_NME ELSE ISNULL(dbo.decryptBinaryData(csd.STT_PRVN_NME),'') END AS STT_PRVN_NME,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.CTRY_RGN_NME ELSE ISNULL(dbo.decryptBinaryData(csd.CTRY_RGN_NME),'') END AS CTRY_RGN_NME,
					CASE WHEN ev.CSG_LVL_ID=0 THEN mp.ZIP_CD ELSE ISNULL(dbo.decryptBinaryData(csd.ZIP_PSTL_CD),'') END AS ZIP_CD,
					mp.SRVC_TME_ZN_CD,
					mp.SRVC_AVLBLTY_HRS
		FROM		dbo.MDS_EVENT mp WITH (NOLOCK)
		INNER JOIN	dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID = mp.EVENT_ID  
		INNER JOIN	dbo.LK_EVENT_STUS les WITH (NOLOCK) ON les.EVENT_STUS_ID = mp.EVENT_STUS_ID    
		INNER JOIN	dbo.LK_WRKFLW_STUS lws WITH (NOLOCK) ON lws.WRKFLW_STUS_ID = mp.WRKFLW_STUS_ID    
		LEFT JOIN	dbo.LK_EVENT_STUS leso WITH (NOLOCK) ON leso.EVENT_STUS_ID = mp.OLD_EVENT_STUS_ID    
		--LEFT JOIN	dbo.LK_MDS_INSTL_ACTY_TYPE mat WITH (NOLOCK) ON  mat.MDS_INSTL_ACTY_TYPE_ID  = mp.MDS_INSTL_ACTY_TYPE_ID    
		LEFT JOIN	dbo.LK_MDS_ACTY_TYPE mac WITH(NOLOCK) ON mac.MDS_ACTY_TYPE_ID = mp.MDS_ACTY_TYPE_ID    
		LEFT JOIN	dbo.MDS_FAST_TRK_TYPE ftt WITH(NOLOCK) ON ftt.MDS_FAST_TRK_TYPE_ID = mp.MDS_FAST_TRK_TYPE_ID    
		LEFT JOIN	dbo.LK_EVENT_TYPE_TME_SLOT ett WITH (NOLOCK) ON ett.TME_SLOT_ID = mp.TME_SLOT_ID     
		LEFT JOIN	dbo.LK_FAIL_REAS fr WITH (NOLOCK) ON mp.FAIL_REAS_ID = fr.FAIL_REAS_ID    
		LEFT JOIN	dbo.LK_ESCL_REAS er WITH (NOLOCK) ON er.ESCL_REAS_ID = mp.ESCL_REAS_ID    
		LEFT JOIN	dbo.LK_USER usrc WITH(NOLOCK) ON usrc.[user_id] = mp.CREAT_BY_USER_ID    
		LEFT JOIN	dbo.LK_USER usrm WITH(NOLOCK) ON usrm.[user_id] = mp.MODFD_BY_USER_ID    
		LEFT JOIN	dbo.LK_USER usrr WITH(NOLOCK) ON usrr.[user_id] = mp.REQOR_USER_ID  
		LEFT JOIN	dbo.LK_MDS_BRDG_NEED_REAS mbn  WITH (NOLOCK) ON mbn.MDS_BRDG_NEED_ID = mp.CNFRC_BRDG_ID
		LEFT JOIN   dbo.CUST_SCRD_DATA  csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mp.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=7
		WHERE	mp.EVENT_ID = @iEventID
	END
	
	SELECT TOP 1	lu.FULL_NME AS MOD_USR_NME,    
					lu.EMAIL_ADR AS MOD_USR_EMAIL,    
					REPLACE(COALESCE(eh.CMNT_TXT, ''),'<br/>','') AS CMNT_TXT    
		FROM		dbo.EVENT_HIST eh WITH (NOLOCK)    
		INNER JOIN	dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID] = eh.CREAT_BY_USER_ID    
		-- Updated by Sarah Sandoval [20210218] - Changed Role to User Profiles
		--INNER JOIN	dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON ugr.[USER_ID] = lu.[USER_ID]    
		--WHERE		ugr.ROLE_ID IN (22, 23) 
		--	AND		ugr.REC_STUS_ID = 1 
		INNER JOIN	dbo.MAP_USR_PRF mup WITH (NOLOCK) ON mup.[USER_ID] = lu.[USER_ID]    
		WHERE		mup.USR_PRF_ID IN (6,20,104,132,160,4,18,102,130,158) -- xNCI/MDS/Salest Support Event Reviewer; xNCI/MDS/Salest Support Event Activator
			AND		mup.REC_STUS_ID = 1 
			AND		lu.[USER_ID] != 5001
			AND		eh.EVENT_ID = @iEventID    
			AND		COALESCE(eh.CMNT_TXT, '') <> '' 
			AND		eh.CMNT_TXT NOT LIKE '%-->%'    
		ORDER BY	eh.EVENT_HIST_ID DESC    

	SELECT [dbo].[GetCommaSepAssignUserIDs] (@iEventID) as AssignUsers    

	SELECT [dbo].[GetCommaSepAssignUserEmails] (@iEventID) as AssignEmails    

	SELECT TOP 1	@CREAT_DT = CREAT_DT    
		FROM		DBO.EVENT_ASN_TO_USER WITH (NOLOCK)    
		WHERE		[EVENT_ID]=@iEventID    
			AND		REC_STUS_ID=0    
			AND		CREAT_DT <> (SELECT TOP 1 CREAT_DT FROM DBO.EVENT_ASN_TO_USER WITH (NOLOCK)    
									WHERE		[EVENT_ID]=@iEventID    
										AND		REC_STUS_ID=1    
									ORDER BY	CREAT_DT DESC)    
		GROUP BY	CREAT_DT    
		ORDER BY	CREAT_DT DESC    

	SELECT DISTINCT eau.[ASN_TO_USER_ID] AS OLD_ASN_TO_USER_ID,    
					lu.[EMAIL_ADR]    
		FROM		DBO.EVENT_ASN_TO_USER eau WITH (NOLOCK) INNER JOIN    
					DBO.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID] = eau.[ASN_TO_USER_ID]    
		WHERE		eau.[EVENT_ID]=@iEventID    
			AND		eau.REC_STUS_ID=0    
			AND		eau.ASN_TO_USER_ID != 5001
			AND		eau.CREAT_DT = @CREAT_DT    

	SELECT TOP 1	lu.FULL_NME AS REV_USR_NME,    
					lu.EMAIL_ADR AS REV_USR_EMAIL,    
					lu.USER_ADID AS REV_USR_ADID,    
					ISNULL(lu.PHN_NBR, '') AS REV_PHN_NBR,
					'' AS CMNT_TXT    
		FROM		dbo.MDS_EVENT me WITH (NOLOCK)
		INNER JOIN	dbo.EVENT_HIST eh WITH (NOLOCK) ON me.EVENT_ID=eh.EVENT_ID
		INNER JOIN	dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID] = eh.CREAT_BY_USER_ID  
		-- Updated by Sarah Sandoval [20210218] - Changed Role to User Profiles  
		--INNER JOIN	dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON lu.[USER_ID] = ugr.[USER_ID] AND ugr.ROLE_ID=22
		INNER JOIN	dbo.MAP_USR_PRF mup WITH (NOLOCK) ON lu.[USER_ID] = mup.[USER_ID] AND mup.USR_PRF_ID IN (6,20,104,132,160)
		WHERE		me.[EVENT_ID]=@iEventID  
			AND		lu.[USER_ID] != 5001	
		ORDER BY	eh.EVENT_HIST_ID DESC			
		
	SELECT [dbo].[getCommaSepRDSNs] (@iEventID, 'MDS')   AS RDSN_NBR

	SELECT [dbo].[getCommaSepAssignUserPhoneNumbers] (@iEventID) as AssignUserPhoneNumbers    	

	IF (@NewMDS = 1)
	BEGIN
		SELECT edd.ODIE_DEV_NME,
			   edd.H5_H6,
			   edd.SITE_ID,
			   edd.DEVICE_ID,
			   ld.DEV_MODEL_NME,
			   ldm.MANF_NME,
			   edd.SERIAL_NBR,
			   edd.THRD_PARTY_CTRCT
		FROM dbo.EVENT_DISCO_DEV edd WITH (NOLOCK) LEFT JOIN
			 dbo.LK_DEV_MODEL ld WITH (NOLOCK) ON ld.DEV_MODEL_ID = edd.DEV_MODEL_ID LEFT JOIN
			 dbo.LK_DEV_MANF ldm WITH (NOLOCK) ON ldm.MANF_ID = edd.MANF_ID
		WHERE edd.EVENT_ID=@iEventID
		  AND edd.REC_STUS_ID=1
		
		SELECT meod.FAST_TRK_CD,
			   meod.FRWL_PROD_CD,
			   meod.INTL_CTRY_CD,
			   meod.PHN_NBR,
			   meod.WOOB_CD,
			   lss.SRVC_ASSRN_SITE_SUPP_DES,
			   ld.DEV_MODEL_NME,
			   ldm.MANF_NME,
			   meod.ODIE_DEV_NME,
			   meod.RDSN_NBR,
			   meod.OPTOUT_CD,
			   meod.RDSN_EXP_DT,
			   meod.SC_CD
		FROM dbo.MDS_EVENT_ODIE_DEV meod WITH (NOLOCK) LEFT JOIN
		     dbo.LK_SRVC_ASSRN_SITE_SUPP lss WITH (NOLOCK) ON lss.SRVC_ASSRN_SITE_SUPP_ID = meod.SRVC_ASSRN_SITE_SUPP_ID LEFT JOIN
		     dbo.LK_DEV_MODEL ld WITH (NOLOCK) ON ld.DEV_MODEL_ID = meod.DEV_MODEL_ID LEFT JOIN
		     dbo.LK_DEV_MANF ldm WITH (NOLOCK) ON ldm.MANF_ID = meod.MANF_ID
		WHERE meod.EVENT_ID=@iEventID 
		
		SELECT 	edc.ODIE_DEV_NME,
				edc.H6,
				edc.CMPLTD_CD				
		FROM dbo.EVENT_DEV_CMPLT edc WITH (NOLOCK)
		WHERE edc.EVENT_ID=@iEventID 
		  AND edc.REC_STUS_ID=1
		
		SELECT  ecd.DEVICE_ID,
				ecd.MAINT_VNDR_PO,
				ecd.ODIE_CD,
				ecd.ODIE_DEV_NME,
				ecd.CPE_ORDR_ID,
				ecd.CPE_ORDR_NBR,
				ecd.ASSOC_H6,
				ecd.CCD,
				ecd.ORDR_STUS,
				ecd.RCPT_STUS
		FROM dbo.EVENT_CPE_DEV ecd WITH (NOLOCK) 
		WHERE ecd.EVENT_ID=@iEventID 
		  AND ecd.REC_STUS_ID = 1
		
		SELECT 	MDS_TRNSPRT_TYPE,
				PRIM_BKUP_CD,
                VNDR_PRVDR_TRPT_CKT_ID,
                VNDR_PRVDR_NME,
                SPRINT_MNGD_CD,
                IS_WIRELESS_CD,
                BW_ESN_MEID,
                SCA_NBR,
                IP_ADR,
                SUBNET_MASK_ADR,
                NXTHOP_GTWY_ADR,
                ODIE_DEV_NME
		FROM dbo.MDS_EVENT_DSL_SBIC_CUST_TRPT WITH (NOLOCK)
		WHERE EVENT_ID=@iEventID 
		  AND REC_STUS_ID = 1
		
		SELECT	MDS_TRNSPRT_TYPE,
                PRIM_BKUP_CD,
                VNDR_PRVDR_TRPT_CKT_ID,
                VNDR_PRVDR_NME,
                SPRINT_MNGD_CD,
                BDWD_CHNL_NME,
                IP_NUA_ADR,
                PL_NBR,
                OLD_CKT_ID,
                MULTI_LINK_CKT_CD,
                DED_ACCS_CD,
                ODIE_DEV_NME,
                DLCI_VPI_VCI,
                FMS_NBR,
                SPA_NUA,
                VLAN_NBR,
				READY_BEGIN_CD,
				OPT_IN_CKT_CD
		FROM dbo.MDS_EVENT_SLNK_WIRED_TRPT WITH (NOLOCK)
		WHERE EVENT_ID=@iEventID 
		  AND REC_STUS_ID = 1
		
		SELECT	mess.MACH5_SRVC_ORDR_ID,
				lmst.SRVC_TYPE_DES,
				mess.THRD_PARTY_ID,
				lm3sl.THRD_PARTY_SRVC_LVL_DES,
				lm3v.THRD_PARTY_VNDR_DES,
				mess.ACTV_DT,
				mess.ODIE_DEV_NME,
				mess.CMNT_TXT
		FROM dbo.MDS_EVENT_SITE_SRVC mess WITH (NOLOCK) LEFT JOIN
			 dbo.LK_MDS_SRVC_TYPE lmst WITH (NOLOCK) ON lmst.SRVC_TYPE_ID = mess.SRVC_TYPE_ID LEFT JOIN
			 dbo.LK_MDS_3RDPARTY_VNDR lm3v WITH (NOLOCK) ON lm3v.THRD_PARTY_VNDR_ID = mess.THRD_PARTY_VNDR_ID LEFT JOIN
			 dbo.LK_MDS_3RDPARTY_SRVC_LVL lm3sl WITH (NOLOCK) ON lm3sl.THRD_PARTY_SRVC_LVL_ID = mess.THRD_PARTY_SRVC_LVL_ID
		WHERE mess.EVENT_ID=@iEventID
		
		SELECT 	edsm.MNS_ORDR_NBR,
				edsm.DEVICE_ID,
				lm.MDS_SRVC_TIER_DES,
				edsm.M5_ORDR_CMPNT_ID,
				edsm.ODIE_DEV_NME,
				edsm.CMPNT_STUS,
				edsm.CMPNT_TYPE_CD,
				edsm.CMPNT_NME,
				edsm.ODIE_CD
		FROM dbo.EVENT_DEV_SRVC_MGMT edsm WITH (NOLOCK) LEFT JOIN
			 dbo.LK_MDS_SRVC_TIER lm WITH (NOLOCK) ON lm.MDS_SRVC_TIER_ID = edsm.MNS_SRVC_TIER_ID
		WHERE edsm.EVENT_ID=@iEventID 
		  AND edsm.REC_STUS_ID = 1
		
		SELECT	WRLS_TYPE_CD,
                PRIM_BKUP_CD,
                ESN_MAC_ID,
                BILL_ACCT_NME,
                BILL_ACCT_NBR,
                ACCT_PIN,
                SALS_CD,
                SOC,
                STATIC_IP_ADR,
                IMEI,
                UICC_ID_NBR,
                PRICE_PLN,
                ODIE_DEV_NME
		FROM dbo.MDS_EVENT_WRLS_TRPT WITH (NOLOCK)
		WHERE EVENT_ID=@iEventID
		
		DECLARE @TRPT_FTN VARCHAR(MAX)
		SELECT	@TRPT_FTN = CASE WHEN (CHARINDEX(ISNULL(ecd.CPE_ORDR_NBR,''),@TRPT_FTN)>0) THEN @TRPT_FTN ELSE COALESCE(@TRPT_FTN + ',', '')   + ISNULL(ecd.CPE_ORDR_NBR, '') END
		FROM dbo.EVENT_CPE_DEV ecd WITH (NOLOCK) 
		WHERE ecd.EVENT_ID=@iEventID
		  AND ecd.REC_STUS_ID = 1
		SELECT @TRPT_FTN
		
		SELECT	M5_ORDR_NBR,
				M5_ORDR_CMPNT_ID,
				NUA,
				PORT_BNDWD,
                ODIE_DEV_NME
		FROM dbo.MDS_EVENT_PORT_BNDWD WITH (NOLOCK)
		WHERE EVENT_ID=@iEventID
		  AND REC_STUS_ID = 1
	END

END TRY      
BEGIN CATCH      
	EXEC [dbo].[insertErrorInfo]   
	   
	DECLARE @ErrMsg			nVarchar(4000),       
			@ErrSeverity	Int            
	
	SELECT	@ErrMsg			=	ERROR_MESSAGE(),            
			@ErrSeverity	=	ERROR_SEVERITY()            
	RAISERROR(@ErrMsg, @ErrSeverity, 1)            
END CATCH      
END  
