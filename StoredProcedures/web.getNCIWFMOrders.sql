USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getNCIWFMOrders]    Script Date: 02/01/2021 2:08:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 05/22/2012
-- Description:	Gets the list of xNCI WFM Orders.
-- =========================================================
 -- [web].[getNCIWFMOrders] 'ASSIGNED_USER asc','REGION = ''AMNCI''  |REGION = ''AMNCI'' ' 
ALTER PROCEDURE [web].[getNCIWFMOrders] 
	@Sort			Varchar(200)	=	''
	,@Filter		Varchar(200)	=	''
AS
BEGIN
SET NOCOUNT ON;
Begin Try
DECLARE @SQL	nVarchar(Max)
DECLARE @SQL1	nVARCHAR(max)

Declare @rgn Varchar(100)

If CharIndex('|',@Filter,0) > 0 
	BEGIN
		Set @rgn = substring(@Filter,CharIndex('|',@Filter,0) + 1,100)
		Set @Filter = substring(@Filter,0,CharIndex('|',@Filter,0))
	END


SET @SQL1 = ''
SET	@SQL	=	'	

	IF OBJECT_ID(N''tempdb..#XNCIWFMTmp'', N''U'') IS NOT NULL
	DROP TABLE #XNCIWFMTmp
CREATE TABLE #XNCIWFMTmp 
		(ORDR_ID		INT
		,FTN			Varchar(20)
		,PROD_TYPE		Varchar(100)
		,ORDR_TYPE		Varchar(100)
		,PLTFM_TYPE		Varchar(100)
		,COUNTRY		Varchar(100)
		,ASSIGNED_USER	Varchar(100)
		,ASSIGNED_BY	Varchar(100)
		,ASSIGN_DT		DateTime
		,SUBMIT_DT		DateTime
		,REGION			Varchar(100)
		,CUST_NME		Varchar(100)
		,CUST_ADDR		Varchar(500)
		,CITY			Varchar(100)
		,RTS			Varchar(3)
		,CSG_LVL_ID		TINYINT
		)
	
	INSERT INTO	#XNCIWFMTmp	(ORDR_ID
						,FTN
						,PROD_TYPE
						,ORDR_TYPE
						,PLTFM_TYPE
						,COUNTRY
						,ASSIGNED_USER
						,ASSIGNED_BY
						,ASSIGN_DT
						,SUBMIT_DT
						,REGION
						,CUST_NME	
						,CUST_ADDR
						,CITY
						,RTS
						,CSG_LVL_ID)
	Select distinct 
							od.ORDR_ID								AS	ORDR_ID
							,fsa.FTN AS	FTN
							,lpt.PROD_TYPE_DES						AS	PROD_TYPE
							,lot.ORDR_TYPE_DES						AS  ORDR_TYPE
							,lt.PLTFRM_NME							AS	PLTFM_TYPE
							,ISNULL(lc.CTRY_NME, '''')				AS	COUNTRY
							,ISNULL(lu.DSPL_NME, '''')				AS	ASSIGNED_USER
							,ISNULL(lw.DSPL_NME, '''')				AS	ASSIGNED_BY
							,wu.ASMT_DT								AS	ASSIGN_DT
							,ISNULL(od.CREAT_DT, '''')				AS	SUBMIT_DT
							,ISNULL(lx.RGN_DES, '''')					AS	REGION
							,Case	od.CSG_LVL_ID
								When	0	Then	foc.CUST_NME
								Else	''Private Customer''
							End										AS	CUST_NME
							,	Case	od.CSG_LVL_ID
											When	0	Then	oa.STREET_ADR_1
											Else	''Private Address''
										End										
								AS	CUST_ADDR
							,Case	od.CSG_LVL_ID
									When	0	Then	oa.CTY_NME
									Else	''Private City''

									End										
								AS	CITY
							,Case	ISNULL(at.TASK_ID, 0)
								When	0	Then		''''
								Else	''Yes''

							End										AS	RTS
							,od.CSG_LVL_ID							AS	CSG_LVL_ID 
					From dbo.ORDR od with (nolock) 	
				inner join dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = od.PPRT_ID
				inner join dbo.FSA_ORDR fsa with (nolock) on fsa.ORDR_ID = od.ORDR_ID
				inner join dbo.LK_PROD_TYPE lpt with (nolock) on lpt.PROD_TYPE_ID = lp.PROD_TYPE_ID
				inner join dbo.LK_XNCI_RGN	lx	WITH (NOLOCK)	ON	lx.RGN_ID		=	od.RGN_ID
				inner join dbo.ORDR_ADR oa with (nolock) on oa.ORDR_ID = od.ORDR_ID and oa.CIS_LVL_TYPE in (''h5'',''h6'') and oa.ADR_TYPE_ID NOT IN (19)
				inner join dbo.LK_ORDR_TYPE	lot WITH (NOLOCK)	ON  lot.FSA_ORDR_TYPE_CD = fsa.ORDR_TYPE_CD
				left join dbo.USER_WFM_ASMT wu with (nolock) on od.ORDR_ID = wu.ORDR_ID and ((wu.GRP_ID in (5,6,7)) OR (wu.USR_PRF_ID IN (14, 28, 112)))
				left join dbo.LK_USER	lu	WITH (NOLOCK)	ON	wu.ASN_USER_ID	  =	lu.USER_ID
				left join dbo.LK_USER	lw	WITH (NOLOCK)	ON	wu.ASN_BY_USER_ID =	lw.USER_ID
				inner join dbo.FSA_ORDR_CUST foc with (nolock) on foc.ORDR_ID = od.ORDR_ID and foc.CIS_LVL_TYPE in (''h5'',''h6'')
				left join dbo.LK_CTRY	lc	WITH (NOLOCK)	ON	lc.CTRY_CD	=	oa.CTRY_CD 
				left join dbo.ACT_TASK at with (nolock) on at.ORDR_ID = od.ORDR_ID and at.TASK_ID = 500 and at.STUS_ID = 0
				left join dbo.LK_PLTFRM		lt	WITH (NOLOCK)	ON	lt.PLTFRM_CD	=	od.PLTFRM_CD
				Where	od.ORDR_STUS_ID in (0,1)
						and od.DMSTC_CD = 1
						and od.ORDR_CAT_ID IN (3,4,6)
						and exists
							(
								select ''x''
									from dbo.SM sm with (nolock)
									inner join dbo.WG_PTRN_SM wpt with (nolock) on sm.DESRD_WG_PTRN_ID = wpt.WG_PTRN_ID
									inner join dbo.WG_PROF_SM wpf with (nolock) on wpt.DESRD_WG_PROF_ID = wpf.WG_PROF_ID
									left join dbo.MAP_GRP_TASK mgt with (nolock) on mgt.TASK_ID = wpf.DESRD_TASK_ID
									LEFT JOIN	dbo.MAP_PRF_TASK	mpt	WITH (NOLOCK)	ON	wpf.DESRD_TASK_ID		=	mpt.TASK_ID
								where	((mgt.GRP_ID in (5,6,7)) OR (mpt.PRNT_PRF_ID		IN	(2,16,100)))
									and sm.SM_ID = lp.SM_ID



							)
		
	UNION 
	SELECT DISTINCT		od.ORDR_ID								AS	ORDR_ID
						,CONVERT(VARCHAR,od.ORDR_ID)			AS	FTN
						,lp.PROD_TYPE_DES						AS	PROD_TYPE
						,lot.ORDR_TYPE_DES						AS  ORDR_TYPE
						,lt.PLTFRM_NME							AS	PLTFM_TYPE
						,ISNULL(lc.CTRY_NME, '''')				AS	COUNTRY
						,ISNULL(lu.DSPL_NME, '''')				AS	ASSIGNED_USER
						,ISNULL(lw.DSPL_NME, '''')				AS	ASSIGNED_BY
						,wu.ASMT_DT								AS	ASSIGN_DT
						,ISNULL(om.SBMT_DT, '''')				AS	SUBMIT_DT
						,ISNULL(lx.RGN_DES, '''')					AS	REGION
						,Case	od.CSG_LVL_ID
							When	0	Then	hf.CUST_NME
							Else	''Private Customer''
						End										AS	CUST_NME
						,''''									AS	CUST_ADDR
						,ncco.SITE_CITY_NME						AS	CITY
						,Case	ISNULL(ts.TASK_ID, 0)
							When	0	Then		''''
							Else	''Yes''
						End										AS	RTS
						,od.CSG_LVL_ID							AS	CSG_LVL_ID                                       
		FROM		dbo.ORDR			od	 WITH (NOLOCK)	
		INNER JOIN dbo.ORDR_MS om with (nolock) ON od.ORDR_ID = om.ORDR_ID
		INNER JOIN	dbo.NCCO_ORDR		ncco WITH (NOLOCK)	ON	od.ORDR_ID		=	ncco.ORDR_ID
															AND	od.ORDR_STUS_ID	IN	(0, 1)
		INNER JOIN	dbo.LK_PROD_TYPE	lp	WITH (NOLOCK)	ON	ncco.PROD_TYPE_ID	=	lp.PROD_TYPE_ID
		INNER JOIN	dbo.LK_XNCI_RGN		lx	WITH (NOLOCK)	ON	lx.RGN_ID		=	od.RGN_ID
		INNER JOIN	dbo.LK_CTRY			lc	WITH (NOLOCK)	ON	ncco.CTRY_CD	=	lc.CTRY_CD
		INNER JOIN dbo.LK_ORDR_TYPE	lot WITH (NOLOCK)	ON  lot.ORDR_TYPE_ID = ncco.ORDR_TYPE_ID
		LEFT JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID		=	at.ORDR_ID
															AND	at.STUS_ID		=	0
		LEFT JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID		=	mt.TASK_ID
															AND	mt.GRP_ID		IN	(5, 6, 7)	
		LEFT JOIN	dbo.MAP_PRF_TASK	mpt	WITH (NOLOCK)	ON	at.TASK_ID		=	mpt.TASK_ID
															AND	mpt.PRNT_PRF_ID		IN	(2,16,100)
		LEFT JOIN	dbo.LK_PLTFRM		lt	WITH (NOLOCK)	ON	lt.PLTFRM_CD	=	od.PLTFRM_CD
		LEFT JOIN	dbo.H5_FOLDR		hf	WITH (NOLOCK)	ON	od.H5_FOLDR_ID	=	hf.H5_FOLDR_ID
		LEFT JOIN	dbo.ACT_TASK		ts	WITH (NOLOCK)	ON	od.ORDR_ID		=	ts.ORDR_ID
															AND	ts.TASK_ID		=	500
															AND	ts.STUS_ID		=	0	
		LEFT JOIN	dbo.USER_WFM_ASMT	wu	WITH (NOLOCK)	ON	wu.ORDR_ID		=	od.ORDR_ID
															AND	wu.GRP_ID		IN	(5, 6, 7)
		LEFT JOIN	dbo.LK_USER			lu	WITH (NOLOCK)	ON	wu.ASN_USER_ID	  =	lu.USER_ID
		LEFT JOIN	dbo.LK_USER			lw	WITH (NOLOCK)	ON	wu.ASN_BY_USER_ID =	lw.USER_ID	'		
		
			
	DECLARE @IsSQLSelect BIT = 0 

	IF	@Filter	!=	''
		BEGIN
			SET @SQL = @SQL + ' SELECT * FROM #XNCIWFMTmp WITH (NOLOCK) ' + ' WHERE ' + @Filter + ' '
			SET @IsSQLSelect = 1
		END
	
	IF @IsSQLSelect = 1
		BEGIN
			IF	@Sort	!=	''
				SET @SQL = @SQL + ' ORDER BY ' + @Sort + ' '
			ELSE
				SET @SQL = @SQL + ' ORDER BY ASSIGNED_USER, ORDR_ID'
		END
	ELSE
		BEGIN
			IF	@Sort	!=	''
				SET @SQL = @SQL + ' SELECT * FROM #XNCIWFMTmp WITH (NOLOCK) ' + ' ORDER BY ' + @Sort + ' '
			ELSE
				SET @SQL = @SQL + ' SELECT * FROM #XNCIWFMTmp WITH (NOLOCK) ' + ' ORDER BY ASSIGNED_USER, ORDR_ID'
		END
	
	
	IF @rgn != ''
		BEGIN
			SET @SQL =  @SQL + '	SELECT COUNT(1) AS CNT,  ASSIGNED_USER, CONCAT(ASSIGNED_USER, '' ('', COUNT(1), '')'') as ASSIGNED_USER_CNT
						FROM #XNCIWFMTmp t WITH (NOLOCK) INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON t.ASSIGNED_USER = lu.DSPL_NME ' + ' WHERE ' + @rgn +  
						' GROUP BY ASSIGNED_USER' +	
							' ORDER BY ASSIGNED_USER '	
		END
	ELSE
		BEGIN
			SET @SQL =  @SQL + '	SELECT COUNT(1) AS CNT,  ASSIGNED_USER, CONCAT(ASSIGNED_USER, '' ('', COUNT(1), '')'') as ASSIGNED_USER_CNT
						FROM #XNCIWFMTmp t WITH (NOLOCK) INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON t.ASSIGNED_USER = lu.DSPL_NME  
						GROUP BY ASSIGNED_USER	
							ORDER BY ASSIGNED_USER '	
		END
	

	--SELECT @SQL
	EXEC sp_executesql @SQL
	
	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END