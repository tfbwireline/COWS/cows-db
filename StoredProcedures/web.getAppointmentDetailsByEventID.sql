USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getAppointmentDetailsByEventID]    Script Date: 1/18/2022 2:49:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/03/2011
-- Description:	Get an Appointment Info by @EVENT_ID
-- 01/11/2022 - jbolano15 - add USER_ID of the assigned user to the result
-- =========================================================
ALTER PROCEDURE [web].[getAppointmentDetailsByEventID]
	@EVENT_ID	Int
	,@APPT_ID	Int	=	0
AS
BEGIN
SET NOCOUNT ON;
	Begin Try
		--SELECT ap.SUBJ_TXT,
		--	ap.APPT_LOC_TXT,
		--	ap.[DES] AS EVENT_DES,
		--	ap.STRT_TMST,
		--	ap.END_TMST,
		--	la.APPT_TYPE_DES,
		--	ap.ASN_TO_USER_ID_LIST_TXT
		--FROM dbo.APPT ap WITH (NOLOCK)
		--	INNER JOIN dbo.LK_APPT_TYPE la WITH (NOLOCK) ON ap.APPT_TYPE_ID = la.APPT_TYPE_ID
		--WHERE 
		--	ap.EVENT_ID = @EVENT_ID
		--	OR (ap.APPT_ID = @APPT_ID AND @APPT_ID != 0)
				
		DECLARE @XML NVARCHAR(MAX) = ''
		DECLARE @hdoc int
		
		SELECT @XML = ASN_TO_USER_ID_LIST_TXT
			FROM		dbo.APPT			ap	WITH (NOLOCK)
			WHERE		EVENT_ID	=	@EVENT_ID	
				OR		(APPT_ID	=	@APPT_ID	AND	@APPT_ID	!=	0)
		--SELECT @XML
		EXEC sp_xml_preparedocument @hdoc OUTPUT, @XML

		SELECT u.USER_ID, u.EMAIL_ADR,
			ap.SUBJ_TXT,
			ap.APPT_LOC_TXT,
			ap.[DES] AS EVENT_DES,
			ap.STRT_TMST,
			ap.END_TMST,
			la.APPT_TYPE_DES
		FROM  (SELECT [Value] AS Id
			FROM OPENXML (@hdoc, '/ResourceIds/ResourceId' , 1)
			WITH([Value] INT)) a
			INNER JOIN dbo.LK_USER u WITH (NOLOCK) ON u.USER_ID = a.Id
			INNER JOIN dbo.APPT ap WITH (NOLOCK) ON ap.ASN_TO_USER_ID_LIST_TXT LIKE '%' + CONVERT(VARCHAR, u.USER_ID) + '%' AND (ap.EVENT_ID = @EVENT_ID OR (APPT_ID = @APPT_ID AND @APPT_ID != 0))
			INNER JOIN dbo.LK_APPT_TYPE la WITH (NOLOCK) ON ap.APPT_TYPE_ID = la.APPT_TYPE_ID
	End Try
Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END