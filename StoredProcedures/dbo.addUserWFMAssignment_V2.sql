USE [COWS]
GO
_CreateObject 'SP'
	,'dbo'
	,'addUserWFMAssignment_V2'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Kyle Wichert
-- Create date: 08/17/2011
-- Description:	Add/Update entries into dbo.USER_WFM in bulk form
-- kh946640: (05022019) Updated SP adding new paramenter @IsNewUI to support bothe Old and New COWS applications.
-- kh946640: (09302020) Updated SP to update REC_STUS_ID = 0 on all other WFM Profiles other than the @usrPrfId passed.
-- ================================================================

ALTER PROCEDURE [dbo].[addUserWFMAssignment_V2]
	@userID INT,
	@groupID SMALLINT,
	@roleIDs VARCHAR(MAX),
	@orderActionIDs VARCHAR(MAX),
	@prodPlatIDs VARCHAR(MAX),
	@wfmLevel TINYINT,
	@countryOrigList VARCHAR(MAX),
	@modUSERID	INT, 
	@usrPrfId INT = 0,
	@IsNewUI BIT = 0
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		DECLARE @roles TABLE (RoleID INT)
		DECLARE @orderActions TABLE (OrderActionID INT)
		DECLARE @tempProdPlat TABLE (tempString	VARCHAR(10), tempCtr Int IDENTITY(1, 1))
		DECLARE @prodplatTypes TABLE (ProductTypeID VARCHAR(3), PlatformCD VARCHAR(2))
		DECLARE @countryOrig TABLE (CountryCodeOrig VARCHAR(2))
		
		INSERT INTO @roles SELECT IntegerID FROM [web].[ParseCommaSeparatedIntegers](@roleIDs, ',')
		INSERT INTO @orderActions SELECT IntegerID FROM [web].[ParseCommaSeparatedIntegers](@orderActionIDs, ',')
		INSERT INTO @tempProdPlat SELECT StringID FROM [web].[ParseCommaSeparatedStrings](@prodPlatIDs)
		INSERT INTO @countryOrig SELECT StringID FROM [web].[ParseCommaSeparatedStrings](@countryOrigList)

		DECLARE @cnt INT
		DECLARE @ctr INT
		DECLARE @prodTypeID INT
		DECLARE @platCode	VARCHAR(2)
		
		SET @ctr = 1
		SELECT @cnt = COUNT(1) FROM @tempProdPlat

		WHILE @ctr <= @cnt
		BEGIN
			SET @prodTypeID = 0
			SET @platCode = NULL

			SELECT	@prodTypeID = Convert(INT,LTRIM(RTRIM(SUBSTRING(tempString, 0, CHARINDEX('-',tempString))))),
					@platCode	= LTRIM(RTRIM(SUBSTRING(tempString + '  ',CHARINDEx('-',tempString)+1, 10)))
			FROM	@tempProdPlat t
			WHERE	t.tempCtr	= @ctr

			INSERT INTO @prodplatTypes 
			SELECT @prodTypeID, @platCode

			SET @ctr = @ctr + 1
		END

		UPDATE	@prodplatTypes	SET		PlatformCD	=	NULL	WHERE	ISNULL(PlatformCD,'')	= ''
		
		--Vendor and Countries are not required.  If the counts are zero, insert a null to allow the join to work
		IF (SELECT COUNT(CountryCodeOrig) FROM @countryOrig) < 1
		BEGIN
			INSERT INTO @countryOrig SELECT NULL
		END
		
		--IPL Workaround - Only displaying one IPL field for user to select, but we need to assign all IPL types.
		IF EXISTS (SELECT 'X' FROM @prodplatTypes WHERE ProductTypeID = '0' AND PlatformCD = '0')
		BEGIN
			INSERT INTO @prodplatTypes
			SELECT PROD_TYPE_ID, NULL
			FROM dbo.LK_PROD_TYPE WITH (NOLOCK)
			WHERE ORDR_CAT_ID IN (1,5)

			DELETE FROM @prodplatTypes WHERE ProductTypeID = '0' AND PlatformCD = '0'
		END

		--NCCO
		IF EXISTS (SELECT 'X' FROM @prodplatTypes WHERE ProductTypeID = '0' AND PlatformCD = '00')
		BEGIN
			INSERT INTO @prodplatTypes
			SELECT PROD_TYPE_ID, NULL
			FROM dbo.LK_PROD_TYPE WITH (NOLOCK)
			WHERE ORDR_CAT_ID = 4

			DELETE FROM @prodplatTypes WHERE ProductTypeID = '0' AND PlatformCD = '00'
		END


		--Update records that already exist if the status != 1
		IF @IsNewUI = 1
			BEGIN
			--Updating the REC_STUS_ID = 0 on all other WFM Profiles other than the @usrPrfId passed.
			UPDATE w
				SET		w.REC_STUS_ID		= 0,
						w.MODFD_BY_USER_ID	= @modUSERID,
						w.MODFD_DT			= GETDATE()
				FROM dbo.USER_WFM w WITH (ROWLOCK)	
				WHERE	w.USER_ID		=	@userID 
					AND w.USR_PRF_ID	<>	@usrPrfId
					AND	w.REC_STUS_ID	=	1	


				UPDATE w
				SET		w.REC_STUS_ID		= 1,
						w.MODFD_BY_USER_ID	= @modUSERID,
						w.MODFD_DT			= GETDATE()
				FROM			@roles r
					INNER JOIN	@orderActions	oa				on 1=1
					INNER JOIN	@prodplatTypes	ppd				on 1=1
					INNER JOIN	@countryOrig	co				on 1=1
					INNER JOIN	dbo.USER_WFM w WITH (ROWLOCK)	on	w.ROLE_ID						=	r.RoleID
																AND w.ORDR_ACTN_ID					=	oa.OrderActionID
																AND w.PROD_TYPE_ID					=	ppd.ProductTypeID
																AND ISNULL(w.PLTFRM_CD, '')			=	ISNULL(ppd.PlatformCD, '')
																AND ISNULL(w.ORGTNG_CTRY_CD,'')		=	ISNULL(co.CountryCodeOrig, '')
																AND w.GRP_ID						=	@groupID
																AND w.USR_PRF_ID					=	@usrPrfId
				WHERE	w.USER_ID		=	@userID
					AND	w.REC_STUS_ID	<>	1				
		
			END

		ELSE
			BEGIN
				UPDATE w
				SET		w.REC_STUS_ID		= 1,
						w.MODFD_BY_USER_ID	= @modUSERID,
						w.MODFD_DT			= GETDATE()
				FROM			@roles r
					INNER JOIN	@orderActions	oa				on 1=1
					INNER JOIN	@prodplatTypes	ppd				on 1=1
					INNER JOIN	@countryOrig	co				on 1=1
					INNER JOIN	dbo.USER_WFM w WITH (ROWLOCK)	on	w.ROLE_ID						=	r.RoleID
																AND w.ORDR_ACTN_ID					=	oa.OrderActionID
																AND w.PROD_TYPE_ID					=	ppd.ProductTypeID
																AND ISNULL(w.PLTFRM_CD, '')			=	ISNULL(ppd.PlatformCD, '')
																AND ISNULL(w.ORGTNG_CTRY_CD,'')		=	ISNULL(co.CountryCodeOrig, '')
																AND w.GRP_ID						=	@groupID
				WHERE	w.USER_ID		=	@userID
					AND	w.REC_STUS_ID	<>	1
			END
		

		--Insert records that don't already exist
		IF @IsNewUI = 1
			BEGIN
				INSERT INTO dbo.USER_WFM	(USER_ID,	GRP_ID,		ROLE_ID,	ORDR_TYPE_ID,	ORDR_ACTN_ID,		PROD_TYPE_ID,		PLTFRM_CD,		VNDR_CD,	ORGTNG_CTRY_CD,		IPL_TRMTG_CTRY_CD,	WFM_ASMT_LVL_ID,	REC_STUS_ID,	CREAT_BY_USER_ID,	CREAT_DT, USR_PRF_ID) 
		SELECT						@userID,	@groupID,	r.RoleID,	1,				oa.OrderActionID,	ppd.ProductTypeID,	ppd.PlatformCD,	NULL,		co.CountryCodeOrig,	NULL,				@wfmLevel,			1,				@modUSERID,			GETDATE(),	 @usrPrfId 
		FROM			@roles r
			INNER JOIN	@orderActions	oa				on 1=1
			INNER JOIN	@prodplatTypes	ppd				on 1=1
			INNER JOIN	@countryOrig	co				on 1=1
			LEFT JOIN	dbo.USER_WFM w WITH (NOLOCK)	on	w.ROLE_ID						=	r.RoleID
														AND w.ORDR_ACTN_ID					=	oa.OrderActionID
														AND w.PROD_TYPE_ID					=	ppd.ProductTypeID
														AND ISNULL(w.PLTFRM_CD, '')			=	ISNULL(ppd.PlatformCD, '')
														AND ISNULL(w.ORGTNG_CTRY_CD,'')		=	ISNULL(co.CountryCodeOrig, '')
														AND w.USER_ID						=	@userID
														AND w.GRP_ID						=	@groupID
														and W.USR_PRF_ID					=	@usrPrfId
		WHERE ISNULL(w.[USER_ID],'') = ''
			END
		ELSE
			BEGIN
				INSERT INTO dbo.USER_WFM	(USER_ID,	GRP_ID,		ROLE_ID,	ORDR_TYPE_ID,	ORDR_ACTN_ID,		PROD_TYPE_ID,		PLTFRM_CD,		VNDR_CD,	ORGTNG_CTRY_CD,		IPL_TRMTG_CTRY_CD,	WFM_ASMT_LVL_ID,	REC_STUS_ID,	CREAT_BY_USER_ID,	CREAT_DT) 
		SELECT						@userID,	@groupID,	r.RoleID,	1,				oa.OrderActionID,	ppd.ProductTypeID,	ppd.PlatformCD,	NULL,		co.CountryCodeOrig,	NULL,				@wfmLevel,			1,				@modUSERID,			GETDATE() 
		FROM			@roles r
			INNER JOIN	@orderActions	oa				on 1=1
			INNER JOIN	@prodplatTypes	ppd				on 1=1
			INNER JOIN	@countryOrig	co				on 1=1
			LEFT JOIN	dbo.USER_WFM w WITH (NOLOCK)	on	w.ROLE_ID						=	r.RoleID
														AND w.ORDR_ACTN_ID					=	oa.OrderActionID
														AND w.PROD_TYPE_ID					=	ppd.ProductTypeID
														AND ISNULL(w.PLTFRM_CD, '')			=	ISNULL(ppd.PlatformCD, '')
														AND ISNULL(w.ORGTNG_CTRY_CD,'')		=	ISNULL(co.CountryCodeOrig, '')
														AND w.USER_ID						=	@userID
														AND w.GRP_ID						=	@groupID
		WHERE ISNULL(w.[USER_ID],'') = ''
			END
		

		--Update User Order Count		
		DECLARE @UserListwCnt TABLE (UserID INT, Cnt INT)

			INSERT INTO @UserListwCnt
				select x.[USER_ID], SUM(x.Cnt)
				FROM (
				select [USER_ID], COUNT(*) as Cnt
				from dbo.USER_WFM with (nolock)
				where REC_STUS_ID=1
				group by [USER_ID], PROD_TYPE_ID, PLTFRM_CD, ORGTNG_CTRY_CD, ORDR_ACTN_ID) as x
				group by x.[USER_ID]
				order by x.[USER_ID] desc
				
			
		    DECLARE @CrntUserTbl TABLE (UserID INT, Cnt INT)

			INSERT INTO @CrntUserTbl
			SELECT @userID, COUNT(1)
			FROM (SELECT	distinct PROD_TYPE_ID,	PLTFRM_CD,	ORGTNG_CTRY_CD, ORDR_ACTN_ID
			FROM dbo.USER_WFM WITH (NOLOCK)
			WHERE [USER_ID] = @userID) as y

			
			DELETE from @UserListwCnt
			 WHERE Cnt <> (SELECT Cnt FROM @CrntUserTbl)
			 
			 DELETE FROM @UserListwCnt
			 WHERE UserID in (SELECT uw.[USER_ID] 
							  FROM dbo.USER_WFM uw WITH (NOLOCK) inner join	
								   @UserListwCnt uc on uc.[UserID] = uw.[USER_ID]
							  WHERE uw.REC_STUS_ID = 1
							    and ((uw.PROD_TYPE_ID 
									not in (SELECT uw2.PROD_TYPE_ID
											FROM dbo.USER_WFM uw2 WITH (NOLOCK)
										    WHERE uw2.[USER_ID] = @userID
										      AND uw2.REC_STUS_ID = 1))
								or (uw.PLTFRM_CD 
									not in (SELECT uw2.PLTFRM_CD
											FROM dbo.USER_WFM uw2 WITH (NOLOCK)
										    WHERE uw2.[USER_ID] = @userID
											  AND uw2.REC_STUS_ID = 1))
								or (uw.ORGTNG_CTRY_CD 
									not in (SELECT uw2.ORGTNG_CTRY_CD
											FROM dbo.USER_WFM uw2 WITH (NOLOCK)
										    WHERE uw2.[USER_ID] = @userID
											  AND uw2.REC_STUS_ID = 1))
								or (uw.ORDR_ACTN_ID 
									not in (SELECT uw2.ORDR_ACTN_ID
											FROM dbo.USER_WFM uw2 WITH (NOLOCK)
										    WHERE uw2.[USER_ID] = @userID
											  AND uw2.REC_STUS_ID = 1))))

			
			DECLARE @OrderCnt DECIMAL(5,2)
			SET @OrderCnt = 0.00

			SELECT @OrderCnt = MIN(USER_ORDR_CNT)
			FROM dbo.LK_USER lu WITH (NOLOCK) INNER JOIN
				 @UserListwCnt ul ON ul.UserID = lu.[USER_ID] INNER JOIN
				 @CrntUserTbl cu ON ul.Cnt = cu.Cnt
			WHERE ul.UserID <> (SELECT [UserID] FROM @CrntUserTbl)
			  AND lu.USER_ORDR_CNT <> 0

			
			UPDATE dbo.LK_USER WITH (ROWLOCK) 
			SET USER_ORDR_CNT=CASE WHEN (COALESCE(@OrderCnt, 0) <= 0) THEN 0
									ELSE @OrderCnt END 
									WHERE [USER_ID] = @userID

	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	
END
