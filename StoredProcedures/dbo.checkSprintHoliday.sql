USE [COWS]
GO
_CreateObject 'SP','dbo','checkSprintHoliday'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:		sbg9814
-- Create date: 09/27/2011
-- Description:	Checks to see if the selected Date is a Sprint Holiday or NOT.
-- Updated by: Sarah Sandoval
-- Update Note: Added an ELSE condition to return @ID = 0 and not NULL
-- =============================================================================
ALTER PROCEDURE [dbo].[checkSprintHoliday]
	@DAY_DT		DateTime
	,@ID		Int	OUTPUT
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	DECLARE	@Date	Varchar(20)
	SET		@Date	=	Convert(Varchar, @DAY_DT,	101)

	IF	EXISTS
		(SELECT 'X'	FROM	dbo.LK_SPRINT_HLDY	WITH (NOLOCK)
					WHERE	Convert(Varchar, SPRINT_HLDY_DT,	101)	=	@Date)
		BEGIN		
			SET	@ID	=	1
		END

	ELSE	SET	@ID	=	0
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO
