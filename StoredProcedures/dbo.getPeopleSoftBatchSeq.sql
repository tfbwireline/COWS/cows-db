USE [COWS]
GO
_CreateObject 'SP','dbo','getPeopleSoftBatchSeq'
GO
/****** Object:  StoredProcedure [dbo].[getPeopleSoftBatchSeq]    Script Date: 10/01/2015 11:00:28 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 08/11/2015
-- Description:	Gets last File Sequence for PeopleSoft Batch process.
-- =========================================================
ALTER PROCEDURE [dbo].[getPeopleSoftBatchSeq]
		@Type  Varchar (1)
        
AS
BEGIN TRY

	IF @Type = 'M'
		BEGIN
					
			SELECT 	TOP 1 ISNULL(BATCH_SEQ,0) AS BATCH_SEQ 
				FROM dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK) ORDER BY BATCH_SEQ DESC
		END
	ELSE
		IF @TYPE = 'R'
		BEGIN
			SELECT 	TOP 1 ISNULL(BATCH_SEQ,0) AS BATCH_SEQ 
				FROM dbo.PS_RCPT_QUEUE WITH (NOLOCK) ORDER BY BATCH_SEQ DESC
		END
	
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH


GO


