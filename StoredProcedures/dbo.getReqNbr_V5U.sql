USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getReqNbr_V5U]    Script Date: 03/18/2016 10:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 06/24/2015
-- Description:	Determines Next Requisition Number for the Order Id.
-- =========================================================
Create PROCEDURE [dbo].[getReqNbr_V5U] 
	   @ORDR_ID int
           
AS
BEGIN
SET NOCOUNT ON;

Declare @prevNbr varchar (10) = null,
		@newReqNbr  varchar(10) = null,
		@tempNbr int


BEGIN TRY

	IF EXISTS
			(
				SELECT 'X'
					FROM	dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK)
					WHERE	ORDR_ID =	@ORDR_ID
					
			)
			BEGIN
				SELECT TOP 1 @prevNbr = REQSTN_NBR 
					FROM dbo.PS_REQ_LINE_ITM_QUEUE 
						WHERE ORDR_ID = @ORDR_ID
					ORDER BY REQ_LINE_ITM_ID DESC
					
				SET @tempNbr = right(@prevNbr,1)
				SET @tempNbr = @tempNbr + 1
				SET @newReqNbr = Left(@prevNbr, 8) + convert(varchar(2),@tempNbr)					
			END
		ELSE
			BEGIN
				SELECT @prevNbr = right(FTN,7) 
					FROM dbo.FSA_ORDR 
						WHERE ORDR_ID = @ORDR_ID
										
					set @tempNbr = 1
					
					IF EXISTS (SELECT * FROM ORDR WITH (NOLOCK) 
									WHERE ORDR_ID = @ORDR_ID AND DMSTC_CD = 1)
						BEGIN
							SET @newReqNbr = 'I' + @prevNbr  + convert(varchar(1),@tempNbr)
							
							WHILE EXISTS (SELECT 'X' FROM	dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK)
								WHERE	REQSTN_NBR =	@newReqNbr)
									BEGIN
									  SET @tempNbr = @tempNbr + 1
									  SET @newReqNbr = 'I' + right(@prevNbr,7)  + convert(varchar(2),@tempNbr)
									END
						END
					ELSE 
						BEGIN
							IF EXISTS (SELECT * FROM ORDR WITH (NOLOCK)
										WHERE ORDR_ID = @ORDR_ID 
											AND PROD_ID IN ('UCCH','UCSV','MIPT','UCSM'))
							  BEGIN
									SET @newReqNbr = 'V' + @prevNbr  + convert(varchar(1),@tempNbr)
									WHILE EXISTS (SELECT 'X' FROM	dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK)
													WHERE	REQSTN_NBR =	@newReqNbr)
										BEGIN
										  SET @tempNbr = @tempNbr + 1
										  SET @newReqNbr = 'V' + right(@prevNbr,7)  + convert(varchar(2),@tempNbr)
										END
					
							  END
							ELSE
								BEGIN  
									SET @newReqNbr = 'D' + @prevNbr  + convert(varchar(1),@tempNbr)
									WHILE EXISTS (SELECT 'X' FROM	dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK)
													WHERE	REQSTN_NBR =	@newReqNbr)
										BEGIN
										  SET @tempNbr = @tempNbr + 1
										  SET @newReqNbr = 'D' + right(@prevNbr,7)  + convert(varchar(2),@tempNbr)
										END
								END
						END
			END

	select @newReqNbr as [newReqNbr]


		
		
END TRY

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch

END

