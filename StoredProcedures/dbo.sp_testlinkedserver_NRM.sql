USE [COWS]
GO

/****** Object:  StoredProcedure [dbo].[sp_testlinkedserver_NRM]    Script Date: 02/20/2019 00:54:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_testlinkedserver_NRM]
AS
 BEGIN TRY
 EXEC sp_testlinkedserver  @server=N'NRM'
 SELECT 'Active'
 END TRY
 
 BEGIN CATCH
 SELECT error_message()
 END CATCH
GO


