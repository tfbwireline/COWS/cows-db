USE [COWS]
GO
_CreateObject 'SP','dbo','insertEmailRequest'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sbg9814
-- Create date: 08/16/2011
-- Description:	Inserts an Email Request to be send.
-- =============================================
ALTER PROCEDURE [dbo].[insertEmailRequest] 
	@ORDR_ID			Int
	,@EMAIL_REQ_TYPE_ID	Int
	,@STUS_ID			Int
	,@EVENT_ID			Int				=	NULL
	,@EMAIL_LIST_TXT	Varchar(Max)	=	NULL	

AS
BEGIN
Begin Try
	DECLARE	@EMAIL_REQ_ID	Int
	SET	@EMAIL_REQ_ID	=	0
	
	INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
				(ORDR_ID
				,EMAIL_REQ_TYPE_ID
				,STUS_ID
				,EVENT_ID
				,EMAIL_LIST_TXT)
		VALUES	(@ORDR_ID
				,@EMAIL_REQ_TYPE_ID
				,Case	ISNULL(@EMAIL_LIST_TXT, '')
					When	''	Then	9				
					Else	10
				End
				,@EVENT_ID
				,@EMAIL_LIST_TXT)
				
	SELECT	@EMAIL_REQ_ID	=	SCOPE_IDENTITY()			
				
	IF	ISNULL(@EMAIL_LIST_TXT, '')	=	''	AND	@ORDR_ID	!=	0
		BEGIN
			EXEC	[dbo].[loadEmailAddresses] 
					@ORDR_ID
					,@EMAIL_REQ_TYPE_ID
					,@EMAIL_REQ_ID
		END			
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO