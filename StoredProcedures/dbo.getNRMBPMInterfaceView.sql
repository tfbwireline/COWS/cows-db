USE [COWS]
GO
_CreateObject 'SP','dbo','getNRMBPMInterfaceView'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <03/16/2016>
-- Description:	<to get NRMBPM View orders>
-- =============================================
ALTER PROCEDURE [dbo].[getNRMBPMInterfaceView]
	@M5OrderNbr VARCHAR(20) = ''
AS

BEGIN
	SET NOCOUNT ON;
	DECLARE @now Varchar(23)
	SET @now = REPLACE(CONVERT(char(30), GETDATE() - 10,126),'T',' ')
	
	BEGIN TRY
		DECLARE @SQL nVarchar(max) = ''
		DECLARE @M5_ORDR_NBR_TBL TABLE (ORDER_NBR Varchar(20), ORDR_ID INT)	
		SET @M5OrderNbr = LTRIM(RTRIM(@M5OrderNbr))
		SET @SQL = ''
		SET @SQL = 'Select top 2000 * From OpenQuery(m5,''Select ORDER_NBR, ORDR_ID From MACH5.V_V5U_ORDR WHERE ORDER_NBR Like UPPER(''''%' + @M5OrderNbr + '%'''') ORDER BY CREAT_DT DESC'')'
		--PRINT @SQL
		INSERT INTO @M5_ORDR_NBR_TBL
		EXEC sp_executeSQL @SQL			
		
		SET @SQL = ''
		SET @SQL = 'Select top 2000 * From OpenQuery(m5,''SELECT CHNG_TXN_NBR, CHNG_TXN_ID FROM MACH5.V_V5U_CHNG_TXN WHERE CHNG_TXN_NBR Like UPPER(''''%' + @M5OrderNbr + '%'''') ORDER BY CREAT_DT DESC'')'
																	
		INSERT INTO @M5_ORDR_NBR_TBL
		EXEC sp_executeSQL @SQL			
		
		--Select * From @M5_ORDR_NBR_TBL
		DECLARE @ORDR_IDs VARCHAR(MAX) = ''
		SELECT @ORDR_IDs = COALESCE(CONVERT(VARCHAR,ORDR_ID),@ORDR_IDs) + ',' + @ORDR_IDs
			FROM @M5_ORDR_NBR_TBL 
			
		DECLARE @NRM_BPM_VIEW TABLE (M5_ORDR_ID INT, RLTD_ORDR_ID INT, TYPE_NME VARCHAR(5), 
										SUB_TYPE_NME VARCHAR(50), PROD_CD VARCHAR(100),XTRCT_CD VARCHAR(1),
										XTRCT_DT SMALLDATETIME, CPE_DEVICE_ID VARCHAR(100), M5_ORDER_NBR Varchar(20),
										M5_RLTD_ORDR_NBR Varchar(20)
									)
										
IF ((LEN(@M5OrderNbr)>0) AND (LEN(@ORDR_IDs) > 0))
	BEGIN 		
										
		SET @SQL = 'SELECT * FROM OpenQuery(NRMBPM,''SELECT ORDR_ID, RELTD_ORDR_ID, TYPE_NME ,SUB_TYPE_NME,
															PROD_CD, XTRCT_CD, XTRCT_DT, CPE_DEV_ID
														FROM BPMF_OWNER.BPM_COWS_NRFC 
														WHERE ORDR_ID IN (' + LEFT(@ORDR_IDs,DATALENGTH(@ORDR_IDs)-1) + ')														
														ORDER BY ORDR_ID DESC'')'
	END	
ELSE
	BEGIN 									
										
		SET @SQL = 'SELECT * FROM OpenQuery(NRMBPM,''SELECT * FROM (SELECT ORDR_ID, RELTD_ORDR_ID, TYPE_NME ,SUB_TYPE_NME,
															PROD_CD, XTRCT_CD, XTRCT_DT, CPE_DEV_ID
														FROM BPMF_OWNER.BPM_COWS_NRFC												
														ORDER BY ORDR_ID DESC) WHERE ROWNUM<=2000 '')'
	END	
														
		
		--Select @SQL
		
		
		INSERT INTO @NRM_BPM_VIEW (M5_ORDR_ID, RLTD_ORDR_ID, TYPE_NME ,SUB_TYPE_NME,
															PROD_CD, XTRCT_CD, XTRCT_DT, 
															CPE_DEVICE_ID)
		EXEC sp_executeSQL @SQL
		
		
		UPDATE t
			SET M5_ORDER_NBR = m.ORDER_NBR,
				M5_RLTD_ORDR_NBR = mr.ORDER_NBR
			FROM 	@NRM_BPM_VIEW t
			LEFT JOIN @M5_ORDR_NBR_TBL m ON t.M5_ORDR_ID = m.ORDR_ID
			LEFT JOIN @M5_ORDR_NBR_TBL mr ON t.RLTD_ORDR_ID = mr.ORDR_ID
		
		Select CPE_DEVICE_ID AS CPE_DEVICE_ID,
				M5_ORDER_NBR AS M5_ORDR_NBR,
				M5_RLTD_ORDR_NBR AS M5_RLTD_ORDR_NBR,
				SUB_TYPE_NME AS SUB_TYPE_NME,
				TYPE_NME AS TYPE_NME,
				PROD_CD AS PROD_CD,
				XTRCT_CD AS XTRCT_CD,
				XTRCT_DT AS XTRCT_DT
			FROM @NRM_BPM_VIEW
			ORDER BY XTRCT_DT DESC
		
		
    END TRY
    
    BEGIN CATCH
		EXEC dbo.insertErrorInfo;
    END CATCH
END
