USE COWS
GO
_CreateObject 'SP','dbo','insertUpdateGOMInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Created By:		sbg9814
-- Created Date:	08/24/2011
-- Description:		Insert the GOM Info for a given Orders.
-- ===========================================================================       
ALTER PROCEDURE [dbo].[insertUpdateGOMInfo]
	@OrderID						Int
	,@SHPMT_TRK_NBR					Varchar(50)
	,@VNDR_SHIP_DT					SmallDateTime
	,@DRTBN_SHIP_DT					SmallDateTime
	,@CUST_DLVRY_DT					SmallDateTime
	,@CSTM_DT						SmallDateTime
	,@GRP_ID						Int
	,@USER_ID						Int
	-- New fields added in UAT for both GOM & xNCI.
	,@HDWR_DLVRD_DT					SmallDateTime	=	NULL
	,@EQPT_INSTL_DT					SmallDateTime	=	NULL
	,@CPE_STUS_ID					SMALLINT		=	NULL
	,@CPE_CMPLT_DT					SmallDateTime	=	NULL	
	,@ATLAS_WRK_ORDR_NBR			Varchar(15)		=	NULL
	,@PLSFT_RQSTN_NBR				Varchar(15)		=	NULL
	,@RQSTN_AMT						smallmoney		=	NULL	
	,@RQSTN_DT						SmallDateTime	=	NULL
	,@PRCH_ORDR_NBR					Varchar(15)		=	NULL
	,@PRCH_ORDR_BACK_ORDR_SHIP_DT	SmallDateTime	=	NULL
	,@VNDR_RECV_FROM_DT				SmallDateTime	=	NULL
	,@VNDR_COURIER_TRK_NBR			Varchar(50)		=	NULL
	,@PLSFT_RELS_DT					SmallDateTime	=	NULL
	,@PLSFT_RELS_ID					Varchar(15)		=	NULL
	,@PLSFT_RCPT_ID					Varchar(10)		=	NULL
	,@COURIER_TRK_NBR				Varchar(15)		=	NULL
	,@CPE_EQPT_TYPE_TXT				Varchar(30)		=	NULL
	,@CPE_VNDR_NME					Varchar(30)		=	NULL
	,@SHPMT_SIGN_BY_NME				Varchar(30)		=	NULL
	,@PLN_NME						Varchar(200)	=	NULL
	,@BILL_FTN						varchar(20)		=	NULL
	,@CUST_PRCH_CD					bit				=	NULL
	,@CUST_RNTL_CD					bit				=	NULL
	,@CUST_RNTL_TERM_NBR			smallint		=	NULL
	,@LOGICALIS_MNTH_LEASE_RT_AMT	money			=	NULL
	,@TERM_STRT_DT					smalldatetime	=	NULL
	,@TERM_END_DT					smalldatetime	=	NULL
	,@TERM_60_DAY_NTFCTN_DT			smalldatetime	=	NULL
	,@CUST_RNL_CD					bit				=	NULL
	,@CUST_RNL_TERM_NBR				smallint		=	NULL
	,@LOGICALIS_RNL_MNTH_LEASE_RT_AMT	money		=	NULL
	,@CUST_DSCNCT_CD				bit				=	NULL
	,@CUST_DSCNCT_DT				smalldatetime	=	NULL
AS      
BEGIN
SET DEADLOCK_PRIORITY 5
Begin Try
	IF	NOT EXISTS
		(SELECT 'X' FROM dbo.FSA_ORDR_GOM_XNCI	WITH (NOLOCK)
			WHERE	ORDR_ID	=	@OrderID
			AND		GRP_ID	=	@GRP_ID)
		BEGIN					
			INSERT INTO	dbo.FSA_ORDR_GOM_XNCI	WITH (ROWLOCK)
						(ORDR_ID
						,SHPMT_TRK_NBR
						,VNDR_SHIP_DT
						,DRTBN_SHIP_DT
						,CUST_DLVRY_DT
						,CSTM_DT
						,GRP_ID
						,CREAT_BY_USER_ID
						,HDWR_DLVRD_DT
						,EQPT_INSTL_DT
						,CPE_STUS_ID
						,CPE_CMPLT_DT
						,ATLAS_WRK_ORDR_NBR
						,PLSFT_RQSTN_NBR
						,RQSTN_AMT
						,RQSTN_DT
						,PRCH_ORDR_NBR
						,PRCH_ORDR_BACK_ORDR_SHIP_DT
						,VNDR_RECV_FROM_DT
						,VNDR_COURIER_TRK_NBR
						,PLSFT_RELS_DT
						,PLSFT_RELS_ID
						,PLSFT_RCPT_ID
						,COURIER_TRK_NBR
						,CPE_EQPT_TYPE_TXT
						,CPE_VNDR_NME
						,SHPMT_SIGN_BY_NME
						,PLN_NME
						,BILL_FTN
						,CUST_PRCH_CD
						,CUST_RNTL_CD
						,CUST_RNTL_TERM_NBR
						,LOGICALIS_MNTH_LEASE_RT_AMT
						,TERM_STRT_DT
						,TERM_END_DT
						,TERM_60_DAY_NTFCTN_DT
						,CUST_RNL_CD
						,CUST_RNL_TERM_NBR
						,LOGICALIS_RNL_MNTH_LEASE_RT_AMT
						,CUST_DSCNCT_CD
						,CUST_DSCNCT_DT)					
				VALUES	(@OrderID
						,@SHPMT_TRK_NBR
						,@VNDR_SHIP_DT
						,@DRTBN_SHIP_DT
						,@CUST_DLVRY_DT
						,@CSTM_DT
						,@GRP_ID
						,@USER_ID
						,@HDWR_DLVRD_DT
						,@EQPT_INSTL_DT
						,Case	@CPE_STUS_ID
							When	0	Then	NULL
							Else	@CPE_STUS_ID
						End
						,@CPE_CMPLT_DT
						,@ATLAS_WRK_ORDR_NBR
						,@PLSFT_RQSTN_NBR
						,@RQSTN_AMT
						,@RQSTN_DT
						,@PRCH_ORDR_NBR
						,@PRCH_ORDR_BACK_ORDR_SHIP_DT
						,@VNDR_RECV_FROM_DT
						,@VNDR_COURIER_TRK_NBR
						,@PLSFT_RELS_DT
						,@PLSFT_RELS_ID
						,@PLSFT_RCPT_ID
						,@COURIER_TRK_NBR
						,@CPE_EQPT_TYPE_TXT
						,@CPE_VNDR_NME
						,@SHPMT_SIGN_BY_NME
						,@PLN_NME
						,@BILL_FTN
						,@CUST_PRCH_CD
						,@CUST_RNTL_CD
						,@CUST_RNTL_TERM_NBR
						,@LOGICALIS_MNTH_LEASE_RT_AMT
						,@TERM_STRT_DT
						,@TERM_END_DT
						,@TERM_60_DAY_NTFCTN_DT
						,@CUST_RNL_CD
						,@CUST_RNL_TERM_NBR
						,@LOGICALIS_RNL_MNTH_LEASE_RT_AMT
						,@CUST_DSCNCT_CD
						,@CUST_DSCNCT_DT					
					)					
		END
	ELSE
		BEGIN
			UPDATE			dbo.FSA_ORDR_GOM_XNCI	WITH (ROWLOCK)
				SET			SHPMT_TRK_NBR					=	@SHPMT_TRK_NBR
							,VNDR_SHIP_DT					=	@VNDR_SHIP_DT
							,DRTBN_SHIP_DT					=	@DRTBN_SHIP_DT
							,CUST_DLVRY_DT					=	@CUST_DLVRY_DT
							,CSTM_DT						=	@CSTM_DT
							,MODFD_BY_USER_ID				=	@USER_ID
							,MODFD_DT						=	getDate()
							,HDWR_DLVRD_DT					=	@HDWR_DLVRD_DT
							,EQPT_INSTL_DT					=	@EQPT_INSTL_DT
							,CPE_STUS_ID					=	Case	@CPE_STUS_ID
																	When	0	Then	NULL
																	Else	@CPE_STUS_ID
																End
							,CPE_CMPLT_DT					=	@CPE_CMPLT_DT
							,ATLAS_WRK_ORDR_NBR				=	@ATLAS_WRK_ORDR_NBR
							,PLSFT_RQSTN_NBR				=	@PLSFT_RQSTN_NBR
							,RQSTN_AMT						=	@RQSTN_AMT
							,RQSTN_DT						=	@RQSTN_DT
							,PRCH_ORDR_NBR					=	@PRCH_ORDR_NBR
							,PRCH_ORDR_BACK_ORDR_SHIP_DT	=	@PRCH_ORDR_BACK_ORDR_SHIP_DT
							,VNDR_RECV_FROM_DT				=	@VNDR_RECV_FROM_DT
							,VNDR_COURIER_TRK_NBR			=	@VNDR_COURIER_TRK_NBR
							,PLSFT_RELS_DT					=	@PLSFT_RELS_DT
							,PLSFT_RELS_ID					=	@PLSFT_RELS_ID
							,PLSFT_RCPT_ID					=	@PLSFT_RCPT_ID
							,COURIER_TRK_NBR				=	@COURIER_TRK_NBR
							,CPE_EQPT_TYPE_TXT				=	@CPE_EQPT_TYPE_TXT
							,CPE_VNDR_NME					=	@CPE_VNDR_NME
							,SHPMT_SIGN_BY_NME				=	@SHPMT_SIGN_BY_NME
							,PLN_NME						=	@PLN_NME
							,BILL_FTN						=	@BILL_FTN
							,CUST_PRCH_CD					=	@CUST_PRCH_CD
							,CUST_RNTL_CD					=	@CUST_RNTL_CD
							,CUST_RNTL_TERM_NBR				=	@CUST_RNTL_TERM_NBR
							,LOGICALIS_MNTH_LEASE_RT_AMT	=	@LOGICALIS_MNTH_LEASE_RT_AMT
							,TERM_STRT_DT					=	@TERM_STRT_DT
							,TERM_END_DT					=	@TERM_END_DT
							,TERM_60_DAY_NTFCTN_DT			=	@TERM_60_DAY_NTFCTN_DT
							,CUST_RNL_CD					=	@CUST_RNL_CD
							,CUST_RNL_TERM_NBR				=	@CUST_RNL_TERM_NBR
							,LOGICALIS_RNL_MNTH_LEASE_RT_AMT=	@LOGICALIS_RNL_MNTH_LEASE_RT_AMT
							,CUST_DSCNCT_CD					=	@CUST_DSCNCT_CD
							,CUST_DSCNCT_DT					=	@CUST_DSCNCT_DT
				WHERE		ORDR_ID	=	@OrderID
				AND			GRP_ID	=	@GRP_ID
		END	
		
		IF ((ISNULL(@SHPMT_TRK_NBR,'') <> '') AND  (ISNULL(@VNDR_SHIP_DT,'') <> ''))
			BEGIN
				IF EXISTS
				(
					SELECT	'X'
						FROM	dbo.ORDR		o WITH (NOLOCK)
					Inner Join	dbo.FSA_ORDR	f WITH (NOLOCK) ON f.ORDR_ID = o.ORDR_ID
						WHERE	o.DMSTC_CD		=	1
							AND	f.PROD_TYPE_CD	=	'CP'
							AND	o.ORDR_ID		=	@OrderID
							AND EXISTS
								(
									SELECT 'X'
										FROM	dbo.LK_PPRT lp WITH (NOLOCK) 
									INNER JOIN	dbo.SM sm WITH (NOLOCK) ON sm.SM_ID = lp.SM_ID
									INNER JOIN	dbo.WG_PTRN_SM ptsm WITH (NOLOCK) ON ptsm.WG_PTRN_ID = sm.DESRD_WG_PTRN_ID
									INNER JOIN	dbo.WG_PROF_SM pfsm WITH (NOLOCK) ON pfsm.WG_PROF_ID = ptsm.DESRD_WG_PROF_ID
										WHERE	lp.PPRT_ID = o.PPRT_ID
											AND	pfsm.PRE_REQST_TASK_ID = 210	 
								)
							AND NOT EXISTS
								(
									SELECT 'X'
										FROM	dbo.ACT_TASK at WITH (NOLOCK)
										WHERE	at.ORDR_ID	=	o.ORDR_ID
											AND	at.TASK_ID	=	210
								)
				)
				BEGIN
					INSERT INTO dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
					VALUES (@OrderID,210,0,203,GETDATE())
				END
			END	
					
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo] 
End Catch		
END
GO