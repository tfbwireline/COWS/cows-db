﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getVendorFolders'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:        Lakshmi Kadiyala
-- Create date: 12th July 2011
-- Description:   Returns Vendor Folders matching input parameters
-- UPDATE - km967761 - 12/21/2020 - Updated to consider "VendorName - VendorCode" searching
-- UPDATE - km967761 - 01/15/2021 - Bug fix for search by country
-- ================================================================

ALTER PROCEDURE [dbo].[getVendorFolders]
      @VendorName VARCHAR(50),
      @CountryCode CHAR(2)    
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
      SELECT VF.[VNDR_FOLDR_ID]
              ,VF.[VNDR_CD]
              ,LV.[VNDR_NME]          
              ,VF.[CTRY_CD]
              ,LC.[CTRY_NME]
              ,(SELECT COUNT(*) FROM dbo.VNDR_TMPLT VT WITH (NOLOCK) WHERE VT.[VNDR_FOLDR_ID] = VF.[VNDR_FOLDR_ID]) VNDR_TMPLT_CNT
      FROM [dbo].[VNDR_FOLDR] VF WITH (NOLOCK)
      INNER JOIN [dbo].[LK_VNDR] LV WITH (NOLOCK) ON VF.[VNDR_CD] = LV.[VNDR_CD]
      LEFT OUTER JOIN [dbo].[LK_CTRY] LC WITH (NOLOCK) ON VF.[CTRY_CD] = LC.[CTRY_CD]
      WHERE 
            VF.REC_STUS_ID = 1 
            AND
            (
                  (LV.VNDR_CD = 'GENVEN')
                  OR
                  (VF.CTRY_CD = CASE WHEN ISNULL(LTRIM(RTRIM(@CountryCode)),'') = '' THEN VF.CTRY_CD
                  ELSE @CountryCode END)
            )
            AND
            CONCAT(LV.VNDR_NME, ' - ', VF.VNDR_CD) LIKE CASE WHEN ISNULL(LTRIM(RTRIM(@VendorName)),'') = '' THEN CONCAT(LV.VNDR_NME, ' - ', VF.VNDR_CD)
                  ELSE '%' + @VendorName + '%'
            END
      GROUP BY VF.[VNDR_FOLDR_ID],VF.[VNDR_CD],LV.[VNDR_NME],VF.[CTRY_CD],LC.[CTRY_NME]
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
