USE [COWS]
GO
_CreateObject 'SP','dbo','GetOrderDataByFTN'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Project:		COWS - ProdSupport
-- Author:		Joshua Judiel Bolano
-- Create date: 07/03/2017
-- Description:	Get Order Data via FTN
-- exec [dbo].[GetOrderDataByFTN] 'DC37WZ000025'
-- =============================================
ALTER PROCEDURE [dbo].[GetOrderDataByFTN]
	-- Add the parameters for the stored procedure here
	@ftn varchar(max),
	@csglvl tinyint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here

	--select * from openquery(m5,'select * from mach5.v_v5U_ordr where ordr_id = ''75951'' ')
	--declare @ftn varchar(max) = 'CN3NKJ079031'-- 'DC3NKT085024,DC3NKT085039,DC3NKT085058,DC3NKT085072,DC3NKT085097,DC3NKT085092'
	declare @ordrids varchar(max)
	--select * from ordr_nte with (nolock) where ordr_id in (450924) order by ordr_id desc, CREAT_DT asc
	--select * from dbo.FSA_ORDR_CPE_LINE_ITEM with (nolock) where device_id='00000000050001'
	IF OBJECT_ID(N'tempdb..#tmpftnordridadhoc', N'U') IS NOT NULL         
					DROP TABLE #tmpftnordridadhoc 
	create table #tmpftnordridadhoc(ftn varchar(20),ordr_id varchar(20),h5foldrid int)
	truncate table #tmpftnordridadhoc
	insert into #tmpftnordridadhoc select distinct fo.ftn, fo.ordr_id, od.H5_FOLDR_ID from (select * from web.ParseCommaSeparatedStrings(@ftn))a inner join dbo.fsa_ordr fo with (nolock) on a.StringID=fo.ftn inner join dbo.ordr od with (nolock) on od.ORDR_ID=fo.ORDR_ID
	--select * from #tmpftnordridadhoc
	declare @listofordrids xml;
	declare @listordridStr nvarchar(max);
	declare @listftnStr nvarchar(max);

	SET @listftnStr = REPLACE(@ftn,',',''''',''''')

	--select @listftnStr
	IF OBJECT_ID(N'tempdb..#tmpftnm5ordridadhoc', N'U') IS NOT NULL         
					DROP TABLE #tmpftnm5ordridadhoc 
	create table #tmpftnm5ordridadhoc(ftn varchar(20),ordr_id varchar(20),csglvl tinyint)
	truncate table #tmpftnm5ordridadhoc
	declare @x nvarchar(max) = 'select order_nbr, ordr_id, csglvl  from openquery(m5, ''select distinct o.order_nbr, o.ordr_id, CASE WHEN a.CSG_LVL IS NULL THEN 0 ELSE TO_NUMBER(a.CSG_LVL) END as csglvl from mach5.v_v5u_ordr o
	LEFT JOIN  MACH5.V_V5U_NVTRY n on o.PRNT_ACCT_ID=n.CUST_ACCT_ID
	LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
	where o.order_nbr in ('''''+@listftnStr+''''')
																				union select distinct vct.chng_txn_nbr as order_nbr, vct.chng_txn_id as ordr_id, CASE WHEN a.CSG_LVL IS NULL THEN 0 ELSE TO_NUMBER(a.CSG_LVL) END as csglvl from mach5.V_V5U_CHNG_TXN vct
																				LEFT JOIN Mach5.V_V5U_CHNG_TXN_DETL d ON vct.CHNG_TXN_ID = d.CHNG_TXN_ID
											LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID = d.NVTRY_ID
											LEFT JOIN MACH5.v_v5U_cust_acct a on n.CUST_ACCT_ID = a.CUST_ACCT_ID 
																				where vct.chng_txn_nbr in ('''''+@listftnStr+''''')
																				union select distinct vvo.order_nbr, vvo.ordr_id,
CASE WHEN a.CSG_LVL IS NULL THEN 0 ELSE TO_NUMBER(a.CSG_LVL) END as csglvl from mach5.v_v5u_ordr vvo inner join mach5.v_v5u_ordr vvo2 on vvo2.ORDR_ID=vvo.RLTD_ORDR_ID
LEFT JOIN  MACH5.V_V5U_NVTRY n on vvo2.PRNT_ACCT_ID=n.CUST_ACCT_ID
	LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
 where vvo2.order_nbr in ('''''+@listftnStr+''''') '')';
	--select @x
	insert into #tmpftnm5ordridadhoc 
	exec sp_executesql @x
	--select * from #tmpftnm5ordridadhoc

IF EXISTS (SELECT 'X' FROM #tmpftnm5ordridadhoc WITH (NOLOCK) WHERE csglvl>0 AND ((csglvl < @csglvl) OR (@csglvl=0)))
RETURN;

		set @listofordrids = CAST((
				SELECT TOP 200 ordr_id AS Value 
				FROM #tmpftnm5ordridadhoc
			FOR XML RAW) AS XML)
			--select @listofordrids
		
			SELECT 
					@listordridStr = 
						CAST(CAST(COALESCE(@listordridStr+CAST(''''',''''' AS nvarchar(max)) ,'''''') AS nvarchar(max)) + 
						CAST(c.value('@Value[1]','nvarchar(max)') AS nvarchar(max)) AS nvarchar(max))
				FROM @listofordrids.nodes('/row') as T(c)
				SET @listordridStr = CAST(@listordridStr + CAST('''''' AS nvarchar(max)) AS nvarchar(max))
	--select @listordridStr
	--select * from #tmpftnordridadhoc
	declare @sqlftnadhoc nvarchar(max) = cast('select DISTINCT ''dbo.ORDR'' as ORDR, tmp.ftn, od.* from dbo.ordr od with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=od.ordr_id;
	select DISTINCT ''dbo.FSA_ORDR'' as FSA_ORDR, fo.* from dbo.fsa_ordr fo with (nolock) inner join #tmpftnordridadhoc tmp on tmp.ftn=fo.ftn;
	select DISTINCT ''dbo.FSA_ORDR_CPE_LINE_ITEM'' as FSA_ORDR_CPE_LINE_ITEM, tmp.ftn, fcl.* from dbo.FSA_ORDR_CPE_LINE_ITEM fcl with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=fcl.ordr_id;
	select DISTINCT ''dbo.ORDR_NTE'' as ORDR_NTE, tmp.ftn, ont.* from dbo.ordr_nte ont with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=ont.ordr_id order by 1 desc;
	select DISTINCT ''dbo.FSA_ORDR_CUST'' as FSA_ORDR_CUST, tmp.ftn, foc.* from dbo.FSA_ORDR_CUST foc with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=foc.ordr_id;
	select DISTINCT ''dbo.ORDR_ADR'' as ORDR_ADR, tmp.ftn, oa.* from dbo.ORDR_ADR oa with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=oa.ordr_id;
	select DISTINCT ''dbo.ORDR_CNTCT'' as ORDR_CNTCT, tmp.ftn, oc.* from dbo.ORDR_CNTCT oc with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=oc.ordr_id;
	select DISTINCT ''dbo.ORDR_JPRDY'' as ORDR_JPRDY, tmp.ftn, oj.* from dbo.ORDR_JPRDY oj with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=oj.ordr_id;
	select DISTINCT ''dbo.H5_FOLDR'' as H5_FOLDR, tmp.ftn, hf.* from dbo.H5_FOLDR hf with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.h5foldrid=hf.H5_FOLDR_ID order by 1 desc;
	select DISTINCT ''dbo.H5_DOC'' as H5_DOC, tmp.ftn, hd.* from dbo.H5_DOC hd with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.h5foldrid=hd.H5_FOLDR_ID order by 1 desc;
	select DISTINCT ''dbo.VNDR_ORDR'' as VNDR_ORDR, tmp.ftn, vo.* from dbo.VNDR_ORDR vo with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=vo.ORDR_ID order by 1 desc;
	select DISTINCT ''dbo.VNDR_FOLDR'' as VNDR_FOLDR, tmp.ftn, vf.* from dbo.VNDR_FOLDR vf with (nolock) inner join dbo.VNDR_ORDR vo with (nolock) on vo.VNDR_FOLDR_ID=vf.VNDR_FOLDR_ID inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=vo.ORDR_ID order by 1 desc;
	select DISTINCT ''dbo.EMAIL_REQ'' as EMAIL_REQ, tmp.ftn, er.* from dbo.EMAIL_REQ er with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=er.ordr_id;
	select DISTINCT ''dbo.ODIE_REQ'' as ODIE_REQ, tmp.ftn, odr.* from dbo.ODIE_REQ odr with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=odr.ordr_id;
	select DISTINCT ''dbo.PS_RCPT_QUEUE'' as PS_RCPT_QUEUE, tmp.ftn, prq.* from dbo.PS_RCPT_QUEUE prq with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=prq.ordr_id order by 1 desc;
	select DISTINCT ''dbo.PS_REQ_HDR_QUEUE'' as PS_REQ_HDR_QUEUE, tmp.ftn, prhq.* from dbo.PS_REQ_HDR_QUEUE prhq with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=prhq.ordr_id order by 1 desc;
	select DISTINCT ''dbo.PS_REQ_LINE_ITM_QUEUE'' as PS_REQ_LINE_ITM_QUEUE, tmp.ftn, prhq.* from dbo.PS_REQ_LINE_ITM_QUEUE prhq with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=prhq.ordr_id order by 1 desc;
	select DISTINCT ''dbo.SSTAT_REQ'' as SSTAT_REQ, tmp.ftn, sr.* from dbo.SSTAT_REQ sr with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=sr.ordr_id order by 1 desc;
	select DISTINCT ''dbo.SSTAT_RSPN'' as SSTAT_RSPN, tmp.ftn, sr.* from dbo.SSTAT_RSPN sr with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ftn=sr.ordr_id order by 1 desc;
	select DISTINCT ''dbo.M5_EVENT_MSG'' as M5_EVENT_MSG, tmp.ftn, mem.* from dbo.M5_EVENT_MSG mem with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ftn=mem.M5_ORDR_NBR order by 1 desc;
	select DISTINCT ''dbo.EVENT_CPE_DEV'' as EVENT_CPE_DEV, tmp.ftn, ecd.* from dbo.EVENT_CPE_DEV ecd with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ftn=ecd.CPE_ORDR_NBR order by 1 desc;
	select DISTINCT ''dbo.EVENT_DEV_SRVC_MGMT'' as EVENT_DEV_SRVC_MGMT, tmp.ftn, eds.* from dbo.EVENT_DEV_SRVC_MGMT eds with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ftn=eds.MNS_ORDR_NBR order by 1 desc;
	select DISTINCT ''dbo.M5_ORDR_MSG'' as M5_ORDR_MSG, tmp.ftn, mom.* from dbo.M5_ORDR_MSG mom with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=mom.ordr_id order by 1 desc;
	select DISTINCT ''dbo.FSA_ORDR_MSG'' as FSA_ORDR_MSG, tmp.ftn, fom.* from dbo.FSA_ORDR_MSG fom with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=fom.ordr_id order by 1 desc;
	select DISTINCT ''dbo.ACT_TASK'' as ACT_TASK, tmp.ftn, act.* from dbo.act_task act with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=act.ordr_id order by 1 desc;
	select DISTINCT ''dbo.WG_PTRN_STUS'' as WG_PTRN_STUS, tmp.ftn, wpt.* from dbo.wg_ptrn_stus wpt with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=wpt.ordr_id order by 1 desc;
	select DISTINCT ''dbo.WG_PROF_STUS'' as WG_PROF_STUS, tmp.ftn, wpr.* from dbo.wg_prof_stus wpr with (nolock) inner join #tmpftnordridadhoc tmp with (nolock) on tmp.ordr_id=wpr.ordr_id order by 1 desc;' AS nvarchar(max))

	--select @sqlftnadhoc

	exec sp_executesql @sqlftnadhoc

	--select @listftnStr
	--select @listordridStr

	set @sqlftnadhoc = cast('select ''MACH5.V_V5U_CHNG_TXN'' as MACH5_V_V5U_CHNG_TXN, * from openquery(m5, ''select * from mach5.V_V5U_CHNG_TXN where CHNG_TXN_NBR in ('''''+@listftnStr+''''')  '');
								SELECT ''#MACH5_ORDR_CHNG'' as #MACH5_ORDR_CHNG, * FROM OPENQUERY(M5,''SELECT DISTINCT d.CHNG_TXN_ID AS M5_ORDR_ID, CHNG_TXN_NBR AS ORDER_NBR, c.CUST_ACCT_ID AS PRNT_ACCT_ID, ''''CH'''' AS ORDR_TYPE_CD, NULL AS PROD_ID, 
												 null AS PROD_TYPE_CD, c.TYPE_CD AS ORDR_SUBTYPE_CD, c.DOM_PROCESS_FLG_CD, 
												 null AS RLTD_ORDR_ID, null AS CUST_SIGN_DT, null AS CUST_WANT_DT, null AS CUST_SBMT_DT, 
												 c.CUST_CMMT_DT, c.EXP_FLG_CD, c.EXP_TYPE_CD, null AS CUST_PREM_OCCU_FLG_CD, 
												 null AS CUST_ACPT_SRVC_FLG_CD, null AS MULT_ORDR_SBMTD_FLG_CD,null AS DSCNCT_REAS_CD, 
												 null AS PRE_QUAL_NBR, null AS COST_CNTR, null AS EXPEDITE_APRVR_NME,null AS APRVR_TITLE, 
												 null AS APRVR_PHN, null AS APPRVL_DT, null, a.CSG_LVL, CASE WHEN a.CSG_LVL IS NULL THEN 0 ELSE 1 END AS SCURD_CD
											FROM  MACH5.V_V5U_CHNG_TXN c
											LEFT JOIN Mach5.V_V5U_CHNG_TXN_DETL d ON c.CHNG_TXN_ID = d.CHNG_TXN_ID
											LEFT JOIN MACH5.V_V5U_ORDR_CMPNT c ON d.NVTRY_ID = c.NVTRY_ID
											LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID = d.NVTRY_ID
											LEFT JOIN MACH5.V_V5U_ORDR  o ON o.ORDR_ID = n.ORIG_ORDR_ID
											LEFT JOIN MACH5.v_v5U_cust_acct a on n.CUST_ACCT_ID = a.CUST_ACCT_ID AND a.CIS_HIER_LVL_CD = ''''H6''''
											WHERE c.CHNG_TXN_NBR in ('''''+@listftnStr+''''') '');
											
											SELECT ''#MACH5_ORDR_CHNG_CMPNT'' as #MACH5_ORDR_CHNG_CMPNT, *
						FROM OPENQUERY(M5,''SELECT d.CHNG_TXN_ID AS ORDR_ID, 0 AS QDA_ORDR_ID, d.CHNG_TXN_DETL_ID AS ORDR_CMPNT_ID, n.CMPNT_TYPE_CD,n.ONE_STOP_SHOP_FLG_CD,
														c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
														c.ACCS_CXR_CD,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD,NULL AS PORT_SPD_DES, 
														d.NVTRY_ID,c.CPE_ACCS_PRVDR_NME,n.CPE_CONTRACT_TERM,
														n.CPE_CONTRACT_TYPE,c.CPE_DLVRY_DUTIES_AMT,c.CPE_DLVRY_DUTIES_CD,
														c.CPE_DROP_SHIP_FLG_CD,c.CPE_EQPT_CKT_ID,n.CPE_ITEM_ID,
														c.CPE_MANAGED_BY,c.CPE_MFR_ID,c.CPE_MFR_NME,c.CPE_MFR_PART_NBR,
														n.CPE_MNGD_FLG_CD,c.CPE_MSCP_CD,c.CPE_LIST_PRICE,c.CPE_REQD_ON_SITE_DT,
														c.CPE_SHIP_CLLI_CD,c.CPE_SPRINT_TEST_TN_NBR,
														n.CPE_DEVICE_ID, c.EQPT_ID, n.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
														n.QTY, n.NME, c.CPE_VNDR_DSCNT, n.CPE_ITEM_MSTR_NBR, c.CPE_RECORD_ONLY_FLG_CD,
														c.CPE_CMPN_FMLY, c.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, c.STUS_CD, c.CPE_MFR_PS_ID,
														c.CPE_ALT_SHIP_ADR,c.CPE_ALT_SHIP_ADR_CTRY_CD,c.CPE_ALT_SHIP_ADR_CTY_NME,
														c.CPE_ALT_SHIP_ADR_LINE_1,c.CPE_ALT_SHIP_ADR_LINE_2,c.CPE_ALT_SHIP_ADR_LINE_3,
														c.CPE_ALT_SHIP_ADR_PRVN_NME,c.CPE_ALT_SHIP_ADR_PSTL_CD,
														c.CPE_ALT_SHIP_ADR_ST_PRVN_CD, c.CPE_ACCS_PRVDR_TN_NBR, c.CPE_EQUIPMENT_ONLY_FLG_CD
													FROM	MACH5.V_V5U_CHNG_TXN c
													LEFT JOIN Mach5.V_V5U_CHNG_TXN_DETL d ON c.CHNG_TXN_ID = d.CHNG_TXN_ID
													LEFT JOIN MACH5.V_V5U_ORDR_CMPNT c ON d.NVTRY_ID = c.NVTRY_ID
													LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID = d.NVTRY_ID 
													WHERE d.ORIG_CD = ''''C'''' AND	c.CHNG_TXN_NBR in ('''''+@listftnStr+''''') '');' as nvarchar(max))
	exec sp_executesql @sqlftnadhoc

	set @sqlftnadhoc = cast('SELECT ''NRMBPM'' as NRMBPM, * FROM OPENQUERY (NRMBPM, ''SELECT DISTINCT * FROM BPMF_OWNER.BPM_COWS_NRFC WHERE	ORDR_ID IN (' AS nvarchar(max))+CAST(@listordridStr AS nvarchar(max))+CAST (') ORDER BY ORDR_ID '');' as nvarchar(max))
	--select @listordridStr
	--select @sqlftnadhoc
	exec sp_executesql @sqlftnadhoc

	set @sqlftnadhoc = cast('select ''MACH5.V_V5U_ORDR'' as MACH5_V_V5U_ORDR, * from openquery(m5, ''select distinct * from mach5.v_v5u_ordr where order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_CUST_ACCT-MOVE'' as MACH5_V_V5U_CUST_ACCT_MOVE, * from openquery(m5, ''select distinct vvca.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_CUST_ACCT vvca on vvca.CUST_ACCT_ID=vvo.MOVE_TO_ACCT_ID where vvo.order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_CUST_ACCT-PRNT'' as MACH5_V_V5U_CUST_ACCT_PRNT, * from openquery(m5, ''select distinct vvca.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_CUST_ACCT vvca on vvca.CUST_ACCT_ID=vvo.PRNT_ACCT_ID where vvo.order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_ORDR_CMPNT'' as MACH5_V_V5U_ORDR_CMPNT, * from openquery(m5, ''select distinct vvoc.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_CMPNT vvoc on vvoc.ordr_id=vvo.ordr_id where order_nbr in ('''''+@listftnStr+''''')
															  union select distinct vvoc.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_CNCL_CMPNT vvcc on vvcc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_ORDR_CMPNT vvoc on vvoc.ordr_cmpnt_id=vvcc.ordr_cmpnt_id where vvo.order_nbr in ('''''+@listftnStr+''''') '');
	select ''MACH5.V_V5U_NVTRY'' as MACH5_V_V5U_NVTRY, * from openquery(m5, ''select distinct nv.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_CMPNT vvoc on vvoc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_NVTRY nv on vvoc.NVTRY_ID= nv.NVTRY_ID where order_nbr in ('''''+@listftnStr+''''')
																			  union select distinct nv.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_CNCL_CMPNT vvcc on vvcc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_ORDR_CMPNT vvoc on vvoc.ordr_cmpnt_id=vvcc.ordr_cmpnt_id inner join mach5.V_V5U_NVTRY nv on vvoc.NVTRY_ID= nv.NVTRY_ID where vvo.order_nbr in ('''''+@listftnStr+''''') '');
	select ''MACH5.V_V5U_ORDR_DISC_CMPNT'' as MACH5_V_V5U_ORDR_DISC_CMPNT, * from openquery(m5, ''select distinct vvoc.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_DISC_CMPNT vvoc on vvoc.ordr_id=vvo.ordr_id where order_nbr in ('''''+@listftnStr+''''')
																   union select distinct vvdc.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_CNCL_CMPNT vvcc on vvcc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_ORDR_DISC_CMPNT vvdc on vvdc.ordr_disc_cmpnt_id=vvcc.ordr_cmpnt_id where vvo.order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_NVTRY-DISC'' as MACH5_V_V5U_NVTRY_DISC, * from openquery(m5, ''select distinct nv.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_DISC_CMPNT vvoc on vvoc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_NVTRY nv on vvoc.NVTRY_CMPNT_ID= nv.NVTRY_ID where order_nbr in ('''''+@listftnStr+''''')
															  union select distinct nv.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_CNCL_CMPNT vvcc on vvcc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_ORDR_DISC_CMPNT vvdc on vvdc.ordr_disc_cmpnt_id=vvcc.ordr_cmpnt_id inner join mach5.V_V5U_NVTRY nv on vvdc.NVTRY_CMPNT_ID= nv.NVTRY_ID where vvo.order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_ORDR-CNCL'' as MACH5_V_V5U_ORDR_CNCL, * from openquery(m5, ''select distinct vvo.* from mach5.v_v5u_ordr vvo inner join mach5.v_v5u_ordr vvo2 on vvo2.ORDR_ID=vvo.RLTD_ORDR_ID inner join mach5.V_V5U_ORDR_CNCL_CMPNT vvcc on vvcc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_ORDR_CMPNT vvoc2 on vvoc2.ordr_cmpnt_id=vvcc.ordr_cmpnt_id where vvo2.order_nbr in ('''''+@listftnStr+''''')
															 union select distinct vvo.* from mach5.v_v5u_ordr vvo inner join mach5.v_v5u_ordr vvo2 on vvo2.ORDR_ID=vvo.RLTD_ORDR_ID inner join mach5.V_V5U_ORDR_CNCL_CMPNT vvcc on vvcc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_ORDR_DISC_CMPNT vvoc2 on vvoc2.ordr_disc_cmpnt_id=vvcc.ordr_cmpnt_id where vvo2.order_nbr in ('''''+@listftnStr+''''') '');
	select ''MACH5.V_V5U_ORDR_CMPNT-CNCL'' as MACH5_V_V5U_ORDR_CMPNT_CNCL, * from openquery(m5, ''select distinct vvcc.* from mach5.v_v5u_ordr vvo inner join mach5.v_v5u_ordr vvo2 on vvo2.ORDR_ID=vvo.RLTD_ORDR_ID inner join mach5.V_V5U_ORDR_CNCL_CMPNT vvcc on vvcc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_ORDR_CMPNT vvoc2 on vvoc2.ordr_cmpnt_id=vvcc.ordr_cmpnt_id where vvo2.order_nbr in ('''''+@listftnStr+''''')
															 union select distinct vvcc.* from mach5.v_v5u_ordr vvo inner join mach5.v_v5u_ordr vvo2 on vvo2.ORDR_ID=vvo.RLTD_ORDR_ID inner join mach5.V_V5U_ORDR_CNCL_CMPNT vvcc on vvcc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_ORDR_DISC_CMPNT vvoc2 on vvoc2.ordr_disc_cmpnt_id=vvcc.ordr_cmpnt_id where vvo2.order_nbr in ('''''+@listftnStr+''''') '');
	select ''MACH5.V_V5U_PORT_SPEED'' as MACH5_V_V5U_PORT_SPEED, * from openquery(m5, ''select distinct ps.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_CMPNT vvoc on vvoc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_PORT_SPEED ps on vvoc.PORT_SPD_CD= ps.PORT_SPD_CD where order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_ACCS_BDWD_TYPE'' as MACH5_V_V5U_ACCS_BDWD_TYPE, * from openquery(m5, ''select distinct bd.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_CMPNT vvoc on vvoc.ordr_id=vvo.ordr_id inner join mach5.V_V5U_ACCS_BDWD_TYPE bd on vvoc.ACCS_BDWD_CD= bd.TYPE_CD where order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_ORDR_NOTE'' as MACH5_V_V5U_ORDR_NOTE, * from openquery(m5, ''select vvon.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_NOTE vvon on vvon.ordr_id=vvo.ordr_id where order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_ORDR_SCA'' as MACH5_V_V5U_ORDR_SCA, * from openquery(m5, ''select distinct vvos.* from mach5.v_v5u_ordr vvo inner join mach5.V_V5U_ORDR_SCA vvos on vvos.ordr_id=vvo.ordr_id where order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_TERM_CUST_ACCT-MOVE'' as MACH5_V_V5U_TERM_CUST_ACCT_MOVE, * from openquery(m5, ''select distinct vtca.* from mach5.v_v5u_ordr vvo  inner join mach5.V_V5U_CUST_ACCT vvca on vvca.CUST_ACCT_ID=vvo.MOVE_TO_ACCT_ID inner join  mach5.V_V5U_TERM_CUST_ACCT vtca on vvca.CUST_ACCT_ID=vtca.CUST_ACCT_ID where vvo.order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_TERM_CUST_ACCT-PRNT'' as MACH5_V_V5U_TERM_CUST_ACCT_PRNT, * from openquery(m5, ''select distinct vtca.* from mach5.v_v5u_ordr vvo  inner join mach5.V_V5U_CUST_ACCT vvca on vvca.CUST_ACCT_ID=vvo.PRNT_ACCT_ID inner join  mach5.V_V5U_TERM_CUST_ACCT vtca on vvca.CUST_ACCT_ID=vtca.CUST_ACCT_ID where vvo.order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_ACCT_TEAM_CONTACT-MOVE'' as MACH5_V_V5U_ACCT_TEAM_CONTACT_MOVE, * from openquery(m5, ''select distinct vtca.* from mach5.v_v5u_ordr vvo  inner join mach5.V_V5U_CUST_ACCT vvca on vvca.CUST_ACCT_ID=vvo.MOVE_TO_ACCT_ID inner join  mach5.V_V5U_ACCT_TEAM_CONTACT vtca on vvca.CUST_ACCT_ID=vtca.CUST_ACCT_ID where vvo.order_nbr in ('''''+@listftnStr+''''')  '');
	select ''MACH5.V_V5U_ACCT_TEAM_CONTACT-PRNT'' as MACH5_V_V5U_ACCT_TEAM_CONTACT_PRNT, * from openquery(m5, ''select distinct vtca.* from mach5.v_v5u_ordr vvo  inner join mach5.V_V5U_CUST_ACCT vvca on vvca.CUST_ACCT_ID=vvo.PRNT_ACCT_ID inner join  mach5.V_V5U_ACCT_TEAM_CONTACT vtca on vvca.CUST_ACCT_ID=vtca.CUST_ACCT_ID where vvo.order_nbr in ('''''+@listftnStr+''''')  '');' AS nvarchar(max))

	--select @sqlftnadhoc
	exec sp_executesql @sqlftnadhoc
END
