USE [COWS]
GO
_CreateObject 'SP','dbo','sendMsgToNRMBPM'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <01/11/2016>
-- Description:	<To send FOC & ASR Notes messages 
--	back to NRM BPM system via DB link>
-- =============================================
ALTER PROCEDURE dbo.sendMsgToNRMBPM
@ORDR_ID INT,
@Notes	Varchar(max) = '',
@FocMsg Bit = '0'		 
AS

BEGIN

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	--DECLARE	@ORDR_ID INT = 5136
	SET NOCOUNT ON;
	DECLARE @FTN Varchar(20), @TDD Date, @CktID Varchar(200)
	DECLARE @SQL nVarchar(max)
	DECLARE @Cnt INT = 0, @Ctr INT = 0
	--NRMBPM Procedure call
	--procedure proc_BPM_COWS_FOC_DETAILS(p_ordr_ID in varchar2, p_FOC_Date in date, p_Ckt_ID in varchar2,p_asr_notes in VARCHAR2);
	
	IF @FocMsg = '1'
		BEGIN
			DECLARE @FOCMessages TABLE 
				(
					ORDR_ID	INT,
					FTN		VARCHAR(20),
					TDD		DATE,
					Ckt_ID		Varchar(200),
					Flag		Bit
				)
			INSERT INTO @FOCMessages (ORDR_ID,FTN, TDD, CKT_ID, Flag)
			(
				SELECT DISTINCT c.ORDR_ID, f.FTN, cMS.TRGT_DLVRY_DT, c.VNDR_CKT_ID, 0
					FROM	dbo.CKT c WITH (NOLOCK)
				INNER JOIN	dbo.FSA_ORDR f WITH (NOLOCK) ON c.ORDR_ID = f.ORDR_ID
				INNER JOIN	(SELECT  CKT_ID,MAX(VER_ID) AS VER_ID 
								FROM dbo.CKT_MS WITH (NOLOCK)
								GROUP BY CKT_ID)cMax ON cMax.CKT_ID = c.CKT_ID
				INNER JOIN	dbo.CKT_MS cMS WITH (NOLOCK) ON cMax.CKT_ID = cMS.CKT_ID
															AND cMax.VER_ID = cMS.VER_ID
					WHERE	f.ORDR_ID = @ORDR_ID
					
			)
			
			SELECT @Cnt = Count(1) FROM @FOCMessages
			IF @Cnt > 0
				BEGIN
					WHILE (@Ctr < @Cnt)
						BEGIN
							SELECT TOP 1 @FTN = FTN,
									@TDD = TDD,
									@CktID = Ckt_ID
								FROM @FOCMessages
								WHERE Flag = 0
							IF @CktID = ''
								SET @CktID = 'Dummy9999'
							--SELECT @FTN, @TDD, @CktID	
							SET @SQL = ''
							SET @SQL = ' EXECUTE ( ''begin BPMF_OWNER.pkg_bpmf_cows.proc_BPM_COWS_FOC_DETAILS(''''' + CONVERT(VARCHAR,@FTN) + ''''',''''' + CONVERT(VARCHAR,@TDD) + ''''',''''' + CONVERT(VARCHAR,@CktID) + ''''',''''' + CONVERT(VARCHAR,@Notes) + '''''); end;'') AT NRMBPM; '
							EXEC sp_executesql @SQL
							
							SET @Ctr = @Ctr + 1
							Update @FOCMessages SET Flag = 1 WHERE FTN = @FTN and Ckt_ID = @CktID AND TDD = @TDD
						END
				END	
		END
	ELSE --Send ASR Notes 
		BEGIN
			Select @FTN = FTN
				From dbo.FSA_ORDR WITH (NOLOCK)
				Where ORDR_ID = @ORDR_ID
			
			SET @TDD = GETDATE()
			SET @CktID = 'Dummy9999'
		
			SET @SQL = ''
			SET @SQL = ' EXECUTE ( ''begin BPMF_OWNER.pkg_bpmf_cows.proc_BPM_COWS_FOC_DETAILS(''''' + CONVERT(VARCHAR,@FTN) + ''''',''''' + CONVERT(VARCHAR,@TDD) + ''''',''''' + CONVERT(VARCHAR,@CktID) + ''''',''''' + CONVERT(VARCHAR,@Notes) + '''''); end;'') AT NRMBPM; '
			EXEC sp_executesql @SQL
		END
	
END TRY

BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH
END
GO
