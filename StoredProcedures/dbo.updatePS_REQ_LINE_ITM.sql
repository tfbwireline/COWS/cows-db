USE [COWS]
GO
_CreateObject 'SP','dbo','updatePS_REQ_LINE_ITM'
GO
/****** Object:  StoredProcedure [dbo].[updatePS_REQ_LINE_ITM]    Script Date: 03/17/2016 12:12:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 08/11/2015
-- Description:	Updates Line Items records staged for SCM.
-- =========================================================
ALTER PROCEDURE [dbo].[updatePS_REQ_LINE_ITM]
	@FSA_CPE_LINE_ITEM_ID  int,
	@REQSTN_NBR varchar (10),
	@REQ_LINE_NBR int
        
AS
BEGIN TRY
				
	UPDATE dbo.PS_REQ_LINE_ITM_QUEUE
		SET SENT_DT = GETDATE(),
			REQ_LINE_NBR = @REQ_LINE_NBR 
	WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
	    AND REQSTN_NBR = @REQSTN_NBR
	    
	UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM
	  SET PLSFT_RQSTN_NBR = @REQSTN_NBR, RQSTN_DT = GETDATE()
	 WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID 
	
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH


