USE [COWS]
GO

/****** Object:  StoredProcedure [web].[getView]    Script Date: 04/25/2011 13:32:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




/*
 ================================================================================================  
 Author:  csb9923  
 Create date: 4/19/2011  
 Description: Gets the View info
 ================================================================================================  
*/

ALTER PROCEDURE [web].[getView]   
	@ViewID INT
 AS
BEGIN  
 
 SET NOCOUNT ON;  
  
 BEGIN TRY  
	SELECT [DSPL_VW_ID],[USER_ID],[DSPL_VW_NME],[SITE_CNTNT_ID],[SITE_CNTNT_COL_VW_CD],[DSPL_VW_DES],[DSPL_VW_WEB_ADR],[DFLT_VW_CD]
		  ,[PBLC_VW_CD],[FILTR_COL_1_OPR_ID],[FILTR_COL_2_OPR_ID],[FILTR_COL_1_ID],[FILTR_COL_2_ID],[FILTR_COL_1_VALU_TXT],[FILTR_COL_2_VALU_TXT]
		  ,[FILTR_ON_CD],[SORT_BY_COL_1_ID],[SORT_BY_COL_2_ID],[GRP_BY_COL_1_ID],[GRP_BY_COL_2_ID],[CREAT_BY_USER_ID],[MODFD_BY_USER_ID],[MODFD_DT]
		  ,[FILTR_LOGIC_OPR_ID],[SORT_BY_COL_1_ASC_ORDR_CD],[SORT_BY_COL_2_ASC_ORDR_CD]
	FROM [COWS].[dbo].[DSPL_VW] WITH (NOLOCK) WHERE DSPL_VW_ID=@ViewID

	SELECT [DSPL_VW_ID],[DSPL_COL_ID],[DSPL_POS_FROM_LEFT_NBR]
	FROM [COWS].[dbo].[DSPL_VW_COL] WITH (NOLOCK) 
	WHERE DSPL_VW_ID=@ViewID
  

 END TRY  
  
 BEGIN CATCH  
  --exec [dbo].[insertErrorInfo]  
 END CATCH  
  
END




GO


