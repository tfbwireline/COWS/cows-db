USE [COWS]
GO
_CreateObject 'SP','dbo','sp_InsertRedesignEmailExcelData'
GO
/****** Object:  StoredProcedure [dbo].[sp_InsertRedesignEmailExcelData]    Script Date: 5/30/2016 1:53:50 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Md Md Monir
-- Create date: 12162015
-- Description:	Transfer Excel Redesign Email Data  to [REDSGN_EMAIL_NTFCTN]
-- =============================================
ALTER PROCEDURE [dbo].[sp_InsertRedesignEmailExcelData] ( 

	@REDSGN_ID varchar(20),
	@EMAILL_ADD varchar(150)
	)
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
      
		    
		SET @REDSGN_ID= LTRIM(RTRIM(@REDSGN_ID))
	    /*  Added by monir Add Prefix 0   if redesign id is less than 9 character long  START*/
		DECLARE @REDSGN_ID_TMP varchar(20)
		Declare @Len int  DECLARE @Position int
		Declare @FirstCharacter char(1)
		--SET @REDSGN_ID = 'M99901'
		SET @FirstCharacter=''
		SET @REDSGN_ID_TMP=LEFT(SUBSTRING(@REDSGN_ID, PATINDEX('%[0-9.-]%', @REDSGN_ID), 8000),PATINDEX('%[^0-9.-]%', SUBSTRING(@REDSGN_ID, PATINDEX('%[0-9.-]%', @REDSGN_ID), 8000) + 'X') -1)
		SET @FirstCharacter  = SUBSTRING (RTRIM(LTRIM(@REDSGN_ID)), 1,2)
		SET @FirstCharacter  = SUBSTRING(RTRIM(LTRIM(@REDSGN_ID)),PATINDEX('%[^0-9]%',RTRIM(LTRIM(@REDSGN_ID))),1)
		SET @Len =  LEN(RTRIM(LTRIM(@REDSGN_ID)))
		SET @Position=PATINDEX('%[^0-9]%',(RTRIM(LTRIM(@REDSGN_ID))))
		IF  @Position <>1 or @Position is null  BEGIN SET @FirstCharacter=''  END 
		BEGIN
			IF @Len =3 BEGIN SET @REDSGN_ID= @FirstCharacter+'000000'+@REDSGN_ID_TMP END 
			IF @Len =4 BEGIN SET @REDSGN_ID= @FirstCharacter+'00000'+@REDSGN_ID_TMP END 
			IF @Len =5 BEGIN SET @REDSGN_ID= @FirstCharacter+'0000'+@REDSGN_ID_TMP END 
			IF @Len =6 BEGIN SET @REDSGN_ID= @FirstCharacter+'000'+@REDSGN_ID_TMP END 
			IF @Len =7 BEGIN SET @REDSGN_ID= @FirstCharacter+'00'+@REDSGN_ID_TMP END 
		END
		/*  Added by monir Add Prefix 0   if redesign id is less than 9 character long  END*/

		IF ((PATINDEX('%@%',@EMAILL_ADD)>0) AND (PATINDEX('%.%',@EMAILL_ADD)>0))
		BEGIN
			INSERT INTO [cnv].[REDSGN_EMAIL_NTFCTN]
			SELECT REDSGN_ID, @EMAILL_ADD, 1, CRETD_BY_CD, CRETD_DT  FROM [cnv].REDSGN
			WHERE REDSGN_NBR = @REDSGN_ID
		END
	
	

	END TRY
	BEGIN CATCH
		EXEC [COWS].[dbo].[insertErrorInfo]
		
	END CATCH;


END
