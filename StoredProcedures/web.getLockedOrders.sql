﻿USE [COWS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Silva,Breno
-- Create date: 11-21-2011
-- Description:	Get Locked Orders from DB
-- =============================================
IF OBJECT_ID ('[web].[getLockedOrders]') IS NOT NULL
DROP PROC [web].[getLockedOrders]
GO
CREATE PROCEDURE [web].[getLockedOrders] 
	@FTN Varchar(20)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		[t0].[ORDR_ID],
		[t1].[LOCK_BY_USER_ID],
		[t4].[FULL_NME],
		CONVERT(VARCHAR,t0.FTN) AS FTN,
		[t2].[PROD_TYPE_DES],
		[t3].[ORDR_TYPE_DES]
	FROM [dbo].[FSA_ORDR] AS [t0] WITH (NOLOCK)
		INNER JOIN [dbo].[ORDR_REC_LOCK] AS [t1] WITH (NOLOCK) ON [t0].[ORDR_ID] = [t1].[ORDR_ID]
		INNER JOIN [dbo].[LK_PROD_TYPE] AS [t2] WITH (NOLOCK) ON [t0].[PROD_TYPE_CD] = [t2].[FSA_PROD_TYPE_CD]
		INNER JOIN [dbo].[LK_ORDR_TYPE] AS [t3] WITH (NOLOCK) ON [t0].[ORDR_TYPE_CD] = [t3].[FSA_ORDR_TYPE_CD]
		INNER JOIN [dbo].[LK_USER] AS [t4] WITH (NOLOCK) ON [t1].[LOCK_BY_USER_ID] = [t4].[USER_ID]
	WHERE CONVERT(VARCHAR,t0.FTN) = @FTN
	UNION
	SELECT
		[t0].[ORDR_ID],
		[t1].[LOCK_BY_USER_ID],
		[t4].[FULL_NME],
		CONVERT(VARCHAR,t0.ORDR_ID) AS FTN,
		[t2].[PROD_TYPE_DES],
		[t3].[ORDR_TYPE_DES]
	FROM [dbo].[ORDR] AS [t0] WITH (NOLOCK)
		LEFT JOIN [dbo].[LK_PPRT] lp WITH (NOLOCK) ON [lp].[PPRT_ID] = [t0].[PPRT_ID]
		INNER JOIN [dbo].[ORDR_REC_LOCK] AS [t1] WITH (NOLOCK) ON [t0].[ORDR_ID] = [t1].[ORDR_ID]
		INNER JOIN [dbo].[LK_PROD_TYPE] AS [t2] WITH (NOLOCK)ON [lp].[PROD_TYPE_ID] = ([t2].[PROD_TYPE_ID])
		INNER JOIN [dbo].[LK_ORDR_TYPE] AS [t3] WITH (NOLOCK) ON [lp].[ORDR_TYPE_ID] = [t3].[ORDR_TYPE_ID]
		INNER JOIN [dbo].[LK_USER] AS [t4] WITH (NOLOCK) ON  [t1].[LOCK_BY_USER_ID] = [t4].[USER_ID]
	WHERE CONVERT(VARCHAR,t0.ORDR_ID) = @FTN    
END
GO
