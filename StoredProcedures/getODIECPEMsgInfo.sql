USE [COWS]
GO
_CreateObject 'SP','dbo','getODIECPEMsgInfo'
GO
/****** Object:  StoredProcedure [dbo].[getODIECPEMsgInfo]    Script Date: 01/29/2016 10:34:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 7/29/2015
	-- Description:	Retrieves ODIE CPE Info.
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getODIECPEMsgInfo] 
		@REQ_ID int  

	AS
	BEGIN


	BEGIN TRY
	
	DECLARE @ORDR_ID int  -- 302519
	
	SELECT @ORDR_ID = ORDR_ID FROM dbo.ODIE_REQ WHERE REQ_ID = @REQ_ID
	
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	-- Order Info.
	SELECT  
			ISNULL(fsa.FTN,'')                                     AS [OrderID],
			'Install'                                              AS [OrderType],
			'Pending'                                              AS [OrderAction],
			ISNULL(lpt.PROD_TYPE_DES,'')                           AS [ProductType],
			CASE
				WHEN ord.DMSTC_CD = 1                   THEN 'D'
				ELSE 'I'
			END                                                   AS [DomesticInternationalFlag],
			ISNULL(RIGHT(ord.CSG_LVL_CD,1),0)                     AS [CSGLevel],		
			ISNULL(fsa.PRNT_FTN,'')                               AS [ParentOrderID],
			ISNULL(fsa.RELTD_FTN,'')                              AS [RelatedOrderID],
			ISNULL(ord.CUST_CMMT_DT, fsa.CUST_CMMT_DT)            AS [CustomerCommitDate],
			ISNULL(fsa.CUST_PRMS_OCPY_CD,'')                      AS [CustomerPremiseCurrentlyOccupiedFlag],
			ISNULL(fc.SITE_ID,'')                                 AS [SiteID]
			
	
	FROM dbo.FSA_ORDR fsa WITH (NOLOCK)
	LEFT OUTER JOIN dbo.LK_PROD_TYPE lpt WITH (NOLOCK) ON fsa.PROD_TYPE_CD = lpt.FSA_PROD_TYPE_CD
	INNER JOIN dbo.ORDR	ord WITH (NOLOCK) ON ord.ORDR_ID = fsa.ORDR_ID
	LEFT OUTER JOIN dbo.FSA_ORDR_CUST fc WITH (NOLOCK) ON fc.ORDR_ID = fsa.ORDR_ID
					AND fc.CIS_LVL_TYPE IN ('H5','H6')
	WHERE fsa.ORDR_ID = @ORDR_ID
		
			
	--CustomerInfo/CISHierarchyInfo Section
	
	SELECT	ISNULL(fc.CIS_LVL_TYPE,'')                                 AS [LevelType],
			ISNULL(fc.CUST_ID,'')								       AS [CustomerID],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(fc.CUST_NME)),'')     AS [CustomerName],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.STREET_ADR_1)),'') AS [AddressLine1],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.STREET_ADR_2)),'') AS [AddressLine2],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.STREET_ADR_3)),'') AS [AddressLine3],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.STE_TXT)),'')      AS [Suite],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.BLDG_NME)),'')     AS [Building],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.FLR_ID)),'')       AS [Floor],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.RM_NBR)),'')       AS [Room],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.CTY_NME)),'')      AS [City],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.STT_CD)),'')       AS [StateCode],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.ZIP_PSTL_CD)),'')  AS [ZIPPostalCode],
			ISNULL(oa.CTRY_CD,'')                                      AS [CntryCode],
			ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.PRVN_NME)),'')     AS [ProvinceMuncipality],
			'Phone'													   AS [PhoneType],
			ISNULL(oc.ISD_CD,'')                                       AS [CountryCode],
			ISNULL(oc.NPA,'')		                                   AS [NPA],
			ISNULL(oc.NXX,'')		                                   AS [NXX],
			ISNULL(oc.STN_NBR,'')	                                   AS [Station],
			ISNULL(oc.CTY_CD,'')	                                   AS [CityCode],
			ISNULL(oc.PHN_NBR,'')                                      AS [Number]

			FROM dbo.FSA_ORDR_CUST fc WITH (NOLOCK)
				LEFT JOIN dbo.ORDR_ADR oa WITH (NOLOCK)	on	oa.ORDR_ID = fc.ORDR_ID  
											AND	oa.CIS_LVL_TYPE in ('H6','H5','H1')
											AND	oa.HIER_LVL_CD	=	1
				LEFT JOIN dbo.ORDR_CNTCT oc	WITH (NOLOCK) on oc.ORDR_ID	= fc.ORDR_ID 
											AND	oc.CIS_LVL_TYPE  in ('H6','H5','H1')
											AND	oc.CNTCT_TYPE_ID	=	17
				LEFT JOIN dbo.LK_CNTCT_TYPE	lc WITH (NOLOCK) on lc.CNTCT_TYPE_ID = oc.CNTCT_TYPE_ID
		   WHERE fc.ORDR_ID = @ORDR_ID 
				AND	fc.CIS_LVL_TYPE  in ('H6','H5','H1') 	


	
	--CustomerInfo/ContactInfo Section
	SELECT
				ISNULL(lc.CNTCT_TYPE_DES,'') AS [Type],
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oc.FRST_NME)),'') AS [FirstName],
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oc.LST_NME)),'') AS [LastName],
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oc.CNTCT_NME)),'') AS [Name],
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oc.EMAIL_ADR)),'') AS [EmailAddress],
				
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.STREET_ADR_1)),'') AS [AddressLine1],
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.STREET_ADR_2)),'') AS [AddressLine2],
				
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.CTY_NME)),'')	AS [City],
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.STT_CD)),'')	AS [StateCode],
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.ZIP_PSTL_CD)),'') AS [ZIPPostalCode],
				ISNULL(oa.CTRY_CD,'')									AS [CntryCode],
				ISNULL(CONVERT(VARCHAR, DecryptByKey(oa.PRVN_NME)),'')	AS [ProvinceMuncipality],
				
				'Phone'													AS [PhoneType],
				ISNULL(oc.ISD_CD,'')                                    AS [CountryCode],
				ISNULL(oc.NPA,'')										AS	[NPA],
				ISNULL(oc.NXX,'')										AS	[NXX],
				ISNULL(oc.STN_NBR,'')									AS	[Station],
				ISNULL(oc.CTY_CD,'')									AS	[CityCode],
				ISNULL(oc.PHN_NBR,'')									AS [Number],
				ISNULL(oc.PHN_EXT_NBR,'')                               AS [Extension]
        FROM	dbo.ORDR_ADR		oa	WITH (NOLOCK)	
			LEFT JOIN	dbo.LK_ADR_TYPE		la	WITH (NOLOCK)	on la.ADR_TYPE_ID = oa.ADR_TYPE_ID
			LEFT JOIN	dbo.ORDR_CNTCT		oc	WITH (NOLOCK)	on oc.ORDR_ID	= oa.ORDR_ID 
																AND	oc.CIS_LVL_TYPE in ('H6','H5')
																AND	oc.CNTCT_TYPE_ID	!=	17
			LEFT JOIN	dbo.LK_CNTCT_TYPE	lc	WITH (NOLOCK)	on lc.CNTCT_TYPE_ID = oc.CNTCT_TYPE_ID
       WHERE	oa.ORDR_ID = @ORDR_ID  
			AND	oa.CIS_LVL_TYPE in ('H6','H5') 
						
	
	-- CPE ORDER Level Section
	SELECT 
			ISNULL(fsa.CPE_CPE_ORDR_TYPE_CD,'')                        AS [CPEOrderTypeCode],
			ISNULL(fsa.CPE_EQPT_ONLY_CD,'')                            AS [EquipmentOnlyFlagCode],
			ISNULL(fsa.CPE_REC_ONLY_CD,'')                             AS [RecordsOnlyOrderFlagCode],
			ISNULL(fsa.CPE_ACCS_PRVDR_CD,'')                           AS [AccessProviderCode],
			ISNULL(fsa.CPE_PHN_NBR_TYPE_CD,'')                         AS [Type],
			ISNULL(fsa.CPE_PHN_NBR, '')                                AS [CPEPhoneNumber],
			ISNULL(fsa.CPE_ECCKT_ID, '')                               AS [ECCKTIdentifier],
			ISNULL(fsa.CPE_REC_ONLY_CD,'')                             AS [RecordsOnlyOrderFlagCode],
			ISNULL(fsa.CPE_MSCP_CD,'')                                 AS [ManagedServicesChannelProgramCode],
			ISNULL(fsa.CPE_DLVRY_DUTY_ID, '')                          AS [DeliveryDuties]
	
		FROM FSA_ORDR fsa WITH (NOLOCK)
		WHERE fsa.ORDR_ID = @ORDR_ID
	
	
	-- CPE Line Items Section	
	
	SELECT 
		'Pending'                                                       AS [ItemAction],
		ISNULL(litm.EQPT_TYPE_ID, '')                                   AS [EquipmentTypeCode],
		ISNULL(litm.MATL_CD,'')                                         AS [MatCode],
		ISNULL(litm.MDS_DES,'')                                         AS [Description],
		ISNULL(litm.MFR_NME,'')                                         AS [Manufacturer],
		ISNULL(litm.CMPNT_ID,'')                                        AS [DeviceID],
		'CPE1001'                                                       AS [ProjectID],
		ISNULL(litm.ORDR_QTY,'')                                        AS [Quantity],
		ISNULL(litm.CNTRC_TYPE_ID,'')                                   AS [ContractType],
		ISNULL(litm.CNTRC_TERM_ID,'')                                   AS [ContractTerm],
		ISNULL(litm.INSTLN_CD,'')                                       AS [InstallationFlagCode],
		ISNULL(litm.MNTC_CD,'')                                         AS [MaintenanceFlagCode],
		ISNULL(litm.FMS_CKT_NBR,ISNULL(DISC_FMS_CKT_NBR,''))            AS [FMSCktNbr],
		ISNULL(litm.CMPNT_FMLY,'')                                      AS [CompFamily]
		
		
	FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK)
		WHERE litm.ORDR_ID = @ORDR_ID
		AND litm.EQPT_TYPE_ID in ('CLEI','MJH')
	
	
	--Notes
	
	SELECT 
		ISNULL(NTE_TXT,'')                                              AS [NoteText]
	
	FROM dbo.ORDR_NTE WITH (NOLOCK)
	WHERE ORDR_ID = @ORDR_ID
		AND NTE_TYPE_ID = 6
	
	RETURN
			
	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH

	END
	



