USE [COWS]
GO
_CreateObject 'SP','web','getOrderDisplayViewData_V2'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--================================================================================================  
-- Author:  kh946640  
-- Create date: 11/05/2019
-- Description: gets the list of Views based on userid and User Profile ID 
--================================================================================================  
ALTER PROCEDURE [web].[getOrderDisplayViewData_V2]
	@USER_ID	SMALLInt
	,@USR_PRF_ID	Int	=	0
 AS
BEGIN  
DECLARE @SiteContentID	Int
SET	@SiteContentID	=	0
SET NOCOUNT ON;  

Begin Try
	IF	@USR_PRF_ID	!=	0	
		BEGIN
			SELECT		@SiteContentID	=	ISNULL(SITE_CNTNT_ID, 0)
				FROM	dbo.LK_SITE_CNTNT	WITH (NOLOCK)
				WHERE	ISNULL(USR_PRF_ID, 0)=	@USR_PRF_ID	
		END


	IF(@SiteContentID = 19)
	BEGIN
		SELECT		vw.DSPL_VW_ID
					,vw.DSPL_VW_NME
					,vw.PBLC_VW_CD,DFLT_VW_CD
					,CASE ISNULL(vw.FILTR_COL_1_VALU_TXT, '')	WHEN ''		THEN ''
																ELSE CASE ISNULL(vw.FILTR_COL_1_OPR_ID,'') 
																		WHEN '' THEN ''
																		WHEN 'b'	THEN 'Convert([' + lc1.SRC_TBL_COL_NME + '], ''System.String'') LIKE ''' + vw.FILTR_COL_1_VALU_TXT + '%'''
																		WHEN 'c'	THEN 'Convert([' + lc1.SRC_TBL_COL_NME + '], ''System.String'') LIKE ''%' + vw.FILTR_COL_1_VALU_TXT + '%'''
																		ELSE '[' + lc1.SRC_TBL_COL_NME + '] ' + vw.FILTR_COL_1_OPR_ID + + ' ''' + vw.FILTR_COL_1_VALU_TXT + ''''
																END
					END + ' ' 				
					+ CASE ISNULL(vw.FILTR_COL_2_VALU_TXT, '')	WHEN ''		THEN ''
																ELSE CASE ISNULL(vw.FILTR_COL_2_OPR_ID,'') 
																		WHEN '' THEN ''
																		WHEN 'b'	THEN vw.FILTR_LOGIC_OPR_ID + ' Convert([' + lc2.SRC_TBL_COL_NME + '], ''System.String'') LIKE ''' + vw.FILTR_COL_2_VALU_TXT + '%'''
																		WHEN 'c'	THEN vw.FILTR_LOGIC_OPR_ID + ' Convert([' + lc2.SRC_TBL_COL_NME + '], ''System.String'') LIKE ''%' + vw.FILTR_COL_2_VALU_TXT + '%'''
																		ELSE vw.FILTR_LOGIC_OPR_ID + ' [' + lc2.SRC_TBL_COL_NME + '] ' + vw.FILTR_COL_2_OPR_ID + ' ''' + vw.FILTR_COL_2_VALU_TXT + ''''
																END
					END AS FILTER
					,CASE ISNULL(SORT_BY_COL_1_ID,0)	WHEN	0	THEN ''
														ELSE	lc3.SRC_TBL_COL_NME + ' ' +	CASE SORT_BY_COL_1_ASC_ORDR_CD	WHEN 'Y' THEN ''
																																			ELSE	'DESC'
																											END
					END
					+ CASE ISNULL(SORT_BY_COL_2_ID,0)	WHEN	0	THEN ''
														ELSE	', ' + lc4.SRC_TBL_COL_NME + ' ' +	CASE SORT_BY_COL_2_ASC_ORDR_CD	WHEN 'Y' THEN ''
																																	ELSE	'DESC'
																									END
					END	AS ORDERBY																					
			FROM		dbo.DSPL_VW		vw	WITH (NOLOCK) 
			LEFT JOIN	dbo.LK_DSPL_COL	lc1 WITH (NOLOCK) ON vw.FILTR_COL_1_ID		=	lc1.DSPL_COL_ID
			LEFT JOIN	dbo.LK_DSPL_COL	lc2 WITH (NOLOCK) ON vw.FILTR_COL_2_ID		=	lc2.DSPL_COL_ID
			LEFT JOIN	dbo.LK_DSPL_COL	lc3 WITH (NOLOCK) ON vw.SORT_BY_COL_1_ID	=	lc3.DSPL_COL_ID
			LEFT JOIN	dbo.LK_DSPL_COL	lc4 WITH (NOLOCK) ON vw.SORT_BY_COL_2_ID	=	lc4.DSPL_COL_ID
			WHERE	SITE_CNTNT_ID	=	@SiteContentID
			AND	((USER_ID	<>	1	AND	USER_ID	=	@USER_ID) 
			OR	(USER_ID	=	1	AND	PBLC_VW_CD	=	'Y')) 
			AND vw.DSPL_VW_ID IN (610,611,612,613,614)
			ORDER BY	vw.SEQ_NBR
			
		SELECT			dc.DSPL_VW_ID
						,dc.DSPL_COL_ID
						,lc.DSPL_COL_NME
						,lc.SRC_TBL_COL_NME_NEW
			FROM		dbo.DSPL_VW_COL	dc	WITH (NOLOCK)
			INNER JOIN	dbo.LK_DSPL_COL	lc	WITH (NOLOCK)	ON	dc.DSPL_COL_ID	=	lc.DSPL_COL_ID
			INNER JOIN	dbo.DSPL_VW		dv	WITH (NOLOCK)	ON	dc.DSPL_VW_ID	=	dv.DSPL_VW_ID
			WHERE		SITE_CNTNT_ID	=	@SiteContentID
				AND		((USER_ID	<>	1	AND	USER_ID	=	@USER_ID) 
				OR		(USER_ID	=	1	AND	PBLC_VW_CD	=	'Y')) 
				AND		dv.DSPL_VW_ID IN (610,611,612,613,614)
			ORDER BY	dv.SEQ_NBR
	END
ELSE
	BEGIN
		SELECT		vw.DSPL_VW_ID
					,vw.DSPL_VW_NME
					,vw.PBLC_VW_CD,DFLT_VW_CD
					,CASE ISNULL(vw.FILTR_COL_1_VALU_TXT, '')	WHEN ''		THEN ''
																ELSE CASE ISNULL(vw.FILTR_COL_1_OPR_ID,'') 
																		WHEN '' THEN ''
																		WHEN 'b'	THEN 'Convert([' + lc1.SRC_TBL_COL_NME + '], ''System.String'') LIKE ''' + vw.FILTR_COL_1_VALU_TXT + '%'''
																		WHEN 'c'	THEN 'Convert([' + lc1.SRC_TBL_COL_NME + '], ''System.String'') LIKE ''%' + vw.FILTR_COL_1_VALU_TXT + '%'''
																		ELSE '[' + lc1.SRC_TBL_COL_NME + '] ' + vw.FILTR_COL_1_OPR_ID + + ' ''' + vw.FILTR_COL_1_VALU_TXT + ''''
																END
					END + ' ' 				
					+ CASE ISNULL(vw.FILTR_COL_2_VALU_TXT, '')	WHEN ''		THEN ''
																ELSE CASE ISNULL(vw.FILTR_COL_2_OPR_ID,'') 
																		WHEN '' THEN ''
																		WHEN 'b'	THEN vw.FILTR_LOGIC_OPR_ID + ' Convert([' + lc2.SRC_TBL_COL_NME + '], ''System.String'') LIKE ''' + vw.FILTR_COL_2_VALU_TXT + '%'''
																		WHEN 'c'	THEN vw.FILTR_LOGIC_OPR_ID + ' Convert([' + lc2.SRC_TBL_COL_NME + '], ''System.String'') LIKE ''%' + vw.FILTR_COL_2_VALU_TXT + '%'''
																		ELSE vw.FILTR_LOGIC_OPR_ID + ' [' + lc2.SRC_TBL_COL_NME + '] ' + vw.FILTR_COL_2_OPR_ID + ' ''' + vw.FILTR_COL_2_VALU_TXT + ''''
																END
					END AS FILTER
					,CASE ISNULL(SORT_BY_COL_1_ID,0)	WHEN	0	THEN ''
														ELSE	lc3.SRC_TBL_COL_NME + ' ' +	CASE SORT_BY_COL_1_ASC_ORDR_CD	WHEN 'Y' THEN ''
																																			ELSE	'DESC'
																											END
					END
					+ CASE ISNULL(SORT_BY_COL_2_ID,0)	WHEN	0	THEN ''
														ELSE	', ' + lc4.SRC_TBL_COL_NME + ' ' +	CASE SORT_BY_COL_2_ASC_ORDR_CD	WHEN 'Y' THEN ''
																																	ELSE	'DESC'
																									END
					END	AS ORDERBY																					
			FROM		dbo.DSPL_VW		vw	WITH (NOLOCK) 
			LEFT JOIN	dbo.LK_DSPL_COL	lc1 WITH (NOLOCK) ON vw.FILTR_COL_1_ID		=	lc1.DSPL_COL_ID
			LEFT JOIN	dbo.LK_DSPL_COL	lc2 WITH (NOLOCK) ON vw.FILTR_COL_2_ID		=	lc2.DSPL_COL_ID
			LEFT JOIN	dbo.LK_DSPL_COL	lc3 WITH (NOLOCK) ON vw.SORT_BY_COL_1_ID	=	lc3.DSPL_COL_ID
			LEFT JOIN	dbo.LK_DSPL_COL	lc4 WITH (NOLOCK) ON vw.SORT_BY_COL_2_ID	=	lc4.DSPL_COL_ID
			WHERE	SITE_CNTNT_ID	=	@SiteContentID
			AND	((USER_ID	<>	1	AND	USER_ID	=	@USER_ID) 
			OR	(USER_ID	=	1	AND	PBLC_VW_CD	=	'Y')) 
			ORDER BY	vw.SEQ_NBR
			
		SELECT			dc.DSPL_VW_ID
						,dc.DSPL_COL_ID
						,lc.DSPL_COL_NME
						,lc.SRC_TBL_COL_NME_NEW
			FROM		dbo.DSPL_VW_COL	dc	WITH (NOLOCK)
			INNER JOIN	dbo.LK_DSPL_COL	lc	WITH (NOLOCK)	ON	dc.DSPL_COL_ID	=	lc.DSPL_COL_ID
			INNER JOIN	dbo.DSPL_VW		dv	WITH (NOLOCK)	ON	dc.DSPL_VW_ID	=	dv.DSPL_VW_ID
			WHERE		SITE_CNTNT_ID	=	@SiteContentID
				AND		((USER_ID	<>	1	AND	USER_ID	=	@USER_ID) 
				OR		(USER_ID	=	1	AND	PBLC_VW_CD	=	'Y')) 
			ORDER BY	dv.SEQ_NBR
	END
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch  
END