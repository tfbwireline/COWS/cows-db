USE [COWS]
GO
_CreateObject 'SP','dbo','getODIECustomerH1Data'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <11/24/2015>
-- Description:	<To get Customer H1 data from ODIE>
-- =============================================
ALTER PROCEDURE [dbo].[getODIECustomerH1Data] 
AS 
BEGIN 
BEGIN TRY
	SET NOCOUNT ON;
	Select *
		From OpenQuery(ODIEP101,'SELECT unique  
												--C.CUST_ID AS  CUSTOMER_SHORT_NAME , 
												C.ORG_NAME AS Customer, 
												CH.H1_ID AS H1  
												--TRIM(C.TEAM_EMAIL) AS CUSTOMER_PDL, 
												--C.DOCUMENT_URL AS SOWS_FOLDER 
									FROM	CUSTOMER C, 
											CUSTOMER_H1 CH
									where	C.CUST_ID = CH.CUST_ID')
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH 									
END
GO
