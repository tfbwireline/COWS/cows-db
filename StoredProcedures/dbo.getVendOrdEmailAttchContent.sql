﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getVendOrdEmailAttchContent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 12th July 2011
-- Description:	Returns content of Vendor Email Attachment
-- ================================================================

ALTER PROCEDURE [dbo].[getVendOrdEmailAttchContent]
	@AttachmentID INT 	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		FILE_CNTNT
	FROM 
		dbo.VNDR_ORDR_EMAIL_ATCHMT WITH (NOLOCK)
	WHERE 
		VNDR_ORDR_EMAIL_ATCHMT_ID = @AttachmentID
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
