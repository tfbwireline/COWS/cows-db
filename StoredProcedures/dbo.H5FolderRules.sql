USE [COWS]
GO
_CreateObject 'SP','dbo','H5FolderRules'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <03/24/2015>
-- Description:	<To validate if H5 folder ID exists 
--				and to insert if it doesn't>
-- =============================================
ALTER PROCEDURE [dbo].[H5FolderRules]
	@OrderID Int
AS
BEGIN

BEGIN TRY
	SET NOCOUNT ON;
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	DECLARE @CustID INT
	DECLARE @CUSTNME Varchar(500)
	DECLARE @City varchar(500)
	DECLARE @CtryCD Varchar(2)
	DECLARE @ID INT
	DECLARE @H5ID Int
	DECLARE @CSGLvlID TINYINT
	
	IF EXISTS (SELECT 'X' FROM dbo.ORDR WITH (NOLOCK) WHERE ORDR_ID = @OrderID AND H5_FOLDR_ID IS NULL AND DMSTC_CD = 1 AND ORDR_CAT_ID = 6)
		BEGIN
			SELECT @CustID	=	CUST_ID
			, @CUSTNME	=	CASE WHEN (ordr.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csdfoc.CUST_NME) ELSE foc.CUST_NME END
			, @CtryCD	=	oa.CTRY_CD
			, @City		=	CASE WHEN (ordr.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csdoa.CTY_NME) ELSE oa.CTY_NME END
			, @CSGLvlID = ordr.CSG_LVL_ID
				FROM dbo.ORDR ordr WITH (NOLOCK)
			INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.ORDR_ID=ordr.ORDR_ID
			INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON foc.ORDR_ID = oa.ORDR_ID
			LEFT JOIN dbo.CUST_SCRD_DATA csdfoc WITH (NOLOCK) ON csdfoc.SCRD_OBJ_ID=foc.FSA_ORDR_CUST_ID AND csdfoc.SCRD_OBJ_TYPE_ID=5
			LEFT JOIN dbo.CUST_SCRD_DATA csdoa WITH (NOLOCK) ON csdoa.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdoa.SCRD_OBJ_TYPE_ID=14
				WHERE foc.CIS_LVL_TYPE IN ('ES','H5','H6')
					AND foc.ORDR_ID = @OrderID
					AND oa.CIS_LVL_TYPE IN ('ES','H5','H6')
			
			IF @CustID != 0
				BEGIN
					IF EXISTS
						(
							SELECT 'X'
								FROM	dbo.H5_FOLDR WITH (NOLOCK)
								WHERE	CUST_ID = @CustID
						)
						BEGIN
							SELECT @H5ID = H5_FOLDR_ID
								FROM dbo.H5_FOLDR WITH (NOLOCK)
								WHERE CUST_ID = @CustID
							
							Update ORDR WITH (ROWLOCK)
								SET		H5_FOLDR_ID = @H5ID,
										H5_H6_CUST_ID = @CustID 
								WHERE	ORDR_ID = @OrderID
							
						END	
					ELSE
						BEGIN
							INSERT INTO dbo.H5_FOLDR (CUST_ID,CUST_NME,CUST_CTY_NME,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,CTRY_CD)
							VALUES (@CustID,CASE WHEN (@CSGLvlID>0) THEN NULL ELSE @CUSTNME END,@City,GETDATE(),1,1,@CtryCD)
							
							
							SELECT @ID = SCOPE_IDENTITY()
							
							IF (@CSGLvlID>0)
							BEGIN
								INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME])
								SELECT @ID, 6, dbo.encryptString(@CUSTNME)
							END
							
							IF @ID != 0
								BEGIN
									UPDATE dbo.ORDR	
										SET H5_FOLDR_ID		=	@ID,
											H5_H6_CUST_ID	=	@CustID
										WHERE ORDR_ID = @OrderID
								END
						END
				END
		END
	
    
		
END TRY

BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH
		
END
