USE [COWS]
GO
_CreateObject 'SP','dbo','updateMDSSlotAvailibility'
GO
-- =============================================================================    
-- Author:  sbg9814    
-- Create date: 09/27/2011    
-- Description: Checks to see if a given TimeSlot is available.    
-- =============================================================================    
ALTER PROCEDURE [dbo].[updateMDSSlotAvailibility]    
 @TME_SLOT_ID  Int    
 ,@DAY_DT   DateTime    
 ,@ASN_USER_ID  Int = 0    
 ,@CREAT_BY_USER_ID Int    
 ,@EVENT_TITLE_TXT Varchar(255)    
 ,@CNFRC_BRDG_NBR Varchar(50)    
 ,@CNFRC_PIN_NBR  Varchar(20)    
 ,@EVENT_ID   BIGINT    
 ,@Update   Bit = 0    
 ,@OLD_STRT_TMST  DATETIME    
 ,@OLD_TME_SLOT_ID TINYINT
 --,@IsReviewer BIT
AS    
BEGIN    
SET NOCOUNT ON;    
    
DECLARE @ID  Int    
DECLARE @Date Varchar(30)    
SET @ID  = 0    
DECLARE @EvCnt INT
    
Begin Try    
 SET @Date = Convert(Varchar, @DAY_DT, 101)    
 SET @ASN_USER_ID = 5001    
 
 DECLARE @Today DATETIME
 SET @Today = GETDATE()
 DECLARE @ListTxt XML

/*IF (@IsReviewer = 1)
BEGIN
	SELECT  @ID  = ISNULL(EVENT_TYPE_RSRC_AVLBLTY_ID, 0)    
	  FROM dbo.EVENT_TYPE_RSRC_AVLBLTY WITH (NOLOCK)    
	  WHERE TME_SLOT_ID       = @TME_SLOT_ID    
	  AND  Convert(Varchar, DAY_DT, 101) = @Date
END
ELSE
BEGIN*/
IF ((@Update = 0) OR ((@Update = 1) AND (@OLD_STRT_TMST IS NULL)) OR ((@Update = 1) AND (@OLD_STRT_TMST IS NOT NULL) AND ((@OLD_STRT_TMST != @DAY_DT) OR ((@OLD_STRT_TMST = @DAY_DT) AND (@TME_SLOT_ID != @OLD_TME_SLOT_ID)))))
  BEGIN
	SELECT  @ID  = ISNULL(EVENT_TYPE_RSRC_AVLBLTY_ID, 0)    
	  FROM dbo.EVENT_TYPE_RSRC_AVLBLTY WITH (NOLOCK)    
	  WHERE TME_SLOT_ID       = @TME_SLOT_ID    
	  AND  Convert(Varchar, DAY_DT, 101) = @Date    
	  AND  CURR_AVAL_TME_SLOT_CNT    < MAX_AVAL_TME_SLOT_CAP_CNT     
	  AND  EVENT_TYPE_ID = 5
  END
--END
      
 IF @TME_SLOT_ID != 0    
  BEGIN    
   SET @EVENT_TITLE_TXT = @EVENT_TITLE_TXT + ' EventID : ' + Convert(Varchar, @EVENT_ID)     
  END     
     
 IF (@Update = 1) AND (@OLD_STRT_TMST IS NOT NULL) AND ((@OLD_STRT_TMST != @DAY_DT) OR ((@OLD_STRT_TMST = @DAY_DT) AND (@TME_SLOT_ID != @OLD_TME_SLOT_ID)))
  BEGIN    
   EXEC dbo.deleteMDSSlotAvailibility    
     @ASN_USER_ID    
     ,@EVENT_ID    
     ,@OLD_STRT_TMST    
     ,@OLD_TME_SLOT_ID    
  END     
     
 IF @ASN_USER_ID = 0    
  SET @ASN_USER_ID = @CREAT_BY_USER_ID     
      
 IF @ID != 0 AND @ASN_USER_ID != 0    
  BEGIN    
   IF EXISTS (SELECT 'x'    
				FROM dbo.EVENT_ASN_TO_USER WITH (NOLOCK)    
				WHERE ASN_TO_USER_ID = @ASN_USER_ID    
				AND  EVENT_ID = @EVENT_ID)    
     BEGIN     
		 UPDATE dbo.EVENT_ASN_TO_USER WITH (ROWLOCK)    
		 SET REC_STUS_ID = 1    
		 WHERE ASN_TO_USER_ID = @ASN_USER_ID    
		  AND  EVENT_ID = @EVENT_ID    
     END    
     ELSE    
     BEGIN    
		 INSERT INTO dbo.EVENT_ASN_TO_USER (EVENT_ID, ASN_TO_USER_ID, CREAT_DT, REC_STUS_ID)    
		 SELECT  @EVENT_ID, @ASN_USER_ID, GETDATE(), 1    
    END       
         
	IF ((@Update = 0) OR ((@Update = 1) AND (@OLD_STRT_TMST IS NULL)) OR ((@Update = 1) AND (@OLD_STRT_TMST IS NOT NULL) AND ((@OLD_STRT_TMST != @DAY_DT) OR ((@OLD_STRT_TMST = @DAY_DT) AND (@TME_SLOT_ID != @OLD_TME_SLOT_ID)))))
	BEGIN

		DELETE FROM dbo.APPT WITH (ROWLOCK) WHERE [EVENT_ID] = @EVENT_ID
	   INSERT INTO dbo.APPT WITH (ROWLOCK)    
		   ([EVENT_ID]    
		   ,[SUBJ_TXT]    
		   ,[DES]    
		   ,[STRT_TMST]    
		   ,[END_TMST]    
		   ,[APPT_LOC_TXT]    
		   ,[APPT_TYPE_ID]    
		   ,[RCURNC_DES_TXT]    
		   ,[CREAT_BY_USER_ID]    
		   ,[CREAT_DT]    
		   ,[ASN_TO_USER_ID_LIST_TXT]    
		   ,[RCURNC_CD]
		   ,[REC_STUS_ID])     
		SELECT  @EVENT_ID    
		   ,@EVENT_TITLE_TXT    
		   ,@EVENT_TITLE_TXT    
		   ,Convert(DateTime, @Date, 101) + Convert(DATETIME,TME_SLOT_STRT_TME) 
		   --,Convert(DateTime, @Date, 101) + TME_SLOT_END_TME    
		   ,CASE @TME_SLOT_ID
				WHEN 21 THEN Convert(DateTime, @Date , 101) + 1 + Convert(DATETIME,TME_SLOT_END_TME)    -- Time Slot from 10 PM to 2 AM 
				ELSE Convert(DateTime, @Date , 101) + Convert(DATETIME,TME_SLOT_END_TME)
				END
		   ,@CNFRC_BRDG_NBR + ' - ' + @CNFRC_PIN_NBR    
		   ,5    
		   ,NULL    
		   ,@CREAT_BY_USER_ID    
		   ,getDate()    
		   ,'<ResourceIds>  <ResourceId Type="System.Int32" Value="' + Convert(Varchar(5), @ASN_USER_ID) + '" />  </ResourceIds>'    
		   ,0 
		   ,1   
		 FROM dbo.LK_EVENT_TYPE_TME_SLOT WITH (NOLOCK)    
		 WHERE TME_SLOT_ID  = @TME_SLOT_ID    
		 AND  EVENT_TYPE_ID = 5

		SELECT @ListTxt = EVENT_ID_LIST_TXT
		FROM dbo.EVENT_TYPE_RSRC_AVLBLTY	WITH (NOLOCK)
		WHERE	EVENT_TYPE_RSRC_AVLBLTY_ID = @ID     
			AND EVENT_TYPE_ID = 5

		IF @ListTxt IS NULL
			SET @ListTxt = '<EventIDs><Decrement /><Increment><Event><ID>'+convert(varchar,@EVENT_ID)+'</ID><DT>'+convert(varchar,GETDATE(), 126)+'</DT></Event></Increment><Active><EventID>'+convert(varchar,@EVENT_ID)+'</EventID></Active></EventIDs>'
		ELSE
		BEGIN
			SET @ListTxt.modify('insert <Event><ID>{sql:variable("@EVENT_ID")}</ID><DT>{sql:variable("@Today")}</DT></Event> into (/EventIDs/Increment)[1]')
			SET @ListTxt.modify('delete /EventIDs/Active/EventID[text() = sql:variable("@EVENT_ID")]')
			SET @ListTxt.modify('insert <EventID>{sql:variable("@EVENT_ID")}</EventID> into (/EventIDs/Active)[1]')
		END

		SET @EvCnt = COALESCE(@ListTxt.value('count(/EventIDs/Active/EventID)', 'INT'), 0)

		UPDATE  dbo.EVENT_TYPE_RSRC_AVLBLTY WITH (ROWLOCK)    
		SET  CURR_AVAL_TME_SLOT_CNT  = @EvCnt,
			 EVENT_ID_LIST_TXT		=	@ListTxt
		WHERE EVENT_TYPE_RSRC_AVLBLTY_ID = @ID     
		  AND EVENT_TYPE_ID = 5

	END
  END     
    
End Try    
    
Begin Catch    
 EXEC [dbo].[insertErrorInfo]    
End Catch    
END 