USE [COWS]
GO
_CreateObject 'SP','dbo','insertOrderAddressInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/13/2011
-- Description:	Inserts the FSA record into the ORDR_ADR table.
-- =========================================================
-- Author:		mjl0081
-- Create date: 04/28/2015
-- Description:	Added new fields for PJ8006
-- =========================================================
ALTER PROCEDURE [dbo].[insertOrderAddressInfo]
	@ORDR_ADR_ID		Int		OUT
	,@ORDR_ID			Int
	,@ADR_TYPE_ID		TinyInt
	,@STREET_ADR_1		Varchar(50)
	,@STREET_ADR_2		Varchar(50)
	,@CTY_NME			Varchar(20)
	,@PRVN_NME			Varchar(200)
	,@STT_CD			Varchar(2)
	,@ZIP_PSTL_CD		Varchar(20)
	,@BLDG_NME			Varchar(50)
	,@FLR_ID			Varchar(10)
	,@RM_NBR			Varchar(10)
	,@CREAT_BY_USER_ID	Int
	,@CTRY_CD			Varchar(2)
	,@CIS_LVL_TYPE		Varchar(2)
	,@FSA_MDUL_ID		Varchar(3)	=	NULL	
	,@HIER_LVL_CD		Bit			=	0
AS
BEGIN
SET NOCOUNT ON;

IF	ISNULL(@ADR_TYPE_ID, 0)	!=	0
AND	(ISNULL(@STT_CD, 0)		!=	''	
OR	ISNULL(@CTRY_CD, 0)		!=	'')
	BEGIN
		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	    
		INSERT INTO dbo.ORDR_ADR WITH (ROWLOCK)
						(ORDR_ID
						,ADR_TYPE_ID
						,STREET_ADR_1
						,STREET_ADR_2
						,CTY_NME
						,PRVN_NME
						,STT_CD
						,ZIP_PSTL_CD
						,BLDG_NME
						,FLR_ID
						,RM_NBR
						,CREAT_BY_USER_ID
						,CTRY_CD
						,CIS_LVL_TYPE
						,FSA_MDUL_ID
						,HIER_LVL_CD
						)
			VALUES		(@ORDR_ID
						,@ADR_TYPE_ID
						,dbo.encryptString(@STREET_ADR_1)
						,dbo.encryptString(@STREET_ADR_2)
						,dbo.encryptString(@CTY_NME)
						,dbo.encryptString(@PRVN_NME)
						,dbo.encryptString(@STT_CD)
						,dbo.encryptString(@ZIP_PSTL_CD)
						,dbo.encryptString(@BLDG_NME)
						,dbo.encryptString(@FLR_ID)
						,dbo.encryptString(@RM_NBR)
						,@CREAT_BY_USER_ID
						,Case	ISNULL(@CTRY_CD, '')
							When	''	Then	NULL
							Else	@CTRY_CD
						End
						,@CIS_LVL_TYPE
						,@FSA_MDUL_ID
						,@HIER_LVL_CD
						)
						
		SELECT	@ORDR_ADR_ID = SCOPE_IDENTITY()		
		
		IF	ISNULL(@CIS_LVL_TYPE, '')	=	'H5'	
		AND	@HIER_LVL_CD				=	1
			BEGIN
				DECLARE @RGN_ID	TinyInt
				SELECT		@RGN_ID	=	RGN_ID	 
					FROM	dbo.LK_CTRY	WITH (NOLOCK)
					WHERE	CTRY_CD	=	@CTRY_CD
					
				UPDATE		dbo.ORDR	WITH (ROWLOCK)
					SET		RGN_ID	=	@RGN_ID
					WHERE	ORDR_ID	=	@ORDR_ID
					AND		@RGN_ID	IS	NOT NULL
			END
			
	END
END
GO