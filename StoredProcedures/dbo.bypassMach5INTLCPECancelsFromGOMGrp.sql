USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[bypassMach5INTLCPECancelsFromGOMGrp]    Script Date: 02/03/2020 11:18:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[bypassMach5INTLCPECancelsFromGOMGrp]
	@OrderID	Int,
	@TaskID		SmallInt,
	@TaskStatus	TinyInt,
	@Comments	Varchar(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;

    IF EXISTS
		(SELECT 'X'
			FROM dbo.ORDR WITH (NOLOCK)
			WHERE ORDR_ID = @OrderID
				AND ORDR_CAT_ID = 6
		)
		BEGIN
			IF EXISTS
				(
					SELECT 'X'
						FROM FSA_ORDR WITH (NOLOCK)
						WHERE ORDR_ID = @OrderID
							AND FTN like 'CN%'
				)
				BEGIN
					EXEC dbo.CompleteActiveTask @orderID,@TaskID,2,'Bypassing GOM Cancel Ready Task'
				END
				
		END
END
