USE [COWS_Reporting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='getCannedFilesByReportID')
EXEC ('CREATE PROCEDURE [dbo].[getCannedFilesByReportID] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

-- =========================================================
-- Author:		jrg7298
-- Create date: 11/05/2013
-- Description:	Gets the List of Report Files using ReportID and CSGLevel.
-- =========================================================
ALTER PROCEDURE [dbo].[getCannedFilesByReportID] --40, 3
@REPORTID int,
@CSGLEVEL int,
@ReportType CHAR(1),
@StartDt DateTime = null,
@EndDt DateTime = null
AS
BEGIN TRY

SET NOCOUNT ON;

--Select report file names from Reporting Database 
-- Automation service will populate the ReportFileWeb table in Reporting database on a daily basis
-- This table will always contain only the current file details.

-- CHECK user CSG level
-- If CSG LEVEL = 3 the  get files for FolderTypeCD = "S"
-- If CSGLEVEL = 2 or 1  get files for FolderTypeCD = "X"
   -- If rowcount == 0 i.e no encrypted data for this report ID then get files for FolderTypeCD = "S"

DECLARE @Temp TABLE
(FullPath VARCHAR(500),
FileNamePattern VARCHAR(100),
[FileName] VARCHAR(100),
FileSizeMb FLOAT,
FileDate SMALLDATETIME)

IF  (@ReportType = 'D')
BEGIN

INSERT INTO @Temp
SELECT TOP 1 FullPath ,FileNamePattern ,
FileName ,FileSizeMb ,FileDate
FROM RAD_Reporting.rad.ReportFileWeb RF with (nolock)
where RF.REPORTID = @REPORTID and (FolderTypeCD = CASE WHEN (@CSGLEVEL = 3) THEN 'S' 
													  WHEN (@CSGLEVEL = 2) THEN 'X'
													  WHEN (@CSGLEVEL = 1) THEN 'X'
													  ELSE 'S' END)

 and ((@StartDt is null) or (CONVERT(DATE,RF.FileDate) >= CONVERT(DATE,@StartDt)))
 and ((@EndDt is null) or (CONVERT(DATE,RF.FileDate) <= CONVERT(DATE,@EndDt)))
 --order by RF.FileDate desc
 order by FileDate DESC, SUBSTRING(CONVERT(char, filename, 120), 2, 10) DESC  
 
 IF NOT EXISTS (SELECT 'x' FROM @Temp)
 BEGIN
	INSERT INTO @Temp
	SELECT TOP 1 FullPath ,FileNamePattern ,
	FileName ,FileSizeMb ,FileDate
	FROM RAD_Reporting.rad.ReportFileWeb RF with (nolock)
	where RF.REPORTID = @REPORTID and (FolderTypeCD = 'S')

	 and ((@StartDt is null) or (CONVERT(DATE,RF.FileDate) >= CONVERT(DATE,@StartDt)))
	 and ((@EndDt is null) or (CONVERT(DATE,RF.FileDate) <= CONVERT(DATE,@EndDt)))
	 --order by RF.FileDate desc
	 order by FileDate DESC, SUBSTRING(CONVERT(char, filename, 120), 2, 10) DESC  
 END

 SELECT * FROM @Temp

END

ELSE IF  (@ReportType IN ('W', 'M'))
BEGIN
INSERT INTO @Temp
SELECT TOP 1 FullPath ,FileNamePattern ,
FileName ,FileSizeMb ,FileDate
FROM RAD_Reporting.rad.ReportFileWeb RF with (nolock)
where RF.REPORTID = @REPORTID and (FolderTypeCD = CASE WHEN (@CSGLEVEL = 3) THEN 'S' 
													  WHEN (@CSGLEVEL = 2) THEN 'X'
													  WHEN (@CSGLEVEL = 1) THEN 'X'
													  ELSE 'S' END)

 and ((@StartDt is null) or (CONVERT(DATE,RF.FileDate) <= CONVERT(DATE,@StartDt)))
 and ((@EndDt is null) or (CONVERT(DATE,RF.FileDate) <= CONVERT(DATE,@EndDt)))
 --order by RF.FileDate desc
 order by FileDate DESC, SUBSTRING(CONVERT(char, filename, 120), 2, 10) DESC  
 
 IF NOT EXISTS (SELECT 'x' FROM @Temp)
 BEGIN
	INSERT INTO @Temp
	SELECT TOP 1 FullPath ,FileNamePattern ,
	FileName ,FileSizeMb ,FileDate
	FROM RAD_Reporting.rad.ReportFileWeb RF with (nolock)
	where RF.REPORTID = @REPORTID and (FolderTypeCD = 'S')

	 and ((@StartDt is null) or (CONVERT(DATE,RF.FileDate) <= CONVERT(DATE,@StartDt)))
	 and ((@EndDt is null) or (CONVERT(DATE,RF.FileDate) <= CONVERT(DATE,@EndDt)))
	 --order by RF.FileDate desc
	 order by FileDate DESC, SUBSTRING(CONVERT(char, filename, 120), 2, 10) DESC  	
 END

 SELECT * FROM @Temp

END
	
END TRY

BEGIN CATCH
END CATCH