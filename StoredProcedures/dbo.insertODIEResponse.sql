USE [COWS]
GO
_CreateObject 'SP','dbo','insertODIEResponse'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		sbg9814
-- Create date: 07/15/2011
-- Description:	Inserts an ODIE Response into the ODIE_RSPN table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertODIEResponse]
	@REQ_ID					Int
	,@RSPN_DT				DateTime
	,@CUST_TEAM_PDL			Varchar(200)
	,@MNSPM_ID				Varchar(10)
	,@NTEAssigned			Varchar(10)
	,@SOWS_FOLDR_PATH_NME	Varchar(300)
	,@RSPN_ERROR_TXT		Varchar(100)
	,@CUST_NME_LIST			Varchar(Max)
	,@ACK_CD				Bit
	,@ODIE_CUST_ID			Varchar(10)
	,@SDEAssigned			Varchar(10) = ''
AS
BEGIN
SET NOCOUNT ON;
DECLARE	@RSPN_ID	Int
DECLARE	@ORDR_ID	Int
DECLARE	@CurrDate	DateTime
DECLARE @CSGLvlID   TINYINT
SET	@RSPN_ID	=	0
SET	@ORDR_ID	=	0	
SET	@CurrDate	=	getDate()

Begin Try

OPEN SYMMETRIC KEY FS@K3y 
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
 
	SELECT		@ORDR_ID	=	ISNULL(ORDR_ID, 0), @CSGLvlID = CSG_LVL_ID
		FROM	dbo.ODIE_REQ	WITH (NOLOCK)
		WHERE	REQ_ID		=	@REQ_ID
		
		----------------------------------------------------------
		-- Mark the Request to Response received status.
		----------------------------------------------------------
		IF	(@ORDR_ID !=	0)
			BEGIN
				EXEC	[dbo].[updateODIERequest]	@REQ_ID, 21
			END

		----------------------------------------------------------
		-- Load the ODIE_RSPN Table with appropriate info.
		----------------------------------------------------------
		IF (@CSGLvlID>0)
		BEGIN
			INSERT INTO dbo.ODIE_RSPN WITH (ROWLOCK)
							(REQ_ID
							,RSPN_DT
							,CUST_TEAM_PDL
							,MNSPM_ID
							,NTE_ID
							,SOWS_FOLDR_PATH_NME
							,RSPN_ERROR_TXT
							,CUST_NME
							,ACT_CD
							,ODIE_CUST_ID
							,ACK_CD
							,SDE_Assigned)
				VALUES		(@REQ_ID
							,@RSPN_DT
							,@CUST_TEAM_PDL
							,@MNSPM_ID
							,@NTEAssigned
							,@SOWS_FOLDR_PATH_NME
							,@RSPN_ERROR_TXT
							,NULL
							,1
							,@ODIE_CUST_ID
							,@ACK_CD
							,@SDEAssigned)

			SET @RSPN_ID = SCOPE_IDENTITY()
			
			INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME])
			SELECT @RSPN_ID, 13, dbo.encryptString(@CUST_NME_LIST)
		END
		ELSE
		BEGIN
			INSERT INTO dbo.ODIE_RSPN WITH (ROWLOCK)
							(REQ_ID
							,RSPN_DT
							,CUST_TEAM_PDL
							,MNSPM_ID
							,NTE_ID
							,SOWS_FOLDR_PATH_NME
							,RSPN_ERROR_TXT
							,CUST_NME
							,ACT_CD
							,ODIE_CUST_ID
							,ACK_CD
							,SDE_Assigned)
				VALUES		(@REQ_ID
							,@RSPN_DT
							,@CUST_TEAM_PDL
							,@MNSPM_ID
							,@NTEAssigned
							,@SOWS_FOLDR_PATH_NME
							,@RSPN_ERROR_TXT
							,@CUST_NME_LIST
							,1
							,@ODIE_CUST_ID
							,@ACK_CD
							,@SDEAssigned)
		END
		
		----------------------------------------------------------
		-- Load the WFM Record.
		----------------------------------------------------------
		EXEC	[dbo].[insertWFMOrderAssignment]	
				@ORDR_ID
				,2
				,@MNSPM_ID
				,1
				,@CurrDate
				,0			
End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END
GO