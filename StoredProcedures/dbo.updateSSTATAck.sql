USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[updateSSTATAck]    Script Date: 10/06/2015 09:56:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 10/06/2015
-- Description:	Update that the ACK has been sent to SSTAT.
-- =========================================================
CREATE PROCEDURE [dbo].[updateSSTATAck]
	@TRANID		Int
	,@ACK_CD	bit

AS
BEGIN
SET NOCOUNT ON;
Begin Try

		UPDATE		dbo.SSTAT_RSPN	WITH (ROWLOCK)
				SET		ACK_CD		=	@ACK_CD
				WHERE	TRAN_ID		=	@TRANID
					
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
