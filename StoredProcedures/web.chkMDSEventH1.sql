﻿USE [COWS]
GO
_CreateObject 'SP','web','chkMDSEventH1'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
 --=====================================================================================================    
 -- Author: Suman Chitemella    
 -- Create Date:     
 --Description: Used to check if H1 is present in the FSA_ORDR table for MDS_EVENT creation.     
 --   If it doesn't exists    
 --    Return 0    
 --      if it exists    
 --    if H1 is secure & user CSG matches H1 CSG    
 --     Return ORDR_ID      
 --    else    
 --     Return -1     
 --=====================================================================================================    
    
 ALTER PROCEDURE [web].[chkMDSEventH1]   --'926868247', 46, '0'
  @H1 BIGINT,    
  @UserID INT,    
  @sOrdrSecure VARCHAR(50) OUTPUT    
 AS    
  DECLARE @iOrdrID INT    
  DECLARE @ordrCSG INT    
  DECLARE @CSG_USER INT    
  DECLARE @ordr_Secure varchar(1)    
  DECLARE @ORDR_CHARS_ID VARCHAR(20)
  DECLARE @APPLICATIONIDENTIFIER VARCHAR(3)
 
 BEGIN    
  SET NOCOUNT ON    
  BEGIN TRY    
     
   --SET @iOrderID = 0    
   SET @iOrdrID = -1    
   SET @ordrCSG = 0    
   SET @CSG_USER = 0    
   SET @ordr_Secure = 'N'
   SET @ORDR_CHARS_ID = ''    
   SET @APPLICATIONIDENTIFIER = 'QDA'
      
   SELECT TOP 1 @iOrdrID = ordr.ORDR_ID, @ordrCSG = ordr.CSG_LVL_ID, @ORDR_CHARS_ID = ISNULL(ordr.CHARS_ID ,'')
       FROM FSA_ORDR_CUST foc  WITH (NOLOCK)  
			INNER JOIN ORDR ordr WITH (NOLOCK) ON ordr.ORDR_ID = foc.ORDR_ID    
	   WHERE foc.CIS_LVL_TYPE = 'H1'     
			AND foc.CUST_ID = @H1    
	   ORDER BY ordr.ORDR_ID DESC    
      
   IF ((@iOrdrID > 0) AND (@ordrCSG > 0))  
   BEGIN    
	IF EXISTS (SELECT 'X' FROM dbo.USER_CSG_LVL WITH (NOLOCK) WHERE user_id = @UserID AND CSG_LVL_ID!=0 AND CSG_LVL_ID <= @ordrCSG)    
    BEGIN    
		SET @ordr_Secure = 'Y'    
		SET @sOrdrSecure = CONVERT(varchar,@iOrdrID) + ','+ @ordr_Secure + ',' + CONVERT(varchar,@ordrCSG) + ','+ @ORDR_CHARS_ID
    END    
    ELSE    
    BEGIN    
		SET @sOrdrSecure = '-1,'+ @ordr_Secure  + ',' + CONVERT(varchar,@ordrCSG) + ','+ @ORDR_CHARS_ID
    END    
   END    
   ELSE IF ((@iOrdrID > 0) AND (@ordrCSG = 0))  
   BEGIN  
		SET @sOrdrSecure = CONVERT(varchar,@iOrdrID) + ','+ @ordr_Secure + ',' + CONVERT(varchar,@ordrCSG)+ ','+ @ORDR_CHARS_ID
   END  
   ELSE IF (@iOrdrID = -1)
   BEGIN
		IF	OBJECT_ID(N'tempdb..#CUST_INFO_SENSITIVE_ALL', N'U') IS NOT NULL 
		DROP TABLE #CUST_INFO_SENSITIVE_ALL

		CREATE TABLE #CUST_INFO_SENSITIVE_ALL
									(
										[CUST_CHARS_ID]			VARCHAR(20)	NULL,
										[SCTY_GRP_CD]			Varchar(5)	NULL
									) 

	DECLARE @CMD2 AS NVARCHAR(MAX)

		SET @CMD2 = 'INSERT INTO #CUST_INFO_SENSITIVE_ALL ' +
		'([CUST_CHARS_ID],[SCTY_GRP_CD]) ' +
		'SELECT CHARS_CUST_ID,CSG_LVL ' +
		'FROM OPENQUERY ' +
		'(M5, ' +
		'''select CHARS_CUST_ID,CSG_LVL ' +
		'from mach5.v_v5u_cust_acct S ' +
		'WHERE ' +
				'((S.CIS_HIER_LVL_CD=''''H1'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY''''))
				  OR  (S.CIS_HIER_LVL_CD=''''H4'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY''''))
				  OR  (S.CIS_HIER_LVL_CD=''''H6'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))' +
				'AND S.CIS_CUST_ID = ''''' + CONVERT(VARCHAR, @H1) + ''''' '') '
		
		EXECUTE sp_executesql @CMD2

		SELECT TOP 1 @ordrCSG = ISNULL(SCTY_GRP_CD,0), @ORDR_CHARS_ID = ISNULL(CUST_CHARS_ID,'') FROM #CUST_INFO_SENSITIVE_ALL

		IF EXISTS (SELECT 'X' FROM #CUST_INFO_SENSITIVE_ALL)
		BEGIN
			IF EXISTS (SELECT 'X' FROM dbo.USER_CSG_LVL WITH (NOLOCK) WHERE user_id = @UserID AND CSG_LVL_ID!=0 AND CSG_LVL_ID <= @ordrCSG)
			BEGIN
				SET @ordr_Secure = 'Y'
				SELECT TOP 1 @sOrdrSecure = '-1,' + ',Y,' + CONVERT(varchar,@ordrCSG) + ','+ @ORDR_CHARS_ID  FROM #CUST_INFO_SENSITIVE_ALL
			END
			ELSE
				SELECT TOP 1 @sOrdrSecure = '-1,'+ @ordr_Secure + ','+ CONVERT(varchar,@ordrCSG) + ','+ @ORDR_CHARS_ID  FROM #CUST_INFO_SENSITIVE_ALL
		END
		ELSE
			SET @sOrdrSecure = '0,' + @ordr_Secure  + ',' + CONVERT(varchar,@ordrCSG)+ ','+ @ORDR_CHARS_ID
		
   END  
   ELSE 
   BEGIN    
    SET @sOrdrSecure = '0,' + @ordr_Secure  + ',' + CONVERT(varchar,@ordrCSG)+ ','+ @ORDR_CHARS_ID  
   END    
     
   ----RETURN @sOrdrSecure    
     
  END TRY    
  BEGIN CATCH    
   EXEC [dbo].[insertErrorInfo]      
   DECLARE @ErrMsg nVarchar(4000),       
     @ErrSeverity Int            
   SELECT @ErrMsg  = ERROR_MESSAGE(),            
    @ErrSeverity = ERROR_SEVERITY()            
   RAISERROR(@ErrMsg, @ErrSeverity, 1)     
  END CATCH    
    
 END