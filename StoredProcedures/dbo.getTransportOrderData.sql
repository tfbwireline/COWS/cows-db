USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getTransportOrderData]    Script Date: 11/12/2020 12:58:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Ramesh Ragi>,Modified by chakri
-- Create date: <08/08/2011>
-- Description:	<Retrieving > Added Category id logic to include IPL Orders
-- =============================================
-- Modify:		<Kyle Wichert>
-- Create date: <09/29/2011>
-- Description:	<Modify for storing Sent/Ack Milestones in new place
-- =============================================
-- Modify: <David Phillips>
-- Create date: <10/28/2013>
-- Description: Changed Email to use function dbo.decryptBinaryData() instead of DecryptByKey() in Contact Info sections.
--				DecryptByKey only returned 30 bytes and field was expanded to 40.
-- kh946640 03/22/18 COWS DB Restructuring as columns have been moved from fsa_ordr->fsa_ordr_cpe_line_item table for DualPort project(PJ020783 - CR93)
-- =============================================
--Exec dbo.getTransportOrderData 8634--5833 -- 5849

ALTER PROCEDURE [dbo].[getTransportOrderData]
	@iOrderID	INT,
	@iIsTerminating BIT = 0
AS
BEGIN
	BEGIN TRY
	
	OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	DECLARE @CAT_ID smallint
	
	SELECT @CAT_ID=ORDR_CAT_ID FROM dbo.ORDR WITH (NOLOCK) WHERE ORDR_ID=@iOrderID
	
	IF (@CAT_ID IN (1,5))
		BEGIN
			SELECT 
				NULL AS Interface
				,f.CKT_SPD_QTY AS AccessSpeed
				,ISNULL(t.SALS_CHNL_ID,0)  AS SalesChannelID
				,ISNULL(t.ORDR_ASN_VW_DT,'') AS OrderAssignedDate
				,ISNULL(t.CUST_BILL_CUR_ID,'') AS CustBillCurrency
				,ISNULL(t.CKT_LGTH_IN_KMS_QTY,0) AS LengthOfCkt
				,ISNULL(t.ACCS_MB_DES,'') AS AccessMBValue
				,ISNULL(t.VNDR_LOCAL_ACCS_PRVDR_GRP_NME,'') AS VAPGName
				,ISNULL(t.VNDR_TYPE_ID,0) AS VendorID
				,ISNULL(t.IPT_AVLBLTY_ID,0) AS IPT
				,ISNULL(t.A_END_SPCL_CUST_ACCS_DETL_TXT,'') AS SCADText
				,ISNULL(t.A_END_NEW_CKT_CD,	'0') AS IsNewCkt
				,ISNULL(t.A_END_RGN_ID,ISNULL(o.RGN_ID,1)) AS ARgnID
				,ISNULL(lv.CXR_NME, '') + '-' + ISNULL(lv.CXR_CD,'') AS Vendor
				,ISNULL(u.FULL_NME,'') AS ManagerName
				,ISNULL(t.VNDR_ORDR_ID,'') AS VendorOrder
				,ISNULL(t.ACCS_CTY_SITE_ID, 0) AS AccCitySite
				,'' AS PRS_QOT_NBR
				,'' AS GCS_NBR
				,'' AS RFQ_NBR
				,ISNULL(t.NRM_UPDT_CD,'0') AS NRM_UPDT_CD
				,ISNULL(dbo.convertOrderMilestonedates(t.CNFRM_RNL_DT),'') AS CNFRM_RNL_DT
				,'' AS PreQualNumber
				FROM	dbo.IPL_ORDR	f	WITH (NOLOCK)
			INNER JOIN	dbo.ORDR		o	WITH (NOLOCK)	ON	o.ORDR_ID	=	f.ORDR_ID
			LEFT JOIN	dbo.TRPT_ORDR	t	WITH (NOLOCK)	ON	t.ORDR_ID	=	f.ORDR_ID
			LEFT JOIN	dbo.LK_USER		u	WITH (NOLOCK)	ON	u.[USER_ID]	=	t.XNCI_ASN_MGR_ID
			LEFT JOIN	dbo.IPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	o.ORDR_ID		=	la.ORDR_ID
																	AND	la.ACCS_TYPE_ID	=	'Terminating'
			LEFT JOIN	dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD
				WHERE	f.ORDR_ID	=	@iOrderID
				
				
			SELECT	TOP 1	f.ORDR_ID AS OrderID
						,ISNULL(o.VER_ID,1) AS VersionID
						,ISNULL(dbo.convertOrderMilestonedates(ISNULL(od.CUST_CMMT_DT,f.CUST_CMMT_DT)),'') AS CCD
						,ISNULL(dbo.convertOrderMilestonedates(f.CUST_WANT_DT),'') AS CWD
						,ISNULL(dbo.convertOrderMilestonedates(o.PRE_SBMT_DT),'') AS PreSubmitDate
						,ISNULL(dbo.convertOrderMilestonedates(o.SBMT_DT),'') AS SubmitDate
						,ISNULL(dbo.convertOrderMilestonedates(o.VLDTD_DT),'') AS ValidatedDate
						,ISNULL(dbo.convertOrderMilestonedates(o.ORDR_BILL_CLEAR_INSTL_DT),'') AS BillClearDate
						,ISNULL(dbo.convertOrderMilestonedates(o.ORDR_DSCNCT_DT),'') AS OrdrDiscDate
						,ISNULL(dbo.convertOrderMilestonedates(o.VNDR_CNCLN_DT),'') AS VendorCancellationDate
						,ISNULL(dbo.convertOrderMilestonedates(o.CUST_ACPTC_TURNUP_DT),'') AS CUST_ACPTC_TURNUP_DT
						,o.CREAT_BY_USER_ID AS CreatedUserID
						,o.CREAT_DT AS CreatedDate
						,ISNULL(dbo.convertOrderMilestonedates(o.XNCI_CNFRM_RNL_DT),'') AS XNCI_CNFRM_RNL_DT
						,o.RNL_DT_REQR_CD AS RNL_DT_REQR_CD
				FROM	dbo.IPL_ORDR	f	WITH (NOLOCK)
			INNER JOIN	dbo.ORDR		od	WITH (NOLOCK)	ON	od.ORDR_ID	=	f.ORDR_ID	
			LEFT JOIN	dbo.ORDR_MS		o	WITH (NOLOCK)	ON	o.ORDR_ID	=	f.ORDR_ID 
			
				WHERE	f.ORDR_ID = @iOrderID
				ORDER BY o.VER_ID DESC	
		END
	ELSE IF @CAT_ID=4
		BEGIN
			SELECT 
				NULL AS Interface
				--,f.CKT_SPD_QTY AS AccessSpeed
				,'' AS AccessSpeed
				,ISNULL(t.SALS_CHNL_ID,0)  AS SalesChannelID
				,ISNULL(t.ORDR_ASN_VW_DT,'') AS OrderAssignedDate
				,ISNULL(t.CUST_BILL_CUR_ID,'') AS CustBillCurrency
				,ISNULL(t.CKT_LGTH_IN_KMS_QTY,0) AS LengthOfCkt
				,ISNULL(t.ACCS_MB_DES,'') AS AccessMBValue
				,ISNULL(t.VNDR_LOCAL_ACCS_PRVDR_GRP_NME,'') AS VAPGName
				,ISNULL(t.VNDR_TYPE_ID,0) AS VendorID
				,ISNULL(t.IPT_AVLBLTY_ID,0) AS IPT
				,ISNULL(t.A_END_SPCL_CUST_ACCS_DETL_TXT,'') AS SCADText
				,ISNULL(t.A_END_NEW_CKT_CD,	'0') AS IsNewCkt
				,ISNULL(t.A_END_RGN_ID,ISNULL(o.RGN_ID,1)) AS ARgnID
				,ISNULL(lv.VNDR_NME,'') + '-' +  ISNULL(lv.vndr_cd,'') AS Vendor
				,ISNULL(u.FULL_NME,'') AS ManagerNameVendor
				,ISNULL(t.VNDR_ORDR_ID,'') AS VendorOrder
				,ISNULL(t.ACCS_CTY_SITE_ID, 0) AS AccCitySite
				,'' AS PRS_QOT_NBR
				,'' AS GCS_NBR
				,'' AS RFQ_NBR
				,ISNULL(t.NRM_UPDT_CD,'0') AS NRM_UPDT_CD
				,ISNULL(dbo.convertOrderMilestonedates(t.CNFRM_RNL_DT),'') AS CNFRM_RNL_DT
				,'' AS PreQualNumber
				FROM	dbo.NCCO_ORDR	ncco	WITH (NOLOCK)
			INNER JOIN	dbo.ORDR		o	WITH (NOLOCK)	ON	o.ORDR_ID	=	ncco.ORDR_ID
			LEFT JOIN	dbo.TRPT_ORDR	t	WITH (NOLOCK)	ON	t.ORDR_ID	=	ncco.ORDR_ID
			LEFT JOIN	dbo.LK_USER		u	WITH (NOLOCK)	ON	u.[USER_ID]	=	t.XNCI_ASN_MGR_ID
			LEFT JOIN	dbo.LK_VNDR		lv WITH (NOLOCK) ON lv.VNDR_CD = ncco.VNDR_CD
				WHERE	ncco.ORDR_ID	=	@iOrderID
				
				
			SELECT	TOP 1	ncco.ORDR_ID AS OrderID
						,ISNULL(o.VER_ID,1) AS VersionID
						,ISNULL(dbo.convertOrderMilestonedates(ISNULL(od.CUST_CMMT_DT,ncco.CCS_DT)),'') AS CCD
						,ISNULL(dbo.convertOrderMilestonedates(ncco.CWD_DT),'') AS CWD
						,ISNULL(dbo.convertOrderMilestonedates(o.PRE_SBMT_DT),'') AS PreSubmitDate
						,ISNULL(dbo.convertOrderMilestonedates(o.SBMT_DT),'') AS SubmitDate
						,ISNULL(dbo.convertOrderMilestonedates(o.VLDTD_DT),'') AS ValidatedDate
						,ISNULL(dbo.convertOrderMilestonedates(o.ORDR_BILL_CLEAR_INSTL_DT),'') AS BillClearDate
						,ISNULL(dbo.convertOrderMilestonedates(o.ORDR_DSCNCT_DT),'') AS OrdrDiscDate
						,ISNULL(dbo.convertOrderMilestonedates(o.VNDR_CNCLN_DT),'') AS VendorCancellationDate
						,ISNULL(dbo.convertOrderMilestonedates(o.CUST_ACPTC_TURNUP_DT),'') AS CUST_ACPTC_TURNUP_DT
						,o.CREAT_BY_USER_ID AS CreatedUserID
						,o.CREAT_DT AS CreatedDate
						,ISNULL(dbo.convertOrderMilestonedates(o.XNCI_CNFRM_RNL_DT),'') AS XNCI_CNFRM_RNL_DT
						,o.RNL_DT_REQR_CD AS RNL_DT_REQR_CD
				FROM	dbo.NCCO_ORDR	ncco	WITH (NOLOCK)
			INNER JOIN	dbo.ORDR		od	WITH (NOLOCK)	ON	od.ORDR_ID	=	ncco.ORDR_ID	
			LEFT JOIN	dbo.ORDR_MS		o	WITH (NOLOCK)	ON	o.ORDR_ID	=	ncco.ORDR_ID 
			
				WHERE	ncco.ORDR_ID = @iOrderID
				ORDER BY o.VER_ID DESC	
		END
	ELSE
		BEGIN
			SELECT TOP 1
				ISNULL(f.TTRPT_JACK_NRFC_TYPE_CD,'') AS Interface
				,COALESCE(pCPED.TTRPT_SPD_OF_SRVC_BDWD_DES,CPED.TTRPT_SPD_OF_SRVC_BDWD_DES,'') AS AccessSpeed
				,ISNULL(t.SALS_CHNL_ID,0)  AS SalesChannelID
				,ISNULL(t.ORDR_ASN_VW_DT,'') AS OrderAssignedDate
				,ISNULL(t.CUST_BILL_CUR_ID,'') AS CustBillCurrency
				,ISNULL(t.CKT_LGTH_IN_KMS_QTY,0) AS LengthOfCkt
				,ISNULL(t.ACCS_MB_DES,'') AS AccessMBValue
				,ISNULL(t.VNDR_LOCAL_ACCS_PRVDR_GRP_NME,'') AS VAPGName
				,ISNULL(t.VNDR_TYPE_ID,0) AS VendorID
				,ISNULL(t.IPT_AVLBLTY_ID,0) AS IPT
				,ISNULL(t.A_END_SPCL_CUST_ACCS_DETL_TXT,'') AS SCADText
				,ISNULL(t.A_END_NEW_CKT_CD,	'0') AS IsNewCkt
				,ISNULL(t.A_END_RGN_ID,ISNULL(o.RGN_ID,1)) AS ARgnID
				--,web.getVendorName(o.ORDR_ID) 	AS	Vendor
				,CASE o.PLTFRM_CD
								WHEN 'SF'	THEN ISNULL(sf.VNDR_NME,ISNULL(rsf.VNDR_NME,ISNULL(web.getVendorName(o.ORDR_ID, o.ORDR_CAT_ID, o.PLTFRM_CD, LOT.FSA_ORDR_TYPE_CD),lvo.VNDR_NME)))
								WHEN 'CP'	THEN ISNULL(cp.VNDR_NME,ISNULL(rcp.VNDR_NME,ISNULL(web.getVendorName(o.ORDR_ID, o.ORDR_CAT_ID, o.PLTFRM_CD, LOT.FSA_ORDR_TYPE_CD),lvo.VNDR_NME)))
								WHEN 'RS'	THEN ISNULL(rs.VNDR_NME,ISNULL(rrs.VNDR_NME,ISNULL(web.getVendorName(o.ORDR_ID, o.ORDR_CAT_ID, o.PLTFRM_CD, LOT.FSA_ORDR_TYPE_CD),lvo.VNDR_NME)))
								ELSE ''
								END AS	Vendor
				,ISNULL(u.FULL_NME,'') AS ManagerName
				,ISNULL(t.VNDR_ORDR_ID,'') AS VendorOrder
				,ISNULL(t.ACCS_CTY_SITE_ID, 0) AS AccCitySite
				,ISNULL(f.TSUP_PRS_QOT_NBR,'') AS PRS_QOT_NBR
				,ISNULL(f.TSUP_GCS_NBR,'') AS GCS_NBR
				,ISNULL(f.TSUP_RFQ_NBR,'') AS RFQ_NBR
				,ISNULL(t.NRM_UPDT_CD,'0') AS NRM_UPDT_CD
				,ISNULL(dbo.convertOrderMilestonedates(t.CNFRM_RNL_DT),'') AS CNFRM_RNL_DT
				,ISNULL(f.PRE_QUAL_NBR,'') AS PreQualNumber
				FROM	dbo.FSA_ORDR	f	WITH (NOLOCK)
			INNER JOIN	dbo.ORDR		o	WITH (NOLOCK)	ON	o.ORDR_ID	=	f.ORDR_ID
			INNER JOIN	dbo.LK_PPRT P WITH (NOLOCK) ON o.PPRT_ID = P.PPRT_ID
			INNER JOIN	dbo.LK_ORDR_TYPE LOT WITH (NOLOCK)ON P.ORDR_TYPE_ID = LOT.ORDR_TYPE_ID 
			LEFT JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM pCPED WITH (NOLOCK) ON pCPED.ORDR_ID= f.ORDR_ID AND pCPED.LINE_ITEM_CD='PRT'
			LEFT JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM CPED WITH (NOLOCK) ON CPED.ORDR_ID= f.ORDR_ID AND CPED.LINE_ITEM_CD='ACS'
			LEFT JOIN	dbo.TRPT_ORDR	t	WITH (NOLOCK)	ON	t.ORDR_ID	=	f.ORDR_ID
			LEFT JOIN	dbo.LK_USER		u	WITH (NOLOCK)	ON	u.[USER_ID]	=	t.XNCI_ASN_MGR_ID
			LEFT JOIN	dbo.LK_VNDR		sf	WITH (NOLOCK)			ON	sf.VNDR_CD		=	CPED.CXR_ACCS_CD
			LEFT JOIN	dbo.LK_VNDR		cp	WITH (NOLOCK)			ON	cp.VNDR_CD		=	f.VNDR_VPN_CD	
			LEFT JOIN	dbo.LK_VNDR		rs	WITH (NOLOCK)			ON	rs.VNDR_CD		=	f.INSTL_VNDR_CD																
			LEFT JOIN	dbo.FSA_ORDR	rfs	WITH (NOLOCK)			ON	rfs.FTN		=	f.RELTD_FTN 
			LEFT JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM rCPED WITH (NOLOCK) ON rCPED.ORDR_ID= rfs.ORDR_ID
			LEFT JOIN	dbo.LK_VNDR		rsf	WITH (NOLOCK)			ON	rsf.VNDR_CD		=	rCPED.CXR_ACCS_CD
			LEFT JOIN	dbo.LK_VNDR		rcp	WITH (NOLOCK)			ON	rcp.VNDR_CD		=	rfs.VNDR_VPN_CD	
			LEFT JOIN	dbo.LK_VNDR		rrs	WITH (NOLOCK)			ON	rrs.VNDR_CD		=	rfs.INSTL_VNDR_CD
			LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)			ON	vo.ORDR_ID		=	o.ORDR_ID
			LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)			ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
			LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)			ON	lvo.VNDR_CD = vf.VNDR_CD
				WHERE	f.ORDR_ID	=	@iOrderID
			
			
			SELECT	TOP 1 f.ORDR_ID AS OrderID
						,ISNULL(o.VER_ID,1) AS VersionID
						,ISNULL(dbo.convertOrderMilestonedates(ISNULL(od.CUST_CMMT_DT,f.CUST_CMMT_DT)),'') AS CCD
						,ISNULL(dbo.convertOrderMilestonedates(f.CUST_WANT_DT),'') AS CWD
						,ISNULL(dbo.convertOrderMilestonedates(o.PRE_SBMT_DT),'') AS PreSubmitDate
						,ISNULL(dbo.convertOrderMilestonedates(o.SBMT_DT),'') AS SubmitDate
						,ISNULL(dbo.convertOrderMilestonedates(o.VLDTD_DT),'') AS ValidatedDate
						,ISNULL(dbo.convertOrderMilestonedates(o.ORDR_BILL_CLEAR_INSTL_DT),'') AS BillClearDate
						,ISNULL(dbo.convertOrderMilestonedates(o.ORDR_DSCNCT_DT),'') AS OrdrDiscDate
						,ISNULL(dbo.convertOrderMilestonedates(o.VNDR_CNCLN_DT),'') AS VendorCancellationDate
						,ISNULL(dbo.convertOrderMilestonedates(o.CUST_ACPTC_TURNUP_DT),'') AS CUST_ACPTC_TURNUP_DT
						,o.CREAT_BY_USER_ID AS CreatedUserID
						,o.CREAT_DT AS CreatedDate
						,ISNULL(dbo.convertOrderMilestonedates(o.XNCI_CNFRM_RNL_DT),'') AS XNCI_CNFRM_RNL_DT
						,o.RNL_DT_REQR_CD AS RNL_DT_REQR_CD
				FROM	dbo.FSA_ORDR	f	WITH (NOLOCK)
			INNER JOIN	dbo.ORDR		od	WITH (NOLOCK)	ON	od.ORDR_ID	=	f.ORDR_ID
			LEFT JOIN	dbo.ORDR_MS		o	WITH (NOLOCK)	ON	o.ORDR_ID	=	f.ORDR_ID 
				WHERE	f.ORDR_ID = @iOrderID
				ORDER BY o.VER_ID DESC
		END		
	
	--Circuit MS	
	SELECT		ISNULL(c.CKT_ID,0) AS CktID
				,ISNULL(c.ORDR_ID,0) AS OrderID
				,ISNULL(cs.VER_ID,1) AS VersionID
				,ISNULL(c.VNDR_CKT_ID,'') AS VendorCktID
				,ISNULL(c.PL_SEQ_NBR,'') AS PLSeq
				,ISNULL(c.LEC_ID,0) AS LECID
				,ISNULL(dbo.convertOrderMilestonedates(cs.TRGT_DLVRY_DT_RECV_DT),'') AS TDDR
				,ISNULL(dbo.convertOrderMilestonedates(cs.TRGT_DLVRY_DT),'') AS  TDD
				,ISNULL(dbo.convertOrderMilestonedates(cs.ACCS_DLVRY_DT),'') AS [ADD]
				,ISNULL(dbo.convertOrderMilestonedates(cs.ACCS_ACPTC_DT),'') AS AAD
				,ISNULL(dbo.convertOrderMilestonedates(cs.VNDR_CNFRM_DSCNCT_DT),'') AS VCDD
				,ISNULL(cs.CNTRC_TERM_ID, ISNULL(f.TTRPT_VNDR_TERM,'')) AS TTRPT_ACCS_CNTRC_TERM_CD
				,ISNULL(dbo.convertOrderMilestonedates(cs.CNTRC_STRT_DT),'') AS CNTRC_STRT_DT
				,ISNULL(dbo.convertOrderMilestonedates(cs.CNTRC_END_DT),'') AS CNTRC_END_DT
				,ISNULL(dbo.convertOrderMilestonedates(cs.RNL_DT),'') AS RNL_DT
				,ISNULL(vo.TRMTG_CD,c.TRMTG_CD) AS TRMTG_CD
				,ISNULL(c.RLTD_VNDR_ORDR_ID,'') AS RLTD_VNDR_ORDR_ID,
				ISNULL(c.IP_ADR_QTY, '') AS IP_ADR_QTY,
				ISNULL(c.RTNG_PRCOL, '') AS RTNG_PRCOL,
				ISNULL(c.CUST_IP_ADR, '') AS CUST_IP_ADR,
				ISNULL(c.SCA_DET, '') AS SCA_DET
	FROM	dbo.CKT c WITH (NOLOCK)
	INNER JOIN  dbo.ORDR_MS		o	WITH (NOLOCK) ON	o.ORDR_ID		=	c.ORDR_ID
													AND	o.VER_ID		=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS WITH (NOLOCK) WHERE ORDR_ID = @iOrderID)
	LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK) ON	vo.VNDR_ORDR_ID	=	c.VNDR_ORDR_ID
	LEFT JOIN	dbo.CKT_MS		cs	WITH (NOLOCK) ON	cs.CKT_ID		=	c.CKT_ID
												 AND	cs.VER_ID		=	(SELECT	MAX(VER_ID) 
																				FROM	dbo.CKT_MS _cMS WITH (NOLOCK) 
																			INNER JOIN	dbo.CKT	_c WITH (NOLOCK) ON _c.CKT_ID = _cMS.CKT_ID
																				WHERE	_c.ORDR_ID = @iOrderID
																					AND _c.CKT_ID = cs.CKT_ID) 
	LEFT JOIN	dbo.FSA_ORDR	f	WITH (NOLOCK) ON c.ORDR_ID = f.ORDR_ID																	
		WHERE	c.ORDR_ID	= @iOrderID
			AND	ISNULL(vo.TRMTG_CD,@iIsTerminating) = @iIsTerminating
			
		
	--Account Team Member Contact Info
	SELECT		lr.ROLE_ID			AS RoleID,
				lrt.ROLE_TYPE_ID	AS RoleTypeID,
				CONVERT(varchar, DecryptByKey(oc.FRST_NME)) AS FirstName,
				CONVERT(varchar, DecryptByKey(oc.LST_NME)) AS LastName,
				ISNULL(oc.EMAIL_ADR,'') AS Email,
				oc.ORDR_CNTCT_ID	AS 	ORDR_CNTCT_ID,
				ISNULL(oc.PHN_NBR,'') AS 	PHN_NBR
		FROM	dbo.ORDR_CNTCT		oc	WITH (NOLOCK)
	INNER JOIN	dbo.LK_ROLE			lr	WITH (NOLOCK)	ON	lr.ROLE_ID			=	oc.ROLE_ID
	INNER JOIN	dbo.LK_ROLE_TYPE	lrt	WITH (NOLOCK)	ON	lrt.ROLE_TYPE_ID	=	lr.ROLE_TYPE_ID
		WHERE	oc.ORDR_ID	=	@iOrderID
			AND ((oc.FSA_MDUL_ID = 'ATM') AND (lr.ROLE_ID IN (10,12,13,14,63,66)) OR (lrt.ROLE_TYPE_ID = '3'))
		ORDER BY oc.ORDR_CNTCT_ID 

	--Customer Contact Info
	SELECT		lr.ROLE_ID			AS RoleID,
				lrt.ROLE_TYPE_ID	AS RoleTypeID,
				CONVERT(varchar, DecryptByKey(oc.FRST_NME)) AS FirstName,
				CONVERT(varchar, DecryptByKey(oc.LST_NME)) AS LastName,
				ISNULL(oc.EMAIL_ADR,'') AS Email,
				oc.ORDR_CNTCT_ID	AS 	ORDR_CNTCT_ID,
				ISNULL(oc.PHN_NBR,'') AS 	PHN_NBR
		FROM	dbo.ORDR_CNTCT		oc	WITH (NOLOCK)
	INNER JOIN	dbo.LK_ROLE			lr	WITH (NOLOCK)	ON	lr.ROLE_ID			=	oc.ROLE_ID
	INNER JOIN	dbo.LK_ROLE_TYPE	lrt	WITH (NOLOCK)	ON	lrt.ROLE_TYPE_ID	=	lr.ROLE_TYPE_ID
		WHERE	oc.ORDR_ID	=	@iOrderID
			AND ((lr.ROLE_ID	IN	(6,7)) OR (lrt.ROLE_TYPE_ID = '4')) --Customer Onsite && Alternate Site
		ORDER BY oc.ORDR_CNTCT_ID 
		
	--SLA Info
	SELECT *
	FROM dbo.ORDR_HOLD_MS WITH (NOLOCK)
	WHERE ORDR_ID = @iOrderID
	  --AND RELS_DT IS NULL
	  --AND RELS_BY_USER_ID IS NULL
	ORDER BY VER_ID DESC
	
	--VLAN IDs
	SELECT * 
	FROM dbo.ORDR_VLAN WITH (NOLOCK)
	WHERE	ORDR_ID	=	@iOrderID
		AND	VLAN_SRC_NME = 'COWS'


	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH	
	
END