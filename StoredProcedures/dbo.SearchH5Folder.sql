USE [COWS]
GO
_CreateObject 'SP','dbo','SearchH5Folder'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		km967761
-- Create date: 09/18/2020
-- Description:	Search H5 folder along with Secured Data if vetted user
-- =============================================
ALTER PROCEDURE [dbo].[SearchH5Folder] -- exec SearchH5Folder 0,0,'','','','',6244
	-- Add the parameters for the stored procedure here
	@H5FoldrId INT = 0,
	@CustId INT = 0,
	@Ftn VARCHAR(50) = '',
	@CustNme VARCHAR(250) = '',
	@City VARCHAR(100) = '',
	@Country VARCHAR(50) = '',
	@UserId INT = 0
AS
BEGIN
	SET NOCOUNT ON;

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;	
	
	DECLARE @CSG_LVL_ID INT = 0 
	SELECT TOP 1 @CSG_LVL_ID = CSG_LVL_ID FROM dbo.USER_CSG_LVL WITH (NOLOCK) WHERE USER_ID = @UserId ORDER BY CSG_LVL_ID ASC

    -- Insert statements for procedure here
	DECLARE @x NVARCHAR(MAX) = 'SELECT DISTINCT *
	FROM (SELECT 
			a.H5_FOLDR_ID AS [H5FoldrId],
			a.CUST_ID AS [CustId],
			CASE
				WHEN a.CSG_LVL_ID = 0 THEN a.CUST_CTY_NME
				WHEN '+CONVERT(VARCHAR,@CSG_LVL_ID)+' > 0 AND '+CONVERT(VARCHAR,@CSG_LVL_ID)+' <= a.CSG_LVL_ID THEN dbo.decryptBinaryData(d.CTY_NME)
				ELSE ''''
			END AS [CustCtyNme],
			CASE
				WHEN a.CSG_LVL_ID = 0 THEN a.CTRY_CD
				WHEN '+CONVERT(VARCHAR,@CSG_LVL_ID)+' > 0 AND '+CONVERT(VARCHAR,@CSG_LVL_ID)+' <= a.CSG_LVL_ID THEN dbo.decryptBinaryData(d.CTRY_CD)
				ELSE ''''
			END AS [CtryCd],
			CASE
				WHEN a.CSG_LVL_ID = 0 THEN a.CUST_NME
				WHEN '+CONVERT(VARCHAR,@CSG_LVL_ID)+' > 0 AND '+CONVERT(VARCHAR,@CSG_LVL_ID)+' <= a.CSG_LVL_ID THEN dbo.decryptBinaryData(d.CUST_NME)
				ELSE ''''
			END AS [CustNme],
			a.CSG_LVL_ID AS [CsgLvlId],
			a.CREAT_DT AS [CreatDt],
			a.MODFD_DT AS [ModfdDt],
			a.CREAT_BY_USER_ID AS [CreatByUserId],
			a.MODFD_BY_USER_ID AS [ModfdByUserId],
			a.REC_STUS_ID AS [RecStusId],
			e.USER_ADID AS [CreatByUserAdId]
		FROM dbo.H5_FOLDR a WITH (NOLOCK)
			LEFT JOIN dbo.ORDR b WITH (NOLOCK) ON b.H5_FOLDR_ID = a.H5_FOLDR_ID
			LEFT JOIN dbo.FSA_ORDR c WITH (NOLOCK) ON c.ORDR_ID = b.ORDR_ID
			LEFT JOIN dbo.CUST_SCRD_DATA d WITH (NOLOCK) ON d.SCRD_OBJ_ID = a.H5_FOLDR_ID AND d.SCRD_OBJ_TYPE_ID = 6
			LEFT JOIN dbo.LK_USER e WITH (NOLOCK) ON e.USER_ID = a.CREAT_BY_USER_ID
		WHERE
			('+CONVERT(VARCHAR,@H5FoldrId)+' = 0 OR ('+CONVERT(VARCHAR,@H5FoldrId)+' > 0 AND a.H5_FOLDR_ID = '+CONVERT(VARCHAR,@H5FoldrId)+'))
			AND ('+CONVERT(VARCHAR,@CustId)+' = 0 OR ('+CONVERT(VARCHAR,@CustId)+' > 0 AND a.CUST_ID = '+CONVERT(VARCHAR,@CustId)+'))
			AND ('''+@Ftn+''' = '''' OR ('''+@Ftn+''' <> '''' AND (c.FTN = '''+@Ftn+''' OR CONVERT(VARCHAR(50), b.ORDR_ID) = '''+@Ftn+''')))) a
	WHERE
		(''' + @CustNme + ''' = '''' OR (''' + @CustNme + ''' <> '''' AND a.CustNme LIKE ''%' + @CustNme + '%''))
		AND (''' + @City+ ''' = '''' OR (''' + @City+ ''' <> '''' AND a.CustCtyNme LIKE ''%' + @City+ '%''))
		AND (''' + @Country + ''' = '''' OR (''' + @Country + ''' <> '''' AND a.CtryCd = ''' + @Country + '''))'
		
	exec sp_executesql @x

END
GO
