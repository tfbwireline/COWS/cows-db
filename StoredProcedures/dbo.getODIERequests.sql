USE [COWS]
GO
_CreateObject 'SP','dbo','getODIERequests'
GO
USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getODIERequests]    Script Date: 09/16/2011 16:43:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/19/2011
-- Description:	Get the details for all ODIES requests
-- kh946640 07/23/18 Added CSG_LVL_ID column.
-- =========================================================
ALTER PROCEDURE [dbo].[getODIERequests]
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

	SELECT DISTINCT orq.REQ_ID	
					,orq.ODIE_MSG_ID
					,ISNULL(orq.H1_CUST_ID, '')							AS	H1_CUST_ID
					,CASE WHEN orq.CSG_LVL_ID=0 THEN ISNULL(orq.CUST_NME,'') ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'') END AS	CUST_NME
					,ISNULL(orq.ODIE_CUST_ID, '')						AS	ODIE_CUST_ID
					,ISNULL(orq.ODIE_CUST_INFO_REQ_CAT_TYPE_ID,0)		AS	ODIE_CUST_INFO_REQ_CAT_TYPE_ID
					,ISNULL(orq.DEV_FLTR,'')							AS	ODIE_DEVICE_FILTER
					,ISNULL(ord.CSG_LVL_ID,0)							AS  CSG_LVL_ID
		FROM		dbo.ODIE_REQ orq	WITH (NOLOCK)
		LEFT JOIN   dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=orq.REQ_ID AND csd.SCRD_OBJ_TYPE_ID=12
		LEFT JOIN   dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = orq.ORDR_ID
		WHERE		orq.STUS_ID		=	10
		
	SELECT DISTINCT	rq.REQ_ID
					,dq.ODIE_DISC_REQ_ID
					,ISNULL(dq.DEV_NME, '')	AS	DEV_NME 	
		FROM		dbo.ODIE_REQ		rq	WITH (NOLOCK)
		INNER JOIN	dbo.ODIE_DISC_REQ	dq	WITH (NOLOCK)	ON	rq.REQ_ID	=	dq.REQ_ID
		WHERE		rq.STUS_ID		=	10	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
