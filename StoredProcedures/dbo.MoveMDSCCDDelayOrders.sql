USE [COWS]
GO
_CreateObject 'SP','dbo','MoveMDSCCDDelayOrders'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	09/11/2011
-- Description:	CCD Delay Orders
 -- ================================================================================================
ALTER PROCEDURE [dbo].[MoveMDSCCDDelayOrders]
AS
BEGIN	
SET NOCOUNT ON;  
 DECLARE @cnt Int
 DECLARE @Ctr Int
 BEGIN TRY
  		SET @Cnt = 0
		SET @Ctr = 1
			
	DECLARE @EventCCD TABLE
	(
		EventID Int,
		CCD SmallDateTime,
		CCDDelay Int DEFAULT 0,
		Status Int DEFAULT 0
	)
	
	INSERT INTO @EventCCD (EventID, CCD, CCDDelay)
	SELECT mo.EVENT_ID,
		   fo.CUST_CMMT_DT,
		   CASE WHEN DATEDIFF (day, fo.CUST_CMMT_DT, GETDATE())> 5 THEN 1 ELSE 0 END	   
		   FROM FSA_MDS_EVENT_ORDR mo WITH (NOLOCK) 
	INNER JOIN FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = mo.ORDR_ID
	INNER JOIN MDS_EVENT_NEW me WITH (NOLOCK) ON me.EVENT_ID = mo.EVENT_ID
	WHERE me.EVENT_STUS_ID NOT IN (6)
	
		SELECT 	@Cnt	=	Count(1) FROM  @EventCCD  Where Status = 0 
   	
		IF @Cnt	>	0
		BEGIN
			WHILE	@Ctr <= @Cnt
				BEGIN 
				
				DECLARE @Delay Int
				DECLARE @EventID Int
				
				SELECT TOP 1 @Delay = CCDDelay,
							 @EventID = EventID
				 FROM @EventCCD   WHERE Status = 0
				
				IF (@Delay > 5)
				BEGIN
				
					UPDATE   MDS_EVENT_NEW WITH (ROWLOCK) SET  WRKFLW_STUS_ID  = 4 WHERE EVENT_ID = @EventID
				
				END
				
				SET @Ctr = @Ctr +1
				
				END		
	 END
 
END TRY
BEGIN CATCH         
		EXEC [dbo].[insertErrorInfo]          
END CATCH    


 SET NOCOUNT OFF
 
 END
 