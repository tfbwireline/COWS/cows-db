USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertMach5OrderDetails]    Script Date: 1/18/2022 2:45:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi/Matt Linnenbrink>
-- Create date: <08/14/2015>
-- Description:	<Insert Mach5 order>
-- jrg7298, 20170702, New extract codes 'M'(Missing Data), 'I'(COWS Internal Error) introduced
	-- which will be monitored to rectify/workaround instead of setting NRMBPM record to E
-- =============================================
--Exec dbo.insertMach5OrderDetails 13192,0,'DC',0
--Exec dbo.insertMach5OrderDetails 12346,0,'IN','',''
ALTER PROCEDURE [dbo].[insertMach5OrderDetails] --740850, 0, 'IN', '', ''
 @M5_ORDR_ID	Int ,
@M5_RELT_ORDR_ID Int,
@ORDR_TYPE_CD Varchar(5) ,
@QDA_ORDR_ID varchar(max) OUTPUT,
@DEVICE_ID Varchar(50) = ''
AS

BEGIN
DECLARE @ERRLog VARCHAR(MAX) = ''
DECLARE @TransName VARCHAR(MAX) = CONVERT(VARCHAR,@M5_ORDR_ID)+'_'+CONVERT(VARCHAR,ISNULL(@M5_RELT_ORDR_ID, '-'))+'_'+@ORDR_TYPE_CD+'_'+@DEVICE_ID
SET XACT_ABORT ON;
	
	DECLARE @PairMsg VARCHAR(MAX) = ''
	declare @tmpids table (tmpid int)
	DECLARE @ORDR_ID INT, @OrderCategoryID TinyInt = 6,	
			@FSA_ORDR_ID INT, @ORDR_ACTN_ID TINYINT = 2,
			@SCA_NBR VARCHAR(1000),@PROD_ID varchar(3), @ORDR_SUBTYPE_CD CHAR(2),
			@SCA_NBR_CPE Varchar(255)
	DECLARE @DMSTC_CD BIT = '0'
	DECLARE @SQL NVARCHAR(MAX)
	DECLARE @H5_H6_CUST_ID Varchar(20)
	DECLARE @CHNG_ORDR_TYPE_FLG BIT = 0
	DECLARE @RLTD_ORDR_ID varchar(150)
	DECLARE @CHG_CTR INT = 0, @CHG_CNT INT = 0, @CHG_ORDR_ID INT
	DECLARE @QDA_ORDR_ID_INT INT, @ORDR_ID_INT INT
	DECLARE @PRNT_FTN VARCHAR(50)
	DECLARE @QDA_ORDR_IDs varchar(max) = ''
	DECLARE @FSA_ORDR_IDS TABLE
			(
				ORDR_ID	Varchar(50),
				FLAG BIT
			)
	
	SET @SQL = ''
	SET @FSA_ORDR_ID = 0
	SET @QDA_ORDR_ID = ''
	IF @DEVICE_ID = ''
		SET @DEVICE_ID = 'null'
	SET NOCOUNT ON;
BEGIN TRY	
	BEGIN TRANSACTION @TransName
	DECLARE @CCDUpdate Bit
	
	DECLARE @MOVE_ORDER TABLE (MOVE_TO_ACCT_ID VARCHAR(200))
	
	SET @SQL = 'SELECT MOVE_TO_ACCT_ID FROM OPENQUERY(M5, ''SELECT MOVE_TO_ACCT_ID 
										FROM MACH5.V_V5U_ORDR WHERE ORDR_ID = ' + CONVERT(VARCHAR(20),@M5_ORDR_ID) + ''')'
	INSERT INTO @MOVE_ORDER
		EXEC sp_executesql @SQL
		
	SELECT * FROM @MOVE_ORDER
	
	IF @ORDR_TYPE_CD IN ('CCD','EXDT') 
		BEGIN
			IF OBJECT_ID(N'tempdb..#MACH5_CHNG_CCD',N'U') IS NOT NULL 
				DROP TABLE #MACH5_CHNG_CCD
			CREATE TABLE #MACH5_CHNG_CCD
				(
					M5_ORDR_ID	Int,
					FTN			Varchar(20),
					TYPE_CD		Varchar(20),
					CCD_CHG_REASON_CD	TinyInt,
					PREV_CUST_CMMT_DT	Date,
					CUST_CMMT_DT		Date,
					CHNG_TXN_NBR		Varchar(20),
					EXP_FLG_CD			Varchar(1),
					EXP_TYPE_CD			Varchar(1),
					COST_CNTR			Varchar(10),
					APPRVL_DT			DATE,
					APRVR_FIRST_NME		VARCHAR(255),
					APRVR_LAST_NME		VARCHAR(255),
					APRVR_PHN			Varchar(20),
					APRVR_TITLE			VARCHAR(15),
					NTE_TXT				Varchar(1000),
					FSA_ORDR_ID			Int
				)
				
			SET @SQL = ''
			
			IF @ORDR_TYPE_CD = 'CCD'
				BEGIN
					SET @SQL = 'SELECT * ' +
						' FROM OPENQUERY ' +
						' (M5,''Select 	CASE WHEN c.ORDR_ID IS NOT NULL THEN c.ORDR_ID
											 ELSE d.CHNG_TXN_ID
											 END  AS ORDR_ID , 
										CASE WHEN b.ORDER_NBR IS NOT NULL THEN b.ORDER_NBR
											 ELSE d.CHNG_TXN_NBR
											 END AS ORDR_NBR,c.TYPE_CD,cd.CCD_CHG_REASON_CD,cd.PREV_CUST_CMMT_DT,' + 
											'c.CUST_CMMT_DT,c.CHNG_TXN_NBR,cd.EXP_FLG_CD,c.EXP_TYPE_CD,c.COST_CNTR,c.APPRVL_DT,' + 			
											'c.APRVR_FIRST_NME,c.APRVR_LAST_NME,c.APRVR_PHN,c.APRVR_TITLE,c.NTE_TXT ' +
						'From Mach5.V_V5U_CHNG_TXN c ' +														
						' LEFT JOIN Mach5.V_V5U_CHNG_TXN_DETL cd ON c.CHNG_TXN_ID = cd.CHNG_TXN_ID AND cd.TYPE_CD = ''''CCD''''' +			
						' LEFT JOIN Mach5.V_V5U_ORDR b ON b.ORDR_ID = c.ORDR_ID ' +
						' LEFT JOIN Mach5.V_V5U_CHNG_TXN  d ON d.CHNG_TXN_ID = c.RLTD_CHNG_TXN_ID ' +
						' Where c.CHNG_TXN_ID =' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ ''')'			
				END
			ELSE
				BEGIN
					SET @SQL = 'SELECT * ' +
						' FROM OPENQUERY ' +
						' (M5,''Select 	c.ORDR_ID, b.ORDER_NBR,c.TYPE_CD,cd.CCD_CHG_REASON_CD,cd.PREV_CUST_CMMT_DT,' + 
											'c.CUST_CMMT_DT,c.CHNG_TXN_NBR,cd.EXP_FLG_CD,c.EXP_TYPE_CD,c.COST_CNTR,c.APPRVL_DT,' + 			
											'c.APRVR_FIRST_NME,c.APRVR_LAST_NME,c.APRVR_PHN,c.APRVR_TITLE,c.NTE_TXT ' +
						'From Mach5.V_V5U_CHNG_TXN c ' +														
						' LEFT JOIN Mach5.V_V5U_CHNG_TXN_DETL cd ON c.CHNG_TXN_ID = cd.CHNG_TXN_ID AND cd.EXP_FLG_CD = ''''Y''''' +			
						' LEFT JOIN Mach5.V_V5U_ORDR b ON b.ORDR_ID = c.ORDR_ID ' +
						' Where c.CHNG_TXN_ID =' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ ''')'			
				END
			
			INSERT INTO #MACH5_CHNG_CCD (M5_ORDR_ID,FTN,TYPE_CD,CCD_CHG_REASON_CD,PREV_CUST_CMMT_DT,CUST_CMMT_DT,CHNG_TXN_NBR,EXP_FLG_CD,
											EXP_TYPE_CD,COST_CNTR,APPRVL_DT,APRVR_FIRST_NME,APRVR_LAST_NME,APRVR_PHN,APRVR_TITLE,
											NTE_TXT)									
			EXEC sp_executesql @SQL		
			
			--Select * From #MACH5_CHNG_CCD
				UPDATE 	t
					SET FSA_ORDR_ID = f.ORDR_ID
					FROM #MACH5_CHNG_CCD t 
					INNER JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON t.FTN = f.FTN
				
				IF EXISTS
					(
						SELECT 'X'
							FROM #MACH5_CHNG_CCD
							WHERE ISNULL(FSA_ORDR_ID,0) != 0 
								AND TYPE_CD = 'CCD'
					)
					BEGIN
						--CCD 
						DECLARE @FTN Varchar(20)
						DECLARE @newCCD Date
						Declare @ReasonCD Varchar(2)
						Declare @NTE_TXT Varchar(1000)
						SELECT @FTN = FTN,
								@newCCD = CUST_CMMT_DT,
								@ReasonCD = CONVERT(VARCHAR,ISNULL(CCD_CHG_REASON_CD,10)),
								@NTE_TXT = ISNULL(NTE_TXT,'')
							FROM #MACH5_CHNG_CCD 
							
						INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('CCD Call:'+@TransName,'insertMach5OrderDetails', GETDATE())
						SET @ERRLog = 'CCD Call:'
						Exec [dbo].[insertCCDChange] @FTN,@newCCD,@ReasonCD,@NTE_TXT,1,0
						
						SELECT @QDA_ORDR_ID = CONVERT(VARCHAR,FSA_ORDR_ID)
							FROM #MACH5_CHNG_CCD
							
					END
				IF EXISTS
						(
							SELECT 'X'
								FROM #MACH5_CHNG_CCD
								WHERE EXP_FLG_CD = 'Y'
						)
					BEGIN
						IF EXISTS
							(
								SELECT 'X'
									FROM dbo.ORDR_EXP e WITH (NOLOCK)
									INNER JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON e.ORDR_ID = f.ORDR_ID
									WHERE f.FTN IN (SELECT FTN FROM #MACH5_CHNG_CCD)
							)
							BEGIN
								UPDATE	oe
									SET oe.EXP_TYPE_ID = (CASE t.EXP_TYPE_CD
															WHEN 'I' THEN 1
															WHEN 'E' THEN 2
															WHEN 'B' THEN 3
															ELSE 4 
														END)
										,oe.AUTHRZR_FRST_NME = t.APRVR_FIRST_NME
										,oe.AUTHRZR_LST_NME = t.APRVR_LAST_NME
										,oe.AUTHRZR_PHN_NBR = t.APRVR_PHN
										,oe.AUTHRZR_TITLE_ID = t.APRVR_TITLE
										,oe.AUTHN_DT = t.APPRVL_DT
										,oe.BILL_TO_COST_CTR_CD = [dbo].[RemoveSpecialCharacters](LEFT(SUBSTRING(t.COST_CNTR, PATINDEX('%[0-9]%', t.COST_CNTR), 10),
           PATINDEX('%[^0-9]%', SUBSTRING(t.COST_CNTR, PATINDEX('%[0-9]%', t.COST_CNTR), 10) + 'X') -1))
										,oe.CREAT_DT = GETDATE()
										,oe.CREAT_BY_USER_ID = 1
										,oe.REC_STUS_ID = 1
									FROM	dbo.ORDR_EXP oe WITH (ROWLOCK)
								INNER JOIN  dbo.FSA_ORDR f WITH (NOLOCK) ON oe.ORDR_ID = f.ORDR_ID
								INNER JOIN	#MACH5_CHNG_CCD t on f.FTN = t.FTN
									WHERE t.EXP_FLG_CD = 'Y'
							END
						ELSE
							BEGIN
								INSERT INTO ORDR_EXP (ORDR_ID,EXP_TYPE_ID,AUTHRZR_FRST_NME,AUTHRZR_LST_NME,
														AUTHRZR_PHN_NBR,AUTHRZR_TITLE_ID,AUTHN_DT,BILL_TO_COST_CTR_CD,
														CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID
													)
								(
									SELECT DISTINCT f.ORDR_ID
													,CASE t.EXP_TYPE_CD
															WHEN 'I' THEN 1
															WHEN 'E' THEN 2
															WHEN 'B' THEN 3
															ELSE 4 
														END AS EXP_TYPE_ID
													,t.APRVR_FIRST_NME
													,t.APRVR_LAST_NME
													,t.APRVR_PHN
													,t.APRVR_TITLE
													,t.APPRVL_DT
													,[dbo].[RemoveSpecialCharacters](LEFT(SUBSTRING(t.COST_CNTR, PATINDEX('%[0-9]%', t.COST_CNTR), 10),
           PATINDEX('%[^0-9]%', SUBSTRING(t.COST_CNTR, PATINDEX('%[0-9]%', t.COST_CNTR), 10) + 'X') -1))
													,GETDATE()
													,1
													,1
										FROM #MACH5_CHNG_CCD t WITH (NOLOCK)
										INNER JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON  t.FTN = f.FTN
										WHERE t.EXP_FLG_CD = 'Y'
										
								)
								
							END
					END
					
					SELECT @QDA_ORDR_ID = CONVERT(VARCHAR,ORDR_ID) + ',' + @QDA_ORDR_ID
							FROM #MACH5_CHNG_CCD t WITH (NOLOCK) 
							INNER JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON t.FTN = f.FTN
					
					IF (ISNULL(@QDA_ORDR_ID,'') = '')
						SET @QDA_ORDR_ID = 'MD'
		END
		
	ELSE IF @ORDR_TYPE_CD = 'CN' 
				--AND (SELECT TOP 1 ISNULL(MOVE_TO_ACCT_ID,'') from @MOVE_ORDER) != ''   removed 10/26/21  
				-- all cancels need this logic for changes at the same H6 are not being cancelled.  If cancel has only install
				-- the disconnect call will not find anything.  
		BEGIN
			
			INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('CNI Call:'+@TransName,'insertMach5OrderDetails', GETDATE())
			SET @ERRLog = @ERRLog + 'CNI Call:'
			EXEC	[dbo].[insertMach5OrderDetails]
					@M5_ORDR_ID ,
					@M5_RELT_ORDR_ID,
					@ORDR_TYPE_CD = N'CNI',
					@QDA_ORDR_ID = @QDA_ORDR_ID OUTPUT
					
			

			/*IF (@QDA_ORDR_ID = 'MD')
			BEGIN
				SET @PairMsg = 'PROC: dbo.insertMach5OrderDetails - Missing Data for Transaction : '+@TransName+', @ORDR_TYPE_CD : CNI'
			END
			ELSE IF (@QDA_ORDR_ID = 'X')
			BEGIN
				SET @PairMsg = 'PROC: dbo.insertMach5OrderDetails - Excluded condition for Transaction : '+@TransName+', @ORDR_TYPE_CD : CNI'
			END
			ELSE IF (@QDA_ORDR_ID = 'IE')
			BEGIN
				SET @PairMsg = 'PROC: dbo.insertMach5OrderDetails - Internal Error for Transaction : '+@TransName+', @ORDR_TYPE_CD : CNI'
			END
			ELSE IF (@QDA_ORDR_ID = '')
			BEGIN
				SET @PairMsg = 'PROC: dbo.insertMach5OrderDetails - Internal Error for Transaction : '+@TransName+', @ORDR_TYPE_CD : CNI, @QDA_ORDR_ID:'
			END*/

			SELECT	@QDA_ORDR_ID as N'@QDA_ORDR_ID'
			SET @QDA_ORDR_IDs = @QDA_ORDR_IDs + @QDA_ORDR_ID

			
			INSERT INTO @FSA_ORDR_IDS (ORDR_ID) SELECT StringID FROM [web].[ParseCommaSeparatedStrings](@QDA_ORDR_ID)
			
			IF EXISTS
				(
					SELECT 'X'
						FROM @FSA_ORDR_IDS 
						WHERE FLAG IS NULL
				) AND (NOT EXISTS (SELECT 'X' FROM @FSA_ORDR_IDS WHERE ORDR_ID LIKE '%[^0-9]%' ))
				BEGIN
					UPDATE dbo.FSA_ORDR	
						SET ORDR_TYPE_CD = 'CN'
						WHERE ORDR_ID IN (SELECT ORDR_ID FROM @FSA_ORDR_IDS WHERE FLAG IS NULL)
				
					SELECT @CHG_CNT = COUNT(1) FROM @FSA_ORDR_IDS WHERE FLAG IS NULL
					
					WHILE (@CHG_CTR < @CHG_CNT)
						BEGIN
							SELECT TOP 1 @CHG_ORDR_ID = ORDR_ID
								FROM @FSA_ORDR_IDS
								WHERE FLAG IS NULL
								
							INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('ProcessMach5CPECancelsI:'+@QDA_ORDR_ID+@TransName,'insertMach5OrderDetails', GETDATE())
							SET @ERRLog = @ERRLog + 'ProcessMach5CPECancelsI:'+@QDA_ORDR_ID
							--SET @QDA_ORDR_ID_INT = CONVERT(INT, @QDA_ORDR_ID)
							EXEC [dbo].[ProcessMach5CPECancels] @CHG_ORDR_ID
								
							Exec dbo.InsertInitialTask @CHG_ORDR_ID,@OrderCategoryID
							
							UPDATE @FSA_ORDR_IDS SET FLAG = 1 WHERE ORDR_ID = @CHG_ORDR_ID AND FLAG IS NULL
							SET @CHG_CTR = @CHG_CTR + 1
						END
					SET @CHG_CNT = 0
					SET @CHG_CTR = 0
					SET @CHG_ORDR_ID = 0
					
				END
			
			DELETE FROM @FSA_ORDR_IDS
			SET @QDA_ORDR_ID = ''

			INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('CND Call:'+@TransName,'insertMach5OrderDetails', GETDATE())
			SET @ERRLog = @ERRLog + 'CND Call:'
			EXEC	[dbo].[insertMach5OrderDetails]
					@M5_ORDR_ID ,
					@M5_RELT_ORDR_ID ,
					@ORDR_TYPE_CD = N'CND',
					@QDA_ORDR_ID = @QDA_ORDR_ID OUTPUT
					
			/*IF ((@QDA_ORDR_ID = 'MD') AND (LEN(@PairMsg)>0))
			BEGIN
				RAISERROR 
				(
					'%s; PROC: %s - Missing Data for Transaction : %s, @ORDR_TYPE_CD : %s', 
					16, 
					1, 
					@PairMsg,
					'dbo.insertMach5OrderDetails',
					@TransName,
					'CND'
				)
			END
			ELSE IF ((@QDA_ORDR_ID = 'X') AND (LEN(@PairMsg)>0))
			BEGIN
				RAISERROR 
				(
					'%s; PROC: %s - Excluded condition for Transaction : %s, @ORDR_TYPE_CD : %s', 
					16, 
					1, 
					@PairMsg,
					'dbo.insertMach5OrderDetails',
					@TransName,
					'CND'
				)
			END
			ELSE IF ((@QDA_ORDR_ID = 'IE') AND (LEN(@PairMsg)>0))
			BEGIN
				RAISERROR 
				(
					'%s; PROC: %s - Internal Error for Transaction : %s, @ORDR_TYPE_CD : %s', 
					16, 
					1, 
					@PairMsg,
					'dbo.insertMach5OrderDetails',
					@TransName,
					'CND'
				)
			END
			ELSE IF ((@QDA_ORDR_ID = '') AND (LEN(@PairMsg)>0))
			BEGIN
				RAISERROR 
				(
					'%s; PROC: %s - Internal Error for Transaction : %s, @ORDR_TYPE_CD : %s, @QDA_ORDR_ID:',
					16, 
					1, 
					@PairMsg,
					'dbo.insertMach5OrderDetails',
					@TransName,
					'CND'
				)
			END*/
					
			SELECT	@QDA_ORDR_ID as N'@QDA_ORDR_ID'

			INSERT INTO @FSA_ORDR_IDS (ORDR_ID) SELECT StringID FROM [web].[ParseCommaSeparatedStrings](@QDA_ORDR_ID)
			IF EXISTS
				(
					SELECT 'X'
						FROM @FSA_ORDR_IDS 
						WHERE FLAG IS NULL
				) AND (NOT EXISTS (SELECT 'X' FROM @FSA_ORDR_IDS WHERE ORDR_ID LIKE '%[^0-9]%'))
				BEGIN
					UPDATE dbo.FSA_ORDR	
						SET ORDR_TYPE_CD = 'CN'
						WHERE ORDR_ID IN (SELECT ORDR_ID FROM @FSA_ORDR_IDS WHERE FLAG IS NULL)
				
					SELECT @CHG_CNT = COUNT(1) FROM @FSA_ORDR_IDS WHERE FLAG IS NULL
					
					WHILE (@CHG_CTR < @CHG_CNT)
						BEGIN
							SELECT TOP 1 @CHG_ORDR_ID = ORDR_ID
								FROM @FSA_ORDR_IDS
								WHERE FLAG IS NULL
							
							INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('ProcessMach5CPECancelsD:'+@QDA_ORDR_ID+@TransName,'insertMach5OrderDetails', GETDATE())
							SET @ERRLog = @ERRLog + 'ProcessMach5CPECancelsD:'+@QDA_ORDR_ID
							--SET @QDA_ORDR_ID_INT = CONVERT(INT, @QDA_ORDR_ID)
							EXEC [dbo].[ProcessMach5CPECancels] @CHG_ORDR_ID
								
							Exec dbo.InsertInitialTask @CHG_ORDR_ID,@OrderCategoryID
							
							UPDATE @FSA_ORDR_IDS SET FLAG = 1 WHERE ORDR_ID = @CHG_ORDR_ID AND FLAG IS NULL
							SET @CHG_CTR = @CHG_CTR + 1
						END
					
				END

				DELETE FROM @FSA_ORDR_IDS
			IF @QDA_ORDR_ID != ''
				SET @QDA_ORDR_IDs = @QDA_ORDR_IDs + ',' + @QDA_ORDR_ID
		
			SET @QDA_ORDR_ID = @QDA_ORDR_IDs	
		END	
		
		
		
	ELSE IF @ORDR_TYPE_CD = 'CH'
		BEGIN
			SET @PairMsg = ''
			INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('CH-IN Call:'+@TransName,'insertMach5OrderDetails', GETDATE())
			SET @ERRLog = @ERRLog + 'CH-IN Call:'
			EXEC	[dbo].[insertMach5OrderDetails]
					@M5_ORDR_ID ,
					@M5_RELT_ORDR_ID,
					@ORDR_TYPE_CD = N'IN',
					@QDA_ORDR_ID = @QDA_ORDR_ID OUTPUT

			/*IF (@QDA_ORDR_ID = 'MD')
			BEGIN
				SET @PairMsg = 'PROC: dbo.insertMach5OrderDetails - Missing Data for Transaction : '+@TransName+', @ORDR_TYPE_CD : IN'
			END
			ELSE IF (@QDA_ORDR_ID = 'X')
			BEGIN
				SET @PairMsg = 'PROC: dbo.insertMach5OrderDetails - Excluded condition for Transaction : '+@TransName+', @ORDR_TYPE_CD : IN'
			END			
			ELSE IF (@QDA_ORDR_ID = 'IE')
			BEGIN
				SET @PairMsg = 'PROC: dbo.insertMach5OrderDetails - Internal Error for Transaction : '+@TransName+', @ORDR_TYPE_CD : IN'
			END
			ELSE IF (@QDA_ORDR_ID = '')
			BEGIN
				SET @PairMsg = 'PROC: dbo.insertMach5OrderDetails - Internal Error for Transaction : '+@TransName+', @ORDR_TYPE_CD : IN, @QDA_ORDR_ID:'
			END*/

			SELECT	@QDA_ORDR_ID as N'@QDA_ORDR_ID'
			SET @QDA_ORDR_IDs = @QDA_ORDR_IDs + @QDA_ORDR_ID

		
			INSERT INTO @FSA_ORDR_IDS (ORDR_ID) SELECT StringID FROM [web].[ParseCommaSeparatedStrings](@QDA_ORDR_ID)
		 
			IF EXISTS
				(
					SELECT 'X'
						FROM @FSA_ORDR_IDS 
						WHERE FLAG IS NULL
				) AND (NOT EXISTS (SELECT 'X' FROM @FSA_ORDR_IDS WHERE ORDR_ID LIKE '%[^0-9]%'))
				BEGIN
					UPDATE dbo.FSA_ORDR	
						SET ORDR_TYPE_CD = 'IN'
						WHERE ORDR_ID IN (SELECT ORDR_ID FROM @FSA_ORDR_IDS WHERE FLAG IS NULL)
				
					SELECT @CHG_CNT = COUNT(1) FROM @FSA_ORDR_IDS WHERE FLAG IS NULL
					
					WHILE (@CHG_CTR < @CHG_CNT)
						BEGIN
							SELECT TOP 1 @CHG_ORDR_ID = ORDR_ID
								FROM @FSA_ORDR_IDS
								WHERE FLAG IS NULL
								
							Exec dbo.InsertInitialTask @CHG_ORDR_ID,@OrderCategoryID
							
							UPDATE @FSA_ORDR_IDS SET FLAG = 1 WHERE ORDR_ID = @CHG_ORDR_ID AND FLAG IS NULL
							SET @CHG_CTR = @CHG_CTR + 1
						END
					SET @CHG_CNT = 0
					SET @CHG_CTR = 0
					SET @CHG_ORDR_ID = 0
					
				END
			
			DELETE FROM @FSA_ORDR_IDS
			SET @QDA_ORDR_ID = ''

			INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('CH-DC Call:'+@TransName,'insertMach5OrderDetails', GETDATE())
			SET @ERRLog = @ERRLog + 'CH-DC Call:'
			EXEC	[dbo].[insertMach5OrderDetails]
					@M5_ORDR_ID ,
					@M5_RELT_ORDR_ID ,
					@ORDR_TYPE_CD = N'DC',
					@QDA_ORDR_ID = @QDA_ORDR_ID OUTPUT

			/*IF ((@QDA_ORDR_ID = 'MD') AND (LEN(@PairMsg)>0))
			BEGIN
				RAISERROR 
				(
					'%s; PROC: %s - Missing Data for Transaction : %s, @ORDR_TYPE_CD : %s', 
					16, 
					1, 
					@PairMsg,
					'dbo.insertMach5OrderDetails',
					@TransName,
					'DC'
				)
			END
			ELSE IF ((@QDA_ORDR_ID = 'X') AND (LEN(@PairMsg)>0))
			BEGIN
				RAISERROR 
				(
					'%s; PROC: %s - Excluded condition for Transaction : %s, @ORDR_TYPE_CD : %s', 
					16, 
					1, 
					@PairMsg,
					'dbo.insertMach5OrderDetails',
					@TransName,
					'DC'
				)
			END
			ELSE IF ((@QDA_ORDR_ID = 'IE') AND (LEN(@PairMsg)>0))
			BEGIN
				RAISERROR 
				(
					'%s; PROC: %s - Internal Error for Transaction : %s, @ORDR_TYPE_CD : %s', 
					16, 
					1, 
					@PairMsg,
					'dbo.insertMach5OrderDetails',
					@TransName,
					'DC'
				)
			END
			ELSE IF ((@QDA_ORDR_ID = '') AND (LEN(@PairMsg)>0))
			BEGIN
				RAISERROR 
				(
					'%s; PROC: %s - Internal Error for Transaction : %s, @ORDR_TYPE_CD : %s, @QDA_ORDR_ID:',
					16, 
					1, 
					@PairMsg,
					'dbo.insertMach5OrderDetails',
					@TransName,
					'DC'
				)
			END*/
					
			SELECT	@QDA_ORDR_ID as N'@QDA_ORDR_ID'

			INSERT INTO @FSA_ORDR_IDS (ORDR_ID) SELECT StringID FROM [web].[ParseCommaSeparatedStrings](@QDA_ORDR_ID)
			IF EXISTS
				(
					SELECT 'X'
						FROM @FSA_ORDR_IDS 
						WHERE FLAG IS NULL
				) AND (NOT EXISTS (SELECT 'X' FROM @FSA_ORDR_IDS WHERE ORDR_ID LIKE '%[^0-9]%'))
				BEGIN
					UPDATE dbo.FSA_ORDR	
						SET ORDR_TYPE_CD = 'DC'
						WHERE ORDR_ID IN (SELECT ORDR_ID FROM @FSA_ORDR_IDS WHERE FLAG IS NULL)
				
					SELECT @CHG_CNT = COUNT(1) FROM @FSA_ORDR_IDS WHERE FLAG IS NULL
					
					WHILE (@CHG_CTR < @CHG_CNT)
						BEGIN
							SELECT TOP 1 @CHG_ORDR_ID = ORDR_ID
								FROM @FSA_ORDR_IDS
								WHERE FLAG IS NULL
								
							INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('InsertInitialTask:'+@QDA_ORDR_ID+@TransName,'insertMach5OrderDetails', GETDATE())
							SET @ERRLog = @ERRLog + 'InsertInitialTask:'+@QDA_ORDR_ID
							Exec dbo.InsertInitialTask @CHG_ORDR_ID,@OrderCategoryID
							
							UPDATE @FSA_ORDR_IDS SET FLAG = 1 WHERE ORDR_ID = @CHG_ORDR_ID AND FLAG IS NULL
							SET @CHG_CTR = @CHG_CTR + 1
						END
					
				END

			DELETE FROM @FSA_ORDR_IDS
			IF @QDA_ORDR_ID != ''
				SET @QDA_ORDR_IDs = @QDA_ORDR_IDs + ',' + @QDA_ORDR_ID
		
			SET @QDA_ORDR_ID = @QDA_ORDR_IDs	
		END
	ELSE
		BEGIN
			IF OBJECT_ID(N'tempdb..#MACH5_ORDR',N'U') IS NOT NULL 
				DROP TABLE #MACH5_ORDR
						
			CREATE TABLE #MACH5_ORDR
				(
					M5_ORDR_ID				INT,
					--QDA_ORDR_ID				INT,
					ORDER_NBR				VARCHAR(20),
					PRNT_ACCT_ID			INT,
					ORDR_TYPE_CD			VARCHAR(2),
					PROD_ID					VARCHAR(5),
					PROD_TYPE_CD			VARCHAR(2),
					ORDR_SUBTYPE_CD			VARCHAR(2),
					DOM_PROCESS_FLG_CD		VARCHAR(1),
					RLTD_ORDR_ID			VARCHAR(150),
					CUST_SIGN_DT			DATE,
					CUST_WANT_DT			DATE,
					CUST_SBMT_DT			DATE,
					CUST_CMMT_DT			DATE,
					EXP_FLG_CD				CHAR(1),
					EXP_TYPE_CD				VARCHAR(1),
					CUST_PREM_OCCU_FLG_CD	CHAR(1),
					CUST_ACPT_SRVC_FLG_CD	CHAR(1),
					MULT_ORDR_SBMTD_FLG_CD	CHAR(1),
					DSCNCT_REAS_CD			VARCHAR(2),
					PRE_QUAL_NBR			VARCHAR(20), -- added back on 3/8/18 for AR orders. dlp0278
					COST_CNTR				VARCHAR(10),
					EXPEDITE_APRVR_NME		VARCHAR(511),
					APRVR_TITLE				VARCHAR(15),
					APRVR_PHN				VARCHAR(20),
					APPRVL_DT				DATE,
					PRNT_FTN				VARCHAR(20),
					CSG_LVL					VARCHAR(5),
					CHARS_CUST_ID			VARCHAR(20),
					MOVE_TO_ACCT_ID         INT,
					CPE_DLVRY_DUTIES_AMT	VARCHAR(20),
					CPE_DLVRY_DUTIES_CD		VARCHAR(6),
					CPE_SHPNG_COST_AMT      VARCHAR(20),
					RLTD_FTN                VARCHAR(200)
				)
						
						
			SET @SQL = 'SELECT *
				FROM OPENQUERY(M5,''SELECT DISTINCT o.ORDR_ID, o.ORDER_NBR, o.PRNT_ACCT_ID, o.ORDR_TYPE_CD, o.PROD_ID, 
												 '''''''' AS PROD_TYPE_CD, o.ORDR_SUBTYPE_CD, o.DOM_PROCESS_FLG_CD, 
												 o.RLTD_ORDR_ID,o.CUST_SIGN_DT, o.CUST_WANT_DT, o.CUST_SBMT_DT, 
												 o.CUST_CMMT_DT,o.EXP_FLG_CD, o.EXP_TYPE_CD, o.CUST_PREM_OCCU_FLG_CD, 
												 o.CUST_ACPT_SRVC_FLG_CD, o.MULT_ORDR_SBMTD_FLG_CD,o.DSCNCT_REAS_CD, o.PRE_QUAL_NBR,
												 o.COST_CNTR, o.EXPEDITE_APRVR_NME,o.APRVR_TITLE, 
												 o.APRVR_PHN, o.APPRVL_DT, null, 
												 a.CSG_LVL, a.CHARS_CUST_ID,
												 o.MOVE_TO_ACCT_ID,
												 c.CPE_DLVRY_DUTIES_AMT,
												 c.CPE_DLVRY_DUTIES_CD,
												 c.CPE_SHPNG_COST_AMT,
												 '''''''' AS RLTD_FTN
											FROM MACH5.V_V5U_ORDR  o
											LEFT JOIN MACH5.V_V5U_CUST_ACCT a on o.PRNT_ACCT_ID = a.CUST_ACCT_ID AND a.TYPE_CD = DECODE(a.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
											LEFT JOIN MACH5.V_V5U_ORDR_CMPNT c on c.ORDR_ID = o.ORDR_ID AND c.CMPNT_TYPE_CD = ''''OCPE''''
											WHERE o.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')'

			--SELECT @SQL
			INSERT INTO #MACH5_ORDR
			EXEC sp_executesql 	@SQL		

			IF EXISTS
				(SELECT 'X'
					FROM	#MACH5_ORDR
					WHERE	M5_ORDR_ID = @M5_ORDR_ID
							AND ORDR_TYPE_CD = 'CH'
							AND @ORDR_TYPE_CD not in('AR','RO'))
				BEGIN
					SET @CHNG_ORDR_TYPE_FLG = 1
				END



			DECLARE	@PROD_TYPE_CD varchar(2)


			SELECT	@PROD_ID		= SUBSTRING(PROD_ID,1,3),
					@ORDR_SUBTYPE_CD = SUBSTRING(ORDR_SUBTYPE_CD,1,2)
				FROM #MACH5_ORDR
				WHERE M5_ORDR_ID = @M5_ORDR_ID

			EXEC	[dbo].[getM5ProductTypeCD] @ORDR_TYPE_CD,@M5_ORDR_ID,@PROD_ID,@ORDR_SUBTYPE_CD,@PROD_TYPE_CD OUTPUT

					
			Update #MACH5_ORDR
				SET PROD_TYPE_CD = @PROD_TYPE_CD

				
			IF @ORDR_TYPE_CD NOT IN ( 'CN','CNI','CND')
				BEGIN
					
					SELECT @RLTD_ORDR_ID = RLTD_ORDR_ID 
						FROM #MACH5_ORDR WITH (NOLOCK)

					DECLARE @Var nvarchar(MAX) 
					DECLARE @rltd_ordr int
					DECLARE @XML AS XML
					DECLARE @Delimiter AS CHAR(1) =','
					DECLARE @temp TABLE (ID INT)
					DECLARE @RLTD_FTN Varchar(200)
					DECLARE @rordr_cnt int = 0, @rordr_ctr int = 0

					Set @Var = @RLTD_ORDR_ID
					Set @XML = CAST(('<X>'+REPLACE(@Var,@Delimiter ,'</X><X>')+'</X>') AS XML)
					
					INSERT INTO @temp
						SELECT N.value('.', 'INT') AS ID FROM @XML.nodes('X') AS T(N)				
			
					SELECT @rordr_cnt = COUNT(*) FROM @temp 
					
					IF OBJECT_ID(N'tempdb..#RelatedFTN', N'U') IS NOT NULL         
						DROP TABLE #RelatedFTN
					 CREATE TABLE #RelatedFTN
								 ( FTN VARCHAR(20) NULL )
					 
					WHILE (@rordr_ctr < @rordr_cnt)
						BEGIN
							SELECT TOP 1 @rltd_ordr = ID
								FROM @temp
							
							select 	@rltd_ordr
							
							DECLARE @RelatedFTN nvarchar(max) = 'INSERT INTO  #RelatedFTN (FTN)
										Select ORDER_NBR from openquery(M5,''select ORDER_NBR 
												from MACH5.V_V5U_ORDR 
											WHERE ORDR_ID =' + Convert(varchar(20),@rltd_ordr) + ''')' 
							
							select @RelatedFTN

							EXEC sp_executeSQL @RelatedFTN 							
							
							DELETE @temp WHERE ID = @rltd_ordr
							
							SET @rordr_ctr = @rordr_ctr + 1
						
						END

					SELECT @RLTD_FTN = ISNULL(@RLTD_FTN,'') + UPPER(ISNULL(FTN,'')) + ', '
						FROM #RelatedFTN
					IF(len(@RLTD_FTN)>2)
					SET  @RLTD_FTN = substring(@RLTD_FTN, 1, len(@RLTD_FTN)- 1)
			
					Update #MACH5_ORDR
						SET  RLTD_FTN = @RLTD_FTN
						
				END
			
						
			Select * From #MACH5_ORDR


			IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CMPNT',N'U') IS NOT NULL 
				DROP TABLE #MACH5_ORDR_CMPNT

			CREATE TABLE #MACH5_ORDR_CMPNT
				(
					M5_ORDR_ID		INT,
					QDA_ORDR_ID		INT,
					ORDR_CMPNT_ID	INT,
					CMPNT_TYPE_CD	VARCHAR(4),
					ONE_STOP_SHOP_FLG_CD	VARCHAR(1), --TTRPT_OSS_CD
					ACCS_ARNGT_CD			VARCHAR(3), --TTRPT_ACCS_ARNGT_CD
					ACCS_TYPE_CD			VARCHAR(3),--TTRPT_ACCS_TYPE_CD
					ACCS_TERM_CD			VARCHAR(12),--TTRPT_ACCS_CNTRC_TERM_CD
					ACCS_BDWD_CD			VARCHAR(30),--TTRPT_SPD_OF_SRVC_BDWD_DES
					ACCS_CXR_CD				VARCHAR(5), -- CXR_ACCS_CD
					ACCS_CXR_NME			VARCHAR(94), -- for 'RES', concatenate into INSTL_VNDR_CD/VNDR_VPN_CD
					DESIGN_DOC_NUM			VARCHAR(100), --INSTL_DSGN_DOC_NBR
					RATE_TYPE_CD			VARCHAR(6), --PORT_RT_TYPE_CD
					PORT_SPD_DES			VARCHAR(50), --TTRPT_SPD_OF_SRVC_BDWD_DES
					NVTRY_ID				INT,
					CPE_ACCS_PRVDR_NME		VARCHAR(50),
					CPE_CONTRACT_TERM		INT,
					CPE_CONTRACT_TYPE		VARCHAR(6),
					CPE_DLVRY_DUTIES_AMT	VARCHAR(8),
					CPE_DLVRY_DUTIES_CD		VARCHAR(1),
					CPE_DROP_SHIP_FLG_CD	VARCHAR(1),
					CPE_EQPT_CKT_ID			VARCHAR(25),
					CPE_ITEM_ID				INT,
					CPE_MANAGED_BY			VARCHAR(10),
					CPE_MFR_ID				VARCHAR(4),
					CPE_MFR_NME				VARCHAR(255),
					CPE_MFR_PART_NBR		VARCHAR(255),
					CPE_MNGD_FLG_CD			VARCHAR(1),
					CPE_SPRINT_MNT_FLG_CD   CHAR(1),
					CPE_MSCP_CD				VARCHAR(15),
					CPE_LIST_PRICE			VARCHAR(14),
					CPE_REQD_ON_SITE_DT		DATE,
					CPE_SHIP_CLLI_CD		VARCHAR(11),
					CPE_SPRINT_TEST_TN_NBR	VARCHAR(25),
					CPE_DEVICE_ID			VARCHAR(14),
					EQPT_ID					VARCHAR(10),
					EQPT_TYPE_CD			VARCHAR(4),
					FLAG					BIT,
					DEVICE_ID				INT,
					QTY						INT,
					NME						VARCHAR(255),
					CPE_VNDR_DSCNT			VARCHAR(30),
					CPE_ITEM_MSTR_NBR		VARCHAR(6),
					CPE_RECORD_ONLY_FLG_CD	VARCHAR(1),
					CPE_CMPN_FMLY			VARCHAR(10),
					RLTD_CMPNT_ID			INT,
					CPE_ITEM_TYPE_CD		Varchar(3),	
					STUS_CD					Varchar(2),
					CPE_MFR_PS_ID			Varchar(30),
					CPE_ALT_SHIP_ADR			INT,
					CPE_ALT_SHIP_ADR_CTRY_CD	VARCHAR(2),
					CPE_ALT_SHIP_ADR_CTY_NME	VARCHAR(255),
					CPE_ALT_SHIP_ADR_LINE_1		VARCHAR(255),
					CPE_ALT_SHIP_ADR_LINE_2		VARCHAR(255),
					CPE_ALT_SHIP_ADR_LINE_3		VARCHAR(255),
					CPE_ALT_SHIP_ADR_PRVN_NME	VARCHAR(255),
					CPE_ALT_SHIP_ADR_PSTL_CD	VARCHAR(15),
					CPE_ALT_SHIP_ADR_ST_PRVN_CD VARCHAR(2),
					CPE_ACCS_PRVDR_TN_NBR        VARCHAR(25),
					CPE_EQUIPMENT_ONLY_FLG_CD	 VARCHAR(1),
					CPE_REPLMT                   INT,
					PRE_QUAL_NBR			VARCHAR(20),
					PL_NBR                  VARCHAR(200),
					CKT                     VARCHAR(200),
					NUA                     VARCHAR(200),
					CSG_LVL					VARCHAR(5),
					QUOTE_ID                VARCHAR(75),
					COS_CD                  VARCHAR(1),
					SMRT_ACCT_DOMN_TXT		VARCHAR(100),
					VRTL_ACCT_DOMN_TXT		VARCHAR(100),
					CPE_CUST_PRVD_SERIAL_NBR VARCHAR(200),
					PROD_CD					VARCHAR(5)

				)
				
			IF OBJECT_ID(N'tempdb..#MACH5_DISC_ORDR_CMPNT',N'U') IS NOT NULL 
						DROP TABLE #MACH5_DISC_ORDR_CMPNT

					CREATE TABLE #MACH5_DISC_ORDR_CMPNT
						(
							M5_ORDR_ID		INT,
							QDA_ORDR_ID		INT,
							ACCS_TYPE_CD	VARCHAR(3),--TTRPT_ACCS_TYPE_CD
							ACCS_BDWD_CD	VARCHAR(20),--TTRPT_SPD_OF_SRVC_BDWD_DES
							PORT_SPD_DES	VARCHAR(15), --TTRPT_SPD_OF_SRVC_BDWD_DES
							ORIG_ORDR_NBR	Varchar(20),
							CKT_ID			VARCHAR(9),
							NTWK_USR_ADR	VARCHAR(9),
							CMPNT_TYPE_CD	VARCHAR(4),
							PRIVATE_LINE_NBR VARCHAR (30),
							NME				 VARCHAR(255)    
						)

			SET @SQL = ''
			
			IF @ORDR_TYPE_CD NOT IN ('DC','CN','CNI','CND')
				BEGIN
					SET @SQL = 'SELECT *
							FROM OPENQUERY(M5,''SELECT DISTINCT c.ORDR_ID, 0 AS QDA_ORDR_ID, c.ORDR_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
															c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
															c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
															CASE WHEN c.NVTRY_ID IS NULL THEN  c.ORDR_CMPNT_ID	ELSE c.NVTRY_ID	END AS NVTRY_ID ,c.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
															c.CPE_CONTRACT_TYPE,c.CPE_DLVRY_DUTIES_AMT,c.CPE_DLVRY_DUTIES_CD,
															c.CPE_DROP_SHIP_FLG_CD,c.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
															c.CPE_MANAGED_BY,c.CPE_MFR_ID,c.CPE_MFR_NME,c.CPE_MFR_PART_NBR,
															c.CPE_MNGD_FLG_CD,c.CPE_SPRINT_MNT_FLG_CD,c.CPE_MSCP_CD,c.CPE_LIST_PRICE,c.CPE_REQD_ON_SITE_DT,
															c.CPE_SHIP_CLLI_CD,c.CPE_SPRINT_TEST_TN_NBR,
															c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															c.QTY, c.NME, c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, c.CPE_RECORD_ONLY_FLG_CD,
															c.CPE_CMPN_FMLY, c.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, c.STUS_CD, c.CPE_MFR_PS_ID,
															c.CPE_ALT_SHIP_ADR,c.CPE_ALT_SHIP_ADR_CTRY_CD,c.CPE_ALT_SHIP_ADR_CTY_NME,
															c.CPE_ALT_SHIP_ADR_LINE_1,c.CPE_ALT_SHIP_ADR_LINE_2,c.CPE_ALT_SHIP_ADR_LINE_3,
															c.CPE_ALT_SHIP_ADR_PRVN_NME,c.CPE_ALT_SHIP_ADR_PSTL_CD,
															c.CPE_ALT_SHIP_ADR_ST_PRVN_CD, c.CPE_ACCS_PRVDR_TN_NBR, c.CPE_EQUIPMENT_ONLY_FLG_CD,c.CPE_REPLMT,
															c.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR, c.CKT_ID,c.NTWK_USR_ADR, a.CSG_LVL, c.QUOTE_ID,c.COS_CD,
															c.SMRT_ACCT_DOMN_TXT,c.VRTL_ACCT_DOMN_TXT,c.CPE_CUST_PRVD_SERIAL_NBR,c.PROD_CD
														FROM	MACH5.V_V5U_ORDR_CMPNT c
														LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID= c.NVTRY_ID
														LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = c.PORT_SPD_CD
														LEFT JOIN MACH5.V_V5U_ORDR  o on c.ORDR_ID = o.ORDR_ID
														WHERE o.ORDR_SUBTYPE_CD NOT IN (''''AR'''', ''''RO'''')  AND c.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) +
													' AND ( (c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''',''''ACST'''') AND o.DOM_PROCESS_FLG_CD = ''''N'''' )
																OR (c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') 
																	))' +
													+ ' AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))'')
								UNION ALL 
								SELECT *
								FROM OPENQUERY(M5,''SELECT DISTINCT o.ORDR_ID, 0 AS QDA_ORDR_ID, '''''''' as ORDR_CMPNT_ID, n.CMPNT_TYPE_CD,n.ONE_STOP_SHOP_FLG_CD,
															n.ACCS_ARNGT_CD,n.ACCS_TYPE_CD,n.ACCS_TERM_CD,n.ACCS_BDWD_CD,
															n.ACCS_CXR_CD,n.ACCS_CXR_NME,n.DESIGN_DOC_NBR,n.RATE_TYPE_CD, p.PORT_SPD_DES, 
															n.NVTRY_ID,'''''''' as CPE_ACCS_PRVDR_NME,n.CPE_CONTRACT_TERM,
															n.CPE_CONTRACT_TYPE,'''''''' as CPE_DLVRY_DUTIES_AMT,'''''''' as CPE_DLVRY_DUTIES_CD,
															'''''''' as CPE_DROP_SHIP_FLG_CD,'''''''' as CPE_EQPT_CKT_ID,n.CPE_ITEM_ID,
															'''''''' as CPE_MANAGED_BY,'''''''' as CPE_MFR_ID,'''''''' as CPE_MFR_NME,'''''''' as CPE_MFR_PART_NBR,
															n.CPE_MNGD_FLG_CD,'''''''' as CPE_SPRINT_MNT_FLG_CD,'''''''' as CPE_MSCP_CD,'''''''' as CPE_LIST_PRICE,'''''''' as CPE_REQD_ON_SITE_DT,
															'''''''' as CPE_SHIP_CLLI_CD,'''''''' as CPE_SPRINT_TEST_TN_NBR,
															n.CPE_DEVICE_ID, n.EQPT_ID, n.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															n.QTY, n.NME, n.CPE_VNDR_DSCNT, n.CPE_ITEM_MSTR_NBR, '''''''' as CPE_RECORD_ONLY_FLG_CD,
															'''''''' as CPE_CMPN_FMLY, '''''''' as RLTD_CMPNT_ID, n.CPE_ITEM_TYPE_CD, ''''CP'''' AS STUS_CD, '''''''' as CPE_MFR_PS_ID,
															'''''''' as CPE_ALT_SHIP_ADR,'''''''' as CPE_ALT_SHIP_ADR_CTRY_CD,'''''''' as CPE_ALT_SHIP_ADR_CTY_NME,
															'''''''' as CPE_ALT_SHIP_ADR_LINE_1,'''''''' as CPE_ALT_SHIP_ADR_LINE_2,'''''''' as CPE_ALT_SHIP_ADR_LINE_3,
															'''''''' as CPE_ALT_SHIP_ADR_PRVN_NME,'''''''' as CPE_ALT_SHIP_ADR_PSTL_CD,
															'''''''' as CPE_ALT_SHIP_ADR_ST_PRVN_CD, '''''''' as CPE_ACCS_PRVDR_TN_NBR, '''''''' as CPE_EQUIPMENT_ONLY_FLG_CD,'''''''' as CPE_REPLMT,
															'''''''' as PRE_QUAL_NBR, n.PRIVATE_LINE_NBR, COALESCE(n.CKT_ID,n.VNDR_CKT_ID) as CKT_ID,n.NTWK_USR_ADR,a.CSG_LVL, n.QUOTE_ID,n.COS_CD,
															'''''''' as SMRT_ACCT_DOMN_TXT,'''''''' as VRTL_ACCT_DOMN_TXT, n.CPE_CUST_PRVD_SERIAL_NBR, n.PROD_CD 
														FROM	MACH5.V_V5U_ORDR o  
														LEFT JOIN  MACH5.V_V5U_NVTRY n on o.PRNT_ACCT_ID=n.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = n.PORT_SPD_CD
														WHERE o.ORDR_SUBTYPE_CD IN (''''AR'''', ''''RO'''')  AND o.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) +
													' AND ( (n.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''',''''ACST'''') AND o.DOM_PROCESS_FLG_CD = ''''N'''' )
																OR (n.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') 
																	))' +
													+ ' AND ((n.CPE_DEVICE_ID IS NULL) OR (n.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN n.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))'')'

					INSERT INTO #MACH5_ORDR_CMPNT
					EXEC sp_executesql @SQL	
					
					IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
					BEGIN
						SET @SQL = 'SELECT *
							FROM OPENQUERY(M5,''SELECT DISTINCT c.ORDR_ID, 0 AS QDA_ORDR_ID, c.ORDR_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
															c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
															c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
															CASE WHEN c.NVTRY_ID IS NULL THEN  c.ORDR_CMPNT_ID	ELSE c.NVTRY_ID	END AS NVTRY_ID,c.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
															c.CPE_CONTRACT_TYPE,c.CPE_DLVRY_DUTIES_AMT,c.CPE_DLVRY_DUTIES_CD,
															c.CPE_DROP_SHIP_FLG_CD,c.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
															c.CPE_MANAGED_BY,c.CPE_MFR_ID,c.CPE_MFR_NME,c.CPE_MFR_PART_NBR,
															c.CPE_MNGD_FLG_CD,c.CPE_SPRINT_MNT_FLG_CD,c.CPE_MSCP_CD,c.CPE_LIST_PRICE,c.CPE_REQD_ON_SITE_DT,
															c.CPE_SHIP_CLLI_CD,c.CPE_SPRINT_TEST_TN_NBR,
															c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															c.QTY, c.NME, c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, c.CPE_RECORD_ONLY_FLG_CD,
															c.CPE_CMPN_FMLY, c.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, c.STUS_CD, c.CPE_MFR_PS_ID,
															c.CPE_ALT_SHIP_ADR,c.CPE_ALT_SHIP_ADR_CTRY_CD,c.CPE_ALT_SHIP_ADR_CTY_NME,
															c.CPE_ALT_SHIP_ADR_LINE_1,c.CPE_ALT_SHIP_ADR_LINE_2,c.CPE_ALT_SHIP_ADR_LINE_3,
															c.CPE_ALT_SHIP_ADR_PRVN_NME,c.CPE_ALT_SHIP_ADR_PSTL_CD,
															c.CPE_ALT_SHIP_ADR_ST_PRVN_CD, c.CPE_ACCS_PRVDR_TN_NBR, c.CPE_EQUIPMENT_ONLY_FLG_CD,c.CPE_REPLMT,
															c.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR, c.CKT_ID,c.NTWK_USR_ADR,a.CSG_LVL,c.QUOTE_ID,c.COS_CD,
															c.SMRT_ACCT_DOMN_TXT,c.VRTL_ACCT_DOMN_TXT,c.CPE_CUST_PRVD_SERIAL_NBR,c.PROD_CD
														FROM	MACH5.V_V5U_ORDR_CMPNT c
														LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID= c.NVTRY_ID
														LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = c.PORT_SPD_CD
														LEFT JOIN MACH5.V_V5U_ORDR  o on c.ORDR_ID = o.ORDR_ID
														WHERE ROWNUM<=1 AND o.ORDR_SUBTYPE_CD NOT IN (''''AR'''', ''''RO'''')  AND c.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) +
													' AND (NOT(((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''',''''ACST'''') AND o.DOM_PROCESS_FLG_CD = ''''N'''' )
																OR (c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') 
																	))' +
													+ ' AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))))'')
								UNION ALL 
								SELECT *
								FROM OPENQUERY(M5,''SELECT DISTINCT o.ORDR_ID, 0 AS QDA_ORDR_ID, '''''''' as ORDR_CMPNT_ID, n.CMPNT_TYPE_CD,n.ONE_STOP_SHOP_FLG_CD,
															n.ACCS_ARNGT_CD,n.ACCS_TYPE_CD,n.ACCS_TERM_CD,n.ACCS_BDWD_CD,
															n.ACCS_CXR_CD,n.ACCS_CXR_NME,n.DESIGN_DOC_NBR,n.RATE_TYPE_CD, p.PORT_SPD_DES, 
															n.NVTRY_ID,'''''''' as CPE_ACCS_PRVDR_NME,n.CPE_CONTRACT_TERM,
															n.CPE_CONTRACT_TYPE,'''''''' as CPE_DLVRY_DUTIES_AMT,'''''''' as CPE_DLVRY_DUTIES_CD,
															'''''''' as CPE_DROP_SHIP_FLG_CD,'''''''' as CPE_EQPT_CKT_ID,n.CPE_ITEM_ID,
															'''''''' as CPE_MANAGED_BY,'''''''' as CPE_MFR_ID,'''''''' as CPE_MFR_NME,'''''''' as CPE_MFR_PART_NBR,
															n.CPE_MNGD_FLG_CD,'''''''' as CPE_SPRINT_MNT_FLG_CD,'''''''' as CPE_MSCP_CD,'''''''' as CPE_LIST_PRICE,'''''''' as CPE_REQD_ON_SITE_DT,
															'''''''' as CPE_SHIP_CLLI_CD,'''''''' as CPE_SPRINT_TEST_TN_NBR,
															n.CPE_DEVICE_ID, n.EQPT_ID, n.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															n.QTY, n.NME, n.CPE_VNDR_DSCNT, n.CPE_ITEM_MSTR_NBR, '''''''' as CPE_RECORD_ONLY_FLG_CD,
															'''''''' as CPE_CMPN_FMLY, '''''''' as RLTD_CMPNT_ID, n.CPE_ITEM_TYPE_CD, ''''CP'''' AS STUS_CD, '''''''' as CPE_MFR_PS_ID,
															'''''''' as CPE_ALT_SHIP_ADR,'''''''' as CPE_ALT_SHIP_ADR_CTRY_CD,'''''''' as CPE_ALT_SHIP_ADR_CTY_NME,
															'''''''' as CPE_ALT_SHIP_ADR_LINE_1,'''''''' as CPE_ALT_SHIP_ADR_LINE_2,'''''''' as CPE_ALT_SHIP_ADR_LINE_3,
															'''''''' as CPE_ALT_SHIP_ADR_PRVN_NME,'''''''' as CPE_ALT_SHIP_ADR_PSTL_CD,
															'''''''' as CPE_ALT_SHIP_ADR_ST_PRVN_CD, '''''''' as CPE_ACCS_PRVDR_TN_NBR, '''''''' as CPE_EQUIPMENT_ONLY_FLG_CD,'''''''' as CPE_REPLMT,
															o.PRE_QUAL_NBR, n.PRIVATE_LINE_NBR, COALESCE(n.CKT_ID,n.VNDR_CKT_ID) as CKT_ID,n.NTWK_USR_ADR,a.CSG_LVL, n.QUOTE_ID,n.COS_CD,
															'''''''' as SMRT_ACCT_DOMN_TXT,'''''''' as VRTL_ACCT_DOMN_TXT, n.CPE_CUST_PRVD_SERIAL_NBR, n.PROD_CD 
														FROM	MACH5.V_V5U_ORDR o  
														LEFT JOIN  MACH5.V_V5U_NVTRY n on o.PRNT_ACCT_ID=n.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = n.PORT_SPD_CD
														WHERE ROWNUM<=1 AND o.ORDR_SUBTYPE_CD IN (''''AR'''', ''''RO'''')  AND o.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) +
													' AND (NOT(((n.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''',''''ACST'''') AND o.DOM_PROCESS_FLG_CD = ''''N'''' )
																OR (n.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') 
																	))' +
													+ ' AND ((n.CPE_DEVICE_ID IS NULL) OR (n.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN n.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))))'')'
						
						INSERT INTO #MACH5_ORDR_CMPNT
						EXEC sp_executesql @SQL	
						
						IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
							SET @QDA_ORDR_ID = 'MD'
						ELSE
							SET @QDA_ORDR_ID = 'X'
						
						TRUNCATE TABLE #MACH5_ORDR_CMPNT
					END
				END
			ELSE IF @ORDR_TYPE_CD = 'DC'
				BEGIN
					SET @SQL = 'SELECT *
							FROM OPENQUERY(M5,''SELECT DISTINCT '
														+ CONVERT(VARCHAR,@M5_ORDR_ID) +
														',0 AS QDA_ORDR_ID, d.ORDR_DISC_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
															c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
															c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
															c.NVTRY_ID	AS NVTRY_ID,o.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
															c.CPE_CONTRACT_TYPE,o.CPE_DLVRY_DUTIES_AMT,o.CPE_DLVRY_DUTIES_CD,
															'''''''' as CPE_DROP_SHIP_FLG_CD,o.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
															o.CPE_MANAGED_BY,c.MFR_ID as CPE_MFR_ID,c.MFR_NME AS CPE_MFR_NME,c.MFR_PART_NBR AS CPE_MFR_PART_NBR,
															c.CPE_MNGD_FLG_CD,o.CPE_SPRINT_MNT_FLG_CD,o.CPE_MSCP_CD,o.CPE_LIST_PRICE,o.CPE_REQD_ON_SITE_DT,
															o.CPE_SHIP_CLLI_CD,o.CPE_SPRINT_TEST_TN_NBR,
															c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															c.QTY, c.NME , c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, o.CPE_RECORD_ONLY_FLG_CD,
															o.CPE_CMPN_FMLY, o.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, ''''WP'''' as STUS_CD, o.CPE_MFR_PS_ID,
															o.CPE_ALT_SHIP_ADR,o.CPE_ALT_SHIP_ADR_CTRY_CD,o.CPE_ALT_SHIP_ADR_CTY_NME,
															o.CPE_ALT_SHIP_ADR_LINE_1,o.CPE_ALT_SHIP_ADR_LINE_2,o.CPE_ALT_SHIP_ADR_LINE_3,
															o.CPE_ALT_SHIP_ADR_PRVN_NME,o.CPE_ALT_SHIP_ADR_PSTL_CD,
															o.CPE_ALT_SHIP_ADR_ST_PRVN_CD, o.CPE_ACCS_PRVDR_TN_NBR, o.CPE_EQUIPMENT_ONLY_FLG_CD, o.CPE_REPLMT,
															o.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR,c.CKT_ID,c.NTWK_USR_ADR,a.CSG_LVL,c.QUOTE_ID,c.COS_CD,
															'''''''' as SMRT_ACCT_DOMN_TXT,'''''''' as VRTL_ACCT_DOMN_TXT, c.CPE_CUST_PRVD_SERIAL_NBR,c.PROD_CD 
														FROM	MACH5.V_V5U_NVTRY c
														INNER JOIN Mach5.V_V5U_ORDR_DISC_CMPNT d ON d.NVTRY_CMPNT_ID = c.NVTRY_ID
														INNER JOIN MACH5.V_V5U_ORDR ord ON d.ORDR_ID = ord.ORDR_ID
														INNER JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=c.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_ORDR_CMPNT o ON c.NVTRY_ID = o.NVTRY_ID
														LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = o.PORT_SPD_CD
														LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON o.ACCS_BDWD_CD = bd.TYPE_CD
														WHERE ((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''',''''ACST'''') AND ord.DOM_PROCESS_FLG_CD = ''''N'''' ) 
																OR ((c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''')  ))) 
															AND (DECODE(d.STUS_CD,''''CN'''',1,0)=0)
															AND c.NVTRY_ID IN (SELECT NVTRY_CMPNT_ID FROM Mach5.V_V5U_ORDR_DISC_CMPNT WHERE ORDR_ID = ' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ ')'+ 
															+ ' AND d.ORDR_ID = ' + CONVERT(VARCHAR(10),@M5_ORDR_ID)
															+ ' AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))'')'
																					
					
					INSERT INTO #MACH5_ORDR_CMPNT
					EXEC sp_executesql @SQL	

					IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
					BEGIN
						SET @SQL = 'SELECT *
							FROM OPENQUERY(M5,''SELECT DISTINCT '
														+ CONVERT(VARCHAR,@M5_ORDR_ID) +
														',0 AS QDA_ORDR_ID, d.ORDR_DISC_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
															c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
															c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
															c.NVTRY_ID,o.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
															c.CPE_CONTRACT_TYPE,o.CPE_DLVRY_DUTIES_AMT,o.CPE_DLVRY_DUTIES_CD,
															'''''''' as CPE_DROP_SHIP_FLG_CD,o.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
															o.CPE_MANAGED_BY,c.MFR_ID as CPE_MFR_ID,c.MFR_NME AS CPE_MFR_NME,c.MFR_PART_NBR AS CPE_MFR_PART_NBR,
															c.CPE_MNGD_FLG_CD,o.CPE_SPRINT_MNT_FLG_CD,o.CPE_MSCP_CD,o.CPE_LIST_PRICE,o.CPE_REQD_ON_SITE_DT,
															o.CPE_SHIP_CLLI_CD,o.CPE_SPRINT_TEST_TN_NBR,
															c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															c.QTY, c.NME , c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, o.CPE_RECORD_ONLY_FLG_CD,
															o.CPE_CMPN_FMLY, o.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, ''''WP'''' as STUS_CD, o.CPE_MFR_PS_ID,
															o.CPE_ALT_SHIP_ADR,o.CPE_ALT_SHIP_ADR_CTRY_CD,o.CPE_ALT_SHIP_ADR_CTY_NME,
															o.CPE_ALT_SHIP_ADR_LINE_1,o.CPE_ALT_SHIP_ADR_LINE_2,o.CPE_ALT_SHIP_ADR_LINE_3,
															o.CPE_ALT_SHIP_ADR_PRVN_NME,o.CPE_ALT_SHIP_ADR_PSTL_CD,
															o.CPE_ALT_SHIP_ADR_ST_PRVN_CD, o.CPE_ACCS_PRVDR_TN_NBR, o.CPE_EQUIPMENT_ONLY_FLG_CD, o.CPE_REPLMT,
															o.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR,c.CKT_ID,c.NTWK_USR_ADR,a.CSG_LVL,c.QUOTE_ID,c.COS_CD,
															o.SMRT_ACCT_DOMN_TXT,o.VRTL_ACCT_DOMN_TXT, c.CPE_CUST_PRVD_SERIAL_NBR,c.PROD_CD
														FROM	MACH5.V_V5U_NVTRY c
														INNER JOIN Mach5.V_V5U_ORDR_DISC_CMPNT d ON d.NVTRY_CMPNT_ID = c.NVTRY_ID
														INNER JOIN MACH5.V_V5U_ORDR ord ON d.ORDR_ID = ord.ORDR_ID
														INNER JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=c.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_ORDR_CMPNT o ON c.NVTRY_ID = o.NVTRY_ID
														LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = o.PORT_SPD_CD
														LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON o.ACCS_BDWD_CD = bd.TYPE_CD
														WHERE ROWNUM<=1 AND d.ORDR_ID = ' + CONVERT(VARCHAR(10),@M5_ORDR_ID) + ' AND c.NVTRY_ID IN (SELECT NVTRY_CMPNT_ID FROM Mach5.V_V5U_ORDR_DISC_CMPNT WHERE ORDR_ID = ' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ ')'+ 
															+ ' AND (NOT(((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''',''''ACST'''') AND ord.DOM_PROCESS_FLG_CD = ''''N'''' ) 
																OR ((c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''')  ))) ' +
															+ ' AND (DECODE(d.STUS_CD,''''CN'''',1,0)=0)
															 AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))))'')'
						
						INSERT INTO #MACH5_ORDR_CMPNT
						EXEC sp_executesql @SQL	
						
						IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
							SET @QDA_ORDR_ID = 'MD'
						ELSE
							SET @QDA_ORDR_ID = 'X'
							
						TRUNCATE TABLE #MACH5_ORDR_CMPNT
					END
				END
			ELSE IF @ORDR_TYPE_CD = 'CN'
				BEGIN
					
					SELECT @RLTD_ORDR_ID = RLTD_ORDR_ID 
						FROM #MACH5_ORDR WITH (NOLOCK)
						
					SET @SQL = ''
					SET @SQL = 'UPDATE 	#MACH5_ORDR
									SET  PRNT_FTN = A.ORDER_NBR ' + 
									' FROM ' + 
									' ( SELECT ORDER_NBR FROM ' +
									' OPENQUERY(M5,' +
									'''	SELECT	ORDER_NBR
										FROM	Mach5.V_V5U_ORDR 
													WHERE ORDR_ID = ' + CONVERT(VARCHAR(20),@RLTD_ORDR_ID)+ ''')) AS A'
														
									
									
					EXEC sp_executeSQL @SQL
				
					SET @SQL = 'SELECT *
									FROM OPENQUERY(M5,''SELECT DISTINCT '
								+ CONVERT(VARCHAR,@M5_ORDR_ID) +
								' AS ORDR_ID,0 AS QDA_ORDR_ID, o.ORDR_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
								c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
								c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
								CASE WHEN c.NVTRY_ID IS NULL THEN  o.ORDR_CMPNT_ID	ELSE c.NVTRY_ID	END AS NVTRY_ID,o.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
								c.CPE_CONTRACT_TYPE,o.CPE_DLVRY_DUTIES_AMT,o.CPE_DLVRY_DUTIES_CD,
								o.CPE_DROP_SHIP_FLG_CD,o.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
								o.CPE_MANAGED_BY,o.CPE_MFR_ID,o.CPE_MFR_NME,o.CPE_MFR_PART_NBR,
								c.CPE_MNGD_FLG_CD,o.CPE_SPRINT_MNT_FLG_CD,o.CPE_MSCP_CD,o.CPE_LIST_PRICE,o.CPE_REQD_ON_SITE_DT,
								o.CPE_SHIP_CLLI_CD,o.CPE_SPRINT_TEST_TN_NBR,
								c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
								c.QTY, c.NME , c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, o.CPE_RECORD_ONLY_FLG_CD,
								o.CPE_CMPN_FMLY, o.RLTD_CMPNT_ID, o.CPE_ITEM_TYPE_CD, ''''CN'''' as STUS_CD, o.CPE_MFR_PS_ID,
								o.CPE_ALT_SHIP_ADR,o.CPE_ALT_SHIP_ADR_CTRY_CD,o.CPE_ALT_SHIP_ADR_CTY_NME,
								o.CPE_ALT_SHIP_ADR_LINE_1,o.CPE_ALT_SHIP_ADR_LINE_2,o.CPE_ALT_SHIP_ADR_LINE_3,
								o.CPE_ALT_SHIP_ADR_PRVN_NME,o.CPE_ALT_SHIP_ADR_PSTL_CD,
								o.CPE_ALT_SHIP_ADR_ST_PRVN_CD, o.CPE_ACCS_PRVDR_TN_NBR, o.CPE_EQUIPMENT_ONLY_FLG_CD, o.CPE_REPLMT,
								o.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR,c.CKT_ID,c.NTWK_USR_ADR,a.CSG_LVL,c.QUOTE_ID,o.COS_CD,
								o.SMRT_ACCT_DOMN_TXT,o.VRTL_ACCT_DOMN_TXT, c.CPE_CUST_PRVD_SERIAL_NBR,c.PROD_CD
							FROM Mach5.V_V5U_ORDR_CNCL_CMPNT can
							INNER JOIN MACH5.V_V5U_ORDR  ord on can.ORDR_ID = ord.ORDR_ID
							INNER JOIN MACH5.V_V5U_ORDR_CMPNT o ON can.ORDR_CMPNT_ID = o.ORDR_CMPNT_ID
							LEFT JOIN MACH5.V_V5U_NVTRY c   ON o.NVTRY_ID = c.NVTRY_ID
							LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=c.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = c.PORT_SPD_CD
							LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON c.ACCS_BDWD_CD = bd.TYPE_CD
							WHERE ((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND ord.DOM_PROCESS_FLG_CD = ''''N'''')
									OR (c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') )) 
								AND can.ORDR_ID = '+ CONVERT(VARCHAR,@M5_ORDR_ID) + 
								+ ' AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))
								
							UNION
							
							SELECT DISTINCT  '
								+ CONVERT(VARCHAR,@M5_ORDR_ID) +
								' AS ORDR_ID,0 AS QDA_ORDR_ID, o.ORDR_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
								c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
								c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
								CASE WHEN c.NVTRY_ID IS NULL THEN  o.ORDR_CMPNT_ID	ELSE c.NVTRY_ID	END AS NVTRY_ID,o.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
								c.CPE_CONTRACT_TYPE,o.CPE_DLVRY_DUTIES_AMT,o.CPE_DLVRY_DUTIES_CD,
								o.CPE_DROP_SHIP_FLG_CD,o.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
								o.CPE_MANAGED_BY,c.MFR_ID as CPE_MFR_ID,c.MFR_NME as CPE_MFR_NME,c.MFR_PART_NBR as CPE_MFR_PART_NBR,
								c.CPE_MNGD_FLG_CD,o.CPE_SPRINT_MNT_FLG_CD,o.CPE_MSCP_CD,o.CPE_LIST_PRICE,o.CPE_REQD_ON_SITE_DT,
								o.CPE_SHIP_CLLI_CD,o.CPE_SPRINT_TEST_TN_NBR,
								c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
								c.QTY, c.NME , c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, o.CPE_RECORD_ONLY_FLG_CD,
								o.CPE_CMPN_FMLY, o.RLTD_CMPNT_ID, o.CPE_ITEM_TYPE_CD, ''''CN'''' as STUS_CD, o.CPE_MFR_PS_ID,
								o.CPE_ALT_SHIP_ADR,o.CPE_ALT_SHIP_ADR_CTRY_CD,o.CPE_ALT_SHIP_ADR_CTY_NME,
								o.CPE_ALT_SHIP_ADR_LINE_1,o.CPE_ALT_SHIP_ADR_LINE_2,o.CPE_ALT_SHIP_ADR_LINE_3,
								o.CPE_ALT_SHIP_ADR_PRVN_NME,o.CPE_ALT_SHIP_ADR_PSTL_CD,
								o.CPE_ALT_SHIP_ADR_ST_PRVN_CD, o.CPE_ACCS_PRVDR_TN_NBR, o.CPE_EQUIPMENT_ONLY_FLG_CD, o.CPE_REPLMT,
								o.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR,c.CKT_ID,c.NTWK_USR_ADR,a.CSG_LVL,c.QUOTE_ID,c.COS_CD,
								o.SMRT_ACCT_DOMN_TXT,o.VRTL_ACCT_DOMN_TXT,c.CPE_CUST_PRVD_SERIAL_NBR,c.PROD_CD
							FROM Mach5.V_V5U_ORDR_CNCL_CMPNT can
							INNER JOIN MACH5.V_V5U_ORDR  ord on can.ORDR_ID = ord.ORDR_ID
							INNER JOIN MACH5.V_V5U_ORDR_DISC_CMPNT d ON can.ORDR_CMPNT_ID = d.ORDR_DISC_CMPNT_ID
							LEFT JOIN MACH5.V_V5U_ORDR_CMPNT o ON ord.ORDR_ID = o.ORDR_ID
							LEFT JOIN MACH5.V_V5U_NVTRY c   ON d.NVTRY_CMPNT_ID = c.NVTRY_ID
							LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=c.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = c.PORT_SPD_CD
							LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON c.ACCS_BDWD_CD = bd.TYPE_CD
							WHERE ((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND ord.DOM_PROCESS_FLG_CD = ''''N'''' )
									OR (c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') )) 
								AND can.ORDR_ID = '+ CONVERT(VARCHAR,@M5_ORDR_ID) + 
								 ' AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))									
							
							UNION
							SELECT DISTINCT o.ORDR_ID, 0 AS QDA_ORDR_ID, 0 as ORDR_CMPNT_ID, n.CMPNT_TYPE_CD,n.ONE_STOP_SHOP_FLG_CD,
															n.ACCS_ARNGT_CD,n.ACCS_TYPE_CD,n.ACCS_TERM_CD,n.ACCS_BDWD_CD,
															n.ACCS_CXR_CD,n.ACCS_CXR_NME,n.DESIGN_DOC_NBR,n.RATE_TYPE_CD, p.PORT_SPD_DES, 
															n.NVTRY_ID,'''''''' as CPE_ACCS_PRVDR_NME,n.CPE_CONTRACT_TERM,
															n.CPE_CONTRACT_TYPE,0 as CPE_DLVRY_DUTIES_AMT,'''''''' as CPE_DLVRY_DUTIES_CD,
															'''''''' as CPE_DROP_SHIP_FLG_CD,'''''''' as CPE_EQPT_CKT_ID,n.CPE_ITEM_ID,
															'''''''' as CPE_MANAGED_BY,'''''''' as CPE_MFR_ID,'''''''' as CPE_MFR_NME,'''''''' as CPE_MFR_PART_NBR,
															n.CPE_MNGD_FLG_CD,'''''''' as CPE_SPRINT_MNT_FLG_CD,'''''''' as CPE_MSCP_CD,0 as CPE_LIST_PRICE,o.CUST_CMMT_DT as CPE_REQD_ON_SITE_DT,
															'''''''' as CPE_SHIP_CLLI_CD,'''''''' as CPE_SPRINT_TEST_TN_NBR,
															n.CPE_DEVICE_ID, n.EQPT_ID, n.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															n.QTY, n.NME, n.CPE_VNDR_DSCNT, n.CPE_ITEM_MSTR_NBR, '''''''' as CPE_RECORD_ONLY_FLG_CD,
															'''''''' as CPE_CMPN_FMLY, 0 as RLTD_CMPNT_ID, n.CPE_ITEM_TYPE_CD, ''''CN'''' AS STUS_CD, '''''''' as CPE_MFR_PS_ID,
															0 as CPE_ALT_SHIP_ADR,'''''''' as CPE_ALT_SHIP_ADR_CTRY_CD,'''''''' as CPE_ALT_SHIP_ADR_CTY_NME,
															'''''''' as CPE_ALT_SHIP_ADR_LINE_1,'''''''' as CPE_ALT_SHIP_ADR_LINE_2,'''''''' as CPE_ALT_SHIP_ADR_LINE_3,
															'''''''' as CPE_ALT_SHIP_ADR_PRVN_NME,'''''''' as CPE_ALT_SHIP_ADR_PSTL_CD,
															'''''''' as CPE_ALT_SHIP_ADR_ST_PRVN_CD, '''''''' as CPE_ACCS_PRVDR_TN_NBR, '''''''' as CPE_EQUIPMENT_ONLY_FLG_CD,0 as CPE_REPLMT,
															'''''''' as PRE_QUAL_NBR, n.PRIVATE_LINE_NBR, n.CKT_ID,n.NTWK_USR_ADR,a.CSG_LVL,n.QUOTE_ID,n.COS_CD,
															'''''''' as SMRT_ACCT_DOMN_TXT,'''''''' as VRTL_ACCT_DOMN_TXT, n.CPE_CUST_PRVD_SERIAL_NBR,n.PROD_CD 
							FROM MACH5.V_V5U_ORDR  o 
							INNER JOIN MACH5.V_V5U_ORDR  relord ON relord.ORDR_ID=o.RLTD_ORDR_ID
							LEFT JOIN  MACH5.V_V5U_NVTRY n on o.PRNT_ACCT_ID=n.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = n.PORT_SPD_CD
							WHERE relord.ORDR_SUBTYPE_CD IN (''''AR'''', ''''RO'''')  AND((n.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND o.DOM_PROCESS_FLG_CD = ''''N'''' )
									OR (n.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') )) 
								AND o.ORDR_ID = '+ CONVERT(VARCHAR,@M5_ORDR_ID) + 
								 ' AND ((n.CPE_DEVICE_ID IS NULL) OR (n.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN n.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))			
								'')'
															
					INSERT INTO #MACH5_ORDR_CMPNT
					EXEC sp_executesql @SQL
					
					IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
					BEGIN
						SET @SQL = 'SELECT *
									FROM OPENQUERY(M5,''SELECT '
								+ CONVERT(VARCHAR,@M5_ORDR_ID) +
								' AS ORDR_ID,0 AS QDA_ORDR_ID, o.ORDR_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
								c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
								c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
								CASE WHEN c.NVTRY_ID IS NULL THEN  o.ORDR_CMPNT_ID	ELSE c.NVTRY_ID	END AS NVTRY_ID,o.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
								c.CPE_CONTRACT_TYPE,o.CPE_DLVRY_DUTIES_AMT,o.CPE_DLVRY_DUTIES_CD,
								o.CPE_DROP_SHIP_FLG_CD,o.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
								o.CPE_MANAGED_BY,o.CPE_MFR_ID,o.CPE_MFR_NME,o.CPE_MFR_PART_NBR,
								c.CPE_MNGD_FLG_CD,o.CPE_SPRINT_MNT_FLG_CD,o.CPE_MSCP_CD,o.CPE_LIST_PRICE,o.CPE_REQD_ON_SITE_DT,
								o.CPE_SHIP_CLLI_CD,o.CPE_SPRINT_TEST_TN_NBR,
								c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
								c.QTY, c.NME , c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, o.CPE_RECORD_ONLY_FLG_CD,
								o.CPE_CMPN_FMLY, o.RLTD_CMPNT_ID, o.CPE_ITEM_TYPE_CD, ''''CN'''' as STUS_CD, o.CPE_MFR_PS_ID,
								o.CPE_ALT_SHIP_ADR,o.CPE_ALT_SHIP_ADR_CTRY_CD,o.CPE_ALT_SHIP_ADR_CTY_NME,
								o.CPE_ALT_SHIP_ADR_LINE_1,o.CPE_ALT_SHIP_ADR_LINE_2,o.CPE_ALT_SHIP_ADR_LINE_3,
								o.CPE_ALT_SHIP_ADR_PRVN_NME,o.CPE_ALT_SHIP_ADR_PSTL_CD,
								o.CPE_ALT_SHIP_ADR_ST_PRVN_CD, o.CPE_ACCS_PRVDR_TN_NBR, o.CPE_EQUIPMENT_ONLY_FLG_CD, o.CPE_REPLMT,
								o.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR,c.CKT_ID,c.NTWK_USR_ADR,a.CSG_LVL,c.QUOTE_ID,c.COS_CD,
								o.SMRT_ACCT_DOMN_TXT,o.VRTL_ACCT_DOMN_TXT, c.CPE_CUST_PRVD_SERIAL_NBR, c.PROD_CD
							FROM Mach5.V_V5U_ORDR_CNCL_CMPNT can
							INNER JOIN MACH5.V_V5U_ORDR  ord on can.ORDR_ID = ord.ORDR_ID
							INNER JOIN MACH5.V_V5U_ORDR_CMPNT o ON can.ORDR_CMPNT_ID = o.ORDR_CMPNT_ID
							LEFT JOIN MACH5.V_V5U_NVTRY c   ON o.NVTRY_ID = c.NVTRY_ID
							LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=c.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = c.PORT_SPD_CD
							LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON c.ACCS_BDWD_CD = bd.TYPE_CD
							WHERE ROWNUM<=1 AND can.ORDR_ID = '+ CONVERT(VARCHAR,@M5_ORDR_ID) + ' AND (NOT(((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND ord.DOM_PROCESS_FLG_CD = ''''N'''')
									OR (c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') )) '
								+ ' AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))))
								
							UNION
							
							SELECT '
								+ CONVERT(VARCHAR,@M5_ORDR_ID) +
								' AS ORDR_ID,0 AS QDA_ORDR_ID, o.ORDR_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
								c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
								c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
								CASE WHEN c.NVTRY_ID IS NULL THEN  o.ORDR_CMPNT_ID	ELSE c.NVTRY_ID	END AS NVTRY_ID,o.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
								c.CPE_CONTRACT_TYPE,o.CPE_DLVRY_DUTIES_AMT,o.CPE_DLVRY_DUTIES_CD,
								o.CPE_DROP_SHIP_FLG_CD,o.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
								o.CPE_MANAGED_BY,c.MFR_ID as CPE_MFR_ID,c.MFR_NME as CPE_MFR_NME,c.MFR_PART_NBR as CPE_MFR_PART_NBR,
								c.CPE_MNGD_FLG_CD,o.CPE_SPRINT_MNT_FLG_CD,o.CPE_MSCP_CD,o.CPE_LIST_PRICE,o.CPE_REQD_ON_SITE_DT,
								o.CPE_SHIP_CLLI_CD,o.CPE_SPRINT_TEST_TN_NBR,
								c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
								c.QTY, c.NME , c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, o.CPE_RECORD_ONLY_FLG_CD,
								o.CPE_CMPN_FMLY, o.RLTD_CMPNT_ID, o.CPE_ITEM_TYPE_CD, ''''CN'''' as STUS_CD, o.CPE_MFR_PS_ID,
								o.CPE_ALT_SHIP_ADR,o.CPE_ALT_SHIP_ADR_CTRY_CD,o.CPE_ALT_SHIP_ADR_CTY_NME,
								o.CPE_ALT_SHIP_ADR_LINE_1,o.CPE_ALT_SHIP_ADR_LINE_2,o.CPE_ALT_SHIP_ADR_LINE_3,
								o.CPE_ALT_SHIP_ADR_PRVN_NME,o.CPE_ALT_SHIP_ADR_PSTL_CD,
								o.CPE_ALT_SHIP_ADR_ST_PRVN_CD, o.CPE_ACCS_PRVDR_TN_NBR, o.CPE_EQUIPMENT_ONLY_FLG_CD, o.CPE_REPLMT,
								o.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR,c.CKT_ID,c.NTWK_USR_ADR,a.CSG_LVL,c.QUOTE_ID,c.COS_CD,
								o.SMRT_ACCT_DOMN_TXT,o.VRTL_ACCT_DOMN_TXT, c.CPE_CUST_PRVD_SERIAL_NBR, c.PROD_CD
							FROM Mach5.V_V5U_ORDR_CNCL_CMPNT can
							INNER JOIN MACH5.V_V5U_ORDR  ord on can.ORDR_ID = ord.ORDR_ID
							INNER JOIN MACH5.V_V5U_ORDR_DISC_CMPNT d ON can.ORDR_CMPNT_ID = d.ORDR_DISC_CMPNT_ID
							LEFT JOIN MACH5.V_V5U_ORDR_CMPNT o ON ord.ORDR_ID = o.ORDR_ID
							LEFT JOIN MACH5.V_V5U_NVTRY c   ON d.NVTRY_CMPNT_ID = c.NVTRY_ID
							LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=c.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = c.PORT_SPD_CD
							LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON c.ACCS_BDWD_CD = bd.TYPE_CD
							WHERE ROWNUM <=1 AND can.ORDR_ID = '+ CONVERT(VARCHAR,@M5_ORDR_ID) + ' AND (NOT(((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND ord.DOM_PROCESS_FLG_CD = ''''N'''' )
									OR (c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') )) ' +
								 ' AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))))									
								 
							UNION
							SELECT DISTINCT o.ORDR_ID, 0 AS QDA_ORDR_ID, 0 as ORDR_CMPNT_ID, n.CMPNT_TYPE_CD,n.ONE_STOP_SHOP_FLG_CD,
															n.ACCS_ARNGT_CD,n.ACCS_TYPE_CD,n.ACCS_TERM_CD,n.ACCS_BDWD_CD,
															n.ACCS_CXR_CD,n.ACCS_CXR_NME,n.DESIGN_DOC_NBR,n.RATE_TYPE_CD, p.PORT_SPD_DES, 
															n.NVTRY_ID,'''''''' as CPE_ACCS_PRVDR_NME,n.CPE_CONTRACT_TERM,
															n.CPE_CONTRACT_TYPE,0 as CPE_DLVRY_DUTIES_AMT,'''''''' as CPE_DLVRY_DUTIES_CD,
															'''''''' as CPE_DROP_SHIP_FLG_CD,'''''''' as CPE_EQPT_CKT_ID,n.CPE_ITEM_ID,
															'''''''' as CPE_MANAGED_BY,'''''''' as CPE_MFR_ID,'''''''' as CPE_MFR_NME,'''''''' as CPE_MFR_PART_NBR,
															n.CPE_MNGD_FLG_CD,'''''''' as CPE_SPRINT_MNT_FLG_CD,'''''''' as CPE_MSCP_CD,0 as CPE_LIST_PRICE,o.CUST_CMMT_DT as CPE_REQD_ON_SITE_DT,
															'''''''' as CPE_SHIP_CLLI_CD,'''''''' as CPE_SPRINT_TEST_TN_NBR,
															n.CPE_DEVICE_ID, n.EQPT_ID, n.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															n.QTY, n.NME, n.CPE_VNDR_DSCNT, n.CPE_ITEM_MSTR_NBR, '''''''' as CPE_RECORD_ONLY_FLG_CD,
															'''''''' as CPE_CMPN_FMLY, 0 as RLTD_CMPNT_ID, n.CPE_ITEM_TYPE_CD, ''''CN'''' AS STUS_CD, '''''''' as CPE_MFR_PS_ID,
															0 as CPE_ALT_SHIP_ADR,'''''''' as CPE_ALT_SHIP_ADR_CTRY_CD,'''''''' as CPE_ALT_SHIP_ADR_CTY_NME,
															'''''''' as CPE_ALT_SHIP_ADR_LINE_1,'''''''' as CPE_ALT_SHIP_ADR_LINE_2,'''''''' as CPE_ALT_SHIP_ADR_LINE_3,
															'''''''' as CPE_ALT_SHIP_ADR_PRVN_NME,'''''''' as CPE_ALT_SHIP_ADR_PSTL_CD,
															'''''''' as CPE_ALT_SHIP_ADR_ST_PRVN_CD, '''''''' as CPE_ACCS_PRVDR_TN_NBR, '''''''' as CPE_EQUIPMENT_ONLY_FLG_CD,0 as CPE_REPLMT,
															'''''''' as PRE_QUAL_NBR, n.PRIVATE_LINE_NBR, n.CKT_ID,n.NTWK_USR_ADR,a.CSG_LVL, n.QUOTE_ID,n.COS_CD,
															'''''''' as SMRT_ACCT_DOMN_TXT,'''''''' as VRTL_ACCT_DOMN_TXT, n.CPE_CUST_PRVD_SERIAL_NBR, n.PROD_CD
							FROM MACH5.V_V5U_ORDR  o 
							INNER JOIN MACH5.V_V5U_ORDR  relord ON relord.ORDR_ID=o.RLTD_ORDR_ID
							LEFT JOIN  MACH5.V_V5U_NVTRY n on o.PRNT_ACCT_ID=n.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = n.PORT_SPD_CD
							WHERE ROWNUM <=1 AND relord.ORDR_SUBTYPE_CD IN (''''AR'''', ''''RO'''')  AND (NOT(((n.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND o.DOM_PROCESS_FLG_CD = ''''N'''' )
									OR (n.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''') )) 
								AND o.ORDR_ID = '+ CONVERT(VARCHAR,@M5_ORDR_ID) + 
								 ' AND ((n.CPE_DEVICE_ID IS NULL) OR (n.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN n.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))))			
								 
								'')'
						
						INSERT INTO #MACH5_ORDR_CMPNT
						EXEC sp_executesql @SQL	
						
						IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
							SET @QDA_ORDR_ID = 'MD'
						ELSE
							SET @QDA_ORDR_ID = 'X'
							
						TRUNCATE TABLE #MACH5_ORDR_CMPNT
					END
				END
				
			ELSE IF @ORDR_TYPE_CD = 'CNI'
				BEGIN
					
					SELECT @RLTD_ORDR_ID = RLTD_ORDR_ID 
						FROM #MACH5_ORDR WITH (NOLOCK)
						
					SET @SQL = ''
					SET @SQL = 'UPDATE 	#MACH5_ORDR
									SET  PRNT_FTN = A.ORDER_NBR ' + 
									' FROM ' + 
									' ( SELECT ORDER_NBR FROM ' +
									' OPENQUERY(M5,' +
									'''	SELECT	ORDER_NBR
										FROM	Mach5.V_V5U_ORDR 
													WHERE ORDR_ID = ' + CONVERT(VARCHAR(20),@RLTD_ORDR_ID)+ ''')) AS A'
				
									
					EXEC sp_executeSQL @SQL
				
					SET @SQL = 'SELECT *
									FROM OPENQUERY(M5,''SELECT DISTINCT '
								+ CONVERT(VARCHAR,@M5_ORDR_ID) +
								' AS ORDR_ID, 0 AS QDA_ORDR_ID, c.ORDR_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
								c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
								c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
								CASE WHEN n.NVTRY_ID IS NULL THEN  c.ORDR_CMPNT_ID	ELSE n.NVTRY_ID	END AS NVTRY_ID,c.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
								c.CPE_CONTRACT_TYPE,c.CPE_DLVRY_DUTIES_AMT,c.CPE_DLVRY_DUTIES_CD,
								c.CPE_DROP_SHIP_FLG_CD,c.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
								c.CPE_MANAGED_BY,c.CPE_MFR_ID,c.CPE_MFR_NME,c.CPE_MFR_PART_NBR,
								c.CPE_MNGD_FLG_CD,c.CPE_SPRINT_MNT_FLG_CD,c.CPE_MSCP_CD,c.CPE_LIST_PRICE,c.CPE_REQD_ON_SITE_DT,
								c.CPE_SHIP_CLLI_CD,c.CPE_SPRINT_TEST_TN_NBR,
								c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
								c.QTY, c.NME, c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, c.CPE_RECORD_ONLY_FLG_CD,
								c.CPE_CMPN_FMLY, c.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, ''''CN'''' AS STUS_CD, c.CPE_MFR_PS_ID,
								c.CPE_ALT_SHIP_ADR,c.CPE_ALT_SHIP_ADR_CTRY_CD,c.CPE_ALT_SHIP_ADR_CTY_NME,
								c.CPE_ALT_SHIP_ADR_LINE_1,c.CPE_ALT_SHIP_ADR_LINE_2,c.CPE_ALT_SHIP_ADR_LINE_3,
								c.CPE_ALT_SHIP_ADR_PRVN_NME,c.CPE_ALT_SHIP_ADR_PSTL_CD,
								c.CPE_ALT_SHIP_ADR_ST_PRVN_CD, c.CPE_ACCS_PRVDR_TN_NBR, c.CPE_EQUIPMENT_ONLY_FLG_CD,c.CPE_REPLMT,
								c.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR,c.CKT_ID,c.NTWK_USR_ADR,a.CSG_LVL, n.QUOTE_ID,c.COS_CD,
								c.SMRT_ACCT_DOMN_TXT,c.VRTL_ACCT_DOMN_TXT, c.CPE_CUST_PRVD_SERIAL_NBR, c.PROD_CD
							FROM	MACH5.V_V5U_ORDR_CMPNT c
							LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID= c.NVTRY_ID
							LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_ORDR  o on c.ORDR_ID = o.ORDR_ID 
							LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = c.PORT_SPD_CD
							LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON c.ACCS_BDWD_CD = bd.TYPE_CD
							WHERE ((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND o.DOM_PROCESS_FLG_CD = ''''N'''' )
									OR ((c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''')  ))) 
								AND (
										c.ORDR_CMPNT_ID IN (SELECT ORDR_CMPNT_ID FROM Mach5.V_V5U_ORDR_CNCL_CMPNT WHERE ORDR_ID = ' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ ')	
									) AND c.ORDR_ID = ' + CONVERT(VARCHAR(10),@RLTD_ORDR_ID) + '
									AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))'')'
															
					--PRINT @SQL
					INSERT INTO #MACH5_ORDR_CMPNT
					EXEC sp_executesql @SQL
					
					IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
					BEGIN
						SET @SQL = 'SELECT *
									FROM OPENQUERY(M5,''SELECT '
								+ CONVERT(VARCHAR,@M5_ORDR_ID) +
								' AS ORDR_ID, 0 AS QDA_ORDR_ID, c.ORDR_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
								c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
								c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
								CASE WHEN n.NVTRY_ID IS NULL THEN  c.ORDR_CMPNT_ID	ELSE n.NVTRY_ID	END AS NVTRY_ID,c.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
								c.CPE_CONTRACT_TYPE,c.CPE_DLVRY_DUTIES_AMT,c.CPE_DLVRY_DUTIES_CD,
								c.CPE_DROP_SHIP_FLG_CD,c.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
								c.CPE_MANAGED_BY,c.CPE_MFR_ID,c.CPE_MFR_NME,c.CPE_MFR_PART_NBR,
								c.CPE_MNGD_FLG_CD,c.CPE_SPRINT_MNT_FLG_CD,c.CPE_MSCP_CD,c.CPE_LIST_PRICE,c.CPE_REQD_ON_SITE_DT,
								c.CPE_SHIP_CLLI_CD,c.CPE_SPRINT_TEST_TN_NBR,
								c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
								c.QTY, c.NME, c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, c.CPE_RECORD_ONLY_FLG_CD,
								c.CPE_CMPN_FMLY, c.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, ''''CN'''' AS STUS_CD, c.CPE_MFR_PS_ID,
								c.CPE_ALT_SHIP_ADR,c.CPE_ALT_SHIP_ADR_CTRY_CD,c.CPE_ALT_SHIP_ADR_CTY_NME,
								c.CPE_ALT_SHIP_ADR_LINE_1,c.CPE_ALT_SHIP_ADR_LINE_2,c.CPE_ALT_SHIP_ADR_LINE_3,
								c.CPE_ALT_SHIP_ADR_PRVN_NME,c.CPE_ALT_SHIP_ADR_PSTL_CD,
								c.CPE_ALT_SHIP_ADR_ST_PRVN_CD, c.CPE_ACCS_PRVDR_TN_NBR, c.CPE_EQUIPMENT_ONLY_FLG_CD,c.CPE_REPLMT,
								c.PRE_QUAL_NBR, c.PRIVATE_LINE_NBR,c.CKT_ID,c.NTWK_USR_ADR,a.CSG_LVL,n.QUOTE_ID,n.COS_CD,
								c.SMRT_ACCT_DOMN_TXT,c.VRTL_ACCT_DOMN_TXT, c.CPE_CUST_PRVD_SERIAL_NBR,n.PROD_CD
							FROM	MACH5.V_V5U_ORDR_CMPNT c
							LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID= c.NVTRY_ID
							LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=n.CUST_ACCT_ID
							LEFT JOIN MACH5.V_V5U_ORDR  o on c.ORDR_ID = o.ORDR_ID 
							LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = c.PORT_SPD_CD
							LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON c.ACCS_BDWD_CD = bd.TYPE_CD
							WHERE ROWNUM<=1 AND (
										c.ORDR_CMPNT_ID IN (SELECT ORDR_CMPNT_ID FROM Mach5.V_V5U_ORDR_CNCL_CMPNT WHERE ORDR_ID = ' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ ')	
									) AND c.ORDR_ID = ' + CONVERT(VARCHAR(10),@RLTD_ORDR_ID) + ' AND (NOT(((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND o.DOM_PROCESS_FLG_CD = ''''N'''' )
									OR ((c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''')  ))) 								
									AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))))'')'
						
						INSERT INTO #MACH5_ORDR_CMPNT
						EXEC sp_executesql @SQL	
						
						IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
							SET @QDA_ORDR_ID = 'MD'
						ELSE
							SET @QDA_ORDR_ID = 'X'
							
						TRUNCATE TABLE #MACH5_ORDR_CMPNT
					END
				END
			
			ELSE IF @ORDR_TYPE_CD = 'CND'
				BEGIN
					
					SELECT @RLTD_ORDR_ID = RLTD_ORDR_ID 
						FROM #MACH5_ORDR WITH (NOLOCK)
						
					SET @SQL = ''
					SET @SQL = 'UPDATE 	#MACH5_ORDR
									SET  PRNT_FTN = A.ORDER_NBR ' + 
									' FROM ' + 
									' ( SELECT ORDER_NBR FROM ' +
									' OPENQUERY(M5,' +
									'''	SELECT	ORDER_NBR
										FROM	Mach5.V_V5U_ORDR 
													WHERE ORDR_ID = ' + CONVERT(VARCHAR(20),@RLTD_ORDR_ID)+ ''')) AS A'
																		
					EXEC sp_executeSQL @SQL
				
					SET @SQL = 'SELECT *
							FROM OPENQUERY(M5,''SELECT DISTINCT '
														+ CONVERT(VARCHAR,@M5_ORDR_ID) +
														',0 AS QDA_ORDR_ID, d.ORDR_DISC_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
															c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
															c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
															c.NVTRY_ID,o.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
															c.CPE_CONTRACT_TYPE,o.CPE_DLVRY_DUTIES_AMT,o.CPE_DLVRY_DUTIES_CD,
															o.CPE_DROP_SHIP_FLG_CD,o.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
															o.CPE_MANAGED_BY,c.MFR_ID as CPE_MFR_ID,c.MFR_NME AS CPE_MFR_NME,c.MFR_PART_NBR AS CPE_MFR_PART_NBR,
															c.CPE_MNGD_FLG_CD,o.CPE_SPRINT_MNT_FLG_CD,o.CPE_MSCP_CD,o.CPE_LIST_PRICE,o.CPE_REQD_ON_SITE_DT,
															o.CPE_SHIP_CLLI_CD,o.CPE_SPRINT_TEST_TN_NBR,
															c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															c.QTY, c.NME , c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, o.CPE_RECORD_ONLY_FLG_CD,
															o.CPE_CMPN_FMLY, o.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, ''''CN'''' as STUS_CD, o.CPE_MFR_PS_ID,
															o.CPE_ALT_SHIP_ADR,o.CPE_ALT_SHIP_ADR_CTRY_CD,o.CPE_ALT_SHIP_ADR_CTY_NME,
															o.CPE_ALT_SHIP_ADR_LINE_1,o.CPE_ALT_SHIP_ADR_LINE_2,o.CPE_ALT_SHIP_ADR_LINE_3,
															o.CPE_ALT_SHIP_ADR_PRVN_NME,o.CPE_ALT_SHIP_ADR_PSTL_CD,
															o.CPE_ALT_SHIP_ADR_ST_PRVN_CD, o.CPE_ACCS_PRVDR_TN_NBR, o.CPE_EQUIPMENT_ONLY_FLG_CD, o.CPE_REPLMT,
															o.PRE_QUAL_NBR, o.PRIVATE_LINE_NBR, c.CKT_ID, c.NTWK_USR_ADR,a.CSG_LVL,c.QUOTE_ID,c.COS_CD,
															o.SMRT_ACCT_DOMN_TXT,o.VRTL_ACCT_DOMN_TXT, c.CPE_CUST_PRVD_SERIAL_NBR, c.PROD_CD
														FROM	MACH5.V_V5U_NVTRY c
														INNER JOIN Mach5.V_V5U_ORDR_DISC_CMPNT d ON d.NVTRY_CMPNT_ID = c.NVTRY_ID
														INNER JOIN MACH5.V_V5U_ORDR ord ON d.ORDR_ID = ord.ORDR_ID
														LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=c.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_ORDR_CMPNT o ON c.NVTRY_ID = o.NVTRY_ID
														LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = o.PORT_SPD_CD
														LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON o.ACCS_BDWD_CD = bd.TYPE_CD
														WHERE ((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND ord.DOM_PROCESS_FLG_CD = ''''N'''' ) 
																OR ((c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''')  ))) 
															AND d.ORDR_DISC_CMPNT_ID IN	(SELECT		ORDR_CMPNT_ID 
																						FROM	Mach5.V_V5U_ORDR_CNCL_CMPNT 
																						WHERE ORDR_ID = ' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ ')
															 AND d.ORDR_ID = ' + CONVERT(VARCHAR(10),@RLTD_ORDR_ID) + '
															 AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))'')'								
					
					--print @SQL
					INSERT INTO #MACH5_ORDR_CMPNT
					EXEC sp_executesql @SQL
					
					IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
					BEGIN
							SET @SQL = 'SELECT *
							FROM OPENQUERY(M5,''SELECT '
														+ CONVERT(VARCHAR,@M5_ORDR_ID) +
														',0 AS QDA_ORDR_ID, d.ORDR_DISC_CMPNT_ID, c.CMPNT_TYPE_CD,c.ONE_STOP_SHOP_FLG_CD,
															c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
															c.ACCS_CXR_CD,c.ACCS_CXR_NME,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD, p.PORT_SPD_DES, 
															c.NVTRY_ID,o.CPE_ACCS_PRVDR_NME,c.CPE_CONTRACT_TERM,
															c.CPE_CONTRACT_TYPE,o.CPE_DLVRY_DUTIES_AMT,o.CPE_DLVRY_DUTIES_CD,
															o.CPE_DROP_SHIP_FLG_CD,o.CPE_EQPT_CKT_ID,c.CPE_ITEM_ID,
															o.CPE_MANAGED_BY,c.MFR_ID as CPE_MFR_ID,c.MFR_NME AS CPE_MFR_NME,c.MFR_PART_NBR AS CPE_MFR_PART_NBR,
															c.CPE_MNGD_FLG_CD,o.CPE_SPRINT_MNT_FLG_CD,o.CPE_MSCP_CD,o.CPE_LIST_PRICE,o.CPE_REQD_ON_SITE_DT,
															o.CPE_SHIP_CLLI_CD,o.CPE_SPRINT_TEST_TN_NBR,
															c.CPE_DEVICE_ID, c.EQPT_ID, c.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
															c.QTY, c.NME , c.CPE_VNDR_DSCNT, c.CPE_ITEM_MSTR_NBR, o.CPE_RECORD_ONLY_FLG_CD,
															o.CPE_CMPN_FMLY, o.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, ''''CN'''' as STUS_CD, o.CPE_MFR_PS_ID,
															o.CPE_ALT_SHIP_ADR,o.CPE_ALT_SHIP_ADR_CTRY_CD,o.CPE_ALT_SHIP_ADR_CTY_NME,
															o.CPE_ALT_SHIP_ADR_LINE_1,o.CPE_ALT_SHIP_ADR_LINE_2,o.CPE_ALT_SHIP_ADR_LINE_3,
															o.CPE_ALT_SHIP_ADR_PRVN_NME,o.CPE_ALT_SHIP_ADR_PSTL_CD,
															o.CPE_ALT_SHIP_ADR_ST_PRVN_CD, o.CPE_ACCS_PRVDR_TN_NBR, o.CPE_EQUIPMENT_ONLY_FLG_CD, o.CPE_REPLMT,
															o.PRE_QUAL_NBR, o.PRIVATE_LINE_NBR, c.CKT_ID, c.NTWK_USR_ADR,a.CSG_LVL, c.QUOTE_ID,c.COS_CD,
															o.SMRT_ACCT_DOMN_TXT,o.VRTL_ACCT_DOMN_TXT, c.CPE_CUST_PRVD_SERIAL_NBR,c.PROD_CD
														FROM	MACH5.V_V5U_NVTRY c
														INNER JOIN Mach5.V_V5U_ORDR_DISC_CMPNT d ON d.NVTRY_CMPNT_ID = c.NVTRY_ID
														INNER JOIN MACH5.V_V5U_ORDR ord ON d.ORDR_ID = ord.ORDR_ID
														LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID=c.CUST_ACCT_ID
														LEFT JOIN MACH5.V_V5U_ORDR_CMPNT o ON c.NVTRY_ID = o.NVTRY_ID
														LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = o.PORT_SPD_CD
														LEFT JOIN MACH5.V_V5U_ACCS_BDWD_TYPE bd ON o.ACCS_BDWD_CD = bd.TYPE_CD
														WHERE ROWNUM<=1 AND d.ORDR_DISC_CMPNT_ID IN	(SELECT		ORDR_CMPNT_ID 
																						FROM	Mach5.V_V5U_ORDR_CNCL_CMPNT 
																						WHERE ORDR_ID = ' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ ')
															 AND d.ORDR_ID = ' + CONVERT(VARCHAR(10),@RLTD_ORDR_ID) + ' AND (NOT(((c.CMPNT_TYPE_CD IN (''''ACCS'''',''''PORT'''') AND ord.DOM_PROCESS_FLG_CD = ''''N'''' ) 
																OR ((c.CMPNT_TYPE_CD IN (''''CPE'''',''''OCPE'''', ''''CPEM'''',''''CPEP'''',''''CPES'''')  ))) 
															 AND ((c.CPE_DEVICE_ID IS NULL) OR (c.CPE_DEVICE_ID = CASE (''''' + @DEVICE_ID +''''') WHEN ''''null'''' THEN c.CPE_DEVICE_ID ELSE ''''' + @DEVICE_ID + ''''' END ))))'')'
						
						INSERT INTO #MACH5_ORDR_CMPNT
						EXEC sp_executesql @SQL	
						
						IF NOT EXISTS (SELECT 'X' FROM #MACH5_ORDR_CMPNT)
							SET @QDA_ORDR_ID = 'MD'
						ELSE
							SET @QDA_ORDR_ID = 'X'
							
						TRUNCATE TABLE #MACH5_ORDR_CMPNT
					END
				END

			Select * from #MACH5_ORDR_CMPNT

			
			IF @ORDR_TYPE_CD = 'DC'
				BEGIN
					SET @SQL = ''
					SET @SQL = 'Select *
						From OpenQuery(M5,''Select	DISTINCT c.ORDR_ID
													,na.ACCS_TYPE_CD
													,na.ACCS_BDWD_CD
													,o.ORDER_NBR
													,na.CKT_ID
													,na.NTWK_USR_ADR
													,na.CMPNT_TYPE_CD
													,na.PRIVATE_LINE_NBR
													,na.NME
											From MACH5.V_V5U_NVTRY na
												LEFT JOIN Mach5.V_V5U_ORDR_DISC_CMPNT c  ON c.NVTRY_CMPNT_ID = na.NVTRY_ID AND na.CMPNT_TYPE_CD in ( ''''ACCS'''',''''ACST'''')
												LEFT JOIN MACH5.V_V5U_ORDR o ON na.ORIG_ORDR_ID = o.ORDR_ID
											Where c.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) +
											''')'	
					
					
					INSERT INTO #MACH5_DISC_ORDR_CMPNT(M5_ORDR_ID,ACCS_TYPE_CD,ACCS_BDWD_CD,ORIG_ORDR_NBR,CKT_ID,NTWK_USR_ADR,CMPNT_TYPE_CD,PRIVATE_LINE_NBR,NME)
					EXEC sp_executeSQL @SQL


					Select * From #MACH5_DISC_ORDR_CMPNT					

					SET @SQL = ''
					SET @SQL = ' UPDATE #MACH5_DISC_ORDR_CMPNT 
									SET PORT_SPD_DES = A.PORT_SPD_DES 
										FROM ( SELECT PORT_SPD_DES 
												FROM OpenQuery(M5,'' SELECT DISTINCT p.PORT_SPD_DES 
																		FROM	MACH5.V_V5U_ORDR_DISC_CMPNT c 
																			LEFT JOIN Mach5.V_V5U_NVTRY np ON c.NVTRY_CMPNT_ID = np.NVTRY_ID AND np.CMPNT_TYPE_CD = ''''PORT'''' 
																			LEFT JOIN MACH5.V_V5U_PORT_SPEED p ON p.PORT_SPD_CD = np.PORT_SPD_CD
																		WHERE c.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) +
																		'''))A'

					EXEC sp_executeSQL @SQL
					Select * From #MACH5_DISC_ORDR_CMPNT
					
				END
			
			
			IF @ORDR_TYPE_CD in ('AR','RO')
				OR 
				EXISTS (SELECT 'X'FROM #MACH5_ORDR_CMPNT)
				
				BEGIN

					DECLARE @CUST_ACCT_ID INT = 0
					SELECT @CUST_ACCT_ID =
							CASE
								WHEN @ORDR_TYPE_CD = 'IN' AND ISNULL(MOVE_TO_ACCT_ID,0) <> 0 THEN MOVE_TO_ACCT_ID
								ELSE PRNT_ACCT_ID 
							END
					 FROM #MACH5_ORDR


					IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CUST',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_CUST
					CREATE TABLE #MACH5_ORDR_CUST 
						(
							CIS_LVL_TYPE Varchar(2),
							CUST_ID Varchar(9),
							BR_CD Varchar(2),
							SOI_CD Varchar(3),
							CUST_NME Varchar(200),
							STREET_ADR_1 varchar(255),
							STREET_ADR_2 varchar(255),
							STREET_ADR_3 varchar(255),
							CTRY_CD Varchar(2),
							CTY_NME Varchar(255),
							STT_CD Varchar(2),
							ZIP_PSTL_CD Varchar(15),
							BLDG_ID Varchar(255),
							FLOOR_ID varchar(255),
							ROOM_ID Varchar(255),
							BILL_CYC_ID Varchar(2),
							H1_ID	Varchar(9),
							TAX_XMPT_CD Varchar(1),
							CHARS_CUST_ID  VARCHAR(9),
							SITE_ID	VARCHAR(14),
							PRVN_NME VARCHAR(255),
							CSG_LVL  VARCHAR(5)
						)


					SET @SQL = ''
					SET @SQL = 	
						'SELECT * ' + 
							' FROM OPENQUERY (M5, ''Select DISTINCT CIS_HIER_LVL_CD,CIS_CUST_ID, BR_CD,SOI_CD,CUST_NME
															,LINE_1,LINE_2,LINE_3,CTRY_CD,CTY_NME,ST_PRVN_CD
															,PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1,TAX_XMPT_CD
															,CHARS_CUST_ID,SITE_ID,PRVN_NME, CSG_LVL
													From Mach5.V_V5U_CUST_ACCT  
													Where CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
													  AND TYPE_CD = DECODE(CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
													  AND ROWNUM = 1 '')'+
															
							
						   ' UNION ' + 
						   ' SELECT * FROM OPENQUERY (M5,''Select DISTINCT h4.CIS_HIER_LVL_CD,h4.CIS_CUST_ID, h4.BR_CD,h4.SOI_CD,h4.CUST_NME
																	,h4.LINE_1,h4.LINE_2,h4.LINE_3,h4.CTRY_CD,h4.CTY_NME,h4.ST_PRVN_CD
																	,h4.PSTL_CD,h4.BLDG_ID,h4.FLOOR_ID,h4.ROOM_ID,h4.BILL_CYC_ID,h4.H1,h4.TAX_XMPT_CD
																	,h4.CHARS_CUST_ID,h4.SITE_ID,h4.PRVN_NME, h4.CSG_LVL
															From Mach5.V_V5U_CUST_ACCT h4 
															INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.H4 = h4.CIS_CUST_ID
															Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ' AND h4.TYPE_CD=DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'+
						   
						   '	UNION	' + 
						   '	Select * FROM OPENQUERY 
									(M5, ''SELECT DISTINCT h1.CIS_HIER_LVL_CD,h1.CIS_CUST_ID, h1.BR_CD,h1.SOI_CD,h1.CUST_NME
																	,h1.LINE_1,h1.LINE_2,h1.LINE_3,h1.CTRY_CD,h1.CTY_NME,h1.ST_PRVN_CD
																	,h1.PSTL_CD,h1.BLDG_ID,h1.FLOOR_ID,h1.ROOM_ID,h1.BILL_CYC_ID,h1.H1,h1.TAX_XMPT_CD
																	,h1.CHARS_CUST_ID,h1.SITE_ID,h1.PRVN_NME, h1.CSG_LVL
																From Mach5.V_V5U_CUST_ACCT h1 
																INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.H1 = h1.CIS_CUST_ID
															Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ' AND h1.TYPE_CD=DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'
					
					--print @SQL
					INSERT INTO #MACH5_ORDR_CUST (CIS_LVL_TYPE ,CUST_ID ,BR_CD ,SOI_CD ,
													CUST_NME ,STREET_ADR_1 ,STREET_ADR_2 ,
													STREET_ADR_3,CTRY_CD,CTY_NME,STT_CD,
													ZIP_PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1_ID
													,TAX_XMPT_CD,CHARS_CUST_ID,SITE_ID,PRVN_NME,CSG_LVL)
					EXEC sp_executesql 	@SQL
									
					Select * From #MACH5_ORDR_CUST		

					IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CNTCT',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_CNTCT
					CREATE TABLE #MACH5_ORDR_CNTCT 
					(
						[CIS_LVL_TYPE] Varchar(2),
						[ROLE_CD] [Varchar](6),
						[PHN_NBR] [Varchar](20) ,
						[TME_ZONE_ID] [varchar](255) ,
						[FRST_NME] [varchar](255) ,
						[LST_NME] [varchar](255) ,
						[EMAIL_ADR] [varchar](255) ,
						[CNTCT_HR_TXT] [varchar](50) ,
						[NPA] [varchar](3) ,
						[NXX] [varchar](3) ,
						[STN_NBR] [varchar](4) ,
						[CTY_CD] [varchar](5) ,
						[ISD_CD] [varchar](3) ,
						[CNTCT_NME] [varchar](255) ,
						[PHN_EXT_NBR] [varchar](10),
						[FSA_MDUL_ID] [varchar] (3),
						[CSG_LVL]  VARCHAR(5)
					)

					SET @SQL = ''
					SET @SQL = '  SELECT * ' + 
								' FROM OPENQUERY (M5,
								''Select DISTINCT ''''H6'''' AS H6,
									vvcc.ROLE_CD AS ROLE_CD,
									CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN vvcc.DOM_PHN_NBR
										  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN vvcc.INTL_PHN_NBR
										  ELSE ''''''''
									END AS PHN_NBR, 
									vvcc.TME_ZN_CD AS TME_ZONE_ID,
									'''''''' AS FRST_NME,
									'''''''' AS LST_NME ,			
									vvcc.EMAIL_ADR AS EMAIL_ADR, 
									vvcc.AVLBLTY_HR_STRT || CASE WHEN LENGTH(vvcc.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||vvcc.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
									CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvcc.DOM_PHN_NBR,1,3) 
										  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvcc.INTL_PHN_NBR,1,3) 
										  ELSE ''''''''
									END AS NPA ,			
								   CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvcc.DOM_PHN_NBR,4,3) 
									  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvcc.INTL_PHN_NBR,4,3) 
									  ELSE ''''''''       
									END AS  NXX ,			
								   CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvcc.DOM_PHN_NBR,7,4) 
									  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvcc.INTL_PHN_NBR,7,4) 
									  ELSE ''''''''      
									END AS STN_NBR ,			
									'''''''' AS CTY_CD ,
									vvcc.INTL_CNTRY_DIAL_CD AS ISD_CD,			
									vvcc.CUST_NME AS CNTCT_NME ,			
									vvcc.PHN_EXT AS PHN_EXT_NBR,
									''''CIS'''' AS FSA_MDUL_ID,
									vvca.CSG_LVL
									From Mach5.v_V5U_CUST_CONTACT vvcc INNER JOIN
										 Mach5.V_V5U_CUST_ACCT  vvca ON vvca.CUST_ACCT_ID = vvcc.CUST_ACCT_ID
								  WHERE vvcc.CUST_ACCT_ID ='   + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'+


								'	UNION	' + 

								' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select DISTINCT ''''H4'''' AS H6,
										hc4.ROLE_CD AS ROLE_CD, 
										CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN hc4.DOM_PHN_NBR
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN hc4.INTL_PHN_NBR
										  ELSE ''''''''
										END AS PHN_NBR, 
										hc4.TME_ZN_CD AS TME_ZONE_ID,
										'''''''' AS FRST_NME,
										'''''''' AS LST_NME ,			
										hc4.EMAIL_ADR AS EMAIL_ADR,
										hc4.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc4.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc4.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
										CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,1,3) 
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,1,3) 
										  ELSE ''''''''
										  END AS NPA ,			
										CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,4,3) 
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,4,3) 
										  ELSE ''''''''       
										  END AS  NXX ,			
									   CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,7,4) 
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,7,4) 
										  ELSE ''''''''      
										  END AS STN_NBR ,			
										'''''''' AS CTY_CD ,
										hc4.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
										hc4.CUST_NME AS CNTCT_NME ,			
										hc4.PHN_EXT AS PHN_EXT_NBR,
										''''CIS'''' AS FSA_MDUL_ID,
										h6.CSG_LVL
								  From Mach5.V_V5U_CUST_ACCT h6 
									INNER JOIN Mach5.V_V5U_CUST_ACCT h4 ON h6.H4 = h4.CIS_CUST_ID
									INNER JOIN  Mach5.v_V5U_CUST_CONTACT hc4 ON h4.CUST_ACCT_ID = hc4.CUST_ACCT_ID
									Where h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
									  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
									  AND h4.TYPE_CD = DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'+


								'	UNION	' + 

								' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select DISTINCT ''''H1'''' AS H6,
									hc1.ROLE_CD AS ROLE_CD, 
									CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN hc1.DOM_PHN_NBR
										  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN hc1.INTL_PHN_NBR
										  ELSE ''''''''
										END AS PHN_NBR,  
									hc1.TME_ZN_CD AS TME_ZONE_ID,
									'''''''' AS FRST_NME,
									'''''''' AS LST_NME ,			
									hc1.EMAIL_ADR AS EMAIL_ADR,
									hc1.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc1.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc1.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
									CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,1,3) 
										WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,1,3) 
									  ELSE ''''''''
									  END AS NPA ,			
								   CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,4,3) 
									  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,4,3) 
									  ELSE ''''''''       
									  END AS  NXX ,			
								   CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,7,4) 
									  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,7,4) 
									  ELSE ''''''''      
									  END AS STN_NBR ,			
									'''''''' AS CTY_CD ,
									hc1.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
									hc1.CUST_NME AS CNTCT_NME ,			
									hc1.PHN_EXT AS PHN_EXT_NBR, 
									''''CIS'''' AS FSA_MDUL_ID,
									h6.CSG_LVL
							  From Mach5.V_V5U_CUST_ACCT h6 
								INNER JOIN Mach5.V_V5U_CUST_ACCT h1 ON h6.H1 = h1.CIS_CUST_ID
								INNER JOIN  Mach5.v_V5U_CUST_CONTACT hc1 ON h1.CUST_ACCT_ID = hc1.CUST_ACCT_ID
								Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
								  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
								  AND h1.TYPE_CD = DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' +
								
								'	UNION	' + 

								' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select DISTINCT ''''H2'''' AS H6,
										hc2.ROLE_CD AS ROLE_CD, 
										CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN hc2.DOM_PHN_NBR
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN hc2.INTL_PHN_NBR
										  ELSE ''''''''
										END AS PHN_NBR, 
										hc2.TME_ZN_CD AS TME_ZONE_ID,
										'''''''' AS FRST_NME,
										'''''''' AS LST_NME ,			
										hc2.EMAIL_ADR AS EMAIL_ADR,
										hc2.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc2.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc2.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
										CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc2.DOM_PHN_NBR,1,3) 
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc2.INTL_PHN_NBR,1,3) 
										  ELSE ''''''''
										  END AS NPA ,			
										CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc2.DOM_PHN_NBR,4,3) 
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc2.INTL_PHN_NBR,4,3) 
										  ELSE ''''''''       
										  END AS  NXX ,			
									   CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc2.DOM_PHN_NBR,7,4) 
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc2.INTL_PHN_NBR,7,4) 
										  ELSE ''''''''      
										  END AS STN_NBR ,			
										'''''''' AS CTY_CD ,
										hc2.INTL_CNTRY_DIAL_CD AS ISD_CD,			
										hc2.CUST_NME AS CNTCT_NME ,			
										hc2.PHN_EXT AS PHN_EXT_NBR,
										''''CIS'''' AS FSA_MDUL_ID,
										h6.CSG_LVL
									  From Mach5.V_V5U_CUST_ACCT h6 
											INNER JOIN Mach5.V_V5U_CUST_ACCT h2 ON h6.H2 = h2.CIS_CUST_ID
											INNER JOIN  Mach5.v_V5U_CUST_CONTACT hc2 ON h2.CUST_ACCT_ID = hc2.CUST_ACCT_ID
											Where h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
											  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
											  AND h2.TYPE_CD = DECODE(h2.CIS_HIER_LVL_CD,''''H2'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' 
		
						INSERT INTO #MACH5_ORDR_CNTCT 
						(
							[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
							[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
							[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR], [FSA_MDUL_ID], [CSG_LVL]
						)
						EXEC sp_executesql @SQL
					
						SET @SQL = ''
						SET @SQL =   
		  
					
			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select DISTINCT ''''H1'''' AS H1,
								atcH1.ROLE_CD AS ROLE_CD, 
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN atcH1.DOM_PHN_NBR
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN atcH1.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID,
								atcH1.FIRST_NME AS FRST_NME,
								atcH1.LAST_NME AS LST_NME ,			
								atcH1.EMAIL_ADR AS EMAIL_ADR, 
								'''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,1,3) 
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,4,3) 
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,7,4) 
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,
								atcH1.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
								atcH1.FIRST_NME || '''' ''''|| atcH1.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,
								''''ATM'''' AS FSA_MDUL_ID,
								h6.CSG_LVL
						From	Mach5.V_V5U_CUST_ACCT h6 
							INNER JOIN Mach5.V_V5U_CUST_ACCT h1 ON h6.H1 = h1.CIS_CUST_ID
							INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH1 ON atcH1.CUST_ACCT_ID = h1.CUST_ACCT_ID
						WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
						   AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
						   AND h1.TYPE_CD = DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' +
						
						'	UNION	' +  
		  
					
			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select DISTINCT ''''H4'''' AS H4,
								atcH4.ROLE_CD AS ROLE_CD, 
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN atcH4.DOM_PHN_NBR
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN atcH4.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID,
								atcH4.FIRST_NME AS FRST_NME,
								atcH4.LAST_NME AS LST_NME ,			
								atcH4.EMAIL_ADR AS EMAIL_ADR, 
								'''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,1,3) 
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,4,3) 
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,7,4) 
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,
								atcH4.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
								atcH4.FIRST_NME || '''' ''''|| atcH4.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,
								''''ATM'''' AS FSA_MDUL_ID,
								h6.CSG_LVL
						From Mach5.V_V5U_CUST_ACCT h6 
								INNER JOIN Mach5.V_V5U_CUST_ACCT h4 ON h6.H4 = h4.CIS_CUST_ID
								INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH4 ON atcH4.CUST_ACCT_ID = h4.CUST_ACCT_ID
						WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
						  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
						  AND h4.TYPE_CD = DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' +
						
						'	UNION	' +  
		  
					
			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select DISTINCT ''''H6'''' AS H6,
								vvat.ROLE_CD AS ROLE_CD, 
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN vvat.DOM_PHN_NBR
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN vvat.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID,
								vvat.FIRST_NME AS FRST_NME,
								vvat.LAST_NME AS LST_NME ,			
								vvat.EMAIL_ADR AS EMAIL_ADR, 
								'''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvat.DOM_PHN_NBR,1,3) 
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvat.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvat.DOM_PHN_NBR,4,3) 
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvat.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvat.DOM_PHN_NBR,7,4) 
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvat.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,
								vvat.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
								vvat.FIRST_NME || '''' ''''|| vvat.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,
								''''ATM'''' AS FSA_MDUL_ID,
								vvca.CSG_LVL
						From Mach5.v_V5U_ACCT_TEAM_CONTACT vvat INNER JOIN
							 Mach5.V_V5U_CUST_ACCT  vvca ON vvca.CUST_ACCT_ID = vvat.CUST_ACCT_ID
						WHERE vvat.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'  +
						'	UNION	' +  			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select ''''H2'''' AS H2,	atcH2.ROLE_CD AS ROLE_CD, 
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN atcH2.DOM_PHN_NBR
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN atcH2.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID, atcH2.FIRST_NME AS FRST_NME,atcH2.LAST_NME AS LST_NME ,atcH2.EMAIL_ADR AS EMAIL_ADR, '''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH2.DOM_PHN_NBR,1,3) 
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH2.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH2.DOM_PHN_NBR,4,3) 
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH2.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH2.DOM_PHN_NBR,7,4) 
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH2.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,atcH2.INTL_CNTRY_DIAL_CD AS ISD_CD ,atcH2.FIRST_NME || '''' ''''|| atcH2.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,''''ATM'''' AS FSA_MDUL_ID,	h6.CSG_LVL
						From Mach5.V_V5U_CUST_ACCT h6 
								INNER JOIN Mach5.V_V5U_CUST_ACCT h2 ON h6.H2 = h2.CIS_CUST_ID
								INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH2 ON atcH2.CUST_ACCT_ID = h2.CUST_ACCT_ID
						WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
						  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
						  AND h2.TYPE_CD = DECODE(h2.CIS_HIER_LVL_CD,''''H2'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'
								

						INSERT INTO #MACH5_ORDR_CNTCT 
							(
								[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
								[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
								[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID], [CSG_LVL]
							)
						EXEC sp_executesql @SQL
						
						
						SET @SQL = ''
						SET @SQL = 
							' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select DISTINCT ''''H6'''' AS H6,
										''''Hrchy'''' AS ROLE_CD, 
										CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN DOM_PHN_NBR 
											  WHEN DOM_INTL_IND_CD = ''''I'''' THEN CONCAT(INTL_CNTRY_DIAL_CD , INTL_PHN_NBR )  
											  ELSE ''''''''
										END AS PHN_NBR, 
										'''''''' AS TME_ZONE_ID,
										'''''''' AS FRST_NME,
										'''''''' AS LST_NME ,			
										'''''''' AS EMAIL_ADR, 
										'''''''' AS CNTCT_HR_TXT,  	
										CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,1,3) 
											  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,1,3) 
											  ELSE ''''''''
										END AS NPA ,			
									   CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,4,3) 
										  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,4,3)
										  ELSE ''''''''       
										END AS  NXX ,			
									   CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,7,4) 
										  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,7,4) 
										  ELSE ''''''''      
										END AS STN_NBR ,			
										'''''''' AS CTY_CD ,
										INTL_CNTRY_DIAL_CD AS ISD_CD ,			
										CUST_NME AS CNTCT_NME ,			
										PHN_EXT AS PHN_EXT_NBR,
										''''CIS'''' AS FSA_MDUL_ID,
										CSG_LVL
									From Mach5.V_V5U_CUST_ACCT
								  WHERE CUST_ACCT_ID ='   + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
								    AND TYPE_CD = DECODE(CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
									AND ROWNUM = 1 '')'
						
					
						INSERT INTO #MACH5_ORDR_CNTCT 
							(
								[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
								[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
								[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID], [CSG_LVL]
							)
						EXEC sp_executesql @SQL

					
						SET @SQL = ''
						SET @SQL = 
							' SELECT * ' + 
								' FROM OPENQUERY (M5,
											''Select DISTINCT ''''OD'''' AS H6,
												oc.ROLE_CD AS ROLE_CD, 
												CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN oc.DOM_PHN_NBR
													WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN oc.INTL_PHN_NBR
													ELSE ''''''''
												END AS PHN_NBR, 
												'''''''' AS TME_ZONE_ID,
												oc.FIRST_NME AS FRST_NME,
												oc.LAST_NME AS LST_NME ,			
												oc.EMAIL_ADR AS EMAIL_ADR,
												''''''''  AS CNTCT_HR_TXT,  	
												CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(oc.DOM_PHN_NBR,1,3) 
												  WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(oc.INTL_PHN_NBR,1,3) 
												  ELSE ''''''''
												  END AS NPA ,			
												CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(oc.DOM_PHN_NBR,4,3) 
												  WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(oc.INTL_PHN_NBR,4,3) 
												  ELSE ''''''''       
												  END AS  NXX ,			
											   CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(oc.DOM_PHN_NBR,7,4) 
												  WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(oc.INTL_PHN_NBR,7,4) 
												  ELSE ''''''''      
												  END AS STN_NBR ,			
												'''''''' AS CTY_CD ,
												oc.INTL_CNTRY_DIAL_CD AS ISD_CD,			
												oc.FIRST_NME || '''' ''''|| oc.LAST_NME AS CNTCT_NME,			
												'''''''' AS PHN_EXT_NBR,
												''''CIS'''' AS FSA_MDUL_ID,
												h6.CSG_LVL
									  From Mach5.V_V5U_ACCT_TEAM_CONTACT_ORDR oc
											INNER JOIN Mach5.V_V5U_ORDR od ON od.ORDR_ID = oc.ORDR_ID
											INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.CUST_ACCT_ID = od.PRNT_ACCT_ID
											Where oc.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')'


							
							INSERT INTO #MACH5_ORDR_CNTCT 
							(
								[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
								[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
								[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID], [CSG_LVL]
							)
						EXEC sp_executesql @SQL
						
						SELECT * FROM #MACH5_ORDR_CNTCT
						
						----Order Notes
						IF OBJECT_ID(N'tempdb..#MACH5_ORDR_NOTES',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_NOTES
						CREATE TABLE #MACH5_ORDR_NOTES
							(
								NTE_TYPE_CD Varchar(50),
								NTE_TXT		Varchar(max)
							)
						SET @SQL = ''
						SET @SQL = 'SELECT TYPE_CD,NTE_TXT 
										FROM OPENQUERY(M5,
											''SELECT  TYPE_CD,NTE_TXT 
													FROM V_V5U_ORDR_NOTE 
													WHERE ORDR_ID =' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')' 
						
						--print @SQL
						INSERT INTO #MACH5_ORDR_NOTES (NTE_TYPE_CD,NTE_TXT)
							EXEC sp_executesql @SQL
							
						Select * From #MACH5_ORDR_NOTES
						
						
						IF OBJECT_ID(N'tempdb..#MACH5_ORDR_SCA',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_SCA
						CREATE TABLE #MACH5_ORDR_SCA
							(
								SCA_NBR varchar(255),
								TYPE_CD varchar(4)
							)
						SET @SQL = ''
						SET @SQL = 'SELECT SCA_NBR, TYPE_CD 
										FROM OPENQUERY (M5,
											''SELECT DISTINCT SCA_NBR, TYPE_CD 
												FROM MACH5.V_V5U_ORDR_SCA WHERE ORDR_ID = '+ CONVERT(VARCHAR,@M5_ORDR_ID) + ''')' 
						INSERT INTO #MACH5_ORDR_SCA (SCA_NBR,TYPE_CD)
							EXEC sp_executesql @SQL
						
						SELECT * FROM #MACH5_ORDR_SCA
						
						IF EXISTS
						(
							SELECT 'X'
								FROM #MACH5_ORDR_SCA WITH (NOLOCK)
						)
						BEGIN
							
							SELECT @SCA_NBR = SUBSTRING(COALESCE(@SCA_NBR + '| ' + SCA_NBR, SCA_NBR),0,60)
										FROM #MACH5_ORDR_SCA WITH (NOLOCK)
							
							IF EXISTS
								(
									SELECT 'X'
										FROM #MACH5_ORDR_SCA WITH (NOLOCK)
										WHERE TYPE_CD = 'EQ'
								)
								BEGIN
									SELECT @SCA_NBR_CPE = SUBSTRING(COALESCE(@SCA_NBR_CPE + '| ' + SCA_NBR, SCA_NBR),0,60)
										FROM #MACH5_ORDR_SCA WITH (NOLOCK)
										WHERE TYPE_CD = 'EQ'	
								END
							Select @SCA_NBR, @SCA_NBR_CPE
						END
						
						DECLARE @Ctr Int = 0, @Cnt Int = 0, @CMPNT_TYPE_CD Varchar(4)
						
						SELECT @Cnt = COUNT(1) FROM #MACH5_ORDR_CMPNT
						
						Select @H5_H6_CUST_ID = CUST_ID
							FROM #MACH5_ORDR_CUST
							WHERE CIS_LVL_TYPE IN ('H5','H6')
						
						SELECT	@Cnt = COUNT(1) 
							FROM	#MACH5_ORDR o
								LEFT JOIN #MACH5_ORDR_CMPNT c on o.M5_ORDR_ID = c.M5_ORDR_ID  
							WHERE	c.CMPNT_TYPE_CD IN ('ACCS','PORT','ACST')
									AND o.ORDR_SUBTYPE_CD not in ('AR','RO')

						IF EXISTS (SELECT 'X' FROM #MACH5_ORDR WHERE ORDR_SUBTYPE_CD in ('AR','RO'))
							SET @Cnt=1
									
						IF @Cnt > 0
							BEGIN
								WHILE @Ctr < @Cnt 
								  BEGIN
									IF EXISTS
									(
										SELECT 'X'
											FROM 
												#MACH5_ORDR m WITH (NOLOCK)
												LEFT JOIN #MACH5_ORDR_CMPNT mo WITH (NOLOCK) ON m.M5_ORDR_ID = mo.M5_ORDR_ID
											WHERE ((m.PROD_TYPE_CD IN ('DN','DO','MP','MO','SN','SO','IN','IO')
												AND mo.CMPNT_TYPE_CD IN ('ACCS','PORT') AND mo.FLAG = 0)
												 OR m.ORDR_SUBTYPE_CD in ('AR','RO'))
												AND m.DOM_PROCESS_FLG_CD = 'N'
												
												
									)
									BEGIN
										OPEN SYMMETRIC KEY FS@K3y 
										DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
										
										INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('ACCS Loop:'+@TransName,'insertMach5OrderDetails', GETDATE())
										SET @ERRLog = @ERRLog + 'ACCS Loop:'
										--default region ID to AMNCI and later modified after order insertion in this SP
										INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID,DMSTC_CD, PLTFRM_CD,PROD_ID)	
												SELECT @OrderCategoryID, 1, 1, CASE t.DOM_PROCESS_FLG_CD
																					WHEN 'Y' THEN '0'
																					WHEN 'N' THEN '1'
																					ELSE '0'
																				END, ISNULL(p.PLTFRM_CD,''),c.PROD_CD
												FROM #MACH5_ORDR t WITH (NOLOCK)
											LEFT JOIN dbo.PLTFRM_MAPNG p WITH (NOLOCK) ON t.PROD_TYPE_CD = p.FSA_PROD_TYPE_CD
											LEFT JOIN (SELECT TOP 1 M5_ORDR_ID, PROD_CD FROM #MACH5_ORDR_CMPNT WITH (NOLOCK) WHERE ISNULL(PROD_CD,'') <> '')c ON c.M5_ORDR_ID = t.M5_ORDR_ID  
												WHERE t.PROD_TYPE_CD IN ('DN','DO','MP','MO','SN','SO', 'IN', 'IO')
										
										SELECT @ORDR_ID = SCOPE_IDENTITY()
									
										SET @FSA_ORDR_ID = @ORDR_ID
										
										IF @QDA_ORDR_ID != ''
											SET @QDA_ORDR_ID = @QDA_ORDR_ID + ',' + CONVERT(varchar,@FSA_ORDR_ID)
										ELSE
											SET @QDA_ORDR_ID = CONVERT(varchar,@FSA_ORDR_ID)
										
										UPDATE		dbo.ORDR	WITH (ROWLOCK)
											SET		PRNT_ORDR_ID	=	@ORDR_ID,
													H5_H6_CUST_ID	=	@H5_H6_CUST_ID,
													CSG_LVL_ID		=	(SELECT TOP 1 ISNULL(lcl.CSG_LVL_ID,0) FROM #MACH5_ORDR mo WITH (NOLOCK) LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=mo.CSG_LVL WHERE mo.M5_ORDR_ID = @M5_ORDR_ID),
													CHARS_ID		=	(SELECT TOP 1 CHARS_CUST_ID FROM #MACH5_ORDR WITH (NOLOCK) WHERE M5_ORDR_ID = @M5_ORDR_ID)
											WHERE	ORDR_ID			=	@ORDR_ID
										
											
																		
											
										-------FSA_ORDR
										INSERT INTO FSA_ORDR (ORDR_ID,ORDR_ACTN_ID,FTN,ORDR_TYPE_CD,ORDR_SUB_TYPE_CD,PROD_TYPE_CD,CUST_SIGNED_DT,CUST_WANT_DT,
																CUST_ORDR_SBMT_DT, CUST_CMMT_DT,CUST_PRMS_OCPY_CD,CUST_ACPT_ERLY_SRVC_CD,MULT_CUST_ORDR_CD,
																DISC_REAS_CD, TTRPT_OSS_CD,
																VNDR_VPN_CD,INSTL_VNDR_CD,INSTL_ESCL_CD, FSA_EXP_TYPE_CD,PRE_QUAL_NBR,PRNT_FTN,RELTD_FTN,
																SITE_ID, TTRPT_NW_ADR
																)
										(
											SELECT	TOP 1 @FSA_ORDR_ID,@ORDR_ACTN_ID,m.ORDER_NBR,
											CASE 
														WHEN m.ORDR_SUBTYPE_CD IN ('AR','RO') THEN 'IN'
														ELSE m.ORDR_TYPE_CD
													END,
													m.ORDR_SUBTYPE_CD,m.PROD_TYPE_CD,m.CUST_SIGN_DT,
													m.CUST_WANT_DT,m.CUST_SBMT_DT,m.CUST_CMMT_DT,m.CUST_PREM_OCCU_FLG_CD,m.CUST_ACPT_SRVC_FLG_CD,m.MULT_ORDR_SBMTD_FLG_CD,
													m.DSCNCT_REAS_CD,a.ONE_STOP_SHOP_FLG_CD,
													CASE 
														WHEN m.PROD_TYPE_CD IN ('MO','IO','IN') 
														THEN CASE WHEN ((SUBSTRING(a.ACCS_CXR_CD,1,3)='RES') AND LEN(ISNULL(a.ACCS_CXR_NME,''))>0)
															THEN SUBSTRING('RES'+'-'+a.ACCS_CXR_NME,1,100)
															ELSE a.ACCS_CXR_CD END	 
														ELSE a.ACCS_CXR_CD
													END	,
													CASE 
														WHEN m.PROD_TYPE_CD IN ('MO','SO','DO','IO','IN') 
														THEN CASE WHEN ((SUBSTRING(a.ACCS_CXR_CD,1,3)='RES') AND LEN(ISNULL(a.ACCS_CXR_NME,''))>0)
															THEN SUBSTRING('RES'+'-'+a.ACCS_CXR_NME,1,100)
															ELSE a.ACCS_CXR_CD END
														ELSE a.ACCS_CXR_CD
													END	,
													m.EXP_FLG_CD,	CASE m.EXP_TYPE_CD 
																										WHEN 'B' THEN 'IE'
																										ELSE m.EXP_TYPE_CD
																										END,
													ISNULL(a.PRE_QUAL_NBR, m.PRE_QUAL_NBR)-- added isnull on 3/8/18 for AR orders. dlp0278
													,CASE m.ORDR_TYPE_CD
														WHEN 'CN' THEN m.PRNT_FTN
														ELSE NULL
													  END
													,CASE m.ORDR_TYPE_CD
														WHEN 'CN' THEN NULL
														ELSE m.RLTD_FTN
													  END	 
													,CONVERT(VARCHAR,ISNULL(a.NVTRY_ID,'')) + ',' + CONVERT(VARCHAR,ISNULL(p.NVTRY_ID,'')) AS SITE_ID
													,ISNULL(a.NUA,p.NUA) AS NUA
													
												FROM #MACH5_ORDR m WITH (NOLOCK)
												LEFT JOIN #MACH5_ORDR_CMPNT a WITH (NOLOCK) ON m.M5_ORDR_ID = a.M5_ORDR_ID AND a.CMPNT_TYPE_CD = 'ACCS'
												LEFT JOIN #MACH5_ORDR_CMPNT p WITH (NOLOCK) ON m.M5_ORDR_ID = p.M5_ORDR_ID AND p.CMPNT_TYPE_CD = 'PORT'
												WHERE m.M5_ORDR_ID = @M5_ORDR_ID
												
										)
										
										--FSA_ORDR_CPE_LINE_ITEM
											INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM ([ORDR_ID], [LINE_ITEM_CD],
											[TTRPT_ACCS_TYPE_CD],
											[TTRPT_ACCS_ARNGT_CD],
											[CXR_ACCS_CD],
											[CKT_ID],
											[TTRPT_ACCS_TERM_DES],
											[TTRPT_ACCS_BDWD_TYPE],
											[PL_NBR],
											CMPNT_ID,
											ORDR_CMPNT_ID,
											TTRPT_COS_CD,
											MDS_DES)
											SELECT DISTINCT @FSA_ORDR_ID, 'ACS',
											a.ACCS_TYPE_CD, a.ACCS_ARNGT_CD,
											CASE 
															WHEN m.PROD_TYPE_CD IN ('MP','DN','SN','IO','IN') THEN a.ACCS_CXR_CD
															ELSE a.ACCS_CXR_CD
											END,
											a.CKT,
											a.ACCS_TERM_CD,
											a.ACCS_BDWD_CD,
											a.PL_NBR,
											a.NVTRY_ID,
											a.NVTRY_ID,
											a.COS_CD,
											a.NME
											FROM #MACH5_ORDR m WITH (NOLOCK)
											LEFT JOIN #MACH5_ORDR_CMPNT a WITH (NOLOCK) ON m.M5_ORDR_ID = a.M5_ORDR_ID AND a.CMPNT_TYPE_CD = 'ACCS'
											
											INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM ([ORDR_ID], [LINE_ITEM_CD], [INSTL_DSGN_DOC_NBR],
											[TTRPT_SPD_OF_SRVC_BDWD_DES],
											[PORT_RT_TYPE_CD],
											CMPNT_ID,
											ORDR_CMPNT_ID,
											MDS_DES)
											SELECT DISTINCT @FSA_ORDR_ID, 'PRT', p.DESIGN_DOC_NUM,
											p.PORT_SPD_DES,
											p.RATE_TYPE_CD,
											p.NVTRY_ID,
											p.NVTRY_ID,
											p.NME
											FROM #MACH5_ORDR_CMPNT p WITH (NOLOCK) WHERE p.CMPNT_TYPE_CD = 'PORT'
											
										--ORDR_EXP	
										IF EXISTS
										(
											SELECT 'X'
												FROM #MACH5_ORDR
												WHERE EXP_FLG_CD = 'Y'
										)
										BEGIN
											INSERT INTO ORDR_EXP (ORDR_ID,
																	EXP_TYPE_ID,
																	AUTHRZR_PHN_NBR,
																	AUTHRZR_TITLE_ID,
																	AUTHN_DT,
																	BILL_TO_COST_CTR_CD,
																	CREAT_DT,
																	CREAT_BY_USER_ID,
																	REC_STUS_ID
																		)
											(
												SELECT TOP 1	@FSA_ORDR_ID
																,CASE EXP_TYPE_CD
																		WHEN 'I' THEN 1
																		WHEN 'E' THEN 2
																		WHEN 'B' THEN 3
																		ELSE 4 
																	END AS EXP_TYPE_ID
																,APRVR_PHN
																,APRVR_TITLE
																,APPRVL_DT
																,[dbo].[RemoveSpecialCharacters](LEFT(SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10),
           PATINDEX('%[^0-9]%', SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10) + 'X') -1))
																,GETDATE()
																,1
																,1
													FROM #MACH5_ORDR WITH (NOLOCK)
													
											)
										END
										
										IF EXISTS	(SELECT 'X' FROM dbo.FSA_ORDR fo with (nolock)
														INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcl WITH (NOLOCK) ON fcl.ORDR_ID=fo.ORDR_ID
														where fo.ORDR_ID = @ORDR_ID
																AND fo.ORDR_SUB_TYPE_CD in ('AR','RO')
																AND fcl.LINE_ITEM_CD='ACS'
																AND (ISNULL(fcl.TTRPT_ACCS_BDWD_TYPE,'')=''))
											BEGIN
												DECLARE @BDWD_NME TABLE (BDWDTH VARCHAR(30))
												SELECT @FTN = FTN FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID
														
												SET @SQL = ''				
												SET @SQL = 'SELECT BDWD_NME FROM OPENQUERY(NRMBPM, ''SELECT BDWD_NME 
													FROM BPMF_OWNER.V_BPMF_COWS_CKT WHERE MACH5_ORDR_NBR = ''''' + @FTN + ''''' '')'
												
												INSERT INTO @BDWD_NME
												EXEC sp_executesql @SQL
												
												UPDATE fcl 
												SET TTRPT_ACCS_BDWD_TYPE = (SELECT TOP 1 BDWDTH FROM @BDWD_NME)
												FROM dbo.FSA_ORDR fo with (nolock)
														INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcl ON fcl.ORDR_ID=fo.ORDR_ID
														where fo.ORDR_ID = @ORDR_ID
																AND fo.ORDR_SUB_TYPE_CD in ('AR','RO')
																AND fcl.LINE_ITEM_CD='ACS'
																AND (ISNULL(fcl.TTRPT_ACCS_BDWD_TYPE,'')='')
												
											END
										
										-----FSA_ORDR_CUST
										INSERT INTO dbo.FSA_ORDR_CUST 
												(	ORDR_ID,
													CIS_LVL_TYPE,
													CUST_ID,
													BR_CD,
													SOI_CD,
													CREAT_DT,
													CUST_NME,
													CURR_BILL_CYC_CD,
													TAX_XMPT_CD,
													CHARS_CUST_ID,
													SITE_ID)
											(SELECT	DISTINCT @FSA_ORDR_ID,
													moc.CIS_LVL_TYPE,
													moc.CUST_ID,
													moc.BR_CD,
													moc.SOI_CD,
													GETDATE(),
													CASE
														WHEN (SELECT PROD_TYPE_CD FROM #MACH5_ORDR) IN ('IO','IN') THEN 'O-' + CUST_NME
														ELSE CUST_NME END,
													ISNULL(moc.BILL_CYC_ID,0),
													moc.TAX_XMPT_CD,
													moc.CHARS_CUST_ID,
													moc.SITE_ID
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												WHERE ISNULL(moc.CSG_LVL,'')=''
											)
											
										--Insert CSG_LVL <> ''
										INSERT INTO dbo.FSA_ORDR_CUST 
												(	ORDR_ID,
													CIS_LVL_TYPE,
													CUST_ID,
													BR_CD,
													SOI_CD,
													CREAT_DT,
													CURR_BILL_CYC_CD,
													TAX_XMPT_CD,
													CHARS_CUST_ID,
													SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
											SELECT	DISTINCT @FSA_ORDR_ID,
													moc.CIS_LVL_TYPE,
													moc.CUST_ID,
													moc.BR_CD,
													moc.SOI_CD,
													GETDATE(),
													ISNULL(moc.BILL_CYC_ID,0),
													moc.TAX_XMPT_CD,
													moc.CHARS_CUST_ID,
													moc.SITE_ID
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												WHERE ISNULL(moc.CSG_LVL,'')!=''
											
										INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
										SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, CASE
														WHEN (SELECT PROD_TYPE_CD FROM #MACH5_ORDR) IN ('IO','IN') THEN dbo.encryptString('O-' + moc.CUST_NME)
														ELSE dbo.encryptString(moc.CUST_NME) END
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												  AND foc.CUST_NME IS NULL
											
										delete from @tmpids
										IF NOT EXISTS
											(
												SELECT 'X'
													FROM dbo.FSA_ORDR_CUST WITH (NOLOCK)
													WHERE ORDR_ID = @ORDR_ID
														AND CIS_LVL_TYPE = 'H1'
											)
											BEGIN
												INSERT INTO dbo.FSA_ORDR_CUST 
												(	ORDR_ID,
													CIS_LVL_TYPE,
													CUST_ID,
													BR_CD,
													SOI_CD,
													CREAT_DT,
													CUST_NME,
													CURR_BILL_CYC_CD,
													TAX_XMPT_CD,
													CHARS_CUST_ID,
													SITE_ID)									
											(SELECT DISTINCT @FSA_ORDR_ID,
													'H1',
													moc.H1_ID,
													'',
													'',
													GETDATE(),
													'',
													moc.BILL_CYC_ID,
													moc.TAX_XMPT_CD,
													moc.CHARS_CUST_ID,
													moc.SITE_ID
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												WHERE ISNULL(moc.CSG_LVL,'')=''
												 AND moc.CIS_LVL_TYPE = 'H6'
											)

											INSERT INTO dbo.FSA_ORDR_CUST 
												(	ORDR_ID,
													CIS_LVL_TYPE,
													CUST_ID,
													BR_CD,
													SOI_CD,
													CREAT_DT,
													CURR_BILL_CYC_CD,
													TAX_XMPT_CD,
													CHARS_CUST_ID,
													SITE_ID)  output inserted.FSA_ORDR_CUST_ID into @tmpids							
											SELECT DISTINCT @FSA_ORDR_ID,
													'H1',
													moc.H1_ID,
													'',
													'',
													GETDATE(),
													moc.BILL_CYC_ID,
													moc.TAX_XMPT_CD,
													moc.CHARS_CUST_ID,
													moc.SITE_ID
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												 AND moc.CIS_LVL_TYPE = 'H6'
											

											INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
											SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString('')
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												 AND moc.CIS_LVL_TYPE = 'H6'
												 AND foc.CUST_NME IS NULL
											END
										
										delete from @tmpids
										----ORDR_ADR
										INSERT INTO dbo.ORDR_ADR
											(
												ORDR_ID,
												ADR_TYPE_ID,
												CREAT_DT,
												CREAT_BY_USER_ID,
												REC_STUS_ID,
												CTRY_CD,
												CIS_LVL_TYPE,
												FSA_MDUL_ID,
												STREET_ADR_1,
												STREET_ADR_2,
												CTY_NME,
												PRVN_NME,
												STT_CD,
												ZIP_PSTL_CD,
												BLDG_NME,
												FLR_ID,
												RM_NBR,
												HIER_LVL_CD,
												STREET_ADR_3
											)
										(
											SELECT	DISTINCT @FSA_ORDR_ID,
													CASE moc.CIS_LVL_TYPE
														WHEN 'H4' THEN 3
														WHEN 'H6' THEN 18
														WHEN 'H5' THEN 18
														ELSE 17
													END,
													GETDATE(),
													1,
													1,
													moc.CTRY_CD,
													moc.CIS_LVL_TYPE,
													'CIS',
													moc.STREET_ADR_1,
													moc.STREET_ADR_2,
													moc.CTY_NME,
													moc.PRVN_NME,
													moc.STT_CD,
													moc.ZIP_PSTL_CD,
													moc.BLDG_ID,
													moc.FLOOR_ID,
													moc.ROOM_ID,
													CASE moc.CIS_LVL_TYPE	
														WHEN 'H4' THEN 0
														WHEN 'H1' THEN 0
														ELSE 1
													END,
													moc.STREET_ADR_3
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												WHERE ISNULL(moc.CSG_LVL,'')=''
										)
										
										INSERT INTO dbo.ORDR_ADR
											(
												ORDR_ID,
												ADR_TYPE_ID,
												CREAT_DT,
												CREAT_BY_USER_ID,
												REC_STUS_ID,
												CTRY_CD,
												CIS_LVL_TYPE,
												FSA_MDUL_ID,
												HIER_LVL_CD
											)  output inserted.ORDR_ADR_ID into @tmpids
											SELECT	DISTINCT @FSA_ORDR_ID,
													CASE moc.CIS_LVL_TYPE
														WHEN 'H4' THEN 3
														WHEN 'H6' THEN 18
														WHEN 'H5' THEN 18
														ELSE 17
													END,
													GETDATE(),
													1,
													1,
													moc.CTRY_CD,
													moc.CIS_LVL_TYPE,
													'CIS',
													CASE moc.CIS_LVL_TYPE	
														WHEN 'H4' THEN 0
														WHEN 'H1' THEN 0
														ELSE 1
													END
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												WHERE ISNULL(moc.CSG_LVL,'')!=''
										
										
										INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
												STREET_ADR_2,
												CTY_NME,
												STT_PRVN_NME,
												STT_CD,
												ZIP_PSTL_CD,
												BLDG_NME,
												FLR_ID,
												RM_NBR,
												STREET_ADR_3)
											SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.STREET_ADR_1),
													dbo.encryptString(moc.STREET_ADR_2),
													dbo.encryptString(moc.CTY_NME),
													dbo.encryptString(moc.PRVN_NME),
													dbo.encryptString(moc.STT_CD),
													dbo.encryptString(moc.ZIP_PSTL_CD),
													dbo.encryptString(moc.BLDG_ID),
													dbo.encryptString(moc.FLOOR_ID),
													dbo.encryptString(moc.ROOM_ID),
													dbo.encryptString(moc.STREET_ADR_3)
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND oa.ORDR_ID = @FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												  AND oa.STREET_ADR_1 IS NULL
												
										delete from @tmpids
										------ORDR_CNTCT
										INSERT INTO dbo.ORDR_CNTCT
											(
												ORDR_ID,
												CNTCT_TYPE_ID,
												PHN_NBR,
												TME_ZONE_ID,
												CREAT_DT,
												CREAT_BY_USER_ID,
												REC_STUS_ID,
												FAX_NBR,
												ROLE_ID,
												CIS_LVL_TYPE,
												FSA_MDUL_ID,
												FRST_NME,
												LST_NME,
												EMAIL_ADR,
												CNTCT_HR_TXT,
												NPA,
												NXX,
												STN_NBR,
												CTY_CD,
												ISD_CD,
												CNTCT_NME,
												PHN_EXT_NBR
											)
										(
											SELECT DISTINCT	@FSA_ORDR_ID ,
													CASE c.ROLE_CD
															WHEN 'Hrchy' THEN 17
															ELSE 1
														END AS CNTCT_TYPE_ID,
													c.PHN_NBR,
													l.TME_ZONE_ID,
													GETDATE(),
													1,
													1,
													'',
													lr.ROLE_ID,
													c.CIS_LVL_TYPE,
													c.FSA_MDUL_ID,
													c.FRST_NME,
													c.LST_NME,
													c.EMAIL_ADR,
													c.CNTCT_HR_TXT,
													c.NPA,
													c.NXX,
													c.STN_NBR,
													c.CTY_CD,
													c.ISD_CD,
													c.CNTCT_NME,
													c.PHN_EXT_NBR
												FROM #MACH5_ORDR_CNTCT c
											INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
											LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																TME_ZONE_NME	AS TME_ZONE_NME 
															FROM dbo.LK_TME_ZONE 
															GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
											WHERE ISNULL(c.CSG_LVL,'')=''
										)
										
										INSERT INTO dbo.ORDR_CNTCT
											(
												ORDR_ID,
												CNTCT_TYPE_ID,
												PHN_NBR,
												TME_ZONE_ID,
												CREAT_DT,
												CREAT_BY_USER_ID,
												REC_STUS_ID,
												FAX_NBR,
												ROLE_ID,
												CIS_LVL_TYPE,
												FSA_MDUL_ID,
												CNTCT_HR_TXT,
												NPA,
												NXX,
												STN_NBR,
												CTY_CD,
												ISD_CD,
												PHN_EXT_NBR
											)  output inserted.ORDR_CNTCT_ID into @tmpids
											SELECT DISTINCT	@FSA_ORDR_ID ,
													CASE c.ROLE_CD
															WHEN 'Hrchy' THEN 17
															ELSE 1
														END AS CNTCT_TYPE_ID,
													c.PHN_NBR,
													l.TME_ZONE_ID,
													GETDATE(),
													1,
													1,
													'',
													lr.ROLE_ID,
													c.CIS_LVL_TYPE,
													c.FSA_MDUL_ID,
													c.CNTCT_HR_TXT,
													c.NPA,
													c.NXX,
													c.STN_NBR,
													c.CTY_CD,
													c.ISD_CD,
													c.PHN_EXT_NBR
												FROM #MACH5_ORDR_CNTCT c
											INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
											LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																TME_ZONE_NME	AS TME_ZONE_NME 
															FROM dbo.LK_TME_ZONE 
															GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
											WHERE ISNULL(c.CSG_LVL,'')!=''
										
										
										INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, FRST_NME,
												LST_NME,
												CUST_EMAIL_ADR,
												CUST_CNTCT_NME)
											SELECT	DISTINCT oc.ORDR_CNTCT_ID, 15, dbo.encryptstring(c.FRST_NME),
													dbo.encryptstring(c.LST_NME),
													dbo.encryptstring(c.EMAIL_ADR),
													dbo.encryptstring(c.CNTCT_NME)
												FROM #MACH5_ORDR_CNTCT c WITH (NOLOCK)
												INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
											INNER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ROLE_ID=lr.ROLE_ID AND oc.CIS_LVL_TYPE=c.CIS_LVL_TYPE AND oc.ORDR_ID = @FSA_ORDR_ID
											INNER JOIN @tmpids tmpids ON tmpids.tmpid = oc.ORDR_CNTCT_ID
											WHERE ISNULL(c.CSG_LVL,'')!=''
											  AND oc.FRST_NME IS NULL
										
										-----ORDER NOTES
										IF EXISTS
											(
												SELECT 'X'
													FROM #MACH5_ORDR_NOTES WITH (NOLOCK)
											)
										BEGIN
											INSERT INTO ORDR_NTE
											(
												NTE_TYPE_ID,
												ORDR_ID,
												CREAT_DT,
												CREAT_BY_USER_ID,
												REC_STUS_ID,
												NTE_TXT
											)
											(
												SELECT DISTINCT
													CASE NTE_TYPE_CD
														WHEN 'CPEN' THEN 26
														WHEN 'CPES' THEN 27
														WHEN 'CPEP' THEN 28
														ELSE 25
													END, 
													@FSA_ORDR_ID,		
													GETDATE(),
													1,
													1,
													NTE_TXT		
												FROM #MACH5_ORDR_NOTES
												WHERE ISNULL(NTE_TXT,'') <> ''
											)
										END
										
										----SCA_NBR
										IF EXISTS
											(
												SELECT 'X'
													FROM #MACH5_ORDR_SCA WITH (NOLOCK)
											)
											BEGIN
												--SELECT @SCA_NBR = COALESCE(@SCA_NBR + '| ' + SCA_NBR, SCA_NBR)
												--	FROM #MACH5_ORDR_SCA WITH (NOLOCK)
													
												UPDATE dbo.FSA_ORDR SET SCA_NBR = @SCA_NBR WHERE ORDR_ID = @ORDR_ID
											END
											
										--FSA_ORDR_CPE_LINE_ITEM For Disconnect Orders
										IF EXISTS
											(
												SELECT 'X'
													FROM #MACH5_DISC_ORDR_CMPNT WITH (NOLOCK)
													WHERE CMPNT_TYPE_CD	 = 'ACCS'
											)
											BEGIN
												INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
													(
														ORDR_ID,
														CREAT_DT,
														LINE_ITEM_CD,
														FSA_ORGNL_INSTL_ORDR_ID,
														CXR_CKT_ID,
														TTRPT_ACCS_TYPE_CD,
														TTRPT_SPD_OF_SRVC_BDWD_DES,
														TTRPT_ACCS_TYPE_DES,
														FMS_CKT_NBR,
														DISC_NW_ADR,
														DISC_PL_NBR
													)
												SELECT DISTINCT
													@FSA_ORDR_ID,
													GETDATE(),
													'DSC',
													ORIG_ORDR_NBR,
													CKT_ID,
													ACCS_TYPE_CD,
													PORT_SPD_DES,
													ACCS_BDWD_CD,
													CASE WHEN (CKT_ID LIKE '%[^0-9]%') THEN 0 ELSE CKT_ID END,
													NTWK_USR_ADR,
													PRIVATE_LINE_NBR
													FROM #MACH5_DISC_ORDR_CMPNT WHERE CMPNT_TYPE_CD = 'ACCS'
											END	
	
									--Update Region ID for xNCI Orders
										Update od
											SET od.RGN_ID = 
															CASE 
																WHEN ISNULL(oa.STT_CD,CONVERT(VARCHAR, DecryptByKey(csdoa.STT_CD))) = 'GU' THEN 3
																ELSE ISNULL(lc.RGN_ID,od.RGN_ID)
															END 
											FROM dbo.ORDR od WITH (ROWLOCK)
										INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON od.ORDR_ID = oa.ORDR_ID
										INNER JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD
										LEFT JOIN dbo.CUST_SCRD_DATA csdoa WITH (NOLOCK) ON csdoa.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdoa.SCRD_OBJ_TYPE_ID=14 
											WHERE od.ORDR_ID = @ORDR_ID
												AND oa.CIS_LVL_TYPE = 'H6'
												
									IF @CHNG_ORDR_TYPE_FLG != 1
										BEGIN
											----State Machine
												Exec dbo.InsertInitialTask @ORDR_ID,@OrderCategoryID
											--SELECT @ORDR_ID	
										END
									
									
										UPDATE #MACH5_ORDR_CMPNT
											SET	FLAG = 1
											WHERE CMPNT_TYPE_CD IN ('ACCS','PORT')
										
									END
									
									ELSE IF EXISTS
										(
											SELECT 'X' FROM
												#MACH5_ORDR m WITH (NOLOCK)
												LEFT JOIN #MACH5_ORDR_CMPNT mo WITH (NOLOCK) ON m.M5_ORDR_ID = mo.M5_ORDR_ID
												WHERE CMPNT_TYPE_CD IN ('ACST')
													AND m.DOM_PROCESS_FLG_CD = 'N'
													AND FLAG = 0
										)
										BEGIN
										
											INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('ACST Loop:'+@TransName,'insertMach5OrderDetails', GETDATE())
											SET @ERRLog = @ERRLog + 'ACST Loop:'
											OPEN SYMMETRIC KEY FS@K3y 
											DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
											
											truncate table #MACH5_ORDR_CUST
											
											SET @SQL = ''
											SET @SQL = 	
												'SELECT * ' + 
													' FROM OPENQUERY (M5, ''Select DISTINCT CIS_HIER_LVL_CD,CIS_CUST_ID, BR_CD,SOI_CD,CUST_NME
																					,LINE_1,LINE_2,LINE_3,CTRY_CD,CTY_NME,ST_PRVN_CD
																					,PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1,TAX_XMPT_CD
																					,CHARS_CUST_ID,SITE_ID,PRVN_NME,CSG_LVL
																			From Mach5.V_V5U_TERM_CUST_ACCT  
																			Where CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'+
																					
													
												   ' UNION ' + 
												   ' SELECT * FROM OPENQUERY (M5,''Select DISTINCT h4.CIS_HIER_LVL_CD,h4.CIS_CUST_ID, h4.BR_CD,h4.SOI_CD,h4.CUST_NME
																							,h4.LINE_1,h4.LINE_2,h4.LINE_3,h4.CTRY_CD,h4.CTY_NME,h4.ST_PRVN_CD
																							,h4.PSTL_CD,h4.BLDG_ID,h4.FLOOR_ID,h4.ROOM_ID,h4.BILL_CYC_ID,h4.H1,h4.TAX_XMPT_CD
																							,h4.CHARS_CUST_ID,h4.SITE_ID,h4.PRVN_NME,h4.CSG_LVL
																					From Mach5.V_V5U_TERM_CUST_ACCT h4 
																					INNER JOIN Mach5.V_V5U_TERM_CUST_ACCT h6 ON h6.H4 = h4.CIS_CUST_ID AND ROWNUM = 1
																					Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'+
												   
												   '	UNION	' + 
												   '	Select * FROM OPENQUERY 
															(M5, ''SELECT DISTINCT h1.CIS_HIER_LVL_CD,h1.CIS_CUST_ID, h1.BR_CD,h1.SOI_CD,h1.CUST_NME
																							,h1.LINE_1,h1.LINE_2,h1.LINE_3,h1.CTRY_CD,h1.CTY_NME,h1.ST_PRVN_CD
																							,h1.PSTL_CD,h1.BLDG_ID,h1.FLOOR_ID,h1.ROOM_ID,h1.BILL_CYC_ID,h1.H1,h1.TAX_XMPT_CD
																							,h1.CHARS_CUST_ID,h1.SITE_ID,h1.PRVN_NME,h1.CSG_LVL
																						From Mach5.V_V5U_TERM_CUST_ACCT h1 
																						INNER JOIN Mach5.V_V5U_TERM_CUST_ACCT h6 ON h6.H1 = h1.CIS_CUST_ID
																					Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'
											
											
											INSERT INTO #MACH5_ORDR_CUST (CIS_LVL_TYPE ,CUST_ID ,BR_CD ,SOI_CD ,
																			CUST_NME ,STREET_ADR_1 ,STREET_ADR_2 ,
																			STREET_ADR_3,CTRY_CD,CTY_NME,STT_CD,
																			ZIP_PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1_ID
																			,TAX_XMPT_CD,CHARS_CUST_ID,SITE_ID,PRVN_NME,CSG_LVL)
											EXEC sp_executesql 	@SQL
															
											Select * From #MACH5_ORDR_CUST		


											TRUNCATE TABLE #MACH5_ORDR_CNTCT

											SET @SQL = ''
											SET @SQL = '  SELECT * ' + 
														' FROM OPENQUERY (M5,
														''Select DISTINCT ''''H6'''' AS H6,
															c.ROLE_CD AS ROLE_CD,
															CASE WHEN c.DOM_INTL_IND_CD = ''''D'''' THEN c.DOM_PHN_NBR
																  WHEN c.DOM_INTL_IND_CD = ''''I'''' THEN c.INTL_PHN_NBR
																  ELSE ''''''''
															END AS PHN_NBR, 
															c.TME_ZN_CD AS TME_ZONE_ID,
															'''''''' AS FRST_NME,
															'''''''' AS LST_NME ,			
															c.EMAIL_ADR AS EMAIL_ADR, 
															c.AVLBLTY_HR_STRT || CASE WHEN LENGTH(c.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||c.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
															CASE WHEN c.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(c.DOM_PHN_NBR,1,3) 
																  WHEN c.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(c.INTL_PHN_NBR,1,3) 
																  ELSE ''''''''
															END AS NPA ,			
														   CASE WHEN c.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(c.DOM_PHN_NBR,4,3) 
															  WHEN c.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(c.INTL_PHN_NBR,4,3) 
															  ELSE ''''''''       
															END AS  NXX ,			
														   CASE WHEN c.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(c.DOM_PHN_NBR,7,4) 
															  WHEN c.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(c.INTL_PHN_NBR,7,4) 
															  ELSE ''''''''      
															END AS STN_NBR ,			
															'''''''' AS CTY_CD ,
															c.INTL_CNTRY_DIAL_CD AS ISD_CD,			
															c.CUST_NME AS CNTCT_NME ,			
															c.PHN_EXT AS PHN_EXT_NBR,
															''''CIS'''' AS FSA_MDUL_ID,
															a.CSG_LVL
															From Mach5.v_V5U_TERM_CUST_CONTACT c
															INNER JOIN Mach5.V_V5U_TERM_CUST_ACCT a ON c.CUST_ACCT_ID = a.CUST_ACCT_ID
														  WHERE c.CUST_ACCT_ID ='   + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'+


														'	UNION	' + 

														' SELECT * ' + 
														' FROM OPENQUERY (M5,
															''Select DISTINCT ''''H4'''' AS H6,
																hc4.ROLE_CD AS ROLE_CD, 
																CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN hc4.DOM_PHN_NBR
																  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN hc4.INTL_PHN_NBR
																  ELSE ''''''''
																END AS PHN_NBR, 
																hc4.TME_ZN_CD AS TME_ZONE_ID,
																'''''''' AS FRST_NME,
																'''''''' AS LST_NME ,			
																hc4.EMAIL_ADR AS EMAIL_ADR,
																hc4.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc4.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc4.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
																CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,1,3) 
																  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,1,3) 
																  ELSE ''''''''
																  END AS NPA ,			
																CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,4,3) 
																  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,4,3) 
																  ELSE ''''''''       
																  END AS  NXX ,			
															   CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,7,4) 
																  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,7,4) 
																  ELSE ''''''''      
																  END AS STN_NBR ,			
																'''''''' AS CTY_CD ,
																hc4.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
																hc4.CUST_NME AS CNTCT_NME ,			
																hc4.PHN_EXT AS PHN_EXT_NBR,
																''''CIS'''' AS FSA_MDUL_ID,
																h6.CSG_LVL
														  From Mach5.V_V5U_TERM_CUST_ACCT h6 
															INNER JOIN Mach5.V_V5U_TERM_CUST_ACCT h4 ON h6.H4 = h4.CIS_CUST_ID
															INNER JOIN  Mach5.v_V5U_TERM_CUST_CONTACT hc4 ON h4.CUST_ACCT_ID = hc4.CUST_ACCT_ID
															Where h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'+


														'	UNION	' + 

														' SELECT * ' + 
														' FROM OPENQUERY (M5,
															''Select DISTINCT ''''H1'''' AS H6,
															hc1.ROLE_CD AS ROLE_CD, 
															CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN hc1.DOM_PHN_NBR
																  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN hc1.INTL_PHN_NBR
																  ELSE ''''''''
																END AS PHN_NBR,  
															hc1.TME_ZN_CD AS TME_ZONE_ID,
															'''''''' AS FRST_NME,
															'''''''' AS LST_NME ,			
															hc1.EMAIL_ADR AS EMAIL_ADR,
															hc1.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc1.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc1.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
															CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,1,3) 
																WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,1,3) 
															  ELSE ''''''''
															  END AS NPA ,			
														   CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,4,3) 
															  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,4,3) 
															  ELSE ''''''''       
															  END AS  NXX ,			
														   CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,7,4) 
															  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,7,4) 
															  ELSE ''''''''      
															  END AS STN_NBR ,			
															'''''''' AS CTY_CD ,
															hc1.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
															hc1.CUST_NME AS CNTCT_NME ,			
															hc1.PHN_EXT AS PHN_EXT_NBR, 
															''''CIS'''' AS FSA_MDUL_ID,
															h6.CSG_LVL
													  From Mach5.V_V5U_TERM_CUST_ACCT h6 
														INNER JOIN Mach5.V_V5U_TERM_CUST_ACCT h1 ON h6.H1 = h1.CIS_CUST_ID
														INNER JOIN  Mach5.v_V5U_TERM_CUST_CONTACT hc1 ON h1.CUST_ACCT_ID = hc1.CUST_ACCT_ID
														Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'

											INSERT INTO #MACH5_ORDR_CNTCT 
											(
												[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
												[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
												[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR], [FSA_MDUL_ID], [CSG_LVL]
											)
											EXEC sp_executesql @SQL
										
											SET @SQL = ''
											SET @SQL =   
							  
										
								  
											' SELECT * ' + 
												' FROM OPENQUERY (M5,
													''Select DISTINCT ''''H1'''' AS H1,
													atcH1.ROLE_CD AS ROLE_CD, 
													CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN atcH1.DOM_PHN_NBR
															WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN atcH1.INTL_PHN_NBR
															ELSE ''''''''
													END AS  PHN_NBR, 
													'''''''' AS TME_ZONE_ID,
													atcH1.FIRST_NME AS FRST_NME,
													atcH1.LAST_NME AS LST_NME ,			
													atcH1.EMAIL_ADR AS EMAIL_ADR, 
													'''''''' AS CNTCT_HR_TXT , 	
													CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,1,3) 
															WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,1,3) 
															ELSE ''''''''
													END AS NPA ,			
													CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,4,3) 
															WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,4,3) 
															ELSE ''''''''       
													END AS  NXX ,			
													CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,7,4) 
															WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,7,4) 
															ELSE ''''''''      
													END AS STN_NBR ,			
													'''''''' AS CTY_CD ,
													atcH1.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
													atcH1.FIRST_NME || '''' ''''|| atcH1.LAST_NME AS CNTCT_NME ,			
													'''''''' AS PHN_EXT_NBR,
													''''ATM'''' AS FSA_MDUL_ID,
													h6.CSG_LVL
											From	Mach5.V_V5U_TERM_CUST_ACCT h6 
												INNER JOIN Mach5.V_V5U_TERM_CUST_ACCT h1 ON h6.H1 = h1.CIS_CUST_ID
												INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH1 ON atcH1.CUST_ACCT_ID = h1.CUST_ACCT_ID
											WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')' +
											
											'	UNION	' +  
							  
										
								  
											' SELECT * ' + 
												' FROM OPENQUERY (M5,
													''Select DISTINCT ''''H4'''' AS H4,
													atcH4.ROLE_CD AS ROLE_CD, 
													CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN atcH4.DOM_PHN_NBR
															WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN atcH4.INTL_PHN_NBR
															ELSE ''''''''
													END AS  PHN_NBR, 
													'''''''' AS TME_ZONE_ID,
													atcH4.FIRST_NME AS FRST_NME,
													atcH4.LAST_NME AS LST_NME ,			
													atcH4.EMAIL_ADR AS EMAIL_ADR, 
													'''''''' AS CNTCT_HR_TXT , 	
													CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,1,3) 
															WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,1,3) 
															ELSE ''''''''
													END AS NPA ,			
													CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,4,3) 
															WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,4,3) 
															ELSE ''''''''       
													END AS  NXX ,			
													CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,7,4) 
															WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,7,4) 
															ELSE ''''''''      
													END AS STN_NBR ,			
													'''''''' AS CTY_CD ,
													atcH4.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
													atcH4.FIRST_NME || '''' ''''|| atcH4.LAST_NME AS CNTCT_NME ,			
													'''''''' AS PHN_EXT_NBR,
													''''ATM'''' AS FSA_MDUL_ID,
													h6.CSG_LVL
											From Mach5.V_V5U_TERM_CUST_ACCT h6 
													INNER JOIN Mach5.V_V5U_TERM_CUST_ACCT h4 ON h6.H4 = h4.CIS_CUST_ID
													INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH4 ON atcH4.CUST_ACCT_ID = h4.CUST_ACCT_ID
											WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')' +
											
											'	UNION	' +  
							  
										
								  
											' SELECT * ' + 
												' FROM OPENQUERY (M5,
													''Select DISTINCT ''''H6'''' AS H6,
													c.ROLE_CD AS ROLE_CD, 
													CASE	WHEN c.DOM_INTL_IND_CD = ''''D'''' THEN c.DOM_PHN_NBR
															WHEN c.DOM_INTL_IND_CD = ''''I'''' THEN c.INTL_PHN_NBR
															ELSE ''''''''
													END AS  PHN_NBR, 
													'''''''' AS TME_ZONE_ID,
													c.FIRST_NME AS FRST_NME,
													c.LAST_NME AS LST_NME ,			
													c.EMAIL_ADR AS EMAIL_ADR, 
													'''''''' AS CNTCT_HR_TXT , 	
													CASE	WHEN c.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(c.DOM_PHN_NBR,1,3) 
															WHEN c.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(c.INTL_PHN_NBR,1,3) 
															ELSE ''''''''
													END AS NPA ,			
													CASE	WHEN c.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(c.DOM_PHN_NBR,4,3) 
															WHEN c.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(c.INTL_PHN_NBR,4,3) 
															ELSE ''''''''       
													END AS  NXX ,			
													CASE	WHEN c.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(c.DOM_PHN_NBR,7,4) 
															WHEN c.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(c.INTL_PHN_NBR,7,4) 
															ELSE ''''''''      
													END AS STN_NBR ,			
													'''''''' AS CTY_CD ,
													c.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
													c.FIRST_NME || '''' ''''|| c.LAST_NME AS CNTCT_NME ,			
													'''''''' AS PHN_EXT_NBR,
													''''ATM'''' AS FSA_MDUL_ID,
													a.CSG_LVL
											From Mach5.v_V5U_ACCT_TEAM_CONTACT c
											INNER JOIN Mach5.V_V5U_TERM_CUST_ACCT a ON c.CUST_ACCT_ID = a.CUST_ACCT_ID
											WHERE c.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')' 
													

											INSERT INTO #MACH5_ORDR_CNTCT 
												(
													[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
													[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
													[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID],[CSG_LVL]
												)
											EXEC sp_executesql @SQL
											
											
											SET @SQL = ''
											SET @SQL = 
												' SELECT * ' + 
													' FROM OPENQUERY (M5,
														''Select DISTINCT ''''H6'''' AS H6,
															''''Hrchy'''' AS ROLE_CD, 
															CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN DOM_PHN_NBR 
																  WHEN DOM_INTL_IND_CD = ''''I'''' THEN CONCAT(INTL_CNTRY_DIAL_CD , INTL_PHN_NBR )  
																  ELSE ''''''''
															END AS PHN_NBR, 
															'''''''' AS TME_ZONE_ID,
															'''''''' AS FRST_NME,
															'''''''' AS LST_NME ,			
															'''''''' AS EMAIL_ADR, 
															'''''''' AS CNTCT_HR_TXT,  	
															CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,1,3) 
																  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,1,3) 
																  ELSE ''''''''
															END AS NPA ,			
														   CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,4,3) 
															  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,4,3)
															  ELSE ''''''''       
															END AS  NXX ,			
														   CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,7,4) 
															  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,7,4) 
															  ELSE ''''''''      
															END AS STN_NBR ,			
															'''''''' AS CTY_CD ,
															INTL_CNTRY_DIAL_CD AS ISD_CD ,			
															CUST_NME AS CNTCT_NME ,			
															PHN_EXT AS PHN_EXT_NBR,
															''''CIS'''' AS FSA_MDUL_ID,
															CSG_LVL
														From Mach5.V_V5U_TERM_CUST_ACCT 
													  WHERE CUST_ACCT_ID ='   + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'
											
											
											INSERT INTO #MACH5_ORDR_CNTCT 
												(
													[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
													[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
													[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID],[CSG_LVL]
												)
											EXEC sp_executesql @SQL
											
											SELECT * FROM #MACH5_ORDR_CNTCT

											
											--default region ID to AMNCI and later modified after order insertion in this SP
											INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID,DMSTC_CD, PLTFRM_CD,PROD_ID)	
													SELECT @OrderCategoryID, 1, 1
													,CASE t.DOM_PROCESS_FLG_CD
																		WHEN 'Y' THEN '0'
																		WHEN 'N' THEN '1'
																		ELSE '0'
																	END
													,p.PLTFRM_CD,c.PROD_CD
													FROM #MACH5_ORDR t WITH (NOLOCK)
												LEFT JOIN dbo.PLTFRM_MAPNG p WITH (NOLOCK) ON t.PROD_TYPE_CD = p.FSA_PROD_TYPE_CD
												LEFT JOIN (SELECT TOP 1 M5_ORDR_ID, PROD_CD FROM #MACH5_ORDR_CMPNT WITH (NOLOCK) WHERE ISNULL(PROD_CD,'') <> '')c ON c.M5_ORDR_ID = t.M5_ORDR_ID
												--WHERE t.PROD_TYPE_CD IN ('DN','DO','MP','MO','SN','SO')
											
											SELECT @ORDR_ID = SCOPE_IDENTITY()
											SET @FSA_ORDR_ID = @ORDR_ID
							
											
											IF @QDA_ORDR_ID != ''
												SET @QDA_ORDR_ID = @QDA_ORDR_ID + ',' + CONVERT(varchar,@FSA_ORDR_ID)
											ELSE
												SET @QDA_ORDR_ID = CONVERT(varchar,@FSA_ORDR_ID)
											
											UPDATE		dbo.ORDR	WITH (ROWLOCK)
												SET		PRNT_ORDR_ID	=	@ORDR_ID,
														H5_H6_CUST_ID	=	@H5_H6_CUST_ID,
														CSG_LVL_ID		=	(SELECT TOP 1 ISNULL(lcl.CSG_LVL_ID,0) FROM #MACH5_ORDR mo WITH (NOLOCK) LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=mo.CSG_LVL WHERE mo.M5_ORDR_ID = @M5_ORDR_ID),
														CHARS_ID		=	(SELECT TOP 1 CHARS_CUST_ID FROM #MACH5_ORDR WITH (NOLOCK) WHERE M5_ORDR_ID = @M5_ORDR_ID)
												WHERE	ORDR_ID			=	@ORDR_ID
											
	
											-------FSA_ORDR
											INSERT INTO FSA_ORDR (ORDR_ID,ORDR_ACTN_ID,FTN,ORDR_TYPE_CD,ORDR_SUB_TYPE_CD,PROD_TYPE_CD,CUST_SIGNED_DT,CUST_WANT_DT,
																	CUST_ORDR_SBMT_DT, CUST_CMMT_DT,CUST_PRMS_OCPY_CD,CUST_ACPT_ERLY_SRVC_CD,MULT_CUST_ORDR_CD,
																	DISC_REAS_CD, TTRPT_OSS_CD, VNDR_VPN_CD,INSTL_VNDR_CD,INSTL_ESCL_CD,
																	FSA_EXP_TYPE_CD,PRE_QUAL_NBR,PRNT_FTN,RELTD_FTN, TTRPT_NW_ADR
																	)
											(
												SELECT	TOP 1 @FSA_ORDR_ID,@ORDR_ACTN_ID,m.ORDER_NBR,m.ORDR_TYPE_CD,m.ORDR_SUBTYPE_CD,m.PROD_TYPE_CD,m.CUST_SIGN_DT,
														m.CUST_WANT_DT,m.CUST_SBMT_DT,m.CUST_CMMT_DT,m.CUST_PREM_OCCU_FLG_CD,m.CUST_ACPT_SRVC_FLG_CD,m.MULT_ORDR_SBMTD_FLG_CD,
														m.DSCNCT_REAS_CD,a.ONE_STOP_SHOP_FLG_CD,
														CASE 
															WHEN m.PROD_TYPE_CD IN ('MO','IO','IN') THEN a.ACCS_CXR_CD
															ELSE a.ACCS_CXR_CD
														END	,
														CASE 
															WHEN m.PROD_TYPE_CD IN ('MO','SO','DO','IO','IN') THEN a.ACCS_CXR_CD
															ELSE a.ACCS_CXR_CD
														END	,
														m.EXP_FLG_CD,	CASE m.EXP_TYPE_CD 
																											WHEN 'B' THEN 'IE'
																											ELSE m.EXP_TYPE_CD
																											END
														,a.PRE_QUAL_NBR
														,CASE m.ORDR_TYPE_CD
															WHEN 'CN' THEN m.PRNT_FTN
															ELSE NULL
														  END
														,CASE m.ORDR_TYPE_CD
															WHEN 'CN' THEN NULL
															ELSE m.RLTD_FTN
														  END
														,ISNULL(a.NUA,p.NUA) AS NUA
													FROM #MACH5_ORDR m WITH (NOLOCK)
													LEFT JOIN #MACH5_ORDR_CMPNT a WITH (NOLOCK) ON m.M5_ORDR_ID = a.M5_ORDR_ID AND a.CMPNT_TYPE_CD = 'ACST'
													LEFT JOIN #MACH5_ORDR_CMPNT p WITH (NOLOCK) ON m.M5_ORDR_ID = p.M5_ORDR_ID AND p.CMPNT_TYPE_CD = 'PORT'
													WHERE m.M5_ORDR_ID = @M5_ORDR_ID
													
											)
											
											--FSA_ORDR_CPE_LINE_ITEM
											INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM ([ORDR_ID], [LINE_ITEM_CD],
											[TTRPT_ACCS_TYPE_CD],
											[TTRPT_ACCS_ARNGT_CD],
											[CXR_ACCS_CD],
											[CKT_ID],
											[TTRPT_ACCS_TERM_DES],
											[TTRPT_ACCS_BDWD_TYPE],
											[PL_NBR],
											CMPNT_ID,
											ORDR_CMPNT_ID,
											TTRPT_COS_CD,
											MDS_DES)
											SELECT DISTINCT @FSA_ORDR_ID, 'ACS',
											a.ACCS_TYPE_CD, a.ACCS_ARNGT_CD,
											CASE 
															WHEN m.PROD_TYPE_CD IN ('MP','DN','SN','IO','IN') THEN a.ACCS_CXR_CD
															ELSE a.ACCS_CXR_CD
											END,
											a.CKT,
											a.ACCS_TERM_CD,
											a.ACCS_BDWD_CD,
											a.PL_NBR,
											a.NVTRY_ID,
											a.NVTRY_ID,
											a.COS_CD,
											a.NME
											FROM #MACH5_ORDR m WITH (NOLOCK)
											LEFT JOIN #MACH5_ORDR_CMPNT a WITH (NOLOCK) ON m.M5_ORDR_ID = a.M5_ORDR_ID AND a.CMPNT_TYPE_CD = 'ACST'
											
											INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM ([ORDR_ID], [LINE_ITEM_CD], [INSTL_DSGN_DOC_NBR],
											[TTRPT_SPD_OF_SRVC_BDWD_DES],
											[PORT_RT_TYPE_CD],
											CMPNT_ID,
											ORDR_CMPNT_ID,
											MDS_DES)
											SELECT DISTINCT @FSA_ORDR_ID, 'PRT', p.DESIGN_DOC_NUM,
											p.PORT_SPD_DES,
											p.RATE_TYPE_CD,
											p.NVTRY_ID,
											p.NVTRY_ID,
											p.NME
											FROM #MACH5_ORDR_CMPNT p WITH (NOLOCK) WHERE p.CMPNT_TYPE_CD = 'PORT'
											
											--ORDR_EXP	
											IF EXISTS
											(
												SELECT 'X'
													FROM #MACH5_ORDR
													WHERE EXP_FLG_CD = 'Y'
											)
											BEGIN
												INSERT INTO ORDR_EXP (ORDR_ID,
																		EXP_TYPE_ID,
																		AUTHRZR_PHN_NBR,
																		AUTHRZR_TITLE_ID,
																		AUTHN_DT,
																		BILL_TO_COST_CTR_CD,
																		CREAT_DT,
																		CREAT_BY_USER_ID,
																		REC_STUS_ID
																			)
												(
													SELECT TOP 1	@FSA_ORDR_ID
																	,CASE EXP_TYPE_CD
																			WHEN 'I' THEN 1
																			WHEN 'E' THEN 2
																			WHEN 'B' THEN 3
																			ELSE 4 
																		END AS EXP_TYPE_ID
																	,APRVR_PHN
																	,APRVR_TITLE
																	,APPRVL_DT
																	,[dbo].[RemoveSpecialCharacters](LEFT(SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10),
           PATINDEX('%[^0-9]%', SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10) + 'X') -1))
																	,GETDATE()
																	,1
																	,1
														FROM #MACH5_ORDR WITH (NOLOCK)
														
												)
											END
											
											delete from @tmpids
											-----FSA_ORDR_CUST
											INSERT INTO dbo.FSA_ORDR_CUST 
													(	ORDR_ID,
														CIS_LVL_TYPE,
														CUST_ID,
														BR_CD,
														SOI_CD,
														CREAT_DT,
														CUST_NME,
														CURR_BILL_CYC_CD,
														TAX_XMPT_CD,
														CHARS_CUST_ID,
														SITE_ID)								
												(SELECT	DISTINCT @FSA_ORDR_ID,
														CIS_LVL_TYPE,
														CUST_ID,
														BR_CD,
														SOI_CD,
														GETDATE(),
														CASE
														WHEN (SELECT PROD_TYPE_CD FROM #MACH5_ORDR) IN ('IO','IN') THEN 'T-' + CUST_NME
														ELSE CUST_NME END,
														ISNULL(BILL_CYC_ID,0),
														TAX_XMPT_CD,
														CHARS_CUST_ID,
														SITE_ID
													FROM #MACH5_ORDR_CUST WITH (NOLOCK)
													WHERE ISNULL(CSG_LVL,'')=''
												)

											INSERT INTO dbo.FSA_ORDR_CUST 
													(	ORDR_ID,
														CIS_LVL_TYPE,
														CUST_ID,
														BR_CD,
														SOI_CD,
														CREAT_DT,
														CURR_BILL_CYC_CD,
														TAX_XMPT_CD,
														CHARS_CUST_ID,
														SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
												SELECT	DISTINCT @FSA_ORDR_ID,
														CIS_LVL_TYPE,
														CUST_ID,
														BR_CD,
														SOI_CD,
														GETDATE(),
														ISNULL(BILL_CYC_ID,0),
														TAX_XMPT_CD,
														CHARS_CUST_ID,
														SITE_ID
													FROM #MACH5_ORDR_CUST WITH (NOLOCK)
													WHERE ISNULL(CSG_LVL,'')!=''
												
												
											INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
											SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, CASE
														WHEN ((SELECT PROD_TYPE_CD FROM #MACH5_ORDR) IN ('IO','IN')) THEN dbo.encryptString('T-' + moc.CUST_NME)
														ELSE dbo.encryptString(moc.CUST_NME) END
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												  AND foc.CUST_NME IS NULL
											
											delete from @tmpids
											IF NOT EXISTS
												(
													SELECT 'X'
														FROM dbo.FSA_ORDR_CUST WITH (NOLOCK)
														WHERE ORDR_ID = @ORDR_ID
															AND CIS_LVL_TYPE = 'H1'
												)
												BEGIN
													INSERT INTO dbo.FSA_ORDR_CUST 
													(	ORDR_ID,
														CIS_LVL_TYPE,
														CUST_ID,
														BR_CD,
														SOI_CD,
														CREAT_DT,
														CUST_NME,
														CURR_BILL_CYC_CD,
														TAX_XMPT_CD,
														CHARS_CUST_ID,
														SITE_ID)									
												(SELECT	DISTINCT @FSA_ORDR_ID,
														'H1',
														H1_ID,
														'',
														'',
														GETDATE(),
														'',
														BILL_CYC_ID,
														TAX_XMPT_CD,
														CHARS_CUST_ID,
														SITE_ID
													FROM #MACH5_ORDR_CUST WITH (NOLOCK)
													WHERE CIS_LVL_TYPE = 'H6'
													  AND ISNULL(CSG_LVL,'')=''
												)

												INSERT INTO dbo.FSA_ORDR_CUST 
													(	ORDR_ID,
														CIS_LVL_TYPE,
														CUST_ID,
														BR_CD,
														SOI_CD,
														CREAT_DT,
														CURR_BILL_CYC_CD,
														TAX_XMPT_CD,
														CHARS_CUST_ID,
														SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
												SELECT	DISTINCT @FSA_ORDR_ID,
														'H1',
														H1_ID,
														'',
														'',
														GETDATE(),
														BILL_CYC_ID,
														TAX_XMPT_CD,
														CHARS_CUST_ID,
														SITE_ID
													FROM #MACH5_ORDR_CUST WITH (NOLOCK)
													WHERE CIS_LVL_TYPE = 'H6'
													  AND ISNULL(CSG_LVL,'')!=''
												
													INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
													SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString('')
													FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
													INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
													INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
													WHERE ISNULL(moc.CSG_LVL,'')!=''
													  AND moc.CIS_LVL_TYPE = 'H6'
													  AND foc.CUST_NME IS NULL
												END
											
											delete from @tmpids
											----ORDR_ADR
											INSERT INTO dbo.ORDR_ADR
												(
													ORDR_ID,
													ADR_TYPE_ID,
													CREAT_DT,
													CREAT_BY_USER_ID,
													REC_STUS_ID,
													CTRY_CD,
													CIS_LVL_TYPE,
													FSA_MDUL_ID,
													STREET_ADR_1,
													STREET_ADR_2,
													CTY_NME,
													PRVN_NME,
													STT_CD,
													ZIP_PSTL_CD,
													BLDG_NME,
													FLR_ID,
													RM_NBR,
													HIER_LVL_CD,
													STREET_ADR_3
												)
											(
												SELECT	DISTINCT @FSA_ORDR_ID,
														CASE CIS_LVL_TYPE
															WHEN 'H4' THEN 3
															WHEN 'H6' THEN 18
															WHEN 'H5' THEN 18
															ELSE 17
														END,
														GETDATE(),
														1,
														1,
														CTRY_CD,
														CIS_LVL_TYPE,
														'CIS',
														STREET_ADR_1,
														STREET_ADR_2,
														CTY_NME,
														PRVN_NME,
														STT_CD,
														ZIP_PSTL_CD,
														BLDG_ID,
														FLOOR_ID,
														ROOM_ID,
														CASE CIS_LVL_TYPE	
															WHEN 'H4' THEN 0
															WHEN 'H1' THEN 0
															ELSE 1
														END,
														STREET_ADR_3
													FROM #MACH5_ORDR_CUST
													WHERE ISNULL(CSG_LVL,'')=''
											)
											
											INSERT INTO dbo.ORDR_ADR
												(
													ORDR_ID,
													ADR_TYPE_ID,
													CREAT_DT,
													CREAT_BY_USER_ID,
													REC_STUS_ID,
													CTRY_CD,
													CIS_LVL_TYPE,
													FSA_MDUL_ID,
													HIER_LVL_CD
												) output inserted.ORDR_ADR_ID into @tmpids
												SELECT	DISTINCT @FSA_ORDR_ID,
														CASE CIS_LVL_TYPE
															WHEN 'H4' THEN 3
															WHEN 'H6' THEN 18
															WHEN 'H5' THEN 18
															ELSE 17
														END,
														GETDATE(),
														1,
														1,
														CTRY_CD,
														CIS_LVL_TYPE,
														'CIS',
														CASE CIS_LVL_TYPE	
															WHEN 'H4' THEN 0
															WHEN 'H1' THEN 0
															ELSE 1
														END
													FROM #MACH5_ORDR_CUST
													WHERE ISNULL(CSG_LVL,'')!=''
											
											INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
												STREET_ADR_2,
												CTY_NME,
												STT_PRVN_NME,
												STT_CD,
												ZIP_PSTL_CD,
												BLDG_NME,
												FLR_ID,
												RM_NBR,
												STREET_ADR_3)
											SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.STREET_ADR_1),
													dbo.encryptString(moc.STREET_ADR_2),
													dbo.encryptString(moc.CTY_NME),
													dbo.encryptString(moc.PRVN_NME),
													dbo.encryptString(moc.STT_CD),
													dbo.encryptString(moc.ZIP_PSTL_CD),
													dbo.encryptString(moc.BLDG_ID),
													dbo.encryptString(moc.FLOOR_ID),
													dbo.encryptString(moc.ROOM_ID),
													dbo.encryptString(moc.STREET_ADR_3)
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND oa.ORDR_ID = @FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												  AND oa.STREET_ADR_1 IS NULL
											
											delete from @tmpids
											------ORDR_CNTCT
											INSERT INTO dbo.ORDR_CNTCT
												(
													ORDR_ID,
													CNTCT_TYPE_ID,
													PHN_NBR,
													TME_ZONE_ID,
													CREAT_DT,
													CREAT_BY_USER_ID,
													REC_STUS_ID,
													FAX_NBR,
													ROLE_ID,
													CIS_LVL_TYPE,
													FSA_MDUL_ID,
													FRST_NME,
													LST_NME,
													EMAIL_ADR,
													CNTCT_HR_TXT,
													NPA,
													NXX,
													STN_NBR,
													CTY_CD,
													ISD_CD,
													CNTCT_NME,
													PHN_EXT_NBR
												)
											(
												SELECT	DISTINCT @FSA_ORDR_ID ,
														CASE c.ROLE_CD
																WHEN 'Hrchy' THEN 17
																ELSE 1
															END AS CNTCT_TYPE_ID,
														c.PHN_NBR,
														l.TME_ZONE_ID,
														GETDATE(),
														1,
														1,
														'',
														lr.ROLE_ID,
														c.CIS_LVL_TYPE,
														CASE c.CIS_LVL_TYPE
															WHEN  'H1' THEN 'CIS'
															WHEN   'H4' THEN 'CIS'
															WHEN   'H5' THEN 'CIS'
															WHEN   'H6' THEN 'CIS'
															ELSE 'ATM'
														END,
														c.FRST_NME,
														c.LST_NME,
														c.EMAIL_ADR,
														c.CNTCT_HR_TXT,
														c.NPA,
														c.NXX,
														c.STN_NBR,
														c.CTY_CD,
														c.ISD_CD,
														c.CNTCT_NME,
														c.PHN_EXT_NBR
													FROM #MACH5_ORDR_CNTCT c
												INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD	
												LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																	TME_ZONE_NME	AS TME_ZONE_NME 
																FROM dbo.LK_TME_ZONE 
																GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
												WHERE ISNULL(c.CSG_LVL,'')=''
											)
											
											INSERT INTO dbo.ORDR_CNTCT
												(
													ORDR_ID,
													CNTCT_TYPE_ID,
													PHN_NBR,
													TME_ZONE_ID,
													CREAT_DT,
													CREAT_BY_USER_ID,
													REC_STUS_ID,
													FAX_NBR,
													ROLE_ID,
													CIS_LVL_TYPE,
													FSA_MDUL_ID,
													CNTCT_HR_TXT,
													NPA,
													NXX,
													STN_NBR,
													CTY_CD,
													ISD_CD,
													PHN_EXT_NBR
												) output inserted.ORDR_CNTCT_ID into @tmpids
												SELECT	DISTINCT @FSA_ORDR_ID ,
														CASE c.ROLE_CD
																WHEN 'Hrchy' THEN 17
																ELSE 1
															END AS CNTCT_TYPE_ID,
														c.PHN_NBR,
														l.TME_ZONE_ID,
														GETDATE(),
														1,
														1,
														'',
														lr.ROLE_ID,
														c.CIS_LVL_TYPE,
														CASE c.CIS_LVL_TYPE
															WHEN  'H1' THEN 'CIS'
															WHEN   'H4' THEN 'CIS'
															WHEN   'H5' THEN 'CIS'
															WHEN   'H6' THEN 'CIS'
															ELSE 'ATM'
														END,
														c.CNTCT_HR_TXT,
														c.NPA,
														c.NXX,
														c.STN_NBR,
														c.CTY_CD,
														c.ISD_CD,
														c.PHN_EXT_NBR
													FROM #MACH5_ORDR_CNTCT c
												INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD	
												LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																	TME_ZONE_NME	AS TME_ZONE_NME 
																FROM dbo.LK_TME_ZONE 
																GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
												WHERE ISNULL(c.CSG_LVL,'')!=''
											
											INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, FRST_NME,
												LST_NME,
												CUST_EMAIL_ADR,
												CUST_CNTCT_NME)
											SELECT	DISTINCT oc.ORDR_CNTCT_ID, 15, dbo.encryptstring(c.FRST_NME),
													dbo.encryptstring(c.LST_NME),
													dbo.encryptstring(c.EMAIL_ADR),
													dbo.encryptstring(c.CNTCT_NME)
												FROM #MACH5_ORDR_CNTCT c WITH (NOLOCK)
												INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
											INNER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ROLE_ID=lr.ROLE_ID AND oc.CIS_LVL_TYPE=c.CIS_LVL_TYPE AND oc.ORDR_ID = @FSA_ORDR_ID
											INNER JOIN @tmpids tmpids ON tmpids.tmpid = oc.ORDR_CNTCT_ID
											WHERE ISNULL(c.CSG_LVL,'')!=''
											  AND oc.FRST_NME IS NULL
											
											-----ORDER NOTES
											IF EXISTS
												(
													SELECT 'X'
														FROM #MACH5_ORDR_NOTES WITH (NOLOCK)
												)
											BEGIN
												INSERT INTO ORDR_NTE
												(
													NTE_TYPE_ID,
													ORDR_ID,
													CREAT_DT,
													CREAT_BY_USER_ID,
													REC_STUS_ID,
													NTE_TXT
												)
												(
													SELECT DISTINCT
														CASE NTE_TYPE_CD
															WHEN 'CPEN' THEN 26
															WHEN 'CPES' THEN 27
															WHEN 'CPEP' THEN 28
															ELSE 25
														END, 
														@FSA_ORDR_ID,		
														GETDATE(),
														1,
														1,
														NTE_TXT		
													FROM #MACH5_ORDR_NOTES
													WHERE ISNULL(NTE_TXT,'') <> ''
												)
											END
											
											----SCA_NBR
											IF EXISTS
												(
													SELECT 'X'
														FROM #MACH5_ORDR_SCA WITH (NOLOCK)
												)
												BEGIN
													SELECT @SCA_NBR = COALESCE(@SCA_NBR + '| ' + SCA_NBR, SCA_NBR)
														FROM #MACH5_ORDR_SCA WITH (NOLOCK)
														
													UPDATE dbo.FSA_ORDR SET SCA_NBR = @SCA_NBR WHERE ORDR_ID = @ORDR_ID
												END
												
												--FSA_ORDR_CPE_LINE_ITEM For Disconnect Orders
										IF EXISTS
											(
												SELECT 'X'
													FROM #MACH5_DISC_ORDR_CMPNT WITH (NOLOCK)
													WHERE CMPNT_TYPE_CD = 'ACST' 
											)
											BEGIN
												INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
													(
														ORDR_ID,
														CREAT_DT,
														LINE_ITEM_CD,
														FSA_ORGNL_INSTL_ORDR_ID,
														CXR_CKT_ID,
														TTRPT_ACCS_TYPE_CD,
														TTRPT_SPD_OF_SRVC_BDWD_DES,
														TTRPT_ACCS_TYPE_DES,
														FMS_CKT_NBR,
														DISC_NW_ADR,
														DISC_PL_NBR
													)
												SELECT DISTINCT
													@FSA_ORDR_ID,
													GETDATE(),
													'DSC',
													ORIG_ORDR_NBR,
													CKT_ID,
													ACCS_TYPE_CD,
													PORT_SPD_DES,
													ACCS_BDWD_CD,
													CASE WHEN (CKT_ID LIKE '%[^0-9]%') THEN 0 ELSE CKT_ID END,
													NTWK_USR_ADR,
													PRIVATE_LINE_NBR
													FROM #MACH5_DISC_ORDR_CMPNT WHERE CMPNT_TYPE_CD = 'ACST'
													
													
											END	
												
										--Update Region ID for xNCI Orders
											Update od
											SET od.RGN_ID = 
															CASE 
																WHEN ISNULL(oa.STT_CD,CONVERT(VARCHAR, DecryptByKey(csdoa.STT_CD))) = 'GU' THEN 3
																ELSE ISNULL(lc.RGN_ID,od.RGN_ID)
															END 
											FROM dbo.ORDR od WITH (ROWLOCK)
										INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON od.ORDR_ID = oa.ORDR_ID
										INNER JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD
										LEFT JOIN dbo.CUST_SCRD_DATA csdoa WITH (NOLOCK) ON csdoa.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdoa.SCRD_OBJ_TYPE_ID=14 
											WHERE od.ORDR_ID = @ORDR_ID
												AND oa.CIS_LVL_TYPE = 'H6'
													
										IF @CHNG_ORDR_TYPE_FLG != 1
											BEGIN
												----State Machine
													INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('InsertInitialTask:'+@TransName,'insertMach5OrderDetails', GETDATE())
													SET @ERRLog = @ERRLog + 'InsertInitialTask:'
													Exec dbo.InsertInitialTask @ORDR_ID,@OrderCategoryID
												--SELECT @ORDR_ID	
											END
										
										
											UPDATE #MACH5_ORDR_CMPNT
												SET	FLAG = 1
												WHERE CMPNT_TYPE_CD IN ('ACST')		
											
										END
									 SET @Ctr = @Ctr + 1
						
								  END
							END
						IF EXISTS
						(
							SELECT 'X'
								FROM #MACH5_ORDR_CMPNT
								WHERE CMPNT_TYPE_CD IN ('CPE','OCPE','CPEP','CPEM','CPES')
						)
						BEGIN
							DECLARE @INTL_FLG BIT
							DECLARE @M5_PROD_ID Varchar(5)
							SELECT @INTL_FLG = CASE DOM_PROCESS_FLG_CD
													WHEN 'N' THEN 1
													ELSE 0
												END,
									@M5_PROD_ID = PROD_ID,
									@PRNT_FTN = PRNT_FTN
								FROM #MACH5_ORDR WITH (NOLOCK)
								
							--For testing, updating Device ID 
							IF	@INTL_FLG = 0 AND @M5_PROD_ID NOT IN ('UCCH','UCSV','MIPT','UCSM')
								BEGIN
									SELECT	@Cnt = COUNT(1) 
										FROM	#MACH5_ORDR_CMPNT 
										WHERE	CMPNT_TYPE_CD IN ('CPE','OCPE','CPEM','CPEP','CPES')
									
									DECLARE @CPE_DEVICE_ID VARCHAR(14), @STUS_CD Varchar(5), @Continue Varchar(1)
									IF @Cnt > 0
										BEGIN
											WHILE @Ctr < @Cnt 
												BEGIN
													SET @CPE_DEVICE_ID = ''
													SET @STUS_CD = ''
													SET @Continue = 'Y'
													SELECT TOP 1 
														@CPE_DEVICE_ID = ISNULL(CPE_DEVICE_ID,''),
														@STUS_CD = ISNULL(STUS_CD,'')
														FROM #MACH5_ORDR_CMPNT
														WHERE FLAG = 0
														 Order by STUS_CD desc -- Added 8/1 dlp0278 because not extracting when top 1 was cancelled.
														
													SELECT @CPE_DEVICE_ID AS CPE_DEVICE_ID
													
													--Filter Cancel Line Items which didn't have a parent compnent ID
													IF @STUS_CD = 'CN' AND ISNULL(@CPE_DEVICE_ID,'') != ''
														BEGIN 
															IF NOT EXISTS
															(
																SELECT 'X'
																	FROM	dbo.FSA_ORDR f WITH (NOLOCK)
																	Inner Join dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON f.ORDR_ID = fc.ORDR_ID
																	Where f.FTN IN (SELECT PRNT_FTN From #MACH5_ORDR)
																		AND fc.DEVICE_ID = @CPE_DEVICE_ID
																		AND fc.STUS_CD != 'CN'
															)
															BEGIN
																SET @Continue = 'N'
															END	
														END
													
													-- ** START ******  Code to handle FTN splits caused by RTS orders. 7/21/17 deployment 
													
																									
													IF @ORDR_TYPE_CD = 'IN' AND ISNULL(@CPE_DEVICE_ID,'') != ''
													   BEGIN 
															IF EXISTS
															(
																SELECT 'X'
																	FROM	#MACH5_ORDR_CMPNT moc 
																INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK)
																					ON moc.NVTRY_ID = fc.ORDR_CMPNT_ID
																					 AND moc.CPE_DEVICE_ID = fc.DEVICE_ID
																					  AND fc.ITM_STUS = 401
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
																-- modified 9/14 to not allowed completed components along with in-flight on the incoming order.
																-- added the FSA_ORDR table join to check the incoming FTN.
																	AND fc.ORDR_ID in (SELECT f.ORDR_ID FROM dbo.ACT_TASK a WITH (NOLOCK)  	
																							INNER JOIN FSA_ORDR f WITH (NOLOCK) ON a.ORDR_ID = f.ORDR_ID
																							INNER JOIN #MACH5_ORDR m5 ON m5.ORDER_NBR = f.FTN 
																							WHERE TASK_ID IN (600,601,602,604,1000,1001)
																										AND STUS_ID in (156,0))
																		   
															)															
																BEGIN
																	SET @Continue = 'N'
																END	
														END
													IF @ORDR_TYPE_CD = 'DC' AND ISNULL(@CPE_DEVICE_ID,'') != ''
													
														BEGIN 
															IF EXISTS
															(
																SELECT 'X'
																	FROM	#MACH5_ORDR_CMPNT moc 
																INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK)
																	ON moc.NVTRY_ID = fc.ORDR_CMPNT_ID AND moc.CPE_DEVICE_ID = fc.DEVICE_ID
																					  AND fc.ITM_STUS = 401
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
																	AND fc.ORDR_ID in (SELECT ORDR_ID FROM dbo.ACT_TASK 
																					WHERE TASK_ID IN (600,604,1000)	AND STUS_ID in (156,0))
															)															
																BEGIN
																	SET @Continue = 'N'
																END	
														END
											-- ** END ******  Code to handle FTN splits caused by RTS orders.  7/21/17 deployment
														
														
													SELECT @Continue
													IF @Continue = 'Y'
														BEGIN
															IF ISNULL(@CPE_DEVICE_ID,'') != '' 
																BEGIN
																	INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('Dmst-CPE:'+@TransName,'insertMach5OrderDetails', GETDATE())
																	SET @ERRLog = @ERRLog + 'Dmst-CPE:'
																	OPEN SYMMETRIC KEY FS@K3y 
																	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
																	--default region ID to AMNCI and later modified after order insertion in this SP
																	INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID,DMSTC_CD,PROD_ID)	
																		SELECT @OrderCategoryID, 1, 1, 0,PROD_ID
																			FROM #MACH5_ORDR WITH (NOLOCK)
																			WHERE M5_ORDR_ID = @M5_ORDR_ID
																									
																	SELECT @ORDR_ID = SCOPE_IDENTITY()
																	SET @FSA_ORDR_ID = @ORDR_ID
																	
																	IF @QDA_ORDR_ID != ''
																		SET @QDA_ORDR_ID = @QDA_ORDR_ID + ',' + CONVERT(varchar,@FSA_ORDR_ID)
																	ELSE
																		SET @QDA_ORDR_ID = CONVERT(varchar,@FSA_ORDR_ID)
																	
																	UPDATE #MACH5_ORDR_CMPNT
																		SET		QDA_ORDR_ID = @FSA_ORDR_ID
																		WHERE	CPE_DEVICE_ID = @CPE_DEVICE_ID
																	
																	UPDATE		odr
																		SET		PRNT_ORDR_ID	=	@ORDR_ID,
																				RAS_DT			=	t.CPE_REQD_ON_SITE_DT,
																				DLVY_CLLI		=	t.CPE_SHIP_CLLI_CD,
																				H5_H6_CUST_ID	=	@H5_H6_CUST_ID,
																				CSG_LVL_ID		=	(SELECT TOP 1 ISNULL(lcl.CSG_LVL_ID,0) FROM #MACH5_ORDR mo WITH (NOLOCK) LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=mo.CSG_LVL WHERE mo.M5_ORDR_ID = @M5_ORDR_ID),
																				CHARS_ID		=	(SELECT TOP 1 CHARS_CUST_ID FROM #MACH5_ORDR WITH (NOLOCK) WHERE M5_ORDR_ID = @M5_ORDR_ID)
																		FROM	dbo.ORDR	odr WITH (ROWLOCK) 
																		INNER JOIN #MACH5_ORDR_CMPNT t WITH (NOLOCK) ON odr.ORDR_ID = t.QDA_ORDR_ID
																		WHERE	ORDR_ID			=	@ORDR_ID
																			AND t.CPE_DEVICE_ID = @CPE_DEVICE_ID
																		
																																
																	--FSA_ORDR
																	INSERT INTO FSA_ORDR (ORDR_ID,ORDR_ACTN_ID,FTN,ORDR_TYPE_CD,ORDR_SUB_TYPE_CD,PROD_TYPE_CD,CUST_SIGNED_DT,CUST_WANT_DT,
																							CUST_ORDR_SBMT_DT, CUST_CMMT_DT,CUST_PRMS_OCPY_CD,CUST_ACPT_ERLY_SRVC_CD,MULT_CUST_ORDR_CD,
																							DISC_REAS_CD, INSTL_ESCL_CD,FSA_EXP_TYPE_CD,PRE_QUAL_NBR,PRNT_FTN,RELTD_FTN,
																							CPE_ACCS_PRVDR_CD,CPE_DLVRY_DUTY_AMT, CPE_DLVRY_DUTY_ID,CPE_ECCKT_ID,CPE_TST_TN_NBR,CPE_REC_ONLY_CD,
																							CPE_PHN_NBR, CPE_EQPT_ONLY_CD                         
														
																							)
																	(
																		SELECT	TOP 1 @FSA_ORDR_ID,@ORDR_ACTN_ID,m.ORDER_NBR,m.ORDR_TYPE_CD,m.ORDR_SUBTYPE_CD,'CP',m.CUST_SIGN_DT,
																				m.CUST_WANT_DT,m.CUST_SBMT_DT,m.CUST_CMMT_DT,m.CUST_PREM_OCCU_FLG_CD,m.CUST_ACPT_SRVC_FLG_CD,m.MULT_ORDR_SBMTD_FLG_CD,
																				m.DSCNCT_REAS_CD,m.EXP_FLG_CD,	CASE m.EXP_TYPE_CD 
																																	WHEN 'B' THEN 'IE'
																																	ELSE m.EXP_TYPE_CD
																																	END
																				,c.PRE_QUAL_NBR
																				,CASE m.ORDR_TYPE_CD
																					WHEN 'CN' THEN m.PRNT_FTN
																					ELSE NULL
																				  END
																				,CASE m.ORDR_TYPE_CD
																					WHEN 'CN' THEN NULL
																					ELSE m.RLTD_FTN
																				  END
																				,c.CPE_ACCS_PRVDR_NME
																				,c.CPE_DLVRY_DUTIES_AMT
																				,c.CPE_DLVRY_DUTIES_CD
																				,c.CPE_EQPT_CKT_ID
																				,c.CPE_SPRINT_TEST_TN_NBR
																				,c.CPE_RECORD_ONLY_FLG_CD
																				,c.CPE_ACCS_PRVDR_TN_NBR
																				,c.CPE_EQUIPMENT_ONLY_FLG_CD
																			FROM #MACH5_ORDR m WITH (NOLOCK)
																			LEFT JOIN #MACH5_ORDR_CMPNT c WITH (NOLOCK) ON m.M5_ORDR_ID = c.M5_ORDR_ID AND c.CMPNT_TYPE_CD = 'OCPE'
																			WHERE m.M5_ORDR_ID = @M5_ORDR_ID
																				
																			
																	)
																	
																	
																	--ORDR_EXP	
																	IF EXISTS
																	(
																		SELECT 'X'
																			FROM #MACH5_ORDR
																			WHERE EXP_FLG_CD = 'Y'
																	)
																	BEGIN
																		INSERT INTO ORDR_EXP (ORDR_ID,
																								EXP_TYPE_ID,
																								AUTHRZR_PHN_NBR,
																								AUTHRZR_TITLE_ID,
																								AUTHN_DT,
																								BILL_TO_COST_CTR_CD,
																								CREAT_DT,
																								CREAT_BY_USER_ID,
																								REC_STUS_ID
																									)
																		(
																			SELECT TOP 1	@FSA_ORDR_ID
																							,CASE EXP_TYPE_CD
																									WHEN 'I' THEN 1
																									WHEN 'E' THEN 2
																									WHEN 'B' THEN 3
																									ELSE 4 
																								END AS EXP_TYPE_ID
																							,APRVR_PHN
																							,APRVR_TITLE
																							,APPRVL_DT
																							,[dbo].[RemoveSpecialCharacters](LEFT(SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10),
           PATINDEX('%[^0-9]%', SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10) + 'X') -1))
																							,GETDATE()
																							,1
																							,1
																				FROM #MACH5_ORDR WITH (NOLOCK)
																				
																		)
																	END
																	
																	delete from @tmpids
																	-----FSA_ORDR_CUST
																	INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CUST_NME,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID)								
																		(SELECT	DISTINCT @FSA_ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				GETDATE(),
																				CUST_NME,
																				ISNULL(BILL_CYC_ID,0),
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CUST WITH (NOLOCK)
																			WHERE ISNULL(CSG_LVL,'')=''
																		)
																	
																	INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID)  output inserted.FSA_ORDR_CUST_ID into @tmpids
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				GETDATE(),
																				ISNULL(BILL_CYC_ID,0),
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CUST WITH (NOLOCK)
																			WHERE ISNULL(CSG_LVL,'')!=''
																		
																		
																		INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																		SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString(moc.CUST_NME)
																				FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
																				INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																				INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																				WHERE ISNULL(moc.CSG_LVL,'')!=''
																				  AND foc.CUST_NME IS NULL
																	
																	delete from @tmpids
																	IF NOT EXISTS
																		(
																			SELECT 'X'
																				FROM dbo.FSA_ORDR_CUST WITH (NOLOCK)
																				WHERE ORDR_ID = @ORDR_ID
																					AND CIS_LVL_TYPE = 'H1'
																		)
																		BEGIN
																			INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CUST_NME,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID)									
																		(SELECT	DISTINCT @FSA_ORDR_ID,
																				'H1',
																				H1_ID,
																				'',
																				'',
																				GETDATE(),
																				'',
																				BILL_CYC_ID,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CUST WITH (NOLOCK)
																			WHERE CIS_LVL_TYPE = 'H6'
																			  AND ISNULL(CSG_LVL,'')=''
																		)

																		INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				'H1',
																				H1_ID,
																				'',
																				'',
																				GETDATE(),
																				BILL_CYC_ID,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CUST WITH (NOLOCK)
																			WHERE CIS_LVL_TYPE = 'H6'
																			  AND ISNULL(CSG_LVL,'')!=''
																		
																		
																		INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																		SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString('')
																				FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
																				INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																				INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																				WHERE ISNULL(moc.CSG_LVL,'')!=''
																				  AND foc.CUST_NME IS NULL
																		END
																	
																	delete from @tmpids
																	----ORDR_ADR
																	INSERT INTO dbo.ORDR_ADR
																		(
																			ORDR_ID,
																			ADR_TYPE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			CTRY_CD,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			STREET_ADR_1,
																			STREET_ADR_2,
																			CTY_NME,
																			PRVN_NME,
																			STT_CD,
																			ZIP_PSTL_CD,
																			BLDG_NME,
																			FLR_ID,
																			RM_NBR,
																			HIER_LVL_CD,
																			STREET_ADR_3
																		)
																	(
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				CASE CIS_LVL_TYPE
																					WHEN 'H4' THEN 3
																					WHEN 'H6' THEN 18
																					WHEN 'H5' THEN 18
																					ELSE 17
																				END,
																				GETDATE(),
																				1,
																				1,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				'CIS',
																				STREET_ADR_1,
																				STREET_ADR_2,
																				CTY_NME,
																				PRVN_NME,
																				STT_CD,
																				ZIP_PSTL_CD,
																				BLDG_ID,
																				FLOOR_ID,
																				ROOM_ID,
																				CASE CIS_LVL_TYPE	
																					WHEN 'H4' THEN 0
																					WHEN 'H1' THEN 0
																					ELSE 1
																				END,
																				STREET_ADR_3
																			FROM #MACH5_ORDR_CUST
																			WHERE ISNULL(CSG_LVL,'')=''
																	)
																	
																	INSERT INTO dbo.ORDR_ADR
																		(
																			ORDR_ID,
																			ADR_TYPE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			CTRY_CD,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			HIER_LVL_CD
																		) output inserted.ORDR_ADR_ID into @tmpids
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				CASE CIS_LVL_TYPE
																					WHEN 'H4' THEN 3
																					WHEN 'H6' THEN 18
																					WHEN 'H5' THEN 18
																					ELSE 17
																				END,
																				GETDATE(),
																				1,
																				1,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				'CIS',
																				CASE CIS_LVL_TYPE	
																					WHEN 'H4' THEN 0
																					WHEN 'H1' THEN 0
																					ELSE 1
																				END
																			FROM #MACH5_ORDR_CUST
																			WHERE ISNULL(CSG_LVL,'')!=''
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
																		STREET_ADR_2,
																		CTY_NME,
																		STT_PRVN_NME,
																		STT_CD,
																		ZIP_PSTL_CD,
																		BLDG_NME,
																		FLR_ID,
																		RM_NBR,
																		STREET_ADR_3)
																	SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.STREET_ADR_1),
																			dbo.encryptString(moc.STREET_ADR_2),
																			dbo.encryptString(moc.CTY_NME),
																			dbo.encryptString(moc.PRVN_NME),
																			dbo.encryptString(moc.STT_CD),
																			dbo.encryptString(moc.ZIP_PSTL_CD),
																			dbo.encryptString(moc.BLDG_ID),
																			dbo.encryptString(moc.FLOOR_ID),
																			dbo.encryptString(moc.ROOM_ID),
																			dbo.encryptString(moc.STREET_ADR_3)
																		FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
																		INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND oa.ORDR_ID = @FSA_ORDR_ID
																		INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
																		WHERE ISNULL(moc.CSG_LVL,'')!=''
																		  AND oa.STREET_ADR_1 IS NULL
																	
																	delete from @tmpids
																	IF EXISTS
																	(
																		SELECT 'x'
																			FROM #MACH5_ORDR_CMPNT
																			WHERE	M5_ORDR_ID = @M5_ORDR_ID
																				AND	CPE_DEVICE_ID = @CPE_DEVICE_ID
																				AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0 	
																	)
																		BEGIN
																			INSERT INTO dbo.ORDR_ADR
																			(
																				ORDR_ID,
																				ADR_TYPE_ID,
																				CREAT_DT,
																				CREAT_BY_USER_ID,
																				REC_STUS_ID,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				FSA_MDUL_ID,
																				STREET_ADR_1,
																				STREET_ADR_2,
																				CTY_NME,
																				PRVN_NME,
																				STT_CD,
																				ZIP_PSTL_CD,
																				BLDG_NME,
																				FLR_ID,
																				RM_NBR,
																				HIER_LVL_CD,
																				STREET_ADR_3
																			)
																		(
																			SELECT	TOP 1 @FSA_ORDR_ID,
																					19, -- Shipping Address Type
																					GETDATE(),
																					1,
																					1,
																					CPE_ALT_SHIP_ADR_CTRY_CD,
																					(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')),
																					'CIS',
																					CPE_ALT_SHIP_ADR_LINE_1,
																					CPE_ALT_SHIP_ADR_LINE_2,
																					CPE_ALT_SHIP_ADR_CTY_NME,
																					CPE_ALT_SHIP_ADR_PRVN_NME,
																					CPE_ALT_SHIP_ADR_ST_PRVN_CD,
																					CPE_ALT_SHIP_ADR_PSTL_CD,
																					'',
																					'',
																					'',
																					1,
																					CPE_ALT_SHIP_ADR_LINE_3
																				FROM #MACH5_ORDR_CMPNT
																				WHERE	M5_ORDR_ID = @M5_ORDR_ID
																					AND	CPE_DEVICE_ID = @CPE_DEVICE_ID
																					AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0
																					AND ISNULL(CSG_LVL,'')=''
																		)
																		
																		INSERT INTO dbo.ORDR_ADR
																			(
																				ORDR_ID,
																				ADR_TYPE_ID,
																				CREAT_DT,
																				CREAT_BY_USER_ID,
																				REC_STUS_ID,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				FSA_MDUL_ID,
																				HIER_LVL_CD
																			) output inserted.ORDR_ADR_ID into @tmpids
																			SELECT	TOP 1 @FSA_ORDR_ID,
																					19, -- Shipping Address Type
																					GETDATE(),
																					1,
																					1,
																					CPE_ALT_SHIP_ADR_CTRY_CD,
																					(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')),
																					'CIS',
																					1
																				FROM #MACH5_ORDR_CMPNT
																				WHERE	M5_ORDR_ID = @M5_ORDR_ID
																					AND	CPE_DEVICE_ID = @CPE_DEVICE_ID
																					AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0
																					AND ISNULL(CSG_LVL,'')!=''
																		
																		INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
																		STREET_ADR_2,
																		CTY_NME,
																		STT_PRVN_NME,
																		STT_CD,
																		ZIP_PSTL_CD,
																		BLDG_NME,
																		FLR_ID,
																		RM_NBR,
																		STREET_ADR_3)
																		SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_1),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_2),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_CTY_NME),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_PRVN_NME),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_ST_PRVN_CD),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_PSTL_CD),
																			dbo.encryptString(''),
																			dbo.encryptString(''),
																			dbo.encryptString(''),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_3)
																		FROM dbo.ORDR_ADR oa WITH (NOLOCK)
																		INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID AND oa.ADR_TYPE_ID = 19 AND oa.CIS_LVL_TYPE=(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')) AND oa.ORDR_ID = @FSA_ORDR_ID
																		CROSS APPLY (SELECT DISTINCT CPE_ALT_SHIP_ADR_LINE_1,
																			CPE_ALT_SHIP_ADR_LINE_2,
																			CPE_ALT_SHIP_ADR_CTY_NME,
																			CPE_ALT_SHIP_ADR_PRVN_NME,
																			CPE_ALT_SHIP_ADR_ST_PRVN_CD,
																			CPE_ALT_SHIP_ADR_PSTL_CD,
																			CPE_ALT_SHIP_ADR_LINE_3 FROM #MACH5_ORDR_CMPNT mc WITH (NOLOCK) WHERE ISNULL(mc.CSG_LVL,'')!=''
																		  AND mc.M5_ORDR_ID = @M5_ORDR_ID
																		  AND mc.CPE_DEVICE_ID = @CPE_DEVICE_ID
																		  AND ISNULL(mc.CPE_ALT_SHIP_ADR,0) != 0) as moc
																		WHERE oa.STREET_ADR_1 IS NULL

																		END
																	
																	delete from @tmpids
																	------ORDR_CNTCT
																	INSERT INTO dbo.ORDR_CNTCT
																		(
																			ORDR_ID,
																			CNTCT_TYPE_ID,
																			PHN_NBR,
																			TME_ZONE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			FAX_NBR,
																			ROLE_ID,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			FRST_NME,
																			LST_NME,
																			EMAIL_ADR,
																			CNTCT_HR_TXT,
																			NPA,
																			NXX,
																			STN_NBR,
																			CTY_CD,
																			ISD_CD,
																			CNTCT_NME,
																			PHN_EXT_NBR
																		)
																	(
																		SELECT	DISTINCT @FSA_ORDR_ID ,
																				CASE c.ROLE_CD
																					WHEN 'Hrchy' THEN 17
																					ELSE 1
																				END AS CNTCT_TYPE_ID,
																				c.PHN_NBR,
																				l.TME_ZONE_ID,
																				GETDATE(),
																				1,
																				1,
																				'',
																				lr.ROLE_ID,
																				c.CIS_LVL_TYPE,
																				c.FSA_MDUL_ID,
																				c.FRST_NME,
																				c.LST_NME,
																				c.EMAIL_ADR,
																				c.CNTCT_HR_TXT,
																				c.NPA,
																				c.NXX,
																				c.STN_NBR,
																				c.CTY_CD,
																				c.ISD_CD,
																				c.CNTCT_NME,
																				c.PHN_EXT_NBR
																			FROM #MACH5_ORDR_CNTCT c
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = c.ROLE_CD
																		LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																							TME_ZONE_NME	AS TME_ZONE_NME 
																						FROM dbo.LK_TME_ZONE 
																						GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
																		WHERE ISNULL(CSG_LVL,'')=''
																	)
																	
																	INSERT INTO dbo.ORDR_CNTCT
																		(
																			ORDR_ID,
																			CNTCT_TYPE_ID,
																			PHN_NBR,
																			TME_ZONE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			FAX_NBR,
																			ROLE_ID,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			CNTCT_HR_TXT,
																			NPA,
																			NXX,
																			STN_NBR,
																			CTY_CD,
																			ISD_CD,
																			PHN_EXT_NBR
																		) output inserted.ORDR_CNTCT_ID into @tmpids
																		SELECT	DISTINCT @FSA_ORDR_ID ,
																				CASE c.ROLE_CD
																					WHEN 'Hrchy' THEN 17
																					ELSE 1
																				END AS CNTCT_TYPE_ID,
																				c.PHN_NBR,
																				l.TME_ZONE_ID,
																				GETDATE(),
																				1,
																				1,
																				'',
																				lr.ROLE_ID,
																				c.CIS_LVL_TYPE,
																				c.FSA_MDUL_ID,
																				c.CNTCT_HR_TXT,
																				c.NPA,
																				c.NXX,
																				c.STN_NBR,
																				c.CTY_CD,
																				c.ISD_CD,
																				c.PHN_EXT_NBR
																			FROM #MACH5_ORDR_CNTCT c
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = c.ROLE_CD
																		LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																							TME_ZONE_NME	AS TME_ZONE_NME 
																						FROM dbo.LK_TME_ZONE 
																						GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
																		WHERE ISNULL(CSG_LVL,'')!=''
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, FRST_NME,
																		LST_NME,
																		CUST_EMAIL_ADR,
																		CUST_CNTCT_NME)
																	SELECT	DISTINCT oc.ORDR_CNTCT_ID, 15, dbo.encryptstring(c.FRST_NME),
																			dbo.encryptstring(c.LST_NME),
																			dbo.encryptstring(c.EMAIL_ADR),
																			dbo.encryptstring(c.CNTCT_NME)
																		FROM #MACH5_ORDR_CNTCT c WITH (NOLOCK)
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
																	INNER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ROLE_ID=lr.ROLE_ID AND oc.CIS_LVL_TYPE=c.CIS_LVL_TYPE AND oc.ORDR_ID = @FSA_ORDR_ID AND oc.FRST_NME IS NULL
																	INNER JOIN @tmpids tmpids ON tmpids.tmpid = oc.ORDR_CNTCT_ID
																	WHERE ISNULL(c.CSG_LVL,'')!=''
																	  AND oc.FRST_NME IS NULL
																	
																	-----ORDER NOTES
																	IF EXISTS
																		(
																			SELECT 'X'
																				FROM #MACH5_ORDR_NOTES WITH (NOLOCK)
																		)
																	BEGIN
																		INSERT INTO ORDR_NTE
																		(
																			NTE_TYPE_ID,
																			ORDR_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			NTE_TXT
																		)
																		(
																			SELECT DISTINCT
																				CASE NTE_TYPE_CD
																					WHEN 'CPEN' THEN 26
																					WHEN 'CPES' THEN 27
																					WHEN 'CPEP' THEN 28
																					ELSE 25
																				END, 
																				@FSA_ORDR_ID,		
																				GETDATE(),
																				1,
																				1,
																				NTE_TXT		
																			FROM #MACH5_ORDR_NOTES
																			WHERE ISNULL(NTE_TXT,'') <> ''
																		)
																	END
																	
																	----SCA_NBR
																	IF EXISTS
																		(
																			SELECT 'X'
																				FROM #MACH5_ORDR_SCA WITH (NOLOCK)
																				WHERE TYPE_CD = 'EQ'
																		)
																		BEGIN
																			
																			--SELECT @SCA_NBR = COALESCE(@SCA_NBR + '| ' + SCA_NBR, SCA_NBR)
																			--	FROM #MACH5_ORDR_SCA WITH (NOLOCK)
																				
																			UPDATE dbo.FSA_ORDR SET SCA_NBR = @SCA_NBR_CPE WHERE ORDR_ID = @ORDR_ID
																		END
																
																	--FSA_ORDR_CPE_LINE_ITEM
																	INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
																		(
																			ORDR_ID,
																			EQPT_TYPE_ID, --EQPT_TYPE_CD 
																			EQPT_ID, --EQPT_ID
																			MFR_NME, --CPE_MFR_NME 
																			CNTRC_TYPE_ID, --CPE_CONTRACT_TYPE
																			CNTRC_TERM_ID, --CPE_CONTRACT_TERM 
																			MNTC_CD, --CPE_MNGD_FLG_CD 
																			CREAT_DT,
																			CMPNT_ID, --CPE_ITEM_ID 
																			LINE_ITEM_CD,
																			MANF_PART_CD, --CPE_MFR_PART_NBR 
																			UNIT_PRICE, --CPE_LIST_PRICE 
																			DROP_SHP, --CPE_DROP_SHIP_FLG_CD 
																			DEVICE_ID, --CPE_DEVICE_ID 
																			ORDR_CMPNT_ID,
																			ITM_STUS,
																			ORDR_QTY,
																			MDS_DES,
																			MANF_DISCNT_CD,
																			MATL_CD,
																			CMPNT_FMLY,
																			RLTD_CMPNT_ID,
																			STUS_CD,
																			MFR_PS_ID,
																			CPE_REUSE_CD,
																			ZSCLR_QUOTE_ID,
																			SMRT_ACCT_DOMN_TXT,
																			VRTL_ACCT_DOMN_TXT,
																			SPRINT_MNTD_FLG,
																			CPE_CUST_PRVD_SERIAL_NBR															
																		)
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				ISNULL(CPE_ITEM_TYPE_CD,EQPT_TYPE_CD),
																				EQPT_ID,
																				SUBSTRING(ISNULL(CPE_MFR_ID,''),1,50) AS CPE_MFR_NME,
																				CPE_CONTRACT_TYPE,
																				CONVERT(VARCHAR,ISNULL(CPE_CONTRACT_TERM,0)) AS CPE_CONTRACT_TERM,
																				CPE_MNGD_FLG_CD,
																				GETDATE(),
																				NVTRY_ID,
																				'CPE' AS LINE_ITEM_CD,
																				SUBSTRING(ISNULL(CPE_MFR_PART_NBR,''),1,20) AS CPE_MFR_PART_NBR,
																				CPE_LIST_PRICE,
																				CPE_DROP_SHIP_FLG_CD,
																				CPE_DEVICE_ID,
																				NVTRY_ID,
																				CASE STUS_CD
																					WHEN 'CN' THEN 402
																					ELSE 401
																				END AS ITM_STUS,
																				QTY,
																				SUBSTRING(ISNULL(NME,''),1,50) AS NME,
																				CPE_VNDR_DSCNT, 
																				CPE_ITEM_MSTR_NBR,
																				CPE_CMPN_FMLY,
																				RLTD_CMPNT_ID,
																				STUS_CD,
																				CPE_MFR_PS_ID,
																				CASE 
																					WHEN CPE_REPLMT = 0 THEN 0
																					WHEN CPE_REPLMT > 0 THEN 1
																					ELSE 0
																					END AS CPE_REUSE_CD,
																				QUOTE_ID,
																				CASE CPE_MFR_ID
																				WHEN 'CSCO' THEN (SELECT SMRT_ACCT_DOMN_TXT FROM #MACH5_ORDR_CMPNT WHERE CMPNT_TYPE_CD = 'OCPE')
																				ELSE null 
																				END AS	SMRT_ACCT_DOMN_TXT,
																				CASE CPE_MFR_ID
																				WHEN 'CSCO' THEN (SELECT VRTL_ACCT_DOMN_TXT FROM #MACH5_ORDR_CMPNT WHERE CMPNT_TYPE_CD = 'OCPE')
																				ELSE null 
																				END AS	VRTL_ACCT_DOMN_TXT,
																				CPE_SPRINT_MNT_FLG_CD,
																				CPE_CUST_PRVD_SERIAL_NBR
																			FROM #MACH5_ORDR_CMPNT
																			WHERE	M5_ORDR_ID = @M5_ORDR_ID
																				AND	CPE_DEVICE_ID = @CPE_DEVICE_ID	
																	
																	--Update Region ID for xNCI Orders
																	Update od
																			SET od.RGN_ID = 
																			CASE 
																				WHEN ISNULL(oa.STT_CD,CONVERT(VARCHAR, DecryptByKey(csdoa.STT_CD))) = 'GU' THEN 3
																				ELSE ISNULL(lc.RGN_ID,od.RGN_ID)
																			END 
																			FROM dbo.ORDR od WITH (ROWLOCK)
																		INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON od.ORDR_ID = oa.ORDR_ID
																		INNER JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD
																		LEFT JOIN dbo.CUST_SCRD_DATA csdoa WITH (NOLOCK) ON csdoa.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdoa.SCRD_OBJ_TYPE_ID=14 
																			WHERE od.ORDR_ID = @ORDR_ID
																				AND oa.CIS_LVL_TYPE = 'H6'
																			
																	IF @CHNG_ORDR_TYPE_FLG != 1
																		BEGIN
																			IF @ORDR_TYPE_CD = 'CN'
																				BEGIN
																					INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('ProcessMach5CPECancels:'+CONVERT(VARCHAR,@ORDR_ID)+@TransName,'insertMach5OrderDetails', GETDATE())
																					SET @ERRLog = @ERRLog + 'ProcessMach5CPECancels:'+CONVERT(VARCHAR,@ORDR_ID)
																					SET @ORDR_ID_INT = CONVERT(INT, @ORDR_ID)
																					EXEC [dbo].[ProcessMach5CPECancels] @ORDR_ID_INT
																				END
																			----State Machine
																				Exec dbo.InsertInitialTask @ORDR_ID,@OrderCategoryID
																			--SELECT @ORDR_ID	
																		END
															
																UPDATE #MACH5_ORDR_CMPNT
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
															
																
															END	
														ELSE
															BEGIN
																UPDATE #MACH5_ORDR_CMPNT
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID is null
															END	
														END
													ELSE
														BEGIN
															UPDATE #MACH5_ORDR_CMPNT
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
														END
													
												SET @Ctr = @Ctr + 1		
													
												END
										END
												
								END
							ELSE 
								BEGIN
									OPEN SYMMETRIC KEY FS@K3y 
									DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
									INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('Intl-CPE:'+@TransName,'insertMach5OrderDetails', GETDATE())
									SET @ERRLog = @ERRLog + 'Intl-CPE:'
									--default region ID to AMNCI and later modified after order insertion in this SP
									INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID,DMSTC_CD,PROD_ID)	
										SELECT @OrderCategoryID, 1, 1, CASE DOM_PROCESS_FLG_CD
																			WHEN 'N' THEN 1
																			ELSE 0
																			END,
												PROD_ID
										FROM #MACH5_ORDR WITH (NOLOCK)
										WHERE M5_ORDR_ID = @M5_ORDR_ID
																	
									SELECT @ORDR_ID = SCOPE_IDENTITY()
									SET @FSA_ORDR_ID = @ORDR_ID
									
									IF @QDA_ORDR_ID != ''
										SET @QDA_ORDR_ID = @QDA_ORDR_ID + ',' + CONVERT(varchar,@FSA_ORDR_ID)
									ELSE
										SET @QDA_ORDR_ID = CONVERT(varchar,@FSA_ORDR_ID)
									
									UPDATE #MACH5_ORDR_CMPNT
									SET		QDA_ORDR_ID = @FSA_ORDR_ID
									
								
									UPDATE		odr
										SET		PRNT_ORDR_ID	=	@ORDR_ID,
												RAS_DT			=	t.CPE_REQD_ON_SITE_DT,
												DLVY_CLLI		=	t.CPE_SHIP_CLLI_CD,
												H5_H6_CUST_ID	=	@H5_H6_CUST_ID,
												CSG_LVL_ID		=	(SELECT TOP 1 ISNULL(lcl.CSG_LVL_ID,0) FROM #MACH5_ORDR mo WITH (NOLOCK) LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=mo.CSG_LVL WHERE mo.M5_ORDR_ID = @M5_ORDR_ID),
												CHARS_ID		=	(SELECT TOP 1 CHARS_CUST_ID FROM #MACH5_ORDR WITH (NOLOCK) WHERE M5_ORDR_ID = @M5_ORDR_ID)
										FROM	dbo.ORDR	odr WITH (ROWLOCK) 
										INNER JOIN #MACH5_ORDR_CMPNT t WITH (NOLOCK) ON odr.ORDR_ID = t.QDA_ORDR_ID
										WHERE	odr.ORDR_ID			=	@ORDR_ID
											
										
									--FSA_ORDR
									INSERT INTO FSA_ORDR (ORDR_ID,ORDR_ACTN_ID,FTN,ORDR_TYPE_CD,ORDR_SUB_TYPE_CD,PROD_TYPE_CD,CUST_SIGNED_DT,CUST_WANT_DT,
															CUST_ORDR_SBMT_DT, CUST_CMMT_DT,CUST_PRMS_OCPY_CD,CUST_ACPT_ERLY_SRVC_CD,MULT_CUST_ORDR_CD,
															DISC_REAS_CD, INSTL_ESCL_CD,FSA_EXP_TYPE_CD,PRE_QUAL_NBR,PRNT_FTN,RELTD_FTN,
															CPE_ACCS_PRVDR_CD,CPE_DLVRY_DUTY_AMT, CPE_DLVRY_DUTY_ID,CPE_ECCKT_ID,CPE_TST_TN_NBR, CPE_REC_ONLY_CD,
															CPE_PHN_NBR,CPE_EQPT_ONLY_CD,CPE_SHIP_CHG_AMT
															)
									(
										SELECT	TOP 1 @FSA_ORDR_ID,@ORDR_ACTN_ID,m.ORDER_NBR,m.ORDR_TYPE_CD,m.ORDR_SUBTYPE_CD,'CP',m.CUST_SIGN_DT,
												m.CUST_WANT_DT,m.CUST_SBMT_DT,m.CUST_CMMT_DT,m.CUST_PREM_OCCU_FLG_CD,m.CUST_ACPT_SRVC_FLG_CD,m.MULT_ORDR_SBMTD_FLG_CD,
												m.DSCNCT_REAS_CD,m.EXP_FLG_CD,	CASE m.EXP_TYPE_CD 
																									WHEN 'B' THEN 'IE'
																									ELSE m.EXP_TYPE_CD
																									END
												,c.PRE_QUAL_NBR
												,CASE m.ORDR_TYPE_CD
													WHEN 'CN' THEN m.PRNT_FTN
													ELSE NULL
												  END
												,CASE m.ORDR_TYPE_CD
													WHEN 'CN' THEN NULL
													ELSE m.RLTD_FTN
												  END
												,c.CPE_ACCS_PRVDR_NME
												,m.CPE_DLVRY_DUTIES_AMT
												,m.CPE_DLVRY_DUTIES_CD
												,c.CPE_EQPT_CKT_ID
												,c.CPE_SPRINT_TEST_TN_NBR
												,c.CPE_RECORD_ONLY_FLG_CD
												,c.CPE_ACCS_PRVDR_TN_NBR
												,c.CPE_EQUIPMENT_ONLY_FLG_CD
												,m.CPE_SHPNG_COST_AMT
											FROM #MACH5_ORDR m WITH (NOLOCK)
												LEFT JOIN #MACH5_ORDR_CMPNT c WITH (NOLOCK) ON m.M5_ORDR_ID = c.M5_ORDR_ID AND c.CMPNT_TYPE_CD = 'OCPE'
											WHERE m.M5_ORDR_ID = @M5_ORDR_ID
												
											
									)
									
									
									--ORDR_EXP	
									IF EXISTS
									(
										SELECT 'X'
											FROM #MACH5_ORDR
											WHERE EXP_FLG_CD = 'Y'
									)
									BEGIN
										INSERT INTO ORDR_EXP (ORDR_ID,
																EXP_TYPE_ID,
																AUTHRZR_PHN_NBR,
																AUTHRZR_TITLE_ID,
																AUTHN_DT,
																BILL_TO_COST_CTR_CD,
																CREAT_DT,
																CREAT_BY_USER_ID,
																REC_STUS_ID
																	)
										(
											SELECT TOP 1	@FSA_ORDR_ID
															,CASE EXP_TYPE_CD
																	WHEN 'I' THEN 1
																	WHEN 'E' THEN 2
																	WHEN 'B' THEN 3
																	ELSE 4 
																END AS EXP_TYPE_ID
															,APRVR_PHN
															,APRVR_TITLE
															,APPRVL_DT
															,[dbo].[RemoveSpecialCharacters](LEFT(SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10),
           PATINDEX('%[^0-9]%', SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10) + 'X') -1))
															,GETDATE()
															,1
															,1
												FROM #MACH5_ORDR WITH (NOLOCK)
												
										)
									END
									
									delete from @tmpids
									-----FSA_ORDR_CUST
									INSERT INTO dbo.FSA_ORDR_CUST 
											(	ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												CREAT_DT,
												CUST_NME,
												CURR_BILL_CYC_CD,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID)								
										(SELECT	DISTINCT @FSA_ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												GETDATE(),
												CUST_NME,
												ISNULL(BILL_CYC_ID,0),
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID
											FROM #MACH5_ORDR_CUST WITH (NOLOCK)
											WHERE ISNULL(CSG_LVL,'')=''
										)
									
									INSERT INTO dbo.FSA_ORDR_CUST 
											(	ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												CREAT_DT,
												CURR_BILL_CYC_CD,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
										SELECT	DISTINCT @FSA_ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												GETDATE(),
												ISNULL(BILL_CYC_ID,0),
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID
											FROM #MACH5_ORDR_CUST WITH (NOLOCK)
											WHERE ISNULL(CSG_LVL,'')!=''
										
										INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
										SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString(moc.CUST_NME)
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												  AND foc.CUST_NME IS NULL
									
									delete from @tmpids
									IF NOT EXISTS
										(
											SELECT 'X'
												FROM #MACH5_ORDR_CUST WITH (NOLOCK)
												WHERE CIS_LVL_TYPE = 'H1'
										)
										BEGIN
											INSERT INTO dbo.FSA_ORDR_CUST 
											(	ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												CREAT_DT,
												CUST_NME,
												CURR_BILL_CYC_CD,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID)									
										SELECT	DISTINCT @FSA_ORDR_ID,
												'H1',
												H1_ID,
												'',
												'',
												GETDATE(),
												'',
												BILL_CYC_ID,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID
											FROM #MACH5_ORDR_CUST WITH (NOLOCK)
											WHERE CIS_LVL_TYPE = 'H6'
											  AND ISNULL(CSG_LVL,'')=''
											
											INSERT INTO dbo.FSA_ORDR_CUST 
											(	ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												CREAT_DT,
												CURR_BILL_CYC_CD,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids									
										SELECT	DISTINCT @FSA_ORDR_ID,
												'H1',
												H1_ID,
												'',
												'',
												GETDATE(),
												BILL_CYC_ID,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID
											FROM #MACH5_ORDR_CUST WITH (NOLOCK)
											WHERE CIS_LVL_TYPE = 'H6'
											  AND ISNULL(CSG_LVL,'')!=''
											  
											INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
											SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString('')
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
												WHERE moc.CIS_LVL_TYPE = 'H6'
												  AND ISNULL(moc.CSG_LVL,'')!=''
												  AND foc.CUST_NME IS NULL
										
										END
									
									delete from @tmpids
									----ORDR_ADR
									INSERT INTO dbo.ORDR_ADR
										(
											ORDR_ID,
											ADR_TYPE_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											CTRY_CD,
											CIS_LVL_TYPE,
											FSA_MDUL_ID,
											STREET_ADR_1,
											STREET_ADR_2,
											CTY_NME,
											PRVN_NME,
											STT_CD,
											ZIP_PSTL_CD,
											BLDG_NME,
											FLR_ID,
											RM_NBR,
											HIER_LVL_CD,
											STREET_ADR_3
										)
									(
										SELECT DISTINCT	@FSA_ORDR_ID,
												CASE CIS_LVL_TYPE
													WHEN 'H4' THEN 3
													WHEN 'H6' THEN 18
													WHEN 'H5' THEN 18
													ELSE 17
												END,
												GETDATE(),
												1,
												1,
												CTRY_CD,
												CIS_LVL_TYPE,
												'CIS',
												STREET_ADR_1,
												STREET_ADR_2,
												CTY_NME,
												PRVN_NME,
												STT_CD,
												ZIP_PSTL_CD,
												BLDG_ID,
												FLOOR_ID,
												ROOM_ID,
												CASE CIS_LVL_TYPE	
													WHEN 'H4' THEN 0
													WHEN 'H1' THEN 0
													ELSE 1
												END,
												STREET_ADR_3
											FROM #MACH5_ORDR_CUST
											WHERE ISNULL(CSG_LVL,'')=''
									)
									
									INSERT INTO dbo.ORDR_ADR
										(
											ORDR_ID,
											ADR_TYPE_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											CTRY_CD,
											CIS_LVL_TYPE,
											FSA_MDUL_ID,
											HIER_LVL_CD
										) output inserted.ORDR_ADR_ID into @tmpids
										SELECT DISTINCT	@FSA_ORDR_ID,
												CASE CIS_LVL_TYPE
													WHEN 'H4' THEN 3
													WHEN 'H6' THEN 18
													WHEN 'H5' THEN 18
													ELSE 17
												END,
												GETDATE(),
												1,
												1,
												CTRY_CD,
												CIS_LVL_TYPE,
												'CIS',
												CASE CIS_LVL_TYPE	
													WHEN 'H4' THEN 0
													WHEN 'H1' THEN 0
													ELSE 1
												END
											FROM #MACH5_ORDR_CUST
											WHERE ISNULL(CSG_LVL,'')!=''
									
									INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
												STREET_ADR_2,
												CTY_NME,
												STT_PRVN_NME,
												STT_CD,
												ZIP_PSTL_CD,
												BLDG_NME,
												FLR_ID,
												RM_NBR,
												STREET_ADR_3)
											SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.STREET_ADR_1),
													dbo.encryptString(moc.STREET_ADR_2),
													dbo.encryptString(moc.CTY_NME),
													dbo.encryptString(moc.PRVN_NME),
													dbo.encryptString(moc.STT_CD),
													dbo.encryptString(moc.ZIP_PSTL_CD),
													dbo.encryptString(moc.BLDG_ID),
													dbo.encryptString(moc.FLOOR_ID),
													dbo.encryptString(moc.ROOM_ID),
													dbo.encryptString(moc.STREET_ADR_3)
												FROM #MACH5_ORDR_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND oa.ORDR_ID = @FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												  AND oa.STREET_ADR_1 IS NULL
									
									delete from @tmpids
									IF EXISTS
									(
										SELECT 'x'
											FROM #MACH5_ORDR_CMPNT
											WHERE	M5_ORDR_ID = @M5_ORDR_ID
												AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0 	
									)
										BEGIN
											INSERT INTO dbo.ORDR_ADR
											(
												ORDR_ID,
												ADR_TYPE_ID,
												CREAT_DT,
												CREAT_BY_USER_ID,
												REC_STUS_ID,
												CTRY_CD,
												CIS_LVL_TYPE,
												FSA_MDUL_ID,
												STREET_ADR_1,
												STREET_ADR_2,
												CTY_NME,
												PRVN_NME,
												STT_CD,
												ZIP_PSTL_CD,
												BLDG_NME,
												FLR_ID,
												RM_NBR,
												HIER_LVL_CD,
												STREET_ADR_3
											)
										(
											SELECT	TOP 1 @FSA_ORDR_ID,
													19, -- Shipping Address Type
													GETDATE(),
													1,
													1,
													CPE_ALT_SHIP_ADR_CTRY_CD,
													(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')),
													'CIS',
													CPE_ALT_SHIP_ADR_LINE_1,
													CPE_ALT_SHIP_ADR_LINE_2,
													CPE_ALT_SHIP_ADR_CTY_NME,
													CPE_ALT_SHIP_ADR_PRVN_NME,
													CPE_ALT_SHIP_ADR_ST_PRVN_CD,
													CPE_ALT_SHIP_ADR_PSTL_CD,
													'',
													'',
													'',
													1,
													CPE_ALT_SHIP_ADR_LINE_3
												FROM #MACH5_ORDR_CMPNT
												WHERE	M5_ORDR_ID = @M5_ORDR_ID
													AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0
													AND ISNULL(CSG_LVL,'')=''
										)
										
										INSERT INTO dbo.ORDR_ADR
											(
												ORDR_ID,
												ADR_TYPE_ID,
												CREAT_DT,
												CREAT_BY_USER_ID,
												REC_STUS_ID,
												CTRY_CD,
												CIS_LVL_TYPE,
												FSA_MDUL_ID,
												HIER_LVL_CD
											) output inserted.ORDR_ADR_ID into @tmpids
											SELECT	TOP 1 @FSA_ORDR_ID,
													19, -- Shipping Address Type
													GETDATE(),
													1,
													1,
													CPE_ALT_SHIP_ADR_CTRY_CD,
													(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')),
													'CIS',
													1
												FROM #MACH5_ORDR_CMPNT
												WHERE	M5_ORDR_ID = @M5_ORDR_ID
													AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0
													AND ISNULL(CSG_LVL,'')!=''
										
										INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
																		STREET_ADR_2,
																		CTY_NME,
																		STT_PRVN_NME,
																		STT_CD,
																		ZIP_PSTL_CD,
																		BLDG_NME,
																		FLR_ID,
																		RM_NBR,
																		STREET_ADR_3)
										SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_1),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_2),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_CTY_NME),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_PRVN_NME),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_ST_PRVN_CD),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_PSTL_CD),
																			dbo.encryptString(''),
																			dbo.encryptString(''),
																			dbo.encryptString(''),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_3)
																		FROM dbo.ORDR_ADR oa WITH (NOLOCK)
																		INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID AND oa.ADR_TYPE_ID = 19 AND oa.CIS_LVL_TYPE=(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')) AND oa.ORDR_ID = @FSA_ORDR_ID
																		CROSS APPLY (SELECT DISTINCT CPE_ALT_SHIP_ADR_LINE_1,
																			CPE_ALT_SHIP_ADR_LINE_2,
																			CPE_ALT_SHIP_ADR_CTY_NME,
																			CPE_ALT_SHIP_ADR_PRVN_NME,
																			CPE_ALT_SHIP_ADR_ST_PRVN_CD,
																			CPE_ALT_SHIP_ADR_PSTL_CD,
																			CPE_ALT_SHIP_ADR_LINE_3 FROM #MACH5_ORDR_CMPNT mc WITH (NOLOCK) WHERE ISNULL(mc.CSG_LVL,'')!=''
																		  AND mc.M5_ORDR_ID = @M5_ORDR_ID
																		  AND mc.CPE_DEVICE_ID = @CPE_DEVICE_ID
																		  AND ISNULL(mc.CPE_ALT_SHIP_ADR,0) != 0) as moc
																		WHERE oa.STREET_ADR_1 IS NULL

										END
									
									delete from @tmpids
									------ORDR_CNTCT
									INSERT INTO dbo.ORDR_CNTCT
										(
											ORDR_ID,
											CNTCT_TYPE_ID,
											PHN_NBR,
											TME_ZONE_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											FAX_NBR,
											ROLE_ID,
											CIS_LVL_TYPE,
											FSA_MDUL_ID,
											FRST_NME,
											LST_NME,
											EMAIL_ADR,
											CNTCT_HR_TXT,
											NPA,
											NXX,
											STN_NBR,
											CTY_CD,
											ISD_CD,
											CNTCT_NME,
											PHN_EXT_NBR
										)
									(
										SELECT DISTINCT	@FSA_ORDR_ID ,
												CASE c.ROLE_CD
													WHEN 'Hrchy' THEN 17
													ELSE 1
												END AS CNTCT_TYPE_ID,
												c.PHN_NBR,
												l.TME_ZONE_ID,
												GETDATE(),
												1,
												1,
												'',
												lr.ROLE_ID,
												c.CIS_LVL_TYPE,
												c.FSA_MDUL_ID,
												c.FRST_NME,
												c.LST_NME,
												c.EMAIL_ADR,
												c.CNTCT_HR_TXT,
												c.NPA,
												c.NXX,
												c.STN_NBR,
												c.CTY_CD,
												c.ISD_CD,
												c.CNTCT_NME,
												c.PHN_EXT_NBR
											FROM #MACH5_ORDR_CNTCT c
										INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = c.ROLE_CD
										LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
															TME_ZONE_NME	AS TME_ZONE_NME 
														FROM dbo.LK_TME_ZONE 
														GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
										WHERE ISNULL(c.CSG_LVL,'')=''
									)
									
									INSERT INTO dbo.ORDR_CNTCT
										(
											ORDR_ID,
											CNTCT_TYPE_ID,
											PHN_NBR,
											TME_ZONE_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											FAX_NBR,
											ROLE_ID,
											CIS_LVL_TYPE,
											FSA_MDUL_ID,
											CNTCT_HR_TXT,
											NPA,
											NXX,
											STN_NBR,
											CTY_CD,
											ISD_CD,
											PHN_EXT_NBR
										) output inserted.ORDR_CNTCT_ID into @tmpids
										SELECT DISTINCT	@FSA_ORDR_ID ,
												CASE c.ROLE_CD
													WHEN 'Hrchy' THEN 17
													ELSE 1
												END AS CNTCT_TYPE_ID,
												c.PHN_NBR,
												l.TME_ZONE_ID,
												GETDATE(),
												1,
												1,
												'',
												lr.ROLE_ID,
												c.CIS_LVL_TYPE,
												c.FSA_MDUL_ID,
												c.CNTCT_HR_TXT,
												c.NPA,
												c.NXX,
												c.STN_NBR,
												c.CTY_CD,
												c.ISD_CD,
												c.PHN_EXT_NBR
											FROM #MACH5_ORDR_CNTCT c
										INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = c.ROLE_CD
										LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
															TME_ZONE_NME	AS TME_ZONE_NME 
														FROM dbo.LK_TME_ZONE 
														GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
										WHERE ISNULL(c.CSG_LVL,'')!=''
									
									INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, FRST_NME,
												LST_NME,
												CUST_EMAIL_ADR,
												CUST_CNTCT_NME)
											SELECT	DISTINCT oc.ORDR_CNTCT_ID, 15, dbo.encryptstring(c.FRST_NME),
													dbo.encryptstring(c.LST_NME),
													dbo.encryptstring(c.EMAIL_ADR),
													dbo.encryptstring(c.CNTCT_NME)
												FROM #MACH5_ORDR_CNTCT c WITH (NOLOCK)
												INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
											INNER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ROLE_ID=lr.ROLE_ID AND oc.CIS_LVL_TYPE=c.CIS_LVL_TYPE AND oc.ORDR_ID = @FSA_ORDR_ID
											INNER JOIN @tmpids tmpids ON tmpids.tmpid = oc.ORDR_CNTCT_ID
											WHERE ISNULL(c.CSG_LVL,'')!=''
											  AND oc.FRST_NME IS NULL
									
									-----ORDER NOTES
									IF EXISTS
										(
											SELECT 'X'
												FROM #MACH5_ORDR_NOTES WITH (NOLOCK)
										)
									BEGIN
										INSERT INTO ORDR_NTE
										(
											NTE_TYPE_ID,
											ORDR_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											NTE_TXT
										)
										(
											SELECT DISTINCT
												CASE NTE_TYPE_CD
													WHEN 'CPEN' THEN 26
													WHEN 'CPES' THEN 27
													WHEN 'CPEP' THEN 28
													ELSE 25
												END, 
												@FSA_ORDR_ID,		
												GETDATE(),
												1,
												1,
												NTE_TXT		
											FROM #MACH5_ORDR_NOTES
											WHERE ISNULL(NTE_TXT,'') <> ''
										)
									END
									
									----SCA_NBR
									IF EXISTS
										(
											SELECT 'X'
												FROM #MACH5_ORDR_SCA WITH (NOLOCK)
												WHERE TYPE_CD = 'EQ'
										)
										BEGIN
											
											--SELECT @SCA_NBR = COALESCE(@SCA_NBR + '| ' + SCA_NBR, SCA_NBR)
											--	FROM #MACH5_ORDR_SCA WITH (NOLOCK)
												
											UPDATE dbo.FSA_ORDR SET SCA_NBR = @SCA_NBR_CPE WHERE ORDR_ID = @ORDR_ID
										END
								
									--FSA_ORDR_CPE_LINE_ITEM
									INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
										(
											ORDR_ID,
											EQPT_TYPE_ID, --EQPT_TYPE_CD 
											EQPT_ID, --EQPT_ID
											MFR_NME, --CPE_MFR_NME 
											CNTRC_TYPE_ID, --CPE_CONTRACT_TYPE
											CNTRC_TERM_ID, --CPE_CONTRACT_TERM 
											MNTC_CD, --CPE_MNGD_FLG_CD 
											CREAT_DT,
											CMPNT_ID, --CPE_ITEM_ID 
											LINE_ITEM_CD,
											MANF_PART_CD, --CPE_MFR_PART_NBR 
											UNIT_PRICE, --CPE_LIST_PRICE 
											DROP_SHP, --CPE_DROP_SHIP_FLG_CD 
											DEVICE_ID, --CPE_DEVICE_ID 
											ORDR_CMPNT_ID,
											ITM_STUS,
											ORDR_QTY,
											MDS_DES,
											MANF_DISCNT_CD,
											MATL_CD,
											CMPNT_FMLY,
											RLTD_CMPNT_ID,
											STUS_CD,
											MFR_PS_ID,
											CPE_REUSE_CD,
											ZSCLR_QUOTE_ID,
											SMRT_ACCT_DOMN_TXT,
											VRTL_ACCT_DOMN_TXT,
											SPRINT_MNTD_FLG,
											CPE_CUST_PRVD_SERIAL_NBR
										)
										SELECT	DISTINCT @FSA_ORDR_ID,
												ISNULL(CPE_ITEM_TYPE_CD,EQPT_TYPE_CD),
												EQPT_ID,
												SUBSTRING(ISNULL(CPE_MFR_ID,''),1,50) AS CPE_MFR_NME,
												CPE_CONTRACT_TYPE,
												CONVERT(VARCHAR,ISNULL(CPE_CONTRACT_TERM,0)) AS CPE_CONTRACT_TERM,
												CPE_MNGD_FLG_CD,
												GETDATE(),
												NVTRY_ID, --CPE_ITEM_ID, changed 7/31/2017 per ODIE issue find dlp0278CPE_ITEM_ID,
												'CPE' AS LINE_ITEM_CD,
												SUBSTRING(ISNULL(CPE_MFR_PART_NBR,''),1,20) AS CPE_MFR_PART_NBR,
												CPE_LIST_PRICE,
												CPE_DROP_SHIP_FLG_CD,
												CPE_DEVICE_ID,
												NVTRY_ID,
												CASE STUS_CD
													WHEN 'CN' THEN 402
													ELSE 401
												END AS ITM_STUS,
												QTY,
												SUBSTRING(ISNULL(NME,''),1,50) AS NME,
												CPE_VNDR_DSCNT, 
												CPE_ITEM_MSTR_NBR,
												CPE_CMPN_FMLY,
												RLTD_CMPNT_ID,
												STUS_CD,
												CPE_MFR_PS_ID,
												CASE 
												  WHEN ISNULL(CPE_REPLMT,0) = 0 THEN 0
												  WHEN ISNULL(CPE_REPLMT,0) > 0 THEN 1
												END AS CPE_REUSE_CD,
												QUOTE_ID,
												CASE CPE_MFR_ID
													WHEN 'CSCO' THEN (SELECT SMRT_ACCT_DOMN_TXT FROM #MACH5_ORDR_CMPNT WHERE CMPNT_TYPE_CD = 'OCPE' AND ISNULL(SMRT_ACCT_DOMN_TXT,'') <> '')
													ELSE null 
													END AS	SMRT_ACCT_DOMN_TXT,
												CASE CPE_MFR_ID
													WHEN 'CSCO' THEN (SELECT VRTL_ACCT_DOMN_TXT FROM #MACH5_ORDR_CMPNT WHERE CMPNT_TYPE_CD = 'OCPE' AND ISNULL(VRTL_ACCT_DOMN_TXT,'') <> '')
													ELSE null 
													END AS	VRTL_ACCT_DOMN_TXT,
												CPE_SPRINT_MNT_FLG_CD,
												CPE_CUST_PRVD_SERIAL_NBR
											FROM #MACH5_ORDR_CMPNT
											WHERE	M5_ORDR_ID = @M5_ORDR_ID
												AND CPE_DEVICE_ID IS NOT NULL
												AND STUS_CD != 'CN'
									
									
									INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
										(
											ORDR_ID,
											EQPT_TYPE_ID, --EQPT_TYPE_CD 
											EQPT_ID, --EQPT_ID
											MFR_NME, --CPE_MFR_NME 
											CNTRC_TYPE_ID, --CPE_CONTRACT_TYPE
											CNTRC_TERM_ID, --CPE_CONTRACT_TERM 
											MNTC_CD, --CPE_MNGD_FLG_CD 
											CREAT_DT,
											CMPNT_ID, --CPE_ITEM_ID 
											LINE_ITEM_CD,
											MANF_PART_CD, --CPE_MFR_PART_NBR 
											UNIT_PRICE, --CPE_LIST_PRICE 
											DROP_SHP, --CPE_DROP_SHIP_FLG_CD 
											DEVICE_ID, --CPE_DEVICE_ID 
											ORDR_CMPNT_ID,
											ITM_STUS,
											ORDR_QTY,
											MDS_DES,
											MANF_DISCNT_CD,
											MATL_CD,
											CMPNT_FMLY,
											RLTD_CMPNT_ID,
											STUS_CD,
											MFR_PS_ID,
											CPE_REUSE_CD,
											ZSCLR_QUOTE_ID,
											SMRT_ACCT_DOMN_TXT,
											VRTL_ACCT_DOMN_TXT,
											SPRINT_MNTD_FLG,
											CPE_CUST_PRVD_SERIAL_NBR
										)
										SELECT DISTINCT	@FSA_ORDR_ID,
												ISNULL(t.CPE_ITEM_TYPE_CD,t.EQPT_TYPE_CD),
												t.EQPT_ID,
												SUBSTRING(ISNULL(t.CPE_MFR_ID,''),1,50) AS CPE_MFR_NME,
												t.CPE_CONTRACT_TYPE,
												CONVERT(VARCHAR,ISNULL(t.CPE_CONTRACT_TERM,0)) AS CPE_CONTRACT_TERM,
												t.CPE_MNGD_FLG_CD,
												GETDATE(),
												t.NVTRY_ID, --t.CPE_ITEM_ID, changed 7/31/2017 dlp0278 per ODIE issue find.
												'CPE' AS LINE_ITEM_CD,
												SUBSTRING(ISNULL(t.CPE_MFR_PART_NBR,''),1,20) AS CPE_MFR_PART_NBR,
												t.CPE_LIST_PRICE,
												t.CPE_DROP_SHIP_FLG_CD,
												t.CPE_DEVICE_ID,
												t.NVTRY_ID,
												CASE t.STUS_CD
													WHEN 'CN' THEN 402
													ELSE 401
												END AS ITM_STUS,
												t.QTY,
												SUBSTRING(ISNULL(t.NME,''),1,50) AS NME,
												t.CPE_VNDR_DSCNT, 
												t.CPE_ITEM_MSTR_NBR,
												t.CPE_CMPN_FMLY,
												t.RLTD_CMPNT_ID,
												t.STUS_CD,
												t.CPE_MFR_PS_ID,
												CASE 
												  WHEN ISNULL(CPE_REPLMT,0) = 0 THEN 0
												  WHEN ISNULL(CPE_REPLMT,0) > 0 THEN 1
												END AS CPE_REUSE_CD,
												QUOTE_ID,
												CASE CPE_MFR_ID
													WHEN 'CSCO' THEN (SELECT SMRT_ACCT_DOMN_TXT FROM #MACH5_ORDR_CMPNT WHERE CMPNT_TYPE_CD = 'OCPE' AND ISNULL(SMRT_ACCT_DOMN_TXT,'') <> '')
													ELSE null 
													END AS	SMRT_ACCT_DOMN_TXT,
												CASE CPE_MFR_ID
													WHEN 'CSCO' THEN (SELECT VRTL_ACCT_DOMN_TXT FROM #MACH5_ORDR_CMPNT WHERE CMPNT_TYPE_CD = 'OCPE' AND ISNULL(VRTL_ACCT_DOMN_TXT,'') <> '')
													ELSE null 
													END AS	VRTL_ACCT_DOMN_TXT,
												t.CPE_SPRINT_MNT_FLG_CD,
												t.CPE_CUST_PRVD_SERIAL_NBR
											FROM #MACH5_ORDR_CMPNT t WITH (NOLOCK)
											INNER JOIN #MACH5_ORDR o WITH (NOLOCK) ON t.M5_ORDR_ID = o.M5_ORDR_ID
											WHERE	t.M5_ORDR_ID = @M5_ORDR_ID
												AND t.STUS_CD = 'CN'
												AND (
														(o.ORDR_TYPE_CD != 'CN'
															AND EXISTS
															(
																SELECT 'X'
																	FROM dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK)
																	INNER JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON fc.ORDR_ID = f.ORDR_ID
																	WHERE f.FTN IN (SELECT ORDER_NBR From #MACH5_ORDR)
																		AND ((fc.DEVICE_ID IN (Select CPE_DEVICE_ID FROM #MACH5_ORDR_CMPNT WHERE M5_ORDR_ID = @M5_ORDR_ID))
																			OR (fc.DEVICE_ID IS NULL))
																		AND fc.ORDR_CMPNT_ID = t.NVTRY_ID
																	
																
															)
														)
													OR 
														(
															o.ORDR_TYPE_CD = 'CN'
															AND EXISTS
																(
																	SELECT 'X'
																	FROM dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK)
																	INNER JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON fc.ORDR_ID = f.ORDR_ID
																	WHERE f.FTN IN (SELECT PRNT_FTN From #MACH5_ORDR)
																		AND ((fc.DEVICE_ID IN (Select CPE_DEVICE_ID FROM #MACH5_ORDR_CMPNT WHERE M5_ORDR_ID = @M5_ORDR_ID))
																			OR (fc.DEVICE_ID IS NULL))
																		AND fc.ORDR_CMPNT_ID = t.NVTRY_ID
																)	
														)
													)
											
									--Update Region ID for xNCI Orders
									Update od
											SET od.RGN_ID = 
															CASE 
																WHEN ISNULL(oa.STT_CD,CONVERT(VARCHAR, DecryptByKey(csdoa.STT_CD))) = 'GU' THEN 3
																ELSE ISNULL(lc.RGN_ID,od.RGN_ID)
															END 
											FROM dbo.ORDR od WITH (ROWLOCK)
										INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON od.ORDR_ID = oa.ORDR_ID
										INNER JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD
										LEFT JOIN dbo.CUST_SCRD_DATA csdoa WITH (NOLOCK) ON csdoa.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdoa.SCRD_OBJ_TYPE_ID=14 
											WHERE od.ORDR_ID = @ORDR_ID
												AND oa.CIS_LVL_TYPE = 'H6'
												
									IF @CHNG_ORDR_TYPE_FLG != 1
										BEGIN
											IF @ORDR_TYPE_CD = 'CN'
												BEGIN
													INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('ProcessMach5CPECancels:'+CONVERT(VARCHAR,@ORDR_ID)+@TransName,'insertMach5OrderDetails', GETDATE())
													SET @ERRLog = @ERRLog + 'ProcessMach5CPECancels:'+CONVERT(VARCHAR,@ORDR_ID)
													SET @ORDR_ID_INT = CONVERT(INT, @ORDR_ID)
													EXEC [dbo].[ProcessMach5CPECancels] @ORDR_ID_INT
												END
											----State Machine
											Exec dbo.InsertInitialTask @ORDR_ID,@OrderCategoryID
											--SELECT @ORDR_ID	
										END
									
								END	
							
							
						END
				END
		END
		--Set Error Severity to MD(Missing Data) or X(Excluded orders), because either order component or data is missing to process the order
		IF (ISNULL(@QDA_ORDR_ID,'') = '')
			SET @QDA_ORDR_ID = 'X'


COMMIT TRANSACTION @TransName
END TRY
BEGIN CATCH
	IF (((XACT_STATE()) IN (1,-1)) AND (@@TRANCOUNT>0))
	BEGIN
		ROLLBACK TRANSACTION @TransName
		INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('Rollback:'+@ERRLog+';'+@TransName,'insertMach5OrderDetails', GETDATE())
	END
	/* update dbo.m5_bpm_cows_nrfc with (rowlock)
		set error_msg = substring(error_message(),1,1000)
		where ordr_id = @M5_ORDR_ID */
	--DECLARE @ErrNote VARCHAR(1000) = '@M5_ORDR_ID'+CONVERT(VARCHAR,@M5_ORDR_ID)
	--Set Error to IE(Internal Error), No need to set NRMBPM record to Extract_CD to E
	IF (ISNULL(@QDA_ORDR_ID,'') = '')
		SET @QDA_ORDR_ID = 'IE'
	EXEC dbo.insertErrorInfo --@ErrNote
END CATCH	
			
END
