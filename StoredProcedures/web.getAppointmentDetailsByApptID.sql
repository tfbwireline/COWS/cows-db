USE [COWS]
GO
_CreateObject 'SP','web','getAppointmentDetailsByApptID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/03/2011
-- Description:	Get an Appointment Info by @EVENT_ID
-- =========================================================
ALTER PROCEDURE [web].[getAppointmentDetailsByApptID]
	@APPT_ID	Int
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	DECLARE	@Users		Varchar(100)
	DECLARE	@CUser		Int
	DECLARE	@Email		Varchar(Max)
	DECLARE	@CEmail		Varchar(100)
	DECLARE	@Subject	Varchar(100)
	DECLARE	@Type		Varchar(100)
	SET	@Email	=	''
	SET	@CEmail	=	''
	SET	@Subject=	''
	
	SELECT			@Users			=	dbo.parseAssignedUsersFromAppt(ASN_TO_USER_ID_LIST_TXT)
					,@CUser			=	ap.CREAT_BY_USER_ID
					,@Subject		=	SUBJ_TXT
					,@Type			=	la.APPT_TYPE_DES
		FROM		dbo.APPT			ap	WITH (NOLOCK)
		INNER JOIN	dbo.LK_APPT_TYPE	la	WITH (NOLOCK)	ON	ap.APPT_TYPE_ID	=	la.APPT_TYPE_ID
		WHERE		ap.APPT_ID		=	@APPT_ID
		
	IF	ISNULL(@Users, '')	!=	''
		BEGIN
			DECLARE	@User	TABLE
				(UserID	Int)
			
			INSERT INTO	@User	(UserID)
				SELECT DISTINCT	StringID
						FROM	web.parseStringWithDelimiter(@Users, '|')	
						WHERE	StringID	!=	''
			
			IF	(SELECT Count(1) FROM @User)	>	0
				BEGIN
					SELECT	@Email = COALESCE(@Email + ', ' + EMAIL_ADR, EMAIL_ADR) 
						FROM 
						(SELECT EMAIL_ADR FROM	dbo.LK_USER WITH (NOLOCK)
							WHERE USER_ID IN (SELECT UserID FROM @User	WHERE	UserID	!=	@CUser)
						)T
						
					SET	@Email = Substring (@Email, 3, Len(@Email))	
					
					SELECT @CEmail = EMAIL_ADR FROM dbo.LK_USER WITH (NOLOCK) WHERE USER_ID = @CUser
				END				
		END
		
	SELECT	@Email		AS	EmailTo
			,@CUser		AS	CreatedBy
			,@CEmail	AS	CCTo
			,@Subject	AS	Subject
			,@Type		AS	ApptType
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END