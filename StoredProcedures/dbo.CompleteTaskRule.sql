USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[CompleteTaskRule]    Script Date: 07/28/2011 19:05:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By:		Jagannath Gangi
-- Modified by:		Ramesh Ragi
-- Create date: 07/28/2011
-- Description:	This SP is used to complete a rule associated to task
-- =============================================
ALTER PROCEDURE [dbo].[CompleteTaskRule]
@OrderID INT, 
@TaskId SMALLINT, 
@TaskStatus TINYINT, 
@Comments VARCHAR(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET DEADLOCK_PRIORITY 3
	
	DECLARE @Cnt SMALLINT
	DECLARE @Ctr		SmallInt
	DECLARE @SPName		VARCHAR(100)
	DECLARE @RuleId SMALLINT
	DECLARE @TStatus TINYINT

	SET @Cnt = 0
	SET @Ctr = 0
	SET @SPName = ''
	SET @RuleId = 0
	SET @TStatus = 0

	DECLARE @RuleTable TABLE (id INT, TaskStatus TINYINT, SPName VARCHAR(100), priority TINYINT, flag TINYINT)

	BEGIN TRY
		INSERT INTO @RuleTable 
			SELECT TASK_RULE_ID, @TaskStatus, SP_NME, PRTY_ID, 0
			FROM dbo.LK_TASK_RULE  WITH(NOLOCK)
			WHERE TASK_ID = @TaskId  AND  TASK_OPT_ID = @TaskStatus 
		

		SELECT @Cnt = COUNT(1) FROM @RuleTable
		SET @Ctr = 0
		IF @Cnt > 0
		BEGIN
			WHILE @Ctr < @Cnt
			BEGIN
				SELECT Top 1 @SPName = SPName, 
						   @TStatus = TaskStatus,
						   @RuleId = id 
						  FROM @RuleTable 
						  WHERE flag = 0
						  ORDER BY priority ASC							

				IF LEN(@SPName) > 5
					EXEC @SPName @OrderID, @TaskId, @TaskStatus, @Comments
				
				UPDATE @RuleTable SET flag = 1 
								WHERE id = @RuleId					
				SET @Ctr = @Ctr + 1
			END
		END
	END TRY
	BEGIN CATCH
		exec [dbo].[insertErrorInfo]
	END CATCH
END

