USE [COWS]
GO
_CreateObject 'SP','dbo','insertPreAndSubmitDate'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <08/06/2011>
-- Description:	<complete Pre Submit and Submit xNNCI Tasks>
-- =============================================
--Exec insertPreAndSubmitDate 390,200,0,''
ALTER PROCEDURE dbo.insertPreAndSubmitDate 
@OrderID	Int,
@TaskID		SmallInt,
@TaskStatus	TinyInt,
@Comments	Varchar(1000) = NULL
AS
BEGIN
	BEGIN TRY
DECLARE @FTN			varchar(20)
DECLARE @PreSubmitDate	smalldatetime
DECLARE @SubmitDate		smalldatetime
DECLARE @UserId			int

SELECT	@SubmitDate	=	CONVERT(smalldatetime,CREAT_DT)
	   ,@FTN		=	FTN
	FROM	dbo.FSA_ORDR WITH (NOLOCK)
	WHERE	ORDR_ID			=	@OrderID	
		AND	ORDR_ACTN_ID	=	2

SELECT		@PreSubmitDate	=	CREAT_DT
	FROM	dbo.FSA_ORDR WITH (NOLOCK)
	WHERE	FTN				=	@FTN
		AND	ORDR_ACTN_ID	=	1		

SELECT	@UserId	=	[USER_ID]
	FROM	dbo.LK_USER WITH (NOLOCK)
	WHERE	USER_ADID	=	'System'
	
		IF NOT EXISTS	
			(
				SELECT 'X'
					FROM dbo.ORDR_MS WITH (NOLOCK)
					WHERE ORDR_ID = @OrderId
			)
			BEGIN
				IF @UserId IS NOT NULL
					BEGIN
						INSERT INTO ORDR_MS (ORDR_ID,VER_ID,PRE_SBMT_DT,SBMT_DT,CREAT_BY_USER_ID)
						VALUES (@OrderID,1,@PreSubmitDate,@SubmitDate,@UserId)
					END
			END
		
	END TRY
	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
END
GO
