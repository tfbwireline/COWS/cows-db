USE [COWS]
GO
_CreateObject 'SP','dbo','checkMDSEventStatus'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 09/07/2011
-- Description:	Checks the Event Status prior before Bill Activation Ready.
-- =========================================================
ALTER PROCEDURE [dbo].[checkMDSEventStatus]
	@OrderID	Int,
	@TaskID		SmallInt	=	1000,
	@TaskStatus	TinyInt		=	0,
	@Comments	Varchar(1000) = NULL
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	IF	EXISTS
		(SELECT 'X' 
			FROM		dbo.ORDR			od	WITH (NOLOCK)
			INNER JOIN	dbo.LK_PPRT			lp	WITH (NOLOCK)	ON	od.PPRT_ID		=	lp.PPRT_ID
			WHERE		od.ORDR_ID		=	@OrderID
			AND			lp.MDS_CD		=	1
			AND			lp.ORDR_TYPE_ID	<>	6 --Billing Change 
			AND		(	
						(	
							lp.TRPT_CD		!= 1
						AND lp.ORDR_TYPE_ID != 7
						)
						OR 
						(
							lp.TRPT_CD	=	1
						AND	NOT EXISTS
							(
								SELECT 'X'
									FROM	dbo.ACT_TASK at WITH (NOLOCK)
									WHERE	at.ORDR_ID	=	od.ORDR_ID	
										AND	at.TASK_ID	IN	(207,212,215)
										AND	at.STUS_ID	=	2
							)
						)
					)
					
		)
		BEGIN
			UPDATE			ct
				SET			ct.STUS_ID	=	3, MODFD_DT = GETDATE()
				FROM		dbo.ACT_TASK			ct	WITH (ROWLOCK)	
				LEFT JOIN	dbo.FSA_MDS_EVENT_ORDR	fo	WITH (NOLOCK)	ON	ct.ORDR_ID				=	fo.ORDR_ID	
																		AND	ISNULL(fo.CMPLTD_CD, 0)	=	0
				WHERE		ct.ORDR_ID			=	@OrderID
				AND			ct.TASK_ID			=	1000
				AND			ct.STUS_ID			=	0	
		END	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END