USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getEventIDByApptID]    Script Date: 11/06/2019 1:22:36 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		sbg9814
-- Create date: 10/11/2011
-- Description:	Get the EventID for a given ApptID.
-- =========================================================
ALTER PROCEDURE [dbo].[getEventIDByApptID]
	@APPT_ID	Int
AS
BEGIN
SET NOCOUNT ON;
Begin Try
		SELECT			ISNULL(ap.EVENT_ID,0) AS EVENT_ID
					,ISNULL(ev.EVENT_TYPE_ID,0) AS EVENT_TYPE_ID
					,luc.DSPL_NME as [CREAT_BY_USER]
					,lum.DSPL_NME as [MODFD_BY_USER]
					,ap.CREAT_DT
					,ap.MODFD_DT
					,ISNULL(me.SOFT_ASSIGN_CD,0) AS SOFT_ASSIGN_CD
		FROM		dbo.APPT	ap	WITH (NOLOCK)
		LEFT JOIN	dbo.EVENT	ev	WITH (NOLOCK)	ON	ap.EVENT_ID	=	ev.EVENT_ID
		LEFT JOIN  dbo.LK_USER luc WITH (NOLOCK)   ON  ap.CREAT_BY_USER_ID = luc.[USER_ID]
		LEFT JOIN  dbo.LK_USER lum WITH (NOLOCK)   ON  ap.MODFD_BY_USER_ID = lum.[USER_ID]
		LEFT JOIN  dbo.MDS_EVENT me WITH (NOLOCK)   ON  ev.EVENT_ID=me.EVENT_ID
		WHERE		ap.APPT_ID		=	@APPT_ID	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO
