USE [COWS]
GO
_CreateObject 'SP','dbo','createOrderMSVersion'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 09/21/2011
-- Description:	Creates a new version everytime a Vendor Order Email is sent out.
-- =========================================================
-- Modify:		Kyle Wichert
-- Create date: 09/30/2011
-- Description:	Move MS to Vendor Order MS table.
-- =========================================================

ALTER PROCEDURE [dbo].[createOrderMSVersion]
	@VendorOrderEmailID	Int
	,@UserID			Int
AS
BEGIN
SET NOCOUNT ON;

DECLARE @OrderID		Int
DECLARE	@VendorOrderID	Int
DECLARE	@SentDate		DateTime
DECLARE	@Ver			Int
DECLARE @ProductType	VARCHAR(100)
DECLARE @term			Int
DECLARE	@orig			Int
SET	@VendorOrderID	=	0
SET @orig = 1
SET @term = 1

Begin Try
	SELECT Top 1	@OrderID		=	vo.ORDR_ID,
					@VendorOrderID	=	ve.VNDR_ORDR_ID,
					@Ver			=	ISNULL(ms.VER_ID,0)
		FROM		dbo.VNDR_ORDR		vo	WITH (NOLOCK)
		INNER JOIN	dbo.VNDR_ORDR_EMAIL	ve	WITH (NOLOCK)	ON	vo.VNDR_ORDR_ID			=	ve.VNDR_ORDR_ID
		LEFT JOIN	dbo.VNDR_ORDR_MS	ms	WITH (NOLOCK)	ON	ve.VNDR_ORDR_EMAIL_ID	=	ms.VNDR_ORDR_EMAIL_ID
		WHERE		ve.VNDR_ORDR_EMAIL_ID	=	@VendorOrderEmailID
			
		
	IF	@VendorOrderID	!=	0	
		BEGIN
			INSERT INTO	dbo.VNDR_ORDR_MS	WITH (ROWLOCK)
							(VNDR_ORDR_ID
							,VER_ID
							,VNDR_ORDR_EMAIL_ID
							,SENT_TO_VNDR_DT
							,ACK_BY_VNDR_DT
							,CREAT_BY_USER_ID
							,CREAT_DT)
				SELECT		@VendorOrderID
							,@Ver	+	1
							,@VendorOrderEmailID
							,getDate()
							,NULL
							,@UserID
							,getDate()

			IF @OrderID != 0
				BEGIN
					SELECT	@ProductType = ISNULL(PROD_TYPE_ID,0)
					FROM	dbo.IPL_ORDR WITH (NOLOCK)
					WHERE	ORDR_ID	=	@OrderID
					
					IF @ProductType = 7
						BEGIN
							IF NOT EXISTS	(SELECT TOP 1 'x'
											FROM			dbo.VNDR_ORDR vo		WITH (NOLOCK)
												INNER JOIN	dbo.VNDR_ORDR_EMAIL ve	WITH (NOLOCK) ON vo.VNDR_ORDR_ID		=	ve.VNDR_ORDR_ID
												INNER JOIN	dbo.VNDR_ORDR_MS	ms	WITH (NOLOCK) ON ve.VNDR_ORDR_EMAIL_ID	=	ms.VNDR_ORDR_EMAIL_ID
											WHERE vo.ORDR_ID = @OrderID AND vo.TRMTG_CD = 0 AND ms.SENT_TO_VNDR_DT IS NOT NULL AND ve.VNDR_EMAIL_TYPE_ID IN (1,2,3))
								BEGIN
									SET @orig = 0
								END
							IF NOT EXISTS	(SELECT TOP 1 'x'
											FROM			dbo.VNDR_ORDR vo		WITH (NOLOCK)
												INNER JOIN	dbo.VNDR_ORDR_EMAIL ve	WITH (NOLOCK) ON vo.VNDR_ORDR_ID	=	ve.VNDR_ORDR_ID
												INNER JOIN	dbo.VNDR_ORDR_MS	ms	WITH (NOLOCK) ON ve.VNDR_ORDR_EMAIL_ID	=	ms.VNDR_ORDR_EMAIL_ID
											WHERE vo.ORDR_ID = @OrderID AND vo.TRMTG_CD = 1 AND ms.SENT_TO_VNDR_DT IS NOT NULL AND ve.VNDR_EMAIL_TYPE_ID IN (1,2,3))
								BEGIN
									SET @term = 0
								END					
						END
					
					IF	EXISTS	(SELECT 'X' FROM dbo.ACT_TASK WITH (NOLOCK) WHERE ORDR_ID = @OrderID AND TASK_ID = 201 AND STUS_ID = 0) AND ((@term + @orig) = 2)
						BEGIN
												
							EXEC	dbo.completeActiveTask
									@OrderID
									,201
									,2
									,'Milestone: Order Sent to Vendor Date Completed.'
							
						END	
				END
		END	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
