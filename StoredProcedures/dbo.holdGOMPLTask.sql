USE [COWS]
GO
_CreateObject 'SP','dbo','holdGOMPLTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <Hold GOM PL in the workflow till 
--	either Vendor Acknowledge Date is populated 
--	or Vendor Disconnect Completed date is populated>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE dbo.holdGOMPLTask
@OrderID	Int,
@TaskID		SmallInt,
@TaskStatus	TinyInt,
@Comments	Varchar(1000) = NULL
AS
BEGIN

BEGIN TRY
	DECLARE @Continue Bit = 0
	IF EXISTS
		(
			SELECT 'X'
				FROM dbo.ACT_TASK WITH (NOLOCK)
				WHERE ORDR_ID = @OrderID
					AND TASK_ID = @TaskID
					AND STUS_ID = 0
		)
		BEGIN
			SET @Continue = 1
		END
	
	IF @Continue = 1
		BEGIN
			IF EXISTS
				(
					SELECT 'X'
						FROM	dbo.ORDR odr WITH (NOLOCK)
					Inner Join dbo.LK_PPRT lp WITH (NOLOCK) ON odr.PPRT_ID = lp.PPRT_ID
						WHERE	odr.ORDR_ID = @OrderID
							AND lp.ORDR_TYPE_ID = 7
							AND EXISTS --Condition added to check for IPL/DPL which bypass xNCI steps
								(
									SELECT 'X'
										FROM	dbo.WG_PTRN_STUS WITH (NOLOCK)
										WHERE	ORDR_ID		=	odr.ORDR_ID
											AND WG_PTRN_ID	IN	(609,612,618,619)
								)
				)
					BEGIN
						UPDATE dbo.ACT_TASK SET STUS_ID = 5, MODFD_DT = GETDATE() WHERE ORDR_ID = @OrderID AND TASK_ID = @TaskID
					END
		END

END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH
END
GO
