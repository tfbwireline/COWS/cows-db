USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertPreQualData]    Script Date: 12/18/2017 09:32:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 10/3/2016
-- Description:	SP gets the PreQual data on Intl orders when the data was not 
--       available when the order was originally extracted.  This program is called from 
--       the GetMach5Order job and will only process orders created in the last 24 hours.
-- =========================================================
ALTER PROCEDURE [dbo].[insertPreQualData]


AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY

		DECLARE @ORDR_ID  int
		DECLARE @IDS TABLE (OrderID int)
		
		INSERT INTO @IDS (OrderID)
			SELECT DISTINCT o.ORDR_ID FROM ORDR o WITH (NOLOCK)
				INNER JOIN FSA_ORDR f WITH (NOLOCK) ON o.ORDR_ID = f.ORDR_ID
			WHERE ISNULL(f.TPORT_CUR_NME,'') = ''
				AND o.ORDR_CAT_ID = 6
				AND o.DMSTC_CD = 1
				AND (FTN LIKE 'IN%' or FTN LIKE 'CH%')
				AND o.CREAT_DT > DATEADD(mm,-1,GETDATE())
				--AND ORDR_SUB_TYPE_CD NOT IN ('AR','AE','ISMV') removed 12/18/201  dlp0278 get info if available.
					
				
				
		WHILE EXISTS (SELECT * FROM @IDS)
			BEGIN
			
				SELECT TOP 1 @ORDR_ID = OrderID from @IDS

				IF @ORDR_ID != 0
					BEGIN
						EXEC dbo.updateNRMBPMData @ordr_id	
					END
				DELETE @IDS WHERE OrderID = @ORDR_ID
			
			END
				
			
	END TRY


	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH


END

