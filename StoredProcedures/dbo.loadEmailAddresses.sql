USE [COWS]
GO
_CreateObject 'SP','dbo','loadEmailAddresses'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sbg9814
-- Create date: 08/16/2011
-- Description:	Get all Email Requests to be sent out.
-- kh946640 06/19/2018 Updated the Role from SIS-Primary (RoleID: 11) to Implementation Project Manager(RoleID: 99)
-- =============================================
ALTER PROCEDURE [dbo].[loadEmailAddresses] 
	@ORDR_ID			Int
	,@EMAIL_REQ_TYPE_ID	Int
	,@EMAIL_REQ_ID		Int	=	0
AS
BEGIN
SET NOCOUNT ON
Begin Try
	DECLARE	@ParentOrderID	Int
	DECLARE	@EmailAddress	Varchar(max)
	DECLARE	@Flag			Bit
	DECLARE @ORDR_CAT_ID	TinyInt
	DECLARE @CSGLvlID		TINYINT=0
	SET	@ParentOrderID	=	@ORDR_ID
	SET	@EmailAddress	=	''	
	SET	@Flag			=	0

	--------------------------------------------------------------------------
	--	Extract the SIS-Primary Email Address for MDS & Cancels.
	--------------------------------------------------------------------------
	SELECT			@ParentOrderID		=	ISNULL(f2.ORDR_ID, @ORDR_ID)
		FROM		dbo.FSA_ORDR		f1	WITH (NOLOCK)	
		INNER JOIN	dbo.FSA_ORDR		f2	WITH (NOLOCK)	ON	f1.PRNT_FTN	=	f2.FTN
		WHERE		f1.ORDR_ID			=	@ORDR_ID
		AND			f2.ORDR_ACTN_ID		=	2
		AND			@EMAIL_REQ_TYPE_ID	=	2
		
	Select		@ORDR_CAT_ID = ORDR_CAT_ID, @CSGLvlID = CSG_LVL_ID
		FROM	dbo.ORDR WITH (NOLOCK)
		WHERE	ORDR_ID = @ORDR_ID
				
	OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;		
	IF @ORDR_CAT_ID IN (2,6)
		BEGIN
			SELECT		@EmailAddress	= CASE WHEN (@CSGLvlID>0) THEN ISNULL(dbo.decryptBinaryData(csd.CUST_EMAIL_ADR), '') ELSE ISNULL(oc.EMAIL_ADR, '') END
				FROM	dbo.ORDR_CNTCT	oc	WITH (NOLOCK) LEFT JOIN
						dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csd.SCRD_OBJ_TYPE_ID=15
				WHERE	oc.ORDR_ID		=	@ParentOrderID
				AND		oc.ROLE_ID		=	99	
		END
	ELSE
		BEGIN
			DECLARE @ORDR_CNTCT_EmailIDs Table (EmailIDs Varchar(100))
			INSERT INTO @ORDR_CNTCT_EmailIDs(EmailIDs)
			Select Distinct CASE WHEN (@CSGLvlID>0) THEN ISNULL(dbo.decryptBinaryData(csd.CUST_EMAIL_ADR), '') ELSE ISNULL(oc.EMAIL_ADR, '') END
			FROM	dbo.ORDR_CNTCT	oc	WITH (NOLOCK) LEFT JOIN
					dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csd.SCRD_OBJ_TYPE_ID=15
				WHERE	oc.ORDR_ID		=	@ORDR_ID
					AND oc.FSA_MDUL_ID = CASE @ORDR_CAT_ID 
											WHEN 6 THEN 'ATM'
											ELSE oc.FSA_MDUL_ID
										  END
					AND (CASE WHEN (@CSGLvlID>0) THEN ISNULL(dbo.decryptBinaryData(csd.CUST_EMAIL_ADR), '') ELSE ISNULL(oc.EMAIL_ADR, '') END) != ''
					AND CHARINDEX('sprint.com',(CASE WHEN (@CSGLvlID>0) THEN ISNULL(dbo.decryptBinaryData(csd.CUST_EMAIL_ADR), '') ELSE ISNULL(oc.EMAIL_ADR, '') END)) > 0
					
			IF ((Select COUNT(1) From @ORDR_CNTCT_EmailIDs) > 0)
				BEGIN
					SELECT		 @EmailAddress =  COALESCE(EmailIDs, @EmailAddress)  +  ',' + @EmailAddress
						FROM	@ORDR_CNTCT_EmailIDs
						
					Set @EmailAddress = SUBSTRING(@EmailAddress,0,LEN(@EmailAddress))	
					
					--Select @EmailAddress
				END
		END
	--------------------------------------------------------------------------
	--	If no EMAIL Found, then throw it into Error.
	--------------------------------------------------------------------------
	IF	@EmailAddress	=	''	OR	Len(@EmailAddress)	<	7
		BEGIN
			EXEC	[dbo].[insertOrderNotes]
					@ORDR_ID
					,9
					,'Email Address of SIS-Primary contact does not Exist in the database. Error sending out notification Email.'
					,1

			SET	@Flag	=	1				
		END
	
	--------------------------------------------------------------------------
	--	Update the dbo.EMAIL_REQ Table to Pedning or Errored Status.
	--------------------------------------------------------------------------
	UPDATE		dbo.EMAIL_REQ	WITH (ROWLOCK)
		SET		STUS_ID			=	Case	@Flag	
										When	1	Then	8
										Else	10
									End
				,EMAIL_LIST_TXT	=	Case	@Flag	
										When	1	Then	NULL
										Else	@EmailAddress
									End			
		WHERE	ORDR_ID				=	@ORDR_ID
			AND	EMAIL_REQ_TYPE_ID	=	@EMAIL_REQ_TYPE_ID
			AND	EMAIL_REQ_ID		=	@EMAIL_REQ_ID
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO