USE [COWS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
 ================================================================================================  
 Author:  csb9923  
 Create date: 4/20/2011  
 Description: gets the details of a view
 ssandov3 [20220215] - Updated Redesign to cater Contact List
 ================================================================================================  
*/
--[web].[getViewDetails_V2]  5, 1, 0
--[web].[getViewDetails_V2]  500, 18, 0
--[web].[getViewDetails_V2]  304,19,6244,'0',1,1
--[web].[getViewDetails_V2]  91,10,6244,'0',1,1

ALTER PROCEDURE [web].[getViewDetails_V2]  --5, 1, 0
	@VIEWID INT,
	@SITE_CNTNT_ID INT,
	@USER_ID INT = 0,
	@NewMDS CHAR(1) = '1',
	@CSGLvlId	TINYINT	= 0,
	@IsNewUI BIT = 0				 
AS
BEGIN
 
 SET NOCOUNT ON;	
  
 BEGIN TRY 

OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

DECLARE @CSG_LVL_ID VARCHAR(10) = CONVERT(VARCHAR(10), @CSGLvlId)
  DECLARE	@USERID INT
 DECLARE	@VIEWNAME VARCHAR(20)
 DECLARE	@DFLTFLAG CHAR(1)
 DECLARE	@PBLCFLAG CHAR(1)
 DECLARE	@FLTRCOLOPR1 VARCHAR(10)
 DECLARE	@FLTRCOLOPR2 VARCHAR(10)
 DECLARE	@FLTRCOL1 INT
 DECLARE	@FLTRCOL2 INT
 DECLARE	@FLTRCOLTXT1VAL VARCHAR(50)
 DECLARE	@FLTRCOLTXT2VAL VARCHAR(50)
 DECLARE	@FLTRFLAG CHAR(1)
 DECLARE	@SORTBYCOL1 INT
 DECLARE	@SORTBYCOL1ORDER CHAR(1)
 DECLARE	@SORTBYCOL2 INT
 DECLARE	@SORTBYCOL2ORDER CHAR(1)
 DECLARE	@LOGICOPR VARCHAR(10)
 DECLARE    @CREATBY INT
 DECLARE    @MODFDBY INT
 DECLARE	@MODFDDT DATETIME
 DECLARE	@CREATDT DATETIME
 DECLARE	@sCOLIDS VARCHAR(500)
 DECLARE	@sPOS VARCHAR(500)
 DECLARE	@MyViewFilter1 VARCHAR(1000)
 DECLARE	@MyViewFilter2 VARCHAR(500)
 DECLARE	@MyViewFilter3 VARCHAR(500)
 DECLARE	@MyViewFilter  VARCHAR(1000)
 SET @MyViewFilter = ''
 DECLARE	@MyViewSORT VARCHAR(50)
 
 SET @NewMDS = '1' -- added 12/7/17 dlp0278  field mismatch between MDS_EVENT and MDS_EVENT_NEW 
				   -- was causing errors. MDS_EVENT_NEW is no longer valid.	
 
	IF @IsNewUI = 1
	BEGIN
		 SET @MyViewFilter1 = ' INNER JOIN dbo.EVENT_HIST eh WITH (NOLOCK) ON eh.EVENT_ID = ev.EVENT_ID
					INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID] = eh.[MODFD_BY_USER_ID]
					INNER JOIN dbo.MAP_USR_PRF mup WITH (NOLOCK) ON mup.[USER_ID] = lu.[USER_ID]
					INNER JOIN dbo.LK_USR_PRF lup WITH (NOLOCK) ON lup.[USR_PRF_ID] = mup.[USR_PRF_ID]
					 WHERE lu.[USER_ID] = '+  CONVERT(VARCHAR(10), @USER_ID) + '
				 AND eh.[MODFD_BY_USER_ID] = '+  CONVERT(VARCHAR(10), @USER_ID) + '
			     AND eh.EVENT_ID = ev.EVENT_ID
			     AND lup.USR_PRF_DES like ''%Event Reviewer%''
			     AND mup.REC_STUS_ID = 1'
	END
ELSE
	BEGIN  
 SET @MyViewFilter1 = ' INNER JOIN dbo.EVENT_HIST eh WITH (NOLOCK) ON eh.EVENT_ID = ev.EVENT_ID
					INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID] = eh.[MODFD_BY_USER_ID]
					INNER JOIN dbo.USER_GRP_ROLE ug WITH (NOLOCK) ON ug.[USER_ID] = lu.[USER_ID]
					 WHERE lu.[USER_ID] = '+  CONVERT(VARCHAR(10), @USER_ID) + '
				 AND eh.[MODFD_BY_USER_ID] = '+  CONVERT(VARCHAR(10), @USER_ID) + '
			     AND eh.EVENT_ID = ev.EVENT_ID
			     AND ug.ROLE_ID = 22
			     AND ug.REC_STUS_ID = 1'
	END		     
SET @MyViewFilter2 = ' INNER JOIN dbo.EVENT_ASN_TO_USER ea WITH (NOLOCK) ON ea.EVENT_ID = ev.EVENT_ID WHERE (ea.ASN_TO_USER_ID = '+  CONVERT(VARCHAR(10), @USER_ID) + ' 
					AND ea.REC_STUS_ID = 1)'

SET @MyViewFilter3 = ' INNER JOIN dbo.EVENT_HIST eh WITH (NOLOCK) ON eh.EVENT_ID = ev.EVENT_ID WHERE ev.MDS_FAST_TRK_TYPE_ID IS NOT NULL
  AND ev.EVENT_STUS_ID = 6
  AND eh.ACTN_ID =	17
  AND eh.CREAT_BY_USER_ID = '+  CONVERT(VARCHAR(10), @USER_ID)
  
SET @MyViewSORT = ' ORDER BY ''EventID'' DESC '

	--DECLARE @FINAL_COLs NVARCHAR(MAX)
	DECLARE @QueryString NVARCHAR(MAX)
	DECLARE @QueryString_MDS NVARCHAR(MAX)
	DECLARE @SITE_CNTNT_COL_VW_CD CHAR(1)
	DECLARE @DSPL_VW_ID INT
	
	DECLARE @REDSGN_USER_ADID VARCHAR(10)
	DECLARE @REDSGN_USER_EMAIL VARCHAR(MAX)
	--SET @REDSGN_USER_ADID = (Select [USER_ADID] from lk_user where [USER_ID] = @USER_ID)
	SELECT @REDSGN_USER_ADID = [USER_ADID], @REDSGN_USER_EMAIL = [EMAIL_ADR] FROM dbo.LK_USER WHERE [USER_ID] = @USER_ID
	
	DECLARE @FLTRSTR VARCHAR(1000)
	SET @FLTRSTR=' WHERE '
	
	DECLARE @FLTRCOL1NAME VARCHAR(100)
	DECLARE @FLTRCOL2NAME VARCHAR(100)
	
	DECLARE @SORTSTR VARCHAR(1000)
	SET @SORTSTR=' ORDER BY '
	
	DECLARE @SORTCOL1NAME VARCHAR(100)
	DECLARE @SORTCOL2NAME VARCHAR(100)
	
	DECLARE @SORTORDER1 VARCHAR(10)
	DECLARE @SORTORDER2 VARCHAR(10)
	
	SET @DSPL_VW_ID=@VIEWID
	SET @SITE_CNTNT_COL_VW_CD='N'
	DECLARE @ViewDesc VARCHAR(200)
	SET @ViewDesc=''
	
	DECLARE @IsEMem BIT=0
	IF EXISTS (SELECT 'x' FROM dbo.MAP_USR_PRF WITH (NOLOCK) 
			WHERE [USER_ID]= @USER_ID AND USR_PRF_ID in(SELECT USR_PRF_ID FROM LK_USR_PRF WHERE USR_PRF_DES LIKE '%Event Member%') AND REC_STUS_ID=1)
	 SET @IsEMem=1
               
   IF @SITE_CNTNT_ID=1
		SET @QueryString='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID,
			CASE 
				WHEN eve.CSG_LVL_ID = 0 THEN (CASE WHEN LEN(ISNULL(ev.EVENT_DES,'''')) > 0 THEN ev.EVENT_TITLE_TXT + '' : ''+ ISNULL(ev.EVENT_DES,'''') ELSE ev.EVENT_TITLE_TXT END)
				WHEN ' + @CSG_LVL_ID + ' > 0 AND ' + @CSG_LVL_ID + ' <= eve.CSG_LVL_ID THEN (CASE WHEN LEN(ISNULL(ev.EVENT_DES,'''')) > 0 THEN dbo.decryptBinaryData(csd.EVENT_TITLE_TXT) + '' : ''+ ISNULL(ev.EVENT_DES,'''') ELSE ev.EVENT_TITLE_TXT END)
				ELSE ''''
			END AS ''Event Title'',  REPLACE(LK_ENHNC_SRVC.enhnc_srvc_nme, ''AD '', '''') AS ''AD Type'', EVENT_STUS_DES AS ''Event Status'',WRKFLW_STUS_DES AS ''Workflow Status'',CASE WHEN (LK_EVENT_STUS.EVENT_STUS_ID IN (1, 2) AND (ev.ESCL_CD = 1) AND ((select COUNT(1) from dbo.EVENT_ASN_TO_USER eau WITH (NOLOCK) where eau.EVENT_ID=ev.EVENT_ID and eau.REC_STUS_ID=1)=0)) THEN ''COWS ES Activators'' ELSE [dbo].[GetCommaSepAssignUserIDs] (ev.EVENT_ID) END AS ''Assigned To'',LK_USER.DSPL_NME AS ''Created By'''
   ELSE IF @SITE_CNTNT_ID=5
		BEGIN
			IF	@VIEWID	NOT IN (16, 19, 20, 22, 38, 60, 65, 71, 84, 85, 86, 87, 117, 118, 119, 120)
			BEGIN
				SET @QueryString='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID '
				SET @QueryString_MDS='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID '
			END
			ELSE IF ((@VIEWID IN (60, 71, 120)) AND (@IsEMem=1))
			BEGIN
				SET @QueryString='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID,
				CASE 
					WHEN eve.CSG_LVL_ID = 0 THEN ev.EVENT_TITLE_TXT
					WHEN ' + @CSG_LVL_ID + ' > 0 AND ' + @CSG_LVL_ID + ' <= eve.CSG_LVL_ID THEN dbo.decryptBinaryData(csd.EVENT_TITLE_TXT)
					ELSE ''''
				END AS ''Event Title'',
				ev.IPM_DES AS ''IPM Description'',EVENT_STUS_DES AS ''Event Status'',WRKFLW_STUS_DES AS ''Workflow Status'', CASE COALESCE(ev.MDS_FAST_TRK_TYPE_ID, '''') WHEN '''' THEN [dbo].[GetCommaSepAssignUserIDs] (ev.EVENT_ID) ELSE ''COWS ES Activators'' END AS ''Assigned To'',LK_USER.DSPL_NME AS ''Created By'''
				SET @QueryString_MDS='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID,
				CASE 
					WHEN eve.CSG_LVL_ID = 0 THEN ev.EVENT_TITLE_TXT
					WHEN ' + @CSG_LVL_ID + ' > 0 AND ' + @CSG_LVL_ID + ' <= eve.CSG_LVL_ID THEN dbo.decryptBinaryData(csd.EVENT_TITLE_TXT)
					ELSE ''''
				END AS ''Event Title'',
				ev.IPM_DES AS ''IPM Description'',EVENT_STUS_DES AS ''Event Status'',WRKFLW_STUS_DES AS ''Workflow Status'',
				CONVERT(varchar,(SELECT TOP 1 CREAT_DT FROM dbo.EVENT_HIST with (nolock)
                                  WHERE ACTN_ID in (5,16) and EVENT_ID = ev.EVENT_ID
                                   ORDER BY CREAT_DT DESC),109) as ''Publish Date'', 
				CASE COALESCE(ev.MDS_FAST_TRK_TYPE_ID, '''') WHEN '''' THEN [dbo].[GetCommaSepAssignUserIDs] (ev.EVENT_ID) ELSE ''COWS ES Activators'' END AS ''Assigned To'',LK_USER.DSPL_NME AS ''Created By'''
			END
			ELSE
			BEGIN
				SET @QueryString='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID,
				CASE 
					WHEN eve.CSG_LVL_ID = 0 THEN ev.EVENT_TITLE_TXT
					WHEN ' + @CSG_LVL_ID + ' > 0 AND ' + @CSG_LVL_ID + ' <= eve.CSG_LVL_ID THEN dbo.decryptBinaryData(csd.EVENT_TITLE_TXT)
					ELSE ''''
				END AS ''Event Title'',
				EVENT_STUS_DES AS ''Event Status'',WRKFLW_STUS_DES AS ''Workflow Status'', CASE COALESCE(ev.MDS_FAST_TRK_TYPE_ID, '''') WHEN '''' THEN [dbo].[GetCommaSepAssignUserIDs] (ev.EVENT_ID) ELSE ''COWS ES Activators'' END AS ''Assigned To'',LK_USER.DSPL_NME AS ''Created By'''
				SET @QueryString_MDS='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID,
				CASE 
					WHEN eve.CSG_LVL_ID = 0 THEN ev.EVENT_TITLE_TXT
					WHEN ' + @CSG_LVL_ID + ' > 0 AND ' + @CSG_LVL_ID + ' <= eve.CSG_LVL_ID THEN dbo.decryptBinaryData(csd.EVENT_TITLE_TXT)
					ELSE ''''
				END AS ''Event Title'',
				EVENT_STUS_DES AS ''Event Status'',WRKFLW_STUS_DES AS ''Workflow Status'', CASE COALESCE(ev.MDS_FAST_TRK_TYPE_ID, '''') WHEN '''' THEN [dbo].[GetCommaSepAssignUserIDs] (ev.EVENT_ID) ELSE ''COWS ES Activators'' END AS ''Assigned To'',LK_USER.DSPL_NME AS ''Created By'''
			END
		END
	ELSE IF	@SITE_CNTNT_ID	=	9
		BEGIN
			SET	@QueryString	=	'SELECT	DISTINCT Top 5000 ev.EVENT_ID AS ''EventID'', Convert(TINYINT,2) AS ''CSG_LVL_ID'''
		END
	ELSE IF	@SITE_CNTNT_ID	IN (2,3,4)
	BEGIN
		SET @QueryString='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID,
		CASE 
			WHEN eve.CSG_LVL_ID = 0 THEN (CASE WHEN LEN(ISNULL(ev.EVENT_DES,'''')) > 0 THEN ev.EVENT_TITLE_TXT + '' : ''+ ISNULL(ev.EVENT_DES,'''') ELSE ev.EVENT_TITLE_TXT END)
			WHEN ' + @CSG_LVL_ID + ' > 0 AND ' + @CSG_LVL_ID + ' <= eve.CSG_LVL_ID THEN (CASE WHEN LEN(ISNULL(ev.EVENT_DES,'''')) > 0 THEN dbo.decryptBinaryData(csd.EVENT_TITLE_TXT) + '' : ''+ ISNULL(ev.EVENT_DES,'''') ELSE ev.EVENT_TITLE_TXT END)
			ELSE ''''
		END AS ''Event Title'',
		EVENT_STUS_DES AS ''Event Status'',WRKFLW_STUS_DES AS ''Workflow Status'',CASE WHEN (LK_EVENT_STUS.EVENT_STUS_ID IN (1, 2) AND (ev.ESCL_CD = 1) AND ((select COUNT(1) from dbo.EVENT_ASN_TO_USER eau WITH (NOLOCK) where eau.EVENT_ID=ev.EVENT_ID and REC_STUS_ID=1)=0)) THEN ''COWS ES Activators'' ELSE [dbo].[GetCommaSepAssignUserIDs] (ev.EVENT_ID) END AS ''Assigned To'',LK_USER.DSPL_NME AS ''Created By'''
	END
	ELSE IF @SITE_CNTNT_ID	=	18
		BEGIN
			SET	@QueryString	=	'SELECT	DISTINCT Top 5000 '
		END
	ELSE IF	@SITE_CNTNT_ID	IN	(10, 19)
		BEGIN
			SET @QueryString='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID,
			CASE 
				WHEN eve.CSG_LVL_ID = 0 THEN ev.EVENT_TITLE_TXT
				WHEN ' + @CSG_LVL_ID + ' > 0 AND ' + @CSG_LVL_ID + ' <= eve.CSG_LVL_ID THEN dbo.decryptBinaryData(csd.EVENT_TITLE_TXT)
				ELSE ''''
			END AS ''Event Title'',
			EVENT_STUS_DES AS ''Event Status'',WRKFLW_STUS_DES AS ''Workflow Status'',[dbo].[GetCommaSepAssignUserIDs] (ev.EVENT_ID) AS ''Assigned To'',LK_USER.DSPL_NME AS ''Created By'''
		END
	ELSE
		SET @QueryString='SELECT TOP 5000 ev.EVENT_ID AS ''EventID'', eve.CSG_LVL_ID,
			CASE 
				WHEN eve.CSG_LVL_ID = 0 THEN ev.EVENT_TITLE_TXT
				WHEN ' + @CSG_LVL_ID + ' > 0 AND ' + @CSG_LVL_ID + ' <= eve.CSG_LVL_ID THEN dbo.decryptBinaryData(csd.EVENT_TITLE_TXT)
				ELSE ''''
			END AS ''Event Title'',
			EVENT_STUS_DES AS ''Event Status'',WRKFLW_STUS_DES AS ''Workflow Status'',CASE WHEN (LK_EVENT_STUS.EVENT_STUS_ID IN (1, 2) AND (ev.ESCL_CD = 1) AND ((select COUNT(1) from dbo.EVENT_ASN_TO_USER eau WITH (NOLOCK) where eau.EVENT_ID=ev.EVENT_ID and REC_STUS_ID=1)=0)) THEN ''COWS ES Activators'' ELSE [dbo].[GetCommaSepAssignUserIDs] (ev.EVENT_ID) END AS ''Assigned To'',LK_USER.DSPL_NME AS ''Created By'''
           
  SELECT  @DFLTFLAG=[DFLT_VW_CD]
      ,@PBLCFLAG=[PBLC_VW_CD]
      ,@FLTRCOLOPR1=[FILTR_COL_1_OPR_ID]
      ,@FLTRCOLOPR2=[FILTR_COL_2_OPR_ID]
      ,@FLTRCOL1=[FILTR_COL_1_ID]
      ,@FLTRCOL2=[FILTR_COL_2_ID]
      ,@FLTRCOLTXT1VAL=[FILTR_COL_1_VALU_TXT]
      ,@FLTRCOLTXT2VAL=[FILTR_COL_2_VALU_TXT]
      ,@FLTRFLAG=[FILTR_ON_CD]
      ,@SORTBYCOL1=[SORT_BY_COL_1_ID]
      ,@SORTBYCOL2=[SORT_BY_COL_2_ID]
      ,@CREATBY=[CREAT_BY_USER_ID]
      ,@MODFDBY=[MODFD_BY_USER_ID]
      ,@MODFDDT=[MODFD_DT]
      ,@CREATDT=[CREAT_DT]
      ,@LOGICOPR=[FILTR_LOGIC_OPR_ID]
      ,@SORTBYCOL1ORDER=[SORT_BY_COL_1_ASC_ORDR_CD]
      ,@SORTBYCOL2ORDER=[SORT_BY_COL_2_ASC_ORDR_CD]
  FROM [COWS].[dbo].[DSPL_VW] WITH (NOLOCK) WHERE DSPL_VW_ID=@VIEWID
  
  DECLARE @TBLSTR VARCHAR(Max)
  DECLARE @TBLSTR_MDS VARCHAR(Max)
  DECLARE @TBLNAME VARCHAR(10)
  
	
	SET @TBLNAME = 'ev'
	
	  
	SET @TBLSTR=CASE WHEN @SITE_CNTNT_ID=1 THEN 'AD_EVENT ev WITH (NOLOCK) INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID
	INNER JOIN LK_WRKFLW_STUS WITH (NOLOCK) ON ev.WRKFLW_STUS_ID=LK_WRKFLW_STUS.WRKFLW_STUS_ID
	INNER JOIN LK_EVENT_STUS WITH (NOLOCK) ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND LK_EVENT_STUS.REC_STUS_ID = 1
	INNER JOIN LK_ENHNC_SRVC WITH (NOLOCK) ON ev.ENHNC_SRVC_ID=LK_ENHNC_SRVC.ENHNC_SRVC_ID
	INNER JOIN LK_USER WITH (NOLOCK) ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID
	INNER JOIN dbo.LK_USER RU WITH (NOLOCK) ON RU.USER_ID = ev.REQOR_USER_ID
	INNER JOIN dbo.LK_USER SU WITH (NOLOCK) ON SU.USER_ID = ev.SALS_USER_ID
	LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=1'
	                   WHEN @SITE_CNTNT_ID=5 AND @VIEWID IN (16, 19, 20, 22, 38, 60, 65, 71, 84, 85, 86, 87, 117, 118, 119, 120) THEN 'MDS_EVENT_NEW ev WITH (NOLOCK) INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID
					   LEFT JOIN FSA_MDS_EVENT_NEW WITH (NOLOCK) ON ev.EVENT_ID = FSA_MDS_EVENT_NEW.EVENT_ID
					   INNER JOIN LK_WRKFLW_STUS WITH (NOLOCK) ON ev.WRKFLW_STUS_ID=LK_WRKFLW_STUS.WRKFLW_STUS_ID
					   INNER JOIN LK_EVENT_STUS WITH (NOLOCK) ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND LK_EVENT_STUS.REC_STUS_ID = 1
					   INNER JOIN LK_USER WITH (NOLOCK) ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID
					   LEFT JOIN dbo.LK_USER RU WITH (NOLOCK) ON RU.USER_ID = ev.REQOR_USER_ID
					   LEFT JOIN dbo.MDS_EVENT_MAC_ACTY  WITH (NOLOCK)	ON  ev.EVENT_ID	= MDS_EVENT_MAC_ACTY.EVENT_ID
					   LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=8'
	                   WHEN @SITE_CNTNT_ID=3 THEN 'MPLS_EVENT ev WITH (NOLOCK) INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID
					   INNER JOIN LK_WRKFLW_STUS WITH (NOLOCK) ON ev.WRKFLW_STUS_ID=LK_WRKFLW_STUS.WRKFLW_STUS_ID
					   INNER JOIN LK_EVENT_STUS WITH (NOLOCK) ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND LK_EVENT_STUS.REC_STUS_ID = 1
					   INNER JOIN LK_USER WITH (NOLOCK) ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID
					   INNER JOIN dbo.LK_USER RU WITH (NOLOCK) ON RU.USER_ID = ev.REQOR_USER_ID
					   INNER JOIN dbo.LK_USER SU WITH (NOLOCK) ON SU.USER_ID = ev.SALS_USER_ID
					   LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=9'
					   WHEN @SITE_CNTNT_ID=2 THEN 'NGVN_EVENT ev WITH (NOLOCK) INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID
					   INNER JOIN LK_WRKFLW_STUS WITH (NOLOCK) ON ev.WRKFLW_STUS_ID=LK_WRKFLW_STUS.WRKFLW_STUS_ID
					   INNER JOIN LK_EVENT_STUS WITH (NOLOCK) ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND LK_EVENT_STUS.REC_STUS_ID = 1
					   INNER JOIN LK_USER WITH (NOLOCK) ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID
					   INNER JOIN dbo.LK_USER RU WITH (NOLOCK) ON RU.USER_ID = ev.REQOR_USER_ID
					   LEFT JOIN dbo.LK_USER SU WITH (NOLOCK) ON SU.USER_ID = ev.SALS_USER_ID
					   LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=11'
					   WHEN @SITE_CNTNT_ID=4 THEN 'SPLK_EVENT ev WITH (NOLOCK) INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID
					   INNER JOIN LK_WRKFLW_STUS WITH (NOLOCK) ON ev.WRKFLW_STUS_ID=LK_WRKFLW_STUS.WRKFLW_STUS_ID
					   INNER JOIN LK_EVENT_STUS WITH (NOLOCK) ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND LK_EVENT_STUS.REC_STUS_ID = 1
					   INNER JOIN LK_USER WITH (NOLOCK) ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID
					   INNER JOIN dbo.LK_USER RU WITH (NOLOCK) ON RU.USER_ID = ev.REQOR_USER_ID
					   INNER JOIN dbo.LK_USER SU WITH (NOLOCK) ON SU.USER_ID = ev.SALS_USER_ID
					   LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=19'
					   WHEN @SITE_CNTNT_ID=5 AND @VIEWID NOT IN (16, 19, 20, 22, 38, 60, 65, 71, 117, 118, 119, 120)	THEN
						'				MDS_EVENT_NEW ev WITH (NOLOCK)
							INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID  
							LEFT JOIN	(SELECT Min(FSA_MDS_EVENT_ID) AS MIN_ID,	EVENT_ID
											FROM	dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK)
											GROUP BY	EVENT_ID)a	ON ev.EVENT_ID = a.EVENT_ID 
							LEFT JOIN	dbo.FSA_MDS_EVENT_NEW	WITH (NOLOCK)	ON	a.MIN_ID	=	FSA_MDS_EVENT_NEW.FSA_MDS_EVENT_ID				
							INNER JOIN	dbo.LK_EVENT_STUS WITH (NOLOCK)	ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND	LK_EVENT_STUS.REC_STUS_ID = 1
							INNER JOIN	dbo.LK_USER WITH (NOLOCK)		ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID 
							LEFT JOIN   dbo.MDS_MNGD_ACT_NEW		WITH (NOLOCK)	ON	ev.EVENT_ID			=	MDS_MNGD_ACT_NEW.EVENT_ID
							LEFT JOIN	dbo.LK_WRKFLW_STUS		WITH (NOLOCK)	ON	ev.WRKFLW_STUS_ID	=	LK_WRKFLW_STUS.WRKFLW_STUS_ID
							LEFT JOIN	dbo.LK_SPRINT_CPE_NCR	WITH (NOLOCK)	ON	ev.SPRINT_CPE_NCR_ID	=	LK_SPRINT_CPE_NCR.SPRINT_CPE_NCR_ID
							LEFT JOIN	dbo.MDS_FAST_TRK_TYPE	WITH (NOLOCK)	ON	ev.MDS_FAST_TRK_TYPE_ID	=	MDS_FAST_TRK_TYPE.MDS_FAST_TRK_TYPE_ID
							LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=8
							 '
					  WHEN @SITE_CNTNT_ID =	9	THEN	'dbo.FEDLINE_EVENT_USER_DATA	WITH (NOLOCK)
															INNER JOIN	dbo.FEDLINE_EVENT_TADPOLE_DATA ev WITH (NOLOCK)	ON	FEDLINE_EVENT_USER_DATA.EVENT_ID		=	ev.EVENT_ID	AND ev.REC_STUS_ID = 1
															INNER JOIN	dbo.LK_EVENT_STUS WITH (NOLOCK)				ON	FEDLINE_EVENT_USER_DATA.EVENT_STUS_ID =	LK_EVENT_STUS.EVENT_STUS_ID	AND	LK_EVENT_STUS.REC_STUS_ID = 1
															INNER JOIN	dbo.LK_FEDLINE_ORDR_TYPE WITH (NOLOCK)		ON	LK_FEDLINE_ORDR_TYPE.ORDR_TYPE_CD		=	ev.ORDR_TYPE_CD
															LEFT JOIN	dbo.LK_USER	leng WITH (NOLOCK)				ON	FEDLINE_EVENT_USER_DATA.ENGR_USER_ID	=	leng.USER_ID
															LEFT JOIN	dbo.LK_USER	lact WITH (NOLOCK)				ON	FEDLINE_EVENT_USER_DATA.ACTV_USER_ID	=	lact.USER_ID
															'
					   WHEN @SITE_CNTNT_ID = 10 THEN 'SIPT_EVENT ev WITH (NOLOCK) INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID
												INNER JOIN LK_WRKFLW_STUS WITH (NOLOCK) ON ev.WRKFLW_STUS_ID=LK_WRKFLW_STUS.WRKFLW_STUS_ID
												INNER JOIN LK_EVENT_STUS WITH (NOLOCK) ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND LK_EVENT_STUS.REC_STUS_ID = 1
												INNER JOIN LK_USER WITH (NOLOCK) ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID
												INNER JOIN dbo.LK_USER RU WITH (NOLOCK) ON RU.USER_ID = ev.REQOR_USER_ID
												LEFT JOIN dbo.LK_USER NEU WITH (NOLOCK) ON NEU.USER_ID = ev.NTWK_ENGR_ID
												LEFT JOIN dbo.LK_USER NTEU WITH (NOLOCK) ON NTEU.USER_ID = ev.NTWK_TECH_ENGR_ID
												LEFT JOIN dbo.LK_USER PMU WITH (NOLOCK) ON PMU.USER_ID = ev.PM_ID 
												LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=18'
					   WHEN @SITE_CNTNT_ID = 19 THEN 'UCaaS_EVENT ev WITH (NOLOCK) INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID 
														INNER JOIN LK_WRKFLW_STUS WITH (NOLOCK) ON ev.WRKFLW_STUS_ID=LK_WRKFLW_STUS.WRKFLW_STUS_ID 
														INNER JOIN LK_EVENT_STUS WITH (NOLOCK) ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND LK_EVENT_STUS.REC_STUS_ID = 1	
														INNER JOIN LK_USER WITH (NOLOCK) ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID 
														INNER JOIN dbo.LK_USER RU WITH (NOLOCK) ON RU.USER_ID = ev.REQOR_USER_ID
														LEFT JOIN 	dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=20
														LEFT JOIN	dbo.LK_SPRINT_CPE_NCR	WITH (NOLOCK)	ON	ev.SPRINT_CPE_NCR_ID	=	LK_SPRINT_CPE_NCR.SPRINT_CPE_NCR_ID
														'
						WHEN @SITE_CNTNT_ID	=	18 THEN '	dbo.REDSGN r WITH (NOLOCK)
															LEFT JOIN 	dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=r.REDSGN_ID AND csd.SCRD_OBJ_TYPE_ID=16
															Left Join	dbo.REDSGN_DEVICES_INFO rd with (nolock) on r.REDSGN_ID = rd.REDSGN_ID AND rd.REC_STUS_ID = 1
															Inner Join	dbo.LK_USER lu with (nolock) on lu.USER_ID = r.CRETD_BY_CD
															Inner Join	dbo.LK_STUS ls with (nolock) on ls.STUS_ID = r.STUS_ID
															Inner Join	dbo.LK_REDSGN_TYPE lrt with (nolock) on lrt.REDSGN_TYPE_ID = r.REDSGN_TYPE_ID
															Left Join	dbo.LK_USER lup with (nolock) on r.PM_Assigned = lup.user_adid
															left join dbo.lk_user lun with (nolock) on r.NE_ASN_NME = lun.user_adid
															left join dbo.lk_user lut with (nolock) on r.NTE_ASSIGNED = lut.user_adid
															left join dbo.lk_user lus with (nolock) on r.SDE_ASN_NME = lus.user_adid
															left join dbo.CNTCT_DETL cd WITH (NOLOCK) on r.REDSGN_ID = cd.OBJ_ID and cd.OBJ_TYP_CD = ''R''
														'
					   END	
					   
	IF (@SITE_CNTNT_ID=5)
	BEGIN
	IF (@VIEWID NOT IN (16, 19, 20, 22, 38, 60, 65, 71, 117, 118, 119, 120))
		SET @TBLSTR_MDS =  '	MDS_EVENT ev WITH (NOLOCK)
							INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID   
							INNER JOIN	dbo.LK_EVENT_STUS WITH (NOLOCK)	ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND	LK_EVENT_STUS.REC_STUS_ID = 1
							INNER JOIN	dbo.LK_USER WITH (NOLOCK)		ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID 
							INNER JOIN dbo.MDS_EVENT_NTWK_ACTY mena WITH (NOLOCK) ON mena.EVENT_ID = ev.EVENT_ID
							INNER JOIN	dbo.LK_MDS_NTWK_ACTY_TYPE lmna WITH (NOLOCK) ON mena.NTWK_ACTY_TYPE_ID=lmna.NTWK_ACTY_TYPE_ID
							LEFT JOIN 	dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=7
							LEFT JOIN	dbo.LK_WRKFLW_STUS		WITH (NOLOCK)	ON	ev.WRKFLW_STUS_ID	=	LK_WRKFLW_STUS.WRKFLW_STUS_ID
							LEFT JOIN	dbo.LK_SPRINT_CPE_NCR	WITH (NOLOCK)	ON	ev.SPRINT_CPE_NCR_ID	=	LK_SPRINT_CPE_NCR.SPRINT_CPE_NCR_ID
							LEFT JOIN	dbo.MDS_FAST_TRK_TYPE	WITH (NOLOCK)	ON	ev.MDS_FAST_TRK_TYPE_ID	=	MDS_FAST_TRK_TYPE.MDS_FAST_TRK_TYPE_ID
							LEFT JOIN	dbo.MDS_EVENT_MAC_ACTY  WITH (NOLOCK)	ON  ev.EVENT_ID	=	MDS_EVENT_MAC_ACTY.EVENT_ID
							LEFT JOIN   dbo.LK_EVENT_TYPE_TME_SLOT	lets	WITH (NOLOCK)	ON	ev.TME_SLOT_ID	=	lets.TME_SLOT_ID
																							AND	lets.EVENT_TYPE_ID = 5
							 '
	ELSE IF (@VIEWID IN (16, 19, 20, 22, 38, 60, 65, 71, 84, 85, 86, 87, 117, 118, 119, 120))
		SET @TBLSTR_MDS =  'MDS_EVENT ev WITH (NOLOCK) INNER JOIN dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = ev.EVENT_ID 
		INNER JOIN LK_WRKFLW_STUS WITH (NOLOCK) ON ev.WRKFLW_STUS_ID=LK_WRKFLW_STUS.WRKFLW_STUS_ID 
		INNER JOIN LK_EVENT_STUS WITH (NOLOCK) ON ev.EVENT_STUS_ID=LK_EVENT_STUS.EVENT_STUS_ID AND LK_EVENT_STUS.REC_STUS_ID = 1	
		INNER JOIN LK_USER WITH (NOLOCK) ON ev.CREAT_BY_USER_ID=LK_USER.USER_ID 
		INNER JOIN dbo.MDS_EVENT_NTWK_ACTY mena WITH (NOLOCK) ON mena.EVENT_ID = ev.EVENT_ID
		INNER JOIN	dbo.LK_MDS_NTWK_ACTY_TYPE lmna WITH (NOLOCK) ON mena.NTWK_ACTY_TYPE_ID=lmna.NTWK_ACTY_TYPE_ID
		LEFT JOIN dbo.LK_USER RU WITH (NOLOCK) ON RU.USER_ID = ev.REQOR_USER_ID
		LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=ev.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=7
		LEFT JOIN   dbo.LK_EVENT_TYPE_TME_SLOT	lets	WITH (NOLOCK)	ON	ev.TME_SLOT_ID	=	lets.TME_SLOT_ID
																							AND	lets.EVENT_TYPE_ID=5 '
	END
  
	DECLARE @DSPL_COL_NAMES TABLE (COLNAME VARCHAR(100),COLDESC VARCHAR(100), IDENT INT IDENTITY(1,1))
	  
	   INSERT INTO @DSPL_COL_NAMES
	   SELECT SRC_TBL_COL_NME,DSPL_COL_NME  
		FROM	LK_DSPL_COL lkd with (NOLOCK) INNER JOIN
				DSPL_VW_COL vc WITH (NOLOCK) ON lkd.DSPL_COL_ID=vc.DSPL_COL_ID
		WHERE vc.DSPL_VW_ID=@VIEWID--5
		ORDER BY vc.DSPL_POS_FROM_LEFT_NBR ASC
	
	DECLARE @Ctr INT
	DECLARE @Cnt INT
	DECLARE @COL_NAME VARCHAR(MAX)
	DECLARE @COL_DESC VARCHAR(MAX)
	DECLARE @COL_NAMES VARCHAR(MAX)
	DECLARE @FUNC_COL_NAMES VARCHAR(MAX)
	DECLARE @FUNC_COL_NAME VARCHAR(MAX)
	SET @Ctr = 1
   	
    SET @Cnt = (SELECT COUNT(1) FROM @DSPL_COL_NAMES)

	SET @COL_NAMES = ''

	WHILE (@Cnt >= @Ctr)
	BEGIN
		SET @FUNC_COL_NAME = ''
		SELECT TOP 1 @COL_NAME = COLNAME,@COL_DESC=COLDESC FROM @DSPL_COL_NAMES WHERE IDENT = @Ctr
		
		IF @SITE_CNTNT_ID = 5 --MDS 
			BEGIN
				IF EXISTS
					(
						SELECT	*
							FROM	sys.tables		AS tbl
						INNER JOIN	sys.all_columns AS clmns	ON	clmns.object_id=tbl.object_id
						LEFT OUTER JOIN sys.types	AS usrt		ON	usrt.user_type_id = clmns.user_type_id
						LEFT OUTER JOIN sys.types	AS baset	ON	baset.user_type_id = clmns.system_type_id 
																AND baset.user_type_id = baset.system_type_id
							WHERE	tbl.name IN ('MDS_Event_NEW','FSA_MDS_Event_NEW','MDS_EVENT','CUST_SCRD_DATA') 
							AND		SCHEMA_NAME(tbl.schema_id)=	N'dbo'
							AND		clmns.name = @COL_NAME
							AND		((usrt.name = 'varbinary') OR (baset.name = 'varbinary'))
					)
					BEGIN
						IF @COL_NAME = 'EVENT_TITLE_TXT'
							SET @COL_NAME = ' ISNULL(dbo.decryptBinaryData(csd.' + @COL_NAME + '),ev.EVENT_TITLE_TXT) ' + ' AS ''' +  @COL_DESC + ''''
						ELSE IF @COL_NAME = 'CUST_EMAIL_ADR'
							SET @COL_NAME = ' ISNULL(dbo.decryptBinaryData(csd.' + @COL_NAME + '),'''') ' + ' AS ''' +  @COL_DESC + ''''						
						ELSE 
							SET @COL_NAME = ' CASE WHEN eve.CSG_LVL_ID !=0 THEN ''Private Customer'' ELSE ISNULL(' + @COL_NAME + ','''') END ' + ' AS ''' +  @COL_DESC + ''''	
					END
				ELSE IF (@COL_NAME = 'STRT_TMST')
					BEGIN
						SET @COL_NAME = 'CASE WHEN (ev.MDS_FAST_TRK_TYPE_ID=''S'') THEN CONVERT(VARCHAR,STRT_TMST,23) + '' '' + SubString(Convert(Varchar(20), lets.TME_SLOT_STRT_TME), 1, 5) +'':00'' ELSE CONVERT(VARCHAR,STRT_TMST,20) END AS ''' +  @COL_DESC + ''''
					END
				ELSE IF (@COL_NAME = 'END_TMST')
					BEGIN
						SET @COL_NAME = 'CASE WHEN (ev.MDS_FAST_TRK_TYPE_ID=''S'') THEN CONVERT(VARCHAR,END_TMST,23) + '' '' + SubString(Convert(Varchar(20), lets.TME_SLOT_END_TME), 1, 5) +'':00'' ELSE CONVERT(VARCHAR,END_TMST,20) END AS ''' +  @COL_DESC + ''''
					END
				ELSE IF (@COL_NAME = 'H1_ID')
					BEGIN
						SET @COL_NAME = 'CASE WHEN (mena.NTWK_ACTY_TYPE_ID = 1) THEN ev.H1_ID ELSE ev.NTWK_H1 END AS ''' +  @COL_DESC + ''''
					END
				ELSE
					BEGIN
						IF (CHARINDEX('.', @COL_NAME) = 0) 
							SET @COL_NAME = ' ev.' + @COL_NAME + ' AS ''' +  @COL_DESC + ''''
						ELSE IF ((CHARINDEX('dbo.',@COL_NAME) != 0) AND (CHARINDEX('ev.EVENT_ID',@COL_NAME) != 0))
						BEGIN
							SET @FUNC_COL_NAME = REPLACE(@COL_NAME,'ev.EVENT_ID','EventID') + ' AS ''' +  @COL_DESC + ''''
						END
						ELSE 
							BEGIN
							SET @COL_NAME = @COL_NAME + ' AS ''' +  @COL_DESC + ''''
							END
					END
			END
		ELSE IF @SITE_CNTNT_ID = 18 -- Redesign
			BEGIN
				IF @COL_NAME = 'DeviceName'
					BEGIN
						SET @COL_NAME = '( SELECT Top 10 DEV_NME + '','' FROM dbo.REDSGN_DEVICES_INFO rdi 
											WITH (NOLOCK) WHERE rdi.REC_STUS_ID = 1 AND rdi.REDSGN_ID = r.REDSGN_ID FOR XML PATH('''') )' + ' AS ''' +  @COL_DESC + '''' 
					END
				ELSE IF @COL_NAME IN ('CUST_NME')
					SET @COL_NAME = ' 
							CASE 
								WHEN r.CSG_LVL_ID = 0 THEN ISNULL(r.' + @COL_NAME + ','''')
								WHEN ' + @CSG_LVL_ID + ' > 0 AND ' + @CSG_LVL_ID + ' <= r.CSG_LVL_ID THEN dbo.decryptBinaryData(csd.' + @COL_NAME + ')
								ELSE ''''
							END AS ''' +  @COL_DESC + ''''
				ELSE  IF ((CHARINDEX('dbo.',@COL_NAME) != 0) AND (CHARINDEX('ev.EVENT_ID',@COL_NAME) != 0))
							SET @FUNC_COL_NAME = REPLACE(@COL_NAME,'ev.EVENT_ID','EventID') + ' AS ''' +  @COL_DESC + ''''
				ELSE
					BEGIN
						SET @COL_NAME = @COL_NAME + ' AS ''' +  @COL_DESC + ''''
					END
			END
		ELSE
			BEGIN
				IF (@SITE_CNTNT_ID!=9) AND @COL_NAME IN ('EVENT_TITLE_TXT', 'CUST_NME', 'CUST_CNTCT_NME', 'CUST_CNTCT_PHN_NBR', 'CUST_EMAIL_ADR', 'CUST_CNTCT_CELL_PHN_NBR', 'CUST_CNTCT_PGR_NBR', 'CUST_CNTCT_PGR_PIN_NBR')
					SET @COL_NAME = ' ISNULL(dbo.decryptBinaryData(csd.' + @COL_NAME + '),'''') ' + ' AS ''' +  @COL_DESC + ''''
				ELSE IF (@SITE_CNTNT_ID=9) AND @COL_NAME IN ('EVENT_TITLE_TXT')
					SET @COL_NAME = ' ISNULL(dbo.decryptBinaryData(dbo.FEDLINE_EVENT_USER_DATA.' + @COL_NAME + '),'''') ' + ' AS ''' +  @COL_DESC + ''''
				ELSE IF (@SITE_CNTNT_ID=9) AND @COL_NAME IN ('CUST_NME')
					SET @COL_NAME = ' ISNULL(dbo.decryptBinaryData(ev.' + @COL_NAME + '),'''') ' + ' AS ''' +  @COL_DESC + ''''
				ELSE IF ((CHARINDEX('dbo.',@COL_NAME) != 0) AND (CHARINDEX('ev.EVENT_ID',@COL_NAME) != 0))
							SET @FUNC_COL_NAME = REPLACE(@COL_NAME,'ev.EVENT_ID','EventID') + ' AS ''' +  @COL_DESC + ''''
				ELSE
					SET @COL_NAME = @COL_NAME + ' AS ''' +  @COL_DESC + ''''
			END			
			
			IF (ISNULL(@FUNC_COL_NAME,'') = '')
			BEGIN
				IF @COL_NAMES<>'' 
					BEGIN
						SET @COL_NAMES= @COL_NAMES + ',' + @COL_NAME
					END
				ELSE
					BEGIN
						SET @COL_NAMES= @COL_NAME
					END
			END
			ELSE 
			BEGIN
				IF @FUNC_COL_NAMES<>'' 
					BEGIN
						SET @FUNC_COL_NAMES= @FUNC_COL_NAMES + ',' + @FUNC_COL_NAME
					END
				ELSE
					BEGIN
						SET @FUNC_COL_NAMES= @FUNC_COL_NAME
					END
			END
		
		SET @Ctr=@Ctr + 1
	END

	IF @COL_NAMES <> ''
		IF @SITE_CNTNT_ID != 18
			SET @COL_NAMES = ',' + @COL_NAMES
			--SELECT @COL_NAMES
	IF @FUNC_COL_NAMES <> ''
		IF @SITE_CNTNT_ID != 18
			SET @FUNC_COL_NAMES = ',' + @FUNC_COL_NAMES
			--SELECT @FUNC_COL_NAMES

	IF (@SITE_CNTNT_ID	=	9)
		SET @COL_NAMES = @COL_NAMES + ', (SELECT REQ_RECV_DT FROM dbo.FEDLINE_EVENT_TADPOLE_DATA WITH (NOLOCK) WHERE EVENT_ID = ev.EVENT_ID AND ORDR_TYPE_CD IN (''NCI'',''DCS'')) AS ''Fedline Order Submission Date'''
	
	IF ISNULL(@SORTBYCOL1,'') <> ''
		BEGIN 	
			SELECT @SORTCOL1NAME=SRC_TBL_COL_NME  
				FROM	LK_DSPL_COL with (NOLOCK) WHERE DSPL_COL_ID=@SORTBYCOL1
				
			SELECT @SORTCOL2NAME=SRC_TBL_COL_NME  
				FROM	LK_DSPL_COL with (NOLOCK) WHERE DSPL_COL_ID=@SORTBYCOL2	

		   IF (@SORTCOL1NAME IN ('CUST_NME', 'EVENT_TITLE_TXT'))
		   BEGIN
				IF ((@SITE_CNTNT_ID=9) AND (@SORTCOL1NAME='CUST_NME'))
					SET @SORTCOL1NAME = ' ISNULL(dbo.decryptBinaryData(ev.' + @SORTCOL1NAME + '),'''') '
				ELSE IF ((@SITE_CNTNT_ID=9) AND (@SORTCOL1NAME='EVENT_TITLE_TXT'))
					SET @SORTCOL1NAME = ' ISNULL(dbo.decryptBinaryData(dbo.FEDLINE_EVENT_USER_DATA.' + @SORTCOL1NAME + '),'''') '
				ELSE IF (@SITE_CNTNT_ID=18)
					SET @SORTCOL1NAME = ' ISNULL(r.' + @SORTCOL1NAME + ','''') '
				ELSE 
					SET @SORTCOL1NAME = 'ev.'+@SORTCOL1NAME
		   END
		   IF (@SORTCOL2NAME IN ('CUST_NME', 'EVENT_TITLE_TXT'))
		   BEGIN
				IF ((@SITE_CNTNT_ID=9) AND (@SORTCOL2NAME='CUST_NME'))
					SET @SORTCOL2NAME = ' ISNULL(dbo.decryptBinaryData(ev.' + @SORTCOL2NAME + '),'''') '
				ELSE IF ((@SITE_CNTNT_ID=9) AND (@SORTCOL2NAME='EVENT_TITLE_TXT'))
					SET @SORTCOL2NAME = ' ISNULL(dbo.decryptBinaryData(dbo.FEDLINE_EVENT_USER_DATA.' + @SORTCOL2NAME + '),'''') '
				ELSE IF (@SITE_CNTNT_ID=18)
					SET @SORTCOL2NAME = ' ISNULL(r.' + @SORTCOL2NAME + ','''') '
				ELSE 
					SET @SORTCOL2NAME = 'ev.'+@SORTCOL2NAME
		   END
				
			SET @SORTORDER1 =CASE @SORTBYCOL1ORDER  WHEN 'Y' THEN ' ASC' ELSE ' DESC' END	
			SET @SORTORDER2 =CASE @SORTBYCOL2ORDER  WHEN 'Y' THEN ' ASC' ELSE ' DESC' END	
			
			IF @SORTCOL1NAME<> @SORTCOL2NAME	
				BEGIN
					SET @SORTSTR= @SORTSTR + ' SORT1 ' + @SORTORDER1 + ',' + ' SORT2 '+ @SORTORDER2
					SET @QueryString= @QueryString + @COL_NAMES + ',' + @SORTCOL1NAME + ' AS SORT1, ' + @SORTCOL2NAME + ' AS SORT2' + ' FROM ' + @TBLSTR
					IF (@SITE_CNTNT_ID=5)
					SET @QueryString_MDS= @QueryString_MDS + @COL_NAMES + ',' + @SORTCOL1NAME + ' AS SORT1, ' + @SORTCOL2NAME + ' AS SORT2' + ' FROM ' + @TBLSTR_MDS
				END
			ELSE
				BEGIN
					SET @SORTSTR= @SORTSTR + ' SORT1 ' + @SORTORDER1
					SET @QueryString= @QueryString + @COL_NAMES + ',' + @SORTCOL1NAME + ' as SORT1 FROM ' + @TBLSTR
					IF (@SITE_CNTNT_ID=5)
					SET @QueryString_MDS= @QueryString_MDS + @COL_NAMES + ',' + @SORTCOL1NAME + ' as SORT1 FROM ' + @TBLSTR_MDS
				END
		END
	ELSE
		BEGIN			
			SET @QueryString= @QueryString + @COL_NAMES + ' FROM ' + @TBLSTR
			IF (@SITE_CNTNT_ID=5)
			SET @QueryString_MDS= @QueryString_MDS + @COL_NAMES + ' FROM ' + @TBLSTR_MDS
		END	
	
	IF @FLTRFLAG='Y'
		BEGIN
			
			SELECT @FLTRCOL1NAME=SRC_TBL_COL_NME  
				FROM	LK_DSPL_COL with (NOLOCK) WHERE DSPL_COL_ID=@FLTRCOL1
				
			SELECT @FLTRCOL2NAME=SRC_TBL_COL_NME  
				FROM	LK_DSPL_COL with (NOLOCK) WHERE DSPL_COL_ID=@FLTRCOL2	
			
			IF COALESCE(@FLTRCOL1NAME,'') <> '' AND ISNULL(@FLTRCOLTXT1VAL,'') <> ''
				BEGIN
					--SET @FLTRSTR = @FLTRSTR + @TBLNAME + '.' + @FLTRCOL1NAME --+ @FLTRCOLOPR1 + '''' +@FLTRCOLTXT1VAL + '''' 					
					IF NOT(@SITE_CNTNT_ID = 9 AND @FLTRCOL1 = 46) AND @FLTRCOLOPR1 NOT IN ('b','c')	--b(egins with) and c(ontains) need to use charindex, so don't lead off the where with column name.
						BEGIN
							IF (CHARINDEX('.', @FLTRCOL1NAME) = 0) 
								BEGIN
									IF (@FLTRCOL1NAME = 'CREAT_BY_USER_ID')
										SET @FLTRSTR = @FLTRSTR + '('+ @TBLNAME + '.' + @FLTRCOL1NAME
									ELSE
									BEGIN
										SET @MyViewFilter = @MyViewFilter + @TBLNAME + '.' + @FLTRCOL1NAME
										SET @FLTRSTR = @FLTRSTR + @TBLNAME + '.' + @FLTRCOL1NAME
									END
								END
								ELSE
								BEGIN
									SET @MyViewFilter = @MyViewFilter + @FLTRCOL1NAME
									SET @FLTRSTR = @FLTRSTR + @FLTRCOL1NAME
								END
						END
					--print @TBLSTR
					
					IF @FLTRCOLTXT1VAL = '$USER_ID$' AND @SITE_CNTNT_ID != 9
						SET @FLTRSTR = @FLTRSTR + ' = ' + CONVERT(VARCHAR(10), @USER_ID) --+ ''' OR ([dbo].[CheckEventAssignee] (ev.EVENT_ID,' + CONVERT(VARCHAR(10), @USER_ID) + ') = 1) OR  ([dbo].[CheckEventReviewer] (ev.EVENT_ID,' + CONVERT(VARCHAR(10), @USER_ID) + ') = 1))'
					ELSE 
					IF @FLTRCOLTXT1VAL = '$USER_ID$' AND @SITE_CNTNT_ID = 9
						SET @FLTRSTR = @FLTRSTR + ' (([dbo].[CheckEventAssignee] (FEDLINE_EVENT_USER_DATA.EVENT_ID,' + CONVERT(VARCHAR(10), @USER_ID) + ') = 1) OR  ([dbo].[CheckEventReviewer] (FEDLINE_EVENT_USER_DATA.EVENT_ID,' + CONVERT(VARCHAR(10), @USER_ID) + ') = 1))'
					ELSE
						BEGIN
							IF	@FLTRCOLOPR1 NOT IN (' IN', 'b', 'c', '>=', '<=')
							BEGIN
								SET @MyViewFilter = @MyViewFilter + @FLTRCOLOPR1 + '''' +@FLTRCOLTXT1VAL + ''''
								SET @FLTRSTR = @FLTRSTR + @FLTRCOLOPR1 + '''' +@FLTRCOLTXT1VAL + '''' 
							END
							ELSE IF @FLTRCOLOPR1 = 'b'	--Begins with...
							BEGIN
								IF (CHARINDEX('.', @FLTRCOL1NAME) = 0)
								BEGIN
									SET @MyViewFilter = @MyViewFilter + 'CHARINDEX('''+ @FLTRCOLTXT1VAL + ''', ' + @TBLNAME + '.' + @FLTRCOL1NAME + ') = 1 '
									SET @FLTRSTR = @FLTRSTR + 'CHARINDEX('''+ @FLTRCOLTXT1VAL + ''', ' + @TBLNAME + '.' + @FLTRCOL1NAME + ') = 1 '
								END
								ELSE
								BEGIN
									SET @MyViewFilter = @MyViewFilter + 'CHARINDEX('''+ @FLTRCOLTXT1VAL + ''', ' + @FLTRCOL1NAME + ') = 1 '
									SET @FLTRSTR = @FLTRSTR + 'CHARINDEX('''+ @FLTRCOLTXT1VAL + ''', ' + @FLTRCOL1NAME + ') = 1 '
								END
							END
							ELSE IF @FLTRCOLOPR1 = 'c'	--Contains...
							BEGIN
								IF (CHARINDEX('.', @FLTRCOL1NAME) = 0)
								BEGIN
									SET @MyViewFilter = @MyViewFilter + 'CHARINDEX('''+ @FLTRCOLTXT1VAL + ''', ' + @TBLNAME + '.' + @FLTRCOL1NAME + ') = 1 '
									SET @FLTRSTR = @FLTRSTR + 'CHARINDEX('''+ @FLTRCOLTXT1VAL + ''', ' + @TBLNAME + '.' + @FLTRCOL1NAME + ') = 1 '
								END
								ELSE
								BEGIN
									SET @MyViewFilter = @MyViewFilter + 'CHARINDEX('''+ @FLTRCOLTXT1VAL + ''', ' + @FLTRCOL1NAME + ') >= 1 '
									SET @FLTRSTR = @FLTRSTR + 'CHARINDEX('''+ @FLTRCOLTXT1VAL + ''', ' + @FLTRCOL1NAME + ') >= 1 '
								END
							END
							ELSE
							BEGIN
								SET @MyViewFilter = @MyViewFilter + @FLTRCOLOPR1 + '' +@FLTRCOLTXT1VAL + '' 	
								SET @FLTRSTR = @FLTRSTR + @FLTRCOLOPR1 + '' +@FLTRCOLTXT1VAL + '' 	
							END
						END					
				END
				--print @FLTRSTR
			IF COALESCE(@FLTRCOL2NAME,'') <> '' AND ISNULL(@FLTRCOLTXT2VAL,'') <> ''
				BEGIN
					IF @FLTRSTR <> ' WHERE '
					BEGIN
						SET @FLTRSTR = @FLTRSTR + ' ' + @LOGICOPR + ' ' -- + @TBLNAME + '.' + @FLTRCOL2NAME --+ @FLTRCOLOPR2 + '''' +@FLTRCOLTXT2VAL + '''' 	
						SET @MyViewFilter = @MyViewFilter + ' ' + @LOGICOPR + ' '
					END

					IF NOT(@SITE_CNTNT_ID = 9 AND @FLTRCOL2 = 46) AND @FLTRCOLOPR2 NOT IN ('b','c')
						BEGIN
							IF (CHARINDEX('.', @FLTRCOL2NAME) = 0) 
								BEGIN
									IF (@FLTRCOL2NAME = 'CREAT_BY_USER_ID')
										SET @FLTRSTR = @FLTRSTR + '('+ @TBLNAME + '.' + @FLTRCOL2NAME
									ELSE
									BEGIN
										SET @MyViewFilter = @MyViewFilter + @TBLNAME + '.' + @FLTRCOL2NAME
										SET @FLTRSTR = @FLTRSTR + @TBLNAME + '.' + @FLTRCOL2NAME
									END
								END
								ELSE
								BEGIN
									SET @MyViewFilter = @MyViewFilter + @FLTRCOL2NAME
									SET @FLTRSTR = @FLTRSTR + @FLTRCOL2NAME
								END
						END	
					
					IF @FLTRCOLTXT2VAL = '$USER_ID$' AND @SITE_CNTNT_ID != 9
						SET @FLTRSTR = @FLTRSTR + ' = ' + CONVERT(VARCHAR(10), @USER_ID) --+ ''' OR ([dbo].[CheckEventAssignee] (ev.EVENT_ID,' + CONVERT(VARCHAR(10), @USER_ID) + ') = 1) OR  ([dbo].[CheckEventReviewer] (ev.EVENT_ID,' + CONVERT(VARCHAR(10), @USER_ID) + ') = 1))'
					ELSE 
					IF @FLTRCOLTXT2VAL = '$USER_ID$' AND @SITE_CNTNT_ID = 9
						SET @FLTRSTR = @FLTRSTR + ' (([dbo].[CheckEventAssignee] (FEDLINE_EVENT_USER_DATA.EVENT_ID,' + CONVERT(VARCHAR(10), @USER_ID) + ') = 1) OR  ([dbo].[CheckEventReviewer] (FEDLINE_EVENT_USER_DATA.EVENT_ID,' + CONVERT(VARCHAR(10), @USER_ID) + ') = 1))'
					ELSE
						BEGIN
							IF	@FLTRCOLOPR2 NOT IN (' IN', 'b', 'c', '>=', '<=')
							BEGIN
								SET @MyViewFilter = @MyViewFilter + @FLTRCOLOPR2 + '''' +@FLTRCOLTXT2VAL + ''''
								SET @FLTRSTR = @FLTRSTR + @FLTRCOLOPR2 + '''' +@FLTRCOLTXT2VAL + ''''
							END
							ELSE IF @FLTRCOLOPR2 = 'b'	--Begins with...
							BEGIN
								IF (CHARINDEX('.', @FLTRCOL2NAME) = 0)
								BEGIN
									SET @MyViewFilter = @MyViewFilter + 'CHARINDEX('''+ @FLTRCOLTXT2VAL + ''', ' + @TBLNAME + '.' + @FLTRCOL2NAME + ') = 1 '
									SET @FLTRSTR = @FLTRSTR + 'CHARINDEX('''+ @FLTRCOLTXT2VAL + ''', ' + @TBLNAME + '.' + @FLTRCOL2NAME + ') = 1 '
								END
								ELSE
								BEGIN
									SET @MyViewFilter = @MyViewFilter + 'CHARINDEX('''+ @FLTRCOLTXT2VAL + ''', ' + @FLTRCOL2NAME + ') = 1 '
									SET @FLTRSTR = @FLTRSTR + 'CHARINDEX('''+ @FLTRCOLTXT2VAL + ''', ' + @FLTRCOL2NAME + ') = 1 '
								END
							END
							ELSE IF @FLTRCOLOPR2 = 'c'	--Contains...
							BEGIN
								IF (CHARINDEX('.', @FLTRCOL2NAME) = 0)
								BEGIN
									SET @MyViewFilter = @MyViewFilter + 'CHARINDEX('''+ @FLTRCOLTXT2VAL + ''', ' + @TBLNAME + '.' + @FLTRCOL2NAME + ') = 1 '
									SET @FLTRSTR = @FLTRSTR + 'CHARINDEX('''+ @FLTRCOLTXT2VAL + ''', ' + @TBLNAME + '.' + @FLTRCOL2NAME + ') = 1 '
								END
								ELSE
								BEGIN
									SET @MyViewFilter = @MyViewFilter + 'CHARINDEX('''+ @FLTRCOLTXT2VAL + ''', ' + @FLTRCOL2NAME + ') >= 1 '
									SET @FLTRSTR = @FLTRSTR + 'CHARINDEX('''+ @FLTRCOLTXT2VAL + ''', ' + @FLTRCOL2NAME + ') >= 1 '
								END
							END	
							ELSE
							BEGIN
								SET @MyViewFilter = @MyViewFilter + @FLTRCOLOPR2 + '' +@FLTRCOLTXT2VAL + '' 	
								SET @FLTRSTR = @FLTRSTR + @FLTRCOLOPR2 + '' +@FLTRCOLTXT2VAL + '' 	
							END
						END
				END
			
			
			IF	@DSPL_VW_ID	=	44 
				BEGIN
					SET	@FLTRSTR	=	@FLTRSTR +	' AND Convert(Date, ev.STRT_TMST) = Convert(Date, getDate())'
				END
			ELSE IF	@DSPL_VW_ID	IN	(41, 42, 43)
				BEGIN
					SET	@FLTRSTR	=	@FLTRSTR +	' AND mena.NTWK_ACTY_TYPE_ID != 4'
				END
			ELSE IF	@DSPL_VW_ID	=	643 
				BEGIN
					SET	@FLTRSTR	=	@FLTRSTR +	' AND mena.NTWK_ACTY_TYPE_ID = 4'
				END
			ELSE IF ((@NewMDS!='1') AND (@DSPL_VW_ID	=	45))
				BEGIN
					SET	@FLTRSTR	=	@FLTRSTR +	' AND ev.MDS_FAST_TRK_TYPE_ID IS NOT NULL AND ev.MDS_ACTY_TYPE_ID != 3'
				END
			ELSE IF ((@NewMDS='1') AND (@DSPL_VW_ID	=	45))
				BEGIN
					SET	@FLTRSTR	=	@FLTRSTR +	' AND ISNULL(ev.MDS_FAST_TRK_TYPE_ID,'''') != '''' AND ev.MDS_ACTY_TYPE_ID != 3'
				END
			ELSE IF @DSPL_VW_ID =	83
				BEGIN
					SET	@FLTRSTR	=	@FLTRSTR +	' Convert(Date, STRT_TME) = Convert(Date, getDate())'
				END
			ELSE IF @DSPL_VW_ID IN (84,85)
			BEGIN
					SET	@FLTRSTR	=	@FLTRSTR +	' AND (MDS_ACTY_TYPE_ID IN (4,5) OR (MDS_EVENT_MAC_ACTY.MDS_MAC_ACTY_ID = 19))'
			END
			ELSE IF @DSPL_VW_ID IN (86, 87)
			BEGIN
					SET	@FLTRSTR	=	@FLTRSTR +	' AND ((MDS_ACTY_TYPE_ID = 6) OR (MDS_EVENT_MAC_ACTY.MDS_MAC_ACTY_ID = 19))'
			END
			--Redesign changes
			ELSE IF @DSPL_VW_ID  IN (512, 513, 514, 515, 516, 517, 518, 519)
			BEGIN
					SET	@FLTRSTR	=	@FLTRSTR +	' AND (PM_Assigned = '''+ @REDSGN_USER_ADID +''' OR NE_ASN_NME = '''+ @REDSGN_USER_ADID +''' OR NTE_ASSIGNED  = '''+ @REDSGN_USER_ADID +''' OR SDE_ASN_NME = '''+ @REDSGN_USER_ADID +''' OR r.CRETD_BY_CD = '+ CONVERT(VARCHAR(10), @USER_ID) +' OR r.MODFD_BY_CD = '+ CONVERT(VARCHAR(10), @USER_ID) +' OR cd.EMAIL_ADR = ''' + @REDSGN_USER_EMAIL +''')'
			END
			
			IF ((@FLTRCOLTXT2VAL = '$USER_ID$') OR (@FLTRCOLTXT1VAL = '$USER_ID$')) AND @SITE_CNTNT_ID != 9
			BEGIN
				IF (@SITE_CNTNT_ID = 5)
				BEGIN
					IF @DSPL_VW_ID IN (84,85)
					SET @QueryString=@QueryString + @FLTRSTR + ') UNION ' + @QueryString + @MyViewFilter1 + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter + ' UNION ' + @QueryString + @MyViewFilter2 + + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter + ' UNION ' + @QueryString + @MyViewFilter3 + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter
					ELSE IF @DSPL_VW_ID IN (86,87)
					SET @QueryString=@QueryString + @FLTRSTR + ') UNION ' + @QueryString + @MyViewFilter1 + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter + ' UNION ' + @QueryString + @MyViewFilter2 + + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter + ' UNION ' + @QueryString + @MyViewFilter3 + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter
					ELSE
					SET @QueryString=@QueryString + @FLTRSTR + ') UNION ' + @QueryString + @MyViewFilter1 + @MyViewFilter + ' UNION ' + @QueryString + @MyViewFilter2 + @MyViewFilter + ' UNION ' + @QueryString + @MyViewFilter3 + @MyViewFilter
					
					IF (@SITE_CNTNT_ID=5)
					BEGIN
						IF @DSPL_VW_ID IN (84,85)
						SET @QueryString_MDS=@QueryString_MDS + @FLTRSTR + ') UNION ' + @QueryString_MDS + @MyViewFilter1 + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter + ' UNION ' + @QueryString_MDS + @MyViewFilter2 + + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter + ' UNION ' + @QueryString_MDS + @MyViewFilter3 + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter
						ELSE IF @DSPL_VW_ID IN (86,87)
						SET @QueryString_MDS=@QueryString_MDS + @FLTRSTR + ') UNION ' + @QueryString_MDS + @MyViewFilter1 + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter + ' UNION ' + @QueryString_MDS + @MyViewFilter2 + + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter + ' UNION ' + @QueryString_MDS + @MyViewFilter3 + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter
						ELSE
						SET @QueryString_MDS=@QueryString_MDS + @FLTRSTR + ') UNION ' + @QueryString_MDS + @MyViewFilter1 + @MyViewFilter + ' UNION ' + @QueryString_MDS + @MyViewFilter2 + @MyViewFilter + ' UNION ' + @QueryString_MDS + @MyViewFilter3 + @MyViewFilter
					END
				END
				ELSE
				BEGIN
					IF @DSPL_VW_ID IN (84,85)
					SET @QueryString=@QueryString + @FLTRSTR + ') UNION ' + @QueryString + @MyViewFilter1 + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter + ' UNION ' + @QueryString + @MyViewFilter2 + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter
					ELSE IF @DSPL_VW_ID IN (86,87)
					SET @QueryString=@QueryString + @FLTRSTR + ') UNION ' + @QueryString + @MyViewFilter1 + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter + ' UNION ' + @QueryString + @MyViewFilter2 + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter
					ELSE
					SET @QueryString=@QueryString + @FLTRSTR + ') UNION ' + @QueryString + @MyViewFilter1 + @MyViewFilter + ' UNION ' + @QueryString + @MyViewFilter2 + @MyViewFilter
					
					IF (@SITE_CNTNT_ID=5)
					BEGIN
						IF @DSPL_VW_ID IN (84,85)
						SET @QueryString_MDS=@QueryString_MDS + @FLTRSTR + ') UNION ' + @QueryString_MDS + @MyViewFilter1 + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter + ' UNION ' + @QueryString_MDS + @MyViewFilter2 + ' AND (MDS_ACTY_TYPE_ID IN (4,5)) ' + @MyViewFilter
						ELSE IF @DSPL_VW_ID IN (86,87)
						SET @QueryString_MDS=@QueryString_MDS + @FLTRSTR + ') UNION ' + @QueryString_MDS + @MyViewFilter1 + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter + ' UNION ' + @QueryString_MDS + @MyViewFilter2 + ' AND (MDS_ACTY_TYPE_ID IN (6)) ' + @MyViewFilter
						ELSE
						SET @QueryString_MDS=@QueryString_MDS + @FLTRSTR + ') UNION ' + @QueryString_MDS + @MyViewFilter1 + @MyViewFilter + ' UNION ' + @QueryString_MDS + @MyViewFilter2 + @MyViewFilter
					END
				END
			END
			ELSE
			BEGIN
				SET @QueryString=@QueryString + @FLTRSTR
				IF (@SITE_CNTNT_ID=5)
				SET @QueryString_MDS=@QueryString_MDS + @FLTRSTR
			END
		END
	/*
			SELECT @SORTCOL1NAME=SRC_TBL_COL_NME  
				FROM	LK_DSPL_COL with (NOLOCK) WHERE DSPL_COL_ID=@SORTBYCOL1
				
			SELECT @SORTCOL2NAME=SRC_TBL_COL_NME  
				FROM	LK_DSPL_COL with (NOLOCK) WHERE DSPL_COL_ID=@SORTBYCOL2	
				
				
			SET @SORTORDER1 =CASE @SORTBYCOL1ORDER  WHEN 'Y' THEN ' ASC' ELSE ' DESC' END	
			SET @SORTORDER2 =CASE @SORTBYCOL2ORDER  WHEN 'Y' THEN ' ASC' ELSE ' DESC' END	
			
			IF @SORTCOL1NAME<> @SORTCOL2NAME	
				BEGIN
					SET @SORTSTR= @SORTSTR + @SORTCOL1NAME + @SORTORDER1 + ',' + + @SORTCOL2NAME + @SORTORDER2
				END
			ELSE
				BEGIN
					SET @SORTSTR= @SORTSTR + @SORTCOL1NAME + @SORTORDER1
				END	
		

SET @QueryString = REPLACE(@QueryString, 'dbo.getBooleanString(ev.PRE_CFG_CMPLT_CD)', 'CASE WHEN ev.PRE_CFG_CMPLT_CD IS NULL THEN '''' ELSE dbo.getBooleanString(ev.PRE_CFG_CMPLT_CD) END')			
SET @QueryString_MDS = REPLACE(@QueryString_MDS, 'dbo.getBooleanString(ev.PRE_CFG_CMPLT_CD)', 'CASE WHEN ev.PRE_CFG_CMPLT_CD IS NULL THEN '''' ELSE dbo.getBooleanString(ev.PRE_CFG_CMPLT_CD) END')	*/		
				
	IF ((@FLTRCOLTXT2VAL = '$USER_ID$') OR (@FLTRCOLTXT1VAL = '$USER_ID$')) AND @SITE_CNTNT_ID != 9
		BEGIN
			IF @SITE_CNTNT_ID = 18
					BEGIN
						SET @QueryString = 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + @QueryString + ' AND ((r.CSG_LVL_ID IS NULL) OR (r.CSG_LVL_ID = 0) OR (r.CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+')) ORDER BY r.REDSGN_ID DESC) as RESULT ' + @MyViewSORT
					END
				ELSE 
					BEGIN
						IF (@SITE_CNTNT_ID = 5)
						BEGIN
							IF (@NewMDS='1')
								SET @QueryString= 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + REPLACE(REPLACE(@QueryString_MDS, 'FTN AS', ''''' AS'), 'H1_ID', 'H1') + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @MyViewSORT
							ELSE IF (@NewMDS='0')
								SET @QueryString= 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + REPLACE(@QueryString, 'CUST_ACCT_TEAM_PDL_NME', 'dbo.decryptBinaryData(csd.CUST_EMAIL_ADR)') + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @MyViewSORT
							ELSE 
								SET @QueryString= 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + @QueryString + ' UNION ' + REPLACE(REPLACE(@QueryString_MDS, 'FTN AS', ''''' AS'), 'H1_ID', 'H1') + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @MyViewSORT
						END
						ELSE IF (@SITE_CNTNT_ID = 19)
						SET @QueryString= 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + REPLACE(REPLACE(@QueryString, 'FTN AS', ''''' AS'), 'H1_ID', 'H1')  + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @MyViewSORT
						ELSE
						SET @QueryString= 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + @QueryString + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @MyViewSORT
					END
		END
	ELSE
		BEGIN
			IF @SORTSTR <> ''		
			BEGIN
				IF @SITE_CNTNT_ID = 18
					BEGIN
						SET @QueryString = 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + @QueryString + ' AND ((r.CSG_LVL_ID IS NULL) OR (r.CSG_LVL_ID = 0) OR (r.CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+')) ORDER BY r.REDSGN_ID DESC) as RESULT ' + @SORTSTR
					END
				ELSE 
					BEGIN
					IF (@SITE_CNTNT_ID = 5)
					BEGIN
						IF (@NewMDS='1')
							SET @QueryString= 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + REPLACE(REPLACE(@QueryString_MDS, 'FTN AS', ''''' AS'), 'H1_ID', 'H1') + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @MyViewSORT
						ELSE IF (@NewMDS='0')
							SET @QueryString= 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + REPLACE(@QueryString, 'CUST_ACCT_TEAM_PDL_NME', 'dbo.decryptBinaryData(csd.CUST_EMAIL_ADR)') + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @MyViewSORT
						ELSE
							SET @QueryString = 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + @QueryString + ' UNION ' + REPLACE(REPLACE(@QueryString_MDS, 'FTN AS', ''''' AS'), 'H1_ID', 'H1') + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @SORTSTR
					END
					ELSE IF (@SITE_CNTNT_ID = 19)
						SET @QueryString= 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + REPLACE(REPLACE(@QueryString, 'FTN AS', ''''' AS'), 'H1_ID', 'H1')  + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @SORTSTR
					ELSE
						SET @QueryString = 'Select Top 500 * '+ CASE WHEN (ISNULL(@FUNC_COL_NAMES,'') <> '') THEN @FUNC_COL_NAMES ELSE '' END + ' from (' + @QueryString + ' ORDER BY ev.EVENT_ID DESC) as RESULT WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ' + @SORTSTR
					END
			END
		END
				
--  web.getViewDetails_TEST 60, 5, 1
-- web.getViewDetails 60,5,1
	--select @QueryString
	EXEC sp_executesql @QueryString
	
 END TRY  
  
 BEGIN CATCH  
	EXEC [dbo].[insertErrorInfo]  
 END CATCH  
  
END