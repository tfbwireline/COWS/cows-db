USE [COWS]
GO
_CreateObject 'SP','dbo','getNoEndRecAppts'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 ================================================================================================  
 Author:  jrg7298  
 Create date: 4/29/2013  
 Description: Get the List of recurring appointments with no end date.
 ================================================================================================  
*/

ALTER PROCEDURE [dbo].[getNoEndRecAppts] 
 AS
BEGIN  
 
 SET NOCOUNT ON;  
  
 BEGIN TRY  

	-- Gets all noend appts whose creat_dt has passed 30 days, redo the 60 day inserts again
	SELECT DISTINCT apt.APPT_ID
	FROM dbo.APPT apt WITH (NOLOCK)
	INNER JOIN dbo.APPT_RCURNC_TRGR apr WITH (NOLOCK) ON apr.APPT_ID = apt.APPT_ID
	WHERE apt.RCURNC_CD = 1
	  AND apt.RCURNC_DES_TXT IS NOT NULL
	  AND apt.ASN_TO_USER_ID_LIST_TXT IS NOT NULL
	  AND apr.STUS_ID = 2
	  AND (ISNULL(CONVERT(XML,apt.RCURNC_DES_TXT).value('(//@Range)[1]', 'int' ), 0) = 0)
	  AND (DATEDIFF(DD, (SELECT TOP 1 apd.CREAT_DT
					  FROM dbo.APPT_RCURNC_DATA apd WITH (NOLOCK)
					  WHERE apd.APPT_ID = apt.APPT_ID 
						ORDER BY apd.CREAT_DT DESC
					  ), GETDATE()) >= 30)
	  AND NOT EXISTS (SELECT 'X'
					  FROM dbo.APPT_RCURNC_TRGR apr2 WITH (NOLOCK)
					  WHERE apr2.APPT_ID = apt.APPT_ID
					    AND apr2.STUS_ID IN (0,1))
	  AND NOT EXISTS (SELECT 'X'
					  FROM dbo.APPT_RCURNC_TRGR apr3 WITH (NOLOCK)
					  WHERE apr3.APPT_ID = apt.APPT_ID
					    AND apr3.STUS_ID = 2
						AND DATEDIFF(DD, apr3.CREAT_DT, GETDATE()) = 0)
	ORDER BY apt.APPT_ID ASC

 END TRY  
  
 BEGIN CATCH  
	EXEC [dbo].[insertErrorInfo]  
 END CATCH  
  
END