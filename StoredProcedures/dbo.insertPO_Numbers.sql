USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertPO_Numbers]    Script Date: 03/04/2019 11:33:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	David Phillips
-- Create date: 10/28/2015
-- Description:	Retrieves PO Numbers from PeopleSoft/Supply Chain Management.
-- =============================================
ALTER PROCEDURE [dbo].[insertPO_Numbers] 
	
AS
BEGIN TRY

	DECLARE @CMPNT_ID INT,
			@REQ_ID VARCHAR (10),
			@REQ_LINE_NBR INT,
			@PO_ID VARCHAR (15),
			@LINE_NBR INT,
			@PLSFT_RQSTN_NBR VARCHAR(10),
			@NAME1  VARCHAR(50),
			@SQL NVARCHAR(1000),
			@ORDR_ID int,
			@DeviceID varchar (25)
	
	DECLARE @LK_MatReq table (PLSFT_RQSTN_NBR VARCHAR(10))
	
	DECLARE @PS_PO_LINE_DISTRIB TABLE
		(	REQ_ID			Varchar(10),
			REQ_LINE_NBR		INT,
			PO_ID			VARCHAR(15),
			LINE_NBR		INT,
			NAME1        VARCHAR(50)			
		)
	
	INSERT INTO @LK_MatReq (PLSFT_RQSTN_NBR) 
		 (SELECT DISTINCT f.PLSFT_RQSTN_NBR
				 FROM dbo.FSA_ORDR_CPE_LINE_ITEM f WITH (NOLOCK)
				 INNER JOIN dbo.PS_REQ_LINE_ITM_QUEUE r WITH (NOLOCK)
					ON r.CMPNT_ID = f.ORDR_CMPNT_ID 
					and f.PLSFT_RQSTN_NBR = r.REQSTN_NBR
				 INNER JOIN dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID=f.ORDR_ID
				 INNER JOIN dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = f.ORDR_ID
			WHERE ISNULL(f.PLSFT_RQSTN_NBR,'') <> '' AND ISNULL(f.PRCH_ORDR_NBR,'') = ''
				   AND ISNULL(r.REQ_LINE_NBR,0) <> 0
				   AND ord.ORDR_STUS_ID=1
				   AND ord.ORDR_CAT_ID=6 AND f.ITM_STUS=401
				   AND ( ((act.TASK_ID = 602 and act.STUS_ID = 0) or
						((act.TASK_ID = 1000 and act.STUS_ID = 0) and f.DROP_SHP = 'Y') )
						OR
						(ord.DMSTC_CD = 1 AND (act.TASK_ID <> 1001 and act.STUS_ID = 0 )
							  )
				    )
					)
			
		
	WHILE EXISTS (SELECT * FROM @LK_MatReq)
		BEGIN
		  SELECT TOP 1  @PLSFT_RQSTN_NBR = PLSFT_RQSTN_NBR
			 FROM @LK_MatReq
					
			SELECT @SQL = 'SELECT REQ_ID, REQ_LINE_NBR, PO_ID, LINE_NBR, NAME1 FROM OPENQUERY
			(PFNP1011,''SELECT T1.REQ_ID, T1.REQ_LINE_NBR, T1.PO_ID, T1.LINE_NBR, T3.NAME1
			 FROM PS_PO_LINE_DISTRIB T1
			LEFT OUTER JOIN PS_PO_HDR T2 ON T2.PO_ID = T1.PO_ID
            LEFT OUTER JOIN PS_VENDOR T3 on T2.VENDOR_ID = T3.VENDOR_ID
			 WHERE T1.REQ_ID = ''''' + @PLSFT_RQSTN_NBR + ''''''')'
			
			INSERT INTO @PS_PO_LINE_DISTRIB (REQ_ID, REQ_LINE_NBR, PO_ID, LINE_NBR, NAME1)
			EXEC sp_executesql @SQL

			WHILE EXISTS (SELECT * FROM @PS_PO_LINE_DISTRIB p
									INNER JOIN dbo.PS_REQ_LINE_ITM_QUEUE r WITH (NOLOCK)
									ON p.REQ_ID = r.REQSTN_NBR AND p.REQ_LINE_NBR = r.REQ_LINE_NBR ) 
			  BEGIN
				
				SELECT TOP 1 @CMPNT_ID = req.CMPNT_ID, @REQ_ID = po.REQ_ID,
					   @REQ_LINE_NBR = po.REQ_LINE_NBR, @PO_ID = po.PO_ID, @LINE_NBR = po.LINE_NBR,
					   @NAME1 = po.NAME1, @ORDR_ID = req.ORDR_ID, @DeviceID = cli.DEVICE_ID
					FROM @PS_PO_LINE_DISTRIB po 
					INNER JOIN dbo.PS_REQ_LINE_ITM_QUEUE req WITH (NOLOCK)
						ON po.REQ_ID = req.REQSTN_NBR AND po.REQ_LINE_NBR = req.REQ_LINE_NBR
					INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON cli.FSA_CPE_LINE_ITEM_ID = req.FSA_CPE_LINE_ITEM_ID
	
				UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM 
				SET PRCH_ORDR_NBR = @PO_ID, PO_LN_NBR = @LINE_NBR, SUPPLIER = @NAME1 
				WHERE ORDR_CMPNT_ID = @CMPNT_ID AND ITM_STUS = 401
								
				DELETE @PS_PO_LINE_DISTRIB WHERE REQ_ID = @REQ_ID AND REQ_LINE_NBR = @REQ_LINE_NBR
				
				IF NOT EXISTS (SELECT * FROM dbo.M5_ORDR_MSG WITH (NOLOCK) 
							WHERE ORDR_ID = @ORDR_ID AND @DeviceID = DEVICE AND @PO_ID = PRCH_ORDR_NBR)
					BEGIN
						INSERT INTO [dbo].[M5_ORDR_MSG]
							([ORDR_ID],[M5_MSG_ID],[NTE],[MSG], [DEVICE],[CMPNT_ID],[STUS_ID],[CREAT_DT],[SENT_DT],[PRCH_ORDR_NBR])     
							VALUES (@ORDR_ID,5,null,null,@DeviceID,null,10,GETDATE(),NULL,@PO_ID)
					END

			  END
			
			DELETE @PS_PO_LINE_DISTRIB
				
			DELETE @LK_MatReq WHERE @PLSFT_RQSTN_NBR = PLSFT_RQSTN_NBR
			
		END
		
	


	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH



