USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getViews]    Script Date: 04/15/2021 10:11:59 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 ================================================================================================  
 Author:  csb9923  
 Create date: 4/11/2011  
 Description: gets the list of Views based on userid and site content ID 
 --	sbg9814	09/09/2011	Changed this to dynamically extract the Views for Orders.
 -- kh946640 03/07/2019 Updated SP to get profile info from dbo.MAP_USR_PRF and dbo.LK_USR_PRF Tables instead of dbo.USER_GRP_ROLE Table 
 -- kh946640: (05022019) Updated SP adding new paramenter @IsNewUI to support bothe Old and New COWS applications.
 ================================================================================================  
*/

ALTER PROCEDURE [web].[getViews]   --40, 1
	@USERID			Int
	,@SITE_CNTNT_ID	Int
	,@VW_TYP		Int	=	1
	,@GRP_ID		Int	=	0,
	@IsNewUI		BIT = 0
 AS
BEGIN  
 
 SET NOCOUNT ON;  
  
 BEGIN TRY  
	IF	@GRP_ID	!=	0	AND	@SITE_CNTNT_ID	=	0
		BEGIN
			SELECT		@SITE_CNTNT_ID	=	ISNULL(SITE_CNTNT_ID, 0)
				FROM	dbo.LK_SITE_CNTNT	WITH (NOLOCK)
				WHERE	ISNULL(GRP_ID, 0)=	@GRP_ID	
		END

	IF @VW_TYP = 1
		BEGIN
		IF @IsNewUI = 1
			BEGIN
				IF(@SITE_CNTNT_ID = 19)
			BEGIN
				(-- Pull up all personal Events by UserIDs.
				SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
					FROM	dbo.DSPL_VW  WITH (NOLOCK) 
					WHERE	USER_ID			!=	1 
					AND		USER_ID			=	@USERID 
					AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
					AND		DSPL_VW_ID NOT IN (610, 611, 612, 613, 614)
				UNION	
				-- Pull up all Non-MDS Public Events.
				SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
					FROM	dbo.DSPL_VW  WITH (NOLOCK) 
					WHERE	USER_ID			=	1 
					AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
					AND		PBLC_VW_CD		=	'Y'
					AND		@SITE_CNTNT_ID	!=	5 
					AND		DSPL_VW_ID NOT IN (610, 611, 612, 613, 614)
				UNION
				-- Pull up all MDS Public Events for Members.
				SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
					FROM	dbo.DSPL_VW  WITH (NOLOCK) 
					WHERE	USER_ID			=	1 
					AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
					AND		@SITE_CNTNT_ID	=	5 
					AND		PBLC_VW_CD		=	'Y' 
					AND		DSPL_VW_ID		NOT IN	(41, 42, 43, 610, 611, 612, 613, 614, 643)
					AND		NOT EXISTS
							(SELECT 'X' FROM dbo.MAP_USR_PRF mup WITH (NOLOCK) 
							 join dbo.LK_USR_PRF lup WITH (NOLOCK)  on lup.USR_PRF_ID = mup.USR_PRF_ID
							 WHERE mup.REC_STUS_ID = 1 
							    AND mup.USER_ID = @USERID 
							    AND (lup.USR_PRF_NME IN ('MDSEActivator', 'MDSEReviewer')
									OR lup.USR_PRF_NME LIKE '%NCIEReviewer%' OR lup.USR_PRF_NME LIKE '%NCIEActivator%'))
				UNION	
				-- Pull up all MDS Public Events for Non-Members.
				SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
					FROM	dbo.DSPL_VW  WITH (NOLOCK) 
					WHERE	USER_ID		=	1 
					AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
					AND		@SITE_CNTNT_ID	=	5 
					AND		PBLC_VW_CD		=	'Y' 
					AND		DSPL_VW_ID NOT IN (610, 611, 612, 613, 614)
					AND		EXISTS
							(SELECT 'X' FROM dbo.MAP_USR_PRF mup WITH (NOLOCK) 
							 join dbo.LK_USR_PRF lup WITH (NOLOCK)  on lup.USR_PRF_ID = mup.USR_PRF_ID
							 WHERE mup.REC_STUS_ID = 1 
							    AND mup.USER_ID = @USERID 
							    AND (lup.USR_PRF_NME IN ('MDSEActivator', 'MDSEReviewer')
									OR lup.USR_PRF_NME LIKE '%NCIEReviewer%' OR lup.USR_PRF_NME LIKE '%NCIEActivator%'))
				)	ORDER BY	SEQ_NBR, DSPL_VW_NME		
			END
		ELSE
			BEGIN
			(-- Pull up all personal Events by UserIDs.
			SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
				FROM	dbo.DSPL_VW  WITH (NOLOCK) 
				WHERE	USER_ID			!=	1 
				AND		USER_ID			=	@USERID 
				AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
			UNION	
			-- Pull up all Non-MDS Public Events.
			SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
				FROM	dbo.DSPL_VW  WITH (NOLOCK) 
				WHERE	USER_ID			=	1 
				AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
				AND		PBLC_VW_CD		=	'Y'
				AND		@SITE_CNTNT_ID	!=	5 
			UNION
			-- Pull up all MDS Public Events for Members.
			SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
				FROM	dbo.DSPL_VW  WITH (NOLOCK) 
				WHERE	USER_ID			=	1 
				AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
				AND		@SITE_CNTNT_ID	=	5 
				AND		PBLC_VW_CD		=	'Y' 
				AND		DSPL_VW_ID		NOT IN	(41, 42, 43, 643)
				AND		NOT EXISTS
						(SELECT 'X' FROM dbo.MAP_USR_PRF mup WITH (NOLOCK) 
							 join dbo.LK_USR_PRF lup WITH (NOLOCK)  on lup.USR_PRF_ID = mup.USR_PRF_ID
							 WHERE mup.REC_STUS_ID = 1 
							    AND mup.USER_ID = @USERID 
							    AND (lup.USR_PRF_NME IN ('MDSEActivator', 'MDSEReviewer')
									OR lup.USR_PRF_NME LIKE '%NCIEReviewer%' OR lup.USR_PRF_NME LIKE '%NCIEActivator%'))
			UNION	
			-- Pull up all MDS Public Events for Non-Members.
			SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
				FROM	dbo.DSPL_VW  WITH (NOLOCK) 
				WHERE	USER_ID		=	1 
				AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
				AND		@SITE_CNTNT_ID	=	5 
				AND		PBLC_VW_CD		=	'Y' 
				AND		EXISTS
						(SELECT 'X' FROM dbo.MAP_USR_PRF mup WITH (NOLOCK) 
							 join dbo.LK_USR_PRF lup WITH (NOLOCK)  on lup.USR_PRF_ID = mup.USR_PRF_ID
							 WHERE mup.REC_STUS_ID = 1 
							    AND mup.USER_ID = @USERID 
							    AND (lup.USR_PRF_NME IN ('MDSEActivator', 'MDSEReviewer')
									OR lup.USR_PRF_NME LIKE '%NCIEReviewer%' OR lup.USR_PRF_NME LIKE '%NCIEActivator%'))
			)	ORDER BY	SEQ_NBR, DSPL_VW_NME		
			END
			END
		ELSE
			BEGIN
				IF(@SITE_CNTNT_ID = 19)
			BEGIN
				(-- Pull up all personal Events by UserIDs.
				SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
					FROM	dbo.DSPL_VW  WITH (NOLOCK) 
					WHERE	USER_ID			!=	1 
					AND		USER_ID			=	@USERID 
					AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
					AND		DSPL_VW_ID NOT IN (610, 611, 612, 613, 614)
				UNION	
				-- Pull up all Non-MDS Public Events.
				SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
					FROM	dbo.DSPL_VW  WITH (NOLOCK) 
					WHERE	USER_ID			=	1 
					AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
					AND		PBLC_VW_CD		=	'Y'
					AND		@SITE_CNTNT_ID	!=	5 
					AND		DSPL_VW_ID NOT IN (610, 611, 612, 613, 614)
				UNION
				-- Pull up all MDS Public Events for Members.
				SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
					FROM	dbo.DSPL_VW  WITH (NOLOCK) 
					WHERE	USER_ID			=	1 
					AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
					AND		@SITE_CNTNT_ID	=	5 
					AND		PBLC_VW_CD		=	'Y' 
					AND		DSPL_VW_ID		NOT IN	(41, 42, 43, 610, 611, 612, 613, 614, 643)
					AND		NOT EXISTS
							(SELECT 'X' FROM dbo.USER_GRP_ROLE	WITH (NOLOCK)
								WHERE	USER_ID		=	@USERID
									AND	GRP_ID		=	2
									AND	REC_STUS_ID	=	1
									AND	ROLE_ID		IN	(22, 23)
									)
				UNION	
				-- Pull up all MDS Public Events for Non-Members.
				SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
					FROM	dbo.DSPL_VW  WITH (NOLOCK) 
					WHERE	USER_ID		=	1 
					AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
					AND		@SITE_CNTNT_ID	=	5 
					AND		PBLC_VW_CD		=	'Y' 
					AND		DSPL_VW_ID NOT IN (610, 611, 612, 613, 614)
					AND		EXISTS
							(SELECT 'X' FROM dbo.USER_GRP_ROLE	WITH (NOLOCK)
								WHERE	USER_ID		=	@USERID
									AND	GRP_ID		=	2
									AND	REC_STUS_ID	=	1
									AND	ROLE_ID		IN	(22, 23)
									)	
				)	ORDER BY	SEQ_NBR, DSPL_VW_NME		
			END
		ELSE
			BEGIN
			(-- Pull up all personal Events by UserIDs.
			SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
				FROM	dbo.DSPL_VW  WITH (NOLOCK) 
				WHERE	USER_ID			!=	1 
				AND		USER_ID			=	@USERID 
				AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
			UNION	
			-- Pull up all Non-MDS Public Events.
			SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
				FROM	dbo.DSPL_VW  WITH (NOLOCK) 
				WHERE	USER_ID			=	1 
				AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
				AND		PBLC_VW_CD		=	'Y'
				AND		@SITE_CNTNT_ID	!=	5 
			UNION
			-- Pull up all MDS Public Events for Members.
			SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
				FROM	dbo.DSPL_VW  WITH (NOLOCK) 
				WHERE	USER_ID			=	1 
				AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
				AND		@SITE_CNTNT_ID	=	5 
				AND		PBLC_VW_CD		=	'Y' 
				AND		DSPL_VW_ID		NOT IN	(41, 42, 43, 643)
				AND		NOT EXISTS
						(SELECT 'X' FROM dbo.USER_GRP_ROLE	WITH (NOLOCK)
							WHERE	USER_ID		=	@USERID
								AND	GRP_ID		=	2
								AND	REC_STUS_ID	=	1
								AND	ROLE_ID		IN	(22, 23)
								)
			UNION	
			-- Pull up all MDS Public Events for Non-Members.
			SELECT		DSPL_VW_ID,DSPL_VW_NME,PBLC_VW_CD,DFLT_VW_CD,	SEQ_NBR
				FROM	dbo.DSPL_VW  WITH (NOLOCK) 
				WHERE	USER_ID		=	1 
				AND		SITE_CNTNT_ID	=	@SITE_CNTNT_ID
				AND		@SITE_CNTNT_ID	=	5 
				AND		PBLC_VW_CD		=	'Y' 
				AND		EXISTS
						(SELECT 'X' FROM dbo.USER_GRP_ROLE	WITH (NOLOCK)
							WHERE	USER_ID		=	@USERID
								AND	GRP_ID		=	2
								AND	REC_STUS_ID	=	1
								AND	ROLE_ID		IN	(22, 23)
								)	
			)	ORDER BY	SEQ_NBR, DSPL_VW_NME		
			END
			END
			
		END
	ELSE
		BEGIN
			SELECT	101 AS DSPL_VW_ID
					,'Default Calendar View' AS DSPL_VW_NME
					,'Y'	AS	DFLT_VW_CD
					,99		AS	SEQ_NBR
		END

 END TRY  
  
 BEGIN CATCH  
	EXEC [dbo].[insertErrorInfo]  
 END CATCH  
  
END
