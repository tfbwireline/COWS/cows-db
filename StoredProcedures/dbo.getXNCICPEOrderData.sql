USE [COWS]
GO
_CreateObject 'SP','dbo','getXNCICPEOrderData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <09/13/2011>
-- Description:	<To Retrieve xNCI CPE Data>
-- =============================================
--Exec getXNCICPEOrderData 375
ALTER PROCEDURE dbo.getXNCICPEOrderData
	@OrderID INT	
AS
BEGIN
	BEGIN TRY
		SELECT 
			ISNULL(dbo.convertOrderMilestonedates(HDWR_DLVRD_DT),'')	AS HDWR_DLVRD_DT,
			ISNULL(dbo.convertOrderMilestonedates(EQPT_INSTL_DT),'')	AS EQPT_INSTL_DT,
			ISNULL(dbo.convertOrderMilestonedates(CUST_DLVRY_DT),'')	AS CUST_DLVRY_DT,
			ISNULL(SHPMT_SIGN_BY_NME, '')								AS SHPMT_SIGN_BY_NME
		FROM	dbo.FSA_ORDR_GOM_XNCI WITH (NOLOCK)
		WHERE 	ORDR_ID	=	@OrderID
			AND GRP_ID	<>	1
		
	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
	
END
GO
