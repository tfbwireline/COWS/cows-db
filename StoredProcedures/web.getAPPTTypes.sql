USE [COWS]
GO
_CreateObject 'SP','web','getAPPTTypes'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--================================================================================================  
-- Author:  csb9923  
-- Create date: 4/19/2011  
-- Description: Gets the Appointment Types
-- sbg9814 07/06/2011 Add Resource names to this list.  
-- sbg9814 09/16/2011 Added a new APPT_TYPE_ID of 0.  
 -- kh946640: (07252019) Updated SP adding new paramenter @IsNewUI to support bothe Old and New COWS applications.
 -- kh946640: (08062019) Updated SP to return Appointment Types based on WF or ES Calendar with new input param @IsWF.
--================================================================================================  

ALTER PROCEDURE [web].[getAPPTTypes]   
	@UserID			Int	=	0,
	@IsMain			Bit	=	0,
	@IsNewUI		BIT = 0,
	@IsWF		BIT = 0
AS
BEGIN  
SET NOCOUNT ON;  

Begin Try

	DECLARE @User	TABLE
		(UserID	Int)

IF @IsNewUI = 1
	BEGIN

	IF @IsWF = 1
		BEGIN
	
		SELECT			0						AS	APPT_TYPE_ID
						,'-Select Appt Type-'	AS	APPT_TYPE_DES
		UNION ALL

		SELECT			APPT_TYPE_ID
						,APPT_TYPE_DES 
			FROM		dbo.LK_APPT_TYPE	WITH (NOLOCK) 
			WHERE 		REC_STUS_ID = 1 
			AND			APPT_TYPE_DES NOT LIKE '%UCaaS%' AND ((APPT_TYPE_ID > 8) AND (APPT_TYPE_ID < 17)) OR APPT_TYPE_ID IN (31)
			ORDER BY	APPT_TYPE_ID

		END
	ELSE
		BEGIN
	
		SELECT			0						AS	APPT_TYPE_ID
						,'-Select Appt Type-'	AS	APPT_TYPE_DES
		UNION ALL

		SELECT			APPT_TYPE_ID
						,APPT_TYPE_DES 
			FROM		dbo.LK_APPT_TYPE	WITH (NOLOCK) 
			WHERE 		REC_STUS_ID = 1 
			AND			APPT_TYPE_DES NOT LIKE '%UCaaS%' AND (((APPT_TYPE_ID > 16) OR ([APPT_TYPE_ID] < 9)) AND APPT_TYPE_ID NOT IN (31,32)) OR APPT_TYPE_ID = 0
			ORDER BY	APPT_TYPE_ID

		END

	IF @UserID	!=	0
		BEGIN
			IF	EXISTS
				(SELECT 'X' FROM dbo.MAP_USR_PRF mup	WITH (NOLOCK)
				 INNER JOIN	dbo.LK_USR_PRF	lup	WITH (NOLOCK)	ON	lup.USR_PRF_ID	=	mup.USR_PRF_ID
					WHERE	[USER_ID]		=	@UserID
						AND	mup.REC_STUS_ID	=	1 AND lup.REC_STUS_ID = 1
						AND	(lup.USR_PRF_DES like '%Event Reviewer%'
						OR	lup.USR_PRF_DES like '%Admin%'
						OR	lup.USR_PRF_DES like '%Fedline Preconfig Engineer%')
				)
				BEGIN
					INSERT INTO	@User	(UserID)
						SELECT  DISTINCT mup.[USER_ID] FROM dbo.MAP_USR_PRF mup	WITH (NOLOCK)
						INNER JOIN dbo.LK_USR_PRF	lup	WITH (NOLOCK)	ON	lup.USR_PRF_ID	=	mup.USR_PRF_ID
						WHERE	(lup.USR_PRF_DES like '%Event Activator%'
							OR	lup.USR_PRF_DES like '%Admin%'
							OR	lup.USR_PRF_DES like '%Fedline Preconfig Engineer%'
							OR	lup.USR_PRF_DES like '%Event Activator%'
							OR	lup.USR_PRF_DES like '%Fedline MNS Activator%')
				END	
			ELSE IF	EXISTS
				(SELECT 'X' FROM dbo.MAP_USR_PRF mup	WITH (NOLOCK)
				INNER JOIN	dbo.LK_USR_PRF	lup	WITH (NOLOCK)	ON	lup.USR_PRF_ID	=	mup.USR_PRF_ID
					WHERE	[USER_ID]		=	@UserID
						AND	mup.REC_STUS_ID	=	1 AND lup.REC_STUS_ID = 1
						AND	(lup.USR_PRF_DES like '%Event Activator%')
				)
				BEGIN
					IF	@IsMain = 0
						BEGIN
							INSERT INTO	@User	(UserID)
								SELECT  DISTINCT mup.[USER_ID] FROM dbo.MAP_USR_PRF mup	WITH (NOLOCK)
								INNER JOIN dbo.MAP_USR_PRF mmup	WITH (NOLOCK) on mmup.USR_PRF_ID = mup.USR_PRF_ID
								INNER JOIN dbo.LK_USR_PRF	lup	WITH (NOLOCK)	ON	lup.USR_PRF_ID	=	mup.USR_PRF_ID
								WHERE	mmup.[USER_ID]		=	@UserID
									AND	lup.USR_PRF_DES like '%Event Activator%'
						END
					ELSE
						BEGIN
							INSERT INTO	@User	(UserID)
									VALUES	(@UserID)
						END
					END
			ELSE IF	EXISTS
				(SELECT 'X' FROM dbo.MAP_USR_PRF mup	WITH (NOLOCK)
				INNER JOIN	dbo.LK_USR_PRF	lup	WITH (NOLOCK)	ON	lup.USR_PRF_ID	=	mup.USR_PRF_ID
					WHERE	[USER_ID]		=	@UserID
						AND	mup.REC_STUS_ID	=	1 AND lup.REC_STUS_ID = 1
						AND	(lup.USR_PRF_DES like '%Fedline Preconfig Engineer%'
							OR lup.USR_PRF_DES like '%Fedline MNS Activator%')
				)
				BEGIN
					IF	@IsMain = 0
						BEGIN
							INSERT INTO	@User	(UserID)
								SELECT  DISTINCT mup.[USER_ID] FROM dbo.MAP_USR_PRF mup	WITH (NOLOCK)
								INNER JOIN dbo.MAP_USR_PRF mmup	WITH (NOLOCK) on mmup.USR_PRF_ID = mup.USR_PRF_ID
								INNER JOIN dbo.LK_USR_PRF	lup	WITH (NOLOCK)	ON	lup.USR_PRF_ID	=	mup.USR_PRF_ID
								WHERE	mmup.[USER_ID]		=	@UserID
									AND	(lup.USR_PRF_DES like '%Fedline Preconfig Engineer%'
										OR lup.USR_PRF_DES like '%Fedline MNS Activator%')
						END
					ELSE
						BEGIN
							INSERT INTO	@User	(UserID)
									VALUES	(@UserID)
						END
				END
			ELSE BEGIN
					INSERT INTO	@User	(UserID)
						VALUES	(@UserID)
				END		
		END
	
	SELECT DISTINCT	a.[USER_ID]	AS	ID  
					,a.FULL_NME AS	Model   
		FROM		dbo.LK_USER			a	WITH (NOLOCK)  
				INNER JOIN dbo.MAP_USR_PRF mup	WITH (NOLOCK) ON mup.USER_ID = a.USER_ID
				INNER JOIN	dbo.LK_USR_PRF	lup	WITH (NOLOCK)	ON	lup.USR_PRF_ID	=	mup.USR_PRF_ID
				INNER JOIN @User u ON mup.[USER_ID]	=	u.UserID
					WHERE	(lup.USR_PRF_DES like '%Event Activator%'
						OR	lup.USR_PRF_DES like '%Fedline MNS Activator%')
						AND mup.REC_STUS_ID = 1
						AND a.USER_ID != 1
						ORDER BY a.FULL_NME
		
	SELECT DISTINCT	l.GRP_ID	
					,l.GRP_NME	
		FROM		dbo.USER_GRP_ROLE	a	WITH (NOLOCK)   
		INNER JOIN	dbo.LK_GRP			l	WITH (NOLOCK)	ON	a.GRP_ID	=	l.GRP_ID   
		INNER JOIN	@User				vt					ON	a.[USER_ID]	=	vt.UserID
		AND			l.GRP_ID	!=	98
		
	SELECT		GRP_ID, ROLE_ID 
		FROM	dbo.USER_GRP_ROLE	WITH (NOLOCK)
		WHERE	[USER_ID]		=	@UserID	
			AND	REC_STUS_ID	=	1
			AND	GRP_ID		IN	(2, 10, 12)

	END 

ELSE

	BEGIN

	SELECT			0						AS	APPT_TYPE_ID
					,'-Select Appt Type-'	AS	APPT_TYPE_DES
	UNION ALL

	SELECT			APPT_TYPE_ID
					,APPT_TYPE_DES 
		FROM		dbo.LK_APPT_TYPE	WITH (NOLOCK) 
		WHERE 		APPT_TYPE_DES NOT LIKE '%UCaaS%'
		ORDER BY	APPT_TYPE_ID

 
					
			  
		IF @UserID	!=	0
		BEGIN
			IF	EXISTS
				(SELECT 'X' FROM dbo.USER_GRP_ROLE	WITH (NOLOCK)
					WHERE	[USER_ID]		=	@UserID
						AND	REC_STUS_ID	=	1
						AND	(ROLE_ID	IN	(22, 42, 72))
				)
				BEGIN
					INSERT INTO	@User	(UserID)
						SELECT DISTINCT	ug.[USER_ID]
							FROM		dbo.USER_GRP_ROLE	ug	WITH (NOLOCK)
							INNER JOIN	dbo.USER_GRP_ROLE	gp	WITH (NOLOCK)	ON	ug.GRP_ID	=	gp.GRP_ID
							WHERE		ug.ROLE_ID		IN	(22, 23, 42, 72, 73)
								AND		gp.[USER_ID]	=	@UserID
				END	
			ELSE IF	EXISTS
				(SELECT 'X' FROM dbo.USER_GRP_ROLE	WITH (NOLOCK)
					WHERE	[USER_ID]		=	@UserID
						AND	REC_STUS_ID	=	1
						AND	(ROLE_ID	IN	(23))
				)
				BEGIN
					IF	@IsMain = 0
						BEGIN
							INSERT INTO	@User	(UserID)
								SELECT DISTINCT	ug.[USER_ID]
									FROM		dbo.USER_GRP_ROLE	ug	WITH (NOLOCK)
									INNER JOIN	dbo.USER_GRP_ROLE	gp	WITH (NOLOCK)	ON	ug.GRP_ID	=	gp.GRP_ID
									WHERE		ug.ROLE_ID		= 23
										AND		gp.[USER_ID]	=	@UserID
						END
					ELSE
						BEGIN
							INSERT INTO	@User	(UserID)
									VALUES	(@UserID)
						END
					END
			ELSE IF	EXISTS
				(SELECT 'X' FROM dbo.USER_GRP_ROLE	WITH (NOLOCK)
					WHERE	[USER_ID]		=	@UserID
						AND	REC_STUS_ID	=	1
						AND	(ROLE_ID	IN	(72, 73))
				)
				BEGIN
					IF	@IsMain = 0
						BEGIN
							INSERT INTO	@User	(UserID)
								SELECT DISTINCT	ug.[USER_ID]
									FROM		dbo.USER_GRP_ROLE	ug	WITH (NOLOCK)
									INNER JOIN	dbo.USER_GRP_ROLE	gp	WITH (NOLOCK)	ON	ug.GRP_ID	=	gp.GRP_ID
									WHERE		ug.ROLE_ID		IN	(72, 73)
										AND		gp.[USER_ID]	=	@UserID
						END
					ELSE
						BEGIN
							INSERT INTO	@User	(UserID)
									VALUES	(@UserID)
						END
				END
			ELSE BEGIN
					INSERT INTO	@User	(UserID)
						VALUES	(@UserID)
				END		
		END
	
	SELECT DISTINCT	a.[USER_ID]	AS	ID  
					,a.FULL_NME AS	Model   
		FROM		dbo.LK_USER			a	WITH (NOLOCK)   
		INNER JOIN	dbo.USER_GRP_ROLE	b	WITH (NOLOCK)	ON	a.[USER_ID]	=	b.[USER_ID]   
		INNER JOIN	@User				vt					ON	b.[USER_ID]	=	vt.UserID
		WHERE		b.ROLE_ID       IN	(23, 73)   
			AND		b.REC_STUS_ID	=	1   
			AND		a.[USER_ID]		!=	1   
		ORDER BY	a.FULL_NME  
		
	SELECT DISTINCT	l.GRP_ID	
					,l.GRP_NME	
		FROM		dbo.USER_GRP_ROLE	a	WITH (NOLOCK)   
		INNER JOIN	dbo.LK_GRP			l	WITH (NOLOCK)	ON	a.GRP_ID	=	l.GRP_ID   
		INNER JOIN	@User				vt					ON	a.[USER_ID]	=	vt.UserID
		AND			l.GRP_ID	!=	98
		
	SELECT		GRP_ID, ROLE_ID 
		FROM	dbo.USER_GRP_ROLE	WITH (NOLOCK)
		WHERE	[USER_ID]		=	@UserID	
			AND	REC_STUS_ID	=	1
			AND	GRP_ID		IN	(2, 10, 12)
	END
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]  
End Catch
END