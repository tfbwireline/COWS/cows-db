﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getVendorTemplates'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 12th July 2011
-- Description:	Returns all Vendor Templates for a Vendor Folder
-- ================================================================

ALTER PROCEDURE [dbo].[getVendorTemplates]
	@VendorFolderID INT 	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		VT.[VNDR_TMPLT_ID]
		,VT.[VNDR_FOLDR_ID]
		,VT.[FILE_NME]
		,VT.[FILE_SIZE_QTY]
		,VT.[CREAT_DT]
		,LU.[FULL_NME]
		,VT.[CREAT_BY_USER_ID]		
	FROM 
		[dbo].[VNDR_TMPLT] VT WITH (NOLOCK)
	INNER JOIN
		[dbo].[LK_USER] LU WITH (NOLOCK) ON VT.[CREAT_BY_USER_ID] = LU.[USER_ID]
	WHERE 
		[VNDR_FOLDR_ID] = @VendorFolderID

END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END

GO

