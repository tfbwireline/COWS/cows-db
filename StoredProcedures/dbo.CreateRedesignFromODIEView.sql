USE COWS
GO
_CreateObject 'SP','dbo','CreateRedesignFromODIEView'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <02/24/2016>
-- Description:	<Create Redesign once PM approves CPT>
--	@cust_short_name = ODIE Customer short name
--	@H1_ID = H1 value
--	@RedesignCategory = 1 for Voice, 2 for Data & 3 for Security
--	@CPT_ID = CPT_ID
-- Modified By:		<Sarah Sandoval>
-- Modified Date:	<03/21/2017>
-- Description:		DEV_NME format from [CPTNumber RedesignNumber_TestDevice] 
--					to [CPTNumber_RedesignNumber_TestDevice]; Removed whitespace
-- =============================================
--Exec dbo.CreateRedesignFromODIEView 'tcav','927337889','2',346
ALTER PROCEDURE [dbo].[CreateRedesignFromODIEView]
	@cust_short_name Varchar(20),
	@H1_ID Varchar(9),
	@RedesignCategory varchar(1),
	@CPT_ID Int
AS
BEGIN

	BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @odieview Table (REDSGN_ID Int,cust_nme varchar(500), 
								H1_ID varchar(9),
								cust_pdl varchar(500), 
								sows_folder varchar(500), 
								staff_id varchar(100),
								staff_role varchar(100),
								CSG_LVL_ID TINYINT
								)
			DECLARE @sql nvarchar(max) = ''
			set @sql = 'Select ' + CONVERT(VARCHAR,0) + ' AS REDSGN_ID, 
						Customer AS CUST_NME, 
						H1 AS H1_ID,
						Isnull(CUSTOMER_PDL,'''') AS CUSTOMER_PDL, 
						Isnull(SOWS_FOLDER,'''') AS SOWS_FOLDER,
						staf_id as staff_id,
						staff_role as staff_role, 0
			From OpenQuery(ODIEP101,''SELECT unique  
													C.CUST_ID AS  CUSTOMER_SHORT_NAME , 
													C.ORG_NAME AS Customer, 
													CH.H1_ID AS H1,  
													TRIM(C.TEAM_EMAIL) AS CUSTOMER_PDL, 
													C.DOCUMENT_URL AS SOWS_FOLDER,
													st.staf_id,
													st.staff_role
										FROM	INSC_V4.CUSTOMER C 
												LEFT JOIN INSC_V4.CUSTOMER_H1 CH ON C.CUST_ID = CH.CUST_ID 
												LEFT JOIN INSC_V4.QDA_CUSTOMER_INFO_VIEW st ON st.customer_short_name = CH.CUST_ID 
																							AND st.customer_H1 = CH.H1_ID 
										where	CH.DATA_VOICE_INDCR_CD = ''''D''''
												AND C.CUST_ID = ''''' + @cust_short_name + '''''
												AND ch.H1_ID = ''''' + CONVERT(VARCHAR,@H1_ID) + ''''' '')'

	--PRINT @sql
	insert into @odieview
	Exec sp_executeSQL @sql
	
	IF OBJECT_ID(N'tempdb..#tmp',N'U') IS NOT NULL 
	DROP TABLE #tmp
	declare @H1s nvarchar(max)
	create table #tmp (H1 CHAR(9) not null, CSG_LVL CHAR(5) not null)
	SELECT @H1s =  COALESCE(@H1s + ''',''', '') + CAST(H1_ID AS CHAR(9)) FROM @odieview 
	declare @listH1s nvarchar(max);
	SET @listH1s = REPLACE(@H1s,',',''',''')
	DECLARE @x AS NVARCHAR(MAX)

		SET @x = CAST('INSERT INTO #tmp ' +
		'([H1],[CSG_LVL]) ' +
		'SELECT DISTINCT CIS_CUST_ID, ISNULL(CSG_LVL,''00000'') ' +
		'FROM OPENQUERY ' +
		'(M5, ' +
		'''select DISTINCT CIS_CUST_ID,CSG_LVL ' +
		'from mach5.v_v5u_cust_acct S ' +
		'WHERE ' +
				'((S.CIS_HIER_LVL_CD=''''H1'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY''''))
				  OR  (S.CIS_HIER_LVL_CD=''''H4'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY''''))
				  OR  (S.CIS_HIER_LVL_CD=''''H6'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))' +
				'AND S.CIS_CUST_ID IN (''''' AS NVARCHAR(MAX)) + CAST(@listH1s AS NVARCHAR(MAX)) + CAST(''''') '') ' AS nvarchar(max))
	
		EXECUTE sp_executesql @x
	
	UPDATE odv
	SET CSG_LVL_ID = ISNULL(lcl.CSG_LVL_ID,0)
	FROM @odieview odv INNER JOIN
	#tmp tmp WITH (NOLOCK) ON tmp.H1=odv.H1_ID LEFT JOIN
	dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=tmp.CSG_LVL

	IF EXISTS
		(
			Select 'X'
				From @odieview
				Where ISNULL(cust_nme, '') != ''
		)
		BEGIN
			OPEN SYMMETRIC KEY FS@K3y 
			DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
			INSERT INTO dbo.CPT_REDSGN (CPT_ID,CUST_SHRT_NME,H1,STUS_ID,CREAT_USER_ID)
			VALUES (@CPT_ID, @cust_short_name, @H1_ID, 0,1)
			
			INSERT INTO dbo.REDSGN (REDSGN_NBR,H1_CD,CUST_NME,REDSGN_TYPE_ID,STUS_ID,ENTIRE_NW_CKD_ID, CRETD_BY_CD, CRETD_DT)
			Select  TOP 1 CASE  LEN(REDSGN_NBR) -  LEN(SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1) 
						WHEN 1 THEN 'R' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
						WHEN 2 THEN 'R0' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
						WHEN 3 THEN 'R00' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
						WHEN 4 THEN 'R000' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
						WHEN 5 THEN 'R0000' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
						WHEN 6 THEN 'R00000' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
						WHEN 7 THEN 'R000000' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
						WHEN 8 THEN 'R0000000' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
						WHEN 9 THEN 'R00000000' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
						WHEN 10 THEN 'R000000000' + CONVERT(VARCHAR,SUBSTRING(REDSGN_NBR,2,LEN(REDSGN_NBR)) + 1)
					END,
					@H1_ID,
					'',
					CASE @RedesignCategory
						WHEN 1 THEN 16
						WHEN 2 THEN 6
						ELSE 12
					END,
					220,
					0,
					1, --system
					GETDATE()
			FROM dbo.REDSGN WITH (NOLOCK)
			Order by REDSGN_ID desc
			
			DECLARE @RedesignID Int
			SET @RedesignID = @@IDENTITY
			
			Update dbo.CPT_REDSGN WITH (ROWLOCK)
				SET REDSGN_ID = @RedesignID
				Where CPT_ID = @CPT_ID
					AND STUS_ID = 0
			
			Update @odieview
				SET REDSGN_ID = @RedesignID
					
			--Update details from ODIE View				  
			Update r
				Set r.CUST_NME = CASE WHEN (o.CSG_LVL_ID>0) THEN NULL ELSE ISNULL(o.cust_nme,'') END,
					r.CUST_EMAIL_ADR = CASE WHEN (o.CSG_LVL_ID>0) THEN NULL ELSE ISNULL(o.cust_pdl,'') END,
					r.DSGN_DOC_LOC_TXT = ISNULL(o.sows_folder,''),
					r.ODIE_CUST_ID = @cust_short_name,
					r.CSG_LVL_ID = o.CSG_LVL_ID
				From dbo.REDSGN r with (rowlock)	
				Left Join @odieview o ON r.REDSGN_ID = o.REDSGN_ID 
				Where r.REDSGN_ID = @RedesignID
				  
			INSERT INTO dbo.CUST_SCRD_DATA ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_EMAIL_ADR], [CUST_NME])
			select x.rid, 16, dbo.encryptString(cust_pdl), dbo.encryptString(cust_nme)
			from (SELECT distinct @RedesignID as rid, ISNULL(o.cust_pdl,'') as cust_pdl, ISNULL(o.cust_nme,'') as cust_nme
				From dbo.REDSGN r with (nolock)	
				Left Join @odieview o ON r.REDSGN_ID = o.REDSGN_ID 
				Where r.REDSGN_ID = @RedesignID
				  AND o.CSG_LVL_ID>0) as x
			
			Update r
				Set r.NTE_ASSIGNED = isnull(nte.staff_id,''),
					r.PM_ASSIGNED = isnull(pm.staff_id,''),
					r.SDE_ASN_NME = isnull(se.staff_id,''),
					r.NE_ASN_NME = isnull(ne.staff_id,'')
				From dbo.REDSGN r with (rowlock)	
				Left Join @odieview nte ON r.REDSGN_ID = nte.REDSGN_ID AND nte.staff_role = 'NTE'
				Left Join @odieview se ON r.REDSGN_ID = se.REDSGN_ID AND se.staff_role = 'SDE'
				Left Join @odieview pm ON r.REDSGN_ID = pm.REDSGN_ID AND pm.staff_role = 'MNS PM'
				Left Join @odieview ne ON r.REDSGN_ID = ne.REDSGN_ID AND ne.staff_role = 'NE'
				Where r.REDSGN_ID = @RedesignID	
			
			--Update other redesign data
			Update dbo.REDSGN with (rowlock)
				SET SUBMIT_DT = GETDATE(),
					EXPRTN_DT = GETDATE() + 180,
					SLA_DT	=	dbo.CalculateEndBusinessDate(GETDATE(),5),
					STUS_ID = 221, --Submitted status
					REDSGN_CAT_ID = @RedesignCategory
				WHERE REDSGN_ID = @RedesignID	
					
				
			INSERT INTO dbo.REDSGN_DEVICES_INFO	(REDSGN_ID,SEQ_NBR,DEV_NME,REC_STUS_ID,CRETD_BY_CD,CRETD_DT,NTE_CHRG_CD)
			SELECT TOP 1 @RedesignID,1, ISNULL(c.CPT_CUST_NBR,'') + '_' + r.REDSGN_NBR + '_TestDevice',1,1,GETDATE(),0
				FROM dbo.REDSGN r WITH (NOLOCK)
				LEFT JOIN dbo.CPT_REDSGN cr WITH (NOLOCK) ON cr.REDSGN_ID = r.REDSGN_ID
				LEFT JOIN dbo.CPT c WITH (NOLOCK) ON c.CPT_ID = cr.CPT_ID
				WHERE r.REDSGN_ID = @RedesignID											
					
			--Insert notes into redesign notes table
			INSERT INTO dbo.REDSGN_NOTES (REDSGN_ID,REDSGN_NTE_TYPE_ID,NOTES,CRETD_BY_CD,CRETD_DT)
			SELECT r.REDSGN_ID, 4, 'Redesign: ' + r.REDSGN_NBR + ' created systematically from CPT process for CPT Customer Number:' + ISNULL(c.CPT_CUST_NBR,''), 1, GETDATE()
				FROM dbo.REDSGN r WITH (NOLOCK)
				LEFT JOIN dbo.CPT_REDSGN cr WITH (NOLOCK) ON cr.REDSGN_ID = r.REDSGN_ID
				LEFT JOIN dbo.CPT c WITH (NOLOCK) ON c.CPT_ID = cr.CPT_ID
				WHERE r.REDSGN_ID = @RedesignID
			
			--Insert notes into CPT history table
			INSERT INTO dbo.CPT_HIST (CPT_ID,ACTN_ID,CMNT_TXT,CREAT_BY_USER_ID,CREAT_DT)	
			SELECT cr.CPT_ID, 81, 'Redesign: '+ r.REDSGN_NBR + ' created systematically from CPT process.', 1, GETDATE()
				FROM dbo.CPT_REDSGN cr with (nolock)
				inner join dbo.REDSGN r with (nolock) on cr.REDSGN_ID = r.REDSGN_ID
				WHERE cr.CPT_ID = @CPT_ID
					AND cr.STUS_ID = 0
					
			UPDATE dbo.CPT_REDSGN
				SET STUS_ID = 2
				WHERE CPT_ID = @CPT_ID
					AND STUS_ID = 0		
		END
	
  
   END TRY
   
   BEGIN CATCH
		BEGIN TRY
			declare @errmsg varchar(max) = ERROR_MESSAGE() + '^|^' + CONVERT(VARCHAR,ERROR_NUMBER())
			RAISERROR (@errmsg, 16, 1) WITH NOWAIT
		END TRY
		BEGIN CATCH
			EXEC dbo.InsertErrorInfo  
		END CATCH
		
		Update	dbo.CPT_REDSGN WITH (ROWLOCK)
			SET		STUS_ID = 1
			Where	CPT_ID = @CPT_ID
				AND STUS_ID = 0	
   END CATCH 
END
GO
