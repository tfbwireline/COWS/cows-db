USE [COWS]
GO
/****** Object:  StoredProcedure [web].[GetRelatedActyForSelectedIPAddress]    Script Date: 01/08/2021 9:34:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Caranay, John
-- Create date: 06/01/2020
-- Description:	Get Related ACTY for Selected MSTR_ID
-- Updated:
--		
-- =============================================
ALTER PROCEDURE [web].[GetRelatedActyForSelectedIPAddress]  
	-- Add the parameters for the stored procedure here
	@IP_TYPE SMALLINT, -- 1 = CARRIER | 2 = SIP
	@IP_MSTR_ID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF @IP_TYPE = 1
	BEGIN
		SELECT 
			na.NID_ACTY_ID as Id,
			im.IP_MSTR_ID as IpMstrId, 
			im.IP_ADR as IpAdress,
			dbo.IPAddressToInteger(im.IP_ADR) as IP_INT,
			na.NID_SERIAL_NBR as NidSerialNbr, 
			na.NID_HOST_NME as NidHostNme,
			na.H6, 
			na.M5_ORDR_NBR as m5OrdrNbr, 
			focli.DEVICE_ID as DeviceId,  
			na.EVENT_ID as EventId,
			ia.REC_STUS_ID as RecStusId, 
			ls.STUS_DES as StusDes,
			ia.CREAT_DT as CreatDt
		FROM IP_MSTR as im
		JOIN IP_ACTY as ia ON im.IP_MSTR_ID = ia.IP_MSTR_ID
		JOIN LK_STUS as ls ON ia.REC_STUS_ID = ls.STUS_ID
		JOIN NID_ACTY as na ON ia.NID_ACTY_ID = na.NID_ACTY_ID
		JOIN FSA_ORDR_CPE_LINE_ITEM as focli ON na.FSA_CPE_LINE_ITEM_ID = focli.FSA_CPE_LINE_ITEM_ID
		--WHERE im.IP_MSTR_ID = @IP_MSTR_ID
		ORDER BY IP_INT ASC ;
	END
	ELSE
	BEGIN
		SELECT 
			nsa.NAT_SIP_ACTY_ID as Id,
			im.IP_MSTR_ID as IpMstrId, 
			im.IP_ADR as IpAdress,
			dbo.IPAddressToInteger(im.IP_ADR) as IP_INT,
			nsa.DD_APRVL_NBR as ddAprvlNbr, 
			nsa.h6, 
			ia.REC_STUS_ID as RecStusId, 
			ls.STUS_DES as StusDes,
			ia.CREAT_DT as CreatDt
		FROM IP_MSTR as im
		JOIN IP_ACTY as ia ON im.IP_MSTR_ID = ia.IP_MSTR_ID
		JOIN LK_STUS as ls ON ia.REC_STUS_ID = ls.STUS_ID
		JOIN NAT_SIP_ACTY as nsa ON ia.NAT_SIP_ACTY_ID = nsa.NAT_SIP_ACTY_ID
		-- WHERE im.IP_MSTR_ID = @IP_MSTR_ID
		ORDER BY IP_INT ASC ;
	END
END
