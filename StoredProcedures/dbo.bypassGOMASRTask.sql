USE [COWS]
GO
_CreateObject 'SP','dbo','bypassGOMASRTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <10/15/2014>
-- Description:	<bypass GOM ASR Task if the order belongs to country other than US>
-- =============================================
ALTER PROCEDURE dbo.bypassGOMASRTask 
@OrderID	Int,
@TaskID		SmallInt,
@TaskStatus	TinyInt,
@Comments	Varchar(1000) = NULL
AS
BEGIN
	IF @TaskID != 0
	BEGIN TRY
		IF NOT EXISTS
			(
				SELECT 'X'
					FROM	dbo.FSA_ORDR	fo	WITH (NOLOCK)
				INNER JOIN	dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
					WHERE	fo.ORDR_ID				=	@OrderID
						AND	fo.PROD_TYPE_CD			IN	('MP','DN')
						AND	ISNULL(oa.CTRY_CD,'')	IN	('GU','MP','PR','VI','US')
			)
			BEGIN
				EXEC dbo.CompleteActiveTask @OrderID,@TaskID,2,'',1
			END
	
		EXEC [dbo].[bypassNRMPLForSPAEOrders] @OrderID,@TaskID,@TaskStatus,''
    END TRY
    BEGIN CATCH
		EXEC dbo.insertErrorInfo
    END CATCH
END
GO
