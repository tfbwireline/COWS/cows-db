USE COWS
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Created By:	Shabu Karunakaran
-- Modified By: Ramesh Ragi
-- CREATE date:	07/27/2011
-- Description:	 Insert Next Task in CaptActiveTask
-- ===========================================================================
      
ALTER PROCEDURE [dbo].[insertNextActiveTask]      
	@OrderID		INT,      
	@ProfileIDs		VARCHAR(MAX)
AS      
BEGIN
	DECLARE	@Ctr			SMALLINT
	DECLARE	@Cnt			SMALLINT
	DECLARE	@RunTaskRule	BIT 
	DECLARE @PendingStatus	TINYINT
	DECLARE @NewTaskID		SMALLINT
	DECLARE @ixml			INT
	--DECLARE @bypass			BIT = '0'

	DECLARE @OrderData		TABLE (OrderID	INT, WGProfileID	SMALLINT, TaskID	SMALLINT)	 	
	DECLARE	@NewTasks		TABLE (OrderID	INT, WGProfileID	SMALLINT, TaskID	SMALLINT)
	DECLARE @TasksForRules	TABLE
		(
			ident					INT IDENTITY,
			TaskID					SMALLINT,			
			RunTaskRule				BIT
		)

	SET @Ctr			=	1
	SET @Cnt			=	0
	SET @PendingStatus	=	0
	SET	@RunTaskRule	=	0
	SET	@NewTaskID		=	0			
	
	BEGIN TRY		

		EXEC sp_xml_preparedocument @ixml OUTPUT, @ProfileIDs
		
		INSERT INTO 
			@OrderData 	
		SELECT OrderID, WGProfileID, TaskID FROM
		OPENXML (@ixml, '/Profiles/IDs', 2)
			WITH (OrderID INT, WGProfileID SMALLINT, TaskID SMALLINT)	 
			
		EXEC sp_xml_removedocument	@ixml
	
		--SELECT * FROM @TNData

		--If TaskID = 0. Insert statting task for profile
		IF EXISTS (SELECT 'X' FROM @OrderData WHERE TaskID	=	0)
			BEGIN
				INSERT INTO 
					@NewTasks				
				SELECT
					o.OrderID,	
					o.WGProfileID,					
					wpf.DESRD_TASK_ID					
				FROM
					@OrderData	o
				INNER JOIN	dbo.WG_PROF_STUS	wps	WITH (NOLOCK) ON	o.OrderID		=	wps.ORDR_ID 
																	AND o.WGProfileId	=	wps.WG_PROF_ID
				INNER JOIN	dbo.WG_PROF_SM		wpf WITH (NOLOCK) ON	wpf.PRE_REQST_TASK_ID	=	o.TaskID	 
																	AND wpf.WG_PROF_ID			=	o.WGProfileId
				WHERE
					o.Taskid		=	0	AND
					wps.STUS_ID = 0
			END
		
		--SELECT * FROM @NewTasks
	
		--Insert into captactivetask table for TaskIds sent as parameter 
		INSERT INTO 
			@NewTasks	
		SELECT 			
			o.OrderID,
			o.WGProfileID,						
			o.TaskID
		FROM			
			@OrderData o
			INNER JOIN	dbo.WG_PROF_STUS	wps	WITH (NOLOCK) ON o.OrderID = wps.ORDR_ID AND o.WGProfileId =  wps.WG_PROF_ID
			INNER JOIN	dbo.WG_PROF_SM		wpf WITH (NOLOCK) ON wpf.PRE_REQST_TASK_ID =	o.TaskID	 AND wpf.WG_PROF_ID =	o.WGProfileId
		WHERE
			o.TaskID			<>	0	AND
			wps.STUS_ID	=	0
	
		--SELECT * FROM @NewTasks
		
		UPDATE	cat
		SET
			STUS_ID = 0, MODFD_DT = GETDATE()
		FROM
			dbo.ACT_TASK cat WITH (ROWLOCK)
		INNER JOIN		@NewTasks nt	ON cat.ORDR_ID =	nt.OrderID AND cat.WG_PROF_ID =	nt.WGProfileID AND cat.ACT_TASK_ID = nt.TaskID

		--Insert Next task into captactivetasks
		INSERT INTO 
			dbo.ACT_TASK (ORDR_ID, TASK_ID, STUS_ID, WG_PROF_ID, CREAT_DT)
		SELECT DISTINCT
			@OrderID,
			TaskID,
			0,
			WGProfileID,
			GETDATE()
		FROM	
			@NewTasks nt
		WHERE
			nt.Taskid NOT IN (
								SELECT
									cat.TASK_ID
								FROM 		
									dbo.ACT_TASK	cat WITH (NOLOCK)
								WHERE
									cat.ORDR_ID		=	nt.OrderID	AND
									cat.WG_PROF_ID	=	nt.WGProfileID
							)

		--Moved logic to [dbo].[WfmGOMTaskRule] SP
		--IF EXISTS
		--	(
		--		SELECT 'X'
		--			FROM	dbo.FSA_ORDR fs WITH (NOLOCK) 
		--			WHERE	fs.ORDR_ID			=	@OrderID 
		--				AND fs.ORDR_ACTN_ID	=	2
		--				AND EXISTS
		--					(
		--						SELECT 'X'
		--							FROM	dbo.FSA_ORDR fps WITH (NOLOCK)
		--							WHERE	fps.FTN	= fs.FTN	
		--								AND	fps.ORDR_ACTN_ID = 1
		--								AND EXISTS
		--									(
		--										SELECT 'X'
		--											FROM	dbo.USER_WFM_ASMT uwa WITH (NOLOCK)
		--											WHERE	uwa.ORDR_ID = fps.ORDR_ID
		--									)	
		--					)
		--	)
		--	SET @bypass = '1'
		
		--Selecting Tasks for executing task rules
		INSERT INTO @TasksForRules
			SELECT
				DISTINCT nt.TaskID,
				CASE 
					WHEN (
							SELECT TOP 1 'Y'
							FROM 
								dbo.LK_TASK_RULE AS ltr WITH (NOLOCK)
							WHERE 
								(ltr.TASK_ID	= nt.TaskID) AND 
								(ltr.TASK_OPT_ID	= 0)
						  )	= 'Y'
					THEN 1	ELSE 0
				END AS RunTaskRule
			FROM
				@NewTasks	nt
		
		--SELECT * FROM @TasksForRules

		SET	@Ctr = 1  
		SET @Cnt = 0
		
		SELECT @Cnt = COUNT(1) FROM @TasksForRules AS tfr

		IF @Cnt > 0
		BEGIN
			WHILE @Ctr <= @Cnt
			BEGIN
				SET @NewTaskID = 0
				SET @RunTaskRule = 0				
				
				SELECT
					@NewTaskID		= tfr.TaskID,
					@RunTaskRule	= tfr.RunTaskRule					
				FROM 
					@TasksForRules AS tfr
				WHERE
					(tfr.ident		= @Ctr)
				
				
				
				
				--print @OrderID 
				--print	@NewTaskID
		
				IF (@OrderID != '')
				BEGIN
					--EXEC dbo.InsertCaptTaskNotes @SprintTNIDList, @LSRID, 0, @NewTaskID, @PendingStatus, '' --Username = ''
					
					-- run task rule
					IF (@RunTaskRule = 1)
						BEGIN
--							Print 'Taskrule'					
--							print @NewTaskID
							--IF ((@NewTaskID = 100 AND @bypass = '0') OR (@NewTaskID != 100))
							EXEC [dbo].[CompleteTaskRule] @OrderID, @NewTaskID, @PendingStatus, ''
						END
				END			
				SET @Ctr = @Ctr + 1  
			END
		END

			
    END TRY      
	  
	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]      
	END CATCH    
	
END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
