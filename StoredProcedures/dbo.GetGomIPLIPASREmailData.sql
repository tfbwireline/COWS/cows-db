USE [COWS]
GO
_CreateObject 'SP','dbo','GetGomIPLIPASREmailData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <10/20/14>
-- Description:	<to get GOM IPL+IP ASR email data>
-- =============================================
ALTER PROCEDURE dbo.GetGomIPLIPASREmailData
	@OrderID  int 
AS
BEGIN
	BEGIN TRY
	IF @OrderID != 0
		BEGIN
			SELECT	EMAIL_LIST_TXT AS EmailTo,
					EMAIL_CC_TXT AS EmailCC,
					EMAIL_SUBJ_TXT AS EmailSubject,
					EMAIL_BODY_TXT AS EmailBody
				FROM	dbo.EMAIL_REQ 
				WHERE	ORDR_ID = @OrderID
					AND STUS_ID = 10
					AND EMAIL_REQ_TYPE_ID = 17
		END
	
	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
	
END
GO

