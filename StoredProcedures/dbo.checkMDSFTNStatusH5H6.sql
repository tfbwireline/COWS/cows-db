USE [COWS]
GO
_CreateObject 'SP','dbo','checkMDSFTNStatusH5H6'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		jrg7298
-- Create date: 09/26/2012
-- Description:	Checks the Status of an FTN and checks to see if it is in Review Complete State or NOT using H5/H6.
-- =========================================================
ALTER PROCEDURE [dbo].[checkMDSFTNStatusH5H6]
	@H5_H6_CUST_ID		VARCHAR(9)
AS
BEGIN
SET NOCOUNT ON;
DECLARE	@OrdrTbl TABLE (ORDR_ID INT, FTN varchar(20))
DECLARE @FTN VARCHAR(1000)	
SET @FTN = ''

BEGIN TRY
INSERT INTO @OrdrTbl (ORDR_ID, FTN)
SELECT       DISTINCT           od.ORDR_ID, fo.FTN            
                        FROM        dbo.ORDR    od    WITH (NOLOCK)            
                                          INNER JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID            
                        WHERE       fo.ORDR_TYPE_CD NOT IN ('CN', 'DC')
                                          AND fo.ordr_actn_id = 2            
                                          AND od.H5_H6_CUST_ID  = @H5_H6_CUST_ID            
                                          AND od.ORDR_STUS_ID = 1            
                                          AND od.ordr_cat_id  IN (2,6)
										  AND (
												(fo.PROD_TYPE_CD IN ('MN','SE'))
												OR (
													(fo.PROD_TYPE_CD NOT IN ('MN','SE')) 
														AND (
																(ISNULL(fo.CPE_CPE_ORDR_TYPE_CD,'') = 'MNS') 
																OR (ISNULL(fo.TTRPT_MNGD_DATA_SRVC_CD,'') = 'Y')
															)
												   )
												)
										  AND NOT EXISTS (SELECT 'X'
														  FROM dbo.ACT_TASK at WITH (NOLOCK)
														  WHERE at.ORDR_ID=od.ORDR_ID
														    AND ((at.TASK_ID = 1000 AND at.STUS_ID = 3)            
																OR (at.TASK_ID in (300, 301) AND at.STUS_ID = 2)))

                                          
SELECT @FTN = convert(varchar, FTN)+ ',' + @FTN  FROM @OrdrTbl

IF (LEN(@FTN) > 0)
SELECT SUBSTRING(@FTN, 1, (LEN(@FTN)-1)) AS FTN
ELSE
SELECT @FTN

END TRY

BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
	SELECT @FTN
END CATCH
END
