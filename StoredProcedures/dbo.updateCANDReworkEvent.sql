USE [COWS]
GO
_CreateObject 'SP','dbo','updateCANDReworkEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --=============================================
 --Author:		<Ramesh Ragi>
 --Create date: <09/04/2014>
 --Description:	<update CAND Rework event>
 --=============================================
ALTER PROCEDURE dbo.updateCANDReworkEvent
	@EventID Int,
	@EventTypeID Int
AS

BEGIN
		IF @EventTypeID = 1
			begin
				Update dbo.AD_EVENT WITH (ROWLOCK)
					SET	EVENT_STUS_ID = 1
						,WRKFLW_STUS_ID = 1
					WHERE EVENT_ID = @EventID
			end
		ELSE IF @EventTypeID = 2
			begin
				Update dbo.NGVN_EVENT WITH (ROWLOCK)
					SET	EVENT_STUS_ID = 1
						,WRKFLW_STUS_ID = 1
					WHERE EVENT_ID = @EventID
			end
		ELSE IF @EventTypeID = 3
			begin
				Update dbo.MPLS_EVENT WITH (ROWLOCK)
					SET	EVENT_STUS_ID = 1
						,WRKFLW_STUS_ID = 1
					WHERE EVENT_ID = @EventID
			end
		ELSE IF @EventTypeID = 4
			begin
				Update dbo.SPLK_EVENT WITH (ROWLOCK)
					SET	EVENT_STUS_ID = 1
						,WRKFLW_STUS_ID = 1
					WHERE EVENT_ID = @EventID
			end	
		
		IF EXISTS
			(
				SELECT 'X'
					FROM dbo.EVENT_ASN_TO_USER
					WHERE EVENT_ID = @EventID
						AND REC_STUS_ID = 1
			)
			BEGIN
				DELETE FROM dbo.APPT WHERE EVENT_ID = @EventID
				UPDATE	dbo.EVENT_ASN_TO_USER WITH (ROWLOCK)
					SET		REC_STUS_ID = 0
					WHERE	EVENT_ID = @EventID
						AND REC_STUS_ID = 1
				
				Insert into dbo.EVENT_HIST (EVENT_ID,ACTN_ID,CMNT_TXT,CREAT_BY_USER_ID,CREAT_DT)
				SELECT @EventID,45,'System moving the event back from Rework to Visible status since event has been in rework in 72 hours.Activator was also unassigned.',
						1,GETDATE()
			END
		ELSE
			BEGIN
				Insert into dbo.EVENT_HIST (EVENT_ID,ACTN_ID,CMNT_TXT,CREAT_BY_USER_ID,CREAT_DT)
				SELECT @EventID,45,'System moving the event back from Rework to Visible status since event has been in rework in 72 hours.',
						1,GETDATE()
			END
		
END
GO
