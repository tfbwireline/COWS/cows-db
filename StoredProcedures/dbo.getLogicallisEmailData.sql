USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getLogicallisEmailData]    Script Date: 07/22/2021 10:06:14 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<David Phillips>
-- Create date: <01/27/2021>
-- Description:	<To retrieve Logicallis email data>
-- =============================================
alter PROCEDURE [dbo].[getLogicallisEmailData]
	@ORDR_ID int
AS
BEGIN
	SET NOCOUNT ON;
 
BEGIN TRY

	DECLARE @PO	varchar (20)

	SELECT TOP 1 @PO = gn.PRCH_ORDR_NBR  FROM FSA_ORDR fo WITH (NOLOCK)
		INNER JOIN FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON cli.ORDR_ID = fo.ORDR_ID
		INNER JOIN FSA_ORDR_GOM_XNCI gn WITH (NOLOCK) ON cli.ORDR_ID = gn.ORDR_ID
		WHERE cli.ORDR_CMPNT_ID in (SELECT ORDR_CMPNT_ID FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
									 WHERE ORDR_ID = @ORDR_ID AND EQPT_TYPE_ID = 'MJH' AND ITM_STUS = 401
										AND CNTRC_TYPE_ID in ('RNTL','LEAS'))
			AND ISNULL(gn.PRCH_ORDR_NBR, '') <> ''
		ORDER BY fo.CREAT_DT desc
			


	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;	

	SELECT TOP 1 COALESCE(dbo.decryptbinarydata(csd.CUST_NME),fc.CUST_NME,'')	AS Customer, 		
			COALESCE(dbo.decryptbinarydata(csda.STREET_ADR_1),oa.STREET_ADR_1,'')	AS Address1,
			COALESCE(dbo.decryptbinarydata(csda.STREET_ADR_2),oa.STREET_ADR_2,'')	AS Address2,
			COALESCE(dbo.decryptbinarydata(csda.STREET_ADR_3),oa.STREET_ADR_3,'')	AS Address3,
			COALESCE(dbo.decryptbinarydata(csda.CTY_NME),oa.CTY_NME,'')				AS CITY,
			COALESCE(lc.CTRY_NME,'')												AS COUNTRY,
			COALESCE(dbo.decryptbinarydata(csda.STT_PRVN_NME),oa.PRVN_NME,'')		AS PRVN,
			COALESCE(dbo.decryptbinarydata(csda.STT_CD),oa.STT_CD,'')				AS STATE,
			COALESCE(dbo.decryptbinarydata(csda.ZIP_PSTL_CD),oa.ZIP_PSTL_CD,'')		AS ZIP,
			COALESCE(dbo.decryptbinarydata(csda.BLDG_NME),oa.BLDG_NME,'')			AS BLDG,
			COALESCE(dbo.decryptbinarydata(csda.FLR_ID),oa.FLR_ID,'')				AS FLOOR,
			COALESCE(dbo.decryptbinarydata(csda.RM_NBR),oa.RM_NBR,'')				AS ROOM,		
			COALESCE(dbo.decryptbinarydata(csdC.CUST_CNTCT_NME),oc.CNTCT_NME,'')	AS SiteContact,		
			ISNULL(oc.PHN_NBR,'')													AS SiteOfficePhone,	 
			ISNULL(dbo.decryptbinarydata(csdC.CUST_CNTCT_CELL_PHN_NBR),'')			AS SiteMobilePhone,	 
			COALESCE (dbo.decryptbinarydata(csdC.CUST_EMAIL_ADR), oc.EMAIL_ADR,'')  AS Siteemail,		
			ISNULL(@PO,'')															AS OriginalPO,	
			REPLACE(REPLACE(Convert(Varchar, COALESCE (od.CUST_CMMT_DT, fo.CUST_CMMT_DT,''), 7), ' ', '/'), ',/', '/')  
																					AS DisconnectionDate	 

		FROM	dbo.ORDR	od	WITH (NOLOCK)
		INNER JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID
		LEFT OUTER JOIN dbo.ORDR_CNTCT		oc	WITH (NOLOCK) on od.ORDR_ID = oc.ORDR_ID
																AND oc.CIS_LVL_TYPE in ('H6','H5')
																AND ROLE_ID = 92
		LEFT OUTER JOIN dbo.CUST_SCRD_DATA csdC WITH (NOLOCK) ON csdC.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csdC.SCRD_OBJ_TYPE_ID=15
		LEFT OUTER JOIN	dbo.ORDR_ADR oa			WITH (NOLOCK)	ON	oa.ORDR_ID = od.ORDR_ID  
														AND	oa.CIS_LVL_TYPE in ('H6','H5')
														AND oa.ADR_TYPE_ID = 18
		LEFT OUTER JOIN LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD
		LEFT OUTER JOIN dbo.CUST_SCRD_DATA csda WITH (NOLOCK)
							ON csda.SCRD_OBJ_ID = oa.ORDR_ADR_ID and csda.SCRD_OBJ_TYPE_ID = 14
		
		LEFT OUTER JOIN dbo.FSA_ORDR_CUST fc WITH (NOLOCK) ON od.ORDR_ID = fc.ORDR_ID AND fc.CIS_LVL_TYPE in ('H5','H6')
		LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=fc.FSA_ORDR_CUST_ID AND csd.SCRD_OBJ_TYPE_ID=5
		
		WHERE	 od.ORDR_ID = @ORDR_ID	
		
END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH
    
END
