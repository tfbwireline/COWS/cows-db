USE [COWS]
GO

/****** Object:  StoredProcedure [dbo].[processFOCMessages]    Script Date: 01/11/2016 11:44:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[processFOCMessages]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[processFOCMessages]
GO