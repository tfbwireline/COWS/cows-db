USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[InsertRedesignEmailData_V2]    Script Date: 1/18/2022 10:15:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- ===============================================================================
---- Author:		Sarah Sandoval
---- Create date:	08082020
---- Description:	Updated InsertRedesignEmailData
----				Inserts redesign email data
---- Update date:	06162021
---- Description:	Added Contact Details and removed CUST PDL, NTE, PM, SDE, NE
---- ===============================================================================
ALTER PROCEDURE [dbo].[InsertRedesignEmailData_V2]
	@redsgn_id int,
	@stus_id smallint,
	@expirationFlag bit,
	@note_updated bit = 0,
	@redsgn_url varchar(max) = ''
AS

BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	
	--declare @redsgn_id int = 397, @stus_id smallint = 221, @expirationFlag bit = 0
	declare @subject varchar(max) = '', @email_req_type_id int 
	declare @dev_nme varchar(max) = '', @email_body_txt varchar(max) = '', @emailTo varchar(max) = ''
	declare @ntetbl table(REDSGN_ID int, nte varchar(max), devcnt int, devcmplt int, expcnt int)
	
	insert into @ntetbl
	select distinct rd.redsgn_id,
	case when ((select count(1) from dbo.REDSGN_NOTES rn with (nolock) where rd.redsgn_id=rn.redsgn_id and rn.notes like 'Status changed to Approved%')>0) then 1 else 0 end as nte,
	(select count(1) from dbo.redsgn_devices_info rdi with (nolock) where rdi.redsgn_id=rd.redsgn_id and rdi.rec_stus_id=1) as devcnt,
	(select count(1) from dbo.redsgn_devices_info rdi with (nolock) where rdi.redsgn_id=rd.redsgn_id and rdi.rec_stus_id=1 and isnull(rdi.DEV_CMPLTN_CD,0)=1) as devcmplt,
	(SELECT count(1) FROM dbo.REDSGN_NOTES rn WITH (NOLOCK) WHERE rd.REDSGN_ID = rn.REDSGN_ID AND rn.NOTES = 'Status Update: Cancelled by System after 180 days') as expcnt
	from dbo.REDSGN rd with (nolock)
	where rd.redsgn_id=@redsgn_id

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	select @email_req_type_id = EMAIL_REQ_TYPE_ID 
		From dbo.LK_EMAIL_REQ_TYPE WITH (NOLOCK)
		where EMAIL_REQ_TYPE_DES = 'Redesign Email'
	
	if(@expirationFlag = 1)
		begin
			Select @subject = CASE WHEN ((nt.nte=1) AND (nt.devcmplt>0) AND (nt.devcnt!=nt.devcmplt) AND (nt.expcnt>0)) THEN 'Expired before all sites implemented, Expiration for Redesign Number:' + rd.REDSGN_NBR + ' has been changed to ' + convert(varchar,rd.EXPRTN_DT,101) 
								WHEN ((nt.nte=1) AND ((nt.devcmplt=0) or (nt.devcnt=nt.devcmplt)) AND (nt.expcnt>0)) THEN 'Expired after approval, Expiration for Redesign Number:' + rd.REDSGN_NBR + ' has been changed to ' + convert(varchar,rd.EXPRTN_DT,101)
								WHEN ((nt.nte=0) AND (nt.expcnt>0)) THEN 'Expired before approval, Expiration for Redesign Number:' + rd.REDSGN_NBR + ' has been changed to ' + convert(varchar,rd.EXPRTN_DT,101)
								ELSE 'Expiration for Redesign Number:' + rd.REDSGN_NBR + ' has been changed to ' + convert(varchar,rd.EXPRTN_DT,101) END
				From dbo.REDSGN rd with (nolock) inner join
				@ntetbl nt on rd.REDSGN_ID=nt.REDSGN_ID
				Where rd.REDSGN_ID = @redsgn_id
		end
	else
		begin
			Select @subject = CASE @stus_id
										when 221 then 'Redesign Request ' + r.REDSGN_NBR + ' ' + ls.STUS_DES + ':' + CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End + ' ,' + lrt.REDSGN_TYPE_DES
										when 222 then 'Redesign Request ' + r.REDSGN_NBR + ' changed to ' + ls.STUS_DES + ' status.'
										when 223 then 'Redesign Request ' + r.REDSGN_NBR + ' changed to ' + ls.STUS_DES + ' status.'
										when 230 then 'Redesign Request ' + r.REDSGN_NBR + ' changed to ' + ls.STUS_DES + ' status.'
										when 231 then 'Redesign Request ' + r.REDSGN_NBR + ' changed to ' + ls.STUS_DES + ' status.'
										when 225 then 'Redesign Request ' + r.REDSGN_NBR + ' ' + ls.STUS_DES + ' : ' + CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End
										WHEN 226 THEN CASE WHEN ((nt.nte=1) AND (nt.devcmplt>0) AND (nt.devcnt!=nt.devcmplt) AND (nt.expcnt=0)) THEN 'Cancelled before all sites implemented, Cancelled: Redesign ' + r.REDSGN_NBR + ' : ' +  CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End
										WHEN ((nt.nte=1) AND (nt.devcmplt>0) AND (nt.devcnt!=nt.devcmplt) AND (nt.expcnt>0)) THEN 'Expired before all sites implemented, Cancelled: Redesign ' + r.REDSGN_NBR + ' : ' +  CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End										
										WHEN ((nt.nte=1) AND ((nt.devcmplt=0) or (nt.devcnt=nt.devcmplt)) AND (nt.expcnt=0)) THEN 'Cancelled after approval, Cancelled: Redesign ' + r.REDSGN_NBR + ' : ' +  CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End
										WHEN ((nt.nte=1) AND ((nt.devcmplt=0) or (nt.devcnt=nt.devcmplt)) AND (nt.expcnt>0)) THEN 'Expired after approval, Cancelled: Redesign ' + r.REDSGN_NBR + ' : ' +  CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End
										WHEN ((nt.nte=0) AND (nt.expcnt=0)) THEN 'Cancelled before approval, Cancelled: Redesign ' + r.REDSGN_NBR + ' : ' +  CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End
										WHEN ((nt.nte=0) AND (nt.expcnt>0)) THEN 'Expired before approval, Cancelled: Redesign ' + r.REDSGN_NBR + ' : ' +  CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End
										ELSE 'Cancelled: Redesign ' + r.REDSGN_NBR + ' : ' +  CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End END
										when 227 then 'Redesign Request ' + r.REDSGN_NBR + ' ' + ls.STUS_DES + ' : ' + CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End
										WHEN 229 THEN 'Expiration Warning: Redesign ' + r.REDSGN_NBR + ' : ' + CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End
										WHEN 2291 THEN 'Expiration Warning: Redesign ' + r.REDSGN_NBR + ' : ' + CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End
										else 'Redesign Request ' + r.REDSGN_NBR + ' changed to ' + ls.STUS_DES + ' status.'
									  END
					From	dbo.redsgn r with (nolock)
						inner join @ntetbl nt on r.REDSGN_ID=nt.REDSGN_ID
						Left Join dbo.LK_STUS ls with (nolock) on r.STUS_ID = ls.STUS_ID
						Left Join dbo.LK_REDSGN_TYPE lrt with (nolock) on lrt.REDSGN_TYPE_ID = r.REDSGN_TYPE_ID
					Where	r.REDSGN_ID = @redsgn_id
			
			IF @note_updated = 1
				BEGIN
					SET @subject = 'Updated:' + @subject
				END

		end

	select @dev_nme = COALESCE(DEV_NME,@dev_nme) + ',' + @dev_nme   From dbo.REDSGN_DEVICES_INFO WITH (NOLOCK) where redsgn_id = @redsgn_id and REC_STUS_ID = 1	 
	if(len(@dev_nme) > 0)
		Set @dev_nme = SUBSTRING(@dev_nme,1,LEN(@dev_nme) - 1)

	--Email BODY
	set @email_body_txt = (Select 
		(select 
				r.REDSGN_NBR as 'RedesignNumber',
				r.H1_CD as 'H1',
				CASE When (r.CSG_LVL_ID>0) Then 'Private Customer' Else r.CUST_NME End as 'CustomerName',
				replace(@dev_nme,',','<br/>') as 'Devices',
				ldt.REDSGN_TYPE_NME as 'RedesignType',
				CASE WHEN ISNULL(r.IS_SDWAN,0)=0 THEN 'No' ELSE 'Yes' END as 'SDWAN',
				CASE WHEN ISNULL(r.CISC_SMRT_LIC_CD,0)=0 THEN 'No' ELSE 'Yes' END as 'CiscoSmart',
				r.SMRT_ACCNT_DMN as 'SmartAccount',
				r.VRTL_ACCNT as 'VirtualAccount',
				r.COST_AMT as 'Cost',
				REPLACE([dbo].[RemoveEmailSpecialCharacters](ISNULL(rnc.NOTES,'')), char(13), '<br/>') as 'Caveats',
				r.REDSGN_ID as 'RedesignID',
				isnull(@stus_id,0) as 'RedesignStatusID',
				ISNULL(ls.STUS_DES,'') as 'RedesignStatus',
				@redsgn_url as 'RedesignURL',
				isnull(convert(varchar,r.exprtn_dt,101),'') as 'ExpirationDate',
				r.REDSGN_CAT_ID,
				convert(int,r.MSS_IMPL_EST_AMT) as MSS_IMPL_EST_AMT
			from dbo.redsgn r with (nolock) 
			inner join dbo.LK_STUS ls with (nolock) on r.STUS_ID = ls.STUS_ID
			inner join dbo.LK_REDSGN_TYPE ldt with (nolock) on ldt.REDSGN_TYPE_ID = r.REDSGN_TYPE_ID
			left join (Select	Max(REDSGN_NOTES_ID) AS rnoteID, REDSGN_ID  AS redesignID 
							FROM	dbo.REDSGN_NOTES rn WITH (NOLOCK)
							Inner Join dbo.LK_REDSGN_NOTES_TYPE lrnt with (nolock) on rn.REDSGN_NTE_TYPE_ID = lrnt.REDSGN_NTE_TYPE_ID
							WHERE	lrnt.REDSGN_NTE_TYPE_CD = 'Caveats'
							Group by REDSGN_ID)rn  ON rn.redesignID = r.REDSGN_ID
			left join dbo.REDSGN_NOTES rnc with (nolock) on rnc.REDSGN_NOTES_ID = rn.rnoteID
			where r.REDSGN_ID = @redsgn_id 
			For XML PATH('RedesignInfo'),Type
		),	
	    (
		Select 
			lrnt.REDSGN_NTE_TYPE_CD as 'RedesignNoteType',
			lu.DSPL_NME as 'CreatedBy',
			r.CRETD_DT as 'CreatedDate',
			--r.NOTES as 'Notes'
			REPLACE([dbo].[RemoveEmailSpecialCharacters](r.NOTES), char(13), '<br/>') as 'Notes' 	 
			From dbo.REDSGN_NOTES r WITH (NOLOCK)
			Inner Join dbo.LK_USER lu with (nolock) on r.CRETD_BY_CD = lu.USER_ID
			Inner Join dbo.LK_REDSGN_NOTES_TYPE lrnt with (nolock) on lrnt.REDSGN_NTE_TYPE_ID = r.REDSGN_NTE_TYPE_ID
			Where r.REDSGN_ID = @redsgn_id
			order by r.REDSGN_NOTES_ID desc
			For XML Path('RedesignNotesInfo'),Type
		),
		-- Added this section for BPM Redesign Documents and Additional Documents
		(
		SELECT 
			REDSGN_DOC_ID AS DocID, FILE_NME AS DocName, FILE_SIZE_QTY AS DocSize,
			(SELECT SUBSTRING(@redsgn_url, 1, CHARINDEX('/redesign', @redsgn_url)) + 'doc-download/' + (SELECT CAST(REDSGN_DOC_ID AS VARCHAR(10))) + '?type=redesign') AS DocURL
			From dbo.REDSGN_DOC WITH (NOLOCK)
			Where REDSGN_ID = @redsgn_id
			For XML Path('RedesignDocsInfo'),Type
		),
		(
		SELECT StringID AS Document FROM
			web.parseStringWithDelimiter((SELECT ADDL_DOC_TXT From dbo.REDSGN WITH (NOLOCK) Where REDSGN_ID = @redsgn_id), ';')
			For XML Path('AdditionalDocuments'),Type
		)
	 FOR XML PATH(''), ROOT('RedesignEmail') )

	--Email To List
	Select @emailTo = PRMTR_VALU_TXT
	From dbo.LK_SYS_CFG WITH (NOLOCK)
	Where PRMTR_NME = 'REDSGN_EMAIL_LIST'

	--Redesign IDentification emails IDs
	Select @emailTo = case ISNULL(USER_IDNTFCTN_EMAIL,'')
					when '' THEN @emailTo
					else 
						case 
							when LEN(@emailTo) = 0  then USER_IDNTFCTN_EMAIL
								else @emailTo + ','  + USER_IDNTFCTN_EMAIL
						end
				  end
	From dbo.REDSGN_EMAIL_NTFCTN with (nolock)	
	Where REDSGN_ID = @redsgn_id
		AND REC_STUS_ID = 1
		AND ((PATINDEX('%@%',USER_IDNTFCTN_EMAIL)>0) AND (PATINDEX('%.%',USER_IDNTFCTN_EMAIL)>0))

	-- Contact Details
	Select @emailTo = case ISNULL(EMAIL_ADR,'')
					when '' THEN @emailTo
					else 
						case 
							when LEN(@emailTo) = 0  then EMAIL_ADR
								else @emailTo + ','  + EMAIL_ADR
						end
				  end
	From dbo.CNTCT_DETL with (nolock)
	Where OBJ_ID = @redsgn_id
		AND OBJ_TYP_CD = 'R'
		AND REC_STUS_ID = 1
		AND @stus_id IN (Select * From web.parseStringWithDelimiter((EMAIL_CD), ','))
		AND SUPRS_EMAIL = 0

	----Cust Team PDL
	--Select @emailTo = case isnull(r.CUST_EMAIL_ADR,'')
	--				when '' then @emailTo
	--				else	
	--					case 
	--						when LEN(@emailTo) = 0  then r.CUST_EMAIL_ADR
	--						else @emailTo + ',' + r.CUST_EMAIL_ADR
	--					end
	--			  end 
				 
	--From dbo.REDSGN r WITH (NOLOCK)
	--Where r.REDSGN_ID = @redsgn_id
 --     and r.CSG_LVL_ID=0	

	----PM Assigned User
	--Select @emailTo = case isnull(pm.EMAIL_ADR,'')
	--						when '' then @emailTo
	--						else 
	--							case 
	--								when len(@emailTo) = 0 then ISNULL(pm.EMAIL_ADR,'')
	--								else @emailTo + ',' + ISNULL(pm.EMAIL_ADR,'')
	--							end
	--					end
	--From dbo.REDSGN r with (nolock)
	--Left join dbo.LK_USER pm with (nolock) ON	r.PM_ASSIGNED = pm.user_adid 
	--						AND pm.REC_STUS_ID = 1
	--Where r.REDSGN_ID = @redsgn_id	

	----NTE Assigned User
	--Select @emailTo = case isnull(nte.EMAIL_ADR,'')
	--						when '' then @emailTo
	--						else 
	--							case 
	--								when len(@emailTo) = 0 then ISNULL(nte.EMAIL_ADR,'')
	--								else @emailTo + ',' + ISNULL(nte.EMAIL_ADR,'')
	--							end
	--					end
	--From dbo.REDSGN r with (nolock)
	--Left join dbo.LK_USER nte with (nolock) ON r.NTE_ASSIGNED = nte.user_adid 
	--						AND nte.REC_STUS_ID = 1
	--Where r.REDSGN_ID = @redsgn_id		

	----SDE Assigned User
	--Select @emailTo = case isnull(sse.EMAIL_ADR,'')
	--						when '' then @emailTo
	--						else 
	--							case 
	--								when len(@emailTo) = 0 then ISNULL(sse.EMAIL_ADR,'')
	--								else @emailTo + ',' + ISNULL(sse.EMAIL_ADR,'')
	--							end
	--					end
	--From dbo.REDSGN r with (nolock)
	--Left join dbo.LK_USER sse with (nolock) ON r.SDE_ASN_NME = sse.user_adid 
	--						AND sse.REC_STUS_ID = 1
	--Where r.REDSGN_ID = @redsgn_id

	----NE Assigned User
	--Select @emailTo = case isnull(ne.EMAIL_ADR,'')
	--						when '' then @emailTo
	--						else 
	--							case 
	--								when len(@emailTo) = 0 then ISNULL(ne.EMAIL_ADR,'')
	--								else @emailTo + ',' + ISNULL(ne.EMAIL_ADR,'')
	--							end
	--					end
	--From dbo.REDSGN r with (nolock)
	--Left join dbo.LK_USER ne with (nolock) ON	r.NE_ASN_NME = ne.user_adid 
	--						--AND ne.REC_STUS_ID = 1	
	--Where r.REDSGN_ID = @redsgn_id	

	--Submitter Email ID
	if @stus_id != 220
		begin
			select @emailTo = case ISNULL(lu.email_adr,'')
									when '' then @emailTo
									else 
										case 
											when len(@emailTo) = 0 then lu.email_adr
											else @emailTo + ',' + lu.email_adr
										end
								end
				from dbo.REDSGN r with (nolock) 
				left join dbo.LK_USER lu with (nolock) on r.CRETD_BY_CD = lu.USER_ID 
				where r.REDSGN_ID = @redsgn_id
		end
	
	--Email user who changed redesign status
	select @emailTo = case ISNULL(lu.email_adr,'')
						when '' then @emailTo
						else 
							case 
								when len(@emailTo) = 0 then lu.email_adr
								else @emailTo + ',' + lu.email_adr
							end
					end
	from dbo.REDSGN r with (nolock) 
	left join dbo.LK_USER lu with (nolock) on r.MODFD_BY_CD = lu.USER_ID 
	where r.REDSGN_ID = @redsgn_id
	
	--Data PDL
	--Updated by Sarah Sandoval (10272017) to include PDL for Security as well
	if exists (Select 'x' from dbo.REDSGN with (nolock) where REDSGN_ID = @redsgn_id and REDSGN_CAT_ID != 1)
		begin
			select @emailTo = case PRMTR_VALU_TXT
								when '' then @emailTo
								else @emailTo + ',' + PRMTR_VALU_TXT
							  end
				From dbo.LK_SYS_CFG with (nolock) 
				where PRMTR_NME = 'RedesignDataSecurityEmailPDL'
					and REC_STUS_ID = 1
		end
		
	--NTE Pool User
	if exists (Select 'x' from dbo.REDSGN with (nolock) where REDSGN_ID = @redsgn_id and REDSGN_CAT_ID = 2 AND ISNULL(NTE_ASSIGNED,'') IN ('nte_pool') )
		begin
			declare @ntePool varchar(2000) = ''
			Select @ntePool = coalesce(lu.EMAIL_ADR,  @ntepool)  + ',' + @ntePool
				From user_grp_role ugr with (nolock) 
				inner join dbo.LK_USER lu with (nolock) on ugr.USER_ID = lu.USER_ID
				where ugr.ROLE_ID = 23 
					and	ugr.POOL_CD = 1 
					and ugr.GRP_ID = 2 
					and ugr.REC_STUS_ID = 1
					and lu.REC_STUS_ID = 1

			--Select SUBSTRING(@ntePool,0,len(@ntePool))
			if(@ntePool != '')
				set @emailTo = @emailTo + ',' + SUBSTRING(@ntePool,0,len(@ntePool))
		end
	
	Set @emailTo = replace(@emailTo,';',',')

	--select * from LK_STUS where STUS_TYPE_ID = 'redsgn'
	--select @emailTo
	--select @subject
	--select @email_body_txt as 'Email Body'
	--set @emailTo = 'Ramesh.Ragi@sprint.com'
	
	Insert into EMAIL_REQ (EMAIL_REQ_TYPE_ID, STUS_ID, CREAT_DT, EMAIL_LIST_TXT, EMAIL_SUBJ_TXT, EMAIL_BODY_TXT, REDSGN_ID)
		Values (@email_req_type_id, 10, GETDATE(),@emailTo,@subject, @email_body_txt, @redsgn_id)

	END TRY
	
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END