USE [COWS]
GO

_CreateObject 'SP'
	,'web'
	,'getM5SecrdValidation'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  jrg7298            
-- Create date: 07/30/2015         
-- Description: Get the CSG Levels using M5 order number or H1
-- =============================================            
ALTER PROCEDURE [web].[getM5SecrdValidation] @H1 VARCHAR(9) = ''
	,@H6 VARCHAR(9) = ''
	,@M5OrdrNbr VARCHAR(20) = ''
AS
BEGIN
	SET NOCOUNT ON;

	BEGIN TRY
		IF OBJECT_ID(N'tempdb..#M5Table', N'U') IS NOT NULL
			DROP TABLE #M5Table

		CREATE TABLE #M5Table (CSG_LVL VARCHAR(5) NULL)

		DECLARE @SrcdQuery NVARCHAR(MAX)

		IF (
				(@H1 <> '')
				AND (@H6 = '')
				AND (@M5OrdrNbr = '')
				)
		BEGIN
			SET @SrcdQuery = 'SELECT CSG_LVL
				FROM OPENQUERY(M5,''SELECT DISTINCT CSG_LVL
											FROM MACH5.v_v5U_cust_acct
											WHERE CIS_CUST_ID = ''''' + @H1 + ''''' AND CIS_HIER_LVL_CD = ''''H1'''' AND CSG_LVL IS NOT NULL AND ROWNUM <= 1 '')'
		END
		ELSE IF (
				(@H1 = '')
				AND (@H6 <> '')
				AND (@M5OrdrNbr = '')
				)
		BEGIN
			SET @SrcdQuery = 'SELECT CSG_LVL
				FROM OPENQUERY(M5,''SELECT DISTINCT CSG_LVL
											FROM MACH5.v_v5U_cust_acct
											WHERE CIS_CUST_ID = ''''' + @H6 + ''''' AND CIS_HIER_LVL_CD = ''''H6'''' AND CSG_LVL IS NOT NULL AND ROWNUM <= 1 '')'
		END
		ELSE IF (
				(@H1 = '')
				AND (@H6 = '')
				AND (@M5OrdrNbr <> '')
				)
		BEGIN
			SET @SrcdQuery = 'SELECT CSG_LVL
				FROM OPENQUERY(M5,''SELECT DISTINCT a.CSG_LVL
											FROM MACH5.V_V5U_ORDR  o
											JOIN MACH5.v_v5U_cust_acct a on o.PRNT_ACCT_ID = a.CUST_ACCT_ID AND ((a.CIS_HIER_LVL_CD = ''''H1'''') OR (a.CIS_HIER_LVL_CD = ''''H6''''))
											WHERE o.ORDER_NBR = ''''' + UPPER(@M5OrdrNbr) + ''''' AND a.CSG_LVL IS NOT NULL AND ROWNUM <= 1 '')'
		END
		ELSE IF (
				(@H1 <> '')
				AND (@H6 = '')
				AND (@M5OrdrNbr <> '')
				)
		BEGIN
			SET @SrcdQuery = 'SELECT CSG_LVL
				FROM OPENQUERY(M5,''SELECT DISTINCT a.CSG_LVL
											FROM MACH5.V_V5U_ORDR  o
											JOIN MACH5.v_v5U_cust_acct a on o.PRNT_ACCT_ID = a.CUST_ACCT_ID AND a.CIS_HIER_LVL_CD = ''''H1''''
											WHERE o.ORDER_NBR = ''''' + UPPER(@M5OrdrNbr) + ''''' AND a.CIS_CUST_ID = ''''' + @H1 + ''''' AND a.CSG_LVL IS NOT NULL AND ROWNUM <= 1 '')'
		END
		ELSE IF (
				(@H1 = '')
				AND (@H6 <> '')
				AND (@M5OrdrNbr <> '')
				)
		BEGIN
			SET @SrcdQuery = 'SELECT CSG_LVL
				FROM OPENQUERY(M5,''SELECT DISTINCT a.CSG_LVL
											FROM MACH5.V_V5U_ORDR  o
											JOIN MACH5.v_v5U_cust_acct a on o.PRNT_ACCT_ID = a.CUST_ACCT_ID AND a.CIS_HIER_LVL_CD = ''''H6''''
											WHERE o.ORDER_NBR = ''''' + UPPER(@M5OrdrNbr) + ''''' AND a.CIS_CUST_ID = ''''' + @H6 + ''''' AND a.CSG_LVL IS NOT NULL AND ROWNUM <= 1 '')'
		END
		ELSE IF (
				(@H1 <> '')
				AND (@H6 <> '')
				AND (@M5OrdrNbr = '')
				)
		BEGIN
			SET @SrcdQuery = 'SELECT CSG_LVL
				FROM OPENQUERY(M5,''SELECT DISTINCT a.CSG_LVL
											FROM MACH5.v_v5U_cust_acct a on o.PRNT_ACCT_ID = a.CUST_ACCT_ID AND a.CIS_HIER_LVL_CD = ''''H1''''
											JOIN MACH5.v_v5U_cust_acct b on o.PRNT_ACCT_ID = b.CUST_ACCT_ID AND b.CIS_HIER_LVL_CD = ''''H6''''
											WHERE a.CIS_CUST_ID = ''''' + @H1 + ''''' AND b.CIS_CUST_ID = ''''' + @H6 + ''''' AND a.CSG_LVL IS NOT NULL AND b.CSG_LVL IS NOT NULL ROWNUM <= 1 '')'
		END
		ELSE IF (
				(@H1 <> '')
				AND (@H6 <> '')
				AND (@M5OrdrNbr <> '')
				)
		BEGIN
			SET @SrcdQuery = 'SELECT CSG_LVL
				FROM OPENQUERY(M5,''SELECT DISTINCT a.CSG_LVL
											FROM MACH5.V_V5U_ORDR  o
											JOIN MACH5.v_v5U_cust_acct a on o.PRNT_ACCT_ID = a.CUST_ACCT_ID AND a.CIS_HIER_LVL_CD = ''''H1''''
											JOIN MACH5.v_v5U_cust_acct b on o.PRNT_ACCT_ID = b.CUST_ACCT_ID AND b.CIS_HIER_LVL_CD = ''''H6''''
											WHERE o.ORDER_NBR = ''''' + UPPER(@M5OrdrNbr) + ''''' AND a.CIS_CUST_ID = ''''' + @H1 + ''''' AND b.CIS_CUST_ID = ''''' + @H6 + ''''' AND a.CSG_LVL IS NOT NULL AND b.CSG_LVL IS NOT NULL ROWNUM <= 1 '')'
		END

		INSERT INTO #M5Table (CSG_LVL)
		EXEC sp_executesql @SrcdQuery

		SELECT DISTINCT CSG_LVL
		FROM #M5Table
		ORDER BY CSG_LVL ASC
	END TRY

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
END
