USE [COWS]
GO
_CreateObject 'SP','web','getFTAvailEvents'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
 ================================================================================================  
 Author:  jrg7298  
 Create date: 7/11/2012  
 Description: Get the List of MDS FT Events using Resource Avail ID.
 ================================================================================================  
*/

ALTER PROCEDURE [web].[getFTAvailEvents]  --202
	@ResourceID			Int
 AS
BEGIN  
 
 SET NOCOUNT ON;  
  
 BEGIN TRY  

	OPEN SYMMETRIC KEY FS@K3y 
			DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

	SELECT DISTINCT CASE me.ESCL_CD WHEN 1 THEN 'YES' ELSE 'NO' END AS ESCLYN,
		CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE me.CUST_NME END AS CUST_NME,
		le.EVENT_STUS_DES,
		dbo.GetPrevMDSFTUserByEventID (me.EVENT_ID) AS AssignedUsers,
		me.EVENT_ID, 0 as IsMDSNew
	FROM dbo.EVENT_TYPE_RSRC_AVLBLTY et WITH (NOLOCK)
	CROSS APPLY EVENT_ID_LIST_TXT.nodes('EventIDs/Active/EventID') T(c)
	INNER JOIN dbo.MDS_EVENT_NEW me WITH (NOLOCK) ON me.EVENT_ID = t.c.value('.', 'int')
	INNER JOIN dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID = me.EVENT_ID 
	INNER JOIN dbo.LK_EVENT_STUS le WITH (NOLOCK) ON me.EVENT_STUS_ID = le.EVENT_STUS_ID
	WHERE et.EVENT_TYPE_RSRC_AVLBLTY_ID = @ResourceID
	  AND et.EVENT_TYPE_ID = 5
	UNION
	SELECT DISTINCT CASE me.ESCL_CD WHEN 1 THEN 'YES' ELSE 'NO' END AS ESCLYN,
		CASE WHEN (ev.CSG_LVL_ID>0) THEN 'Private Customer' ELSE me.CUST_NME END AS CUST_NME,
		le.EVENT_STUS_DES,
		dbo.GetPrevMDSFTUserByEventID (me.EVENT_ID) AS AssignedUsers,
		me.EVENT_ID, 1 as IsMDSNew
	FROM dbo.EVENT_TYPE_RSRC_AVLBLTY et WITH (NOLOCK)
	CROSS APPLY EVENT_ID_LIST_TXT.nodes('EventIDs/Active/EventID') T(c)
	INNER JOIN dbo.MDS_EVENT me WITH (NOLOCK) ON me.EVENT_ID = t.c.value('.', 'int')
	INNER JOIN dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID = me.EVENT_ID 
	INNER JOIN dbo.LK_EVENT_STUS le WITH (NOLOCK) ON me.EVENT_STUS_ID = le.EVENT_STUS_ID
	WHERE et.EVENT_TYPE_RSRC_AVLBLTY_ID = @ResourceID
	  AND et.EVENT_TYPE_ID = 5

 END TRY  
  
 BEGIN CATCH  
	EXEC [dbo].[insertErrorInfo]  
 END CATCH  
  
END
