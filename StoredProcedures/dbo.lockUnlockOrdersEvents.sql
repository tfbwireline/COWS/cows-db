USE [COWS]
GO
_CreateObject 'SP','dbo','lockUnlockOrdersEvents'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 08/26/2011
-- Description:	Locks/Unlocks the Orders and Events.
-- kh946640 02/13/2018 updated SP to Lock and Unlock an Order/Event.
-- =========================================================
ALTER PROCEDURE [dbo].[lockUnlockOrdersEvents]
	@ORDR_ID	Int	=	0
	,@EVENT_ID	Int	=	0
	,@USER_ID	Int
	,@IsOrder	Bit
	,@Unlock	Bit	
	,@IsLocked	Int	
	
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	IF	@Unlock	=	1
		BEGIN
			DELETE FROM dbo.EVENT_REC_LOCK	WITH (ROWLOCK)
				WHERE	LOCK_BY_USER_ID	=	@USER_ID and EVENT_ID = @EVENT_ID
			DELETE FROM dbo.ORDR_REC_LOCK	WITH (ROWLOCK)
				WHERE	LOCK_BY_USER_ID	=	@USER_ID and ORDR_ID = @ORDR_ID
			Select @IsLocked = 0
		END
	ELSE	
		BEGIN
			-----------------------------------------------------------
			--	Check the Event locks and then if NOT Locked lock it.
			-----------------------------------------------------------
			IF	((ISNULL(@IsOrder,0)	=	0) AND (ISNULL(@EVENT_ID,0) != 0))
				BEGIN										
					IF	NOT EXISTS
						(SELECT 'X' FROM dbo.EVENT_REC_LOCK	WITH (NOLOCK)
							WHERE	EVENT_ID = @EVENT_ID)
						BEGIN
							INSERT INTO dbo.EVENT_REC_LOCK	WITH (ROWLOCK)
										(EVENT_ID
										,STRT_REC_LOCK_TMST
										,LOCK_BY_USER_ID)
							VALUES	(@EVENT_ID
									,getDate()
									,@USER_ID)		
							Select @IsLocked = 0
						END		
					ELSE IF	EXISTS
						(SELECT 'X' FROM dbo.EVENT_REC_LOCK	WITH (NOLOCK)
							WHERE	EVENT_ID = @EVENT_ID and LOCK_BY_USER_ID = @USER_ID)
						BEGIN
							Select @IsLocked = 0
						END
					ELSE
						BEGIN
							Select @IsLocked = 1
						END
				END			
			ELSE
			-----------------------------------------------------------
			--	Check the Order locks and then if NOT Locked lock it.
			-----------------------------------------------------------
				BEGIN
					IF (ISNULL(@ORDR_ID,0) > 0)
					BEGIN
						IF	NOT EXISTS
							(SELECT 'X' FROM dbo.ORDR_REC_LOCK	WITH (NOLOCK)
								WHERE	ORDR_ID	= @ORDR_ID)
							BEGIN
								INSERT INTO dbo.ORDR_REC_LOCK	WITH (ROWLOCK)
											(ORDR_ID
											,STRT_REC_LOCK_TMST
											,LOCK_BY_USER_ID)
									VALUES	(@ORDR_ID
											,getDate()
											,@USER_ID)
								Select @IsLocked = 0
							END	
						ELSE IF	EXISTS
							(SELECT 'X' FROM dbo.ORDR_REC_LOCK	WITH (NOLOCK)
								WHERE	ORDR_ID	= @ORDR_ID and LOCK_BY_USER_ID = @USER_ID)
							BEGIN
								Select @IsLocked = 0
							END
						ELSE
							BEGIN
								Select @IsLocked = 1
							END
					END
				END						
		END
		
	SELECT ISNULL(@IsLocked,1)
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
	Select 0
End Catch
END