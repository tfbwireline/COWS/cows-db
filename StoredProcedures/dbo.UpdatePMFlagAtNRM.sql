USE [COWS]
GO
_CreateObject 'SP','dbo','UpdatePMFlagAtNRM'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  na157643            
-- Create date: 01/24/2018   
-- Description: Update PM flag at NRM using H6 from MDS Event
-- =============================================            
ALTER PROCEDURE [dbo].[UpdatePMFlagAtNRM]
	@H6 varchar(9),
	@NUA varchar(50),
	@IsDisc BIT,
	@EventID int
AS
Declare @dSQL nvarchar(MAX), @MNSCd char(3)='',
	@OptInCktCd char(1)='',
	@ReadyBeginCd char(1)=''
	
IF (@IsDisc=1)
BEGIN
	SET @MNSCd='No'
	SET @OptInCktCd='Y'
	SET @ReadyBeginCd='N'
END
ELSE
BEGIN
	SET @MNSCd='Yes'
	SET @OptInCktCd='N'
	SET @ReadyBeginCd='N'
END

DECLARE @NTE_TXT VARCHAR(1000) = 'MNS Flag updated to ' +@MNSCd+ ', PM Flag updated to ' +@optInCktCd+ ', Ready Begin Flag updated to '+@ReadyBeginCd+' for H6 : ' + @h6 + ', NUA : ' + @nua

	BEGIN

		BEGIN TRY
			DECLARE @UpdDT Varchar(19)
			SET @UpdDT = CONVERT(varchar,getdate(),110)
			
			IF (LEN(@readyBeginCd)>0) AND (LEN(@optInCktCd)>0)
				SET @dSQL = ' EXECUTE ( ''begin BPMF_OWNER.PKG_BPMF_COWS.proc_BPM_COWS_EPM_MNS_CHNG(''''' + @h6 + ''''',TO_DATE(''''' + @UpdDT + ''''',''''MM-DD-YYYY''''),''''' + LTRIM(RTRIM(@MNSCd)) + ''''',''''UPDATE'''',''''' + @nua + ''''',''''' + @optInCktCd + ''''',''''' + @readyBeginCd + '''''); end;'') AT NRMBPM; '
			
				
			EXEC sp_executesql @dSQL

			IF (LEN(@readyBeginCd)>0) AND (LEN(@optInCktCd)>0)
			BEGIN
				Insert into dbo.EVENT_HIST  (EVENT_ID,ACTN_ID,CMNT_TXT,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)
					VALUES (@EventID,20,@NTE_TXT,1,1,GETDATE(),GETDATE()) 
			END
	
		END TRY

		BEGIN CATCH
			EXEC dbo.insertErrorInfo
		END CATCH
	END