USE [COWS]
GO
_CreateObject 'SP','dbo','getODIEDiscoResponse'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		jrg7298
-- Create date: 03/25/2018
-- Description:	Gets the ODIE Disco Response using REQ_ID.
-- UPDATE - km967761 - 12/31/2020 - Insert new record to LK_DEV_MODEL if ODIE_DISC_REQ.MODEL_NME is NON-EXISTENT
-- =========================================================
ALTER PROCEDURE [dbo].[getODIEDiscoResponse]
	@REQ_ID				Int
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	IF OBJECT_ID(N'tempdb..#NRMBPMCkt', N'U') IS NOT NULL         
		DROP TABLE #NRMBPMCkt
	IF OBJECT_ID(N'tempdb..#TmpNRMBPMCKT', N'U') IS NOT NULL         
		DROP TABLE #TmpNRMBPMCKT

	DECLARE @H6s varchar(max)
		
	CREATE TABLE #NRMBPMCkt (H6_ID CHAR(9) NULL,
	OPT_IN_H_CD CHAR(9) NULL,
	OPT_IN_CKT_CD CHAR(9) NULL,
	READY_BEGIN_FLG_NME CHAR(9) NULL)
	
	INSERT INTO dbo.LK_DEV_MODEL (DEV_MODEL_ID, DEV_MODEL_NME, MIN_DRTN_TME_REQR_AMT, REC_STUS_ID, CREAT_BY_USER_ID, CREAT_DT, MANF_ID) 
	SELECT (SELECT MAX(DEV_MODEL_ID)+1 FROM LK_DEV_MODEL), a.MODEL_NME, 60, 1, 1, GETDATE(), d.MANF_ID
	FROM ODIE_DISC_REQ a WITH (NOLOCK)
		INNER JOIN ODIE_REQ b WITH (NOLOCK) ON b.REQ_ID = a.REQ_ID
		LEFT JOIN LK_DEV_MODEL c WITH (NOLOCK) ON c.DEV_MODEL_NME = a.MODEL_NME AND c.REC_STUS_ID = 1
		LEFT JOIN LK_DEV_MANF d WITH (NOLOCK) ON d.MANF_NME = a.VNDR_NME AND d.REC_STUS_ID = 1
	WHERE
		b.ODIE_MSG_ID = 4
		AND b.STUS_ID = 21
		AND b.REQ_ID = @REQ_ID
		AND c.DEV_MODEL_ID IS NULL
	ORDER BY a.ODIE_DISC_REQ_ID DESC
	
	SELECT DISTINCT odr.ODIE_DISC_REQ_ID,
    odr.H5_H6_CUST_ID,
    odr.MODEL_NME,
    odr.VNDR_NME,
    ISNULL(ldm.DEV_MODEL_ID,0) as DEV_MODEL_ID,
    odr.REDSGN_NBR,
    odr.DEV_NME as ODIE_DEV_NME,
    ISNULL(ldmf.MANF_ID,0) as MANF_ID,
    odr.SERIAL_NBR,
    odr.M5_DEVICE_ID,
    odr.SITE_ID,
    odr.CTRCT_NBR
	INTO #TmpNRMBPMCKT
	FROM dbo.ODIE_DISC_REQ odr WITH (NOLOCK) INNER JOIN
	dbo.ODIE_REQ orq WITH (NOLOCK) ON odr.REQ_ID=orq.REQ_ID LEFT JOIN
	dbo.LK_DEV_MODEL ldm WITH (NOLOCK) ON ldm.DEV_MODEL_NME=odr.MODEL_NME AND ldm.REC_STUS_ID=1 LEFT JOIN
	dbo.LK_DEV_MANF ldmf WITH (NOLOCK) ON ldmf.MANF_NME=odr.VNDR_NME AND ldmf.REC_STUS_ID=1
	WHERE orq.ODIE_MSG_ID=4
	AND orq.STUS_ID=21
	AND orq.REQ_ID=@REQ_ID
	ORDER BY odr.ODIE_DISC_REQ_ID DESC

	SELECT @H6s	=	ISNULL(@H6s,'') + ISNULL(CONVERT(VARCHAR,H5_H6_CUST_ID),'') + ''''', '''''
	FROM #TmpNRMBPMCKT
	WHERE H5_H6_CUST_ID IS NOT NULL

	IF (len(@H6s)>2)
		SET  @H6s = SUBSTRING(@H6s, 1, LEN(@H6s)-6)

	--SELECT orq.REQ_ID, odr.DEV_NME, COUNT(1) as cnt
	--FROM dbo.ODIE_DISC_REQ odr WITH (NOLOCK) INNER JOIN
	--dbo.ODIE_REQ orq WITH (NOLOCK) ON odr.REQ_ID=orq.REQ_ID LEFT JOIN
	--dbo.LK_DEV_MODEL ldm WITH (NOLOCK) ON ldm.DEV_MODEL_NME=odr.MODEL_NME LEFT JOIN
	--dbo.LK_DEV_MANF ldmf WITH (NOLOCK) ON ldmf.MANF_NME=odr.VNDR_NME
	--WHERE orq.ODIE_MSG_ID=4
	--AND orq.STUS_ID=21
	----AND orq.REQ_ID=
	--GROUP BY orq.REQ_ID, odr.DEV_NME
	--HAVING COUNT(1)>1
	--56146,55869
	
	IF (LEN(@H6s)>2)
	BEGIN
		declare @cktinfo nvarchar(max) = 'select *
					from openquery(NRMBPM,''select DISTINCT H6_ID, OPT_IN_H_CD, OPT_IN_CKT_CD, READY_BEGIN_FLG_NME
											from BPMF_OWNER.V_BPMF_MONTR_COWS_CKT
											WHERE H6_ID IN (''''' + @H6s + ''''') '')'
		INSERT INTO #NRMBPMCkt
		exec sp_executesql @cktinfo
	END

	SELECT DISTINCT tmp.ODIE_DISC_REQ_ID,
    tmp.H5_H6_CUST_ID,
    tmp.MODEL_NME,
    tmp.VNDR_NME,
    tmp.DEV_MODEL_ID,
    tmp.REDSGN_NBR,
    tmp.ODIE_DEV_NME,
    tmp.MANF_ID,
    tmp.SERIAL_NBR,
    tmp.M5_DEVICE_ID,
    tmp.SITE_ID,
    tmp.CTRCT_NBR,
	LTRIM(RTRIM(nrm.OPT_IN_H_CD)) as OPT_IN_H_CD,
	LTRIM(RTRIM(nrm.OPT_IN_CKT_CD)) as OPT_IN_CKT_CD,
	LTRIM(RTRIM(nrm.READY_BEGIN_FLG_NME)) as READY_BEGIN_FLG_NME
	FROM #TmpNRMBPMCKT tmp LEFT JOIN
	#NRMBPMCkt nrm ON nrm.H6_ID=tmp.H5_H6_CUST_ID


End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END
