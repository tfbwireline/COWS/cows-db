USE COWS
GO
_CreateObject 'SP','dbo','CompleteActiveTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Created By:	Joseph Nixon
-- Modified By: Ramesh Ragi
-- Created Ts:	07/27/2011
-- Description:	Completes the provided task and profile/pattern if applicable.
-- ===========================================================================
       
ALTER PROCEDURE [dbo].[CompleteActiveTask]      
	@OrderID		INT,      
	@TaskID			INT,      
	@TaskStatus		SMALLINT,      
	@Comments		VARCHAR(max) = NULL,
	@UserID			Int  = NULL,
	@NteType		TinyInt = NULL
AS      
BEGIN
	SET DEADLOCK_PRIORITY 10
	BEGIN TRY
		
		--Added: SM was loading next tasks even though current taskID was not loaded
		IF EXISTS
			(
				SELECT 'X'
					FROM	dbo.ACT_TASK WITH (NOLOCK) 
					WHERE	ORDR_ID = @OrderID 
						AND TASK_ID = @TaskID 
						AND STUS_ID = 0	
			)
			BEGIN
				----------------------------------------------------------
				-- BEGIN INSERT NOTES
				----------------------------------------------------------
				-- Reset the Status
				DECLARE @TempStatus TINYINT
				SET	@TempStatus	=	CASE
										WHEN @TaskStatus >= 2	THEN 	2
										ELSE @TaskStatus
									END

				If ISNULL(@NteType,0) = 0
					SELECT @NteType = NTE_TYPE_ID FROM dbo.LK_NTE_TYPE WITH (NOLOCK) WHERE NTE_TYPE_DES = 'SM'
		
		
				IF ISNULL(@NteType,0) <> 0
					EXEC dbo.insertOrderNotes  @OrderID,@NteType,@Comments,@UserID	
			
				----------------------------------------------------------
				-- BEGIN TASK RULES FOR CURRENT TASKS
				----------------------------------------------------------
				IF EXISTS (SELECT 'Y' FROM dbo.LK_TASK_RULE WITH (NOLOCK) WHERE TASK_ID = @TaskID AND TASK_OPT_ID = @TaskStatus)
				BEGIN
					EXEC dbo.CompleteTaskRule @OrderID, @TaskID, @TaskStatus, ''     
				END		
				--------------------------------------------------------
				-- END TASK RULES FOR CURRENT TASKS
				----------------------------------------------------------
		
				----------------------------------------------------------
				-- BEGIN GET NEXT TASKS
				----------------------------------------------------------
				DECLARE @NextTasks TABLE
				(
					 OrderID			INT
					,SMID				INT
					,PreReqWGPatternID	INT
					,PreReqWGProfileID	INT
					,PreReqTaskID		SMALLINT
					,DesiredWGPatternID	INT
					,DesiredWGProfileID	INT
					,DesiredTaskID		SMALLINT
					,HasDependentTasks	BIT
					,HasDependentProfiles	BIT
				)

				DECLARE @ProfileID SMALLINT
				SELECT @ProfileID = WG_PROF_ID FROM dbo.ACT_TASK WITH (NOLOCK) WHERE TASK_ID = @TaskID AND ORDR_ID = @OrderID

				INSERT INTO @NextTasks
				SELECT * FROM dbo.getNextTasks(@OrderID, @TaskID, @TaskStatus, @ProfileID)
				----------------------------------------------------------
				-- END GET NEXT TASKS
				----------------------------------------------------------

				----------------------------------------------------------
				-- BEGIN UPDATE CURRENT TASK STATUS
				----------------------------------------------------------
				-- Complete the current pattern if...
				--	1. The desired pattern is different than the prereq pattern
				--	2. There are no dependent tasks and no dependent profiles
				--	3. The exception category allows to complete the current pattern
				UPDATE	wps
				SET		wps.STUS_ID = 2
				FROM	dbo.WG_PTRN_STUS wps WITH (ROWLOCK)
						INNER JOIN @NextTasks nt
					ON wps.ORDR_ID = nt.OrderID
					AND wps.WG_PTRN_ID = nt.PreReqWGPatternID
				WHERE nt.DesiredWGPatternID <> nt.PreReqWGPatternID
				AND nt.HasDependentTasks = 0
				AND nt.HasDependentProfiles = 0
		

				-- Insert the completed pattern if it does NOT already exist and...
				--	1. The desired pattern is different than the prereq pattern
				--	2. There are no dependent tasks and no dependent profiles
				--	3. The exception category allows to complete the current pattern
				INSERT INTO dbo.WG_PTRN_STUS WITH (ROWLOCK)
				(ORDR_ID, WG_PTRN_ID, STUS_ID)
				SELECT DISTINCT nt.OrderID, nt.PreReqWGPatternID, 2
				FROM @NextTasks nt
				INNER JOIN dbo.LK_WG_PTRN wpt WITH (NOLOCK) ON nt.PreReqWGPatternID = wpt.WG_PTRN_ID
				WHERE nt.DesiredWGPatternID <> nt.PreReqWGPatternID
				AND nt.HasDependentTasks = 0
				AND nt.HasDependentProfiles = 0
				AND NOT EXISTS
				(
						SELECT 'X'
						FROM dbo.WG_PTRN_STUS wps WITH (NOLOCK)
						WHERE wps.ORDR_ID = nt.OrderID
						AND wps.WG_PTRN_ID = nt.PreReqWGPatternID
				)

				-- Complete the current profile if...
				--	1. The desired profile is different than the prereq profile
				--	2. There are no dependent tasks
				--	(It doesn't matter if there are dependent profiles)
				--	3. The exception category allows to complete the current profile
				UPDATE wps
				SET wps.STUS_ID = 2
				FROM dbo.WG_PROF_STUS wps WITH (ROWLOCK)
				INNER JOIN @NextTasks nt
					ON wps.ORDR_ID = nt.OrderID
					AND wps.WG_PTRN_ID = nt.PreReqWGPatternID
					AND wps.WG_PROF_ID = nt.PreReqWGProfileID
				WHERE nt.DesiredWGProfileID <> nt.PreReqWGProfileID
				AND nt.HasDependentTasks = 0

				-- Insert the completed profile if it does NOT already exist and...
				--	1. The desired profile is different than the prereq profile
				--	2. There are no dependent tasks
				--	(It doesn't matter if there are dependent profiles)
				--	3. The exception category allows to complete the current profile
				INSERT INTO dbo.WG_PROF_STUS WITH (ROWLOCK)
				(ORDR_ID, WG_PTRN_ID, WG_PROF_ID, STUS_ID)
				SELECT DISTINCT nt.OrderID, nt.PreReqWGPatternID, nt.PreReqWGProfileID, 2
				FROM @NextTasks nt
				INNER JOIN dbo.LK_WG_PTRN wpt WITH (NOLOCK) ON nt.PreReqWGPatternID = wpt.WG_PTRN_ID
				INNER JOIN dbo.LK_WG_PROF wpf WITH (NOLOCK) ON nt.PreReqWGProfileID = wpf.WG_PROF_ID
				WHERE nt.DesiredWGProfileID <> nt.PreReqWGProfileID
				AND nt.HasDependentTasks = 0
				AND NOT EXISTS
				(
						SELECT 'X'
						FROM dbo.WG_PROF_STUS wps WITH (NOLOCK)
						WHERE wps.ORDR_ID = nt.OrderID
						AND wps.WG_PROF_ID = nt.PreReqWGProfileID
						AND wps.WG_PTRN_ID = nt.PreReqWGPatternID
				)

				-- Complete the current task if...
				--	1. The current task has a next task to go to
				UPDATE cat
				SET	STUS_ID = @TempStatus,
					MODFD_DT = GETDATE()
				FROM dbo.ACT_TASK cat WITH (ROWLOCK)
				INNER JOIN @NextTasks nt ON cat.ORDR_ID = nt.OrderID
				AND	cat.TASK_ID = nt.PreReqTaskID
				----------------------------------------------------------
				-- END UPDATE CURRENT TASK STATUS
				----------------------------------------------------------

				------------------------------------------------------------
				---- BEGIN TASK RULES FOR CURRENT TASKS
				------------------------------------------------------------
				--IF EXISTS (SELECT 'Y' FROM dbo.LK_TASK_RULE WITH (NOLOCK) WHERE TASK_ID = @TaskID AND TASK_STUS_ID = @TempStatus)
				--BEGIN
				--	EXEC dbo.CompleteTaskRule @OrderID, @TaskID, @TaskStatus, ''     
				--END		
				----------------------------------------------------------
				---- END TASK RULES FOR CURRENT TASKS
				------------------------------------------------------------

				----------------------------------------------------------
				-- BEGIN CLEAN UP CURRENT TASKS
				----------------------------------------------------------
				-- Remove all patterns & profiles if...
				--	1. There are no dependent tasks and no dependent profiles
				--	2. The desired pattern is the end
				--	3. The desired profile is the end
				--	4. The desired task is the end
				--			OR
				--	1.	The exception category
				--DELETE			wps
				--	FROM		dbo.WG_PTRN_STUS wps WITH (ROWLOCK)
				--	INNER JOIN	@NextTasks			nt	ON	wps.ORDR_ID	=	nt.OrderID
				--	WHERE		(nt.HasDependentTasks	=	0
				--			AND nt.HasDependentProfiles =	0
				--			AND nt.DesiredWGPatternID	=	1
				--			AND nt.DesiredWGProfileID	=	1
				--			AND nt.DesiredTaskID		=	1)

				--DELETE			wps
				--	FROM		dbo.WG_PROF_STUS wps WITH (ROWLOCK)
				--	INNER JOIN	@NextTasks			nt	ON	wps.ORDR_ID	=	nt.OrderID
				--	WHERE		 (nt.HasDependentTasks	=	0
				--			AND nt.HasDependentProfiles =	0
				--			AND nt.DesiredWGPatternID	=	1
				--			AND nt.DesiredWGProfileID	=	1
				--			AND nt.DesiredTaskID		=	1)

				---- Remove the tasks within the current profile if...
				----	1. The desired profile is different than the prereq profile
				----	2. There are no dependent tasks
				----	(It doesn't matter if there are dependent profiles)
				----	3. The exception category allows to complete the current profile
		
				---- Commented by sh446624 for PJ002133: Ticket #9131547,Ticket #9345071 
				
				--DELETE			cat
				--	FROM		dbo.ACT_TASK cat WITH (ROWLOCK)
				--	INNER JOIN	@NextTasks			nt	ON	cat.ORDR_ID		=	nt.OrderID
				--										AND	cat.WG_PROF_ID	=	nt.PreReqWGProfileID
				--	WHERE		nt.DesiredWGProfileID		<>	nt.PreReqWGProfileID
				--			AND nt.HasDependentTasks		=	0
					
				----------------------------------------------------------
				-- END CLEAN UP CURRENT TASKS
				----------------------------------------------------------

				----------------------------------------------------------
				-- BEGIN LOAD NEXT TASKS
				----------------------------------------------------------
				-- Reset the next pattern status if...
				--	1. There are no dependent tasks and no dependent profiles
				--	2. It already exists
				UPDATE wps
				SET STUS_ID = 0
				FROM dbo.WG_PTRN_STUS wps WITH (ROWLOCK)
				INNER JOIN @NextTasks nt
					ON wps.ORDR_ID = nt.OrderID
					AND wps.WG_PTRN_ID = nt.DesiredWGPatternID
				WHERE nt.HasDependentTasks = 0
				AND nt.HasDependentProfiles = 0

				-- Insert the next pattern if...
				--	1. There are no dependent tasks and no dependent profiles
				--	2. The desired pattern is not the end
				--	3. It does NOT already exist
				INSERT INTO dbo.WG_PTRN_STUS
				SELECT DISTINCT
					 OrderID				
					,DesiredWGPatternID
					,0
					,GETDATE()
				FROM @NextTasks nt
				WHERE nt.HasDependentTasks = 0
				AND nt.HasDependentProfiles = 0
				AND nt.DesiredWGPatternID <> 1
				AND NOT EXISTS
				(
					SELECT 'X'
					FROM dbo.WG_PTRN_STUS wps WITH (NOLOCK)
					WHERE wps.ORDR_ID = nt.OrderID
					AND wps.WG_PTRN_ID = nt.DesiredWGPatternID
				)
		
				-- Reset the next profile status if...
				--  1. There are no dependent tasks and no dependent profiles
				--  2. It already exists
				UPDATE wps
				SET  STUS_ID = 0
				FROM dbo.WG_PROF_STUS wps WITH (ROWLOCK)
				INNER JOIN @NextTasks nt
					ON wps.ORDR_ID = nt.OrderID
					AND wps.WG_PTRN_ID = nt.DesiredWGPatternID
					AND wps.WG_PROF_ID = nt.DesiredWGProfileID
				WHERE nt.HasDependentTasks = 0
				AND nt.HasDependentProfiles = 0

				-- Insert the next profile if...
				--	1. There are no dependent tasks and no dependent profiles
				--	2. The desired profile is not the end
				--	3. It does NOT already exist
				INSERT INTO dbo.WG_PROF_STUS				
				SELECT DISTINCT
					 OrderID				
					,DesiredWGProfileID
					,DesiredWGPatternID
					,0
					,GETDATE()
				FROM @NextTasks nt
				WHERE nt.HasDependentTasks = 0
				AND nt.HasDependentProfiles = 0
				AND nt.DesiredWGProfileID <> 1
				AND NOT EXISTS
				(
					SELECT 'X'
					FROM dbo.WG_PROF_STUS wps WITH (NOLOCK)
					WHERE wps.ORDR_ID = nt.OrderID
					AND wps.WG_PTRN_ID = nt.DesiredWGPatternID
					AND wps.WG_PROF_ID = nt.DesiredWGProfileID
				)

				-- Reset the next task status if...
				--  1. There are no dependent tasks and no dependent profiles
				--  2. It already exists
				UPDATE cat
				SET STUS_ID = 0,
					WG_PROF_ID = nt.DesiredWGProfileID,
					MODFD_DT = GETDATE()
				FROM dbo.ACT_TASK cat WITH (ROWLOCK)
				INNER JOIN @NextTasks nt
					ON cat.ORDR_ID = nt.OrderID
					--AND cat.WG_PROF_ID = nt.DesiredWGProfileID
					AND cat.TASK_ID = nt.DesiredTaskID
				WHERE nt.HasDependentTasks = 0
				AND nt.HasDependentProfiles = 0

				-- Insert the next task if...
				--  1. There are no dependent tasks and no dependent profiles
				--	2. The desired task is not the end
				--  3. It does NOT already exist
				INSERT INTO dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT) 
				SELECT DISTINCT
					 OrderID
					,DesiredTaskID
					,0
					,DesiredWGProfileID
					,GETDATE()
				FROM @NextTasks nt
				WHERE nt.HasDependentTasks = 0
				AND nt.HasDependentProfiles = 0
				AND nt.DesiredTaskID <> 1
				AND NOT EXISTS
				(
					SELECT 'X'
					FROM dbo.ACT_TASK cat WITH (NOLOCK)
					WHERE cat.ORDR_ID = nt.OrderID
					AND cat.WG_PROF_ID = nt.DesiredWGProfileID
					AND cat.TASK_ID = nt.DesiredTaskID
				)
				----------------------------------------------------------
				-- END LOAD NEXT TASKS
				----------------------------------------------------------

				--TO DO List
				----------------------------------------------------------
				-- BEGIN TASK NOTES AND TASK RULES FOR NEXT TASKS
				----------------------------------------------------------
				DECLARE @DesiredTasks TABLE
				(
					 ident			INT IDENTITY
					,DesiredTaskID	SMALLINT		
					,RunTaskRule	BIT
				)

				-- Get tasks for executing task rules
				INSERT INTO @DesiredTasks
				SELECT DISTINCT
					 nt.DesiredTaskID
					,CASE 
						WHEN EXISTS
							(
								SELECT 'X'
								FROM dbo.LK_TASK_RULE tr WITH (NOLOCK)
								WHERE tr.TASK_ID = nt.DesiredTaskID
								AND tr.TASK_OPT_ID = 0
							)
						THEN 1	ELSE 0
					END AS RunTaskRule
				FROM @NextTasks	nt
				WHERE nt.HasDependentTasks = 0
				AND nt.HasDependentProfiles = 0

				DECLARE @DesiredTaskID	SMALLINT
				DECLARE @RunTaskRule	BIT
		
				DECLARE @Ctr TINYINT
				DECLARE @Cnt TINYINT
				SET	@Ctr = 1  
				SET @Cnt = 0
		
				SELECT @Cnt = COUNT(1) FROM @DesiredTasks

				IF @Cnt > 0
				BEGIN
					WHILE @Ctr <= @Cnt
					BEGIN
						SET @DesiredTaskID = 0
						SET @RunTaskRule = 0				
				
						SELECT
							 @DesiredTaskID = dt.DesiredTaskID
							,@RunTaskRule = dt.RunTaskRule					
						FROM @DesiredTasks dt
						WHERE dt.ident = @Ctr
				
				
				
						IF (@OrderID != '')
						BEGIN
							-- Insert notes into the captasknotes table
							--EXEC dbo.InsertCaptTaskNotes @SprintTNIDList, @LSRID, 0, @DesiredTaskID, 0, ''
					
							-- Execute the task rule
							IF (@RunTaskRule = 1)
							BEGIN
								EXEC dbo.CompleteTaskRule @OrderID, @DesiredTaskID, 0, ''
							END
						END			
						SET @Ctr = @Ctr + 1  
					END
				END
				----------------------------------------------------------
				-- END TASK NOTES AND TASK RULES FOR NEXT TASKS
				----------------------------------------------------------
			END	
    END TRY      
	  
	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]      
	END CATCH    
	
END