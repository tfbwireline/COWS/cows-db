USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getCpeLineItemsAll_V5U]    Script Date: 03/10/2017 07:58:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 5/4/2015
	-- Description:	Appian CPE LINE ITEM Info.
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getCpeLineItemsAll_V5U]
	@OrderID	Int = 0
	AS
	BEGIN

	DECLARE @V5U_CPE_ORDR_INFO TABLE
			(
				ORDR_ID	int,
				FTN	Varchar(50),
				ACT Varchar(10)
			)
			
IF(@OrderID > 0)
	BEGIN
		INSERT INTO @V5U_CPE_ORDR_INFO (ORDR_ID, FTN, ACT) SELECT ORDR_ID, FTN, ACT FROM  OPENQUERY(local, '[COWS].[dbo].[getCpeOrdrInfo_V5U]') where ORDR_ID = @OrderID
	END
ELSE
	BEGIN
		INSERT INTO @V5U_CPE_ORDR_INFO (ORDR_ID, FTN, ACT) SELECT ORDR_ID, FTN, ACT FROM  OPENQUERY(local, '[COWS].[dbo].[getCpeOrdrInfo_V5U]')
	END
	
	SELECT	DISTINCT	 litm.FSA_CPE_LINE_ITEM_ID 
				,fo.ORDR_ID      
				,fo.FTN                          											   
				,litm.CMPNT_ID
                ,litm.EQPT_TYPE_ID
                ,litm.MDS_DES
                ,litm.EQPT_ID	
                ,litm.MFR_NME
                ,litm.CNTRC_TYPE_ID
                ,litm.MNTC_CD	
				,litm.SRVC_LINE_ITEM_CD
				,litm.INSTLN_CD
				,litm.DISC_PL_NBR
				,litm.DISC_FMS_CKT_NBR
				,litm.DISC_NW_ADR
				,litm.LINE_ITEM_CD
				,litm.CXR_CKT_ID
				,litm.TTRPT_ACCS_TYPE_CD
				,litm.TTRPT_SPD_OF_SRVC_BDWD_DES
				,litm.TTRPT_ACCS_TYPE_DES 
				,litm.PLSFT_RQSTN_NBR
				,litm.RQSTN_DT
				,litm.PRCH_ORDR_NBR
				,litm.EQPT_ITM_RCVD_DT
				,litm.ORDR_QTY  -- changed name
				,litm.MATL_CD
				,litm.UNIT_MSR 
				-- new fields
				,litm.UNIT_PRICE
				,litm.MANF_PART_CD
				,litm.VNDR_CD
				, litm.SUPPLIER    AS VNDR_NME
				, litm.MANF_DISCNT_CD     
				,CASE
					WHEN ISNULL(litm.MANF_DISCNT_CD,'') <> '' THEN 'Y'
					ELSE ''
					END								AS DEAL_ID
				, litm.EQPT_RCVD_BY_ADID			AS EQPT_RCVD_BY_ADID
				,litm.ITM_STUS                     AS ITM_STUS
				,litm.CMPL_DT                      AS CMPL_DT
				,litm.PO_LN_NBR                             AS PO_LN_NBR
				,litm.RCVD_QTY						AS RCVD_QTY
				,litm.PS_RCVD_STUS				AS PS_RCVD_STUS
				,litm.PID						AS PID
				,litm.DROP_SHP					AS DROP_SHP
				,litm.DEVICE_ID					AS DEVICE_ID
				,litm.SUPPLIER                  AS SUPPLIER
				,litm.CMPNT_FMLY                AS CMPNT_FMLY
				,litm.RLTD_CMPNT_ID				AS RLTD_CMPNT_ID
				,CASE 
					WHEN ISNULL(litm.CNTRC_TYPE_ID,'') <> '' THEN Substring(litm.CNTRC_TYPE_ID,1,1)
					--WHEN ISNULL(litm.CNTRC_TYPE_ID,'') = '' THEN  SUBSTRING(ritm.CNTRC_TYPE_ID,1,1)
					ELSE '' 
				END AS CT
				,CASE 
					WHEN ISNULL(litm.CPE_REUSE_CD,0) = 0 THEN 'N'
					WHEN ISNULL(litm.CPE_REUSE_CD,0) > 0 THEN 'Y'
				END								AS REUSE
	

	FROM         @V5U_CPE_ORDR_INFO fo
	INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
	WHERE
	 (ISNULL(litm.DEVICE_ID, '') <> '') AND (fo.ACT <> 'CAN') AND (litm.ITM_STUS = 401) OR
     (ISNULL(litm.DEVICE_ID, '') <> '') AND (fo.ACT = 'CAN') AND (litm.ITM_STUS = 402)
		
		

	END


