USE [COWS]
GO
_CreateObject 'SP','web','getSTDIInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <03/15/2013>
-- Description:	<to retrieve standard interval data>
-- =============================================
ALTER PROCEDURE web.getSTDIInfo 
	@ORDR_ID INT
AS
BEGIN
	BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT STDI_REAS_ID,STDI_REAS_DES FROM dbo.LK_STDI_REAS WITH (NOLOCK) WHERE REC_STUS_ID = 1
    SELECT	lsr.STDI_REAS_ID,dbo.convertOrderMilestonedates(t.STDI_DT) AS STDI_DT
		FROM	dbo.TRPT_ORDR t WITH (NOLOCK) 
	INNER JOIN	dbo.LK_STDI_REAS lsr WITH (NOLOCK) ON t.STDI_REAS_ID = lsr.STDI_REAS_ID
		WHERE	ORDR_ID = @ORDR_ID
	
	SELECT	osh.ORDR_STDI_ID AS ORDR_STDI_ID,
			dbo.convertOrderMilestonedates(osh.STDI_DT) AS STDI_DT,
			lsr.STDI_REAS_DES AS STDI_REAS_DES,
			cusr.FULL_NME AS CREATE_BY_USER,
			musr.FULL_NME AS MODIFY_BY_USER,
			dbo.convertOrderMilestonedates(osh.CREAT_DT) AS CREAT_DT,
			dbo.convertOrderMilestonedates(osh.MODFD_DT) AS MODFD_DT,
			ISNULL(osh.CMNT_TXT,'') AS COMMENTS
		FROM	dbo.ORDR_STDI_HIST osh WITH (NOLOCK) 
	INNER JOIN	dbo.LK_STDI_REAS lsr WITH (NOLOCK) ON osh.STDI_REAS_ID = lsr.STDI_REAS_ID
	LEFT JOIN	dbo.LK_USER cusr WITH (NOLOCK) ON cusr.USER_ID = osh.CREAT_BY_USER_ID
	LEFT JOIN	dbo.LK_USER musr WITH (NOLOCK) ON musr.USER_ID = osh.MODFD_BY_USER_ID
		WHERE	ORDR_ID = @ORDR_ID 
		ORDER BY osh.CREAT_DT DESC
    
    END TRY
    BEGIN CATCH
		EXEC dbo.insertErrorInfo
    END CATCH
END
GO
