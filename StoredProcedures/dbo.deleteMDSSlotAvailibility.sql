USE [COWS]
GO
_CreateObject 'SP','dbo','deleteMDSSlotAvailibility'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:		sbg9814
-- Create date: 12/22/2011
-- Description:	Delete a given Slot.
-- =============================================================================
ALTER PROCEDURE [dbo].[deleteMDSSlotAvailibility]
	@ASN_USER_ID		Int	=	0
	,@EVENT_ID			Int
	,@OLD_STRT_TMST		DATETIME
	,@OLD_TME_SLOT_ID	TINYINT
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	
	DECLARE @Today DATETIME
	SET @Today = GETDATE()
	DECLARE @ListTxt XML
	DECLARE @EvCnt INT

	DELETE FROM dbo.APPT	WITH (ROWLOCK)
		WHERE	EVENT_ID	=	@EVENT_ID

	UPDATE		dbo.EVENT_ASN_TO_USER	WITH (ROWLOCK)
		SET		REC_STUS_ID		=	0,
				MODFD_DT		=	GETDATE()
		WHERE	ASN_TO_USER_ID	=	Case	@ASN_USER_ID
										When	0	Then	ASN_TO_USER_ID
										Else	@ASN_USER_ID
									End
		 AND	EVENT_ID		=	@EVENT_ID
	
	SELECT @ListTxt = EVENT_ID_LIST_TXT
	FROM dbo.EVENT_TYPE_RSRC_AVLBLTY	WITH (NOLOCK)
	WHERE	Convert(Varchar, DAY_DT,	101)	=	Convert(Varchar, @OLD_STRT_TMST,	101)
			AND	TME_SLOT_ID						=	@OLD_TME_SLOT_ID 
			AND	EVENT_TYPE_ID = 5

	IF @ListTxt IS NULL
		SET @ListTxt = '<EventIDs><Decrement><Event><ID>'+convert(varchar,@EVENT_ID)+'</ID><DT>'+convert(varchar,GETDATE(), 126)+'</DT></Event></Decrement><Increment /><Active /></EventIDs>'
	ELSE
	BEGIN
		SET @ListTxt.modify('insert <Event><ID>{sql:variable("@EVENT_ID")}</ID><DT>{sql:variable("@Today")}</DT></Event> into (/EventIDs/Decrement)[1]')
		SET @ListTxt.modify('delete /EventIDs/Active/EventID[text() = sql:variable("@EVENT_ID")]')
	END

	SET @EvCnt = COALESCE(@ListTxt.value('count(/EventIDs/Active/EventID)', 'INT'), 0)

	UPDATE		ea
		SET		CURR_AVAL_TME_SLOT_CNT	=	@EvCnt,
				EVENT_ID_LIST_TXT		=	@ListTxt,
				MODFD_DT				=	GETDATE()
		FROM	dbo.EVENT_TYPE_RSRC_AVLBLTY	ea	WITH (ROWLOCK)
		WHERE	Convert(Varchar, ea.DAY_DT,	101)	=	Convert(Varchar, @OLD_STRT_TMST,	101)
			AND	ea.TME_SLOT_ID						=	@OLD_TME_SLOT_ID 
			AND	ea.EVENT_TYPE_ID = 5

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END