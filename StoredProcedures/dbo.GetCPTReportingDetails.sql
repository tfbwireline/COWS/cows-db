USE [COWS]
GO
_CreateObject 'SP','dbo','GetCPTReportingDetails'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================================================================
-- Author:	qi931353
-- Create date: 11/17/2015
-- Description:	Get all CPT Reporting Details that is needed for CPT Report on the COWS Web Page.
-- Returns records for 5 reporting grid displayed on the web page.
-- 1. CPT requests created/submitted within 30 days.
-- 2. Shortname Needed - CPT requests without shortname.
-- 3. IsGatekeeperNeeded - CPT requests that needed Gatekeeper task when Manager is not yet assigned.
-- 4. IsManagerNeeded - CPT requests that needed Manager task when NTE is not yet assigned.
-- 5. IsPMNeeded - CPT requests that needed PM task when the request is not yet reviewed by PM.
-- Note: Commented IsNTENeeded for future reference.
-- =======================================================================================================
ALTER PROCEDURE [dbo].[GetCPTReportingDetails] 
AS
BEGIN
SET NOCOUNT ON;
Begin Try

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	DECLARE @TempSelect TABLE
	(
		ID					int IDENTITY (1, 1) PRIMARY KEY NOT NULL
		,CPTID				int
		,CPTNumber			varchar(13)
		,CPTStusID			int
		,CustomerName		varchar(100)
		,CompanyName		varchar(500)
		,CreatedDate		datetime
		,CustomerTypeID		int
		,CustomerType		varchar(100)
		,ServiceTierID		int
		,ServiceTier		varchar(100)
		,ReviewedByPM		bit
	)
	
	DECLARE @Temp TABLE
	(
		CPTID				int
		,CPTNumber			varchar(13)
		,CPTStusID			int
		,CustomerName		varchar(100)
		,CompanyName		varchar(500)
		,CreatedDate		datetime
		,CustomerTypeID		int
		,CustomerType		varchar(100)
		,ServiceTierID		int
		,ServiceTier		varchar(100)
		,IsGatekeeperNeeded	bit
		,GatekeeperID		int
		,GatekeeperName		varchar(100)
		,IsManagerNeeded	bit
		,ManagerID			int
		,ManagerName		varchar(100)
		--,IsNTENeeded		bit
		--,NTEID			int
		--,NTEName			varchar(100)
		,IsPMNeeded			bit
		,PMID				int
		,PMName				varchar(100)
	)
	
	DECLARE @counter				int
			,@maxCount				int
			,@cptID					int
			,@cptNumber				varchar(13)
			,@customerName			varchar(100)
			,@companyName			varchar(500)
			,@createdDate			datetime
			,@customerTypeID		int
			,@customerType			varchar(100)
			,@serviceTierID			int
			,@serviceTier			varchar(100)
			,@IsWpaas				bit
			,@IsPMReviewed			bit
			,@userID				int
			,@roleID				int
			,@IsGatekeeperNeeded	bit
			,@GatekeeperID			int
			,@Gatekeeper			varchar(100)
			,@IsManagerNeeded		bit
			,@ManagerID				int
			,@Manager				varchar(100)
			--,@IsNTENeeded			bit
			--,@NTEID				int
			--,@NTE					varchar(100)
			,@IsPMNeeded			bit
			,@PMID					int
			,@PM					varchar(100)
			,@CPTStusID				int
	
	-- Insert into @TempSelect table from result query
	INSERT INTO @TempSelect (CPTID, CPTNumber, CPTStusID, CustomerName, CompanyName,
		CreatedDate, ReviewedByPM, CustomerTypeID, CustomerType,
		ServiceTierID, ServiceTier)
	SELECT cpt.CPT_ID, cpt.CPT_CUST_NBR, cpt.CPT_STUS_ID, cpt.CUST_SHRT_NME,
		CASE WHEN (cpt.CSG_LVL_ID>0) THEN ISNULL(dbo.decryptBinaryData(csd.[CUST_NME]), '') ELSE ISNULL(cpt.[COMPNY_NME],'') END, cpt.CREAT_DT, cpt.REVWD_PM_CD,
		cptinfo.CPT_CUST_TYPE_ID, lkCustType.CPT_CUST_TYPE,
		cptinfo.CPT_PLN_SRVC_TIER_ID, lkServiceTier.CPT_PLN_SRVC_TIER
	FROM dbo.CPT cpt WITH (NOLOCK)
	JOIN dbo.CPT_RLTD_INFO cptinfo WITH (NOLOCK) ON cpt.CPT_ID = cptinfo.CPT_ID
	JOIN dbo.LK_CPT_CUST_TYPE lkCustType WITH (NOLOCK) ON cptinfo.CPT_CUST_TYPE_ID = lkCustType.CPT_CUST_TYPE_ID
	LEFT JOIN dbo.LK_CPT_PLN_SRVC_TIER lkServiceTier WITH (NOLOCK) ON cptinfo.CPT_PLN_SRVC_TIER_ID = lkServiceTier.CPT_PLN_SRVC_TIER_ID
	LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID = cpt.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
	WHERE cptinfo.CPT_CUST_TYPE_ID != 4 --Customer Type is not UnmanagedE2E
	AND cpt.REC_STUS_ID = 1 --Active CPT
	AND cpt.CPT_STUS_ID NOT IN (308) --CPT Status not Deleted
	AND DATEADD(day, 30, cpt.CREAT_DT) > GETDATE()
	
	
	SET @counter = 1
	SELECT @maxCount = COUNT(ID) FROM @TempSelect
	
	WHILE (@counter <= @maxCount)
	BEGIN
		SELECT	@cptID = ISNULL(CPTID, 0)
				,@cptNumber = ISNULL(CPTNumber, '')
				,@customerName = ISNULL(CustomerName, '')
				,@companyName = ISNULL(CompanyName, '')
				,@createdDate = ISNULL(CreatedDate, GETDATE())
				,@IsPMReviewed = ISNULL(ReviewedByPM, 0)
				,@customerTypeID = ISNULL(CustomerTypeID, 0)
				,@customerType = ISNULL(CustomerType, '')
				,@serviceTierID = ISNULL(ServiceTierID, 0)
				,@serviceTier = ISNULL(ServiceTier, '')
				,@CPTStusID = ISNULL(CPTStusID, 0)
		FROM @TempSelect WHERE ID = @counter
	
		SET @IsGatekeeperNeeded = 0
		SET @IsManagerNeeded = 0
		--SET @IsNTENeeded = 0
		SET @IsPMNeeded = 0
		
		IF (@cptID != 0)
		BEGIN
			-- Check for MDS Complete
			IF (@customerTypeID = 1 AND @serviceTierID = 2)
			BEGIN
				-- Gatekeeper is needed if Manager is not yet assigned
				IF NOT EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
					AND ROLE_ID = 151 AND REC_STUS_ID = 1)
						SET @IsGatekeeperNeeded = 1
				
				-- Manager is needed if NTE is not yet assigned
				IF NOT EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
					AND ROLE_ID = 23 AND REC_STUS_ID = 1)
						SET @IsManagerNeeded = 1
				
				-- PM is needed if request is not yet reviewed
				IF EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
					AND ROLE_ID = 22 AND REC_STUS_ID = 1)
					IF (@IsPMReviewed = 0) SET @IsPMNeeded = 1
			END
			
			-- Check for MDS Support
			IF (@customerTypeID = 1 AND @serviceTierID = 3)
			BEGIN
				
				IF NOT EXISTS (SELECT 1 FROM dbo.CPT_SUPRT_TIER WITH (NOLOCK) WHERE CPT_ID = @cptID
					AND CPT_MDS_SUPRT_TIER_ID = 3)
					
					-- Set IsGatekeeperNeeded, IsManagerNeeded and IsPMNeeded
					-- for MDS Support - Design and Design & Implementation
					BEGIN
						-- Gatekeeper is needed if Manager is not yet assigned
						IF NOT EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
							AND ROLE_ID = 151 AND REC_STUS_ID = 1)
								SET @IsGatekeeperNeeded = 1
						
						-- Manager is needed if NTE is not yet assigned
						IF NOT EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
							AND ROLE_ID = 23 AND REC_STUS_ID = 1)
								SET @IsManagerNeeded = 1
						
						-- PM is needed if request is not yet reviewed except for MDS Support - Design		
						IF EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
							AND ROLE_ID = 22 AND REC_STUS_ID = 1)
							BEGIN
								IF EXISTS (SELECT 1 FROM dbo.CPT_SUPRT_TIER WITH (NOLOCK)
									WHERE CPT_ID = @cptID AND CPT_MDS_SUPRT_TIER_ID != 1)
										IF (@IsPMReviewed = 0) SET @IsPMNeeded = 1
							END
					END
				ELSE
					-- IsGatekeeperNeeded and IsManagerNeeded is not required
					-- MDS Support - Monitor & Notify
					BEGIN
						SET @IsGatekeeperNeeded = 0
						SET @IsManagerNeeded = 0
						
						-- PM is needed if request is not yet reviewed
						IF EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
							AND ROLE_ID = 22 AND REC_STUS_ID = 1)
							IF (@IsPMReviewed = 0) SET @IsPMNeeded = 1
					END
			END
			
			-- Check for MDS Wholesale Carrier
			-- Gatekeeper and Manager is not needed for this type
			IF (@customerTypeID = 1 AND @serviceTierID = 4)
			BEGIN
				-- PM is needed if request is not yet reviewed
				IF EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
					AND ROLE_ID = 22 AND REC_STUS_ID = 1)
					IF (@IsPMReviewed = 0) SET @IsPMNeeded = 1
			END
			
			-- Check for MDS Wholesale VAR
			-- Gatekeeper and Manager is not needed for this type
			IF (@customerTypeID = 1 AND @serviceTierID = 5)
			BEGIN
				IF EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
					AND ROLE_ID = 22 AND REC_STUS_ID = 1)
					IF (@IsPMReviewed = 0) SET @IsPMNeeded = 1
			END
			
			-- Check for MSS, MVS and SPS
			IF (@customerTypeID IN (2,3,5))
			BEGIN
				-- MVS and SPS doesn't need Shortname
				IF (@customerTypeID IN (3,5))
					SET @customerName = 'N/A'
					
				-- Gatekeeper is needed if Manager is not yet assigned
				IF NOT EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
					AND ROLE_ID = 151 AND REC_STUS_ID = 1)
						SET @IsGatekeeperNeeded = 1
					
				-- Manager is needed if NTE is not yet assigned	
				IF NOT EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
					AND ROLE_ID = 23 AND REC_STUS_ID = 1)
						SET @IsManagerNeeded = 1
				
				-- PM is needed if request is not yet reviewed		
				IF EXISTS (SELECT 1 FROM dbo.CPT_USER_ASMT WITH (NOLOCK) WHERE CPT_ID = @cptID
					AND ROLE_ID = 22 AND REC_STUS_ID = 1)
					IF (@IsPMReviewed = 0) SET @IsPMNeeded = 1
			END
			
			-- Get Gatekeeper ID and Name
			SET @Gatekeeper = ''
			SET @GatekeeperID = 0
			SELECT @GatekeeperID = lkuser.USER_ID, @Gatekeeper = lkuser.FULL_NME
			FROM dbo.CPT_USER_ASMT cptUser WITH (NOLOCK)
			JOIN dbo.LK_USER lkuser WITH (NOLOCK) ON cptUser.CPT_USER_ID = lkuser.USER_ID
			WHERE cptUser.CPT_ID = @cptID
			AND cptUser.ROLE_ID = 152 AND cptUser.REC_STUS_ID = 1
			AND lkuser.REC_STUS_ID = 1
			
			-- Get Manager ID and Name
			SET @Manager = ''
			SET @ManagerID = 0
			SELECT @ManagerID = lkuser.USER_ID, @Manager = lkuser.FULL_NME
			FROM dbo.CPT_USER_ASMT cptUser WITH (NOLOCK)
			JOIN dbo.LK_USER lkuser WITH (NOLOCK) ON cptUser.CPT_USER_ID = lkuser.USER_ID
			WHERE cptUser.CPT_ID = @cptID
			AND cptUser.ROLE_ID = 151 AND cptUser.REC_STUS_ID = 1
			AND lkuser.REC_STUS_ID = 1
			
			-- Get NTE ID and Name
			--SET @NTE = ''
			--SET @NTEID = 0
			--SELECT @NTEID = lkuser.USER_ID, @NTE = lkuser.FULL_NME
			--FROM CPT_USER_ASMT cptUser
			--JOIN LK_USER lkuser ON cptUser.CPT_USER_ID = lkuser.USER_ID
			--WHERE cptUser.CPT_ID = @cptID
			--AND cptUser.ROLE_ID = 23 AND cptUser.REC_STUS_ID = 1
			--AND lkuser.REC_STUS_ID = 1
			
			-- Get PM ID and Name
			SET @PM = ''
			SET @PMID = 0
			SELECT @PMID = lkuser.USER_ID, @PM = lkuser.FULL_NME
			FROM dbo.CPT_USER_ASMT cptUser WITH (NOLOCK)
			JOIN dbo.LK_USER lkuser WITH (NOLOCK) ON cptUser.CPT_USER_ID = lkuser.USER_ID
			WHERE cptUser.CPT_ID = @cptID
			AND cptUser.ROLE_ID = 22 AND cptUser.REC_STUS_ID = 1
			AND lkuser.REC_STUS_ID = 1
			
			-- Insert Records into @Temp table
			INSERT INTO @Temp (CPTID, CPTNumber, CPTStusID, CustomerName, CompanyName,
					CreatedDate, CustomerTypeID, CustomerType,
					ServiceTierID, ServiceTier,
					IsGatekeeperNeeded, GatekeeperID, GatekeeperName,
					IsManagerNeeded, ManagerID, ManagerName,
					--IsNTENeeded, NTEID, NTEName,
					IsPMNeeded, PMID, PMName)
			SELECT @cptID, @cptnumber, @CPTStusID, @customerName, @companyName,
					@createdDate, @customerTypeID, @customerType,
					@serviceTierID, @serviceTier,
					@IsGatekeeperNeeded, @GatekeeperID, @Gatekeeper,
					@IsManagerNeeded, @ManagerID, @Manager,
					--@IsNTENeeded, @NTEID, @NTE,
					@IsPMNeeded, @PMID, @PM
		END
		
		-- Add counter
		SET @counter = @counter + 1
	
	END
		
	SELECT * FROM @Temp

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END