USE [COWS]
GO
/****** Object:  StoredProcedure [web].[insertM5CmpltMsg]    Script Date: 11/05/2019 2:52:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--================================================================================================  
-- Author:  dlp0278  
-- Create date: 10/29/2019  
-- Description: COWS stub for testing to complete MNS/MSS components without testers needing to create events. 
--================================================================================================  
ALTER PROCEDURE [web].[insertM5CmpltMsg] --'CH43E7524343',0
		@FTN VARCHAR(50),
		@MsgType INT = 0
 
AS
BEGIN  
SET NOCOUNT ON;  

BEGIN TRY  
	DECLARE @CMPNT_ID			INT,
			@DEV_ID				VARCHAR(25),
			@SQL				NVARCHAR(MAX),
			@M5_Ordr_Id			INT,
			@ReturnMsg			INT,
			@ORDR_ID            INT

		IF @MsgType = 1 -- Events
			BEGIN
				IF OBJECT_ID(N'tempdb..#Mach5_Event_Cmplt',N'U') IS NOT NULL 
						DROP TABLE #Mach5_Event_Cmplt
				CREATE TABLE #Mach5_Event_Cmplt
						(
							M5_ORDR_ID	Int,
							CPE_DEVICE_ID varchar(25),
							ORDR_CMPNT_ID Int
						)
				
				SET @SQL = ''
				SET @SQL = 'SELECT *' +
								' FROM OPENQUERY ' +
								' (M5,''Select 	o.ORDR_ID, c.CPE_DEVICE_ID, c.ORDR_CMPNT_ID ' +
								'From mach5.V_V5U_ORDR o ' +														
								' LEFT JOIN mach5.V_V5U_ORDR_CMPNT c ON o.ORDR_ID = c.ORDR_ID '+ 
								' where order_nbr = ''''' + CONVERT(VARCHAR(25),@FTN)+ '''''' + ' AND c.CMPNT_TYPE_CD = ''''MNST'''''')'			

				INSERT INTO #Mach5_Event_Cmplt (M5_ORDR_ID,CPE_DEVICE_ID,ORDR_CMPNT_ID)								
				EXEC sp_executesql @SQL		

				SELECT @M5_Ordr_Id = M5_ORDR_ID, @DEV_ID = CPE_DEVICE_ID, @CMPNT_ID = ORDR_CMPNT_ID FROM  #Mach5_Event_Cmplt

				IF ISNULL(@M5_Ordr_Id,'') <> '' AND ISNULL(@DEV_ID,'') <> '' AND ISNULL(@CMPNT_ID,'') <> ''
					BEGIN 
						INSERT INTO [COWS].[dbo].[M5_EVENT_MSG]
						   ([M5_ORDR_ID],[M5_ORDR_NBR],[M5_MSG_ID],[DEV_ID],[CMPNT_ID],[STUS_ID],[CREAT_DT],[SENT_DT],[EVENT_ID])
						 VALUES
						   (@M5_Ordr_Id    -- M5_ORDR_ID
						   ,@FTN -- FTN
						   ,2			-- MSG_ID  2 for MNS and 3 for MSS components.
						   ,@DEV_ID  --DEV_ID
						   , @CMPNT_ID          --CMPNT_ID, int,>
						   , 10            --<STUS_ID, smallint,>
						   , GETDATE()    --<CREAT_DT, smalldatetime,>
						   , NULL         --<SENT_DT, smalldatetime,>
						   , 9999 -- <EVENT_ID, int,>
						   )
						SET @ReturnMsg = 1
					END

				TRUNCATE TABLE #Mach5_Event_Cmplt
			
				SET @SQL = ''
				SET @SQL = 'SELECT ORDR_ID, CPE_DEVICE_ID, ORDR_CMPNT_ID ' +
								' FROM OPENQUERY ' +
								' (M5,''Select 	o.ORDR_ID, c.CPE_DEVICE_ID, c.ORDR_CMPNT_ID ' +
								'From mach5.V_V5U_ORDR o ' +														
								' LEFT JOIN mach5.V_V5U_ORDR_CMPNT c ON o.ORDR_ID = c.ORDR_ID' + 
								' where order_nbr = ''''' + CONVERT(VARCHAR(25),@FTN)+ '''''' + ' AND c.CMPNT_TYPE_CD = ''''MSSS'''''')'			
	
				INSERT INTO #Mach5_Event_Cmplt (M5_ORDR_ID,CPE_DEVICE_ID,ORDR_CMPNT_ID)									
				EXEC sp_executesql @SQL	

				SELECT @M5_Ordr_Id = null, @DEV_ID = null, @CMPNT_ID = null	

				SELECT @M5_Ordr_Id = M5_ORDR_ID, @DEV_ID = CPE_DEVICE_ID, @CMPNT_ID = ORDR_CMPNT_ID FROM  #Mach5_Event_Cmplt

				IF ISNULL(@M5_Ordr_Id,'') <> '' AND ISNULL(@DEV_ID,'') <> '' AND ISNULL(@CMPNT_ID,'') <> ''
						BEGIN 
							INSERT INTO [COWS].[dbo].[M5_EVENT_MSG]
							   ([M5_ORDR_ID],[M5_ORDR_NBR],[M5_MSG_ID],[DEV_ID],[CMPNT_ID],[STUS_ID],[CREAT_DT],[SENT_DT],[EVENT_ID])
							 VALUES
							   (@M5_Ordr_Id    -- M5_ORDR_ID
							   ,@FTN -- FTN
							   ,3			-- MSG_ID  2 for MNS and 3 for MSS components.
							   ,@DEV_ID  --DEV_ID
							   , @CMPNT_ID          --CMPNT_ID, int,>
							   , 10            --<STUS_ID, smallint,>
							   , GETDATE()    --<CREAT_DT, smalldatetime,>
							   , NULL         --<SENT_DT, smalldatetime,>
							   , 9999 -- <EVENT_ID, int,>
							   )
							SET @ReturnMsg = 1
						END	
			END				
		ELSE
			IF @MsgType = 0 -- Orders
			   BEGIN

					IF OBJECT_ID(N'tempdb..#Mach5_Ordr_Cmplt',N'U') IS NOT NULL 
						DROP TABLE #Mach5_Ordr_Cmplt

					CREATE TABLE #Mach5_Ordr_Cmplt
						(
							ORDR_ID	Int,
							DEVICE_ID varchar(25)
						)

					INSERT INTO #Mach5_Ordr_Cmplt (ORDR_ID, DEVICE_ID )
						SELECT f.ORDR_ID,  li.DEVICE_ID 
							FROM dbo.FSA_ORDR f with (nolock)
							LEFT OUTER JOIN (SELECT DISTINCT ORDR_ID, DEVICE_ID FROM  dbo.FSA_ORDR_CPE_LINE_ITEM with (nolock)
								 WHERE ITM_STUS = 401 and ISNULL(DEVICE_ID, '') <> '' )li
								 on f.ORDR_ID = li.ORDR_ID
							where f.FTN = @FTN

					DECLARE @CTR INT = 0, @CNT INT

					select * from  #Mach5_Ordr_Cmplt

					SELECT @CNT = COUNT(*) from #Mach5_Ordr_Cmplt

					WHILE @CTR < @CNT
						BEGIN
								SELECT TOP 1 @ORDR_ID = ORDR_ID, @DEV_ID = DEVICE_ID FROM #Mach5_Ordr_Cmplt	
										
								IF ISNULL(@DEV_ID,'') <> ''
									BEGIN
							
										UPDATE dbo.ACT_TASK 
										SET STUS_ID = 2
										WHERE ORDR_ID = @ORDR_ID

										IF (SELECT ORDR_TYPE_CD FROM FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID) = 'IN'
											BEGIN
													UPDATE dbo.ORDR 
													SET ORDR_STUS_ID = 2
													WHERE ORDR_ID = @ORDR_ID
											END
										 IF (SELECT ORDR_TYPE_CD FROM FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID) = 'DC'
											BEGIN
													UPDATE dbo.ORDR 
													SET ORDR_STUS_ID = 5
													WHERE ORDR_ID = @ORDR_ID
											END
											


										INSERT INTO [dbo].[ORDR_NTE]
											   (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT,MODFD_BY_USER_ID,MODFD_DT)
										 VALUES
											   (6 --<NTE_TYPE_ID, tinyint,>
											   ,@ORDR_ID --<ORDR_ID, int,>
											   ,getdate()--<CREAT_DT, smalldatetime,>
											   ,1 --<CREAT_BY_USER_ID, int,>
											   ,1 --<REC_STUS_ID, tinyint,>
											   ,'Order completed by Testing Stub process'--<NTE_TXT, varchar(max),>
											   ,null --<MODFD_BY_USER_ID, int,>
											   ,null --<MODFD_DT, smalldatetime,>
											   )
										 INSERT INTO dbo.M5_ORDR_MSG
										   (ORDR_ID,M5_MSG_ID,NTE,MSG,DEVICE,CMPNT_ID,STUS_ID,CREAT_DT,SENT_DT,PRCH_ORDR_NBR)					
     					   				 VALUES
											   (@ORDR_ID --<ORDR_ID, int,>
											   ,1 --<M5_MSG_ID, int,>
											   ,'Order completed by Testing Stub process'--<NTE, varchar(500),>
											   ,'' --<MSG, varchar(500),>
											   ,@DEV_ID --<DEVICE, varchar(25),>
											   ,null --<CMPNT_ID, int,>
											   ,10 --<STUS_ID, smallint,>
											   ,getdate() --<CREAT_DT, smalldatetime,>
											   ,null --<SENT_DT, smalldatetime,>
											   ,null --<PRCH_ORDR_NBR, varchar(15),>
											   )

										  SET @ReturnMsg = 2
									  END
									
									SET @CTR  = @CTR + 1
									
									DELETE FROM #Mach5_Ordr_Cmplt WHERE ORDR_ID = @ORDR_ID
							END
				END
			ELSE 
				BEGIN
					SET @ReturnMsg = 0
				END 
			
		SELECT  @ReturnMsg

END TRY  

BEGIN CATCH  
	EXEC [dbo].[insertErrorInfo]  
END CATCH  
  
END