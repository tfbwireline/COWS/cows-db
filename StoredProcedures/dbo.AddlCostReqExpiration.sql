USE [COWS]
GO
_CreateObject 'SP','dbo','AddlCostReqExpiration'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Cereated By:	Kyle Wichert
-- Create date:	08/24/2011 
-- Description:	Finds orders past the expiration date with status = pending and
--				sets the status to expired.  Orders within 1 day of expiration 
--				get a new note, RT Code, and the RTS task reloaded
-- ============================================================================

ALTER PROCEDURE [dbo].[AddlCostReqExpiration]

AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ACR TABLE
	(
		orderID		INT,
		chargeType	SMALLINT,
		ver			SMALLINT,
		status		SMALLINT,
		expDate		SMALLDATETIME,
		noteID		INT
	)		
	DECLARE @Now SMALLDATETIME

	BEGIN TRY
		
		INSERT INTO @ACR
		SELECT	ORDR_ID,
				CKT_CHG_TYPE_ID,
				MAX(VER_ID),
				NULL,
				NULL,
				NULL
		FROM 	dbo.ORDR_CKT_CHG	WITH (NOLOCK)
		GROUP BY ORDR_ID, CKT_CHG_TYPE_ID

		UPDATE	a
		SET		status	= SALS_STUS_ID,
				expDate	= XPIRN_DT
		FROM			@ACR a
			INNER JOIN	dbo.ORDR_CKT_CHG	cc	WITH (NOLOCK)	ON	cc.ORDR_ID			=	a.orderID
																AND	cc.CKT_CHG_TYPE_ID	=	a.chargeType
																AND	cc.VER_ID			=	a.ver
		
		
		SET @Now = DATEADD(DAY, 1, GETDATE())
		--Delete from table if the expiration is greater than one day out or is NULL
		DELETE FROM @ACR WHERE ISNULL(expDate, @Now) >= @Now
		--Delete from table if the status has been set already
		DELETE FROM @ACR WHERE ISNULL(status, 0) <> 0
		
		SET @Now = GETDATE()
		
		--SR-401 -- If expiration > @Now, insert note and set jep code to be new value
		INSERT INTO dbo.ORDR_NTE	(NTE_TYPE_ID,	ORDR_ID,	CREAT_DT,	CREAT_BY_USER_ID,	REC_STUS_ID,	NTE_TXT)
		SELECT DISTINCT				14,				a.orderID,	@Now,		1,					1,				'Early termination penalties are within 1 day of expiration'
		FROM	@ACR a
		WHERE	a.expDate > @Now
			AND NOT EXISTS (SELECT TOP 1 'X' FROM dbo.ORDR_JPRDY j WITH (NOLOCK) WHERE j.ORDR_ID = a.orderID AND j.JPRDY_CD = 105 AND DATEADD(DAY, 1, j.CREAT_DT ) >= @Now)

		UPDATE	a
		SET noteID = (SELECT MAX(n.NTE_ID) FROM dbo.ORDR_NTE n WITH (NOLOCK) WHERE a.orderID = n.ORDR_ID AND n.NTE_TYPE_ID = 14 AND	CHARINDEX('within 1 day of', n.NTE_TXT) > 0)
		FROM	@ACR a

		INSERT INTO dbo.ORDR_JPRDY	(ORDR_ID,	JPRDY_CD,	NTE_ID,		CREAT_DT)
		SELECT DISTINCT				a.orderID,	105,		a.noteID,	@Now
		FROM @ACR a
		WHERE	a.expDate > @Now		
			AND ISNULL(a.noteID,0) > 0
			AND NOT EXISTS (SELECT TOP 1 'X' FROM dbo.ORDR_JPRDY j WITH (NOLOCK) WHERE j.ORDR_ID = a.orderID AND j.JPRDY_CD = 105 AND DATEADD(DAY, 1, j.CREAT_DT ) >= @Now)

		--If RTS was cleared but status not set, reload RTS task
		UPDATE at
		SET		STUS_ID	= 0, MODFD_DT = GETDATE()
		FROM			@ACR a
			INNER JOIN	dbo.ACT_TASK	at	WITH (NOLOCK)	ON	at.ORDR_ID	=	a.orderID
		WHERE	at.TASK_ID	=	500
			AND	at.STUS_ID	<>	0
			AND	a.expDate > @Now
		
		INSERT INTO dbo.ACT_TASK	(ORDR_ID,	TASK_ID,	STUS_ID,	WG_PROF_ID,	CREAT_DT)
		SELECT						orderID,	500,		0,		500,		GETDATE()
		FROM			@ACR a
		WHERE	a.expDate > @Now
			AND NOT EXISTS (SELECT TOP 1 'X' FROM dbo.ACT_TASK at WITH (NOLOCK) WHERE at.ORDR_ID = a.orderID AND at.TASK_ID = 500)

		--End Set Jep Code
			
		--Expire if the date is in the past
		UPDATE	cc
		SET		SALS_STUS_ID = 33
		FROM			@ACR a
			INNER JOIN	dbo.ORDR_CKT_CHG	cc	WITH (ROWLOCK)	ON	cc.ORDR_ID			=	a.orderID
																AND	cc.CKT_CHG_TYPE_ID	=	a.chargeType
																AND cc.VER_ID			=	a.ver
		WHERE	a.expDate < @Now
			
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	
END