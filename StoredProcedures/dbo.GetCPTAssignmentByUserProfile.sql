USE [COWS]
GO
_CreateObject 'SP','dbo','GetCPTAssignmentByUserProfile'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:	qi931353
-- Create date: 12/19/2019
-- Description:	Get pooled users for CPT Assignment
-- =========================================================
ALTER PROCEDURE [dbo].[GetCPTAssignmentByUserProfile]
@USR_PRF_ID INT
AS
BEGIN
SET NOCOUNT ON;
	BEGIN TRY
		
		SELECT DISTINCT 
			usr.USER_ID AS UserId
			,usr.USER_ADID AS UserAdId
			,usr.FULL_NME AS FullName
			,usr.EMAIL_ADR AS Email
			,up.USR_PRF_ID AS UsrPrfId
			,up.USR_PRF_NME AS UsrPrfName
			,mup.POOL_CD AS PoolCd
			,Tasks = (SELECT COUNT(cpt.CPT_ID) FROM dbo.CPT cpt WITH (NOLOCK)
						JOIN dbo.CPT_USER_ASMT cua WITH (NOLOCK) ON cua.CPT_ID = cpt.CPT_ID
						WHERE cua.REC_STUS_ID = 1 AND cua.CPT_USER_ID = usr.USER_ID
						AND cpt.REC_STUS_ID = 1 AND cpt.CPT_STUS_ID NOT IN (308, 311)
						AND cpt.CREAT_DT >= DATEADD(MONTH, -1, GETDATE()))
			,Devices = (SELECT COALESCE(SUM(cpt.DEV_CNT),0) FROM dbo.CPT cpt WITH (NOLOCK)
						JOIN dbo.CPT_USER_ASMT cua WITH (NOLOCK) ON cua.CPT_ID = cpt.CPT_ID
						WHERE cua.REC_STUS_ID = 1 AND cua.CPT_USER_ID = usr.USER_ID
						AND cpt.REC_STUS_ID = 1 AND cpt.CPT_STUS_ID NOT IN (308, 311)
						AND cpt.CREAT_DT >= DATEADD(MONTH, -1, GETDATE()))
		FROM dbo.LK_USER usr WITH (NOLOCK)
		JOIN dbo.MAP_USR_PRF mup WITH (NOLOCK) ON usr.USER_ID = mup.USER_ID
		JOIN dbo.LK_USR_PRF up WITH (NOLOCK) ON mup.USR_PRF_ID = up.USR_PRF_ID
		WHERE (usr.REC_STUS_ID = 1 OR usr.USER_ADID = 'NTEPool')
		AND usr.USER_ID != 1 -- System
		AND mup.REC_STUS_ID = 1 AND mup.USR_PRF_ID = @USR_PRF_ID
		AND mup.POOL_CD = (CASE WHEN @USR_PRF_ID IN (130) THEN POOL_CD ELSE 1 END)

	END TRY

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
END
