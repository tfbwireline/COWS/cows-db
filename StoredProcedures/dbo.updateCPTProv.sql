USE [COWS]
GO
_CreateObject 'SP','dbo','updateCPTProv'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		jrg7298
-- Create date: 09/02/2015
-- Description:	Update the status of a given CPT
-- =========================================================
ALTER PROCEDURE [dbo].[updateCPTProv]
@PrvsnID INT = -1,
@Status SMALLINT = -1,
@SrvrNme VARCHAR(200) = '',
@NteTxt VARCHAR(2000) = ''
AS
BEGIN
SET NOCOUNT ON;
Begin Try

IF (@PrvsnID != -1)
BEGIN
	IF (@Status = -1)
	  SELECT 'ERROR: INVALID STATUS'
	ELSE
	IF (@SrvrNme = '')
	  SELECT 'ERROR: INVALID SERVER NAME'
	ELSE
	BEGIN
		UPDATE dbo.CPT_PRVSN WITH (ROWLOCK)
		SET REC_STUS_ID = @Status,
		PRVSN_NTE_TXT = @NteTxt,
		SRVR_NME = @SrvrNme,
		MODFD_DT=GETDATE()
		WHERE CPT_PRVSN_ID=@PrvsnID
		SELECT 'SUCCESS'
	END
END
ELSE
	SELECT 'ERROR: INVALID ID'


End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
	SELECT 'ERROR'
End Catch
END
