USE [COWS]
GO
_CreateObject 'SP','dbo','xNCISLATimeChk'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		David Phillips
-- Create date: 09/012/2011
-- Description:	This SP is used check for SLA expirations
-- =============================================

ALTER  PROCEDURE dbo.xNCISLATimeChk

AS
BEGIN
	SET NOCOUNT ON;
	SET DEADLOCK_PRIORITY 3

	DECLARE @OUT_TIME			DATETIME -- Used as the time the order should have been out of the task.
	DECLARE @IN_TIME			DATETIME -- The create date time the task was created in ACT_TASK
	DECLARE @DAY_CTR	DATETIME -- The order submit date from Order Submit to xNCI Validation
	DECLARE @SLA				INT
	DECLARE @ORDR_ID			INT 
	DECLARE @TASK_ID			INT 
	DECLARE @SLA_VIOLATED_FLAG  INT
	DECLARE @HOLIDAY			INT
	DECLARE @MILESTONE_DES		VARCHAR(50)
	DECLARE @NOTE				VARCHAR (100)
	DECLARE @COUNTRY_CODE		VARCHAR (2)
	DECLARE @PLATFORM_PRODUCT	VARCHAR (2)

	DECLARE @RowCount			INT
	DECLARE @CurCount			INT
	DECLARE @DayCount			INT
	DECLARE @TotalDaysBetween	INT
	
	
    DECLARE @tblOrders TABLE (Seq int identity(1,1), ORDR_ID INT, TIME_IN DATETIME, TASK_ID INT, SLA INT, SLA_VIOLATE_FLAG INT, PROCESSED INT)
    
	SET @HOLIDAY = 0

	
	BEGIN TRY
	
		INSERT INTO @tblOrders ( ORDR_ID, TIME_IN, TASK_ID, SLA, SLA_VIOLATE_FLAG,PROCESSED)
		SELECT DISTINCT atk.ORDR_ID
			,atk.CREAT_DT
			,atk.TASK_ID
			,sl.SLA_IN_DAY_QTY
			,IsNull(o.SLA_VLTD_CD, 0)
			,0
		FROM [dbo].ACT_TASK atk WITH (NOLOCK)
		INNER JOIN [dbo].LK_XNCI_MS ms WITH (NOLOCK) ON ms.MS_TASK_ID = atk.TASK_ID
		INNER JOIN [dbo].LK_XNCI_MS_SLA sl WITH (NOLOCK) ON ms.MS_ID = sl.MS_ID
		INNER JOIN [dbo].LK_TASK lt WITH (NOLOCK) ON lt.TASK_ID = atk.TASK_ID
		INNER JOIN [dbo].ORDR o WITH (NOLOCK) ON o.ORDR_ID = atk.ORDR_ID			
		LEFT OUTER JOIN [dbo].IPL_ORDR ipl WITH (NOLOCK) ON o.ORDR_ID=ipl.ORDR_ID
		LEFT OUTER JOIN [dbo].LK_XNCI_MS_SLA sl2 WITH (NOLOCK) ON o.PLTFRM_CD=sl2.PLTFRM_CD
		WHERE atk.STUS_ID = 0 AND o.ORDR_STUS_ID IN (0,1) AND atk.TASK_ID <> 102 AND IsNull(o.PLTFRM_CD, 0) = IsNull(sl.PLTFRM_CD, 0)
			AND NOT EXISTS (SELECT ORDR_ID
				FROM [dbo].ORDR_HOLD_MS ohs WITH (NOLOCK)
				WHERE IsNull(RELS_DT, 0) = 0 AND ohs.ORDR_ID=o.ORDR_ID
				)
			AND (
				sl2.PLTFRM_CD IS NOT NULL
				OR ipl.ORDR_ID IS NOT NULL
				)



			UPDATE dbo.ORDR WITH (ROWLOCK)
				SET SLA_VLTD_CD = 0
				WHERE SLA_VLTD_CD = 1 AND ORDR_STUS_ID IN (0,1) AND ORDR_ID NOT IN (SELECT ORDR_ID FROM @tblOrders)

	
--*************************************************************************************************
-- This logic is for all SLA's except Order Submit to xNCI Order Validation 
--************************************************************************************************
										 
			
		SET @RowCount = (Select count(*) from @tblOrders)
		SET @CurCount = 0

		WHILE @CurCount < @RowCount
			BEGIN
				SELECT TOP 1 @ORDR_ID = ORDR_ID, 
							 @IN_TIME = TIME_IN, 
							 @TASK_ID = TASK_ID, 
							 @SLA     = SLA,
							 @SLA_VIOLATED_FLAG = SLA_VIOLATE_FLAG
				FROM @tblOrders 
					WHERE PROCESSED = 0

				IF ((DATEDIFF(day, @IN_TIME, GETDATE()) < @SLA) OR (@SLA = 0))
					AND @SLA_VIOLATED_FLAG = 1
					BEGIN
						UPDATE [dbo].ORDR WITH (ROWLOCK)
							SET SLA_VLTD_CD = 0
							WHERE ORDR_ID = @ORDR_ID

						SET @SLA_VIOLATED_FLAG = 0
					END

				IF @SLA > 0 AND @SLA_VIOLATED_FLAG = 0
					BEGIN

						IF EXISTS (SELECT ORDR_ID from [dbo].IPL_ORDR WITH (NOLOCK) WHERE ORDR_ID=@ORDR_ID)
							BEGIN
								SET @COUNTRY_CODE = (SELECT top 1 CTRY_CD FROM dbo.ORDR_ADR WITH (NOLOCK)
												WHERE ORDR_ID = @ORDR_ID 
													AND ADR_TYPE_ID = 11)
							END
						ELSE
							BEGIN
								SET @COUNTRY_CODE = (SELECT top 1 CTRY_CD FROM dbo.ORDR_ADR WITH (NOLOCK)
												WHERE ORDR_ID = @ORDR_ID 
													AND CIS_LVL_TYPE = 'H5')
							END
					
			
						SET @OUT_TIME = DATEADD(day, @SLA, @IN_TIME)
						SET @HOLIDAY = 0
						SET @DAY_CTR = @IN_TIME
						SET @DayCount = 0

						SET @TotalDaysBetween = DATEDIFF(DAY, @IN_TIME, GETDATE())

						IF @TotalDaysBetween > 0 and @TotalDaysBetween >= @SLA 
							BEGIN
								WHILE  @DayCount <= @TotalDaysBetween  -- loop thru each day to see if Holiday or weekend to subtract from time allowed.
									BEGIN
											SELECT @HOLIDAY = COUNT(1) FROM dbo.LK_WRLD_HLDY WITH (NOLOCK)
											WHERE WRLD_HLDY_DT = @DAY_CTR
											AND CTRY_CD = @COUNTRY_CODE
						
										IF @HOLIDAY > 0
										  AND DATENAME(WEEKDAY, @DAY_CTR) <> 'SATURDAY'
										  AND DATENAME(WEEKDAY, @DAY_CTR) <> 'SUNDAY'
											BEGIN
												SET @OUT_TIME = DATEADD(DAY, 1, @OUT_TIME)
											END
										ELSE
											BEGIN					
											  IF (SELECT DATENAME(WEEKDAY, @DAY_CTR)) = 'SATURDAY'
												BEGIN
													SET @OUT_TIME = DATEADD(DAY, 1, @OUT_TIME)
												END
											  IF (SELECT DATENAME(WEEKDAY, @DAY_CTR)) = 'SUNDAY'
												BEGIN
													SET @OUT_TIME = DATEADD(DAY, 1, @OUT_TIME)
												END
											END
										SET @DayCount = @DayCount + 1
										SET @DAY_CTR = @DAY_CTR + 1
									END
							IF (GETDATE()> @OUT_TIME)
								BEGIN
									UPDATE [dbo].ORDR WITH (ROWLOCK)
										SET SLA_VLTD_CD = 1
									WHERE ORDR_ID = @ORDR_ID
					
									SELECT @MILESTONE_DES = MS_DES FROM dbo.LK_XNCI_MS WITH (NOLOCK)
										WHERE MS_TASK_ID = @TASK_ID
							
									SET @NOTE = 'Order missed SLA in beginning milestone ' + @MILESTONE_DES	
					
									EXEC insertOrderNotes @ORDR_ID,16,@NOTE,1 
						 
								END	
						END
					END
					
					SET @CurCount = @CurCount + 1	

					UPDATE @tblOrders 
						SET PROCESSED = 1
							WHERE ORDR_ID = @ORDR_ID
			
			END

--***********************************************************************************
	--The below logic is to handle the SLA from Order Submit to xNCI Validation only
--************************************************************************************

		SET @RowCount = (Select count(*) from @tblOrders 
							WHERE PROCESSED = 0  
							AND TASK_ID = 200
							AND SLA > 0)

		SET @CurCount = 0

		UPDATE @tblOrders SET PROCESSED = 0

		WHILE @CurCount < @RowCount
			BEGIN
				SELECT TOP 1 @ORDR_ID = ORDR_ID, 
							 @IN_TIME = TIME_IN, 
							 @TASK_ID = TASK_ID, 
							 @SLA     = SLA,
							 @SLA_VIOLATED_FLAG = SLA_VIOLATE_FLAG
				FROM @tblOrders
					WHERE PROCESSED = 0  
							AND TASK_ID = 200
							AND SLA > 0
						
				SET @PLATFORM_PRODUCT = (SELECT top 1 PLTFRM_CD FROM [dbo].ORDR WITH (NOLOCK)
								WHERE @ORDR_ID = ORDR_ID)

				SET @SLA = (SELECT TOP 1 SLA_IN_DAY_QTY FROM [dbo].LK_XNCI_MS_SLA ms WITH (NOLOCK)
							JOIN [dbo].LK_XNCI_MS lm WITH (NOLOCK) ON ms.TO_MS_ID = lm.MS_ID
							WHERE ms.PLTFRM_CD = @PLATFORM_PRODUCT
								AND lm.MS_TASK_ID = 200)

				IF @SLA <> 0
					BEGIN

						IF EXISTS (SELECT ORDR_ID from [dbo].IPL_ORDR WITH (NOLOCK) WHERE ORDR_ID=@ORDR_ID)
							BEGIN
								SET @COUNTRY_CODE = (SELECT TOP 1 CTRY_CD FROM dbo.ORDR_ADR WITH (NOLOCK)
														WHERE ORDR_ID = @ORDR_ID 
															AND ADR_TYPE_ID = 11)
								SET @OUT_TIME = (SELECT TOP 1 CREAT_DT FROM [dbo].IPL_ORDR WITH (NOLOCK)
												WHERE ORDR_ID = @ORDR_ID)
							END
						ELSE
							BEGIN
								SET @COUNTRY_CODE = (SELECT top 1 CTRY_CD FROM dbo.ORDR_ADR WITH (NOLOCK)
														WHERE ORDR_ID = @ORDR_ID 
															AND CIS_LVL_TYPE = 'H5')
								SET @OUT_TIME = (SELECT top 1 ORDR_SBMT_DT FROM [dbo].FSA_ORDR WITH (NOLOCK)
														WHERE ORDR_ID = @ORDR_ID) 
							END
									
						SET @DAY_CTR = @OUT_TIME
						SET @HOLIDAY = 0
						SET @DayCount = 0
						
						SET @TotalDaysBetween = DATEDIFF(DAY, @DAY_CTR, GETDATE())

						SET @OUT_TIME = DATEADD(DAY,@TotalDaysBetween,@OUT_TIME) -- Get the total number of days between submit date and entry into task 200

						IF @TotalDaysBetween > 0 
							BEGIN
								
								WHILE  @DayCount < @TotalDaysBetween  -- loop thru each day to see if Holiday or weekend to subtract from time between
									BEGIN
   									   SELECT @HOLIDAY = COUNT(1) FROM dbo.LK_WRLD_HLDY 
											WHERE WRLD_HLDY_DT = @DAY_CTR
												AND CTRY_CD = @COUNTRY_CODE
												
										IF @HOLIDAY > 0 
											AND NOT DATENAME(WEEKDAY, @DAY_CTR) = 'SATURDAY'
											AND NOT DATENAME(WEEKDAY, @DAY_CTR) = 'SUNDAY'
											BEGIN
												SET @OUT_TIME = DATEADD(DAY, -1, @OUT_TIME)
											END
										ELSE
													BEGIN
														IF (SELECT DATENAME(WEEKDAY, @DAY_CTR)) = 'SATURDAY'
															BEGIN
																SET @OUT_TIME = DATEADD(DAY, -1, @OUT_TIME)
															END
														IF (SELECT DATENAME(WEEKDAY, @DAY_CTR)) = 'SUNDAY'
															BEGIN
																SET @OUT_TIME = DATEADD(DAY, -1, @OUT_TIME)
															END
													END
												SET @DayCount = @DayCount + 1
												SET @DAY_CTR = @DAY_CTR + 1 -- started at submit date, add 1 day between and check for holiday or weekend.
										END										
											
								IF (@IN_TIME < @OUT_TIME)
									BEGIN	

										IF NOT EXISTS(SELECT ORDR_ID FROM dbo.ORDR_NTE WITH (NOLOCK)
																WHERE ORDR_ID = @ORDR_ID
																	AND NTE_TYPE_ID = 16 
																	AND ORDR_ID=@ORDR_ID
																	AND NTE_TXT = 'Order missed SLA in milestone Order Submit to xNCI Validation. ')
												BEGIN
													SET @NOTE = 'Order missed SLA in milestone Order Submit to xNCI Validation. '	
													EXEC insertOrderNotes @ORDR_ID,16,@NOTE,1 
												END
						 
									END
							END

					END
					
					SET @CurCount = @CurCount + 1	
					
					UPDATE @tblOrders 
						SET PROCESSED = 1
							WHERE ORDR_ID = @ORDR_ID
			END

	END TRY

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
	
	SET NOCOUNT OFF
END