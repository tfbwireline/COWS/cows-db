USE [COWS]
GO
_CreateObject 'SP','dbo','updateASREmailStatus'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <04/14/2015>
-- Description:	<update ASR email status>
-- =============================================
ALTER PROCEDURE dbo.updateASREmailStatus
	@ASR_ID Int,
	@Status  Int
AS
BEGIN
	SET NOCOUNT ON;

    Update dbo.ASR WITH (ROWLOCK)
		SET STUS_ID = @Status
		WHERE ASR_ID = @ASR_ID
END
GO
