USE [COWS]
GO
_CreateObject 'SP'
	,'dbo'
	,'getSSTATCPEDispatchEmail'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		km967761
-- Create date: 08/23/2021
-- Description:	Get the details for all SSTAT CPE Dispatch Emails
-- =========================================================
ALTER PROCEDURE [dbo].[getSSTATCPEDispatchEmail]
	 @TRAN_ID int 
AS
BEGIN
SET NOCOUNT ON;
Begin Try

	SELECT
		EMAIL_BODY_TXT
	FROM dbo.SSTAT_REQ a
		INNER JOIN dbo.EMAIL_REQ b ON b.EMAIL_REQ_ID = a.EMAIL_REQ_ID
	WHERE
		a.TRAN_ID = @TRAN_ID
		--AND b.STUS_ID = 10
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
