USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[BillOnlyTaskRule]    Script Date: 04/29/2019 2:57:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:		David Phillips
-- Create date: 04/29/2019
-- Description:	This SP is used to process orders in Task 110 - IS Initiate Bill Only order in M5.

-- =============================================

ALTER PROCEDURE [dbo].[BillOnlyTaskRule] --451764
	@OrderID INT,
	@TaskId INT, 
	@TaskStatus SMALLINT, 
	@Comments VARCHAR(1000) = NULL


AS
BEGIN
	
	BEGIN TRY

		DECLARE @FTN Int,
				@toList Varchar(max) =  'centralizedorders@sprint.com',
				@Subject Varchar (max) = 'IS Initiate Billing Order',
				@ASN_User int

				INSERT INTO dbo.EMAIL_REQ
						   (ORDR_ID,EMAIL_REQ_TYPE_ID,STUS_ID,CREAT_DT,EVENT_ID
						   ,EMAIL_LIST_TXT,EMAIL_CC_TXT,EMAIL_SUBJ_TXT,EMAIL_BODY_TXT,SENT_DT,REDSGN_ID,CPT_ID
						   )
					 VALUES
						   (@OrderID --ORDR_ID
						   ,42 -- EMAIL_REQ_TYPE_ID
						   ,10 -- STUS_ID
						   ,getdate() --CREAT_DT
						   ,null --EVENT_ID
						   ,@toList --EMAIL_LIST_TXT
						   , null --EMAIL_CC_TXT
						   ,@Subject --EMAIL_SUBJ_TXT
						   ,null --EMAIL_BODY_TXT
						   ,null -- SENT_DT
						   ,null --REDSGN_ID
						   ,null--CPT_ID
						   )
			
				SELECT @ASN_User = USER_ID 
					FROM LK_USER WITH (NOLOCK)
						WHERE USER_ADID = 'br295157' --Brandy Haggar gets all orders as of 4/29/19

					
					IF NOT EXISTS (SELECT 'X' FROM dbo.USER_WFM_ASMT WITH (NOLOCK)
									WHERE ORDR_ID = @OrderID AND ((GRP_ID = 1) OR (USR_PRF_ID=126)) AND ASN_USER_ID = @ASN_User)
						BEGIN
							INSERT INTO dbo.USER_WFM_ASMT
									   (ORDR_ID,GRP_ID,ASN_USER_ID,ASN_BY_USER_ID,ASMT_DT,CREAT_DT,ORDR_HIGHLIGHT_CD
									   ,ORDR_HIGHLIGHT_MODFD_DT,DSPTCH_TM,INRTE_TM,ONSITE_TM,CMPLT_TM,EVENT_ID,[USR_PRF_ID]
									   )
								 VALUES
									   (@OrderID	--<ORDR_ID, int,>
									   ,1			--<GRP_ID, smallint,>
									   ,@ASN_User	--<ASN_USER_ID, int,>
									   ,1			--<ASN_BY_USER_ID, int,>
									   ,getdate()	--<ASMT_DT, smalldatetime,>
									   ,getdate()	--<CREAT_DT, smalldatetime,>
									   ,0			--<ORDR_HIGHLIGHT_CD, bit,>
									   ,null		--<ORDR_HIGHLIGHT_MODFD_DT, smalldatetime,>
									   ,null		--<DSPTCH_TM, datetime,>
									   ,null		--<INRTE_TM, datetime,>
									   ,null		--<ONSITE_TM, datetime,>
									   ,null		--<CMPLT_TM, datetime,>
									   ,null		--<EVENT_ID, int,>
									   ,126
									   )

						END
	END TRY

	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	SET NOCOUNT OFF
END


