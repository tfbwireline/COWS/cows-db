USE [COWS]
GO
_CreateObject 'SP','web','getM5CPEOrderData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  jrg7298            
-- Create date: 07/30/2015         
-- Description: Get the CPE Order data for an MDS/UCaaS Event when a H6 value is entered.  
-- Updated:     vn370313            
-- Create date: 02/13/2017        
-- Description: Rcpt_Status Return Status of  Multiple Orders Thats why same Equipment Return Multiple Recept statuses
--              At Pramods Advice i have put Additional Condition to choose the last Order Related to M5CPEOrdrNbr
/*
Issue Example
That’s the issue  M5CPEOrdrNbr = IN3NC0009918      and   same DEVICE_ID='00000000015425'   is associated with 3   
Orders as well   13025,13036,13048    for Order  13025 / 13036   its  Cancelled and for Order 13048   it is   “Equip Receipt-Pending”
So its getting the Top 1   Vancelled in this case.
*/
          
-- =============================================            
ALTER PROCEDURE [web].[getM5CPEOrderData]  --'928218835','10/05/2016','N',''
 @H6  VARCHAR(9),
 @CCD VARCHAR(10),
 @UCaaSCD CHAR(1),
 @EventID INT = 0
AS            
BEGIN            
SET NOCOUNT ON;
BEGIN TRY

IF OBJECT_ID(N'tempdb..#M5POTable', N'U') IS NOT NULL         
	DROP TABLE #M5POTable 

IF OBJECT_ID(N'tempdb..#M5CPETable', N'U') IS NOT NULL         
	DROP TABLE #M5CPETable
	
IF OBJECT_ID(N'tempdb..#M5InvTable', N'U') IS NOT NULL         
	DROP TABLE #M5InvTable

CREATE TABLE #M5CPETable
(M5CPEOrdrNbr VARCHAR(20) NULL,
DevID VARCHAR(20) NULL,
OrderQty INT NULL,
Descrption	VARCHAR(511) NULL,
OrdrExcp	VARCHAR(511) NULL,
CntrctType	VARCHAR(511) NULL,
VndrPO VARCHAR(20) NULL,
EquipRcvdDt	VARCHAR(511) NULL,
CPECmpntFmly VARCHAR(511) NULL,
RcptStus VARCHAR(511) NULL,
RecordOnly varchar(1)NULL
)

CREATE TABLE #M5POTable
 (DevID VARCHAR(20) NULL 
 ,OdieDevNme VARCHAR(200) NULL
 ) 

CREATE TABLE #M5InvTable
 (CPE_DEVICE_ID VARCHAR(20) NULL
 ,ORDER_NBR VARCHAR(20) NULL
 ,NME VARCHAR(200) NULL
 ,INST_DT  DATETIME NULL
 )    
 
 declare @potbl nvarchar(max) = 'INSERT INTO  #M5POTable (DevID)
 Select CPE_DEVICE_ID  from openquery(M5,'''
set  @potbl = @potbl + 'select DISTINCT voc.CPE_DEVICE_ID
							from MACH5.V_V5U_ORDR_CMPNT voc
							inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
              inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
							where vvo.CUST_CMMT_DT=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
              and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
			  and voc.CPE_DEVICE_ID IS NOT NULL
			  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
              and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')'
IF @UCaaSCD = 'N'
BEGIN
	SET @potbl = @potbl + '
			  and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
END
ELSE
BEGIN
	SET @potbl = @potbl + ' and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
END

exec sp_executesql @potbl

IF NOT EXISTS (SELECT 'X' FROM #M5POTable)
BEGIN
 set @potbl = 'INSERT INTO  #M5POTable (DevID)
 Select CPE_DEVICE_ID  from openquery(M5,'''
set  @potbl = @potbl + 'select DISTINCT voc.CPE_DEVICE_ID
							from MACH5.V_V5U_ORDR_CMPNT voc
							inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
              inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
							where vvo.CUST_CMMT_DT=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
              and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
			  and voc.CPE_DEVICE_ID IS NOT NULL
			  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
              and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')'
IF @UCaaSCD = 'N'
BEGIN
	SET @potbl = @potbl + '
			  and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
END
ELSE
BEGIN
	SET @potbl = @potbl + ' and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
END

exec sp_executesql @potbl
END

UPDATE po
	SET OdieDevNme = ISNULL((SELECT TOP 1 ODIE_DEV_NME FROM dbo.EVENT_CPE_DEV WITH (NOLOCK) WHERE EVENT_ID=@EventID and DEVICE_ID=po.DevID and REC_STUS_ID=1 and ISNULL(ODIE_DEV_NME,'0') <> '0'),'')
	FROM #M5POTable po


select DISTINCT DevID, OdieDevNme  from #M5POTable order by DevID


 declare @cpeordr nvarchar(max) = 'INSERT INTO  #M5CPETable (M5CPEOrdrNbr,DevID,OrderQty,Descrption,OrdrExcp,CntrctType,CPECmpntFmly,RecordOnly)
 Select ORDER_NBR
,CPE_DEVICE_ID
,QTY
,NME
,EXP_TYPE_CD
,CPE_CONTRACT_TYPE
,CPE_CMPN_FMLY
,CPE_RECORD_ONLY_FLG_CD
from openquery(M5,'''
set @cpeordr = @cpeordr + 'select DISTINCT vvo.ORDER_NBR
,voc.CPE_DEVICE_ID
,voc.QTY
,voc.NME
,c.EXP_TYPE_CD
,voc.CPE_CONTRACT_TYPE
,voc.CPE_CMPN_FMLY
,voc.CPE_RECORD_ONLY_FLG_CD

							from MACH5.V_V5U_ORDR_CMPNT voc
							inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
              inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
			  left outer join MACH5.V_V5U_CHNG_TXN c ON c.CHNG_TXN_ID = vvo.ORDR_ID
			  left outer join MACH5.V_V5U_NVTRY inv ON inv.NVTRY_ID = voc.NVTRY_ID 
							where vvo.CUST_CMMT_DT=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
              and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
			  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')
			  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
              and inv.STUS_CD NOT IN ( ''''ACT'''',''''CAN'''')'
IF @UCaaSCD = 'N'
BEGIN
	SET @cpeordr = @cpeordr + '
			  
              and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
END
ELSE
BEGIN
	SET @cpeordr = @cpeordr + ' and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
END

exec sp_executesql @cpeordr


IF NOT EXISTS (SELECT 'X' FROM #M5CPETable)
BEGIN
 set @cpeordr  = 'INSERT INTO  #M5CPETable (M5CPEOrdrNbr,DevID,OrderQty,Descrption,OrdrExcp,CntrctType,CPECmpntFmly,RecordOnly)
 Select ORDER_NBR
,CPE_DEVICE_ID
,QTY
,NME
,EXP_TYPE_CD
,CPE_CONTRACT_TYPE
,CPE_CMPN_FMLY
,CPE_RECORD_ONLY_FLG_CD
from openquery(M5,'''
set @cpeordr = @cpeordr + 'select DISTINCT vvo.ORDER_NBR
,voc.CPE_DEVICE_ID
,voc.QTY
,voc.NME
,c.EXP_TYPE_CD
,voc.CPE_CONTRACT_TYPE
,voc.CPE_CMPN_FMLY
,voc.CPE_RECORD_ONLY_FLG_CD

							from MACH5.V_V5U_ORDR_CMPNT voc
							inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
              inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
			  left outer join MACH5.V_V5U_CHNG_TXN c ON c.CHNG_TXN_ID = vvo.ORDR_ID
			  left outer join MACH5.V_V5U_NVTRY inv ON inv.NVTRY_ID = voc.NVTRY_ID 
							where vvo.CUST_CMMT_DT=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
              and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
              and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')
			  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
              and inv.STUS_CD NOT IN ( ''''ACT'''',''''CAN'''')'
IF @UCaaSCD = 'N'
BEGIN
	SET @cpeordr = @cpeordr + '
			  
              and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
END
ELSE
BEGIN
	SET @cpeordr = @cpeordr + ' and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
END

exec sp_executesql @cpeordr
END

 declare @invtbl nvarchar(max) = 'INSERT INTO  #M5InvTable (CPE_DEVICE_ID,ORDER_NBR,NME,INST_DT)
 SELECT CPE_DEVICE_ID, ORDER_NBR, NME, INST_DT 
		FROM OPENQUERY(m5,''select inv.CPE_DEVICE_ID, vvo.ORDER_NBR, inv.NME, inv.INST_DT  
					FROM MACH5.V_V5U_NVTRY inv
					INNER JOIN MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = inv.ORIG_ORDR_ID
					INNER JOIN MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
			        WHERE vvo.CUST_CMMT_DT=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
			        AND inv.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
			        AND inv.STUS_CD = ''''ACT''''
					and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')
					and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
			        AND vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'

exec sp_executesql @invtbl

IF NOT EXISTS (SELECT 'X' FROM #M5InvTable)
BEGIN
 set @invtbl = 'INSERT INTO  #M5InvTable (CPE_DEVICE_ID,ORDER_NBR,NME,INST_DT)
 SELECT CPE_DEVICE_ID, ORDER_NBR, NME, INST_DT 
		FROM OPENQUERY(m5,''select inv.CPE_DEVICE_ID, vvo.ORDER_NBR, inv.NME, inv.INST_DT  
					FROM MACH5.V_V5U_NVTRY inv
					INNER JOIN MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = inv.ORIG_ORDR_ID
					INNER JOIN MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
			        WHERE vvo.CUST_CMMT_DT=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
			        AND inv.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
			        AND inv.STUS_CD = ''''ACT''''
					and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')
					and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
			        AND vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'

 exec sp_executesql @invtbl
END

			  
UPDATE mc
SET EquipRcvdDt = (SELECT TOP 1  ISNULL(fc.EQPT_ITM_RCVD_DT,'')
				   FROM dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK)
				   WHERE fcli.FSA_CPE_LINE_ITEM_ID=fc.FSA_CPE_LINE_ITEM_ID
				     AND fc.EQPT_ITM_RCVD_DT IS NOT NULL),
    VndrPO = (SELECT TOP 1 ISNULL(fcl.PRCH_ORDR_NBR,'')
			  FROM dbo.FSA_ORDR_CPE_LINE_ITEM fcl WITH (NOLOCK)
			  WHERE fcli.FSA_CPE_LINE_ITEM_ID=fcl.FSA_CPE_LINE_ITEM_ID),
	RcptStus = ([dbo].[getOrderStatusForEvent](fo.ORDR_ID))
FROM #M5CPETable mc 
		INNER JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON mc.M5CPEOrdrNbr = fo.FTN
		 INNER JOIN dbo.ORDR od WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
		INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcli WITH (NOLOCK) ON fcli.DEVICE_ID=mc.DevID AND fcli.ORDR_ID=fo.ORDR_ID AND LTRIM(RTRIM(fcli.MDS_DES)) = LTRIM(RTRIM(mc.Descrption))
	WHERE od.ORDR_STUS_ID NOT IN (3,5)
			  AND fo.ORDR_TYPE_CD != 'DC'
			  AND EXISTS (SELECT 'X'
						  FROM dbo.ORDR od2 WITH (NOLOCK) INNER JOIN
						  (select fo2.ftn, max(fo2.ordr_id) as max_ordr_id
							from dbo.fsa_ordr fo2 with (nolock) INNER JOIN
								 dbo.FSA_ORDR_CPE_LINE_ITEM fc2 WITH (NOLOCK) ON fc2.ORDR_ID=fo2.ORDR_ID AND fc2.DEVICE_ID=mc.DevID
							where fo2.ftn = fo.FTN
							  AND fo2.ORDR_TYPE_CD != 'DC'
							group by fo2.ftn) x on x.max_ordr_id=od2.ORDR_ID
						  WHERE od2.ORDR_ID=od.ORDR_ID
							and od2.ORDR_STUS_ID NOT IN (3,5))

select DISTINCT * from #M5CPETable order by DevID, [Descrption]

select DISTINCT * from #M5InvTable order by INST_DT

            
END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END 