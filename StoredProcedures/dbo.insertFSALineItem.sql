USE [COWS]
GO
_CreateObject 'SP','dbo','insertFSALineItem'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/15/2011
-- Description:	Inserts a FSA Line Item into the FSA_ORDR_CPE_LINE_ITEM table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertFSALineItem]
	@FSA_CPE_LINE_ITEM_ID	Int		OUT
	,@ORDR_ID			Int
	,@LINE_ITEM_CD		Varchar(3)
	,@SRVC_LINE_ITEM_CD	Varchar(50)
	,@EQPT_TYPE_ID		Varchar(5)
	,@EQPT_ID			Varchar(20)
	,@MFR_NME			Varchar(50)
	,@CNTRC_TYPE_ID		Varchar(15)
	,@CNTRC_TERM_ID		Varchar(2)
	,@INSTLN_CD			Varchar(1)
	,@MNTC_CD			Varchar(1)
	,@MDS_DES			Varchar(50)
	,@CMPNT_ID			Int
	,@DISC_PL_NBR		Varchar(30)
	,@DISC_FMS_CKT_NBR	Int
	,@DISC_NW_ADR		Varchar(30)
	,@BILL_ITEM_TYPE_ID	SmallInt
	,@LINE_ITEM_DES		Varchar(50)
	,@LINE_ITEM_QTY		SmallInt
	,@MRC_CHG_AMT		Varchar(20)
	,@NRC_CHG_AMT		Varchar(20)
	,@CHG_CD			Varchar(6)
	,@ACTN_CD			Varchar(10)
	,@HasBillItem		Bit
	
	,@FSA_ORGNL_INSTL_ORDR_ID	varchar(20)
	,@CXR_SRVC_ID		Varchar(50)	=	NULL
	,@CXR_CKT_ID		Varchar(50)	=	NULL
	,@ACCS_TYPE_CD		Varchar(5)	=	NULL
	,@SPD_OF_SRVC		Varchar(50)	=	NULL
	,@ACCS_TYPE_DES		Varchar(255)=	NULL
	,@VAS_ID			Int			=	NULL
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	DECLARE @FSA_ORDR_BILL_LINE_ITEM_ID		Int
	SET		@FSA_ORDR_BILL_LINE_ITEM_ID	=	0
	
	INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM WITH (ROWLOCK)
					(ORDR_ID
					,SRVC_LINE_ITEM_CD
					,EQPT_TYPE_ID
					,EQPT_ID
					,MFR_NME
					,CNTRC_TYPE_ID
					,CNTRC_TERM_ID
					,INSTLN_CD
					,MNTC_CD
--					,FSA_ORDR_BILL_LINE_ITEM_ID
					,MDS_DES
					,CMPNT_ID
					,LINE_ITEM_CD
					,DISC_PL_NBR
					,DISC_FMS_CKT_NBR
					,DISC_NW_ADR
					,FSA_ORGNL_INSTL_ORDR_ID
					,CXR_SRVC_ID	
					,CXR_CKT_ID
					,TTRPT_SPD_OF_SRVC_BDWD_DES
					,TTRPT_ACCS_TYPE_CD
					,TTRPT_ACCS_TYPE_DES
					,FSA_ORDR_VAS_ID
					)
		VALUES		(@ORDR_ID
					,@SRVC_LINE_ITEM_CD
					,@EQPT_TYPE_ID 
					,@EQPT_ID
					,@MFR_NME
					,@CNTRC_TYPE_ID
					,@CNTRC_TERM_ID
					,@INSTLN_CD
					,@MNTC_CD
--					,@FSA_ORDR_BILL_LINE_ITEM_ID
					,ISNULL(@LINE_ITEM_DES,@MDS_DES)
					,@CMPNT_ID
					,@LINE_ITEM_CD
					,@DISC_PL_NBR
					,@DISC_FMS_CKT_NBR
					,@DISC_NW_ADR
					,Case	@FSA_ORGNL_INSTL_ORDR_ID
						When	'0'	Then	NULL
						Else	@FSA_ORGNL_INSTL_ORDR_ID
					End
					,@CXR_SRVC_ID	
					,@CXR_CKT_ID
					,@SPD_OF_SRVC
					,@ACCS_TYPE_CD
					,@ACCS_TYPE_DES
					,@VAS_ID
					)
					
	SELECT @FSA_CPE_LINE_ITEM_ID	=	SCOPE_IDENTITY()				
					
	IF	@HasBillItem	= 1
		BEGIN
			EXEC	[dbo].[insertBillingLineItem]
					@FSA_ORDR_BILL_LINE_ITEM_ID	OUT
					,@ORDR_ID
					,@BILL_ITEM_TYPE_ID
					,NULL	--@BIC_CD
					,@LINE_ITEM_DES
					,@LINE_ITEM_QTY
					,NULL	--@ITEM_CD
					,NULL	--@ITEM_DES
					,@MRC_CHG_AMT
					,@NRC_CHG_AMT
					,@ACTN_CD
					,@CHG_CD
					,@FSA_CPE_LINE_ITEM_ID	
		END				
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END

GO