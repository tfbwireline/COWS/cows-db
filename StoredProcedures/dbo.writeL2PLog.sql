USE [COWS]
GO
_CreateObject 'SP','dbo','writeL2PLog'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		sbg9814
-- Create date: 07/11/2011
-- Description:	Inserts a Log everytime L2P is dipped.
-- =========================================================
ALTER PROCEDURE [dbo].[writeL2PLog]
	@H1				Int
	,@ORDR_ID		Int
	,@L2P_UPDT_DT	DateTime	=	NULL
	,@ERROR_MSG		Varchar(500)=	''
	,@EVENT_ID		VARCHAR(50)=''
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	INSERT INTO dbo.L2P_LOG	WITH (ROWLOCK)
				(H1
				,ORDR_ID
				,L2P_UPDT_DT
				,ERROR_MSG
				,EVENT_ID)
		VALUES	(@H1
				,CASE @ORDR_ID WHEN 0 THEN NULL
				ELSE @ORDR_ID END
				,@L2P_UPDT_DT
				,@ERROR_MSG
				,CASE LEN(@EVENT_ID) WHEN 0 THEN NULL
				ELSE CONVERT(Int,@EVENT_ID)END)
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO
