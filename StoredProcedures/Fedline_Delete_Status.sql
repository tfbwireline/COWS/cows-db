USE [COWS]
GO
_CreateObject 'SP','dbo','Fedline_Delete_Status'
GO
/****** Object:  StoredProcedure [dbo].[Fedline_Delete_Status]    Script Date: 05/30/2013 12:44:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Description:	This SP can be modified byt commenting the ALTER line out and putting
-- in the Event ID to change the status to DELETE on a FEDLINE EVENT.  The query will change the status
-- to DELETE and modify the TIME SLOT Availability count.
--=================================================

-- EXEC dbo.Fedline_Delete_Status 7401    



ALTER  PROCEDURE [dbo].[Fedline_Delete_Status] 
		 @Event_ID INT

AS 

	IF EXISTS (SELECT td.EVENT_ID FROM dbo.FEDLINE_EVENT_TADPOLE_DATA td WITH (NOLOCK)
					INNER JOIN dbo.FEDLINE_EVENT_USER_DATA eu WITH (NOLOCK)ON td.EVENT_ID = eu.EVENT_ID
				 WHERE td.EVENT_ID	= @Event_ID AND REC_STUS_ID = 1)
		BEGIN	
	
			UPDATE dbo.FEDLINE_EVENT_USER_DATA
				SET EVENT_STUS_ID = 8
			WHERE EVENT_ID = @Event_ID
		 
			
			DECLARE @TME_SLOT_ID INT,
					@STRT_TME DATETIME
		
			SET @STRT_TME = (SELECT STRT_TME FROM dbo.FEDLINE_EVENT_TADPOLE_DATA WITH (NOLOCK)
								WHERE EVENT_ID = @Event_ID AND REC_STUS_ID = 1)   		 
			
			SET @TME_SLOT_ID = (SELECT TME_SLOT_ID from dbo.LK_EVENT_TYPE_TME_SLOT WITH (NOLOCK)
				  				   WHERE TME_SLOT_STRT_TME = CONVERT(VARCHAR(8),@STRT_TME, 114)
				  				    AND EVENT_TYPE_ID = 9)
			
			IF EXISTS (SELECT 1 from dbo.EVENT_TYPE_RSRC_AVLBLTY WITH (NOLOCK)
						WHERE CONVERT(VARCHAR(10), DAY_DT, 101) = CONVERT(VARCHAR(10), @STRT_TME, 101)
								AND TME_SLOT_ID = @TME_SLOT_ID 
								AND EVENT_TYPE_ID = 9
								AND CURR_AVAL_TME_SLOT_CNT > 0)
				
				BEGIN 	  
				
					UPDATE EVENT_TYPE_RSRC_AVLBLTY
						SET CURR_AVAL_TME_SLOT_CNT = CURR_AVAL_TME_SLOT_CNT - 1
					WHERE CONVERT(VARCHAR(10), DAY_DT, 101) = CONVERT (VARCHAR (10), @STRT_TME, 101)
							AND TME_SLOT_ID = @TME_SLOT_ID AND EVENT_TYPE_ID = 9

				END
			IF EXISTS (SELECT 1 from dbo.EVENT_NVLD_TME_SLOT WITH (NOLOCK)
						WHERE EVENT_ID = @Event_ID)
				BEGIN
					DELETE dbo.EVENT_NVLD_TME_SLOT WHERE EVENT_ID = @Event_ID
				END
						
		END
	
		

