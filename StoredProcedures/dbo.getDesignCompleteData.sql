USE [COWS]
GO
_CreateObject 'SP','dbo','getDesignCompleteData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================================
-- Author:		sbg9814
-- Create date: 09/12/2011
-- Description:	Gets the data for the Design Complete API for ODIE.
-- BR 12: Ability to disable the push of the 1MB to ODIE for all events
-- Updated By:		vn370313/ Md M Monir
-- Create date: 03/03/2017
-- Description:	Added SRVC_AVLBLTY_HRS  and  ServiceAssuranceTimeZone  at MDS_EVENT  for first Return Result
-- ==================================================================================
-- To test in Dev run:	EXEC dbo.getDesignCompleteData	18 
ALTER PROCEDURE [dbo].[getDesignCompleteData] 
	@REQ_ID	Int	
AS
BEGIN
Begin Try
DECLARE	@EVENT_ID		Int
DECLARE	@TAB_SEQ_NBR	TinyInt
SET	@EVENT_ID		=	0
SET	@TAB_SEQ_NBR	=	0

OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

SELECT		@EVENT_ID		=	COALESCE(MDS_EVENT_ID, 0)
			,@TAB_SEQ_NBR	=	COALESCE(TAB_SEQ_NBR, 1)
	FROM	dbo.ODIE_REQ	WITH (NOLOCK)
	WHERE	REQ_ID	=	@REQ_ID
	
DECLARE	@ACTY_TYPE		Varchar(20)

SET	@ACTY_TYPE		=	''	

IF EXISTS (SELECT 'x' FROM dbo.UCaaS_EVENT WITH (NOLOCK) WHERE EVENT_ID = @EVENT_ID)
BEGIN
	SELECT @ACTY_TYPE = CASE WHEN uc.UCaaS_ACTY_TYPE_ID IN (1,2) THEN 'VoiceInstall'
							 ELSE 'VoiceChange' END
	FROM dbo.UCaaS_EVENT uc WITH (NOLOCK)
	WHERE uc.EVENT_ID = @EVENT_ID
END
ELSE IF EXISTS (SELECT 'x' FROM dbo.MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EVENT_ID)
BEGIN
	SELECT TOP 1 @ACTY_TYPE	=	CASE WHEN ((me.MDS_ACTY_TYPE_ID IN (4,6)) OR (me.MDS_ACTY_TYPE_ID	IN	(2,5)	AND	(COALESCE(mm.MDS_MAC_ACTY_ID,0)	IN	(11,17,18))))
								 THEN 'Install'
								 WHEN (me.MDS_ACTY_TYPE_ID	IN	(2,5)	AND	(COALESCE(mm.MDS_MAC_ACTY_ID,0) = 5))
								 THEN 'MAC-Move'
								 WHEN (me.MDS_ACTY_TYPE_ID	IN	(2,5)	AND	(COALESCE(mm.MDS_MAC_ACTY_ID,0) IN (6,7,8)))
								 THEN 'MAC-Change'
								 WHEN (me.MDS_ACTY_TYPE_ID	IN	(2,5)	AND	(COALESCE(mm.MDS_MAC_ACTY_ID,0) IN (1,2,3,9,12,13,14,15,16)))
								 THEN 'MAC-Change2'
								 ELSE '' END
	FROM		dbo.MDS_EVENT_NEW		me	WITH (NOLOCK)
	LEFT JOIN	dbo.MDS_EVENT_MAC_ACTY	mm	WITH (NOLOCK)	ON	me.EVENT_ID	=	mm.EVENT_ID
	WHERE		me.EVENT_ID		=	@EVENT_ID
END
ELSE IF EXISTS (SELECT 'x' FROM dbo.MDS_EVENT WITH (NOLOCK) WHERE EVENT_ID = @EVENT_ID)
BEGIN
	SELECT TOP 1 @ACTY_TYPE	=	CASE WHEN ((me.MDS_ACTY_TYPE_ID IN (4,6)) OR (me.MDS_ACTY_TYPE_ID	IN	(2,5)	AND	(COALESCE(mm.MDS_MAC_ACTY_ID,0)	IN	(11,14,18,19,20))))
								 THEN 'MDSInstall'
								 WHEN (me.MDS_ACTY_TYPE_ID	IN	(2,5)	AND	(COALESCE(mm.MDS_MAC_ACTY_ID,0)	= 17)) THEN 'MDSMACGuest'
								 WHEN (me.MDS_ACTY_TYPE_ID	IN	(2,5)	AND	(COALESCE(mm.MDS_MAC_ACTY_ID,0) = 5))
								 THEN 'MDSMACMove'
								 WHEN (me.MDS_ACTY_TYPE_ID	IN	(2,5)	AND	(COALESCE(mm.MDS_MAC_ACTY_ID,0) IN (6,7,8)))
								 THEN 'MDSMACSwap'
								 WHEN (me.MDS_ACTY_TYPE_ID	IN	(2,5)	AND	(COALESCE(mm.MDS_MAC_ACTY_ID,0) IN (1,2,3,9,12,13,15,16)))
								 THEN 'MDSMACChange'
								 ELSE '' END
	FROM		dbo.MDS_EVENT		me	WITH (NOLOCK)
	LEFT JOIN	dbo.MDS_EVENT_MAC_ACTY	mm	WITH (NOLOCK)	ON	me.EVENT_ID	=	mm.EVENT_ID
	WHERE		me.EVENT_ID		=	@EVENT_ID
END


IF	@ACTY_TYPE	=	''
	BEGIN
		UPDATE		dbo.ODIE_REQ	WITH (ROWLOCK)
			SET		STUS_ID	=	21
			WHERE	REQ_ID	=	@REQ_ID
	END
ELSE IF	@EVENT_ID	!=	0	AND	@TAB_SEQ_NBR	!=	0	
	BEGIN	
		
		SELECT		DISTINCT	@REQ_ID																AS	TranID
						,@ACTY_TYPE															AS	ActivityType
						
						,me.H1_ID																AS	H1
						,me.EVENT_ID														AS	EventID
						,dbo.getH5H6ByEventID(@EVENT_ID, @TAB_SEQ_NBR)						AS	H5_H6_Cust_ID
						,'' AS SITE_ID_TXT
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.STREET_ADR_1) ELSE fm.SITE_ADR END, '')							AS	[Address]
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.FLR_ID) ELSE fm.FLR_BLDG_NME END, '')					AS	[Floor]
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE fm.CTY_NME END, '')						AS	City
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.STT_PRVN_NME) ELSE fm.STT_PRVN_NME END, '')					AS	[State]
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.ZIP_PSTL_CD) ELSE fm.ZIP_CD END, '')						AS	ZipCode	
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTRY_RGN_NME) ELSE fm.CTRY_RGN_NME END, '')					AS	Country				
						,COALESCE(fm.SRVC_ASSRN_POC_INTL_PHN_CD, '') + COALESCE(fm.SRVC_ASSRN_POC_PHN_NBR, '')		AS	ServiceAssuranceContactPhone
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.SRVC_ASSRN_POC_NME) ELSE fm.SRVC_ASSRN_POC_NME END, '')			AS	ServiceAssuranceContactName
						,COALESCE(fm.SA_POC_HR_NME,'') AS ServiceAssuranceContactHours
						,CASE WHEN (CHARINDEX('UTC-05:00', COALESCE(fm.TME_ZONE_ID,'')) > 0) THEN 'Eastern'
							  WHEN (CHARINDEX('UTC-06:00', COALESCE(fm.TME_ZONE_ID,'')) > 0) THEN 'Central'
							  WHEN (CHARINDEX('UTC-07:00', COALESCE(fm.TME_ZONE_ID,'')) > 0) THEN 'Mountain'
							  WHEN (CHARINDEX('UTC-08:00', COALESCE(fm.TME_ZONE_ID,'')) > 0) THEN 'Pacific'
							  ELSE 'Zulu' END
						  AS ServiceAssuranceTimeZone
						,Case	COALESCE(US_INTL_ID, '')
							When	'I'	Then	'International'
							Else	'Domestic'
						End											AS	Domestic_International_Flag
			FROM		dbo.MDS_EVENT_NEW		me	WITH (NOLOCK)	
			INNER JOIN	dbo.LK_MDS_ACTY_TYPE	la	WITH (NOLOCK)	ON	me.MDS_ACTY_TYPE_ID	=	la.MDS_ACTY_TYPE_ID
			INNER JOIN	dbo.FSA_MDS_EVENT_NEW	fm	WITH (NOLOCK)	ON	me.EVENT_ID		=	fm.EVENT_ID
			INNER JOIN	dbo.[EVENT]				ev  WITH (NOLOCK) 	ON	ev.EVENT_ID		=	me.EVENT_ID
			LEFT JOIN	dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=me.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=8
			WHERE		me.EVENT_ID		=	@EVENT_ID
				AND		fm.TAB_SEQ_NBR	=	@TAB_SEQ_NBR
			UNION
			SELECT	DISTINCT @REQ_ID																AS	TranID
						,@ACTY_TYPE															AS	ActivityType
						
						,me.H1																AS	H1
						,me.EVENT_ID														AS	EventID
						,me.H6					AS	H5_H6_Cust_ID
						,ISNULL(me.SITE_ID_TXT,'') AS SITE_ID_TXT
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.STREET_ADR_1) ELSE me.STREET_ADR END, '')							AS	[Address]
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.FLR_ID) ELSE me.FLR_BLDG_NME END, '')					AS	[Floor]
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE me.CTY_NME END, '')						AS	City
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.STT_PRVN_NME) ELSE me.STT_PRVN_NME END, '')					AS	[State]
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.ZIP_PSTL_CD) ELSE me.ZIP_CD END, '')						AS	ZipCode	
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTRY_RGN_NME) ELSE me.CTRY_RGN_NME END, '')					AS	Country				
						,COALESCE(me.SRVC_ASSRN_POC_INTL_PHN_CD, '') + COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.SRVC_ASSRN_POC_PHN_NBR) ELSE me.SRVC_ASSRN_POC_PHN_NBR END, '')		AS	ServiceAssuranceContactPhone
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.SRVC_ASSRN_POC_NME) ELSE me.SRVC_ASSRN_POC_NME END, '')			AS	ServiceAssuranceContactName
						/*
						,'' AS ServiceAssuranceContactHours
						,'' AS ServiceAssuranceTimeZone
						*/  ---vn370313
						,COALESCE(me.SRVC_AVLBLTY_HRS,'') AS ServiceAssuranceContactHours
						,CASE WHEN (CHARINDEX('UTC-05:00', COALESCE(me.[SRVC_TME_ZN_CD],'')) > 0) THEN 'Eastern'
							  WHEN (CHARINDEX('UTC-06:00', COALESCE(me.[SRVC_TME_ZN_CD],'')) > 0) THEN 'Central'
							  WHEN (CHARINDEX('UTC-07:00', COALESCE(me.[SRVC_TME_ZN_CD],'')) > 0) THEN 'Mountain'
							  WHEN (CHARINDEX('UTC-08:00', COALESCE(me.[SRVC_TME_ZN_CD],'')) > 0) THEN 'Pacific'
							  ELSE 'Zulu' END
						  AS ServiceAssuranceTimeZone
						,Case	COALESCE(US_INTL_CD,'')
							When	'I'	Then	'International'
							Else	'Domestic'
						End											AS	Domestic_International_Flag
			FROM		dbo.MDS_EVENT		me	WITH (NOLOCK)
			INNER JOIN	dbo.[EVENT]			ev  WITH (NOLOCK) 	ON	ev.EVENT_ID		=	me.EVENT_ID
			LEFT JOIN	dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=me.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=7
			WHERE		me.EVENT_ID		=	@EVENT_ID
			UNION
			SELECT	DISTINCT @REQ_ID																AS	TranID
						,@ACTY_TYPE															AS	ActivityType
						
						,uc.H1																AS	H1
						,uc.EVENT_ID														AS	EventID
						,uc.H6					AS	H5_H6_Cust_ID
						,COALESCE(uc.SITE_ID,'') AS SITE_ID_TXT
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.STREET_ADR_1) ELSE uc.STREET_ADR END, '')							AS	[Address]
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.FLR_ID) ELSE uc.FLR_BLDG_NME END, '')					AS	[Floor]
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE uc.CTY_NME END, '')						AS	City
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.STT_PRVN_NME) ELSE uc.STT_PRVN_NME END, '')					AS	[State]
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.ZIP_PSTL_CD) ELSE uc.ZIP_CD END, '')						AS	ZipCode	
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTRY_RGN_NME) ELSE uc.CTRY_RGN_NME END, '')					AS	Country								
						,COALESCE(uc.SRVC_ASSRN_POC_INTL_PHN_CD, '') + COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.SRVC_ASSRN_POC_PHN_NBR) ELSE uc.SRVC_ASSRN_POC_PHN_NBR END, '')		AS	ServiceAssuranceContactPhone
						,COALESCE(CASE WHEN (ev.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.SRVC_ASSRN_POC_NME) ELSE uc.SRVC_ASSRN_POC_NME END, '')			AS	ServiceAssuranceContactName
						,'' AS ServiceAssuranceContactHours
						,'' AS ServiceAssuranceTimeZone
						,Case	COALESCE(US_INTL_CD,'')
							When	'I'	Then	'International'
							Else	'Domestic'
						End											AS	Domestic_International_Flag
			FROM		dbo.UCaaS_EVENT		uc	WITH (NOLOCK)
			INNER JOIN	dbo.[EVENT]			ev  WITH (NOLOCK) 	ON	ev.EVENT_ID		=	uc.EVENT_ID
			LEFT JOIN	dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=uc.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=20
			WHERE		uc.EVENT_ID		=	@EVENT_ID


		SELECT DISTINCT Top 1	COALESCE(mns.MNS_FTN_NBR,'0')			AS	FTN
								,COALESCE(lm.MDS_SRVC_TIER_DES, '')	AS	Service_Tier
			FROM		dbo.FSA_MDS_EVENT_NEW	fe	WITH (NOLOCK)
			INNER JOIN	dbo.MDS_EVENT_MNS		mns	WITH (NOLOCK)	ON	fe.FSA_MDS_EVENT_ID		=	mns.FSA_MDS_EVENT_ID
			INNER JOIN	dbo.LK_MDS_SRVC_TIER	lm	WITH (NOLOCK)	ON	mns.MDS_SRVC_TIER_ID	=	lm.MDS_SRVC_TIER_ID
			WHERE		fe.EVENT_ID		=	@EVENT_ID
				AND		fe.TAB_SEQ_NBR	=	1
				AND		fe.CMPLTD_CD	=	1	
		UNION
		SELECT DISTINCT Top 1	'' AS FTN
								,COALESCE(lm.MDS_SRVC_TIER_DES, '')	AS	Service_Tier
			FROM		dbo.EVENT_DEV_SRVC_MGMT	eds	WITH (NOLOCK)
			INNER JOIN	dbo.EVENT_DEV_CMPLT		edc	WITH (NOLOCK)	ON	edc.EVENT_ID=eds.EVENT_ID
			INNER JOIN	dbo.LK_MDS_SRVC_TIER	lm	WITH (NOLOCK)	ON	eds.MNS_SRVC_TIER_ID	=	lm.MDS_SRVC_TIER_ID
			WHERE		eds.EVENT_ID		=	@EVENT_ID
				AND		edc.CMPLTD_CD	=	1
				AND		eds.REC_STUS_ID = 	1
				AND		edc.REC_STUS_ID = 	1
				AND		edc.ODIE_SENT_DT IS NULL
				AND 	((eds.ODIE_CD IS NULL) OR (eds.ODIE_CD = 0))
			
		
		SELECT 		DISTINCT	COALESCE(mact.ODIE_DEV_NME,'')			AS	device_id	
						,''											AS Mach5DeviceID
						,''											AS ServiceTier
						,'Y'										AS	Redesign_Flag
						,''											AS OdieH6
						,COALESCE(mact.RDSN_NBR, '')				AS	RedesignNumber
						,COALESCE(lm.MANF_NME, '')				AS	PrimaryVendorName
						,Convert(Varchar(10), getDate(), 101)	AS	Equipment_Install_Date
						,''										AS  CoverageBy
						,''										AS  SerialNumber
						,''										AS  ContractNumber
						,''										AS  ContractType
						,''										AS  VendorExpirationDate
			FROM		dbo.FSA_MDS_EVENT_NEW		fm	WITH (NOLOCK)
			INNER JOIN	dbo.MDS_EVENT_ODIE_DEV_NME	odie WITH (NOLOCK)	ON	odie.FSA_MDS_EVENT_ID	=	fm.FSA_MDS_EVENT_ID
			INNER JOIN	dbo.MDS_MNGD_ACT_NEW		mact WITH (NOLOCK)	ON	mact.EVENT_ID			=	fm.EVENT_ID 			
																		AND	mact.ODIE_DEV_NME		=	odie.ODIE_DEV_NME
			LEFT JOIN	dbo.LK_DEV_MANF				lm	WITH (NOLOCK)	ON	mact.MANF_ID			=	lm.MANF_ID
			WHERE		fm.EVENT_ID		=	@EVENT_ID
				AND		fm.TAB_SEQ_NBR	=	@TAB_SEQ_NBR
				AND		mact.SLCTD_CD	=	1
		UNION
		SELECT DISTINCT	COALESCE(edc.ODIE_DEV_NME,'')			AS	device_id	
						,COALESCE(ecd.DEVICE_ID,COALESCE(eds.DEVICE_ID,''))				AS  Mach5DeviceID
						,ISNULL(lms.MDS_SRVC_TIER_DES, '')	    AS  ServiceTier
						,'Y'										AS	Redesign_Flag
						,edc.H6									AS OdieH6
						,COALESCE(modev.RDSN_NBR, '')				AS	RedesignNumber
						,CASE WHEN (@ACTY_TYPE IN ('MDSMACChange', 'VoiceChange')) THEN '' ELSE COALESCE(lm.MANF_NME, '') END AS	PrimaryVendorName
						,CASE WHEN (@ACTY_TYPE IN ('MDSMACChange', 'VoiceChange')) THEN '' ELSE Convert(Varchar(10), getDate(), 101) END AS	Equipment_Install_Date
						,CASE WHEN (@ACTY_TYPE IN ('MDSMACChange', 'VoiceChange')) THEN '' ELSE CASE WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Customer') THEN 'Customer' 
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'CPE') THEN 'Cisco'  
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'NCR') THEN 'NCR/Cisco' 
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Orange') THEN 'Orange/Cisco' 
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Velociti') THEN 'N/A' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Customer') THEN 'Customer' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'CPE') THEN 'Juniper' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'NCR') THEN 'NCR/Juniper' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Orange') THEN 'Orange/Juniper' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Velociti') THEN 'N/A'
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Customer') THEN 'Customer' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'CPE') THEN 'N/A' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'NCR') THEN 'N/A' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Orange') THEN 'N/A' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Velociti') THEN 'Ruckus/Velociti'	ELSE ISNULL(lss.SRVC_ASSRN_SITE_SUPP_DES,'')	END	END	AS  CoverageBy
						,CASE WHEN (@ACTY_TYPE IN ('MDSMACChange', 'VoiceChange')) THEN '' ELSE ISNULL(foc.SERIAL_NBR,'')			END				AS  SerialNumber
						,CASE WHEN (@ACTY_TYPE IN ('MDSMACChange', 'VoiceChange')) THEN '' ELSE CASE WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Customer') THEN '1111111' 
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'CPE') THEN '1111111'  
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'NCR') THEN '1111111' 
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Orange') THEN '1111111' 
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Velociti') THEN '1111111' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Customer') THEN '1111111' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'CPE') THEN 'Use Serial# for RMA' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'NCR') THEN 'Use Serial# for RMA' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Orange') THEN 'Use Serial# for RMA' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Velociti') THEN 'N/A'
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Customer') THEN '1111111' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'CPE') THEN 'N/A' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'NCR') THEN 'N/A' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Orange') THEN 'N/A' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Velociti') THEN 'Lifetime Warranty'	 ELSE ISNULL(foc.PRCH_ORDR_TXT,'')	END	END	AS  ContractNumber
						,CASE WHEN (@ACTY_TYPE IN ('MDSMACChange', 'VoiceChange')) THEN '' ELSE CASE WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Customer') THEN 'Customer' 
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'CPE') THEN 'Customer'  
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'NCR') THEN 'Customer' 
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Orange') THEN 'Customer' 
							WHEN (CHARINDEX('cisco',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Velociti') THEN 'Customer' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Customer') THEN 'Customer' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'CPE') THEN '24x7 (Next day)' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'NCR') THEN '24x7 (Next day)' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Orange') THEN '24x7 (Next day)' 
							WHEN (CHARINDEX('juniper',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Velociti') THEN 'N/A'
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Customer') THEN 'Customer' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'CPE') THEN 'N/A' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'NCR') THEN 'N/A' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Orange') THEN 'N/A' 
							WHEN (CHARINDEX('Ruckus',lm.MANF_NME) > 0) AND (lss.SRVC_ASSRN_SITE_SUPP_DES = 'Velociti') THEN 'N/A'	ELSE ISNULL(foc.MTRL_RQSTN_TXT,'')	END	 END	AS  ContractType
						,CASE WHEN (@ACTY_TYPE IN ('MDSMACChange', 'VoiceChange')) THEN '' ELSE ISNULL(foc.INTL_MNTC_SCHRG_TXT,'')		END		AS  VendorExpirationDate
			FROM		dbo.EVENT_DEV_CMPLT		edc	WITH (NOLOCK)	
			INNER JOIN  dbo.MDS_EVENT_ODIE_DEV modev WITH (NOLOCK)	ON modev.EVENT_ID = edc.EVENT_ID AND modev.ODIE_DEV_NME = edc.ODIE_DEV_NME
			INNER JOIN	dbo.LK_SRVC_ASSRN_SITE_SUPP lss WITH (NOLOCK) ON lss.SRVC_ASSRN_SITE_SUPP_ID = modev.SRVC_ASSRN_SITE_SUPP_ID
			LEFT JOIN  dbo.EVENT_CPE_DEV	ecd	WITH (NOLOCK) ON	edc.EVENT_ID=ecd.EVENT_ID AND edc.ODIE_DEV_NME = ecd.ODIE_DEV_NME AND ecd.REC_STUS_ID = 	1
			LEFT JOIN  dbo.EVENT_DEV_SRVC_MGMT eds WITH (NOLOCK) ON edc.EVENT_ID=eds.EVENT_ID AND edc.ODIE_DEV_NME = eds.ODIE_DEV_NME AND eds.REC_STUS_ID = 	1
			LEFT JOIN  dbo.LK_MDS_SRVC_TIER lms WITH (NOLOCK) ON lms.MDS_SRVC_TIER_ID = eds.MNS_SRVC_TIER_ID
			LEFT JOIN  dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN = ecd.CPE_ORDR_NBR
			LEFT JOIN  dbo.FSA_ORDR_CPE_LINE_ITEM focl WITH (NOLOCK) ON focl.DEVICE_ID = ecd.DEVICE_ID AND fo.ORDR_ID=focl.ORDR_ID
			LEFT JOIN  dbo.LK_DEV_MANF				lm	WITH (NOLOCK)	ON	modev.MANF_ID			=	lm.MANF_ID
			LEFT JOIN  dbo.FSA_ORDR_CSC		foc WITH (NOLOCK) ON foc.ORDR_ID = focl.ORDR_ID		
			WHERE		edc.EVENT_ID		=	@EVENT_ID
				AND		edc.REC_STUS_ID=1
				--AND		foc.ORDR_ID IS NOT NULL
				AND		edc.CMPLTD_CD	=	1
				AND		((lms.MDS_SRVC_TIER_DES IS NULL) 
						OR ((lms.MDS_SRVC_TIER_DES IS NOT NULL) 
							AND ((lms.MDS_SRVC_TIER_DES != 'MSS') OR ((lms.MDS_SRVC_TIER_DES = 'MSS') AND (LEN(ISNULL(edc.ODIE_DEV_NME,''))>0)))
							)
						)
				AND		edc.ODIE_SENT_DT IS NULL
				AND		((ecd.ODIE_CD IS NULL) OR (ecd.ODIE_CD = 0))
				AND		((ecd.DEVICE_ID IS NOT NULL) OR (eds.DEVICE_ID IS NOT NULL) OR (lms.MDS_SRVC_TIER_DES IS NOT NULL))
				-- The additional condition was added by Sarah Sandoval [04222021] to send only common device to ODIE on EVENT_CPE_DEV and EVENT_DEV_SRVC_MGMT
				AND		ecd.DEVICE_ID = eds.DEVICE_ID
		UNION
		SELECT DISTINCT	COALESCE(edc.ODIE_DEV_NME,'')			AS	device_id	
						,COALESCE(ecd.DEVICE_ID,COALESCE(eds.DEVICE_ID,''))				AS  Mach5DeviceID
						,ISNULL(lms.MDS_SRVC_TIER_DES, '')	    AS  ServiceTier
						,'Y'										AS	Redesign_Flag
						,edc.H6									AS OdieH6
						,COALESCE(udev.RDSN_NBR, '')				AS	RedesignNumber
						,CASE WHEN (@ACTY_TYPE IN ('MDSMACChange', 'VoiceChange')) THEN '' ELSE COALESCE(lm.MANF_NME, '')		END		AS	PrimaryVendorName
						,CASE WHEN (@ACTY_TYPE IN ('MDSMACChange', 'VoiceChange')) THEN '' ELSE Convert(Varchar(10), getDate(), 101) END	AS	Equipment_Install_Date
						,''										AS  CoverageBy
						,''										AS  SerialNumber
						,''										AS  ContractNumber
						,''										AS  ContractType
						,''										AS  VendorExpirationDate
			FROM		dbo.EVENT_DEV_CMPLT		edc	WITH (NOLOCK)	
			INNER JOIN  dbo.UCaaS_EVENT_ODIE_DEV udev WITH (NOLOCK)	ON udev.EVENT_ID = edc.EVENT_ID AND udev.ODIE_DEV_NME = edc.ODIE_DEV_NME
			LEFT JOIN  dbo.EVENT_CPE_DEV	ecd	WITH (NOLOCK) ON	edc.EVENT_ID=ecd.EVENT_ID AND edc.ODIE_DEV_NME = ecd.ODIE_DEV_NME AND ecd.REC_STUS_ID = 	1
			LEFT JOIN  dbo.EVENT_DEV_SRVC_MGMT eds WITH (NOLOCK) ON edc.EVENT_ID=eds.EVENT_ID AND edc.ODIE_DEV_NME = eds.ODIE_DEV_NME AND eds.REC_STUS_ID = 	1
			LEFT JOIN  dbo.LK_MDS_SRVC_TIER lms WITH (NOLOCK) ON lms.MDS_SRVC_TIER_ID = eds.MNS_SRVC_TIER_ID
			LEFT JOIN	dbo.LK_DEV_MANF				lm	WITH (NOLOCK)	ON	udev.MANF_ID			=	lm.MANF_ID
			WHERE		edc.EVENT_ID		=	@EVENT_ID
				AND		edc.CMPLTD_CD	=	1
				AND		edc.REC_STUS_ID=1
				AND		((lms.MDS_SRVC_TIER_DES IS NULL) 
						OR ((lms.MDS_SRVC_TIER_DES IS NOT NULL) 
							AND ((lms.MDS_SRVC_TIER_DES != 'MSS') OR ((lms.MDS_SRVC_TIER_DES = 'MSS') AND (LEN(ISNULL(edc.ODIE_DEV_NME,''))>0)))
							)
						)
				AND		edc.ODIE_SENT_DT IS NULL
				AND		((ecd.ODIE_CD IS NULL) OR (ecd.ODIE_CD = 0))
				AND		((ecd.DEVICE_ID IS NOT NULL) OR (eds.DEVICE_ID IS NOT NULL) OR (lms.MDS_SRVC_TIER_DES IS NOT NULL))
				-- The additional condition was added by Sarah Sandoval [04222021] to send only common device to ODIE on EVENT_CPE_DEV and EVENT_DEV_SRVC_MGMT
				AND		ecd.DEVICE_ID = eds.DEVICE_ID
				
				
	SELECT DISTINCT       
	  COALESCE(mact.ODIE_DEV_NME,'')	   AS	service_device_id
	 ,COALESCE(CONVERT(Varchar,mes.MACH5_H6_SRVC_ID), '')    AS MACH5_H6_SRVC_ID  -- changed int to varchar by replacing 0 with ''        
     ,COALESCE (lst.SRVC_TYPE_DES, '')  AS ServiceType   
     ,COALESCE (lm3v.THRD_PARTY_VNDR_DES, '')  AS ThirdPartyVendor     
	 ,COALESCE(mes.THRD_PARTY_ID, '')    AS ThirdPartyID      
     ,COALESCE(lm3s.THRD_PARTY_SRVC_LVL_DES, '')  AS ThirdPartyServiceLevel          
     ,COALESCE(Convert(Varchar(10), mes.ACTV_DT, 101), '') AS ActivationDate
	 ,COALESCE(mes.CMNT_TXT,'') AS Comments
   FROM  dbo.FSA_MDS_EVENT_NEW fm WITH (NOLOCK)       
     INNER JOIN dbo.MDS_EVENT_SRVC AS mes WITH (NOLOCK) ON fm.FSA_MDS_EVENT_ID = mes.FSA_MDS_EVENT_ID       
	 INNER JOIN	dbo.MDS_EVENT_ODIE_DEV_NME	odie WITH (NOLOCK)	ON	odie.FSA_MDS_EVENT_ID	=	fm.FSA_MDS_EVENT_ID
	 INNER JOIN	dbo.MDS_MNGD_ACT_NEW		mact WITH (NOLOCK)	ON	mact.EVENT_ID			=	fm.EVENT_ID 			
																		AND	mact.ODIE_DEV_NME		=	odie.ODIE_DEV_NME
     LEFT JOIN dbo.LK_MDS_3RDPARTY_VNDR   lm3v WITH (NOLOCK)  ON mes.THRD_PARTY_VNDR_ID = lm3v.THRD_PARTY_VNDR_ID                     
     LEFT JOIN dbo.LK_MDS_SRVC_TYPE     lst WITH (NOLOCK) ON mes.SRVC_TYPE_ID    = lst.SRVC_TYPE_ID 
	 LEFT JOIN dbo.LK_MDS_3RDPARTY_SRVC_LVL lm3s WITH (NOLOCK) ON lm3s.THRD_PARTY_SRVC_LVL_ID = mes.THRD_PARTY_SRVC_LVL_ID
	 WHERE fm.EVENT_ID		=	@EVENT_ID
	  AND  fm.TAB_SEQ_NBR	=	@TAB_SEQ_NBR
	 UNION
	 SELECT DISTINCT       
	  COALESCE(mes.ODIE_DEV_NME,'')	   AS	service_device_id
	 ,COALESCE(mes.MACH5_SRVC_ORDR_ID, '')    AS MACH5_H6_SRVC_ID -- changed int to varchar by replacing 0 with ''        
     ,COALESCE (lst.SRVC_TYPE_DES, '')  AS ServiceType   
     ,COALESCE (lm3v.THRD_PARTY_VNDR_DES, '')  AS ThirdPartyVendor     
	 ,COALESCE(mes.THRD_PARTY_ID, '')    AS ThirdPartyID      
     ,COALESCE(lm3s.THRD_PARTY_SRVC_LVL_DES, '')  AS ThirdPartyServiceLevel          
     ,COALESCE(Convert(Varchar(10), mes.ACTV_DT, 101), '') AS ActivationDate
	 ,COALESCE(mes.CMNT_TXT,'') AS Comments
	FROM  dbo.MDS_EVENT fm WITH (NOLOCK)       
     INNER JOIN dbo.MDS_EVENT_SITE_SRVC AS mes WITH (NOLOCK) ON fm.EVENT_ID = mes.EVENT_ID     
     LEFT JOIN dbo.LK_MDS_3RDPARTY_VNDR   lm3v WITH (NOLOCK)  ON mes.THRD_PARTY_VNDR_ID = lm3v.THRD_PARTY_VNDR_ID                     
     LEFT JOIN dbo.LK_MDS_SRVC_TYPE     lst WITH (NOLOCK) ON mes.SRVC_TYPE_ID    = lst.SRVC_TYPE_ID 
	 LEFT JOIN dbo.LK_MDS_3RDPARTY_SRVC_LVL lm3s WITH (NOLOCK) ON lm3s.THRD_PARTY_SRVC_LVL_ID = mes.THRD_PARTY_SRVC_LVL_ID
	 WHERE fm.EVENT_ID		= 	@EVENT_ID
	  
	END
End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END