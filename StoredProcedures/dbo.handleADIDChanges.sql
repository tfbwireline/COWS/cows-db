USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[handleADIDChanges]    Script Date: 07/06/2021 10:13:01 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		John Caranay
-- Create date: 06/28/2021
-- Description: Handle new ADID for Legacy Users
-- =============================================
-- EXECUTE [dbo].[handleADIDChanges] 'Jullom1', 'juadtest', 'Jonathan.Ullom1@T-Mobile.com'
CREATE PROCEDURE [dbo].[handleADIDChanges] --'dx160495', '', '', 0
 @USER_ADID VARCHAR(10),
 @OLD_USER_ADID VARCHAR(10),
 @NTID_EMAIL VARCHAR(100)
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	UPDATE LK_USER 
		SET 
			OLD_USER_ADID = @OLD_USER_ADID,
			USER_ADID = @USER_ADID,
			EMAIL_ADR = @NTID_EMAIL,
			MODFD_DT = CURRENT_TIMESTAMP
	WHERE USER_ADID = @OLD_USER_ADID;

	---------------------------------------------------------------------------------------------------
	----------------------------------------LK_SYS_CFG - START ----------------------------------------
	---------------------------------------------------------------------------------------------------
	-- PROCESS NEW LK_SYS_CFG for NEW USER

	DECLARE @INDEX INT;
	DECLARE @USER_SYS_CFG_COUNT INT;
	SET @INDEX = 0;

	DECLARE @NTID_EMAIL_COPY NVARCHAR(100) = @NTID_EMAIL;
	DECLARE @OLD_EMAIL NVARCHAR(100) = REPLACE(@NTID_EMAIL_COPY, '@t-mobile.com', '@sprint.com');
	DECLARE @USER_SYS_CFG table (
			ARRAYINDEX INT identity(1,1),
			CFG_ID INT,
			PRMTR_NME NVARCHAR(100),  
			PRMTR_VALU_TXT NVARCHAR(MAX)  
		); 

	-- Insert USER LK_SYS_CFG to TEMP for iteration
    INSERT INTO @USER_SYS_CFG (CFG_ID, PRMTR_NME, PRMTR_VALU_TXT)
	SELECT CFG_ID, PRMTR_NME, PRMTR_VALU_TXT FROM LK_SYS_CFG WHERE PRMTR_VALU_TXT LIKE CONCAT('%',  @OLD_USER_ADID, '%') OR PRMTR_VALU_TXT LIKE  CONCAT('%',  @OLD_EMAIL, '%');

	-- Get TABLE COUNT
	SELECT @USER_SYS_CFG_COUNT = COUNT(*) FROM @USER_SYS_CFG;

	-- GET OLD EMAIL For Checking
	

	WHILE @INDEX < @USER_SYS_CFG_COUNT
	BEGIN
		-- INDEX STARTS AT 1 BECAUSE OF THE TEMP INDEX
		SELECT @INDEX = @INDEX + 1;

		DECLARE @CFG_ID INT;
		DECLARE	@PRMTR_NME NVARCHAR(100);
		DECLARE	@PRMTR_VALU_TXT NVARCHAR(MAX);

		SELECT 
			@CFG_ID = CFG_ID,
			@PRMTR_NME = PRMTR_NME,
			@PRMTR_VALU_TXT = PRMTR_VALU_TXT  
		FROM @USER_SYS_CFG WHERE ARRAYINDEX = @INDEX;

		 IF CHARINDEX(@OLD_EMAIL, @PRMTR_VALU_TXT) > 0 AND  CHARINDEX(',', @PRMTR_VALU_TXT) > 0
			BEGIN
				-- IF OLD_EMAIL is included in a config with multiple users. Append new EMAIL to @PRMTR_VALU_TXT
				UPDATE LK_SYS_CFG 
					SET PRMTR_VALU_TXT = CONCAT(@PRMTR_VALU_TXT, ', ' ,@NTID_EMAIL) 
					WHERE CFG_ID = @CFG_ID;
			END
		ELSE IF CHARINDEX(@OLD_USER_ADID, @PRMTR_VALU_TXT) > 0 AND  CHARINDEX(',', @PRMTR_VALU_TXT) > 0
			BEGIN
				-- IF OLD_USER_ADID is included in a config with multiple users. Append new ADID to @PRMTR_VALU_TXT
				UPDATE LK_SYS_CFG 
					SET PRMTR_VALU_TXT = CONCAT(@PRMTR_VALU_TXT, ', ' ,@USER_ADID) 
					WHERE CFG_ID = @CFG_ID;
			END
		ELSE
			BEGIN
			   -- INSERT new USER_ADID to LK_SYS_CFG
				INSERT INTO LK_SYS_CFG(PRMTR_NME, PRMTR_VALU_TXT, REC_STUS_ID, CREAT_DT)
				VALUES (@PRMTR_NME, @USER_ADID, 1, CURRENT_TIMESTAMP);
			END	
	END
	---------------------------------------------------------------------------------------------------
	---------------------------------------- LK_SYS_CFG - END -----------------------------------------
	---------------------------------------------------------------------------------------------------

	---------------------------------------------------------------------------------------------------
	---------------------------------------- ADID RELATED UPDATES - START -----------------------------
	---------------------------------------------------------------------------------------------------

	-- MDS_EVENT
	UPDATE MDS_EVENT SET MNS_PM_ID = @USER_ADID WHERE MNS_PM_ID = @OLD_USER_ADID
	-- UCaaS_EVENT
	UPDATE UCaaS_EVENT SET MNS_PM_ID = @USER_ADID WHERE MNS_PM_ID =  @OLD_USER_ADID
	-- REDSGN - PM_ASSIGNED
	UPDATE REDSGN SET PM_ASSIGNED = @USER_ADID WHERE PM_ASSIGNED =  @OLD_USER_ADID
	-- REDSGN - NTE_ASSIGNED
	UPDATE REDSGN SET NTE_ASSIGNED = @USER_ADID WHERE NTE_ASSIGNED =  @OLD_USER_ADID
	-- REDSGN - SDE_ASN_NME
	UPDATE REDSGN SET SDE_ASN_NME = @USER_ADID WHERE SDE_ASN_NME =  @OLD_USER_ADID
	-- REDSGN - NE_ASN_NME
	UPDATE REDSGN SET NE_ASN_NME = @USER_ADID WHERE NE_ASN_NME =  @OLD_USER_ADID

	---------------------------------------------------------------------------------------------------
	----------------------------------------  ADID RELATED UPDATES - END ------------------------------
	---------------------------------------------------------------------------------------------------

END
