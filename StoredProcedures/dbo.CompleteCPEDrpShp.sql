USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[CompleteCPEDrpShp]    Script Date: 01/29/2020 9:11:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 02/02/2016
-- Description:	Completes CPE Drop Ship on CCD date.
-- =========================================================
ALTER PROCEDURE [dbo].[CompleteCPEDrpShp]
	
AS

BEGIN
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @ORDR_ID  INT,
			@DEVICE_ID VARCHAR(25)
	
	DECLARE @IDS table (ORDR_ID INT, DEVICE_ID VARCHAR(25))
	
	INSERT INTO @IDS (ORDR_ID,DEVICE_ID) 
		 (SELECT DISTINCT fo.ORDR_ID, litm.DEVICE_ID FROM dbo.FSA_ORDR fo WITH (NOLOCK)
							INNER JOIN dbo.ORDR ord WITH (NOLOCK) 
								ON fo.ORDR_ID = ord.ORDR_ID AND ord.ORDR_CAT_ID = 6
							INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) 
								ON litm.ORDR_ID = fo.ORDR_ID 
							WHERE ISNULL(ord.CUST_CMMT_DT,fo.CUST_CMMT_DT) < GETDATE()
								  AND litm.CMPL_DT IS NULL
								  AND fo.ORDR_ID in (SELECT ORDR_ID FROM ACT_TASK WITH (NOLOCK)
															WHERE TASK_ID = 1000 AND STUS_ID = 0)
								  AND (litm.DROP_SHP = 'Y' 
										OR ord.PROD_ID IN ('UCCH','UCSV','MIPT','UCSM')
										OR ord.DMSTC_CD = 1
										OR fo.ORDR_SUB_TYPE_CD IN ('PRCV')
										--  OR ord.DLVY_CLLI in ('MTLDFLTBACP','DLLSTX53ACP','OVPKKSTDACP','RVSDMO09ACP')  removed dlp0278 - 5/20/2019
										)
								  AND ISNULL(litm.DEVICE_ID,'') != '' -- 9/26/2017 3:14 pm added dlp0278
			)
			
		
	
	DELETE @IDS where ISNULL(DEVICE_ID,'') = ''

	WHILE EXISTS (SELECT * FROM @IDS)
		BEGIN
			SELECT TOP 1 @ORDR_ID = ORDR_ID,
						 @DEVICE_ID = DEVICE_ID
					FROM @IDS  
				
		  IF @DEVICE_ID NOT IN (SELECT DEVICE FROM dbo.M5_ORDR_MSG WITH (NOLOCK)
								WHERE ORDR_ID = @ORDR_ID AND M5_MSG_ID = 1)
			 BEGIN

				DECLARE @Items TABLE
					( ITM_ID  int)

				DECLARE @Cnt INT = 0
						,@Ctr INT = 0
						,@itm INT

				INSERT INTO @Items
					SELECT  Distinct  FSA_CPE_LINE_ITEM_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
							WHERE ORDR_ID = @ORDR_ID AND ISNULL(PRCH_ORDR_NBR,'') <> '' 
		
				select * from @Items
	
				SET @Cnt = (SELECT COUNT(*) FROM @Items)
	
				IF (@Cnt > 0)
					BEGIN
						WHILE (@Ctr < @Cnt)
						BEGIN
								SELECT TOP 1 @itm = ITM_ID FROM @Items

								IF (@itm NOT IN (SELECT FSA_CPE_LINE_ITEM_ID from dbo.PS_RCPT_QUEUE with (nolock)))
									BEGIN

										INSERT INTO [dbo].[PS_RCPT_QUEUE]
										   ([PRCH_ORDR_NBR],[ORDR_ID],[REQSTN_NBR],[FSA_CPE_LINE_ITEM_ID]
										   ,[EQPT_RCVD_BY_ADID],[RCVD_QTY],[PS_RCVD_STUS],[PS_SENT_DT]
										   ,[CREAT_DT],[RCVD_DT],[BATCH_SEQ])

									   (SELECT DISTINCT PRCH_ORDR_NBR,ORDR_ID,PLSFT_RQSTN_NBR,FSA_CPE_LINE_ITEM_ID
											   ,'System',ORDR_QTY
											   ,NULL, NULL, GETDATE(), GETDATE(),NULL           
									   FROM dbo.FSA_ORDR_CPE_LINE_ITEM 
									      WHERE FSA_CPE_LINE_ITEM_ID = @itm)						
									END
								
								DELETE @Items where  ITM_ID = @itm 

								SET @Ctr = @Ctr + 1	
						 END 
					END
				
				EXEC dbo.insertOrdrCmplAll_V5U @ORDR_ID, @DEVICE_ID, 'V5U Note: Order Auto Completed in BPM'

			END
			DELETE @IDS WHERE ORDR_ID = @ORDR_ID AND @DEVICE_ID = DEVICE_ID
			
			IF @ORDR_ID NOT IN (SELECT ORDR_ID from @IDS)
				BEGIN
			
					EXEC dbo.CompleteActiveTask @ORDR_ID,1000,2,'System completed BAR-Pending Task',1,9
					
					DELETE dbo.BPM_ORDR_ADR WHERE ORDR_ID = @ORDR_ID
				
				END
			
		END
		
		
		-- Below added 11/30/17 to handle Purchase to Rental International order completions.  dlp0278
		DELETE @IDS
		
		INSERT INTO @IDS (ORDR_ID,DEVICE_ID) 
		 (SELECT Distinct fo.ORDR_ID, '' from FSA_ORDR fo WITH (NOLOCK)
			INNER JOIN ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
			WHERE  ord.DMSTC_CD = 1
					AND ORDR_SUB_TYPE_CD = 'PRCV'
					AND  ISNULL(ord.CUST_CMMT_DT,fo.CUST_CMMT_DT) < GETDATE()
					AND  fo.ORDR_ID in (SELECT ORDR_ID FROM ACT_TASK WITH (NOLOCK)
											WHERE TASK_ID = 1000 AND STUS_ID = 0)
			)
		
		WHILE EXISTS (SELECT * FROM @IDS)
		
		BEGIN
			SELECT TOP 1 @ORDR_ID = ORDR_ID,
					 @DEVICE_ID = DEVICE_ID
				FROM @IDS  
				
			
			DELETE @IDS WHERE ORDR_ID = @ORDR_ID 
			
			IF @ORDR_ID NOT IN (SELECT ORDR_ID from @IDS)
				BEGIN
					
					EXEC dbo.CompleteActiveTask @ORDR_ID,1000,2,'System completed BAR-Pending Task',1,9
					
					DELETE dbo.BPM_ORDR_ADR WHERE ORDR_ID = @ORDR_ID
				
				END
			
		END
		
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH

END
