USE [COWS]
GO
_CreateObject 'SP','web','getM5CANDEventData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  jrg7298            
-- Create date: 06/30/2012            
-- Description: Get the required data for an MDS/CAND Event when a M5 Order#/H6 value is entered.  
--@VASCEFlg => 0:Network,1:VAS,2:Network+VAS,3:Network+CE,4:VAS+CE,5:Network+CE+VAS 
-- km967761 - 06/30/2021 - Added H1_CUST_NME on #M5CANDTable; H1_CUST_NME on @ordr           
-- =============================================            
ALTER PROCEDURE [web].[getM5CANDEventData] --[web].[getM5CANDEventData] '', '928668186', 5
 @M5OrderNbr  Varchar(20) = '',
 @H6 		  Varchar(max) = '',
 @EType		  TINYINT    = 3,
 @VASCEFlg	  TINYINT = 0,
 @CEChngFlg	  BIT = 0
AS            
BEGIN            
SET NOCOUNT ON            
Begin Try            
            
SET @h6 = LTRIM(RTRIM(@h6))

IF OBJECT_ID(N'tempdb..#M5CANDTable', N'U') IS NOT NULL         
	DROP TABLE #M5CANDTable 

IF OBJECT_ID(N'tempdb..#M5COMPTable', N'U') IS NOT NULL         
	DROP TABLE #M5COMPTable 

IF OBJECT_ID(N'tempdb..#M5ADRTable', N'U') IS NOT NULL         
DROP TABLE #M5ADRTable
	
CREATE TABLE #M5ADRTable
(M5_ORDR_NBR VARCHAR(50) NULL,
CIS_HIER_LVL_CD VARCHAR(2) NULL,
SITE_ID VARCHAR(20) NULL,
LINE_1	VARCHAR(255) NULL,
LINE_2	VARCHAR(255) NULL,
LINE_3	VARCHAR(255) NULL,
CTY_NME	VARCHAR(255) NULL,
ST_PRVN_CD	VARCHAR(20) NULL,
PSTL_CD	VARCHAR(15) NULL,
CTRY_CD	VARCHAR(50) NULL,
BLDG_ID	VARCHAR(255) NULL,
ROOM_ID	VARCHAR(255) NULL,
FLOOR_ID	VARCHAR(255) NULL)

CREATE TABLE #M5COMPTable
(ORDR_ID INT NOT NULL
 ,CKT_ID VARCHAR(9) NULL
 ,DESIGN_DOC_NBR VARCHAR(20) NULL)
 
CREATE TABLE #M5CANDTable
 (M5_ORDR_ID   VARCHAR(20)   NOT NULL
 ,ORDR_ID INT NOT NULL 
 ,CHARS_ID VARCHAR(9) NULL
 ,SITE_ID VARCHAR(50) NULL
 ,CCD VARCHAR(20) NULL
 ,H1 VARCHAR(9) NULL
 ,H1_CUST_NME VARCHAR(255) NULL
 ,H6 VARCHAR(9) NULL
 ,CUST_NME	VARCHAR(255) NULL
 ,CUST_CNTCT_NME VARCHAR(511) NULL
 ,PHN_NBR	VARCHAR(30) NULL
 ,EMAIL_ADR VARCHAR(255) NULL
 ,DESIGN_DOC_NBR VARCHAR(20) NULL
 ,CKT_ID VARCHAR(9) NULL
 ,CSG_LVL VARCHAR(5) NULL
 ,CUST_ACCT_ID INT NULL
 ,[ISD_CD] [varchar](3) NULL
 ,[PHN_EXT_NBR] [varchar](10) NULL
 ,[ROLE_CD] VARCHAR(10) NULL)     

declare @ordr nvarchar(max) = ''
declare @adrinfo nvarchar(max) = ''
            
IF @M5OrderNbr != ''          
 BEGIN            

 --MPLS WPaaS, DIA WPaaS, WPaaS Product, Standalone MNS, Managed WiFi Product, Standalone MSS
 -- NOT IN (Completed, Cancelled, Rejected)
 -- NOT IN (Full Cancel, Partial Cancel, Full Disconnect, Partial Disconnect)
set @ordr = 'INSERT INTO #M5CANDTable (M5_ORDR_ID, ORDR_ID, CHARS_ID, SITE_ID, CCD, H1, H6, CUST_NME, CUST_CNTCT_NME, PHN_NBR, EMAIL_ADR,
								CSG_LVL, CUST_ACCT_ID,ISD_CD,PHN_EXT_NBR,ROLE_CD) Select DISTINCT
							ORDER_NBR, ORDR_ID, CHARS_CUST_ID, SITE_ID, convert(date,CUST_CMMT_DT), H1, H6, CUST_NME, CUST_CNTCT_NME, PHN_NBR, EMAIL_ADR,
								CSG_LVL, CUST_ACCT_ID, ISD_CD, PHN_EXT_NBR, ROLE_CD  from openquery(M5,''select DISTINCT vbo.ORDR_SUBTYPE_CD, vbo.PROD_ID, vbo.ORDR_STUS_CD, vbo.ORDER_NBR, vbo.ORDR_ID,
								vbca.CHARS_CUST_ID, vbca.SITE_ID, vbo.CUST_CMMT_DT, vbca.H1, vbca.H6, vbca.CUST_NME, vbcc.CUST_NME as CUST_CNTCT_NME,
								CASE	WHEN vbcc.DOM_INTL_IND_CD = ''''D'''' THEN vbcc.DOM_PHN_NBR
															WHEN vbcc.DOM_INTL_IND_CD = ''''I'''' THEN vbcc.INTL_PHN_NBR   
													END AS  PHN_NBR,
								vbcc.EMAIL_ADR, 
								vbca.CSG_LVL, vbca.CUST_ACCT_ID,vbcc.INTL_CNTRY_DIAL_CD AS ISD_CD,			
									vbcc.PHN_EXT AS PHN_EXT_NBR, vbcc.ROLE_CD
							from MACH5.V_V5U_ORDR vbo,
							MACH5.V_V5U_CUST_CONTACT vbcc,
							MACH5.V_V5U_CUST_ACCT vbca
							where vbo.ORDR_SUBTYPE_CD NOT IN (''''FD'''', ''''PD'''', ''''FC'''', ''''PC'''') 
							AND vbca.CIS_HIER_LVL_CD IN (''''H5'''', ''''H6'''') 
							and ((vbca.TYPE_CD = DECODE(vbca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
								or (vbca.TYPE_CD = DECODE(vbca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
							AND vbca.CUST_ACCT_ID = vbo.PRNT_ACCT_ID 
							AND vbo.ORDR_STUS_CD NOT IN (''''CN'''', ''''RJ'''')
							AND vbo.PRNT_ACCT_ID = vbca.CUST_ACCT_ID
							AND vbca.CUST_ACCT_ID = vbcc.CUST_ACCT_ID
							AND vbcc.ROLE_CD = ''''CONT''''
							AND vbo.ORDER_NBR IN (''''' + UPPER(@M5OrderNbr) + ''''')
							AND ROWNUM <=1 '')'

exec sp_executesql @ordr

declare @ordrids varchar(max)
select @ordrids = COALESCE(@ordrids + ''''',''''', '''''') + CONVERT(VARCHAR,ORDR_ID)
FROM #M5CANDTable
set @ordrids =  substring(@ordrids, 2, len(@ordrids)-1)

declare @comp nvarchar(max) = 'INSERT INTO #M5COMPTable (ORDR_ID, CKT_ID, DESIGN_DOC_NBR)
							   SELECT ORDR_ID, CKT_ID, DESIGN_DOC_NBR
								FROM OPENQUERY(M5, ''select DISTINCT ORDR_ID, CKT_ID, DESIGN_DOC_NBR
								FROM MACH5.V_V5U_ORDR_CMPNT
								where ORDR_ID IN (''' + @ordrids + ''''') '')'

exec sp_executesql @comp							
UPDATE mct
SET CKT_ID = mco.CKT_ID
FROM #M5CANDTable mct INNER JOIN
#M5COMPTable mco ON mco.ORDR_ID = mct.ORDR_ID
WHERE ISNULL(mco.CKT_ID,'') <> ''

UPDATE mct
SET DESIGN_DOC_NBR = mco.DESIGN_DOC_NBR
FROM #M5CANDTable mct INNER JOIN
#M5COMPTable mco ON mco.ORDR_ID = mct.ORDR_ID
WHERE ISNULL(mco.DESIGN_DOC_NBR,'') <> ''	


--SELECT * FROM #M5CANDTable
IF (EXISTS (SELECT 'x' FROM #M5CANDTable))
BEGIN
	
		declare @ordrnbrs varchar(max)
		select @ordrnbrs = ISNULL(@ordrnbrs,'') + ISNULL(M5_ORDR_ID,'') + ''''', '''''
		from #M5CANDTable
		if(len(@ordrnbrs)>2)
		set  @ordrnbrs = substring(@ordrnbrs, 1, len(@ordrnbrs)-6)

		declare @acctids varchar(max)
		select @acctids = ISNULL(@acctids,'') + ISNULL(CONVERT(VARCHAR,CUST_ACCT_ID),'') + ', '
		from #M5CANDTable
		if(len(@acctids)>2)
		set  @acctids = substring(@acctids, 1, len(@acctids)-1)

		set @adrinfo = 'INSERT INTO #M5ADRTable (M5_ORDR_NBR,
			CIS_HIER_LVL_CD,
			SITE_ID,
			LINE_1,
			LINE_2,
			LINE_3,
			CTY_NME,
			ST_PRVN_CD,
			PSTL_CD,
			CTRY_CD,
			BLDG_ID,
			ROOM_ID,
			FLOOR_ID) Select ORDER_NBR,
				CIS_HIER_LVL_CD,
				SITE_ID,
				LINE_1,
				LINE_2,
				LINE_3,
				CTY_NME,
				ST_PRVN_CD,
				PSTL_CD,
				CTRY_CD,
				BLDG_ID,
				ROOM_ID,
				FLOOR_ID  from openquery(M5,''Select DISTINCT vvo.ORDER_NBR,
								vvca.CIS_HIER_LVL_CD,
								vvca.SITE_ID,
								vvca.LINE_1,
								vvca.LINE_2,
								vvca.LINE_3,
								vvca.CTY_NME,
								vvca.ST_PRVN_CD,
								vvca.PSTL_CD,
								vvca.CTRY_CD,
								vvca.BLDG_ID,
								vvca.ROOM_ID,
								vvca.FLOOR_ID
											From MACH5.V_V5U_ORDR vvo
							  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
											WHERE vvca.CUST_ACCT_ID IN (' + @acctids + ')
							  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
							  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
								or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
							  and vvo.ORDER_NBR IN (''''' + @ordrnbrs + ''''') '')'

							  --print @adrinfo
							  exec sp_executesql @adrinfo
	
		UPDATE ma
		SET CTRY_CD = CTRY_NME
		FROM #M5ADRTable ma INNER JOIN
			 dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = ma.CTRY_CD

END							
        
END
ELSE IF ((@H6 <> '') AND (@EType <> 5))
BEGIN

--MPLS WPaaS, DIA WPaaS, WPaaS Product, Standalone MNS, Managed WiFi Product, Standalone MSS
 -- NOT IN (Completed, Cancelled, Rejected)
 -- NOT IN (Full Cancel, Partial Cancel, Full Disconnect, Partial Disconnect)
set @ordr = 'INSERT INTO #M5CANDTable (M5_ORDR_ID, ORDR_ID, CHARS_ID, SITE_ID, H1, H6, CUST_NME, CUST_CNTCT_NME, PHN_NBR, EMAIL_ADR,
								CSG_LVL, CUST_ACCT_ID,ISD_CD,PHN_EXT_NBR,ROLE_CD) Select DISTINCT
							'''' AS ORDER_NBR, -1 AS ORDR_ID, CHARS_CUST_ID, SITE_ID, H1, H6, CUST_NME, CUST_CNTCT_NME, PHN_NBR, EMAIL_ADR,
								CSG_LVL, CUST_ACCT_ID, ISD_CD, PHN_EXT_NBR, ROLE_CD  from openquery(M5,''select DISTINCT 
								vbca.CHARS_CUST_ID, vbca.SITE_ID, vbca.H1, vbca.H6, vbca.CUST_NME, vbcc.CUST_NME as CUST_CNTCT_NME,
								CASE	WHEN vbcc.DOM_INTL_IND_CD = ''''D'''' THEN vbcc.DOM_PHN_NBR
															WHEN vbcc.DOM_INTL_IND_CD = ''''I'''' THEN vbcc.INTL_PHN_NBR   
													END AS  PHN_NBR,
								vbcc.EMAIL_ADR, 
								vbca.CSG_LVL, vbca.CUST_ACCT_ID,vbcc.INTL_CNTRY_DIAL_CD AS ISD_CD,			
									vbcc.PHN_EXT AS PHN_EXT_NBR, vbcc.ROLE_CD
							from
							MACH5.V_V5U_CUST_CONTACT vbcc,
							MACH5.V_V5U_CUST_ACCT vbca
							where vbca.CIS_HIER_LVL_CD IN (''''H5'''', ''''H6'''') 
							and ((vbca.TYPE_CD = DECODE(vbca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
								or (vbca.TYPE_CD = DECODE(vbca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
							AND vbca.CUST_ACCT_ID = vbcc.CUST_ACCT_ID
							AND vbcc.ROLE_CD = ''''CONT''''
							AND vbca.CIS_CUST_ID IN (''''' + @h6 + ''''')
							AND ROWNUM <=1 '')'

exec sp_executesql @ordr

set @adrinfo = 'INSERT INTO #M5ADRTable (M5_ORDR_NBR,
			CIS_HIER_LVL_CD,
			SITE_ID,
			LINE_1,
			LINE_2,
			LINE_3,
			CTY_NME,
			ST_PRVN_CD,
			PSTL_CD,
			CTRY_CD,
			BLDG_ID,
			ROOM_ID,
			FLOOR_ID) Select '''' AS ORDER_NBR,
				CIS_HIER_LVL_CD,
				SITE_ID,
				LINE_1,
				LINE_2,
				LINE_3,
				CTY_NME,
				ST_PRVN_CD,
				PSTL_CD,
				CTRY_CD,
				BLDG_ID,
				ROOM_ID,
				FLOOR_ID  from openquery(M5,''Select DISTINCT 
								vvca.CIS_HIER_LVL_CD,
								vvca.SITE_ID,
								vvca.LINE_1,
								vvca.LINE_2,
								vvca.LINE_3,
								vvca.CTY_NME,
								vvca.ST_PRVN_CD,
								vvca.PSTL_CD,
								vvca.CTRY_CD,
								vvca.BLDG_ID,
								vvca.ROOM_ID,
								vvca.FLOOR_ID
											From MACH5.V_V5U_CUST_ACCT vvca
											WHERE vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
											and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
												or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
							  and vvca.CIS_CUST_ID = ''''' + @h6 + ''''' '')'

							  --print @adrinfo
							  exec sp_executesql @adrinfo
	
		UPDATE ma
		SET CTRY_CD = CTRY_NME
		FROM #M5ADRTable ma INNER JOIN
			 dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = ma.CTRY_CD
			 
END

IF (@EType <> 5)
BEGIN
SELECT DISTINCT TOP 1 c.M5_ORDR_ID as FTN, c.CHARS_ID as CHARSID, c.SITE_ID as SITEID, c.CCD, c.H1, c.H6, c.CUST_NME as CustomerName, 
c.CUST_CNTCT_NME as CustomerContactName, c.ISD_CD, c.PHN_NBR as CustomerContactPhone, c.PHN_EXT_NBR, c.EMAIL_ADR as CustomerEmail, '0' as IsEscalation, 
'0' as CARRIER_WHOLESALE_TYPE, '' as IP_VERSION, c.DESIGN_DOC_NBR as DesignDocumentApprovalNumber, 'N' as MDSManaged, 
ISNULL(lcl.CSG_LVL_ID,0) AS CSG_LVL_ID 
FROM #M5CANDTable c LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=CSG_LVL

SELECT DISTINCT TOP 1 M5_ORDR_ID as FTN, CKT_ID as Circuit_ID FROM #M5CANDTable

select DISTINCT TOP 1 SITE_ID as SiteID, ISNULL(LINE_1,'') + ISNULL(LINE_2,'') + ISNULL(LINE_3,'') as StAddr, BLDG_ID + FLOOR_ID + ROOM_ID as FlrBldg, 
ST_PRVN_CD as ProvState, CTY_NME as City, PSTL_CD as ZipCD, CTRY_CD as Ctry  from #M5ADRTable

declare @grpn nvarchar(max) = 'SELECT TOP 1 ISNULL(GRP_NME,'''') AS GRP_NME FROM OPENQUERY(nrmbpm, ''SELECT DISTINCT a1.GRP_NME
 FROM BPMF_OWNER.V_BPMF_COWS_ACTV_EVENT a1 WHERE a1.H6_H5_ID = '''''+ (SELECT TOP 1 H6 fROM #M5CANDTable)  +''''' AND a1.GRP_NME = ''''SPRINT''''
 AND NOT EXISTS (SELECT * FROM BPMF_OWNER.V_BPMF_COWS_ACTV_EVENT a2 WHERE a2.DD_NBR=a1.DD_NBR AND a2.NUA_ADR=a1.NUA_ADR AND a1.FTN_NBR=a2.FTN_NBR AND a1.H6_H5_ID=a2.H6_H5_ID AND a2.GRP_NME IS NULL)
 UNION
 SELECT DISTINCT a1.GRP_NME
 FROM BPMF_OWNER.V_BPMF_CE_COWS_ACTV_EVENT a1 WHERE a1.H6_ID = '''''+ (SELECT TOP 1 H6 fROM #M5CANDTable)  +''''' AND a1.GRP_NME = ''''SPRINT''''
 AND NOT EXISTS (SELECT * FROM BPMF_OWNER.V_BPMF_CE_COWS_ACTV_EVENT a2 WHERE a2.DD_NBR=a1.DD_NBR AND a2.NUA_ADR=a1.NUA_ADR AND a1.FTN_NBR=a2.FTN_NBR AND a1.H6_ID=a2.H6_ID AND a2.GRP_NME IS NULL)
 UNION
 SELECT DISTINCT a1.GRP_NME
 FROM BPMF_OWNER.V_BPMF_VAS_COWS_ACTV_EVENT a1 WHERE a1.H6_H5_ID = '''''+ (SELECT TOP 1 H6 fROM #M5CANDTable)  +''''' AND a1.GRP_NME = ''''SPRINT''''
 AND NOT EXISTS (SELECT * FROM BPMF_OWNER.V_BPMF_VAS_COWS_ACTV_EVENT a2 WHERE a2.DD_NBR=a1.DD_NBR AND a2.NUA_ADR=a1.NUA_ADR AND a1.FTN_NBR=a2.FTN_NBR AND a1.H6_H5_ID=a2.H6_H5_ID AND a2.GRP_NME IS NULL) '')'
exec sp_executesql @grpn
END

IF ((@H6 <> '') AND (@EType = 5))
BEGIN

--SELECT @H6, '', @VASCEFlg, @CEChngFlg, 0
EXEC [web].[getDesignDocData] @H6, '', @VASCEFlg, @CEChngFlg, 0

END
               
END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END
GO
