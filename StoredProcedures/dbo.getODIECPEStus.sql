USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getODIECPEStus]    Script Date: 07/06/2017 15:18:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 7/29/2015
	-- Description:	Retrieves ODIE CPE status Data.
	---- =============================================
	
	--  [dbo].[getODIECPEStus] 68322
	
	ALTER PROCEDURE [dbo].[getODIECPEStus] 
		@REQ_ID int 

	AS
	BEGIN

	BEGIN TRY
	
	DECLARE @ORDR_ID int  
	DECLARE @DEVICE_ID varchar (25)
	
	SELECT @ORDR_ID = ORDR_ID, @DEVICE_ID = DEV_FLTR FROM dbo.ODIE_REQ WHERE REQ_ID = @REQ_ID
	
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	-- Order Info.
	SELECT  DISTINCT
			ISNULL(fsa.FTN,'')                              AS [OrderID],
			CASE ord.ORDR_STUS_ID
				WHEN     2         THEN 'Complete'
				WHEN     5         THEN 'Complete'
				WHEN     4         THEN 'Cancel'
				ELSE ''
			END                                             AS [OrderStatus], --Complete, Cancel
			GETDATE()                                       AS [StatusDate],
			@DEVICE_ID										AS [DeviceID],
			foc.CUST_ID                                     AS [H6_CustomerID]

	FROM dbo.FSA_ORDR fsa WITH (NOLOCK)
	INNER JOIN dbo.ORDR	ord WITH (NOLOCK) ON ord.ORDR_ID = fsa.ORDR_ID
	INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cpe WITH (NOLOCK) ON cpe.ORDR_ID = fsa.ORDR_ID
	INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.ORDR_ID = fsa.ORDR_ID 
								AND CIS_LVL_TYPE = 'H6'
	WHERE fsa.ORDR_ID = @ORDR_ID
	
	SELECT DISTINCT ORDR_CMPNT_ID AS [ComponentID] 
			FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK)
				INNER JOIN FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = litm.ORDR_ID
				INNER JOIN dbo.ORDR	ord WITH (NOLOCK) ON ord.ORDR_ID = litm.ORDR_ID
				WHERE litm.ORDR_ID = @ORDR_ID
				AND litm.EQPT_TYPE_ID = 'MJH'
				AND ((ord.ORDR_STUS_ID in (2,5) AND litm.ITM_STUS = 401)
										OR
					(ord.ORDR_STUS_ID in (4) AND litm.ITM_STUS = 402))
			
		
	--Notes
	
	SELECT 
		ISNULL(NTE_TXT,'')                                              AS [NoteText]
	
	FROM dbo.ORDR_NTE WITH (NOLOCK)
	WHERE ORDR_ID = @ORDR_ID
		AND NTE_TYPE_ID = 6 AND ISNULL(NTE_TXT,'') != ''
	
	RETURN
			
	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH

	END
	




