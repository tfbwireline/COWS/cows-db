USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertBpmAdr_V5U]    Script Date: 05/10/2019 8:54:43 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	---- =============================================
	---- Author:		David Phillips
	---- Create date: 8/28/2017
	---- Description:	Appian CPE Customer Address Info.
	------ =============================================
	

	ALTER PROCEDURE [dbo].[insertBpmAdr_V5U] 
	 @ORDR_ID int

	AS
	BEGIN


	
	----Truncate table [dbo].[BPM_ORDR_ADR]
	
	IF ((SELECT ISNULL(CSG_LVL_ID, 0) FROM dbo.ORDR WITH (NOLOCK)
			WHERE ORDR_ID = @ORDR_ID) = 0)
	
		BEGIN
	
			INSERT INTO [dbo].[BPM_ORDR_ADR]
				   ([ORDR_ID],[STREET_ADR_1],[STREET_ADR_2],[STREET_ADR_3],[BLDG_NME],[FLR_ID],[RM_NBR]
				   ,[CTY_NME],[STT_CD],[ZIP_PSTL_CD],[CTRY_CD],[PRVN_NME]
				   ,[SHIP_STREET_ADR_1],[SHIP_STREET_ADR_2],[SHIP_STREET_ADR_3],[SHIP_BLDG_NME],[SHIP_FLR_ID]
				   ,[SHIP_RM_NBR],[SHIP_CTY_NME],[SHIP_STT_CD],[SHIP_ZIP_PSTL_CD],[SHIP_CTRY_CD],[SHIP_PRVN_NME]
				   ,[CUST_NME], [CUST_ID], [DEVICE_ID], [SITE_ID])
		        	
			
			SELECT	DISTINCT fo.ORDR_ID
						
						,substring(ISNULL(oa.STREET_ADR_1,''),1,30)
																	AS STREET_ADR_1
						,substring(ISNULL(oa.STREET_ADR_2,''),1,30)          
																AS STREET_ADR_2
						,substring(ISNULL(oa.STREET_ADR_3,''),1,30)          
																AS STREET_ADR_3
						 ,substring(ISNULL(oa.BLDG_NME,''),1,15)          
																AS BLDG_NME
						 ,substring(ISNULL(oa.FLR_ID,''),1,10)          
																AS FLR_ID
						 ,substring(ISNULL(oa.RM_NBR,'') ,1,10)         
																AS RM_NBR
						 ,substring(ISNULL(oa.CTY_NME,''),1,20)          
																AS  CTY_NME
						 ,substring(ISNULL(oa.STT_CD,''),1,2)          
																AS  STT_CD
						 ,substring(ISNULL(oa.ZIP_PSTL_CD,''),1,5)          
																AS  ZIP_PSTL_CD
						 ,ictry.L2P_CTRY_CD                     AS  CTRY_CD
						 ,substring(ISNULL(oa.PRVN_NME,''),1,30)          
																AS  PRVN_NME
						-- Shipping address below  --- 	
				 						 ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = '' 
												 THEN substring(ISNULL(oa.STREET_ADR_1,''),1,30)          
							ELSE substring(ISNULL(oas.STREET_ADR_1,''),1,30)
						  END									AS SHIP_STREET_ADR_1
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
												THEN substring(ISNULL(oa.STREET_ADR_2,''),1,30)          
							ELSE substring(ISNULL(oas.STREET_ADR_2,''),1,30)
						   END									AS SHIP_STREET_ADR_2
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
											 THEN substring(ISNULL(oa.STREET_ADR_3,''),1,30)          
							ELSE substring(ISNULL(oas.STREET_ADR_3,''),1,30)
						   END									AS SHIP_STREET_ADR_3
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
											THEN substring(ISNULL(oa.BLDG_NME,''),1,30)          
							ELSE substring(ISNULL(oas.BLDG_NME,''),1,30)
						   END									 AS SHIP_BLDG_NME
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
											THEN substring(ISNULL(oa.FLR_ID,''),1,30)          
							ELSE substring(ISNULL(oas.FLR_ID,''),1,30)
						   END									  AS SHIP_FLR_ID
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
											THEN substring(ISNULL(oa.RM_NBR,''),1,30)          
							ELSE substring(ISNULL(oas.RM_NBR,''),1,30)
						   END									  AS SHIP_RM_NBR
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
										 THEN 	substring(ISNULL(oa.CTY_NME,''),1,30)          
							ELSE substring(ISNULL(oas.CTY_NME,''),1,30)
						   END									  AS SHIP_CTY_NME
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
										 THEN substring(ISNULL(oa.STT_CD,''),1,30)          
							ELSE substring(ISNULL(oas.STT_CD,''),1,30)
						   END									  AS SHIP_STT_CD
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
										 THEN substring(ISNULL(oa.ZIP_PSTL_CD,''),1,30)          
							ELSE substring(ISNULL(oas.ZIP_PSTL_CD,''),1,30)
						   END									  AS SHIP_ZIP_PSTL_CD
						   ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
								   THEN substring(ISNULL(ictry.L2P_CTRY_CD,''),1,30)	
							 ELSE substring(ISNULL(sctry.L2P_CTRY_CD,''),1,30)
							 END
																AS  SHIP_CTRY_CD
						 ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(oas.STREET_ADR_1,'') = ''
									 THEN substring(ISNULL(oa.PRVN_NME,''),1,30)          
							ELSE substring(ISNULL(oas.PRVN_NME,''),1,30)
						   END									AS  SHIP_PRVN_NME      
						   
						 ,substring(ISNULL(foc.CUST_NME,'') ,1,30)         
																   AS CUST_NME 
																   
						,ISNULL(foc.CUST_ID,'')				   AS CUST_ID 
						,substring(ISNULL(cli.DEVICE_ID,'') ,1,30)                 AS DEVICE_ID 
						,substring(ISNULL(oc.SITE_ID,'') ,1,30)                     AS SITE_ID     
						 
			FROM         dbo.FSA_ORDR fo			WITH (NOLOCK)
			INNER JOIN   dbo.ORDR ord				WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
			INNER JOIN dbo.ACT_TASK act				WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID
			INNER JOIN   dbo.LK_TASK lt				WITH (NOLOCK) ON act.TASK_ID = lt.TASK_ID
			LEFT OUTER JOIN   dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.ORDR_ID = fo.ORDR_ID
														AND foc.CIS_LVL_TYPE IN ('H5','H6')
			LEFT OUTER JOIN	dbo.ORDR_ADR oa			WITH (NOLOCK)	ON	oa.ORDR_ID = fo.ORDR_ID  
														AND	oa.CIS_LVL_TYPE in ('H6','H5')
														AND oa.ADR_TYPE_ID = 18
			LEFT OUTER JOIN	dbo.ORDR_ADR oas			WITH (NOLOCK)	ON	oas.ORDR_ID = fo.ORDR_ID  
														AND	oas.CIS_LVL_TYPE in ('H6','H5')
														AND oas.ADR_TYPE_ID = 19
			LEFT OUTER JOIN dbo.USER_WFM_ASMT uwa	WITH (NOLOCK) ON uwa.ORDR_ID = fo.ORDR_ID
														AND ((uwa.GRP_ID in (13,1,14)) OR (uwa.USR_PRF_ID in (98,126)))
			LEFT OUTER JOIN dbo.LK_USER lu			WITH (NOLOCK) ON lu.USER_ID = uwa.ASN_USER_ID
			LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK)
								ON cli.ORDR_ID = fo.ORDR_ID 
			LEFT OUTER JOIN dbo.FSA_ORDR_CUST oc WITH (NOLOCK)
								ON oc.ORDR_ID = fo.ORDR_ID AND oc.CIS_LVL_TYPE in ('H5','H6')
			LEFT OUTER JOIN dbo.LK_CTRY ictry WITH (NOLOCK) ON ictry.CTRY_CD = oa.CTRY_CD
			LEFT OUTER JOIN dbo.LK_CTRY sctry WITH (NOLOCK) ON sctry.CTRY_CD = oas.CTRY_CD
			--LEFT OUTER JOIN dbo.V_V5U_SSTAT_INFO clli WITH (NOLOCK) ON ord.CPE_CLLI = clli.CLLI_Code  removed 3/12/17 8:20 a.m.
			WHERE  fo.PROD_TYPE_CD = 'CP'
				AND ord.ORDR_CAT_ID = 6
				AND NOT EXISTS (SELECT 'X' FROM dbo.FSA_ORDR_CPE_LINE_ITEM focli2 WITH (NOLOCK)
											WHERE focli2.DROP_SHP = 'Y' AND focli2.ORDR_ID = fo.ORDR_ID)
				--AND ord.SCURD_CD = 0
				AND ((act.TASK_ID in (600,601,602,604,1000) and act.STUS_ID in (156,0) AND DMSTC_CD = 0)
						or
					(act.TASK_ID in (600,601) and act.STUS_ID in (0) AND DMSTC_CD = 1))
				AND @ORDR_ID = fo.ORDR_ID
				AND @ORDR_ID not in (SELECT ORDR_ID from dbo.BPM_ORDR_ADR with (NOLOCK))
		END	
		
	ELSE
	
		BEGIN
		
			OPEN SYMMETRIC KEY FS@K3y 
			DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
			
			--Truncate table [dbo].[BPM_ORDR_ADR]
			
			INSERT INTO [dbo].[BPM_ORDR_ADR]
				   ([ORDR_ID],[STREET_ADR_1],[STREET_ADR_2],[STREET_ADR_3],[BLDG_NME],[FLR_ID],[RM_NBR]
				   ,[CTY_NME],[STT_CD],[ZIP_PSTL_CD],[CTRY_CD],[PRVN_NME]
				   ,[SHIP_STREET_ADR_1],[SHIP_STREET_ADR_2],[SHIP_STREET_ADR_3],[SHIP_BLDG_NME],[SHIP_FLR_ID]
				   ,[SHIP_RM_NBR],[SHIP_CTY_NME],[SHIP_STT_CD],[SHIP_ZIP_PSTL_CD],[SHIP_CTRY_CD],[SHIP_PRVN_NME]
				   ,[CUST_NME], [CUST_ID], [DEVICE_ID], [SITE_ID])
		        	
			
			SELECT	DISTINCT fo.ORDR_ID
						
						,ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.STREET_ADR_1)),'')
																	AS STREET_ADR_1
						,ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.STREET_ADR_2)),'')          
																AS STREET_ADR_2
						,ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.STREET_ADR_3)),'')          
																AS STREET_ADR_3
						 ,ISNULL(CONVERT(VARCHAR(15), DecryptByKey(csd.BLDG_NME)),'')          
																AS BLDG_NME
						 ,ISNULL(CONVERT(VARCHAR(10), DecryptByKey(csd.FLR_ID)),'')          
																AS FLR_ID
						 ,ISNULL(CONVERT(VARCHAR(10), DecryptByKey(csd.RM_NBR)),'')          
																AS RM_NBR
						 ,ISNULL(CONVERT(VARCHAR(20), DecryptByKey(csd.CTY_NME)),'')          
																AS  CTY_NME
						 ,ISNULL(CONVERT(VARCHAR(2), DecryptByKey(csd.STT_CD)),'')          
																AS  STT_CD
						 ,ISNULL(CONVERT(VARCHAR(5), DecryptByKey(csd.ZIP_PSTL_CD)),'')          
																AS  ZIP_PSTL_CD
						 ,ictry.L2P_CTRY_CD                     AS  CTRY_CD
						 ,ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.STT_PRVN_NME)),'')          
																AS  PRVN_NME
						-- Shipping address below  --- 	
				 						 ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = '' 
												 THEN ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.STREET_ADR_1)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'')
						  END									AS SHIP_STREET_ADR_1
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
												THEN ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.STREET_ADR_2)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_2)),'')
						   END									AS SHIP_STREET_ADR_2
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
											 THEN ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.STREET_ADR_3)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_3)),'')
						   END									AS SHIP_STREET_ADR_3
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
											THEN ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.BLDG_NME)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.BLDG_NME)),'')
						   END									 AS SHIP_BLDG_NME
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
											THEN ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.FLR_ID)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(10), DecryptByKey(csds.FLR_ID)),'')
						   END									  AS SHIP_FLR_ID
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
											THEN ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.RM_NBR)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(10), DecryptByKey(csds.RM_NBR)),'')
						   END									  AS SHIP_RM_NBR
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
										 THEN 	ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.CTY_NME)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(10), DecryptByKey(csds.CTY_NME)),'')
						   END									  AS SHIP_CTY_NME
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
										 THEN ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.STT_CD)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(10), DecryptByKey(csds.STT_CD)),'')
						   END									  AS SHIP_STT_CD
						  ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
										 THEN ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.ZIP_PSTL_CD)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(10), DecryptByKey(csds.ZIP_PSTL_CD)),'')
						   END									  AS SHIP_ZIP_PSTL_CD
						   ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
								   THEN ISNULL(ictry.L2P_CTRY_CD,'')	
							 ELSE ISNULL(sctry.L2P_CTRY_CD,'')
							 END
																AS  SHIP_CTRY_CD
						 ,CASE
							WHEN ord.DMSTC_CD = 1 AND ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csds.STREET_ADR_1)),'') = ''
									 THEN ISNULL(CONVERT(VARCHAR(30), DecryptByKey(csd.STT_PRVN_NME)),'')          
							ELSE ISNULL(CONVERT(VARCHAR(10), DecryptByKey(oas.PRVN_NME)),'')
						   END									AS  SHIP_PRVN_NME      
						   
						 ,ISNULL(CONVERT(VARCHAR(50), DecryptByKey(csdfc.CUST_NME)),'')          
																   AS CUST_NME 
																   
						,ISNULL(foc.CUST_ID,'')					   AS CUST_ID 
						,ISNULL(cli.DEVICE_ID,'')                  AS DEVICE_ID 
						,ISNULL(oc.SITE_ID,'')                      AS SITE_ID     
						 
			FROM         dbo.FSA_ORDR fo			WITH (NOLOCK)
			INNER JOIN   dbo.ORDR ord				WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
			INNER JOIN dbo.ACT_TASK act				WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID
			INNER JOIN   dbo.LK_TASK lt				WITH (NOLOCK) ON act.TASK_ID = lt.TASK_ID
			LEFT OUTER JOIN   dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.ORDR_ID = fo.ORDR_ID
														AND foc.CIS_LVL_TYPE IN ('H5','H6')
			LEFT OUTER JOIN	dbo.ORDR_ADR oa			WITH (NOLOCK)	ON	oa.ORDR_ID = fo.ORDR_ID  
														AND	oa.CIS_LVL_TYPE in ('H6','H5')
														AND oa.ADR_TYPE_ID = 18
			LEFT OUTER JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK)
							ON csd.SCRD_OBJ_ID = oa.ORDR_ADR_ID and csd.SCRD_OBJ_TYPE_ID = 14
			LEFT OUTER JOIN	dbo.ORDR_ADR oas			WITH (NOLOCK)	ON	oas.ORDR_ID = fo.ORDR_ID  
														AND	oas.CIS_LVL_TYPE in ('H6','H5')
														AND oas.ADR_TYPE_ID = 19
			LEFT OUTER JOIN dbo.CUST_SCRD_DATA csds WITH (NOLOCK)
							ON csds.SCRD_OBJ_ID = oas.ORDR_ADR_ID and csds.SCRD_OBJ_TYPE_ID = 14
							
			LEFT OUTER JOIN dbo.USER_WFM_ASMT uwa	WITH (NOLOCK) ON uwa.ORDR_ID = fo.ORDR_ID
														AND ((uwa.GRP_ID in (13,1,14)) OR (uwa.USR_PRF_ID in (98,126)))
			LEFT OUTER JOIN dbo.LK_USER lu			WITH (NOLOCK) ON lu.USER_ID = uwa.ASN_USER_ID
			LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK)
								ON cli.ORDR_ID = fo.ORDR_ID 
			LEFT OUTER JOIN dbo.FSA_ORDR_CUST oc WITH (NOLOCK)
								ON oc.ORDR_ID = fo.ORDR_ID AND oc.CIS_LVL_TYPE in ('H5','H6')
			LEFT OUTER JOIN dbo.LK_CTRY ictry WITH (NOLOCK) ON ictry.CTRY_CD = oa.CTRY_CD
			LEFT OUTER JOIN dbo.LK_CTRY sctry WITH (NOLOCK) ON sctry.CTRY_CD = oas.CTRY_CD
			LEFT OUTER JOIN dbo.CUST_SCRD_DATA csdfc WITH (NOLOCK) ON csdfc.SCRD_OBJ_ID=foc.FSA_ORDR_CUST_ID AND csdfc.SCRD_OBJ_TYPE_ID=5
			--LEFT OUTER JOIN dbo.V_V5U_SSTAT_INFO clli WITH (NOLOCK) ON ord.CPE_CLLI = clli.CLLI_Code  removed 3/12/17 8:20 a.m.
			WHERE  fo.PROD_TYPE_CD = 'CP'
				AND ord.ORDR_CAT_ID = 6
				AND NOT EXISTS (SELECT 'X' FROM dbo.FSA_ORDR_CPE_LINE_ITEM focli2 WITH (NOLOCK)
											WHERE focli2.DROP_SHP = 'Y' AND focli2.ORDR_ID = fo.ORDR_ID)
				--AND ord.SCURD_CD = 0
				AND ((act.TASK_ID in (600,601,602,604,1000) and act.STUS_ID in (156,0) AND DMSTC_CD = 0)
						or
					(act.TASK_ID in (600,601) and act.STUS_ID in (0) AND DMSTC_CD = 1))
				AND @ORDR_ID = fo.ORDR_ID
				AND @ORDR_ID not in (SELECT ORDR_ID from dbo.BPM_ORDR_ADR with (NOLOCK))
			
		END	

	END

