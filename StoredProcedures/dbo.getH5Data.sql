USE [COWS]
GO
_CreateObject 'SP','dbo','getH5Data'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
	-- Author:		<Ramesh Ragi>
	-- Create date: <09/17/2013>
	-- Description:	<To get H5 Data>
	-- =============================================
	--Exec dbo.getH5Data '  AND dbo.decryptBinaryData(CUST_NME) = ''testing secure h5'' ',' CUST_ID ASC',' AND (odr.ORDR_ID = 9725 OR fsa.FTN = 9725) '
	ALTER PROCEDURE [dbo].[getH5Data]
		@search varchar(max),
		@sort	varchar(200),
		@ftn	varchar(500)
	AS
	BEGIN
	DECLARE @SQLStr			nVARCHAR(max)

		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;	
		IF @ftn = ''
			BEGIN
				SET @SQLStr = 'SELECT h5.H5_FOLDR_ID AS H5_FOLDR_ID,
									h5.CUST_ID AS CUST_ID,
									CASE WHEN (h5.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE h5.CUST_CTY_NME END AS CUST_CTY_NME,
									h5.CREAT_DT AS CREAT_DT,
									h5.MODFD_DT AS MODFD_DT,
									h5.MODFD_BY_USER_ID AS MODFD_BY_USER_ID,
									h5.CREAT_BY_USER_ID AS CREAT_BY_USER_ID,
									h5.REC_STUS_ID AS REC_STUS_ID,
									CASE WHEN (h5.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTRY_CD) ELSE h5.CTRY_CD END AS CTRY_CD,
									h5.CSG_LVL_ID,
									CASE WHEN (h5.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE h5.CUST_NME END AS CUST_NME,
									luc.USER_ADID AS CREAT_BY_USER_ADID,
									lum.USER_ADID AS MODFD_BY_USER_ADID,
									CASE WHEN (h5.CSG_LVL_ID>0) THEN lc2.CTRY_NME ELSE lc.CTRY_NME END AS CTRY_NME
								FROM dbo.H5_FOLDR h5 WITH (NOLOCK) 
								LEFT JOIN dbo.LK_USER luc WITH (NOLOCK) ON luc.USER_ID = h5.CREAT_BY_USER_ID
								LEFT JOIN dbo.LK_USER lum WITH (NOLOCK) ON lum.USER_ID = h5.MODFD_BY_USER_ID
								LEFT JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = h5.CTRY_CD
								LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID = h5.H5_FOLDR_ID AND csd.SCRD_OBJ_TYPE_ID=6
								LEFT JOIN dbo.LK_CTRY lc2 WITH (NOLOCK) ON dbo.decryptbinarydata(csd.CTRY_CD) = lc2.CTRY_CD
								WHERE 1 = 1'
			END
		ELSE
			BEGIN
				SET @SQLStr = 'SELECT h5.H5_FOLDR_ID AS H5_FOLDR_ID,
									h5.CUST_ID AS CUST_ID,
									CASE WHEN (h5.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE h5.CUST_CTY_NME END AS CUST_CTY_NME,
									h5.CREAT_DT AS CREAT_DT,
									h5.MODFD_DT AS MODFD_DT,
									h5.MODFD_BY_USER_ID AS MODFD_BY_USER_ID,
									h5.CREAT_BY_USER_ID AS CREAT_BY_USER_ID,
									h5.REC_STUS_ID AS REC_STUS_ID,
									CASE WHEN (h5.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTRY_CD) ELSE h5.CTRY_CD END AS CTRY_CD,
									h5.CSG_LVL_ID,
									CASE WHEN (h5.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE h5.CUST_NME END AS CUST_NME,
									luc.USER_ADID AS CREAT_BY_USER_ADID,
									lum.USER_ADID AS MODFD_BY_USER_ADID,
									CASE WHEN (h5.CSG_LVL_ID>0) THEN lc2.CTRY_NME ELSE lc.CTRY_NME END AS CTRY_NME
								FROM dbo.H5_FOLDR h5 WITH (NOLOCK) 
								LEFT JOIN dbo.ORDR odr WITH (NOLOCK) ON odr.H5_FOLDR_ID = h5.H5_FOLDR_ID
								LEFT JOIN dbo.FSA_ORDR fsa WITH (NOLOCK) ON fsa.ORDR_ID = odr.ORDR_ID
								LEFT JOIN dbo.LK_USER luc WITH (NOLOCK) ON luc.USER_ID = h5.CREAT_BY_USER_ID
								LEFT JOIN dbo.LK_USER lum WITH (NOLOCK) ON lum.USER_ID = h5.MODFD_BY_USER_ID
								LEFT JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = h5.CTRY_CD
								LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID = h5.H5_FOLDR_ID AND csd.SCRD_OBJ_TYPE_ID=6
								LEFT JOIN dbo.LK_CTRY lc2 WITH (NOLOCK) ON dbo.decryptbinarydata(csd.CTRY_CD) = lc2.CTRY_CD
								WHERE 1 = 1'
			END		

		IF @ftn != ''
			SET @SQLStr = @SQLStr + ' '  + @ftn 

		IF @search != ''
			SET @SQLStr = @SQLStr + ' '  + @search 

		IF @sort != ''
			SET @SQLStr = @SQLStr + ' order by ' + @sort
		EXEC sp_executesql @SQLStr 
	END
