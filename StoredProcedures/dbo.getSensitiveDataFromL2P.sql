USE [COWS]
GO
_CreateObject 'SP','dbo','getSensitiveDataFromL2P'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/11/2011
-- Description:	Inserts the FSA record into the FSA_ORDR_CUST table.
-- =========================================================
ALTER PROCEDURE [dbo].[getSensitiveDataFromL2P]
	@H1			Int = 0 
	,@ORDR_ID	Int = 0 
	,@H5		Int = 0	
AS
BEGIN
SET NOCOUNT ON;

DECLARE	@L2P_UPDT_DT	DateTime
DECLARE	@ERROR_MSG		Varchar(500)
SET		@ERROR_MSG	=	''

Begin Try
	IF	OBJECT_ID(N'tempdb..#H_CUST_IDs', N'U') IS NOT NULL 
		DROP TABLE #H_CUST_IDs
	CREATE TABLE #H_CUST_IDs	
		(CUST_ID		Varchar(10)
		,LVL_TYPE		Varchar(2))
		
	
		INSERT INTO #H_CUST_IDs	(CUST_ID, LVL_TYPE)
		SELECT DISTINCT CUST_ID, CIS_LVL_TYPE
		FROM	dbo.FSA_ORDR_CUST	WITH (NOLOCK)
		WHERE	ORDR_ID	=	@ORDR_ID
		UNION
		SELECT CONVERT(VARCHAR,@H5),'H5' where @H5 <> 0
		UNION
		SELECT CONVERT(VARCHAR,@H1),'H1' where @H1 <> 0
		
		
		IF (@H1=0)
		BEGIN
			SELECT @H1 = CONVERT(INT,CUST_ID) FROM #H_CUST_IDs WHERE LVL_TYPE='H1'
		END
		IF (@H5=0)
		BEGIN
			SELECT @H5 = CONVERT(INT,CUST_ID) FROM #H_CUST_IDs WHERE LVL_TYPE IN ('H5','H6')
		END
	
		
	IF (SELECT Count(1) FROM #H_CUST_IDs) > 0
		BEGIN
			DECLARE @CustIDs	Varchar(100)
			DECLARE @CustTypes	Varchar(1000)
			SET @CustIDs	=	''
			SET @CustTypes	=	''	 
			
			SELECT		@CustIDs	=	COALESCE(@CustIDs + ',' + CUST_ID, CUST_ID) 
				FROM	#H_CUST_IDs
			SELECT		@CustTypes	=	COALESCE(@CustTypes + ',' + '''''' + SENS_DATA_TYPE_CD + '''''', SENS_DATA_TYPE_CD) 
				FROM	dbo.LK_L2P_SENS_DATA_TYPE	WITH (NOLOCK)
				WHERE	CNTCT_TYPE_ID IS NOT NULL
				
			SET @CustIDs = SubString(@CustIDs, 2, Len(@CustIDs))
			SET @CustTypes = SubString(@CustTypes, 2, Len(@CustTypes))

		DECLARE @SCTY_GRP_CD	VARCHAR(10)
		DECLARE @CHARS_ID		Numeric
		SET		@SCTY_GRP_CD	=	''
		SET		@CHARS_ID		=	0
		
		IF	OBJECT_ID(N'tempdb..#CUST_INFO_SENSITIVE_ALL', N'U') IS NOT NULL 
			DROP TABLE #CUST_INFO_SENSITIVE_ALL

		CREATE TABLE #CUST_INFO_SENSITIVE_ALL
			(
				[CUST_CHARS_ID]			Int			NOT NULL,
				[SENS_DATA_TYPE_CD]		Varchar(4)	NOT NULL,
				[EFF_BEGIN_TMST]		DateTime	NOT NULL,
				[EFF_END_TMST]			DateTime	NOT NULL,
				[SCTY_GRP_CD]			Varchar(5)	NULL,
				[CNTRC_CD]				Varchar(3)	NOT NULL,
				[MSTR_CHARS_ID]			Int			NOT NULL,
				[SENS_NME]				Varchar(50) NULL,
				[TITLE_TXT]				Varchar(30) NULL,
				[LINE_1_ADR]			Varchar(26) NULL,
				[LINE_2_ADR]			Varchar(26) NULL,
				[CTY_NME]				Varchar(20) NULL,
				[STT_CD]				Varchar(2)	NULL,
				[PSTL_CD]				Varchar(15) NULL,
				[PROVINCE_CD]			Varchar(2)	NULL,
				[CTRY_CD]				Varchar(3)	NULL,
				[PHN_NBR]				Varchar(20) NULL,
				[PHN_EXT_NBR]			Varchar(4)	NULL,
				[EMAIL_ADR]				Varchar(60) NULL,
				[IPS_CYC_ID]			Varchar(2)	NULL,
				[IPS_RCVBL_TYPE_CD]		Varchar(1)	NULL,
				[SRVC_CD]				Varchar(3)	NULL,
				[RLAT_KEY_ID]			Varchar(50) NOT NULL,
				[ID_TYPE_CD]			Varchar(20) NOT NULL,
				[ID_TYPE_DES]			Varchar(50) NULL,
				[SCTY_GRP_DES]			Varchar(50) NULL,
				[SENS_DATA_TYPE_DES]	Varchar(30) NULL,
				[SENS_DATA_CAT_CD]		Varchar(4)	NULL,
				[SENS_DATA_CAT_DES]		Varchar(30)	NULL
			) 
		EXEC('CALL SENS_CTX.SET_CONTEXT_FGAC('''+'QDA'+''', '''', '''')') AT L2S										

		DECLARE @CMD2 AS NVARCHAR(MAX)
		SET @CMD2 = 'INSERT INTO #CUST_INFO_SENSITIVE_ALL ' +
			'([CUST_CHARS_ID],[SENS_DATA_TYPE_CD],[EFF_BEGIN_TMST],[EFF_END_TMST],[SCTY_GRP_CD],[CNTRC_CD] ' +
			',[MSTR_CHARS_ID],[SENS_NME],[TITLE_TXT],[LINE_1_ADR],[LINE_2_ADR],[CTY_NME],[STT_CD],[PSTL_CD] ' +
			',[PROVINCE_CD],[CTRY_CD],[PHN_NBR],[PHN_EXT_NBR],[EMAIL_ADR],[IPS_CYC_ID],[IPS_RCVBL_TYPE_CD] ' +
			',[SRVC_CD],[RLAT_KEY_ID],[ID_TYPE_CD],[ID_TYPE_DES],[SCTY_GRP_DES],[SENS_DATA_TYPE_DES] ' +
			',[SENS_DATA_CAT_CD],[SENS_DATA_CAT_DES]) ' +
			'SELECT CUST_CHARS_ID,SENS_DATA_TYPE_CD,EFF_BEGIN_TMST,EFF_END_TMST,SCTY_GRP_CD,CNTRC_CD ' +
			',MSTR_CHARS_ID,SENS_NME,TITLE_TXT,LINE_1_ADR,LINE_2_ADR,CTY_NME,STT_CD,PSTL_CD ' +
			',PROVINCE_CD,ISO_CTRY_CD,PHN_NBR,PHN_EXT_NBR,EMAIL_ADR,IPS_CYC_ID,IPS_RCVBL_TYPE_CD ' +
			',SRVC_CD,RLAT_KEY_ID,ID_TYPE_CD,ID_TYPE_DES,SCTY_GRP_DES,SENS_DATA_TYPE_DES ' +
			',SENS_DATA_CAT_CD,SENS_DATA_CAT_DES ' +
			'FROM OPENQUERY ' +
					'(L2S, ' +
					'''select CUST_CHARS_ID,SENS_DATA_TYPE_CD,EFF_BEGIN_TMST,EFF_END_TMST,SCTY_GRP_CD,CNTRC_CD ' +
					',MSTR_CHARS_ID,SENS_NME,TITLE_TXT,LINE_1_ADR,LINE_2_ADR,CTY_NME,STT_CD,PSTL_CD ' +
					',PROVINCE_CD,ISO_CTRY_CD,PHN_NBR,PHN_EXT_NBR,EMAIL_ADR,IPS_CYC_ID,IPS_RCVBL_TYPE_CD ' +
					',SRVC_CD,RLAT_KEY_ID,ID_TYPE_CD,ID_TYPE_DES,SCTY_GRP_DES,SENS_DATA_TYPE_DES ' +
					',SENS_DATA_CAT_CD,SENS_DATA_CAT_DES from sens_owner.v_sens_adr S ' +
					'WHERE ' +
					'S.SENS_DATA_TYPE_CD IN (' +@CustTypes + ') 
					AND S.ID_TYPE_CD = ''''CIP_CUST_ID'''' AND S.RLAT_KEY_ID IN (' + @CustIDs + ') 
					AND S.EFF_END_TMST > to_date(''''12/31/3333'''', ''''mm/dd/yyyy'''')'')'
		--PRINT @CMD2
		EXECUTE sp_executesql @CMD2
		EXEC('CALL SENS_CTX.CLEAR_CONTEXT_FGAC()') AT L2S
--SELECT * FROM #CUST_INFO_SENSITIVE_ALL
		IF	(SELECT Count(1) FROM #CUST_INFO_SENSITIVE_ALL)	>	0
			BEGIN
				OPEN SYMMETRIC KEY FS@K3y 
					DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
					
				---------------------------------------------------------------------------------------------
				--	Update the CSG level & CharsID in the ORDR Table.	
				---------------------------------------------------------------------------------------------
				IF EXISTS (SELECT 'X'
						   FROM #CUST_INFO_SENSITIVE_ALL WITH (NOLOCK)
						   WHERE SRVC_CD				=	'H1'	
							 AND SENS_DATA_TYPE_CD		=	'PHYS')
				BEGIN
					UPDATE			oc
						SET			oc.CSG_LVL_CD	=	vc.SCTY_GRP_CD	
									,oc.CHARS_ID	=	vc.CUST_CHARS_ID
									,oc.SCURD_CD	=	CASE (oc.ORDR_CAT_ID) 
															WHEN 4 THEN 1
															ELSE oc.SCURD_CD
														END
						FROM		#CUST_INFO_SENSITIVE_ALL	vc	WITH (NOLOCK)	
						INNER JOIN	dbo.ORDR					oc	WITH (ROWLOCK)	ON	oc.ORDR_ID	=	@ORDR_ID
						WHERE		vc.SRVC_CD					=	'H1'	
								AND	vc.SENS_DATA_TYPE_CD		=	'PHYS'
				END
				ELSE
				BEGIN
					IF EXISTS (SELECT 'X'
								FROM #CUST_INFO_SENSITIVE_ALL WITH (NOLOCK)
								WHERE SRVC_CD				IN	('H5','H6','ES')	
							 )
							BEGIN
								SELECT @SCTY_GRP_CD = SCTY_GRP_CD
									FROM #CUST_INFO_SENSITIVE_ALL WITH (NOLOCK)
									WHERE SRVC_CD				IN	('H5','H6','ES')	

								IF @ORDR_ID != 0 AND @H5 != 0
									BEGIN
										UPDATE			oc
											SET			oc.CSG_LVL_CD	=	vc.SCTY_GRP_CD	
														,oc.CHARS_ID	=	vc.CUST_CHARS_ID
														,oc.SCURD_CD	=	CASE (oc.ORDR_CAT_ID) 
																				WHEN 4 THEN 1
																				ELSE oc.SCURD_CD
																			END
											FROM		#CUST_INFO_SENSITIVE_ALL	vc	WITH (NOLOCK)	
											INNER JOIN	dbo.ORDR					oc	WITH (ROWLOCK)	ON	oc.ORDR_ID	=	@ORDR_ID
											WHERE		vc.SRVC_CD					IN	('ES','H6','H5')	
											

										UPDATE	h5
											SET h5.SCURD_CD		=	'1',
												h5.CSG_LVL_CD	=	vc.SCTY_GRP_CD
											FROM #CUST_INFO_SENSITIVE_ALL	vc	WITH (NOLOCK)	
											INNER JOIN	dbo.H5_FOLDR	h5	WITH (ROWLOCK)	ON	h5.CUST_ID = vc.RLAT_KEY_ID
											WHERE		vc.SRVC_CD					IN	('ES','H6','H5')		
													
									END
							END
								
					
				END
		

				IF @ORDR_ID != 0 AND @H1 != 0
					BEGIN
						---------------------------------------------------------------------------------------------
						--	Update the Customer Name in the FSA_ORDR_CUST Table.	
						---------------------------------------------------------------------------------------------
						UPDATE			oc
							SET			oc.CUST_NME		=	dbo.encryptString(vc.SENS_NME)
							FROM		#CUST_INFO_SENSITIVE_ALL	vc	WITH (NOLOCK)	
							INNER JOIN	dbo.FSA_ORDR_CUST			oc	WITH (ROWLOCK)	ON	oc.ORDR_ID	=	@ORDR_ID
							WHERE		(		ISNULL(oc.CIS_LVL_TYPE, '')	=	'H1'
										AND		vc.SRVC_CD					=	'H1'	
										AND		vc.SENS_DATA_TYPE_CD		=	'PHYS'
										)
								OR		(		ISNULL(oc.CIS_LVL_TYPE, '')	=	'H4'
										AND		vc.SRVC_CD					=	'H4'	
										AND		vc.SENS_DATA_TYPE_CD		=	'BILL'
										)
								OR		(		ISNULL(oc.CIS_LVL_TYPE, '')	IN	('H5', 'H6')	
										AND		vc.SRVC_CD					IN	('H5', 'H6', 'ES', 'IP', 'CP')--,'EP')
										AND		vc.SENS_DATA_TYPE_CD		=		'PHYS'
										)
						
						---------------------------------------------------------------------------------------------
						--	Update the Contact Info in the ORDR_CNTCT Table.	
						---------------------------------------------------------------------------------------------		
						UPDATE			oc
							SET			oc.LST_NME		=	CASE 
																WHEN	CHARINDEX(' ', RTRIM(LTRIM(vc.SENS_NME))) = 0 THEN dbo.encryptString(RTRIM(LTRIM(vc.SENS_NME))) 
																ELSE	dbo.encryptString(SUBSTRING(vc.SENS_NME, CHARINDEX(' ', vc.SENS_NME) + 1, LEN(vc.SENS_NME)))
															END
										,oc.FRST_NME	=	CASE 
																WHEN	CHARINDEX(' ', RTRIM(LTRIM(vc.SENS_NME))) = 0 THEN NULL
																ELSE	dbo.encryptString(SUBSTRING(vc.SENS_NME, 1, CHARINDEX(' ', vc.SENS_NME) - 1))
															END
										,oc.CNTCT_NME	=	dbo.encryptString(vc.SENS_NME)							
										,oc.EMAIL_ADR	=	dbo.encryptString(vc.EMAIL_ADR)
										,oc.PHN_NBR		=	vc.PHN_NBR
										,oc.NPA			=	''
										,oc.NXX			=	''
										,oc.STN_NBR		=	''
										,oc.ISD_CD		=	Case	od.DMSTC_CD
																When	1	Then	lc.ISD_CD
																Else	NULL
															End
							FROM		#CUST_INFO_SENSITIVE_ALL	vc	WITH (NOLOCK)
							INNER JOIN	dbo.ORDR_CNTCT				oc	WITH (ROWLOCK)	ON	oc.ORDR_ID	=	@ORDR_ID
							INNER JOIN	dbo.ORDR					od	WITH (NOLOCK)	ON	oc.ORDR_ID	=	od.ORDR_ID
							LEFT JOIN	dbo.LK_CTRY					lc	WITH (NOLOCK)	ON	vc.CTRY_CD	=	lc.L2P_CTRY_CD	
							WHERE		(		ISNULL(oc.CIS_LVL_TYPE, '')	=	'H1'
										AND		vc.SRVC_CD					=	'H1'	
										AND		vc.SENS_DATA_TYPE_CD		=	'PHYS'
										)
								OR		(		ISNULL(oc.CIS_LVL_TYPE, '')	=	'H4'
										AND		vc.SRVC_CD					=	'H4'	
										AND		vc.SENS_DATA_TYPE_CD		=	'BILL'
										)
								OR		(		ISNULL(oc.CIS_LVL_TYPE, '')	IN	('H5', 'H6')
										AND		vc.SRVC_CD					IN	('H5', 'H6', 'ES', 'IP', 'CP')--,'EP')
										AND		vc.SENS_DATA_TYPE_CD		=	'PHYS'
										)	
								OR		(		ISNULL(oc.CIS_LVL_TYPE, '')	=		''	
										AND		vc.SRVC_CD					IN	('H5', 'H6', 'ES', 'IP', 'CP')--,'EP')
										AND		(	(ISNULL(oc.FSA_MDUL_ID, '')	=	'MDS'	AND	vc.SENS_DATA_TYPE_CD	=	Case	oc.ROLE_ID
																																When	8	Then	'SVCA'
																																Else	Case	oc.CNTCT_TYPE_ID
																																			When	1	Then	'CONT'
																																			Else	'CON2'
																																		End
																															End)
												OR	(ISNULL(oc.FSA_MDUL_ID, '')	IN	('SUP','PRT')AND vc.SENS_DATA_TYPE_CD	=	Case	oc.ROLE_ID
																																	When	8	Then	'SVCA'
																																	When	7	Then	'CON2'
																																	Else	'CONT'
																																End))
										)
								
						---------------------------------------------------------------------------------------------
						--	Update the Customer Name in the FSA_ORDR_CUST Table.	
						---------------------------------------------------------------------------------------------
						UPDATE			oc
							SET			oc.STREET_ADR_1	 =	dbo.encryptString(vc.LINE_1_ADR)
										,oc.STREET_ADR_2 =	dbo.encryptString(vc.LINE_2_ADR)
										,oc.CTY_NME		 =	dbo.encryptString(vc.CTY_NME)
										,oc.STT_CD		 =	dbo.encryptString(vc.STT_CD)
										,oc.CTRY_CD		 =	Case	
																When	Len(ISNULL(lc.CTRY_CD, ''))	<	2	Then	NULL
																Else	lc.CTRY_CD
															End
										,oc.PRVN_NME	=	dbo.encryptString(vc.PROVINCE_CD)
										,oc.ZIP_PSTL_CD	=	dbo.encryptString(vc.PSTL_CD)
							FROM		#CUST_INFO_SENSITIVE_ALL	vc	WITH (NOLOCK)
							INNER JOIN	dbo.ORDR_ADR				oc	WITH (ROWLOCK)	ON	oc.ORDR_ID	=	@ORDR_ID
							LEFT JOIN	dbo.LK_CTRY					lc	WITH (NOLOCK)	ON	vc.CTRY_CD	=	lc.L2P_CTRY_CD	
							WHERE		(		ISNULL(oc.CIS_LVL_TYPE, '')	=	'H1'
										AND		vc.SRVC_CD					=	'H1'	
										AND		vc.SENS_DATA_TYPE_CD		=	'PHYS'
										)
								OR		(		ISNULL(oc.CIS_LVL_TYPE, '')	=	'H4'
										AND		vc.SRVC_CD					=	'H4'	
										AND		vc.SENS_DATA_TYPE_CD		=	'BILL'
										)
								OR		(		ISNULL(oc.CIS_LVL_TYPE, '')	IN	('H5', 'H6')
										AND		vc.SRVC_CD					IN	('H5', 'H6', 'ES', 'IP', 'CP')--,'EP')
										AND		vc.SENS_DATA_TYPE_CD		=	'PHYS'
										)				
								OR		(		ISNULL(oc.CIS_LVL_TYPE, '')	=		''	
										AND		vc.SRVC_CD					IN	('H5', 'H6', 'ES', 'IP', 'CP')--,'EP')
										AND		(	(ISNULL(oc.FSA_MDUL_ID, '')	=	'MDS'	AND	vc.SENS_DATA_TYPE_CD	=	Case	oc.ADR_TYPE_ID
																																When	8	Then	'SVCA'
																																Else	'CONT'
																															End)
												OR	(ISNULL(oc.FSA_MDUL_ID, '')	IN	('SUP','PRT')AND vc.SENS_DATA_TYPE_CD	=	Case	oc.ADR_TYPE_ID
																																	When	8	Then	'SVCA'
																																	When	7	Then	'CON2'
																																	Else	'CONT'
																																End))
										)
					END			
				---------------------------------------------------------------------------------------------
				--	Update the Region Info in the ORDR Table.	
				---------------------------------------------------------------------------------------------
				DECLARE	@CTRY_CD	Varchar(3)			
				SELECT Top 1	@CTRY_CD	=	ISNULL(CTRY_CD, '')
					FROM 		#CUST_INFO_SENSITIVE_ALL WITH (NOLOCK)
					WHERE		SRVC_CD	NOT IN	('H1', 'H4')
						AND		SENS_DATA_TYPE_CD	=	'PHYS'		
						
				UPDATE			od
					SET			od.RGN_ID	=	ISNULL(lc.RGN_ID, 1)
					FROM		dbo.ORDR	od	WITH (ROWLOCK)
					INNER JOIN	dbo.LK_CTRY	lc	WITH (NOLOCK)	ON	od.ORDR_ID	=	@ORDR_ID
					WHERE		lc.L2P_CTRY_CD	=	@CTRY_CD
								
				SET	@L2P_UPDT_DT	=	getDate()
			END	--	End for "If Count(#CUST_INFO_SENSITIVE_ALL) > 1"
		END		--	End for "IF Count(#H_CUST_IDs) > 1"
End Try

Begin Catch
	SET @ERROR_MSG	=	ERROR_MESSAGE()
End Catch	

---------------------------------------------------------------------------------------------
--	Write logs for every L2P Request.	
---------------------------------------------------------------------------------------------
IF @H1 != 0 
	BEGIN
		EXEC	[dbo].[writeL2PLog]
			@H1			
			,@ORDR_ID	
			,@L2P_UPDT_DT
			,@ERROR_MSG
	END
ELSE IF @H5 != 0 
	BEGIN
		EXEC	[dbo].[writeL2PLog]
			@H5			
			,@ORDR_ID	
			,@L2P_UPDT_DT
			,@ERROR_MSG
			
		IF @SCTY_GRP_CD != ''	
			SELECT @SCTY_GRP_CD AS SCTY_GRP_CD
	END

			
END
