USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getSSTATAcks]    Script Date: 06/13/2019 11:23:00 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		dlp0278
-- Create date: 10/2/2015
-- Description:	Get the details for all STTAT requests
-- =========================================================
alter PROCEDURE [web].[getMDSDiscoDuration] 
	@ID INT OUTPUT

AS
BEGIN
SET NOCOUNT ON;
Begin Try

	SELECT TOP 1  @ID = PRMTR_VALU_TXT 
	  
  FROM [dbo].[LK_SYS_CFG]
  WHERE PRMTR_NME = 'MDSDiscoDuration'

  SELECT @ID
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

