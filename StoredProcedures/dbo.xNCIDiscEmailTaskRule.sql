USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[xNCIDiscEmailTaskRule]    Script Date: 07/22/2021 10:24:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<David Phillips>
-- Create date: <01/27/2021>
-- Description:	<Complete task 223 when no CPE Logicallis email required.>
-- =============================================
Create PROCEDURE [dbo].[xNCIDiscEmailTaskRule] --15878
	@OrderID	Int,
	@TaskID		SmallInt = NULL,
	@TaskStatus	TinyInt = NULL,
	@Comments	Varchar(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;
 
BEGIN TRY

	DECLARE @PO	varchar (20)

	  IF NOT EXISTS (SELECT 'x' FROM dbo.ORDR WITH (NOLOCK) WHERE RGN_ID = 3 AND @OrderID = ORDR_ID)

			BEGIN	
				exec [dbo].[CompleteActiveTask] @OrderID,223,2,'System completed Disc CPE Email task due to xNCI region is not Asia.',1,1
			END 
	  ELSE
	  	IF NOT EXISTS (SELECT 'X'  FROM dbo.FSA_ORDR fo WITH (NOLOCK)
			INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON cli.ORDR_ID = fo.ORDR_ID
			LEFT OUTER JOIN	dbo.ORDR_ADR oa			WITH (NOLOCK)	ON	oa.ORDR_ID = fo.ORDR_ID  
															AND	oa.CIS_LVL_TYPE in ('H6','H5')
															AND oa.ADR_TYPE_ID = 18
				WHERE  fo.ORDR_ID in (SELECT ORDR_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
										 WHERE ORDR_ID = @OrderID AND EQPT_TYPE_ID = 'MJH' AND ITM_STUS = 401
											AND CNTRC_TYPE_ID in ('RNTL','LEAS'))
				AND oa.CTRY_CD not in ('AU','HK','JP','KP','KR','NZ','MY','SG','TW','TH')
				AND fo.ORDR_ID = @OrderID )
			
			BEGIN
				exec [dbo].[CompleteActiveTask] @OrderID,223,2,'System completed Disc CPE Email task.  Logicallis email not required.',1,1
			END 
		
		
END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH
    
END
