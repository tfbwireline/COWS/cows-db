USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[ProcessMach5CPECancels]    Script Date: 05/21/2019 11:35:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <10/23/2015>
-- Description:	<To Process CPE Mach5 Cancels>
-- =============================================
ALTER PROCEDURE [dbo].[ProcessMach5CPECancels] 
	@ORDR_ID INT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
	DECLARE @INTL_CD BIT, @PRNT_ORDR_ID INT
	
	SELECT TOP 1	@PRNT_ORDR_ID	=	cp.ORDR_ID
		FROM   dbo.ORDR o WITH (NOLOCK)
	INNER JOIN dbo.FSA_ORDR f	WITH (NOLOCK) ON o.ORDR_ID	= f.ORDR_ID
	INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM c WITH (NOLOCK) ON f.ORDR_ID = c.ORDR_ID
	INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cp WITH (NOLOCK) 
				ON  cp.ORDR_CMPNT_ID = c.ORDR_CMPNT_ID
		WHERE	o.ORDR_ID = @ORDR_ID
			AND EXISTS
				(
					SELECT 'X'
						FROM dbo.FSA_ORDR_CPE_LINE_ITEM c WITH (NOLOCK)
						WHERE	c.ORDR_ID = o.ORDR_ID	
							AND ISNULL(c.ITM_STUS,0) = 402
							AND EXISTS
								(
									SELECT 'X'
										FROM	dbo.FSA_ORDR_CPE_LINE_ITEM pl WITH (NOLOCK)
										WHERE	pl.ORDR_ID = cp.ORDR_ID
											AND	ISNULL(pl.ITM_STUS,0) != 402
											AND pl.ORDR_CMPNT_ID = c.ORDR_CMPNT_ID
								)
				)

	IF @PRNT_ORDR_ID != 0
		BEGIN
			PRINT @PRNT_ORDR_ID
			
			/* UPDATE	o
				SET o.ITM_STUS = 402
				FROM	dbo.FSA_ORDR_CPE_LINE_ITEM o WITH (ROWLOCK)
			INNER JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM c WITH (NOLOCK) ON o.ORDR_CMPNT_ID = c.ORDR_CMPNT_ID
				WHERE	o.ORDR_ID	=	@PRNT_ORDR_ID
					AND c.ORDR_ID	=	@ORDR_ID */
					
			UPDATE o
				SET o.ITM_STUS = 402
				FROM  dbo.FSA_ORDR_CPE_LINE_ITEM o WITH (ROWLOCK)
				WHERE o.ORDR_ID	=	@PRNT_ORDR_ID
				  AND EXISTS (SELECT 'X'
							  FROM dbo.FSA_ORDR_CPE_LINE_ITEM c WITH (NOLOCK)
							  WHERE o.ORDR_CMPNT_ID = c.ORDR_CMPNT_ID
							    AND c.ORDR_ID	=	@ORDR_ID)
			
		END
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
    
END
