﻿USE [COWS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Silva,Breno
-- Create date: 11-21-2011
-- Description:	Get Locked Events from DB
-- =============================================
IF OBJECT_ID ('[web].[getLockedEvents]') IS NOT NULL
DROP PROC [web].[getLockedEvents]
GO
CREATE PROCEDURE [web].[getLockedEvents] 
	@EVENTID Int
AS
BEGIN
	SET NOCOUNT ON;

    SELECT [t0].[EVENT_ID], [t1].[LOCK_BY_USER_ID], [t3].[FULL_NME], [t2].[EVENT_TYPE_NME]
    FROM [dbo].[EVENT] AS [t0] WITH (NOLOCK)
    INNER JOIN [dbo].[EVENT_REC_LOCK] AS [t1] WITH (NOLOCK) ON [t0].[EVENT_ID] = [t1].[EVENT_ID] 
    INNER JOIN [dbo].[LK_EVENT_TYPE] AS [t2] WITH (NOLOCK) ON [t0].[EVENT_TYPE_ID] = [t2].[EVENT_TYPE_ID]
    INNER JOIN [dbo].[LK_USER] AS [t3] WITH (NOLOCK) ON [t3].[USER_ID] = [t1].[LOCK_BY_USER_ID]
    WHERE [t1].[EVENT_ID] = @EVENTID
    
END
GO
