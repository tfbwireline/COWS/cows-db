USE [COWS]
GO
_CreateObject 'SP','dbo','getSCMInterfaceView'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<David Phillips>
-- Create date: <09/27/2017>
-- Description:	<to get PeopleSoft View orders>
-- =============================================
ALTER PROCEDURE [dbo].[getSCMInterfaceView]
	@M5OrderNbr VARCHAR(20) = ''
	,@ReqNbr VARCHAR(20) = ''
AS

BEGIN
	SET NOCOUNT ON;
	
	
	BEGIN TRY
		SELECT  DISTINCT TOP 500
							 fo.FTN 
							,litm.DEVICE_ID
							,litm.PLSFT_RQSTN_NBR
							,litm.RQSTN_DT
							,litm.PRCH_ORDR_NBR
							,Case	
								When	ISNULL(lt.TASK_NME, '') = 'Bill Activated'	Then	ts.ORDR_STUS_DES 
								WHEN	ISNULL(lt.TASK_NME, '') = 'xNCI Milestones Complete' THEN 'xNCI Milestones Complete'
								WHEN    ord.ORDR_STUS_ID = 4 THEN 'Cancelled'
								When	ISNULL(lt.TASK_NME, '') = ''			Then	ts.ORDR_STUS_DES 
								Else	ISNULL(lt.TASK_NME, '')	+ '-' + ls.STUS_DES
							End	AS OrderStatus 
				
	FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
	INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
	INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
	INNER JOIN   dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID --AND	act.STUS_ID IN	(0, 3)
	INNER JOIN	 dbo.LK_ORDR_STUS ts WITH (NOLOCK) ON ord.ORDR_STUS_ID = ts.ORDR_STUS_ID
	LEFT JOIN	 dbo.LK_TASK lt WITH (NOLOCK) ON lt.TASK_ID = act.TASK_ID AND act.STUS_ID IN (0, 3)
	LEFT JOIN	 dbo.LK_STUS ls WITH (NOLOCK) ON act.STUS_ID = ls.STUS_ID

	WHERE  fo.PROD_TYPE_CD = 'CP'
		--AND litm.RQSTN_DT > DATEADD(dd,-11,getdate())
		AND ISNULL(DEVICE_ID,'') <> ''
		--AND litm.ITM_STUS = 401 
		AND ISNULL(litm.RQSTN_DT,'') <> ''
		AND FTN like '%'+ @M5OrderNbr +'%'
		AND litm.PLSFT_RQSTN_NBR like '%'+ @ReqNbr +'%'
		AND (act.STUS_ID IN (0, 3) OR ord.ORDR_STUS_ID = 4)
	ORDER BY RQSTN_DT desc
				
		
    END TRY
    
    BEGIN CATCH
		EXEC dbo.insertErrorInfo;
    END CATCH
END
