USE [COWS]
GO

/****** Object:  StoredProcedure [dbo].[updatePS_PO_RCVD]    Script Date: 10/01/2015 11:07:15 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 08/11/2015
-- Description:	Updates Receipt has been sent to PeopleSoft(SCM).
-- =========================================================
CREATE PROCEDURE [dbo].[updatePS_PO_RCVD]
	@PO_RCVD_ID  int,
	@BATCH_SEQ   int
	
        
AS
BEGIN TRY
				
	UPDATE dbo.PS_RCPT_QUEUE
		SET PS_SENT_DT = GETDATE(),
		    BATCH_SEQ = @BATCH_SEQ
	WHERE  PO_RCVD_ID = @PO_RCVD_ID
	
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH


GO


