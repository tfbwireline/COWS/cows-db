USE [COWS]
GO
_CreateObject 'SP','dbo','processCPEMve_V5U'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 10/3/2016
-- Description:	SP moves Domestic CPE orders back in the flow within Appian.      
-- =========================================================
ALTER PROCEDURE [dbo].[processCPEMve_V5U]
	@ORDR_ID	int,
	@ADID	varchar (10),
	@TASK_NME	varchar (30),
	@COMMENTS	varchar (max)


AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @NTE  varchar(max)
	DECLARE @TASK_ID int
	DECLARE @FTN varchar(50)
	
	SELECT @TASK_ID = TASK_ID FROM dbo.LK_TASK WITH (NOLOCK) where TASK_NME = @TASK_NME

	BEGIN TRY
		IF @TASK_ID = 600
			BEGIN
				
				UPDATE dbo.ACT_TASK
				 SET STUS_ID = 0, MODFD_DT = GETDATE()
				 WHERE ORDR_ID = @ORDR_ID AND TASK_ID = 600
				 
				 SET @NTE = 'Order moved to Eqp Review by ' + @ADID + '.  ' + @COMMENTS	
				 
				 IF EXISTS (SELECT * FROM dbo.ACT_TASK WITH (NOLOCK) WHERE TASK_ID = 601 
									AND ORDR_ID = @ORDR_ID)
						BEGIN	
							EXEC dbo.insertOrderNotes_V5U @ORDR_ID, 6, @NTE, @ADID 
						END
				 
				DELETE dbo.ACT_TASK
					WHERE ORDR_ID = @ORDR_ID AND TASK_ID IN (601,602,604,1000)
					
				IF EXISTS (SELECT * FROM dbo.SSTAT_REQ  WITH (NOLOCK)
								WHERE (ORDR_ID = @ORDR_ID 
										AND SSTAT_MSG_ID = 1 
										AND STUS_ID = 11))
					BEGIN
						UPDATE dbo.SSTAT_REQ
						 SET STUS_ID = 9
						WHERE (ORDR_ID = ORDR_ID AND SSTAT_MSG_ID = 1 AND STUS_ID = 11)
						
						SELECT @FTN = FTN FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID
						
						EXEC dbo.insertSSTATReq @ORDR_ID,4,@FTN
					END
										
						
			END
			
			IF @TASK_ID = 601
				BEGIN
					
					UPDATE dbo.ACT_TASK
					 SET STUS_ID = 0, MODFD_DT = GETDATE()
					 WHERE ORDR_ID = @ORDR_ID AND TASK_ID = 601
					 
					 SET @NTE = 'Order moved to Mat Req by ' + @ADID + '.  ' + @COMMENTS	
					 
					 IF EXISTS (SELECT * FROM dbo.ACT_TASK WITH (NOLOCK) WHERE TASK_ID = 602 
									AND ORDR_ID = @ORDR_ID)
						BEGIN	
							EXEC dbo.insertOrderNotes_V5U @ORDR_ID, 6, @NTE, @ADID 
						END
					 
					DELETE dbo.ACT_TASK
						WHERE ORDR_ID = @ORDR_ID AND TASK_ID IN (602,604,1000)
						
					IF EXISTS (SELECT * FROM dbo.SSTAT_REQ WITH (NOLOCK) 
							WHERE (ORDR_ID = @ORDR_ID 
									AND SSTAT_MSG_ID = 1 
									AND STUS_ID = 11))
						BEGIN
							UPDATE dbo.SSTAT_REQ
							 SET STUS_ID = 9
							WHERE (ORDR_ID = ORDR_ID AND SSTAT_MSG_ID = 1 AND STUS_ID = 11)
							
							SELECT @FTN = FTN FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID
							
							EXEC dbo.insertSSTATReq @ORDR_ID,4,@FTN
						END
	
				END
			IF @TASK_ID = 602
				BEGIN
					
					UPDATE dbo.ACT_TASK
					 SET STUS_ID = 0, MODFD_DT = GETDATE()
					 WHERE ORDR_ID = @ORDR_ID AND TASK_ID = 602
					 
				 	SET @NTE = 'Order moved to Eqp Reciepts by ' + @ADID + '.  ' + @COMMENTS
				 
				 	IF EXISTS (SELECT * FROM dbo.ACT_TASK WITH (NOLOCK) WHERE TASK_ID = 1000
								AND ORDR_ID = @ORDR_ID)
					BEGIN	
						EXEC dbo.insertOrderNotes_V5U @ORDR_ID, 6, @NTE, @ADID
					END
					 
					DELETE dbo.ACT_TASK
						WHERE ORDR_ID = @ORDR_ID AND TASK_ID IN (604,1000)
					
					IF EXISTS (SELECT * FROM dbo.SSTAT_REQ WITH (NOLOCK) 
							WHERE (ORDR_ID = @ORDR_ID 
									AND SSTAT_MSG_ID = 1 
									AND STUS_ID = 11))
						BEGIN
							UPDATE dbo.SSTAT_REQ
							 SET STUS_ID = 9
							WHERE (ORDR_ID = ORDR_ID AND SSTAT_MSG_ID = 1 AND STUS_ID = 11)
							
							SELECT @FTN = FTN FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID
							
							EXEC dbo.insertSSTATReq @ORDR_ID,4,@FTN
						END
						
		
				END
			

			
	END TRY


	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH


END

