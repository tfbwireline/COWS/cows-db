USE [COWS]
GO
_CreateObject 'SP','web','getVASSubTypesFromBPM'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  jrg7298            
-- Create date: 2/16/2020       
-- Description: Get all VAS SubTypes from BPM
-- =============================================            
ALTER PROCEDURE [web].[getVASSubTypesFromBPM]
AS            
BEGIN            
SET NOCOUNT ON;
BEGIN TRY

IF ((datepart(hour,getdate()) % 2) = 0)
BEGIN
	SELECT * FROM (SELECT '' AS SUB_TYPE 
	UNION 
	SELECT 'Half Tunnel' AS SUB_TYPE 
	UNION
	SELECT 'RAS' AS SUB_TYPE 
	UNION
	SELECT 'Firewall' AS SUB_TYPE 
	UNION
	SELECT SUB_TYPE FROM OPENQUERY(NRMBPM,'SELECT DISTINCT SUB_TYPE FROM BPMF_OWNER.V_BPMF_VAS_COWS_ACTV_EVENT WHERE SUB_TYPE IS NOT NULL')) as x
	ORDER BY x.SUB_TYPE
END
ELSE 
BEGIN
	SELECT * FROM (SELECT '' AS SUB_TYPE 
	UNION 
	SELECT 'Half Tunnel' AS SUB_TYPE 
	UNION
	SELECT 'RAS' AS SUB_TYPE 
	UNION
	SELECT 'Firewall' AS SUB_TYPE) as x
	ORDER BY x.SUB_TYPE
END

END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END