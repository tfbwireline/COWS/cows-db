USE [COWS]
GO
_CreateObject 'SP','dbo','lockUnlockRedesigns'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 08/26/2011
-- Description:	Locks/Unlocks the Orders and Events.
-- =========================================================
ALTER PROCEDURE [dbo].[lockUnlockRedesigns]
	@ORDR_ID	varchar(10)
	,@EVENT_ID	varchar(10)
	,@USER_ID	Int
	,@IsOrder	int
	,@Unlock	Bit	
	,@IsLocked	Int	
	
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	IF	@Unlock	=	1
		BEGIN
			DELETE FROM REDSGN_REC_LOCK	WITH (ROWLOCK)
				WHERE	LOCK_BY_USER_ID	=	@USER_ID
		END
	ELSE	
		BEGIN
			
					SELECT		@IsLocked	=	1
						WHERE	EXISTS
								(SELECT 'X' FROM dbo.REDSGN_REC_LOCK AS [t1]	WITH (NOLOCK)
								 INNER JOIN dbo.REDSGN AS [t0] WITH (NOLOCK) ON  [t0].[REDSGN_ID] = [t1].[REDSGN_ID] 
									WHERE	[t0].[REDSGN_NBR]			=	@ORDR_ID
										AND	[t1].[LOCK_BY_USER_ID]	!=	@USER_ID)
										
					IF	@IsLocked	=	0
					AND	NOT EXISTS
						(SELECT 'X' FROM dbo.REDSGN_REC_LOCK AS [t1]	WITH (NOLOCK)
								 INNER JOIN dbo.REDSGN AS [t0] WITH (NOLOCK) ON  [t0].[REDSGN_ID] = [t1].[REDSGN_ID] 
									WHERE	[t0].[REDSGN_NBR]			=	@ORDR_ID
								AND	LOCK_BY_USER_ID	=	@USER_ID)
						BEGIN
							INSERT INTO dbo.REDSGN_REC_LOCK	WITH (ROWLOCK)
										(REDSGN_ID
										,STRT_REC_LOCK_TMST
										,LOCK_BY_USER_ID,CREAT_DT)
								SELECT [REDSGN_ID], GETDATE(), @USER_ID,GETDATE()
								FROM dbo.REDSGN  WITH (NOLOCK) 
								WHERE	[REDSGN_NBR]			=	@ORDR_ID
								
								
								--VALUES	(@ORDR_ID
								--		,getDate()
								--		,@USER_ID)
						END	

		END
		
	SELECT @IsLocked
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END