USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[CpeEqRvwTskRule]    Script Date: 10/10/2018 07:56:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Created By:		David Phillips
-- Create date: 05/18/2017
-- Description:	This SP is used to initiate the CPE workflow from Equip Review.
-- =============================================

ALTER PROCEDURE [dbo].[CpeEqRvwTskRule]
AS

BEGIN
BEGIN TRY
	SET NOCOUNT ON;

	DECLARE @EquipReviewOrdrs TABLE
		( ORDR_ID             int
		) 

	DECLARE @Cnt Int = 0
			,@Ctr Int = 0
			,@ORDR_ID Int
			
	INSERT INTO @EquipReviewOrdrs
	SELECT  Distinct  a.ORDR_ID FROM dbo.ACT_TASK a WITH (NOLOCK)
				inner join dbo.ORDR o WITH (NOLOCK) ON a.ORDR_ID = o.ORDR_ID
				WHERE (a.TASK_ID = 600 and a.STUS_ID = 0)
						AND a.CREAT_DT > DATEADD(mi,-60,getdate())
						AND ISNULL(o.CPE_CLLI,'') = ''
		
	select * from @EquipReviewOrdrs
	
	SET @Cnt = (SELECT COUNT(*) FROM @EquipReviewOrdrs)
	
	IF (@Cnt > 0)
		BEGIN
			WHILE (@Ctr < @Cnt)
				BEGIN
										
					SELECT TOP 1
						@ORDR_ID   = ORDR_ID
						
					FROM @EquipReviewOrdrs		
					
					IF ISNULL(@ORDR_ID,0) != 0
						BEGIN
							exec dbo.CpeWrkFlwTaskRule @ORDR_ID,600,0
						END
					
					DELETE @EquipReviewOrdrs where ORDR_ID   = @ORDR_ID

					SET @Ctr = @Ctr + 1
				END
			END
  
--EXEC [arch].[DELETEBatcEventOrderDomesticFSA] '2011-03-01'

END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH    
END
