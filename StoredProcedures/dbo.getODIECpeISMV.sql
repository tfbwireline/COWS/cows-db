USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getODIECpeISMV]    Script Date: 1/18/2022 9:56:13 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 12/05/2016
	-- Description:	Retrieves ODIE CPE InSide Move Transaction Data.
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getODIECpeISMV] 
			@REQ_ID int  

	AS
	BEGIN


	BEGIN TRY
	
	DECLARE @ORDR_ID int  
	DECLARE @DEVICE_ID varchar (25)
	
	SELECT @ORDR_ID = ORDR_ID FROM dbo.ODIE_REQ WHERE REQ_ID = @REQ_ID
	
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	-- Order Info.
	SELECT  DISTINCT			
			ISNULL(fsa.FTN,'')											AS OrderID,
			GETDATE()													AS StatusDate,
			DEVICE_ID													AS DeviceID,

			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.BLDG_NME) ELSE oa.BLDG_NME END,'') AS [Building],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.FLR_ID) ELSE oa.FLR_ID END,'') AS [Floor],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.RM_NBR) ELSE oa.RM_NBR END,'') AS [Room]
					
			
	FROM dbo.FSA_ORDR fsa WITH (NOLOCK)
	INNER JOIN dbo.ORDR	ord WITH (NOLOCK) ON ord.ORDR_ID = fsa.ORDR_ID
	Left JOIN	dbo.ORDR_ADR oa			WITH (NOLOCK)	ON	oa.ORDR_ID = fsa.ORDR_ID  
												AND	oa.CIS_LVL_TYPE in ('H6','H5')
												AND oa.ADR_TYPE_ID = 18
	LEFT JOIN dbo.CUST_SCRD_DATA csdA WITH (NOLOCK) ON csdA.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdA.SCRD_OBJ_TYPE_ID=14
	INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cpe ON cpe.ORDR_ID = fsa.ORDR_ID
	WHERE fsa.ORDR_ID = @ORDR_ID
		
	--Notes
	
	SELECT 
		ISNULL(NTE_TXT,'')                                              AS [NoteText]
	
	FROM dbo.ORDR_NTE WITH (NOLOCK)
	WHERE ORDR_ID = @ORDR_ID
		AND NTE_TYPE_ID = 6
	
	RETURN
			
	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH

	END
	




