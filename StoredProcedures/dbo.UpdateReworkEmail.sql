USE [COWS]
GO
_CreateObject 'SP','dbo','UpdateReworkEmail'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  jrg7298  
-- Create date: 09/01/2011  
-- Description: Inserts an Event Email Request to be send.  
-- =============================================  
ALTER PROCEDURE [dbo].[UpdateReworkEmail]   
 @EVENT_ID   Int  
 ,@STUS_ID   Int   
  
AS  
BEGIN  
DECLARE @OLD_STRT_TMST  DateTime  
DECLARE @OLD_TME_SLOT_ID TinyInt 
DECLARE @OLD_MDS_FAST_TRK_TYPE_ID CHAR(1) 
  
Begin Try  
 UPDATE  dbo.EMAIL_REQ WITH (ROWLOCK)  
  SET  STUS_ID    = @STUS_ID,  
    SENT_DT    =   CASE @STUS_ID   
           WHEN 11 THEN GETDATE()   
           ELSE NULL   
          END  
  WHERE EVENT_ID   = @EVENT_ID  
    AND EMAIL_REQ_TYPE_ID = 6  
    
 --UPDATE  dbo.MDS_EVENT_NEW WITH (ROWLOCK)  
 -- SET  EVENT_STUS_ID = 1,  
 --   WRKFLW_STUS_ID = 1,  
 --   MODFD_DT  = GETDATE()  
 --WHERE  EVENT_ID  = @EVENT_ID

UPDATE  dbo.MDS_EVENT WITH (ROWLOCK)  
  SET  EVENT_STUS_ID = 1,  
    WRKFLW_STUS_ID = 1,  
    MODFD_DT  = GETDATE()  
 WHERE  EVENT_ID  = @EVENT_ID 
   
 --SELECT  @OLD_STRT_TMST  = STRT_TMST  
 --   ,@OLD_TME_SLOT_ID = TME_SLOT_ID 
	--,@OLD_MDS_FAST_TRK_TYPE_ID = ISNULL(MDS_FAST_TRK_TYPE_ID, '')  
 -- FROM dbo.MDS_EVENT_NEW WITH (NOLOCK)  
 -- WHERE EVENT_ID = @EVENT_ID

SELECT  @OLD_STRT_TMST  = STRT_TMST  
    ,@OLD_TME_SLOT_ID = TME_SLOT_ID 
	,@OLD_MDS_FAST_TRK_TYPE_ID = ISNULL(MDS_FAST_TRK_TYPE_ID, '')  
  FROM dbo.MDS_EVENT WITH (NOLOCK)  
  WHERE EVENT_ID = @EVENT_ID  
 
 IF (ISNULL(@OLD_MDS_FAST_TRK_TYPE_ID, '') = 'S')
	BEGIN	
	 -- Releasing the activator & decrementing for FT events
	 EXEC dbo.deleteMDSSlotAvailibility  
	   0  
	   ,@EVENT_ID  
	   ,@OLD_STRT_TMST  
	   ,@OLD_TME_SLOT_ID  
   END

 -- Releasing the activator non-FT events			
 UPDATE dbo.EVENT_ASN_TO_USER WITH (ROWLOCK)  SET REC_STUS_ID = 0, MODFD_DT	= GETDATE() WHERE EVENT_ID = @EVENT_ID     
 DELETE FROM dbo.APPT WITH (ROWLOCK) WHERE EVENT_ID = @EVENT_ID
 
 --Notes added when event is returned to visible status
 Insert into dbo.EVENT_HIST  (EVENT_ID,ACTN_ID,CMNT_TXT,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)
 VALUES (@EVENT_ID,29,'This event has been returned to your queue for one of the following reasons: <br> 1. The stated requested implementation date has passed without initiation <br> 2. The event remained in rework within five days of the requested date <br> 3. MNS has marked this event On Hold, and it was not completed by the close of business on the requested implementation date. <br> MNS Activator: Please review this event and ensure all appropriate actions have been taken, <br> Submitter: Please select a valid date for work to be performed and resubmit.',1,1,GETDATE(),GETDATE())


End Try  
  
Begin Catch  
 EXEC [dbo].[insertErrorInfo]  
End Catch  
END  