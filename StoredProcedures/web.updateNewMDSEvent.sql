USE [COWS]
GO
_CreateObject 'SP','web','updateNewMDSEvent'
GO
/****** Object:  StoredProcedure [web].[updateNewMDSEvent]    Script Date: 08/10/2012 17:10:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================                            
-- Author:  Suman Chitemella                            
-- Create date: 2012-7-15                            
-- Description: This SP is used to update MDS Event Data              
        
/*****************        
New MDS Event        
*****************/                      
-- =========================================================                            
                            
ALTER PROCEDURE [web].[updateNewMDSEvent]                        
 @iEventID BIGINT,                           
 @EventData NVARCHAR(MAX),                          
 @DISCOE NVARCHAR(MAX) = NULL,                          
 @ManagedActivity NVARCHAR(MAX) = NULL,                          
 @MACActivity NVARCHAR(MAX) = NULL,            
 @LOC NVARCHAR(MAX) = NULL,                        
 @ID INT OUTPUT                           
AS                          
                        
BEGIN                        
 SET NOCOUNT ON;                        
OPEN SYMMETRIC KEY FS@K3y                       
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;                       
                         
  DECLARE @hDoc   INT                          
  DECLARE @EventID  BIGINT                                             
  DECLARE @RECSTUSID TINYINT                           
                        
  DECLARE @EVENT_STUS_ID TINYINT                          
  DECLARE @MDS_FAST_TRK_CD BIT   
  DECLARE @OLD_MDS_FAST_TRK_CD	BIT
  DECLARE @MDS_FAST_TRK_TYPE_ID VARCHAR(1)                          
                        
  DECLARE @FTN varchar(20)                          
  DECLARE @CHARS_ID VARCHAR(20)                          
  DECLARE @H1 dbo.H1                          
  DECLARE @H6 dbo.H6                          
  DECLARE @CUST_NME VARCHAR(100)                          
  DECLARE @CUST_EMAIL_ADR VARCHAR(100)                       
  DECLARE @ADR VARCHAR(200)                          
  DECLARE @FLR_BLDG_NME VARCHAR(10)                          
  DECLARE @CTY_NME VARCHAR(20)                          
  DECLARE @STT_PRVN_NME VARCHAR(20)                          
  DECLARE @ZIP_CD VARCHAR(10)                          
  DECLARE @CTRY_RGN_NME VARCHAR(50)                          
  DECLARE @US_TMZN_TXT VARCHAR(20)                          
  DECLARE @INSTL_SITE_POC_NME VARCHAR(50)                          
  DECLARE @INSTL_SITE_POC_PHN_NBR VARCHAR(12)                          
  DECLARE @INSTL_SITE_POC_CELL_PHN_NBR VARCHAR(12)                          
  DECLARE @SRVC_ASSRN_POC_NME VARCHAR(50)                          
  DECLARE @SRVC_ASSRN_POC_PHN_NBR VARCHAR(12)                          
  DECLARE @SRVC_ASSRN_POC_EMAIL_ADR VARCHAR(100)                          
  DECLARE @MDS_SRVC_TIER_ID TINYINT                          
  DECLARE @MDS_ACTY_TYPE_ID TINYINT                          
  DECLARE @MDS_INSTL_ACTY_TYPE_ID TINYINT                          
  DECLARE @FULL_CUST_DISC_CD BIT                          
  DECLARE @DEV_CNT INT                          
  DECLARE @FRWL_SCTY_PROD_CD BIT                          
  DECLARE @DSGN_DOC_LOC_TXT VARCHAR(200)                          
  DECLARE @RDSGN_NBR VARCHAR(50)                          
  --DECLARE @MDS_BRDG_NEED_CD BIT                          
  DECLARE @MDS_BRDG_NEED_ID TINYINT
  DECLARE @US_INTL_ID VARCHAR(1)                          
  DECLARE @SRVC_ASSRN_SITE_SUPP_ID TINYINT                          
  DECLARE @SPRINT_CPE_NCR_ID TINYINT                          
  DECLARE @DES_CMNT_TXT VARCHAR(1000)                          
  DECLARE @SHRT_DES VARCHAR(1000)                          
  DECLARE @CPE_DSPCH_EMAIL_ADR VARCHAR(200)                          
  DECLARE @CPE_DSPCH_CMNT_TXT VARCHAR(1000)                          
  DECLARE @WIRED_DEV_TRNSPRT_REQR_CD BIT                          
  DECLARE @WRLS_TRNSPRT_REQR_CD BIT                          
  DECLARE @MULTI_LINK_CKT_CD BIT                          
  DECLARE @VRTL_CNCTN_CD BIT                          
  DECLARE @REQOR_USER_ID INT                          
  DECLARE @PUB_EMAIL_CC_TXT VARCHAR(200)                          
  DECLARE @CMPLTD_EMAIL_CC_TXT VARCHAR(200)                          
  DECLARE @DISC_NTFY_PDL_NME VARCHAR(200)                 
  DECLARE @CUST_ACCT_TEAM_PDL_NME VARCHAR(200)                          
  DECLARE @ESCL_CD BIT                          
  DECLARE @PRIM_REQ_DT SMALLDATETIME                          
  DECLARE @SCNDY_REQ_DT SMALLDATETIME                          
  DECLARE @ESCL_REAS_ID TINYINT                          
  DECLARE @STRT_TMST SMALLDATETIME                          
  DECLARE @EXTRA_DRTN_TME_AMT VARCHAR(10)                          
  DECLARE @END_TMST SMALLDATETIME                          
  DECLARE @CREAT_BY_USER_ID INT                          
  DECLARE @CREAT_DT SMALLDATETIME                          
  DECLARE @EVENT_TITLE_TXT VARCHAR(255)                          
  DECLARE @CNFRC_BRDG_NBR VARCHAR(450)                          
  DECLARE @CNFRC_PIN_NBR VARCHAR(40)       
  DECLARE @ONLINE_MEETING_ADR VARCHAR(1000)                  
  DECLARE @TME_SLOT_ID TinyInt                       
  DECLARE @WRKFLW_STUS_ID TINYINT                
  DECLARE @FAIL_REAS_ID SMALLINT                      
  DECLARE @EVENT_DRTN_IN_MIN_QTY SMALLINT                      
  DECLARE @PRE_CFG_CMPLT_CD BIT                      
  DECLARE @ASN_USER_ID INT                      
  DECLARE @MNSPMID VARCHAR(10)           
  DECLARE @ODIE_CUST_ID VARCHAR(10)        
  DECLARE @OPT_OUT_REAS_TXT VARCHAR(250)                     
  DECLARE @BUS_JUSTN_TXT VARCHAR(250)        
  DECLARE @SCURD_CD BIT 
  DECLARE @OLD_SCURD_CD BIT
  DECLARE @CSG_LVL VARCHAR(5)  
  DECLARE @SPRS_EMAIL_ON_PUB_CD BIT   
  DECLARE @BUS_JUSTN_CMNT_TXT VARCHAR(1000)   
  DECLARE @IsReviewer BIT
  --DECLARE @FULFILLDT SMALLDATETIME
 DECLARE @SHIPPEDDT SMALLDATETIME       
 --DECLARE @SHIPCUSTNME VARCHAR(200)
 DECLARE @SHIPCUSTEMAIL VARCHAR(200)
 --DECLARE @SHIPCUSTPH VARCHAR(100)
 DECLARE @SHIPTRKNO VARCHAR(200)
 DECLARE @SHIPDEVSNBR VARCHAR(100)
 DECLARE @WOOBIPADR VARCHAR(100)
 DECLARE @MDS_BRDG_NEED_CD BIT
                        
  DECLARE @OLD_STRT_TMST DATETIME                      
  DECLARE @OLD_TME_SLOT_ID TinyInt        
  DECLARE @OLD_WRKFLW_STUS_ID TINYINT
  DECLARE @OLD_MDS_FAST_TRK_TYPE_ID CHAR(1)
                        
  SET @OLD_STRT_TMST = NULL                      
  SET @RECSTUSID = 1          
                                  
  DECLARE @MngdActivityTBL TABLE                      
  (RSPN_INFO_ID int,                       
   Selected BIT,                       
   RedesignNumber VARCHAR(25),                       
   ODIE_DEV_NME VARCHAR(255),          
   DEV_MODEL_ID VARCHAR(50) ,          
   MANF_ID VARCHAR(50),                       
   Fast_Track_Flag VARCHAR(255),                      
   Fast_Track_Flag_CD bit,                      
   Optout VARCHAR(255),                       
   Optout_CD VARCHAR(255),                       
   INTL_CTRY_CD VARCHAR(255),                       
   PHN_NBR VARCHAR(255),                       
   SRVC_ASSRN_SITE_SUPP_ID TinyInt,
   Firewall_Product_CD CHAR(1),
   WOOB_CD CHAR(1),           
   ManageActivityID VARCHAR(255)                       
  )                         
          
    DECLARE @MDS_EVENT_LOC TABLE            
     ( MDS_LOC_CAT_ID INT,          
        MDS_LOC_TYPE_ID INT,          
        MDS_LOC_NBR INT,          
        MDS_LOC_RAS_DT DATETIME,          
        CREAT_DT DATETIME)          
                         
 BEGIN TRY                        
                          
  IF (LEN(@EventData) > 10)                        
  BEGIN                        
                          
   EXEC sp_xml_preparedocument @hDoc OUTPUT, @EventData                        
         
   SELECT @EVENT_TITLE_TXT = med.EVENT_TITLE_TXT,                 
 @EVENT_STUS_ID = EVENT_STUS_ID,                  
 @MDS_FAST_TRK_CD =  med.MDS_FAST_TRK_CD,                 
 @MDS_FAST_TRK_TYPE_ID = CASE COALESCE(MDS_FAST_TRK_TYPE_ID, '') WHEN '' THEN NULL ELSE MDS_FAST_TRK_TYPE_ID END,                      
 @H1 = med.H1,                      
 @CHARS_ID = med.CHARS_ID,                      
 @CUST_NME = med.CUST_NME,                      
 @CUST_EMAIL_ADR = med.CUST_EMAIL_ADR,                   
 @DSGN_DOC_LOC_TXT= med.DSGN_DOC_LOC_TXT,                      
 @SHRT_DES = med.SHRT_DES,                   
 @MDS_ACTY_TYPE_ID = CONVERT(TINYINT,med.MDS_ACTY_TYPE_ID),                      
 @MDS_INSTL_ACTY_TYPE_ID = CONVERT(TINYINT,med.MDS_INSTL_ACTY_TYPE_ID),                      
 @FULL_CUST_DISC_CD = CONVERT(BIT,med.FULL_CUST_DISC_CD),                      
 @DEV_CNT = CONVERT(INT,med.DEV_CNT),                      
 @MDS_BRDG_NEED_ID = CONVERT(TINYINT,med.MDS_BRDG_NEED_ID),                      
 @STRT_TMST = CONVERT(SMALLDATETIME, med.STRT_TMST, 101),                      
 @EXTRA_DRTN_TME_AMT = CONVERT(SMALLINT, med.EXTRA_DRTN_TME_AMT),                      
 @END_TMST = CONVERT(SMALLDATETIME, med.END_TMST, 101),     
 @CREAT_BY_USER_ID = CONVERT(INT, med.CREAT_BY_USER_ID),                      
 @CREAT_DT = CONVERT(SMALLDATETIME, med.CREAT_DT, 101),                      
 @CNFRC_BRDG_NBR = med.CNFRC_BRDG_NBR,                      
 @CNFRC_PIN_NBR = med.CNFRC_PIN_NBR,    
 @ONLINE_MEETING_ADR = med.ONLINE_MEETING_ADR,                
 @TME_SLOT_ID = CASE med.TME_SLOT_ID WHEN 0 THEN NULL ELSE med.TME_SLOT_ID END,                
 @PUB_EMAIL_CC_TXT = REPLACE(med.PUB_EMAIL_CC_TXT, ';', ','),                
 @CMPLTD_EMAIL_CC_TXT = REPLACE(med.CMPLTD_EMAIL_CC_TXT, ';', ','),                
 @WRKFLW_STUS_ID = CASE med.WRKFLW_STUS_ID WHEN 0 THEN 1 ELSE med.WRKFLW_STUS_ID END,                
 @FAIL_REAS_ID = CASE WHEN med.FAIL_REAS_ID = 0 THEN NULL WHEN med.FAIL_REAS_ID < 0 THEN NULL ELSE med.FAIL_REAS_ID END,                
 @ESCL_REAS_ID = CASE WHEN med.ESCL_REAS_ID = 0 THEN NULL WHEN med.ESCL_REAS_ID < 0 THEN NULL ELSE med.ESCL_REAS_ID END,                
 @ESCL_CD = CONVERT(BIT, med.ESCL_CD),                
 @PRIM_REQ_DT = med.PRIM_REQ_DT,                
 @SCNDY_REQ_DT = med.SCNDY_REQ_DT,                
 @REQOR_USER_ID = med.REQOR_USER_ID,                
 @CUST_ACCT_TEAM_PDL_NME  =  med.CUST_ACCT_TEAM_PDL_NME,                
 @EVENT_DRTN_IN_MIN_QTY = CASE med.EVENT_DRTN_IN_MIN_QTY WHEN NULL THEN 0 ELSE med.EVENT_DRTN_IN_MIN_QTY END,                
 @PRE_CFG_CMPLT_CD = med.PRE_CFG_CMPLT_CD,                
 @SCURD_CD = med.SCURD_CD,    
 @OLD_SCURD_CD = med.OLD_SCURD_CD,         
 @CSG_LVL = med.CSG_LVL_CD,                
 @DISC_NTFY_PDL_NME = med.DISC_NTFY_PDL_NME,              
 @MNSPMID = med.MNS_PM_ID,            
 @ODIE_CUST_ID = med.ODIE_CUST_ID,          
 @BUS_JUSTN_TXT = med.BUS_JUSTN_TXT,          
 @OPT_OUT_REAS_TXT = med.OPT_OUT_REAS_TXT,        
 @SPRINT_CPE_NCR_ID = med.SPRINT_CPE_NCR_ID,        
 @CPE_DSPCH_EMAIL_ADR = med.CPE_DSPCH_EMAIL_ADR,        
 @CPE_DSPCH_CMNT_TXT = med.CPE_DSPCH_CMNT_TXT,
 @SPRS_EMAIL_ON_PUB_CD = med.SPRS_EMAIL_ON_PUB_CD ,
 @BUS_JUSTN_CMNT_TXT = med.BUS_JUSTN_CMNT_TXT,
 @IsReviewer = med.IsReviewer,
 @OLD_WRKFLW_STUS_ID = med.OldWFStatus,
 --@FULFILLDT = med.FULFILLED_DT,
 @SHIPPEDDT = med.SHIPPED_DT,
 --@SHIPCUSTNME = med.SHIP_CUST_NME,
 @SHIPCUSTEMAIL = med.SHIP_CUST_EMAIL_ADR,
 --@SHIPCUSTPH = med.SHIP_CUST_PHN_NBR,
 @SHIPDEVSNBR = med.DEV_SERIAL_NBR,
 @SHIPTRKNO = med.SHIP_TRK_REFR_NBR,

 @WOOBIPADR = med.WOOB_IP_ADR,
 @MDS_BRDG_NEED_CD = med.MDS_BRDG_NEED_CD
 FROM OPENXML (@hDoc, '/MDSEvent', 2)                      
 WITH (EVENT_TITLE_TXT VARCHAR(255),                      
  EVENT_STUS_ID TINYINT,                  
  MDS_FAST_TRK_CD VARCHAR(1) ,                      
  MDS_FAST_TRK_TYPE_ID VARCHAR(1) ,                      
  H1 VARCHAR(9) ,                      
  CHARS_ID VARCHAR(10) ,                      
  CUST_NME VARCHAR(100) ,                      
  CUST_EMAIL_ADR VARCHAR(100) ,                      
  DSGN_DOC_LOC_TXT VARCHAR(200) ,                      
  SHRT_DES VARCHAR(1000) ,                      
  MDS_ACTY_TYPE_ID VARCHAR(3) ,                      
  MDS_INSTL_ACTY_TYPE_ID VARCHAR(3) ,                      
  FULL_CUST_DISC_CD VARCHAR(1) ,                      
  DEV_CNT VARCHAR(20) ,                      
  MDS_BRDG_NEED_ID TINYINT ,                      
  STRT_TMST VARCHAR(50),                      
  EXTRA_DRTN_TME_AMT VARCHAR(10),                      
  END_TMST VARCHAR(50),                      
  CREAT_BY_USER_ID VARCHAR(20),                      
  CREAT_DT VARCHAR(50),                      
  CNFRC_BRDG_NBR VARCHAR(450),                      
  CNFRC_PIN_NBR VARCHAR(40),
  ONLINE_MEETING_ADR VARCHAR(1000),
  TME_SLOT_ID TINYINT,                
  PUB_EMAIL_CC_TXT VARCHAR(200),                
  CMPLTD_EMAIL_CC_TXT VARCHAR(200),                
  WRKFLW_STUS_ID TINYINT,                    
  FAIL_REAS_ID  SMALLINT,                
  ESCL_REAS_ID TINYINT,                
  ESCL_CD BIT,                
  PRIM_REQ_DT SMALLDATETIME,                
  SCNDY_REQ_DT SMALLDATETIME,                
  REQOR_USER_ID INT,                
  CUST_ACCT_TEAM_PDL_NME VARCHAR(200),                
  EVENT_DRTN_IN_MIN_QTY SMALLINT,                
 PRE_CFG_CMPLT_CD BIT,                
  SCURD_CD BIT,
  OLD_SCURD_CD BIT,                
  CSG_LVL_CD VARCHAR(5),                
  DISC_NTFY_PDL_NME VARCHAR(200),              
  MNS_PM_ID VARCHAR(10),            
  ODIE_CUST_ID VARCHAR(10),          
  BUS_JUSTN_TXT VARCHAR(1000),          
  OPT_OUT_REAS_TXT VARCHAR(250),        
  SPRINT_CPE_NCR_ID TINYINT,        
  CPE_DSPCH_EMAIL_ADR VARCHAR(200),        
  CPE_DSPCH_CMNT_TXT VARCHAR(1000),        
  SPRS_EMAIL_ON_PUB_CD BIT,
  BUS_JUSTN_CMNT_TXT VARCHAR(1000),
  IsReviewer BIT,
  OldWFStatus TINYINT,
  --FULFILLED_DT SMALLDATETIME 'FULFILLED_DT/text()',
  SHIPPED_DT SMALLDATETIME 'SHIPPED_DT/text()',
  --SHIP_CUST_NME VARCHAR(200),
  SHIP_CUST_EMAIL_ADR VARCHAR(200),
  --SHIP_CUST_PHN_NBR VARCHAR(100),
  DEV_SERIAL_NBR VARCHAR(100),
  SHIP_TRK_REFR_NBR VARCHAR(200),

  WOOB_IP_ADR VARCHAR(100),
  MDS_BRDG_NEED_CD BIT
 ) med        
     
               
     SELECT @OLD_STRT_TMST = STRT_TMST, @OLD_TME_SLOT_ID = ISNULL(TME_SLOT_ID, 0), @OLD_MDS_FAST_TRK_TYPE_ID = ISNULL(MDS_FAST_TRK_TYPE_ID, ''), @OLD_MDS_FAST_TRK_CD = ISNULL(MDS_FAST_TRK_CD, 0)  FROM dbo.MDS_EVENT_NEW WITH (NOLOCK) WHERE (EVENT_ID = @iEventID)
             
     IF (((@WRKFLW_STUS_ID=2) AND (@EVENT_STUS_ID=1)) OR ((@OLD_WRKFLW_STUS_ID = 9) AND (@EVENT_STUS_ID=3) AND (@WRKFLW_STUS_ID=2)))
		BEGIN
			SET @OLD_STRT_TMST = NULL
			SET @OLD_TME_SLOT_ID = 0
		END

UPDATE dbo.MDS_EVENT_NEW WITH (ROWLOCK)        
SET                       
  MDS_FAST_TRK_CD = @MDS_FAST_TRK_CD,                        
 MDS_FAST_TRK_TYPE_ID =@MDS_FAST_TRK_TYPE_ID,                      
 CHARS_ID = @CHARS_ID,                       
 H1_ID = @H1,                            
 CUST_NME = dbo.encryptString(@CUST_NME),                   
 CUST_EMAIL_ADR = dbo.encryptString(@CUST_EMAIL_ADR),                      
 MDS_ACTY_TYPE_ID = @MDS_ACTY_TYPE_ID,                        
 FULL_CUST_DISC_CD = @FULL_CUST_DISC_CD,                       
 DEV_CNT = @DEV_CNT,                        
 DSGN_DOC_LOC_TXT =@DSGN_DOC_LOC_TXT,                      
 SHRT_DES = @SHRT_DES,                      
 MDS_BRDG_NEED_ID = @MDS_BRDG_NEED_ID,                        
 STRT_TMST = @STRT_TMST,                   
 EXTRA_DRTN_TME_AMT = @EXTRA_DRTN_TME_AMT,                      
 END_TMST = @END_TMST,                       
 --CREAT_BY_USER_ID,                      
 --CREAT_DT,                        
 EVENT_TITLE_TXT = dbo.encryptString(@EVENT_TITLE_TXT),                      
 CNFRC_BRDG_NBR = @CNFRC_BRDG_NBR,                        
 CNFRC_PIN_NBR = @CNFRC_PIN_NBR,   
 ONLINE_MEETING_ADR = @ONLINE_MEETING_ADR,
 --DES_CMNT_TXT,                    
 TME_SLOT_ID = @TME_SLOT_ID,                
 PUB_EMAIL_CC_TXT = @PUB_EMAIL_CC_TXT,                
 CMPLTD_EMAIL_CC_TXT = @CMPLTD_EMAIL_CC_TXT,                
 --WRKFLW_STUS_ID = @WRKFLW_STUS_ID,                 
 FAIL_REAS_ID = @FAIL_REAS_ID,                
 ESCL_REAS_ID = @ESCL_REAS_ID,                
 ESCL_CD = @ESCL_CD,                
 PRIM_REQ_DT = @PRIM_REQ_DT,                
 SCNDY_REQ_DT = @SCNDY_REQ_DT,                
 REQOR_USER_ID = @REQOR_USER_ID,                
 CUST_ACCT_TEAM_PDL_NME = @CUST_ACCT_TEAM_PDL_NME,          
 PRE_CFG_CMPLT_CD = @PRE_CFG_CMPLT_CD,              
 EVENT_DRTN_IN_MIN_QTY =@EVENT_DRTN_IN_MIN_QTY,                
 --SCURD_CD = @SCURD_CD,                
 CSG_LVL_CD = @CSG_LVL,                
 DISC_NTFY_PDL_NME = @DISC_NTFY_PDL_NME,              
 MNS_PM_ID = @MNSPMID,            
 ODIE_CUST_ID = @ODIE_CUST_ID,          
 BUS_JUSTN_TXT = @BUS_JUSTN_TXT,               
 OPT_OUT_REAS_TXT = @OPT_OUT_REAS_TXT,        
 SPRINT_CPE_NCR_ID = @SPRINT_CPE_NCR_ID,        
  CPE_DSPCH_EMAIL_ADR = @CPE_DSPCH_EMAIL_ADR ,        
  CPE_DSPCH_CMNT_TXT = @CPE_DSPCH_CMNT_TXT,
  SPRS_EMAIL_ON_PUB_CD = @SPRS_EMAIL_ON_PUB_CD,
  BUS_JUSTN_CMNT_TXT = @BUS_JUSTN_CMNT_TXT,
  --FULFILLED_DT= @FULFILLDT,
 SHIPPED_DT = @SHIPPEDDT,
 --SHIP_CUST_NME = @SHIPCUSTNME,
 SHIP_CUST_EMAIL_ADR = @SHIPCUSTEMAIL,
 --SHIP_CUST_PHN_NBR = @SHIPCUSTPH,
 DEV_SERIAL_NBR = @SHIPDEVSNBR,
 SHIP_TRK_REFR_NBR = @SHIPTRKNO,

 WOOB_IP_ADR = @WOOBIPADR,
 MDS_BRDG_NEED_CD = @MDS_BRDG_NEED_CD
 WHERE  EVENT_ID = @iEventID                       
    SET @ID = @iEventID      

IF (@SCURD_CD != @OLD_SCURD_CD)
BEGIN
UPDATE dbo.[EVENT] WITH (ROWLOCK)
SET SCURD_CD = @SCURD_CD
WHERE EVENT_ID = @iEventID
END

IF ((@MDS_FAST_TRK_CD = 0) AND (@OLD_MDS_FAST_TRK_CD = 1))
BEGIN
	EXEC [dbo].[deleteMDSSlotAvailibility]
		0,
		@iEventID,
		@OLD_STRT_TMST,
		@OLD_TME_SLOT_ID
END
                             
IF ((ISNULL(@MDS_FAST_TRK_TYPE_ID, '') = 'A') AND (@MDS_FAST_TRK_CD = 1))
BEGIN
	IF ((ISNULL(@OLD_MDS_FAST_TRK_TYPE_ID, '') = 'S') AND (@MDS_FAST_TRK_CD = 1))
	BEGIN
		EXEC [dbo].[deleteMDSSlotAvailibility]
		0,
		@iEventID,
		@OLD_STRT_TMST,
		@OLD_TME_SLOT_ID
	END

	DELETE FROM dbo.APPT WITH (ROWLOCK) WHERE [EVENT_ID] = @iEventID
	INSERT INTO dbo.APPT WITH (ROWLOCK)    
		   ([EVENT_ID]    
		   ,[SUBJ_TXT]    
		   ,[DES]    
		   ,[STRT_TMST]    
		   ,[END_TMST]    
		   ,[APPT_LOC_TXT]    
		   ,[APPT_TYPE_ID]    
		   ,[RCURNC_DES_TXT]    
		   ,[CREAT_BY_USER_ID]    
		   ,[CREAT_DT]    
		   ,[ASN_TO_USER_ID_LIST_TXT]    
		   ,[RCURNC_CD]
		   ,[REC_STUS_ID])     
		SELECT  @iEventID    
		   ,@EVENT_TITLE_TXT    
		   ,@EVENT_TITLE_TXT 
		   ,(CONVERT(DATE, @STRT_TMST, 101) + convert(datetime,'00:00:00'))
		   ,(CONVERT(DATE, @STRT_TMST, 101) + convert(datetime,'01:00:00'))
		   ,@CNFRC_BRDG_NBR + ' - ' + @CNFRC_PIN_NBR    
		   ,5    
		   ,NULL    
		   ,@CREAT_BY_USER_ID    
		   ,getDate()    
		   ,'<ResourceIds>  <ResourceId Type="System.Int32" Value="5001" />  </ResourceIds>'    
		   ,0 
		   ,1
END

                          
IF (ISNULL(@TME_SLOT_ID, 0) != 0)  AND (@MDS_FAST_TRK_CD = 1) AND  (ISNULL(@MDS_FAST_TRK_TYPE_ID, '') != '') AND (ISNULL(@MDS_FAST_TRK_TYPE_ID, '') != 'A') AND ((@WRKFLW_STUS_ID IN (2, 4)) OR ((@WRKFLW_STUS_ID = 3) AND (@EVENT_STUS_ID IN (2,4))) OR ((@WRKFLW_STUS_ID = 8) AND (@EVENT_STUS_ID IN (2,3,4))) OR ((@WRKFLW_STUS_ID = 5) AND (@EVENT_STUS_ID = 5)))      
  BEGIN                      
   -- Randomise()                    
   SELECT TOP 1 @ASN_USER_ID = [USER_ID]                    
   FROM dbo.EVENT_AVAL_USER WITH (NOLOCK)                    
   WHERE TME_SLOT_ID = @TME_SLOT_ID                    
   ORDER BY NEWID()                    
                    
   EXEC [dbo].[updateMDSSlotAvailibility]                     
     @TME_SLOT_ID                    
     ,@STRT_TMST                    
     ,@ASN_USER_ID                    
     ,@CREAT_BY_USER_ID                    
     ,@EVENT_TITLE_TXT                    
     ,@CNFRC_BRDG_NBR                    
     ,@CNFRC_PIN_NBR                    
     ,@iEventID                    
     ,1                    
     ,@OLD_STRT_TMST                    
     ,@OLD_TME_SLOT_ID
	 --,@IsReviewer 
  END                 
                  
 EXEC sp_xml_removedocument @hDoc                      
                         
 IF (LEN(@ManagedActivity) > 10)                            
    BEGIN                            
               
 SET @ManagedActivity = REPLACE (@ManagedActivity, 'NewDataSet', 'DocumentElement')               
                                 
    EXEC sp_xml_preparedocument @hDoc OUTPUT, @ManagedActivity                            
    --DELETE FROM #ManagedActivity                             
                                 
    INSERT INTO @MngdActivityTBL                      
 SELECT RSPN_INFO_ID, Selected, RedesignNumber, ODIE_DEV_NME, DEV_MODEL_ID, MANF_ID, Fast_Track_Flag, Fast_Track_Flag_CD, Optout, Optout_CD,           
    INTL_CTRY_CD, PHN_NBR, SRVC_ASSRN_SITE_SUPP_ID, Firewall_Product_CD, WOOB_CD, ManagedActivityId          
 FROM OPENXML (@hDoc, '/DocumentElement/ManagedActivity', 2)                        
 WITH (RSPN_INFO_ID VARCHAR(255),                       
   Selected BIT,                       
   RedesignNumber VARCHAR(255),                       
   ODIE_DEV_NME VARCHAR(255),              
   DEV_MODEL_ID VARCHAR(50) ,          
   MANF_ID VARCHAR(50),          
   Fast_Track_Flag VARCHAR(255),                      
   Fast_Track_Flag_CD BIT,                      
   Optout VARCHAR(255),                     
   Optout_CD VARCHAR(255),                      
   INTL_CTRY_CD VARCHAR(255),                       
   PHN_NBR VARCHAR(255),                       
   SRVC_ASSRN_SITE_SUPP_ID TinyInt,
   Firewall_Product_CD CHAR(1),
   WOOB_CD CHAR(1),                        
   ManagedActivityID VARCHAR(255) )               
        
DELETE FROM dbo.MDS_MNGD_ACT_NEW WITH (ROWLOCK) WHERE EVENT_ID = @iEventID                
        
 INSERT INTO MDS_MNGD_ACT_NEW            
   (REQ_ID, DEV_MODEL_ID, MANF_ID, ODIE_DEV_NME, RDSN_NBR, FAST_TRK_CD,  RSPN_INFO_DT, SLCTD_CD, CREAT_DT,             
  OPT_OUT_CD,  INTL_CTRY_CD, PHN_NBR, SRVC_ASSRN_SITE_SUPP_ID, FRWL_PROD_CD, WOOB_CD, EVENT_ID)            
   SELECT NULL, DEV_MODEL_ID,MANF_ID, ODIE_DEV_NME, RedesignNumber, ISNULL(FAST_TRACK_FLAG_CD, 0), getdate(), Selected,  getdate()          
  ,CASE ma.Optout_CD WHEN 'true' THEN 'Y' WHEN 'false' THEN 'N' ELSE 'N' END, INTL_CTRY_CD, PHN_NBR, SRVC_ASSRN_SITE_SUPP_ID ,Firewall_Product_CD, WOOB_CD, @iEventID                   
  FROM @MngdActivityTBL ma                      
  WHERE ma.Selected = 1              
                       
    --DELETE FROM @ManagedActivity                    
    EXEC sp_xml_removedocument @hDoc                              
   END --End IF (LEN(@ManagedActivity) > 10)                       
                      
   IF (LEN(@DISCOE) > 10)                            
   BEGIN                      
    DELETE FROM dbo.MDS_EVENT_DISCO WITH (ROWLOCK) WHERE EVENT_ID = @iEventID                      
                          
    EXEC sp_xml_preparedocument @hDoc OUTPUT, @DISCOE                    
           
 INSERT INTO MDS_EVENT_DISCO (EVENT_ID, H5_H6_CUST_ID, ODIE_DEV_NME, SERIAL_NBR, MODEL_NME, VNDR_NME)                      
 SELECT @iEventID, DISC_H5_H6 AS H5_H6_CUST_ID, ODIE_DEV_NME, SERIAL_NBR, MODEL_NME, VNDR_NME                 
 FROM OPENXML (@hDoc, '/DocumentElement/DISCOE', 2)                      
 WITH (DISC_H5_H6 VARCHAR(10) 'DISC_H5_H6[not(@nil = "true")]',                                            
    --DEV_MODEL_NME VARCHAR(100),                   
    ODIE_DEV_NME VARCHAR(100),                      
    SERIAL_NBR VARCHAR(50),        
    MODEL_NME VARCHAR(150),                
    VNDR_NME VARCHAR(150)
    --INTL_CTRY_CD VARCHAR(25),
    --PHN_NBR VARCHAR(20)          
    )                  
              
   EXEC sp_xml_removedocument @hDoc                              
   END --IF LEN(@DISCOE) > 10)                         
                         
   IF(LEN(@MACActivity) > 10)                      
    BEGIN                      
    EXEC sp_xml_preparedocument @hDoc OUTPUT, @MACActivity                       
                          
    DELETE FROM dbo.MDS_EVENT_MAC_ACTY WITH (ROWLOCK) WHERE EVENT_ID = @iEventID                      
                          
    INSERT INTO dbo.MDS_EVENT_MAC_ACTY                      
    (EVENT_ID,                      
     MDS_MAC_ACTY_ID,                      
   CREAT_DT)                      
     SELECT @iEventID,                      
   CONVERT(INT, MDS_MAC_ACTY_ID),                      
      GETDATE()                      
    FROM OPENXML (@hDoc, '/DocumentElement/MACActivity', 2)                       
      WITH (MDS_MAC_ACTY_ID VARCHAR(20))                      
                            
    INSERT INTO dbo.MDS_EVENT_MAC_ACTY                      
    (EVENT_ID,                      
     MDS_MAC_ACTY_ID,                      
     CREAT_DT)                      
     SELECT @iEventID,                      
      CONVERT(INT, MDS_MAC_ACTY_ID),                      
      GETDATE()                      
    FROM OPENXML (@hDoc, '/NewDataSet/MACActivity', 2)                       
      WITH (MDS_MAC_ACTY_ID VARCHAR(20))                      
                          
    EXEC sp_xml_removedocument @hDoc                      
    END          
            
     --CPE: LOC          
  IF (LEN(@LOC) > 10)           
  BEGIN          
     EXEC sp_xml_preparedocument @hDoc OUTPUT, @LOC          
              
     INSERT INTO @MDS_EVENT_LOC          
     SELECT  MDS_LOC_CAT_ID, MDS_LOC_TYPE_ID,MDS_LOC_NBR, MDS_LOC_RAS_DT, GETDATE()          
      FROM OPENXML (@hDoc, '/DocumentElement/LOC', 2)          
      WITH (          
           MDS_LOC_CAT_ID INT,          
           MDS_LOC_TYPE_ID INT,          
              MDS_LOC_NBR INT,          
              MDS_LOC_RAS_DT DATETIME          
            )          
     EXEC sp_xml_removedocument @hDoc          
             
 DELETE FROM dbo.MDS_EVENT_LOC WITH (ROWLOCK) WHERE EVENT_ID = @iEventID        
         
        INSERT INTO MDS_EVENT_LOC (EVENT_ID,          
           MDS_LOC_CAT_ID,          
           MDS_LOC_TYPE_ID,          
           MDS_LOC_NBR,          
           MDS_LOC_RAS_DT,          
        CREAT_DT)          
      SELECT @iEventID,          
   MDS_LOC_CAT_ID,          
   MDS_LOC_TYPE_ID,          
   MDS_LOC_NBR,          
            MDS_LOC_RAS_DT,          
   CREAT_DT           
      FROM @MDS_EVENT_LOC          
  END -- End IF(LEN@(LOC))                         
                
    IF @EVENT_STUS_ID = 8            
  BEGIN            
   DELETE FROM dbo.APPT WITH (ROWLOCK) WHERE EVENT_ID = @iEventID            
  END                      
                            
  END --IF LEN(@EventData) > 10)                        
                          
 END TRY                          
 BEGIN CATCH                          
  EXEC [dbo].[insertErrorInfo]                            
  DECLARE @ErrMsg nVarchar(4000),                             
    @ErrSeverity Int                                  
  SELECT @ErrMsg  = ERROR_MESSAGE(),                                  
         @ErrSeverity = ERROR_SEVERITY()                                  
  RAISERROR(@ErrMsg, @ErrSeverity, 1)                                  
 END CATCH                            
END 