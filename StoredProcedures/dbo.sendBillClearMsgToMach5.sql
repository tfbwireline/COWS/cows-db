USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[sendBillClearMsgToMach5]    Script Date: 07/22/2021 10:22:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <06/20/2016>
-- Description:	<send bill clear message to Mach5>
-- =============================================

--Exec dbo.sendBillClearMsgToMach5 13666,207,2,''

ALTER PROCEDURE [dbo].[sendBillClearMsgToMach5] 
	@ORDR_ID INT,
	@TaskId		SMALLINT = 0,
	@TaskStatus	TINYINT = 0,
	@Comments	Varchar(100) 
	
AS
BEGIN
	SET NOCOUNT ON;

    BEGIN TRY
		IF EXISTS
			(
				SELECT 'X'
					FROM dbo.ORDR odr WITH (NOLOCK)
				INNER JOIN dbo.FSA_ORDR fsa WITH (NOLOCK) ON odr.ORDR_ID = fsa.ORDR_ID
					WHERE odr.ORDR_ID = @ORDR_ID
						AND odr.ORDR_CAT_ID = 6
						-- Moved below in Jun21 release per Lajan       AND fsa.PROD_TYPE_CD IN ('MO','DO','SO', -- OFFNet Access
						-- Moved below in Jun21 release per Lajan 	  'MP','MN','DN','SN') -- ONNET added 3/22/17 10 a.m.			
						AND EXISTS
							(
								SELECT 'X'
									FROM dbo.ACT_TASK at WITH (NOLOCK)
									WHERE at.ORDR_ID = odr.ORDR_ID
										AND at.TASK_ID = 207
										--AND at.STUS_ID = 2
							)
						AND NOT EXISTS
							(
								SELECT 'X'
									FROM dbo.M5_ORDR_MSG WITH (NOLOCK)
									WHERE ORDR_ID = @ORDR_ID
										AND M5_MSG_ID = 6
							)
			)
			BEGIN

				IF EXISTS (SELECT 'X' FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE @ORDR_ID = ORDR_ID AND ORDR_SUB_TYPE_CD NOT IN ('AR','AN','ISMV'))
					AND EXISTS (SELECT 'X' FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE @ORDR_ID = ORDR_ID AND PROD_TYPE_CD IN ('MO','DO','SO'))
					BEGIN
						INSERT INTO dbo.M5_ORDR_MSG (ORDR_ID,M5_MSG_ID,STUS_ID,CREAT_DT)
						VALUES (@ORDR_ID,6,10,GETDATE())
				
						INSERT INTO dbo.ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
						VALUES (9,@ORDR_ID,GETDATE(),1,1,'Bill Clear message inserted into Mach5 Interface table.')
					END

				ELSE IF EXISTS (SELECT 'X' FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE @ORDR_ID = ORDR_ID AND ORDR_SUB_TYPE_CD = 'ISMV')
					BEGIN
						INSERT INTO dbo.M5_ORDR_MSG (ORDR_ID,M5_MSG_ID,STUS_ID,CREAT_DT)
						VALUES (@ORDR_ID,7,10,GETDATE())
				
						INSERT INTO dbo.ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
						VALUES (9,@ORDR_ID,GETDATE(),1,1,'ISMV Bill Clear message inserted into Mach5 Interface table.')
					END
				ELSE IF EXISTS (SELECT 'X' FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE @ORDR_ID = ORDR_ID AND ORDR_SUB_TYPE_CD IN ('AR','AN'))
					BEGIN
						DECLARE @BILL_DATE DATETIME = (SELECT ORDR_BILL_CLEAR_INSTL_DT FROM dbo.ORDR_MS WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID)
						
						INSERT INTO dbo.M5_BILL_CLEAR (ORDR_ID, BILL_CLEAR_DT, CREAT_DT,SENT_DT,STUS_ID)
							VALUES (@ORDR_ID,@BILL_DATE,GETDATE(),NULL,1)
							
						INSERT INTO dbo.ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
						VALUES (9,@ORDR_ID,GETDATE(),1,1,'Message inserted into M5 Bill Clear table holding until Bill Clear date entered by user.')				

					END
				-- Added in Jun21 Release to stop complete messages supposed to come from NRM.
				 ELSE IF EXISTS (SELECT 'X' FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE @ORDR_ID = ORDR_ID AND ORDR_SUB_TYPE_CD NOT IN ('AR','AN','ISMV'))
						AND EXISTS (SELECT 'X' FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE @ORDR_ID = ORDR_ID AND PROD_TYPE_CD IN ('MP','MN','DN','SN'))
					BEGIN
						INSERT INTO dbo.ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
						VALUES (9,@ORDR_ID,GETDATE(),1,1,'OnNet order, no complete message sent to M5.  OnNet complete to come from NRM.')				
					END

			END


    END TRY
    BEGIN CATCH
		EXEC dbo.insertErrorInfo
    END CATCH
END