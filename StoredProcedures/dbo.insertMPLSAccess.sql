USE [COWS]
GO
_CreateObject 'SP','dbo','insertMPLSAccess'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 03/05/2012
-- Description:	Inserts a record into the MPLS_ACCS Table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertMPLSAccess]
	@SIP_TRNK_GRP_ID	Int
	,@CKT_ID			Varchar(15)
	,@NW_USER_ADR		Varchar(15)
AS
BEGIN
SET NOCOUNT ON;
	INSERT INTO	dbo.MPLS_ACCS	WITH (ROWLOCK)
				(SIP_TRNK_GRP_ID
				,CKT_ID
				,NW_USER_ADR)	
		VALUES	(@SIP_TRNK_GRP_ID
				,@CKT_ID
				,@NW_USER_ADR)		
END
GO