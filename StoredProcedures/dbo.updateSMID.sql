USE COWS
GO
_CreateObject 'SP','dbo','updateSMID'
GO
SET ANSI_NULLS ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <08/17/2011>
-- Description:	<To Update IPL Install SMID From 
--	INTL IPL Install w xNCI --> INTL IPL Install w xNCI Including Milestones>
--  INTL IPL Install w/o xNCI --> INTL IPL Install w xNCI bypass Milestones>
-- =============================================

ALTER PROCEDURE dbo.updateSMID
@OrderID	Int,
@TaskID		SmallInt,
@TaskStatus	TinyInt,
@Comments	Varchar(1000) = NULL	
AS
BEGIN
	BEGIN TRY
		DECLARE @CatID			INT
		DECLARE @OrderTypeID		TinyInt
		DECLARE @ProductTypeID		TinyInt
		DECLARE @PPRT_ID			INT

		IF @TaskStatus = 2
		BEGIN
			SELECT 
				@CatID = ORDR_CAT_ID
			FROM	dbo.ORDR WITH (NOLOCK)
			WHERE	ORDR_ID	=	@OrderID
	
			IF @CatID IN (1,4,5)
				BEGIN
					SELECT	@OrderTypeID	=	ISNULL(ipl.ORDR_TYPE_ID,ncco.ORDR_TYPE_ID),
							@ProductTypeID	=	ISNULL(ipl.PROD_TYPE_ID,ncco.PROD_TYPE_ID)
						FROM	dbo.ORDR		odr WITH (NOLOCK)
					LEFT JOIN	dbo.IPL_ORDR	ipl WITH (NOLOCK)	ON	ipl.ORDR_ID = odr.ORDR_ID
					LEFT JOIN	dbo.NCCO_ORDR	ncco WITH (NOLOCK)	ON 	ncco.ORDR_ID = odr.ORDR_ID
						WHERE	odr.ORDR_ID	=	@OrderID
			
					IF ((SELECT ORDR_TYPE_ID FROM dbo.LK_ORDR_TYPE WITH (NOLOCK) WHERE ORDR_TYPE_DES = 'Cancel') <> @OrderTypeID) 
						BEGIN
							SELECT	@PPRT_ID	=	PPRT_ID
								FROM	dbo.LK_PPRT	WITH (NOLOCK)
								WHERE	ORDR_TYPE_ID	=	@OrderTypeID
									AND	PROD_TYPE_ID	=	@ProductTypeID
									AND	PPRT_NME		LIKE	CASE	@TaskID
																WHEN  102	THEN '%w/oxNCI%'
																WHEN  210	THEN '%wxNCIbypassMS%'
																END 
							
							IF ISNULL(@PPRT_ID,0) <> 0
								BEGIN
									UPDATE		dbo.ORDR WITH (ROWLOCK)
										SET		PPRT_ID		=	@PPRT_ID
										WHERE	ORDR_ID		=	@OrderID	
								END 
					
						END
				END
		END
	END TRY
	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
END
GO
