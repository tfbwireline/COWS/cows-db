USE [COWS]
GO
_CreateObject 'SP','dbo','insertODIEResponseInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================  
-- Author:  sbg9814  
-- Create date: 07/15/2011  
-- Description: Inserts an ODIE Info Response into the ODIE_RSPN_INFO table.  
-- =========================================================  
ALTER PROCEDURE [dbo].[insertODIEResponseInfo]  
 @REQ_ID   Int  
 ,@MODEL_ID  Varchar(50)  
 ,@DEV_ID  Varchar(50)   
 ,@SERIAL_NO  Varchar(32)  
 ,@RDSN_NBR  varchar(50)  
 ,@FAST_TRK_CD Bit  
 ,@DSPCH_RDY_CD Bit  
 ,@RSPN_INFO_DT DateTime  
 ,@SLCTD_CD  Bit  
 ,@MANF_ID  Varchar(20) 
 ,@H6_CUST_ID Varchar(18) 
AS  
BEGIN  
SET NOCOUNT ON;  
Begin Try  
 INSERT INTO dbo.ODIE_RSPN_INFO WITH (ROWLOCK)  
     (REQ_ID  
     ,MODEL_ID  
     ,DEV_ID  
     ,SERIAL_NO  
     ,RDSN_NBR  
     ,FAST_TRK_CD  
     ,DSPCH_RDY_CD  
     ,RSPN_INFO_DT  
     ,SLCTD_CD  
     ,MANF_ID
     ,H6_CUST_ID)  
  VALUES  (@REQ_ID  
     ,@MODEL_ID  
     ,@DEV_ID  
     ,@SERIAL_NO  
     ,@RDSN_NBR  
     ,@FAST_TRK_CD  
     ,@DSPCH_RDY_CD  
     ,@RSPN_INFO_DT  
     ,@SLCTD_CD  
     ,@MANF_ID
     ,@H6_CUST_ID)  
       
 --IF NOT EXISTS  
 -- (SELECT 'X' FROM dbo.ODIE_REQ WITH (NOLOCK)  
 --  WHERE REQ_ID = @REQ_ID  
 --   AND STUS_ID = 21)  
 -- BEGIN  
 --  EXEC [dbo].[updateODIERequest] @REQ_ID, 21      
 -- END  
End Try  
  
Begin Catch  
 EXEC [dbo].[insertErrorInfo]  
End Catch  
END  