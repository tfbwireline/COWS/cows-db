USE [COWS]
GO
_CreateObject 'SP','dbo','getCPTDataByH1'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jagannath Gangi
-- Create date: 01/12/2016
-- Description:	To Get CPT Data by H1 from NRMBPM
-- =============================================
ALTER PROCEDURE [dbo].[getCPTDataByH1]
	@H1	VARCHAR(9) = ''
AS
BEGIN

BEGIN TRY

DECLARE @SQL nVarchar(max)
	SET @SQL = ''
	--Gather data from NRM BPM Interface table which needs to be processed. 
	--For PJ17929, we are looking at product types like SLFR, MPLS and DIA Off/On net
	SET @SQL = 'SELECT *
					FROM OPENQUERY (NRMBPM,		
				''SELECT DISTINCT *  
					FROM BPMF_OWNER.V_BPMF_COWS_CPT '
	IF @H1 <> ''
		SET @SQL = @SQL + ' WHERE H1_ID = ''''' + @H1 + ''''''

	SET @SQL = @SQL + '	'') '

	EXEC sp_executesql @SQL


	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
END