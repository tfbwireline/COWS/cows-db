USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[processBillActivation]    Script Date: 11/04/2020 10:40:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/19/2011
-- Description:	Get the details for all FSA orders where we need to send responses!
-- =========================================================
ALTER PROCEDURE [dbo].[processBillActivation]
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	DECLARE	@Table	TABLE
		(ORDR_ID		Int
		,PRNT_ORDR_ID	Int
		,ORDR_TYPE_CD	Varchar(2)
		,STUS_ID		SmallInt DEFAULT(10)
		,Flag			Int)
		
	DECLARE	@Cnt		Int
	DECLARE	@TCnt		Int	
	DECLARE	@CurrDate	Varchar(10)
	SET		@Cnt		=	0
	SET		@TCnt		=	0
	SELECT	@CurrDate	=	Convert(Varchar(10), getDate(), 101)
	DECLARE @OnHoldStatus TinyInt = 9
	DECLARE @disc_date varchar(100)
	DECLARE @FSA_Note Varchar(100) = 'Bill Activation Pending Task completed.'


	INSERT INTO	@Table	(ORDR_ID	
						,PRNT_ORDR_ID
						,ORDR_TYPE_CD
						,Flag)
		SELECT DISTINCT	pd.ORDR_ID
						,od.PRNT_ORDR_ID
						,fs.ORDR_TYPE_CD
						,ISNULL(at.TASK_ID, 0)
			FROM		dbo.ORDR		od	WITH (NOLOCK)
			INNER JOIN	dbo.ORDR		pd	WITH (NOLOCK)	ON	od.PRNT_ORDR_ID	=	pd.PRNT_ORDR_ID
			INNER JOIN	dbo.FSA_ORDR	fs	WITH (NOLOCK)	ON	pd.ORDR_ID		=	fs.ORDR_ID
			LEFT JOIN	dbo.ACT_TASK	at	WITH (NOLOCK)	ON	fs.ORDR_ID		=	at.ORDR_ID
															AND	at.TASK_ID		=	1000
															AND	at.STUS_ID		=	0
			INNER JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK)	ON	lp.PPRT_ID		=	pd.PPRT_ID
			LEFT JOIN   dbo.ORDR_MS     ms WITH (NOLOCK)    ON ms.ORDR_ID       =   od.ORDR_ID	
			   WHERE	fs.ORDR_TYPE_CD		NOT IN	('CN') 
						AND	lp.ORDR_TYPE_ID	NOT IN	(8)
						AND ((od.ORDR_CAT_ID = 2) OR (od.ordr_cat_id = 6 AND fs.PROD_TYPE_CD NOT IN ('CP')))
						AND (@CurrDate		>=	Convert(Varchar(10), ISNULL(pd.CUST_CMMT_DT, fs.CUST_CMMT_DT), 101)
								or
							@CurrDate		>=	Convert(Varchar(10), ms.ORDR_BILL_CLEAR_INSTL_DT,101))	      
						AND pd.ORDR_STUS_ID =	1 
						and ISNULL(at.TASK_ID, 0)=1000
						AND NOT EXISTS (SELECT 'X' 
											FROM	dbo.FSA_ORDR_MSG fom WITH (NOLOCK)
											WHERE	fom.ordr_id = pd.Ordr_id
										)
						
	DELETE 
		FROM	@Table	
		WHERE	PRNT_ORDR_ID	IN	(SELECT PRNT_ORDR_ID FROM @Table WHERE	Flag	=	0)
	DELETE		vt
		FROM	@Table	vt
		WHERE	vt.PRNT_ORDR_ID	IN
				(SELECT		PRNT_ORDR_ID 
					FROM	dbo.ORDR			od	WITH (NOLOCK)
				LEFT JOIN	dbo.LK_PPRT			lp	WITH (NOLOCK)	ON	od.PPRT_ID		=	lp.PPRT_ID
				LEFT JOIN	dbo.LK_ORDR_TYPE	lt	WITH (NOLOCK)	ON	lt.ORDR_TYPE_ID	=	lp.ORDR_TYPE_ID
					WHERE	od.ORDR_STUS_ID IN	(0,1)
						AND	lt.ORDR_TYPE_ID	NOT IN	(8)
						AND	od.PRNT_ORDR_ID = vt.PRNT_ORDR_ID
						AND od.ORDR_CAT_ID	=	2
						-- AND	NOT EXISTS
								-- (SELECT 'X' 
									-- FROM @Table 
									-- WHERE PRNT_ORDR_ID =	od.PRNT_ORDR_ID
									-- AND	ORDR_ID	=	od.ORDR_ID)
				)					
	UPDATE	@Table	SET	Flag	=	0	
	
	UPDATE t
		SET t.STUS_ID = @OnHoldStatus
		FROM	@Table t 
	INNER JOIN	dbo.FSA_ORDR fsa WITH (NOLOCK) ON fsa.ORDR_ID = t.ORDR_ID
		WHERE	fsa.ORDR_TYPE_CD = 'DC'
			AND	fsa.RELTD_FTN IS NULL
			AND EXISTS
				(
					SELECT 'X'
						FROM	dbo.ORDR odr WITH (NOLOCK)
						WHERE	t.ORDR_ID = odr.ORDR_ID
							AND odr.ORDR_CAT_ID != 6
				)
	
	SELECT @Cnt	=	Count(1) FROM	@Table
	IF	@Cnt	>	0
		BEGIN
			-----------------------------------------------------------
			--	Load FSA Activation messages. 
			-----------------------------------------------------------
			-- INSERT INTO dbo.FSA_ORDR_MSG	WITH (ROWLOCK) (ORDR_ID,	FSA_MSG_ID,	STUS_ID)
				-- SELECT DISTINCT	t.ORDR_ID,
								-- CASE t.ORDR_TYPE_CD	WHEN 'DC' THEN 5
													-- WHEN 'CH' THEN 6
													-- ELSE 3
								-- END,
								-- t.STUS_ID
					-- FROM	@Table t
					-- WHERE	EXISTS
							-- (
								-- SELECT 'X'
									-- FROM	dbo.ORDR odr WITH (NOLOCK)
									-- WHERE	odr.ORDR_ID		=	t.ORDR_ID
										-- AND	odr.ORDR_CAT_ID	=	2
							-- )
			
			-----------------------------------------------------------
			--	Moves the orders to Bill Activated status. 
			-----------------------------------------------------------		
			WHILE @TCnt	<	@Cnt
				BEGIN
					DECLARE	@OrderID	Int
					SELECT Top 1 @OrderID	=	ORDR_ID
						FROM	@Table
						WHERE	Flag	=	0
							AND STUS_ID !=  @OnHoldStatus
						
					EXEC	dbo.completeActiveTask	@OrderID,	1000,	2,	@FSA_Note,	1	
					
					UPDATE @Table SET Flag = 1 WHERE ORDR_ID	=	@OrderID
					SET	@TCnt	=	@TCnt	+	1
				END
		END
		
	--Process disconnects at 23:59:59
	Delete from @Table 
	SET @TCnt = 0
	SET @Cnt = 0 
	
	
	Insert into @Table (ORDR_ID,Flag)
	SELECT distinct fmsg.ORDR_ID,0
		FROM	dbo.FSA_ORDR_MSG fmsg WITH (NOLOCK)
		WHERE	fmsg.STUS_ID = 9
			AND GETDATE() > CONVERT(varchar,fmsg.CREAT_DT,101) + ' 23' + ':' + '54' + ':' + '59'
			
	SELECT @Cnt = COUNT(1) FROM @Table
	IF @Cnt > 0 
		BEGIN
			Update dbo.FSA_ORDR_MSG 
				SET		STUS_ID = 10 
				WHERE	ORDR_ID IN (SELECT ORDR_ID FROM @Table)
					AND	STUS_ID = @OnHoldStatus
					
			WHILE @TCnt	<	@Cnt
				BEGIN
					SET @OrderID = 0
					SELECT Top 1 @OrderID	=	ORDR_ID
						FROM	@Table
						WHERE	Flag	=	0
					EXEC	dbo.completeActiveTask	@OrderID,	1000,	2,	@FSA_Note,	1
					
					UPDATE @Table SET Flag = 1 WHERE ORDR_ID	=	@OrderID
					SET	@TCnt	=	@TCnt	+	1
				END
		END
	
	--Process NCCO Orders
	DELETE FROM @Table 
	
	INSERT INTO @Table (ORDR_ID,PRNT_ORDR_ID,Flag)
	SELECT DISTINCT	od.ORDR_ID,od.ORDR_ID,0
			FROM		dbo.ORDR		od		WITH (NOLOCK)
			INNER JOIN	dbo.NCCO_ORDR	ncco	WITH (NOLOCK)	ON	od.ORDR_ID		=	ncco.ORDR_ID
			LEFT JOIN	dbo.ACT_TASK	at		WITH (NOLOCK)	ON	ncco.ORDR_ID	=	at.ORDR_ID
																AND	at.TASK_ID		=	1000
																AND	at.STUS_ID		=	0
			   WHERE	ncco.ORDR_TYPE_ID	<>	8 
						AND @CurrDate		=	Convert(Varchar(10), ISNULL(od.CUST_CMMT_DT, ncco.CCS_DT), 101)      
						AND od.ORDR_STUS_ID =	1 

	SET @TCnt = 0
	SET @Cnt = 0
	SELECT @Cnt = COUNT(1) FROM @Table
	IF @Cnt > 0 
		BEGIN
			WHILE @TCnt	<	@Cnt
				BEGIN
					SET @OrderID = 0
					SELECT Top 1 @OrderID	=	ORDR_ID
						FROM	@Table
						WHERE	Flag	=	0
						
					EXEC	dbo.completeActiveTask	@OrderID,	1000,	2,	'Updating NCCO order status to Completed/Disconnected on CCD date.',	1	
					
					UPDATE @Table SET Flag = 1 WHERE ORDR_ID	=	@OrderID
					SET	@TCnt	=	@TCnt	+	1
				END
		END

	-- Process Access Refresh/Renewal orders on Bill Clear date entered by users,
	Delete from @Table 
	SET @TCnt = 0
	SET @Cnt = 0 
	
	
	Insert into @Table (ORDR_ID,Flag)
	SELECT distinct bc.ORDR_ID,0
		FROM	dbo.M5_BILL_CLEAR bc WITH (NOLOCK)
		WHERE	GETDATE() > CONVERT(varchar,bc.BILL_CLEAR_DT,101) + ' 23' + ':' + '50' + ':' + '59'
				AND ISNULL(SENT_DT,'') = '' AND STUS_ID = 1
			
	SELECT @Cnt = COUNT(1) FROM @Table
	IF @Cnt > 0 
		BEGIN
			WHILE @TCnt	<	@Cnt
				BEGIN
					SET @OrderID = 0
					SELECT Top 1 @OrderID	=	ORDR_ID
						FROM	@Table
						WHERE	Flag	=	0
						
						INSERT INTO dbo.M5_ORDR_MSG (ORDR_ID,M5_MSG_ID,STUS_ID,CREAT_DT)
							VALUES (@OrderID,6,10,GETDATE())
				
						INSERT INTO dbo.ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
							VALUES (9,@OrderID,GETDATE(),1,1,'Bill Clear date met. Complete message inserted into Mach5 Interface table.')

						UPDATE dbo.M5_BILL_CLEAR
							set SENT_DT = getdate(), STUS_ID = 0
							where ORDR_ID = @OrderID
					
					UPDATE @Table SET Flag = 1 WHERE ORDR_ID	=	@OrderID
					SET	@TCnt	=	@TCnt	+	1
				END
		END
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
