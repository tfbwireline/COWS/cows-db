USE [COWS]
GO
_CreateObject 'SP','dbo','getCpeAcctCntct_V5U'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 5/4/2015
	-- Description:	Appian Account Contacts.
	-- kh946640 04/17/18 Fix for pulling data based on CSG_LVL_ID 
	-- kh946640 07/06/18 Fix to pull based on OrderID instead of all orders to reduce performance.
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getCpeAcctCntct_V5U]
	@userCSGLvl TinyInt = 0,
	@OrderID	Int = 0
	AS
	BEGIN

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;	
	
IF @OrderID = 0
	BEGIN
	select distinct	
				ORDR_CNTCT_ID,
				fo.FTN                 AS FTN,
				fo.ORDR_ID             AS ORDR_ID,
				ISNULL(lr.ROLE_NME,'') AS RoleName,
				ISNULL(CONVERT(VARCHAR (50), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.FRST_NME) ELSE oc.FRST_NME END),'') AS FirstName,
				ISNULL(CONVERT(VARCHAR (50), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.LST_NME) ELSE oc.LST_NME END),'') AS LastName,
				ISNULL(CONVERT(VARCHAR (200), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.CUST_EMAIL_ADR) ELSE oc.EMAIL_ADR END),'') AS Email,
				ISNULL(oc.PHN_NBR,'') AS PHN_NBR,
				ISNULL(oc.PHN_EXT_NBR,'') AS PHN_NBR_EXTN,
				CASE
				 WHEN CONVERT(VARCHAR (200), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.CUST_CNTCT_NME) ELSE oc.CNTCT_NME END) = 'PRIVATE CONTACT'
					THEN 'Private Contac'
				ELSE ISNULL(CONVERT(VARCHAR (200), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.CUST_CNTCT_NME) ELSE oc.CNTCT_NME END),'') 
				END AS CNTCT_NME,
				ISNULL(oc.ROLE_ID,0)     AS ROLE_ID,
				oc.CIS_LVL_TYPE             AS CIS_LVL_TYPE
        FROM	dbo.FSA_ORDR_CUST	fc	WITH (NOLOCK)
        INNER JOIN dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = fc.ORDR_ID
		INNER JOIN dbo.FSA_ORDR fo	WITH (NOLOCK) ON fo.ORDR_ID = fc.ORDR_ID
		INNER JOIN dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID
		LEFT JOIN	dbo.ORDR_CNTCT		oc	WITH (NOLOCK)	on oc.ORDR_ID	= fc.ORDR_ID 
		LEFT JOIN	dbo.LK_ROLE			lr	WITH (NOLOCK)	on lr.ROLE_ID	= oc.ROLE_ID
		LEFT JOIN	dbo.LK_CNTCT_TYPE	lc	WITH (NOLOCK)	on lc.CNTCT_TYPE_ID = oc.CNTCT_TYPE_ID
		LEFT JOIN dbo.CUST_SCRD_DATA csdC WITH (NOLOCK) ON csdC.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csdC.SCRD_OBJ_TYPE_ID=15
		WHERE  fo.PROD_TYPE_CD = 'CP'		
		AND (
			(
				(act.TASK_ID IN (600,601,602,1000,604,603) AND (act.STUS_ID in (156, 0)))
					AND ord.DMSTC_CD = 0 AND ord.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')
			)
			
		OR (
			(act.TASK_ID in (600,601) AND act.STUS_ID = 0)
			 AND ord.DMSTC_CD = 1
			)
		
		OR (
			(
			  (act.TASK_ID IN (601,602) AND act.STUS_ID = 0) 
			  AND ord.PROD_ID in ('UCCH','UCSV','MIPT','UCSM')
			 )
		   ))
		AND ord.ORDR_CAT_ID = 6
		AND NOT EXISTS (SELECT 'X' FROM dbo.FSA_ORDR_CPE_LINE_ITEM focli2 WITH (NOLOCK)
									WHERE focli2.DROP_SHP = 'Y' AND focli2.ORDR_ID = fo.ORDR_ID)	
		AND oc.ROLE_ID not in (3, 69, 17)
	END	
ELSE
BEGIN
	select distinct	
				ORDR_CNTCT_ID,
				fo.FTN                 AS FTN,
				fo.ORDR_ID             AS ORDR_ID,
				ISNULL(lr.ROLE_NME,'') AS RoleName,
				ISNULL(CONVERT(VARCHAR (50), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.FRST_NME) ELSE oc.FRST_NME END),'') AS FirstName,
				ISNULL(CONVERT(VARCHAR (50), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.LST_NME) ELSE oc.LST_NME END),'') AS LastName,
				ISNULL(CONVERT(VARCHAR (200), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.CUST_EMAIL_ADR) ELSE oc.EMAIL_ADR END),'') AS Email,
				ISNULL(oc.PHN_NBR,'') AS PHN_NBR,
				ISNULL(oc.PHN_EXT_NBR,'') AS PHN_NBR_EXTN,
				CASE
				 WHEN CONVERT(VARCHAR (200), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.CUST_CNTCT_NME) ELSE oc.CNTCT_NME END) = 'PRIVATE CONTACT'
					THEN 'Private Contac'
				ELSE ISNULL(CONVERT(VARCHAR (200), CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.CUST_CNTCT_NME) ELSE oc.CNTCT_NME END),'') 
				END AS CNTCT_NME,
				ISNULL(oc.ROLE_ID,0)     AS ROLE_ID,
				oc.CIS_LVL_TYPE             AS CIS_LVL_TYPE
        FROM	dbo.FSA_ORDR_CUST	fc	WITH (NOLOCK)
        INNER JOIN dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = fc.ORDR_ID
		INNER JOIN dbo.FSA_ORDR fo	WITH (NOLOCK) ON fo.ORDR_ID = fc.ORDR_ID
		INNER JOIN dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID
		LEFT JOIN	dbo.ORDR_CNTCT		oc	WITH (NOLOCK)	on oc.ORDR_ID	= fc.ORDR_ID 
		LEFT JOIN	dbo.LK_ROLE			lr	WITH (NOLOCK)	on lr.ROLE_ID	= oc.ROLE_ID
		LEFT JOIN	dbo.LK_CNTCT_TYPE	lc	WITH (NOLOCK)	on lc.CNTCT_TYPE_ID = oc.CNTCT_TYPE_ID
		LEFT JOIN dbo.CUST_SCRD_DATA csdC WITH (NOLOCK) ON csdC.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csdC.SCRD_OBJ_TYPE_ID=15
		WHERE  fo.PROD_TYPE_CD = 'CP' AND ord.ORDR_ID = @OrderID		
		AND (
			(
				(act.TASK_ID IN (600,601,602,1000,604,603) AND (act.STUS_ID in (156, 0)))
					AND ord.DMSTC_CD = 0 AND ord.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')
			)
			
		OR (
			(act.TASK_ID in (600,601) AND act.STUS_ID = 0)
			 AND ord.DMSTC_CD = 1
			)
		
		OR (
			(
			  (act.TASK_ID IN (601,602) AND act.STUS_ID = 0) 
			  AND ord.PROD_ID in ('UCCH','UCSV','MIPT','UCSM')
			 )
		   ))
		AND ord.ORDR_CAT_ID = 6
		AND NOT EXISTS (SELECT 'X' FROM dbo.FSA_ORDR_CPE_LINE_ITEM focli2 WITH (NOLOCK)
									WHERE focli2.DROP_SHP = 'Y' AND focli2.ORDR_ID = fo.ORDR_ID)	
		AND oc.ROLE_ID not in (3, 69, 17)
	END	
	END

