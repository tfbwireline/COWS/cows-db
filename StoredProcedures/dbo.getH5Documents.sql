﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getH5Documents'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 4th October 2011
-- Description:	Returns all H5 Documents for given H5 Folder
-- ================================================================

ALTER PROCEDURE [dbo].[getH5Documents]
	@H5FolderID INT 	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	SELECT
		HD.H5_FOLDR_ID 
		,HD.H5_DOC_ID
		,HD.FILE_NME
		,HD.FILE_SIZE_QTY
		,HD.CREAT_DT
		,LU.FULL_NME
		,HD.CREAT_BY_USER_ID
	FROM 
		[dbo].[H5_DOC] HD	WITH (NOLOCK)
	INNER JOIN
		[dbo].[LK_USER] LU	WITH (NOLOCK) ON HD.[CREAT_BY_USER_ID] = LU.[USER_ID]
	WHERE 
		HD.H5_FOLDR_ID = @H5FolderID
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
