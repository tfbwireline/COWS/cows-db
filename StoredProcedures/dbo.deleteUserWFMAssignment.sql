USE [COWS]
GO
_CreateObject 'SP','dbo','deleteUserWFMAssignment'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Kyle Wichert
-- Create date: 08/17/2011
-- Description:	Set entries in dbo.USER_WFM to inactive in bulk 
-- ================================================================

ALTER PROCEDURE [dbo].[deleteUserWFMAssignment]
	@WFMIDList VARCHAR(MAX),
	@modUSERID	INT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		
		IF (@WFMIDList = 'All')
		BEGIN
			--Delete All for @modUserID
			--DELETE FROM dbo.USER_WFM WITH (ROWLOCK) WHERE USER_ID = @modUSERID
			UPDATE w
			SET		w.REC_STUS_ID		= 0
			FROM	dbo.USER_WFM	w WITH (ROWLOCK)
			WHERE	w.REC_STUS_ID	=	1
			  AND	w.[USER_ID] = @modUSERID
		END
		ELSE
		BEGIN
			DECLARE @WFMIDs TABLE (WFMID INT)
		
			INSERT INTO @WFMIDs SELECT IntegerID FROM [web].[ParseCommaSeparatedIntegers](@WFMIDList, ',')
		
			--Set Inactive
			UPDATE w
			SET		w.REC_STUS_ID		= 0,
					w.MODFD_BY_USER_ID	= @modUSERID,
					w.MODFD_DT			= GETDATE()
			FROM			dbo.USER_WFM	w WITH (ROWLOCK)
				INNER JOIN	@WFMIDs			r				on	w.WFM_ID = r.WFMID
			WHERE	w.REC_STUS_ID	=	1
		END
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	
END