USE [COWS]
GO
_CreateObject 'SP','dbo','insertFSAOrderVASInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/11/2011
-- Description:	Inserts the FSA VAS data into the FSA_ORDR_VAS table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertFSAOrderVASInfo]
	@ORDR_ID			Int
	,@VAS_TYPE_CD		Varchar(6)
	,@SRVC_LVL_ID		Varchar(4)
	,@NW_ADR			Varchar(30)
	,@VAS_QTY			SmallInt
	
	,@BILL_ITEM_TYPE_ID	SmallInt
	,@BIC_CD			Varchar(10)
	,@LINE_ITEM_DES		Varchar(50)
	,@LINE_ITEM_QTY		SmallInt
	,@ITEM_CD			Varchar(10)
	,@ITEM_DES			Varchar(40)
	,@MRC_CHG_AMT		Varchar(20)
	,@NRC_CHG_AMT		Varchar(20)
	,@ACTN_CD			Varchar(10)
	,@HasBill			Bit
	,@CHG_CD			Varchar(6)
	,@VAS_ID			Int OUT
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	DECLARE	@FSA_ORDR_BILL_LINE_ITEM_ID	Int
	SET		@FSA_ORDR_BILL_LINE_ITEM_ID	=	NULL

	IF @HasBill = 1
		BEGIN
			EXEC [dbo].[insertBillingLineItem]
					@FSA_ORDR_BILL_LINE_ITEM_ID	OUT
					,@ORDR_ID		
					,@BILL_ITEM_TYPE_ID
					,@BIC_CD		
					,@LINE_ITEM_DES	
					,@LINE_ITEM_QTY	
					,@ITEM_CD		
					,@ITEM_DES		
					,@MRC_CHG_AMT	
					,@NRC_CHG_AMT	
					,@ACTN_CD
					,@CHG_CD
		END
		
	INSERT INTO dbo.FSA_ORDR_VAS WITH (ROWLOCK)
					(ORDR_ID
					,VAS_TYPE_CD
					,SRVC_LVL_ID
					,NW_ADR
					,VAS_QTY
					,FSA_ORDR_BILL_LINE_ITEM_ID)	
		VALUES		(@ORDR_ID
					,@VAS_TYPE_CD
					,@SRVC_LVL_ID
					,@NW_ADR
					,@VAS_QTY
					,@FSA_ORDR_BILL_LINE_ITEM_ID)

	SELECT @VAS_ID = SCOPE_IDENTITY()
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
