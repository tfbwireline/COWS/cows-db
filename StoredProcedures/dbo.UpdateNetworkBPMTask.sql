USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[UpdateNetworkBPMTask]    Script Date: 1/18/2022 2:43:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  jrg7298            
-- Create date: 12/01/2018   
-- Description: Update BPM task for Network-MDS Event using Design # and TaskID
-- Only completing for CE events
-- km967761 - 02/03/2021 - Bypass if already in EVENT_HIST
-- Jbolano15 - 12/10/2021 - Do not send BPM Task completion for Port product change aka "-DC" orders
-- =============================================            
ALTER PROCEDURE [dbo].[UpdateNetworkBPMTask]
	@DDNbr VARCHAR(100)
	,@H6 VARCHAR(511)
	,@VASCEFlg TINYINT = 0
	,@CEChngFlg	  BIT = 0
	,@EventID INT
	,@ModfdByUserID INT
AS
BEGIN
	BEGIN TRY
	
		DECLARE @dSQL NVARCHAR(MAX)
		DECLARE @UpdDT VARCHAR(19)

		SET @UpdDT = CONVERT(VARCHAR, getdate(), 110)

		IF OBJECT_ID(N'tempdb..#TaskTbl', N'U') IS NOT NULL
			DROP TABLE #TaskTbl

		CREATE TABLE #TaskTbl (
			TaskTblID INT IDENTITY(1, 1) NOT NULL
			,TASK_ID VARCHAR(100) NULL
			,TASK_NME VARCHAR(255) NULL
			,NUA_ADR VARCHAR(511) NULL
			,CE_SRVC_ID VARCHAR(511) NULL
			,Flag BIT NULL
			)

		CREATE CLUSTERED INDEX IDX_C_Temp_DDTbl ON #TaskTbl (TaskTblID)

		DECLARE @SQL NVARCHAR(4000)
			,@TaskID INT = 0
			,@Ctr SMALLINT = 1
			,@Cnt SMALLINT = 0
			,@NTE_TXT VARCHAR(1000) = ''
			,@TaskNme VARCHAR(255) = ''
			,@NUA VARCHAR(511) = ''
			,@CESrvcID VARCHAR(511) = ''

		IF (@VASCEFlg = 2)
		BEGIN
			SET @SQL = 'INSERT INTO #TaskTbl (TASK_ID,TASK_NME,NUA_ADR,CE_SRVC_ID,Flag) select DISTINCT TASK_ID,QUEUE_NME,NUA_ADR,CE_SRVC_ID,0 FROM openquery(nrmbpm, ''select DISTINCT TASK_ID,QUEUE_NME,NUA_ADR,CE_SRVC_ID FROM BPMF_OWNER.V_BPMF_CE_COWS_ACTV_EVENT WHERE FTN_NBR NOT LIKE ''''%-DC'''''
			IF (@CEChngFlg = 1)
				SET @SQL = @SQL + ' AND CE_SRVC_ID=''''' + @H6 + ''''' AND TASK_ID IS NOT NULL AND ((TASK_STUS_NME != ''''Cancelled'''') AND (TASK_STUS_NME != ''''Installed'''')) AND QUEUE_NME LIKE ''''%ACTIV%'''' '')'
			ELSE
				SET @SQL = @SQL + ' AND DD_NBR=''''' + @DDNbr + ''''' AND H6_ID=''''' + @H6 + ''''' AND TASK_ID IS NOT NULL AND ((TASK_STUS_NME != ''''Cancelled'''') AND (TASK_STUS_NME != ''''Installed'''')) AND QUEUE_NME LIKE ''''%ACTIV%'''' '')'
		END

		EXEC sp_executesql @SQL

		SELECT @Cnt = COUNT(1)
		FROM #TaskTbl

		WHILE (@Ctr <= @Cnt)
		BEGIN
			SELECT TOP 1 @TaskID = TASK_ID
				,@TaskNme = TASK_NME
				,@NUA = NUA_ADR
				,@CESrvcID = CE_SRVC_ID
			FROM #TaskTbl
			WHERE Flag = 0
				AND TaskTblID = @Ctr

			IF (@VASCEFlg=2)
				SET @NTE_TXT = 'BPM Task - ' + CONVERT(VARCHAR, @TaskID) + ' completed for H6 - ' + @H6 + ', Design - ' + @DDNbr + ', CE ServiceID - ' + @CESrvcID + ', Queue - ' + @TaskNme + ' from MDS Event - ' + CONVERT(VARCHAR, @EventID)

			IF NOT EXISTS (SELECT 'x' FROM EVENT_HIST WITH (NOLOCK) WHERE EVENT_ID = @EventID AND CMNT_TXT LIKE '%'+@NTE_TXT+'%')
			BEGIN
				IF (LEN(@NUA) > 0)
					AND (@TaskID > 0)
				BEGIN
					--SELECT 'ASD'
					IF OBJECT_ID(N'tempdb..#bpmtaskupdt', N'U') IS NOT NULL
						DROP TABLE #bpmtaskupdt

					CREATE TABLE #bpmtaskupdt (retval INT);

					IF (@VASCEFlg=2)
					BEGIN
						set @dSQL = 'declare @retval int; EXECUTE (''declare v_ret number; begin v_ret := BPMF_OWNER.BPMF_CE_COWS_AUTO_TASKUPDATE('+convert(varchar,@TaskID) + ','''''+ @CESrvcID + '''''); ? := v_ret; end;'',  @retval OUTPUT) AT NRMBPM; insert into #bpmtaskupdt(retval) select @retval;'
					END

					EXEC sp_executesql @dSQL

					IF EXISTS (SELECT 'x' FROM #bpmtaskupdt WHERE retval=1)
						SET @NTE_TXT = @NTE_TXT + '; Action was Unsuccessful'
					ELSE
						SET @NTE_TXT = @NTE_TXT + '; Action was Successful'

					INSERT INTO dbo.EVENT_HIST (
						EVENT_ID
						,ACTN_ID
						,CMNT_TXT
						,CREAT_BY_USER_ID
						,CREAT_DT
						)
					VALUES (
						@EventID
						,20
						,@NTE_TXT
						,@ModfdByUserID
						,GETDATE()
						)
				END
			END

			UPDATE #TaskTbl
			SET Flag = 1
			WHERE TaskTblID = @Ctr
				AND Flag = 0

			SET @Ctr = @Ctr + 1
		END
	END TRY

	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END