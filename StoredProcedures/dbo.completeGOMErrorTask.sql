USE [COWS]
GO
_CreateObject 'SP','dbo','completeGOMErrorTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================================
-- Author:		sbg9814
-- Create date: 08/10/2011
-- Description:	Associate H5 folder to the ORDR.
-- ==================================================================================
ALTER PROCEDURE dbo.completeGOMErrorTask 
	@OrderID	Int,
	@TaskID		SmallInt	=	101,
	@TaskStatus	TinyInt		=	3,
	@Comments	Varchar(1000) = NULL
AS
BEGIN
Begin Try
	UPDATE			o
		SET			o.H5_FOLDR_ID	=	hf.H5_FOLDR_ID					
		FROM		dbo.ORDR		o	WITH (ROWLOCK)
		LEFT JOIN	dbo.H5_FOLDR	hf	WITH (NOLOCK)	ON	o.H5_H6_CUST_ID	=	hf.CUST_ID
			WHERE	o.ORDR_ID		=	@OrderID
			AND		o.H5_FOLDR_ID	IS	NULL
End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END
GO
