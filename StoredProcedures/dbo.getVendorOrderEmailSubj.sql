﻿USE [COWS]
GO
_CreateObject 'FS','dbo','getVendorOrderEmailSubject'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 12th July 2011
-- Description:	Gets Vendor Email Subject
-- ================================================================

ALTER FUNCTION [dbo].[getVendorOrderEmailSubject]
(
	@VendorOrderID INT 	
)
RETURNS VARCHAR(500)
AS
BEGIN	
	DECLARE @Circuits VARCHAR(300)
	DECLARE @EmailSubject VARCHAR(500)
	
	--Get Vendor Emails
	SELECT TOP 1
		@EmailSubject = FO.FTN + '-' + 
		CONVERT(VARCHAR(50),FOC.CUST_NME) + '-' + 
		CONVERT(VARCHAR(50),OA.CTY_NME) + '-' + 
		CONVERT(VARCHAR(50),LC.CTRY_NME)		
	FROM dbo.VNDR_ORDR VO WITH (NOLOCK) 
	LEFT OUTER JOIN dbo.FSA_ORDR FO WITH (NOLOCK) ON VO.ORDR_ID = FO.ORDR_ID
	LEFT OUTER JOIN dbo.FSA_ORDR_CUST FOC WITH (NOLOCK) ON FO.ORDR_ID = FOC.ORDR_ID
	LEFT OUTER JOIN dbo.ORDR_ADR OA WITH (NOLOCK) ON FO.ORDR_ID = OA.ORDR_ID
	LEFT OUTER JOIN dbo.LK_CTRY LC WITH (NOLOCK) ON OA.CTRY_CD = LC.CTRY_CD
	WHERE VO.VNDR_ORDR_ID = @VendorOrderID
	--print @EmailSubject
	
	--Get Circuits
	SELECT
		@Circuits = COALESCE(@Circuits + ',', '') + CONVERT(VARCHAR(15), C.VNDR_CKT_ID)
	FROM dbo.CKT C WITH (NOLOCK)
	WHERE C.VNDR_ORDR_ID = @VendorOrderID

	RETURN @EmailSubject + '-' + @Circuits
END