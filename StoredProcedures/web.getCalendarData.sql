USE [COWS]
GO
_CreateObject 'SP','web','getCalendarData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		sbg9814
-- Create date: 09/19/2011
-- Description:	Gets the data for a given Calendar page
-- =========================================================
-- Modify:		kaw4664
-- Create date: 04/11/2012
-- Description:	Add Fedline Events
-- =========================================================
-- Modify:		kh946640
-- Create date: 07/22/2020
-- Description:	Add Created Date, Created By, Modified Date and Modified By columns to the resultset
-- =========================================================
-- Modify:		qi931353
-- Create date: 12/11/2020
-- Description:	Updated APPT_TYPE_ID IN (4,5,37) for Network Intl
-- =========================================================
ALTER PROCEDURE [web].[getCalendarData] --2, 207, 0, 2
	@IsWFCalendar	TinyInt	=	1
	,@USER_ID		Int		=	0
	,@APPT_TYPE_ID	Int		=	0
	,@CSGLvlId	TINYINT	= 0
AS
BEGIN
SET NOCOUNT ON;
Begin Try

DECLARE @User	TABLE
	(UserID	Int)
DECLARE	@ApptUsers	TABLE
	(ApptID		Int
	,UserIDs	Varchar(Max)
	,Flag		Bit
	,STime		DateTime)	

DECLARE @ApptIDs TABLE
(APPT_ID INT)

DECLARE	@Cnt	Int
DECLARE	@TCnt	Int
DECLARE	@ApptID	Int
DECLARE	@Users	Varchar(Max)	
DECLARE @Admin	Bit
DECLARE @NewLine	Char(2)
SET @NewLine=	Char(13) + Char(10)
SET @Admin	=	0	
SET	@Cnt	=	0
SET	@TCnt	=	0
			
IF	@APPT_TYPE_ID	!=	0
	BEGIN
		IF @APPT_TYPE_ID	!=	30
			BEGIN
				SELECT		 CASE WHEN ev.CSG_LVL_ID>0 THEN 'Private Customer, Event ID : '+CONVERT(VARCHAR,ev.EVENT_ID) ELSE 
							 CASE WHEN (ISNULL(me.EVENT_ID,0)>0 AND ISNULL(me.SOFT_ASSIGN_CD,0)=0) THEN ap.[SUBJ_TXT] + ', Soft Assigned: False' WHEN (ISNULL(me.EVENT_ID,0)>0 AND ISNULL(me.SOFT_ASSIGN_CD,0)!=0) THEN ap.[SUBJ_TXT] + ', Soft Assigned: True' ELSE ap.[SUBJ_TXT] END END AS [SUBJ_TXT]
							,CASE WHEN ev.CSG_LVL_ID>0 THEN 'Private Customer' ELSE REPLACE(ap.[DES], @NewLine, ' ')	END	AS	[DES]
							,ap.[STRT_TMST]
							,ap.[END_TMST]
							,CASE WHEN ev.CSG_LVL_ID>0 THEN '6200 Sprint Parkway' ELSE ap.[APPT_LOC_TXT] END AS [APPT_LOC_TXT]
							,ap.[APPT_TYPE_ID]
							,REPLACE(ap.[RCURNC_DES_TXT], @NewLine, ' ')			AS	[RCURNC_DES_TXT]
							,ap.[APPT_ID]
							,REPLACE(ap.[ASN_TO_USER_ID_LIST_TXT], @NewLine, '')	AS	[ASN_TO_USER_ID_LIST_TXT]
							,ap.[RCURNC_CD] 
							,ISNULL(me.SOFT_ASSIGN_CD,0) AS SOFT_ASSIGN_CD
					FROM	dbo.APPT	ap WITH (NOLOCK) INNER JOIN
							dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID = ap.EVENT_ID  LEFT JOIN
							dbo.[MDS_EVENT] me WITH (NOLOCK) ON me.EVENT_ID = ev.EVENT_ID
					WHERE	((	ap.APPT_TYPE_ID	=	@APPT_TYPE_ID	AND	@APPT_TYPE_ID	IN (4, 5)
							AND	EXISTS
								(SELECT 'X' FROM dbo.MDS_EVENT me	WITH (NOLOCK)
									INNER JOIN MDS_EVENT_NTWK_ACTY mena WITH (NOLOCK) ON mena.EVENT_ID = me.EVENT_ID
									WHERE	me.EVENT_ID		=	ap.EVENT_ID
										AND mena.NTWK_ACTY_TYPE_ID != 4
										AND	((me.EVENT_STUS_ID	IN	(2, 4, 5, 6, 7, 9, 10)) OR ((me.EVENT_STUS_ID=3) AND (me.WRKFLW_STUS_ID IN (3,5,8)))))	
							)
						OR	(	ap.APPT_TYPE_ID	=	@APPT_TYPE_ID	AND	@APPT_TYPE_ID	= 37
							AND	EXISTS
								(SELECT 'X' FROM dbo.MDS_EVENT me	WITH (NOLOCK)
									INNER JOIN MDS_EVENT_NTWK_ACTY mena WITH (NOLOCK) ON mena.EVENT_ID = me.EVENT_ID
									WHERE	me.EVENT_ID		=	ap.EVENT_ID
										AND mena.NTWK_ACTY_TYPE_ID = 4
										AND	((me.EVENT_STUS_ID	IN	(2, 4, 5, 6, 7, 9, 10)) OR ((me.EVENT_STUS_ID=3) AND (me.WRKFLW_STUS_ID IN (3,5,8)))))	
							)
						OR	(	ap.APPT_TYPE_ID	=	@APPT_TYPE_ID	AND	@APPT_TYPE_ID	= 34
							AND	EXISTS
								(SELECT 'X' FROM dbo.UCaaS_EVENT	WITH (NOLOCK)
									WHERE	EVENT_ID		=	ap.EVENT_ID
										AND	((EVENT_STUS_ID	IN	(2, 4, 5, 6, 7, 9, 10)) OR ((EVENT_STUS_ID=3) AND (WRKFLW_STUS_ID IN (3,5,8)))))	
							)
						OR  (	ap.APPT_TYPE_ID	=	@APPT_TYPE_ID	AND	@APPT_TYPE_ID	= 35
							AND	EXISTS
								(SELECT 'X' FROM dbo.SIPT_EVENT	WITH (NOLOCK)
									WHERE	EVENT_ID		=	ap.EVENT_ID
										AND	((EVENT_STUS_ID	IN	(2, 4, 5, 6, 7)) OR ((EVENT_STUS_ID=3) AND (WRKFLW_STUS_ID IN (3,5,8)))))	
							))
							AND ((ap.REC_STUS_ID IS NULL) OR ap.REC_STUS_ID IN (0,1))
							AND ((ev.CSG_LVL_ID = 0) OR ((@CSGLvlId!=0) AND (ev.CSG_LVL_ID >= @CSGLvlId)))
			END
			ELSE
			BEGIN
				OPEN SYMMETRIC KEY FS@K3y 
				DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

				SELECT		dbo.decryptBinaryData(csd.EVENT_TITLE_TXT)			AS	[SUBJ_TXT]
							,REPLACE([DES], @NewLine, ' ')						AS	[DES]
							,[STRT_TMST]
							,[END_TMST]
							,'Installation Contact Phone: ' + cn.PHN_NBR		AS	[APPT_LOC_TXT]
							,[APPT_TYPE_ID]
							,REPLACE([RCURNC_DES_TXT], @NewLine, ' ')			AS	[RCURNC_DES_TXT]
							,[APPT_ID]
							,REPLACE([ASN_TO_USER_ID_LIST_TXT], @NewLine, '')	AS	[ASN_TO_USER_ID_LIST_TXT]
							,[RCURNC_CD] 
				FROM			dbo.APPT						ap  WITH (NOLOCK)
					INNER JOIN	dbo.FEDLINE_EVENT_USER_DATA		fe  WITH (NOLOCK)	ON	ap.EVENT_ID			=	fe.EVENT_ID
					INNER JOIN	dbo.FEDLINE_EVENT_TADPOLE_DATA	td  WITH (NOLOCK)	ON	ap.EVENT_ID			=	td.EVENT_ID	AND	td.REC_STUS_ID	=	1
					INNER JOIN	dbo.FEDLINE_EVENT_CNTCT			cn  WITH (NOLOCK)	ON	td.FEDLINE_EVENT_ID	=	cn.FEDLINE_EVENT_ID
					INNER JOIN  dbo.CUST_SCRD_DATA csd WITH (NOLOCK)   ON csd.SCRD_OBJ_ID=fe.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=25
				WHERE		fe.EVENT_STUS_ID	IN (2, 5, 6, 9, 10, 14, 15, 16, 17)
						AND	cn.ROLE_ID			=	1
						AND	cn.CNTCT_TYPE_ID	=	1
						AND (@CSGLvlId IN (1,2))
			END
						
	END
ELSE
	BEGIN
		INSERT INTO @ApptIDs
		SELECT DISTINCT APPT_ID
		FROM dbo.APPT a WITH (NOLOCK)
		WHERE (ISNULL(REC_STUS_ID, 0) IN (0,1))
		  AND (DATEDIFF(month, STRT_TMST, getdate()) <= 3)
		  AND RCURNC_CD = 0

		INSERT INTO @ApptIDs
		SELECT DISTINCT a.APPT_ID
		FROM dbo.APPT a WITH (NOLOCK) INNER JOIN
		     dbo.APPT_RCURNC_DATA ar WITH (NOLOCK) ON ar.APPT_ID = a.APPT_ID
		WHERE (ISNULL(a.REC_STUS_ID, 0) IN (0,1))
		  AND (DATEDIFF(month, ar.STRT_TMST, getdate()) <= 3)
		  AND a.RCURNC_CD = 1

		INSERT INTO @ApptUsers (ApptID,	UserIDs,	Flag, STime)
			SELECT	a.APPT_ID,	dbo.parseAssignedUsersFromAppt(ASN_TO_USER_ID_LIST_TXT),	0,	STRT_TMST
			FROM dbo.APPT a WITH (NOLOCK) INNER JOIN @ApptIDs aid ON aid.APPT_ID = a.APPT_ID
			WHERE (((APPT_TYPE_ID IN (31,33) OR (APPT_TYPE_ID	>	8	AND	APPT_TYPE_ID	<	17)) AND	@IsWFCalendar	=	1)
				OR ((APPT_TYPE_ID	>	16) AND (APPT_TYPE_ID	< 28)	AND	(@IsWFCalendar	!=	1))
				OR (((APPT_TYPE_ID	<	9) OR (APPT_TYPE_ID	IN (28, 29, 30, 32, 34, 35)))							 AND	(@IsWFCalendar	!=	1)
							 AND	(EXISTS	(SELECT 'X'	FROM dbo.MDS_EVENT	WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND ((EVENT_STUS_ID	IN	(2, 4, 5, 6, 7, 9, 10)) OR ((EVENT_STUS_ID=3) AND (WRKFLW_STUS_ID IN (3,5,8)))) AND  (ISNULL(MDS_FAST_TRK_TYPE_ID,'') = ''))
									OR	EXISTS	(SELECT 'X'	FROM dbo.UCaaS_EVENT	WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND ((EVENT_STUS_ID	IN	(2, 4, 5, 6, 7, 9, 10)) OR ((EVENT_STUS_ID=3) AND (WRKFLW_STUS_ID IN (3,5,8)))))
									OR	EXISTS	(SELECT 'X'	FROM dbo.SIPT_EVENT	WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND ((EVENT_STUS_ID	IN	(2, 4, 5, 6, 7)) OR ((EVENT_STUS_ID=3) AND (WRKFLW_STUS_ID IN (3,5,8)))))
									OR	EXISTS	(SELECT 'X'	FROM dbo.FEDLINE_EVENT_USER_DATA	WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND EVENT_STUS_ID	IN (2, 5, 6, 9, 10)))))

		DELETE FROM	@ApptUsers
			WHERE	@IsWFCalendar	!=	1
			AND		STime < GETDATE() - 2
			AND		ApptID	IN
					(SELECT APPT_ID FROM dbo.APPT	a WITH (NOLOCK)	
						WHERE	EXISTS	(SELECT 'X'	FROM dbo.MDS_EVENT				WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND EVENT_STUS_ID = 6)
							OR	EXISTS	(SELECT 'X'	FROM dbo.UCaaS_EVENT				WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND EVENT_STUS_ID = 6)
							OR	EXISTS	(SELECT 'X'	FROM dbo.SIPT_EVENT					WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND EVENT_STUS_ID = 6)
							OR	EXISTS	(SELECT 'X'	FROM dbo.FEDLINE_EVENT_USER_DATA	WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND EVENT_STUS_ID = 6)
					)				

		SELECT 			 CASE WHEN ev.CSG_LVL_ID>0 THEN 'Private Customer, Event ID : '+CONVERT(VARCHAR,ev.EVENT_ID) ELSE 
						CASE WHEN (ISNULL(me.EVENT_ID,0)>0 AND ISNULL(me.SOFT_ASSIGN_CD,0)=0) THEN ap.[SUBJ_TXT] + ', Soft Assigned: False' WHEN (ISNULL(me.EVENT_ID,0)>0 AND ISNULL(me.SOFT_ASSIGN_CD,0)!=0) THEN ap.[SUBJ_TXT] + ', Soft Assigned: True' ELSE ap.[SUBJ_TXT] END END AS [SUBJ_TXT]
						,CASE WHEN ev.CSG_LVL_ID>0 THEN 'Private Customer' ELSE REPLACE(ap.[DES], @NewLine, ' ')	END	AS	[DES]
						,ap.[STRT_TMST]
						,ap.[END_TMST]
						,CASE WHEN ev.CSG_LVL_ID>0 THEN '6200 Sprint Parkway' ELSE ap.[APPT_LOC_TXT] END AS [APPT_LOC_TXT]
						,ap.[APPT_TYPE_ID]
						,REPLACE(ap.[RCURNC_DES_TXT], @NewLine, ' ')			AS	[RCURNC_DES_TXT]
						,ap.[APPT_ID]
						,REPLACE(ap.[ASN_TO_USER_ID_LIST_TXT], @NewLine, '')	AS	[ASN_TO_USER_ID_LIST_TXT]
						,ap.[RCURNC_CD] 
						,ISNULL(me.SOFT_ASSIGN_CD,0) AS SOFT_ASSIGN_CD
						,u1.[DSPL_NME] AS [CREAT_BY_USER] 
						,u2.[DSPL_NME] AS [MODFD_BY_USER] 
						,ap.[CREAT_DT]
						,ap.[MODFD_DT]
			FROM		dbo.[APPT]	ap	WITH (NOLOCK) LEFT JOIN
						dbo.[EVENT] ev  WITH (NOLOCK) ON ap.EVENT_ID = ev.EVENT_ID LEFT JOIN
						dbo.[MDS_EVENT] me WITH (NOLOCK) ON me.EVENT_ID = ev.EVENT_ID LEFT JOIN
						dbo.[LK_USER] u1 WITH (NOLOCK) ON u1.USER_ID = ap.CREAT_BY_USER_ID LEFT JOIN
						dbo.[LK_USER] u2 WITH (NOLOCK) ON u2.USER_ID = ap.MODFD_BY_USER_ID
			INNER JOIN	@ApptUsers	vs	ON	ap.APPT_ID	=	vs.ApptID
			WHERE		(((convert(varchar,vs.UserIDs) LIKE	convert(varchar,@USER_ID)+'|%')
						  AND (LEN(convert(varchar,@USER_ID)) = (CHARINDEX('|', convert(varchar,vs.UserIDs))-1)))
						  OR (convert(varchar,vs.UserIDs) LIKE	'%|'+convert(varchar,@USER_ID)+'|%'))
						 AND ((ISNULL(ev.CSG_LVL_ID,0) = 0) OR ((@CSGLvlId!=0) AND (ev.CSG_LVL_ID >= @CSGLvlId)))
	END

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END