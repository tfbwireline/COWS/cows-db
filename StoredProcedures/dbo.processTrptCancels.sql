USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[processTrptCancels]    Script Date: 01/25/2021 1:36:46 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <09/19/2011>
-- Description:	<To Process Transport Cancels>
-- =============================================
ALTER PROCEDURE [dbo].[processTrptCancels] 
@ORDR_ID		INT,
@CNCL_ORDR_ID	INT = NULL
AS
BEGIN
	BEGIN TRY
DECLARE @ORDR_CAT_ID		TinyInt
DECLARE @PRE_SBMT_ORDR_ID	Int
DECLARE @INTL_CD			Bit
DECLARE @H5_FLDR_ID			Int
DECLARE @ORDR_ACTN_ID		TinyInt		
DECLARE @WGPatternIDs		VarChar(MAX)
DECLARE @CNCL_ORDR_CAT_ID	Int = 0
DECLARE @PRNT_PPRT_ID		Int

		SELECT @ORDR_CAT_ID		=	ORDR_CAT_ID
			  ,@H5_FLDR_ID		=	ISNULL(H5_FOLDR_ID,0)
			  ,@INTL_CD			=	DMSTC_CD
			  ,@ORDR_ACTN_ID	=	ISNULL(fsa.ORDR_ACTN_ID,2)
			FROM	dbo.ORDR odr WITH (NOLOCK)
		LEFT JOIN	dbo.FSA_ORDR fsa WITH (NOLOCK) ON fsa.ORDR_ID = odr.ORDR_ID	
			WHERE	odr.ORDR_ID = CASE WHEN (@ORDR_ID=-1) THEN @CNCL_ORDR_ID ELSE @ORDR_ID END
		
		IF ISNULL(@CNCL_ORDR_ID,0) != 0 -- Cancel order received from FSA
			BEGIN
				IF @ORDR_CAT_ID = 2
					BEGIN
						EXEC	dbo.insertOrderNotes	@CNCL_ORDR_ID,	9,	'COWS received order from FSA.', 1
						
						SELECT @PRE_SBMT_ORDR_ID	=	ISNULL(fPS.ORDR_ID,0)
							FROM	dbo.FSA_ORDR	fS WITH (NOLOCK)
						LEFT JOIN	dbo.FSA_ORDR	fPS WITH (NOLOCK)	ON	fs.FTN				=	fPS.FTN
														AND	fPS.ORDR_ACTN_ID	=	1
							WHERE	fs.ORDR_ID	=	@CNCL_ORDR_ID
							
						IF @PRE_SBMT_ORDR_ID <> 0
							BEGIN
								EXEC	dbo.deletePartialFSAOrder	@PRE_SBMT_ORDR_ID
							END
							
						IF @ORDR_ACTN_ID	=	1
							BEGIN
								EXEC [dbo].[deleteActiveTasks] @ORDR_ID
								UPDATE dbo.ORDR SET ORDR_STUS_ID = 4 WHERE ORDR_ID = @ORDR_ID
							END	
							
						UPDATE		dbo.ORDR	WITH (ROWLOCK)
							SET		ORDR_STUS_ID	=	4
							WHERE	ORDR_ID			=	@CNCL_ORDR_ID
					END
				
				IF @ORDR_CAT_ID = 6
					BEGIN

						DECLARE @CnclOrdrFTN varchar (20), @Note varchar(max)

						select @CnclOrdrFTN = FTN FROM FSA_ORDR with (nolock)
								where ORDR_ID = @CNCL_ORDR_ID
						
						SET @Note = 'COWS received cancel order from Mach5: ' + @CnclOrdrFTN
						
						EXEC	dbo.insertOrderNotes @ORDR_ID,	9,	@Note, 1
						
						EXEC	dbo.insertOrderNotes @CNCL_ORDR_ID,	9,'COWS received order from Mach5', 1
						
						IF EXISTS
							(
								SELECT 'X'
									FROM	dbo.FSA_ORDR WITH (NOLOCK)
									WHERE	ORDR_ID = @CNCL_ORDR_ID
										AND PROD_TYPE_CD = 'CP'
							)
							BEGIN
								SET  @WGPatternIds = (SELECT DISTINCT o.ORDR_ID AS OrderID, sm.DESRD_WG_PTRN_ID AS WGPatternID 
									  FROM	[dbo].ORDR   o WITH (NOLOCK) 
											 INNER JOIN [dbo].LK_PPRT lp WITH (NOLOCK) ON o.PPRT_ID		= lp.PPRT_ID
											 INNER JOIN [dbo].SM         sm WITH (NOLOCK) ON lp.SM_ID   = sm.SM_ID
										WHERE
											sm.PRE_REQST_WG_PTRN_ID  =  0
										AND	o.ORDR_ID = @CNCL_ORDR_ID
									  FOR XML PATH('IDs'), ROOT('NextPatterns'))
								--SELECT @CNCL_ORDR_ID, @WGPatternIDs
								EXEC [dbo].insertNextWGPattern @CNCL_ORDR_ID, @WGPatternIds
							END
						ELSE
							BEGIN
								UPDATE		dbo.ORDR	WITH (ROWLOCK)
									SET		ORDR_STUS_ID	=	4
									WHERE	ORDR_ID			=	@CNCL_ORDR_ID	
							
							-- Begin new code for Mach5 cancels in PJ017930 3/3/17
									-- Removed 3/13/2017  8:14 a.m.
									--SELECT	@PRNT_PPRT_ID	=	ISNULL(PPRT_ID,0)
									--		FROM		dbo.ORDR	
									--		WHERE	ORDR_ID	=	@ORDR_ID
												
									--DELETE  FROM dbo.ACT_TASK		WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
									--DELETE  FROM dbo.WG_PROF_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
									--DELETE  FROM dbo.WG_PTRN_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
									--EXEC [dbo].[deleteActiveTasks]	@CNCL_ORDR_ID 
									
									
									--UPDATE		dbo.ORDR	WITH (ROWLOCK)
									--	SET		ORDR_STUS_ID	=	4
									--	WHERE	ORDR_ID			=	@ORDR_ID
										
									--UPDATE		dbo.ORDR	WITH (ROWLOCK)
									--	SET		ORDR_STUS_ID	=	4,
									--			PPRT_ID			=	@PRNT_PPRT_ID -- To display cancelled order in grp specific completed view
									--	WHERE	ORDR_ID			=	@CNCL_ORDR_ID

									----To remove related order association if exists & insert notes.
									--Exec dbo.removeRelatedOrderAssociation @ORDR_ID
									
									--INSERT INTO dbo.ACT_TASK(ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
									--	VALUES (@ORDR_ID,212,0,205,GETDATE())
							
							--  END New Mach5 cancel modifications 3/3/17
							
							END
						
						
					END	
					
			END 
		ELSE -- Cancel order initiated from COWS
			BEGIN
				IF  (@ORDR_ACTN_ID = 1)
					BEGIN
						EXEC [dbo].[deleteActiveTasks] @ORDR_ID
						UPDATE dbo.ORDR SET ORDR_STUS_ID = 4 WHERE ORDR_ID = @ORDR_ID
					END
				ELSE
					BEGIN
						--EXEC [dbo].[deleteActiveTasks]	@ORDR_ID --causing decrement in user assignment
						DELETE  FROM dbo.ACT_TASK		WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
						DELETE  FROM dbo.WG_PROF_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
						DELETE  FROM dbo.WG_PTRN_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
					End

				SET  @WGPatternIds = (SELECT DISTINCT o.ORDR_ID AS OrderID, sm.DESRD_WG_PTRN_ID AS WGPatternID 
									  FROM	[dbo].ORDR   o WITH (NOLOCK) 
											 INNER JOIN [dbo].LK_PPRT lp WITH (NOLOCK) ON o.PPRT_ID		= lp.PPRT_ID
											 INNER JOIN [dbo].SM         sm WITH (NOLOCK) ON lp.SM_ID   = sm.SM_ID
										WHERE
											sm.PRE_REQST_WG_PTRN_ID  =  0
										AND	o.ORDR_ID = @ORDR_ID
									  FOR XML PATH('IDs'), ROOT('NextPatterns'))
			
				EXEC [dbo].insertNextWGPattern @ORDR_ID, @WGPatternIds
			END	
		
		UPDATE dbo.ORDR_MS SET CUST_ACPTC_TURNUP_DT = NULL WHERE ORDR_ID = @ORDR_ID
		
		IF @ORDR_CAT_ID != 6
			BEGIN
				IF @INTL_CD = 1 OR @ORDR_CAT_ID = 5 
					BEGIN
						IF (@H5_FLDR_ID <> 0)
							BEGIN
								EXEC	dbo.CompleteActiveTask	@ORDR_ID, 100,	2, 'Cancel Order Received.'				
							END
						ELSE
							BEGIN
								EXEC	dbo.CompleteActiveTask	@ORDR_ID, 100,	1, 'Unable to associate H5 Folder. Moving the order to Error.'
							END
					END
				ELSE
					BEGIN
						EXEC	dbo.CompleteActiveTask	@ORDR_ID, 100,	2, 'Cancel Order Received.'
					END

				--To remove related order association if exists & insert notes.
				Exec dbo.removeRelatedOrderAssociation @ORDR_ID	
			END		
		
	END TRY
	
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH

    
END
