USE [COWS]
GO
_CreateObject 'SP','web','getSDEView_V2'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 ============================================================  
 Author:		qi931353  
 Created date:	08/17/2017  
 Description:	Gets the Data for SDE (Cagate) View
 ============================================================
*/

ALTER PROCEDURE [web].[getSDEView_V2]
	@USER_ID	INT		=	0,
	@STUS_ID	INT		=	0
AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRY

	OPEN SYMMETRIC KEY FS@K3y
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
		DECLARE @SDETbl TABLE (
			SDEOpportunityID		INT
			, StatusID				INT
			, StatusDesc			VARCHAR(50)
			, CompanyName			VARCHAR(MAX)
			, ContactName			VARCHAR(MAX)
			, CreatedDateTime		DATETIME
			, DisplayDateTime		DATETIME
			, CreatedByFullName		VARCHAR(100)
			, CreatedByUserID		INT
			, AssignedToID			INT
			, AssignedTo			VARCHAR(100)
		)
		
		INSERT INTO @SDETbl
		SELECT DISTINCT sde.SDE_OPPTNTY_ID
						, sde.SDE_STUS_ID
						, stus.STUS_DES
						, ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'') AS CMPNY_NME
						, ISNULL(dbo.decryptBinaryData(csd.CUST_CNTCT_NME),'') AS CNTCT_NME
						, sde.CREAT_DT
						, CASE
								WHEN sde.MODFD_DT IS NULL  THEN sde.CREAT_DT ELSE sde.MODFD_DT
							END [DISP_DT]
						, usr.FULL_NME
						, sde.CREAT_BY_USER_ID
						, sde.ASN_TO_USER_ID
						, ass.FULL_NME
		FROM dbo.SDE_OPPTNTY sde WITH (NOLOCK)
		INNER JOIN dbo.LK_STUS stus WITH (NOLOCK) ON stus.STUS_ID = sde.SDE_STUS_ID
		INNER JOIN dbo.LK_USER usr WITH (NOLOCK) ON usr.USER_ID = sde.CREAT_BY_USER_ID
		INNER JOIN dbo.LK_USER ass WITH (NOLOCK) ON ass.USER_ID = sde.ASN_TO_USER_ID
		INNER JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=sde.SDE_OPPTNTY_ID AND csd.SCRD_OBJ_TYPE_ID=26
		WHERE sde.REC_STUS_ID = 1
		ORDER BY [DISP_DT] DESC

		-- Return records by @USER_ID and @STUS_ID
		IF (ISNULL(@USER_ID, 0) != 0 AND ISNULL(@STUS_ID, 0) != 0)
			SELECT * FROM @SDETbl WHERE StatusID = @STUS_ID 
				AND (CreatedByUserID = @USER_ID OR AssignedToID = @USER_ID)
		
		-- Return records by @USER_ID
		ELSE IF (ISNULL(@USER_ID, 0) != 0)
			SELECT * FROM @SDETbl WHERE (CreatedByUserID = @USER_ID OR AssignedToID = @USER_ID)
		
		-- Return records by @STUS_ID
		ELSE IF (ISNULL(@STUS_ID, 0) != 0)
			SELECT * FROM @SDETbl WHERE StatusID = @STUS_ID
		
		-- Return all records
		ELSE
			SELECT * FROM @SDETbl

	END TRY  
  
	BEGIN CATCH  
		EXEC [dbo].[insertErrorInfo]  
	END CATCH  
  
END