USE [COWS]
GO
_CreateObject 'SP','dbo','getM5BillDsptchMsgs'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		jrg7298
-- Create date: 10/01/2019
-- Description:	Get all Billable Dispatch messages for Mach5 Sender
-- =========================================================
ALTER PROCEDURE [dbo].[getM5BillDsptchMsgs]
AS
BEGIN
SET NOCOUNT ON;
Begin Try

SET XACT_ABORT ON;

BEGIN TRANSACTION
declare @tmpids table (tmpid int)
IF OBJECT_ID(N'tempdb..#BDRTbl', N'U') IS NOT NULL         
				DROP TABLE #BDRTbl 
IF OBJECT_ID(N'tempdb..#M5Tbl', N'U') IS NOT NULL         
				DROP TABLE #M5Tbl 

CREATE TABLE #BDRTbl (ID INT NOT NULL, [CUST_ID] [CHAR](9) NOT NULL,
	[CLEAR_LOC] VARCHAR(10) NOT NULL,
	[DISP_CD] [varchar](20) NOT NULL,
	[DISP_CAT_NAME] [varchar](50) NOT NULL,
	[DISP_SUBCAT_NAME] [varchar](255) NOT NULL,
	[DISP_CA_NAME] [varchar](255) NOT NULL,
	[CLS_DT] DATE NOT NULL,
	[TICK_NBR] [varchar](20) NOT NULL)

CREATE TABLE #M5Tbl ([CUST_ID] [CHAR](9) NOT NULL,
[INTL_CD] CHAR(1) NULL
)

INSERT INTO #BDRTbl (ID, [CUST_ID], [CLEAR_LOC], [DISP_CD], [DISP_CAT_NAME], [DISP_SUBCAT_NAME], [DISP_CA_NAME], [CLS_DT], [TICK_NBR])
EXEC [dbo].[getBillDsptchData] 1

INSERT INTO dbo.[BILL_DSPTCH] ([CUST_ID], [CLEAR_LOC], [DISP_CD], [DISP_CAT_NAME], [DISP_SUBCAT_NAME], [DISP_CA_NAME], [CLS_DT], [TICK_NBR], [CMNT_TXT], [STUS_ID], [CREAT_BY_USER_ID]) output inserted.BILL_DSPTCH_ID into @tmpids
SELECT [CUST_ID], [CLEAR_LOC], [DISP_CD], [DISP_CAT_NAME], [DISP_SUBCAT_NAME], [DISP_CA_NAME], [CLS_DT], [TICK_NBR], 'Systematically sent to M5 as there was no user action to accept or reject', 241, 1
FROM #BDRTbl bt
WHERE NOT EXISTS (SELECT 'X' FROM dbo.[BILL_DSPTCH] bd WITH (NOLOCK) WHERE bd.[TICK_NBR]=bt.[TICK_NBR])

COMMIT TRANSACTION

INSERT INTO dbo.[M5_BILL_DSPTCH_MSG] ([BILL_DSPTCH_ID], [STUS_ID])
SELECT DISTINCT bd.[BILL_DSPTCH_ID], 10
FROM dbo.[BILL_DSPTCH] bd WITH (NOLOCK)
WHERE ISNULL(bd.[ACPT_RJCT_CD],'') = 'A'
AND bd.[STUS_ID]=241 --Retrieved and Never SentToM5
AND NOT EXISTS (SELECT 'X' FROM dbo.[M5_BILL_DSPTCH_MSG] mb WITH (NOLOCK) WHERE bd.[BILL_DSPTCH_ID]=mb.[BILL_DSPTCH_ID])
UNION
SELECT DISTINCT tmpid, 10
FROM @tmpids

INSERT INTO #M5Tbl (CUST_ID)
SELECT DISTINCT bd.CUST_ID
FROM dbo.[M5_BILL_DSPTCH_MSG] mbd WITH (NOLOCK) INNER JOIN
dbo.[BILL_DSPTCH] bd WITH (NOLOCK) ON mbd.BILL_DSPTCH_ID=bd.BILL_DSPTCH_ID
WHERE mbd.[STUS_ID]=10

DECLARE @h6s nVARCHAR(MAX) = null
			select @h6s = ISNULL(@h6s,'') + ISNULL(CUST_ID,'') + ''''', '''''
			from #M5Tbl
			if(len(@h6s)>2)
			set  @h6s = substring(@h6s, 1, len(@h6s)-6)

DECLARE @updtIntl nVARCHAR(MAX) = 'UPDATE mt
								   SET [INTL_CD] = x.DOM_INTL_IND_CD
								   FROM #M5Tbl mt INNER JOIN
								   (select cis_cust_id, DOM_INTL_IND_CD from openquery(m5,''select cis_cust_id, DOM_INTL_IND_CD from mach5.v_v5u_cust_acct where cis_cust_id IN (''''' + @h6s + ''''') '')) as x on x.cis_cust_id=mt.CUST_ID'

exec sp_executesql @updtIntl

SELECT DISTINCT mbd.M5_BILL_DSPTCH_MSG_ID,
bd.BILL_DSPTCH_ID,
bd.CUST_ID,
--bd.TICK_NBR + ' - ' + bd.DISP_CAT_NAME + ' ' + bd.DISP_SUBCAT_NAME + ' ' + bd.DISP_CA_NAME AS TICK_DESC,
'Miscellaneous Dispatch Request' AS TICK_DESC,
bd.TICK_NBR,
bd.CLS_DT,
CASE WHEN mt.INTL_CD='I' THEN ISNULL((SELECT TOP 1 ISNULL(PRMTR_VALU_TXT,'950.00') FROM dbo.LK_SYS_CFG WITH (NOLOCK) WHERE PRMTR_NME='M5IntlBillDsptchCharge'), '950.00')
ELSE ISNULL((SELECT TOP 1 ISNULL(PRMTR_VALU_TXT,'500.00') FROM dbo.LK_SYS_CFG WITH (NOLOCK) WHERE PRMTR_NME='M5BillDsptchCharge'), '500.00') END as [CHARGE],
CASE WHEN mt.INTL_CD='I' THEN ISNULL((SELECT TOP 1 ISNULL(PRMTR_VALU_TXT,'200011821') FROM dbo.LK_SYS_CFG WITH (NOLOCK) WHERE PRMTR_NME='M5IntlBillDsptchBEC'), '200011821')
ELSE ISNULL((SELECT TOP 1 ISNULL(PRMTR_VALU_TXT,'200000087') FROM dbo.LK_SYS_CFG WITH (NOLOCK) WHERE PRMTR_NME='M5BillDsptchBEC'), '200000087') END AS BEC
FROM dbo.[M5_BILL_DSPTCH_MSG] mbd WITH (NOLOCK) INNER JOIN
dbo.[BILL_DSPTCH] bd WITH (NOLOCK) ON mbd.BILL_DSPTCH_ID=bd.BILL_DSPTCH_ID INNER JOIN
#M5Tbl mt WITH (NOLOCK) ON mt.CUST_ID = bd.CUST_ID
WHERE mbd.[STUS_ID]=10

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END