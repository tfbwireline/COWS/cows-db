USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertODIEReq_V5U]    Script Date: 11/14/2016 09:06:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 11/14/2016
-- Description:	sets up 3rd Party Techs for order assignments.
-- =========================================================
CREATE PROCEDURE [dbo].[insertNewTech_V5U]
			@ADID varchar (10)        
       
AS
BEGIN
SET NOCOUNT ON;


DECLARE	@Tech_FULL_NME varchar(100)

BEGIN TRY

	SELECT @Tech_FULL_NME = FULL_NME FROM LK_USER WITH (NOLOCK)
			WHERE USER_ADID = @ADID
			
	IF ISNULL(@Tech_FULL_NME,'') <> ''
		BEGIN
			INSERT INTO [COWS].[dbo].[USER_CPE_TECH]
					([USER_CPE_TECH_ADID],[USER_CPE_TECH_NME],[REC_STUS_ID],[CREAT_DT])
			 VALUES
					(@ADID,@Tech_FULL_NME,1,GETDATE())
        END

END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH
END

