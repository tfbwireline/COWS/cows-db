USE [COWS]
GO
_CreateObject 'SP','dbo','updateODIEDiscoResponse'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		sbg9814
-- Create date: 03/30/2012
-- Description:	Updates the ODIE Disco Response into the ODIE_DISC_REQ table.
-- =========================================================
ALTER PROCEDURE [dbo].[updateODIEDiscoResponse]
	@REQ_ID				Int
	,@ODIE_DISC_REQ_ID	Int
	,@H5_H6_CUST_ID		Int
	,@VNDR_NME			Varchar(50)
	,@MODEL_NME			Varchar(50)
	,@SERIAL_NBR		Varchar(50)
	,@M5_DEVICE_ID		Varchar(50)=''
	,@SITE_ID			Varchar(50)=''
	,@CTRCT_NBR			Varchar(50)=''
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	UPDATE		dbo.ODIE_DISC_REQ	WITH (ROWLOCK)
		SET		H5_H6_CUST_ID		=	@H5_H6_CUST_ID
				,VNDR_NME			=	@VNDR_NME
				,MODEL_NME			=	@MODEL_NME
				,SERIAL_NBR			=	@SERIAL_NBR
				,M5_DEVICE_ID		=	@M5_DEVICE_ID
				,SITE_ID			=	@SITE_ID
				,CTRCT_NBR			=	@CTRCT_NBR
				,STUS_ID			=	1
		WHERE	ODIE_DISC_REQ_ID	=	@ODIE_DISC_REQ_ID
End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END
GO