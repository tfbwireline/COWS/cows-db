USE COWS
GO
_CreateObject 'SP','web','insertODIEDiscoRequest'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================              
-- Author:  Suman Chitemella              
-- Create date: 2011-8-06              
-- Description: This SP is used to insert MDS Event Disco Request to ODIE      
-- =========================================================              
ALTER PROCEDURE [web].[insertODIEDiscoRequest]             
 @DISCOE NVARCHAR(MAX),            
 @H1_CUST_ID VARCHAR(9),  
 @ID INT OUTPUT             
AS            
           
  DECLARE @hDoc   INT    
  DECLARE @iReqID BIGINT  
  SET @ID = 0
    
  DECLARE @ODIE_DISC_REQ TABLE (  
 REQ_ID BIGINT,  
 DEV_NME VARCHAR(150)  
  )  
   
BEGIN            
 SET NOCOUNT ON;            
 BEGIN TRY            
                 
    IF (LEN(@DISCOE) > 10)            
 BEGIN            
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @DISCOE     
  --select top 1 * from ODIE_DISC_REQ  
  INSERT INTO dbo.ODIE_REQ (ORDR_ID, ODIE_MSG_ID, STUS_MOD_DT, H1_CUST_ID, CREAT_DT, STUS_ID)  
   VALUES (NULL, 4, GETDATE(), @H1_CUST_ID, GETDATE(), 9)  
    
   SELECT @iReqID = SCOPE_IDENTITY()   
   
   IF ((CAST(@DISCOE as XML).exist('/DISCOE/Device')) =1)
	BEGIN
	   INSERT INTO @ODIE_DISC_REQ  
	   SELECT  @iReqID, ODIEDeviceName  
	   FROM OPENXML (@hDoc, '/DISCOE/Device',2)  
	   WITH (ODIEDeviceName VARCHAR(150))  
	END
	ELSE
   BEGIN
	   INSERT INTO @ODIE_DISC_REQ  
	   SELECT @iReqID, ODIE_DEV_NME  
	   FROM OPENXML (@hDoc, '/DocumentElement/DISCOE', 2)  
	   WITH (ODIE_DEV_NME VARCHAR(150))  
   END
     
  INSERT INTO dbo.ODIE_DISC_REQ (REQ_ID, DEV_NME)    
   Select DISTINCT REQ_ID, SUBSTRING(DEV_NME,1,50) FROM @ODIE_DISC_REQ  
     
  UPDATE dbo.ODIE_REQ WITH (ROWLOCK) SET STUS_ID = 10 WHERE REQ_ID = @iReqID   
    
  SELECT @ID = @iReqID    
    
 END    
     
 END TRY            
 BEGIN CATCH            
  EXEC [dbo].[insertErrorInfo]              
  DECLARE @ErrMsg nVarchar(4000),               
       @ErrSeverity Int                    
  SELECT @ErrMsg  = ERROR_MESSAGE(),                    
      @ErrSeverity = ERROR_SEVERITY()                    
  RAISERROR(@ErrMsg, @ErrSeverity, 1)                    
 END CATCH              
END