USE [COWS]
GO
_CreateObject 'SP','dbo','InsertGOMMissBillOnlyTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		csb9923
-- Create date: 09/29/2011
-- Description:	Move the Cancels/Disonnect orders to GOM Missing Billing Orders 
--if the Bill only order is not recived in 5 business days after completion
-- =========================================================
ALTER PROCEDURE [dbo].[InsertGOMMissBillOnlyTask]
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	
DECLARE @tblBMO TABLE
(
	ORDR_ID INT,
	FTN		varchar(20),
	Flag	BIT	DEFAULT	0,
	ORDR_TYPE_CD VARCHAR(2) 
)
	
DECLARE @FTN	varchar(20)
DECLARE @ORDR_ID INT
DECLARE @ORDR_TYPE_CD VARCHAR(2) 
DECLARE @iCnt	INT
DECLARE @iCtr	INT
SET @ORDR_TYPE_CD = ''
SET @iCtr	= 0
SET @iCnt	= 0


INSERT INTO @tblBMO (ORDR_ID,FTN,ORDR_TYPE_CD)
	SELECT		DISTINCT o.ORDR_ID,f.FTN,ISNULL(f.ORDR_TYPE_CD,'')
		FROM	dbo.ORDR			o  WITH (NOLOCK)
	INNER JOIN	dbo.LK_PPRT			lp WITH (NOLOCK) ON o.PPRT_ID		=	lp.PPRT_ID
	INNER JOIN	dbo.ACT_TASK		at WITH (NOLOCK) ON o.ORDR_ID		=	at.ORDR_ID
	INNER JOIN	dbo.LK_ORDR_TYPE	lo WITH (NOLOCK) ON	lo.ORDR_TYPE_ID	=	lp.ORDR_TYPE_ID
	INNER JOIN	dbo.LK_PROD_TYPE	lt WITH (NOLOCK) ON lt.PROD_TYPE_ID =	lp.PROD_TYPE_ID
	LEFT JOIN	dbo.FSA_ORDR		f  WITH (NOLOCK) ON	f.ORDR_ID		=	o.ORDR_ID
		WHERE	at.TASK_ID					=	1001
			AND	at.STUS_ID					=	0
			AND lo.ORDR_TYPE_ID				IN	(7,8) --Disconnects and Cancels
			AND o.DMSTC_CD					=	1
			AND	lt.TRPT_CD					=	1
			AND	o.ORDR_CAT_ID				=	2
			AND dbo.getBusinessDays(at.CREAT_DT,GETDATE())	>= 5
			AND NOT EXISTS
				(
					SELECT		'X'
						FROM	dbo.ORDR_NTE n WITH (NOLOCK)
						WHERE	n.ORDR_ID	=	o.ORDR_ID
							AND	n.NTE_TXT	=	'Validated Cancel/Disconnect order for missing billing order.'
				)
	

IF ((Select COUNT(1) FROM @tblBMO) > 0)
	BEGIN
		SELECT @iCnt = COUNT(1) FROM @tblBMO
			WHILE (@iCtr < @iCnt)
				BEGIN
					SET @FTN	=	''
					SELECT TOP 1 @FTN		=	ISNULL(FTN,''),
								 @ORDR_ID	=	ORDR_ID,
								 @ORDR_TYPE_CD	= ORDR_TYPE_CD
						FROM	@tblBMO
						WHERE	Flag = 0
						
					IF @FTN <> '' AND @ORDR_ID <> 0
						BEGIN
							IF NOT EXISTS
								(
									SELECT		'X'
										FROM	dbo.FSA_ORDR WITH (NOLOCK)
										WHERE	((RELTD_FTN	=	@FTN AND @ORDR_TYPE_CD = 'BC') OR (PRNT_FTN = @FTN AND @ORDR_TYPE_CD = 'CN'))
								)
								BEGIN
									DELETE FROM dbo.ACT_TASK WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID AND TASK_ID = 109
								
									INSERT INTO ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID)
									VALUES  (@ORDR_ID,109,0,100)
								END
							INSERT INTO ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
							VALUES (9,@ORDR_ID,GETDATE(),1,1,'Validated Cancel/Disconnect order for missing billing order.')
						END	
					UPDATE @tblBMO SET Flag = 1 WHERE ORDR_ID = @ORDR_ID AND FTN = @FTN AND Flag = 0
					SET @iCtr = @iCtr + 1	
				END

	END
	
	
End Try
Begin Catch
	EXEC dbo.insertErrorInfo
End Catch
END


GO


