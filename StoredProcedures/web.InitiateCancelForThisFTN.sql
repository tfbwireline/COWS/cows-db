USE [COWS]
GO
_CreateObject 'SP','web','InitiateCancelForThisFTN'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <09/26/2012>
-- Description:	<Initiating Cancels from COWS>
-- =============================================
ALTER PROCEDURE [web].[InitiateCancelForThisFTN] 
	@FTN	Varchar(20),
	@Notes	Varchar(500) = NULL,
	@UserID	INT
		
AS
BEGIN
DECLARE @TRPT_CD		BIT
DECLARE @PROD_TYPE_ID	TINYINT
DECLARE @DMSTC_CD		BIT
DECLARE @PPRT_ID		INT
DECLARE @ORDR_ID		INT = 0
DECLARE @ORDR_CAT_ID    INT -- 3/13/2017 ADDED
DECLARE @PROD_TYPE_CD   VARCHAR(2) -- 3/13/2017 Added

	BEGIN TRY
		SELECT	@TRPT_CD		=	lp.TRPT_CD,
				@PROD_TYPE_ID	=	lp.PROD_TYPE_ID,
				@DMSTC_CD		=	odr.DMSTC_CD,
				@ORDR_ID		=	ISNULL(odr.ORDR_ID,0),
				@ORDR_CAT_ID    =   odr.ORDR_CAT_ID, -- 3/13/2017 Added
				@PROD_TYPE_CD   =   fsa.PROD_TYPE_CD -- 3/13/2017 Added
			FROM	dbo.FSA_ORDR	fsa WITH (NOLOCK)
		INNER JOIN	dbo.ORDR		odr WITH (NOLOCK) ON odr.ORDR_ID = fsa.ORDR_ID
		INNER JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK) ON lp.PPRT_ID = odr.PPRT_ID
			WHERE	fsa.FTN = @FTN
		
		IF (@ORDR_ID != 0)
		BEGIN
			SELECT	@PPRT_ID	=	PPRT_ID
				FROM	dbo.LK_PPRT	WITH (NOLOCK)
				WHERE	PROD_TYPE_ID	=	@PROD_TYPE_ID
					AND	TRPT_CD			=	@TRPT_CD
					AND	INTL_CD			=	@DMSTC_CD
					AND ORDR_TYPE_ID	=	8	

			IF ISNULL(@Notes,'') <> ''
				BEGIN
					EXEC dbo.insertOrderNotes @ORDR_ID,20,@Notes,@UserID
				END

			IF ISNULL(@PPRT_ID,0)	!= 0	-- INTL Transport and CPE Orders
				AND @ORDR_CAT_ID != 6    -- 3/13/2017 Added
				AND @PROD_TYPE_CD != 'CP'  -- 3/13/2017 Added to process FSA CPE as Non Transport Cancels
				BEGIN
					UPDATE	dbo.ORDR WITH (ROWLOCK)
						SET		PPRT_ID = @PPRT_ID
						WHERE	ORDR_ID	=	@ORDR_ID
					
					EXEC dbo.processTrptCancels	@ORDR_ID
				END
			ELSE	--	Non Transport or Domestic orders
				BEGIN
					EXEC dbo.ProcessNonTransportCancels @ORDR_ID
				END
		END
			
			SELECT @ORDR_ID
	END TRY
	
	BEGIN CATCH
		SELECT 0
		Exec dbo.insertErrorInfo
	END CATCH	
END
