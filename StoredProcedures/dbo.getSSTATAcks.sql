USE [COWS]
GO
_CreateObject 'SP','dbo','getSSTATAcks'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		dlp0278
-- Create date: 10/2/2015
-- Description:	Get the details for all STTAT requests
-- =========================================================
ALTER PROCEDURE [dbo].[getSSTATAcks]

AS
BEGIN
SET NOCOUNT ON;
Begin Try

	SELECT 
	   SSTAT_RSPN_ID
      ,TRAN_ID
      ,ORDR_ID
      ,DEVICE_ID
      ,ORDER_ACTION
      ,CONVERT(VARCHAR,COMPLETION_DATE) AS COMPLETION_DATE
      ,NOTE
      ,ERROR_MSG
      ,CREAT_DT
      ,ACK_CD
      ,ACT_CD
  FROM [dbo].[SSTAT_RSPN] WITH (NOLOCK)
  WHERE ACK_CD = 0
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END


