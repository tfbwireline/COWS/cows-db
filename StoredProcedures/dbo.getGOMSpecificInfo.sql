USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getGOMSpecificInfo]    Script Date: 10/12/2018 10:20:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Exec [dbo].[getGOMSpecificInfo] 60703
-- ===========================================================================
-- Created By:		sbg9814
-- Created Date:	08/24/2011
-- Description:		Gets the GOM Info for a given Order.
-- ===========================================================================       
ALTER PROCEDURE [dbo].[getGOMSpecificInfo]
	@OrderID	Int
	,@GRP_ID	Int	=	1
AS      
BEGIN
SET DEADLOCK_PRIORITY 5
Begin Try
	DECLARE @tbl TABLE 
			(
				ORDR_ID		int
				,SHPMT_TRK_NBR 	varchar(30)
				,VNDR_SHIP_DT	smalldatetime
				,DRTBN_SHIP_DT	smalldatetime
				,CUST_DLVRY_DT	smalldatetime
				,CSTM_DT		smalldatetime
				,CREAT_BY_USER_ID	int
				,HDWR_DLVRD_DT		smalldatetime
				,EQPT_INSTL_DT		smalldatetime
				,CPE_STUS_ID		smallint
				,CPE_CMPLT_DT		smalldatetime
				,ATLAS_WRK_ORDR_NBR	varchar(15)
				,PLSFT_RQSTN_NBR	varchar(15)
				,RQSTN_AMT			money
				,RQSTN_DT			smalldatetime
				,PRCH_ORDR_NBR		varchar(15)
				,PRCH_ORDR_BACK_ORDR_SHIP_DT	smalldatetime
				,VNDR_RECV_FROM_DT	smalldatetime	
				,VNDR_COURIER_TRK_NBR	varchar(50)
				,PLSFT_RELS_DT		smalldatetime
				,PLSFT_RELS_ID		varchar(15)
				,PLSFT_RCPT_ID		varchar(10)
				,COURIER_TRK_NBR	varchar(50)
				,CPE_EQPT_TYPE_TXT	varchar(30)
				,CPE_VNDR_NME		varchar(30)
				,SHPMT_SIGN_BY_NME	varchar(30)
				,BILL_FTN			varchar(20)
				,CUST_PRCH_CD		bit
				,CUST_RNTL_CD		bit
				,CUST_RNTL_TERM_NBR	smallint
				,LOGICALIS_MNTH_LEASE_RT_AMT money
				,TERM_STRT_DT		smalldatetime
				,TERM_END_DT		smalldatetime
				,TERM_60_DAY_NTFCTN_DT	smalldatetime
				,CUST_RNL_CD		bit
				,CUST_RNL_TERM_NBR	smallint
				,LOGICALIS_RNL_MNTH_LEASE_RT_AMT	money
				,CUST_DSCNCT_CD		bit
				,CUST_DSCNCT_DT		smalldatetime
				)
	
		INSERT INTO @tbl 
			(
				ORDR_ID		
				,SHPMT_TRK_NBR 	
				,VNDR_SHIP_DT	
				,DRTBN_SHIP_DT	
				,CUST_DLVRY_DT	
				,CSTM_DT		
				,CREAT_BY_USER_ID	
				,HDWR_DLVRD_DT		
				,EQPT_INSTL_DT		
				,CPE_STUS_ID		
				,CPE_CMPLT_DT		
				,ATLAS_WRK_ORDR_NBR	
				,PLSFT_RQSTN_NBR	
				,RQSTN_AMT			
				,RQSTN_DT			
				,PRCH_ORDR_NBR		
				,PRCH_ORDR_BACK_ORDR_SHIP_DT	
				,VNDR_RECV_FROM_DT	
				,VNDR_COURIER_TRK_NBR	
				,PLSFT_RELS_DT		
				,PLSFT_RELS_ID		
				,PLSFT_RCPT_ID		
				,COURIER_TRK_NBR	
				,CPE_EQPT_TYPE_TXT	
				,CPE_VNDR_NME		
				,SHPMT_SIGN_BY_NME	
				,BILL_FTN
				,CUST_PRCH_CD
				,CUST_RNTL_CD
				,CUST_RNTL_TERM_NBR
				,LOGICALIS_MNTH_LEASE_RT_AMT
				,TERM_STRT_DT
				,TERM_END_DT
				,TERM_60_DAY_NTFCTN_DT
				,CUST_RNL_CD
				,CUST_RNL_TERM_NBR
				,LOGICALIS_RNL_MNTH_LEASE_RT_AMT
				,CUST_DSCNCT_CD
				,CUST_DSCNCT_DT			
			)
		SELECT	TOP 1 
			ORDR_ID
			,SHPMT_TRK_NBR
			,VNDR_SHIP_DT
			,DRTBN_SHIP_DT
			,CUST_DLVRY_DT
			,CSTM_DT
			,CREAT_BY_USER_ID
			,HDWR_DLVRD_DT
			,EQPT_INSTL_DT
			,CPE_STUS_ID
			,CPE_CMPLT_DT
			,ATLAS_WRK_ORDR_NBR
			,PLSFT_RQSTN_NBR
			,'$' + Convert(Varchar(15), CAST(RQSTN_AMT as Decimal(10,2))) AS	RQSTN_AMT
			,RQSTN_DT
			,PRCH_ORDR_NBR
			,PRCH_ORDR_BACK_ORDR_SHIP_DT
			,VNDR_RECV_FROM_DT
			,VNDR_COURIER_TRK_NBR
			,PLSFT_RELS_DT
			,PLSFT_RELS_ID
			,PLSFT_RCPT_ID
			,COURIER_TRK_NBR
			,CPE_EQPT_TYPE_TXT
			,CPE_VNDR_NME
			,SHPMT_SIGN_BY_NME
			,ISNULL(BILL_FTN,'') AS BILL_FTN
			,CUST_PRCH_CD
			,CUST_RNTL_CD
			,CUST_RNTL_TERM_NBR
			,LOGICALIS_MNTH_LEASE_RT_AMT
			,TERM_STRT_DT
			,TERM_END_DT
			,TERM_60_DAY_NTFCTN_DT
			,CUST_RNL_CD
			,CUST_RNL_TERM_NBR
			,LOGICALIS_RNL_MNTH_LEASE_RT_AMT
			,CUST_DSCNCT_CD
			,CUST_DSCNCT_DT	
	FROM	dbo.FSA_ORDR_GOM_XNCI	WITH (NOLOCK)
	WHERE	ORDR_ID	=	@OrderID
	 -- AND   FSA_CPE_LINE_ITEM_ID IS NULL  dlp0278 commented 10/12/2018
			/***** most of the fields appear to be CPE related and all CPE orders have FSA_CPE_LINE_ITEM_ID
			    populated.  Access and Port now have FSA_CPE_LINE_ITEM records so I removed this condition.***/

	IF ((SELECT COUNT(1) FROM @tbl) = 1 )
		BEGIN
			UPDATE	t
				SET	t.SHPMT_TRK_NBR		=	GOM.SHPMT_TRK_NBR  
					,t.VNDR_SHIP_DT		=	GOM.VNDR_SHIP_DT
					,t.DRTBN_SHIP_DT	=	GOM.DRTBN_SHIP_DT
					,t.CUST_DLVRY_DT	=	XNCI.CUST_DLVRY_DT
					,t.CSTM_DT			=	GOM.CSTM_DT
					,t.CREAT_BY_USER_ID	=	GOM.CREAT_BY_USER_ID
					,t.HDWR_DLVRD_DT	=	XNCI.HDWR_DLVRD_DT
					,t.EQPT_INSTL_DT	=	XNCI.EQPT_INSTL_DT
					,t.CPE_STUS_ID		=	GOM.CPE_STUS_ID
					,t.CPE_CMPLT_DT		=	GOM.CPE_CMPLT_DT
					,t.ATLAS_WRK_ORDR_NBR	=	GOM.ATLAS_WRK_ORDR_NBR
					,t.PLSFT_RQSTN_NBR		=	GOM.PLSFT_RQSTN_NBR
					,t.RQSTN_AMT			=	'$' + Convert(Varchar(15), CAST(GOM.RQSTN_AMT as Decimal(10,2))) 
					,t.RQSTN_DT				=	GOM.RQSTN_DT
					,t.PRCH_ORDR_NBR		=	GOM.PRCH_ORDR_NBR
					,t.PRCH_ORDR_BACK_ORDR_SHIP_DT	=	GOM.PRCH_ORDR_BACK_ORDR_SHIP_DT
					,t.VNDR_RECV_FROM_DT			=	GOM.VNDR_RECV_FROM_DT
					,t.VNDR_COURIER_TRK_NBR			=	GOM.VNDR_COURIER_TRK_NBR
					,t.PLSFT_RELS_DT				=	GOM.PLSFT_RELS_DT
					,t.PLSFT_RELS_ID				=	GOM.PLSFT_RELS_ID
					,t.PLSFT_RCPT_ID				=	GOM.PLSFT_RCPT_ID
					,t.COURIER_TRK_NBR				=	GOM.COURIER_TRK_NBR
					,t.CPE_EQPT_TYPE_TXT			=	GOM.CPE_EQPT_TYPE_TXT
					,t.CPE_VNDR_NME					=	GOM.CPE_VNDR_NME
					,t.SHPMT_SIGN_BY_NME			=	XNCI.SHPMT_SIGN_BY_NME
					,t.BILL_FTN						=	ISNULL(GOM.BILL_FTN,'') 
					,t.CUST_PRCH_CD					=	GOM.CUST_PRCH_CD
					,t.CUST_RNTL_CD					=	GOM.CUST_RNTL_CD
					,t.CUST_RNTL_TERM_NBR			=	GOM.CUST_RNTL_TERM_NBR
					,t.LOGICALIS_MNTH_LEASE_RT_AMT	=	GOM.LOGICALIS_MNTH_LEASE_RT_AMT
					,t.TERM_STRT_DT					=	GOM.TERM_STRT_DT
					,t.TERM_END_DT					=	GOM.TERM_END_DT
					,t.TERM_60_DAY_NTFCTN_DT		=	GOM.TERM_60_DAY_NTFCTN_DT
					,t.CUST_RNL_CD					=	GOM.CUST_RNL_CD
					,t.CUST_RNL_TERM_NBR			=	GOM.CUST_RNL_TERM_NBR
					,t.LOGICALIS_RNL_MNTH_LEASE_RT_AMT=	GOM.LOGICALIS_RNL_MNTH_LEASE_RT_AMT
					,t.CUST_DSCNCT_CD				=	GOM.CUST_DSCNCT_CD
					,t.CUST_DSCNCT_DT				=	GOM.CUST_DSCNCT_DT
				FROM	@tbl t 
			LEFT JOIN	dbo.FSA_ORDR_GOM_XNCI GOM WITH (NOLOCK) ON	t.ORDR_ID	=	GOM.ORDR_ID
																AND	GOM.GRP_ID	=	1
			LEFT JOIN	dbo.FSA_ORDR_GOM_XNCI XNCI WITH (NOLOCK) ON	t.ORDR_ID	=	XNCI.ORDR_ID
																AND	XNCI.GRP_ID	IN	(5,6,7)
		END

	SELECT * FROM @tbl
	
	SELECT TOP 1 nac.NID_SERIAL_NBR, ims.IP_ADR as NID_IP
	FROM dbo.FSA_ORDR fo WITH (NOLOCK) INNER JOIN
	     dbo.NID_ACTY nac WITH (NOLOCK) ON nac.M5_ORDR_NBR = fo.FTN and nac.REC_STUS_ID=251 left join
		dbo.IP_ACTY iac WITH (NOLOCK) ON nac.NID_ACTY_ID = iac.NID_ACTY_ID AND iac.REC_STUS_ID=251 left join
		dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID = iac.IP_MSTR_ID AND ims.REC_STUS_ID IN (251,253)
	WHERE  fo.ORDR_ID=@OrderID
	ORDER BY nac.CREAT_DT DESC
	

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo] 
End Catch		
END