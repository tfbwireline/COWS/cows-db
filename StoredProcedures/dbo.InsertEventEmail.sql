USE [COWS]
GO
_CreateObject 'SP','dbo','InsertEventEmail'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jrg7298
-- Create date: 09/01/2011
-- Description:	Inserts an Event Email Request to be send.
-- =============================================
ALTER PROCEDURE [dbo].[InsertEventEmail] 
	@EVENT_ID			Int
	,@STUS_ID			Int
	,@sTo	Varchar(max)	=	NULL	
	,@sCC	Varchar(1000)	=	NULL	
	,@sSubj	Varchar(1000)	=	NULL	
	,@sBody	Varchar(max)	=	NULL	

AS
BEGIN
Begin Try
	INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
				(EVENT_ID
				,EMAIL_REQ_TYPE_ID
				,STUS_ID
				,EMAIL_LIST_TXT
				,EMAIL_CC_TXT
				,EMAIL_SUBJ_TXT
				,EMAIL_BODY_TXT)
		VALUES	(@EVENT_ID
				,6
				,Case	ISNULL(@sTo, '')
					When	''	Then	9				
					Else	10
				End
				,@sTo
				,@sCC
				,@sSubj
				,@sBody)
			
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
