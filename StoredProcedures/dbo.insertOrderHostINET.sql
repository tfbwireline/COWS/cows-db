USE [COWS]
GO
_CreateObject 'SP','dbo','insertOrderHostINET'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		mjl0081
-- Create date: 04/28/2015
-- Description:	Inserts an IP Address into the ORDR_HOST_INET table.
-- =========================================================
ALTER PROCEDURE [dbo].insertOrderHostINET
	@ORDR_ID	INT
	,@IP_ADR	VARCHAR(50)
	,@CREAT_DT	DATE
AS
BEGIN
SET NOCOUNT OFF;

BEGIN TRY
	INSERT INTO dbo.ORDR_HOST_INET
					(ORDR_ID
					,IP_ADR
					,CREAT_DT				
					)
		VALUES		(@ORDR_ID
					,@IP_ADR
					,@CREAT_DT	
					)		
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
