USE [COWS]
GO
_CreateObject 'SP','dbo','insertWFMOrderAssignment'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/11/2011
-- Description:	Inserts the FSA record into the FSA_ORDR table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertWFMOrderAssignment]
	@ORDR_ID			Int
	,@GRP_ID			Int
	,@ASN_USER			Varchar(30)
	,@ASN_BY_USER_ID	Int
	,@ASMT_DT			DateTime	
	,@ASN_USER_ID		Int	OUTPUT
AS
BEGIN
SET NOCOUNT ON;
SET	@ASN_USER_ID	=	0

Begin Try
	SELECT		@ASN_USER_ID	=	ISNULL(USER_ID, 0)
		FROM	dbo.LK_USER	WITH (NOLOCK)
		WHERE	USER_ADID	=	@ASN_USER
		
	IF @ASN_USER_ID	!=	0
		BEGIN	
			IF	NOT EXISTS
				(SELECT 'X' FROM dbo.USER_WFM_ASMT WITH (NOLOCK)
					WHERE	ORDR_ID	=	@ORDR_ID
						AND	GRP_ID	=	@GRP_ID)
				BEGIN
					INSERT INTO dbo.USER_WFM_ASMT WITH (ROWLOCK)
									(ORDR_ID
									,GRP_ID
									,ASN_USER_ID
									,ASN_BY_USER_ID
									,ASMT_DT,[USR_PRF_ID])
						SELECT		@ORDR_ID
									,@GRP_ID
									,@ASN_USER_ID
									,@ASN_BY_USER_ID
									,@ASMT_DT
									,(SELECT TOP 1 WFM_PRF_ID FROM dbo.MAP_WFM_PRF WITH (NOLOCK) WHERE GRP_ID=GRP_ID)
						FROM	dbo.ORDR	WITH (NOLOCK)
						WHERE	ORDR_ID	=	@ORDR_ID		
				END
			ELSE
				BEGIN
					UPDATE		dbo.USER_WFM_ASMT WITH (ROWLOCK)
						SET		ASN_USER_ID		=	@ASN_USER_ID
								,ASN_BY_USER_ID	=	@ASN_BY_USER_ID
						WHERE	ORDR_ID			=	@ORDR_ID
							AND	GRP_ID			=	@GRP_ID
				END
		END
End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END