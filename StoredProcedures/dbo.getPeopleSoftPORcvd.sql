USE [COWS]
GO
_CreateObject 'SP','dbo','getPeopleSoftPORcvd'
GO
/****** Object:  StoredProcedure [dbo].[getPeopleSoftPORcvd]    Script Date: 09/22/2016 09:18:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 08/24/2015
-- Description:	Gets Eqp Receipt data for PeopleSoft Batch process.  
-- =========================================================
ALTER PROCEDURE [dbo].[getPeopleSoftPORcvd]
        
AS
BEGIN TRY
				
	SELECT  Convert(varchar (9),PO_RCVD_ID) AS [PO_RCVD_ID]
		   ,ppr.PRCH_ORDR_NBR
		   ,Convert(varchar (4),li.PO_LN_NBR) AS [PO_LN_NBR]	
		   ,ppr.ORDR_ID
		   ,ppr.REQSTN_NBR
		   ,ppr.FSA_CPE_LINE_ITEM_ID
           ,ppr.RCVD_QTY
           ,li.ORDR_QTY
           ,RIGHT(Convert(varchar(10),ppr.RCVD_DT,112),6) AS [RCVD_DT]
           
           ,RIGHT('0' + CONVERT(VARCHAR (2),DATEPART(HH,ppr.RCVD_DT)),2) +
			RIGHT('0' + CONVERT(VARCHAR (2),DATEPART(MINUTE, ppr.RCVD_DT)),2) +
			RIGHT('0' + CONVERT(VARCHAR (2), DATEPART(SS, ppr.RCVD_DT)),2) AS [RCVD_TIME]           
    FROM dbo.PS_RCPT_QUEUE ppr WITH (NOLOCK)
     INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM li WITH (NOLOCK) 
		ON ppr.FSA_CPE_LINE_ITEM_ID = li.FSA_CPE_LINE_ITEM_ID
    WHERE ISNULL(PS_SENT_DT,'') = ''	
    
    
    
 
		
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH


