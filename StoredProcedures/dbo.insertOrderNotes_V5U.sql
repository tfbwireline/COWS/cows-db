USE [COWS]
GO
_CreateObject 'SP','dbo','insertOrderNotes_V5U'
GO
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		dlp0278
-- Create date: 07/20/2015
-- Description:	Inserts the Appian Notes into the ORDR_NTE table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertOrderNotes_V5U]
	@ORDR_ID		Int	
	,@NTE_TYPE_ID	TinyInt
	,@NTE_TXT		Varchar(Max) = ''
	,@CREAT_BY_USER_ADID	VARCHAR(10)= null
	
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY

    DECLARE @CREAT_BY_USER_ID int
	
	SET @CREAT_BY_USER_ADID = LTRIM(RTRIM(ISNULL(@CREAT_BY_USER_ADID,'')))

	IF (@CREAT_BY_USER_ADID = '')
		SET @CREAT_BY_USER_ID = 1
	ELSE
	    BEGIN
			SELECT @CREAT_BY_USER_ID = USER_ID
				FROM dbo.LK_USER WITH (NOLOCK)
			WHERE @CREAT_BY_USER_ADID = USER_ADID
	    END

	
	 
	IF @NTE_TXT IS NOT NULL
	BEGIN
		INSERT INTO dbo.ORDR_NTE WITH (ROWLOCK)
						(ORDR_ID
						,NTE_TYPE_ID
						,NTE_TXT
						,CREAT_BY_USER_ID)
			VALUES		(@ORDR_ID
						,@NTE_TYPE_ID
						,SUBSTRING(@NTE_TXT,1,1000)
						,ISNULL(@CREAT_BY_USER_ID,1))
	END
END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH					
END
