USE [COWS]
GO
_CreateObject 'SP','dbo','insertOrderContactInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/13/2011
-- Description:	Inserts the FSA record into the ORDR_CNTCT table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertOrderContactInfo]
	@ORDR_CNTCT_ID			Int		OUT
	,@ORDR_ID				Int
	,@CNTCT_TYPE_ID			TinyInt
	,@FRST_NME				Varchar(50)
	,@LST_NME				Varchar(50)
	,@NME					Varchar(50)	=	NULL
	,@PHN_NBR				Varchar(20)
	,@EMAIL_ADR				Varchar(200)
	,@TME_ZONE_ID			Varchar(5)
	,@INTPRTR_CD			Bit
	,@CREAT_BY_USER_ID		Int
	,@FAX_NBR				Varchar(20)
	,@ROLE_ID				TinyInt
	,@CIS_LVL_TYPE			Varchar(2)
	,@FSA_MDUL_ID			Varchar(3)
	,@NPA					Varchar(3)	=	NULL
	,@NXX					Varchar(3)	=	NULL
	,@STN_NBR				Varchar(4)	=	NULL
	,@CTY_CD				Varchar(5)	=	NULL
	,@ISD_CD				Varchar(3)	=	NULL
	,@PHN_EXT_NBR			Varchar(10)	=	NULL
	,@FSA_TME_ZONE_CD		Varchar(50) =	NULL
	,@SUPPD_LANG_NME		Varchar(50)	=	NULL
	,@CNTCT_HR_TXT			Varchar(50)	=	NULL
AS
BEGIN
SET NOCOUNT ON;

IF	(ISNULL(@CNTCT_TYPE_ID, 0) != 0) 
	BEGIN
		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

		INSERT INTO dbo.ORDR_CNTCT WITH (ROWLOCK)
						(ORDR_ID
						,CNTCT_TYPE_ID
						,FRST_NME
						,LST_NME
						,CNTCT_NME
						,PHN_NBR
						,EMAIL_ADR
						,TME_ZONE_ID
						,INTPRTR_CD
						,CREAT_BY_USER_ID
						,FAX_NBR
						,ROLE_ID
						,CIS_LVL_TYPE
						,FSA_MDUL_ID
						,NPA
						,NXX
						,STN_NBR
						,CTY_CD
						,ISD_CD
						,PHN_EXT_NBR
						,FSA_TME_ZONE_CD
						,SUPPD_LANG_NME
						,CNTCT_HR_TXT
						)
			VALUES		(@ORDR_ID
						,@CNTCT_TYPE_ID
						,dbo.encryptString(@FRST_NME)
						,dbo.encryptString(@LST_NME)
						,dbo.encryptString(@NME)
						,@PHN_NBR
						,dbo.encryptString(@EMAIL_ADR)
						,@TME_ZONE_ID
						,@INTPRTR_CD
						,1
						,@FAX_NBR
						,@ROLE_ID
						,@CIS_LVL_TYPE
						,@FSA_MDUL_ID
						,@NPA
						,@NXX
						,@STN_NBR
						,@CTY_CD
						,@ISD_CD
						,@PHN_EXT_NBR
						,@FSA_TME_ZONE_CD
						,@SUPPD_LANG_NME
						,@CNTCT_HR_TXT
						)
						
		SELECT	@ORDR_CNTCT_ID = SCOPE_IDENTITY()	
	END	
END