USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertMatlReqDropShip_V5U]    Script Date: 1/18/2022 1:26:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	-- =============================================
	-- Author:		David Phillips
	-- Create date: 5/4/2015
	-- Description:	Domestic Drop Ship Material Requestion.
	---- =============================================
	

	ALTER PROCEDURE [dbo].[insertMatlReqDropShip_V5U] 
	 @ORDR_ID int 

	AS
	BEGIN TRY
	
	DECLARE @FTN        VARCHAR(50)
	DECLARE @SMRT_ACCT_DOMN_TXT Varchar(100), 	@VRTL_ACCT_DOMN_TXT Varchar(100)


	SET @FTN = (SELECT FTN FROM FSA_ORDR WHERE ORDR_ID = @ORDR_ID)
	
	IF EXISTS (SELECT 'x' FROM dbo.ORDR WITH (NOLOCK) 
				WHERE DMSTC_CD = 1 AND ORDR_ID =  @ORDR_ID)
		BEGIN
				INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
											(ORDR_ID,EMAIL_REQ_TYPE_ID,STUS_ID,EMAIL_LIST_TXT,EMAIL_CC_TXT,EMAIL_SUBJ_TXT,EMAIL_BODY_TXT)
				VALUES	(@ORDR_ID,34,10,'Lajan.V.Howard@sprint.com,Tammy.Leppo@sprint.com,Ho.K.Ling@sprint.com',
											null,
												'International Drop-Ship order received - Please Cancel',
												'M5 Order ID:  ' + @FTN + ' needs to be cancelled.  International flagged as Drop-Ship.')
		END
	ELSE 
		BEGIN		
		
		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
		
		DECLARE @REQSTN_NBR VARCHAR(15)
		
		DECLARE @CNT             int = 1
		DECLARE @dealId	    VARCHAR (500)
		
		DECLARE @Deal_ids table (MANF_DISCNT_CD VARCHAR(500))
		
		INSERT INTO @Deal_ids (MANF_DISCNT_CD) 
		 (SELECT DISTINCT MANF_DISCNT_CD FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
							WHERE ORDR_ID = @ORDR_ID) 
				
		SET @REQSTN_NBR = 'K' + right(@FTN,7)  + convert(varchar(2),@CNT)
		
		WHILE EXISTS (SELECT 'X' FROM	dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK)
			WHERE	REQSTN_NBR =	@REQSTN_NBR)
				BEGIN
				  SET @CNT = @CNT + 1
				  SET @REQSTN_NBR = 'K' + right(@FTN,7)  + convert(varchar(2),@CNT)
				END
				
		IF EXISTS (SELECT 'X' FROM FSA_ORDR_CPE_LINE_ITEM WHERE ORDR_ID = @ORDR_ID and  MFR_NME in ('CRAD','FRTT'))
			BEGIN
				DECLARE @NOTE VARCHAR (350)
				DECLARE @CustNme Varchar (100)
				DECLARE @CustEmail Varchar (150)


				SELECT @CustNme = ISNULL(oc.CNTCT_NME,DECRYPTBYKEY(csd.CUST_CNTCT_NME)),
					   @CustEmail = ISNULL(oc.EMAIL_ADR,DECRYPTBYKEY(csd.CUST_EMAIL_ADR)) 
					FROM dbo.ORDR_CNTCT oc WITH (NOLOCK)
						LEFT OUTER JOIN dbo.CUST_SCRD_DATA csd on csd.SCRD_OBJ_ID = oc.ORDR_CNTCT_ID					
					WHERE CNTCT_TYPE_ID = 1 AND oc.ORDR_ID = @ORDR_ID AND ROLE_ID = 94 AND CIS_LVL_TYPE = 'OD'
				
				SELECT @CustEmail = REPLACE(@CustEmail,'@', 'AT')
				
				SELECT @NOTE = 'Please send all customer email communications and notifications to the following address ' 
						+ @CustNme + ' at '  + @CustEmail + '.'

				exec dbo.insertOrderNotes @ORDR_ID, 27, @NOTE, 1
			
			END
		ELSE  IF EXISTS (SELECT 'X' FROM FSA_ORDR_CPE_LINE_ITEM where MFR_NME = 'CSCO'
																	AND MDS_DES not like '%Meraki%' 
																	AND ISNULL(CPE_REUSE_CD,0) <> 0 
																	AND ORDR_ID = @ORDR_ID)
			BEGIN

				SET @NOTE = NULL

				SELECT TOP 1 @SMRT_ACCT_DOMN_TXT = SMRT_ACCT_DOMN_TXT,
								 @VRTL_ACCT_DOMN_TXT = VRTL_ACCT_DOMN_TXT
					FROM FSA_ORDR_CPE_LINE_ITEM 
					  WHERE MFR_NME = 'CSCO' 
										AND MDS_DES not like '%Meraki%' 
										AND ISNULL(CPE_REUSE_CD,0) <> 0 
										AND ORDR_ID = @ORDR_ID
										AND ISNULL(DROP_SHP,'N') = 'Y'
										AND ISNULL(SMRT_ACCT_DOMN_TXT,'') <> ''												
				
				SET @NOTE = 'Drop Ship of Router or Switch.    Smart Account Domain Identifier  ' + @SMRT_ACCT_DOMN_TXT + 
										', Virtual account  ' + @VRTL_ACCT_DOMN_TXT
						
				EXEC dbo.insertOrderNotes @ORDR_ID, 27, @NOTE, 1
						
			 END	

		WHILE EXISTS (SELECT 'X' FROM	@Deal_ids)
			BEGIN
				
			    SELECT TOP 1 @dealId = ISNULL(MANF_DISCNT_CD,'') FROM @Deal_ids
				
				IF EXISTS (SELECT 'X' FROM	dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK)
							WHERE	REQSTN_NBR =	@REQSTN_NBR)
						BEGIN
							SET @CNT = @CNT + 1
							SET @REQSTN_NBR = 'K' + right(@FTN,7)  + convert(varchar(2),@CNT)
						END 	
				
					
				INSERT INTO [dbo].[PS_REQ_LINE_ITM_QUEUE]
					   ([ORDR_ID],[CMPNT_ID],[REQSTN_NBR],[MAT_CD],[DLVY_CLLI],[ITM_DES],[MANF_PART_NBR]
					   ,[MANF_ID],[ORDR_QTY],[UNT_MSR],[UNT_PRICE],[RAS_DT],[VNDR_NME],[BUS_UNT_GL],[ACCT]
					   ,[COST_CNTR],[PRODCT],[MRKT],[AFFLT],[REGN],[PROJ_ID],[BUS_UNT_PC],[ACTVY],[SOURCE_TYP]
					   ,[RSRC_CAT],[RSRC_SUB],[CNTRCT_ID],[CNTRCT_LN_NBR],[AXLRY_ID],[INST_CD],[REC_STUS_ID]
					   ,[CREAT_DT],[SENT_DT],[FSA_CPE_LINE_ITEM_ID],[EQPT_TYPE_ID],[COMMENTS],[MANF_DISCNT_CD])
				 (SELECT DISTINCT
						fo.ORDR_ID                      AS ORDR_ID
					   ,cli.CMPNT_ID                    AS CMPNT_ID
					   ,@REQSTN_NBR                     AS REQSTN_NBR
					   ,cli.MATL_CD                     AS MAT_CD
					   ,null                            AS DLVY_CLLI
					   ,cli.MDS_DES                     AS ITM_DES
					   ,cli.MANF_PART_CD                AS MANF_PART_NBR
					   ,cli.MFR_PS_ID                   AS MANF_ID
					   ,cli.ORDR_QTY                    AS ORDR_QTY
					   ,ISNULL(cli.UNIT_MSR,'EA ')      AS UNT_MSR
					   ,ISNULL(CAST(cli.UNIT_PRICE AS DECIMAL(15,5)),0) AS UNT_PRICE
					   ,fo.CUST_CMMT_DT                 AS RAS_DT
					   ,cli.VNDR_CD                     AS VNDR_NME
					   ,pid.BUS_UNT_GL                  AS BUS_UNT_GL
					   ,pid.ACCT                        AS ACCT
					   ,pid.COST_CNTR                   AS COST_CNTR
					   ,pid.PRODCT                      AS PRODCT
					   ,pid.MRKT                        AS MRKT
					   ,null                            AS AFFLT
					   ,pid.REGN                        AS REGN
					   ,pid.PROJ_ID                     AS PROJ_ID
					   ,pid.BUS_UNT_PC                  AS BUS_UNT_PC
					   ,pid.ACTVY                       AS ACTVY
					   ,pid.SOURCE_TYP                  AS SOURCE_TYP
					   ,pid.RSRC_CAT                    AS RSRC_CAT 
					   ,null							AS RSRC_SUB
					   ,null							AS CNTRCT_ID
					   ,null							AS CNTRCT_LN_NBR
					   ,null							AS AXLRY_ID
					   ,null							AS INST_CD
					   ,1								AS REC_STUS_ID
					   ,getdate()						AS CREAT_DT
					   ,null							AS SENT_DT
					   ,cli.FSA_CPE_LINE_ITEM_ID		AS FSA_CPE_LINE_ITEM_ID
					   ,null							AS EQPT_TYPE_ID
					   ,CASE
					    WHEN ISNULL(nte.NTE_TXT,'') != '' THEN nte.NTE_TXT
						WHEN  cli.MFR_NME = 'CSCO' AND cli.CMPNT_FMLY in ('Router mnt','Switch mnt','Ip mnt')  THEN 'Do not order maintenance. Maintainance req created by CSC. ' 
					    ELSE cli.MANF_DISCNT_CD	
					    END								AS COMMENTS
					   ,cli.MANF_DISCNT_CD				AS MANF_DISCNT_CD
			           
					   FROM dbo.FSA_ORDR fo WITH (NOLOCK)
					   INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK)
								ON cli.ORDR_ID = fo.ORDR_ID
								AND cli.EQPT_TYPE_ID not in ('SPK','SVC')
								AND ISNULL(cli.MANF_DISCNT_CD,'') = @dealId
					   INNER JOIN dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
					   LEFT OUTER JOIN  dbo.LK_CPE_PID pid WITH (NOLOCK)
								ON pid.DMSTC_CD = ord.DMSTC_CD
								AND pid.REC_STUS_ID = 1					
								--AND pid.PID_PROD_ID = ord.PROD_ID
								--AND pid.EQPT_TYPE_ID = cli.EQPT_TYPE_ID
								AND pid.PID_CNTRCT_TYPE = cli.CNTRC_TYPE_ID
								AND cli.DROP_SHP = pid.DROP_SHP
						LEFT OUTER JOIN (SELECT TOP 1 ORDR_ID, NTE_TXT, NTE_TYPE_ID from dbo.ORDR_NTE onte WITH (NOLOCK) 
										WHERE onte.NTE_TYPE_ID = 28 and onte.ORDR_ID = @ORDR_ID)nte ON nte.ORDR_ID = fo.ORDR_ID
					   
					   WHERE fo.ORDR_ID = @ORDR_ID)
					  

				INSERT INTO [dbo].[PS_REQ_HDR_QUEUE]
				   ([ORDR_ID],[REQSTN_NBR],[CUST_ELID],[DLVY_CLLI],[DLVY_NME],[DLVY_ADDR1],[DLVY_ADDR2]
				   ,[DLVY_ADDR3],[DLVY_ADDR4],[DLVY_CTY],[DLVY_CNTY],[DLVY_ST],[DLVY_ZIP],[DLVY_PHN_NBR]
				   ,[INST_CLLI],[MRK_PKG],[REQSTN_DT],[REF_NBR],[SHIP_CMMTS],[RFQ_INDCTR],[REC_STUS_ID]
				   ,[CREAT_DT],[SENT_DT])
		           
				(SELECT DISTINCT
						@ORDR_ID      AS ORDR_ID
					   ,@REQSTN_NBR    AS REQSTN_NBR
					   ,'fr235990'     AS CUST_ELID -- Frank Xavier Perez is the contact elid
					   ,null           AS DLVY_CLLI
					   ,CASE
							WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(foc.CUST_NME,1,30)
					        ELSE CONVERT(VARCHAR(30), DecryptByKey(sa.CUST_NME))				
					   END                                                             AS DLVY_NME
					   ,CASE
							WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN ISNULL(substring(oas.STREET_ADR_1,1,30),substring(oa.STREET_ADR_1,1,30))
							ELSE ISNULL(CONVERT(VARCHAR(30), DecryptByKey(sas.STREET_ADR_1)),
												CONVERT(VARCHAR(30), DecryptByKey(sa.STREET_ADR_1)))		
						END                                                            AS DLVY_ADDR1
					   ,CASE
							WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN ISNULL(substring(oas.STREET_ADR_2,1,30),substring(oa.STREET_ADR_2,1,30))
							ELSE ISNULL(CONVERT(VARCHAR(30), DecryptByKey(sas.STREET_ADR_2)),
							                    CONVERT(VARCHAR(30), DecryptByKey(sa.STREET_ADR_2)))		
					   END                                                             AS DLVY_ADDR2
					   ,CASE
							WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN ISNULL(substring(oas.STREET_ADR_3,1,30),substring(oa.STREET_ADR_3,1,30))
							ELSE ISNULL(CONVERT(VARCHAR(30), DecryptByKey(sas.STREET_ADR_3)),
							                    CONVERT(VARCHAR(30), DecryptByKey(sa.STREET_ADR_3)))		
					   END	                                                            AS DLVY_ADDR3
					   ,null															AS DLVY_ADDR4
					   ,CASE
							WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN ISNULL(substring(oas.CTY_NME,1,20),substring(oa.CTY_NME,1,20))
							ELSE ISNULL(CONVERT(VARCHAR(20), DecryptByKey(sas.CTY_NME)),
							                    CONVERT(VARCHAR(20), DecryptByKey(sa.CTY_NME)))			
						END                                                             AS DLVY_CTY
					   ,ISNULL(ics.L2P_CTRY_CD,ic.L2P_CTRY_CD)							AS DLVY_CNTY
					   ,CASE
							WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN ISNULL(substring(oas.STT_CD,1,2),substring(oa.STT_CD,1,2))
							ELSE ISNULL(CONVERT(VARCHAR(2), DecryptByKey(sas.STT_CD)),
							                    CONVERT(VARCHAR(2), DecryptByKey(sa.STT_CD)))				
						END                                                             AS DLVY_ST
					   ,CASE
							WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN ISNULL(substring(oas.ZIP_PSTL_CD,1,5),substring(oa.ZIP_PSTL_CD,1,5))
							ELSE  ISNULL(CONVERT(VARCHAR(5), DecryptByKey(sas.ZIP_PSTL_CD)),
							                    CONVERT(VARCHAR(5), DecryptByKey(sa.ZIP_PSTL_CD)))			
					    END                                                             AS DLVY_ZIP
					   ,ISNULL(fo.CPE_PHN_NBR,'')										AS DLVY_PHN_NBR
					   ,null															AS INST_CLLI
					   ,null															AS MRK_PKG
					   ,getdate()														AS REQSTN_DT
					   ,null															AS REF_NBR
					   ,ISNULL(nte.NTE_TXT,'')											AS SHIP_CMMTS
					   ,null															AS RFQ_INDCTR
					   ,1																AS REC_STUS_ID
					   ,getdate()														AS CREAT_DT
					   ,null															AS SENT_DT
			           
					   FROM			dbo.FSA_ORDR fo WITH (NOLOCK)
					   INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
					   INNER JOIN   dbo.FSA_ORDR_CUST foc WITH (NOLOCK)
										ON foc.ORDR_ID = fo.ORDR_ID AND CIS_LVL_TYPE = 'H6'
					   INNER JOIN	dbo.ORDR_ADR oa	WITH (NOLOCK)
						   ON	oa.ORDR_ID = fo.ORDR_ID  
							AND	oa.CIS_LVL_TYPE in ('H6','H5')
							AND oa.ADR_TYPE_ID = 18
							AND oa.ORDR_ID = @ORDR_ID
					   LEFT OUTER JOIN dbo.CUST_SCRD_DATA sa WITH (NOLOCK)
							ON sa.SCRD_OBJ_ID = oa.ORDR_ADR_ID and sa.SCRD_OBJ_TYPE_ID = 14
					   
					   LEFT OUTER JOIN	dbo.ORDR_ADR oas	WITH (NOLOCK)
						   ON	oas.ORDR_ID = fo.ORDR_ID  
							AND	oas.CIS_LVL_TYPE in ('H6','H5')
							AND oas.ADR_TYPE_ID = 19
							AND oas.ORDR_ID = @ORDR_ID
					   LEFT OUTER JOIN dbo.CUST_SCRD_DATA sas WITH (NOLOCK)
							ON sas.SCRD_OBJ_ID = oas.ORDR_ADR_ID and sas.SCRD_OBJ_TYPE_ID = 14
					   
					   Left JOIN   dbo.ORDR_CNTCT oc WITH (NOLOCK)
							ON oc.ROLE_ID = 94 
							AND oc.CIS_LVL_TYPE in ('H6')
							AND oc.ORDR_ID = @ORDR_ID 
						LEFT OUTER JOIN dbo.CUST_SCRD_DATA sc WITH (NOLOCK)
							ON sc.SCRD_OBJ_ID = oc.ORDR_CNTCT_ID and sc.SCRD_OBJ_TYPE_ID = 15
					   
					   LEFT OUTER JOIN dbo.LK_CTRY ic WITH (NOLOCK) ON ic.CTRY_CD = oa.CTRY_CD
					   LEFT OUTER JOIN dbo.LK_CTRY ics WITH (NOLOCK) ON ics.CTRY_CD = oas.CTRY_CD
					   LEFT OUTER JOIN (SELECT TOP 1 ORDR_ID, NTE_TXT, NTE_TYPE_ID from dbo.ORDR_NTE onte WITH (NOLOCK) 
										WHERE onte.NTE_TYPE_ID = 27 AND onte.ORDR_ID = @ORDR_ID)nte ON nte.ORDR_ID = fo.ORDR_ID
					  WHERE fo.ORDR_ID = @ORDR_ID)
						  
					DELETE @Deal_ids WHERE ISNULL(MANF_DISCNT_CD,'') = @dealId
				
				END
			END
	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH


