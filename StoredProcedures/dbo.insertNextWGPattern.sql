USE COWS
GO
_CreateObject 'SP','dbo','insertNextWGPattern'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Created By:	Shabu Karunakaran
-- Modified By: Ramesh Ragi
-- Modified date:	07/27/2011
-- Description:	insert Next Pattern in wgPatternStatus table
-- ===========================================================================
       
ALTER PROCEDURE [dbo].[insertNextWGPattern]
	@OrderID		INT,      
	@NextPatternIDs	VARCHAR(MAX)	
AS      
BEGIN
	DECLARE @NewPatternIDs		VARCHAR(MAX)
	DECLARE @ixml INT
	
	DECLARE @OrderData			TABLE (OrderID  INT, WGPatternID SMALLINT)	
	DECLARE	@NewPatterns	TABLE (OrderID  INT, WGPatternID SMALLINT)
	
	
	BEGIN TRY
		
		EXEC sp_xml_preparedocument @ixml OUTPUT, @NextPatternIDs
		
		INSERT INTO 
			@OrderData 	
		SELECT OrderID, WGPatternID FROM
		OPENXML (@ixml, '/NextPatterns/IDs', 2)
			WITH (OrderID INT, WGPatternID SMALLINT)	 
			
		EXEC sp_xml_removedocument	@ixml
			
		--Fetching the Profile from profile list
		INSERT INTO 
			@NewPatterns	
		SELECT 			
			od.OrderID,						
			od.WGPatternID
		FROM	
			@OrderData od
		INNER JOIN	dbo.ORDR	o	WITH (NOLOCK)	ON	o.ORDR_ID		=	od.OrderID
		INNER JOIN	dbo.LK_PPRT	lp	WITH (NOLOCK)	ON	lp.PPRT_ID		=	o.PPRT_ID		
		INNER JOIN	dbo.SM			sm	WITH (NOLOCK) ON sm.SM_ID		=	lp.SM_ID
													AND	sm.DESRD_WG_PTRN_ID = od.WGPatternID		
			
	
--		SELECT * FROM @NewPatterns
		--UPDATE dbo.WGPatternStatus if already the pattern exists
		UPDATE wps
		SET	
			wps.STUS_ID	=	0
		FROM 	
			dbo.WG_PROF_STUS	wps WITH (ROWLOCK)
		INNER JOIN 	@NewPatterns np	ON	wps.ORDR_ID		=	np.OrderID 
									AND np.WGPatternId	=	wps.WG_PTRN_ID		
		
		--Insert Next Pattern into dbo.WGPatternStatus
		INSERT INTO 
			dbo.WG_PTRN_STUS				
		SELECT DISTINCT
			OrderID,						
			WGPatternId,
			0,
			GETDATE()
		FROM	
			@NewPatterns np
		WHERE	
			np.WGPatternId	NOT IN 
								(
									SELECT 
										WGPatternId	
									FROM 
										dbo.WG_PTRN_STUS wps WITH (NOLOCK) 
									WHERE 
										wps.ORDR_ID	=	np.OrderID
								)		
	
	
		SET  @NewPatternIDs = (SELECT DISTINCT OrderID, WGPatternID, 0 AS WGProfileID From @NewPatterns FOR XML PATH('IDs'), ROOT('Patterns'))
		

		EXEC dbo.insertNextWGProfile  @OrderID, @NewPatternIDs
		
    END TRY  

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]      
	END CATCH    
	
END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
