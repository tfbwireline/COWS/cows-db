USE [COWS]
GO
_CreateObject 'SP','dbo','getCustCPEOrdrs_V5U'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 4/5/2016
	-- Description:	Appian CPE PO Numbers Info.
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getCustCPEOrdrs_V5U]

	AS
	BEGIN

	SELECT		 
			DISTINCT	 fo.ORDR_ID
						, litm.DEVICE_ID + '-' + fo.FTN AS DEVICE_FTN     
						,fo.FTN                          											   
						,litm.DEVICE_ID

						
	FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
	INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
	INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
	INNER JOIN   dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID

	WHERE  fo.PROD_TYPE_CD = 'CP'
		AND (act.TASK_ID IN (602) AND act.STUS_ID = 0)
		AND fo.ORDR_ID in (SELECT ORDR_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
								WHERE litm.CNTRC_TYPE_ID  IN ('CUST','INSI'))

	END


