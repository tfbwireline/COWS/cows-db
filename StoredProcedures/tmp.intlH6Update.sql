USE [COWS]
GO
_CreateObject 'SP','tmp','intlH6Update'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*  
		Assumption that  tmp.H5H6Change   temp table can be created in Prod DB with 2 columns OldH6  , NewH6
		Comma seperate Text file will be give  with OldH6, NewH6    OR
		Excel File will be given with 2 column   OldH6    |   New H6
		While Loading  external File  Old H6 become  Db Tables NEW H6 and Vice versa - Dont Confuse
 */
 ALTER PROCEDURE tmp.intlH6Update 
 AS
 BEGIN
	DECLARE @Cnt SMALLINT,
	        @Cnt2 SMALLINT
	DECLARE @Ctr SMALLINT,
			@Ctr2 SMALLINT
	DECLARE @UserTbl  TABLE (Ident SMALLINT IDENTITY(1,1), OldH6 VARCHAR(9), NewH6 VARCHAR(9), FTN VARCHAR(50))
	IF OBJECT_ID(N'tempdb..##UserTbl2', N'U') IS NOT NULL          
	DROP TABLE ##UserTbl2
	CREATE TABLE ##UserTbl2  (Ident SMALLINT IDENTITY(1,1), ORDR_ID int, FTN VARCHAR(50))
	DECLARE @OldH6 VARCHAR(9)
	DECLARE @NewH6 VARCHAR(9)
	DECLARE @FTN VARCHAR(50)
	DECLARE @ORDR_ID int
	DECLARE @NTE_TXT Varchar(MAX)
	
	SET     @NTE_TXT        =''
	SET		@Cnt			= 0
	SET		@Ctr			= 1
		
	INSERT INTO @UserTbl (OldH6, NewH6, FTN)
	(SELECT DISTINCT   LTRIM(RTRIM(hc.OldH6)), LTRIM(RTRIM(hc.NewH6)), LTRIM(RTRIM(hc.FTN))
	FROM    tmp.H5H6Change hc 	WITH (NOLOCK) INNER JOIN
	dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN=hc.FTN INNER JOIN
	dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID=fo.ORDR_ID AND hc.OldH6=ord.H5_H6_CUST_ID
	WHERE hc.REC_STUS_ID=0
	)
		
		
	SELECT 		@Cnt	=	Count(1) FROM @UserTbl

	IF @Cnt	>	0
		BEGIN
			WHILE	@Ctr <= @Cnt
				BEGIN
					SELECT	@OldH6		=	OldH6,
							@NewH6		=	NewH6,
							@FTN		=	FTN
					FROM	@UserTbl
					WHERE	Ident = @Ctr
					PRINT  @OldH6 Print @NewH6 PRINT @FTN

					---ORDR   TABLE
					IF EXISTS (Select ord.ORDR_ID 
							   FROM dbo.ORDR ord WITH (NOLOCK) INNER JOIN
							   dbo.FSA_ORDR fo WITH (NOLOCK) ON ord.ORDR_ID=fo.ORDR_ID
							   WHERE ord.H5_H6_CUST_ID	=@OldH6
							      AND fo.FTN=@FTN)
					BEGIN 
						SET		@Cnt2			= 0
						SET		@Ctr2			= 1
						TRUNCATE TABLE ##UserTbl2
						INSERT INTO ##UserTbl2 (ORDR_ID, FTN) 
						Select DISTINCT ord.ORDR_ID, fo.FTN
							   FROM dbo.ORDR ord WITH (NOLOCK) INNER JOIN
							   dbo.FSA_ORDR fo WITH (NOLOCK) ON ord.ORDR_ID=fo.ORDR_ID
							   WHERE ord.H5_H6_CUST_ID	=@OldH6
							      AND fo.FTN=@FTN
						UPDATE ord
						SET H5_H6_CUST_ID	=@NewH6
						FROM dbo.ORDR ord INNER JOIN
							   dbo.FSA_ORDR fo WITH (NOLOCK) ON ord.ORDR_ID=fo.ORDR_ID
							   WHERE ord.H5_H6_CUST_ID	=@OldH6
							      AND fo.FTN=@FTN
						SELECT 		@Cnt2	=	Count(1) FROM ##UserTbl2
						IF @Cnt2	>	0
						BEGIN
							WHILE	@Ctr2 <= @Cnt2
								BEGIN
								   SELECT @ORDR_ID=ORDR_ID  FROM ##UserTbl2  WHERE  Ident = @Ctr2
								   SET @NTE_TXT='International Network migration H6 has been converted back to the original H6: '+@NewH6 + '  FROM H6: '+@OldH6
								   INSERT INTO [dbo].[ORDR_NTE]    ([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT])
								   VALUES( 23, @ORDR_ID, Getdate(),1,1, @NTE_TXT )

								   SET @Ctr2 = @Ctr2 + 1
								END
						END
						UPDATE tmp.H5H6Change SET REC_STUS_ID=1 WHERE FTN=@FTN AND NewH6=@NewH6 AND OldH6=@OldH6
					END
					  
					UPDATE foc
					SET CUST_ID			=@NewH6
					FROM dbo.FSA_ORDR_CUST	foc INNER JOIN
					##UserTbl2 ut WITH (NOLOCK) ON ut.ORDR_ID=foc.ORDR_ID
					WHERE foc.CUST_ID		=@OldH6

					UPDATE nsi
					SET H5_H6_ID		=@NewH6
					FROM dbo.NRM_SRVC_INSTC	 nsi INNER JOIN
					##UserTbl2 ut WITH (NOLOCK) ON ut.FTN=nsi.FTN
					WHERE nsi.H5_H6_ID		=@OldH6
					
					UPDATE nv
					SET H6_H5_ID		=@NewH6	
					FROM dbo.NRM_VNDR nv INNER JOIN
					##UserTbl2 ut WITH (NOLOCK) ON ut.FTN=nv.FTN
					WHERE nv.H6_H5_ID		=@OldH6  

					UPDATE hf
					set CUST_ID=@NewH6
					FROM dbo.H5_FOLDR hf INNER JOIN
					dbo.ORDR ord WITH (NOLOCK) ON ord.H5_FOLDR_ID=hf.H5_FOLDR_ID INNER JOIN
					##UserTbl2 ut WITH (NOLOCK) ON ut.ORDR_ID=ord.ORDR_ID
					WHERE hf.CUST_ID=@OldH6
					
					SET @Ctr = @Ctr + 1
				END
		END
END