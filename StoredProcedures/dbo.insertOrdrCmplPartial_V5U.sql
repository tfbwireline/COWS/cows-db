USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertOrdrCmplPartial_V5U]    Script Date: 10/26/2015 10:00:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	David Phillips
-- Create date: 9/21/2015
-- Description:	This SP Activates Line Item Selected.
-- =============================================
CREATE PROCEDURE [dbo].[insertOrdrCmplPartial_V5U] 
 	@FSA_CPE_LINE_ITEM_ID        INT = 0,
 	@CMPL_DT                    DATETIME = NULL 
 	
AS
BEGIN TRY

	IF @FSA_CPE_LINE_ITEM_ID <> 0
		BEGIN
				--INSERT INTO [dbo].[M5_ORDR_MSG]
				--		([ORDR_ID],[M5_MSG_ID],[STUS_ID],[CREAT_DT],[EVENT_ID],[SENT_DT],[FSA_CPE_LINE_ITEM_ID])
				--	(SELECT ORDR_ID,7,10,GETDATE(),NULL, NULL, @FSA_CPE_LINE_ITEM_ID           
				--	   FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
				--	   WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID)
			
			
				UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM 
				SET CMPL_DT = ISNULL(@CMPL_DT, GETDATE())
				WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
			
			END

	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH



