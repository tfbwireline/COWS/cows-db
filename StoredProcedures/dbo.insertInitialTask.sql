USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[InsertInitialTask]    Script Date: 1/18/2022 1:32:21 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	-- ================================================================
	-- Create By:		Jagannath Gangi
	-- Modified By:		Ramesh Ragi
	-- Create date:		07/28/2011
	-- Description:		This SP is used to insert 
	--					initial task in captactivetasks for multiline orders
	--=================================================================

	ALTER PROCEDURE [dbo].[InsertInitialTask] --12269,6
		@OrderID		INT,
		@CategoryID		Int,
		@TaskID			SMALLINT = 0,
		@TaskStatus		TINYINT = 0
	AS
	BEGIN

	DECLARE @WGPatternIDs	VarChar(MAX)
	DECLARE @CnclFlg		Bit
	DECLARE @PtrlCnclFlg	Bit = 0
	DECLARE @RLTOrdrId		Int
	DECLARE @CnclOrdrID		Int
	DECLARE @ORIG_CNCL_ORDR_ID Int
	DECLARE @RLTOrdIDs TABLE (RLTD_ORDR_ID INT, FLAG BIT)
	DECLARE @Cnt Int = 0, @Ctr int = 0
	SET @WGPatternIDs = ''
	SET @CnclFlg	=	'0'
	SET @RLTOrdrId	=	0

	SET NOCOUNT ON

	BEGIN TRY
		IF ISNULL(@OrderID,0) <> 0
		BEGIN
		
			DECLARE @PropertyID Int
			Select @PropertyID = ISNULL(PPRT_ID,ISNULL(dbo.getPropertyID(@OrderID,@CategoryID),0))
			FROM dbo.ORDR WITH (NOLOCK) 
			WHERE ORDR_ID=@OrderID
			
			IF @TaskStatus = 0 AND @PropertyID <> 0
			BEGIN
				IF EXISTS
					(Select 
							'X' 
						FROM	dbo.LK_PPRT			lp	WITH (NOLOCK)		
					INNER JOIN	dbo.LK_ORDR_TYPE	lo	WITH (NOLOCK)	ON	lp.ORDR_TYPE_ID	=	lo.ORDR_TYPE_ID
						WHERE	lp.PPRT_ID	=	@PropertyID
							AND (
									((lp.TRPT_CD	=	'1') AND (lp.PROD_TYPE_ID != 9))
									OR
									(	(lp.PROD_TYPE_ID = 9)	AND
										EXISTS
											(SELECT 'X'
												FROM	dbo.ORDR	odr	WITH (NOLOCK)
											INNER JOIN	dbo.FSA_ORDR fsa WITH (NOLOCK) ON fsa.ORDR_ID = odr.ORDR_ID
												WHERE	odr.ORDR_ID		=	@OrderID
													AND	
													(	
														(odr.DMSTC_CD	=	1 AND odr.ORDR_CAT_ID	IN	(2,6))
															OR
														((odr.DMSTC_CD	=	0 AND odr.ORDR_CAT_ID	=	2)
															AND	(	(ISNULL(fsa.CPE_CPE_ORDR_TYPE_CD,'') != 'MNS')
																	AND
																	(ISNULL(fsa.TTRPT_MNGD_DATA_SRVC_CD, '') != 'Y')
																)
														)
													)
											 )
									)
								)
							--AND lp.INTL_CD	=	'1'
							AND	((lo.FSA_ORDR_TYPE_CD	=	'CN') OR (lo.ORDR_TYPE_DES = 'Cancel'))
					)
					BEGIN
						SET @CnclFlg = '1'
						--Updating PropertyID for Cancel Order
						UPDATE		dbo.ORDR WITH (ROWLOCK)
							SET		PPRT_ID = @PropertyID
							WHERE	ORDR_ID = @OrderID
						
						SELECT @PtrlCnclFlg = CASE ORDR_SUB_TYPE_CD WHEN 'PC' THEN 1 ELSE 0 END
						FROM dbo.FSA_ORDR WITH (NOLOCK)
						WHERE ORDR_ID = @OrderID
						  AND EXISTS (SELECT * 
									  FROM dbo.FSA_ORDR fo WITH (NOLOCK)
										INNER JOIN 	dbo.FSA_ORDR fc WITH (NOLOCK) ON fo.FTN = fc.PRNT_FTN
										INNER jOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcl WITH (NOLOCK) ON fo.ORDR_ID=fcl.ORDR_ID 
									  WHERE fc.ORDR_ID = @OrderID
										-- AND fc.PROD_TYPE_CD = fo.PROD_TYPE_CD removed dlp0278 10/26/21
										and fcl.CMPNT_ID in (select CMPNT_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM where ORDR_ID = fc.ORDR_ID) -- added 10/26/21
										--AND fcl.ITM_STUS = 401 removed dlp0278	10/26/21
										)
					
						INSERT INTO @RLTOrdIDs (RLTD_ORDR_ID,FLAG)
						SELECT distinct	ISNULL(fo.ORDR_ID,0),0
							FROM	dbo.FSA_ORDR fo WITH (NOLOCK)
								INNER JOIN 	dbo.FSA_ORDR fc WITH (NOLOCK) ON fo.FTN = fc.PRNT_FTN
								INNER jOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcl WITH (NOLOCK) ON fo.ORDR_ID=fcl.ORDR_ID
							WHERE	fc.ORDR_ID = @OrderID
								and fcl.CMPNT_ID in (select CMPNT_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM where ORDR_ID = fc.ORDR_ID) -- added 10/26/21
								--AND fc.PROD_TYPE_CD = fo.PROD_TYPE_CD removed dlp0278	10/26/21															
						
						SET @CnclOrdrID = @OrderID
						
						--IF @RLTOrdrId	<> 0
						--	BEGIN
						--		SET @CnclOrdrID = @OrderID
						--		SET @OrderID	= @RLTOrdrId
						--	END
					END	
				--IPL && NCCO Change - TRPT_CD <> 1 for IPL & NCCO orders, so set Cancel Flag here
				ELSE IF (
						EXISTS (SELECT 'X' FROM dbo.IPL_ORDR WITH (NOLOCK) WHERE ORDR_ID = @OrderID AND ORDR_TYPE_ID = 8)
							OR
						EXISTS (SELECT 'X' FROM dbo.NCCO_ORDR WITH (NOLOCK) WHERE ORDR_ID = @OrderID AND ORDR_TYPE_ID = 8)	
						)
					BEGIN
						SET @CnclFlg = '1'
						SET @RLTOrdrId = -1
					END
				--End IPL && NCCO Change
				

				UPDATE		dbo.ORDR WITH (ROWLOCK)
					SET		PPRT_ID = @PropertyID
					WHERE	ORDR_ID = @OrderID

				IF (@CnclFlg = '1') 
					BEGIN
						IF @RLTOrdrId = -1
							BEGIN
								DELETE FROM dbo.WG_PTRN_STUS	WITH (ROWLOCK)	WHERE	ORDR_ID = @OrderID
								DELETE FROM dbo.WG_PROF_STUS	WITH (ROWLOCK)	WHERE	ORDR_ID = @OrderID
								DELETE FROM dbo.ACT_TASK		WITH (ROWLOCK)	WHERE	ORDR_ID = @OrderID	
								
								SET  @WGPatternIds = (SELECT DISTINCT o.ORDR_ID AS OrderID, sm.DESRD_WG_PTRN_ID AS WGPatternID 
									  FROM	[dbo].ORDR   o WITH (NOLOCK) 
											 INNER JOIN [dbo].LK_PPRT lp WITH (NOLOCK) ON o.PPRT_ID		= lp.PPRT_ID
											 INNER JOIN [dbo].SM         sm WITH (NOLOCK) ON lp.SM_ID   = sm.SM_ID
										WHERE
											sm.PRE_REQST_WG_PTRN_ID  =  0
										AND	o.ORDR_ID = @OrderID
									  FOR XML PATH('IDs'), ROOT('NextPatterns'))
			
								EXEC [dbo].insertNextWGPattern @OrderID, @WGPatternIds
							END
						ELSE IF ((@PtrlCnclFlg=0) AND ((SELECT COUNT(1) FROM @RLTOrdIDs) > 0))
							BEGIN
								SELECT @Cnt = COUNT(1) FROM @RLTOrdIDs
								
								WHILE (@Ctr < @Cnt)
									BEGIN
										SELECT Top 1 @RLTOrdrId = RLTD_ORDR_ID
											FROM @RLTOrdIDs
											WHERE FLAG = 0
										
										UPDATE		dbo.ORDR WITH (ROWLOCK)
											SET		PPRT_ID = @PropertyID
													,ORDR_STUS_ID = 4 -- added 4/13/2017 10:25 a.m.
											WHERE	ORDR_ID = @RLTOrdrId
										
										DELETE FROM dbo.WG_PTRN_STUS	WITH (ROWLOCK)	WHERE	ORDR_ID = @RLTOrdrId
										DELETE FROM dbo.WG_PROF_STUS	WITH (ROWLOCK)	WHERE	ORDR_ID = @RLTOrdrId
										DELETE FROM dbo.ACT_TASK		WITH (ROWLOCK)	WHERE	ORDR_ID = @RLTOrdrId	
										
										SET  @WGPatternIds = (SELECT DISTINCT o.ORDR_ID AS OrderID, sm.DESRD_WG_PTRN_ID AS WGPatternID 
											  FROM	[dbo].ORDR   o WITH (NOLOCK) 
													 INNER JOIN [dbo].LK_PPRT lp WITH (NOLOCK) ON o.PPRT_ID		= lp.PPRT_ID
													 INNER JOIN [dbo].SM         sm WITH (NOLOCK) ON lp.SM_ID   = sm.SM_ID
												WHERE
													sm.PRE_REQST_WG_PTRN_ID  =  0
												AND	o.ORDR_ID = @RLTOrdrId
											  FOR XML PATH('IDs'), ROOT('NextPatterns'))
					
										EXEC [dbo].insertNextWGPattern @RLTOrdrId, @WGPatternIds
										
										SET @Ctr = @Ctr + 1
										UPDATE @RLTOrdIDs SET FLAG = 1 WHERE RLTD_ORDR_ID = @RLTOrdrId
									END
								UPDATE @RLTOrdIDs SET FLAG = 0
								SET @Ctr = 0 
								SET @CNT = 0
							END
						END
					ELSE
						BEGIN
							SET  @WGPatternIds = (SELECT DISTINCT o.ORDR_ID AS OrderID, sm.DESRD_WG_PTRN_ID AS WGPatternID 
									  FROM	[dbo].ORDR   o WITH (NOLOCK) 
											 INNER JOIN [dbo].LK_PPRT lp WITH (NOLOCK) ON o.PPRT_ID		= lp.PPRT_ID
											 INNER JOIN [dbo].SM         sm WITH (NOLOCK) ON lp.SM_ID   = sm.SM_ID
										WHERE
											sm.PRE_REQST_WG_PTRN_ID  =  0
										AND	o.ORDR_ID = @OrderID
									  FOR XML PATH('IDs'), ROOT('NextPatterns'))
			
							EXEC [dbo].insertNextWGPattern @OrderID, @WGPatternIds		
						END
			END
		
		IF @CnclFlg = '0'
			BEGIN
				IF @CategoryID	=	2 
					BEGIN
						EXEC dbo.completeFSAInitialTask @OrderID
						EXEC dbo.UpdateSTDIInfo @OrderID
					END
				ELSE IF @CategoryID	=	6
					BEGIN
						EXEC [dbo].[H5FolderRules] @OrderID
						EXEC dbo.completeFSAInitialTask @OrderID
						EXEC dbo.UpdateSTDIInfo @OrderID
					END
				ELSE IF @PropertyID	!=	0 AND @CnclFlg != '1'
					BEGIN
						DECLARE @Note	Varchar(50)
						SET @Note = 'Auto-Completing Pre Submit task.'	
						INSERT INTO	dbo.ORDR_MS	WITH (ROWLOCK)
								(ORDR_ID
								,VER_ID
								,SBMT_DT
								,CREAT_BY_USER_ID)
						VALUES	(@OrderID
								,1			
								,GETDATE()
								,1)	
						EXEC dbo.UpdateSTDIInfo @OrderID	
						EXEC dbo.completeActiveTask @OrderID,100,2,@Note,NULL,NULL

						--IF @CategoryID = 4
						--	BEGIN
						--		DECLARE @H5_ACCT_NBR INT = 0
						--		SELECT @H5_ACCT_NBR = H5_ACCT_NBR
						--			FROM dbo.NCCO_ORDR 
						--			WHERE ORDR_ID = @OrderID
						--		EXEC	[dbo].[getSensitiveDataFromL2P]	0, @OrderID,@H5_ACCT_NBR
						--	END
					END	
			END
		ELSE 
			BEGIN
				IF @RLTOrdrId = -1 --IPL & NCCO Cancels
					BEGIN
						EXEC dbo.processTrptCancels @OrderID,@CnclOrdrID	
					END
				ELSE --FSA/MAch5 INTL Access/ INTL CPE Cancels
					BEGIN
						IF ((@PtrlCnclFlg=0) AND ((SELECT COUNT(1) FROM @RLTOrdIDs) > 0))
							BEGIN
								SET @Ctr = 0
								SET @Cnt = 0
								SELECT @Cnt = COUNT(1) FROM @RLTOrdIDs
								WHILE (@Ctr < @Cnt)
									BEGIN
										SELECT Top 1 @RLTOrdrId = RLTD_ORDR_ID
											FROM @RLTOrdIDs
											WHERE FLAG = 0
										
										EXEC dbo.processTrptCancels @RLTOrdrId,@CnclOrdrID
										
										SET @Ctr = @Ctr + 1
										UPDATE @RLTOrdIDs SET FLAG = 1 WHERE RLTD_ORDR_ID = @RLTOrdrId
									END
							END
							ELSE IF (@PtrlCnclFlg=1)
							BEGIN
								EXEC dbo.processTrptCancels -1,@CnclOrdrID
							END
					END
				
			END
		END
	END TRY

	BEGIN CATCH
		exec [dbo].[insertErrorInfo]
	END CATCH

	END