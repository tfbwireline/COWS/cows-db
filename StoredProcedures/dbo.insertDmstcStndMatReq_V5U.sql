USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertDmstcStndMatReq_V5U]    Script Date: 1/18/2022 9:53:53 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	-- =============================================
	-- Author:		David Phillips
	-- Create date: 5/4/2015
	-- Description:	Appian Domestic Standard Material Requisition.
	---- =============================================
	ALTER PROCEDURE [dbo].[insertDmstcStndMatReq_V5U]
 		@ORDR_ID int  

	AS
	BEGIN TRY

		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
		
		DECLARE @REQSTN_NBR VARCHAR(15)
		DECLARE @FTN        VARCHAR(50)
		DECLARE @CNT          INT = 1
		
		SET @FTN = (SELECT FTN FROM FSA_ORDR WHERE ORDR_ID = @ORDR_ID)
		
		SET @REQSTN_NBR = 'D' + right(@FTN,7)  + convert(varchar(2),1)
		
		WHILE EXISTS (SELECT 'X' FROM	dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK)
					WHERE	REQSTN_NBR =	@REQSTN_NBR)
				BEGIN
				  SET @CNT = @CNT + 1
				  SET @REQSTN_NBR = 'D' + right(@FTN,7)  + convert(varchar(2),@CNT)
				END 
			
		
		INSERT INTO [dbo].[PS_REQ_LINE_ITM_QUEUE]
			   ([ORDR_ID],[CMPNT_ID],[REQSTN_NBR],[MAT_CD],[DLVY_CLLI],[ITM_DES],[MANF_PART_NBR]
			   ,[MANF_ID],[ORDR_QTY],[UNT_MSR],[UNT_PRICE],[RAS_DT],[VNDR_NME],[BUS_UNT_GL],[ACCT]
			   ,[COST_CNTR],[PRODCT],[MRKT],[AFFLT],[REGN],[PROJ_ID],[BUS_UNT_PC],[ACTVY],[SOURCE_TYP]
			   ,[RSRC_CAT],[RSRC_SUB],[CNTRCT_ID],[CNTRCT_LN_NBR],[AXLRY_ID],[INST_CD],[REC_STUS_ID]
			   ,[CREAT_DT],[SENT_DT],[FSA_CPE_LINE_ITEM_ID],[EQPT_TYPE_ID],[COMMENTS],[MANF_DISCNT_CD])
		 (SELECT
				fo.ORDR_ID                      AS ORDR_ID
			   ,cli.CMPNT_ID                    AS CMPNT_ID
			   ,@REQSTN_NBR                     AS REQSTN_NBR
			   ,cli.MATL_CD                     AS MAT_CD
			   ,ord.DLVY_CLLI                   AS DLVY_CLLI
			   ,cli.MDS_DES                     AS ITM_DES
			   ,cli.MANF_PART_CD                AS MANF_PART_NBR
			   ,cli.MFR_PS_ID                   AS MANF_ID
			   ,cli.ORDR_QTY                    AS ORDR_QTY
			   ,ISNULL(cli.UNIT_MSR,'EA ')      AS UNT_MSR
			   ,ISNULL(CAST(cli.UNIT_PRICE AS DECIMAL(15,4)),0) AS UNT_PRICE
			   ,DATEADD(d,-7,isnull(ord.CUST_CMMT_DT,fo.CUST_CMMT_DT)) AS RAS_DT
			   ,cli.VNDR_CD                     AS VNDR_NME
			   ,pid.BUS_UNT_GL                  AS BUS_UNT_GL
			   ,pid.ACCT                        AS ACCT
			   ,pid.COST_CNTR                   AS COST_CNTR
			   ,pid.PRODCT                      AS PRODCT
			   ,pid.MRKT                        AS MRKT
			   ,null                            AS AFFLT
			   ,pid.REGN                        AS REGN
			   ,pid.PROJ_ID                     AS PROJ_ID
			   ,pid.BUS_UNT_PC                  AS BUS_UNT_PC
			   ,pid.ACTVY                       AS ACTVY
			   ,pid.SOURCE_TYP                  AS SOURCE_TYP
			   ,pid.RSRC_CAT                    AS RSRC_CAT 
			   ,null							AS RSRC_SUB
			   ,null							AS CNTRCT_ID
			   ,null							AS CNTRCT_LN_NBR
			   ,null							AS AXLRY_ID
			   ,null							AS INST_CD
			   ,1								AS REC_STUS_ID
			   ,getdate()						AS CREAT_DT
			   ,null							AS SENT_DT
			   ,cli.FSA_CPE_LINE_ITEM_ID		AS FSA_CPE_LINE_ITEM_ID
			   ,null							AS EQPT_TYPE_ID
			   ,null							AS COMMENTS
			   ,cli.MANF_DISCNT_CD				AS MANF_DISCNT_CD
	           
			   FROM dbo.FSA_ORDR fo WITH (NOLOCK)
			   INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK)
						ON cli.ORDR_ID = fo.ORDR_ID 
						AND cli.EQPT_TYPE_ID NOT IN ('SPK','SVC')
						AND cli.CNTRC_TYPE_ID IN ('PRCH','RNTL', 'LEAS','INST')
			   INNER JOIN dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
			   INNER JOIN  dbo.LK_CPE_PID pid WITH (NOLOCK)
					ON pid.DMSTC_CD = ord.DMSTC_CD
						AND pid.REC_STUS_ID = 1					
						AND pid.PID_CNTRCT_TYPE = cli.CNTRC_TYPE_ID
						AND cli.DROP_SHP = pid.DROP_SHP

			   WHERE fo.ORDR_ID = @ORDR_ID 
			   AND cli.ITM_STUS = 401)
			  

		INSERT INTO [dbo].[PS_REQ_HDR_QUEUE]
           ([ORDR_ID],[REQSTN_NBR],[CUST_ELID],[DLVY_CLLI],[DLVY_NME],[DLVY_ADDR1],[DLVY_ADDR2]
           ,[DLVY_ADDR3],[DLVY_ADDR4],[DLVY_CTY],[DLVY_CNTY],[DLVY_ST],[DLVY_ZIP],[DLVY_BLDG]
           ,[DLVY_FLR],[DLVY_RM],[DLVY_PHN_NBR],[INST_CLLI],[MRK_PKG],[REQSTN_DT],[REF_NBR]
           ,[SHIP_CMMTS],[RFQ_INDCTR],[REC_STUS_ID],[CREAT_DT],[SENT_DT],[CSG_LVL])
           
		SELECT DISTINCT    @ORDR_ID    AS ORDR_ID  
			   ,@REQSTN_NBR                                                      AS REQSTN_NBR
			   ,'pe208093'															AS CUST_ELID
			   ,LEFT(ord.DLVY_CLLI,10)												AS DLVY_CLLI
			   ,CASE
							WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(foc.CUST_NME,1,30)
					        ELSE CONVERT(VARCHAR(30), DecryptByKey(sa.CUST_NME))				
				END                                                             AS DLVY_NME
			   ,CASE
					WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(oa.STREET_ADR_1,1,30)

					ELSE CONVERT(VARCHAR(30), DecryptByKey(sa.STREET_ADR_1))			
				END                                                             AS DLVY_ADDR1
			   ,CASE
					WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(oa.STREET_ADR_2,1,30)
					ELSE CONVERT(VARCHAR(30), DecryptByKey(sa.STREET_ADR_2))			
				END                                                             AS DLVY_ADDR2
			   ,CASE
					WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(oa.STREET_ADR_3,1,30)
					ELSE CONVERT(VARCHAR(30), DecryptByKey(sa.STREET_ADR_3))			
				END                                                             AS DLVY_ADDR3
			   ,null															AS DLVY_ADDR4
			   ,CASE
					WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(oa.CTY_NME,1,20)
					ELSE CONVERT(VARCHAR(20), DecryptByKey(sa.CTY_NME))			
				END                                                             AS DLVY_CTY
			   ,ISNULL(CONVERT(VARCHAR(3), LEFT(oa.CTRY_CD,3)),'')		        AS DLVY_CNTY
			   ,CASE
							WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(oa.STT_CD,1,2)
							ELSE CONVERT(VARCHAR(2), DecryptByKey(sa.STT_CD))				
				END                                                             AS DLVY_ST
			   ,CASE
					WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(oa.ZIP_PSTL_CD,1,5)
					ELSE  CONVERT(VARCHAR(5), DecryptByKey(sa.ZIP_PSTL_CD))			
				END                                                             AS DLVY_ZIP
			   ,CASE
					WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(oa.BLDG_NME,1,10)
					ELSE  CONVERT(VARCHAR(10), DecryptByKey(sa.BLDG_NME))			
				END																AS DLVY_BLDG
			   ,CASE
					WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(oa.FLR_ID,1,5)
					ELSE  CONVERT(VARCHAR(5), DecryptByKey(sa.FLR_ID))			
				END																AS DLVY_FLR
			   ,CASE
					WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN substring(oa.RM_NBR,1,5)
					ELSE  CONVERT(VARCHAR(5), DecryptByKey(sa.RM_NBR))			
				END				                                            	AS DLVY_RM
			   ,LEFT(ISNULL(fo.CPE_PHN_NBR,''),24)								AS DLVY_PHN_NBR
			   ,null															AS INST_CLLI
			   ,null															AS MRK_PKG
			   ,getdate()														AS REQSTN_DT
			   ,null															AS REF_NBR
			   ,null															AS SHIP_CMMTS
			   ,null															AS RFQ_INDCTR
			   ,1																AS REC_STUS_ID
			   ,getdate()														AS CREAT_DT
			   ,null															AS SENT_DT
			   ,lcl.CSG_LVL_CD													AS CSG_LVL
	           
			   FROM			dbo.FSA_ORDR fo WITH (NOLOCK)
			   INNER JOIN dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
			   INNER JOIN   dbo.FSA_ORDR_CUST foc WITH (NOLOCK)
										ON foc.ORDR_ID = fo.ORDR_ID AND CIS_LVL_TYPE = 'H6'							


			   INNER JOIN dbo.ORDR_ADR oa	WITH (NOLOCK) ON	oa.ORDR_ID = fo.ORDR_ID  
					AND	oa.CIS_LVL_TYPE in ('H6','H5')
					AND oa.ADR_TYPE_ID = 18 
					AND fo.ORDR_ID =  @ORDR_ID
			   LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_ID=ord.CSG_LVL_ID
			   LEFT JOIN  dbo.ORDR_CNTCT oc	WITH (NOLOCK)	on oc.ORDR_ID	= fo.ORDR_ID
									AND ROLE_ID = 94 AND oc.CIS_LVL_TYPE in ('OD')
			   LEFT OUTER JOIN dbo.CUST_SCRD_DATA sc WITH (NOLOCK)
							ON sc.SCRD_OBJ_ID = oc.ORDR_CNTCT_ID and sc.SCRD_OBJ_TYPE_ID = 15
			   LEFT OUTER JOIN dbo.CUST_SCRD_DATA sa WITH (NOLOCK)
							ON sa.SCRD_OBJ_ID = oa.ORDR_ADR_ID and sa.SCRD_OBJ_TYPE_ID = 14
			   
				  

	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH

