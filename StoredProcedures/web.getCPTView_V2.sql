USE [COWS]
GO

_CreateObject 'SP'
	,'web'
	,'getCPTView_V2'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
 ================================================================================================  
 Author:		qi931353  
 Create date:	08/05/2019
 Description:	Gets the Data for CPT View for COWSRewrite
 ================================================================================================  
*/

ALTER PROCEDURE [web].[getCPTView_V2] 
	@CSGLvlId		TINYINT	= 0,
	@IsSearchView	TINYINT	= 0
 AS
BEGIN  
 
 SET NOCOUNT ON;  
  
 BEGIN TRY  

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

	DECLARE @CPTTbl TABLE (
		CptId			INT,
		CptCustNbr		VARCHAR(13),
		CustShortName	VARCHAR(100),
		ReturnedToSDE	BIT,
		ReviewedByPM	BIT,
		CompanyName		VARCHAR(1000),
		H1				VARCHAR(9),
		CntDevices		SMALLINT,
		CreatedDateTime	DATETIME,
		CreatedBy		VARCHAR(1000),
		Nte				VARCHAR(1000),
		Mss				VARCHAR(1000),
		Pm				VARCHAR(1000),
		PmReviewedDate	DATETIME,
		LockedByUser	VARCHAR(1000)
	)
		

	IF (@IsSearchView = 0)
		BEGIN
			INSERT INTO @CPTTbl
			SELECT DISTINCT 
				cpt.CPT_ID,
				cpt.CPT_CUST_NBR,
				cpt.CUST_SHRT_NME,
				cpt.RETRN_SDE_CD,
				cpt.REVWD_PM_CD,
				CASE
					WHEN ISNULL(cpt.CSG_LVL_ID,0) = 0 THEN cpt.COMPNY_NME
					WHEN @CSGLvlId > 0 AND @CSGLvlId <= ISNULL(cpt.CSG_LVL_ID,0) THEN dbo.decryptBinaryData(csd.CUST_NME)
					ELSE ''
				END AS COMPNY_NME,
				cpt.H1,
				cpt.DEV_CNT,
				cpt.CREAT_DT,
				usr.DSPL_NME,
				(SELECT usrNTE.DSPL_NME FROM dbo.CPT_USER_ASMT cptNTE WITH (NOLOCK)
					INNER JOIN dbo.LK_USER usrNTE WITH (NOLOCK) ON usrNTE.USER_ID = cptNTE.CPT_USER_ID
					WHERE cptNTE.CPT_ID = cpt.CPT_ID
					AND cptNTE.USR_PRF_ID = (SELECT USR_PRF_ID FROM dbo.LK_USR_PRF WITH (NOLOCK) WHERE USR_PRF_NME = 'MDSEActivator')
					AND cptNTE.REC_STUS_ID = 1) AS NTE,
				(SELECT usrMSS.DSPL_NME FROM dbo.CPT_USER_ASMT cptMSS WITH (NOLOCK)
					INNER JOIN dbo.LK_USER usrMSS WITH (NOLOCK) ON usrMSS.USER_ID = cptMSS.CPT_USER_ID
					WHERE cptMSS.CPT_ID = cpt.CPT_ID
					AND cptMSS.USR_PRF_ID = (SELECT USR_PRF_ID FROM dbo.LK_USR_PRF WITH (NOLOCK) WHERE USR_PRF_NME = 'MDSSystemSecurityEngineer')
					AND cptMSS.REC_STUS_ID = 1) AS MSS,
				(SELECT usrPM.DSPL_NME FROM dbo.CPT_USER_ASMT cptPM WITH (NOLOCK)
					INNER JOIN dbo.LK_USER usrPM WITH (NOLOCK) ON usrPM.USER_ID = cptPM.CPT_USER_ID
					WHERE cptPM.CPT_ID = cpt.CPT_ID
					AND cptPM.USR_PRF_ID = (SELECT USR_PRF_ID FROM dbo.LK_USR_PRF WITH (NOLOCK) WHERE USR_PRF_NME = 'MDSEReviewer')
					AND cptPM.REC_STUS_ID = 1) AS PM,
				(SELECT TOP 1 ch.CREAT_DT FROM dbo.CPT_HIST ch WITH (NOLOCK)
					WHERE ch.CPT_ID = cpt.CPT_ID AND ch.ACTN_ID = 75
					ORDER BY ch.CREAT_DT DESC) AS PmReviewedDate,
				(SELECT usrLock.DSPL_NME FROM dbo.CPT_REC_LOCK cptLock WITH (NOLOCK)
					INNER JOIN dbo.LK_USER usrLock WITH (NOLOCK) ON usrLock.USER_ID = cptLock.LOCK_BY_USER_ID
					WHERE cptLock.CPT_ID = cpt.CPT_ID) AS LOCK_BY_USER
			FROM dbo.CPT cpt WITH (NOLOCK) INNER JOIN
					dbo.LK_USER usr WITH (NOLOCK) ON usr.[USER_ID] = cpt.CREAT_BY_USER_ID LEFT JOIN
					dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID = cpt.CPT_ID AND csd.SCRD_OBJ_TYPE_ID = 2
			WHERE cpt.CPT_STUS_ID NOT IN (308, 309, 311) -- Deleted, Completed, Cancelled
				AND cpt.REC_STUS_ID = 1
				AND ((cpt.CSG_LVL_ID = 0) OR ((@CSGLvlId != 0) AND (cpt.CSG_LVL_ID >= @CSGLvlId)))
		END
	ELSE
		BEGIN
			INSERT INTO @CPTTbl
			SELECT DISTINCT 
				cpt.CPT_ID,
				cpt.CPT_CUST_NBR,
				cpt.CUST_SHRT_NME,
				cpt.RETRN_SDE_CD,
				cpt.REVWD_PM_CD,
				CASE
					WHEN ISNULL(cpt.CSG_LVL_ID,0) = 0 THEN cpt.COMPNY_NME
					WHEN @CSGLvlId > 0 AND @CSGLvlId <= ISNULL(cpt.CSG_LVL_ID,0) THEN dbo.decryptBinaryData(csd.CUST_NME)
					ELSE ''
				END AS COMPNY_NME,
				cpt.H1,
				cpt.DEV_CNT,
				cpt.CREAT_DT,
				usr.DSPL_NME AS CREAT_BY,
				(SELECT usrNTE.DSPL_NME FROM dbo.CPT_USER_ASMT cptNTE WITH (NOLOCK)
					INNER JOIN dbo.LK_USER usrNTE WITH (NOLOCK) ON usrNTE.USER_ID = cptNTE.CPT_USER_ID
					WHERE cptNTE.CPT_ID = cpt.CPT_ID
					AND cptNTE.USR_PRF_ID = (SELECT USR_PRF_ID FROM dbo.LK_USR_PRF WITH (NOLOCK) WHERE USR_PRF_NME = 'MDSEActivator')
					AND cptNTE.REC_STUS_ID = 1) AS NTE,
				(SELECT usrMSS.DSPL_NME FROM dbo.CPT_USER_ASMT cptMSS WITH (NOLOCK)
					INNER JOIN dbo.LK_USER usrMSS WITH (NOLOCK) ON usrMSS.USER_ID = cptMSS.CPT_USER_ID
					WHERE cptMSS.CPT_ID = cpt.CPT_ID
					AND cptMSS.USR_PRF_ID = (SELECT USR_PRF_ID FROM dbo.LK_USR_PRF WITH (NOLOCK) WHERE USR_PRF_NME = 'MDSSystemSecurityEngineer')
					AND cptMSS.REC_STUS_ID = 1) AS MSS,
				(SELECT usrPM.DSPL_NME FROM dbo.CPT_USER_ASMT cptPM WITH (NOLOCK)
					INNER JOIN dbo.LK_USER usrPM WITH (NOLOCK) ON usrPM.USER_ID = cptPM.CPT_USER_ID
					WHERE cptPM.CPT_ID = cpt.CPT_ID
					AND cptPM.USR_PRF_ID = (SELECT USR_PRF_ID FROM dbo.LK_USR_PRF WITH (NOLOCK) WHERE USR_PRF_NME = 'MDSEReviewer')
					AND cptPM.REC_STUS_ID = 1) AS PM,
				(SELECT TOP 1 ch.CREAT_DT FROM dbo.CPT_HIST ch WITH (NOLOCK)
					WHERE ch.CPT_ID = cpt.CPT_ID AND ch.ACTN_ID = 75
					ORDER BY ch.CREAT_DT DESC) AS PmReviewedDate,
				(SELECT usrLock.DSPL_NME FROM dbo.CPT_REC_LOCK cptLock WITH (NOLOCK)
					INNER JOIN dbo.LK_USER usrLock WITH (NOLOCK) ON usrLock.USER_ID = cptLock.LOCK_BY_USER_ID
					WHERE cptLock.CPT_ID = cpt.CPT_ID) AS LOCK_BY_USER
			FROM dbo.CPT cpt WITH (NOLOCK) INNER JOIN
					dbo.LK_USER usr WITH (NOLOCK) ON usr.[USER_ID] = cpt.CREAT_BY_USER_ID LEFT JOIN
					dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID = cpt.CPT_ID AND csd.SCRD_OBJ_TYPE_ID = 2
			WHERE cpt.CPT_STUS_ID NOT IN (308) -- Deleted
				AND cpt.REC_STUS_ID = 1
				AND ((cpt.CSG_LVL_ID = 0) OR ((@CSGLvlId != 0) AND (cpt.CSG_LVL_ID >= @CSGLvlId)))
		END

	SELECT * FROM @CPTTbl

 END TRY  
  
 BEGIN CATCH  
	EXEC [dbo].[insertErrorInfo]  
 END CATCH  
  
END