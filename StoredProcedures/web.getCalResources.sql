USE [COWS]
GO
_CreateObject 'SP','web','getCalResources'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		jrg7298
-- Create date: 09/1/2011
-- Description:	Gets the Resources data for calendars
-- =========================================================
ALTER PROCEDURE [web].[getCalResources]
	@USER_ID		Int		=	0
AS
BEGIN
SET NOCOUNT ON;
Begin Try

IF	@USER_ID	=	0
	BEGIN		
		SELECT DISTINCT lu.[USER_ID] as ID, lu.[FULL_NME] AS Model 
		FROM dbo.[LK_USER] lu WITH (NOLOCK) INNER JOIN
		     dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON ugr.[USER_ID] = lu.[USER_ID]
		WHERE lu.[USER_ID] != 1
		  AND ugr.ROLE_ID = 23
	END
ELSE
	BEGIN
		IF EXISTS (SELECT 'X'
			FROM dbo.USER_GRP_ROLE WITH (NOLOCK)
			WHERE [USER_ID] = @USER_ID
			  AND ROLE_ID = 23)
		BEGIN
			SELECT DISTINCT lu.[USER_ID] as ID, lu.[FULL_NME] AS Model 
			FROM dbo.[LK_USER] lu WITH (NOLOCK) INNER JOIN
			     dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON ugr.[USER_ID] = lu.[USER_ID]
			WHERE lu.[USER_ID] != 1
			  AND ugr.ROLE_ID = 23
		END
		ELSE
		BEGIN
			SELECT [USER_ID] as ID, [FULL_NME] AS Model 
			FROM dbo.[LK_USER] WITH (NOLOCK) 
			WHERE [USER_ID] != 1
			  AND [USER_ID] = @USER_ID
		END
	END
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO
DROP PROCEDURE web.getCalResources
GO