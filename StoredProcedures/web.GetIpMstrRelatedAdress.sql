USE [COWS]
GO
/****** Object:  StoredProcedure [web].[GetIpMstrRelatedAdress]    Script Date: 01/07/2021 9:37:43 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		John Caranay
-- Create date: 05/20/2020
-- Description:	Transform IP string to BIGINT to be able to filter it.
-- =============================================
ALTER PROCEDURE [web].[GetIpMstrRelatedAdress]
	@id INT
AS
BEGIN

	DECLARE @startRange BIGINT -- Currently no use
	DECLARE @endRange BIGINT -- Currently no use

	DECLARE @TempIpMstr AS TABLE 
	(
		
		IP_MSTR_ID INT NOT NULL PRIMARY KEY,
		ID INT,
		IP_ADR VARCHAR(15) NOT NULL,
		REC_STUS_ID INT,
		STUS_DES VARCHAR(20),
		CREAT_BY_USER_ID INT,
		MODFD_BY_USER_ID INT,
		CREAT_DT DATETIME,
		MODFD_DT DATETIME,
		IP_INT BIGINT
	)
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Fetch Start and End Range
	SELECT  @startRange = dbo.IPAddressToInteger(STRT_IP_ADR), @endRange = dbo.IPAddressToInteger(END_IP_ADR)  FROM LK_IP_MSTR WHERE ID = @id;

	-- Insert into Temp table to be able to filter the IP by range
	INSERT INTO @TempIpMstr(IP_MSTR_ID, ID, IP_ADR, REC_STUS_ID, STUS_DES, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, MODFD_DT, IP_INT)
	SELECT 
		im.IP_MSTR_ID, 
		@id as ID,
		im.IP_ADR, 
		im.REC_STUS_ID, 
		ls.STUS_DES,
		im.CREAT_BY_USER_ID, 
		im.MODFD_BY_USER_ID, 
		im.CREAT_DT, im.MODFD_DT, 
		dbo.IPAddressToInteger(IP_ADR) as IP_INT 
	FROM IP_MSTR as im
	LEFT JOIN LK_STUS as ls ON im.REC_STUS_ID = ls.STUS_ID;

	-- Select and filter record by Start and End Range of LK_IP_MSTR
	SELECT ID, IP_MSTR_ID, IP_ADR, REC_STUS_ID, STUS_DES, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, MODFD_DT, IP_INT
	FROM @TempIpMstr 
	WHERE IP_INT >= @startRange and IP_INT <= @endRange
	ORDER BY IP_INT ASC;

END
