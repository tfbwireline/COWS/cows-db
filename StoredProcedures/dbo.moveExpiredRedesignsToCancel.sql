USE COWS
GO
_CreateObject 'SP','dbo','moveExpiredRedesignsToCancel'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <04/08/2016>
-- Description:	<Systematic process to move 
--			expired redesigns to cancelled status>
-- =============================================
ALTER PROCEDURE dbo.moveExpiredRedesignsToCancel
	
AS
BEGIN
	BEGIN TRY
		SET NOCOUNT ON;

		DECLARE @MoveToCancelStatus TABLE (REDSGN_ID INT,FLAG BIT)
		
		INSERT INTO @MoveToCancelStatus
		SELECT REDSGN_ID,0
			FROM	dbo.REDSGN WITH (NOLOCK)
			WHERE	EXPRTN_DT < GETDATE()
				AND STUS_ID	NOT IN (226,227,228)

		--SELECT * FROM @MoveToCancelStatus

		DECLARE @CTR INT = 0, @CNT INT = 0	, @REDSGN_ID INT
		SELECT @CNT = COUNT(1) 
			FROM @MoveToCancelStatus
		
		WHILE (@CTR < @CNT)
			BEGIN
				SELECT @REDSGN_ID = REDSGN_ID
					FROM @MoveToCancelStatus
					WHERE FLAG = 0
				
				UPDATE dbo.REDSGN
					SET STUS_ID = 226
					WHERE REDSGN_ID = @REDSGN_ID
				
				INSERT INTO dbo.REDSGN_NOTES (REDSGN_ID,REDSGN_NTE_TYPE_ID,NOTES,CRETD_BY_CD,CRETD_DT)
				VALUES (@REDSGN_ID, 1, 'Status Update: Cancelled by System after 180 days', 1, GETDATE()) 
				
				EXEC dbo.insertRedesignEmailData @REDSGN_ID, 226, 0 		
					
				UPDATE @MoveToCancelStatus SET FLAG = 1 WHERE REDSGN_ID = @REDSGN_ID AND FLAG = 0
				SET @CTR = @CTR + 1
			END	
			
		--SELECT r.REDSGN_NBR 
		--	FROM @MoveToCancelStatus t
		--	inner join dbo.REDSGN r on t.REDSGN_ID = r.REDSGN_ID
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
		
END
GO

