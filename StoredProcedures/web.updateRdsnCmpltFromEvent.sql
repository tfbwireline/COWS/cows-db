USE [COWS]
GO
_CreateObject 'SP','web','updateRdsnCmpltFromEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:  jrg7298            
-- Create date: 01/30/2016         
-- Description: Update the redesign to complete from Event
-- Updated by:	qi931353
-- Update date:	12/29/2020
-- Description:	Added @redsgn_url for the new redesign url
-- =========================================================
ALTER PROCEDURE [web].[updateRdsnCmpltFromEvent] --9881
 @EventID INT,
 @redsgn_url varchar(max) = ''
AS            
BEGIN            
	SET NOCOUNT ON;
	BEGIN TRY

	DECLARE @RdsnCmplt TABLE (ID INT IDENTITY(1,1), REDSGN_ID INT, FLAG TINYINT)
	DECLARE @Ctr INT
	DECLARE @Cnt INT
	DECLARE @RdsnID INT
	DECLARE @TotalCnt INT
	DECLARE @CmpltCnt INT
	DECLARE @InCmpltCnt INT

	SET @Ctr = 1
	SET @Cnt = 0
	SET @TotalCnt = 0
	SET @CmpltCnt = 0
	SET @InCmpltCnt = 0


	INSERT INTO @RdsnCmplt (REDSGN_ID, FLAG)
	SELECT DISTINCT rd.REDSGN_ID, 0
	FROM dbo.EVENT_DEV_CMPLT edc WITH (NOLOCK) INNER JOIN
	dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK) ON edc.REDSGN_DEV_ID=rdi.REDSGN_DEV_ID INNER JOIN
	dbo.REDSGN rd WITH (NOLOCK) ON rd.REDSGN_ID = rdi.REDSGN_ID
	WHERE edc.CMPLTD_CD = 1
	AND edc.EVENT_ID = @EventID
	UNION
	select DISTINCT rdi.REDSGN_ID,0
	from dbo.FSA_MDS_EVENT_NEW fm with (nolock) inner join
	dbo.MDS_EVENT_ODIE_DEV_NME modn with (nolock) on modn.FSA_MDS_EVENT_ID = fm.FSA_MDS_EVENT_ID inner join
	dbo.MDS_MNGD_ACT_NEW mma with (nolock) on mma.ODIE_DEV_NME = modn.ODIE_DEV_NME and fm.event_id = mma.event_id inner join
	dbo.REDSGN_DEVICES_INFO rdi with (nolock) on mma.ODIE_DEV_NME = rdi.DEV_NME inner join
	dbo.REDSGN rd with (nolock) on rd.REDSGN_NBR=mma.RDSN_NBR and rd.redsgn_id=rdi.redsgn_id
	where fm.event_id=@EventID
	AND fm.CMPLTD_CD = 1
	AND rdi.REC_STUS_ID = 1

	SELECT @Cnt = COUNT(*) FROM @RdsnCmplt

	IF (@Cnt > 0)
	BEGIN
		WHILE (@Ctr <= @Cnt)
		BEGIN
		SELECT TOP 1 @RdsnID = REDSGN_ID
		FROM @RdsnCmplt
		WHERE ID = @Ctr
		  AND FLAG = 0

		SELECT @TotalCnt = COUNT(1)
						FROM dbo.REDSGN_DEVICES_INFO WITH (NOLOCK)
						WHERE REDSGN_ID = @RdsnID
						  AND REC_STUS_ID=1

		SELECT @InCmpltCnt = COUNT(1)
						FROM dbo.REDSGN_DEVICES_INFO WITH (NOLOCK)
						WHERE REDSGN_ID = @RdsnID
						  AND REC_STUS_ID=1
						  AND (ISNULL(DEV_CMPLTN_CD,0)=0)


		SELECT @CmpltCnt = COUNT(1)
						FROM dbo.REDSGN_DEVICES_INFO WITH (NOLOCK)
						WHERE REDSGN_ID = @RdsnID
						  AND REC_STUS_ID=1
						  AND DEV_CMPLTN_CD=1

		IF ((@InCmpltCnt=0) AND (@TotalCnt = @CmpltCnt))
		BEGIN
			UPDATE dbo.REDSGN
			SET STUS_ID = 227,
				MODFD_DT = GETDATE()
			WHERE REDSGN_ID = @RdsnID
		
			--EXEC dbo.insertRedesignEmailData  @RdsnID, 227, 0
			EXEC dbo.InsertRedesignEmailData_V2 @RdsnID, 227, 0, 0, @redsgn_url
		END

		UPDATE @RdsnCmplt SET FLAG=1 WHERE REDSGN_ID = @RdsnID AND FLAG=0
		SET @Ctr = @Ctr + 1
		END
	END


	END TRY            
            
	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]            
	END CATCH            
END