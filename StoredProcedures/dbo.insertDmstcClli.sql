USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertDmstcClli]    Script Date: 01/26/2021 10:13:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	David Phillips
-- Create date: 2/28/2016
-- Description:	Loads the LK_DMSTC_CLLI_CD table nightly for Drop Down in Appian Mat Req.
-- =============================================
ALTER PROCEDURE [dbo].[insertDmstcClli] 
 	
AS
BEGIN TRY

	DECLARE @loadclli varchar(11)
	
	DECLARE @CLLI table (srvCLLI varchar(11))
	
	INSERT INTO @CLLI (srvCLLI) 
		 (SELECT distinct[CLLI_Code] FROM dbo.V_V5U_SSTAT_INFO)
	
	IF EXISTS (SELECT * from @CLLI)
		BEGIN
		   DELETE [COWS].[dbo].[LK_DMSTC_CLLI_CD] WHERE CLLI_CD NOT IN ('MTLDFLTBACP','DLLSTX53ACP','OVPKKSTDACP','RVSDMO09ACP','HOUSTX01CPE','AUSTTX03CPE')
		   WHILE EXISTS (SELECT * FROM @CLLI)
			 BEGIN
				SELECT TOP 1 @loadclli = srvCLLI FROM @CLLI
		  
				  INSERT INTO [COWS].[dbo].[LK_DMSTC_CLLI_CD]
						([CLLI_CD],[CREAT_DT],[MODFD_DT],[MODFD_BY_USER_ID],[CREAT_BY_USER_ID],[REC_STUS_ID])
					VALUES
						(@loadclli,GETDATE(),null,null,1,1)

				DELETE @CLLI WHERE srvCLLI = @loadclli
			
			  END
		END
		
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH



