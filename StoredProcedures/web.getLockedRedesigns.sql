USE [COWS]
GO
_CreateObject 'SP','web','getLockedRedesigns'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [web].[getLockedRedesigns] 
	@REDID varchar(10)
AS
BEGIN
BEGIN TRY
	SET NOCOUNT ON;

    SELECT [t0].[REDSGN_NBR], [t1].[LOCK_BY_USER_ID], [t3].[FULL_NME], [t2].[REDSGN_TYPE_NME]
    FROM [dbo].[REDSGN] AS [t0] WITH (NOLOCK)
    INNER JOIN [dbo].[REDSGN_REC_LOCK] AS [t1] WITH (NOLOCK) ON [t0].[REDSGN_ID] = [t1].[REDSGN_ID] 
    INNER JOIN [dbo].[LK_REDSGN_TYPE] AS [t2] WITH (NOLOCK) ON [t0].[REDSGN_TYPE_ID] = [t2].[REDSGN_TYPE_ID]
    INNER JOIN [dbo].[LK_USER] AS [t3] WITH (NOLOCK) ON [t3].[USER_ID] = [t1].[LOCK_BY_USER_ID]
    WHERE [t0].[REDSGN_NBR] = @REDID

END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END
