USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getPeopleSoftLnItms]    Script Date: 12/19/2019 8:12:38 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 08/11/2015
-- Description:	Gets data for PeopleSoft Batch process.
-- =========================================================
ALTER PROCEDURE [dbo].[getPeopleSoftLnItms]
	@REQSTN_NBR varchar(10) 
        
AS
BEGIN TRY
				
	SELECT 
	   ltm.REQ_LINE_ITM_ID
      ,ltm.ORDR_ID
      ,ltm.CMPNT_ID
      ,ltm.REQSTN_NBR
      ,Left(ISNULL(ltm.MAT_CD,''),6)		AS [MAT_CD]
      ,LEFT(ISNULL(ltm.DLVY_CLLI,''),10)				AS [DLVY_CLLI]
      ,LEFT(ISNULL(ltm.ITM_DES,''),254)				AS [ITM_DES]
      ,LEFT(ISNULL(ltm.MANF_PART_NBR,''),20)			AS [MANF_PART_NBR]
      ,LEFT(ISNULL(ltm.MANF_ID,''),10)  			AS [MANF_ID]
      ,LEFT(ISNULL(ltm.ORDR_QTY,0),11)				AS [ORDR_QTY]
      ,LEFT(ISNULL(ltm.UNT_MSR,''),3)				AS [UNT_MSR]
      ,LEFT(ISNULL(ltm.UNT_PRICE,0),15)				AS [UNT_PRICE]
      ,Convert(varchar(8),ltm.RAS_DT,112)			AS [RAS_DT]
      ,LEFT(ISNULL(lvs.VNDR_CD,''),10)				AS [VNDR_NME]
      ,LEFT(ISNULL(ltm.BUS_UNT_GL,''),5)            AS [BUS_UNT_GL]
      ,LEFT(ISNULL(ltm.ACCT,''),6)                  AS [ACCT]
      ,LEFT(ISNULL(ltm.COST_CNTR,0),10)             AS [COST_CNTR]
      ,LEFT(ISNULL(ltm.PRODCT,''),6)                AS [PRODCT]
      ,LEFT(ISNULL(ltm.MRKT,''),4)                  AS [MRKT]
      ,LEFT(ISNULL(ltm.AFFLT,''),5)                 AS [AFFLT]
      ,LEFT(ISNULL(ltm.REGN,''),6)                  AS [REGN]
      ,LEFT(ISNULL(ltm.PROJ_ID, ''),15)             AS [PROJ_ID]
      ,LEFT(ISNULL(ltm.BUS_UNT_PC,''),5)            AS [BUS_UNT_PC]
      ,LEFT(ISNULL(ltm.ACTVY,''),15)                AS [ACTVY]
      ,LEFT(ISNULL(ltm.SOURCE_TYP,''),5)            AS [SOURCE_TYP]
      ,LEFT(ISNULL(ltm.RSRC_CAT,''),5)              AS [RSRC_CAT]
      ,LEFT(ISNULL(ltm.RSRC_SUB,''),5)              AS [RSRC_SUB]
      ,LEFT(ISNULL(ltm.CNTRCT_ID,''),25)            AS [CNTRCT_ID]
      ,LEFT(ISNULL(ltm.CNTRCT_LN_NBR,0),5)          AS [CNTRCT_LN_NBR]
      ,LEFT(ISNULL(ltm.AXLRY_ID,''),24)             AS [AXLRY_ID]
      ,LEFT(ISNULL(ltm.INST_CD,''),10)              AS [INST_CD]
      ,ltm.REC_STUS_ID
      ,ltm.CREAT_DT
      ,ltm.SENT_DT
      ,ltm.FSA_CPE_LINE_ITEM_ID
      ,ltm.EQPT_TYPE_ID
      ,CASE
		WHEN fli.DROP_SHP = 'Y' THEN LEFT(ISNULL(ltm.COMMENTS,''),512)
		WHEN fli.MFR_NME = 'CSCO' AND fli.CMPNT_FMLY in ('Router mnt','Switch mnt','Ip mnt')
			AND NOT EXISTS (SELECT 'x' FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) WHERE MANF_PART_CD = 'NONMNGINST' )  
			THEN 'Do not order maintenance. Maintainance req created by CSC. ' 
			+ LEFT(ISNULL(ltm.COMMENTS,''),451)
		WHEN ISNULL(ltm.MANF_DISCNT_CD,'') != '' THEN 'Deal ID: ' + ltm.MANF_DISCNT_CD + '   ' + LEFT(ISNULL(ltm.COMMENTS,''),400)
		ELSE LEFT(ISNULL(ltm.COMMENTS,''),512)
		END               AS [COMMENTS]
      ,LEFT(ISNULL(ltm.MANF_DISCNT_CD,''),500)         AS [MANF_DISCNT_CD]
      ,ltm.REQ_LINE_NBR
      ,LEFT(ISNULL(hdr.RFQ_INDCTR,''),1)             AS [RFQ_INDCTR]
  FROM dbo.PS_REQ_LINE_ITM_QUEUE ltm WITH (NOLOCK)
	JOIN dbo.PS_REQ_HDR_QUEUE hdr WITH (NOLOCK) ON ltm.REQSTN_NBR = hdr.REQSTN_NBR
	INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fli WITH (NOLOCK) 
		on fli.FSA_CPE_LINE_ITEM_ID = ltm.FSA_CPE_LINE_ITEM_ID
	LEFT OUTER JOIN  dbo.LK_VNDR_SCM lvs WITH (NOLOCK) ON lvs.VNDR_NME = ltm.VNDR_NME 
    WHERE ltm.REQSTN_NBR = @REQSTN_NBR 	
    
		
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH


