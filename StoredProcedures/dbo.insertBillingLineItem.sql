USE [COWS]
GO
_CreateObject 'SP','dbo','insertBillingLineItem'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/15/2011
-- Description:	Inserts a FSA Billing Line Item into the FSA_ORDR_BILL_LINE_ITEM table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertBillingLineItem]
	@FSA_ORDR_BILL_LINE_ITEM_ID	Int	OUT
	,@ORDR_ID					Int
	,@BILL_ITEM_TYPE_ID			SmallInt
	,@BIC_CD					Varchar(10)
	,@LINE_ITEM_DES				Varchar(50)
	,@LINE_ITEM_QTY				SmallInt
	,@ITEM_CD					Varchar(10)
	,@ITEM_DES					Varchar(40)
	,@MRC_CHG_AMT				Varchar(20)
	,@NRC_CHG_AMT				Varchar(20)
	,@ACTN_CD					Varchar(10)
	,@CHG_CD					Varchar(6)
	,@FSA_CPE_LINE_ITEM_ID		Int		=	0
	
	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	INSERT INTO dbo.FSA_ORDR_BILL_LINE_ITEM WITH (ROWLOCK)
					(ORDR_ID
					,BILL_ITEM_TYPE_ID
					,BIC_CD
					,LINE_ITEM_DES
					,LINE_ITEM_QTY
					,ITEM_CD
					,ITEM_DES
					,MRC_CHG_AMT
					,NRC_CHG_AMT
					,ACTN_CD
					,CHG_CD
					,FSA_CPE_LINE_ITEM_ID
					)
		VALUES		(@ORDR_ID
					,@BILL_ITEM_TYPE_ID
					,@BIC_CD
					,@LINE_ITEM_DES
					,@LINE_ITEM_QTY
					,@ITEM_CD
					,@ITEM_DES
					,@MRC_CHG_AMT
					,@NRC_CHG_AMT
					,@ACTN_CD
					,@CHG_CD
					,Case	ISNULL(@FSA_CPE_LINE_ITEM_ID, 0)
						When	0	Then	NULL
						Else	@FSA_CPE_LINE_ITEM_ID
						End
					)
		SELECT @FSA_ORDR_BILL_LINE_ITEM_ID = SCOPE_IDENTITY()
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END

GO