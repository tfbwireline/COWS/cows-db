USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getSSTATCancel]    Script Date: 05/10/2017 14:02:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		dlp0278
-- Create date: 10/8/2015
-- Description:	Get the details for all STTAT Cancel requests
-- =========================================================
ALTER PROCEDURE [dbo].[getSSTATCancel] 
	@TRAN_ID  int

AS
BEGIN
SET NOCOUNT ON;
Begin Try

	SELECT DISTINCT sr.TRAN_ID	
					,fo.FTN
					,c.DEVICE_ID
					,'Cancel'  AS OrderAction
					
		FROM		dbo.SSTAT_REQ sr WITH (NOLOCK)
		INNER JOIN dbo.FSA_ORDR fo	WITH (NOLOCK) ON sr.ORDR_ID = fo.ORDR_ID
		INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM c WITH (NOLOCK)ON c.ORDR_ID = sr.ORDR_ID
		where TRAN_ID = @TRAN_ID AND ISNULL(c.DEVICE_ID,'') != ''
		
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

