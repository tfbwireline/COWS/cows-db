USE [COWS]
GO
_CreateObject 'SP','dbo','insertODIEReq_V5U'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 06/02/2015
-- Description:	Inserts an ODIE Request into the ODIE_REQ table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertODIEReq_V5U]
			@ORDR_ID int        
			,@ODIE_MSG_ID int
        
AS
BEGIN
SET NOCOUNT ON;


DECLARE	@CurrDate	DateTime
SET	@CurrDate	=	getDate()

Begin Try

		IF EXISTS 
				(SELECT 'X' FROM dbo.FSA_ORDR WITH (NOLOCK) where ORDR_ID = @ORDR_ID AND
									 ORDR_SUB_TYPE_CD  IN ('ROCP'))
				 BEGIN
					EXEC dbo.insertOrderNotes @ORDR_ID,1,'No message sent to ODIE for these order types.',1
				 END
		ELSE
				BEGIN
					INSERT INTO dbo.ODIE_REQ WITH (ROWLOCK)
							   ([ORDR_ID],[ODIE_MSG_ID],[STUS_MOD_DT],[H1_CUST_ID],[CREAT_DT],[STUS_ID]
							   ,[MDS_EVENT_ID],[TAB_SEQ_NBR],[CUST_NME],[ODIE_CUST_ID])
						VALUES		
								( @ORDR_ID, @ODIE_MSG_ID, @CurrDate, null, @CurrDate, 10
								, null, null, null, null)	
					EXEC dbo.insertOrderNotes @ORDR_ID,9,'Order Information successfully sent to ODIE.',1
				END
End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END

