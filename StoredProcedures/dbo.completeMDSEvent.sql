USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[completeMDSEvent]    Script Date: 02/25/2017 06:48:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 09/07/2011
-- Description:	Completes the Event and loads the Bill Activation Ready Task.
-- =========================================================
-- =========================================================
-- Altered By:		Naidu Vattigunta
-- Create date: 09/21/2011
-- Description:	Completes the Event and loads the Bill Activation Ready Task for Disconnects.
-- =========================================================
ALTER PROCEDURE [dbo].[completeMDSEvent]
@EVENT_ID		Int
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	--------------------------------------------------------------------------------
	--	Get all the Orders that are in Completed State.
	--------------------------------------------------------------------------------
	DECLARE	@Order	TABLE
		(ORDR_ID		Int
		,TAB_SEQ_NBR	TinyInt)
		
	INSERT INTO @Order	(ORDR_ID,	TAB_SEQ_NBR)
		SELECT DISTINCT	ORDR_ID,	TAB_SEQ_NBR	
			FROM	dbo.FSA_MDS_EVENT_ORDR	fo	WITH (NOLOCK)
			WHERE	fo.EVENT_ID		=	@EVENT_ID
				AND	fo.CMPLTD_CD	=	1
				AND	NOT EXISTS
					(SELECT 'X' 
						FROM	dbo.FSA_MDS_EVENT_ORDR	fme WITH (NOLOCK)
					INNER JOIN	dbo.MDS_EVENT_NEW			mds WITH (NOLOCK) ON mds.EVENT_ID = fme.EVENT_ID
						WHERE	fme.ORDR_ID			=	fo.ORDR_ID
							AND	fme.TAB_SEQ_NBR		!=	fo.TAB_SEQ_NBR
							AND	mds.EVENT_STUS_ID	!=	8
							AND	fme.CMPLTD_CD		=	0)
							
	IF	((SELECT Count(1) FROM @Order) > 0)						
		BEGIN				
			--------------------------------------------------------------------------------
			--	Release the Regular Install Orders from BAR On-Hold Status.
			--------------------------------------------------------------------------------
			UPDATE			ct
				SET			ct.STUS_ID		=	0, MODFD_DT = GETDATE()
				FROM		dbo.ACT_TASK	ct	WITH (ROWLOCK)
				INNER JOIN	@Order			vt					ON	ct.ORDR_ID	=	vt.ORDR_ID
				INNER JOIN	dbo.ORDR		o	WITH (NOLOCK)	ON	o.ORDR_ID	=	vt.ORDR_ID
				INNER JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK)	ON	lp.PPRT_ID	=	o.PPRT_ID
				WHERE		ct.TASK_ID			=	1000
				AND			ct.STUS_ID			=	3	
				AND			lp.TRPT_CD			=	0
				
			--------------------------------------------------------------------------------
			--	Release the Related Disconnect Orders from BAR On-Hold Status.
			--------------------------------------------------------------------------------
			UPDATE			ct
				SET			ct.STUS_ID	=	0, MODFD_DT = GETDATE()
				FROM		@Order			vt
				INNER JOIN	dbo.FSA_ORDR	fs	WITH (NOLOCK)	ON	fs.ORDR_ID	=	vt.ORDR_ID
				INNER JOIN	dbo.FSA_ORDR	fp	WITH (NOLOCK)	ON	fs.FTN		=	fp.RELTD_FTN
				INNER JOIN	dbo.ACT_TASK	ct	WITH (ROWLOCK)	ON	ct.ORDR_ID	=	fp.ORDR_ID
				INNER JOIN	dbo.ORDR		o	WITH (NOLOCK)	ON	o.ORDR_ID	=	ct.ORDR_ID
				INNER JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK)	ON	lp.PPRT_ID	=	o.PPRT_ID
				AND			ct.TASK_ID		=	1000
				AND			ct.STUS_ID		=	3	
				AND			fp.ORDR_ACTN_ID	=	2
				AND			lp.TRPT_CD		=	0
				
			--------------------------------------------------------------------------------
			--	Send a Design Complete Message to ODIE .
			--------------------------------------------------------------------------------	
			INSERT INTO		dbo.ODIE_REQ	WITH (ROWLOCK)	
								(ODIE_MSG_ID
								,STUS_ID
								,STUS_MOD_DT
								,MDS_EVENT_ID
								,TAB_SEQ_NBR)
				SELECT DISTINCT	3
								,9
								,getDate()
								,@EVENT_ID
								,fo.TAB_SEQ_NBR
						FROM	dbo.MDS_EVENT_NEW me with (nolock)
						Inner Join dbo.FSA_MDS_EVENT_ORDR	fo	WITH (NOLOCK) ON me.EVENT_ID = fo.EVENT_ID
						WHERE	fo.EVENT_ID	=	@EVENT_ID
							AND	fo.CMPLTD_CD	=	1
							AND me.MDS_ACTY_TYPE_ID != 6
							AND	NOT EXISTS
								(SELECT 'X' FROM dbo.ODIE_REQ	WITH (NOLOCK)
									WHERE	MDS_EVENT_ID	=	@EVENT_ID
										AND	TAB_SEQ_NBR		=	fo.TAB_SEQ_NBR
										AND	ODIE_MSG_ID		=	3)
				UNION
				SELECT DISTINCT	3
								,9
								,getDate()
								,@EVENT_ID
								,fo.TAB_SEQ_NBR
						FROM		dbo.MDS_EVENT_NEW me with (nolock)
						Inner Join	dbo.FSA_MDS_EVENT_NEW	fo	WITH (NOLOCK) ON me.EVENT_ID = fo.EVENT_ID
						WHERE	me.EVENT_ID	=	@EVENT_ID
							AND me.MDS_ACTY_TYPE_ID	=	6
							AND ((me.EVENT_STUS_ID	= 18) OR (fo.CMPLTD_CD	= 1))
							AND	((SELECT COUNT('X') FROM dbo.ODIE_REQ	WITH (NOLOCK)
									WHERE	MDS_EVENT_ID	=	@EVENT_ID
										AND	TAB_SEQ_NBR		=	fo.TAB_SEQ_NBR
										AND	ODIE_MSG_ID		=	3) <= 2)
			--------------------------------------------------------------------------------
			--	Change CCD of all related order linked with H5/H6 and CCD provided CCD is 
			--	not next day.
			--------------------------------------------------------------------------------
					DECLARE @tblParentID TABLE (PRNT_ORDR_ID INT,CUST_COMMT_DT DATETIME,FLAG BIT DEFAULT 0)
					INSERT INTO @tblParentID (PRNT_ORDR_ID,CUST_COMMT_DT)
					SELECT DISTINCT ord.PRNT_ORDR_ID,ISNULL(ord.CUST_CMMT_DT,ISNULL(fsa.CUST_CMMT_DT,''))
						FROM	dbo.ORDR		ord WITH (NOLOCK)
					INNER JOIN	dbo.FSA_ORDR	fsa	WITH (NOLOCK)	ON	ord.PRNT_ORDR_ID	=	fsa.ORDR_ID
					INNER JOIN	@Order			vt					ON	vt.ORDR_ID			=	ord.ORDR_ID
						
					DECLARE @Count		INT = 0
					DECLARE @Ctr		INT = 0
					DECLARE @PRNT_ORDR_ID	INT = 0
					DECLARE @new_ccd		DATE
					DECLARE @old_ccd		DATETIME
					DECLARE @out		INT
					DECLARE @nte		VARCHAR(100) = 'New CCD date updated as a result of completion of EventID: ' + CONVERT(VARCHAR,@Event_ID)
					DECLARE @FTNs		VARCHAR(1000)
					SELECT @Count = COUNT(1) FROM @tblParentID
					WHILE @Ctr < @Count
						BEGIN
							SET @PRNT_ORDR_ID = 0
							SET @FTNs = ''
							SET @new_ccd = GETDATE() + 1
							SET @out = 0
							SELECT TOP 1	@PRNT_ORDR_ID	=	PRNT_ORDR_ID,
											@old_ccd		=	CUST_COMMT_DT
								FROM	@tblParentID 
								WHERE	FLAG = 0
							
							Select @FTNs  = COALESCE(CONVERT(VARCHAR,fsa.FTN) + ',', @FTNs) + @FTNs 
								From	dbo.ORDR odr WITH (NOLOCK) 
							Inner Join	dbo.FSA_ORDR fsa WITH (NOLOCK) ON fsa.ORDR_ID = odr.ORDR_ID 
								WHERE	odr.PRNT_ORDR_ID = @PRNT_ORDR_ID 

							IF (@new_ccd  < CONVERT(DATE,@old_ccd))
								BEGIN
									SET @new_ccd = GETDATE()
									EXEC dbo.insertCCDChange @FTNs,@new_ccd,'',@nte,1,@out OUTPUT
								END
							
							UPDATE @tblParentID SET FLAG = 1 WHERE PRNT_ORDR_ID = @PRNT_ORDR_ID
							SET @Ctr = @Ctr + 1	
						END						
		END
	--------------------------------------------------------------------------------
	--	For manual Events, load it up from different tables .
	--------------------------------------------------------------------------------		
	ELSE
		BEGIN
			INSERT INTO		dbo.ODIE_REQ	WITH (ROWLOCK)	
								(ODIE_MSG_ID
								,STUS_ID
								,STUS_MOD_DT
								,MDS_EVENT_ID
								,TAB_SEQ_NBR)
				SELECT DISTINCT	3
								,9
								,getDate()
								,@EVENT_ID
								,fo.TAB_SEQ_NBR
						FROM	dbo.MDS_EVENT_NEW me with (nolock)
						Inner Join	dbo.FSA_MDS_EVENT_NEW	fo	WITH (NOLOCK) ON me.EVENT_ID = fo.EVENT_ID
						WHERE	fo.EVENT_ID	=	@EVENT_ID
							AND	fo.CMPLTD_CD	=	1
							AND me.MDS_ACTY_TYPE_ID != 6	
							AND	NOT EXISTS
								(SELECT 'X' FROM dbo.ODIE_REQ	WITH (NOLOCK)
									WHERE	MDS_EVENT_ID	=	@EVENT_ID
										AND	TAB_SEQ_NBR		=	fo.TAB_SEQ_NBR
										AND	ODIE_MSG_ID		=	3)
										
				UNION

				SELECT DISTINCT	3
								,9
								,getDate()
								,@EVENT_ID
								,fo.TAB_SEQ_NBR
						FROM		dbo.MDS_EVENT_NEW me with (nolock)
						Inner Join	dbo.FSA_MDS_EVENT_NEW	fo	WITH (NOLOCK) ON me.EVENT_ID = fo.EVENT_ID
						WHERE	me.EVENT_ID	=	@EVENT_ID
							AND me.MDS_ACTY_TYPE_ID	=	6
							AND ((me.EVENT_STUS_ID	= 18) OR (fo.CMPLTD_CD	= 1))
							AND	((SELECT COUNT('X') FROM dbo.ODIE_REQ	WITH (NOLOCK)
									WHERE	MDS_EVENT_ID	=	@EVENT_ID
										AND	TAB_SEQ_NBR		=	fo.TAB_SEQ_NBR
										AND	ODIE_MSG_ID		=	3) <= 2)
		
		END
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
