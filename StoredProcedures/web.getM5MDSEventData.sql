USE [COWS]
GO
_CreateObject 'SP','web','getM5MDSEventData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  jrg7298
-- Create date: 07/01/2015          
-- Description: Get the required data for an MDS Event when a H5/H6 value is entered.            
-- =============================================            
ALTER PROCEDURE [web].[getM5MDSEventData] --'989340232', 1             
 @H5_H6_CUST_ID  Varchar(20) = ''                   
 ,@Tab_Seq_Nbr TinyInt            
AS            
BEGIN            
SET NOCOUNT ON            
Begin Try            
            
OPEN SYMMETRIC KEY FS@K3y             
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;            
            
IF OBJECT_ID(N'tempdb..#M5MDSTable', N'U') IS NOT NULL         
	DROP TABLE #M5MDSTable 

IF OBJECT_ID(N'tempdb..#M5CUSTTable', N'U') IS NOT NULL         
	DROP TABLE #M5CUSTTable

IF OBJECT_ID(N'tempdb..#M5ADRTable', N'U') IS NOT NULL         
	DROP TABLE #M5ADRTable

CREATE TABLE #M5ADRTable
(M5_ORDR_ID VARCHAR(20),
CIS_HIER_LVL_CD VARCHAR(2),
LINE_1	VARCHAR(255),
LINE_2	VARCHAR(255),
LINE_3	VARCHAR(255),
CTY_NME	VARCHAR(255),
ST_PRVN_CD	VARCHAR(2),
PSTL_CD	VARCHAR(15),
CTRY_CD	VARCHAR(2),
BLDG_ID	VARCHAR(255),
ROOM_ID	VARCHAR(255),
FLOOR_ID	VARCHAR(255),
DMSTC_CD tinyint)

CREATE TABLE #M5CUSTTable
( M5_ORDR_ID VARCHAR(20),
CUST_NME	VARCHAR(511),
TME_ZN_CD	VARCHAR(255),
AVLBLTY_HR_STRT	VARCHAR(5),
AVLBLTY_HR_END	VARCHAR(5),
PHN_NBR	VARCHAR(30),
EMAIL_ADR	VARCHAR(255),
ROLE_CD	VARCHAR(6),
DOM_INTL_IND_CD	CHAR(1),
DOM_PHN_NBR	VARCHAR(10),
INTL_CNTRY_DIAL_CD	VARCHAR(3),
INTL_PHN_NBR	VARCHAR(30),
PHN_EXT	VARCHAR(5))

CREATE TABLE #M5MDSTable
 (M5_ORDR_ID   VARCHAR(20)   NULL            
 ,PROD_TYPE_CD  VARCHAR(5) NULL
 ,ORDR_TYPE_CD VARCHAR(10) NULL
 ,DMSTC_CD tinyint NULL
 ,CIS_CUST_ID VARCHAR(9) NULL
 ,LINE_ITEM_DES VARCHAR(100) NULL
 ,INSTL_SRVC_TIER_CD VARCHAR(10) NULL
 ,TTRPT_ACCS_TYPE_CD VARCHAR(10) NULL
 ,TTRPT_NW_ADR VARCHAR(50) NULL
 ,INSTL_TRNSPRT_TYPE_CD VARCHAR(50) NULL
 ,CSG_LVL VARCHAR(2) NULL)     
         
            
IF @H5_H6_CUST_ID != ''           
 BEGIN            

 --MPLS WPaaS, DIA WPaaS, WPaaS Product, Standalone MNS, Managed WiFi Product, Standalone MSS
 -- NOT IN (Completed, Cancelled, Rejected)
 -- NOT IN (Full Cancel, Partial Cancel, Full Disconnect, Partial Disconnect)
declare @ordr nvarchar(max) = 'INSERT INTO #M5MDSTable (M5_ORDR_ID, PROD_TYPE_CD, ORDR_TYPE_CD, DMSTC_CD, CIS_CUST_ID, 
							LINE_ITEM_DES, INSTL_SRVC_TIER_CD, TTRPT_ACCS_TYPE_CD, CSG_LVL) Select 
							ORDER_NBR, PROD_ID, ORDR_TYPE_CD, CASE DOM_PROCESS_FLG_CD WHEN ''Y'' THEN 1 ELSE 0 END, CIS_CUST_ID, '''', '''', '''', ISNULL(CSG_LVL,''0'')  from openquery(M5,''select DISTINCT vbo.ORDER_NBR, 
							vbo.PROD_ID, vbo.ORDR_TYPE_CD, vbo.DOM_PROCESS_FLG_CD, ca.CIS_CUST_ID, ca.CSG_LVL
							from MACH5.V_V5U_ORDR vbo,
							MACH5.V_V5U_CUST_ACCT ca where vbo.ORDR_SUBTYPE_CD NOT IN (''''FD'''', ''''PD'''', ''''FC'''', ''''PC'''') 
							AND vbo.PROD_ID IN (''''MSS'''', ''''MNS'''', ''''MWIFI'''', ''''WPPRD'''', ''''WPDIA'''', ''''WPMPL'''') 
							AND ca.CIS_HIER_LVL_CD IN (''''H5'''', ''''H6'''') 
							AND ca.CUST_ACCT_ID = vbo.PRNT_ACCT_ID 
							AND vbo.ORDR_STUS_CD NOT IN (''''CP'''', ''''CN'''', ''''RJ'''') 
							AND ca.CIS_CUST_ID = '''''+ @H5_H6_CUST_ID +''''' '')'

exec sp_executesql @ordr

declare @ordrids varchar(max)
select @ordrids = COALESCE(@ordrids + ''''',''''', '''''') + M5_ORDR_ID
FROM #M5MDSTable
set @ordrids =  substring(@ordrids, 2, len(@ordrids)-1)

declare @cust nvarchar(max) = 'INSERT INTO #M5CUSTTable (M5_ORDR_ID, CUST_NME, TME_ZN_CD, AVLBLTY_HR_STRT, AVLBLTY_HR_END, PHN_NBR, EMAIL_ADR,
								ROLE_CD, DOM_INTL_IND_CD, DOM_PHN_NBR, INTL_CNTRY_DIAL_CD, INTL_PHN_NBR, PHN_EXT)
							   SELECT ORDER_NBR, CUST_NME, TME_ZN_CD, AVLBLTY_HR_STRT, AVLBLTY_HR_END, PHN_NBR, EMAIL_ADR,
								ROLE_CD, DOM_INTL_IND_CD, DOM_PHN_NBR, INTL_CNTRY_DIAL_CD, INTL_PHN_NBR, PHN_EXT
								FROM OPENQUERY(M5, ''select DISTINCT vbo.ORDER_NBR, vbcc.CUST_NME, vbcc.TME_ZN_CD, vbcc.AVLBLTY_HR_STRT, 
								vbcc.AVLBLTY_HR_END, vbcc.PHN_NBR, vbcc.EMAIL_ADR, vbcc.ROLE_CD, 
								vbcc.DOM_INTL_IND_CD, vbcc.DOM_PHN_NBR, vbcc.INTL_CNTRY_DIAL_CD, vbcc.INTL_PHN_NBR, vbcc.PHN_EXT
								FROM MACH5.V_V5U_CUST_CONTACT vbcc,
								MACH5.V_V5U_ORDR vbo,
								MACH5.V_V5U_CUST_ACCT vbca
								where vbo.PRNT_ACCT_ID = vbca.CUST_ACCT_ID
								  and vbca.CUST_ACCT_ID = vbcc.CUST_ACCT_ID
								  and vbo.ORDER_NBR IN (''' + @ordrids + ''''') '')
								'

exec sp_executesql @cust

declare @adr nvarchar(max) = 'INSERT INTO #M5ADRTable (M5_ORDR_ID, CIS_HIER_LVL_CD, LINE_1, LINE_2, LINE_3, CTY_NME, ST_PRVN_CD,
										PSTL_CD, CTRY_CD, BLDG_ID, ROOM_ID, FLOOR_ID, DMSTC_CD)
							   SELECT ORDER_NBR, CIS_HIER_LVL_CD, LINE_1, LINE_2, LINE_3, CTY_NME, ST_PRVN_CD,
										PSTL_CD, CTRY_CD, BLDG_ID, ROOM_ID, FLOOR_ID, CASE DOM_PROCESS_FLG_CD WHEN ''Y'' THEN 1 ELSE 0 END
								FROM OPENQUERY(M5, ''select DISTINCT vbo.ORDER_NBR, vbca.CIS_HIER_LVL_CD, vbca.LINE_1, vbca.LINE_2, vbca.LINE_3, vbca.CTY_NME, vbca.ST_PRVN_CD,
										vbca.PSTL_CD, vbca.CTRY_CD, vbca.BLDG_ID, vbca.ROOM_ID, vbca.FLOOR_ID, vbo.DOM_PROCESS_FLG_CD
								FROM MACH5.V_V5U_ORDR vbo,
									MACH5.V_V5U_CUST_ACCT vbca
								where vbo.PRNT_ACCT_ID = vbca.CUST_ACCT_ID
								  and vbo.ORDER_NBR IN (''' + @ordrids + ''''') '')
								'
exec sp_executesql @adr

 END            
          
IF (SELECT Count(1) FROM #M5MDSTable) > 0            
 BEGIN             
  IF @H5_H6_CUST_ID != 0            
   BEGIN            
 -----------------------------------------------------------------------            
    -- Table 00 - ODIE Dev Name.            
    -----------------------------------------------------------------------            
 SELECT DISTINCT @Tab_Seq_Nbr      AS TAB_SEQ_NBR,      
     0      AS FSA_MDS_EVENT_ID,      
     ' '      AS ODIE_DEV_NME,      
     0      AS dtMDSODIENmeInfo      
 FROM #M5MDSTable tt 
       
 -----------------------------------------------------------------------            
    -- Table 01 - Access Info FTN Table.            
    -----------------------------------------------------------------------             
 SELECT DISTINCT @Tab_Seq_Nbr    AS TAB_SEQ_NBR,      
     0       AS FSA_MDS_EVENT_ID,      
     tt.M5_ORDR_ID      AS ACCESS_FTN,      
     ISNULL(fo.ORDR_ID, 0)    AS ORDR_ID,      
     0       AS AccessInfoID         
 FROM #M5MDSTable tt LEFT JOIN       
   dbo.FSA_ORDR AS fo WITH (NOLOCK) ON fo.FTN = tt.M5_ORDR_ID           
 WHERE tt.PROD_TYPE_CD NOT IN ('MNS', 'EQUIP', 'MSS')        -- NOT IN CPE, MNS, MSS    
   AND tt.ORDR_TYPE_CD in ('IN')      
       
 -----------------------------------------------------------------------            
    -- Table 02 - CPE Info FTN Table.            
    -----------------------------------------------------------------------             
 SELECT DISTINCT @Tab_Seq_Nbr     AS TAB_SEQ_NBR,      
     0        AS FSA_MDS_EVENT_ID,      
     tt.M5_ORDR_ID       AS CPE_FTN,     
     ISNULL(fo.ORDR_ID, 0)   AS ORDR_ID,      
     ISNULL(fob.LINE_ITEM_DES,'') AS EQUIP_DESC,            
     --ISNULL(foc.MFR_NME, '')   AS EQUIP_VNDR,    
     '' AS EQUIP_VNDR,        
     0        AS CPEInfoID            
  FROM #M5MDSTable tt LEFT JOIN       
    dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN = tt.M5_ORDR_ID  LEFT JOIN       
    dbo.FSA_ORDR_CPE_LINE_ITEM foc WITH (NOLOCK) ON fo.ORDR_ID = foc.ORDR_ID LEFT JOIN       
    dbo.FSA_ORDR_BILL_LINE_ITEM fob WITH (NOLOCK) ON foc.FSA_CPE_LINE_ITEM_ID = fob.FSA_CPE_LINE_ITEM_ID            
  WHERE  tt.ORDR_TYPE_CD IN ('IN')            
    AND  ( (tt.DMSTC_CD = 1 AND tt.PROD_TYPE_CD = 'EQUIP')            
      OR ( tt.DMSTC_CD       = 0                
      AND tt.PROD_TYPE_CD      IN ('EQUIP', 'IP')  --IN CPE or Dedicated IP        
      --AND ISNULL(tt.CPE_CPE_ORDR_TYPE_CD, '') != '')            //commented out, need to check
      ))            
 -----------------------------------------------------------------------            
    -- Table 03 - MNS Info FTN Table.            
    -----------------------------------------------------------------------        
    SELECT DISTINCT @Tab_Seq_Nbr            AS TAB_SEQ_NBR,      
     0            AS FSA_MDS_EVENT_ID,            
     tt.M5_ORDR_ID           AS MDS_MNS_FTN,      
     ISNULL(ls.MDS_SRVC_TIER_DES, ' ')  AS MDS_SRVC_TIER_DES,            
     ISNULL(ls.MDS_SRVC_TIER_ID, 0)   AS MDS_SRVC_TIER_ID,            
	 ' '   AS MDS_ENTLMNT_DES,            
	 0   AS MDS_ENTLMNT_ID,            
	 ' '  AS MDS_ENTLMNT_OE_ID,            
     0          AS MDSMNSInfoID,            
     ISNULL(tt.M5_ORDR_ID, 0)     AS ORDR_ID            
  FROM  #M5MDSTable   tt LEFT JOIN       
     dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ftn = tt.M5_ORDR_ID LEFT JOIN       
     dbo.LK_MDS_SRVC_TIER ls WITH (NOLOCK) ON RTRIM(LTRIM(fo.INSTL_SRVC_TIER_CD)) = ls.MDS_SRVC_TIER_ID       
  WHERE  tt.PROD_TYPE_CD IN ('MNS', 'MSS')   -- IN MNS or MSS         
      AND  tt.ORDR_TYPE_CD IN ('IN')      
       
 -----------------------------------------------------------------------            
    -- Table 04 - Wired Device Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS WdID,            
     ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '') AS WdTransportType,             
     ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0)  AS MDS_TRNSPRT_TYPE_ID,            
     '0'          AS PRIM_BKUP_CD,      
           ''          AS WdPrimaryBackup,      
           ''          AS BDWL_CHNL_NME,                  
     ''          AS FMS_CKT_ID,            
     ''          AS PL_NBR,            
     ISNULL(fo.TTRPT_NW_ADR, '')       AS NUA_449_ADR,            
     ''          AS OLD_CKT_ID,            
     0          AS MULTI_LINK_CKT,            
     --''          AS WdMultilinkCircuit,      
     ''          AS WdSprintManaged,      
     '0'          AS SPRINT_MNGD_CD,      
     0          AS TELCO_ID,      
     ' '          AS WdTelcoCallout,      
     ''          AS FOC_DT,      
     0          AS GRPWdID,      
     ' '          AS NEW_FMS_CKT_ID,      
     ' '          AS NEW_NUA      
   FROM  #M5MDSTable AS tt LEFT JOIN       
    dbo.FSA_ORDR AS fo WITH (NOLOCK) ON tt.M5_ORDR_ID = fo.ftn LEFT JOIN       
    dbo.LK_MDS_TRNSPRT_TYPE AS lm WITH (NOLOCK) ON fo.INSTL_TRNSPRT_TYPE_CD = lm.MDS_TRNSPRT_CD            
   WHERE  tt.ORDR_TYPE_CD IN  ('IN')            
    AND  ( (tt.DMSTC_CD = 1 AND tt.PROD_TYPE_CD NOT IN ('MNS', 'EQUIP', 'MSS')) -- NOT IN MNS, MSS, CPE           
      OR ( tt.DMSTC_CD       = 0                
   AND tt.PROD_TYPE_CD      IN ('EQUIP', 'IP')  -- IN CPE or Dedicated IP            
      --AND ISNULL(fo.TTRPT_ACCS_TYPE_CD, '') != '' //need to check
	  ))       
    AND ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0) <> 0      
       
 -----------------------------------------------------------------------            
    -- Table 05 - SPA-E Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS SpID,            
     ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '') AS WdTransportType,             
     ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0)  AS MDS_TRNSPRT_TYPE_ID,            
     '0'          AS PRIM_BKUP_CD,      
           ''          AS WdPrimaryBackup,      
           ''          AS BDWL_CHNL_NME,                  
     ''          AS FMS_CKT_ID,            
     ''          AS ECCKT_ID,            
     ISNULL(fo.TTRPT_NW_ADR, '')       AS UNI_NUA_ADDR,            
     ''          AS OLD_CKT_ID,            
     --0          AS MULTI_LINK_CKT_CD,            
     --''          AS SpMultilinkCircuit,      
     ''          AS WdSprintManaged,      
     '0'          AS SPRINT_MNGD_CD,      
     0          AS TELCO_ID,      
     ' '          AS WdTelcoCallout,      
     ''          AS FOC_DT,      
     0          AS GrpWdID,      
     0          AS SpaEID      
   FROM  #M5MDSTable AS tt LEFT JOIN       
    dbo.FSA_ORDR AS fo WITH (NOLOCK) ON tt.M5_ORDR_ID = fo.ftn LEFT JOIN            
    dbo.ORDR AS od WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID LEFT JOIN       
    dbo.LK_MDS_TRNSPRT_TYPE AS lm WITH (NOLOCK) ON fo.INSTL_TRNSPRT_TYPE_CD = lm.MDS_TRNSPRT_CD            
   WHERE  tt.ORDR_TYPE_CD IN  ('IN')            
    AND  ( (tt.DMSTC_CD = 1 AND tt.PROD_TYPE_CD NOT IN ('MNS', 'EQUIP', 'MSS')) -- NOT IN MNS, MSS, CPE           
      OR ( tt.DMSTC_CD       = 0                
   AND tt.PROD_TYPE_CD      IN ('EQUIP', 'IP')  -- IN CPE or Dedicated IP            
      --AND ISNULL(fo.TTRPT_ACCS_TYPE_CD, '') != '' //need to check
	  ))                 
            
 -----------------------------------------------------------------------            
    -- Table 06 - Sprint Link & ATM Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS SLID,            
     ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '') AS WdTransportType,             
     ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0)  AS MDS_TRNSPRT_TYPE_ID,            
     '0'          AS PRIM_BKUP_CD,      
           ''          AS WdPrimaryBackup,      
           ''          AS BDWL_CHNL_NME,                  
     ''          AS FMS_CKT_ID,            
     ''          AS PL_NBR,            
     ISNULL(fo.TTRPT_NW_ADR, '')       AS NUA_449_ADR,            
     ''          AS OLD_CKT_ID,            
     ''          AS WdSprintManaged,      
     '0'          AS SPRINT_MNGD_CD,      
     0          AS TELCO_ID,      
     ' '          AS WdTelcoCallout,      
     ''          AS FOC_DT,      
     0          AS GrpWdID,      
     0          AS WdID      
   FROM  #M5MDSTable AS tt LEFT JOIN       
    dbo.FSA_ORDR AS fo WITH (NOLOCK) ON tt.M5_ORDR_ID = fo.ftn LEFT JOIN       
    dbo.LK_MDS_TRNSPRT_TYPE AS lm WITH (NOLOCK) ON fo.INSTL_TRNSPRT_TYPE_CD = lm.MDS_TRNSPRT_CD            
   WHERE  tt.ORDR_TYPE_CD IN  ('IN')            
    AND  ( (tt.DMSTC_CD = 1 AND tt.PROD_TYPE_CD NOT IN ('MNS', 'EQUIP', 'MSS')) -- NOT IN MNS, MSS, CPE           
      OR ( tt.DMSTC_CD       = 0                
   AND tt.PROD_TYPE_CD      IN ('EQUIP', 'IP')  -- IN CPE or Dedicated IP            
      --AND ISNULL(fo.TTRPT_ACCS_TYPE_CD, '') != '' //need to check
	  ))        
            
 -----------------------------------------------------------------------            
    -- Table 07 - Wireless Transport Data.       
    -----------------------------------------------------------------------            
    SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS WlID,            
     ''         AS WlPrimaryBackup,            
     ISNULL(md.PRIM_BKUP_CD, '')    AS PRIM_BKUP_CD,            
     ISNULL(md.ESN_MAC_ID, '')      AS ESN            
  FROM  dbo.MDS_EVENT_WRLS_TRPT md WITH (NOLOCK)             
  WHERE  1 != 1            
                
    -----------------------------------------------------------------------            
    -- Table 08 - Virtual Connection PVC Data.      
    -----------------------------------------------------------------------            
    SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS VirtualConnVCID,            
     ' '          AS FMS_NME,      
     ISNULL(DLCI_VPI_CD, '')     AS DLCI_VPI_CD            
      FROM  dbo.MDS_EVENT_VRTL_CNCTN WITH (NOLOCK)             
   WHERE  1 != 1                           
                
    -----------------------------------------------------------------------            
    -- Table 09 - Get the remaining data for a given MNS Event Data.            
    -----------------------------------------------------------------------            
    IF EXISTS(SELECT 'X'       
     FROM    #M5CUSTTable     
    WHERE  ROLE_CD = 'SVCA')            
  BEGIN                  
   SELECT DISTINCT TOP 1-- CPE Section              
      0     AS WIRED_DEV_TRPT_REQR_CD      
     ,0     AS SPAE_TRNSPRT_REQR_CD      
	 ,0     AS DSL_TRNSPRT_REQR_CD      
     ,0     AS SPRF_DEV_TRNSPRT_REQR_CD      
     ,0     AS WRLS_TRNSPRT_REQR_CD      
     ,0     AS VRTL_CNCTN_CD          
     ,0     AS CMPLTD_CD              
     ,CUST_NME AS  INSTL_SITE_POC_NME      
     ,ISNULL(INTL_CNTRY_DIAL_CD,'') AS  INSTL_SITE_POC_INTL_PHN_CD      
     ,CASE ISNULL(DOM_INTL_IND_CD, '')              
       WHEN 'D' THEN ISNULL(DOM_PHN_NBR, '')
      ELSE ISNULL(INTL_PHN_NBR, '')
      END AS  INSTL_SITE_POC_PHN_NBR               
     ,''     AS  INSTL_SITE_POC_INTL_CELL_PHN_CD      
     ,''           AS  INSTL_SITE_POC_CELL_PHN_NBR              
     ,CUST_NME  AS  SRVC_ASSRN_POC_NME          
     ,ISNULL(INTL_CNTRY_DIAL_CD,'') AS  SRVC_ASSRN_POC_INTL_PHN_CD
     , CASE ISNULL(DOM_INTL_IND_CD, '')              
       WHEN 'D' THEN ISNULL(DOM_PHN_NBR, '')
      ELSE ISNULL(INTL_PHN_NBR, '')
      END AS  SRVC_ASSRN_POC_PHN_NBR      
     ,''      AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD      
     ,''         AS  SRVC_ASSRN_POC_CELL_NBR 
	 , CASE WHEN (ISNULL(AVLBLTY_HR_STRT, '') <> '') THEN AVLBLTY_HR_STRT + ' - ' + AVLBLTY_HR_END ELSE '' END  AS SA_POC_HR_NME  
	 ,ISNULL(TME_ZN_CD, '')			AS TME_ZONE_ID
     ,@Tab_Seq_Nbr        AS TAB_SEQ_NBR              
   FROM  #M5CUSTTable            
   WHERE  ROLE_CD = 'SVCA'            
 END             
    ELSE IF EXISTS(SELECT 'X'       
     FROM    #M5CUSTTable     
    WHERE  ROLE_CD = 'CONT')            
  BEGIN                  
   SELECT DISTINCT TOP 1 -- CPE Section              
      0     AS WIRED_DEV_TRPT_REQR_CD      
     ,0     AS SPAE_TRNSPRT_REQR_CD      
	 ,0     AS DSL_TRNSPRT_REQR_CD      
     ,0     AS SPRF_DEV_TRNSPRT_REQR_CD      
     ,0     AS WRLS_TRNSPRT_REQR_CD      
     ,0     AS VRTL_CNCTN_CD          
     ,0     AS CMPLTD_CD              
     ,CUST_NME AS  INSTL_SITE_POC_NME      
     ,ISNULL(INTL_CNTRY_DIAL_CD,'') AS  INSTL_SITE_POC_INTL_PHN_CD      
     ,CASE ISNULL(DOM_INTL_IND_CD, '')              
       WHEN 'D' THEN ISNULL(DOM_PHN_NBR, '')
      ELSE ISNULL(INTL_PHN_NBR, '')
      END AS  INSTL_SITE_POC_PHN_NBR               
     ,''     AS  INSTL_SITE_POC_INTL_CELL_PHN_CD      
     ,''           AS  INSTL_SITE_POC_CELL_PHN_NBR              
     ,CUST_NME  AS  SRVC_ASSRN_POC_NME          
     ,ISNULL(INTL_CNTRY_DIAL_CD,'') AS  SRVC_ASSRN_POC_INTL_PHN_CD
     , CASE ISNULL(DOM_INTL_IND_CD, '')              
       WHEN 'D' THEN ISNULL(DOM_PHN_NBR, '')
      ELSE ISNULL(INTL_PHN_NBR, '')
      END AS  SRVC_ASSRN_POC_PHN_NBR      
     ,''      AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD      
     ,''         AS  SRVC_ASSRN_POC_CELL_NBR 
	 , CASE WHEN (ISNULL(AVLBLTY_HR_STRT, '') <> '') THEN AVLBLTY_HR_STRT + ' - ' + AVLBLTY_HR_END ELSE '' END  AS SA_POC_HR_NME  
	 ,ISNULL(TME_ZN_CD, '')			AS TME_ZONE_ID
     ,@Tab_Seq_Nbr        AS TAB_SEQ_NBR              
   FROM  #M5CUSTTable            
   WHERE  ROLE_CD = 'CONT'
   END            
   
     -----------------------------------------------------------------------            
     -- Table 09 - Get the Order IDs for a given MNS Event Data.            
     -----------------------------------------------------------------------            
     SELECT DISTINCT @Tab_Seq_Nbr      AS TAB_SEQ_NBR            
             ,fo.ORDR_ID            
     FROM  #M5MDSTable   vt            
     LEFT JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON vt.M5_ORDR_ID = fo.ftn            
                 
     -----------------------------------------------------------------------            
     -- Table 10 - Get the Address elements for the order .            
     -----------------------------------------------------------------------            
            
     SELECT DISTINCT TOP 1           
        -- Site Location Info            
        ISNULL(LINE_1, '') + ' ' +            
         ISNULL(LINE_2, '') + ' ' +            
         ISNULL(LINE_3, '') AS  SITE_ADR            
        ,ISNULL(FLOOR_ID, '') +' '+ISNULL(BLDG_ID, '') + ' '+ISNULL(ROOM_ID, '')  FLR_BLDG_NME            
        ,ISNULL(CTY_NME, '')   AS  CTY_NME            
        ,ISNULL(ST_PRVN_CD, '')  AS  STT_PRVN_NME            
        ,ISNULL(lc.CTRY_NME, '')        AS  CTRY_RGN_NME            
        ,ISNULL(vt.CTRY_CD, '')         AS  CTRY_CD            
        ,ISNULL(PSTL_CD, '')  AS  ZIP_CD            
        ,CASE vt.DMSTC_CD            
         WHEN NULL THEN 'D'            
         WHEN 1 THEN 'D'            
         ELSE 'I'            
        END              AS US_INTL_ID            
     FROM #M5ADRTable    vt   
	        LEFT JOIN dbo.LK_CTRY   lc WITH (NOLOCK) ON vt.CTRY_CD = lc.CTRY_CD                                 
      WHERE vt.CIS_HIER_LVL_CD IN ('H5', 'H6')

            
      ----------------------------------------------------------------------            
     -- Table 12 - Get the VLAN data            
     -----------------------------------------------------------------------                   
           
       SELECT DISTINCT @Tab_Seq_Nbr  AS TAB_SEQ_NBR      
     ,0     AS FSA_MDS_EVENT_ID      
     ,0     AS VLANID      
     ,' '    AS SPA_NUA      
     ,' '    AS VLAN      
      FROM  dbo.MDS_EVENT_VLAN WITH (NOLOCK)       
      WHERE 1 != 1      
	  
	  
	  ----------------------------------------------------------------------            
    -- Table 13 - DSL Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS SpID,                     
     '0'          AS PRIM_BKUP_CD,      
           ''          AS PrimaryBackup,      
           ''          AS IP_ADDR,                  
     ''          AS SUBNET_MASK,            
     ''          AS NHOP_GWY,            
     ''       AS PrvdrCKTID,             
     ''          AS SprintManaged,      
     '0'          AS SPRINT_MNGD_CD,
	 ''          AS SprintCustomer,      
     '0'          AS SPRINT_CUST_CD,      
     0          AS TELCO_ID,      
     ' '          AS TelcoCallout,           
     0          AS GrpWdID,      
     0          AS DSLID      
   FROM  #M5MDSTable AS tt LEFT JOIN       
    dbo.FSA_ORDR AS fo WITH (NOLOCK) ON tt.M5_ORDR_ID = fo.ftn
	
	------------------------------------------------------------------------------------
	-- MACH5 Service Table
	------------------------------------------------------------------------------------
	SELECT DISTINCT @Tab_Seq_Nbr    AS TAB_SEQ_NBR      
     ,0    AS FSA_MDS_EVENT_ID      
	 ,0    AS MACH5_H6_SRVC_ID
     ,0    AS SRVC_TYPE_ID          
     ,''  AS SrvcType   
	 ,0    AS THRD_PARTY_VNDR_ID          
     ,''  AS ThirdPartyVndr     
	 ,''    AS ThirdParty
     ,0       AS THRD_PARTY_SRVC_LVL_ID
	 ,''  AS ThirdPartySrvcLvl
     ,'' AS ACTV_DT
	 ,'' AS CMNT_TXT
	 ,'FALSE' AS EMAIL_CD
     ,0           AS SrvcTblID      
   FROM  #M5MDSTable
   WHERE 1 != 1  
   
             
   END            
 END            
END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END 