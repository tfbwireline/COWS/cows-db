USE [COWS]
GO
_CreateObject 'SP','dbo','updateODIERequest'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 08/02/2011
-- Description:	Update the status of a given ODIE Request
-- =========================================================
ALTER PROCEDURE [dbo].[updateODIERequest]
	@REQ_ID		Int
	,@STUS_ID	Int
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	IF	@STUS_ID	=	11
		BEGIN
			UPDATE		dbo.ODIE_RSPN	WITH (ROWLOCK)
				SET		ACT_CD		=	0
				WHERE	REQ_ID		=	@REQ_ID
			
			UPDATE		dbo.ODIE_DISC_REQ	WITH (ROWLOCK)
				SET		STUS_ID		=	0
				WHERE	REQ_ID		=	@REQ_ID
				
			UPDATE edc
			SET ODIE_SENT_DT = GETDATE()
			FROM dbo.EVENT_DEV_CMPLT edc	WITH (ROWLOCK) INNER JOIN
				 dbo.UCaaS_EVENT uc WITH (NOLOCK) ON uc.EVENT_ID = edc.EVENT_ID INNER JOIN
				 dbo.EVENT_CPE_DEV ecd WITH (NOLOCK) ON ecd.EVENT_ID = uc.EVENT_ID AND edc.ODIE_DEV_NME = ecd.ODIE_DEV_NME INNER JOIN
				 dbo.ODIE_REQ od WITH (NOLOCK) ON od.MDS_EVENT_ID = edc.EVENT_ID
			WHERE edc.CMPLTD_CD = 1
			  AND od.STUS_ID !=	21
			  AND ecd.ODIE_CD IS NOT NULL
			  AND ecd.ODIE_CD = 0
			  AND edc.ODIE_SENT_DT IS NULL
			  AND od.REQ_ID=@REQ_ID

			UPDATE edc
			SET ODIE_SENT_DT = GETDATE()
			FROM dbo.EVENT_DEV_CMPLT edc	WITH (ROWLOCK) INNER JOIN
				 dbo.UCaaS_EVENT uc WITH (NOLOCK) ON uc.EVENT_ID = edc.EVENT_ID INNER JOIN
				 dbo.EVENT_DEV_SRVC_MGMT eds WITH (NOLOCK) ON eds.EVENT_ID = uc.EVENT_ID AND eds.ODIE_DEV_NME = edc.ODIE_DEV_NME INNER JOIN
				 dbo.ODIE_REQ od WITH (NOLOCK) ON od.MDS_EVENT_ID = edc.EVENT_ID
			WHERE edc.CMPLTD_CD = 1
			  AND od.STUS_ID !=	21
			  AND eds.ODIE_CD IS NOT NULL
			  AND eds.ODIE_CD = 0
			  AND edc.ODIE_SENT_DT IS NULL
			  AND od.REQ_ID=@REQ_ID

			UPDATE edc
			SET ODIE_SENT_DT = GETDATE()
			FROM dbo.EVENT_DEV_CMPLT edc	WITH (ROWLOCK) INNER JOIN
				 dbo.UCaaS_EVENT uc WITH (NOLOCK) ON uc.EVENT_ID = edc.EVENT_ID INNER JOIN
				 dbo.ODIE_REQ od WITH (NOLOCK) ON od.MDS_EVENT_ID = edc.EVENT_ID
			WHERE edc.CMPLTD_CD = 1
			  AND od.STUS_ID !=	21
			  AND edc.ODIE_SENT_DT IS NULL
			  AND NOT EXISTS (SELECT 'x'
							  FROM dbo.EVENT_CPE_DEV ecd WITH (NOLOCK)
							  WHERE ecd.EVENT_ID=edc.EVENT_ID AND ecd.ODIE_DEV_NME = edc.ODIE_DEV_NME)
			  AND NOT EXISTS (SELECT 'x'
							  FROM dbo.EVENT_DEV_SRVC_MGMT eds WITH (NOLOCK)
							  WHERE eds.EVENT_ID=edc.EVENT_ID AND eds.ODIE_DEV_NME = edc.ODIE_DEV_NME)
			  AND od.REQ_ID=@REQ_ID

			UPDATE edc
			SET ODIE_SENT_DT = GETDATE()
			FROM dbo.EVENT_DEV_CMPLT edc	WITH (ROWLOCK) INNER JOIN
				 dbo.MDS_EVENT uc WITH (NOLOCK) ON uc.EVENT_ID = edc.EVENT_ID INNER JOIN
				 dbo.ODIE_REQ od WITH (NOLOCK) ON od.MDS_EVENT_ID = edc.EVENT_ID
			WHERE edc.CMPLTD_CD = 1
			  AND od.STUS_ID !=	21
			  AND edc.ODIE_SENT_DT IS NULL
			  AND od.REQ_ID=@REQ_ID
		END

	
	UPDATE		dbo.ODIE_REQ	WITH (ROWLOCK)
		SET		STUS_ID		=	@STUS_ID
				,STUS_MOD_DT=	GETDATE()
		WHERE	REQ_ID		=	@REQ_ID
			AND	STUS_ID		!=	21
	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END