USE [COWS]
GO

/****** Object:  StoredProcedure [dbo].[sp_testlinkedserver_NRM]    Script Date: 02/20/2019 00:54:44 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_testlinkedserver_IDMData]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_testlinkedserver_IDMData]
GO

CREATE PROCEDURE [dbo].[sp_testlinkedserver_IDMData]
AS
 BEGIN TRY
 EXEC sp_testlinkedserver  @server=N'ADAM'
 SELECT 'Active'
 END TRY
 
 BEGIN CATCH
 SELECT error_message()
 END CATCH
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_testlinkedserver_PPLSOFT]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_testlinkedserver_PPLSOFT]
GO

CREATE PROCEDURE [dbo].[sp_testlinkedserver_PPLSOFT]
AS
 BEGIN TRY
 EXEC sp_testlinkedserver  @server=N'PFNP1011'
 SELECT 'Active'
 END TRY
 
 BEGIN CATCH
 SELECT error_message()
 END CATCH

