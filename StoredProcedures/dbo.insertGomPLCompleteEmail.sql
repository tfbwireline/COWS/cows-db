USE [COWS]
GO
_CreateObject 'SP','dbo','insertGomPLCompleteEmail'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David Phillips
-- Create date: 02/28/2012
-- Description:	This SP is used to insert GOM PL Completion notices into the EMAIL REQ table 
--               for sending out of EmailSender service.
-- =============================================

ALTER  PROCEDURE [dbo].[insertGomPLCompleteEmail] 
	@OrderID INT, 
	@TaskId SMALLINT, 
	@TaskStatus TINYINT, 
	@Comments VARCHAR(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET DEADLOCK_PRIORITY 3

	BEGIN TRY

	IF ((SELECT COUNT(1) FROM dbo.ORDR WITH (NOLOCK) WHERE ORDR_ID = @OrderID AND ORDR_CAT_ID = 2) > 0)
		BEGIN
			IF NOT EXISTS
				(
					SELECT 'X'
						FROM	dbo.FSA_ORDR WITH (NOLOCK)
						WHERE	ORDR_ID = @OrderID	
							AND (
									(ISNULL(TTRPT_ACCS_TYPE_DES,'') = 'Ethernet') --Do not send PL email as it is blank.
									OR 
									(ISNULL(ORDR_TYPE_CD,'') = 'DC')
								)
				)
				BEGIN
					DECLARE @EMAIL_LIST_TXT VARCHAR(1000)= 'ITP-IPLNotification@sprint.com'		
					DECLARE @EMAIL_SUBJ_TXT	VARCHAR(1000) = 'GOM PL Completion Notice'
					DECLARE @EMAIL_BODY_TXT VARCHAR(1000)
			
					INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
							(ORDR_ID
							,EMAIL_REQ_TYPE_ID
							,STUS_ID
							,EMAIL_LIST_TXT
							,EMAIL_CC_TXT
							,EMAIL_SUBJ_TXT
							,EMAIL_BODY_TXT)
					SELECT	@OrderID
						,9 -- GOM PL Complete Req Type ID is 9
						,10 --  Status 10 is ready to send Email
						,@EMAIL_LIST_TXT
						,ISNULL(lu.EMAIL_ADR,'') AS EMAIL_CC_TXT
						,@EMAIL_SUBJ_TXT
						,@EMAIL_BODY_TXT
					FROM	dbo.USER_WFM_ASMT uwa WITH (NOLOCK)
				INNER JOIN	dbo.LK_USER	lu WITH (NOLOCK) ON lu.USER_ID	=	uwa.ASN_USER_ID			
					WHERE	ORDR_ID = @OrderID
						AND	((GRP_ID	=	1) OR (USR_PRF_ID=126))
				END
		END
	END TRY

	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	SET NOCOUNT OFF
END

