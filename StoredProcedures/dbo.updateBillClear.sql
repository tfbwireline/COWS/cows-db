USE [COWS]
GO
_CreateObject 'SP','dbo','updateBillClear'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		csb9923
-- Create date: 07/19/2011
-- Description:	Get the details for all ODIES requests
-- =========================================================
ALTER PROCEDURE [dbo].[updateBillClear]
AS
BEGIN
SET NOCOUNT ON;
Begin Try
		
		DECLARE @ORDID INT
		DECLARE @BILLCLDATE DATETIME
		DECLARE @FTN Varchar(20)
		DECLARE @xNCIBillClearTaskID INT
		DECLARE @FSA_CMMT_DT DATETIME
		DECLARE @ORDR_CMMT_DT DATETIME
		DECLARE @DATE DATETIME = GETDATE()
		DECLARE @ORDR_SUB_TYPE VARCHAR (7)
		
		SET @xNCIBillClearTaskID=207
			
		DECLARE @BillClear TABLE
			(ORDR_ID INT,
			BILLCLR_DT DATETIME,
			FTN Varchar(20),
			Flag INT default 0
			)
			
			INSERT INTO @BillClear	
			SELECT fo.ORDR_ID,ns.BILL_CLEAR_DT,fo.FTN,0 
			FROM	dbo.FSA_ORDR		fo WITH (NOLOCK)  
		INNER JOIN	dbo.ACT_TASK		at WITH (NOLOCK)	ON	fo.ORDR_ID	=	at.ORDR_ID
		INNER JOIN	dbo.NRM_SRVC_INSTC	ns WITH (NOLOCK)	ON	fo.FTN		=	ns.FTN
		INNER JOIN	dbo.ORDR			o  WITH (NOLOCK)	ON	o.ORDR_ID	=	fo.ORDR_ID
			WHERE	at.TASK_ID	=	@xNCIBillClearTaskID
				AND at.STUS_ID	=	0
				AND ISNULL(ns.BILL_CLEAR_DT,'')	<>	''
				AND (
						(o.DMSTC_CD = 0)
						OR
						(o.DMSTC_CD = 1 AND dbo.VerifyCircuitMilestones(fo.ORDR_ID) = 'Y')
						OR 
						(fo.ORDR_SUB_TYPE_CD = 'MPLSVAS')
						OR EXISTS
							(
								SELECT 'X'
									FROM	dbo.ORDR_ADR	oa WITH (NOLOCK) 
									WHERE	oa.ORDR_ID			=	fo.ORDR_ID
										AND	oa.CIS_LVL_TYPE		IN	('H5','H6')
										AND	fo.PROD_TYPE_CD	IN	('MP','DN')
										AND	ISNULL(oa.CTRY_CD,'')	IN	('GU','MP','PR','VI','US')
							)
					)
					
			
			WHILE EXISTS (SELECT 'X' FROM @BillClear WHERE Flag = 0)
			BEGIN
				SELECT TOP 1 @ORDID = ORDR_ID,@BILLCLDATE=BILLCLR_DT,@FTN=FTN FROM @BillClear WHERE Flag = 0
				
				--update bill clear date in order milestones table
				UPDATE ORDR_MS WITH (ROWLOCK)
				SET ORDR_BILL_CLEAR_INSTL_DT=@BILLCLDATE
				WHERE ORDR_ID=@ORDID
				
				SELECT @ORDR_SUB_TYPE = ORDR_SUB_TYPE_CD FROM FSA_ORDR WHERE ORDR_ID = @ORDID
				SELECT @FSA_CMMT_DT = CUST_CMMT_DT FROM FSA_ORDR WHERE ORDR_ID = @ORDID
				SELECT @ORDR_CMMT_DT = CUST_CMMT_DT FROM ORDR WHERE ORDR_ID = @ORDID
				
				IF @ORDR_SUB_TYPE = 'MPLSVAS' AND Convert(Varchar (10),ISNULL(@ORDR_CMMT_DT,@FSA_CMMT_DT),121) < CONVERT(VARCHAR(10),@DATE, 121)
					BEGIN	
						EXEC [dbo].[insertCCDChange] @FTN,@DATE,10,'Pass thru order, bill clear after CCD date.  COWS is systematically updating CCD to Bill Clear Date.', 1,null
					 END
				
				--complete xNCI Bill Clear/Install Date task
				
				EXEC [dbo].[CompleteActiveTask] @ORDID,@xNCIBillClearTaskID,2,'Order moved forward as bill clear date is received from NRM',1,null
				
				UPDATE @BillClear SET Flag = 1 WHERE ORDR_ID = @ORDID
			END
			
			
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

