USE COWS
GO
_CreateObject 'SP','dbo','insertNextWGProfile'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Created By:	Shabu Karunakaran
-- Modified By: Ramesh Ragi
-- CREATE date:	07/27/2011
-- Description:	insert Next Profile
-- ===========================================================================
       
ALTER PROCEDURE [dbo].[insertNextWGProfile]      
	@OrderID		INT,      
	@PatternIDs		VARCHAR(MAX)	
AS      
BEGIN
	DECLARE @NewProfileIDs		VARCHAR(MAX)
	DECLARE @ixml INT
	
	DECLARE @OrderData		TABLE (OrderID  INT, WGPatternID SMALLINT, WGProfileID SMALLINT)	
	DECLARE	@NewProfiles	TABLE (OrderID  INT, WGPatternID SMALLINT, WGProfileID SMALLINT)
	
	SET @NewProfileIDs		=	''
	
	BEGIN TRY
	
		EXEC sp_xml_preparedocument @ixml OUTPUT, @PatternIDs
		
		INSERT INTO 
			@OrderData 	
		SELECT OrderID, WGPatternID, WGProfileID FROM
		OPENXML (@ixml, '/Patterns/IDs', 2)
			WITH (OrderID INT, WGPatternID SMALLINT, WGProfileID SMALLINT)	 
			
		EXEC sp_xml_removedocument	@ixml
		
		--If profile = 0. Insert statting profile for pattern
		IF EXISTS (SELECT 'X' FROM @OrderData WHERE WGProfileID	=	0)
			BEGIN
				INSERT INTO 
					@NewProfiles				
				SELECT
					o.OrderID,
					o.WGPatternID,						
					wpt.DESRD_WG_PROF_ID
				FROM
					@OrderData	o				
				INNER JOIN	dbo.WG_PTRN_STUS	wps	WITH (NOLOCK) ON	wps.ORDR_ID		=	o.OrderID	
																	AND	wps.WG_PTRN_ID	=	o.WGPatternID	
				INNER JOIN	dbo.WG_PTRN_SM		wpt WITH (NOLOCK) ON	wpt.PRE_REQST_WG_PROF_ID	=	o.WGProfileID 
																	AND	wpt.WG_PTRN_ID				=	o.WGPatternID	
				WHERE
					o.WGProfileID		=	0				AND
					wps.STUS_ID	=	0
			END

		--SELECT * FROM @TNDATA
						
		--Fetching the Profile from profile list
		INSERT INTO 
			@NewProfiles	
		SELECT 			
			o.OrderID,	
			o.WGPatternID,					
			o.WGProfileID			
		FROM	
			@OrderData		o
		INNER JOIN	dbo.WG_PTRN_STUS	wps	WITH (NOLOCK) ON	wps.ORDR_ID		=	o.OrderID	
															AND	wps.WG_PTRN_ID	=	o.WGPatternID	
		INNER JOIN	dbo.WG_PTRN_SM		wpt WITH (NOLOCK) ON	wpt.PRE_REQST_WG_PROF_ID	=	o.WGProfileID 
															AND	wpt.WG_PTRN_ID				=	o.WGPatternID	
		WHERE
			wps.STUS_ID	=	0	AND
			o.WGProfileID		<>	0	
		
	
--		SELECT * FROM @NewProfiles
		
		UPDATE	wps
		SET	
			STUS_ID = 0 
		FROM
			dbo.WG_PROF_STUS	wps WITH (ROWLOCK)
		INNER JOIN	@NewProfiles np	ON	np.OrderID		=	wps.ORDR_ID	
									AND	np.WGPatternID	=	wps.WG_PTRN_ID
									AND np.WGProfileID	=	wps.WG_PROF_ID

		--Insert Next task into dbo.WGProfileStatus
		INSERT INTO 
			dbo.WG_PROF_STUS				
		SELECT DISTINCT
			OrderID,						
			WGProfileID,
			WGPatternID,
			0,
			GETDATE()
		FROM	
			@NewProfiles np
		WHERE
			np.WGProfileID		NOT IN 
									(
										SELECT 
											wps.WG_PROF_ID	
										FROM 
											dbo.WG_PROF_STUS wps WITH (NOLOCK) 
										WHERE 
											np.OrderID		=	wps.ORDR_ID	AND
											np.WGPatternID	=	wps.WG_PTRN_ID
									)		
	
		SET  @NewProfileIDs = (SELECT DISTINCT OrderID, WGProfileID, 0 AS TaskID From @NewProfiles FOR XML PATH('IDs'), ROOT('Profiles'))
		
		EXEC dbo.insertNextActiveTask @OrderID, @NewProfileIDs
		
    END TRY  

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]      
	END CATCH    
	
END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
