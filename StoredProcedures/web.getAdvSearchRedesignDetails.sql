USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getAdvSearchRedesignDetails]    Script Date: 1/18/2022 2:31:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =======================================================
-- Created By:	Ramesh Ragi	
-- Create date:	07/29/2015
-- Description:	Advanced Search
-- Jbolano15 - 11/24/2021 - use dbo.getRedesignRoleUserAssigned for Assigned NTE/PM
-- =======================================================
--Exec [web].[getAdvSearchRedesignDetails] 'R%','Estee Lauder DSL VPN'
--Exec [web].[getAdvSearchRedesignDetails] 'R%',''
ALTER PROCEDURE [web].[getAdvSearchRedesignDetails]
	@RedesignNumbers	Varchar(1000)	= '',
	@CustomerNames		Varchar(1000)	= '',
	@NTE				Varchar(1000)	= '',
	@ODIEDevices		Varchar(1000)	= '',
	@PM                 Varchar (1000)  = '',
	@CSGLvlId		TINYINT			= 0
	
AS
BEGIN
Begin Try
DECLARE @OrdrBy VARCHAR(50)
DECLARE @DropTbl NVARCHAR(400)				
DECLARE @CrtTbl NVARCHAR(4000)
DECLARE @InsrtTbl VARCHAR(100)
DECLARE @FinalSel NVARCHAR(4000)
			    
SET NOCOUNT ON;  
	SET @OrdrBy = ' ORDER BY r.[REDSGN_ID] DESC '
	SET @DropTbl = ' IF OBJECT_ID(N''tempdb..#RedesignResults'', N''U'') IS NOT NULL         
				DROP TABLE #RedesignResults '
	SET @CrtTbl = 	' CREATE TABLE #RedesignResults 
		(REDSGN_ID VARCHAR(100), 
		Submitted_Date DateTime, 
		Redesign_Number VARCHAR(100), 
		BPM_Redesign_Number VARCHAR(50),
		H1 VARCHAR(9), 
		Assigned_NTE VARCHAR(100), 
		Assigned_PM VARCHAR(100), 
		Redesign_Type Varchar(100), 
		Device_Name Varchar(1000), 
		Redesign_Status_ID SmallInt,
		Redesign_Status VARCHAR(50),
		SLA_Due_Date DateTime,
		CUST_NME Varchar(200),
		Expiration_Date DateTime,
		CSG_LVL_ID	tinyint)'
	SET @InsrtTbl = ' INSERT INTO #RedesignResults '
	SET @FinalSel = ' SELECT REDSGN_ID AS  ''Redesign ID'', 
		Submitted_Date AS  ''Submitted Date'', 
		Redesign_Number AS	''Redesign Number'', 
		BPM_Redesign_Number AS	''BPM Redesign Number'', 
		H1 AS ''H1'', 
		Assigned_NTE AS  ''Assigned NTE'', 
		Assigned_PM AS	''Assigned PM'', 
		Redesign_Type AS ''Redesign Type'', 
		Device_Name AS ''Device Name'', 
		Redesign_Status_ID AS ''Redesign Status ID'',
		Redesign_Status AS	''Redesign Status'',
		SLA_Due_Date AS ''SLA Due Date'',
		CASE WHEN (CSG_LVL_ID>0) THEN ''Private Customer'' ELSE CUST_NME END AS	''Customer Name'',
		Expiration_Date AS ''Expiration Date'',
		CSG_LVL_ID as ''CSG Level''
		FROM #RedesignResults '
    SET @RedesignNumbers           = LTRIM(RTRIM(@RedesignNumbers))
    SET @CustomerNames        = LTRIM(RTRIM(@CustomerNames))
    

    -- Baseline '0' & null to '', 
    IF    ISNULL(@RedesignNumbers,'0')		  =     '0'
          SET @RedesignNumbers                =     ''
    IF    ISNULL(@CustomerNames, '0')         =     '0'
          SET @CustomerNames                  =     ''
    
    
  
	 
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
		BEGIN
			DECLARE @IsFilter	Bit
			DECLARE	@Select		nVarchar(Max)
			DECLARE @Filter		nVarchar(Max)
			DECLARE @SQL		nVarchar(Max)
			DECLARE @From	nVarchar(Max)
			DECLARE @Where	nVarchar(Max)
			
			
			SET @IsFilter	=	0		
			SET	@Select		=	''
			SET	@SQL		=	''
			SET	@Filter		=	''
			SET	@From		=	''
			SET @Where		=	''
			
			
			SET	@Select		=	'SELECT DISTINCT TOP 100	r.REDSGN_ID AS ''REDSGN_ID'',
															r.SUBMIT_DT AS  ''Submitted_Date'' ,
															r.REDSGN_NBR AS	''Redesign_Number'',
															r.BPM_REDSGN_NBR AS	''BPM_Redesign_Number'',
															r.H1_CD AS ''H1'',
															--lut.DSPL_NME AS  ''Assigned_NTE'',
															--lup.DSPL_NME AS  ''Assigned_PM'',
															dbo.getRedesignRoleUserAssigned(r.REDSGN_ID, 81) AS  ''Assigned_NTE'',
															dbo.getRedesignRoleUserAssigned(r.REDSGN_ID, 122) AS  ''Assigned_PM'',
															lrt.REDSGN_TYPE_NME AS ''Redesign_Type'', 
															( SELECT top 10 DEV_NME + '','' 
																	FROM dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK) 
																	WHERE rdi.REC_STUS_ID = 1 AND rdi.REDSGN_ID = r.REDSGN_ID FOR XML PATH('''') ) AS ''Device_Name'', 
															r.STUS_ID AS ''Redesign_Status_ID'',
															ls.STUS_DES AS	''Redesign_Status'',
															r.SLA_DT AS ''SLA_Due_Date'',
															CASE WHEN (r.CSG_LVL_ID>0) THEN LEFT(dbo.decryptBinaryData(csd.CUST_NME),200) ELSE r.CUST_NME END AS ''Customer_Name'',
															r.EXPRTN_DT AS ''Expiration_Date'',
															r.CSG_LVL_ID AS ''CSG Level''
															
								'

										
											   
			SET @From		=	'	FROM	 dbo.REDSGN r WITH (NOLOCK)
															Inner Join	dbo.LK_USER lu with (nolock) on lu.USER_ID = r.CRETD_BY_CD
															Inner Join	dbo.LK_STUS ls with (nolock) on ls.STUS_ID = r.STUS_ID
															Inner Join	dbo.LK_REDSGN_TYPE lrt with (nolock) on lrt.REDSGN_TYPE_ID = r.REDSGN_TYPE_ID
															Left Join	dbo.REDSGN_DEVICES_INFO rd with (nolock) on r.REDSGN_ID = rd.REDSGN_ID AND rd.REC_STUS_ID = 1
															LEFT JOIN  dbo.CUST_SCRD_DATA csd WITH (NOLOCK)   ON csd.SCRD_OBJ_ID=r.REDSGN_ID AND csd.SCRD_OBJ_TYPE_ID=16
															Left Join	dbo.LK_USER lut with (nolock) on r.NTE_ASSIGNED = lut.USER_ADID 
															Left join	dbo.LK_USER lup with (nolock) on r.PM_ASSIGNED = lup.USER_ADID
									 WHERE		1 = 1	'	
									 
				--select @SQL

			---------------------------------------------------------------
			--	FTN Filter 
			---------------------------------------------------------------
			IF    @RedesignNumbers <> ''
				BEGIN
				  SET @RedesignNumbers = REPLACE(@RedesignNumbers, '*', '%')
				  SET @RedesignNumbers = REPLACE(@RedesignNumbers, '\r\n', ',')
				  SET @IsFilter = 1 

				  --If there is a comma, do the comma method
				  IF CHARINDEX(',',@RedesignNumbers) > 0 
						BEGIN
							SET @RedesignNumbers = REPLACE(@RedesignNumbers, ',', ''',''')
							SET @Where 	+=	' AND r.REDSGN_NBR IN (''' + @RedesignNumbers + ''') '	
							
						END
				  --Else if we have a wildcard
				  ELSE IF CHARINDEX('%',@RedesignNumbers) > 0
						BEGIN
							SET @Where	+=	' AND r.REDSGN_NBR LIKE (''' + @RedesignNumbers + ''') '
							
						END
						
				  ELSE
					BEGIN
							SET @Where	+=	' AND r.REDSGN_NBR IN (''' + @RedesignNumbers + ''') '
					END
             END
            
			
				
			---------------------------------------------------------------
			--	Customer Name Filter 
			---------------------------------------------------------------
			IF	@CustomerNames	<>	''
				BEGIN
					SET @Where	+=	' AND CHARINDEX(''' + @CustomerNames + ''', CASE WHEN (r.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE r.CUST_NME END) > 0 '
					SET @IsFilter = 1
				END		

            ---------------------------------------------------------------
			--	ODIE Device Filter 
			---------------------------------------------------------------
			IF    @ODIEDevices <> ''
				BEGIN
					SET @Where	+=	' AND rd.DEV_NME LIKE (''%' + @ODIEDevices + '%'') AND rd.REC_STUS_ID = 1 '
					SET @IsFilter = 1
				END

			DECLARE @NTE_PM_FILTER NVARCHAR(MAX) = ''
			---------------------------------------------------------------
			--	NTE Filter 
			---------------------------------------------------------------
				
			IF @NTE	<> ''
				BEGIN
					SET @NTE_PM_FILTER	+=	' AND Assigned_NTE LIKE (''%' + @NTE + '%'') '
					SET @IsFilter = 1
				END

			---------------------------------------------------------------
			--	PM Filter 
			---------------------------------------------------------------	
			IF    @PM <> ''
				BEGIN
					SET @NTE_PM_FILTER	+=	' AND Assigned_PM LIKE (''%' + @PM + '%'')'
					SET @IsFilter = 1
				END

			SET @SQL	=		@DropTbl + 	@CrtTbl	+ @InsrtTbl +	@Select	 + @From	+	@Where	+ @OrdrBy
						+	@FinalSel +  ' WHERE (CSG_LVL_ID = 0 OR ('+CONVERT(CHAR,@CSGLvlId)+'!=0 AND CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+')) ' + @NTE_PM_FILTER + ' ORDER BY REDSGN_ID DESC'
		

            IF @IsFilter = 1
				BEGIN
					--select @SQL
					EXEC sp_executesql @SQL
					
				END
			END

		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

