USE COWS
GO
_CreateObject 'SP','dbo','bypassGOMPLForCancels'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <09/19/2011>
-- Description:	<To Bypass GOM Cancel Ready Task 
--	when Private Line was not keyed before Cancel was received>
-- =============================================
ALTER PROCEDURE dbo.bypassGOMPLForCancels 
@OrderID	Int,
@TaskID		SmallInt,
@TaskStatus	TinyInt,
@Comments	Varchar(1000) = NULL
AS
BEGIN
	BEGIN TRY
DECLARE @ORDR_CAT_ID TinyInt

SELECT	@ORDR_CAT_ID = ORDR_CAT_ID
	FROM	dbo.ORDR WITH (NOLOCK)
	WHERE	ORDR_ID = @OrderID

IF @ORDR_CAT_ID = 2
	BEGIN
		IF ((SELECT COUNT(1) FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @OrderID AND ORDR_TYPE_CD = 'DC') > 0)
			RETURN
	
		IF NOT EXISTS
		(
			SELECT		'X'
				FROM	dbo.ORDR_NTE	orn	WITH (NOLOCK)
				WHERE	orn.ORDR_ID		=	@OrderID
					AND orn.NTE_TXT		=	'NRM Status:Pending OE & PL.Systematically completing NRM: Pending OE & PL task.'
		)
		BEGIN
			Exec dbo.CompleteActiveTask @OrderID,@TaskID,2,'Bypassing GOM Cancel Ready, Private Line was never keyed for this order.',1,NULL
		END
	END
	END TRY
	
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END
GO
