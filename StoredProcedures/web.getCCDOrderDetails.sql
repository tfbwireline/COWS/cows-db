USE [COWS]
GO
_CreateObject 'SP','web','getCCDOrderDetails'
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Cereated By:	Kyle Wichert
-- Create date:	08/24/2011 
-- Description:	Search Orders by FTN/CTN and/or H5 so that a CCD can be initiated.
--				First get all Parent IDs so that we can pull back everything tied together.
--				Second remove any parentID that has an order in pre-submit status
--				Remove any parentID that has an order in bill ready/complete status.
--				Then return all orders for the remaining parentIDs.
-- Alter date:	11/29/2011
--				Need to associate orders related via the Related-FTN field as well.
--				Introducing new column to @ParentIDs table to find the related
--				parent's ID.  Do same checks to clear out the related orders.  Then
--				Insert the related Parent IDs back into the orderID column with 
--				a unique signature so it is clear what the relationships are.
--				relParID = 0 means no relation.  Negative value is the "main" parentID
-- Alter date:	04/03/2012
--				Wishlist/Fallout changes.  Give selected users the ability to bypass 
--				the current logic which makes parts of an order ineligible.
--				Groups of orders will be allowed to change as long as any one
--				element of the group is eligible.  Singlular FTN groupings will
--				be subjected to old logic because there was no requirement given
--				when this hole was discussed.
--				04/25/2012
--				Closing the loophole for singular FTNs to not be bypassed.
-- ============================================================================

ALTER PROCEDURE [web].[getCCDOrderDetails] --'IN3GBB020565',0,0
	@FTN	varchar(20),
	@H5		int,
	@bypass	bit	=	0
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		IF ((ISNULL(@FTN, '') <> '') OR (ISNULL(@H5,-1) <> -1))
		BEGIN
			DECLARE @ParentIDs TABLE 
			(
				orderID		INT,
				relParID	INT
			)
			DECLARE @OrderList TABLE
			(
				ParentOrderID	INT,
				OrderID			INT,	
				FTN				varchar(20),
				DeviceID		varchar(25),
				ActionType		VARCHAR(250),
				ProductType		VARCHAR(100),
				OrderType		VARCHAR(100),
				H5				INT,
				DomesticFlag	VARCHAR(50),
				H1				INT,
				OrderSubType	VARCHAR(7),
				CCD				DateTime,
				Error			VARCHAR(100),
				isLocked		BIT,
				PrimaryParent	INT,
				relParID		INT,
				OrderFTN		Varchar(20),
				OrderRelFTN		Varchar(20)
			)
			
			INSERT INTO @ParentIDs
			SELECT	DISTINCT o.PRNT_ORDR_ID, 0
			FROM			dbo.ORDR		o	WITH (NOLOCK)	
				INNER JOIN	dbo.FSA_ORDR	fo	WITH (NOLOCK)	ON	o.ORDR_ID	=	fo.ORDR_ID
			WHERE	(fo.FTN	= CASE WHEN ISNULL(@FTN,'') = '' THEN fo.FTN ELSE @FTN END)
				AND	(ISNULL(o.H5_H6_CUST_ID, -1) = CASE WHEN ISNULL(@H5, 0) = 0 THEN ISNULL(o.H5_H6_CUST_ID, -1) ELSE ISNULL(@H5, -1) END)
			UNION --IPL && NCCO
			SELECT	DISTINCT o.PRNT_ORDR_ID, 0
			FROM			dbo.ORDR		o	WITH (NOLOCK)	
				LEFT JOIN	dbo.IPL_ORDR	io	WITH (NOLOCK)	ON	o.ORDR_ID	=	io.ORDR_ID
																AND io.ORDR_STUS_ID <> 20
				LEFT JOIN	dbo.NCCO_ORDR	ncco WITH (NOLOCK)  ON o.ORDR_ID	=	ncco.ORDR_ID
			WHERE	(Convert(varchar,o.ORDR_ID)	= CASE WHEN ISNULL(@FTN,'') = '' THEN Convert(varchar,o.ORDR_ID) ELSE @FTN END)
				AND	(ISNULL(o.H5_H6_CUST_ID, -1) = CASE WHEN ISNULL(@H5, 0) = 0 THEN ISNULL(o.H5_H6_CUST_ID, -1) ELSE ISNULL(@H5, -1) END)
				
			
			--Update related ParentID
			UPDATE p
			SET		relParID = ro.PRNT_ORDR_ID
			FROM			@ParentIDs		p
				INNER JOIN	dbo.ORDR		o	WITH (NOLOCK)	ON	p.orderID		=	o.PRNT_ORDR_ID	
				INNER JOIN	dbo.FSA_ORDR	fo	WITH (NOLOCK)	ON	o.ORDR_ID		=	fo.ORDR_ID
				INNER JOIN	dbo.FSA_ORDR	fr	WITH (NOLOCK)	ON	fo.RELTD_FTN	=	fr.FTN
				INNER JOIN	dbo.ORDR		ro  WITH (NOLOCK)	ON	fr.ORDR_ID		=	ro.ORDR_ID
			WHERE	o.H5_H6_CUST_ID = ro.H5_H6_CUST_ID
				AND	ISNULL(o.CUST_CMMT_DT,fo.CUST_CMMT_DT) = ISNULL(ro.CUST_CMMT_DT,fr.CUST_CMMT_DT)


			--Insert related parent ID in with the current set of IDs, with a negative relParID value
			INSERT INTO @ParentIDs
			SELECT	p.relParID, -1 * p.orderID
			FROM	@ParentIDs p
			WHERE	p.relParID	<>	0

			INSERT INTO @OrderList
			--FSA
			SELECT	o.PRNT_ORDR_ID,
					o.ORDR_ID,
					fo.FTN,
					'',
					loa.ORDR_ACTN_DES,
					lpt.PROD_TYPE_DES,
					lot.ORDR_TYPE_DES,
					o.H5_H6_CUST_ID,
					CASE o.DMSTC_CD	WHEN 0 THEN 'Domestic'
									WHEN 1 THEN 'International'  
					END  AS DomesticFlag,
					foc.CUST_ID,
					fo.ORDR_SUB_TYPE_CD,
					ISNULL(o.CUST_CMMT_DT, fo.CUST_CMMT_DT),
					''		AS Error,
					CASE	WHEN ISNULL(ol.ORDR_ID,0) = 0 THEN 0 ELSE 1 END,
					CASE	WHEN pID.relParID < 0	THEN	-1 * pID.relParID
							ELSE pID.orderID
					END													AS	'Primary Parent',
					CASE	WHEN pID.relParID < 0	THEN	-1 * pID.relParID
							ELSE pID.relParID
					END													AS	'Related Parent',
					fo.FTN	AS	OrderFTN,
					ISNULL(fo.RELTD_FTN,'') AS OrderRelFTN
			FROM			@ParentIDs				pID
				INNER JOIN	dbo.ORDR				o	WITH (NOLOCK)	ON	o.PRNT_ORDR_ID	=	pID.orderID
				INNER JOIN	dbo.FSA_ORDR			fo	WITH (NOLOCK)	ON	o.ORDR_ID		=	fo.ORDR_ID
				INNER JOIN	dbo.LK_ORDR_ACTN		loa	WITH (NOLOCK)	ON	fo.ORDR_ACTN_ID	=	loa.ORDR_ACTN_ID
				INNER JOIN	dbo.LK_PROD_TYPE		lpt	WITH (NOLOCK)	ON	fo.PROD_TYPE_CD	=	lpt.FSA_PROD_TYPE_CD
				INNER JOIN	dbo.LK_ORDR_TYPE		lot WITH (NOLOCK)	ON	fo.ORDR_TYPE_CD	=	lot.FSA_ORDR_TYPE_CD
				INNER JOIN	dbo.FSA_ORDR_CUST		foc	WITH (NOLOCK)	ON	fo.ORDR_ID		=	foc.ORDR_ID
				LEFT JOIN	dbo.ORDR_REC_LOCK		ol	WITH (NOLOCK)	ON	o.ORDR_ID		=	ol.ORDR_ID
			WHERE	foc.CIS_LVL_TYPE = 'H1'
				AND	o.ORDR_STUS_ID <> 4
			UNION
			--IPL && NCCO
			SELECT	o.PRNT_ORDR_ID,
					o.ORDR_ID,
					CONVERT(VARCHAR,ISNULL(io.ORDR_ID,ncco.ORDR_ID)),
					'',
					'',--loa.ORDR_ACTN_DES,
					lpt.PROD_TYPE_DES,
					lot.ORDR_TYPE_DES,
					o.H5_H6_CUST_ID,
					CASE o.DMSTC_CD	WHEN 0 THEN 'Domestic'
									WHEN 1 THEN 'International'  
					END  AS DomesticFlag,
					'',
					'',
					ISNULL(o.CUST_CMMT_DT, ISNULL(io.CUST_CMMT_DT,ncco.CCS_DT)),
					''		AS Error,
					CASE	WHEN ISNULL(ol.ORDR_ID,0) = 0 THEN 0 ELSE 1 END,
					pID.orderID	AS	'Primary Parent',
					0			AS	'Related Parent',
					CONVERT(VARCHAR,o.ORDR_ID)	AS	OrderFTN,
					CONVERT(Varchar,0)			AS	OrderRelFTN
			FROM			@ParentIDs				pID
				INNER JOIN	dbo.ORDR				o	WITH (NOLOCK)	ON	o.PRNT_ORDR_ID	=	pID.orderID
				LEFT  JOIN	dbo.IPL_ORDR			io	WITH (NOLOCK)	ON	o.ORDR_ID		=	io.ORDR_ID
																		AND	io.ORDR_TYPE_ID <> 8
				LEFT  JOIN	dbo.NCCO_ORDR			ncco WITH (NOLOCK)  ON	ncco.ORDR_ID	=	o.ORDR_ID
																		AND	ncco.ORDR_TYPE_ID <> 8
				INNER JOIN	dbo.LK_PROD_TYPE		lpt	WITH (NOLOCK)	ON	((io.PROD_TYPE_ID	=	lpt.PROD_TYPE_ID)
																				OR
																			 (ncco.PROD_TYPE_ID	=	lpt.PROD_TYPE_ID)
																			)
				INNER JOIN	dbo.LK_ORDR_TYPE		lot WITH (NOLOCK)	ON	((io.ORDR_TYPE_ID	=	lot.ORDR_TYPE_ID)
																				OR
																			 (ncco.ORDR_TYPE_ID	=	lot.ORDR_TYPE_ID)
																			)
				LEFT  JOIN	dbo.ORDR_REC_LOCK		ol	WITH (NOLOCK)	ON	o.ORDR_ID		=	ol.ORDR_ID
			

			--Update DeviceID's
			UPDATE ol
			SET DeviceID = fc.DEVICE_ID
			FROM @OrderList ol INNER JOIN
			dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON fc.ORDR_ID=ol.OrderID
			WHERE fc.DEVICE_ID IS NOT NULL
			
			--Set error for each line that is too far
			Update	ol
			Set		Error = Convert(Varchar(10),ol.FTN) + ' in BAR Pending/Complete, CCD not allowed'
			FROM			@OrderList		ol
				INNER JOIN	dbo.ORDR		o	WITH (NOLOCK)	ON ol.orderID	=	o.ORDR_ID
				INNER JOIN	dbo.ACT_TASK	at	WITH (NOLOCK)	ON at.ORDR_ID	=	o.ORDR_ID
				LEFT JOIN	dbo.FSA_ORDR	f	WITH (NOLOCK)	ON o.ORDR_ID	=	f.ORDR_ID
			WHERE	at.TASK_ID	IN	(1000, 1001)
				AND at.STUS_ID	<>	3
				AND NOT (o.DMSTC_CD = 0 AND ISNULL(f.ORDR_TYPE_CD,'') = 'DC' AND at.TASK_ID = 1000 AND at.STUS_ID <> 2)
				AND NOT (ISNULL(f.ORDR_TYPE_CD,'') = 'CH' AND at.TASK_ID = 1000 AND at.STUS_ID <> 2)
				AND o.ORDR_STUS_ID <> 4
			
			--Remove if there are no pending orders
			Delete	ol
			From	@Orderlist	ol
			Where	NOT EXISTS	(SELECT 1 FROM dbo.ORDR o WITH (NOLOCK) Where o.PRNT_ORDR_ID IN (ol.ParentOrderID, ol.relParID) AND o.ORDR_STUS_ID IN (0,1))

			--Bypass scenarios
			If @bypass = 1
			BEGIN
				Update	ol
				Set		Error = '' 
				From	@Orderlist	ol
				WHERE	NOT EXISTS
						(
							SELECT 'X'
								From	dbo.FSA_ORDR_MSG fom
								Where	fom.ORDR_ID = ol.OrderID
									AND fom.STUS_ID = 9
						)
				--Where	PrimaryParent in (Select PrimaryParent From @Orderlist o Group by PrimaryParent Having Count(OrderID) > 1)
			END

			--Return List 
			SELECT DISTINCT * FROM @OrderList ORDER BY PrimaryParent, OrderID
			
			
		END --Not Null
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	
END
