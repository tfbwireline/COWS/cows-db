USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertOrdrCmplAll_V5U_V2]    Script Date: 07/22/2021 10:21:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Kumar
-- Create date: 12/10/2019
-- Description:	This SP activates all items have been sent Mach5 for new COWS app.
-- =============================================
ALTER PROCEDURE [dbo].[insertOrdrCmplAll_V5U_V2]
	@ORDR_ID        INT = 0,
	@DEVICE_ID         VARCHAR(25),
	@NOTE           VARCHAR (1000) = NULL,
	@TECH           VARCHAR (100) = NULL,
	@INROUTE        DATETIME = NULL,
 	@ONSITE         DATETIME = NULL,
 	@COMPLETED       DATETIME = NULL
 	
AS
BEGIN TRY

	SET @NOTE = ISNULL(@NOTE,'')
	--Skip for MDS Disconnect Events
	DECLARE @EventID INT = CASE WHEN (ISNUMERIC(LTRIM(RTRIM(substring(@NOTE,(PATINDEX('%, EventID : %',@NOTE)+12),LEN(@NOTE)))))=1) THEN CONVERT(INT,LTRIM(RTRIM(substring(@NOTE,(PATINDEX('%, EventID : %',@NOTE)+12),LEN(@NOTE))))) ELSE -1 END
	IF NOT(((@NOTE LIKE '%, EventID%' ) AND (@ORDR_ID=0) AND EXISTS(SELECT 'X' FROM dbo.MDS_EVENT WITH (NOLOCK) WHERE EVENT_ID=@EventID AND MDS_ACTY_TYPE_ID=3)))
	BEGIN
	DECLARE @FSA_CPE_LINE_ITEM_ID INT	
			,@FTN VARCHAR(50)
			,@ORDR_TYPE_CD VARCHAR(2)
	DECLARE @IDS table (FSA_CPE_LINE_ITEM_ID INT)
	
	IF @ORDR_ID = 0
		BEGIN
			SELECT DISTINCT @ORDR_ID = a.ORDR_ID 
				FROM dbo.ACT_TASK a WITH (NOLOCK)
				INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM li WITH (NOLOCK)
							ON li.ORDR_ID = a.ORDR_ID
				WHERE (a.TASK_ID = 1000 AND a.STUS_ID = 0)
					AND li.DEVICE_ID = @DEVICE_ID 
		END 
		
	IF ISNULL(@ORDR_ID,0) <> 0
	
		BEGIN
			IF EXISTS (SELECT * FROM dbo.ACT_TASK WITH (NOLOCK)
							WHERE ORDR_ID = @ORDR_ID AND (TASK_ID IN (1000,1001) AND STUS_ID = 0))
				BEGIN	
					SELECT @ORDR_TYPE_CD = ORDR_TYPE_CD, @FTN = FTN FROM dbo.FSA_ORDR WITH (NOLOCK) 
										WHERE ORDR_ID = @ORDR_ID
					IF @ORDR_TYPE_CD = 'DC'
						BEGIN
							UPDATE dbo.ORDR 
								SET ORDR_STUS_ID = 5
							WHERE ORDR_ID = @ORDR_ID
						END
					ELSE
						BEGIN
							UPDATE dbo.ORDR 
								SET ORDR_STUS_ID = 2
							WHERE ORDR_ID = @ORDR_ID
						END					
					
					
					IF NOT EXISTS (SELECT * FROM SSTAT_RSPN WITH (NOLOCK)
									 WHERE ORDER_ACTION	= 'Complete' AND DEVICE_ID = @DEVICE_ID)
						BEGIN
								EXEC dbo.insertSSTATReq @ORDR_ID,3,@FTN
						END
					INSERT INTO @IDS (FSA_CPE_LINE_ITEM_ID) 
						 (SELECT FSA_CPE_LINE_ITEM_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
											WHERE ORDR_ID = @ORDR_ID AND CMPL_DT IS NULL
											AND DEVICE_ID = @DEVICE_ID AND ITM_STUS = 401) 
					--SELECT * FROM @IDS 
					WHILE EXISTS (SELECT * FROM @IDS)
						BEGIN
						  SELECT TOP 1 @FSA_CPE_LINE_ITEM_ID = FSA_CPE_LINE_ITEM_ID
									FROM @IDS
						  
						  UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM 
								SET CMPL_DT =  GETDATE()
								WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
										AND DEVICE_ID = @DEVICE_ID		

						  DELETE @IDS WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
										
						END
						
					IF @DEVICE_ID NOT IN (SELECT DEVICE FROM dbo.M5_ORDR_MSG WITH (NOLOCK)
												 WHERE ORDR_ID = @ORDR_ID AND M5_MSG_ID In (1,7))
							BEGIN
								
								IF EXISTS (SELECT 'X' FROM FSA_ORDR WITH (NOLOCK) 
											WHERE ORDR_ID = @ORDR_ID AND ORDR_SUB_TYPE_CD = 'ISMV' )
									BEGIN
										INSERT INTO [dbo].[M5_ORDR_MSG]
									([ORDR_ID],[M5_MSG_ID],[NTE],[MSG], [DEVICE],[CMPNT_ID],[STUS_ID],[CREAT_DT],[SENT_DT])     
										VALUES (@ORDR_ID,7,@NOTE,null,@DEVICE_ID,null,10,GETDATE(),NULL)
									END
								
								ELSE
									BEGIN 
										INSERT INTO [dbo].[M5_ORDR_MSG]
											([ORDR_ID],[M5_MSG_ID],[NTE],[MSG], [DEVICE],[CMPNT_ID],[STUS_ID],[CREAT_DT],[SENT_DT])     
										VALUES (@ORDR_ID,1,@NOTE,null,@DEVICE_ID,null,10,GETDATE(),NULL)
									END	
							END

					IF EXISTS (SELECT * FROM dbo.ODIE_REQ WITH (NOLOCK)
								WHERE @ORDR_ID = ORDR_ID AND ODIE_MSG_ID = 5)
							BEGIN								
								INSERT INTO [COWS].[dbo].[ODIE_REQ]
								   ([ORDR_ID],[ODIE_MSG_ID],[STUS_MOD_DT],[H1_CUST_ID],[CREAT_DT]
								   ,[STUS_ID],[MDS_EVENT_ID],[TAB_SEQ_NBR],[CUST_NME],[ODIE_CUST_ID]
								   ,[CPT_ID],[ODIE_CUST_INFO_REQ_CAT_TYPE_ID],[DEV_FLTR])
								VALUES
								   (@ORDR_ID,6,GETDATE(),NULL,GETDATE()
								   ,10,NULL,NULL,NULL,NULL
								   ,NULL,NULL,@DEVICE_ID)

								EXEC dbo.insertOrderNotes @ORDR_ID,9,'Order Information successfully sent to ODIE.',1
							END

					IF ISNULL(@TECH,'') <> ''
						BEGIN		
							UPDATE dbo.USER_WFM_ASMT
							SET INRTE_TM = @INROUTE, ONSITE_TM = @ONSITE, CMPLT_TM = @COMPLETED
							WHERE ORDR_ID = @ORDR_ID AND (USR_PRF_ID IN (44,56))
						END
				
					IF EXISTS (SELECT * from dbo.BPM_ORDR_ADR WITH (NOLOCK)
								where @ORDR_ID = ORDR_ID)
						BEGIN
							DELETE dbo.BPM_ORDR_ADR WHERE ORDR_ID = @ORDR_ID
						END
					
					EXEC dbo.CompleteActiveTask @ORDR_ID,1000,2,'Order Complete in COWS.',1,6
				END
			END
		END
	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH
