USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getM5Orders]    Script Date: 07/22/2021 10:08:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--=============================================
 --Author:		<Ramesh Ragi>
 --Create date: <08/06/2015>
 --Description:	<This SP retrieves orders from M5 
	--which are ready to be inserted into COWS>
-- jrg7298, 20170702, New extract codes 'M'(Missing Data), 'I'(COWS Internal Error) introduced
	-- which will be monitored to rectify/workaround instead of setting NRMBPM record to E
 --=============================================
ALTER PROCEDURE [dbo].[getM5Orders]
AS

BEGIN
BEGIN TRY
	SET NOCOUNT ON;
	SET DEADLOCK_PRIORITY 9;

	DECLARE @BPM_COWS_NRFC TABLE
	
		(
			H1_ID			Varchar(9),
			H6_ID			Varchar(9),
			ORDR_ID			Int,
			RELTD_ORDR_ID	Int,
			PROD_CD			Varchar(5),
			TYPE_NME		Varchar(5),
			SUB_TYPE_NME	Varchar(2),
			XTRCT_CD		Varchar(1),
			XTRCT_DT		Date,
			PROCESSED_FLAG_CD BIT DEFAULT 0,
			FSA_ORDR_ID		varchar(max) DEFAULT '',
			CPE_DEV_ID		varchar(50),
			ORDR_SEQ_NBR	Int
			
		)
	DECLARE @Cnt Int = 0
	DECLARE @Ctr Int = 0
	DECLARE @FTN_ID VARCHAR(20)
	
	DECLARE @ORDR_SEQ_NBR INT
	DECLARE @M5_ORDR_ID INT
	DECLARE @M5_RELT_ORDR_ID INT
	DECLARE @ORDR_TYPE_CD Varchar(5)
	DECLARE @QDA_ORDR_ID varchar(max)
	DECLARE @ordr_ids varchar(max)
	DECLARE @XTRCT_CD Varchar(1)
	DECLARE @XTRCT_DT Varchar(19)
	SET @XTRCT_DT = REPLACE(CONVERT(varchar,getdate(),126),'T',' ')
	DECLARE @now Varchar(23)
	SET @now = REPLACE(CONVERT(char(30), GETDATE(),126),'T',' ')
	DECLARE @CPE_DEV_ID Varchar(50)
	DECLARE @IsCCDEXPDOrder int = 0
	DECLARE @IsRTSHold Bit = 0
	
	DECLARE @SQL nVarchar(max)
	SET @SQL = ''
	--Gather data from NRM BPM Interface table which needs to be processed. 
	--For PJ17929, we are looking at product types like SLFR, MPLS and DIA Off/On net
	SET @SQL = 'SELECT H1_ID,H6_ID,ORDR_ID,RELTD_ORDR_ID, PROD_CD, TYPE_NME, SUB_TYPE_NME,XTRCT_CD,XTRCT_DT,CPE_DEV_ID, ORDR_SEQ_NBR
					FROM OPENQUERY (NRMBPM,		
				''SELECT DISTINCT H1_ID,H6_ID,ORDR_ID,RELTD_ORDR_ID, PROD_CD, TYPE_NME, SUB_TYPE_NME,XTRCT_CD,XTRCT_DT, CPE_DEV_ID, ORDR_SEQ_NBR
					FROM BPMF_OWNER.BPM_COWS_NRFC  
					WHERE	(ORDR_ID !=130904) AND (ORDR_ID != 286442) AND (XTRCT_CD = ''''N'''')
						AND ((XTRCT_DT IS NULL) OR (to_char(XTRCT_DT + Interval ''''15'''' minute,''''yyyy-mm-dd hh24:mi:ss'''')  <=  ''''' + CONVERT(VARCHAR,@now) +'''''))
					ORDER BY ORDR_ID 
					'') '	
	INSERT INTO @BPM_COWS_NRFC (H1_ID,H6_ID,ORDR_ID,RELTD_ORDR_ID,PROD_CD,TYPE_NME,SUB_TYPE_NME,XTRCT_CD,XTRCT_DT,CPE_DEV_ID,ORDR_SEQ_NBR)	
	
	EXEC sp_executesql @SQL
	
	--**##--Select * From @BPM_COWS_NRFC
	
	
	INSERT INTO dbo.M5_BPM_COWS_NRFC (ORDR_ID,TYPE_NME,SUB_TYPE_NME,RELTD_ORDR_ID,PROD_CD,H6_ID,H1_ID,XTRCT_CD,XTRCT_DT,STUS_ID,ORDR_SEQ_NBR)
	SELECT ORDR_ID,TYPE_NME,SUB_TYPE_NME,RELTD_ORDR_ID,PROD_CD,H6_ID,H1_ID,XTRCT_CD,GETDATE(),0,ORDR_SEQ_NBR
		FROM @BPM_COWS_NRFC 
	
	IF ((SELECT COUNT(1) FROM dbo.M5_BPM_COWS_NRFC WITH (NOLOCK) WHERE STUS_ID = 0) > 0 )
		BEGIN
			declare @sOrdrIDs Varchar(max) = ''
			SELECT @sOrdrIDs = @sOrdrIDs + ' ' + convert(varchar,ordr_id ) + ','   FROM (select distinct ordr_id from dbo.M5_BPM_COWS_NRFC with (nolock) WHERE STUS_ID = 0) as x	
			Select @sOrdrIDs = SUBSTRING(@sOrdrIDs,1,len(@sOrdrIDs) - 1)
			
			DECLARE @FTN TABLE (M5_ORDR_ID Int, FTN Varchar(20))
				
			SET  @SQL = 'SELECT * 
							FROM OPENQUERY(M5,''SELECT ORDR_ID, ORDER_NBR 
														FROM Mach5.V_V5U_ORDR 
														WHERE ORDR_ID IN ('   + Convert(varchar(max),@sOrdrIDs) + ')'')'
							+ 
						'UNION 
							SELECT * FROM 
									OPENQUERY(M5,''SELECT CHNG_TXN_ID, CHNG_TXN_NBR 
														FROM  Mach5.V_V5U_CHNG_TXN  
														WHERE CHNG_TXN_ID IN ('   + Convert(varchar(max),@sOrdrIDs) + ')'')'
			INSERT INTO @FTN
			Exec sp_executesql @SQL

			
			Update m
				SET FTN = f.FTN
				FROM  dbo.M5_BPM_COWS_NRFC m
			LEFT JOIN @FTN f ON f.M5_ORDR_ID = m.ORDR_ID
				
			SELECT @Cnt = COUNT(1) FROM @BPM_COWS_NRFC
			
			
			IF (@Cnt > 0)
				BEGIN
					WHILE (@Ctr < @Cnt)
						BEGIN
							
							SET @M5_ORDR_ID = 0
							SET @M5_RELT_ORDR_ID = 0
							SET @ORDR_TYPE_CD = ''
							SET @XTRCT_CD = ''
							SET @QDA_ORDR_ID = ''
							SET @CPE_DEV_ID = ''
							
							SELECT TOP 1 
								@M5_ORDR_ID = ORDR_ID,
								@M5_RELT_ORDR_ID = RELTD_ORDR_ID,
								@ORDR_TYPE_CD = CASE 
												WHEN SUB_TYPE_NME = 'AR'    THEN 'AR'
												WHEN SUB_TYPE_NME = 'PRCV'   THEN 'PRCV'
												WHEN SUB_TYPE_NME = 'ISMV'   THEN 'ISMV'
												WHEN SUB_TYPE_NME = 'ROCP'   THEN 'ROCP'
												WHEN SUB_TYPE_NME = 'CO'	 THEN 'CO'
												WHEN SUB_TYPE_NME = 'ACRF'   THEN 'ACRF'
												WHEN SUB_TYPE_NME = 'CSCH'   THEN 'CSCH'
												WHEN SUB_TYPE_NME = 'ACRN'   THEN 'ACRN'
												WHEN SUB_TYPE_NME = 'REUS'   THEN 'REUS'
												ELSE TYPE_NME
												END,
								@CPE_DEV_ID = ISNULL(CPE_DEV_ID,''),
								@ORDR_SEQ_NBR = ORDR_SEQ_NBR
							FROM @BPM_COWS_NRFC
							WHERE PROCESSED_FLAG_CD = 0

							INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('@M5_ORDR_ID:'+CONVERT(VARCHAR,@M5_ORDR_ID)+'@CPE_DEV_ID:'+@CPE_DEV_ID+';@ORDR_SEQ_NBR:'+CONVERT(VARCHAR,@ORDR_SEQ_NBR),'getM5Orders', GETDATE())
							
							SELECT @FTN_ID = FTN
								FROM dbo.M5_BPM_COWS_NRFC WITH (NOLOCK)
								WHERE	ORDR_ID =	@M5_ORDR_ID 
									AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
									AND	STUS_ID	=	0
							
							SELECT @IsCCDEXPDOrder = CASE @ORDR_TYPE_CD
														WHEN 'CCD'	 THEN 1
														WHEN 'EXDT'  THEN 1
														WHEN 'PRCV'  THEN 2 -- Purchase/Rental conversion
														WHEN 'ISMV'  THEN 2 -- Inside Move
														WHEN 'ROCP'  THEN 2 -- Reoption
														WHEN 'CO'    THEN 2  -- Class of service
														WHEN 'ACRF'  THEN 2  -- Access Refresh
														WHEN 'CSCH'  THEN 2  -- Zscaler
														WHEN 'FCCN'  THEN 2  -- Transaction Cancel
														WHEN 'ACRN'  THEN 2  -- Access Renewal
														WHEN 'REUS'  THEN 2  -- ODIE NUA Change
														ELSE 0
													END		
							
							IF @IsCCDEXPDOrder = 1
								BEGIN
									INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('@IsCCDEXPDOrder=1:','getM5Orders', GETDATE())
									EXEC dbo.insertMach5OrderDetails @M5_ORDR_ID, @M5_RELT_ORDR_ID, @ORDR_TYPE_CD, @QDA_ORDR_ID OUTPUT, ''	
								END
							
							IF @IsCCDEXPDOrder = 2
								BEGIN
									IF EXISTS
									(
										SELECT 'X'
											FROM	dbo.FSA_ORDR WITH (NOLOCK)
											WHERE	FTN = @FTN_ID
									)
										BEGIN
											UPDATE	dbo.M5_BPM_COWS_NRFC
											SET		Error_msg	=	'This FTN already exists in COWS. Please delete existing order and try again.',
													STUS_ID =	1,
													UPDATED_DT = GETDATE()													
											WHERE	ORDR_ID = @M5_ORDR_ID
											    AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
												AND STUS_ID = 0
												
											UPDATE @BPM_COWS_NRFC 
												SET PROCESSED_FLAG_CD = 1, 
													XTRCT_CD = 'D'
												WHERE	ORDR_ID = @M5_ORDR_ID 
													AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
													AND PROCESSED_FLAG_CD = 0
										END
									ELSE
										BEGIN
											INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('@IsCCDEXPDOrder=2:','getM5Orders', GETDATE())
											EXEC dbo.insertMach5TxnDetails @M5_ORDR_ID, @M5_RELT_ORDR_ID, @ORDR_TYPE_CD, @QDA_ORDR_ID OUTPUT, ''	
										END
								END
							
							IF EXISTS
								(
									SELECT 'X'
										FROM dbo.FSA_ORDR f WITH (NOLOCK)
									Inner Join dbo.FSA_ORDR_CPE_LINE_ITEM fc with (nolock) on f.ordr_id = fc.ordr_id
										Where	f.FTN IN (SELECT FTN FROM dbo.M5_BPM_COWS_NRFC WITH (NOLOCK) WHERE ORDR_ID = @M5_ORDR_ID)
											AND fc.DEVICE_ID in (SELECT CPE_DEV_ID FROM @BPM_COWS_NRFC WHERE ORDR_ID = @M5_ORDR_ID)
								)
								BEGIN
									SET @IsRTSHold = 1
									INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('@IsRTSHold=1:','getM5Orders', GETDATE())
									EXEC dbo.insertMach5OrderDetails @M5_ORDR_ID, @M5_RELT_ORDR_ID, @ORDR_TYPE_CD, @QDA_ORDR_ID OUTPUT, @CPE_DEV_ID
								END
								
							IF @IsCCDEXPDOrder = 0 AND @IsRTSHold = 0
								BEGIN
									IF EXISTS
									(
										SELECT 'X'
											FROM	dbo.FSA_ORDR WITH (NOLOCK)
											WHERE	FTN = @FTN_ID
									)
										BEGIN
											UPDATE	dbo.M5_BPM_COWS_NRFC
											SET		Error_msg	=	'This FTN already exists in COWS. Please delete existing order and try again.',
													STUS_ID =	1,
													UPDATED_DT = GETDATE()													
											WHERE	ORDR_ID = @M5_ORDR_ID
											    AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
												AND STUS_ID = 0
												
											UPDATE @BPM_COWS_NRFC 
												SET PROCESSED_FLAG_CD = 1, 
													XTRCT_CD = 'D'
												WHERE	ORDR_ID = @M5_ORDR_ID 
													AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
													AND PROCESSED_FLAG_CD = 0
										END
									ELSE
										BEGIN
											INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('@IsRTSHold=0&@IsCCDEXPDOrder=0:','getM5Orders', GETDATE())
											EXEC dbo.insertMach5OrderDetails @M5_ORDR_ID, @M5_RELT_ORDR_ID, @ORDR_TYPE_CD, @QDA_ORDR_ID OUTPUT, ''	
										END
								END		
							
							IF @QDA_ORDR_ID NOT IN ('MD','IE','X','MD,MD','MD,X','X,MD','X,X','R')
								BEGIN
									DECLARE @ORDR_ID_CD INT
									DECLARE @QDA_ORDR TABLE (ORDR_ID varchar(20), Flag bit)
									DECLARE @FSA_ORDR_ID INT
									INSERT INTO @QDA_ORDR (ORDR_ID, Flag)
									Select *,0 From web.ParseCommaSeparatedStrings(@QDA_ORDR_ID)
									
									--Atleast one valid ORDR_ID for paired orders like CH, CNI, CND
									IF ((EXISTS (SELECT 'X' FROM @QDA_ORDR WHERE ORDR_ID NOT LIKE '%[^0-9]%'))
										AND NOT EXISTS (SELECT 'X' FROM @QDA_ORDR WHERE ORDR_ID = 'IE')
										AND (LEN(@QDA_ORDR_ID)>1))
									BEGIN									
										IF ((SELECT COUNT(DISTINCT ORDR_ID) FROM @QDA_ORDR) > 0)
										BEGIN
											SELECT TOP 1 @FSA_ORDR_ID = CONVERT(INT,ORDR_ID)
													FROM @QDA_ORDR 
													WHERE ISNULL(ORDR_ID,'0') NOT IN ('','0')
													  AND ORDR_ID NOT IN ('MD','X','IE','MD,MD','MD,X','X,MD','X,X')
													  
												--**##--SELECT @FSA_ORDR_ID AS 'FSA_ORDR_ID'
												IF @FSA_ORDR_ID != 0
													BEGIN
														--Gather additional information relating to transport from NRM BPM view provided.													
														EXEC dbo.updateNRMBPMData @FSA_ORDR_ID				
													END	
										END
									
									
										INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('@QDA_ORDR_ID:'+@QDA_ORDR_ID,'getM5Orders', GETDATE())
										UPDATE @BPM_COWS_NRFC 
											SET		PROCESSED_FLAG_CD	=	2,
													XTRCT_CD			=	'Y',
													FSA_ORDR_ID			=	@QDA_ORDR_ID
											WHERE	ORDR_ID = @M5_ORDR_ID 
												AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
												AND PROCESSED_FLAG_CD = 0
											
										
										UPDATE	dbo.M5_BPM_COWS_NRFC
											SET FSA_ORDR_ID = @QDA_ORDR_ID,
												XTRCT_CD = 'Y',
												STUS_ID = 2,
												UPDATED_DT = GETDATE()
											WHERE	ORDR_ID = @M5_ORDR_ID
												AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
												AND STUS_ID = 0
									END
									ELSE
									BEGIN
										UPDATE @BPM_COWS_NRFC 
										SET PROCESSED_FLAG_CD = 1, 
											XTRCT_CD = 'I'
										WHERE	ORDR_ID = @M5_ORDR_ID 
											AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
											AND PROCESSED_FLAG_CD = 0
									
										UPDATE	dbo.M5_BPM_COWS_NRFC
											SET		XTRCT_CD = 'I',
													STUS_ID = 1,
													UPDATED_DT = GETDATE()
											WHERE	ORDR_ID = @M5_ORDR_ID
												AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
												AND STUS_ID = 0
										INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('@QDA_ORDR_ID:'+@QDA_ORDR_ID,'getM5Orders', GETDATE())
									END
								END
							ELSE
								BEGIN 
									UPDATE @BPM_COWS_NRFC 
										SET PROCESSED_FLAG_CD = 1, 
											XTRCT_CD = CASE @QDA_ORDR_ID
															WHEN 'MD' THEN 'M'
															WHEN 'IE' THEN 'I'
															WHEN 'X'  THEN 'X'
															WHEN 'MD,MD' THEN 'M'
															WHEN 'MD,X'  THEN 'X'
															WHEN 'X,MD'  THEN 'X'
															WHEN 'X,X'   THEN 'X'
															WHEN 'R'     THEN 'Y' -- REUS transaction successs code is 'R'  7/15/21 dlp0278
															ELSE 'E' END
										WHERE	ORDR_ID = @M5_ORDR_ID 
											AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
											AND PROCESSED_FLAG_CD = 0
									
									UPDATE	dbo.M5_BPM_COWS_NRFC
										SET		XTRCT_CD = CASE @QDA_ORDR_ID
															WHEN 'MD' THEN 'M'
															WHEN 'IE' THEN 'I'
															WHEN 'X'  THEN 'X'
															WHEN 'MD,MD' THEN 'M'
															WHEN 'MD,X'  THEN 'X'
															WHEN 'X,MD'  THEN 'X'
															WHEN 'X,X'   THEN 'X'
															WHEN 'R'     THEN 'Y' -- REUS transaction successs code is 'R' 7/15/21 dlp0278
															ELSE 'E' END,
												STUS_ID = 1,
												UPDATED_DT = GETDATE()
										WHERE	ORDR_ID = @M5_ORDR_ID
											AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
											AND STUS_ID = 0
									INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('@QDA_ORDR_ID:'+@QDA_ORDR_ID,'getM5Orders', GETDATE())
								END
							
								
							SELECT @XTRCT_CD = XTRCT_CD
								FROM	@BPM_COWS_NRFC
								WHERE	ORDR_ID = @M5_ORDR_ID 
									AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
							
							--**##--Select @XTRCT_CD, @XTRCT_DT, @M5_ORDR_ID
							
							
							SELECT  @XTRCT_DT = REPLACE(CONVERT(char(30), UPDATED_DT,126),'T',' ') 
								FROM dbo.M5_BPM_COWS_NRFC WITH (NOLOCK)
								WHERE ORDR_ID = @M5_ORDR_ID AND ORDR_SEQ_NBR = @ORDR_SEQ_NBR
									
							
							--Call NRMBPM Stored Procedure to update inserted record, only for D and Y codes
							IF (@XTRCT_CD IN ('D','M','X','Y'))
							BEGIN
								INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('@XTRCT_CD:'+@XTRCT_CD,'getM5Orders', GETDATE())
								--INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES (CONVERT(VARCHAR,@M5_ORDR_ID),'getM5Orders', GETDATE())
								--INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES (@XTRCT_CD,'getM5Orders', GETDATE())
								--INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES (@XTRCT_DT,'getM5Orders', GETDATE())
								--INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES (CONVERT(VARCHAR,@ORDR_SEQ_NBR),'getM5Orders', GETDATE())
								SET @SQL = ''
								SET @SQL = ' EXECUTE ( ''begin BPMF_OWNER.pkg_bpmf_cows.proc_bpm_cows_nrfc(' + CONVERT(VARCHAR,@M5_ORDR_ID) + ',''''' + @XTRCT_CD + ''''',''''' + @XTRCT_DT + ''''','''''+@CPE_DEV_ID+''''', null,'+ CONVERT(VARCHAR,@ORDR_SEQ_NBR) + ',''''U'''',null,null); end;'') AT NRMBPM; '
								EXEC sp_executesql @SQL
							END
							
							SET @Ctr = @Ctr + 1
						END
					END
					SET @sql = 'Select * From OpenQuery(NRMBPM,''Select * From BPMF_OWNER.BPM_COWS_NRFC WHERE ORDR_ID IN (' + @sOrdrIDs + ')'')'
					EXEC sp_executesql @SQL
		
		END
  
	--Completion of BAR pending orders
	EXEC [dbo].[processBillActivation]
	EXEC [dbo].[CpeEqRvwTskRule]
END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH    
END