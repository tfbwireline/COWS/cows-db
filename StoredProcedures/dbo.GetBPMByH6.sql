USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[GetBPMByH6]    Script Date: 01/11/2021 9:42:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		km967761
-- Create date: 01/07/2021
-- Description:	Get BPM Data via H6
-- =============================================
ALTER PROCEDURE [dbo].[GetBPMByH6] -- exec dbo.GetBPMByH6 '928425770 '
	-- Add the parameters for the stored procedure here
	@H6 VARCHAR(MAX)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	declare @x nvarchar(max) = 'select * from openquery(nrmbpm,''select * from BPMF_OWNER.V_BPMF_COWS_ACTV_EVENT where H6_H5_ID=''''' + @H6 + ''''' '')'
	declare @x2 nvarchar(max) = 'select * from openquery(nrmbpm,''select * from BPMF_OWNER.V_BPMF_VAS_COWS_ACTV_EVENT where H6_H5_ID=''''' + @H6 + ''''' '')'
	declare @x3 nvarchar(max) = 'select * from openquery(nrmbpm,''select * from BPMF_OWNER.V_BPMF_CE_COWS_ACTV_EVENT where H6_ID=''''' + @H6 + ''''' '')'
	declare @x4 nvarchar(max) = 'select * from openquery(nrmbpm,''select * from BPMF_OWNER.V_BPMF_COWS_REFLAG_ACTV_EVENT where H6_H5_ID=''''' + @H6 + ''''' '')'
	declare @x5 nvarchar(max) = 'select * from openquery(nrmbpm,''select * from BPMF_OWNER.NSF_COWS_NID_DETL where H6_ID=''''' + @H6 + ''''' '')'
	exec sp_executesql @x
	exec sp_executesql @x2
	exec sp_executesql @x3
	exec sp_executesql @x4
	exec sp_executesql @x5
END
