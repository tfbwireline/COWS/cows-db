USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[processZsclrDiscCncl]    Script Date: 05/30/2018 07:58:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By:		David Phillips
-- Create date: 01/10/2018
-- Description:	This SP is used to process CPE Zscaler Disc and Cancels.
-- =============================================
ALTER PROCEDURE [dbo].[processZsclrDiscCncl] --481271
@ORDR_ID INT, 
@TaskId SMALLINT = 600, 
@TaskStatus TINYINT = 2, 
@Comments VARCHAR(1000) = NULL

AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRY
	
	DECLARE @RELATED_ORDR INT
			,@PRNT_FTN VARCHAR(50)
	
	IF EXISTS (SELECT TOP 1 'X' FROM FSA_ORDR fo WITH (NOLOCK)
					INNER JOIN FSA_ORDR_CPE_LINE_ITEM itm WITH (NOLOCK)
						ON fo.ORDR_ID = itm.ORDR_ID AND itm.MFR_NME = 'ZSCA'
				WHERE fo.ORDR_ID = @ORDR_ID 
					AND fo.ORDR_TYPE_CD in ('DC','CN'))
			BEGIN
				 IF EXISTS (SELECT 'x' FROM FSA_ORDR WITH (NOLOCK) 
								WHERE ORDR_ID = @ORDR_ID
									AND ORDR_TYPE_CD = 'DC')
				
					BEGIN
						  INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
							(ORDR_ID,EMAIL_REQ_TYPE_ID,STUS_ID,EMAIL_LIST_TXT,EMAIL_CC_TXT,EMAIL_SUBJ_TXT,EMAIL_BODY_TXT)
						  VALUES	(@ORDR_ID,39,10,'david.l2.phillips@sprint.com,julie.a.peters@sprint.com','','SS Web Disconnect Request','')
					END
				 
				 ELSE 
				
					BEGIN
						SELECT TOP 1  @RELATED_ORDR = cp.ORDR_ID, @PRNT_FTN = f.PRNT_FTN 
								FROM dbo.FSA_ORDR f WITH (NOLOCK)
								INNER JOIN dbo.ORDR o ON o.ORDR_ID = f.ORDR_ID AND o.ORDR_CAT_ID = 6 -- added this for the conversion orders
								INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM c WITH (NOLOCK) ON f.ORDR_ID = c.ORDR_ID
								INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cp WITH (NOLOCK) 
									ON  cp.ORDR_CMPNT_ID = c.ORDR_CMPNT_ID 	 
								WHERE f.ORDR_ID = @ORDR_ID
									and cp.ORDR_ID <> @ORDR_ID 
								ORDER by cp.ORDR_ID DESC
								
								DECLARE @ReqLineNbrs TABLE (REQSTN_NBR varchar (10),CMPNT_ID int, REQ_LINE_NBR int)
								DECLARE @Req     varchar(50)
								       , @Line    int
								       , @CancelItems   varchar(max) = ''
								       , @IPM   varchar (200)
								       , @IPMphone varchar (50)
								       
								OPEN SYMMETRIC KEY FS@K3y 
										DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
								
								SELECT  
									@IPM = CASE
										WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN ISNULL(oc.FRST_NME + ' ' + oc.LST_NME,oc1.FRST_NME + ' ' + oc1.LST_NME)
										ELSE ISNULL(CONVERT(VARCHAR (max), DecryptByKey(sc.FRST_NME)) + ' ' + CONVERT(VARCHAR (max), DecryptByKey(sc.LST_NME))
											 ,CONVERT(VARCHAR (max), DecryptByKey(sc1.FRST_NME)) + ' ' + CONVERT(VARCHAR (max), DecryptByKey(sc1.LST_NME))) 
									END                                     
									,@IPMphone = '(' + ISNULL(oc.NPA,oc1.NPA) + ') ' + ISNULL(oc.NXX,oc1.NXX) + '-'+ ISNULL(oc.STN_NBR,oc1.STN_NBR)     
			
								FROM FSA_ORDR fo WITH (NOLOCK)
								INNER JOIN ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
								INNER JOIN FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.ORDR_ID = fo.ORDR_ID
													AND foc.CIS_LVL_TYPE in ('H6')
								INNER JOIN ORDR_ADR oa WITH (NOLOCK) ON oa.ORDR_ID = fo.ORDR_ID
													AND oa.CIS_LVL_TYPE in  ('H6')
								LEFT OUTER JOIN CUST_SCRD_DATA sa WITH (NOLOCK) 
										ON sa.SCRD_OBJ_ID = oa.ORDR_ADR_ID and sa.SCRD_OBJ_TYPE_ID = 14
			
								LEFT OUTER JOIN ORDR_CNTCT oc WITH (NOLOCK) ON oc.ORDR_ID = fo.ORDR_ID											
													AND oc.ROLE_ID = 99
													AND oc.CIS_LVL_TYPE in  ('H6')
								LEFT OUTER JOIN CUST_SCRD_DATA sc WITH (NOLOCK) 
										ON sc.SCRD_OBJ_ID = oc.ORDR_CNTCT_ID and sc.SCRD_OBJ_TYPE_ID = 15
			
								LEFT OUTER JOIN ORDR_CNTCT oc1 WITH (NOLOCK) ON oc1.ORDR_ID = fo.ORDR_ID											
													AND oc1.ROLE_ID = 99
													AND oc1.CIS_LVL_TYPE in  ('H1')
								LEFT OUTER JOIN CUST_SCRD_DATA sc1 WITH (NOLOCK) 
										ON sc1.SCRD_OBJ_ID = oc1.ORDR_CNTCT_ID and sc1.SCRD_OBJ_TYPE_ID = 15
								WHERE fo.ORDR_ID = @ORDR_ID	
											
								
								
								INSERT INTO @ReqLineNbrs (REQSTN_NBR,CMPNT_ID, REQ_LINE_NBR)
								 SELECT REQSTN_NBR, CMPNT_ID, REQ_LINE_NBR FROM dbo.PS_REQ_LINE_ITM_QUEUE WITH (NOLOCK)
									WHERE CMPNT_ID IN (SELECT ORDR_CMPNT_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
									                     WHERE ORDR_ID = @ORDR_ID)   
																
								WHILE EXISTS (SELECT * FROM @ReqLineNbrs)
									BEGIN
										 SELECT TOP 1 @Req = REQSTN_NBR  FROM @ReqLineNbrs
										 SELECT TOP 1 @Line = REQ_LINE_NBR FROM @ReqLineNbrs
												
										 SET @CancelItems = @CancelItems + (@Req + '/' + CONVERT(varchar(5), @Line)) + ' *** '											
											
										 DELETE @ReqLineNbrs WHERE REQSTN_NBR = @Req AND REQ_LINE_NBR = @Line									
									END
						
								INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
									(ORDR_ID,EMAIL_REQ_TYPE_ID,STUS_ID,EMAIL_LIST_TXT,EMAIL_CC_TXT,EMAIL_SUBJ_TXT,EMAIL_BODY_TXT)
								  VALUES	(@ORDR_ID,34,10,'david.l2.phillips@sprint.com,julie.a.peters@sprint.com','',
												'Zscaler Order Cancellation',
										'The following Req Number/Req Line Number(s) have been cancelled:  ' + @CancelItems 
												+ '  M5 Order ID:  ' + @PRNT_FTN + '.              Contact ' + @IPM + '  at ' + @IPMphone + ' with questions.')
	
					END
				
				
				EXEC dbo.CompleteActiveTask @ORDR_ID,600,2,'Zscaler - System Completed Equip Review task.',1,9
			    EXEC dbo.CompleteActiveTask @ORDR_ID,604,2,'Zscaler - System Completed Tech Assign Task',1,9
				EXEC dbo.CompleteActiveTask @ORDR_ID,1000,2,'Zscaler - System Completed BAR-Pending Task',1,9
			
			END	
	END TRY
	BEGIN CATCH
		exec [dbo].[insertErrorInfo]
	END CATCH
END

