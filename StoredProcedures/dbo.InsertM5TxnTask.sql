USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[InsertM5TxnTask]    Script Date: 07/22/2021 10:16:54 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	-- ================================================================
	-- Create By:		David Phillips
	-- Create date:		12/05/2016
	-- Description:		This SP is used to insert 
	--					initial task for M5 Change Transactions.
	--=================================================================

	ALTER PROCEDURE [dbo].[InsertM5TxnTask] --12675,'PRCV'
		@ORDR_ID		INT,
		@ORDR_TYPE_CD	VARCHAR (7)
	AS
	BEGIN


	SET NOCOUNT ON

	BEGIN TRY

	DECLARE @device varchar (25)
	

		
		IF ISNULL(@ORDR_ID,0) <> 0
		BEGIN
		
			DECLARE @FTN Varchar(50)
					
			SELECT @FTN = FTN FROM dbo.FSA_ORDR WHERE ORDR_ID = @ORDR_ID
		
			
			IF @ORDR_TYPE_CD = 'ROCP'
				BEGIN
					
					EXEC dbo.insertOrderNotes @ORDR_ID,1,'ReOption Transaction Received from M5.',1
					
					INSERT INTO dbo.ACT_TASK(ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
					VALUES (@ORDR_ID,600,0,601, GETDATE())
					
					/* Below removed 6/1/17 due to change in workflow
					 EXEC dbo.CpeWrkFlwTaskRule @ORDR_ID,604,0,'' 
					 EXEC [dbo].[insertSSTATReq]	@ORDR_ID,1,@FTN 
					*/
				END
			IF @ORDR_TYPE_CD = 'ISMV'
				BEGIN
				
					EXEC dbo.insertOrderNotes @ORDR_ID,1,'Inside Move Transaction Received from M5.',1
					
					INSERT INTO dbo.ODIE_REQ
					   (ORDR_ID,ODIE_MSG_ID,STUS_MOD_DT,H1_CUST_ID,CREAT_DT,STUS_ID,MDS_EVENT_ID
					   ,TAB_SEQ_NBR,CUST_NME,ODIE_CUST_ID,CPT_ID,ODIE_CUST_INFO_REQ_CAT_TYPE_ID,DEV_FLTR)
					VALUES
						(@ORDR_ID,9,GETDATE(),NULL,GETDATE(),10,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
						
					EXEC dbo.insertOrderNotes @ORDR_ID,1,'Message staged for ODIE.',1
					
					INSERT INTO dbo.ACT_TASK(ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
					VALUES (@ORDR_ID,604,0,601, GETDATE())
					
					EXEC dbo.CpeWrkFlwTaskRule @ORDR_ID,604,0,''
										
					EXEC [dbo].[insertSSTATReq]	@ORDR_ID,1,@FTN
					
					INSERT INTO dbo.USER_WFM_ASMT
						([ORDR_ID],[GRP_ID],[ASN_USER_ID],[ASN_BY_USER_ID],[ASMT_DT]
						,[CREAT_DT],[ORDR_HIGHLIGHT_CD],[ORDR_HIGHLIGHT_MODFD_DT],[USR_PRF_ID])
					VALUES
						(@ORDR_ID,13,1405,1,GETDATE(),GETDATE(),null,null,98)
					
					EXEC dbo.insertOrderNotes @ORDR_ID,1,'Order Assigned to Peggy Wheaton.',1
				
				END
			IF @ORDR_TYPE_CD = 'PRCV'
				BEGIN
				
					EXEC dbo.insertOrderNotes @ORDR_ID,1,'P/R Conversion Transaction Received from M5.',1
					
					SELECT TOP 1 @device = ISNULL(DEVICE_ID,'') FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
									WHERE ORDR_ID = @ORDR_ID AND ISNULL(DEVICE_ID,'') <> ''
										AND ITM_STUS = 401

					IF @device <> '' 
								AND EXISTS (SELECT 'X'
												FROM FSA_ORDR_CPE_LINE_ITEM cli
													INNER JOIN FSA_ORDR f on f.ORDR_ID = cli.ORDR_ID
													INNER JOIN ODIE_REQ orq on orq.ORDR_ID = f.ORDR_ID 
												WHERE cli.DEVICE_ID = @device 
														AND cli.ITM_STUS = 401
													AND orq.ODIE_MSG_ID = 1)
							BEGIN
								INSERT INTO dbo.ODIE_REQ
								   (ORDR_ID,ODIE_MSG_ID,STUS_MOD_DT,H1_CUST_ID,CREAT_DT,STUS_ID,MDS_EVENT_ID
								   ,TAB_SEQ_NBR,CUST_NME,ODIE_CUST_ID,CPT_ID,ODIE_CUST_INFO_REQ_CAT_TYPE_ID,DEV_FLTR)
								VALUES
									(@ORDR_ID,8,GETDATE(),NULL,GETDATE(),10,NULL,NULL,NULL,NULL,NULL,NULL,NULL)
				
								EXEC dbo.insertOrderNotes @ORDR_ID,1,'PRCV Message sent to ODIE.',1
									
								INSERT INTO dbo.USER_WFM_ASMT
										([ORDR_ID],[GRP_ID],[ASN_USER_ID],[ASN_BY_USER_ID],[ASMT_DT]
										,[CREAT_DT],[ORDR_HIGHLIGHT_CD],[ORDR_HIGHLIGHT_MODFD_DT],[USR_PRF_ID])
									VALUES
										(@ORDR_ID,13,1405,1,GETDATE(),GETDATE(),null,null,98)
					
								EXEC dbo.insertOrderNotes @ORDR_ID,1,'Order Assigned to Peggy Wheaton.',1	
					
							END


					INSERT INTO dbo.ACT_TASK(ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
					VALUES (@ORDR_ID,1000,0,1000, GETDATE())

				END
				
				IF @ORDR_TYPE_CD = 'CSCH' -- Zscaler
				BEGIN
				
					EXEC dbo.insertOrderNotes @ORDR_ID,1,'Zscaler Order recieved in EQ Review.',1
				
					IF ((SELECT ORDR_TYPE_CD FROM FSA_ORDR WHERE ORDR_ID = @ORDR_ID) = 'IN')
					
					BEGIN
					
						INSERT INTO dbo.ACT_TASK(ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
							VALUES (@ORDR_ID,600,0,600, GETDATE())
					END 
					
					ELSE IF ((SELECT ORDR_TYPE_CD FROM FSA_ORDR WHERE ORDR_ID = @ORDR_ID) = 'DC')
						BEGIN			
							INSERT INTO dbo.ACT_TASK(ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
								VALUES (@ORDR_ID,600,0,601, GETDATE())
						END				
					
								
				
				END
				IF @ORDR_TYPE_CD = 'CO' 
				BEGIN
				
					EXEC dbo.insertOrderNotes @ORDR_ID,1,'Class of Service order received',1
				
					EXEC dbo.updateNRMBPMData @ORDR_ID
				
				END

							
		END

	
	END TRY
	

	BEGIN CATCH
		exec [dbo].[insertErrorInfo]
	END CATCH

	END

