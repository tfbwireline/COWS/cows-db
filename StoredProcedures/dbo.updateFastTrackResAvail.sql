USE [COWS]
GO
_CreateObject 'SP','dbo','updateFastTrackResAvail'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Kyle Wichert
-- Create date: 09/04/2011
-- Description:	Weekly job to forecast user availability over the
--				next six months.  Gathers all scheduled shifts from 
--				dbo.Appt with MDS Fast Track type.  I parse out each
--				recurring appointment into the set of regular appts
--				for the six month period, then add up totals.
-- Change:		Change Type to MDS Shift not MDS Fast Trach Shift
--				Don't count Pending/Completed Events
--				Set up only 1 months worth of data, don't add up 
--				current events.  Partial hours count now...
-- Change:		AVAL_TME_SLOT_CNT no longer used, took out all workforce
--				functionality as per Pramod, 12/3/2012
-- ================================================================

 ALTER PROCEDURE [dbo].[updateFastTrackResAvail]
	
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY


		/*****************************************************
		Variable declarations with descriptions of use start
		*****************************************************/

		--This table is used to temp store the time slots from LK_EVENT_TYPE_TME_SLOT 
		--so we don't hit the table 180 times
		DECLARE @TimeSlots	TABLE (	TimeSlotID	TINYINT, StartTime TIME, EndTime TIME )
		--Create List of days for the next 6 months.
		DECLARE @Days		TABLE ( IntDay		Date)
		--This table will be used to track the time slot and resource counts.  In the end 
		--this is the table that will be used to insert into the actual COWS table
		DECLARE @EventAvl	TABLE 
		(
			EventIntervalID	INT IDENTITY(1,1),
			Day				SMALLDATETIME,
			timeslot		TINYINT,
			StartTime		TIME,
			EndTime			TIME,
		--	tsOnCalendar	SMALLINT,
			tsMaxResources	SMALLINT,
			tsTaken			SMALLINT,
			listTxt			XML
		)
		--This table is going to gather all events together of the correct type to itterate through
		DECLARE @Appt TABLE --#Appt
		(
			ApptID			INT IDENTITY(1,1),
			StartTime		SMALLDATETIME,
			EndTime			SMALLDATETIME,
			ApptType		INT,
			UserID			INT--,
			--esID			VARCHAR(50),
			--esOccIndex		INT	--Index i is the i-th occurance of a repeating event
		)

		--This table is going to be used to track which users are available for the event interval
		--in case a user is selected in multiple events.  This allows for an accurate count
		DECLARE @EventAvlUser TABLE 
		(
			ID				INT,
			EventIntervalID INT,
			UserID			INT,
			ShiftStart		TIME,
			ShiftEnd		TIME,
			toDelete		TINYINT,
			shiftInterval	INT
		)

		--Map Event to Appt (many to many)
		DECLARE @mapEventAppt TABLE
		(
			EventIntervalID	INT,
			ApptID			INT,
			ShiftStart		TIME,
			ShiftEnd		TIME
		)

		--Temp table for Fedline to iterate through each TimeSlot
		DECLARE @FedTS		TABLE
		(
			idx		tinyint Primary Key IDENTITY(1,1),
			tsID	tinyint
		)

		DECLARE @FedEventType	INT				--Constant for new Fedline Event Type
		DECLARE @EventType		INT				--Constant for MDS FastTrack
		DECLARE @ApptType		INT				--Constant for MDS FastTrack Shift
		DECLARE @Today			DATE			--Start of the 6 months out, iterator
		DECLARE @SixMonthsOut	SMALLDATETIME	--End of 6 months out
		DECLARE @cntr			INT				--Iterator for loops
		DECLARE @loopSize		INT				--# of times to loop
		DECLARE @deEndByDate	DATE
		DECLARE @getDATE		DATETIME		--Use one getdate() in case this procedure ever runs long
		DECLARE @StartSeed		INT				--Use to ensure that the Fedline Events start at 5000+
		DECLARE @tsID			TINYINT			--Use to store TimeSlotID in while loop
		--DECLARE @t1				SMALLINT
		--DECLARE @t2				SMALLINT
		--DECLARE @t3				SMALLINT
		--DECLARE @t4				SMALLINT
		DECLARE @FTDay			DATETIME

		/*Used for deciding the last day of the month */		
		DECLARE @dtval int
		DECLARE @NextMonthDT DATETIME
		DECLARE @CurrDT DATETIME
		DECLARE @DT DATETIME

		/*****************************************************
		Variable declarations with descriptions of use end
		*****************************************************/

		SET DATEFIRST 1		--WeekDay now returns 1 for Monday
		SET @FedEventType	= 9
		SET @EventType		= 5
		SET @ApptType		= 12	--Changed from MDS FastTrack Shift to MDS Shift
		SET @getDATE		= GETDATE()
		SET @Today			= @getDATE
		SET @SixMonthsOut	= DATEADD(month,6,@Today)
		SET @FTDay			= CONVERT(DATETIME, CONVERT(VARCHAR, DATEADD(day, -1, DATEADD(month,1,@Today)), 101) + ' 00:00:00')
		
		
		DECLARE @bDetermineFT BIT
		SET @bDetermineFT = 0
		WHILE(@bDetermineFT = 0)
		BEGIN
			IF (DATENAME(dw, @FTDay) = 'Sunday')
				SET @FTDay = CONVERT(DATETIME, CONVERT(VARCHAR, DATEADD(day, -2, @FTDay), 101) + ' 00:00:00')
			ELSE IF (DATENAME(dw, @FTDay) = 'Saturday')
				SET @FTDay = CONVERT(DATETIME, CONVERT(VARCHAR, DATEADD(day, -1, @FTDay), 101) + ' 00:00:00')

			IF NOT EXISTS (SELECT 'x'
					   FROM dbo.LK_SPRINT_HLDY with (nolock)
					   WHERE SPRINT_HLDY_DT = @FTDay)
				SET @bDetermineFT = 1
			ELSE
			BEGIN
				SET @FTDay = CONVERT(DATETIME, CONVERT(VARCHAR, DATEADD(day, -1, @FTDay), 101) + ' 00:00:00')
			END
		END

		--Pre-fetch time slots instead of dipping into table for every occurence 
		INSERT INTO @TimeSlots
		SELECT TME_SLOT_ID, TME_SLOT_STRT_TME, TME_SLOT_END_TME
		FROM dbo.LK_EVENT_TYPE_TME_SLOT WITH (NOLOCK)
		WHERE EVENT_TYPE_ID = @EventType

		--Set up 6 months of days
		WHILE @Today <= @SixMonthsOut
		BEGIN
			INSERT INTO @Days
			SELECT @Today
			
			SET @Today = DATEADD(day, 1, @Today)
		END

		--Do a cross join to map out all day/timeslot combinations instead of iterating through
		INSERT INTO @EventAvl (Day, timeslot, StartTime, EndTime, tsTaken, ListTxt)
		SELECT IntDay, TimeSlotID, StartTime, EndTime, 0, NULL
		FROM		@TimeSlots	
		INNER JOIN	@Days			ON 1=1

		UPDATE ea
		SET ea.tsMaxResources = e.MAX_AVAL_TME_SLOT_CAP_CNT,
			ea.ListTxt	=	e.EVENT_ID_LIST_TXT
		FROM @EventAvl ea 
		INNER JOIN  dbo.EVENT_TYPE_RSRC_AVLBLTY e WITH (NOLOCK) ON ea.timeslot = e.TME_SLOT_ID AND ea.[Day] = e.[DAY_DT]
		WHERE e.EVENT_TYPE_ID = 5

		DECLARE @slotCaptCnt Table (matscc smallint, ts smallint, dp tinyint, daydt datetime)
		insert into @slotCaptCnt 
		Select MAX_AVAL_TME_SLOT_CAP_CNT,TME_SLOT_ID,DatePart(WEEKDAY,et.DAY_DT),et.DAY_DT
			From dbo.EVENT_TYPE_RSRC_AVLBLTY et WITH (NOLOCK)
			Where et.DAY_DT BETWEEN @FTDay - 5 AND @FTDay + 1
				AND et.EVENT_TYPE_ID = 5
			ORDER BY et.DAY_DT

		Update e
			Set	e.tsMaxResources = t.matscc
			From	@EventAvl e 
		Inner Join @slotCaptCnt t ON DATEPART(weekday,e.Day) = t.dp AND e.timeslot = t.ts
			WHERE	e.Day > @FTDay + 1
			
		--SELECT @t4 = (SELECT e.MAX_AVAL_TME_SLOT_CAP_CNT
		--					FROM dbo.EVENT_TYPE_RSRC_AVLBLTY e WITH (NOLOCK)
		--					WHERE e.DAY_DT = @FTDay
		--					  AND e.EVENT_TYPE_ID = 5
		--					  AND e.TME_SLOT_ID = 4),
		--		@t3 = (SELECT e.MAX_AVAL_TME_SLOT_CAP_CNT
		--					FROM dbo.EVENT_TYPE_RSRC_AVLBLTY e WITH (NOLOCK)
		--					WHERE e.DAY_DT = @FTDay
		--					  AND e.EVENT_TYPE_ID = 5
		--					  AND e.TME_SLOT_ID = 3),
		--		@t2 = (SELECT e.MAX_AVAL_TME_SLOT_CAP_CNT
		--					FROM dbo.EVENT_TYPE_RSRC_AVLBLTY e WITH (NOLOCK)
		--					WHERE e.DAY_DT = @FTDay
		--					  AND e.EVENT_TYPE_ID = 5
		--					  AND e.TME_SLOT_ID = 2),
		--		@t1 = (SELECT e.MAX_AVAL_TME_SLOT_CAP_CNT
		--					FROM dbo.EVENT_TYPE_RSRC_AVLBLTY e WITH (NOLOCK)
		--					WHERE e.DAY_DT = @FTDay
		--					  AND e.EVENT_TYPE_ID = 5
		--					  AND e.TME_SLOT_ID = 1)
		
		
		--UPDATE @EventAvl
		--SET tsMaxResources = CASE timeslot WHEN 1 THEN @t1
		--								   WHEN 2 THEN @t2
		--								   WHEN 3 THEN @t3
		--								   WHEN 4 THEN @t4
		--								   END
		--WHERE [Day] > @FTDay

		UPDATE @EventAvl
		SET tsMaxResources = 0
		WHERE tsMaxResources IS NULL


		--Get all Shift appointments that are non-recurring
		INSERT INTO @Appt
		SELECT STRT_TMST, END_TMST, APPT_TYPE_ID, CONVERT(INT, SUBSTRING(ASN_TO_USER_ID_LIST_TXT, CHARINDEX('Value="',ASN_TO_USER_ID_LIST_TXT) + 7, (CHARINDEX('" />',ASN_TO_USER_ID_LIST_TXT) - (CHARINDEX('Value="',ASN_TO_USER_ID_LIST_TXT)+7))))
		FROM	dbo.APPT	a	WITH (NOLOCK)
		WHERE	END_TMST		> @getDATE
			AND	STRT_TMST		< @SixMonthsOut
			AND RCURNC_CD		= 0
			AND APPT_TYPE_ID	= @ApptType
			--AND 0=1	--Uncomment this to see only recurring appointments while testing

		INSERT INTO @Appt
		SELECT STRT_TMST, END_TMST, APPT_TYPE_ID, ASN_USER_ID
		FROM	dbo.APPT_RCURNC_DATA	WITH (NOLOCK)
		WHERE	APPT_TYPE_ID	= @ApptType

		
		--Get which intervals are covered by which appointments
		INSERT INTO @mapEventAppt
		SELECT		e.EventIntervalID,
					a.ApptID,
					(CASE WHEN (a.StartTime <= e.Day + convert(smalldatetime,e.StartTime)) THEN e.StartTime ELSE a.StartTime END),
					(CASE WHEN (a.EndTime <= e.Day + convert(smalldatetime,e.EndTime)) THEN a.EndTime ELSE e.EndTime END)
		FROM			@Appt a 
			INNER JOIN	@EventAvl	e 			ON	a.StartTime < (e.Day + convert(smalldatetime,e.EndTime)) --Count if it starts before the end time
												AND	a.EndTime > (e.Day + convert(smalldatetime,e.StartTime))	 --and after the start time


		/*****************************************************
		Extract Users for each Calendar Appointment Start
		*****************************************************/


		INSERT INTO @EventAvlUser
		SELECT	DISTINCT	m.ApptID,
							m.EventIntervalID,
							a.UserID,
							m.ShiftStart,
							m.ShiftEnd,
							0,
							0
		FROM	@mapEventAppt	m	
		INNER JOIN @Appt	a	ON	m.ApptID	=	a.ApptID
		WHERE	NOT EXISTS (SELECT 'X' FROM @EventAvlUser e WHERE e.UserID = a.UserID AND e.EventIntervalID = m.EventIntervalID AND e.ShiftStart = m.ShiftStart AND e.ShiftEnd = m.ShiftEnd)
	
		SET @cntr = 1
		--Merge Time Slots for users with multiple shifts in time block (i.e. 6-7 and 8-10)
		WHILE (@cntr <> (SELECT DISTINCT COUNT(1) FROM @EventAvlUser))
		BEGIN
			
			SET @cntr = (SELECT DISTINCT COUNT(1) FROM @EventAvlUser)
			
			--If e1 starts before or same as e2, and e1 ends after e2 starts, and e2 ends after e1, then merge the end times.
			UPDATE	e1
			SET		e1.ShiftEnd	=	e2.ShiftEnd
			FROM			@EventAvlUser e1
				INNER JOIN	@EventAvlUser e2	ON	e1.EventIntervalID	= e2.EventIntervalID
												AND	e1.UserID			= e2.UserID
												AND e1.ShiftEnd			>= e2.ShiftStart
												AND	e1.ShiftStart		<= e2.ShiftStart
												AND e1.ShiftEnd			< e2.ShiftEnd
			--if e1 ends after  or same as e2, and e1 starts before e2 ends, and e2 starts before e1, then merge the start times.
			UPDATE	e1
			SET		e1.ShiftStart	=	e2.ShiftStart
			FROM			@EventAvlUser e1
				INNER JOIN	@EventAvlUser e2	ON	e1.EventIntervalID	= e2.EventIntervalID
												AND	e1.UserID			= e2.UserID
												AND e1.ShiftEnd			>= e2.ShiftEnd
												AND	e1.ShiftStart		<= e2.ShiftEnd
												AND e1.ShiftStart		> e2.ShiftStart
												
			UPDATE	e1
			SET		e1.toDelete = 1
			FROM			@EventAvlUser e1
				INNER JOIN	@EventAvlUser e2	ON	e1.EventIntervalID	=	e2.EventIntervalID
												AND	e1.UserID			=	e2.UserID
												AND e1.ShiftStart		=	e2.ShiftStart
												AND	e1.ShiftEnd			<	e2.ShiftEnd
												AND e1.ID				>	e2.ID
				
			UPDATE	e1
			SET		e1.toDelete = 1
			FROM			@EventAvlUser e1
				INNER JOIN	@EventAvlUser e2	ON	e1.EventIntervalID	=	e2.EventIntervalID
												AND	e1.UserID			=	e2.UserID
												AND e1.ShiftStart		>	e2.ShiftStart
												AND	e1.ShiftEnd			=	e2.ShiftEnd
												AND e1.ID				>	e2.ID
												
			UPDATE	e1
			SET		e1.toDelete = 1
			FROM			@EventAvlUser e1
				INNER JOIN	@EventAvlUser e2	ON	e1.EventIntervalID	=	e2.EventIntervalID
												AND	e1.UserID			=	e2.UserID
												AND e1.ShiftStart		=	e2.ShiftStart
												AND	e1.ShiftEnd			=	e2.ShiftEnd
												AND e1.ID				>	e2.ID	
														
			DELETE FROM @EventAvlUser WHERE toDelete = 1					
		END
		
		UPDATE @EventAvlUser SET shiftInterval = DATEDIFF(HOUR, ShiftStart, ShiftEnd)
		/*****************************************************
		Extract Users for each Calendar Appointment End
		*****************************************************/

		/*****************************************************
		Update totals Start
		*****************************************************/
		/*UPDATE ea 
		SET tsOnCalendar = ISNULL(eau2.CNT, 0)
		FROM @EventAvl ea
			LEFT JOIN (SELECT EventIntervalID, SUM(shiftInterval) AS CNT FROM @EventAvlUser eau GROUP BY eau.EventIntervalID) eau2 ON ea.EventIntervalID = eau2.EventIntervalID
--Divide by 200 because average event is 2 hours long, and 100 is from conversion to %
		UPDATE ea
		SET tsMaxResources = CEILING((tsOnCalendar * l.AVLBLTY_CAP_PCT_QTY)/200.0)
		FROM @EventAvl ea
			INNER JOIN dbo.LK_EVENT_TYPE_TME_SLOT l WITH (NOLOCK) ON l.TME_SLOT_ID = ea.timeslot*/

		UPDATE @EventAvl
		SET	tsTaken =  COALESCE(ListTxt.value('count(/EventIDs/Active/EventID)', 'int'), 0)
		--FROM @EventAvl ea
			--INNER JOIN (
			--			SELECT EVENT_TYPE_ID, STRT_TMST, TME_SLOT_ID, COUNT(EVENT_ID) AS CNT 
			--			FROM dbo.MDS_EVENT_NEW WITH (NOLOCK) 
			--			WHERE MDS_FAST_TRK_TYPE_ID = 'S' and MDS_FAST_TRK_CD = 1 
			--			AND TME_SLOT_ID IS NOT NULL AND
			--			((EVENT_STUS_ID NOT IN (1, 3, 8, 11, 12, 13)) OR ((EVENT_STUS_ID=3) AND (WRKFLW_STUS_ID IN (3,5,8))))
			--			GROUP BY EVENT_TYPE_ID, STRT_TMST, TME_SLOT_ID
			--			) ev ON		ev.TME_SLOT_ID	=	ea.timeslot
			--					AND	ev.STRT_TMST	=	ea.Day					
		/*****************************************************
		Update totals End
		*****************************************************/
		/*****************************************************
		Delete Weekends tweak - 
		*****************************************************/
		
		--Delete FROM @EventAvl WHERE DATEPART(weekday, Day) IN (6,7)
		
		/*****************************************************
		Delete Weekends tweak - End
		*****************************************************/


		/*****************************************************
		Finish by inserting into main tracking table
		*****************************************************/
		-- Copy the MAX_AVAL_TME_SLOT_CAP_CNT into a temp table.
		/*IF	OBJECT_ID(N'tempdb..#TempResources', N'U') IS NOT NULL 
			DROP TABLE #TempResources			
		SELECT 		EVENT_TYPE_ID
					,DAY_DT
					,TME_SLOT_ID
					,MAX_AVAL_TME_SLOT_CAP_CNT
					,MODFD_BY_USER_ID
					,MODFD_DT
			INTO	#TempResources
			FROM	dbo.EVENT_TYPE_RSRC_AVLBLTY	WITH (NOLOCK)*/
		
		-- Delete and then Reload this data based off the old logic.			
		DELETE FROM dbo.EVENT_TYPE_RSRC_AVLBLTY WITH (ROWLOCK) WHERE EVENT_TYPE_ID = @EventType
		INSERT INTO dbo.EVENT_TYPE_RSRC_AVLBLTY
				(EVENT_TYPE_RSRC_AVLBLTY_ID,
				EVENT_TYPE_ID,
				DAY_DT,
				TME_SLOT_ID,
				AVAL_TME_SLOT_CNT,
				MAX_AVAL_TME_SLOT_CAP_CNT,
				CURR_AVAL_TME_SLOT_CNT,
				CREAT_DT,
				EVENT_ID_LIST_TXT)
		SELECT	EventIntervalID,
				@EventType,
				Day,
				timeslot,
				0,
				tsMaxResources,
				tsTaken,
				GETDATE(),
				ListTxt	
		FROM @EventAvl	
		
		--Determine the next month dates
		/*SET @DT = @getDATE
		--SELECT 'Run Date', DATENAME (DW, @DT), @DT
		PRINT ('Running On: ' + CONVERT(VARCHAR, @DT))

		select  'Add Month', DATENAME(dw, DATEADD(MM, 1, @DT)), DATEADD(MM, 1, @DT)

		--Check to see if next month's date is a weekend
		/*Leaving the select statements in there for debugging in the short term*/
		
		SELECT @dtval = datepart(dw, DATEADD(MM, 1, @DT))
		--SELECT DATENAME(dw, DATEADD(MM, 1, @DT))
		IF (@dtval  = 7) OR (@dtval  = 6)
		BEGIN
			--SELECT 'Next month is weekend'
			IF (@dtval  = 7)
			BEGIN
				SET @CurrDT = DATEADD(MM, 1, @DT) - 2
				--SELECT DATENAME(dw,  DATEADD(MM, 1, @DT) - 2)
				--SELECT 'From Slot Date', DATENAME(dw,  DATEADD(MM, 1, @DT) - 2), @CurrDT 
				
				SET @NextMonthDT = DATEADD(MM, 1, @DT) + 1
				--SELECT DATENAME(dw, DATEADD(MM, 1, @DT) + 1)
				--SELECT 'To Month Date', DATENAME(dw,  DATEADD(MM, 1, @DT) + 1), @NextMonthDT 
			END
			ELSE 
			IF (@dtval  = 6)
			BEGIN
				SET @CurrDT  =  DATEADD(MM, 1, @DT) - 1
				--SELECT DATENAME(dw,  DATEADD(MM, 1, @DT) - 1)
				--SELECT 'From Slot Date', DATENAME(dw,  DATEADD(MM, 1, @DT) - 1), @CurrDT  
				
				SET @NextMonthDT = DATEADD(MM, 1, @DT) + 2
				--SELECT DATENAME(dw, DATEADD(MM, 1, @DT) + 2)
				--SELECT 'To Month Date', DATENAME(dw,  DATEADD(MM, 1, @DT) +2), @NextMonthDT 
			END
		END
		ELSE
			IF (@dtval = 5)
			BEGIN
				SET @CurrDT = DATEADD(MM, 1, @DT) 
				--SELECT DATENAME(dw, DATEADD(MM, 1, @DT))
				--SELECT 'From Slot Date', DATENAME(dw, DATEADD(MM, 1, @DT)), @CurrDT 
				
				SET @NextMonthDT = DATEADD(MM, 1, @DT) + 3 
				--SELECT DATENAME(dw, DATEADD(MM, 1, @DT))
				--SELECT 'To Month Date', DATENAME(dw, DATEADD(MM, 1, @DT) + 3), @NextMonthDT 
			END
		ELSE
		BEGIN
				SET @CurrDT = DATEADD(MM, 1, @DT) 
				--SELECT DATENAME(dw, DATEADD(MM, 1, @DT))
				--SELECT 'From Slot Date', DATENAME(dw, DATEADD(MM, 1, @DT)), @CurrDT 
				
				SET @NextMonthDT = DATEADD(MM, 1, @DT) + 1
				--SELECT DATENAME(dw, DATEADD(MM, 1, @DT))
				--SELECT 'To Month Date', DATENAME(dw, DATEADD(MM, 1, @DT) + 1), @NextMonthDT 
		END		

		-- Update the MAX_AVAL_TME_SLOT_CAP_CNT	with the previous version.
		UPDATE			et
			SET			et.MAX_AVAL_TME_SLOT_CAP_CNT	=	vt.MAX_AVAL_TME_SLOT_CAP_CNT
						,et.MODFD_BY_USER_ID			=	vt.MODFD_BY_USER_ID
						,et.MODFD_DT					=	vt.MODFD_DT
			FROM		dbo.EVENT_TYPE_RSRC_AVLBLTY	et	WITH (ROWLOCK)
			INNER JOIN	#TempResources				vt	ON	et.EVENT_TYPE_ID	=	vt.EVENT_TYPE_ID
														AND	et.TME_SLOT_ID		=	vt.TME_SLOT_ID
														AND	et.DAY_DT			=	vt.DAY_DT
		
		UPDATE			et1
			SET			et1.MAX_AVAL_TME_SLOT_CAP_CNT	=	et2.MAX_AVAL_TME_SLOT_CAP_CNT
						,et1.MODFD_BY_USER_ID			=	et2.MODFD_BY_USER_ID
						,et1.MODFD_DT					=	et2.MODFD_DT
			FROM		dbo.EVENT_TYPE_RSRC_AVLBLTY	et1	WITH (ROWLOCK)
			INNER JOIN	dbo.EVENT_TYPE_RSRC_AVLBLTY	et2	WITH (NOLOCK)	ON	et1.EVENT_TYPE_ID	=	et1.EVENT_TYPE_ID
																		AND	et1.TME_SLOT_ID		=	et2.TME_SLOT_ID
			WHERE	et1.EVENT_TYPE_ID = @EventType	
				AND	CONVERT(varchar(10), et1.DAY_DT, 101) >= CONVERT (varchar(10), @NextMonthDT, 101)
				AND	CONVERT(varchar(10), et2.DAY_DT, 101) = CONVERT (varchar(10), @CurrDT, 101)*/
																		
										
		--INSERTING INTO dbo.EVENT_AVAL_USER table to be used by for picking a userid for mds fast track
		INSERT INTO dbo.EVENT_AVAL_USER  
		(
			TME_SLOT_ID,
			[USER_ID],
			TME_SLOT_STRT_TME,
			TME_SLOT_END_TME,
			TME_SLOT_DRTN_IN_MIN_QTY
		)
		SELECT DISTINCT
			let.TME_SLOT_ID,
			eau.UserID,
			eau.ShiftStart,
			eau.ShiftEnd,
			240	
		FROM @EventAvlUser eau
		INNER JOIN dbo.LK_EVENT_TYPE_TME_SLOT let WITH (NOLOCK) ON convert(varchar,let.TME_SLOT_STRT_TME,108) = convert(varchar,eau.ShiftStart,108)



		/*Fedline Start...
		Similar rules apply for setting the max resource count, instead of thirty days out it is 42 days out.  
		*/
		SET @Today = @getDATE
		SET @deEndByDate = DATEADD(day, 50, @getDATE)
		SELECT @StartSeed = MAX(EVENT_TYPE_RSRC_AVLBLTY_ID) FROM dbo.EVENT_TYPE_RSRC_AVLBLTY WITH (NOLOCK)
		
		INSERT INTO @FedTS
		SELECT	TME_SLOT_ID
		FROM	dbo.LK_EVENT_TYPE_TME_SLOT WITH (NOLOCK)
		WHERE	EVENT_TYPE_ID	=	@FedEventType


		WHILE @Today < @deEndByDate
		BEGIN
			SELECT @loopSize = COUNT(1) FROM @FedTS
			SET @cntr	=	1
			WHILE @cntr <= @loopSize
			BEGIN
				SELECT @tsID = tsID FROM @FedTS where idx = @cntr
				IF NOT EXISTS  (SELECT TOP 1 'x' 
								FROM dbo.EVENT_TYPE_RSRC_AVLBLTY WITH (NOLOCK)
								WHERE	EVENT_TYPE_ID = 9 
									AND DAY_DT = @Today 
									AND TME_SLOT_ID = @tsID)
				BEGIN
					INSERT INTO dbo.EVENT_TYPE_RSRC_AVLBLTY 
					(
						EVENT_TYPE_RSRC_AVLBLTY_ID,
						EVENT_TYPE_ID,
						DAY_DT,
						TME_SLOT_ID,
						AVAL_TME_SLOT_CNT,
						MAX_AVAL_TME_SLOT_CAP_CNT,
						CURR_AVAL_TME_SLOT_CNT,
						CREAT_DT
					)
					VALUES
					(
						CASE WHEN @StartSeed < 5000 THEN 5000 ELSE (SELECT MAX(EVENT_TYPE_RSRC_AVLBLTY_ID) + 1 FROM EVENT_TYPE_RSRC_AVLBLTY) END,
						@FedEventType,
						@Today,
						@tsID,
						0,
						0,
						0,
						@getDATE
					)
					
					SET @StartSeed = 5000
					
				END	--End Not exists
				SET @cntr = @cntr + 1
			END	--End Time Slot
			SET @Today = DATEADD(DAY,1,@Today)
		END	--End 50 day out loop
		
		--Copy the last day from Calendar out to the next days values....
		SET @DT = @getDATE
		SET @deEndByDate = DATEADD(DAYOFYEAR, 42, @DT)
		SELECT @dtval = datepart(dw, @deEndByDate)
--SELECT DATENAME(dw, @deEndByDate)
		IF (DATENAME(dw, @deEndByDate) = 'Sunday') or (DATENAME(dw, @deEndByDate) = 'Saturday')
			or (DATENAME(dw, @deEndByDate) = 'Friday')
		  BEGIN
			IF (DATENAME(dw, @deEndByDate) = 'Sunday')
			  BEGIN
				SET @CurrDT = DateAdd(DAY, -2, @deEndByDate)
				SET @NextMonthDT = DATEADD(DAY, 1, @deEndByDate)
			  END
			ELSE 
				IF (DATENAME(dw, @deEndByDate) = 'Saturday')
    				BEGIN
						SET @CurrDT  =  DateAdd(DAY, -1, @deEndByDate)
						SET @NextMonthDT = DATEADD(DAY, 2, @deEndByDate)
					END
				ELSE
					BEGIN
						SET @CurrDT = @deEndByDate
						SET @NextMonthDT = DATEADD(DAY, 3, @deEndByDate)
					END
				
		  END
		ELSE
	     BEGIN
				SET @CurrDT = @deEndByDate
				SET @NextMonthDT = DATEADD(DAY, 1, @deEndByDate)
	     END
SELECT @CurrDT, @NextMonthDT

		--And copy the data from the last web page day to the next one to appear
		UPDATE	et1
		SET		et1.MAX_AVAL_TME_SLOT_CAP_CNT	=	et2.MAX_AVAL_TME_SLOT_CAP_CNT,
				et1.MODFD_BY_USER_ID			=	1,
				et1.MODFD_DT					=	GETDATE()
		FROM		dbo.EVENT_TYPE_RSRC_AVLBLTY	et1	WITH (ROWLOCK)
		INNER JOIN	dbo.EVENT_TYPE_RSRC_AVLBLTY	et2	WITH (NOLOCK)	ON	et1.EVENT_TYPE_ID	=	et1.EVENT_TYPE_ID
																	AND	et1.TME_SLOT_ID		=	et2.TME_SLOT_ID
		WHERE	et1.EVENT_TYPE_ID = @FedEventType	
			AND	et1.DAY_DT = @NextMonthDT
			AND	et2.DAY_DT = @CurrDT
					

	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	
END
