USE [COWS]
GO
_CreateObject 'SP','dbo','AccessCompleteMsgToMach5'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <04/27/2015,>
-- Description:	<To send Access Complete 
--	milestone completionMessage back to Mach5>
-- =============================================
Alter PROCEDURE dbo.AccessCompleteMsgToMach5
	@OrderID Int,
	@TaskID smallint,
	@TaskStatus tinyint,
	@Comments varchar(1000)	
AS
BEGIN
	SET NOCOUNT ON;

    IF Exists(Select 'X' From dbo.ORDR with (nolock) Where ORDR_ID = @orderID AND ORDR_CAT_ID = 6)
		begin
			Insert into M5_ORDR_MSG (ORDR_ID,M5_MSG_ID,STUS_ID,CREAT_DT)
			Values (@OrderID,3, 10, GETDATE())
		end
END
GO
