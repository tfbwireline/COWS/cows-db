USE [COWS]
GO
_CreateObject 'SP','web','RetrieveByFTN'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Naidu Vattigunta
-- Create date:  09/12/2011
--Modified by chakri for defect  #499976 
-- Description:	<This Stored Procedure is used to get Customer information   including CSG Levels>
-- sbg9814	01/16/2011	Pull up Non-Site Info from Hierarchy Contact info.
-- kh946640 03/22/18 COWS DB Restructuring as columns have been moved from fsa_ordr->fsa_ordr_cpe_line_item table for DualPort project(PJ020783 - CR93)
-- =============================================
ALTER PROCEDURE [web].[RetrieveByFTN]  
	@FTN varchar(20)
AS
BEGIN
BEGIN TRY
	OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SELECT TOP 1	fo.FTN											AS FTN,
					fc1.CUST_ID										AS H1, 
					ISNULL(od.CHARS_ID,ISNULL(fo.CHARS_ID,''))		AS CHARSID,
					ISNULL(fc6.CUST_ID,0)							AS H6,
					CASE WHEN (od.CSG_LVL_ID=0) THEN ISNULL(fc1.CUST_NME,'') ELSE ISNULL(dbo.decryptBinaryData(csdfc1.CUST_NME),'')	END AS	CustomerName,
					CASE WHEN (od.CSG_LVL_ID=0) THEN  CASE WHEN ISNULL(ocs.CNTCT_NME,'') = '' THEN ISNULL(ocs.FRST_NME,'') + ' ' + ISNULL(ocs.LST_NME,'')
						ELSE ocs.CNTCT_NME END
						ELSE CASE WHEN ISNULL(dbo.decryptBinaryData(csdocs.CUST_CNTCT_NME),'') = '' THEN ISNULL(dbo.decryptBinaryData(csdocs.FRST_NME),'') + ' ' + ISNULL(dbo.decryptBinaryData(csdocs.LST_NME),'') 
						ELSE dbo.decryptBinaryData(csdocs.CUST_CNTCT_NME) END
					END												AS CustomerContactName,
					CASE WHEN (od.CSG_LVL_ID=0) THEN ISNULL(ocs.EMAIL_ADR,'') ELSE ISNULL(dbo.decryptBinaryData(csdocs.CUST_EMAIL_ADR),'') END	AS	CustomerEmail,
					ISNULL(ocs.PHN_NBR,'')							AS CustomerContactPhone, 
					CASE ISNULL(lp.MDS_CD,'0') 
						WHEN '1' THEN 'Y' 
						ELSE 'N' 
					END												AS	MDSManaged, 	
	    			CASE ISNULL(fo.INSTL_ESCL_CD,'N')
	    				WHEN 'Y' THEN 1 
	    				ELSE 0 
	    			END												AS	IsEscalation,	 
		 			ISNULL(fo.CPW_TYPE,'')							AS	CARRIER_WHOLESALE_TYPE , 			 
					ISNULL(CPED.INSTL_DSGN_DOC_NBR,'')				AS	DesignDocumentApprovalNumber,
					ISNULL(CPED.TPORT_IP_VER_TYPE_CD,'')				AS	IP_VERSION,
					od.CSG_LVL_ID
	 	FROM		dbo.ORDR			od	WITH (NOLOCK) 
		INNER JOIN	dbo.FSA_ORDR		fo	WITH (NOLOCK)	ON	od.ORDR_ID			=	fo.ORDR_ID
		LEFT JOIN	dbo.FSA_ORDR_CUST	fc1 WITH (NOLOCK)	ON	od.ORDR_ID			=	fc1.ORDR_ID	
															AND	fc1.CIS_LVL_TYPE	=	'H1'
		LEFT JOIN	dbo.FSA_ORDR_CUST	fc6 WITH (NOLOCK)	ON	od.ORDR_ID			=	fc6.ORDR_ID	
															AND	fc6.CIS_LVL_TYPE	IN ('H5', 'H6')
		LEFT JOIN	dbo.ORDR_CNTCT		ocs	WITH (NOLOCK)	ON	fo.ORDR_ID			=	ocs.ORDR_ID
															AND	ocs.CIS_LVL_TYPE	IN	('H5', 'H6')
															AND	ISNULL(ocs.ROLE_ID,0)	!=	17
		LEFT JOIN   dbo.CUST_SCRD_DATA  csdfc1 WITH (NOLOCK) ON csdfc1.SCRD_OBJ_ID=fc1.FSA_ORDR_CUST_ID AND csdfc1.SCRD_OBJ_TYPE_ID=5
		LEFT JOIN   dbo.CUST_SCRD_DATA  csdocs WITH (NOLOCK) ON csdocs.SCRD_OBJ_ID=ocs.ORDR_CNTCT_ID AND csdocs.SCRD_OBJ_TYPE_ID=15
		LEFT JOIN	dbo.LK_PPRT			lp	WITH (NOLOCK)	ON	lp.PPRT_ID			=	od.PPRT_ID
		LEFT JOIN	dbo.FSA_ORDR_VAS	fov	WITH (NOLOCK)	ON	od.ORDR_ID			=	fov.ORDR_ID
		LEFT JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM CPED WITH (NOLOCK) ON od.ORDR_ID= CPED.ORDR_ID	
		WHERE		fo.FTN			=	@FTN  
					
		SELECT DISTINCT FMS_CKT_ID as Circuit_ID,
						FTN as FTN
		FROM dbo.NRM_CKT WITH (NOLOCK) 
		WHERE FTN			=	@FTN
		  AND isnull(FMS_CKT_ID,'') <> ''
END TRY
		
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH	
	
END
