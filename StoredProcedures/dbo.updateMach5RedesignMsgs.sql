USE [COWS]
GO
_CreateObject 'SP','dbo','updateMach5RedesignMsgs'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		jrg7298
-- Create date: 10/30/2016
-- Description:	SP to update status of MACH5 responses for redesign charges
-- =========================================================
ALTER PROCEDURE [dbo].[updateMach5RedesignMsgs]
	@TRAN_ID		Int
	,@STUS_ID		Int
	,@ERRMSG			VARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

Begin Try

	UPDATE		dbo.M5_REDSGN_MSG	WITH (ROWLOCK)
	SET		STUS_ID		=	@STUS_ID, SENT_DT = GETDATE(), ERR_MSG=CASE WHEN LEN(@ERRMSG)>0 THEN @ERRMSG ELSE NULL END
	WHERE	M5_REDSGN_MSG_ID		=	@TRAN_ID
	  AND  STUS_ID=10
	  

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch

END

