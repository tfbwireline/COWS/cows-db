USE [COWS]
GO
_CreateObject 'SP','dbo','CheckMDSDiscoFTNStatus'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================  
-- Author:  Suman S Chitemella
-- Create date: 12/13/2011  
-- Description: Checks the Status of a Disco FTN and checks to see if it is in a cancelled state.  
-- =========================================================  
ALTER PROCEDURE [dbo].[CheckMDSDiscoFTNStatus]  
 @FTN  varchar(20)  
 ,@RetVal Int  OUTPUT  
AS  
BEGIN  
SET NOCOUNT ON;  
Begin Try  
DECLARE @OrderID Int  
SET @OrderID = 0  
  

   
 IF EXISTS  
  (SELECT 'X' FROM dbo.FSA_ORDR WITH (NOLOCK)  
		WHERE  PRNT_FTN = @FTN
				AND ORDR_TYPE_CD = 'CN'
  )  
  BEGIN  
   SET @RetVal = -1
  END  
 ELSE  
  BEGIN  
   SET @RetVal = 0  
  END  
End Try  
  
Begin Catch  
 EXEC [dbo].[insertErrorInfo]  
End Catch  
END  