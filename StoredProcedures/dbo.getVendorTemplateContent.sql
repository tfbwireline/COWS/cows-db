﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getVendorTemplateContent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 12th July 2011
-- Description:	Returns content of Vendor Template
-- ================================================================

ALTER PROCEDURE [dbo].getVendorTemplateContent
	@VendorTemplateID INT 	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		[FILE_CNTNT]
	FROM 
		[dbo].[VNDR_TMPLT] WITH (NOLOCK)
	WHERE 
		[VNDR_TMPLT_ID] = @VendorTemplateID
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
