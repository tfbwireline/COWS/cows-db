USE [COWS]
GO
_CreateObject 'SP','dbo','checkMDSSlotAvailability'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================================================
-- Author:		sbg9814
-- Create date: 09/27/2011
-- Description:	Checks to see if a given TimeSlot is available.
-- =============================================================================
ALTER PROCEDURE [dbo].[checkMDSSlotAvailability]
	@TME_SLOT_ID	Int
	,@DAY_DT		DateTime
	,@ID			Int	OUTPUT
	,@EVENT_ID		Int	=	0
AS
BEGIN
SET NOCOUNT ON;
Begin Try
DECLARE	@Date	Varchar(20)
SET	@Date	=	Convert(Varchar, @DAY_DT,	101)

SET @ID = 0

IF	NOT EXISTS
	(SELECT 'X'	FROM	dbo.LK_SPRINT_HLDY	WITH (NOLOCK)
		WHERE	Convert(Varchar, SPRINT_HLDY_DT,	101)	=	@Date)
	BEGIN		
		SELECT		@ID		=	ISNULL(EVENT_TYPE_RSRC_AVLBLTY_ID, 0)
			FROM	dbo.EVENT_TYPE_RSRC_AVLBLTY	WITH (NOLOCK)
			WHERE	TME_SLOT_ID							=	@TME_SLOT_ID
			AND		Convert(Varchar, DAY_DT,	101)	=	@Date
			AND		CURR_AVAL_TME_SLOT_CNT				<	MAX_AVAL_TME_SLOT_CAP_CNT	
			AND		EVENT_TYPE_ID						=	5
	END

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO
