USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertMach5TxnDetails]    Script Date: 1/18/2022 2:38:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		David Phillips
-- Create date: <12/02/2016>
-- Description:	<Insert Mach5 Change transaction order>
-- =============================================
--exec deletepartialfsaorder 13572
-- exec [dbo].[insertMach5TxnDetails] 639106, 0, 'PRCV', '', ''
ALTER PROCEDURE  [dbo].[insertMach5TxnDetails] 
 @M5_ORDR_ID	Int ,
@M5_RELT_ORDR_ID Int,
@ORDR_TYPE_CD Varchar(5) ,
@QDA_ORDR_ID varchar(max)OUTPUT, 
@DEVICE_ID Varchar(50) = ''
AS

BEGIN
DECLARE @ERRLog VARCHAR(MAX) = ''
DECLARE @TransName VARCHAR(MAX) = CONVERT(VARCHAR,@M5_ORDR_ID)+'_'+CONVERT(VARCHAR,ISNULL(@M5_RELT_ORDR_ID, '-'))+'_'+@ORDR_TYPE_CD+'_'+@DEVICE_ID
SET XACT_ABORT ON;
	DECLARE @ORDR_ID INT, @OrderCategoryID TinyInt = 6,	
			@FSA_ORDR_ID INT, @ORDR_ACTN_ID TINYINT = 2,
			@PROD_ID varchar(3),@ORDR_SUBTYPE_CD CHAR(2)
	DECLARE @Ctr Int = 0, @Cnt Int = 0, @CMPNT_TYPE_CD Varchar(4)
	DECLARE @tmpids table (tmpid int)
	DECLARE @DMSTC_CD BIT = '0'
	DECLARE @SQL NVARCHAR(MAX)
	DECLARE @H5_H6_CUST_ID Varchar(20)
	DECLARE @CHNG_ORDR_TYPE_FLG BIT = 0
	SET @SQL = ''
	SET @FSA_ORDR_ID = 0
	SET @QDA_ORDR_ID = ''
	IF @DEVICE_ID = ''
		SET @DEVICE_ID = 'null' 
		
	DECLARE @PPRT_ID INT
	DECLARE @INTL_FLG BIT
	DECLARE @CUST_ACCT_ID INT = 0
	DECLARE @M5_PROD_ID Varchar(5)
	DECLARE @CPE_DEVICE_ID VARCHAR(14)
	DECLARE @STUS_CD Varchar(5)
	DECLARE @Continue Varchar(1)
	
		
		
	SET NOCOUNT ON;
	BEGIN TRY	
	BEGIN TRANSACTION @TransName

	IF @ORDR_TYPE_CD in ('FCCN') -- Full Cancel of Transactions
		BEGIN
						
			INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('FCCN:'+ @TransName,'insertMach5TxnDetails', GETDATE())
			
			IF OBJECT_ID(N'tempdb..#MACH5_CHNG_CNCL',N'U') IS NOT NULL 
				DROP TABLE #MACH5_CHNG_CNCL

			CREATE TABLE #MACH5_CHNG_CNCL
				(
					M5_ORDR_ID		Int,
					CHNG_TXN_NBR	Varchar(20),
					FTN				Varchar(20)
					
				)
			IF OBJECT_ID(N'tempdb..#MACH5_CHNG_ORDR_IDs',N'U') IS NOT NULL 
				DROP TABLE #MACH5_CHNG_ORDR_IDs
			 CREATE TABLE #MACH5_CHNG_ORDR_IDs
				(
					ORDR_ID		Int,
					FTN			Varchar(20)
					
				)
				
				SET @SQL = 'SELECT * ' +
						' FROM OPENQUERY ' +
						' (M5,''Select 	c.CHNG_TXN_ID AS M5_ORDR_ID, c.CHNG_TXN_NBR,f.CHNG_TXN_NBR AS FTN ' +
						'From Mach5.V_V5U_CHNG_TXN c ' +
						'INNER JOIN Mach5.V_V5U_CHNG_TXN f on c.RLTD_CHNG_TXN_ID = f.CHNG_TXN_ID ' +														
						' Where c.CHNG_TXN_ID =' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ ''')'
		
			
			INSERT INTO #MACH5_CHNG_CNCL (M5_ORDR_ID, CHNG_TXN_NBR,FTN)									
			EXEC sp_executesql @SQL

			INSERT INTO #MACH5_CHNG_ORDR_IDs (ORDR_ID,FTN)
			SELECT  ORDR_ID, FTN  FROM [dbo].[FSA_ORDR] with (nolock) 
									where FTN in (SELECT TOP 1 FTN FROM #MACH5_CHNG_CNCL)

			DECLARE @CNCL_TXN_NBR Varchar(20), @RLTOrdrId int, @Note varchar(max)
						
			SELECT	@Cnt = COUNT(1) FROM #MACH5_CHNG_ORDR_IDs
									
			SET @Ctr = 0
								
			IF @Cnt > 0
					BEGIN
					   WHILE @Ctr < @Cnt 
							BEGIN
								SELECT TOP 1 @RLTOrdrId  = ORDR_ID  FROM #MACH5_CHNG_ORDR_IDs								
								
								SELECT TOP 1 @CNCL_TXN_NBR = CHNG_TXN_NBR FROM #MACH5_CHNG_CNCL
			
								SET @Note = 'COWS received Cancel Transaction from M5:' + @CNCL_TXN_NBR
			
								SELECT @PPRT_ID = PPRT_ID from dbo.LK_PPRT with (nolock)
									where ORDR_CAT_ID = 6 and ORDR_TYPE_ID = 8
									and INTL_CD in (SELECT DMSTC_CD from dbo.ORDR with (nolock)
													where ORDR_ID = @RLTOrdrId) 
									and PROD_TYPE_ID in (SELECT PROD_TYPE_ID from dbo.LK_PROD_TYPE pt with (nolock)
																		Inner join dbo.FSA_ORDR fo with (nolock)
																			on fo.PROD_TYPE_CD = pt.FSA_PROD_TYPE_CD
																where fo.ORDR_ID = @RLTOrdrId )

										UPDATE dbo.ORDR 
											SET ORDR_STUS_ID = 4, 
												PPRT_ID = @PPRT_ID,
												CPE_CLLI = null 
										WHERE ORDR_ID = @RLTOrdrId

										UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM	
											SET ITM_STUS = 402
										WHERE ORDR_ID = @RLTOrdrId
						

										EXEC	dbo.insertOrderNotes @RLTOrdrId,1,@Note, 1

										EXEC [dbo].[deleteActiveTasks] @RLTOrdrId

										DECLARE @WGPatternIDs		VarChar(MAX)

										SET  @WGPatternIds = (SELECT DISTINCT o.ORDR_ID AS OrderID, sm.DESRD_WG_PTRN_ID AS WGPatternID 
																	FROM	[dbo].ORDR   o WITH (NOLOCK) 
																			INNER JOIN [dbo].LK_PPRT lp WITH (NOLOCK) ON o.PPRT_ID		= lp.PPRT_ID
																			INNER JOIN [dbo].SM         sm WITH (NOLOCK) ON lp.SM_ID   = sm.SM_ID
																	WHERE
																		sm.PRE_REQST_WG_PTRN_ID  =  0
																	AND	o.ORDR_ID = @RLTOrdrId
																	FOR XML PATH('IDs'), ROOT('NextPatterns'))	

										EXEC [dbo].insertNextWGPattern @RLTOrdrId, @WGPatternIds

										DELETE #MACH5_CHNG_ORDR_IDs where ORDR_ID = @RLTOrdrId
														
								
								SET @Ctr = @Ctr + 1
								
								SET @QDA_ORDR_ID = @RLTOrdrId 						
							END
					END				
		END
	
	IF @ORDR_TYPE_CD IN ('ROCP','PRCV','ISMV','CO','ACRF','ACRN') 
		BEGIN
			IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG',N'U') IS NOT NULL 
				DROP TABLE #MACH5_ORDR_CHNG
						
			CREATE TABLE #MACH5_ORDR_CHNG
				(
					M5_ORDR_ID				INT,
					--QDA_ORDR_ID				INT,
					ORDER_NBR				VARCHAR(20),
					PRNT_ACCT_ID			INT,
					ORDR_TYPE_CD			VARCHAR(2),
					PROD_ID					VARCHAR(5),
					PROD_TYPE_CD			VARCHAR(2),
					ORDR_SUBTYPE_CD			VARCHAR(7),
					DOM_PROCESS_FLG_CD		VARCHAR(1),
					RLTD_ORDR_ID			INT,
					CUST_SIGN_DT			DATE,
					CUST_WANT_DT			DATE,
					CUST_SBMT_DT			DATE,
					CUST_CMMT_DT			DATE,
					EXP_FLG_CD				CHAR(1),
					EXP_TYPE_CD				VARCHAR(1),
					CUST_PREM_OCCU_FLG_CD	CHAR(1),
					CUST_ACPT_SRVC_FLG_CD	CHAR(1),
					MULT_ORDR_SBMTD_FLG_CD	CHAR(1),
					DSCNCT_REAS_CD			VARCHAR(2),
					PRE_QUAL_NBR			VARCHAR(20),
					COST_CNTR				VARCHAR(10),
					EXPEDITE_APRVR_NME		VARCHAR(511),
					APRVR_TITLE				VARCHAR(15),
					APRVR_PHN				VARCHAR(20),
					APPRVL_DT				DATE,
					PRNT_FTN				VARCHAR(20),
					CSG_LVL					VARCHAR(5),
					CHARS_CUST_ID			VARCHAR(20)
					
				)
						
			
						
			SET @SQL = 'SELECT * FROM OPENQUERY(M5,''SELECT DISTINCT d.CHNG_TXN_ID AS M5_ORDR_ID, CHNG_TXN_NBR AS ORDER_NBR, c.CUST_ACCT_ID AS PRNT_ACCT_ID,
												 ''''IN'''' AS ORDR_TYPE_CD, n.PROD_CD AS PROD_ID, 
												 null AS PROD_TYPE_CD, c.TYPE_CD AS ORDR_SUBTYPE_CD, 
												 c.DOM_PROCESS_FLG_CD, null AS RLTD_ORDR_ID, null AS CUST_SIGN_DT, null AS CUST_WANT_DT, null AS CUST_SBMT_DT, 
												 c.CUST_CMMT_DT, c.EXP_FLG_CD, c.EXP_TYPE_CD, null AS CUST_PREM_OCCU_FLG_CD, 
												 null AS CUST_ACPT_SRVC_FLG_CD, null AS MULT_ORDR_SBMTD_FLG_CD,null AS DSCNCT_REAS_CD, 
												 c.PRE_QUAL_NBR AS PRE_QUAL_NBR, null AS COST_CNTR, null AS EXPEDITE_APRVR_NME,null AS APRVR_TITLE, 
												 null AS APRVR_PHN, null AS APPRVL_DT, CHNG_TXN_NBR AS PRNT_FTN, a.CSG_LVL, a.CHARS_CUST_ID
											FROM  MACH5.V_V5U_CHNG_TXN c
											LEFT JOIN Mach5.V_V5U_CHNG_TXN_DETL d ON c.CHNG_TXN_ID = d.CHNG_TXN_ID
											LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID = d.NVTRY_ID
											LEFT JOIN MACH5.V_V5U_ORDR  o ON o.ORDR_ID = n.ORIG_ORDR_ID
											LEFT JOIN MACH5.V_V5U_CUST_ACCT a on c.CUST_ACCT_ID = a.CUST_ACCT_ID AND a.TYPE_CD = DECODE(a.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
											WHERE c.CHNG_TXN_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')'

			--SELECT @SQL
			INSERT INTO #MACH5_ORDR_CHNG
			EXEC sp_executesql 	@SQL			

			select * from #MACH5_ORDR_CHNG
			--DECLARE	@PROD_ID varchar(3), @ORDR_TYPE_CD Varchar(2)
			DECLARE	@PROD_TYPE_CD varchar(2)

			SELECT	@PROD_ID		= SUBSTRING(PROD_ID,1,3),
					@ORDR_SUBTYPE_CD = SUBSTRING(ORDR_SUBTYPE_CD,1,4)
				FROM #MACH5_ORDR_CHNG
				WHERE M5_ORDR_ID = @M5_ORDR_ID

			EXEC	[dbo].[getM5ProductTypeCD] @ORDR_TYPE_CD,@M5_ORDR_ID,@PROD_ID,@ORDR_SUBTYPE_CD,@PROD_TYPE_CD OUTPUT

					
			Update #MACH5_ORDR_CHNG
				SET PROD_TYPE_CD = @PROD_TYPE_CD
				
			Select * From #MACH5_ORDR_CHNG
		

			IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG_CMPNT',N'U') IS NOT NULL 
				DROP TABLE #MACH5_ORDR_CHNG_CMPNT

			CREATE TABLE #MACH5_ORDR_CHNG_CMPNT
				(
					M5_ORDR_ID		INT,
					QDA_ORDR_ID		INT,
					ORDR_CMPNT_ID	INT,
					CMPNT_TYPE_CD	VARCHAR(4),
					ONE_STOP_SHOP_FLG_CD	VARCHAR(1), --TTRPT_OSS_CD
		
			
					DESIGN_DOC_NUM			VARCHAR(20), --INSTL_DSGN_DOC_NBR
					RATE_TYPE_CD			VARCHAR(6), --PORT_RT_TYPE_CD
					PORT_SPD_DES			VARCHAR(50), --TTRPT_SPD_OF_SRVC_BDWD_DES
					NVTRY_ID				INT,
					CPE_ACCS_PRVDR_NME		VARCHAR(50),
					CPE_CONTRACT_TERM		INT,
					CPE_CONTRACT_TYPE		VARCHAR(6),
					CPE_DLVRY_DUTIES_AMT	VARCHAR(8),
					CPE_DLVRY_DUTIES_CD		VARCHAR(1),
					CPE_DROP_SHIP_FLG_CD	VARCHAR(1),
					CPE_EQPT_CKT_ID			VARCHAR(25),
					CPE_ITEM_ID				INT,
					CPE_MANAGED_BY			VARCHAR(10),
					CPE_MFR_ID				VARCHAR(4),
					CPE_MFR_NME				VARCHAR(255),
					CPE_MFR_PART_NBR		VARCHAR(255),
					CPE_MNGD_FLG_CD			VARCHAR(1),
					CPE_SPRINT_MNT_FLG_CD   CHAR(1),
					CPE_MSCP_CD				VARCHAR(15),
					CPE_LIST_PRICE			VARCHAR(14),
					CPE_REQD_ON_SITE_DT		DATE,
					CPE_SHIP_CLLI_CD		VARCHAR(11),
					CPE_SPRINT_TEST_TN_NBR	VARCHAR(10),
					CPE_DEVICE_ID			VARCHAR(14),
					EQPT_ID					VARCHAR(10),
					EQPT_TYPE_CD			VARCHAR(4),
					FLAG					BIT,
					DEVICE_ID				INT,
					QTY						INT,
					NME						VARCHAR(255),
					CPE_VNDR_DSCNT			VARCHAR(30),
					CPE_ITEM_MSTR_NBR		VARCHAR(6),
					CPE_RECORD_ONLY_FLG_CD	VARCHAR(1),
					CPE_CMPN_FMLY			VARCHAR(10),
					RLTD_CMPNT_ID			INT,
					CPE_ITEM_TYPE_CD		Varchar(3),	
					STUS_CD					Varchar(2),
					CPE_MFR_PS_ID			Varchar(30),
					CPE_ALT_SHIP_ADR			INT,
					CPE_ALT_SHIP_ADR_CTRY_CD	VARCHAR(2),
					CPE_ALT_SHIP_ADR_CTY_NME	VARCHAR(255),
					CPE_ALT_SHIP_ADR_LINE_1		VARCHAR(255),
					CPE_ALT_SHIP_ADR_LINE_2		VARCHAR(255),
					CPE_ALT_SHIP_ADR_LINE_3		VARCHAR(255),
					CPE_ALT_SHIP_ADR_PRVN_NME	VARCHAR(255),
					CPE_ALT_SHIP_ADR_PSTL_CD	VARCHAR(15),
					CPE_ALT_SHIP_ADR_ST_PRVN_CD VARCHAR(2),
					CPE_ACCS_PRVDR_TN_NBR        VARCHAR(10),
					CPE_EQUIPMENT_ONLY_FLG_CD	 VARCHAR(1),
					CSG_LVL						 VARCHAR(5),
					PL_NBR                  VARCHAR(200),
					CKT                     VARCHAR(200),
					NUA                     VARCHAR(200),
					QUOTE_ID                VARCHAR(75),
					
				--PJ022384/PJ023226  AccessRefresh and Class of Service Fields.	
					COS_CD                  VARCHAR(1), -- TTRPT_COS_CD  LItm
					ACCS_ARNGT_CD			VARCHAR(3), -- TTRPT_ACCS_ARNGT_CD  LItm
					ACCS_TERM_CD			VARCHAR(12),-- TTRPT_ACCS_CNTRC_TERM_CD Ordr
					ACCS_TERM_VNDR_CD      VARCHAR(12), -- TTRPT_ACCS_CNTRC_TERM_CD  Ordr
					ACCS_TYPE_CD			VARCHAR(3),-- TTRPT_ACCS_TYPE_CD LItm
					ETH_TYPE_CD             VARCHAR(5),
					ACCS_CXR_CD				VARCHAR(5), -- CXR_ACCS_CD  LItm
					ACCS_BDWD_CD			VARCHAR(30),-- TTRPT_ACCS_BDWD_TYPE LItm
					INTL_NW_TYPE            VARCHAR(4),
					IPL_FLG_CD              VARCHAR(1),
					ACCS_RATE_MRC           VARCHAR(25), -- TPORT_CALC_RT_MRC_USD_AMT LItm
					ACCS_RATE_NRC           VARCHAR(25), -- TPORT_CALC_RT_NRC_USD_AMT LItm
					RAW_ACCS_COST_MRC       VARCHAR(25), -- TPORT_VNDR_RAW_MRC_IN_CUR_AMT Ordr
					RAW_ACCS_COST_NRC       VARCHAR(25),
					RAW_ACCS_COST_QOT_CURR  VARCHAR(25),
					RAW_ACCS_COST_MRC_QOT_CURR VARCHAR(25),-- TPORT_VNDR_RAW_MRC_QOT_CUR_AMT Ordr
					RAW_ACCS_COST_NRC_QOT_CURR VARCHAR(25) -- TPORT_VNDR_RAW_NRC_QOT_CUR_AMT Ordr



				)
				

			SET @SQL = ''
			
			IF @ORDR_TYPE_CD IN ('ISMV') 
				BEGIN 
					SET @SQL = 'SELECT *
								FROM OPENQUERY(M5,''SELECT c.CHNG_TXN_ID AS ORDR_ID, 0 AS QDA_ORDR_ID, n.NVTRY_ID AS ORDR_CMPNT_ID, n.CMPNT_TYPE_CD,n.ONE_STOP_SHOP_FLG_CD,
																
									n.DESIGN_DOC_NBR,n.RATE_TYPE_CD,NULL AS PORT_SPD_DES, 
									n.NVTRY_ID,NULL as CPE_ACCS_PRVDR_NME,n.CPE_CONTRACT_TERM,
									n.CPE_CONTRACT_TYPE,NULL as CPE_DLVRY_DUTIES_AMT,NULL as CPE_DLVRY_DUTIES_CD,
									NULL as CPE_DROP_SHIP_FLG_CD,NULL as CPE_EQPT_CKT_ID,n.CPE_ITEM_ID,
									NULL as CPE_MANAGED_BY,NULL as CPE_MFR_ID,NULL as CPE_MFR_NME,NULL as CPE_MFR_PART_NBR,
									n.CPE_MNGD_FLG_CD,NULL as CPE_SPRINT_MNT_FLG_CD,NULL as CPE_MSCP_CD,NULL as CPE_LIST_PRICE,NULL as CPE_REQD_ON_SITE_DT,
									NULL as CPE_SHIP_CLLI_CD,NULL as CPE_SPRINT_TEST_TN_NBR,
									n.CPE_DEVICE_ID, NULL as EQPT_ID, n.EQPT_TYPE_CD, 0 AS FLAG, n.CPE_DEVICE_ID AS DEVICE_ID,
									n.QTY, n.NME, n.CPE_VNDR_DSCNT, n.CPE_ITEM_MSTR_NBR, NULL as CPE_RECORD_ONLY_FLG_CD,
									NULL as CPE_CMPN_FMLY, NULL as RLTD_CMPNT_ID, n.CPE_ITEM_TYPE_CD, ''''CP'''' AS STUS_CD,NULL as CPE_MFR_PS_ID,
									NULL as CPE_ALT_SHIP_ADR,NULL as CPE_ALT_SHIP_ADR_CTRY_CD,NULL as CPE_ALT_SHIP_ADR_CTY_NME,
									NULL as CPE_ALT_SHIP_ADR_LINE_1,NULL as CPE_ALT_SHIP_ADR_LINE_2,NULL as CPE_ALT_SHIP_ADR_LINE_3,
									NULL as CPE_ALT_SHIP_ADR_PRVN_NME,NULL as CPE_ALT_SHIP_ADR_PSTL_CD,
									NULL as CPE_ALT_SHIP_ADR_ST_PRVN_CD, NULL as CPE_ACCS_PRVDR_TN_NBR, NULL as CPE_EQUIPMENT_ONLY_FLG_CD, a.CSG_LVL,
									n.PRIVATE_LINE_NBR,n.CKT_ID,n.NTWK_USR_ADR, n.QUOTE_ID,
									
									n.COS_CD,n.ACCS_ARNGT_CD,n.ACCS_TERM_CD,NULL as ACCS_TERM_VNDR_CD, n.ACCS_TYPE_CD,n.ETH_TYPE_CD,
									n.ACCS_CXR_CD,n.ACCS_BDWD_CD,n.INTL_NW_TYPE,n.IPL_FLG_CD,NULL as ACCS_RATE_MRC,NULL as ACCS_RATE_NRC,
									NULL as RAW_ACCS_COST_MRC,NULL as RAW_ACCS_COST_NRC,NULL as RAW_ACCS_COST_QOT_CURR,NULL as RAW_ACCS_COST_MRC_QOT_CURR,NULL as RAW_ACCS_COST_NRC_QOT_CURR
																			
									FROM	MACH5.V_V5U_CHNG_TXN c
										LEFT JOIN MACH5.V_V5U_NVTRY n ON c.CUST_ACCT_ID = n.CUST_ACCT_ID 
										LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID = c.CUST_ACCT_ID
										WHERE n.STUS_CD = ''''ACT'''' AND c.CHNG_TXN_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')' 
					END
			ELSE
				IF @ORDR_TYPE_CD IN ('CO','ACRF','ACRN') 
					BEGIN
						SET @SQL = 'SELECT *
									FROM OPENQUERY(M5,''SELECT d.CHNG_TXN_ID AS ORDR_ID, 0 AS QDA_ORDR_ID, d.CHNG_TXN_DETL_ID AS ORDR_CMPNT_ID, n.CMPNT_TYPE_CD,n.ONE_STOP_SHOP_FLG_CD,
																	
																	c.DESIGN_DOC_NBR,c.RATE_TYPE_CD,NULL AS PORT_SPD_DES, 
																	d.NVTRY_ID,c.CPE_ACCS_PRVDR_NME,n.CPE_CONTRACT_TERM,
																	n.CPE_CONTRACT_TYPE,c.CPE_DLVRY_DUTIES_AMT,c.CPE_DLVRY_DUTIES_CD,
																	c.CPE_DROP_SHIP_FLG_CD,c.CPE_EQPT_CKT_ID,n.CPE_ITEM_ID,
																	c.CPE_MANAGED_BY,c.CPE_MFR_ID,c.CPE_MFR_NME,c.CPE_MFR_PART_NBR,
																n.CPE_MNGD_FLG_CD,c.CPE_SPRINT_MNT_FLG_CD,c.CPE_MSCP_CD,c.CPE_LIST_PRICE,c.CPE_REQD_ON_SITE_DT,
																	c.CPE_SHIP_CLLI_CD,c.CPE_SPRINT_TEST_TN_NBR,
																	n.CPE_DEVICE_ID, c.EQPT_ID, n.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
																	n.QTY, n.NME, c.CPE_VNDR_DSCNT, n.CPE_ITEM_MSTR_NBR, c.CPE_RECORD_ONLY_FLG_CD,
																	c.CPE_CMPN_FMLY, c.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, c.STUS_CD, c.CPE_MFR_PS_ID,
																	c.CPE_ALT_SHIP_ADR,c.CPE_ALT_SHIP_ADR_CTRY_CD,c.CPE_ALT_SHIP_ADR_CTY_NME,
																	c.CPE_ALT_SHIP_ADR_LINE_1,c.CPE_ALT_SHIP_ADR_LINE_2,c.CPE_ALT_SHIP_ADR_LINE_3,
																	c.CPE_ALT_SHIP_ADR_PRVN_NME,c.CPE_ALT_SHIP_ADR_PSTL_CD,
																	c.CPE_ALT_SHIP_ADR_ST_PRVN_CD, c.CPE_ACCS_PRVDR_TN_NBR, c.CPE_EQUIPMENT_ONLY_FLG_CD, a.CSG_LVL,
																	n.PRIVATE_LINE_NBR,n.CKT_ID,n.NTWK_USR_ADR, n.QUOTE_ID,
																	
																	d.COS_CD,d.ACCS_ARNGT_CD,d.ACCS_TERM_CD,d.ACCS_TERM_VNDR_CD,d.ACCS_TYPE_CD,d.ETH_TYPE_CD,
																	n.ACCS_CXR_CD,d.ACCS_BDWD_CD,d.INTL_NW_TYPE,d.IPL_FLG_CD,d.ACCS_RATE_MRC,d.ACCS_RATE_NRC,
																	d.RAW_ACCS_COST_MRC,d.RAW_ACCS_COST_NRC,d.RAW_ACCS_COST_QOT_CURR,d.RAW_ACCS_COST_MRC_QOT_CURR,d.RAW_ACCS_COST_NRC_QOT_CURR
																													
																	FROM	Mach5.V_V5U_CHNG_TXN_DETL d
															LEFT JOIN Mach5.V_V5U_CHNG_TXN t ON t.CHNG_TXN_ID = d.CHNG_TXN_ID
															LEFT JOIN MACH5.V_V5U_ORDR_CMPNT c ON d.NVTRY_ID = c.NVTRY_ID
															LEFT JOIN MACH5.V_V5U_NVTRY n ON d.NVTRY_ID = n.NVTRY_ID
															LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID = t.CUST_ACCT_ID
															WHERE d.ORIG_CD = ''''C'''' AND	d.CHNG_TXN_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')'  
					END

				ELSE
					BEGIN
						SET @SQL = 'SELECT *
									FROM OPENQUERY(M5,''SELECT d.CHNG_TXN_ID AS ORDR_ID, 0 AS QDA_ORDR_ID, d.CHNG_TXN_DETL_ID AS ORDR_CMPNT_ID, n.CMPNT_TYPE_CD,n.ONE_STOP_SHOP_FLG_CD,
																	
																	c.DESIGN_DOC_NBR,c.RATE_TYPE_CD,NULL AS PORT_SPD_DES, 
																	d.NVTRY_ID,c.CPE_ACCS_PRVDR_NME,n.CPE_CONTRACT_TERM,
																	n.CPE_CONTRACT_TYPE,c.CPE_DLVRY_DUTIES_AMT,c.CPE_DLVRY_DUTIES_CD,
																	c.CPE_DROP_SHIP_FLG_CD,c.CPE_EQPT_CKT_ID,n.CPE_ITEM_ID,
																	c.CPE_MANAGED_BY,c.CPE_MFR_ID,c.CPE_MFR_NME,c.CPE_MFR_PART_NBR,
																	n.CPE_MNGD_FLG_CD,c.CPE_SPRINT_MNT_FLG_CD,c.CPE_MSCP_CD,c.CPE_LIST_PRICE,c.CPE_REQD_ON_SITE_DT,
																	c.CPE_SHIP_CLLI_CD,c.CPE_SPRINT_TEST_TN_NBR,
																	n.CPE_DEVICE_ID, c.EQPT_ID, n.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID,
																	n.QTY, n.NME, c.CPE_VNDR_DSCNT, n.CPE_ITEM_MSTR_NBR, c.CPE_RECORD_ONLY_FLG_CD,
																	c.CPE_CMPN_FMLY, c.RLTD_CMPNT_ID, c.CPE_ITEM_TYPE_CD, c.STUS_CD, c.CPE_MFR_PS_ID,
																	c.CPE_ALT_SHIP_ADR,c.CPE_ALT_SHIP_ADR_CTRY_CD,c.CPE_ALT_SHIP_ADR_CTY_NME,
																	c.CPE_ALT_SHIP_ADR_LINE_1,c.CPE_ALT_SHIP_ADR_LINE_2,c.CPE_ALT_SHIP_ADR_LINE_3,
																	c.CPE_ALT_SHIP_ADR_PRVN_NME,c.CPE_ALT_SHIP_ADR_PSTL_CD,
																	c.CPE_ALT_SHIP_ADR_ST_PRVN_CD, c.CPE_ACCS_PRVDR_TN_NBR, c.CPE_EQUIPMENT_ONLY_FLG_CD, a.CSG_LVL,
																	n.PRIVATE_LINE_NBR,n.CKT_ID,n.NTWK_USR_ADR, n.QUOTE_ID,
																	
																	d.COS_CD,d.ACCS_ARNGT_CD,d.ACCS_TERM_CD,d.ACCS_TERM_VNDR_CD,d.ACCS_TYPE_CD,d.ETH_TYPE_CD,
																	d.ACCS_CXR_CD,d.ACCS_BDWD_CD,d.INTL_NW_TYPE,d.IPL_FLG_CD,d.ACCS_RATE_MRC,d.ACCS_RATE_NRC,
																	d.RAW_ACCS_COST_MRC,d.RAW_ACCS_COST_NRC,d.RAW_ACCS_COST_QOT_CURR,d.RAW_ACCS_COST_MRC_QOT_CURR,d.RAW_ACCS_COST_NRC_QOT_CURR
																													
																	FROM	Mach5.V_V5U_CHNG_TXN_DETL d
															LEFT JOIN Mach5.V_V5U_CHNG_TXN t ON t.CHNG_TXN_ID = d.CHNG_TXN_ID
															LEFT JOIN MACH5.V_V5U_ORDR_CMPNT c ON d.NVTRY_ID = c.NVTRY_ID
															LEFT JOIN MACH5.V_V5U_NVTRY n ON d.NVTRY_ID = n.NVTRY_ID 
															LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID = t.CUST_ACCT_ID
															WHERE d.ORIG_CD = ''''C'''' AND	d.CHNG_TXN_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')'  
					END
			--select @SQL
			INSERT INTO #MACH5_ORDR_CHNG_CMPNT
			EXEC sp_executesql @SQL	
	
			Select * from #MACH5_ORDR_CHNG_CMPNT
			
			
			IF EXISTS
				(
					SELECT 'X'
						FROM #MACH5_ORDR_CHNG_CMPNT
				)
				BEGIN
				
					SELECT @CUST_ACCT_ID = PRNT_ACCT_ID FROM #MACH5_ORDR_CHNG


					IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG_CUST',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_CHNG_CUST
					CREATE TABLE #MACH5_ORDR_CHNG_CUST 
						(
							CIS_LVL_TYPE Varchar(2),
							CUST_ID Varchar(9),
							BR_CD Varchar(2),
							SOI_CD Varchar(3),
							CUST_NME Varchar(200),
							STREET_ADR_1 varchar(255),
							STREET_ADR_2 varchar(255),
							STREET_ADR_3 varchar(255),
							CTRY_CD Varchar(2),
							CTY_NME Varchar(255),
							STT_CD Varchar(2),
							ZIP_PSTL_CD Varchar(15),
							BLDG_ID Varchar(255),
							FLOOR_ID varchar(255),
							ROOM_ID Varchar(255),
							BILL_CYC_ID Varchar(2),
							H1_ID	Varchar(9),
							TAX_XMPT_CD Varchar(1),
							CHARS_CUST_ID  VARCHAR(9),
							SITE_ID	VARCHAR(14),
							PRVN_NME VARCHAR(255),
							CSG_LVL VARCHAR(5)
						)


					SET @SQL = ''
					SET @SQL = 	
						'SELECT * ' + 
							' FROM OPENQUERY (M5, ''Select CIS_HIER_LVL_CD,CIS_CUST_ID, BR_CD,SOI_CD,CUST_NME
															,LINE_1,LINE_2,LINE_3,CTRY_CD,CTY_NME,ST_PRVN_CD
															,PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1,TAX_XMPT_CD
															,CHARS_CUST_ID,SITE_ID,PRVN_NME,CSG_LVL
													From Mach5.V_V5U_CUST_ACCT  
													Where CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
													  AND TYPE_CD = DECODE(CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
													  AND ROWNUM = 1 '')'+															
							
						   ' UNION ' + 
						   ' SELECT * FROM OPENQUERY (M5,''Select h4.CIS_HIER_LVL_CD,h4.CIS_CUST_ID, h4.BR_CD,h4.SOI_CD,h4.CUST_NME
																	,h4.LINE_1,h4.LINE_2,h4.LINE_3,h4.CTRY_CD,h4.CTY_NME,h4.ST_PRVN_CD
																	,h4.PSTL_CD,h4.BLDG_ID,h4.FLOOR_ID,h4.ROOM_ID,h4.BILL_CYC_ID,h4.H1,h4.TAX_XMPT_CD
																	,h4.CHARS_CUST_ID,h4.SITE_ID,h4.PRVN_NME,h4.CSG_LVL
															From Mach5.V_V5U_CUST_ACCT h4 
															INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.H4 = h4.CIS_CUST_ID
															Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ' AND h4.TYPE_CD=DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'+
						   
						   '	UNION	' + 
						   '	Select * FROM OPENQUERY 
									(M5, ''SELECT h1.CIS_HIER_LVL_CD,h1.CIS_CUST_ID, h1.BR_CD,h1.SOI_CD,h1.CUST_NME
																	,h1.LINE_1,h1.LINE_2,h1.LINE_3,h1.CTRY_CD,h1.CTY_NME,h1.ST_PRVN_CD
																	,h1.PSTL_CD,h1.BLDG_ID,h1.FLOOR_ID,h1.ROOM_ID,h1.BILL_CYC_ID,h1.H1,h1.TAX_XMPT_CD
																	,h1.CHARS_CUST_ID,h1.SITE_ID,h1.PRVN_NME,h1.CSG_LVL
																From Mach5.V_V5U_CUST_ACCT h1 
																INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.H1 = h1.CIS_CUST_ID
															Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ' AND h1.TYPE_CD=DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'

					INSERT INTO #MACH5_ORDR_CHNG_CUST (CIS_LVL_TYPE ,CUST_ID ,BR_CD ,SOI_CD ,
													CUST_NME ,STREET_ADR_1 ,STREET_ADR_2 ,
													STREET_ADR_3,CTRY_CD,CTY_NME,STT_CD,
													ZIP_PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1_ID
													,TAX_XMPT_CD,CHARS_CUST_ID,SITE_ID,PRVN_NME,CSG_LVL)
					EXEC sp_executesql 	@SQL
									
					Select * From #MACH5_ORDR_CHNG_CUST		



					IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG_CNTCT',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_CHNG_CNTCT
					CREATE TABLE #MACH5_ORDR_CHNG_CNTCT 
					(
						[CIS_LVL_TYPE] Varchar(2),
						[ROLE_CD] [Varchar](6),
						[PHN_NBR] [Varchar](20) ,
						[TME_ZONE_ID] [varchar](255) ,
						[FRST_NME] [varchar](255) ,
						[LST_NME] [varchar](255) ,
						[EMAIL_ADR] [varchar](255) ,
						[CNTCT_HR_TXT] [varchar](50) ,
						[NPA] [varchar](3) ,
						[NXX] [varchar](3) ,
						[STN_NBR] [varchar](4) ,
						[CTY_CD] [varchar](5) ,
						[ISD_CD] [varchar](3) ,
						[CNTCT_NME] [varchar](255) ,
						[PHN_EXT_NBR] [varchar](10),
						[FSA_MDUL_ID] [varchar] (3),
						[CSG_LVL] [varchar] (5)
					)

					SET @SQL = ''
					SET @SQL = '  SELECT * ' + 
								' FROM OPENQUERY (M5,
								''Select ''''H6'''' AS H6,
									vvcc.ROLE_CD AS ROLE_CD,
									CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN vvcc.DOM_PHN_NBR
										  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN vvcc.INTL_PHN_NBR
										  ELSE ''''''''
									END AS PHN_NBR, 
									vvcc.TME_ZN_CD AS TME_ZONE_ID,
									'''''''' AS FRST_NME,
									'''''''' AS LST_NME ,			
									vvcc.EMAIL_ADR AS EMAIL_ADR, 
									vvcc.AVLBLTY_HR_STRT || CASE WHEN LENGTH(vvcc.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||vvcc.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
									CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvcc.DOM_PHN_NBR,1,3) 
										  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvcc.INTL_PHN_NBR,1,3) 
										  ELSE ''''''''
									END AS NPA ,			
								   CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvcc.DOM_PHN_NBR,4,3) 
									  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvcc.INTL_PHN_NBR,4,3) 
									  ELSE ''''''''       
									END AS  NXX ,			
								   CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvcc.DOM_PHN_NBR,7,4) 
									  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvcc.INTL_PHN_NBR,7,4) 
									  ELSE ''''''''      
									END AS STN_NBR ,			
									'''''''' AS CTY_CD ,
									vvcc.INTL_CNTRY_DIAL_CD AS ISD_CD,			
									vvcc.CUST_NME AS CNTCT_NME ,			
									vvcc.PHN_EXT AS PHN_EXT_NBR,
									''''CIS'''' AS FSA_MDUL_ID,
									vvca.CSG_LVL
									From Mach5.v_V5U_CUST_CONTACT vvcc INNER JOIN
										 Mach5.V_V5U_CUST_ACCT  vvca ON vvca.CUST_ACCT_ID = vvcc.CUST_ACCT_ID
								  WHERE vvcc.CUST_ACCT_ID ='   + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'+


								'	UNION	' + 

								' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select ''''H4'''' AS H6,
										hc4.ROLE_CD AS ROLE_CD, 
										CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN hc4.DOM_PHN_NBR
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN hc4.INTL_PHN_NBR
										  ELSE ''''''''
										END AS PHN_NBR, 
										hc4.TME_ZN_CD AS TME_ZONE_ID,
										'''''''' AS FRST_NME,
										'''''''' AS LST_NME ,			
										hc4.EMAIL_ADR AS EMAIL_ADR,
										hc4.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc4.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc4.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
										CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,1,3) 
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,1,3) 
										  ELSE ''''''''
										  END AS NPA ,			
										CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,4,3) 
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,4,3) 
										  ELSE ''''''''       
										  END AS  NXX ,			
									   CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,7,4) 
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,7,4) 
										  ELSE ''''''''      
										  END AS STN_NBR ,			
										'''''''' AS CTY_CD ,
										hc4.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
										hc4.CUST_NME AS CNTCT_NME ,			
										hc4.PHN_EXT AS PHN_EXT_NBR,
										''''CIS'''' AS FSA_MDUL_ID,
										h6.CSG_LVL
								  From Mach5.V_V5U_CUST_ACCT h6 
									INNER JOIN Mach5.V_V5U_CUST_ACCT h4 ON h6.H4 = h4.CIS_CUST_ID
									INNER JOIN  Mach5.v_V5U_CUST_CONTACT hc4 ON h4.CUST_ACCT_ID = hc4.CUST_ACCT_ID
									Where h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
									  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
									  AND h4.TYPE_CD = DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'+


								'	UNION	' + 

								' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select ''''H1'''' AS H6,
									hc1.ROLE_CD AS ROLE_CD, 
									CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN hc1.DOM_PHN_NBR
										  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN hc1.INTL_PHN_NBR
										  ELSE ''''''''
										END AS PHN_NBR,  
									hc1.TME_ZN_CD AS TME_ZONE_ID,
									'''''''' AS FRST_NME,
									'''''''' AS LST_NME ,			
									hc1.EMAIL_ADR AS EMAIL_ADR,
									hc1.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc1.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc1.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
									CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,1,3) 
										WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,1,3) 
									  ELSE ''''''''
									  END AS NPA ,			
								   CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,4,3) 
									  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,4,3) 
									  ELSE ''''''''       
									  END AS  NXX ,			
								   CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,7,4) 
									  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,7,4) 
									  ELSE ''''''''      
									  END AS STN_NBR ,			
									'''''''' AS CTY_CD ,
									hc1.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
									hc1.CUST_NME AS CNTCT_NME ,			
									hc1.PHN_EXT AS PHN_EXT_NBR, 
									''''CIS'''' AS FSA_MDUL_ID,
									h6.CSG_LVL
							  From Mach5.V_V5U_CUST_ACCT h6 
								INNER JOIN Mach5.V_V5U_CUST_ACCT h1 ON h6.H1 = h1.CIS_CUST_ID
								INNER JOIN  Mach5.v_V5U_CUST_CONTACT hc1 ON h1.CUST_ACCT_ID = hc1.CUST_ACCT_ID
								Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
								  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
								  AND h1.TYPE_CD = DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' +
								
								'	UNION	' + 

								' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select DISTINCT ''''H2'''' AS H6,
										hc2.ROLE_CD AS ROLE_CD, 
										CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN hc2.DOM_PHN_NBR
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN hc2.INTL_PHN_NBR
										  ELSE ''''''''
										END AS PHN_NBR, 
										hc2.TME_ZN_CD AS TME_ZONE_ID,
										'''''''' AS FRST_NME,
										'''''''' AS LST_NME ,			
										hc2.EMAIL_ADR AS EMAIL_ADR,
										hc2.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc2.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc2.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
										CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc2.DOM_PHN_NBR,1,3) 
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc2.INTL_PHN_NBR,1,3) 
										  ELSE ''''''''
										  END AS NPA ,			
										CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc2.DOM_PHN_NBR,4,3) 
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc2.INTL_PHN_NBR,4,3) 
										  ELSE ''''''''       
										  END AS  NXX ,			
									   CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc2.DOM_PHN_NBR,7,4) 
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc2.INTL_PHN_NBR,7,4) 
										  ELSE ''''''''      
										  END AS STN_NBR ,			
										'''''''' AS CTY_CD ,
										hc2.INTL_CNTRY_DIAL_CD AS ISD_CD,			
										hc2.CUST_NME AS CNTCT_NME ,			
										hc2.PHN_EXT AS PHN_EXT_NBR,
										''''CIS'''' AS FSA_MDUL_ID,
										h6.CSG_LVL
									  From Mach5.V_V5U_CUST_ACCT h6 
											INNER JOIN Mach5.V_V5U_CUST_ACCT h2 ON h6.H2 = h2.CIS_CUST_ID
											INNER JOIN  Mach5.v_V5U_CUST_CONTACT hc2 ON h2.CUST_ACCT_ID = hc2.CUST_ACCT_ID
											Where h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
											  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
											  AND h2.TYPE_CD = DECODE(h2.CIS_HIER_LVL_CD,''''H2'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' 

						INSERT INTO #MACH5_ORDR_CHNG_CNTCT 
						(
							[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
							[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
							[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR], [FSA_MDUL_ID], [CSG_LVL]
						)
						EXEC sp_executesql @SQL
					
						SET @SQL = ''
						SET @SQL =   
		  					
			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select ''''H1'''' AS H1,	atcH1.ROLE_CD AS ROLE_CD, 
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN atcH1.DOM_PHN_NBR
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN atcH1.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR
								, '''''''' AS TME_ZONE_ID, atcH1.FIRST_NME AS FRST_NME, atcH1.LAST_NME AS LST_NME , atcH1.EMAIL_ADR AS EMAIL_ADR, '''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,1,3) 
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,4,3) 
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,7,4) 
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,
								atcH1.INTL_CNTRY_DIAL_CD AS ISD_CD ,atcH1.FIRST_NME || '''' ''''|| atcH1.LAST_NME AS CNTCT_NME ,'''''''' AS PHN_EXT_NBR,''''ATM'''' AS FSA_MDUL_ID,	h6.CSG_LVL
						From	Mach5.V_V5U_CUST_ACCT h6 
							INNER JOIN Mach5.V_V5U_CUST_ACCT h1 ON h6.H1 = h1.CIS_CUST_ID
							INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH1 ON atcH1.CUST_ACCT_ID = h1.CUST_ACCT_ID
						WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
						  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
						  AND h1.TYPE_CD = DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' +
						'	UNION	' +  			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select ''''H4'''' AS H4,	atcH4.ROLE_CD AS ROLE_CD, 
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN atcH4.DOM_PHN_NBR
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN atcH4.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID, atcH4.FIRST_NME AS FRST_NME,atcH4.LAST_NME AS LST_NME ,atcH4.EMAIL_ADR AS EMAIL_ADR, '''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,1,3) 
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,4,3) 
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,7,4) 
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,atcH4.INTL_CNTRY_DIAL_CD AS ISD_CD ,atcH4.FIRST_NME || '''' ''''|| atcH4.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,''''ATM'''' AS FSA_MDUL_ID,	h6.CSG_LVL
						From Mach5.V_V5U_CUST_ACCT h6 
								INNER JOIN Mach5.V_V5U_CUST_ACCT h4 ON h6.H4 = h4.CIS_CUST_ID
								INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH4 ON atcH4.CUST_ACCT_ID = h4.CUST_ACCT_ID
						WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
						  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
						  AND h4.TYPE_CD = DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' +
						'	UNION	' +  			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select ''''H6'''' AS H6, vvat.ROLE_CD AS ROLE_CD, 
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN vvat.DOM_PHN_NBR
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN vvat.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID,vvat.FIRST_NME AS FRST_NME,	vvat.LAST_NME AS LST_NME ,vvat.EMAIL_ADR AS EMAIL_ADR, 	'''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvat.DOM_PHN_NBR,1,3) 
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvat.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvat.DOM_PHN_NBR,4,3) 
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvat.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvat.DOM_PHN_NBR,7,4) 
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvat.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,vvat.INTL_CNTRY_DIAL_CD AS ISD_CD ,	vvat.FIRST_NME || '''' ''''|| vvat.LAST_NME AS CNTCT_NME , '''''''' AS PHN_EXT_NBR,	''''ATM'''' AS FSA_MDUL_ID,	vvca.CSG_LVL
						From Mach5.v_V5U_ACCT_TEAM_CONTACT vvat INNER JOIN
							 Mach5.V_V5U_CUST_ACCT  vvca ON vvca.CUST_ACCT_ID = vvat.CUST_ACCT_ID
						WHERE vvat.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'  +
						'	UNION	' +  			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select ''''H2'''' AS H2,	atcH2.ROLE_CD AS ROLE_CD, 
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN atcH2.DOM_PHN_NBR
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN atcH2.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID, atcH2.FIRST_NME AS FRST_NME,atcH2.LAST_NME AS LST_NME ,atcH2.EMAIL_ADR AS EMAIL_ADR, '''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH2.DOM_PHN_NBR,1,3) 
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH2.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH2.DOM_PHN_NBR,4,3) 
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH2.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH2.DOM_PHN_NBR,7,4) 
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH2.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,atcH2.INTL_CNTRY_DIAL_CD AS ISD_CD ,atcH2.FIRST_NME || '''' ''''|| atcH2.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,''''ATM'''' AS FSA_MDUL_ID,	h6.CSG_LVL
						From Mach5.V_V5U_CUST_ACCT h6 
								INNER JOIN Mach5.V_V5U_CUST_ACCT h2 ON h6.H2 = h2.CIS_CUST_ID
								INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH2 ON atcH2.CUST_ACCT_ID = h2.CUST_ACCT_ID
						WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
						  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
						  AND h2.TYPE_CD = DECODE(h2.CIS_HIER_LVL_CD,''''H2'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'
								

						INSERT INTO #MACH5_ORDR_CHNG_CNTCT 
							(
								[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
								[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
								[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID], [CSG_LVL]
							)
						EXEC sp_executesql @SQL
						
						
						SET @SQL = ''
						SET @SQL = 
							' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select ''''H6'''' AS H6,
										''''Hrchy'''' AS ROLE_CD, 
										CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN DOM_PHN_NBR 
											  WHEN DOM_INTL_IND_CD = ''''I'''' THEN CONCAT(INTL_CNTRY_DIAL_CD , INTL_PHN_NBR )  
											  ELSE ''''''''
										END AS PHN_NBR, 
										'''''''' AS TME_ZONE_ID,
										'''''''' AS FRST_NME,
										'''''''' AS LST_NME ,			
										'''''''' AS EMAIL_ADR, 
										'''''''' AS CNTCT_HR_TXT,  	
										CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,1,3) 
											  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,1,3) 
											  ELSE ''''''''
										END AS NPA ,			
									   CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,4,3) 
										  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,4,3)
										  ELSE ''''''''       
										END AS  NXX ,			
									   CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,7,4) 
										  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,7,4) 
										  ELSE ''''''''      
										END AS STN_NBR ,			
										'''''''' AS CTY_CD ,
										INTL_CNTRY_DIAL_CD AS ISD_CD ,			
										CUST_NME AS CNTCT_NME ,			
										PHN_EXT AS PHN_EXT_NBR,
										''''CIS'''' AS FSA_MDUL_ID,
										CSG_LVL
									From Mach5.V_V5U_CUST_ACCT
								  WHERE CUST_ACCT_ID ='   + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
								    AND TYPE_CD = DECODE(CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
									AND ROWNUM = 1 '')'
						
						
						INSERT INTO #MACH5_ORDR_CHNG_CNTCT 
							(
								[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
								[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
								[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID],[CSG_LVL]
							)
						EXEC sp_executesql @SQL


						SET @SQL = ''
						SET @SQL = 
							' SELECT * ' + 
								' FROM OPENQUERY (M5,
											''Select DISTINCT ''''OD'''' AS H6,
												oc.ROLE_CD AS ROLE_CD, 
												CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN oc.DOM_PHN_NBR
													WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN oc.INTL_PHN_NBR
													ELSE ''''''''
												END AS PHN_NBR, 
												'''''''' AS TME_ZONE_ID,
												oc.FIRST_NME AS FRST_NME,
												oc.LAST_NME AS LST_NME ,			
												oc.EMAIL_ADR AS EMAIL_ADR,
												''''''''  AS CNTCT_HR_TXT,  	
												CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(oc.DOM_PHN_NBR,1,3) 
												  WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(oc.INTL_PHN_NBR,1,3) 
												  ELSE ''''''''
												  END AS NPA ,			
												CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(oc.DOM_PHN_NBR,4,3) 
												  WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(oc.INTL_PHN_NBR,4,3) 
												  ELSE ''''''''       
												  END AS  NXX ,			
											   CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(oc.DOM_PHN_NBR,7,4) 
												  WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(oc.INTL_PHN_NBR,7,4) 
												  ELSE ''''''''      
												  END AS STN_NBR ,			
												'''''''' AS CTY_CD ,
												oc.INTL_CNTRY_DIAL_CD AS ISD_CD,			
												oc.FIRST_NME || '''' ''''|| oc.LAST_NME AS CNTCT_NME,			
												'''''''' AS PHN_EXT_NBR,
												''''CIS'''' AS FSA_MDUL_ID,
												h6.CSG_LVL
									  From Mach5.V_V5U_ACCT_TEAM_CONTACT_ORDR oc
											INNER JOIN Mach5.V_V5U_ORDR od ON od.ORDR_ID = oc.ORDR_ID
											INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.CUST_ACCT_ID = od.PRNT_ACCT_ID
											Where oc.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')'


							
							INSERT INTO #MACH5_ORDR_CHNG_CNTCT
							(
								[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
								[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
								[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID], [CSG_LVL]
							)

						
						SELECT * FROM #MACH5_ORDR_CHNG_CNTCT
						
						----Order Notes
						IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG_NOTES',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_CHNG_NOTES
						CREATE TABLE #MACH5_ORDR_CHNG_NOTES
							(
								NTE_TYPE_CD Varchar(50),
								NTE_TXT		Varchar(max)
							)
						SET @SQL = ''
						SET @SQL = 'SELECT TYPE_CD,NTE_TXT 
										FROM OPENQUERY(M5,
											''SELECT TYPE_CD,NTE_TXT 
													FROM V_V5U_ORDR_NOTE 
													WHERE ORDR_ID =' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')' 
						
						INSERT INTO #MACH5_ORDR_CHNG_NOTES (NTE_TYPE_CD,NTE_TXT)
							EXEC sp_executesql @SQL
							
						IF @ORDR_TYPE_CD = 'ISMV'
							BEGIN  
								DECLARE @NTE varchar(max)
								DECLARE @BLDG varchar(max)
								DECLARE @FLR varchar(max)
								DECLARE @RM varchar(max)
								DECLARE @DMARC Table (Bldg varchar(max),Flr varchar (max), Rm varchar(max)) 
								
								SET @SQL = ''
								SET @SQL = 'SELECT ISNULL(BLDG_ID,''''), ISNULL(FLOOR_ID,''''), ISNULL(ROOM_ID,'''') FROM OPENQUERY(M5,''SELECT BLDG_ID, FLOOR_ID, ROOM_ID	
										FROM	Mach5.V_V5U_CHNG_TXN_DETL d	
										where ORIG_CD = ''''O'''' AND CHNG_TXN_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')' 
								INSERT INTO @DMARC (Bldg,Flr, Rm)
								EXEC sp_executesql @SQL													
								
								SELECT top 1 @BLDG = Bldg, @FLR = Flr, @RM = Rm from @DMARC
								
								SELECT @NTE = ISNULL('Original Building/Floor/Room : ' + @BLDG + '/'+@FLR+'/'+@RM + '      ','')

								SET @BLDG = ''
								SET @FLR = ''
								SET @RM = '' 
								DELETE FROM @DMARC
								
								SET @SQL = ''
								SET @SQL = 'SELECT ISNULL(BLDG_ID,''''), ISNULL(FLOOR_ID,''''), ISNULL(ROOM_ID,'''') FROM OPENQUERY(M5,''SELECT BLDG_ID, FLOOR_ID, ROOM_ID	
										FROM	Mach5.V_V5U_CHNG_TXN_DETL d	
										where ORIG_CD = ''''C'''' AND CHNG_TXN_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')' 
								
								INSERT INTO @DMARC (Bldg,Flr, Rm)
								EXEC sp_executesql @SQL	
								
								SELECT top 1 @BLDG = Bldg, @FLR = Flr, @RM = Rm from @DMARC	

								SELECT @NTE = @NTE + '  Destination Building/Floor/Room : ' + @BLDG + '/'+@FLR+'/'+@RM + '      '
											
								INSERT INTO #MACH5_ORDR_CHNG_NOTES (NTE_TYPE_CD,NTE_TXT)
										values (25,@NTE)	
								
								UPDATE #MACH5_ORDR_CHNG_CUST
									set BLDG_ID = @BLDG,
										FLOOR_ID = @FLR,
										ROOM_ID = @RM
									WHERE CIS_LVL_TYPE = 'H6'

							
							END	
						Select * From #MACH5_ORDR_CHNG_NOTES
						
						
						
						SELECT @Cnt = COUNT(1) FROM #MACH5_ORDR_CHNG_CMPNT
						
						Select @H5_H6_CUST_ID = CUST_ID
							FROM #MACH5_ORDR_CHNG_CUST
							WHERE CIS_LVL_TYPE IN ('H5','H6')
						
											
						SELECT @INTL_FLG = CASE DOM_PROCESS_FLG_CD
													WHEN 'N' THEN 1
													ELSE 0
												END
								FROM #MACH5_ORDR_CHNG WITH (NOLOCK)


						
						-- Domestic PPRT_ID defaulted to Domestic Disconnect to skip Mat Req and Eqp Receipts.
						SET @PPRT_ID = CASE 
							WHEN @ORDR_TYPE_CD = 'ROCP'	AND @INTL_FLG = 0	THEN 631 
							WHEN @ORDR_TYPE_CD = 'ISMV' AND @INTL_FLG = 0	THEN 631
							WHEN @ORDR_TYPE_CD = 'PRCV' AND @INTL_FLG = 0   THEN 631
							ELSE null --Changed from default 573 on 2/17/19 -- Default for ISMV International MPLS off-net 3/13/17
							END
						
						
						IF EXISTS
						(
							SELECT 'X'
								FROM 
									#MACH5_ORDR_CHNG m WITH (NOLOCK)
									LEFT JOIN #MACH5_ORDR_CHNG_CMPNT mo WITH (NOLOCK) ON m.M5_ORDR_ID = mo.M5_ORDR_ID
								WHERE (m.ORDR_SUBTYPE_CD in ('ISMV','CCOS','ACRF','ACRN')
									AND m.DOM_PROCESS_FLG_CD = 'N')
									
						)
						BEGIN
							
							OPEN SYMMETRIC KEY FS@K3y 
							DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
							INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('ISMV:'+@TransName,'insertMach5TxnDetails', GETDATE())
							SET @ERRLog = 'ISMV:'
							DECLARE @ProdType table (INTL_NW_TYPE varchar (50))
							DECLARE @INTL_NW_TYPE varchar(50)
							DECLARE @CUST_ID INT
							
							SELECT @CUST_ID = PRNT_ACCT_ID FROM #MACH5_ORDR_CHNG 
							
							SET @SQL = 'SELECT INTL_NW_TYPE FROM OPENQUERY(M5,
											''SELECT INTL_NW_TYPE 
												FROM Mach5.V_V5U_NVTRY 
												WHERE  CMPNT_TYPE_CD = ''''ACCS'''' AND CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ID) + '
												UNION ' +
						' Select 	cp.INTL_NW_TYPE ' + 
						' FROM	MACH5.V_V5U_CHNG_TXN c
													LEFT JOIN Mach5.V_V5U_CHNG_TXN_DETL d ON c.CHNG_TXN_ID = d.CHNG_TXN_ID
													LEFT JOIN MACH5.V_V5U_ORDR_CMPNT cp ON d.NVTRY_ID = cp.NVTRY_ID
													WHERE cp.CMPNT_TYPE_CD = ''''ACCS'''' AND	c.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ID) + '  '')'  
							
							INSERT INTO @ProdType (INTL_NW_TYPE)
								EXEC sp_executesql @SQL	
							
							SELECT TOP 1 @INTL_NW_TYPE = INTL_NW_TYPE from @ProdType -- MACH5 only has OFN,ONN & null as values.
							
							--default region ID to AMNCI and later modified after order insertion in this SP
							INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID,DMSTC_CD, PLTFRM_CD,PROD_ID)	
									SELECT @OrderCategoryID, 1, 1, CASE t.DOM_PROCESS_FLG_CD
																		WHEN 'Y' THEN '0'
																		WHEN 'N' THEN '1'
																		ELSE '0'
																	END, p.PLTFRM_CD, t.PROD_ID
																-- Removed 4/17/17 and added case statement 
																--	 19 -- Default to MPLS Offnet for ISMV International 3/13/17
																	
									FROM #MACH5_ORDR_CHNG t WITH (NOLOCK)
								LEFT JOIN dbo.PLTFRM_MAPNG p WITH (NOLOCK) ON t.PROD_TYPE_CD = p.FSA_PROD_TYPE_CD 
								
							
							SELECT @ORDR_ID = SCOPE_IDENTITY()
							SET @FSA_ORDR_ID = @ORDR_ID
							
							IF @QDA_ORDR_ID != ''
								SET @QDA_ORDR_ID = @QDA_ORDR_ID + ',' + CONVERT(varchar,@FSA_ORDR_ID)
							ELSE
								SET @QDA_ORDR_ID = CONVERT(varchar,@FSA_ORDR_ID)
							
							UPDATE		dbo.ORDR	WITH (ROWLOCK)
								SET		PRNT_ORDR_ID	=	@ORDR_ID,
										H5_H6_CUST_ID	=	@H5_H6_CUST_ID,
										CSG_LVL_ID		=	(SELECT TOP 1 ISNULL(lcl.CSG_LVL_ID,0) FROM #MACH5_ORDR_CHNG mo WITH (NOLOCK) LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=mo.CSG_LVL WHERE mo.M5_ORDR_ID = @M5_ORDR_ID),
										CHARS_ID		=	(SELECT TOP 1 CHARS_CUST_ID FROM #MACH5_ORDR_CHNG WITH (NOLOCK) WHERE M5_ORDR_ID = @M5_ORDR_ID),
										PPRT_ID         =   @PPRT_ID
								WHERE	ORDR_ID			=	@ORDR_ID
							
								
															
								
							-------FSA_ORDR
							INSERT INTO FSA_ORDR (ORDR_ID,ORDR_ACTN_ID,FTN,ORDR_TYPE_CD,ORDR_SUB_TYPE_CD,PROD_TYPE_CD,CUST_SIGNED_DT,CUST_WANT_DT,
													CUST_ORDR_SBMT_DT, CUST_CMMT_DT,CUST_PRMS_OCPY_CD,CUST_ACPT_ERLY_SRVC_CD,MULT_CUST_ORDR_CD,
													DISC_REAS_CD, TTRPT_OSS_CD,VNDR_VPN_CD,INSTL_VNDR_CD,INSTL_ESCL_CD,
													FSA_EXP_TYPE_CD,PRE_QUAL_NBR,PRNT_FTN,RELTD_FTN,SITE_ID
													)
							(
								SELECT	TOP 1 @FSA_ORDR_ID,@ORDR_ACTN_ID,m.ORDER_NBR,m.ORDR_TYPE_CD,
										CASE m.ORDR_SUBTYPE_CD
											WHEN  'ISMV' THEN 'ISMV'
											WHEN  'CCOS'  THEN 'CCOS'
											WHEN 'ACRF'  THEN 'AR'
											WHEN 'ACRN'  THEN 'AN'
											ELSE NULL
											END AS ORDR_SUBTYPE_CD,
										m.PROD_TYPE_CD,
										m.CUST_SIGN_DT,
										m.CUST_WANT_DT,m.CUST_SBMT_DT,m.CUST_CMMT_DT,m.CUST_PREM_OCCU_FLG_CD,m.CUST_ACPT_SRVC_FLG_CD,m.MULT_ORDR_SBMTD_FLG_CD,
										m.DSCNCT_REAS_CD,a.ONE_STOP_SHOP_FLG_CD,
										CASE 
											WHEN m.PROD_TYPE_CD IN ('MO') THEN a.ACCS_CXR_CD
											--ELSE NULL  changed dlp0278 12/2019 populating carrier when M5 provides regardless of PROD_TYE
											ELSE a.ACCS_CXR_CD  
										END	,
										CASE 
											WHEN m.PROD_TYPE_CD IN ('MO','SO','DO') THEN a.ACCS_CXR_CD
											--ELSE NULL  changed dlp0278 12/2019 populating carrier when M5 provides regardless of PROD_TYE
											ELSE a.ACCS_CXR_CD
										END	,
										m.EXP_FLG_CD,	CASE m.EXP_TYPE_CD 
																							WHEN 'B' THEN 'IE'
																							ELSE m.EXP_TYPE_CD
																							END
										,m.PRE_QUAL_NBR
										,CASE 
											WHEN m.ORDR_TYPE_CD ='CN' THEN m.PRNT_FTN
											WHEN m.ORDR_SUBTYPE_CD = 'ISMV' THEN m.PRNT_FTN
											ELSE null
										  END
										,CASE m.ORDR_TYPE_CD
											WHEN 'CN' THEN NULL
											ELSE m.RLTD_ORDR_ID
										  END 
										,CONVERT(VARCHAR,ISNULL(a.ORDR_CMPNT_ID,'')) + ',' + CONVERT(VARCHAR,ISNULL(p.ORDR_CMPNT_ID,'')) AS SITE_ID
									FROM #MACH5_ORDR_CHNG m WITH (NOLOCK)
									LEFT JOIN #MACH5_ORDR_CHNG_CMPNT a WITH (NOLOCK) ON m.M5_ORDR_ID = a.M5_ORDR_ID AND a.CMPNT_TYPE_CD = 'ACCS'
									LEFT JOIN #MACH5_ORDR_CHNG_CMPNT p WITH (NOLOCK) ON m.M5_ORDR_ID = p.M5_ORDR_ID AND p.CMPNT_TYPE_CD = 'PORT'
									WHERE m.M5_ORDR_ID = @M5_ORDR_ID
									
							)
							
							--FSA_ORDR_CPE_LINE_ITEM
											INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM ([ORDR_ID], [LINE_ITEM_CD],
											[TTRPT_ACCS_TYPE_CD],
											[TTRPT_ACCS_ARNGT_CD],
											[CXR_ACCS_CD],
											[CKT_ID],
											[TTRPT_ACCS_TERM_DES],
											[TTRPT_ACCS_BDWD_TYPE],
											[PL_NBR],
											CMPNT_ID,
											ORDR_CMPNT_ID,
											TTRPT_COS_CD)
											SELECT DISTINCT @FSA_ORDR_ID, 'ACS',
											a.ACCS_TYPE_CD, a.ACCS_ARNGT_CD,
											CASE 
												WHEN m.PROD_TYPE_CD IN ('MP','DN','SN','IO','IN') THEN a.ACCS_CXR_CD
												ELSE a.ACCS_CXR_CD  -- changed dlp0278 12/2019 populating carrier when M5 provides regardless of PROD TYPE.
											END,
											a.CKT,
											a.ACCS_TERM_CD,
											a.ACCS_BDWD_CD,
											a.PL_NBR,
											a.NVTRY_ID,
											a.NVTRY_ID,
											a.COS_CD
											FROM #MACH5_ORDR_CHNG m WITH (NOLOCK)
											LEFT JOIN #MACH5_ORDR_CHNG_CMPNT a WITH (NOLOCK) ON m.M5_ORDR_ID = a.M5_ORDR_ID 
													AND a.CMPNT_TYPE_CD = 'ACCS'
											
											INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM ([ORDR_ID], [LINE_ITEM_CD], [INSTL_DSGN_DOC_NBR],
											[TTRPT_SPD_OF_SRVC_BDWD_DES],
											[PORT_RT_TYPE_CD],
											CMPNT_ID,
											ORDR_CMPNT_ID,TTRPT_COS_CD)
											SELECT DISTINCT @FSA_ORDR_ID, 'PRT', p.DESIGN_DOC_NUM,
											p.PORT_SPD_DES,
											p.RATE_TYPE_CD,
											p.NVTRY_ID,
											p.NVTRY_ID,
											P.COS_CD
											FROM #MACH5_ORDR_CHNG_CMPNT p WITH (NOLOCK) WHERE p.CMPNT_TYPE_CD = 'PORT'
											
							--ORDR_EXP	
							IF EXISTS
							(
								SELECT 'X'
									FROM #MACH5_ORDR_CHNG
									WHERE EXP_FLG_CD = 'Y'
							)
							BEGIN
								INSERT INTO ORDR_EXP (ORDR_ID,
														EXP_TYPE_ID,
														AUTHRZR_PHN_NBR,
														AUTHRZR_TITLE_ID,
														AUTHN_DT,
														BILL_TO_COST_CTR_CD,
														CREAT_DT,
														CREAT_BY_USER_ID,
														REC_STUS_ID
															)
								(
									SELECT TOP 1	@FSA_ORDR_ID
													,CASE EXP_TYPE_CD
															WHEN 'I' THEN 1
															WHEN 'E' THEN 2
															WHEN 'B' THEN 3
															ELSE 4 
														END AS EXP_TYPE_ID
													,APRVR_PHN
													,APRVR_TITLE
													,APPRVL_DT
													,[dbo].[RemoveSpecialCharacters](LEFT(SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10),
           PATINDEX('%[^0-9]%', SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10) + 'X') -1))
													,GETDATE()
													,1
													,1
										FROM #MACH5_ORDR_CHNG WITH (NOLOCK)
										
								)
							END
							
							delete from @tmpids
							-----FSA_ORDR_CUST
							INSERT INTO dbo.FSA_ORDR_CUST 
									(	ORDR_ID,
										CIS_LVL_TYPE,
										CUST_ID,
										BR_CD,
										SOI_CD,
										CREAT_DT,
										CUST_NME,
										CURR_BILL_CYC_CD,
										TAX_XMPT_CD,
										CHARS_CUST_ID,
										SITE_ID)								
								(SELECT	@FSA_ORDR_ID,
										CIS_LVL_TYPE,
										CUST_ID,
										BR_CD,
										SOI_CD,
										GETDATE(),
										CUST_NME,
										ISNULL(BILL_CYC_ID,0),
										TAX_XMPT_CD,
										CHARS_CUST_ID,
										SITE_ID
									FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
									WHERE ISNULL(CSG_LVL,'')=''
								)

							INSERT INTO dbo.FSA_ORDR_CUST 
									(	ORDR_ID,
										CIS_LVL_TYPE,
										CUST_ID,
										BR_CD,
										SOI_CD,
										CREAT_DT,
										CURR_BILL_CYC_CD,
										TAX_XMPT_CD,
										CHARS_CUST_ID,
										SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
								SELECT	@FSA_ORDR_ID,
										CIS_LVL_TYPE,
										CUST_ID,
										BR_CD,
										SOI_CD,
										GETDATE(),
										ISNULL(BILL_CYC_ID,0),
										TAX_XMPT_CD,
										CHARS_CUST_ID,
										SITE_ID
									FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
									WHERE ISNULL(CSG_LVL,'')!=''
								
							
							INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
										SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString(moc.CUST_NME)
												FROM #MACH5_ORDR_CHNG_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												  AND foc.CUST_NME IS NULL
												  
							delete from @tmpids
							IF NOT EXISTS
								(
									SELECT 'X'
										FROM dbo.FSA_ORDR_CUST WITH (NOLOCK)
										WHERE ORDR_ID = @ORDR_ID
											AND CIS_LVL_TYPE = 'H1'
								)
								BEGIN
									INSERT INTO dbo.FSA_ORDR_CUST 
									(	ORDR_ID,
										CIS_LVL_TYPE,
										CUST_ID,
										BR_CD,
										SOI_CD,
										CREAT_DT,
										CUST_NME,
										CURR_BILL_CYC_CD,
										TAX_XMPT_CD,
										CHARS_CUST_ID,
										SITE_ID)									
								(SELECT	@FSA_ORDR_ID,
										'H1',
										H1_ID,
										'',
										'',
										GETDATE(),
										'',
										BILL_CYC_ID,
										TAX_XMPT_CD,
										CHARS_CUST_ID,
										SITE_ID
									FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
									WHERE CIS_LVL_TYPE = 'H6'
									AND ISNULL(CSG_LVL,'')=''
								)

								INSERT INTO dbo.FSA_ORDR_CUST 
									(	ORDR_ID,
										CIS_LVL_TYPE,
										CUST_ID,
										BR_CD,
										SOI_CD,
										CREAT_DT,
										CURR_BILL_CYC_CD,
										TAX_XMPT_CD,
										CHARS_CUST_ID,
										SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
								SELECT	@FSA_ORDR_ID,
										'H1',
										H1_ID,
										'',
										'',
										GETDATE(),
										BILL_CYC_ID,
										TAX_XMPT_CD,
										CHARS_CUST_ID,
										SITE_ID
									FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
									WHERE CIS_LVL_TYPE = 'H6'
									  AND ISNULL(CSG_LVL,'')!=''
								
								
								INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
										SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString('')
												FROM #MACH5_ORDR_CHNG_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												  AND foc.CUST_NME IS NULL
								END
							
							delete from @tmpids
							----ORDR_ADR
							INSERT INTO dbo.ORDR_ADR
								(
									ORDR_ID,
									ADR_TYPE_ID,
									CREAT_DT,
									CREAT_BY_USER_ID,
									REC_STUS_ID,
									CTRY_CD,
									CIS_LVL_TYPE,
									FSA_MDUL_ID,
									STREET_ADR_1,
									STREET_ADR_2,
									CTY_NME,
									PRVN_NME,
									STT_CD,
									ZIP_PSTL_CD,
									BLDG_NME,
									FLR_ID,
									RM_NBR,
									HIER_LVL_CD,
									STREET_ADR_3
								)
							(
								SELECT	@FSA_ORDR_ID,
										CASE CIS_LVL_TYPE
											WHEN 'H4' THEN 3
											WHEN 'H6' THEN 18
											WHEN 'H5' THEN 18
											ELSE 17
										END,
										GETDATE(),
										1,
										1,
										CTRY_CD,
										CIS_LVL_TYPE,
										'CIS',
										STREET_ADR_1,
										STREET_ADR_2,
										CTY_NME,
										PRVN_NME,
										STT_CD,
										ZIP_PSTL_CD,
										BLDG_ID,
										FLOOR_ID,
										ROOM_ID,
										CASE CIS_LVL_TYPE	
											WHEN 'H4' THEN 0
											WHEN 'H1' THEN 0
											ELSE 1
										END,
										STREET_ADR_3
									FROM #MACH5_ORDR_CHNG_CUST
									WHERE ISNULL(CSG_LVL,'')=''
							)
							
							INSERT INTO dbo.ORDR_ADR
								(
									ORDR_ID,
									ADR_TYPE_ID,
									CREAT_DT,
									CREAT_BY_USER_ID,
									REC_STUS_ID,
									CTRY_CD,
									CIS_LVL_TYPE,
									FSA_MDUL_ID,
									HIER_LVL_CD
								) output inserted.ORDR_ADR_ID into @tmpids
								SELECT	@FSA_ORDR_ID,
										CASE CIS_LVL_TYPE
											WHEN 'H4' THEN 3
											WHEN 'H6' THEN 18
											WHEN 'H5' THEN 18
											ELSE 17
										END,
										GETDATE(),
										1,
										1,
										CTRY_CD,
										CIS_LVL_TYPE,
										'CIS',
										CASE CIS_LVL_TYPE	
											WHEN 'H4' THEN 0
											WHEN 'H1' THEN 0
											ELSE 1
										END
									FROM #MACH5_ORDR_CHNG_CUST
									WHERE ISNULL(CSG_LVL,'')!=''
							
							INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
												STREET_ADR_2,
												CTY_NME,
												STT_PRVN_NME,
												STT_CD,
												ZIP_PSTL_CD,
												BLDG_NME,
												FLR_ID,
												RM_NBR,
												STREET_ADR_3)
											SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.STREET_ADR_1),
													dbo.encryptString(moc.STREET_ADR_2),
													dbo.encryptString(moc.CTY_NME),
													dbo.encryptString(moc.PRVN_NME),
													dbo.encryptString(moc.STT_CD),
													dbo.encryptString(moc.ZIP_PSTL_CD),
													dbo.encryptString(moc.BLDG_ID),
													dbo.encryptString(moc.FLOOR_ID),
													dbo.encryptString(moc.ROOM_ID),
													dbo.encryptString(moc.STREET_ADR_3)
												FROM #MACH5_ORDR_CHNG_CUST moc WITH (NOLOCK)
												INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND oa.ORDR_ID = @FSA_ORDR_ID
												INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
												WHERE ISNULL(moc.CSG_LVL,'')!=''
												  AND oa.STREET_ADR_1 IS NULL
							
							delete from @tmpids
							------ORDR_CNTCT
							INSERT INTO dbo.ORDR_CNTCT
								(
									ORDR_ID,
									CNTCT_TYPE_ID,
									PHN_NBR,
									TME_ZONE_ID,
									CREAT_DT,
									CREAT_BY_USER_ID,
									REC_STUS_ID,
									FAX_NBR,
									ROLE_ID,
									CIS_LVL_TYPE,
									FSA_MDUL_ID,
									FRST_NME,
									LST_NME,
									EMAIL_ADR,
									CNTCT_HR_TXT,
									NPA,
									NXX,
									STN_NBR,
									CTY_CD,
									ISD_CD,
									CNTCT_NME,
									PHN_EXT_NBR
								)
							(
								SELECT	@FSA_ORDR_ID ,
										CASE c.ROLE_CD
												WHEN 'Hrchy' THEN 17
												ELSE 1
											END AS CNTCT_TYPE_ID,
										c.PHN_NBR,
										l.TME_ZONE_ID,
										GETDATE(),
										1,
										1,
										'',
										lr.ROLE_ID,
										c.CIS_LVL_TYPE,
										c.FSA_MDUL_ID,
										c.FRST_NME,
										c.LST_NME,
										c.EMAIL_ADR,
										c.CNTCT_HR_TXT,
										c.NPA,
										c.NXX,
										c.STN_NBR,
										c.CTY_CD,
										c.ISD_CD,
										c.CNTCT_NME,
										c.PHN_EXT_NBR
									FROM #MACH5_ORDR_CHNG_CNTCT c
								INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD	
								LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
													TME_ZONE_NME	AS TME_ZONE_NME 
												FROM dbo.LK_TME_ZONE 
												GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
								WHERE ISNULL(CSG_LVL,'')=''
							)
							
							INSERT INTO dbo.ORDR_CNTCT
								(
									ORDR_ID,
									CNTCT_TYPE_ID,
									PHN_NBR,
									TME_ZONE_ID,
									CREAT_DT,
									CREAT_BY_USER_ID,
									REC_STUS_ID,
									FAX_NBR,
									ROLE_ID,
									CIS_LVL_TYPE,
									FSA_MDUL_ID,
									CNTCT_HR_TXT,
									NPA,
									NXX,
									STN_NBR,
									CTY_CD,
									ISD_CD,
									PHN_EXT_NBR
								) output inserted.ORDR_CNTCT_ID into @tmpids							
								SELECT	@FSA_ORDR_ID ,
										CASE c.ROLE_CD
												WHEN 'Hrchy' THEN 17
												ELSE 1
											END AS CNTCT_TYPE_ID,
										c.PHN_NBR,
										l.TME_ZONE_ID,
										GETDATE(),
										1,
										1,
										'',
										lr.ROLE_ID,
										c.CIS_LVL_TYPE,
										c.FSA_MDUL_ID,
										c.CNTCT_HR_TXT,
										c.NPA,
										c.NXX,
										c.STN_NBR,
										c.CTY_CD,
										c.ISD_CD,
										c.PHN_EXT_NBR
									FROM #MACH5_ORDR_CHNG_CNTCT c
								INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD	
								LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
													TME_ZONE_NME	AS TME_ZONE_NME 
												FROM dbo.LK_TME_ZONE 
												GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
								WHERE ISNULL(CSG_LVL,'')!=''
							
							
							INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, FRST_NME,
												LST_NME,
												CUST_EMAIL_ADR,
												CUST_CNTCT_NME)
											SELECT	DISTINCT oc.ORDR_CNTCT_ID, 15, dbo.encryptstring(c.FRST_NME),
													dbo.encryptstring(c.LST_NME),
													dbo.encryptstring(c.EMAIL_ADR),
													dbo.encryptstring(c.CNTCT_NME)
												FROM #MACH5_ORDR_CHNG_CNTCT c WITH (NOLOCK)
												INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
											INNER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ROLE_ID=lr.ROLE_ID AND oc.CIS_LVL_TYPE=c.CIS_LVL_TYPE AND oc.ORDR_ID = @FSA_ORDR_ID
											INNER JOIN @tmpids tmpids ON tmpids.tmpid = oc.ORDR_CNTCT_ID
											WHERE ISNULL(c.CSG_LVL,'')!=''
											  AND oc.FRST_NME IS NULL
							
							-----ORDER NOTES
							IF EXISTS
								(
									SELECT 'X'
										FROM #MACH5_ORDR_CHNG_NOTES WITH (NOLOCK)
								)
							BEGIN
								INSERT INTO ORDR_NTE
								(
									NTE_TYPE_ID,
									ORDR_ID,
									CREAT_DT,
									CREAT_BY_USER_ID,
									REC_STUS_ID,
									NTE_TXT
								)
								(
									SELECT 
										CASE NTE_TYPE_CD
											WHEN 'CPEN' THEN 26
											ELSE 25
										END, 
										@FSA_ORDR_ID,		
										GETDATE(),
										1,
										1,
										NTE_TXT		
									FROM #MACH5_ORDR_CHNG_NOTES
									WHERE NTE_TXT IS NOT NULL
								)
							END
								
						--Update Region ID for xNCI Orders
							Update od
								SET od.RGN_ID = ISNULL(lc.RGN_ID,od.RGN_ID)
								FROM dbo.ORDR od WITH (ROWLOCK)
							INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON od.ORDR_ID = oa.ORDR_ID
							INNER JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD 
								WHERE od.ORDR_ID = @ORDR_ID
									AND oa.CIS_LVL_TYPE = 'H6'
									
						IF @CHNG_ORDR_TYPE_FLG != 1
							BEGIN
								----State Machine
									INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('InsertInitialTask:'+CONVERT(VARCHAR,@ORDR_ID)+@TransName,'insertMach5TxnDetails', GETDATE())
									SET @ERRLog = @ERRLog + 'InsertInitialTask:'+CONVERT(VARCHAR,@ORDR_ID)
									Exec dbo.InsertInitialTask @ORDR_ID,@OrderCategoryID
								--SELECT @ORDR_ID	
							END

							Exec dbo.updateNRMBPMData @ORDR_ID
						
						
							UPDATE #MACH5_ORDR_CHNG_CMPNT
								SET	FLAG = 1
								WHERE CMPNT_TYPE_CD IN ('ACCS','PORT')
							
						END
						
						IF EXISTS
						(
							SELECT 'X'
								FROM #MACH5_ORDR_CHNG_CMPNT
								WHERE CMPNT_TYPE_CD IN ('CPE','OCPE','CPEP','CPEM','CPES')
						)
						BEGIN
							
						
							SELECT @INTL_FLG = CASE DOM_PROCESS_FLG_CD
													WHEN 'N' THEN 1
													ELSE 0
												END,
									@M5_PROD_ID = PROD_ID
								FROM #MACH5_ORDR_CHNG WITH (NOLOCK)
								
							--For testing, updating Device ID 
							IF	@INTL_FLG = 0
								BEGIN
									SELECT	@Cnt = COUNT(1) 
										FROM	#MACH5_ORDR_CHNG_CMPNT 
										WHERE	CMPNT_TYPE_CD IN ('CPE','OCPE','CPEM','CPEP','CPES')
									
									
									IF @Cnt > 0
										BEGIN
											WHILE @Ctr < @Cnt 
												BEGIN
													SET @CPE_DEVICE_ID = ''
													SET @STUS_CD = ''
													SET @Continue = 'Y'
													SELECT TOP 1 
														@CPE_DEVICE_ID = ISNULL(CPE_DEVICE_ID,''),
														@STUS_CD = ISNULL(STUS_CD,'')
														FROM #MACH5_ORDR_CHNG_CMPNT
														WHERE FLAG = 0
														
													SELECT @CPE_DEVICE_ID AS CPE_DEVICE_ID
													
													SELECT @Continue
													IF @Continue = 'Y'
														BEGIN
															IF ISNULL(@CPE_DEVICE_ID,'') != '' 
																BEGIN
																	INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('Dmstc-CPE:'+@TransName,'insertMach5TxnDetails', GETDATE())
																	SET @ERRLog = @ERRLog + 'Dmstc-CPE:'
																	OPEN SYMMETRIC KEY FS@K3y 
																	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
																	--default region ID to AMNCI and later modified after order insertion in this SP
																	INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID,DMSTC_CD,PROD_ID)	
																		SELECT @OrderCategoryID, 1, 1, 0,PROD_ID
																			FROM #MACH5_ORDR_CHNG WITH (NOLOCK)
																			WHERE M5_ORDR_ID = @M5_ORDR_ID
																									
																	SELECT @ORDR_ID = SCOPE_IDENTITY()
																	SET @FSA_ORDR_ID = @ORDR_ID
																	
																	IF @QDA_ORDR_ID != ''
																		SET @QDA_ORDR_ID = @QDA_ORDR_ID + ',' + CONVERT(varchar,@FSA_ORDR_ID)
																	ELSE
																		SET @QDA_ORDR_ID = CONVERT(varchar,@FSA_ORDR_ID)
																	
																	UPDATE #MACH5_ORDR_CHNG_CMPNT
																		SET		QDA_ORDR_ID = @FSA_ORDR_ID
																		WHERE	CPE_DEVICE_ID = @CPE_DEVICE_ID
																	
																	UPDATE		odr
																		SET		PRNT_ORDR_ID	=	@ORDR_ID,
																				RAS_DT			=	t.CPE_REQD_ON_SITE_DT,
																				DLVY_CLLI		=	t.CPE_SHIP_CLLI_CD,
																				H5_H6_CUST_ID	=	@H5_H6_CUST_ID,
																				CSG_LVL_ID		=	(SELECT TOP 1 ISNULL(lcl.CSG_LVL_ID,0) FROM #MACH5_ORDR_CHNG mo WITH (NOLOCK) LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=mo.CSG_LVL WHERE mo.M5_ORDR_ID = @M5_ORDR_ID),
																				CHARS_ID		=	(SELECT TOP 1 CHARS_CUST_ID FROM #MACH5_ORDR_CHNG WITH (NOLOCK) WHERE M5_ORDR_ID = @M5_ORDR_ID),
																				PPRT_ID         =   @PPRT_ID
																		FROM	dbo.ORDR	odr WITH (ROWLOCK) 
																		INNER JOIN #MACH5_ORDR_CHNG_CMPNT t WITH (NOLOCK) ON odr.ORDR_ID = t.QDA_ORDR_ID
																		WHERE	ORDR_ID			=	@ORDR_ID
																			AND t.CPE_DEVICE_ID = @CPE_DEVICE_ID
																		
																																
																	--FSA_ORDR
																	INSERT INTO FSA_ORDR (ORDR_ID,ORDR_ACTN_ID,FTN,ORDR_TYPE_CD,ORDR_SUB_TYPE_CD,PROD_TYPE_CD,CUST_SIGNED_DT,CUST_WANT_DT,
																							CUST_ORDR_SBMT_DT, CUST_CMMT_DT,CUST_PRMS_OCPY_CD,CUST_ACPT_ERLY_SRVC_CD,MULT_CUST_ORDR_CD,
																							DISC_REAS_CD, INSTL_ESCL_CD,FSA_EXP_TYPE_CD,PRE_QUAL_NBR,PRNT_FTN,RELTD_FTN,
																							CPE_ACCS_PRVDR_CD,CPE_DLVRY_DUTY_AMT, CPE_DLVRY_DUTY_ID,CPE_ECCKT_ID,CPE_TST_TN_NBR,CPE_REC_ONLY_CD,
																							CPE_PHN_NBR, CPE_EQPT_ONLY_CD                         
														
																							)
																	(
																		SELECT	TOP 1 @FSA_ORDR_ID,@ORDR_ACTN_ID,m.ORDER_NBR,m.ORDR_TYPE_CD,m.ORDR_SUBTYPE_CD,'CP',m.CUST_SIGN_DT,
																				m.CUST_WANT_DT,m.CUST_SBMT_DT,m.CUST_CMMT_DT,m.CUST_PREM_OCCU_FLG_CD,m.CUST_ACPT_SRVC_FLG_CD,m.MULT_ORDR_SBMTD_FLG_CD,
																				m.DSCNCT_REAS_CD,m.EXP_FLG_CD,	CASE m.EXP_TYPE_CD 
																																	WHEN 'B' THEN 'IE'
																																	ELSE m.EXP_TYPE_CD
																																	END
																				,m.PRE_QUAL_NBR
																				,CASE 
																					WHEN m.ORDR_TYPE_CD ='CN' THEN m.PRNT_FTN
																					WHEN m.ORDR_SUBTYPE_CD = 'ISMV' THEN m.PRNT_FTN
																					ELSE null
																				  END
																				,CASE m.ORDR_TYPE_CD
																					WHEN 'CN' THEN NULL
																					ELSE m.RLTD_ORDR_ID
																				  END
																				,c.CPE_ACCS_PRVDR_NME
																				,c.CPE_DLVRY_DUTIES_AMT
																				,c.CPE_DLVRY_DUTIES_CD
																				,c.CPE_EQPT_CKT_ID
																				,c.CPE_SPRINT_TEST_TN_NBR
																				,c.CPE_RECORD_ONLY_FLG_CD
																				,c.CPE_ACCS_PRVDR_TN_NBR
																				,c.CPE_EQUIPMENT_ONLY_FLG_CD
																			FROM #MACH5_ORDR_CHNG m WITH (NOLOCK)
																			LEFT JOIN #MACH5_ORDR_CHNG_CMPNT c WITH (NOLOCK) ON m.M5_ORDR_ID = c.M5_ORDR_ID AND c.CMPNT_TYPE_CD = 'OCPE'
																			WHERE m.M5_ORDR_ID = @M5_ORDR_ID
																				
																			
																	)
																	
																	
																	--ORDR_EXP	
																	IF EXISTS
																	(
																		SELECT 'X'
																			FROM #MACH5_ORDR_CHNG
																			WHERE EXP_FLG_CD = 'Y'
																	)
																	BEGIN
																		INSERT INTO ORDR_EXP (ORDR_ID,
																								EXP_TYPE_ID,
																								AUTHRZR_PHN_NBR,
																								AUTHRZR_TITLE_ID,
																								AUTHN_DT,
																								BILL_TO_COST_CTR_CD,
																								CREAT_DT,
																								CREAT_BY_USER_ID,
																								REC_STUS_ID
																									)
																		(
																			SELECT TOP 1	@FSA_ORDR_ID
																							,CASE EXP_TYPE_CD
																									WHEN 'I' THEN 1
																									WHEN 'E' THEN 2
																									WHEN 'B' THEN 3
																									ELSE 4 
																								END AS EXP_TYPE_ID
																							,APRVR_PHN
																							,APRVR_TITLE
																							,APPRVL_DT
																							,[dbo].[RemoveSpecialCharacters](LEFT(SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10),
           PATINDEX('%[^0-9]%', SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10) + 'X') -1))
																							,GETDATE()
																							,1
																							,1
																				FROM #MACH5_ORDR_CHNG WITH (NOLOCK)
																				
																		)
																	END
																	
																	delete from @tmpids
																	-----FSA_ORDR_CUST
																	INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CUST_NME,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID)								
																		(SELECT	@FSA_ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				GETDATE(),
																				CUST_NME,
																				ISNULL(BILL_CYC_ID,0),
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
																			WHERE ISNULL(CSG_LVL,'')=''
																		)
																	
																	INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
																		SELECT	@FSA_ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				GETDATE(),
																				ISNULL(BILL_CYC_ID,0),
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
																			WHERE ISNULL(CSG_LVL,'')!=''
																		
																		
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																	SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString(moc.CUST_NME)
																			FROM #MACH5_ORDR_CHNG_CUST moc WITH (NOLOCK)
																			INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																			INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																			WHERE ISNULL(moc.CSG_LVL,'')!=''
																			  AND foc.CUST_NME IS NULL
																	
																	delete from @tmpids
																	IF NOT EXISTS
																		(
																			SELECT 'X'
																				FROM dbo.FSA_ORDR_CUST WITH (NOLOCK)
																				WHERE ORDR_ID = @ORDR_ID
																					AND CIS_LVL_TYPE = 'H1'
																		)
																		BEGIN
																			INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CUST_NME,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID)									
																		(SELECT	@FSA_ORDR_ID,
																				'H1',
																				H1_ID,
																				'',
																				'',
																				GETDATE(),
																				'',
																				BILL_CYC_ID,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
																			WHERE CIS_LVL_TYPE = 'H6'
																			  AND ISNULL(CSG_LVL,'')=''
																		)	
																		
																		INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
																		SELECT	@FSA_ORDR_ID,
																				'H1',
																				H1_ID,
																				'',
																				'',
																				GETDATE(),
																				BILL_CYC_ID,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
																			WHERE CIS_LVL_TYPE = 'H6'
																			  AND ISNULL(CSG_LVL,'')!=''
																			
																		
																		INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																		SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString('')
																			FROM #MACH5_ORDR_CHNG_CUST moc WITH (NOLOCK)
																			INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																			INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																			WHERE ISNULL(moc.CSG_LVL,'')!=''
																			 AND moc.CIS_LVL_TYPE = 'H6'
																			 AND foc.CUST_NME IS NULL
																		END
																	
																	delete from @tmpids
																	----ORDR_ADR
																	INSERT INTO dbo.ORDR_ADR
																		(
																			ORDR_ID,
																			ADR_TYPE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			CTRY_CD,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			STREET_ADR_1,
																			STREET_ADR_2,
																			CTY_NME,
																			PRVN_NME,
																			STT_CD,
																			ZIP_PSTL_CD,
																			BLDG_NME,
																			FLR_ID,
																			RM_NBR,
																			HIER_LVL_CD,
																			STREET_ADR_3
																		)
																	(
																		SELECT	@FSA_ORDR_ID,
																				CASE CIS_LVL_TYPE
																					WHEN 'H4' THEN 3
																					WHEN 'H6' THEN 18
																					WHEN 'H5' THEN 18
																					ELSE 17
																				END,
																				GETDATE(),
																				1,
																				1,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				'CIS',
																				STREET_ADR_1,
																				STREET_ADR_2,
																				CTY_NME,
																				PRVN_NME,
																				STT_CD,
																				ZIP_PSTL_CD,
																				BLDG_ID,
																				FLOOR_ID,
																				ROOM_ID,
																				CASE CIS_LVL_TYPE	
																					WHEN 'H4' THEN 0
																					WHEN 'H1' THEN 0
																					ELSE 1
																				END,
																				STREET_ADR_3
																			FROM #MACH5_ORDR_CHNG_CUST
																			WHERE ISNULL(CSG_LVL,'')=''
																	)
																	
																	INSERT INTO dbo.ORDR_ADR
																		(
																			ORDR_ID,
																			ADR_TYPE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			CTRY_CD,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			HIER_LVL_CD
																		) output inserted.ORDR_ADR_ID into @tmpids																	
																		SELECT	@FSA_ORDR_ID,
																				CASE CIS_LVL_TYPE
																					WHEN 'H4' THEN 3
																					WHEN 'H6' THEN 18
																					WHEN 'H5' THEN 18
																					ELSE 17
																				END,
																				GETDATE(),
																				1,
																				1,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				'CIS',
																				CASE CIS_LVL_TYPE	
																					WHEN 'H4' THEN 0
																					WHEN 'H1' THEN 0
																					ELSE 1
																				END
																			FROM #MACH5_ORDR_CHNG_CUST
																			WHERE ISNULL(CSG_LVL,'')!=''
																	
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
																		STREET_ADR_2,
																		CTY_NME,
																		STT_PRVN_NME,
																		STT_CD,
																		ZIP_PSTL_CD,
																		BLDG_NME,
																		FLR_ID,
																		RM_NBR,
																		STREET_ADR_3)
																	SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.STREET_ADR_1),
																			dbo.encryptString(moc.STREET_ADR_2),
																			dbo.encryptString(moc.CTY_NME),
																			dbo.encryptString(moc.PRVN_NME),
																			dbo.encryptString(moc.STT_CD),
																			dbo.encryptString(moc.ZIP_PSTL_CD),
																			dbo.encryptString(moc.BLDG_ID),
																			dbo.encryptString(moc.FLOOR_ID),
																			dbo.encryptString(moc.ROOM_ID),
																			dbo.encryptString(moc.STREET_ADR_3)
																		FROM #MACH5_ORDR_CHNG_CUST moc WITH (NOLOCK)
																		INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND oa.ORDR_ID = @FSA_ORDR_ID
																		INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
																		WHERE ISNULL(moc.CSG_LVL,'')!=''
																		  AND oa.STREET_ADR_1 IS NULL
																	
																	delete from @tmpids
																	IF EXISTS
																	(
																		SELECT 'x'
																			FROM #MACH5_ORDR_CHNG_CMPNT
																			WHERE	M5_ORDR_ID = @M5_ORDR_ID
																				AND	CPE_DEVICE_ID = @CPE_DEVICE_ID
																				AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0 	
																	)
																		BEGIN
																			INSERT INTO dbo.ORDR_ADR
																			(
																				ORDR_ID,
																				ADR_TYPE_ID,
																				CREAT_DT,
																				CREAT_BY_USER_ID,
																				REC_STUS_ID,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				FSA_MDUL_ID,
																				STREET_ADR_1,
																				STREET_ADR_2,
																				CTY_NME,
																				PRVN_NME,
																				STT_CD,
																				ZIP_PSTL_CD,
																				BLDG_NME,
																				FLR_ID,
																				RM_NBR,
																				HIER_LVL_CD,
																				STREET_ADR_3
																			)
																		(
																			SELECT	TOP 1 @FSA_ORDR_ID,
																					19, -- Shipping Address Type
																					GETDATE(),
																					1,
																					1,
																					CPE_ALT_SHIP_ADR_CTRY_CD,
																					(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CHNG_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')),
																					'CIS',
																					CPE_ALT_SHIP_ADR_LINE_1,
																					CPE_ALT_SHIP_ADR_LINE_2,
																					CPE_ALT_SHIP_ADR_CTY_NME,
																					CPE_ALT_SHIP_ADR_PRVN_NME,
																					CPE_ALT_SHIP_ADR_ST_PRVN_CD,
																					CPE_ALT_SHIP_ADR_PSTL_CD,
																					'',
																					'',
																					'',
																					1,
																					CPE_ALT_SHIP_ADR_LINE_3
																				FROM #MACH5_ORDR_CHNG_CMPNT
																				WHERE	M5_ORDR_ID = @M5_ORDR_ID
																					AND	CPE_DEVICE_ID = @CPE_DEVICE_ID
																					AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0
																					AND ISNULL(CSG_LVL,'')=''
																		)
																		
																		INSERT INTO dbo.ORDR_ADR
																			(
																				ORDR_ID,
																				ADR_TYPE_ID,
																				CREAT_DT,
																				CREAT_BY_USER_ID,
																				REC_STUS_ID,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				FSA_MDUL_ID,
																				HIER_LVL_CD
																			) output inserted.ORDR_ADR_ID into @tmpids
																			SELECT	TOP 1 @FSA_ORDR_ID,
																					19, -- Shipping Address Type
																					GETDATE(),
																					1,
																					1,
																					CPE_ALT_SHIP_ADR_CTRY_CD,
																					(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CHNG_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')),
																					'CIS',
																					1
																				FROM #MACH5_ORDR_CHNG_CMPNT
																				WHERE	M5_ORDR_ID = @M5_ORDR_ID
																					AND	CPE_DEVICE_ID = @CPE_DEVICE_ID
																					AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0
																					AND ISNULL(CSG_LVL,'')!=''

																		
																		INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
																		STREET_ADR_2,
																		CTY_NME,
																		STT_PRVN_NME,
																		STT_CD,
																		ZIP_PSTL_CD,
																		BLDG_NME,
																		FLR_ID,
																		RM_NBR,
																		STREET_ADR_3)
																	SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_1),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_2),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_CTY_NME),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_PRVN_NME),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_ST_PRVN_CD),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_PSTL_CD),
																			dbo.encryptString(''),
																			dbo.encryptString(''),
																			dbo.encryptString(''),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_3)
																		FROM #MACH5_ORDR_CHNG_CMPNT moc WITH (NOLOCK)
																		INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.ADR_TYPE_ID = 19 AND oa.CIS_LVL_TYPE=(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CHNG_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')) AND oa.ORDR_ID = @FSA_ORDR_ID
																		INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
																		WHERE ISNULL(moc.CSG_LVL,'')!=''
																		  AND moc.M5_ORDR_ID = @M5_ORDR_ID
																		  AND moc.CPE_DEVICE_ID = @CPE_DEVICE_ID
																		  AND ISNULL(moc.CPE_ALT_SHIP_ADR,0) != 0
																		  AND oa.STREET_ADR_1 IS NULL
																		END
																	
																	delete from @tmpids
																	------ORDR_CNTCT
																	INSERT INTO dbo.ORDR_CNTCT
																		(
																			ORDR_ID,
																			CNTCT_TYPE_ID,
																			PHN_NBR,
																			TME_ZONE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			FAX_NBR,
																			ROLE_ID,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			FRST_NME,
																			LST_NME,
																			EMAIL_ADR,
																			CNTCT_HR_TXT,
																			NPA,
																			NXX,
																			STN_NBR,
																			CTY_CD,
																			ISD_CD,
																			CNTCT_NME,
																			PHN_EXT_NBR
																		)
																	(
																		SELECT	@FSA_ORDR_ID ,
																				CASE c.ROLE_CD
																					WHEN 'Hrchy' THEN 17
																					ELSE 1
																				END AS CNTCT_TYPE_ID,
																				c.PHN_NBR,
																				l.TME_ZONE_ID,
																				GETDATE(),
																				1,
																				1,
																				'',
																				lr.ROLE_ID,
																				c.CIS_LVL_TYPE,
																				c.FSA_MDUL_ID,
																				c.FRST_NME,
																				c.LST_NME,
																				c.EMAIL_ADR,
																				c.CNTCT_HR_TXT,
																				c.NPA,
																				c.NXX,
																				c.STN_NBR,
																				c.CTY_CD,
																				c.ISD_CD,
																				c.CNTCT_NME,
																				c.PHN_EXT_NBR
																			FROM #MACH5_ORDR_CHNG_CNTCT c
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = c.ROLE_CD
																		LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																							TME_ZONE_NME	AS TME_ZONE_NME 
																						FROM dbo.LK_TME_ZONE 
																						GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
																		WHERE ISNULL(CSG_LVL,'')=''
																	)
																	
																	INSERT INTO dbo.ORDR_CNTCT
																		(
																			ORDR_ID,
																			CNTCT_TYPE_ID,
																			PHN_NBR,
																			TME_ZONE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			FAX_NBR,
																			ROLE_ID,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			CNTCT_HR_TXT,
																			NPA,
																			NXX,
																			STN_NBR,
																			CTY_CD,
																			ISD_CD,
																			PHN_EXT_NBR
																		) output inserted.ORDR_CNTCT_ID into @tmpids
																		SELECT	@FSA_ORDR_ID ,
																				CASE c.ROLE_CD
																					WHEN 'Hrchy' THEN 17
																					ELSE 1
																				END AS CNTCT_TYPE_ID,
																				c.PHN_NBR,
																				l.TME_ZONE_ID,
																				GETDATE(),
																				1,
																				1,
																				'',
																				lr.ROLE_ID,
																				c.CIS_LVL_TYPE,
																				c.FSA_MDUL_ID,
																				c.CNTCT_HR_TXT,
																				c.NPA,
																				c.NXX,
																				c.STN_NBR,
																				c.CTY_CD,
																				c.ISD_CD,
																				c.PHN_EXT_NBR
																			FROM #MACH5_ORDR_CHNG_CNTCT c
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = c.ROLE_CD
																		LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																							TME_ZONE_NME	AS TME_ZONE_NME 
																						FROM dbo.LK_TME_ZONE 
																						GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
																		WHERE ISNULL(CSG_LVL,'')!=''		
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, FRST_NME,
																		LST_NME,
																		CUST_EMAIL_ADR,
																		CUST_CNTCT_NME)
																	SELECT	DISTINCT oc.ORDR_CNTCT_ID, 15, dbo.encryptstring(c.FRST_NME),
																			dbo.encryptstring(c.LST_NME),
																			dbo.encryptstring(c.EMAIL_ADR),
																			dbo.encryptstring(c.CNTCT_NME)
																		FROM #MACH5_ORDR_CHNG_CNTCT c WITH (NOLOCK)
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
																	INNER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ROLE_ID=lr.ROLE_ID AND oc.CIS_LVL_TYPE=c.CIS_LVL_TYPE AND oc.ORDR_ID = @FSA_ORDR_ID AND oc.FRST_NME IS NULL
																	INNER JOIN @tmpids tmpids ON tmpids.tmpid = oc.ORDR_CNTCT_ID
																	WHERE ISNULL(c.CSG_LVL,'')!=''
																	  AND oc.FRST_NME IS NULL
																	
																	-----ORDER NOTES
																	IF EXISTS
																		(
																			SELECT 'X'
																				FROM #MACH5_ORDR_CHNG_NOTES WITH (NOLOCK)
																		)
																	BEGIN
																		INSERT INTO ORDR_NTE
																		(
																			NTE_TYPE_ID,
																			ORDR_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			NTE_TXT
																		)
																		(
																			SELECT 
																				CASE NTE_TYPE_CD
																					WHEN 'CPEN' THEN 26
																					ELSE 25
																				END, 
																				@FSA_ORDR_ID,		
																				GETDATE(),
																				1,
																				1,
																				NTE_TXT		
																			FROM #MACH5_ORDR_CHNG_NOTES
																			WHERE NTE_TXT IS NOT NULL
																		)
																	END
																
																	--FSA_ORDR_CPE_LINE_ITEM
																	INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
																		(
																			ORDR_ID,
																			EQPT_TYPE_ID, --EQPT_TYPE_CD 
																			EQPT_ID, --EQPT_ID
																			MFR_NME, --CPE_MFR_NME 
																			CNTRC_TYPE_ID, --CPE_CONTRACT_TYPE
																			CNTRC_TERM_ID, --CPE_CONTRACT_TERM 
																			MNTC_CD, --CPE_MNGD_FLG_CD 
																			CREAT_DT,
																			CMPNT_ID, --CPE_ITEM_ID 
																			LINE_ITEM_CD,
																			MANF_PART_CD, --CPE_MFR_PART_NBR 
																			UNIT_PRICE, --CPE_LIST_PRICE 
																			DROP_SHP, --CPE_DROP_SHIP_FLG_CD 
																			DEVICE_ID, --CPE_DEVICE_ID 
																			ORDR_CMPNT_ID,
																			ITM_STUS,
																			ORDR_QTY,
																			MDS_DES,
																			MANF_DISCNT_CD,
																			MATL_CD,
																			CMPNT_FMLY,
																			RLTD_CMPNT_ID,
																			STUS_CD,
																			MFR_PS_ID,
																			CPE_REUSE_CD,
																			SPRINT_MNTD_FLG															
																		)
																		SELECT	@FSA_ORDR_ID,
																				CPE_ITEM_TYPE_CD,
																				EQPT_ID,
																				SUBSTRING(ISNULL(CPE_MFR_ID,''),1,50) AS CPE_MFR_NME,
																				CPE_CONTRACT_TYPE,
																				CONVERT(VARCHAR,ISNULL(CPE_CONTRACT_TERM,0)) AS CPE_CONTRACT_TERM,
																				CPE_MNGD_FLG_CD,
																				GETDATE(),
																				NVTRY_ID,
																				'CPE' AS LINE_ITEM_CD,
																				SUBSTRING(ISNULL(CPE_MFR_PART_NBR,''),1,20) AS CPE_MFR_PART_NBR,
																				CPE_LIST_PRICE,
																				CPE_DROP_SHIP_FLG_CD,
																				CPE_DEVICE_ID,
																				NVTRY_ID,
																				CASE STUS_CD
																					WHEN 'CN' THEN 402
																					ELSE 401
																				END AS ITM_STUS,
																				QTY,
																				SUBSTRING(ISNULL(NME,''),1,50) AS NME,
																				CPE_VNDR_DSCNT, 
																				CPE_ITEM_MSTR_NBR,
																				CPE_CMPN_FMLY,
																				RLTD_CMPNT_ID,
																				STUS_CD,
																				CPE_MFR_PS_ID,
																				0 AS CPE_REUSE_CD,
																				CPE_SPRINT_MNT_FLG_CD
																			FROM #MACH5_ORDR_CHNG_CMPNT
																			WHERE	M5_ORDR_ID = @M5_ORDR_ID
																				AND	CPE_DEVICE_ID = @CPE_DEVICE_ID	
																	
																	--Update Region ID for xNCI Orders
																	Update od
																		SET od.RGN_ID = ISNULL(lc.RGN_ID,od.RGN_ID)
																		FROM dbo.ORDR od WITH (ROWLOCK)
																	INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON od.ORDR_ID = oa.ORDR_ID
																	INNER JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD 
																		WHERE od.ORDR_ID = @ORDR_ID
																			AND oa.CIS_LVL_TYPE = 'H6'
																			
																	IF @CHNG_ORDR_TYPE_FLG != 1
																		BEGIN
																			----State Machine
																				INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('InsertM5TxnTask:'+CONVERT(VARCHAR,@ORDR_ID)+@TransName,'insertMach5TxnDetails', GETDATE())
																				SET @ERRLog = @ERRLog + 'InsertM5TxnTask:'+CONVERT(VARCHAR,@ORDR_ID)
																				Exec dbo.InsertM5TxnTask @ORDR_ID,@ORDR_TYPE_CD
																			--SELECT @ORDR_ID	
																		END
															
																UPDATE #MACH5_ORDR_CHNG_CMPNT
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
															
																
															END	
														ELSE
															BEGIN
																UPDATE #MACH5_ORDR_CHNG_CMPNT
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID is null
															END	
														END
													ELSE
														BEGIN
															UPDATE #MACH5_ORDR_CHNG_CMPNT
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
														END
													
												SET @Ctr = @Ctr + 1		
													
												END
										END
												
								END
							ELSE 
								BEGIN
									OPEN SYMMETRIC KEY FS@K3y 
									DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
									INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('Intl-CPE:'+@TransName,'insertMach5TxnDetails', GETDATE())
									SET @ERRLog = @ERRLog + 'Intl-CPE:'
									--default region ID to AMNCI and later modified after order insertion in this SP
									INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID,DMSTC_CD,PROD_ID)	
										SELECT @OrderCategoryID, 1, 1, CASE DOM_PROCESS_FLG_CD
																			WHEN 'N' THEN 1
																			ELSE 0
																			END,
												PROD_ID
										FROM #MACH5_ORDR_CHNG WITH (NOLOCK)
										WHERE M5_ORDR_ID = @M5_ORDR_ID
																	
									SELECT @ORDR_ID = SCOPE_IDENTITY()
									SET @FSA_ORDR_ID = @ORDR_ID
									
									IF @QDA_ORDR_ID != ''
										SET @QDA_ORDR_ID = @QDA_ORDR_ID + ',' + CONVERT(varchar,@FSA_ORDR_ID)
									ELSE
										SET @QDA_ORDR_ID = CONVERT(varchar,@FSA_ORDR_ID)
									
									UPDATE #MACH5_ORDR_CHNG_CMPNT
									SET		QDA_ORDR_ID = @FSA_ORDR_ID
									
								
									UPDATE		odr
										SET		PRNT_ORDR_ID	=	@ORDR_ID,
												RAS_DT			=	t.CPE_REQD_ON_SITE_DT,
												DLVY_CLLI		=	t.CPE_SHIP_CLLI_CD,
												H5_H6_CUST_ID	=	@H5_H6_CUST_ID,
												CSG_LVL_ID		=	(SELECT TOP 1 ISNULL(lcl.CSG_LVL_ID,0) FROM #MACH5_ORDR_CHNG mo WITH (NOLOCK) LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=mo.CSG_LVL WHERE mo.M5_ORDR_ID = @M5_ORDR_ID),
												CHARS_ID		=	(SELECT TOP 1 CHARS_CUST_ID FROM #MACH5_ORDR_CHNG WITH (NOLOCK) WHERE M5_ORDR_ID = @M5_ORDR_ID)
										FROM	dbo.ORDR	odr WITH (ROWLOCK) 
										INNER JOIN #MACH5_ORDR_CHNG_CMPNT t WITH (NOLOCK) ON odr.ORDR_ID = t.QDA_ORDR_ID
										WHERE	odr.ORDR_ID			=	@ORDR_ID
											
										
									--FSA_ORDR
									INSERT INTO FSA_ORDR (ORDR_ID,ORDR_ACTN_ID,FTN,ORDR_TYPE_CD,ORDR_SUB_TYPE_CD,PROD_TYPE_CD,CUST_SIGNED_DT,CUST_WANT_DT,
															CUST_ORDR_SBMT_DT, CUST_CMMT_DT,CUST_PRMS_OCPY_CD,CUST_ACPT_ERLY_SRVC_CD,MULT_CUST_ORDR_CD,
															DISC_REAS_CD, INSTL_ESCL_CD,FSA_EXP_TYPE_CD,PRE_QUAL_NBR,PRNT_FTN,RELTD_FTN,
															CPE_ACCS_PRVDR_CD,CPE_DLVRY_DUTY_AMT, CPE_DLVRY_DUTY_ID,CPE_ECCKT_ID,CPE_TST_TN_NBR, CPE_REC_ONLY_CD,
															CPE_PHN_NBR,CPE_EQPT_ONLY_CD
															)
									(
										SELECT	TOP 1 @FSA_ORDR_ID,@ORDR_ACTN_ID,m.ORDER_NBR,m.ORDR_TYPE_CD,m.ORDR_SUBTYPE_CD,'CP',m.CUST_SIGN_DT,
												m.CUST_WANT_DT,m.CUST_SBMT_DT,m.CUST_CMMT_DT,m.CUST_PREM_OCCU_FLG_CD,m.CUST_ACPT_SRVC_FLG_CD,m.MULT_ORDR_SBMTD_FLG_CD,
												m.DSCNCT_REAS_CD,m.EXP_FLG_CD,	CASE m.EXP_TYPE_CD 
																									WHEN 'B' THEN 'IE'
																									ELSE m.EXP_TYPE_CD
																									END
												,m.PRE_QUAL_NBR
												,CASE 
													WHEN m.ORDR_TYPE_CD = 'CN' THEN m.PRNT_FTN
													WHEN m.ORDR_SUBTYPE_CD = 'ISMV' THEN m.PRNT_FTN
													ELSE NULL
												  END
												,CASE m.ORDR_TYPE_CD
													WHEN 'CN' THEN NULL
													ELSE m.RLTD_ORDR_ID
												  END
												,c.CPE_ACCS_PRVDR_NME
												,c.CPE_DLVRY_DUTIES_AMT
												,c.CPE_DLVRY_DUTIES_CD
												,c.CPE_EQPT_CKT_ID
												,c.CPE_SPRINT_TEST_TN_NBR
												,c.CPE_RECORD_ONLY_FLG_CD
												,c.CPE_ACCS_PRVDR_TN_NBR
												,c.CPE_EQUIPMENT_ONLY_FLG_CD
											FROM #MACH5_ORDR_CHNG m WITH (NOLOCK)
												LEFT JOIN #MACH5_ORDR_CHNG_CMPNT c WITH (NOLOCK) ON m.M5_ORDR_ID = c.M5_ORDR_ID AND c.CMPNT_TYPE_CD = 'OCPE'
											WHERE m.M5_ORDR_ID = @M5_ORDR_ID
												
											
									)
									
									
									--ORDR_EXP	
									IF EXISTS
									(
										SELECT 'X'
											FROM #MACH5_ORDR_CHNG
											WHERE EXP_FLG_CD = 'Y'
									)
									BEGIN
										INSERT INTO ORDR_EXP (ORDR_ID,
																EXP_TYPE_ID,
																AUTHRZR_PHN_NBR,
																AUTHRZR_TITLE_ID,
																AUTHN_DT,
																BILL_TO_COST_CTR_CD,
																CREAT_DT,
																CREAT_BY_USER_ID,
																REC_STUS_ID
																	)
										(
											SELECT TOP 1	@FSA_ORDR_ID
															,CASE EXP_TYPE_CD
																	WHEN 'I' THEN 1
																	WHEN 'E' THEN 2
																	WHEN 'B' THEN 3
																	ELSE 4 
																END AS EXP_TYPE_ID
															,APRVR_PHN
															,APRVR_TITLE
															,APPRVL_DT
															,[dbo].[RemoveSpecialCharacters](LEFT(SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10),
           PATINDEX('%[^0-9]%', SUBSTRING(COST_CNTR, PATINDEX('%[0-9]%', COST_CNTR), 10) + 'X') -1))
															,GETDATE()
															,1
															,1
												FROM #MACH5_ORDR_CHNG WITH (NOLOCK)
												
										)
									END
									
									delete from @tmpids
									-----FSA_ORDR_CUST
									INSERT INTO dbo.FSA_ORDR_CUST 
											(	ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												CREAT_DT,
												CUST_NME,
												CURR_BILL_CYC_CD,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID)								
										(SELECT	@FSA_ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												GETDATE(),
												CUST_NME,
												ISNULL(BILL_CYC_ID,0),
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID
											FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
											WHERE ISNULL(CSG_LVL,'')=''
										)
									
									INSERT INTO dbo.FSA_ORDR_CUST 
											(	ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												CREAT_DT,
												CURR_BILL_CYC_CD,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
										SELECT	@FSA_ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												GETDATE(),
												ISNULL(BILL_CYC_ID,0),
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID
											FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
											WHERE ISNULL(CSG_LVL,'')!=''
									
									INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																		SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString(moc.CUST_NME)
																				FROM #MACH5_ORDR_CHNG_CUST moc WITH (NOLOCK)
																				INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																				INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																				WHERE ISNULL(moc.CSG_LVL,'')!=''
																				  AND foc.CUST_NME IS NULL
																				  
									IF NOT EXISTS
										(
											SELECT 'X'
												FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
												WHERE CIS_LVL_TYPE = 'H1'
										)
										BEGIN
											INSERT INTO dbo.FSA_ORDR_CUST 
											(	ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												CREAT_DT,
												CUST_NME,
												CURR_BILL_CYC_CD,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID)									
										(SELECT	@FSA_ORDR_ID,
												'H1',
												H1_ID,
												'',
												'',
												GETDATE(),
												'',
												BILL_CYC_ID,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID
											FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
											WHERE CIS_LVL_TYPE = 'H6'
											  AND ISNULL(CSG_LVL,'')=''
										)
										
										INSERT INTO dbo.FSA_ORDR_CUST 
											(	ORDR_ID,
												CIS_LVL_TYPE,
												CUST_ID,
												BR_CD,
												SOI_CD,
												CREAT_DT,
												CURR_BILL_CYC_CD,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
										SELECT	@FSA_ORDR_ID,
												'H1',
												H1_ID,
												'',
												'',
												GETDATE(),
												BILL_CYC_ID,
												TAX_XMPT_CD,
												CHARS_CUST_ID,
												SITE_ID
											FROM #MACH5_ORDR_CHNG_CUST WITH (NOLOCK)
											WHERE CIS_LVL_TYPE = 'H6'
											  AND ISNULL(CSG_LVL,'')!=''
										
										
										INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																		SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString('')
																				FROM #MACH5_ORDR_CHNG_CUST moc WITH (NOLOCK)
																				INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																				INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																				WHERE ISNULL(moc.CSG_LVL,'')!=''
																				  AND foc.CUST_NME IS NULL
										END
									
									delete from @tmpids
									----ORDR_ADR
									INSERT INTO dbo.ORDR_ADR
										(
											ORDR_ID,
											ADR_TYPE_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											CTRY_CD,
											CIS_LVL_TYPE,
											FSA_MDUL_ID,
											STREET_ADR_1,
											STREET_ADR_2,
											CTY_NME,
											PRVN_NME,
											STT_CD,
											ZIP_PSTL_CD,
											BLDG_NME,
											FLR_ID,
											RM_NBR,
											HIER_LVL_CD,
											STREET_ADR_3
										)
									(
										SELECT	@FSA_ORDR_ID,
												CASE CIS_LVL_TYPE
													WHEN 'H4' THEN 3
													WHEN 'H6' THEN 18
													WHEN 'H5' THEN 18
													ELSE 17
												END,
												GETDATE(),
												1,
												1,
												CTRY_CD,
												CIS_LVL_TYPE,
												'CIS',
												STREET_ADR_1,
												STREET_ADR_2,
												CTY_NME,
												PRVN_NME,
												STT_CD,
												ZIP_PSTL_CD,
												BLDG_ID,
												FLOOR_ID,
												ROOM_ID,
												CASE CIS_LVL_TYPE	
													WHEN 'H4' THEN 0
													WHEN 'H1' THEN 0
													ELSE 1
												END,
												STREET_ADR_3
											FROM #MACH5_ORDR_CHNG_CUST
											WHERE ISNULL(CSG_LVL,'')=''
									)
									
									INSERT INTO dbo.ORDR_ADR
										(
											ORDR_ID,
											ADR_TYPE_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											CTRY_CD,
											CIS_LVL_TYPE,
											FSA_MDUL_ID,
											HIER_LVL_CD
										) output inserted.ORDR_ADR_ID into @tmpids
										SELECT	@FSA_ORDR_ID,
												CASE CIS_LVL_TYPE
													WHEN 'H4' THEN 3
													WHEN 'H6' THEN 18
													WHEN 'H5' THEN 18
													ELSE 17
												END,
												GETDATE(),
												1,
												1,
												CTRY_CD,
												CIS_LVL_TYPE,
												'CIS',
												CASE CIS_LVL_TYPE	
													WHEN 'H4' THEN 0
													WHEN 'H1' THEN 0
													ELSE 1
												END
											FROM #MACH5_ORDR_CHNG_CUST
											WHERE ISNULL(CSG_LVL,'')!=''
									
									INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
																		STREET_ADR_2,
																		CTY_NME,
																		STT_PRVN_NME,
																		STT_CD,
																		ZIP_PSTL_CD,
																		BLDG_NME,
																		FLR_ID,
																		RM_NBR,
																		STREET_ADR_3)
																	SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.STREET_ADR_1),
																			dbo.encryptString(moc.STREET_ADR_2),
																			dbo.encryptString(moc.CTY_NME),
																			dbo.encryptString(moc.PRVN_NME),
																			dbo.encryptString(moc.STT_CD),
																			dbo.encryptString(moc.ZIP_PSTL_CD),
																			dbo.encryptString(moc.BLDG_ID),
																			dbo.encryptString(moc.FLOOR_ID),
																			dbo.encryptString(moc.ROOM_ID),
																			dbo.encryptString(moc.STREET_ADR_3)
																		FROM #MACH5_ORDR_CHNG_CUST moc WITH (NOLOCK)
																		INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND oa.ORDR_ID = @FSA_ORDR_ID
																		INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
																		WHERE ISNULL(moc.CSG_LVL,'')!=''
																		  AND oa.STREET_ADR_1 IS NULL
									
									delete from @tmpids
										IF EXISTS
									(
										SELECT 'x'
											FROM #MACH5_ORDR_CHNG_CMPNT
											WHERE	M5_ORDR_ID = @M5_ORDR_ID
												AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0 	
									)
										BEGIN
											INSERT INTO dbo.ORDR_ADR
											(
												ORDR_ID,
												ADR_TYPE_ID,
												CREAT_DT,
												CREAT_BY_USER_ID,
												REC_STUS_ID,
												CTRY_CD,
												CIS_LVL_TYPE,
												FSA_MDUL_ID,
												STREET_ADR_1,
												STREET_ADR_2,
												CTY_NME,
												PRVN_NME,
												STT_CD,
												ZIP_PSTL_CD,
												BLDG_NME,
												FLR_ID,
												RM_NBR,
												HIER_LVL_CD,
												STREET_ADR_3
											)
										(
											SELECT	TOP 1 @FSA_ORDR_ID,
													19, -- Shipping Address Type
													GETDATE(),
													1,
													1,
													CPE_ALT_SHIP_ADR_CTRY_CD,
													(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CHNG_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')),
													'CIS',
													CPE_ALT_SHIP_ADR_LINE_1,
													CPE_ALT_SHIP_ADR_LINE_2,
													CPE_ALT_SHIP_ADR_CTY_NME,
													CPE_ALT_SHIP_ADR_PRVN_NME,
													CPE_ALT_SHIP_ADR_ST_PRVN_CD,
													CPE_ALT_SHIP_ADR_PSTL_CD,
													'',
													'',
													'',
													1,
													CPE_ALT_SHIP_ADR_LINE_3
												FROM #MACH5_ORDR_CHNG_CMPNT
												WHERE	M5_ORDR_ID = @M5_ORDR_ID
													AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0
													AND ISNULL(CSG_LVL,'')=''
										)
										
										INSERT INTO dbo.ORDR_ADR
											(
												ORDR_ID,
												ADR_TYPE_ID,
												CREAT_DT,
												CREAT_BY_USER_ID,
												REC_STUS_ID,
												CTRY_CD,
												CIS_LVL_TYPE,
												FSA_MDUL_ID,
												HIER_LVL_CD
											) output inserted.ORDR_ADR_ID into @tmpids
											SELECT	TOP 1 @FSA_ORDR_ID,
													19, -- Shipping Address Type
													GETDATE(),
													1,
													1,
													CPE_ALT_SHIP_ADR_CTRY_CD,
													(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CHNG_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')),
													'CIS',
													1
												FROM #MACH5_ORDR_CHNG_CMPNT
												WHERE	M5_ORDR_ID = @M5_ORDR_ID
													AND ISNULL(CPE_ALT_SHIP_ADR,0) != 0
													AND ISNULL(CSG_LVL,'')!=''
										
										INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
																		STREET_ADR_2,
																		CTY_NME,
																		STT_PRVN_NME,
																		STT_CD,
																		ZIP_PSTL_CD,
																		BLDG_NME,
																		FLR_ID,
																		RM_NBR,
																		STREET_ADR_3)
																	SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_1),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_2),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_CTY_NME),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_PRVN_NME),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_ST_PRVN_CD),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_PSTL_CD),
																			dbo.encryptString(''),
																			dbo.encryptString(''),
																			dbo.encryptString(''),
																			dbo.encryptString(moc.CPE_ALT_SHIP_ADR_LINE_3)
																		FROM #MACH5_ORDR_CHNG_CMPNT moc WITH (NOLOCK)
																		INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.ADR_TYPE_ID = 19 AND oa.CIS_LVL_TYPE=(SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CHNG_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')) AND oa.ORDR_ID = @FSA_ORDR_ID
																		INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
																		WHERE ISNULL(moc.CSG_LVL,'')!=''
																		  AND moc.M5_ORDR_ID = @M5_ORDR_ID
																		  AND moc.CPE_DEVICE_ID = @CPE_DEVICE_ID
																		  AND ISNULL(moc.CPE_ALT_SHIP_ADR,0) != 0
																		  AND oa.STREET_ADR_1 IS NULL
										END
									
									delete from @tmpids
									------ORDR_CNTCT
									INSERT INTO dbo.ORDR_CNTCT
										(
											ORDR_ID,
											CNTCT_TYPE_ID,
											PHN_NBR,
											TME_ZONE_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											FAX_NBR,
											ROLE_ID,
											CIS_LVL_TYPE,
											FSA_MDUL_ID,
											FRST_NME,
											LST_NME,
											EMAIL_ADR,
											CNTCT_HR_TXT,
											NPA,
											NXX,
											STN_NBR,
											CTY_CD,
											ISD_CD,
											CNTCT_NME,
											PHN_EXT_NBR
										)
									(
										SELECT	@FSA_ORDR_ID ,
												CASE c.ROLE_CD
													WHEN 'Hrchy' THEN 17
													ELSE 1
												END AS CNTCT_TYPE_ID,
												c.PHN_NBR,
												l.TME_ZONE_ID,
												GETDATE(),
												1,
												1,
												'',
												lr.ROLE_ID,
												c.CIS_LVL_TYPE,
												c.FSA_MDUL_ID,
												c.FRST_NME,
												c.LST_NME,
												c.EMAIL_ADR,
												c.CNTCT_HR_TXT,
												c.NPA,
												c.NXX,
												c.STN_NBR,
												c.CTY_CD,
												c.ISD_CD,
												c.CNTCT_NME,
												c.PHN_EXT_NBR
											FROM #MACH5_ORDR_CHNG_CNTCT c
										INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = c.ROLE_CD
										LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
															TME_ZONE_NME	AS TME_ZONE_NME 
														FROM dbo.LK_TME_ZONE 
														GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
										WHERE ISNULL(CSG_LVL,'')=''
									)
									
									INSERT INTO dbo.ORDR_CNTCT
										(
											ORDR_ID,
											CNTCT_TYPE_ID,
											PHN_NBR,
											TME_ZONE_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											FAX_NBR,
											ROLE_ID,
											CIS_LVL_TYPE,
											FSA_MDUL_ID,
											CNTCT_HR_TXT,
											NPA,
											NXX,
											STN_NBR,
											CTY_CD,
											ISD_CD,
											PHN_EXT_NBR
										) output inserted.ORDR_CNTCT_ID into @tmpids
										SELECT	@FSA_ORDR_ID ,
												CASE c.ROLE_CD
													WHEN 'Hrchy' THEN 17
													ELSE 1
												END AS CNTCT_TYPE_ID,
												c.PHN_NBR,
												l.TME_ZONE_ID,
												GETDATE(),
												1,
												1,
												'',
												lr.ROLE_ID,
												c.CIS_LVL_TYPE,
												c.FSA_MDUL_ID,
												c.CNTCT_HR_TXT,
												c.NPA,
												c.NXX,
												c.STN_NBR,
												c.CTY_CD,
												c.ISD_CD,
												c.PHN_EXT_NBR
											FROM #MACH5_ORDR_CHNG_CNTCT c
										INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = c.ROLE_CD
										LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
															TME_ZONE_NME	AS TME_ZONE_NME 
														FROM dbo.LK_TME_ZONE 
														GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
										WHERE ISNULL(CSG_LVL,'')!=''
									
									INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, FRST_NME,
																		LST_NME,
																		CUST_EMAIL_ADR,
																		CUST_CNTCT_NME)
																	SELECT	DISTINCT oc.ORDR_CNTCT_ID, 15, dbo.encryptstring(c.FRST_NME),
																			dbo.encryptstring(c.LST_NME),
																			dbo.encryptstring(c.EMAIL_ADR),
																			dbo.encryptstring(c.CNTCT_NME)
																		FROM #MACH5_ORDR_CHNG_CNTCT c WITH (NOLOCK)
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
																	INNER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ROLE_ID=lr.ROLE_ID AND oc.CIS_LVL_TYPE=c.CIS_LVL_TYPE AND oc.ORDR_ID = @FSA_ORDR_ID AND oc.FRST_NME IS NULL
																	INNER JOIN @tmpids tmpids ON tmpids.tmpid = oc.ORDR_CNTCT_ID
																	WHERE ISNULL(c.CSG_LVL,'')!=''
																	  AND oc.FRST_NME IS NULL
									
									-----ORDER NOTES
									IF EXISTS
										(
											SELECT 'X'
												FROM #MACH5_ORDR_CHNG_NOTES WITH (NOLOCK)
										)
									BEGIN
										INSERT INTO ORDR_NTE
										(
											NTE_TYPE_ID,
											ORDR_ID,
											CREAT_DT,
											CREAT_BY_USER_ID,
											REC_STUS_ID,
											NTE_TXT
										)
										(
											SELECT 
												CASE NTE_TYPE_CD
													WHEN 'CPEN' THEN 26
													ELSE 25
												END, 
												@FSA_ORDR_ID,		
												GETDATE(),
												1,
												1,
												NTE_TXT		
											FROM #MACH5_ORDR_CHNG_NOTES
											WHERE NTE_TXT IS NOT NULL
										)
									END
									
									--FSA_ORDR_CPE_LINE_ITEM
									INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
										(
											ORDR_ID,
											EQPT_TYPE_ID, --EQPT_TYPE_CD 
											EQPT_ID, --EQPT_ID
											MFR_NME, --CPE_MFR_NME 
											CNTRC_TYPE_ID, --CPE_CONTRACT_TYPE
											CNTRC_TERM_ID, --CPE_CONTRACT_TERM 
											MNTC_CD, --CPE_MNGD_FLG_CD 
											CREAT_DT,
											CMPNT_ID, --CPE_ITEM_ID 
											LINE_ITEM_CD,
											MANF_PART_CD, --CPE_MFR_PART_NBR 
											UNIT_PRICE, --CPE_LIST_PRICE 
											DROP_SHP, --CPE_DROP_SHIP_FLG_CD 
											DEVICE_ID, --CPE_DEVICE_ID 
											ORDR_CMPNT_ID,
											ITM_STUS,
											ORDR_QTY,
											MDS_DES,
											MANF_DISCNT_CD,
											MATL_CD,
											CMPNT_FMLY,
											RLTD_CMPNT_ID,
											STUS_CD,
											MFR_PS_ID,
											SPRINT_MNTD_FLG
										)
										SELECT	@FSA_ORDR_ID,
												CPE_ITEM_TYPE_CD,
												EQPT_ID,
												SUBSTRING(ISNULL(CPE_MFR_ID,''),1,50) AS CPE_MFR_NME,
												CPE_CONTRACT_TYPE,
												CONVERT(VARCHAR,ISNULL(CPE_CONTRACT_TERM,0)) AS CPE_CONTRACT_TERM,
												CPE_MNGD_FLG_CD,
												GETDATE(),
												CPE_ITEM_ID,
												'CPE' AS LINE_ITEM_CD,
												SUBSTRING(ISNULL(CPE_MFR_PART_NBR,''),1,20) AS CPE_MFR_PART_NBR,
												CPE_LIST_PRICE,
												CPE_DROP_SHIP_FLG_CD,
												CPE_DEVICE_ID,
												NVTRY_ID,
												CASE STUS_CD
													WHEN 'CN' THEN 402
													ELSE 401
												END AS ITM_STUS,
												QTY,
												SUBSTRING(ISNULL(NME,''),1,50) AS NME,
												CPE_VNDR_DSCNT, 
												CPE_ITEM_MSTR_NBR,
												CPE_CMPN_FMLY,
												RLTD_CMPNT_ID,
												STUS_CD,
												CPE_MFR_PS_ID,
												CPE_SPRINT_MNT_FLG_CD
											FROM #MACH5_ORDR_CHNG_CMPNT
											WHERE	M5_ORDR_ID = @M5_ORDR_ID
												AND STUS_CD != 'CN'
									
									
									INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
										(
											ORDR_ID,
											EQPT_TYPE_ID, --EQPT_TYPE_CD 
											EQPT_ID, --EQPT_ID
											MFR_NME, --CPE_MFR_NME 
											CNTRC_TYPE_ID, --CPE_CONTRACT_TYPE
											CNTRC_TERM_ID, --CPE_CONTRACT_TERM 
											MNTC_CD, --CPE_MNGD_FLG_CD 
											CREAT_DT,
											CMPNT_ID, --CPE_ITEM_ID 
											LINE_ITEM_CD,
											MANF_PART_CD, --CPE_MFR_PART_NBR 
											UNIT_PRICE, --CPE_LIST_PRICE 
											DROP_SHP, --CPE_DROP_SHIP_FLG_CD 
											DEVICE_ID, --CPE_DEVICE_ID 
											ORDR_CMPNT_ID,
											ITM_STUS,
											ORDR_QTY,
											MDS_DES,
											MANF_DISCNT_CD,
											MATL_CD,
											CMPNT_FMLY,
											RLTD_CMPNT_ID,
											STUS_CD,
											MFR_PS_ID,
											SPRINT_MNTD_FLG
										)
										SELECT	@FSA_ORDR_ID,
												t.CPE_ITEM_TYPE_CD,
												t.EQPT_ID,
												SUBSTRING(ISNULL(t.CPE_MFR_ID,''),1,50) AS CPE_MFR_NME,
												t.CPE_CONTRACT_TYPE,
												CONVERT(VARCHAR,ISNULL(t.CPE_CONTRACT_TERM,0)) AS CPE_CONTRACT_TERM,
												t.CPE_MNGD_FLG_CD,
												GETDATE(),
												t.CPE_ITEM_ID,
												'CPE' AS LINE_ITEM_CD,
												SUBSTRING(ISNULL(t.CPE_MFR_PART_NBR,''),1,20) AS CPE_MFR_PART_NBR,
												t.CPE_LIST_PRICE,
												t.CPE_DROP_SHIP_FLG_CD,
												t.CPE_DEVICE_ID,
												t.NVTRY_ID,
												CASE t.STUS_CD
													WHEN 'CN' THEN 402
													ELSE 401
												END AS ITM_STUS,
												t.QTY,
												SUBSTRING(ISNULL(t.NME,''),1,50) AS NME,
												t.CPE_VNDR_DSCNT, 
												t.CPE_ITEM_MSTR_NBR,
												t.CPE_CMPN_FMLY,
												t.RLTD_CMPNT_ID,
												t.STUS_CD,
												t.CPE_MFR_PS_ID,
												t.CPE_SPRINT_MNT_FLG_CD
											FROM #MACH5_ORDR_CHNG_CMPNT t WITH (NOLOCK)
											INNER JOIN #MACH5_ORDR_CHNG o WITH (NOLOCK) ON t.M5_ORDR_ID = o.M5_ORDR_ID
											WHERE	t.M5_ORDR_ID = @M5_ORDR_ID
												AND t.STUS_CD = 'CN'
												AND (
														(o.ORDR_TYPE_CD != 'CN'
															AND EXISTS
															(
																SELECT 'X'
																	FROM dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK)
																	INNER JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON fc.ORDR_ID = f.ORDR_ID
																	WHERE f.FTN IN (SELECT ORDER_NBR From #MACH5_ORDR_CHNG)
																		AND fc.DEVICE_ID IN (Select CPE_DEVICE_ID FROM #MACH5_ORDR_CHNG_CMPNT WHERE M5_ORDR_ID = @M5_ORDR_ID)
																		AND fc.ORDR_CMPNT_ID = t.ORDR_CMPNT_ID
																	
																
															)
														)
													OR 
														(
															o.ORDR_TYPE_CD = 'CN'
															AND EXISTS
																(
																	SELECT 'X'
																	FROM dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK)
																	INNER JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON fc.ORDR_ID = f.ORDR_ID
																	WHERE f.FTN IN (SELECT PRNT_FTN From #MACH5_ORDR_CHNG)
																		AND fc.DEVICE_ID IN (Select CPE_DEVICE_ID FROM #MACH5_ORDR_CHNG_CMPNT WHERE M5_ORDR_ID = @M5_ORDR_ID)
																		AND fc.ORDR_CMPNT_ID = t.ORDR_CMPNT_ID
																)	
														)
													)
											
									--Update Region ID for xNCI Orders
									Update od
										SET od.RGN_ID = ISNULL(lc.RGN_ID,od.RGN_ID)
										FROM dbo.ORDR od WITH (ROWLOCK)
									INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON od.ORDR_ID = oa.ORDR_ID
									INNER JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD 
										WHERE od.ORDR_ID = @ORDR_ID
											AND oa.CIS_LVL_TYPE = 'H6'
												
									IF @CHNG_ORDR_TYPE_FLG != 1
										BEGIN
											----State Machine
											INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('InsertInitialTask:'+CONVERT(VARCHAR,@ORDR_ID)+@TransName,'insertMach5TxnDetails', GETDATE())
											SET @ERRLog = @ERRLog + 'InsertInitialTask:'+CONVERT(VARCHAR,@ORDR_ID)
											Exec dbo.InsertInitialTask @ORDR_ID,@OrderCategoryID
											--SELECT @ORDR_ID	
										END
									
								END	
							
							
						END
					END
				END
				--Set Error Severity to MD(Missing Data), because either order component or data is missing to process the order
			
	-- Zscaler Change Orders.		
	IF @ORDR_TYPE_CD IN ('CSCH') -- ZCLR is not correct and will need changed. 
		BEGIN
		
		DECLARE @ORD_TYP VARCHAR(2),
				@QTY     INT,
				@NQTY    INT,
				@OQTY    INT
		
			IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG_ZCLR',N'U') IS NOT NULL 
				DROP TABLE #MACH5_ORDR_CHNG_ZCLR
						
			CREATE TABLE #MACH5_ORDR_CHNG_ZCLR
				(
					M5_ORDR_ID				INT,
					--QDA_ORDR_ID				INT,
					ORDER_NBR				VARCHAR(20),
					PRNT_ACCT_ID			INT,
					ORDR_TYPE_CD			VARCHAR(2),
					PROD_ID					VARCHAR(5),
					PROD_TYPE_CD			VARCHAR(2),
					ORDR_SUBTYPE_CD			VARCHAR(7),
					DOM_PROCESS_FLG_CD		VARCHAR(1),
					RLTD_ORDR_ID			INT,
					CUST_SIGN_DT			DATE,
					CUST_WANT_DT			DATE,
					CUST_SBMT_DT			DATE,
					CUST_CMMT_DT			DATE,
					EXP_FLG_CD				CHAR(1),
					EXP_TYPE_CD				VARCHAR(1),
					CUST_PREM_OCCU_FLG_CD	CHAR(1),
					CUST_ACPT_SRVC_FLG_CD	CHAR(1),
					MULT_ORDR_SBMTD_FLG_CD	CHAR(1),
					DSCNCT_REAS_CD			VARCHAR(2),
					PRE_QUAL_NBR			VARCHAR(20),
					COST_CNTR				VARCHAR(10),
					EXPEDITE_APRVR_NME		VARCHAR(511),
					APRVR_TITLE				VARCHAR(15),
					APRVR_PHN				VARCHAR(20),
					APPRVL_DT				DATE,
					PRNT_FTN				VARCHAR(20),
					CSG_LVL					VARCHAR(5),
					CHARS_CUST_ID			VARCHAR(20)
				)
						
						
			SET @SQL = 'SELECT * FROM OPENQUERY(M5,''SELECT DISTINCT d.CHNG_TXN_ID AS M5_ORDR_ID, CHNG_TXN_NBR AS ORDER_NBR, c.CUST_ACCT_ID AS PRNT_ACCT_ID, ''''CH'''' AS ORDR_TYPE_CD, NULL AS PROD_ID, 
												 null AS PROD_TYPE_CD, c.TYPE_CD AS ORDR_SUBTYPE_CD, c.DOM_PROCESS_FLG_CD, 
												 null AS RLTD_ORDR_ID, null AS CUST_SIGN_DT, null AS CUST_WANT_DT, null AS CUST_SBMT_DT, 
												 c.CUST_CMMT_DT, c.EXP_FLG_CD, c.EXP_TYPE_CD, null AS CUST_PREM_OCCU_FLG_CD, 
												 null AS CUST_ACPT_SRVC_FLG_CD, null AS MULT_ORDR_SBMTD_FLG_CD,null AS DSCNCT_REAS_CD, 
												 null AS PRE_QUAL_NBR, null AS COST_CNTR, null AS EXPEDITE_APRVR_NME,null AS APRVR_TITLE, 
												 null AS APRVR_PHN, null AS APPRVL_DT, null, a.CSG_LVL, a.CHARS_CUST_ID
											FROM  MACH5.V_V5U_CHNG_TXN c
											LEFT JOIN Mach5.V_V5U_CHNG_TXN_DETL d ON c.CHNG_TXN_ID = d.CHNG_TXN_ID
											LEFT JOIN MACH5.V_V5U_ORDR_CMPNT c ON d.NVTRY_ID = c.NVTRY_ID
											LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID = d.NVTRY_ID
											LEFT JOIN MACH5.V_V5U_ORDR  o ON o.ORDR_ID = n.ORIG_ORDR_ID
											LEFT JOIN MACH5.V_V5U_CUST_ACCT a on n.CUST_ACCT_ID = a.CUST_ACCT_ID AND a.TYPE_CD = DECODE(a.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
											WHERE c.CHNG_TXN_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')'

			--SELECT @SQL
			INSERT INTO #MACH5_ORDR_CHNG_ZCLR
			EXEC sp_executesql 	@SQL			

			select * from #MACH5_ORDR_CHNG_ZCLR

			IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG_CMPNT_ZCLR',N'U') IS NOT NULL 
				DROP TABLE #MACH5_ORDR_CHNG_CMPNT_ZCLR

			CREATE TABLE #MACH5_ORDR_CHNG_CMPNT_ZCLR
				(
					M5_ORDR_ID		INT,
					QDA_ORDR_ID		INT,
					ORDR_CMPNT_ID	INT,
					CMPNT_TYPE_CD	VARCHAR(4),
					ONE_STOP_SHOP_FLG_CD	VARCHAR(1), --TTRPT_OSS_CD
					ACCS_ARNGT_CD			VARCHAR(3), --TTRPT_ACCS_ARNGT_CD
					ACCS_TYPE_CD			VARCHAR(3),--TTRPT_ACCS_TYPE_CD
					ACCS_TERM_CD			VARCHAR(12),--TTRPT_ACCS_CNTRC_TERM_CD
					ACCS_BDWD_CD			VARCHAR(30),--TTRPT_SPD_OF_SRVC_BDWD_DES
					ACCS_CXR_CD				VARCHAR(5), -- CXR_ACCS_CD
					DESIGN_DOC_NUM			VARCHAR(20), --INSTL_DSGN_DOC_NBR
					RATE_TYPE_CD			VARCHAR(6), --PORT_RT_TYPE_CD
					PORT_SPD_DES			VARCHAR(50), --TTRPT_SPD_OF_SRVC_BDWD_DES
					NVTRY_ID				INT,
					CPE_ACCS_PRVDR_NME		VARCHAR(50),
					CPE_CONTRACT_TERM		INT,
					CPE_CONTRACT_TYPE		VARCHAR(6),
					CPE_DLVRY_DUTIES_AMT	VARCHAR(8),
					CPE_DLVRY_DUTIES_CD		VARCHAR(1),
					CPE_DROP_SHIP_FLG_CD	VARCHAR(1),
					CPE_EQPT_CKT_ID			VARCHAR(25),
					CPE_ITEM_ID				INT,
					CPE_MANAGED_BY			VARCHAR(10),
					CPE_MFR_ID				VARCHAR(4),
					CPE_MFR_NME				VARCHAR(255),
					CPE_MFR_PART_NBR		VARCHAR(255),
					CPE_MNGD_FLG_CD			VARCHAR(1),
					CPE_SPRINT_MNT_FLG_CD   CHAR(1),
					CPE_MSCP_CD				VARCHAR(15),
					CPE_LIST_PRICE			VARCHAR(14),
					CPE_REQD_ON_SITE_DT		DATE,
					CPE_SHIP_CLLI_CD		VARCHAR(11),
					CPE_SPRINT_TEST_TN_NBR	VARCHAR(10),
					CPE_DEVICE_ID			VARCHAR(14),
					EQPT_ID					VARCHAR(10),
					EQPT_TYPE_CD			VARCHAR(4),
					FLAG					BIT,
					DEVICE_ID				INT,
					QTY                     INT,
					OLD_QTY					INT,
					NEW_QTY                 INT,
					NME						VARCHAR(255),
					CPE_VNDR_DSCNT			VARCHAR(14),
					CPE_ITEM_MSTR_NBR		VARCHAR(6),
					CPE_RECORD_ONLY_FLG_CD	VARCHAR(1),
					CPE_CMPN_FMLY			VARCHAR(10),
					RLTD_CMPNT_ID			INT,
					CPE_ITEM_TYPE_CD		Varchar(3),	
					STUS_CD					Varchar(2),
					CPE_MFR_PS_ID			Varchar(30),
					CPE_ALT_SHIP_ADR			INT,
					CPE_ALT_SHIP_ADR_CTRY_CD	VARCHAR(2),
					CPE_ALT_SHIP_ADR_CTY_NME	VARCHAR(255),
					CPE_ALT_SHIP_ADR_LINE_1		VARCHAR(255),
					CPE_ALT_SHIP_ADR_LINE_2		VARCHAR(255),
					CPE_ALT_SHIP_ADR_LINE_3		VARCHAR(255),
					CPE_ALT_SHIP_ADR_PRVN_NME	VARCHAR(255),
					CPE_ALT_SHIP_ADR_PSTL_CD	VARCHAR(15),
					CPE_ALT_SHIP_ADR_ST_PRVN_CD VARCHAR(2),
					CPE_ACCS_PRVDR_TN_NBR        VARCHAR(10),
					CPE_EQUIPMENT_ONLY_FLG_CD	 VARCHAR(1),
					QUOTE_ID					 VARCHAR(75),
					CSG_LVL                      VARCHAR(5),
					PL_NBR						VARCHAR(200),
					CKT							VARCHAR(200),
					NUA							VARCHAR(200)
				)
				

			SET @SQL = ''
			

			SET @SQL = 'SELECT *
						FROM OPENQUERY(M5,''SELECT d.CHNG_TXN_ID AS ORDR_ID, 0 AS QDA_ORDR_ID, d.CHNG_TXN_DETL_ID AS ORDR_CMPNT_ID, n.CMPNT_TYPE_CD,n.ONE_STOP_SHOP_FLG_CD,
														c.ACCS_ARNGT_CD,c.ACCS_TYPE_CD,c.ACCS_TERM_CD,c.ACCS_BDWD_CD,
														c.ACCS_CXR_CD,c.DESIGN_DOC_NBR,c.RATE_TYPE_CD,NULL AS PORT_SPD_DES, 
														d.NVTRY_ID,c.CPE_ACCS_PRVDR_NME,d.CPE_CONTRACT_TERM,
														d.CPE_CONTRACT_TYPE,c.CPE_DLVRY_DUTIES_AMT,c.CPE_DLVRY_DUTIES_CD,
														d.CPE_DROP_SHIP_FLG_CD,c.CPE_EQPT_CKT_ID,d.CPE_ITEM_ID,
														c.CPE_MANAGED_BY,d.CPE_MFR_ID,d.CPE_MFR_NME,d.CPE_MFR_PART_NBR,
														n.CPE_MNGD_FLG_CD,c.CPE_SPRINT_MNT_FLG_CD,c.CPE_MSCP_CD,d.CPE_LIST_PRICE,c.CPE_REQD_ON_SITE_DT,
														c.CPE_SHIP_CLLI_CD,c.CPE_SPRINT_TEST_TN_NBR,
														d.CPE_DEVICE_ID, d.EQPT_ID, d.EQPT_TYPE_CD, 0 AS FLAG, 0 AS DEVICE_ID, 0 AS QTY,
														d2.QTY AS OLD_QTY, d.QTY AS NEW_QTY, n.NME, n.CPE_VNDR_DSCNT, d.CPE_ITEM_MSTR_NBR, c.CPE_RECORD_ONLY_FLG_CD,
														c.CPE_CMPN_FMLY, c.RLTD_CMPNT_ID, d.CPE_ITEM_TYPE_CD, ''''CP'''' AS STUS_CD, d.CPE_MFR_PS_ID,
														c.CPE_ALT_SHIP_ADR,c.CPE_ALT_SHIP_ADR_CTRY_CD,c.CPE_ALT_SHIP_ADR_CTY_NME,
														c.CPE_ALT_SHIP_ADR_LINE_1,c.CPE_ALT_SHIP_ADR_LINE_2,c.CPE_ALT_SHIP_ADR_LINE_3,
														c.CPE_ALT_SHIP_ADR_PRVN_NME,c.CPE_ALT_SHIP_ADR_PSTL_CD,
														c.CPE_ALT_SHIP_ADR_ST_PRVN_CD, c.CPE_ACCS_PRVDR_TN_NBR, c.CPE_EQUIPMENT_ONLY_FLG_CD,
														n.QUOTE_ID,a.CSG_LVL,n.PRIVATE_LINE_NBR, n.CKT_ID,n.NTWK_USR_ADR
													FROM	Mach5.V_V5U_CHNG_TXN_DETL d
													INNER JOIN Mach5.V_V5U_CHNG_TXN_DETL d2 ON d.NVTRY_ID = d2.NVTRY_ID 
																							AND d.CHNG_TXN_ID = d2.CHNG_TXN_ID
																							AND d2.ORIG_CD = ''''O''''
													LEFT JOIN MACH5.V_V5U_ORDR_CMPNT c ON d.NVTRY_ID = c.NVTRY_ID
													LEFT JOIN MACH5.V_V5U_NVTRY n ON n.NVTRY_ID = d.NVTRY_ID
													LEFT JOIN MACH5.V_V5U_CUST_ACCT a ON a.CUST_ACCT_ID = n.CUST_ACCT_ID 
													WHERE d.ORIG_CD = ''''C'''' AND	d.CHNG_TXN_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')' 

			--select @SQL
			INSERT INTO #MACH5_ORDR_CHNG_CMPNT_ZCLR
			EXEC sp_executesql @SQL	
	
			Select * from #MACH5_ORDR_CHNG_CMPNT_ZCLR
			
			-- Determine whether Install or Disconnect
			--SELECT @NQTY = NEW_QTY, @OQTY = OLD_QTY FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR
			
			--IF (@NQTY > @OQTY)
			--	BEGIN
					
			--		SET @QTY = @NQTY - @OQTY
					
			--		UPDATE  #MACH5_ORDR_CHNG_ZCLR
			--			SET ORDR_TYPE_CD = 'IN'
						
			--		UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
			--			SET QTY = @QTY, CPE_DROP_SHIP_FLG_CD = 'Y'
						
			--	END
				
			--ELSE IF (@OQTY > @NQTY)
			--	BEGIN
			--		SET @QTY = @OQTY - @NQTY
			--		UPDATE  #MACH5_ORDR_CHNG_ZCLR
			--			SET ORDR_TYPE_CD = 'DC'
						
			--		UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
			--			SET QTY = @QTY
					
			--	END
			--  End of install/disc logic
	
			IF EXISTS
				(
					SELECT 'X'
						FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR
				)
				BEGIN
				
					SELECT @CUST_ACCT_ID = PRNT_ACCT_ID FROM #MACH5_ORDR_CHNG_ZCLR


					IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG_CUST_ZCLR',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_CHNG_CUST_ZCLR
					CREATE TABLE #MACH5_ORDR_CHNG_CUST_ZCLR 
						(
							CIS_LVL_TYPE Varchar(2),
							CUST_ID Varchar(9),
							BR_CD Varchar(2),
							SOI_CD Varchar(3),
							CUST_NME Varchar(200),
							STREET_ADR_1 varchar(255),
							STREET_ADR_2 varchar(255),
							STREET_ADR_3 varchar(255),
							CTRY_CD Varchar(2),
							CTY_NME Varchar(255),
							STT_CD Varchar(2),
							ZIP_PSTL_CD Varchar(15),
							BLDG_ID Varchar(255),
							FLOOR_ID varchar(255),
							ROOM_ID Varchar(255),
							BILL_CYC_ID Varchar(2),
							H1_ID	Varchar(9),
							TAX_XMPT_CD Varchar(1),
							CHARS_CUST_ID  VARCHAR(9),
							SITE_ID	VARCHAR(14),
							PRVN_NME VARCHAR(255),
							CSG_LVL VARCHAR(5)
							
						)


					SET @SQL = ''
					SET @SQL = 	
						'SELECT * ' + 
							' FROM OPENQUERY (M5, ''Select CIS_HIER_LVL_CD,CIS_CUST_ID, BR_CD,SOI_CD,CUST_NME
															,LINE_1,LINE_2,LINE_3,CTRY_CD,CTY_NME,ST_PRVN_CD
															,PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1,TAX_XMPT_CD
															,CHARS_CUST_ID,SITE_ID,PRVN_NME,CSG_LVL
													From Mach5.V_V5U_CUST_ACCT  
													Where CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
													  AND TYPE_CD = DECODE(CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
													  AND ROWNUM = 1 '')'+															
							
						   ' UNION ' + 
						   ' SELECT * FROM OPENQUERY (M5,''Select h4.CIS_HIER_LVL_CD,h4.CIS_CUST_ID, h4.BR_CD,h4.SOI_CD,h4.CUST_NME
																	,h4.LINE_1,h4.LINE_2,h4.LINE_3,h4.CTRY_CD,h4.CTY_NME,h4.ST_PRVN_CD
																	,h4.PSTL_CD,h4.BLDG_ID,h4.FLOOR_ID,h4.ROOM_ID,h4.BILL_CYC_ID,h4.H1,h4.TAX_XMPT_CD
																	,h4.CHARS_CUST_ID,h4.SITE_ID,h4.PRVN_NME, h6.CSG_LVL
															From Mach5.V_V5U_CUST_ACCT h4 
															INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.H4 = h4.CIS_CUST_ID
															Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ' AND h4.TYPE_CD=DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'+
						   
						   '	UNION	' + 
						   '	Select * FROM OPENQUERY 
									(M5, ''SELECT h1.CIS_HIER_LVL_CD,h1.CIS_CUST_ID, h1.BR_CD,h1.SOI_CD,h1.CUST_NME
																	,h1.LINE_1,h1.LINE_2,h1.LINE_3,h1.CTRY_CD,h1.CTY_NME,h1.ST_PRVN_CD
																	,h1.PSTL_CD,h1.BLDG_ID,h1.FLOOR_ID,h1.ROOM_ID,h1.BILL_CYC_ID,h1.H1,h1.TAX_XMPT_CD
																	,h1.CHARS_CUST_ID,h1.SITE_ID,h1.PRVN_NME, h6.CSG_LVL
																From Mach5.V_V5U_CUST_ACCT h1 
																INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.H1 = h1.CIS_CUST_ID
															Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ' AND h1.TYPE_CD=DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'

					select @SQL
					
					INSERT INTO #MACH5_ORDR_CHNG_CUST_ZCLR (CIS_LVL_TYPE ,CUST_ID ,BR_CD ,SOI_CD ,
													CUST_NME ,STREET_ADR_1 ,STREET_ADR_2 ,
													STREET_ADR_3,CTRY_CD,CTY_NME,STT_CD,
													ZIP_PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1_ID
													,TAX_XMPT_CD,CHARS_CUST_ID,SITE_ID,PRVN_NME,CSG_LVL)
					EXEC sp_executesql 	@SQL
									
					Select * From #MACH5_ORDR_CHNG_CUST_ZCLR		

IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CUST',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_CUST
					CREATE TABLE #MACH5_ORDR_CUST 
						(
							CIS_LVL_TYPE Varchar(2),
							CUST_ID Varchar(9),
							BR_CD Varchar(2),
							SOI_CD Varchar(3),
							CUST_NME Varchar(200),
							STREET_ADR_1 varchar(255),
							STREET_ADR_2 varchar(255),
							STREET_ADR_3 varchar(255),
							CTRY_CD Varchar(2),
							CTY_NME Varchar(255),
							STT_CD Varchar(2),
							ZIP_PSTL_CD Varchar(15),
							BLDG_ID Varchar(255),
							FLOOR_ID varchar(255),
							ROOM_ID Varchar(255),
							BILL_CYC_ID Varchar(2),
							H1_ID	Varchar(9),
							TAX_XMPT_CD Varchar(1),
							CHARS_CUST_ID  VARCHAR(9),
							SITE_ID	VARCHAR(14),
							PRVN_NME VARCHAR(255),
							CSG_LVL  VARCHAR(5)
						)


					SET @SQL = ''
					SET @SQL = 	
						'SELECT * ' + 
							' FROM OPENQUERY (M5, ''Select DISTINCT CIS_HIER_LVL_CD,CIS_CUST_ID, BR_CD,SOI_CD,CUST_NME
															,LINE_1,LINE_2,LINE_3,CTRY_CD,CTY_NME,ST_PRVN_CD
															,PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1,TAX_XMPT_CD
															,CHARS_CUST_ID,SITE_ID,PRVN_NME, CSG_LVL
													From Mach5.V_V5U_CUST_ACCT  
													Where CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
													  AND TYPE_CD = DECODE(CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
													  AND ROWNUM = 1 '')'+
															
							
						   ' UNION ' + 
						   ' SELECT * FROM OPENQUERY (M5,''Select DISTINCT h4.CIS_HIER_LVL_CD,h4.CIS_CUST_ID, h4.BR_CD,h4.SOI_CD,h4.CUST_NME
																	,h4.LINE_1,h4.LINE_2,h4.LINE_3,h4.CTRY_CD,h4.CTY_NME,h4.ST_PRVN_CD
																	,h4.PSTL_CD,h4.BLDG_ID,h4.FLOOR_ID,h4.ROOM_ID,h4.BILL_CYC_ID,h4.H1,h4.TAX_XMPT_CD
																	,h4.CHARS_CUST_ID,h4.SITE_ID,h4.PRVN_NME, h4.CSG_LVL
															From Mach5.V_V5U_CUST_ACCT h4 
															INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.H4 = h4.CIS_CUST_ID
															Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ' AND h4.TYPE_CD=DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'+
						   
						   '	UNION	' + 
						   '	Select * FROM OPENQUERY 
									(M5, ''SELECT DISTINCT h1.CIS_HIER_LVL_CD,h1.CIS_CUST_ID, h1.BR_CD,h1.SOI_CD,h1.CUST_NME
																	,h1.LINE_1,h1.LINE_2,h1.LINE_3,h1.CTRY_CD,h1.CTY_NME,h1.ST_PRVN_CD
																	,h1.PSTL_CD,h1.BLDG_ID,h1.FLOOR_ID,h1.ROOM_ID,h1.BILL_CYC_ID,h1.H1,h1.TAX_XMPT_CD
																	,h1.CHARS_CUST_ID,h1.SITE_ID,h1.PRVN_NME, h1.CSG_LVL
																From Mach5.V_V5U_CUST_ACCT h1 
																INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.H1 = h1.CIS_CUST_ID
															Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ' AND h1.TYPE_CD=DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'
					
					--print @SQL
					INSERT INTO #MACH5_ORDR_CUST (CIS_LVL_TYPE ,CUST_ID ,BR_CD ,SOI_CD ,
													CUST_NME ,STREET_ADR_1 ,STREET_ADR_2 ,
													STREET_ADR_3,CTRY_CD,CTY_NME,STT_CD,
													ZIP_PSTL_CD,BLDG_ID,FLOOR_ID,ROOM_ID,BILL_CYC_ID,H1_ID
													,TAX_XMPT_CD,CHARS_CUST_ID,SITE_ID,PRVN_NME,CSG_LVL)
					EXEC sp_executesql 	@SQL
									
					Select * From #MACH5_ORDR_CUST	
						
					SELECT TOP 1 CIS_LVL_TYPE FROM #MACH5_ORDR_CUST WHERE CIS_LVL_TYPE IN ('H5','H6')



					IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG_CNTCT_ZCLR',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_CHNG_CNTCT_ZCLR
					CREATE TABLE #MACH5_ORDR_CHNG_CNTCT_ZCLR 
					(
						[CIS_LVL_TYPE] Varchar(2),
						[ROLE_CD] [Varchar](6),
						[PHN_NBR] [Varchar](20) ,
						[TME_ZONE_ID] [varchar](255) ,
						[FRST_NME] [varchar](255) ,
						[LST_NME] [varchar](255) ,
						[EMAIL_ADR] [varchar](255) ,
						[CNTCT_HR_TXT] [varchar](50) ,
						[NPA] [varchar](3) ,
						[NXX] [varchar](3) ,
						[STN_NBR] [varchar](4) ,
						[CTY_CD] [varchar](5) ,
						[ISD_CD] [varchar](3) ,
						[CNTCT_NME] [varchar](255) ,
						[PHN_EXT_NBR] [varchar](10),
						[FSA_MDUL_ID] [varchar] (3),
						[CSG_LVL]  VARCHAR(5)
					)

					SET @SQL = ''
					SET @SQL = '  SELECT * ' + 
								' FROM OPENQUERY (M5,
								''Select DISTINCT ''''H6'''' AS H6,
									vvcc.ROLE_CD AS ROLE_CD,
									CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN vvcc.DOM_PHN_NBR
										  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN vvcc.INTL_PHN_NBR
										  ELSE ''''''''
									END AS PHN_NBR, 
									vvcc.TME_ZN_CD AS TME_ZONE_ID,
									'''''''' AS FRST_NME,
									'''''''' AS LST_NME ,			
									vvcc.EMAIL_ADR AS EMAIL_ADR, 
									vvcc.AVLBLTY_HR_STRT || CASE WHEN LENGTH(vvcc.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||vvcc.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
									CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvcc.DOM_PHN_NBR,1,3) 
										  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvcc.INTL_PHN_NBR,1,3) 
										  ELSE ''''''''
									END AS NPA ,			
								   CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvcc.DOM_PHN_NBR,4,3) 
									  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvcc.INTL_PHN_NBR,4,3) 
									  ELSE ''''''''       
									END AS  NXX ,			
								   CASE WHEN vvcc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvcc.DOM_PHN_NBR,7,4) 
									  WHEN vvcc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvcc.INTL_PHN_NBR,7,4) 
									  ELSE ''''''''      
									END AS STN_NBR ,			
									'''''''' AS CTY_CD ,
									vvcc.INTL_CNTRY_DIAL_CD AS ISD_CD,			
									vvcc.CUST_NME AS CNTCT_NME ,			
									vvcc.PHN_EXT AS PHN_EXT_NBR,
									''''CIS'''' AS FSA_MDUL_ID,
									vvca.CSG_LVL
									From Mach5.v_V5U_CUST_CONTACT vvcc INNER JOIN
										 Mach5.V_V5U_CUST_ACCT  vvca ON vvca.CUST_ACCT_ID = vvcc.CUST_ACCT_ID
								  WHERE vvcc.CUST_ACCT_ID ='   + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')'+


								'	UNION	' + 

								' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select DISTINCT ''''H4'''' AS H6,
										hc4.ROLE_CD AS ROLE_CD, 
										CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN hc4.DOM_PHN_NBR
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN hc4.INTL_PHN_NBR
										  ELSE ''''''''
										END AS PHN_NBR, 
										hc4.TME_ZN_CD AS TME_ZONE_ID,
										'''''''' AS FRST_NME,
										'''''''' AS LST_NME ,			
										hc4.EMAIL_ADR AS EMAIL_ADR,
										hc4.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc4.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc4.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
										CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,1,3) 
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,1,3) 
										  ELSE ''''''''
										  END AS NPA ,			
										CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,4,3) 
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,4,3) 
										  ELSE ''''''''       
										  END AS  NXX ,			
									   CASE WHEN hc4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc4.DOM_PHN_NBR,7,4) 
										  WHEN hc4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc4.INTL_PHN_NBR,7,4) 
										  ELSE ''''''''      
										  END AS STN_NBR ,			
										'''''''' AS CTY_CD ,
										hc4.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
										hc4.CUST_NME AS CNTCT_NME ,			
										hc4.PHN_EXT AS PHN_EXT_NBR,
										''''CIS'''' AS FSA_MDUL_ID,
										h6.CSG_LVL
								  From Mach5.V_V5U_CUST_ACCT h6 
									INNER JOIN Mach5.V_V5U_CUST_ACCT h4 ON h6.H4 = h4.CIS_CUST_ID
									INNER JOIN  Mach5.v_V5U_CUST_CONTACT hc4 ON h4.CUST_ACCT_ID = hc4.CUST_ACCT_ID
									Where h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
									  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
									  AND h4.TYPE_CD = DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'+


								'	UNION	' + 

								' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select DISTINCT ''''H1'''' AS H6,
									hc1.ROLE_CD AS ROLE_CD, 
									CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN hc1.DOM_PHN_NBR
										  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN hc1.INTL_PHN_NBR
										  ELSE ''''''''
										END AS PHN_NBR,  
									hc1.TME_ZN_CD AS TME_ZONE_ID,
									'''''''' AS FRST_NME,
									'''''''' AS LST_NME ,			
									hc1.EMAIL_ADR AS EMAIL_ADR,
									hc1.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc1.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc1.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
									CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,1,3) 
										WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,1,3) 
									  ELSE ''''''''
									  END AS NPA ,			
								   CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,4,3) 
									  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,4,3) 
									  ELSE ''''''''       
									  END AS  NXX ,			
								   CASE WHEN hc1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc1.DOM_PHN_NBR,7,4) 
									  WHEN hc1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc1.INTL_PHN_NBR,7,4) 
									  ELSE ''''''''      
									  END AS STN_NBR ,			
									'''''''' AS CTY_CD ,
									hc1.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
									hc1.CUST_NME AS CNTCT_NME ,			
									hc1.PHN_EXT AS PHN_EXT_NBR, 
									''''CIS'''' AS FSA_MDUL_ID,
									h6.CSG_LVL
							  From Mach5.V_V5U_CUST_ACCT h6 
								INNER JOIN Mach5.V_V5U_CUST_ACCT h1 ON h6.H1 = h1.CIS_CUST_ID
								INNER JOIN  Mach5.v_V5U_CUST_CONTACT hc1 ON h1.CUST_ACCT_ID = hc1.CUST_ACCT_ID
								Where h6.CUST_ACCT_ID = ' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
								  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
								  AND h1.TYPE_CD = DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'+
								
								'	UNION	' + 

								' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select DISTINCT ''''H2'''' AS H6,
										hc2.ROLE_CD AS ROLE_CD, 
										CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN hc2.DOM_PHN_NBR
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN hc2.INTL_PHN_NBR
										  ELSE ''''''''
										END AS PHN_NBR, 
										hc2.TME_ZN_CD AS TME_ZONE_ID,
										'''''''' AS FRST_NME,
										'''''''' AS LST_NME ,			
										hc2.EMAIL_ADR AS EMAIL_ADR,
										hc2.AVLBLTY_HR_STRT || CASE WHEN LENGTH(hc2.AVLBLTY_HR_STRT) > 0 THEN ''''-'''' ELSE '''''''' END  ||hc2.AVLBLTY_HR_END AS CNTCT_HR_TXT,  	
										CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc2.DOM_PHN_NBR,1,3) 
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc2.INTL_PHN_NBR,1,3) 
										  ELSE ''''''''
										  END AS NPA ,			
										CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc2.DOM_PHN_NBR,4,3) 
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc2.INTL_PHN_NBR,4,3) 
										  ELSE ''''''''       
										  END AS  NXX ,			
									   CASE WHEN hc2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(hc2.DOM_PHN_NBR,7,4) 
										  WHEN hc2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(hc2.INTL_PHN_NBR,7,4) 
										  ELSE ''''''''      
										  END AS STN_NBR ,			
										'''''''' AS CTY_CD ,
										hc2.INTL_CNTRY_DIAL_CD AS ISD_CD,			
										hc2.CUST_NME AS CNTCT_NME ,			
										hc2.PHN_EXT AS PHN_EXT_NBR,
										''''CIS'''' AS FSA_MDUL_ID,
										h6.CSG_LVL
									  From Mach5.V_V5U_CUST_ACCT h6 
											INNER JOIN Mach5.V_V5U_CUST_ACCT h2 ON h6.H2 = h2.CIS_CUST_ID
											INNER JOIN  Mach5.v_V5U_CUST_CONTACT hc2 ON h2.CUST_ACCT_ID = hc2.CUST_ACCT_ID
											Where h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
											  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
											  AND h2.TYPE_CD = DECODE(h2.CIS_HIER_LVL_CD,''''H2'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' 

					
						INSERT INTO #MACH5_ORDR_CHNG_CNTCT_ZCLR 
						(
							[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
							[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
							[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR], [FSA_MDUL_ID], [CSG_LVL]
						)
						EXEC sp_executesql @SQL
					
						SET @SQL = ''
						SET @SQL =   
		  
					
			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select DISTINCT ''''H1'''' AS H1,
								atcH1.ROLE_CD AS ROLE_CD, 
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN atcH1.DOM_PHN_NBR
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN atcH1.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID,
								atcH1.FIRST_NME AS FRST_NME,
								atcH1.LAST_NME AS LST_NME ,			
								atcH1.EMAIL_ADR AS EMAIL_ADR, 
								'''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,1,3) 
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,4,3) 
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN atcH1.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH1.DOM_PHN_NBR,7,4) 
										WHEN atcH1.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH1.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,
								atcH1.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
								atcH1.FIRST_NME || '''' ''''|| atcH1.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,
								''''ATM'''' AS FSA_MDUL_ID,
								h6.CSG_LVL
						From	Mach5.V_V5U_CUST_ACCT h6 
							INNER JOIN Mach5.V_V5U_CUST_ACCT h1 ON h6.H1 = h1.CIS_CUST_ID
							INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH1 ON atcH1.CUST_ACCT_ID = h1.CUST_ACCT_ID
						WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
						   AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
						   AND h1.TYPE_CD = DECODE(h1.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' +
						
						'	UNION	' +  
		  
					
			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select DISTINCT ''''H4'''' AS H4,
								atcH4.ROLE_CD AS ROLE_CD, 
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN atcH4.DOM_PHN_NBR
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN atcH4.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID,
								atcH4.FIRST_NME AS FRST_NME,
								atcH4.LAST_NME AS LST_NME ,			
								atcH4.EMAIL_ADR AS EMAIL_ADR, 
								'''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,1,3) 
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,4,3) 
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN atcH4.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH4.DOM_PHN_NBR,7,4) 
										WHEN atcH4.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH4.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,
								atcH4.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
								atcH4.FIRST_NME || '''' ''''|| atcH4.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,
								''''ATM'''' AS FSA_MDUL_ID,
								h6.CSG_LVL
						From Mach5.V_V5U_CUST_ACCT h6 
								INNER JOIN Mach5.V_V5U_CUST_ACCT h4 ON h6.H4 = h4.CIS_CUST_ID
								INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH4 ON atcH4.CUST_ACCT_ID = h4.CUST_ACCT_ID
						WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
						  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
						  AND h4.TYPE_CD = DECODE(h4.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')' +
						
						'	UNION	' +  
		  
					
			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select DISTINCT ''''H6'''' AS H6,
								vvat.ROLE_CD AS ROLE_CD, 
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN vvat.DOM_PHN_NBR
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN vvat.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID,
								vvat.FIRST_NME AS FRST_NME,
								vvat.LAST_NME AS LST_NME ,			
								vvat.EMAIL_ADR AS EMAIL_ADR, 
								'''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvat.DOM_PHN_NBR,1,3) 
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvat.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvat.DOM_PHN_NBR,4,3) 
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvat.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN vvat.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(vvat.DOM_PHN_NBR,7,4) 
										WHEN vvat.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(vvat.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,
								vvat.INTL_CNTRY_DIAL_CD AS ISD_CD ,			
								vvat.FIRST_NME || '''' ''''|| vvat.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,
								''''ATM'''' AS FSA_MDUL_ID,
								vvca.CSG_LVL
						From Mach5.v_V5U_ACCT_TEAM_CONTACT vvat INNER JOIN
							 Mach5.V_V5U_CUST_ACCT  vvca ON vvca.CUST_ACCT_ID = vvat.CUST_ACCT_ID
						WHERE vvat.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + ''')' +
						
						'	UNION	' +  			  
						' SELECT * ' + 
							' FROM OPENQUERY (M5,
								''Select ''''H2'''' AS H2,	atcH2.ROLE_CD AS ROLE_CD, 
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN atcH2.DOM_PHN_NBR
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN atcH2.INTL_PHN_NBR
										ELSE ''''''''
								END AS  PHN_NBR, 
								'''''''' AS TME_ZONE_ID, atcH2.FIRST_NME AS FRST_NME,atcH2.LAST_NME AS LST_NME ,atcH2.EMAIL_ADR AS EMAIL_ADR, '''''''' AS CNTCT_HR_TXT , 	
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH2.DOM_PHN_NBR,1,3) 
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH2.INTL_PHN_NBR,1,3) 
										ELSE ''''''''
								END AS NPA ,			
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH2.DOM_PHN_NBR,4,3) 
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH2.INTL_PHN_NBR,4,3) 
										ELSE ''''''''       
								END AS  NXX ,			
								CASE	WHEN atcH2.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(atcH2.DOM_PHN_NBR,7,4) 
										WHEN atcH2.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(atcH2.INTL_PHN_NBR,7,4) 
										ELSE ''''''''      
								END AS STN_NBR ,			
								'''''''' AS CTY_CD ,atcH2.INTL_CNTRY_DIAL_CD AS ISD_CD ,atcH2.FIRST_NME || '''' ''''|| atcH2.LAST_NME AS CNTCT_NME ,			
								'''''''' AS PHN_EXT_NBR,''''ATM'''' AS FSA_MDUL_ID,	h6.CSG_LVL
						From Mach5.V_V5U_CUST_ACCT h6 
								INNER JOIN Mach5.V_V5U_CUST_ACCT h2 ON h6.H2 = h2.CIS_CUST_ID
								INNER JOIN Mach5.v_V5U_ACCT_TEAM_CONTACT atcH2 ON atcH2.CUST_ACCT_ID = h2.CUST_ACCT_ID
						WHERE h6.CUST_ACCT_ID =' + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
						  AND h6.TYPE_CD = DECODE(h6.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
						  AND h2.TYPE_CD = DECODE(h2.CIS_HIER_LVL_CD,''''H2'''',''''BILL'''',''''PHYS'''',''''PHYY'''') '')'
								
						
						
						INSERT INTO #MACH5_ORDR_CHNG_CNTCT_ZCLR 
							(
								[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
								[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
								[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID], [CSG_LVL]
							)
						EXEC sp_executesql @SQL
						
						
						SET @SQL = ''
						SET @SQL = 
							' SELECT * ' + 
								' FROM OPENQUERY (M5,
									''Select DISTINCT ''''H6'''' AS H6,
										''''Hrchy'''' AS ROLE_CD, 
										CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN DOM_PHN_NBR 
											  WHEN DOM_INTL_IND_CD = ''''I'''' THEN CONCAT(INTL_CNTRY_DIAL_CD , INTL_PHN_NBR )  
											  ELSE ''''''''
										END AS PHN_NBR, 
										'''''''' AS TME_ZONE_ID,
										'''''''' AS FRST_NME,
										'''''''' AS LST_NME ,			
										'''''''' AS EMAIL_ADR, 
										'''''''' AS CNTCT_HR_TXT,  	
										CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,1,3) 
											  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,1,3) 
											  ELSE ''''''''
										END AS NPA ,			
									   CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,4,3) 
										  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,4,3)
										  ELSE ''''''''       
										END AS  NXX ,			
									   CASE WHEN DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(DOM_PHN_NBR,7,4) 
										  WHEN DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(INTL_PHN_NBR,7,4) 
										  ELSE ''''''''      
										END AS STN_NBR ,			
										'''''''' AS CTY_CD ,
										INTL_CNTRY_DIAL_CD AS ISD_CD ,			
										CUST_NME AS CNTCT_NME ,			
										PHN_EXT AS PHN_EXT_NBR,
										''''CIS'''' AS FSA_MDUL_ID,
										CSG_LVL
									From Mach5.V_V5U_CUST_ACCT
								  WHERE CUST_ACCT_ID ='   + CONVERT(VARCHAR,@CUST_ACCT_ID) + '
								    AND TYPE_CD = DECODE(CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')
									AND ROWNUM = 1 '')'
						
						INSERT INTO #MACH5_ORDR_CHNG_CNTCT_ZCLR 
							(
								[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
								[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
								[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID], [CSG_LVL]
							)
						EXEC sp_executesql @SQL


						SET @SQL = ''
						SET @SQL = 
							' SELECT * ' + 
								' FROM OPENQUERY (M5,
											''Select DISTINCT ''''OD'''' AS H6,
												oc.ROLE_CD AS ROLE_CD, 
												CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN oc.DOM_PHN_NBR
													WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN oc.INTL_PHN_NBR
													ELSE ''''''''
												END AS PHN_NBR, 
												'''''''' AS TME_ZONE_ID,
												oc.FIRST_NME AS FRST_NME,
												oc.LAST_NME AS LST_NME ,			
												oc.EMAIL_ADR AS EMAIL_ADR,
												''''''''  AS CNTCT_HR_TXT,  	
												CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(oc.DOM_PHN_NBR,1,3) 
												  WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(oc.INTL_PHN_NBR,1,3) 
												  ELSE ''''''''
												  END AS NPA ,			
												CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(oc.DOM_PHN_NBR,4,3) 
												  WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(oc.INTL_PHN_NBR,4,3) 
												  ELSE ''''''''       
												  END AS  NXX ,			
											   CASE WHEN oc.DOM_INTL_IND_CD = ''''D'''' THEN SUBSTR(oc.DOM_PHN_NBR,7,4) 
												  WHEN oc.DOM_INTL_IND_CD = ''''I'''' THEN SUBSTR(oc.INTL_PHN_NBR,7,4) 
												  ELSE ''''''''      
												  END AS STN_NBR ,			
												'''''''' AS CTY_CD ,
												oc.INTL_CNTRY_DIAL_CD AS ISD_CD,			
												oc.FIRST_NME || '''' ''''|| oc.LAST_NME AS CNTCT_NME,			
												'''''''' AS PHN_EXT_NBR,
												''''CIS'''' AS FSA_MDUL_ID,
												h6.CSG_LVL
									  From Mach5.V_V5U_ACCT_TEAM_CONTACT_ORDR oc
											INNER JOIN Mach5.V_V5U_ORDR od ON od.ORDR_ID = oc.ORDR_ID
											INNER JOIN Mach5.V_V5U_CUST_ACCT h6 ON h6.CUST_ACCT_ID = od.PRNT_ACCT_ID
											Where oc.ORDR_ID = ' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')'


							
							INSERT INTO #MACH5_ORDR_CHNG_CNTCT_ZCLR 
							(
								[CIS_LVL_TYPE] ,[ROLE_CD],[PHN_NBR],[TME_ZONE_ID],[FRST_NME],
								[LST_NME],[EMAIL_ADR],[CNTCT_HR_TXT],[NPA],[NXX],[STN_NBR],
								[CTY_CD],[ISD_CD],[CNTCT_NME],[PHN_EXT_NBR],[FSA_MDUL_ID], [CSG_LVL]
							)
						EXEC sp_executesql @SQL



						
						SELECT * FROM #MACH5_ORDR_CHNG_CNTCT_ZCLR
						
						
						----Order Notes
						IF OBJECT_ID(N'tempdb..#MACH5_ORDR_CHNG_NOTES_ZCLR',N'U') IS NOT NULL 
						DROP TABLE #MACH5_ORDR_CHNG_NOTES_ZCLR
						CREATE TABLE #MACH5_ORDR_CHNG_NOTES_ZCLR
							(
								NTE_TYPE_CD Varchar(50),
								NTE_TXT		Varchar(max)
							)
						SET @SQL = ''
						SET @SQL = 'SELECT TYPE_CD,NTE_TXT 
										FROM OPENQUERY(M5,
											''SELECT TYPE_CD,NTE_TXT 
													FROM V_V5U_ORDR_NOTE 
													WHERE ORDR_ID =' + CONVERT(VARCHAR,@M5_ORDR_ID) + ''')' 
						
						INSERT INTO #MACH5_ORDR_CHNG_NOTES_ZCLR (NTE_TYPE_CD,NTE_TXT)
							EXEC sp_executesql @SQL
							
						
						Select * From #MACH5_ORDR_CHNG_NOTES_ZCLR
						
						SELECT @Cnt = COUNT(1) FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR
						
						Select @H5_H6_CUST_ID = CUST_ID
							FROM #MACH5_ORDR_CHNG_CUST_ZCLR
							WHERE CIS_LVL_TYPE IN ('H5','H6')
						
						SELECT @INTL_FLG = CASE DOM_PROCESS_FLG_CD
													WHEN 'N' THEN 1
													ELSE 0
												END
								FROM #MACH5_ORDR_CHNG_ZCLR WITH (NOLOCK)
						
						
						--IF ((SELECT ORDR_TYPE_CD from #MACH5_ORDR_CHNG_ZCLR) = 'IN')
						--	SET @PPRT_ID = 627
						--ELSE IF ((SELECT ORDR_TYPE_CD from #MACH5_ORDR_CHNG_ZCLR) = 'DC')
						--	SET @PPRT_ID = 631 
						
						
										
						IF EXISTS
						(
							SELECT 'X'
								FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR
								WHERE CMPNT_TYPE_CD IN ('CPE','OCPE','CPEP','CPEM','CPES')
						)
						BEGIN
							
							SELECT @INTL_FLG = CASE DOM_PROCESS_FLG_CD
													WHEN 'N' THEN 1
													ELSE 0
												END,
									@M5_PROD_ID = PROD_ID
								FROM #MACH5_ORDR_CHNG_ZCLR WITH (NOLOCK)
-- Change transactions for Zscaler can have mixed with install and disconnects on the same tranaction.
-- This begins the Zscaler Install logic.

							IF	@INTL_FLG = 0 AND EXISTS
								(SELECT 'X' FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR 
									WHERE CMPNT_TYPE_CD IN ('CPE','OCPE','CPEM','CPEP','CPES')
									AND NEW_QTY > OLD_QTY)
								BEGIN
								
									SELECT	@Cnt = COUNT(1) 
										FROM	#MACH5_ORDR_CHNG_CMPNT_ZCLR 
										WHERE	CMPNT_TYPE_CD IN ('CPE','OCPE','CPEM','CPEP','CPES')
										AND NEW_QTY > OLD_QTY
										
									SET @Ctr = 0	
									SET @PPRT_ID = 627
									
									IF @Cnt > 0
										BEGIN
											
											WHILE @Ctr < @Cnt 
												BEGIN
												select 'start cows'
													SET @CPE_DEVICE_ID = ''
													SET @STUS_CD = ''
													SET @Continue = 'Y'
													SELECT TOP 1 
														@CPE_DEVICE_ID = ISNULL(CPE_DEVICE_ID,''),
														@STUS_CD = ISNULL(STUS_CD,'')
														FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR
														WHERE FLAG = 0
														AND NEW_QTY > OLD_QTY

													SELECT @CPE_DEVICE_ID AS CPE_DEVICE_ID
													
													SELECT @Continue
													IF @Continue = 'Y'
														BEGIN
															IF ISNULL(@CPE_DEVICE_ID,'') != '' 
																BEGIN
																	INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('Dmstc-CPE:'+@TransName,'insertMach5TxnDetails', GETDATE())
																	SET @ERRLog = @ERRLog + 'Dmstc-CPE:'
																	OPEN SYMMETRIC KEY FS@K3y 
																	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
																	--default region ID to AMNCI and later modified after order insertion in this SP
																	INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID,DMSTC_CD,PROD_ID)	
																		SELECT @OrderCategoryID, 1, 1, 0,PROD_ID
																			FROM #MACH5_ORDR_CHNG_ZCLR o WITH (NOLOCK)
																				INNER JOIN (SELECT top 1 M5_ORDR_ID,NEW_QTY, OLD_QTY FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR  WITH (NOLOCK)
																								WHERE NEW_QTY > OLD_QTY)c
																					ON c.M5_ORDR_ID = o.M5_ORDR_ID
																			WHERE o.M5_ORDR_ID = @M5_ORDR_ID
																				
																			 
																									
																	SELECT @ORDR_ID = SCOPE_IDENTITY()
																	SET @FSA_ORDR_ID = @ORDR_ID
																	
																	select @FSA_ORDR_ID
																	
																	
																	IF @QDA_ORDR_ID != ''
																		SET @QDA_ORDR_ID = @QDA_ORDR_ID + ',' + CONVERT(varchar,@FSA_ORDR_ID)
																	ELSE
																		SET @QDA_ORDR_ID = CONVERT(varchar,@FSA_ORDR_ID)
																	
																	UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
																		SET		QDA_ORDR_ID = @FSA_ORDR_ID
																		WHERE	CPE_DEVICE_ID = @CPE_DEVICE_ID
																				AND NEW_QTY > OLD_QTY
																	
																	UPDATE		odr
																		SET		PRNT_ORDR_ID	=	@ORDR_ID,
																				RAS_DT			=	t.CPE_REQD_ON_SITE_DT,
																				DLVY_CLLI		=	t.CPE_SHIP_CLLI_CD,
																				H5_H6_CUST_ID	=	@H5_H6_CUST_ID,
																				CSG_LVL_ID		=	(SELECT TOP 1 ISNULL(lcl.CSG_LVL_ID,0) FROM #MACH5_ORDR_CHNG_ZCLR mo WITH (NOLOCK) 
																														LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) 
																															ON lcl.CSG_LVL_CD = mo.CSG_LVL 
																														WHERE mo.M5_ORDR_ID = @M5_ORDR_ID),
																				CHARS_ID		=	(SELECT TOP 1 CHARS_CUST_ID FROM #MACH5_ORDR_CHNG_ZCLR WITH (NOLOCK) WHERE M5_ORDR_ID = @M5_ORDR_ID),
																				PPRT_ID         =   @PPRT_ID
																		FROM	dbo.ORDR	odr WITH (ROWLOCK) 
																		INNER JOIN #MACH5_ORDR_CHNG_CMPNT_ZCLR t WITH (NOLOCK) ON odr.ORDR_ID = t.QDA_ORDR_ID
																		WHERE	ORDR_ID			=	@ORDR_ID
																			AND t.CPE_DEVICE_ID = @CPE_DEVICE_ID
																			AND t.NEW_QTY > t.OLD_QTY
																		
																														
																	--FSA_ORDR
																	INSERT INTO FSA_ORDR (ORDR_ID,ORDR_ACTN_ID,FTN,ORDR_TYPE_CD,ORDR_SUB_TYPE_CD,PROD_TYPE_CD,CUST_SIGNED_DT,CUST_WANT_DT,
																							CUST_ORDR_SBMT_DT, CUST_CMMT_DT,CUST_PRMS_OCPY_CD,CUST_ACPT_ERLY_SRVC_CD,MULT_CUST_ORDR_CD,
																							DISC_REAS_CD, INSTL_ESCL_CD,FSA_EXP_TYPE_CD,PRE_QUAL_NBR,PRNT_FTN,RELTD_FTN,
																							CPE_ACCS_PRVDR_CD,CPE_DLVRY_DUTY_AMT, CPE_DLVRY_DUTY_ID,CPE_ECCKT_ID,CPE_TST_TN_NBR,CPE_REC_ONLY_CD,
																							CPE_PHN_NBR, CPE_EQPT_ONLY_CD                         
														
																							)
																	(
																		SELECT	TOP 1 @FSA_ORDR_ID,@ORDR_ACTN_ID,m.ORDER_NBR,'IN' AS ORDR_TYPE_CD,m.ORDR_SUBTYPE_CD,'CP',m.CUST_SIGN_DT,
																				m.CUST_WANT_DT,m.CUST_SBMT_DT,m.CUST_CMMT_DT,m.CUST_PREM_OCCU_FLG_CD,m.CUST_ACPT_SRVC_FLG_CD,m.MULT_ORDR_SBMTD_FLG_CD,
																				m.DSCNCT_REAS_CD,m.EXP_FLG_CD,	CASE m.EXP_TYPE_CD 
																																	WHEN 'B' THEN 'IE'
																																	ELSE m.EXP_TYPE_CD
																																	END
																				,m.PRE_QUAL_NBR
																				,CASE m.ORDR_TYPE_CD
																					WHEN 'CN' THEN m.PRNT_FTN
																					ELSE NULL
																				  END
																				,CASE m.ORDR_TYPE_CD
																					WHEN 'CN' THEN NULL
																					ELSE m.RLTD_ORDR_ID
																				  END
																				,c.CPE_ACCS_PRVDR_NME
																				,c.CPE_DLVRY_DUTIES_AMT
																				,c.CPE_DLVRY_DUTIES_CD
																				,c.CPE_EQPT_CKT_ID
																				,c.CPE_SPRINT_TEST_TN_NBR
																				,c.CPE_RECORD_ONLY_FLG_CD
																				,c.CPE_ACCS_PRVDR_TN_NBR
																				,c.CPE_EQUIPMENT_ONLY_FLG_CD
																			FROM #MACH5_ORDR_CHNG_ZCLR m WITH (NOLOCK)
																			INNER JOIN #MACH5_ORDR_CHNG_CMPNT_ZCLR c WITH (NOLOCK) ON m.M5_ORDR_ID = c.M5_ORDR_ID --AND c.CMPNT_TYPE_CD = 'OCPE'
																			WHERE m.M5_ORDR_ID = @M5_ORDR_ID
																				AND c.NEW_QTY > c.OLD_QTY
																				
																			
																	)
																	
																	select * from FSA_ORDR where ORDR_ID = @FSA_ORDR_ID
																	
																	--ORDR_EXP	
																	IF EXISTS
																	(
																		SELECT 'X'
																			FROM #MACH5_ORDR_CHNG_ZCLR
																			WHERE EXP_FLG_CD = 'Y'
																	)
																	BEGIN
																		INSERT INTO ORDR_EXP (ORDR_ID,
																								EXP_TYPE_ID,
																								AUTHRZR_PHN_NBR,
																								AUTHRZR_TITLE_ID,
																								AUTHN_DT,
																								BILL_TO_COST_CTR_CD,
																								CREAT_DT,
																								CREAT_BY_USER_ID,
																								REC_STUS_ID
																									)
																		(
																			SELECT TOP 1	@FSA_ORDR_ID
																							,CASE EXP_TYPE_CD
																									WHEN 'I' THEN 1
																									WHEN 'E' THEN 2
																									WHEN 'B' THEN 3
																									ELSE 4 
																								END AS EXP_TYPE_ID
																							,APRVR_PHN
																							,APRVR_TITLE
																							,APPRVL_DT
																							,COST_CNTR
																							,GETDATE()
																							,1
																							,1
																				FROM #MACH5_ORDR_CHNG_ZCLR WITH (NOLOCK)
																				
																		)
																	END
																	
																	select 'order id'
																	select @FSA_ORDR_ID
																	select CSG_LVL from #MACH5_ORDR_CHNG_CUST_ZCLR
																	
																	delete from @tmpids
																	-----FSA_ORDR_CUST
																	INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CUST_NME,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID)								
																		(SELECT DISTINCT	@FSA_ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				GETDATE(),
																				CUST_NME,
																				ISNULL(BILL_CYC_ID,0),
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR WITH (NOLOCK)
																			WHERE ISNULL(CSG_LVL,'')=''
																		)

																	INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
																		SELECT DISTINCT	@FSA_ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				GETDATE(),
																				ISNULL(BILL_CYC_ID,0),
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR WITH (NOLOCK)
																			WHERE ISNULL(CSG_LVL,'')!=''
																		
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																				SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString(moc.CUST_NME)
																						FROM #MACH5_ORDR_CHNG_CUST_ZCLR moc WITH (NOLOCK)
																						INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																						INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																						WHERE ISNULL(moc.CSG_LVL,'')!=''
																						  AND foc.CUST_NME IS NULL
																						  
																	delete from @tmpids
																	IF NOT EXISTS
																		(
																			SELECT 'X'
																				FROM dbo.FSA_ORDR_CUST WITH (NOLOCK)
																				WHERE ORDR_ID = @ORDR_ID
																					AND CIS_LVL_TYPE = 'H1'
																		)
																		BEGIN
																			INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CUST_NME,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID)									
																		(SELECT	DISTINCT @FSA_ORDR_ID,
																				'H1',
																				H1_ID,
																				'',
																				'',
																				GETDATE(),
																				'',
																				BILL_CYC_ID,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR WITH (NOLOCK)
																			WHERE CIS_LVL_TYPE = 'H6'
																			AND ISNULL(CSG_LVL,'')=''
																		)

																		INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				'H1',
																				H1_ID,
																				'',
																				'',
																				GETDATE(),
																				BILL_CYC_ID,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR WITH (NOLOCK)
																			WHERE CIS_LVL_TYPE = 'H6'
																			  AND ISNULL(CSG_LVL,'')!=''
																		
																		
																		INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																				SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString('')
																						FROM #MACH5_ORDR_CHNG_CUST_ZCLR moc WITH (NOLOCK)
																						INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																						INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																						WHERE ISNULL(moc.CSG_LVL,'')!=''
																						  AND foc.CUST_NME IS NULL
																		END
																	
																	delete from @tmpids
																	----ORDR_ADR
																	INSERT INTO dbo.ORDR_ADR
																		(
																			ORDR_ID,
																			ADR_TYPE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			CTRY_CD,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			STREET_ADR_1,
																			STREET_ADR_2,
																			CTY_NME,
																			PRVN_NME,
																			STT_CD,
																			ZIP_PSTL_CD,
																			BLDG_NME,
																			FLR_ID,
																			RM_NBR,
																			HIER_LVL_CD,
																			STREET_ADR_3
																		)
																	(
																		SELECT DISTINCT	@FSA_ORDR_ID,
																				CASE CIS_LVL_TYPE
																					WHEN 'H4' THEN 3
																					WHEN 'H6' THEN 18
																					WHEN 'H5' THEN 18
																					ELSE 17
																				END,
																				GETDATE(),
																				1,
																				1,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				'CIS',
																				STREET_ADR_1,
																				STREET_ADR_2,
																				CTY_NME,
																				PRVN_NME,
																				STT_CD,
																				ZIP_PSTL_CD,
																				BLDG_ID,
																				FLOOR_ID,
																				ROOM_ID,
																				CASE CIS_LVL_TYPE	
																					WHEN 'H4' THEN 0
																					WHEN 'H1' THEN 0
																					ELSE 1
																				END,
																				STREET_ADR_3
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR
																			WHERE ISNULL(CSG_LVL,'')=''
																	)
																	
																	INSERT INTO dbo.ORDR_ADR
																		(
																			ORDR_ID,
																			ADR_TYPE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			CTRY_CD,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			HIER_LVL_CD
																		) output inserted.ORDR_ADR_ID into @tmpids
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				CASE CIS_LVL_TYPE
																					WHEN 'H4' THEN 3
																					WHEN 'H6' THEN 18
																					WHEN 'H5' THEN 18
																					ELSE 17
																				END,
																				GETDATE(),
																				1,
																				1,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				'CIS',
																				CASE CIS_LVL_TYPE	
																					WHEN 'H4' THEN 0
																					WHEN 'H1' THEN 0
																					ELSE 1
																				END
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR
																			WHERE ISNULL(CSG_LVL,'')!=''
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
																						STREET_ADR_2,
																						CTY_NME,
																						STT_PRVN_NME,
																						STT_CD,
																						ZIP_PSTL_CD,
																						BLDG_NME,
																						FLR_ID,
																						RM_NBR,
																						STREET_ADR_3)
																					SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.STREET_ADR_1),
																							dbo.encryptString(moc.STREET_ADR_2),
																							dbo.encryptString(moc.CTY_NME),
																							dbo.encryptString(moc.PRVN_NME),
																							dbo.encryptString(moc.STT_CD),
																							dbo.encryptString(moc.ZIP_PSTL_CD),
																							dbo.encryptString(moc.BLDG_ID),
																							dbo.encryptString(moc.FLOOR_ID),
																							dbo.encryptString(moc.ROOM_ID),
																							dbo.encryptString(moc.STREET_ADR_3)
																						FROM #MACH5_ORDR_CHNG_CUST_ZCLR moc WITH (NOLOCK)
																						INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND oa.ORDR_ID = @FSA_ORDR_ID
																						INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
																						WHERE ISNULL(moc.CSG_LVL,'')!=''
																						  AND oa.STREET_ADR_1 IS NULL
																	
																	delete from @tmpids
																	------ORDR_CNTCT
																	INSERT INTO dbo.ORDR_CNTCT
																		(
																			ORDR_ID,
																			CNTCT_TYPE_ID,
																			PHN_NBR,
																			TME_ZONE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			FAX_NBR,
																			ROLE_ID,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			FRST_NME,
																			LST_NME,
																			EMAIL_ADR,
																			CNTCT_HR_TXT,
																			NPA,
																			NXX,
																			STN_NBR,
																			CTY_CD,
																			ISD_CD,
																			CNTCT_NME,
																			PHN_EXT_NBR
																		)
																	(
																		SELECT DISTINCT	@FSA_ORDR_ID ,
																				CASE c.ROLE_CD
																						WHEN 'Hrchy' THEN 17
																						ELSE 1
																					END AS CNTCT_TYPE_ID,
																				c.PHN_NBR,
																				l.TME_ZONE_ID,
																				GETDATE(),
																				1,
																				1,
																				'',
																				lr.ROLE_ID,
																				c.CIS_LVL_TYPE,
																				c.FSA_MDUL_ID,
																				c.FRST_NME,
																				c.LST_NME,
																				c.EMAIL_ADR,
																				c.CNTCT_HR_TXT,
																				c.NPA,
																				c.NXX,
																				c.STN_NBR,
																				c.CTY_CD,
																				c.ISD_CD,
																				c.CNTCT_NME,
																				c.PHN_EXT_NBR
																			FROM #MACH5_ORDR_CHNG_CNTCT_ZCLR c
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD	
																		LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																							TME_ZONE_NME	AS TME_ZONE_NME 
																						FROM dbo.LK_TME_ZONE 
																						GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
																		WHERE ISNULL(CSG_LVL,'')=''
																	)
																	
																	INSERT INTO dbo.ORDR_CNTCT
																		(
																			ORDR_ID,
																			CNTCT_TYPE_ID,
																			PHN_NBR,
																			TME_ZONE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			FAX_NBR,
																			ROLE_ID,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			CNTCT_HR_TXT,
																			NPA,
																			NXX,
																			STN_NBR,
																			CTY_CD,
																			ISD_CD,
																			PHN_EXT_NBR
																		) output inserted.ORDR_CNTCT_ID into @tmpids							
																		SELECT DISTINCT	@FSA_ORDR_ID ,
																				CASE c.ROLE_CD
																						WHEN 'Hrchy' THEN 17
																						ELSE 1
																					END AS CNTCT_TYPE_ID,
																				c.PHN_NBR,
																				l.TME_ZONE_ID,
																				GETDATE(),
																				1,
																				1,
																				'',
																				lr.ROLE_ID,
																				c.CIS_LVL_TYPE,
																				c.FSA_MDUL_ID,
																				c.CNTCT_HR_TXT,
																				c.NPA,
																				c.NXX,
																				c.STN_NBR,
																				c.CTY_CD,
																				c.ISD_CD,
																				c.PHN_EXT_NBR
																			FROM #MACH5_ORDR_CHNG_CNTCT_ZCLR c
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD	
																		LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																							TME_ZONE_NME	AS TME_ZONE_NME 
																						FROM dbo.LK_TME_ZONE 
																						GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
																		WHERE ISNULL(CSG_LVL,'')!=''
																	
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, FRST_NME,
																						LST_NME,
																						CUST_EMAIL_ADR,
																						CUST_CNTCT_NME)
																					SELECT	DISTINCT oc.ORDR_CNTCT_ID, 15, dbo.encryptstring(c.FRST_NME),
																							dbo.encryptstring(c.LST_NME),
																							dbo.encryptstring(c.EMAIL_ADR),
																							dbo.encryptstring(c.CNTCT_NME)
																						FROM #MACH5_ORDR_CHNG_CNTCT_ZCLR c WITH (NOLOCK)
																						INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
																					INNER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ROLE_ID=lr.ROLE_ID AND oc.CIS_LVL_TYPE=c.CIS_LVL_TYPE AND oc.ORDR_ID = @FSA_ORDR_ID
																					INNER JOIN @tmpids tmpids ON tmpids.tmpid = oc.ORDR_CNTCT_ID
																					WHERE ISNULL(c.CSG_LVL,'')!=''
																					  AND oc.FRST_NME IS NULL
																	
																	-----ORDER NOTES
																	IF EXISTS
																		(
																			SELECT 'X'
																				FROM #MACH5_ORDR_CHNG_NOTES_ZCLR WITH (NOLOCK)
																		)
																	BEGIN
																		INSERT INTO ORDR_NTE
																		(
																			NTE_TYPE_ID,
																			ORDR_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			NTE_TXT
																		)
																		(
																			SELECT DISTINCT
																					CASE NTE_TYPE_CD
																						WHEN 'CPEN' THEN 26
																						WHEN 'CPES' THEN 27
																						WHEN 'CPEP' THEN 28
																						ELSE 25
																					END, 
																					@FSA_ORDR_ID,		
																					GETDATE(),
																					1,
																					1,
																					NTE_TXT			
																			FROM #MACH5_ORDR_CHNG_NOTES_ZCLR
																			WHERE NTE_TXT IS NOT NULL
																		)
																	END
																
																	--FSA_ORDR_CPE_LINE_ITEM
																	INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
																		(
																			ORDR_ID,
																			EQPT_TYPE_ID, --EQPT_TYPE_CD 
																			EQPT_ID, --EQPT_ID
																			MFR_NME, --CPE_MFR_NME 
																			CNTRC_TYPE_ID, --CPE_CONTRACT_TYPE
																			CNTRC_TERM_ID, --CPE_CONTRACT_TERM 
																			MNTC_CD, --CPE_MNGD_FLG_CD 
																			CREAT_DT,
																			CMPNT_ID, --CPE_ITEM_ID 
																			LINE_ITEM_CD,
																			MANF_PART_CD, --CPE_MFR_PART_NBR 
																			UNIT_PRICE, --CPE_LIST_PRICE 
																			DROP_SHP, --CPE_DROP_SHIP_FLG_CD 
																			DEVICE_ID, --CPE_DEVICE_ID 
																			ORDR_CMPNT_ID,
																			ITM_STUS,
																			ORDR_QTY,
																			MDS_DES,
																			MANF_DISCNT_CD,
																			MATL_CD,
																			CMPNT_FMLY,
																			RLTD_CMPNT_ID,
																			STUS_CD,
																			MFR_PS_ID,
																			CPE_REUSE_CD,
																			ZSCLR_QUOTE_ID,
																			SPRINT_MNTD_FLG															
																		)
																		SELECT DISTINCT	@FSA_ORDR_ID,
																				CPE_ITEM_TYPE_CD,
																				EQPT_ID,
																				SUBSTRING(ISNULL(CPE_MFR_ID,''),1,50) AS CPE_MFR_NME,
																				CPE_CONTRACT_TYPE,
																				CONVERT(VARCHAR,ISNULL(CPE_CONTRACT_TERM,0)) AS CPE_CONTRACT_TERM,
																				CPE_MNGD_FLG_CD,
																				GETDATE(),
																				ORDR_CMPNT_ID,
																				'CPE' AS LINE_ITEM_CD,
																				SUBSTRING(ISNULL(CPE_MFR_PART_NBR,''),1,20) AS CPE_MFR_PART_NBR,
																				CPE_LIST_PRICE,
																				'Y',      --CPE_DROP_SHIP_FLG_CD,
																				CPE_DEVICE_ID,
																				ORDR_CMPNT_ID,
																				CASE STUS_CD
																					WHEN 'CN' THEN 402
																					ELSE 401
																				END AS ITM_STUS,
																				NEW_QTY - OLD_QTY AS QTY,
																				SUBSTRING(ISNULL(NME,''),1,50) AS NME,
																				CPE_VNDR_DSCNT, 
																				CPE_ITEM_MSTR_NBR,
																				CPE_CMPN_FMLY,
																				RLTD_CMPNT_ID,
																				STUS_CD,
																				CPE_MFR_PS_ID,
																				0 AS CPE_REUSE_CD,
																				QUOTE_ID,
																				CPE_SPRINT_MNT_FLG_CD
																			FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR
																			WHERE	M5_ORDR_ID = @M5_ORDR_ID
																				AND	CPE_DEVICE_ID = @CPE_DEVICE_ID
																				AND NEW_QTY > OLD_QTY	
																	
																	--Update Region ID for xNCI Orders
																	Update od
																		SET od.RGN_ID = ISNULL(lc.RGN_ID,od.RGN_ID)
																		FROM dbo.ORDR od WITH (ROWLOCK)
																	INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON od.ORDR_ID = oa.ORDR_ID
																	INNER JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD 
																		WHERE od.ORDR_ID = @ORDR_ID
																			AND oa.CIS_LVL_TYPE = 'H6'
																			
																	IF @CHNG_ORDR_TYPE_FLG != 1
																		BEGIN
																			----State Machine
																				INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('InsertM5TxnTask:'+CONVERT(VARCHAR,@ORDR_ID)+@TransName,'insertMach5TxnDetails', GETDATE())
																				SET @ERRLog = @ERRLog + 'InsertM5TxnTask:'+CONVERT(VARCHAR,@ORDR_ID)
																				Exec dbo.InsertM5TxnTask @ORDR_ID,@ORDR_TYPE_CD
																			--SELECT @ORDR_ID	
																		END
															
																UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
																	AND NEW_QTY > OLD_QTY
															
																
															END	
														ELSE
															BEGIN
																UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID is null
																AND NEW_QTY > OLD_QTY
															END	
														END
													ELSE
														BEGIN
															UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
																AND NEW_QTY > OLD_QTY
														END
													
												SET @Ctr = @Ctr + 1		
													
												END
										END
												
								END
-- This begins the Zscaler Disconnect logic							
						    IF	@INTL_FLG = 0 AND EXISTS
								(SELECT 'X' FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR 
									WHERE CMPNT_TYPE_CD IN ('CPE','OCPE','CPEM','CPEP','CPES')
									AND OLD_QTY > NEW_QTY)
								BEGIN
																
									SELECT	@Cnt = COUNT(1) 
										FROM	#MACH5_ORDR_CHNG_CMPNT_ZCLR 
										WHERE	CMPNT_TYPE_CD IN ('CPE','OCPE','CPEM','CPEP','CPES')
										AND OLD_QTY > NEW_QTY
										
									SET @Ctr = 0	
									SET @PPRT_ID = 631
									
									IF @Cnt > 0
										BEGIN
											
											WHILE @Ctr < @Cnt 
												BEGIN
												select 'start cows'
													SET @CPE_DEVICE_ID = ''
													SET @STUS_CD = ''
													SET @Continue = 'Y'
													SELECT TOP 1 
														@CPE_DEVICE_ID = ISNULL(CPE_DEVICE_ID,''),
														@STUS_CD = ISNULL(STUS_CD,'')
														FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR
														WHERE FLAG = 0
														AND OLD_QTY > NEW_QTY

													SELECT @CPE_DEVICE_ID AS CPE_DEVICE_ID
													
													SELECT @Continue
													IF @Continue = 'Y'
														BEGIN
															IF ISNULL(@CPE_DEVICE_ID,'') != '' 
																BEGIN
																	INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('Dmstc-CPE:'+@TransName,'insertMach5TxnDetails', GETDATE())
																	SET @ERRLog = @ERRLog + 'Dmstc-CPE:'
																	OPEN SYMMETRIC KEY FS@K3y 
																	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
																	--default region ID to AMNCI and later modified after order insertion in this SP
																	INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID,DMSTC_CD,PROD_ID)	
																		SELECT @OrderCategoryID, 1, 1, 0,PROD_ID
																			FROM #MACH5_ORDR_CHNG_ZCLR o WITH (NOLOCK)
																			INNER JOIN (SELECT top 1 M5_ORDR_ID,NEW_QTY, OLD_QTY FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR  WITH (NOLOCK)
																								WHERE OLD_QTY > NEW_QTY)c
																					ON c.M5_ORDR_ID = o.M5_ORDR_ID
																			WHERE o.M5_ORDR_ID = @M5_ORDR_ID
																									
																	SELECT @ORDR_ID = SCOPE_IDENTITY()
																	SET @FSA_ORDR_ID = @ORDR_ID
																	
																	select @FSA_ORDR_ID
																	
																	IF @QDA_ORDR_ID != ''
																		SET @QDA_ORDR_ID = @QDA_ORDR_ID + ',' + CONVERT(varchar,@FSA_ORDR_ID)
																	ELSE
																		SET @QDA_ORDR_ID = CONVERT(varchar,@FSA_ORDR_ID)
																	
																																		
																	
																	UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
																		SET		QDA_ORDR_ID = @FSA_ORDR_ID
																		WHERE	CPE_DEVICE_ID = @CPE_DEVICE_ID
																				AND OLD_QTY > NEW_QTY
																	
																																
																	UPDATE		odr
																		SET		PRNT_ORDR_ID	=	@ORDR_ID,
																				RAS_DT			=	t.CPE_REQD_ON_SITE_DT,
																				DLVY_CLLI		=	t.CPE_SHIP_CLLI_CD,
																				H5_H6_CUST_ID	=	@H5_H6_CUST_ID,
																				CSG_LVL_ID		=	(SELECT TOP 1 ISNULL(lcl.CSG_LVL_ID,0) FROM #MACH5_ORDR_CHNG_ZCLR mo WITH (NOLOCK) 
																														LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) 
																															ON lcl.CSG_LVL_CD = mo.CSG_LVL 
																														WHERE mo.M5_ORDR_ID = @M5_ORDR_ID),
																				CHARS_ID		=	(SELECT TOP 1 CHARS_CUST_ID FROM #MACH5_ORDR_CHNG_ZCLR WITH (NOLOCK) WHERE M5_ORDR_ID = @M5_ORDR_ID),
																				PPRT_ID         =   @PPRT_ID
																		FROM	dbo.ORDR	odr WITH (ROWLOCK) 
																		INNER JOIN #MACH5_ORDR_CHNG_CMPNT_ZCLR t WITH (NOLOCK) ON odr.ORDR_ID = t.QDA_ORDR_ID
																		WHERE	ORDR_ID			=	@ORDR_ID
																			AND t.CPE_DEVICE_ID = @CPE_DEVICE_ID
																			AND t.OLD_QTY > t.NEW_QTY
																		
																											
																	--FSA_ORDR
																	INSERT INTO FSA_ORDR (ORDR_ID,ORDR_ACTN_ID,FTN,ORDR_TYPE_CD,ORDR_SUB_TYPE_CD,PROD_TYPE_CD,CUST_SIGNED_DT,CUST_WANT_DT,
																							CUST_ORDR_SBMT_DT, CUST_CMMT_DT,CUST_PRMS_OCPY_CD,CUST_ACPT_ERLY_SRVC_CD,MULT_CUST_ORDR_CD,
																							DISC_REAS_CD, INSTL_ESCL_CD,FSA_EXP_TYPE_CD,PRE_QUAL_NBR,PRNT_FTN,RELTD_FTN,
																							CPE_ACCS_PRVDR_CD,CPE_DLVRY_DUTY_AMT, CPE_DLVRY_DUTY_ID,CPE_ECCKT_ID,CPE_TST_TN_NBR,CPE_REC_ONLY_CD,
																							CPE_PHN_NBR, CPE_EQPT_ONLY_CD                         
														
																							)
																	(
																		SELECT	TOP 1 @FSA_ORDR_ID,@ORDR_ACTN_ID,m.ORDER_NBR,'DC',m.ORDR_SUBTYPE_CD,'CP',m.CUST_SIGN_DT,
																				m.CUST_WANT_DT,m.CUST_SBMT_DT,m.CUST_CMMT_DT,m.CUST_PREM_OCCU_FLG_CD,m.CUST_ACPT_SRVC_FLG_CD,m.MULT_ORDR_SBMTD_FLG_CD,
																				m.DSCNCT_REAS_CD,m.EXP_FLG_CD,	CASE m.EXP_TYPE_CD 
																																	WHEN 'B' THEN 'IE'
																																	ELSE m.EXP_TYPE_CD
																																	END
																				,m.PRE_QUAL_NBR
																				,CASE m.ORDR_TYPE_CD
																					WHEN 'CN' THEN m.PRNT_FTN
																					ELSE NULL
																				  END
																				,CASE m.ORDR_TYPE_CD
																					WHEN 'CN' THEN NULL
																					ELSE m.RLTD_ORDR_ID
																				  END
																				,c.CPE_ACCS_PRVDR_NME
																				,c.CPE_DLVRY_DUTIES_AMT
																				,c.CPE_DLVRY_DUTIES_CD
																				,c.CPE_EQPT_CKT_ID
																				,c.CPE_SPRINT_TEST_TN_NBR
																				,c.CPE_RECORD_ONLY_FLG_CD
																				,c.CPE_ACCS_PRVDR_TN_NBR
																				,c.CPE_EQUIPMENT_ONLY_FLG_CD
																			FROM #MACH5_ORDR_CHNG_ZCLR m WITH (NOLOCK)
																			INNER JOIN #MACH5_ORDR_CHNG_CMPNT_ZCLR c WITH (NOLOCK) ON m.M5_ORDR_ID = c.M5_ORDR_ID --AND c.CMPNT_TYPE_CD = 'OCPE'
																			WHERE m.M5_ORDR_ID = @M5_ORDR_ID
																				AND c.OLD_QTY > c.NEW_QTY
																				
																			
																	)
																	
																	select * from FSA_ORDR where ORDR_ID = @FSA_ORDR_ID
																	
																	
																	--ORDR_EXP	
																	IF EXISTS
																	(
																		SELECT 'X'
																			FROM #MACH5_ORDR_CHNG_ZCLR
																			WHERE EXP_FLG_CD = 'Y'
																	)
																	BEGIN
																		INSERT INTO ORDR_EXP (ORDR_ID,
																								EXP_TYPE_ID,
																								AUTHRZR_PHN_NBR,
																								AUTHRZR_TITLE_ID,
																								AUTHN_DT,
																								BILL_TO_COST_CTR_CD,
																								CREAT_DT,
																								CREAT_BY_USER_ID,
																								REC_STUS_ID
																									)
																		(
																			SELECT TOP 1	@FSA_ORDR_ID
																							,CASE EXP_TYPE_CD
																									WHEN 'I' THEN 1
																									WHEN 'E' THEN 2
																									WHEN 'B' THEN 3
																									ELSE 4 
																								END AS EXP_TYPE_ID
																							,APRVR_PHN
																							,APRVR_TITLE
																							,APPRVL_DT
																							,COST_CNTR
																							,GETDATE()
																							,1
																							,1
																				FROM #MACH5_ORDR_CHNG_ZCLR WITH (NOLOCK)
																				
																		)
																	END
																	
																
																	
																	delete from @tmpids
																	-----FSA_ORDR_CUST
																	INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CUST_NME,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID)								
																		(SELECT DISTINCT	@FSA_ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				GETDATE(),
																				CUST_NME,
																				ISNULL(BILL_CYC_ID,0),
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR WITH (NOLOCK)
																			WHERE ISNULL(CSG_LVL,'')=''
																		)

																	INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				GETDATE(),
																				ISNULL(BILL_CYC_ID,0),
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR WITH (NOLOCK)
																			WHERE ISNULL(CSG_LVL,'')!=''
																		
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																				SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString(moc.CUST_NME)
																						FROM #MACH5_ORDR_CHNG_CUST_ZCLR moc WITH (NOLOCK)
																						INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																						INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																						WHERE ISNULL(moc.CSG_LVL,'')!=''
																						  AND foc.CUST_NME IS NULL
																						  
																	delete from @tmpids
																	IF NOT EXISTS
																		(
																			SELECT 'X'
																				FROM dbo.FSA_ORDR_CUST WITH (NOLOCK)
																				WHERE ORDR_ID = @ORDR_ID
																					AND CIS_LVL_TYPE = 'H1'
																		)
																		BEGIN
																			INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CUST_NME,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID)									
																		(SELECT DISTINCT	@FSA_ORDR_ID,
																				'H1',
																				H1_ID,
																				'',
																				'',
																				GETDATE(),
																				'',
																				BILL_CYC_ID,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR WITH (NOLOCK)
																			WHERE CIS_LVL_TYPE = 'H6'
																			AND ISNULL(CSG_LVL,'')=''
																		)

																		INSERT INTO dbo.FSA_ORDR_CUST 
																			(	ORDR_ID,
																				CIS_LVL_TYPE,
																				CUST_ID,
																				BR_CD,
																				SOI_CD,
																				CREAT_DT,
																				CURR_BILL_CYC_CD,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID) output inserted.FSA_ORDR_CUST_ID into @tmpids
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				'H1',
																				H1_ID,
																				'',
																				'',
																				GETDATE(),
																				BILL_CYC_ID,
																				TAX_XMPT_CD,
																				CHARS_CUST_ID,
																				SITE_ID
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR WITH (NOLOCK)
																			WHERE CIS_LVL_TYPE = 'H6'
																			  AND ISNULL(CSG_LVL,'')!=''
																		
																		
																		INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, CUST_NME)
																				SELECT	DISTINCT foc.FSA_ORDR_CUST_ID, 5, dbo.encryptString('')
																						FROM #MACH5_ORDR_CHNG_CUST_ZCLR moc WITH (NOLOCK)
																						INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.CUST_ID = moc.CUST_ID AND foc.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND foc.ORDR_ID=@FSA_ORDR_ID
																						INNER JOIN @tmpids tmpids ON tmpids.tmpid = foc.FSA_ORDR_CUST_ID
																						WHERE ISNULL(moc.CSG_LVL,'')!=''
																						  AND foc.CUST_NME IS NULL
																		END
																	
																	delete from @tmpids
																	----ORDR_ADR
																	INSERT INTO dbo.ORDR_ADR
																		(
																			ORDR_ID,
																			ADR_TYPE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			CTRY_CD,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			STREET_ADR_1,
																			STREET_ADR_2,
																			CTY_NME,
																			PRVN_NME,
																			STT_CD,
																			ZIP_PSTL_CD,
																			BLDG_NME,
																			FLR_ID,
																			RM_NBR,
																			HIER_LVL_CD,
																			STREET_ADR_3
																		)
																	(
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				CASE CIS_LVL_TYPE
																					WHEN 'H4' THEN 3
																					WHEN 'H6' THEN 18
																					WHEN 'H5' THEN 18
																					ELSE 17
																				END,
																				GETDATE(),
																				1,
																				1,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				'CIS',
																				STREET_ADR_1,
																				STREET_ADR_2,
																				CTY_NME,
																				PRVN_NME,
																				STT_CD,
																				ZIP_PSTL_CD,
																				BLDG_ID,
																				FLOOR_ID,
																				ROOM_ID,
																				CASE CIS_LVL_TYPE	
																					WHEN 'H4' THEN 0
																					WHEN 'H1' THEN 0
																					ELSE 1
																				END,
																				STREET_ADR_3
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR
																			WHERE ISNULL(CSG_LVL,'')=''
																	)
																	
																	INSERT INTO dbo.ORDR_ADR
																		(
																			ORDR_ID,
																			ADR_TYPE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			CTRY_CD,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			HIER_LVL_CD
																		) output inserted.ORDR_ADR_ID into @tmpids
																		SELECT	DISTINCT @FSA_ORDR_ID,
																				CASE CIS_LVL_TYPE
																					WHEN 'H4' THEN 3
																					WHEN 'H6' THEN 18
																					WHEN 'H5' THEN 18
																					ELSE 17
																				END,
																				GETDATE(),
																				1,
																				1,
																				CTRY_CD,
																				CIS_LVL_TYPE,
																				'CIS',
																				CASE CIS_LVL_TYPE	
																					WHEN 'H4' THEN 0
																					WHEN 'H1' THEN 0
																					ELSE 1
																				END
																			FROM #MACH5_ORDR_CHNG_CUST_ZCLR
																			WHERE ISNULL(CSG_LVL,'')!=''
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, STREET_ADR_1,
																						STREET_ADR_2,
																						CTY_NME,
																						STT_PRVN_NME,
																						STT_CD,
																						ZIP_PSTL_CD,
																						BLDG_NME,
																						FLR_ID,
																						RM_NBR,
																						STREET_ADR_3)
																					SELECT	DISTINCT oa.ORDR_ADR_ID, 14, dbo.encryptString(moc.STREET_ADR_1),
																							dbo.encryptString(moc.STREET_ADR_2),
																							dbo.encryptString(moc.CTY_NME),
																							dbo.encryptString(moc.PRVN_NME),
																							dbo.encryptString(moc.STT_CD),
																							dbo.encryptString(moc.ZIP_PSTL_CD),
																							dbo.encryptString(moc.BLDG_ID),
																							dbo.encryptString(moc.FLOOR_ID),
																							dbo.encryptString(moc.ROOM_ID),
																							dbo.encryptString(moc.STREET_ADR_3)
																						FROM #MACH5_ORDR_CHNG_CUST_ZCLR moc WITH (NOLOCK)
																						INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.CIS_LVL_TYPE=moc.CIS_LVL_TYPE AND oa.ORDR_ID = @FSA_ORDR_ID
																						INNER JOIN @tmpids tmpids ON tmpids.tmpid = oa.ORDR_ADR_ID
																						WHERE ISNULL(moc.CSG_LVL,'')!=''
																						  AND oa.STREET_ADR_1 IS NULL
																	
																	delete from @tmpids
																	------ORDR_CNTCT
																	INSERT INTO dbo.ORDR_CNTCT
																		(
																			ORDR_ID,
																			CNTCT_TYPE_ID,
																			PHN_NBR,
																			TME_ZONE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			FAX_NBR,
																			ROLE_ID,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			FRST_NME,
																			LST_NME,
																			EMAIL_ADR,
																			CNTCT_HR_TXT,
																			NPA,
																			NXX,
																			STN_NBR,
																			CTY_CD,
																			ISD_CD,
																			CNTCT_NME,
																			PHN_EXT_NBR
																		)
																	(
																		SELECT DISTINCT	@FSA_ORDR_ID ,
																				CASE c.ROLE_CD
																						WHEN 'Hrchy' THEN 17
																						ELSE 1
																					END AS CNTCT_TYPE_ID,
																				c.PHN_NBR,
																				l.TME_ZONE_ID,
																				GETDATE(),
																				1,
																				1,
																				'',
																				lr.ROLE_ID,
																				c.CIS_LVL_TYPE,
																				c.FSA_MDUL_ID,
																				c.FRST_NME,
																				c.LST_NME,
																				c.EMAIL_ADR,
																				c.CNTCT_HR_TXT,
																				c.NPA,
																				c.NXX,
																				c.STN_NBR,
																				c.CTY_CD,
																				c.ISD_CD,
																				c.CNTCT_NME,
																				c.PHN_EXT_NBR
																			FROM #MACH5_ORDR_CHNG_CNTCT_ZCLR c
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD	
																		LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																							TME_ZONE_NME	AS TME_ZONE_NME 
																						FROM dbo.LK_TME_ZONE 
																						GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
																		WHERE ISNULL(CSG_LVL,'')=''
																	)
																	
																	INSERT INTO dbo.ORDR_CNTCT
																		(
																			ORDR_ID,
																			CNTCT_TYPE_ID,
																			PHN_NBR,
																			TME_ZONE_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			FAX_NBR,
																			ROLE_ID,
																			CIS_LVL_TYPE,
																			FSA_MDUL_ID,
																			CNTCT_HR_TXT,
																			NPA,
																			NXX,
																			STN_NBR,
																			CTY_CD,
																			ISD_CD,
																			PHN_EXT_NBR
																		) output inserted.ORDR_CNTCT_ID into @tmpids							
																		SELECT	DISTINCT @FSA_ORDR_ID ,
																				CASE c.ROLE_CD
																						WHEN 'Hrchy' THEN 17
																						ELSE 1
																					END AS CNTCT_TYPE_ID,
																				c.PHN_NBR,
																				l.TME_ZONE_ID,
																				GETDATE(),
																				1,
																				1,
																				'',
																				lr.ROLE_ID,
																				c.CIS_LVL_TYPE,
																				c.FSA_MDUL_ID,
																				c.CNTCT_HR_TXT,
																				c.NPA,
																				c.NXX,
																				c.STN_NBR,
																				c.CTY_CD,
																				c.ISD_CD,
																				c.PHN_EXT_NBR
																			FROM #MACH5_ORDR_CHNG_CNTCT_ZCLR c
																		INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD	
																		LEFT JOIN (SELECT	MIN(TME_ZONE_ID) AS TME_ZONE_ID, 
																							TME_ZONE_NME	AS TME_ZONE_NME 
																						FROM dbo.LK_TME_ZONE 
																						GROUP BY TME_ZONE_NME) l on c.TME_ZONE_ID = l.TME_ZONE_NME
																		WHERE ISNULL(CSG_LVL,'')!=''
																	
																	
																	INSERT INTO dbo.CUST_SCRD_DATA (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, FRST_NME,
																						LST_NME,
																						CUST_EMAIL_ADR,
																						CUST_CNTCT_NME)
																					SELECT	DISTINCT oc.ORDR_CNTCT_ID, 15, dbo.encryptstring(c.FRST_NME),
																							dbo.encryptstring(c.LST_NME),
																							dbo.encryptstring(c.EMAIL_ADR),
																							dbo.encryptstring(c.CNTCT_NME)
																						FROM #MACH5_ORDR_CHNG_CNTCT_ZCLR c WITH (NOLOCK)
																						INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD
																					INNER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ROLE_ID=lr.ROLE_ID AND oc.CIS_LVL_TYPE=c.CIS_LVL_TYPE AND oc.ORDR_ID = @FSA_ORDR_ID
																					INNER JOIN @tmpids tmpids ON tmpids.tmpid = oc.ORDR_CNTCT_ID
																					WHERE ISNULL(c.CSG_LVL,'')!=''
																					  AND oc.FRST_NME IS NULL
																	
																	-----ORDER NOTES
																	IF EXISTS
																		(
																			SELECT 'X'
																				FROM #MACH5_ORDR_CHNG_NOTES_ZCLR WITH (NOLOCK)
																		)
																	BEGIN
																		INSERT INTO ORDR_NTE
																		(
																			NTE_TYPE_ID,
																			ORDR_ID,
																			CREAT_DT,
																			CREAT_BY_USER_ID,
																			REC_STUS_ID,
																			NTE_TXT
																		)
																		(
																			SELECT DISTINCT
																				CASE NTE_TYPE_CD
																					WHEN 'CPEN' THEN 26
																					WHEN 'CPES' THEN 27
																					WHEN 'CPEP' THEN 28
																					ELSE 25
																				END, 
																				@FSA_ORDR_ID,		
																				GETDATE(),
																				1,
																				1,
																				NTE_TXT				
																			FROM #MACH5_ORDR_CHNG_NOTES_ZCLR
																			WHERE NTE_TXT IS NOT NULL
																		)
																	END
																
																	--FSA_ORDR_CPE_LINE_ITEM
																	INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM
																		(
																			ORDR_ID,
																			EQPT_TYPE_ID, --EQPT_TYPE_CD 
																			EQPT_ID, --EQPT_ID
																			MFR_NME, --CPE_MFR_NME 
																			CNTRC_TYPE_ID, --CPE_CONTRACT_TYPE
																			CNTRC_TERM_ID, --CPE_CONTRACT_TERM 
																			MNTC_CD, --CPE_MNGD_FLG_CD 
																			CREAT_DT,
																			CMPNT_ID, --CPE_ITEM_ID 
																			LINE_ITEM_CD,
																			MANF_PART_CD, --CPE_MFR_PART_NBR 
																			UNIT_PRICE, --CPE_LIST_PRICE 
																			DROP_SHP, --CPE_DROP_SHIP_FLG_CD 
																			DEVICE_ID, --CPE_DEVICE_ID 
																			ORDR_CMPNT_ID,
																			ITM_STUS,
																			ORDR_QTY,
																			MDS_DES,
																			MANF_DISCNT_CD,
																			MATL_CD,
																			CMPNT_FMLY,
																			RLTD_CMPNT_ID,
																			STUS_CD,
																			MFR_PS_ID,
																			CPE_REUSE_CD,
																			ZSCLR_QUOTE_ID,
																			SPRINT_MNTD_FLG															
																		)
																		SELECT DISTINCT	@FSA_ORDR_ID,
																				CPE_ITEM_TYPE_CD,
																				EQPT_ID,
																				SUBSTRING(ISNULL(CPE_MFR_ID,''),1,50) AS CPE_MFR_NME,
																				CPE_CONTRACT_TYPE,
																				CONVERT(VARCHAR,ISNULL(CPE_CONTRACT_TERM,0)) AS CPE_CONTRACT_TERM,
																				CPE_MNGD_FLG_CD,
																				GETDATE(),
																				ORDR_CMPNT_ID,
																				'CPE' AS LINE_ITEM_CD,
																				SUBSTRING(ISNULL(CPE_MFR_PART_NBR,''),1,20) AS CPE_MFR_PART_NBR,
																				CPE_LIST_PRICE,
																				CPE_DROP_SHIP_FLG_CD,
																				CPE_DEVICE_ID,
																				ORDR_CMPNT_ID,
																				CASE STUS_CD
																					WHEN 'CN' THEN 402
																					ELSE 401
																				END AS ITM_STUS,
																				(NEW_QTY - OLD_QTY) * -1 AS QTY,
																				SUBSTRING(ISNULL(NME,''),1,50) AS NME,
																				CPE_VNDR_DSCNT, 
																				CPE_ITEM_MSTR_NBR,
																				CPE_CMPN_FMLY,
																				RLTD_CMPNT_ID,
																				STUS_CD,
																				CPE_MFR_PS_ID,
																				0 AS CPE_REUSE_CD,
																				QUOTE_ID,
																				CPE_SPRINT_MNT_FLG_CD
																			FROM #MACH5_ORDR_CHNG_CMPNT_ZCLR
																			WHERE	M5_ORDR_ID = @M5_ORDR_ID
																				AND	CPE_DEVICE_ID = @CPE_DEVICE_ID
																				AND OLD_QTY > NEW_QTY	
																	
																	--Update Region ID for xNCI Orders
																	Update od
																		SET od.RGN_ID = ISNULL(lc.RGN_ID,od.RGN_ID)
																		FROM dbo.ORDR od WITH (ROWLOCK)
																	INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON od.ORDR_ID = oa.ORDR_ID
																	INNER JOIN dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD 
																		WHERE od.ORDR_ID = @ORDR_ID
																			AND oa.CIS_LVL_TYPE = 'H6'
																			
																	IF @CHNG_ORDR_TYPE_FLG != 1
																		BEGIN
																			----State Machine
																				INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('InsertM5TxnTask:'+CONVERT(VARCHAR,@ORDR_ID)+@TransName,'insertMach5TxnDetails', GETDATE())
																				SET @ERRLog = @ERRLog + 'InsertM5TxnTask:'+CONVERT(VARCHAR,@ORDR_ID)
																				Exec dbo.InsertM5TxnTask @ORDR_ID,@ORDR_TYPE_CD
																			--SELECT @ORDR_ID	
																		END
															
																UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
																		AND OLD_QTY > NEW_QTY
															
																
															END	
														ELSE
															BEGIN
																UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID is null
																		AND OLD_QTY > NEW_QTY
															END	
														END
													ELSE
														BEGIN
															UPDATE #MACH5_ORDR_CHNG_CMPNT_ZCLR
																SET	FLAG = 1
																WHERE CPE_DEVICE_ID = @CPE_DEVICE_ID
																		AND OLD_QTY > NEW_QTY
														END
													
												SET @Ctr = @Ctr + 1		
													
												END
										END
												
								END	
								

							
							
						END
					END
				END		
	
	IF @ORDR_TYPE_CD in ('REUS') -- NUA change on Access Reuse CPE. Only need to send to ODIE.
		BEGIN 
			INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('REUS:'+ @TransName,'insertMach5TxnDetails', GETDATE())
			
			IF OBJECT_ID(N'tempdb..#MACH5_CHNG_REUS',N'U') IS NOT NULL 
				DROP TABLE #MACH5_CHNG_REUS

			CREATE TABLE #MACH5_CHNG_REUS
				(
					M5_CHNG_TXN_ID		Int,
					CPE_DEV_ID		Varchar(50),
					CUR_NUA			Varchar(50),
					OLD_NUA			Varchar(50)
				)
				
				SET @SQL = 'SELECT ORDR_ID, CPE_DEV_ID, CUR_NUA, OLD_NUA 
					FROM OPENQUERY (NRMBPM,		
					''SELECT DISTINCT ORDR_ID,CPE_DEV_ID,CUR_NUA, OLD_NUA 
					FROM BPMF_OWNER.BPM_COWS_NRFC  
					WHERE	ORDR_ID = ' + CONVERT(VARCHAR(10),@M5_ORDR_ID)+ '
								ORDER BY ORDR_ID '')'
			
			INSERT INTO #MACH5_CHNG_REUS (M5_CHNG_TXN_ID, CPE_DEV_ID, CUR_NUA, OLD_NUA)									
			EXEC sp_executesql @SQL

			DECLARE @M5_CHNG_TXN_ID		Int,
					@CPE_DEV_ID		Varchar(50),
					@CUR_NUA			Varchar(50),
					@OLD_NUA			Varchar(50)

			SELECT DISTINCT	 @M5_CHNG_TXN_ID = M5_CHNG_TXN_ID
						   , @CPE_DEV_ID = CPE_DEV_ID 
						   , @CUR_NUA = CUR_NUA
						   , @OLD_NUA = OLD_NUA		   
					FROM #MACH5_CHNG_REUS

			INSERT INTO CPE_NUA_CHNG WITH (ROWLOCK) (M5_CHNG_TXN_ID, CPE_DEV_ID, CUR_NUA, OLD_NUA,CREAT_DT)
				VALUES (@M5_CHNG_TXN_ID,@CPE_DEV_ID,@CUR_NUA,@OLD_NUA,GETDATE())
			
			INSERT INTO dbo.ODIE_REQ WITH (ROWLOCK)
							   ([ODIE_MSG_ID],[STUS_MOD_DT],[CREAT_DT],[STUS_ID]
							   ,[DEV_FLTR],[CSG_LVL_ID])
						VALUES		
								( 10, GETDATE(),GETDATE(), 10, @CPE_DEV_ID, 0)	
			
			SET @QDA_ORDR_ID = 'R'-- This 'R' is use to get pass the order extract validations in getM5Order and update the NRMBPM view with a 'Y' extract flag.
								  -- These REUS transactions only send a message to ODIE to update NUA, no order data to extract and populate COWS order table and workflow.
								  -- PJ022373 July 21 Release. dlp0278  	
						
		END 	
			
	IF (ISNULL(@QDA_ORDR_ID,'') = '')
		SET @QDA_ORDR_ID = 'MD'
	
	COMMIT TRANSACTION	@TransName
	
	END TRY
	
	BEGIN CATCH
		IF (((XACT_STATE()) IN (1,-1)) AND (@@TRANCOUNT>0))
		BEGIN
			ROLLBACK TRANSACTION @TransName
			INSERT INTO dbo.SYS_LOG (Notes, Process, CreatDt) VALUES ('Rollback:'+@ERRLog+';'+@TransName,'insertMach5TxnDetails', GETDATE())
		END
		UPDATE dbo.M5_BPM_COWS_NRFC
			SET Error_msg = SUBSTRING(ERROR_MESSAGE(),1,1000)
			WHERE ORDR_ID = @M5_ORDR_ID
		DECLARE @ErrNote VARCHAR(1000) = '@M5_ORDR_ID'+CONVERT(VARCHAR,@M5_ORDR_ID)
		--Set Error to IE(Internal Error), No need to set NRMBPM record to Extract_CD to E
		SET @QDA_ORDR_ID = 'IE'
		EXEC dbo.insertErrorInfo --@ErrNote
	END CATCH	
			
END