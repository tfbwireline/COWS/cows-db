USE [COWS]
GO
_CreateObject 'SP','web','getEventDevSrvcMgmt'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  jrg7298            
-- Create date: 01/30/2016         
-- Description: Get the device management data using EventID
-- =============================================            
ALTER PROCEDURE [web].[getEventDevSrvcMgmt] --9881
 @eventID INT
AS            
BEGIN            
SET NOCOUNT ON;
BEGIN TRY

 IF OBJECT_ID(N'tempdb..#DevSrvc', N'U') IS NOT NULL         
	DROP TABLE #DevSrvc

 IF OBJECT_ID(N'tempdb..#M5Ordr', N'U') IS NOT NULL         
	DROP TABLE #M5Ordr

CREATE TABLE #DevSrvc
(DevSrvcMgmtID INT NULL,
EventID INT NULL,
DeviceID VARCHAR(200) NULL,
MNSOrdrID INT NULL,
MNSOrdrNbr VARCHAR(255) NULL,
OdieDevNme	VARCHAR(255) NULL,
ServiceTierID  TINYINT NULL,
ServiceTier  VARCHAR(255) NULL,
CmpntID	VARCHAR(255) NULL,
CmpntStus	VARCHAR(255) NULL,
CmpntType   CHAR(4) NULL,
CmpntNme	VARCHAR(255) NULL,
SendToOdie  BIT NULL,
CreatDt  VARCHAR(255) NULL)

CREATE TABLE #M5Ordr
(ORDR_ID INT NULL,
ORDR_NBR VARCHAR(50) NULL)
 
 INSERT INTO #DevSrvc
 SELECT DISTINCT EVENT_DEV_SRVC_MGMT_ID,
 ed.EVENT_ID,
 ed.DEVICE_ID,
 0,
 ed.MNS_ORDR_NBR,
 ed.ODIE_DEV_NME,
 ed.MNS_SRVC_TIER_ID,
 lm.MDS_SRVC_TIER_DES,
 ed.M5_ORDR_CMPNT_ID,
 ed.CMPNT_STUS,
 ed.CMPNT_TYPE_CD,
 ed.CMPNT_NME,
 ISNULL(ed.ODIE_CD,0),
 CONVERT(VARCHAR,ed.CREAT_DT,101)
 FROM dbo.EVENT_DEV_SRVC_MGMT ed WITH (NOLOCK) INNER JOIN
 dbo.LK_MDS_SRVC_TIER lm WITH (NOLOCK) ON lm.MDS_SRVC_TIER_ID = ed.MNS_SRVC_TIER_ID
 WHERE ed.EVENT_ID = @eventID
   AND ed.REC_STUS_ID = 1
 
 
		declare @ordrnbrs varchar(max)
		select @ordrnbrs = ISNULL(@ordrnbrs,'') + ISNULL(MNSOrdrNbr,'') + ''''', '''''
		from #DevSrvc
		if(len(@ordrnbrs)>2)
		set  @ordrnbrs = substring(@ordrnbrs, 1, len(@ordrnbrs)-6)
 
declare @mnsordr nvarchar(max) = 'INSERT INTO #M5Ordr (ORDR_NBR
 ,ORDR_ID) Select ORDER_NBR
,ORDR_ID  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
,vvo.ORDR_ID
							from MACH5.V_V5U_ORDR vvo
							where vvo.ORDER_NBR IN (''''' + @ordrnbrs + ''''') '')'

								--print @mnsordr
exec sp_executesql @mnsordr


UPDATE ds
SET MNSOrdrID = mo.ORDR_ID
FROM #DevSrvc ds INNER JOIN
#M5Ordr mo WITH (NOLOCK) ON mo.ORDR_NBR = ds.MNSOrdrNbr

SELECT * FROM #DevSrvc

END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END 