USE [COWS]
GO
_CreateObject 'SP','dbo','ProcessNonTransportCancels'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <09/26/2012>
-- Description:	<To process non transport and non cpe cancels>
-- =============================================
ALTER PROCEDURE dbo.ProcessNonTransportCancels 
	@ORDR_ID INT
AS
BEGIN
	BEGIN TRY

DECLARE @ORDR_TYPE_CD	CHAR(2)
DECLARE @PrntFTN		varchar(20)
DECLARE @PrntOrderId	INT
DECLARE @PRNT_PPRT_ID	INT
DECLARE @ORDR_ACTN_ID	INT

		
	SELECT	@ORDR_TYPE_CD	=	ORDR_TYPE_CD,
			@PrntFTN		=	PRNT_FTN,
			@ORDR_ACTN_ID   =   ORDR_ACTN_ID
		FROM	dbo.FSA_ORDR  WITH (NOLOCK)
		WHERE	ORDR_ID = @ORDR_ID
	EXEC	dbo.insertEmailRequest	@ORDR_ID,	2,	10
		
	IF @ORDR_TYPE_CD = 'CN' --Cancel orders submitted from FSA
		BEGIN
			SELECT		@PrntOrderId	=	ISNULL(fo.ORDR_ID, 0),
								@PRNT_PPRT_ID	=	ISNULL(o.PPRT_ID,0)
						FROM	dbo.FSA_ORDR	fo	WITH (NOLOCK)
					INNER JOIN	dbo.ORDR		o	WITH (NOLOCK) ON fo.ORDR_ID = o.ORDR_ID
						WHERE	FTN				=	@PrntFTN
			
			DELETE  FROM dbo.ACT_TASK		WITH (ROWLOCK) WHERE ORDR_ID = @PrntOrderId
			DELETE  FROM dbo.WG_PROF_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @PrntOrderId
			DELETE  FROM dbo.WG_PTRN_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @PrntOrderId
			EXEC [dbo].[deleteActiveTasks]	@ORDR_ID 
			
			
			UPDATE		dbo.ORDR	WITH (ROWLOCK)
				SET		ORDR_STUS_ID	=	4
				WHERE	ORDR_ID			=	@PrntOrderId
				
			UPDATE		dbo.ORDR	WITH (ROWLOCK)
				SET		ORDR_STUS_ID	=	4,
						PPRT_ID			=	@PRNT_PPRT_ID -- To display cancelled order in grp specific completed view
				WHERE	ORDR_ID			=	@ORDR_ID

			--To remove related order association if exists & insert notes.
			Exec dbo.removeRelatedOrderAssociation @PrntOrderId	
		END
	ELSE -- Cancel orders initiated from COWS
		BEGIN
			--EXEC [dbo].[deleteActiveTasks]	@ORDR_ID
			IF  (@ORDR_ACTN_ID = 1)
					BEGIN
						EXEC [dbo].[deleteActiveTasks] @ORDR_ID
					END
				ELSE
					BEGIN
						--EXEC [dbo].[deleteActiveTasks]	@ORDR_ID --causing decrement in user assignment
						DELETE  FROM dbo.ACT_TASK		WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
						DELETE  FROM dbo.WG_PROF_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
						DELETE  FROM dbo.WG_PTRN_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
					End
			
			UPDATE		dbo.ORDR	WITH (ROWLOCK)
				SET		ORDR_STUS_ID	=	4
				WHERE	ORDR_ID			=	@ORDR_ID
				
			--To remove related order association if exists & insert notes.
			Exec dbo.removeRelatedOrderAssociation @ORDR_ID
		END
		
	END TRY
	
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END
GO
