USE [COWS]
GO
_CreateObject 'SP','dbo','CreateSDEEmailRequest'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: <Sarah Sandoval>
-- Create Date: 08032017
-- Description:	Create Email Request for New SDE Opportunity
-- =============================================
ALTER PROCEDURE [dbo].[CreateSDEEmailRequest] 
	@SDE_OPPTNTY_ID		INT		=	0
AS
BEGIN
	BEGIN TRY
		
		SET NOCOUNT ON;
		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
		DECLARE @CMPNY_NME				VARCHAR(MAX)	=	NULL
				,@REQ_EMAIL				VARCHAR(MAX)	=	NULL
				,@MODFD_BY_EMAIL		VARCHAR(MAX)	=	NULL
				,@SDE_PDL				VARCHAR(MAX)	=	NULL
				,@RFP_PDL				VARCHAR(MAX)	=	NULL
				,@IS_RFP				BIT				=	0
				,@EMAIL_REQ_TYPE_ID		INT				=	0
				,@ASSIGNED_USR			INT				=	0
				,@ASSIGNED_EMAIL		VARCHAR(MAX)	=	NULL
				,@ASSIGNED_TEXT			VARCHAR(MAX)	=	NULL
				,@CC_ADR				VARCHAR(MAX)	=	NULL
				,@DOC_LIST				VARCHAR(MAX)	=	NULL
				,@EMAIL_BODY			VARCHAR(MAX)	=	NULL
				,@EMAIL_TITLE			VARCHAR(MAX)	=	NULL
				,@EMAIL_TO				VARCHAR(MAX)	=	NULL
				,@EMAIL_CC				VARCHAR(MAX)	=	NULL
		
		-- Set SDE Opportunity Values from DB record
		SELECT @CMPNY_NME = dbo.decryptBinaryData(csd.CUST_NME), @IS_RFP = so.RFP_CD,
			@REQ_EMAIL = lu.EMAIL_ADR, @CC_ADR = so.CC_ADR, @ASSIGNED_USR = la.USER_ID, 
			@MODFD_BY_EMAIL = lm.EMAIL_ADR, @ASSIGNED_EMAIL = la.EMAIL_ADR
		FROM dbo.SDE_OPPTNTY so WITH (NOLOCK)
		INNER JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=so.SDE_OPPTNTY_ID AND csd.SCRD_OBJ_TYPE_ID=26
		JOIN dbo.LK_USER lu WITH (NOLOCK) ON so.CREAT_BY_USER_ID = lu.USER_ID
		JOIN dbo.LK_USER la WITH (NOLOCK) ON so.ASN_TO_USER_ID = la.USER_ID
		LEFT JOIN dbo.LK_USER lm WITH (NOLOCK) ON so.MODFD_BY_USER_ID = lm.USER_ID
		WHERE so.SDE_OPPTNTY_ID = @SDE_OPPTNTY_ID
		
		SELECT @EMAIL_REQ_TYPE_ID = EMAIL_REQ_TYPE_ID 
		FROM dbo.LK_EMAIL_REQ_TYPE WITH (NOLOCK)
		WHERE EMAIL_REQ_TYPE = 'SDEOpportunityEmail'
		
		SELECT @SDE_PDL = CFG_KEY_VALU_TXT
		FROM dbo.LK_COWS_APP_CFG WITH (NOLOCK)
		WHERE CFG_KEY_NME = 'SDE Opportunity PDL'
		
		SELECT @RFP_PDL = CFG_KEY_VALU_TXT
		FROM dbo.LK_COWS_APP_CFG WITH (NOLOCK)
		WHERE CFG_KEY_NME = 'RFP PDL'
		
		IF (@ASSIGNED_USR = 1)
		BEGIN
			SET @EMAIL_TITLE = 'MDS Design Opportunity Submitted for ' 
					+ CONVERT(VARCHAR,@SDE_OPPTNTY_ID) + ' - ' + @CMPNY_NME
			SET @ASSIGNED_TEXT = ''
			SET @EMAIL_TO = @REQ_EMAIL
			SET @EMAIL_CC = CASE WHEN (LEN(ISNULL(@MODFD_BY_EMAIL,''))>0) 
								THEN @SDE_PDL + ';' + @MODFD_BY_EMAIL 
								ELSE @SDE_PDL END
			SET @EMAIL_CC = CASE WHEN (LEN(ISNULL(@CC_ADR,''))>0) 
								THEN @EMAIL_CC + ';' + @CC_ADR 
								ELSE @EMAIL_CC END
			SET @EMAIL_CC = CASE WHEN (@IS_RFP = 1) 
								THEN @EMAIL_CC + ';' + @RFP_PDL 
								ELSE @EMAIL_CC END
		END
		ELSE
		BEGIN
			SET @EMAIL_TITLE = 'MDS Design Opportunity Assignment for ' 
					+ CONVERT(VARCHAR,@SDE_OPPTNTY_ID) + ' - ' + @CMPNY_NME
			SET @ASSIGNED_TEXT = 'You have been assigned to this request.'
			SET @EMAIL_TO = @ASSIGNED_EMAIL
			SET @EMAIL_CC = CASE WHEN (LEN(ISNULL(@MODFD_BY_EMAIL,''))>0) 
								THEN @SDE_PDL + ';' + @REQ_EMAIL + ';' + @MODFD_BY_EMAIL 
								ELSE @SDE_PDL + ';' + @REQ_EMAIL END
			SET @EMAIL_CC = CASE WHEN (LEN(ISNULL(@CC_ADR,''))>0) 
								THEN @EMAIL_CC + ';' + @CC_ADR 
								ELSE @EMAIL_CC END
			SET @EMAIL_CC = CASE WHEN (@IS_RFP = 1) 
								THEN @EMAIL_CC + ';' + @RFP_PDL 
								ELSE @EMAIL_CC END
		END
		
		-- Set Email Body
		SET @EMAIL_BODY = (SELECT	
			(SELECT	sde.SDE_OPPTNTY_ID AS 'OpportunityID'
					,dbo.decryptBinaryData(csd.CUST_NME) AS 'CompanyName'
					,dbo.decryptBinaryData(csd.STREET_ADR_1) AS 'Address'
					,sde.RGN AS 'Region'
					,dbo.decryptBinaryData(csd.STT_PRVN_NME) AS 'State'
					,dbo.decryptBinaryData(csd.CTY_NME) AS 'City'
					,dbo.decryptBinaryData(csd.CUST_CNTCT_NME) AS 'ContactName'
					,dbo.decryptBinaryData(csd.CUST_EMAIL_ADR) AS 'ContactEmail'
					,dbo.decryptBinaryData(csd.CUST_CNTCT_PHN_NBR) AS 'ContactPhone'
					,CASE sde.SALE_TYPE
						WHEN 'L' THEN 'Lease' 
						WHEN 'P' THEN 'Purchase' 
						ELSE 'Unknown' 
					END AS 'SaleType'
					,CASE ISNULL(sde.RFP_CD, 0)  WHEN 1 THEN 'YES' ELSE 'NO' END AS	'IsRFP'
					,CASE ISNULL(sde.WRLNE_CUST_CD, 0)  WHEN 1 THEN 'YES' ELSE 'NO' END AS 'IsWireline'
					,CASE ISNULL(sde.MNGD_SRVC_CUST_CD, 0)  WHEN 1 THEN 'YES' ELSE 'NO' END AS 'IsManagedService'
					,sde.CMNT_TXT AS 'Comments'
					,sde.SDE_STUS_ID AS 'SDEStatusID'
					,ls.STUS_DES AS 'SDEStatusDesc'
					,sde.CREAT_BY_USER_ID AS 'RequestorID'
					,lu.FULL_NME AS 'RequestorName'
					,lu.EMAIL_ADR AS 'RequestorEmail'
					,lu.PHN_NBR AS 'RequestorPhone'
					,lu.CTY_NME + ' ' + lu.STT_CD AS 'RequestorLocation'
					,'' AS 'SDEURL'
					,'' AS 'DOCURL'
					,(SELECT @ASSIGNED_TEXT) AS 'AssignedText'
			FROM dbo.SDE_OPPTNTY sde WITH (NOLOCK)
			INNER JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=sde.SDE_OPPTNTY_ID AND csd.SCRD_OBJ_TYPE_ID=26
			JOIN dbo.LK_USER lu WITH (NOLOCK) ON sde.CREAT_BY_USER_ID = lu.USER_ID
			JOIN dbo.LK_STUS ls WITH (NOLOCK) ON sde.SDE_STUS_ID = ls.STUS_ID
			WHERE sde.SDE_OPPTNTY_ID = @SDE_OPPTNTY_ID
			FOR XML PATH(''), TYPE ) AS 'SDEInfo'
			,
			(SELECT sdt.SDE_PRDCT_TYPE_ID AS 'ProductTypeID'
				,sdt.SDE_PRDCT_TYPE_NME AS 'ProductTypeName'
				,sop.QTY AS 'ProductTypeQty'
			FROM dbo.SDE_OPPTNTY_PRDCT sop WITH (NOLOCK)
			JOIN dbo.LK_SDE_PRDCT_TYPE sdt WITH (NOLOCK) ON sop.SDE_PRDCT_TYPE_ID = sdt.SDE_PRDCT_TYPE_ID
			WHERE sop.SDE_OPPTNTY_ID = @SDE_OPPTNTY_ID AND sop.REC_STUS_ID = 1
			FOR XML PATH('SDEProductInfo'), TYPE )
			,
			(SELECT doc.SDE_OPPTNTY_DOC_ID AS 'DocID'
				,doc.FILE_NME AS 'DocName'
				,dbo.GetFormattedFileSize(doc.FILE_SIZE_QTY) AS 'DocSize'
			FROM dbo.SDE_OPPTNTY_DOC doc WITH (NOLOCK)
			WHERE doc.SDE_OPPTNTY_ID = @SDE_OPPTNTY_ID AND doc.REC_STUS_ID = 1
			FOR XML PATH('SDEDocInfo'), TYPE )
			
		FOR XML PATH(''), ROOT('SDEEmail') )
		
		INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
					(EMAIL_REQ_TYPE_ID
					,STUS_ID
					,CREAT_DT
					,EMAIL_LIST_TXT
					,EMAIL_CC_TXT
					,EMAIL_SUBJ_TXT
					,EMAIL_BODY_TXT)
			VALUES	(@EMAIL_REQ_TYPE_ID -- SDEOpportunityEmail
					,10 --(9-Onhold, 10-Pending, 11-Sent, 12-Error)
					,GETDATE()
					,@EMAIL_TO
					,@EMAIL_CC
					,@EMAIL_TITLE
					,@EMAIL_BODY)
					
	END TRY

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
END