USE [COWS]
GO
_CreateObject 'SP','dbo','getCPTDataByH1_v2'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Alan Carvajal
-- Create date: 03/25/2022
-- Description:	
--      v1: Get CPT Data by H1 from NRMBPM.
-- 	    v2: Optimize returned data set by removing null customers/H1 
--          rows and partitioning data set for unique CUST_NME <-> H1_ID 
-- =============================================
ALTER PROCEDURE [dbo].[getCPTDataByH1_v2]
	@H1	VARCHAR(9) = ''
AS
BEGIN

BEGIN TRY

DECLARE @SQL nVarchar(max)
	IF @H1 = ''
	BEGIN 
		SET @SQL = '
			SELECT
				* 
			FROM
				OPENQUERY (NRMBPM,		
							''
					SELECT
						t.DD_NBR, t.CUST_NME, t.H1_ID, t.CUST_ID, t.ACCT_ROLE_NME, t.CNTCT_NME, t.EMAIL_ADR, t.CC_EMAIL_ADR, t.DSGN_LINK_NME
					FROM
						(
						SELECT
							d.*,
							ROW_NUMBER() OVER (PARTITION BY d.CUST_NME, d.H1_ID ORDER BY d.H1_ID) AS p
						FROM
							BPMF_OWNER.V_BPMF_COWS_CPT d
						WHERE
							NULLIF(d.CUST_NME, '''') IS NOT NULL AND
							NULLIF(d.H1_ID, '''') IS NOT NULL
					     ) t
					WHERE
						p = 1
				'')
		';
	END
	ELSE 
	BEGIN 
		SET @SQL = 'SELECT *
					FROM OPENQUERY (NRMBPM,		
				''SELECT DISTINCT *  
					FROM BPMF_OWNER.V_BPMF_COWS_CPT WHERE H1_ID = ''''' + @H1 + ''''' '') '; 
	END
	EXEC sp_executesql @SQL


	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
END