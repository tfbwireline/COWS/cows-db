USE [COWS]
GO
_CreateObject 'SP','dbo','deletePartialMDSEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================    
-- Author:  sbg9814    
-- Create date: 07/14/2011    
-- Description: Deletes a given Tab from the MDS Event.    
-- =========================================================    
ALTER PROCEDURE [dbo].[deletePartialMDSEvent]    
 @EVENT_ID  Int    
 ,@TAB_SEQ_NBR Int  
 ,@UserID Int  
AS    
BEGIN    
SET NOCOUNT ON;    
DECLARE @FSA_MDS_EVENT_ID Int    
SET  @FSA_MDS_EVENT_ID = 0    
  
DECLARE @TAB_NME VARCHAR(200)  
SET @TAB_NME = ''  
    
Begin Try    
 SELECT  @FSA_MDS_EVENT_ID = ISNULL(FSA_MDS_EVENT_ID, 0), @TAB_NME =ISNULL(TAB_NME, '')  
  FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK)    
  WHERE EVENT_ID = @EVENT_ID    
  AND  TAB_SEQ_NBR = @TAB_SEQ_NBR    
      
 IF @FSA_MDS_EVENT_ID != 0    
  BEGIN    
   -----------------------------------------------------------------------------------------------------------------------------    
   -- Delete the data for a given TAB_SEQ_NBR.    
   -----------------------------------------------------------------------------------------------------------------------------    
   DELETE FROM dbo.MDS_EVENT_ACCS    WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
   DELETE FROM dbo.MDS_EVENT_CPE WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
   DELETE FROM dbo.MDS_EVENT_MNS WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
   DELETE FROM dbo.MDS_EVENT_ODIE_DEV_NME   WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
   DELETE FROM dbo.MDS_EVENT_SPAE_TRPT    WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
   DELETE FROM dbo.MDS_EVENT_SPRINT_LINK_ATM_TRPT   WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
   DELETE FROM dbo.MDS_EVENT_VLAN   WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
   DELETE FROM dbo.MDS_EVENT_VRTL_CNCTN   WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
   DELETE FROM dbo.MDS_EVENT_WIRED_TRPT    WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
   DELETE FROM dbo.MDS_EVENT_WRLS_TRPT    WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID   
   DELETE FROM dbo.MDS_EVENT_SRVC WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID   
   DELETE FROM dbo.EVENT_HIST     WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID 
   DELETE FROM dbo.MDS_EVENT_DSL_TRPT     WITH (ROWLOCK) WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
       
   UPDATE  dbo.ODIE_RSPN_INFO WITH (ROWLOCK)    
    SET  FSA_MDS_EVENT_ID = NULL    
    WHERE FSA_MDS_EVENT_ID = @FSA_MDS_EVENT_ID    
        
   UPDATE  dbo.ODIE_REQ WITH (ROWLOCK)    
    SET  TAB_SEQ_NBR  = NULL    
    WHERE MDS_EVENT_ID = @EVENT_ID    
    AND  TAB_SEQ_NBR  = @TAB_SEQ_NBR    
   
   DELETE FROM dbo.FSA_MDS_EVENT_ORDR WITH (ROWLOCK) WHERE EVENT_ID = @EVENT_ID AND  TAB_SEQ_NBR = @TAB_SEQ_NBR    
   DELETE FROM dbo.FSA_MDS_EVENT_NEW  WITH (ROWLOCK) WHERE EVENT_ID = @EVENT_ID AND  TAB_SEQ_NBR = @TAB_SEQ_NBR     
       
   -----------------------------------------------------------------------------------------------------------------------------    
   -- For existing TABs, decrement the TAB_SEQ_NBR where applicable.    
   -----------------------------------------------------------------------------------------------------------------------------      
   UPDATE  dbo.ODIE_REQ WITH (ROWLOCK)    
    SET  TAB_SEQ_NBR  = TAB_SEQ_NBR - 1    
    WHERE MDS_EVENT_ID = @EVENT_ID    
    AND  TAB_SEQ_NBR  > @TAB_SEQ_NBR    
       
   UPDATE  dbo.FSA_MDS_EVENT_ORDR WITH (ROWLOCK)    
    SET  TAB_SEQ_NBR = TAB_SEQ_NBR - 1    
    WHERE EVENT_ID = @EVENT_ID    
    AND  TAB_SEQ_NBR > @TAB_SEQ_NBR    
        
   UPDATE  dbo.FSA_MDS_EVENT_NEW WITH (ROWLOCK)    
    SET  TAB_SEQ_NBR = TAB_SEQ_NBR - 1    
    WHERE EVENT_ID = @EVENT_ID    
    AND  TAB_SEQ_NBR > @TAB_SEQ_NBR         
      
   INSERT INTO MDS_EVENT_FORM_CHNG (MDS_EVENT_ID, CHNG_TYPE_NME, CREAT_DT, CREAT_BY_USER_ID)       
 VALUES (@EVENT_ID, 'Deleted Tab ' + @TAB_NME, GETDATE(), @UserID)  
  
  END    
End Try    
    
Begin Catch    
 EXEC [dbo].[insertErrorInfo]    
End Catch     
END 
GO