USE [COWS]
GO
_CreateObject 'SP','dbo','insertFSAIPAddress'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/11/2011
-- Description:	Inserts the FSA record into the FSA_ORDR table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertFSAIPAddress]
	@ORDR_ID			Int
	,@IP_ADR_TYPE_CD	Varchar(4)
	,@IP_ADR				Varchar(50)
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	--------------------------------------------------------------------
	--	Insert a record into the FSA_ORDR Table.				
	--------------------------------------------------------------------
	INSERT INTO dbo.FSA_IP_ADR	WITH (ROWLOCK)
					(ORDR_ID
					,IP_ADR_TYPE_CD
					,IP_ADR				
					)
		VALUES		(@ORDR_ID
					,@IP_ADR_TYPE_CD
					,@IP_ADR	
					)		
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
