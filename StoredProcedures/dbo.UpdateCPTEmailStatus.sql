USE [COWS]
GO
_CreateObject 'SP','dbo','UpdateCPTEmailStatus'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author: <Sarah Sandoval>
-- Create date: <09/16/2015>
-- Description:	<Update CPT Email Status>
-- =============================================
ALTER PROCEDURE dbo.UpdateCPTEmailStatus
	@EMAIL_REQ_ID INT,
	@CPT_ID Int,
	@Status  Int
AS
BEGIN
Begin Try

IF @CPT_ID > 0
	BEGIN
		UPDATE		dbo.EMAIL_REQ	WITH (ROWLOCK)
			SET		STUS_ID	=	@Status,
					SENT_DT =   CASE @Status WHEN 11 THEN GETDATE() ELSE NULL END
			WHERE	CPT_ID	=	@CPT_ID
			  AND   EMAIL_REQ_ID = @EMAIL_REQ_ID
	END
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO
