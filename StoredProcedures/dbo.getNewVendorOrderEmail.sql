USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getNewVendorOrderEmail]    Script Date: 08/29/2011 13:46:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 12th July 2011
-- Description:	Creates Vendor Order Email
-- ================================================================

ALTER PROCEDURE [dbo].[getNewVendorOrderEmail]
	@VendorOrderID INT, 
	@EmailTypeID TINYINT,
	@EmailSubject VARCHAR(500),
	@EmailBody VARCHAR(MAX),
	@UserID INT
AS
BEGIN

BEGIN TRY

DECLARE @UserEmailID VARCHAR(200)
DECLARE @EmailID INT
DECLARE @ToEmailAddress VARCHAR(200) 

SET @EmailID = 0

-- Get User email
SELECT
	@UserEmailID = EMAIL_ADR	
FROM dbo.LK_USER WITH (NOLOCK)
WHERE [USER_ID] = @UserID

IF(@EmailTypeID = 4)
BEGIN
	SELECT
		@ToEmailAddress = CAC.CFG_KEY_VALU_TXT
	FROM dbo.LK_COWS_APP_CFG CAC WITH (NOLOCK)
	WHERE CAC.CFG_KEY_NME = 'Vendor Confirmation Email'
END
ELSE IF(@EmailTypeID = 5)
BEGIN
	SELECT
		@ToEmailAddress = CAC.CFG_KEY_VALU_TXT
	FROM dbo.LK_COWS_APP_CFG CAC WITH (NOLOCK)
	WHERE CAC.CFG_KEY_NME = 'Vendor Disc Confirmation Email'
END
ELSE IF(@EmailTypeID = 6)
BEGIN
	SELECT
		@ToEmailAddress = CAC.CFG_KEY_VALU_TXT
	FROM dbo.LK_COWS_APP_CFG CAC WITH (NOLOCK)
	WHERE CAC.CFG_KEY_NME = 'ASR Email'
END

-- Create new Vendor email from Vendor Order
INSERT INTO [dbo].[VNDR_ORDR_EMAIL]
           ([VNDR_ORDR_ID]
           ,[VER_NBR]
		   ,[VNDR_EMAIL_TYPE_ID]
           ,[FROM_EMAIL_ADR]
           ,[TO_EMAIL_ADR]
           ,[CC_EMAIL_ADR]
		   ,[EMAIL_SUBJ_TXT]
		   ,[EMAIL_BODY]
           ,[EMAIL_STUS_ID]
           ,[CREAT_DT]
           ,[CREAT_BY_USER_ID]
           ,[REC_STUS_ID]) 
SELECT @VendorOrderID
      ,ISNULL((SELECT MAX(VOE.VER_NBR) FROM dbo.VNDR_ORDR_EMAIL VOE WITH (NOLOCK) WHERE VOE.VNDR_ORDR_ID = @VendorOrderID),0) + 1
	  ,@EmailTypeID
      ,@UserEmailID
      ,ISNULL(@ToEmailAddress, VF.VNDR_EMAIL_LIST_TXT) 
      ,@UserEmailID
	  ,@EmailSubject
	  ,@EmailBody
      ,13
      ,GETDATE()
      ,@UserID
      ,1
FROM dbo.VNDR_FOLDR VF WITH (NOLOCK)
INNER JOIN dbo.VNDR_ORDR VO WITH (NOLOCK) ON VF.VNDR_FOLDR_ID = VO.VNDR_FOLDR_ID  
WHERE VO.VNDR_ORDR_ID = @VendorOrderID
SET @EmailID = SCOPE_IDENTITY()

SELECT @EmailID

END TRY
BEGIN Catch
       exec  [dbo].[insertErrorInfo]
END Catch

END
