﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getAllVendorOrderFiles'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 16th October 2011
-- Description:	Returns all files belonging to a Vendor Order
-- ================================================================

ALTER PROCEDURE [dbo].[getAllVendorOrderFiles]
	@VendorOrderID INT 	
AS
BEGIN

BEGIN TRY

	SELECT
		'H5 FOLDER' FileHeader,		
		H5_FOLDR_ID FileHeaderID,
		'A' SortField
	FROM dbo.ORDR	WITH (NOLOCK)
	WHERE ORDR_ID = @VendorOrderID
	UNION
	SELECT
		FILE_NME FileHeader,
		VNDR_ORDR_FORM_ID FileHeaderID,
		'B' SortField
	FROM dbo.VNDR_ORDR_FORM	WITH (NOLOCK)
	WHERE VNDR_ORDR_ID = @VendorOrderID
	AND	TMPLT_CD = 1
	ORDER BY SortField
		
	SELECT
		'H5' FileType,
		hd.H5_DOC_ID FileID,
		hd.FILE_NME [FileName],
		hd.H5_FOLDR_ID FileHeaderID
	FROM dbo.ORDR od	WITH (NOLOCK)
	INNER JOIN dbo.H5_FOLDR hf	WITH (NOLOCK) ON od.H5_FOLDR_ID = hf.H5_FOLDR_ID
	INNER JOIN dbo.H5_DOC hd	WITH (NOLOCK) ON hf.H5_FOLDR_ID = hd.H5_FOLDR_ID
	WHERE od.ORDR_ID = @VendorOrderID
	UNION
	SELECT
		'Form' FileType,
		VNDR_ORDR_FORM_ID FileID,
		FILE_NME [FileName],
		TMPLT_ID FileHeaderID
	FROM dbo.VNDR_ORDR_FORM	WITH (NOLOCK)
	WHERE VNDR_ORDR_ID = @VendorOrderID
	AND TMPLT_CD = 0
END TRY
BEGIN Catch
       exec  [dbo].[insertErrorInfo]
END Catch

END

