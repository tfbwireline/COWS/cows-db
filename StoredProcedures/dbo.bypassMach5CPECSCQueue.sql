USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[bypassMach5CPECSCQueue]    Script Date: 12/03/2019 10:36:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <02/03/2016>
-- Description:	<This SP will bypass CSC workgroup 
--	for Manufacturer != 'CSCO'>
-- =============================================
ALTER PROCEDURE [dbo].[bypassMach5CPECSCQueue]
@OrderID	Int,
@TaskID		SmallInt,
@TaskStatus	TinyInt,
@Comments	Varchar(1000) = NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @isBypass BIT
	IF EXISTS
		(Select 'X' 
			From dbo.ORDR 
			Where ordr_cat_id = 6
				AND ORDR_ID = @OrderID
		)
		BEGIN
			IF EXISTS
				(
					Select 'X'
					From dbo.FSA_ORDR_CPE_LINE_ITEM  WITH (NOLOCK)
					Where ORDR_ID = @OrderID	
						AND MFR_NME = 'CSCO'
						AND CMPNT_FMLY IN ('Router mnt','Switch mnt','Ip mnt')
						
				)
				BEGIN
					IF EXISTS (Select * From dbo.FSA_ORDR WITH (NOLOCK)
								Where ORDR_ID = @OrderID AND ISNULL(CPE_REC_ONLY_CD,'N') = 'Y')
						BEGIN
							SET @isBypass = 1
							EXEC dbo.CompleteActiveTask @OrderID,@TaskID,2,'Completing CSC Queue since Records-Only order.',1
						END	
					ELSE		
						SET @isBypass = 0
				END
			ELSE
				BEGIN
					SET @isBypass = 1
				END
		
			IF @isBypass = 1
				BEGIN
					EXEC dbo.CompleteActiveTask @OrderID,@TaskID,2,'Bypassing CSC Queue since manufacturer is either not CISCO or is CISCO but doesnot belong to component family(Router MNT,Switch MNT or IP Mnt).',1
				END
			declare @order_nbr Varchar(20) 
			Select @order_nbr = FTN from dbo.FSA_ORDR where ORDR_ID = @OrderID
			declare @cpe_mngd_tbl table (cpe_device_id varchar(20), cpe_mngd_flg_cd varchar(1), nme varchar(500) )
			declare @sql nVarchar(max) = ''
			set @sql = 'SELECT cpe_device_id,cpe_mngd_flg_cd, NME
							FROM OpenQuery(M5, ''Select oc.cpe_device_id, oc.cpe_mngd_flg_cd, oc.NME 
									From	Mach5.V_V5U_ORDR o 
									Inner Join	Mach5.V_V5U_ORDR_CMPNT oc ON o.ORDR_ID = oc.ORDR_ID
									Where o.ORDER_NBR =''''' + @order_nbr + ''''''')'
			insert into @cpe_mngd_tbl
			exec sp_executeSQL @sql

			IF exists
				(
					select 'x' 
						from @cpe_mngd_tbl
						where cpe_mngd_flg_cd = 'y'
				) 
				AND	 not exists
				( select 'x' 
						from @cpe_mngd_tbl
						where  nme = 'MNS Support Solution - Monitor & Notify'
				)

				begin
					Print 'Do Nothing'
				end
			else
				begin
					Exec dbo.CompleteActiveTask @orderID,@TaskID, 2, 'Bypass CSC workgroup since managed flag is set to false or Monitor and Notify ordered.'
				end	
		END
	
	
    
	
END
