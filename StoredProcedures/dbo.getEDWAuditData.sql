USE [COWS]
GO

_CreateObject 'SP'
	,'dbo'
	,'getEDWAuditData'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  jrg7298            
-- Create date: 6/1/2020
-- Description: Get EDW Audit Data
-- =============================================            
ALTER PROCEDURE [dbo].[getEDWAuditData]
AS
BEGIN
	BEGIN TRY
		
		SELECT DISTINCT [USER_ADID] + '|' + CASE WHEN (charindex('[',dspl_nme)<=2) THEN REPLACE(ISNULL(dspl_nme,''),', ',',') ELSE REPLACE(LTRIM(RTRIM(REPLACE(substring(dspl_nme,1,(charindex('[',dspl_nme)-1)),', ',','))),' ',',') END  + '|' + lr.ROLE_NME  + '|' + 'qda'  + '|' + 'application'  + '|' +	isnull(format(lu.LST_LOGGDIN_DT,'yyyyMMddHHmmss'),'')  + '|' + format(getdate(),'yyyyMMddHHmmss')  + '|' + (case when lu.REC_STUS_ID=1 then 'active' else 'inactive' end) as 'EDWRecord'
		FROM dbo.LK_USER lu WITH (NOLOCK) INNER JOIN
		dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON lu.[USER_ID]=ugr.[USER_ID] AND ugr.REC_STUS_ID=1 INNER JOIN
		dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_ID=ugr.ROLE_ID
		WHERE lu.USER_ADID NOT IN ('System', '$Premise', 'NTEPool', 'nte_pool', '$b5psa','INT-OPS1','INT-OPS2','INT-OPS3') AND lu.DSPL_NME IS NOT NULL
		UNION
		SELECT DISTINCT [USER_ADID] + '|' + CASE WHEN (charindex('[',dspl_nme)<=2) THEN REPLACE(ISNULL(dspl_nme,''),', ',',') ELSE REPLACE(LTRIM(RTRIM(REPLACE(substring(dspl_nme,1,(charindex('[',dspl_nme)-1)),', ',','))),' ',',') END  + '|' + lup.USR_PRF_NME  + '|' + 'qda'  + '|' + 'application'  + '|' +	isnull(format(lu.LST_LOGGDIN_DT,'yyyyMMddHHmmss'),'')  + '|' + format(getdate(),'yyyyMMddHHmmss')  + '|' + (case when lu.REC_STUS_ID=1 then 'active' else 'inactive' end) as 'EDWRecord'
		FROM dbo.LK_USER lu WITH (NOLOCK) INNER JOIN
		dbo.MAP_USR_PRF mup WITH (NOLOCK) ON lu.[USER_ID]=mup.[USER_ID] AND mup.REC_STUS_ID=1 INNER JOIN
		dbo.LK_USR_PRF lup WITH (NOLOCK) ON lup.USR_PRF_ID=mup.USR_PRF_ID
		WHERE lu.USER_ADID NOT IN ('System', '$Premise', 'NTEPool', 'nte_pool', '$b5psa','INT-OPS1','INT-OPS2','INT-OPS3') AND lu.DSPL_NME IS NOT NULL and (charindex('[',dspl_nme)<=2)
		
	END TRY

	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END