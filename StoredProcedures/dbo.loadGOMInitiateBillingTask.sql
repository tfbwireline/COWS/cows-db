USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[loadGOMInitiateBillingTask]    Script Date: 07/05/2019 2:31:58 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <06/15/2012>
-- Description:	<Load GOM Initiate Billing Task and assign it to GOM user if needed>
-- =============================================
ALTER PROCEDURE  [dbo].[loadGOMInitiateBillingTask]
	@ORDR_ID INT	
AS
BEGIN
	BEGIN TRY
		IF NOT EXISTS
			(
				SELECT 'X'
					FROM	dbo.ACT_TASK WITH (NOLOCK)
					WHERE	ORDR_ID =	@ORDR_ID
						AND	TASK_ID	=	110
			)
			BEGIN
				INSERT INTO dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT)
				VALUES (@ORDR_ID,110,0,0,GETDATE())
			END
		ELSE
			BEGIN
				UPDATE	dbo.ACT_TASK WITH (ROWLOCK)
					SET		STUS_ID = 0, MODFD_DT = GETDATE()
					WHERE	ORDR_ID =	@ORDR_ID
						AND	TASK_ID	=	110
			END
		

		IF EXISTS  -- Added dlp0278 7/5/2019 2:44 p.m.  Taskrule was not being called thru completeActiveTask process.
			(
				SELECT 'X'
					FROM	dbo.ACT_TASK WITH (NOLOCK)
					WHERE	ORDR_ID =	@ORDR_ID
						AND	TASK_ID	=	110 AND STUS_ID = 0
			)
			BEGIN
				EXEC dbo.BillOnlyTaskRule @ORDR_ID, 110, 0, ''  
			END
				

		--IF NOT EXISTS           removed dlp0278 7/5/2019 2:44 p.m. assignments handled in above SP dbo.BillOnlyTaskRule
		--	(
		--		SELECT 'X'
		--			FROM	dbo.USER_WFM_ASMT WITH (NOLOCK)
		--			WHERE	ORDR_ID	=	@ORDR_ID
		--				AND GRP_ID	=	1
		--	)
		--	BEGIN
		--		EXEC [dbo].[WfmGOMTaskRule] @ORDR_ID,0,0,''
		--	END	
			
	END TRY
	
	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]  
	END CATCH
END
