USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getODIECpeReus]    Script Date: 07/22/2021 10:11:11 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	-- =============================================
	-- Author:		David Phillips
	-- Create date: 12/29/2020
	-- Description:	Retrieves ODIE message data for CPE Reuse Access NUA change
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getODIECpeReus] 
		@REQ_ID int  

	AS
	BEGIN


		BEGIN TRY
	
			SELECT TOP 1			
					ISNULL(M5_CHNG_TXN_ID,'')		AS OrderID,
					GETDATE()						AS StatusDate,
					CPE_DEV_ID						AS DeviceID,
					OLD_NUA							AS OldNUA,
					CUR_NUA							AS CurrentNUA                      
					
			FROM dbo.CPE_NUA_CHNG WITH (NOLOCK)
			WHERE CPE_DEV_ID = (SELECT [DEV_FLTR]FROM [dbo].[ODIE_REQ] WITH (NOLOCK) 
										WHERE [REQ_ID] = @REQ_ID)
			ORDER BY CREAT_DT DESC
		
			RETURN
			
		END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH

	END
	




