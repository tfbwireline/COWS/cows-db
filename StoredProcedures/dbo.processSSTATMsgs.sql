USE [COWS]
GO
_CreateObject 'SP','dbo','processSSTATMsgs'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 10/05/2015
-- Description:	Processes an SSTAT Response from the SSTAT_RSPN table.
-- =========================================================
ALTER PROCEDURE [dbo].[processSSTATMsgs]
	
AS

BEGIN
SET NOCOUNT ON;

BEGIN TRY
	DECLARE @ORDR_ID int
	DECLARE @SSTAT_RSPN_ID INT
	DECLARE @ORDER_ACTION VARCHAR(50)
	DECLARE @NOTE VARCHAR(500)
	DECLARE @COMPLETION_DATE DATETIME
	DECLARE @DEVICE_ID VARCHAR(25)

	DECLARE @ORDR_IDS table (ORDR_ID INT, SSTAT_RSPN_ID INT, ORDER_ACTION VARCHAR(50),
								NOTE VARCHAR(500), COMPLETION_DATE DATETIME, DEVICE_ID VARCHAR(25))

	INSERT INTO @ORDR_IDS (ORDR_ID, SSTAT_RSPN_ID, ORDER_ACTION, NOTE, COMPLETION_DATE,DEVICE_ID) 
		 (SELECT DISTINCT fo.ORDR_ID, sst.SSTAT_RSPN_ID, sst.ORDER_ACTION, sst.NOTE, sst.COMPLETION_DATE, sst.DEVICE_ID
		  FROM  dbo.SSTAT_RSPN sst WITH (NOLOCK)
			INNER JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN=sst.ORDR_ID AND sst.ACT_CD = 0
			INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM itm WITH (NOLOCK)  ON sst.DEVICE_ID = itm.DEVICE_ID AND fo.ORDR_ID=itm.ORDR_ID AND itm.ITM_STUS = 401
			INNER JOIN dbo.ACT_TASK at WITH (NOLOCK) ON at.ORDR_ID=fo.ORDR_ID AND at.TASK_ID = 1000 and at.STUS_ID = 0) 
	
	WHILE EXISTS (SELECT * FROM @ORDR_IDS)
		BEGIN
		
			 SELECT TOP 1 @ORDR_ID = ORDR_ID,
								   @SSTAT_RSPN_ID = SSTAT_RSPN_ID,
								   @ORDER_ACTION = ORDER_ACTION,
								   @NOTE = NOTE,
								   @COMPLETION_DATE = COMPLETION_DATE,
								   @DEVICE_ID = DEVICE_ID
								FROM @ORDR_IDS 
								 
								
			IF @ORDER_ACTION = 'Complete'
				BEGIN 				
					  Set @NOTE = 'SSTAT Complete Note: ' + @NOTE			
		
					  --EXEC dbo.CompleteActiveTask @ORDR_ID,1000,2,@NOTE,1,6
							
					    EXEC dbo.insertOrdrCmplAll_V5U @ORDR_ID,@DEVICE_ID,@NOTE
					  
					 -- EXEC dbo.insertODIEReq_V5U @ORDR_ID,6
				END
			ELSE
				IF @ORDER_ACTION = 'RTSHold'
				  BEGIN
					   
						SET @NOTE = 'SSTAT RTS Note: ' + @NOTE
							
						--EXEC dbo.CompleteActiveTask @ORDR_ID,1000,1,@NOTE,1,6
					    UPDATE dbo.ACT_TASK WITH (ROWLOCK)
							SET STUS_ID = 1
							WHERE ORDR_ID = @ORDR_ID AND TASK_ID = 1000
						
						EXEC dbo.insertOrderNotes @ORDR_ID,26,@NOTE,1
						
						IF NOT EXISTS (SELECT * from dbo.ACT_TASK WITH (NOLOCK) where ORDR_ID = @ORDR_ID AND TASK_ID = 603)
							BEGIN
								INSERT INTO dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT) 
									VALUES (@ORDR_ID,603,0,600,GETDATE())
							END
						
						INSERT INTO [dbo].[M5_ORDR_MSG]
						([ORDR_ID],[M5_MSG_ID],[NTE],[MSG], [DEVICE],[CMPNT_ID],[STUS_ID],[CREAT_DT],[SENT_DT])     
						VALUES (@ORDR_ID,4,@NOTE,null,@DEVICE_ID,null,10,GETDATE(),NULL)
				  END
			
			UPDATE dbo.SSTAT_RSPN WITH (ROWLOCK)
			SET ACT_CD = 1
			WHERE SSTAT_RSPN_ID =   @SSTAT_RSPN_ID  
					
			DELETE FROM @ORDR_IDS WHERE  ORDR_ID = @ORDR_ID
				
		END


END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH
END