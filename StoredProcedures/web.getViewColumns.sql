USE [COWS]
GO
_CreateObject 'SP','web','getViewColumns'
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--================================================================================================  
-- Author:  csb9923  
-- Create date: 4/11/2011  
-- Description: Get the list of columns for a view
-- sbg9814	09/09/2011	Changed this to dynamically extract the Views for Orders.
--================================================================================================  
ALTER PROCEDURE [web].[getViewColumns]   
	@SITE_CNTNT_ID	Int
	,@GRP_ID		Int	=	0
AS
BEGIN  
SET NOCOUNT ON;  

Begin Try
	IF	@SITE_CNTNT_ID	=	0	AND	@GRP_ID	!=	0
		BEGIN
			SELECT		@SITE_CNTNT_ID	=	SITE_CNTNT_ID
				FROM	dbo.LK_SITE_CNTNT	WITH (NOLOCK)
				WHERE	GRP_ID			=	@GRP_ID
	
			SELECT DISTINCT	lc.DSPL_COL_ID
							,lc.DSPL_COL_NME
							,lc.SRC_TBL_COL_NME
							,CAST(1 AS tinyint)	AS	DSPL_POS_FROM_LEFT_NBR
							,@SITE_CNTNT_ID	AS	SITE_CNTNT_ID 
				FROM		dbo.DSPL_VW_COL		vc	WITH (NOLOCK) 
				INNER JOIN	dbo.LK_DSPL_COL		lc	WITH (NOLOCK)	ON	vc.DSPL_COL_ID	=	lc.DSPL_COL_ID
				WHERE		vc.DSPL_VW_ID	BETWEEN	201	AND	214
		END
	ELSE
		BEGIN
			DECLARE 	@ViewName VARCHAR(20)
			DECLARE @ViewID INT

			SELECT 	 @ViewName = CASE SITE_CNTNT_NME
								  WHEN 'Access Delivery' THEN 'ADAllPossible'	 
								  WHEN 'MDS' THEN 'MDS All Fields'
								  WHEN 'MPLS' THEN 'MPLSAllPossible'
								  WHEN 'NGVN' THEN 'NGVNAllPossible'
								  WHEN 'SprintLink' THEN 'SLAllPossible'
								  WHEN 'Fedline' THEN 'FLAllPossible'
								  WHEN 'SIPT' THEN 'SIPTAllPossible'
								  WHEN 'MVS'  THEN 'MVSAllPossible'
								  END
								  FROM dbo.LK_SITE_CNTNT WITH (NOLOCK) WHERE SITE_CNTNT_ID=@SITE_CNTNT_ID
			
			SELECT @ViewID=DSPL_VW_ID 
			FROM dbo.DSPL_VW WITH (NOLOCK) WHERE DSPL_VW_NME=@ViewName
			
			IF	@ViewID	!=	40
				BEGIN
					SELECT DISTINCT	lc.DSPL_COL_ID
									,lc.DSPL_COL_NME
									,lc.SRC_TBL_COL_NME
									,vc.DSPL_POS_FROM_LEFT_NBR 
						FROM		dbo.DSPL_VW_COL	vc	WITH (NOLOCK) 
						INNER JOIN	dbo.LK_DSPL_COL lc	WITH (NOLOCK)	ON	vc.DSPL_COL_ID	=	lc.DSPL_COL_ID
						WHERE		vc.DSPL_VW_ID	=	@ViewID	
				END
			ELSE
				BEGIN
					SELECT			lc.DSPL_COL_ID
									,lc.DSPL_COL_NME
									,lc.SRC_TBL_COL_NME
									,vc.DSPL_POS_FROM_LEFT_NBR 
						FROM		dbo.DSPL_VW_COL	vc	WITH (NOLOCK) 
						INNER JOIN	dbo.LK_DSPL_COL lc	WITH (NOLOCK)	ON	vc.DSPL_COL_ID	=	lc.DSPL_COL_ID
						WHERE		vc.DSPL_VW_ID	=	@ViewID	
						ORDER BY	vc.DSPL_POS_FROM_LEFT_NBR
				END
		END
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]  
End Catch
END
