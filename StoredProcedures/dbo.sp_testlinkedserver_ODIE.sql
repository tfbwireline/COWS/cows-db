USE [COWS]
GO

/****** Object:  StoredProcedure [dbo].[sp_testlinkedserver_NRM]    Script Date: 02/19/2019 22:45:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_testlinkedserver_ODIE]
AS
 BEGIN TRY
 EXEC sp_testlinkedserver  @server=N'D1ODIE1'
 SELECT 'Active'
 END TRY
 
 BEGIN CATCH
 SELECT error_message()
 END CATCH
GO