USE [COWS]
GO
_CreateObject 'SP','dbo','completeDISCGOMTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==================================================================================
-- Author:		Naidu Vattigunta
-- Create date: 09/15/2011
-- Description:	GOM COMPLETE FOR Disconnect Orders 
-- ==================================================================================
ALTER PROCEDURE [dbo].[completeDISCGOMTask] 
	@OrderID	Int,
	@TaskID		SmallInt,	--=	102,
	@TaskStatus	TinyInt,		--=	2,
	@Comments	Varchar(1000) = NULL
 
AS
BEGIN
SET NOCOUNT ON;
Begin Try

DECLARE @UserID  Int
DECLARE @CNTRCId Varchar(50)
 
SET @TaskStatus = 2
SET @UserID = 1
SET @TaskID = 102
SET @Comments = 'Order Systematically moved out of GOM' 
SET @CNTRCId = ' '

	IF EXISTS (	SELECT 'X'
						FROM  FSA_ORDR_CPE_LINE_ITEM  fsa WITH (NOLOCK)
						INNER JOIN ACT_TASK at WITH (NOLOCK) ON fsa.ORDR_ID = at.ORDR_ID
						INNER JOIN FSA_ORDR fo WITH (NOLOCK) ON fsa.ORDR_ID = fo.ORDR_ID
					    WHERE fsa.ORDR_ID = @OrderID
					    AND at.TASK_ID = @TaskID
					    AND fo.ORDR_TYPE_CD = 'DC'
					    AND fsa.CNTRC_TYPE_ID <> 'Rental')
		 			BEGIN
				EXEC	[dbo].[CompleteActiveTask] @OrderID,@TaskID,@TaskStatus,@Comments,@UserID
			END 
			
 
End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END