USE [COWS]
GO
_CreateObject 'SP','dbo','GetCANDDecryptValues'
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetCANDDecryptValues]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[GetCANDDecryptValues]
GO
