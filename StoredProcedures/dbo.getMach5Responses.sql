USE [COWS]
GO
_CreateObject 'SP','dbo','getMach5Responses'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		DLP0278
-- Create date: 3/27/2015
-- Description:	Get the details for all Mach5 orders where we need to send responses!
-- kh946640 07/23/18 Added CSG_LVL_ID column.
-- =========================================================
ALTER PROCEDURE [dbo].[getMach5Responses]
AS
BEGIN
SET NOCOUNT ON;
Begin Try
		
	SELECT	distinct	 m5.M5_TRAN_ID
					,m5.ORDR_ID AS ORDR_ID		
					,fo.FTN		
					,m5.M5_MSG_ID
					,m5.NTE
					,m5.MSG
					,m5.CREAT_DT
					,m5.DEVICE
					,m5.CMPNT_ID
					,m5.STUS_ID
					,m5.PRCH_ORDR_NBR
					,foc.CUST_ID
					,[dbo].[getMaxVndrCktID](m5.ORDR_ID) as VNDR_CKT_ID
					,li.SUPPLIER
					,fo.ORDR_SUB_TYPE_CD
					,ord.CSG_LVL_ID
		FROM		dbo.M5_ORDR_MSG	m5	WITH (NOLOCK)
		INNER JOIN	dbo.FSA_ORDR fo	WITH (NOLOCK)	
				ON	m5.ORDR_ID	=	fo.ORDR_ID
	    INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = m5.ORDR_ID
		INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK)
				ON foc.ORDR_ID = m5.ORDR_ID
					AND foc.CIS_LVL_TYPE = 'H6'
		LEFT OUTER JOIN  dbo.FSA_ORDR_CPE_LINE_ITEM li WITH (NOLOCK)
				ON li.PRCH_ORDR_NBR = m5.PRCH_ORDR_NBR
		WHERE		m5.STUS_ID		=	10
		UNION
		SELECT	distinct	 m5.M5_EVENT_MSG_ID AS M5_TRAN_ID
					,m5.M5_ORDR_ID AS ORDR_ID		
					,m5.M5_ORDR_NBR AS FTN		
					,m5.M5_MSG_ID
					,'Event ID : ' + CONVERT(VARCHAR,m5.EVENT_ID)  AS NTE
					,'' AS MSG
					,m5.CREAT_DT
					,m5.DEV_ID AS DEVICE
					,m5.CMPNT_ID
					,m5.STUS_ID
					,null AS PRCH_ORDR_NBR
					,null AS CUST_ID
					,null AS VNDR_CKT_ID
					,null AS SUPPLIER
					,null AS ORDR_SUB_TYPE_CD
					,eve.CSG_LVL_ID		
		FROM		dbo.M5_EVENT_MSG	m5	WITH (NOLOCK)
		LEFT JOIN   dbo.EVENT eve WITH (NOLOCK) ON eve.EVENT_ID = m5.EVENT_ID
		WHERE		m5.STUS_ID		=	10



End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END