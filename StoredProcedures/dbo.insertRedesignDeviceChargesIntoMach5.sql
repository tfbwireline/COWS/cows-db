USE COWS
GO
_CreateObject 'SP','dbo','insertRedesignDeviceChargesIntoMach5'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================================================
-- Author:		<Ramesh Ragi>
-- Create date: <10/31/2016>
-- Description:	<Get Device information whose charges need to be calculated and send to Mach5>
-- Updated by:	<Sarah Sandoval>
-- Update date:	<0929/2021>
-- Description:	<Update Voice billing computation as per John Hamilton's issue - IM6281259>
--				Voice redesign will only send one billing computation to M5 on Redesign level
--					upon completion of 1st event association.
-- =========================================================================================================
ALTER PROCEDURE [dbo].[insertRedesignDeviceChargesIntoMach5] 
@REDSGN_DEV_IDS Varchar(max),
@EventID INT = -1
AS
BEGIN
	--Declare	@RedesignDeviceIDs Varchar(Max) = '5511,5512'--'5499,5500'
	BEGIN TRY
		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
		
		SET NOCOUNT ON;
		IF @REDSGN_DEV_IDS != ''
			BEGIN
				DECLARE @RedesignDeviceIDsTable Table (RedesignDevID Int, RedesignID Int,Total_Amt decimal(10,2),Flag Bit)
				INSERT INTO @RedesignDeviceIDsTable (RedesignDevID,RedesignID,Total_Amt,Flag)
					SELECT DISTINCT x.IntegerID,rdi.REDSGN_ID,0,0 
						FROM web.ParseCommaSeparatedIntegers(@REDSGN_DEV_IDS,',') x
						INNER JOIN dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK) ON x.IntegerID = rdi.REDSGN_DEV_ID
						INNER JOIN dbo.REDSGN rd WITH (NOLOCK) ON rd.REDSGN_ID=rdi.REDSGN_ID
						LEFT JOIN dbo.REDSGN_H1_MRC_VALUES rhm WITH (NOLOCK) ON rhm.H1_ID=rd.H1_CD AND rhm.REC_STUS_ID=1
						WHERE NOT EXISTS (SELECT 'X'
										  FROM dbo.M5_REDSGN_MSG mrm WITH (NOLOCK) INNER JOIN
										  dbo.REDSGN_DEVICES_INFO rdim WITH (NOLOCK) ON rdim.REDSGN_DEV_ID = mrm.REDSGN_DEV_ID INNER JOIN
										  dbo.REDSGN_DEVICES_INFO rdi2 WITH (NOLOCK) ON rdi2.REDSGN_ID = rdim.REDSGN_ID INNER JOIN
										  dbo.REDSGN r WITH (NOLOCK) ON r.REDSGN_ID = rdi2.REDSGN_ID
										  WHERE r.BILL_OVRRDN_CD = 1
										    AND rdi2.REDSGN_DEV_ID = rdi.REDSGN_DEV_ID)
						AND NOT EXISTS (SELECT 'X' FROM dbo.M5_REDSGN_MSG mrm WITH (NOLOCK) WHERE mrm.REDSGN_DEV_ID = rdi.REDSGN_DEV_ID)
						AND rhm.H1_ID IS NULL
						
					SELECT *,0,0,0 
						FROM web.ParseCommaSeparatedIntegers(@REDSGN_DEV_IDS,',')
						
				--Select * From @RedesignDeviceIDsTable
				DECLARE @NTE_CHARGE_RATE decimal(10,2)
						,@NE_VOICE_CHARGE_RATE decimal(10,2)
						,@NE_VOICE_OT_CHARGE_RATE decimal(10,2)
						,@SPS_VOICE_CHARGE_RATE decimal(10,2)
						,@SE_VOICE_CHARGE_RATE decimal(10,2)
						,@PM_VOICE_OT_CHARGE_RATE decimal(10,2)
						,@PM_VOICE_CHARGE_RATE decimal(10,2)
						,@NTE_VOICE_OT_CHARGE_RATE decimal(10,2)
						,@NTE_VOICE_CHARGE_RATE decimal(10,2)
						,@SPS_VOICE_OT_CHARGE_RATE decimal(10,2)
						,@NTE_CHARGE_RATE_LOE decimal(10,2)
						,@SDE_CHARGE_RATE decimal(10,2)
						,@SE_CHARGE_RATE decimal(10,2)
						,@PM_CHARGE_RATE decimal (10,2)
						,@TOT_COST_AMT decimal (10,2) = '0.00'
						,@BILL_OVRRDN_AMT decimal (10,2) = '0.00'
						,@NTE_TXT VARCHAR(MAX)
			
			
				SELECT @NTE_CHARGE_RATE = ISNULL(NTE_CHARGE_RATE,0.00)
						,@SDE_CHARGE_RATE = ISNULL(SDE_CHARGE_RATE,0.00)
						,@SE_CHARGE_RATE = ISNULL(SE_CHARGE_RATE,0.00)
						,@PM_CHARGE_RATE = ISNULL(PM_CHARGE_RATE,0.00)
						,@NE_VOICE_CHARGE_RATE = ISNULL(NE_VOICE_CHARGE_RATE,0.00)
						,@NE_VOICE_OT_CHARGE_RATE = ISNULL(NE_VOICE_OT_CHARGE_RATE,0.00)
						,@SPS_VOICE_CHARGE_RATE = ISNULL(SPS_VOICE_CHARGE_RATE,0.00)
						,@SE_VOICE_CHARGE_RATE = ISNULL(SE_VOICE_CHARGE_RATE,0.00)
						,@PM_VOICE_OT_CHARGE_RATE = ISNULL(PM_VOICE_OT_CHARGE_RATE,0.00)
						,@PM_VOICE_CHARGE_RATE = ISNULL(PM_VOICE_CHARGE_RATE,0.00)
						,@NTE_VOICE_OT_CHARGE_RATE = ISNULL(NTE_VOICE_OT_CHARGE_RATE,0.00)
						,@NTE_VOICE_CHARGE_RATE = ISNULL(NTE_VOICE_CHARGE_RATE,0.00)
						,@SPS_VOICE_OT_CHARGE_RATE = ISNULL(SPS_VOICE_OT_CHARGE_RATE,0.00)
						FROM
						(
						  SELECT PRMTR_VALU_TXT, PRMTR_NME
						  FROM dbo.LK_SYS_CFG WITH (NOLOCK)
						  WHERE PRMTR_NME IN ('NTE_CHARGE_RATE','SDE_CHARGE_RATE','SE_CHARGE_RATE'
											,'PM_CHARGE_RATE','NE_VOICE_CHARGE_RATE','NE_VOICE_OT_CHARGE_RATE'
											,'SPS_VOICE_CHARGE_RATE','SE_VOICE_CHARGE_RATE','PM_VOICE_OT_CHARGE_RATE'
											,'PM_VOICE_CHARGE_RATE','NTE_VOICE_OT_CHARGE_RATE','NTE_VOICE_CHARGE_RATE'
											,'SPS_VOICE_OT_CHARGE_RATE')
						) d
						PIVOT
						(
						  MAX(PRMTR_VALU_TXT)
						  FOR PRMTR_NME IN (NTE_CHARGE_RATE,SDE_CHARGE_RATE,SE_CHARGE_RATE
											,PM_CHARGE_RATE,NE_VOICE_CHARGE_RATE,NE_VOICE_OT_CHARGE_RATE
											,SPS_VOICE_CHARGE_RATE,SE_VOICE_CHARGE_RATE,PM_VOICE_OT_CHARGE_RATE
											,PM_VOICE_CHARGE_RATE,NTE_VOICE_OT_CHARGE_RATE,NTE_VOICE_CHARGE_RATE
											,SPS_VOICE_OT_CHARGE_RATE)
						) piv;			
				
				DECLARE @Cnt INT, 
						@Ctr INT = 0, 
						@DeviceID INT, 
						@RedesignID INT, 
						@RedesignNbr VARCHAR(20), 
						@IsBillableRT BIT = 0, 
						@bypassCD BIT = 0,
						@nte_chrg_cd BIT = 0,
						@redsgn_cat_id TINYINT = 0,
						@BillOvrrdnCd BIT = 0,
						@hasM5Data BIT = 0
						
						
				SELECT @Cnt = COUNT(1) FROM @RedesignDeviceIDsTable
				
				WHILE (@Ctr < @Cnt)
					BEGIN
						SELECT Top 1 @DeviceID = RedesignDevID,
									 @RedesignID = RedesignID
							FROM @RedesignDeviceIDsTable
							WHERE Flag = 0
						
						--Select @DeviceID
						IF ((@DeviceID > 0) AND (@RedesignID > 0))
						BEGIN
							SELECT	@nte_chrg_cd = i.NTE_CHRG_CD,
									@redsgn_cat_id = r.REDSGN_CAT_ID,
									@BillOvrrdnCd = r.BILL_OVRRDN_CD,
									@BILL_OVRRDN_AMT = r.BILL_OVRRDN_AMT,
									@RedesignNbr = r.REDSGN_NBR
								FROM dbo.REDSGN_DEVICES_INFO i WITH (NOLOCK)
								INNER JOIN dbo.REDSGN r with (nolock) on i.REDSGN_ID = r.REDSGN_ID
								WHERE REDSGN_DEV_ID = @DeviceID
							
							--Select @RedesignID
							
							SELECT @IsBillableRT = IS_BLBL_CD
								FROM	dbo.REDSGN r WITH (NOLOCK)
								INNER JOIN dbo.LK_REDSGN_TYPE t WITH (NOLOCK) ON r.REDSGN_TYPE_ID = t.REDSGN_TYPE_ID
								WHERE REDSGN_ID = @RedesignID
							
							--Select @IsBillableRT
							IF (@BillOvrrdnCd = 0)
							BEGIN
								IF @IsBillableRT = 1
									BEGIN
										IF EXISTS
											(
												SELECT 'X'
													FROM	dbo.REDSGN r WITH (NOLOCK)	
													INNER JOIN dbo.REDSGN_CUST_BYPASS b WITH (NOLOCK) ON b.H1_CD = r.H1_CD
														AND b.CUST_NME =  r.CUST_NME
													WHERE	b.REC_STUS_ID	= 1 AND r.CSG_LVL_ID=0 AND b.CSG_LVL_ID=0																	 	
														AND r.REDSGN_ID = @RedesignID
											) OR EXISTS(SELECT 'X'
													FROM	dbo.REDSGN r WITH (NOLOCK)	
													INNER JOIN dbo.REDSGN_CUST_BYPASS b WITH (NOLOCK) ON b.H1_CD = r.H1_CD
													INNER JOIN dbo.CUST_SCRD_DATA csdr WITH (NOLOCK) ON csdr.SCRD_OBJ_ID = r.REDSGN_ID AND csdr.SCRD_OBJ_TYPE_ID=16
													INNER JOIN dbo.CUST_SCRD_DATA csdb WITH (NOLOCK) ON csdb.SCRD_OBJ_ID = b.ID AND csdb.SCRD_OBJ_TYPE_ID=17
														AND dbo.decryptBinaryData(csdb.CUST_NME) =  dbo.decryptBinaryData(csdr.CUST_NME)
													WHERE	b.REC_STUS_ID	= 1 AND r.CSG_LVL_ID=1 AND b.CSG_LVL_ID=1																	 	
														AND r.REDSGN_ID = @RedesignID)
											BEGIN
												SET @bypassCD = 1
											END
											
											
											
										IF @IsBillableRT = 1 AND @bypassCD = 0
											BEGIN
												If @redsgn_cat_id = 1
													BEGIN
														-- Added condition to check if Redesign has completed device already
														IF NOT EXISTS
														(
															SELECT 'X'
															FROM dbo.REDSGN r WITH (NOLOCK)
															INNER JOIN dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK) ON rdi.REDSGN_ID = r.REDSGN_ID
															INNER JOIN dbo.M5_REDSGN_MSG m5 WITH (NOLOCK) ON rdi.REDSGN_DEV_ID = m5.REDSGN_DEV_ID
															WHERE r.REDSGN_ID = @RedesignID
														) AND @hasM5Data = 0
															BEGIN
																IF @nte_chrg_cd = 1
																	BEGIN
																		SELECT @TOT_COST_AMT = 
																					((NTE_LVL_EFFORT_AMT/60) * @NTE_VOICE_CHARGE_RATE) + 
																					((PM_LVL_EFFORT_AMT/60) * @PM_VOICE_CHARGE_RATE) +
																					((NE_LVL_EFFORT_AMT/60) * @NE_VOICE_CHARGE_RATE) +
																					((NE_OT_LVL_EFFORT_AMT/60) * @NE_VOICE_OT_CHARGE_RATE) +
																					((SPS_LVL_EFFORT_AMT/60) * @SPS_VOICE_CHARGE_RATE) +
																					((PM_OT_LVL_EFFORT_AMT/60) * @PM_VOICE_OT_CHARGE_RATE) +
																					((NTE_OT_LVL_EFFORT_AMT/60) * @NTE_VOICE_OT_CHARGE_RATE) +
																					((SPS_OT_LVL_EFFORT_AMT/60) * @SPS_VOICE_OT_CHARGE_RATE) 
																			FROM	dbo.REDSGN r WITH (NOLOCK)
																			WHERE	REDSGN_ID = @RedesignID
																		
																	END	
																ELSE
																	BEGIN
																		SELECT @TOT_COST_AMT = 
																					((PM_LVL_EFFORT_AMT/60) * @PM_VOICE_CHARGE_RATE) +
																					((NE_LVL_EFFORT_AMT/60) * @NE_VOICE_CHARGE_RATE) +
																					((NE_OT_LVL_EFFORT_AMT/60) * @NE_VOICE_OT_CHARGE_RATE) +
																					((SPS_LVL_EFFORT_AMT/60) * @SPS_VOICE_CHARGE_RATE) +
																					((PM_OT_LVL_EFFORT_AMT/60) * @PM_VOICE_OT_CHARGE_RATE) +
																					((NTE_OT_LVL_EFFORT_AMT/60) * @NTE_VOICE_OT_CHARGE_RATE) +
																					((SPS_OT_LVL_EFFORT_AMT/60) * @SPS_VOICE_OT_CHARGE_RATE) 
																			FROM	dbo.REDSGN r WITH (NOLOCK)
																			WHERE	REDSGN_ID = @RedesignID

																	END

																SET @hasM5Data = 1
															END
													END
												ELSE IF @redsgn_cat_id = 2
													BEGIN
														IF @nte_chrg_cd = 1
															BEGIN
																SELECT @TOT_COST_AMT = 
																			((NTE_LVL_EFFORT_AMT/60) * @NTE_CHARGE_RATE) + 
																			@SDE_CHARGE_RATE
																	FROM	dbo.REDSGN r WITH (NOLOCK)
																	WHERE	REDSGN_ID = @RedesignID
																		
															END	
														ELSE
															BEGIN
																SELECT @TOT_COST_AMT = @SDE_CHARGE_RATE
																	FROM	dbo.REDSGN r WITH (NOLOCK)
																	WHERE	REDSGN_ID = @RedesignID

															END
													END 
												ELSE 
													BEGIN
														IF @nte_chrg_cd = 1
															BEGIN
																SET @TOT_COST_AMT = 0
																--Select @TOT_COST_AMT = 
																--			((NTE_LVL_EFFORT_AMT/60) * @NTE_CHARGE_RATE) + 
																--			@PM_VOICE_CHARGE_RATE
																--	From	dbo.REDSGN r with (nolock)
																--	Where	REDSGN_ID = @RedesignID
																		
															END	
														ELSE
															BEGIN
																SET @TOT_COST_AMT = 0
																--Select @TOT_COST_AMT = @PM_VOICE_CHARGE_RATE
																--	From	dbo.REDSGN r with (nolock)
																--	Where	REDSGN_ID = @RedesignID

															END
													END
												
											END
									END	
							END
							ELSE
							BEGIN
								SET @TOT_COST_AMT = @BILL_OVRRDN_AMT
								UPDATE @RedesignDeviceIDsTable SET Flag = 1, Total_Amt = 0 WHERE RedesignID = @RedesignID AND RedesignDevID != @DeviceID
							END
							--Select @IsBillableRT As 'Billable RT',@bypassCD AS 'Bypass CD'
							
							IF (@TOT_COST_AMT>0)
							BEGIN
								IF (@BillOvrrdnCd=1)
									SET @NTE_TXT = 'Redesign Charge sent to Mach5 with Overriden Billing Amount - ' + @RedesignNbr + ' - $' + CONVERT(VARCHAR,@TOT_COST_AMT)
								ELSE
									SET @NTE_TXT = 'Redesign Charge sent to Mach5 with Billing Amount - ' + @RedesignNbr + ' - $' + CONVERT(VARCHAR,@TOT_COST_AMT)

								IF (@EventID != -1)
								BEGIN
									INSERT INTO dbo.EVENT_HIST  (EVENT_ID,ACTN_ID,CMNT_TXT,CREAT_BY_USER_ID,MODFD_BY_USER_ID,MODFD_DT,CREAT_DT)
									VALUES (@EventID,20,@NTE_TXT,1,1,GETDATE(),GETDATE())
								END
								INSERT INTO dbo.REDSGN_NOTES (REDSGN_ID,REDSGN_NTE_TYPE_ID,NOTES,CRETD_BY_CD,CRETD_DT)
								SELECT @RedesignID, 4, @NTE_TXT, 1, GETDATE()
							END
							
							UPDATE @RedesignDeviceIDsTable 
								SET	RedesignID = @RedesignID,
									Total_Amt = @TOT_COST_AMT
								WHERE RedesignDevID = @DeviceID 
							
							UPDATE @RedesignDeviceIDsTable SET Flag = 1 WHERE RedesignDevID = @DeviceID
						END
						SET @Ctr = @Ctr + 1	
						SET @TOT_COST_AMT = 0
						SET @DeviceID = 0
						SET @RedesignID = 0						
					END
				--Select * From @RedesignDeviceIDsTable
				
				IF((SELECT COUNT(1) FROM @RedesignDeviceIDsTable) > 0)
					BEGIN
						INSERT INTO dbo.M5_REDSGN_MSG (REDSGN_DEV_ID,CHARGE_AMT,SITE,STUS_ID,CREAT_DT,SENT_DT,EVENT_ID)
							SELECT DISTINCT r.RedesignDevID,r.Total_Amt,rdi.DEV_NME,10,GETDATE(),null,rdeh.EVENT_ID
								FROM @RedesignDeviceIDsTable r 
									INNER JOIN	dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK) ON r.RedesignDevID = rdi.REDSGN_DEV_ID
									LEFT JOIN	dbo.REDSGN_DEV_EVENT_HIST rdeh WITH (NOLOCK) ON rdi.REDSGN_DEV_ID = rdeh.REDSGN_DEV_ID
								WHERE r.Total_Amt > 0
									AND NOT EXISTS (SELECT 'X' FROM dbo.M5_REDSGN_MSG mrm WITH (NOLOCK) WHERE mrm.REDSGN_DEV_ID = r.RedesignDevID)
					END
			END
    END TRY
    BEGIN CATCH
		EXEC dbo.insertErrorInfo
    END CATCH
END