USE [COWS]
GO
_CreateObject 'SP','dbo','getFSAResponses'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/19/2011
-- Description:	Get the details for all FSA orders where we need to send responses!
-- =========================================================
ALTER PROCEDURE [dbo].[getFSAResponses]
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	EXEC [dbo].[processBillActivation]
	
	--Begin
	/*
		Added code below to delete messages inserted into FSA_ORDR_MSG table for which BAR pending has been loaded on CCD date by completing MDS Review task
		but event for order is still in pending status  
	*/
	DECLARE @OnHoldOrders TABLE (ORDR_ID INT)
	INSERT INTO @OnHoldOrders
	SELECT fom.ORDR_ID
		FROM	dbo.FSA_ORDR_MSG fom WITH (NOLOCK)
		WHERE	fom.STUS_ID = 10
			AND	EXISTS
				(
					SELECT 'X'
						FROM	dbo.ACT_TASK at WITH (NOLOCK)
						WHERE	at.ORDR_ID	=	fom.ORDR_ID
							AND	at.TASK_ID	=	1000
							AND	at.STUS_ID	=	3
				)
	
	IF ((SELECT COUNT(1) FROM  @OnHoldOrders) > 0)
		BEGIN
			DELETE fom
				FROM dbo.FSA_ORDR_MSG fom WITH (ROWLOCK)
			INNER JOIN @OnHoldOrders t on fom.ORDR_ID = t.ORDR_ID
		END
	
	--End
		
	SELECT			fo.ORDR_ID AS ORDR_ID		
					,fo.FTN
					,fm.FSA_MSG_ID	
		FROM		dbo.FSA_ORDR_MSG	fm	WITH (NOLOCK)
		INNER JOIN	dbo.FSA_ORDR		fo	WITH (NOLOCK)	ON	fm.ORDR_ID	=	fo.ORDR_ID
		WHERE		fm.STUS_ID		=	10
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO
