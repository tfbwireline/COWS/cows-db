﻿USE [COWS]
GO
_CreateObject 'SP','dbo','unlinkOrderFromVendorOrder'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 3rd Oct 2011
-- Description:	Disassociates Order with Vendor Order
-- ================================================================

ALTER PROCEDURE [dbo].[unlinkOrderFromVendorOrder]
	@OrderID INT,
	@VendorOrderTypeID TINYINT,
	@IsTerminating BIT,
	@UserID INT
	--,@IsSwap BIT
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	UPDATE	dbo.VNDR_ORDR WITH (ROWLOCK)
		SET		ORDR_ID = NULL,
				MODFD_BY_USER_ID = @UserID,
				MODFD_DT = GETDATE()
				--,PREV_ORDR_ID = CASE WHEN @IsSwap = 1 THEN  @OrderID
				--					END
		WHERE	ORDR_ID = @OrderID
			AND VNDR_ORDR_TYPE_ID = @VendorOrderTypeID
			AND TRMTG_CD = @IsTerminating
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
