USE [COWS]
GO
_CreateObject 'SP','dbo','unHoldGOMPLTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <11/26/2013>
-- Description:	<unhold GOM PL for disconnect orders>
-- =============================================
ALTER PROCEDURE dbo.unHoldGOMPLTask
@OrderID	Int,
@TaskID		SmallInt,
@TaskStatus	TinyInt,
@Comments	Varchar(1000) = NULL
AS
BEGIN
	BEGIN TRY

DECLARE @unhold bit = 1
	IF ((SELECT COUNT(1) FROM dbo.ACT_TASK WITH (NOLOCK) WHERE ORDR_ID = @OrderID AND TASK_ID = 103 AND STUS_ID = 5) > 0)
		BEGIN
			IF @TaskID = 202
				BEGIN
					IF EXISTS
					(
						SELECT 'X'
							FROM dbo.VNDR_ORDR WITH (NOLOCK)
							WHERE ORDR_ID = @OrderID 
								AND ISNULL(BYPAS_VNDR_ORDR_MS_CD,'') = '1'
					)
					BEGIN
						SET @unhold = 0
					END

					IF EXISTS
					(
						SELECT 'X'
							FROM dbo.ORDR_NTE WITH (NOLOCK)
							WHERE	ORDR_ID = @OrderID
								AND ( 
										NTE_TXT = 'bypassing xNCI Order Sent to Vendor Date milestone.'
											OR
										NTE_TXT = 'bypassing xNCI Order Acknowledged by Vendor Date milestone.'
									)
					)
						BEGIN
							Set @unhold = 0
						END
				END			
			IF @unhold = 1
				BEGIN
					UPDATE	dbo.ACT_TASK 
						SET		STUS_ID = 0, MODFD_DT = GETDATE()
						WHERE	ORDR_ID = @OrderID 
							AND TASK_ID = 103
					
				END
		END
	END TRY

	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
END
GO
