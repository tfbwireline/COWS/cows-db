USE [COWS]
GO
_CreateObject 'SP','web','getNewMDSEventData'
GO
-- =========================================================          
-- Author:  Suman Chitemella          
-- Create date: 2012-7-10          
-- Description: This SP is used to get MDS Event Data          
      
--********************************************************      
-- This is the New MDS rewrite      
--********************************************************      
      
-- =========================================================          
ALTER PROCEDURE [web].[getNewMDSEventData]      
 @iEventID BIGINT          
AS          
BEGIN          
SET NOCOUNT ON;          
          
Begin Try          
 IF OBJECT_ID (N'tempdb..#FSA_MDS_EVENT',N'U') IS NOT NULL          
  DROP TABLE #FSA_MDS_EVENT         
        
 OPEN SYMMETRIC KEY FS@K3y             
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;             
            
 SELECT MDS_EVENT_NEW.EVENT_ID AS EVENT_ID, FSA_MDS_EVENT_NEW.FSA_MDS_EVENT_ID           
  INTO #FSA_MDS_EVENT           
  FROM dbo.MDS_EVENT_NEW WITH (NOLOCK)          
    LEFT JOIN dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) ON FSA_MDS_EVENT_NEW.EVENT_ID = MDS_EVENT_NEW.EVENT_ID          
  WHERE MDS_EVENT_NEW.EVENT_ID = @iEventID          
            
 SELECT mp.EVENT_ID,          
   ev.CSG_LVL_ID,
   CASE WHEN ev.CSG_LVL_ID=0 THEN mp.EVENT_TITLE_TXT ELSE ISNULL(dbo.decryptBinaryData(csd.EVENT_TITLE_TXT),'') END AS EVENT_TITLE_TXT,    
   mp.MDS_FAST_TRK_CD,          
   mp.MDS_FAST_TRK_TYPE_ID,          
   mp.H1_ID,      
   mp.CHARS_ID,      
   CASE WHEN ev.CSG_LVL_ID=0 THEN mp.CUST_NME ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'') END AS CUST_NME,    
   CASE WHEN ev.CSG_LVL_ID=0 THEN mp.CUST_EMAIL_ADR ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_EMAIL_ADR),'') END AS CUST_EMAIL_ADR,
   mp.MDS_ACTY_TYPE_ID,          
   --mp.MDS_INSTL_ACTY_TYPE_ID,          
   mp.DSGN_DOC_LOC_TXT,          
   mp.SHRT_DES,          
   mp.FULL_CUST_DISC_CD,          
   CASE WHEN mp.DEV_CNT < 0 THEN 0     
					ELSE mp.DEV_CNT      
					END AS DEV_CNT,          
   mp.DISC_NTFY_PDL_NME,          
   CASE mp.MDS_BRDG_NEED_ID WHEN NULL THEN 0
							ELSE mp.MDS_BRDG_NEED_ID END AS MDS_BRDG_NEED_ID,          
   mp.CNFRC_BRDG_NBR,          
   mp.CNFRC_PIN_NBR,
   mp.ONLINE_MEETING_ADR,          
   mp.STRT_TMST,          
   mp.END_TMST,          
   mp.CREAT_BY_USER_ID,          
   mp.EXTRA_DRTN_TME_AMT,          
   mp.EVENT_DRTN_IN_MIN_QTY,          
   --mp.MANL_CD,          
   ISNULL(mp.TME_SLOT_ID, 0) AS TME_SLOT_ID,          
   mp.EVENT_STUS_ID,          
   --mp.DES_CMNT_TXT,          
   mp.REC_STUS_ID,          
   mp.MODFD_BY_USER_ID,          
   mp.MODFD_DT,          
   mp.CREAT_DT,          
   mp.PUB_EMAIL_CC_TXT,          
   mp.CMPLTD_EMAIL_CC_TXT,          
   mp.WRKFLW_STUS_ID,              
   mp.FAIL_REAS_ID,          
   ISNULL(mp.ESCL_REAS_ID , 0) AS ESCL_REAS_ID,          
   mp.PRIM_REQ_DT,          
   mp.SCNDY_REQ_DT,          
   ISNULL(mp.REQOR_USER_ID, 0)AS REQOR_USER_ID,          
   mp.ESCL_CD,          
   mp.CUST_ACCT_TEAM_PDL_NME,          
   les.EVENT_STUS_DES,          
   lws.WRKFLW_STUS_DES,          
   leso.EVENT_STUS_DES AS OLD_EVENT_STUS_DES,          
   mp.PRE_CFG_CMPLT_CD,      
   mp.SOWS_EVENT_ID,        
   mp.MNS_PM_ID,      
   mp.ODIE_CUST_ID,      
   mp.BUS_JUSTN_TXT,      
   mp.OPT_OUT_REAS_TXT,      
   mp.SPRINT_CPE_NCR_ID,      
   mp.CPE_DSPCH_EMAIL_ADR,      
   mp.CPE_DSPCH_CMNT_TXT,      
   ISNULL(mp.SPRS_EMAIL_ON_PUB_CD, 0) AS SPRS_EMAIL_ON_PUB_CD,      
   mp.BUS_JUSTN_CMNT_TXT,
   --mp.FULFILLED_DT,
   mp.SHIPPED_DT,
   --mp.SHIP_CUST_NME,
   mp.SHIP_CUST_EMAIL_ADR,
   --mp.SHIP_CUST_PHN_NBR,
   mp.SHIP_TRK_REFR_NBR,
   mp.DEV_SERIAL_NBR,
   mp.WOOB_IP_ADR, CASE mp.MDS_BRDG_NEED_CD WHEN NULL THEN 0 ELSE mp.MDS_BRDG_NEED_CD END AS MDS_BRDG_NEED_CD
  FROM  dbo.MDS_EVENT_NEW mp WITH (NOLOCK)
  INNER JOIN dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID = mp.EVENT_ID          
  INNER JOIN #FSA_MDS_EVENT fe WITH (NOLOCK) ON mp.EVENT_ID = fe.EVENT_ID          
  INNER JOIN  dbo.LK_EVENT_STUS les WITH (NOLOCK) ON les.EVENT_STUS_ID = mp.EVENT_STUS_ID          
  INNER JOIN  dbo.LK_WRKFLW_STUS lws WITH (NOLOCK) ON lws.WRKFLW_STUS_ID = mp.WRKFLW_STUS_ID      
  LEFT JOIN   dbo.CUST_SCRD_DATA  csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mp.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=8
  LEFT JOIN   dbo.LK_EVENT_STUS leso WITH (NOLOCK) ON leso.EVENT_STUS_ID = mp.OLD_EVENT_STUS_ID          
            
      
       
 SELECT MDS_MNGD_ACT_ID AS RSPN_INFO_ID,           
   ISNULL(mngd.SLCTD_CD, 0) AS Selected,           
   mngd.RDSN_NBR AS RedesignNumber,           
   mngd.ODIE_DEV_NME AS ODIE_DEV_NME,          
   CASE mngd.FAST_TRK_CD          
     WHEN 0 THEN 'No'          
     WHEN 1 THEN 'Yes'          
     ELSE ''           
     END AS Fast_Track_Flag,           
   CASE mngd.FAST_TRK_CD           
     WHEN 0 THEN '0'           
     WHEN 1 THEN '1'          
     ELSE ''          
     END AS Fast_Track_Flag_CD, 
	 ldm.MANF_NME AS MANF_NME, 
	 mngd.MANF_ID AS MANF_ID,  
	 dm.DEV_MODEL_NME AS DEV_MODEL_NME, 
	 mngd.DEV_MODEL_ID AS DEV_MODEL_ID,     
   Convert(Bit, CASE mngd.OPT_OUT_CD                       
     WHEN 'Y' THEN 'true'                      
     WHEN 'N' THEN 'false'                
     ELSE 'false'                      
     END) AS Optout_CD,    
   --mngd.OPT_OUT_CD AS Optout_CD,          
   mngd.INTL_CTRY_CD AS INTL_CTRY_CD,      
   mngd.PHN_NBR AS PHN_NBR,      
   SaSS.SRVC_ASSRN_SITE_SUPP_DES AS SRVC_ASSRN_SITE_SUPP,      
   mngd.SRVC_ASSRN_SITE_SUPP_ID AS SRVC_ASSRN_SITE_SUPP_ID,
   CASE mngd.FRWL_PROD_CD           
        WHEN 'Y' THEN 'Yes'          
     WHEN 'N' THEN 'No'          
     ELSE 'No'         
     END AS Firewall_Product,                   
     ISNULL(mngd.FRWL_PROD_CD, 'N') AS Firewall_Product_CD,
   CASE mngd.WOOB_CD           
        WHEN 'Y' THEN 'Yes'          
     WHEN 'N' THEN 'No'          
     ELSE 'No'         
     END AS WOOB,                   
     ISNULL(mngd.WOOB_CD, 'N') AS WOOB_CD,
   ROW_NUMBER() OVER(ORDER BY MDS_MNGD_ACT_ID) AS ManagedActivityID          
  FROM MDS_MNGD_ACT_NEW  mngd WITH (NOLOCK)          
    INNER JOIN LK_DEV_MANF ldm WITH (NOLOCK) ON ldm.MANF_ID = mngd.MANF_ID      
    INNER JOIN LK_DEV_MODEL dm WITH (NOLOCK) ON dm.DEV_MODEL_ID = mngd.DEV_MODEL_ID      
    INNER JOIN LK_SRVC_ASSRN_SITE_SUPP SaSS WITH (NOLOCK) ON SaSS.SRVC_ASSRN_SITE_SUPP_ID = mngd.SRVC_ASSRN_SITE_SUPP_ID      
  WHERE mngd.EVENT_ID = @iEventID          
            
  SELECT ea.EVENT_ID,          
      ea.ASN_TO_USER_ID,          
      lu.USER_ADID,          
      lu.DSPL_NME          
  FROM dbo.EVENT_ASN_TO_USER ea WITH (NOLOCK) INNER JOIN          
     dbo.LK_USER lu WITH (NOLOCK) on lu.[USER_ID] = ea.ASN_TO_USER_ID          
  WHERE ea.EVENT_ID = @iEventID          
    AND ea.REC_STUS_ID = 1          
       
SELECT   mp.*, lm.MDS_MAC_ACTY_NME AS MDS_MAC_ACTY_NME                       
  FROM  MDS_EVENT_MAC_ACTY mp WITH (NOLOCK)                      
  INNER JOIN #FSA_MDS_EVENT  fe WITH (NOLOCK) ON mp.EVENT_ID = fe.EVENT_ID                      
  LEFT JOIN LK_MDS_MAC_ACTY lm WITH (NOLOCK) ON lm.MDS_MAC_ACTY_ID = mp.MDS_MAC_ACTY_ID  
      
      
  SELECT DISTINCT  ISNULL(lm.MDS_LOC_CAT_DES, '')       AS MDS_LOC_CAT_DES          
         ,ISNULL(ml.MDS_LOC_CAT_ID, '')      AS MDS_LOC_CAT_ID          
         ,ISNULL(lt.MDS_LOC_TYPE_DES, '')     AS MDS_LOC_TYPE_DES          
         ,ISNULL(ml.MDS_LOC_TYPE_ID, '')      AS MDS_LOC_TYPE_ID          
         ,ISNULL(ml.MDS_LOC_NBR, '')       AS MDS_LOC_NBR          
         ,ISNULL(Convert(Varchar(10), MDS_LOC_RAS_DT, 101), '') AS MDS_LOC_RAS_DT          
         ,0             AS MDSLOCID            
      FROM  dbo.MDS_EVENT_LOC ml WITH (NOLOCK)       
    LEFT JOIN dbo.LK_MDS_LOC_CAT lm WITH (NOLOCK) ON ml.MDS_LOC_CAT_ID = lm.MDS_LOC_CAT_ID          
    LEFT JOIN dbo.LK_MDS_LOC_TYPE lt WITH (NOLOCK) ON ml.MDS_LOC_TYPE_ID = lt.MDS_LOC_TYPE_ID             
      WHERE ml.EVENT_ID = @iEventID  
          
 SELECT   mp.*          
  FROM  EVENT_HIST  mp WITH (NOLOCK)          
  INNER JOIN #FSA_MDS_EVENT fe WITH (NOLOCK) ON mp.FSA_MDS_EVENT_ID = fe.EVENT_ID            
          
          
 SELECT TOP 1 lu.DSPL_NME, lu.[USER_ID]          
 FROM dbo.EVENT_HIST eh WITH (NOLOCK)          
 INNER JOIN #FSA_MDS_EVENT fe WITH (NOLOCK) ON eh.EVENT_ID = fe.EVENT_ID          
 INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID] = eh.MODFD_BY_USER_ID          
 INNER JOIN dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON ugr.[USER_ID] = lu.[USER_ID]          
 WHERE ugr.ROLE_ID = 22          
 ORDER BY eh.EVENT_HIST_ID DESC          

	SELECT	H5_H6_CUST_ID AS DISC_H5_H6,
			SERIAL_NBR,
			ODIE_DEV_NME,
			VNDR_NME,
			MODEL_NME,
			0 AS MDSDiscOEID    
      FROM  dbo.MDS_EVENT_DISCO  WITH (NOLOCK)             
      WHERE EVENT_ID = @iEventID  
          
End Try          
          
Begin Catch          
 EXEC [dbo].[insertErrorInfo]          
 DECLARE @ErrMsg nVarchar(4000),           
  @ErrSeverity Int                
 SELECT @ErrMsg  = ERROR_MESSAGE(),                
  @ErrSeverity = ERROR_SEVERITY()                
 RAISERROR(@ErrMsg, @ErrSeverity, 1)                
End Catch          
END