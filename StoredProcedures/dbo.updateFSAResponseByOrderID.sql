USE [COWS]
GO
_CreateObject 'SP','dbo','updateFSAResponseByOrderID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/19/2011
-- Description:	Update the FSA order message status!
-- =========================================================
ALTER PROCEDURE [dbo].[updateFSAResponseByOrderID]
	@ORDR_ID	Int
	,@STUS_ID	Int
AS
BEGIN
--SET NOCOUNT ON;
Begin Try
	UPDATE		dbo.FSA_ORDR_MSG	WITH (ROWLOCK)
		SET		STUS_ID	=	@STUS_ID
		WHERE	ORDR_ID	=	@ORDR_ID
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO
