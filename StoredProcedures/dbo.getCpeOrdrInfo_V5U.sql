USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getCpeOrdrInfo_V5U]    Script Date: 11/05/2020 7:56:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
	-- =============================================
	-- Author:		David Phillips
	-- Create date: 5/4/2015
	-- Description:	Appian CPE Customer Info.
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getCpeOrdrInfo_V5U] 
		@OrderID	Int = 0,
		@userADID  VARCHAR(20)='',
		@userCSGLvl  TINYINT=0					
	AS
	BEGIN
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;	

IF @OrderID = 0
	BEGIN 	
	SELECT	DISTINCT fo.ORDR_ID
				, CASE
					WHEN ord.DMSTC_CD = 1					THEN fo.FTN -- Intl 
					WHEN ord.PROD_ID  in ('UCCH','UCSV','MIPT','UCSM')	THEN  fo.FTN	-- Voice 
					WHEN (ord.DMSTC_CD = 0) THEN boa.DEVICE_ID + '-' + fo.FTN  -- Domestic 
				    ELSE ''                                 
				    END							           AS DEVICE_ID_FTN
				, CASE
					WHEN ((ord.DMSTC_CD = 1) AND (ord.PROD_ID IN ('CETH','CEPL','CEVPL','CENNI')))	THEN fcl.DEVICE_ID
					WHEN ord.PROD_ID  in ('UCCH','UCSV','MIPT','UCSM')	THEN  '' -- Voice 
					WHEN (ord.DMSTC_CD = 0) THEN boa.DEVICE_ID   -- Domestic 
				    ELSE ''                                 
				    END							           AS DEVICE_ID
				,fo.FTN									   AS FTN
				,boa.CUST_NME 
                ,case
				   WHEN    fo.ORDR_SUB_TYPE_CD = 'PRCV' THEN 'PRCV'
				   WHEN    fo.ORDR_SUB_TYPE_CD = 'ROCP' THEN 'ROCP'
				   WHEN    fo.ORDR_SUB_TYPE_CD = 'ISMV' THEN 'ISMV'
                   when    fo.ORDR_TYPE_CD = 'IN' then 'INST'
                   when    fo.ORDR_TYPE_CD = 'DC' then 'DISC'
                   WHEN    fo.ORDR_TYPE_CD = 'CN' then 'CAN'
                  END					       AS ACT
                ,CASE
					WHEN act.STUS_ID = 156 THEN 'HOLD--' + lt.TASK_NME
					ELSE lt.TASK_NME
					END							   AS STUS
                ,ISNULL(ord.CUST_CMMT_DT,fo.CUST_CMMT_DT)  AS CUST_CMMT_DT
                ,ISNULL(lu.FULL_NME,'Unassigned')          AS ASN_USER
                ,boa.CUST_ID
				,ISNULL(ord.DLVY_CLLI,'')				   AS CLLI_CD
				,boa.STREET_ADR_1
				,boa.STREET_ADR_2
				,boa.STREET_ADR_3
                 ,boa. BLDG_NME
                 ,boa.FLR_ID
                 ,boa.RM_NBR
                 ,boa.CTY_NME
				 ,boa.STT_CD
				 ,boa.ZIP_PSTL_CD
				 ,boa.CTRY_CD
                 ,boa.PRVN_NME
				 ,ISNULL(fo.CPE_EQPT_ONLY_CD,'N')		AS CPE_EQPT_ONLY_CD
				 ,ISNULL(fo.CPE_ACCS_PRVDR_CD,'')		AS CPE_ACCS_PRVDR_CD
				 ,ISNULL (fo.CPE_ECCKT_ID,'')           AS CPE_ECCKT_ID
				 ,ISNULL(fo.CPE_PHN_NBR,'')             AS CPE_PHN_NBR
				 ,ISNULL(fo.CPE_REC_ONLY_CD,'N')		AS CPE_REC_ONLY_CD
			--**************************************************************************	
				 ,boa.SHIP_STREET_ADR_1
				  ,boa.SHIP_STREET_ADR_2
				  ,boa.SHIP_STREET_ADR_3
				  ,boa.SHIP_BLDG_NME
                  ,boa.SHIP_FLR_ID
				  ,boa.SHIP_RM_NBR
                  ,boa.SHIP_CTY_NME
				  ,boa.SHIP_STT_CD								  AS SHIP_STT_CD
				  ,boa.SHIP_ZIP_PSTL_CD		  AS SHIP_ZIP_PSTL_CD
				  
				 ,boa.SHIP_CTRY_CD							AS  SHIP_CTRY_CD
				 ,	SHIP_PRVN_NME AS  SHIP_PRVN_NME      
				 , ISNULL(lu.USER_ID,'')                AS ASN_USER_ID
				 , ISNULL(lu.USER_ADID,'')              AS ASN_ADID
				 , ord.DMSTC_CD                         AS DMSTC_CD
				 , CASE WHEN ord.CSG_LVL_ID>0 THEN 1 ELSE 0 END  AS SCURD_CD
				 ,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_CD,'')
						FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
						     dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
						WHERE oj.ORDR_ID = fo.ORDR_ID
							ORDER BY oj.CREAT_DT DESC) AS JPRDY_CD
				,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_DES,'')
						FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
						     dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
						WHERE oj.ORDR_ID = fo.ORDR_ID
							ORDER BY oj.CREAT_DT DESC) AS JPRDY_DES
				 ,(SELECT TOP 1 DATEDIFF(dd,at1.CREAT_DT,GETDATE()) 
						FROM dbo.ACT_TASK at1 WITH (NOLOCK)
						WHERE at1.TASK_ID IN (600,601,602,1000,604)
						  AND at1.STUS_ID = 0
						  AND at1.ORDR_ID = fo.ORDR_ID) AS INBOX
				 , fo.INSTL_ESCL_CD                     AS EXPEDITE
				 , fo.CREAT_DT                          AS CREAT_DT -- GOM received order.
				 ,CASE
				    WHEN ord.DMSTC_CD = 1 AND act.TASK_ID = 601  THEN fo.CUST_CMMT_DT -- Intl default
				    WHEN isnull(ord.RAS_DT,'') = '' AND act.TASK_ID = 601  THEN DATEADD(d,-7,isnull(ord.CUST_CMMT_DT,fo.CUST_CMMT_DT))
				    ELSE ord.RAS_DT
				    END                                 AS RAS_DT 
				 , CASE
					WHEN ord.PROD_ID in ('UCCH','UCSV','MIPT','UCSM') THEN ''
					WHEN ord.DMSTC_CD = 0							  THEN 'pe208093' -- Domestic default
				    ELSE ''                                 
				    END                                AS ELID   -- space holder for later
				 ,CASE 
					when ord.PROD_ID in ('UCCH','UCSV','MIPT','UCSM')        THEN 'Y'
				    ELSE 'N'                                  
				    END AS VOICE_DATA_ORDR -- space holder
				 ,ISNULL(fo.DDR,'')                     AS DDR
				 ,ISNULL(fo.DDU,'')                     AS DDU
				 ,ISNULL(ord.SMR_NMR,'')                AS SMR_NMR
				 ,ISNULL(boa.SITE_ID,'')                 AS SITE_ID
				 ,ISNULL(ord.CPE_CLLI,'')               AS CPE_CLLI
				 ,CASE
					WHEN act.STUS_ID = 156 THEN 'Y'
					ELSE 'N'
					END								    AS HOLD
				 ,CASE fo.CPE_VNDR 
					WHEN 'Ericsson'                  THEN 'N'
					ELSE 'Y'
				 END                                   AS THIRD_PARTY_SITE
				 ,fo.PRNT_FTN                           AS RELTD_FTN
				 ,fo.SCA_NBR							AS SCA_NBR
				 ,ISNULL(nac.NID_SERIAL_NBR,'') AS NID_SERIAL_NBR
				 ,ISNULL(ims.IP_ADR,'') AS NID_IP
				 , cli.CPE_CUST_PRVD_SERIAL_NBR AS CPE_CUST_PRVD_SERIAL_NBR
				 ,ISNULL(fo.NUA,fo.CKT)         AS NuaCkt
	FROM         dbo.FSA_ORDR fo			WITH (NOLOCK)
	INNER JOIN   dbo.ORDR ord				WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
	INNER JOIN dbo.ACT_TASK act				WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID
	INNER JOIN   dbo.LK_TASK lt				WITH (NOLOCK) ON act.TASK_ID = lt.TASK_ID
	LEFT OUTER JOIN dbo.USER_WFM_ASMT uwa	WITH (NOLOCK) ON uwa.ORDR_ID = fo.ORDR_ID
												AND ((uwa.GRP_ID in (13,1,14)) OR (uwa.USR_PRF_ID IN (98,126)))
	LEFT OUTER JOIN dbo.LK_USER lu			WITH (NOLOCK) ON lu.USER_ID = uwa.ASN_USER_ID
	LEFT JOIN dbo.BPM_ORDR_ADR boa WITH (NOLOCK) ON ord.ORDR_ID = boa.ORDR_ID
	LEFT JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcl WITH (NOLOCK) ON fcl.ORDR_ID=ord.ORDR_ID AND fcl.SPRINT_MNTD_FLG='Y' AND fcl.CMPNT_FMLY='NID'
	LEFT JOIN dbo.MDS_EVENT_NTWK_TRPT ment WITH (NOLOCK) ON ment.MACH5_ORDR_NBR=fo.FTN AND ment.REC_STUS_ID=1
	LEFT JOIN dbo.EVENT_CPE_DEV ecd WITH (NOLOCK) ON ecd.CPE_ORDR_NBR=fo.FTN AND ecd.DEVICE_ID=fcl.DEVICE_ID AND ecd.REC_STUS_ID=1
	LEFT JOIN dbo.NID_ACTY nac WITH (NOLOCK) ON nac.FSA_CPE_LINE_ITEM_ID = fcl.FSA_CPE_LINE_ITEM_ID AND nac.REC_STUS_ID=251
	LEFT JOIN dbo.IP_ACTY iac WITH (NOLOCK) ON nac.NID_ACTY_ID = iac.NID_ACTY_ID AND iac.REC_STUS_ID=251
	LEFT JOIN dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID = iac.IP_MSTR_ID AND ims.REC_STUS_ID IN (251,253)
	LEFT OUTER JOIN (SELECT TOP 1 ORDR_ID, CPE_CUST_PRVD_SERIAL_NBR 
						FROM  dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) WHERE ISNULL(CPE_CUST_PRVD_SERIAL_NBR,'') <> '' 
						AND ORDR_ID = @OrderID )cli
					ON cli.ORDR_ID = fo.ORDR_ID
	WHERE  fo.PROD_TYPE_CD = 'CP'
		AND (
			(
				(act.TASK_ID IN (600,601,602,604,1000) AND act.STUS_ID = 0) 
					AND ord.DMSTC_CD = 0 AND  ISNULL(ord.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
				OR
				(act.TASK_ID IN (600,601,602,604,1000) AND act.STUS_ID = 156) 
					AND ord.DMSTC_CD = 0 AND  ISNULL(ord.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
			)
			
		OR (
			(act.TASK_ID in (600,601) AND act.STUS_ID = 0)
			 AND ord.DMSTC_CD = 1
			)
		
		OR (
			(
			  (act.TASK_ID IN (601,602,1000) AND act.STUS_ID = 0) 
			  AND ISNULL(ord.PROD_ID,'') in ('UCCH','UCSV','MIPT','UCSM')
			 )
		   ))

		AND ord.ORDR_CAT_ID = 6
	END	
		--AND NOT EXISTS (SELECT 'X' FROM dbo.FSA_ORDR_CPE_LINE_ITEM focli2 WITH (NOLOCK)
		--							WHERE focli2.DROP_SHP = 'Y' AND focli2.ORDR_ID = fo.ORDR_ID)
		
ELSE
	BEGIN			
		
		SELECT	DISTINCT fo.ORDR_ID
				, CASE
					WHEN ord.DMSTC_CD = 1					THEN fo.FTN -- Intl 
					WHEN ord.PROD_ID  in ('UCCH','UCSV','MIPT','UCSM')	THEN  fo.FTN	-- Voice 
					WHEN (ord.DMSTC_CD = 0) THEN boa.DEVICE_ID + '-' + fo.FTN  -- Domestic 
				    ELSE ''                                 
				    END							           AS DEVICE_ID_FTN
				, CASE
					WHEN ((ord.DMSTC_CD = 1) AND (ord.PROD_ID IN ('CETH','CEPL','CEVPL','CENNI')))	THEN fcl.DEVICE_ID
					WHEN ord.PROD_ID  in ('UCCH','UCSV','MIPT','UCSM')	THEN  '' -- Voice 
					WHEN (ord.DMSTC_CD = 0) THEN boa.DEVICE_ID   -- Domestic 
				    ELSE ''                                 
				    END							           AS DEVICE_ID
				,fo.FTN									   AS FTN
				,boa.CUST_NME 
                ,case
				   WHEN    fo.ORDR_SUB_TYPE_CD = 'PRCV' THEN 'PRCV'
				   WHEN    fo.ORDR_SUB_TYPE_CD = 'ROCP' THEN 'ROCP'
				   WHEN    fo.ORDR_SUB_TYPE_CD = 'ISMV' THEN 'ISMV'
                   when    fo.ORDR_TYPE_CD = 'IN' then 'INST'
                   when    fo.ORDR_TYPE_CD = 'DC' then 'DISC'
                   WHEN    fo.ORDR_TYPE_CD = 'CN' then 'CAN'
                  END					       AS ACT
                ,CASE
					WHEN act.STUS_ID = 156 THEN 'HOLD--' + lt.TASK_NME
					ELSE lt.TASK_NME
					END							   AS STUS
                ,ISNULL(ord.CUST_CMMT_DT,fo.CUST_CMMT_DT)  AS CUST_CMMT_DT
                ,ISNULL(lu.FULL_NME,'Unassigned')          AS ASN_USER
                ,boa.CUST_ID
				,ISNULL(ord.DLVY_CLLI,'')				   AS CLLI_CD
				,boa.STREET_ADR_1
				,boa.STREET_ADR_2
				,boa.STREET_ADR_3
                 ,boa. BLDG_NME
                 ,boa.FLR_ID
                 ,boa.RM_NBR
                 ,boa.CTY_NME
				 ,boa.STT_CD
				 ,boa.ZIP_PSTL_CD
				 ,boa.CTRY_CD
                 ,boa.PRVN_NME
				 ,ISNULL(fo.CPE_EQPT_ONLY_CD,'N')		AS CPE_EQPT_ONLY_CD
				 ,ISNULL(fo.CPE_ACCS_PRVDR_CD,'')		AS CPE_ACCS_PRVDR_CD
				 ,ISNULL (fo.CPE_ECCKT_ID,'')           AS CPE_ECCKT_ID
				 ,ISNULL(fo.CPE_PHN_NBR,'')             AS CPE_PHN_NBR
				 ,ISNULL(fo.CPE_REC_ONLY_CD,'N')		AS CPE_REC_ONLY_CD
			--**************************************************************************	
				 ,boa.SHIP_STREET_ADR_1
				  ,boa.SHIP_STREET_ADR_2
				  ,boa.SHIP_STREET_ADR_3
				  ,boa.SHIP_BLDG_NME
                  ,boa.SHIP_FLR_ID
				  ,boa.SHIP_RM_NBR
                  ,boa.SHIP_CTY_NME
				  ,boa.SHIP_STT_CD								  AS SHIP_STT_CD
				  ,boa.SHIP_ZIP_PSTL_CD		  AS SHIP_ZIP_PSTL_CD
				  
				 ,boa.SHIP_CTRY_CD							AS  SHIP_CTRY_CD
				 ,	SHIP_PRVN_NME AS  SHIP_PRVN_NME      
				 , ISNULL(lu.USER_ID,'')                AS ASN_USER_ID
				 , ISNULL(lu.USER_ADID,'')              AS ASN_ADID
				 , ord.DMSTC_CD                         AS DMSTC_CD
				 , CASE WHEN ord.CSG_LVL_ID>0 THEN 1 ELSE 0 END  AS SCURD_CD
				 ,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_CD,'')
						FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
						     dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
						WHERE oj.ORDR_ID = fo.ORDR_ID
							ORDER BY oj.CREAT_DT DESC) AS JPRDY_CD
				,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_DES,'')
						FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
						     dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
						WHERE oj.ORDR_ID = fo.ORDR_ID
							ORDER BY oj.CREAT_DT DESC) AS JPRDY_DES
				 ,(SELECT DATEDIFF(dd,at1.CREAT_DT,GETDATE()) 
						FROM dbo.ACT_TASK at1 WITH (NOLOCK)
						WHERE at1.TASK_ID IN (600,601,602,1000,604)
						  AND at1.STUS_ID = 0
						  AND at1.ORDR_ID = fo.ORDR_ID) AS INBOX
				 , fo.INSTL_ESCL_CD                     AS EXPEDITE
				 , fo.CREAT_DT                          AS CREAT_DT -- GOM received order.
				 ,CASE
				    WHEN ord.DMSTC_CD = 1 AND act.TASK_ID = 601  THEN fo.CUST_CMMT_DT -- Intl default
				    WHEN isnull(ord.RAS_DT,'') = '' AND act.TASK_ID = 601  THEN DATEADD(d,-7,isnull(ord.CUST_CMMT_DT,fo.CUST_CMMT_DT))
				    ELSE ord.RAS_DT
				    END                                 AS RAS_DT 
				 , CASE
					WHEN ord.PROD_ID in ('UCCH','UCSV','MIPT','UCSM') THEN ''
					WHEN ord.DMSTC_CD = 0							  THEN 'pe208093' -- Domestic default
				    ELSE ''                                 
				    END                                AS ELID   -- space holder for later
				 ,CASE 
					when ord.PROD_ID in ('UCCH','UCSV','MIPT','UCSM')        THEN 'Y'
				    ELSE 'N'                                  
				    END AS VOICE_DATA_ORDR -- space holder
				 ,ISNULL(fo.DDR,'')                     AS DDR
				 ,ISNULL(fo.DDU,'')                     AS DDU
				 ,ISNULL(ord.SMR_NMR,'')                AS SMR_NMR
				 ,ISNULL(boa.SITE_ID,'')                 AS SITE_ID
				 ,ISNULL(ord.CPE_CLLI,'')               AS CPE_CLLI
				 ,CASE
					WHEN act.STUS_ID = 156 THEN 'Y'
					ELSE 'N'
					END								    AS HOLD
				 ,CASE fo.CPE_VNDR 
					WHEN 'Ericsson'                  THEN 'N'
					ELSE 'Y'
				 END                                   AS THIRD_PARTY_SITE
				 ,fo.PRNT_FTN                           AS RELTD_FTN
				 ,fo.SCA_NBR							AS SCA_NBR
				 ,ISNULL(nac.NID_SERIAL_NBR,'') AS NID_SERIAL_NBR
				 ,ISNULL(ims.IP_ADR,'') AS NID_IP
				 , cli.CPE_CUST_PRVD_SERIAL_NBR AS CPE_CUST_PRVD_SERIAL_NBR
				 ,ISNULL(fo.NUA,fo.CKT)         AS NuaCkt
	FROM         dbo.FSA_ORDR fo			WITH (NOLOCK)
	INNER JOIN   dbo.ORDR ord				WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
	INNER JOIN dbo.ACT_TASK act				WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID
	INNER JOIN   dbo.LK_TASK lt				WITH (NOLOCK) ON act.TASK_ID = lt.TASK_ID
	LEFT OUTER JOIN dbo.USER_WFM_ASMT uwa	WITH (NOLOCK) ON uwa.ORDR_ID = fo.ORDR_ID
												AND ((uwa.GRP_ID in (13,1,14)) OR (uwa.USR_PRF_ID IN (98,126)))
	LEFT OUTER JOIN dbo.LK_USER lu			WITH (NOLOCK) ON lu.USER_ID = uwa.ASN_USER_ID
	LEFT JOIN dbo.BPM_ORDR_ADR boa WITH (NOLOCK) ON ord.ORDR_ID = boa.ORDR_ID
	LEFT JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcl WITH (NOLOCK) ON fcl.ORDR_ID=ord.ORDR_ID AND fcl.SPRINT_MNTD_FLG='Y' AND fcl.CMPNT_FMLY='NID'
	LEFT JOIN dbo.MDS_EVENT_NTWK_TRPT ment WITH (NOLOCK) ON ment.MACH5_ORDR_NBR=fo.FTN AND ment.REC_STUS_ID=1
	LEFT JOIN dbo.EVENT_CPE_DEV ecd WITH (NOLOCK) ON ecd.CPE_ORDR_NBR=fo.FTN AND ecd.DEVICE_ID=fcl.DEVICE_ID AND ecd.REC_STUS_ID=1
	LEFT JOIN dbo.NID_ACTY nac WITH (NOLOCK) ON nac.FSA_CPE_LINE_ITEM_ID = fcl.FSA_CPE_LINE_ITEM_ID AND nac.REC_STUS_ID=251
	LEFT JOIN dbo.IP_ACTY iac WITH (NOLOCK) ON nac.NID_ACTY_ID = iac.NID_ACTY_ID AND iac.REC_STUS_ID=251
	LEFT JOIN dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID = iac.IP_MSTR_ID AND ims.REC_STUS_ID IN (251,253)
	LEFT OUTER JOIN (SELECT TOP 1 ORDR_ID, CPE_CUST_PRVD_SERIAL_NBR 
							FROM  dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) WHERE ISNULL(CPE_CUST_PRVD_SERIAL_NBR,'') <> ''
								AND ORDR_ID = @OrderID )cli
							ON cli.ORDR_ID = fo.ORDR_ID
	WHERE  fo.PROD_TYPE_CD = 'CP'
		AND (
			(
				(act.TASK_ID IN (600,601,602,604,1000) AND act.STUS_ID = 0) 
					AND ord.DMSTC_CD = 0 AND  ISNULL(ord.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
				OR
				(act.TASK_ID IN (600,601,602,604,1000) AND act.STUS_ID = 156) 
					AND ord.DMSTC_CD = 0 AND  ISNULL(ord.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
			)
			
		OR (
			(act.TASK_ID in (600,601) AND act.STUS_ID = 0)
			 AND ord.DMSTC_CD = 1
			)
		
		OR (
			(
			  (act.TASK_ID IN (601,602,1000) AND act.STUS_ID = 0) 
			  AND ISNULL(ord.PROD_ID,'') in ('UCCH','UCSV','MIPT','UCSM')
			 )
		   ))

		AND ord.ORDR_CAT_ID = 6
		AND fo.ORDR_ID = @OrderID
	END
END