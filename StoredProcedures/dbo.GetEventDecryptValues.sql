USE [COWS]
GO
_CreateObject 'SP','dbo','GetEventDecryptValues'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		Jagan Gangi
-- Create date: 7/6/12
-- Using a CAND EventID get the decrypted values of all encrypted columns
 -- ======================================================================

ALTER PROCEDURE [dbo].[GetEventDecryptValues]  (@EID INT, @EType VARCHAR(10))
AS
    BEGIN
	BEGIN TRY
		OPEN SYMMETRIC KEY FS@K3y 
			DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
			
		IF (@EType = 'AD')
		BEGIN
			SELECT	CONVERT(varchar(Max), DecryptByKey(CUST_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_EMAIL_ADR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_CELL_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PGR_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PGR_PIN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_TITLE_TXT)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_DES))
			FROM dbo.CUST_SCRD_DATA WITH (NOLOCK)
			WHERE SCRD_OBJ_ID = @EID 
			  AND SCRD_OBJ_TYPE_ID = 1
        END
        ELSE IF  (@EType = 'MPLS')
		BEGIN
			SELECT	CONVERT(varchar(Max), DecryptByKey(CUST_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_EMAIL_ADR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_CELL_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PGR_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PGR_PIN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_TITLE_TXT)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_DES))
			FROM dbo.CUST_SCRD_DATA WITH (NOLOCK)
			WHERE SCRD_OBJ_ID = @EID 
			  AND SCRD_OBJ_TYPE_ID = 9
        END
        ELSE IF (@EType = 'NGVN')
		BEGIN
			SELECT	CONVERT(varchar(Max), DecryptByKey(CUST_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_EMAIL_ADR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_CELL_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PGR_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PGR_PIN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_TITLE_TXT)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_DES))
			FROM dbo.CUST_SCRD_DATA WITH (NOLOCK)
			WHERE SCRD_OBJ_ID = @EID 
			  AND SCRD_OBJ_TYPE_ID = 11
        END
        ELSE IF (@EType = 'SPLK')
		BEGIN
			SELECT	CONVERT(varchar(Max), DecryptByKey(CUST_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_EMAIL_ADR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_CELL_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PGR_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PGR_PIN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_TITLE_TXT)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_DES))
			FROM dbo.CUST_SCRD_DATA WITH (NOLOCK)
			WHERE SCRD_OBJ_ID = @EID 
			  AND SCRD_OBJ_TYPE_ID = 19
        END
		ELSE IF (@EType = 'SIPT')
		BEGIN
			SELECT	CONVERT(varchar(Max), DecryptByKey(CUST_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PHN_NBR)),
					--CONVERT(varchar(Max), DecryptByKey(SITE_CNTCT_HR_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_EMAIL_ADR)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_TITLE_TXT)),
					CONVERT(varchar(Max), DecryptByKey(STREET_ADR_1)),
					CONVERT(varchar(Max), DecryptByKey(FLR_ID)),
					CONVERT(varchar(Max), DecryptByKey(CTY_NME)),
					CONVERT(varchar(Max), DecryptByKey(STT_PRVN_NME)),
					CONVERT(varchar(Max), DecryptByKey(CTRY_RGN_NME)),
					CONVERT(varchar(Max), DecryptByKey(ZIP_PSTL_CD))
			FROM dbo.CUST_SCRD_DATA WITH (NOLOCK)
			WHERE SCRD_OBJ_ID = @EID 
			  AND SCRD_OBJ_TYPE_ID = 18
        END
		ELSE IF (@EType = 'UCaaS')
		BEGIN
			SELECT	CONVERT(varchar(Max), DecryptByKey(CUST_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_CELL_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(SRVC_ASSRN_POC_NME)),
					CONVERT(varchar(Max), DecryptByKey(SRVC_ASSRN_POC_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(SRVC_ASSRN_POC_CELL_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(STREET_ADR_1)),
					CONVERT(varchar(Max), DecryptByKey(FLR_ID)),
					CONVERT(varchar(Max), DecryptByKey(CTY_NME)),
					CONVERT(varchar(Max), DecryptByKey(STT_PRVN_NME)),
					CONVERT(varchar(Max), DecryptByKey(CTRY_RGN_NME)),
					CONVERT(varchar(Max), DecryptByKey(ZIP_PSTL_CD)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_TITLE_TXT)),
					CONVERT(varchar(Max), DecryptByKey(CUST_EMAIL_ADR))
			FROM dbo.CUST_SCRD_DATA WITH (NOLOCK)
			WHERE SCRD_OBJ_ID = @EID 
			  AND SCRD_OBJ_TYPE_ID = 20
        END
		ELSE IF (@EType = 'MDS')
		BEGIN
			SELECT	CONVERT(varchar(Max), DecryptByKey(CUST_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_NME)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(CUST_CNTCT_CELL_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(SRVC_ASSRN_POC_NME)),
					CONVERT(varchar(Max), DecryptByKey(SRVC_ASSRN_POC_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(SRVC_ASSRN_POC_CELL_PHN_NBR)),
					CONVERT(varchar(Max), DecryptByKey(STREET_ADR_1)),
					CONVERT(varchar(Max), DecryptByKey(FLR_ID)),
					CONVERT(varchar(Max), DecryptByKey(CTY_NME)),
					CONVERT(varchar(Max), DecryptByKey(STT_PRVN_NME)),
					CONVERT(varchar(Max), DecryptByKey(CTRY_RGN_NME)),
					CONVERT(varchar(Max), DecryptByKey(ZIP_PSTL_CD)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_TITLE_TXT)),
					CONVERT(varchar(Max), DecryptByKey(CUST_EMAIL_ADR)),
					CONVERT(varchar(Max), DecryptByKey(FRST_NME)),
					CONVERT(varchar(Max), DecryptByKey(STREET_ADR_2)),
					CONVERT(varchar(Max), DecryptByKey(BLDG_NME)),
					CONVERT(varchar(Max), DecryptByKey(RM_NBR)),
					CONVERT(varchar(Max), DecryptByKey(STT_CD)),
					CONVERT(varchar(Max), DecryptByKey(CTRY_CD)),
					CONVERT(varchar(Max), DecryptByKey(STREET_ADR_3)),
					CONVERT(varchar(Max), DecryptByKey(EVENT_DES)),
					CONVERT(varchar(Max), DecryptByKey(LST_NME)),
					CONVERT(varchar(Max), DecryptByKey(NTE_TXT))
			FROM dbo.CUST_SCRD_DATA WITH (NOLOCK)
			WHERE SCRD_OBJ_ID = @EID 
			  AND SCRD_OBJ_TYPE_ID = 7
        END
		END TRY
BEGIN CATCH
EXEC dbo.insertErrorInfo
END CATCH
    END