USE [COWS]
GO
_CreateObject 'SP','dbo','UpdateSDEEmailStatus'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Created By:	Sarah Sandoval
-- Create date: 08092017
-- Description:	Update SDE Opportunity Email Status
-- =============================================
ALTER PROCEDURE [dbo].[UpdateSDEEmailStatus]
	@EMAIL_REQ_ID	INT,
	@STUS_ID		INT
AS
BEGIN
	BEGIN TRY
		UPDATE		dbo.EMAIL_REQ	WITH (ROWLOCK)
			SET		STUS_ID	=	@STUS_ID,
					SENT_DT =   CASE @STUS_ID WHEN 11 THEN GETDATE() ELSE NULL END
			WHERE	EMAIL_REQ_ID = @EMAIL_REQ_ID
	END TRY

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
END