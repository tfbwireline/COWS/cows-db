USE [COWS]
GO
_CreateObject 'SP','web','updtMNSRevwr'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================================================            
-- Author:  jrg7298            
-- Create date: 05/10/2019      
-- Description: This is used to handle the Network Only MDS Event Least count mechanism
-- ssandov3 - 02/15/2021 - Added notes on Event History for MNSPM assignment
-- ==========================================================================================            
ALTER PROCEDURE [web].[updtMNSRevwr] --1,1,1
 @EVENT_ID  INT,
 @WRKFLW_STUS_ID TINYINT 
AS            
BEGIN            
SET NOCOUNT ON;
BEGIN TRY

IF EXISTS (SELECT 'X' FROM dbo.MDS_EVENT WITH (NOLOCK) WHERE EVENT_ID=@EVENT_ID AND LEN(ISNULL(MNS_PM_ID,''))>0 AND @WRKFLW_STUS_ID=2)
RETURN;

DECLARE @RvwrTbl TABLE ([USER_ID] INT, [ADID] VARCHAR(10), EVENT_QTY SMALLINT)
DECLARE @RvwrADID VARCHAR(10), @EventQty SMALLINT

INSERT INTO @RvwrTbl (ADID)
select DISTINCT StringID
from web.ParseCommaSeparatedStrings(
(select PRMTR_VALU_TXT
from dbo.LK_SYS_CFG with (nolock)
where PRMTR_NME='MDSNWOnlyReviewers')) as x INNER JOIN
dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ADID=x.StringID AND lu.REC_STUS_ID=1 INNER JOIN
dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON ugr.[USER_ID]=lu.[USER_ID] AND ugr.GRP_ID=2 AND ugr.[ROLE_ID]=22 AND ugr.REC_STUS_ID=1
UNION
select DISTINCT StringID
from web.ParseCommaSeparatedStrings(
(select PRMTR_VALU_TXT
from dbo.LK_SYS_CFG with (nolock)
where PRMTR_NME='MDSNWOnlyReviewers')) as x INNER JOIN
dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ADID=x.StringID AND lu.REC_STUS_ID=1 INNER JOIN
dbo.MAP_USR_PRF mup WITH (NOLOCK) ON mup.[USER_ID]=lu.[USER_ID] AND mup.USR_PRF_ID=132 AND mup.REC_STUS_ID=1

--Assign least count-1 or 0 for first time reviewers
UPDATE lu
SET EVENT_QTY = (SELECT TOP 1 CASE WHEN (lu2.EVENT_QTY > 0) THEN (lu2.EVENT_QTY-1) ELSE 0 END FROM dbo.LK_USER lu2 WITH (NOLOCK) WHERE lu2.EVENT_QTY IS NOT NULL ORDER BY lu2.EVENT_QTY ASC)
FROM dbo.LK_USER lu INNER JOIN
@RvwrTbl rt ON lu.[USER_ADID]=rt.[ADID]
WHERE lu.EVENT_QTY IS NULL
OR (NOT EXISTS (SELECT 'X' FROM dbo.MDS_EVENT me WITH (NOLOCK) INNER JOIN dbo.MDS_EVENT_NTWK_ACTY mena ON mena.EVENT_ID=me.EVENT_ID WHERE mena.NTWK_ACTY_TYPE_ID IN (2,3) AND lu.[USER_ADID]=me.MNS_PM_ID AND NOT EXISTS (SELECT 'x' FROM dbo.MDS_EVENT_NTWK_ACTY mena2 WITH (NOLOCK) WHERE mena2.EVENT_ID=me.EVENT_ID AND mena2.NTWK_ACTY_TYPE_ID = 1)))

UPDATE rt
SET EVENT_QTY = lu.EVENT_QTY,
[USER_ID] = lu.[USER_ID]
FROM @RvwrTbl rt INNER JOIN
dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ADID]=rt.[ADID]

IF (@WRKFLW_STUS_ID = 2) --Assign Reviewer for Submitted Events
BEGIN
	--Pick Reviewer with least count
	--SELECT TOP 1 @RvwrADID = ADID, @EventQty = EVENT_QTY FROM @RvwrTbl WHERE EVENT_QTY IS NOT NULL ORDER BY EVENT_QTY ASC
	--Pickup reviewer using round-robin
	DECLARE @AssnRvwrTbl TABLE ([ADID] VARCHAR(10))
	DECLARE @RvwCnt SMALLINT
	SELECT @RvwCnt = COUNT(1) FROM @RvwrTbl
	DECLARE @x nVARCHAR(MAX) = 'SELECT TOP ' +convert(varchar,@RvwCnt)+ ' me.MNS_PM_ID
	FROM dbo.MDS_EVENT me WITH (NOLOCK) INNER JOIN
	dbo.EVENT_HIST eh WITH (NOLOCK) ON eh.ACTN_ID=1 AND eh.EVENT_ID=me.EVENT_ID INNER JOIN
	dbo.MDS_EVENT_NTWK_ACTY mena WITH (NOLOCK) ON mena.EVENT_ID=me.EVENT_ID AND mena.NTWK_ACTY_TYPE_ID IN (2,3)
	WHERE NOT EXISTS (SELECT ''x'' FROM dbo.MDS_EVENT_NTWK_ACTY mena2 WITH (NOLOCK) WHERE me.EVENT_ID=mena2.EVENT_ID AND mena2.NTWK_ACTY_TYPE_ID=1)
	 AND me.EVENT_STUS_ID != 8
	 AND ISNULL(me.MNS_PM_ID,'''') <> ''''
	ORDER BY eh.CREAT_DT DESC'
	--Take top n(matching count of eligible reviewers) from mds_event table
	INSERT INTO @AssnRvwrTbl
	exec sp_executesql @x
	
	--if distinct n of already assigned reviewers in @AssnRvwrTbl table  then pick the next avail rev. based on adid alphabetical order
	SELECT top 1 @RvwrADID = ADID
	FROM @RvwrTbl 
	WHERE ADID NOT IN (SELECT [ADID] FROM @AssnRvwrTbl)
	  AND ISNULL(@RvwrADID,'')=''
	ORDER BY EVENT_QTY ASC

	--Pick Reviewer with least count
	SELECT TOP 1 @RvwrADID = ADID FROM @RvwrTbl WHERE EVENT_QTY IS NOT NULL AND ISNULL(@RvwrADID,'')='' ORDER BY EVENT_QTY ASC

	IF (LEN(ISNULL(@RvwrADID,'')) <= 3)
	BEGIN
		SELECT top 1 @RvwrADID = ADID
		FROM @RvwrTbl
		ORDER BY ADID ASC
	END

	UPDATE dbo.MDS_EVENT
	SET MNS_PM_ID = @RvwrADID
	WHERE EVENT_ID = @EVENT_ID
	
	UPDATE dbo.LK_USER
	SET EVENT_QTY = EVENT_QTY + 1
	WHERE USER_ADID=@RvwrADID

	-- Add Event History for the MNS PM Round Robin Assignment
	DECLARE @mnsPmName VARCHAR(MAX) = ''
	SELECT @mnsPmName = CASE WHEN ISNULL(FULL_NME, '') <> '' THEN FULL_NME ELSE @RvwrADID END
	FROM LK_USER WITH (NOLOCK) WHERE USER_ADID = @RvwrADID
	INSERT INTO dbo.EVENT_HIST (EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_DT, CREAT_BY_USER_ID)
	VALUES (@EVENT_ID, 20, 'MNSPM is assigned to ' + @mnsPmName, GETDATE(), 1)
END
ELSE IF (@WRKFLW_STUS_ID IN (7,11)) --Decrease Count for Deleted/Completed Events
BEGIN
	UPDATE lu
	SET EVENT_QTY = EVENT_QTY - 1
	FROM dbo.LK_USER lu INNER JOIN
	dbo.MDS_EVENT me WITH (NOLOCK) ON me.[MNS_PM_ID]=lu.[USER_ADID]
	WHERE me.EVENT_ID = @EVENT_ID
END

END TRY            
            
BEGIN CATCH            
 EXEC [dbo].[insertErrorInfo]            
END CATCH            
END