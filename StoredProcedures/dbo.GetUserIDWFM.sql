USE [COWS]
GO
_CreateObject 'SP','dbo','GetUserIDWFM'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO	
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
ALTER PROCEDURE dbo.GetUserIDWFM
(
	@UserIDs	varchar(max),
	@GrpID		tinyint,
	@User_id	Int out

)
AS
BEGIN
DECLARE @UsersCount Table (User_id int, Cnt int)
DECLARE @Ctr INT 	
INSERT INTO @UsersCount(User_id,Cnt)
SELECT	StringId,0 FROM [web].[ParseStringWithDelimiter](@UserIDs, '|')

DECLARE @UID INT
DECLARE @Cnt INT
DECLARE @Count INT
DECLARE @NCI BIT = 0

IF @GrpID = 5 OR @GrpID = 6 OR @GrpID = 7
	SET @NCI = 1

SELECT @Cnt = COUNT(1) FROM @UsersCount
SET @Ctr = 0

WHILE (@Ctr < @Cnt)
	BEGIN
		SET @UID = 0
		
		SELECT TOP 1 @UID = USER_ID
			FROM @UsersCount 
			WHERE Cnt = 0
		
			IF @NCI = 1
				BEGIN
					SELECT 	@Count = SUM( lot.XNCI_WFM_ORDR_WEIGHTAGE_NBR)
						From	dbo.USER_WFM_ASMT u WITH (NOLOCK)
							Inner Join	dbo.ORDR odr WITH (NOLOCK) ON odr.ORDR_ID = u.ORDR_ID
							Inner Join	dbo.LK_PPRT lp WITH (NOLOCK) ON lp.PPRT_ID = odr.PPRT_ID
							Inner Join	dbo.LK_ORDR_TYPE lot WITH (NOLOCK) ON lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
								WHERE u.ASN_USER_ID = @UID
									AND u.GRP_ID IN (5,6,7)
									AND EXISTS 
										(
											SELECT 'X'
												FROM dbo.ACT_TASK at WITH (NOLOCK) 
											Inner Join dbo.MAP_GRP_TASK mgt WITH (NOLOCK) ON at.TASK_ID = mgt.Task_ID
												WHERE	mgt.Grp_ID IN (5,6,7)
													AND		at.ORDR_ID = u.ORDR_ID
													AND		at.STUS_ID = 0
										)				
				END
			ELSE
				BEGIN
					SELECT 	@Count = SUM( CASE lot.ORDR_TYPE_DES 
								WHEN 'Disconnect' THEN .5
								ELSE 1
								END)
						From	dbo.USER_WFM_ASMT u WITH (NOLOCK)
							Inner Join	dbo.ORDR odr WITH (NOLOCK) ON odr.ORDR_ID = u.ORDR_ID
							Inner Join	dbo.LK_PPRT lp WITH (NOLOCK) ON lp.PPRT_ID = odr.PPRT_ID
							Inner Join	dbo.LK_ORDR_TYPE lot WITH (NOLOCK) ON lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
								WHERE u.ASN_USER_ID = @UID
									AND u.GRP_ID = @GrpID
									AND EXISTS 
										(
											SELECT 'X'
												FROM dbo.ACT_TASK at WITH (NOLOCK) 
											Inner Join dbo.MAP_GRP_TASK mgt WITH (NOLOCK) ON at.TASK_ID = mgt.Task_ID
												WHERE	mgt.Grp_ID = @GrpID
													AND		at.ORDR_ID = u.ORDR_ID
													AND		at.STUS_ID = 0
										)		
				END
			
			Update @UsersCount 
				SET Cnt = @Count
				Where User_id = @UID

			Set @Ctr = @Ctr + 1
				
	END

	SELECT Top 1 @User_id = USER_ID
		FROM @UsersCount 
		ORDER BY Cnt 

	--Select uc.User_id,lu.USER_ORDR_CNT,uc.Cnt 
	--	FROM	@UsersCount uc 
	--Inner Join	dbo.lk_user lu with (nolock) on uc.User_id = lu.USER_ID

END
GO

