USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertSSTATAsmt]    Script Date: 11/15/2021 10:16:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 03/22/2017
-- Description:	loads SSTAT info for user assignment.  Runs daily from job.
-- =========================================================
ALTER PROCEDURE [dbo].[insertSSTATAsmt]

AS
BEGIN
SET NOCOUNT ON;

Begin Try
	TRUNCATE TABLE LK_SSTAT_ASMT
	
	INSERT INTO dbo.LK_SSTAT_ASMT(Zip_Code
									,CLLI_CODE
									,FMS_ID
									,AD_ID
									,FullName
									,Email
									,Phone
									,COWS_PDL
									,VENDOR_COMPANY
									,DLVY_CLLI)
	
	SELECT Zip_Code,CLLI_CODE,FMS_ID,AD_ID,FullName,Email,Phone,COWS_PDL,VENDOR_COMPANY,EQ_DELIVERY_CLLI 
		FROM OPENQUERY (SSTAT_WFM, 'SELECT DISTINCT Zip_Code
												,CLLI_CODE
												,FMS_ID
												,AD_ID
												,FullName
												,Email,Phone
												,COWS_PDL
												,VENDOR_COMPANY 
												,EQ_DELIVERY_CLLI
												FROM dbo.vZipCodeCLLIReference')
 
	
End Try
Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END