USE [COWS]
GO
_CreateObject 'SP','web','retrieveCustomerByH1'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <5/1/2011>
-- Description:	<This Stored Procedure is used to get Customer information from L2P interface including CSG Levels>
-- =============================================
ALTER PROCEDURE [web].[retrieveCustomerByH1] --'925173427'
@CIS_ID AS VARCHAR(50)
AS
BEGIN

BEGIN TRY

	IF OBJECT_ID(N'tempdb..#CUST_INFO_SENSITIVE_ALL', N'U') IS NOT NULL         
		DROP TABLE #CUST_INFO_SENSITIVE_ALL 
	
	CREATE TABLE #CUST_INFO_SENSITIVE_ALL
									(
										[CUST_CHARS_ID]			VARCHAR(20) NULL,
										[SCTY_GRP_CD]			Varchar(5)	NULL,
										[RLAT_KEY_ID]			Varchar(9)  NULL
									) 

	DECLARE @CMD2 AS NVARCHAR(MAX)

		SET @CMD2 = 'INSERT INTO #CUST_INFO_SENSITIVE_ALL ' +
		'([CUST_CHARS_ID],[SCTY_GRP_CD],[RLAT_KEY_ID]) ' +
		'SELECT CHARS_CUST_ID,CSG_LVL,CIS_CUST_ID ' +
		'FROM OPENQUERY ' +
		'(M5, ' +
		'''select DISTINCT CHARS_CUST_ID,CSG_LVL,CIS_CUST_ID ' +
		'from mach5.v_v5u_cust_acct S ' +
		'WHERE ' +
				'((S.CIS_HIER_LVL_CD=''''H1'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY''''))
				  OR  (S.CIS_HIER_LVL_CD=''''H4'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY''''))
				  OR  (S.CIS_HIER_LVL_CD=''''H6'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))' +
				'AND S.CIS_CUST_ID = ''''' + CONVERT(VARCHAR, @CIS_ID) + ''''' '') '
		
		EXECUTE sp_executesql @CMD2
		
		
		SELECT DISTINCT
						CASE WHEN LEN(S.CUST_CHARS_ID) > 0 THEN  NULL ELSE S.CUST_CHARS_ID END AS CUST_CHARS_ID, 
						--S.CUST_CHARS_ID,
						S.RLAT_KEY_ID	AS RLAT_KEY_ID,
						S.SCTY_GRP_CD	AS SCTY_GRP_CD
		FROM	#CUST_INFO_SENSITIVE_ALL S
		WHERE SCTY_GRP_CD IS NOT NULL
		ORDER BY SCTY_GRP_CD ASC
										
END TRY
		
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH	

END