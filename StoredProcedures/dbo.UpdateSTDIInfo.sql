USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[UpdateSTDIInfo]    Script Date: 03/16/2017 11:32:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <06/19/2013>
-- Description:	<to update STDI info with customer commit date>
-- =============================================
ALTER PROCEDURE [dbo].[UpdateSTDIInfo] 
	@ORDR_ID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @ORDR_CAT_ID INT
	DECLARE @CUST_CMMT_DT DATETIME
	SELECT @ORDR_CAT_ID = ORDR_CAT_ID
		FROM	dbo.ORDR WITH (NOLOCK)
		WHERE	ORDR_ID = @ORDR_ID
		
	IF @ORDR_CAT_ID = 1
		BEGIN
			SELECT	@CUST_CMMT_DT = CUST_CMMT_DT
				FROM	dbo.IPL_ORDR WITH (NOLOCK)
				WHERE	ORDR_ID = @ORDR_ID
					
		END
	ELSE IF @ORDR_CAT_ID IN (2,6)
		BEGIN
			SELECT @CUST_CMMT_DT = CUST_CMMT_DT
				FROM dbo.FSA_ORDR WITH (NOLOCK)
				WHERE	ORDR_ID = @ORDR_ID 
					AND ORDR_ACTN_ID != 1
		END
	ELSE IF @ORDR_CAT_ID = 4
		BEGIN
			SELECT @CUST_CMMT_DT = CCS_DT
				FROM dbo.NCCO_ORDR WITH (NOLOCK)
				WHERE ORDR_ID = @ORDR_ID
		END
    
    IF @CUST_CMMT_DT IS NOT NULL AND ISNULL(@ORDR_ID,0) <> 0 
		BEGIN
			IF NOT EXISTS
				(
					SELECT 'X'
						FROM dbo.TRPT_ORDR WITH (NOLOCK)
						WHERE ORDR_ID = @ORDR_ID
				)
				BEGIN
					INSERT INTO dbo.TRPT_ORDR (ORDR_ID,REC_STUS_ID,STDI_DT,STDI_REAS_ID,CREAT_BY_USER_ID,CREAT_DT)
					VALUES (@ORDR_ID,1,@CUST_CMMT_DT,1,1,GETDATE())
				END
			ELSE
				BEGIN
					UPDATE dbo.TRPT_ORDR WITH (ROWLOCK) SET STDI_DT = @CUST_CMMT_DT, STDI_REAS_ID = 1 WHERE ORDR_ID = @ORDR_ID
				END
			
			IF NOT EXISTS
				(
					SELECT 'X'
						FROM	dbo.ORDR_STDI_HIST WITH (NOLOCK)
						WHERE	ORDR_ID = @ORDR_ID 
							AND STDI_REAS_ID = 1
							AND STDI_DT = @CUST_CMMT_DT
				)
				BEGIN
					INSERT INTO dbo.ORDR_STDI_HIST (ORDR_ID,STDI_REAS_ID,CREAT_BY_USER_ID,CREAT_DT,STDI_DT)
					VALUES (@ORDR_ID,1,1,GETDATE(),@CUST_CMMT_DT)
				END
		END
		
    
    
END
