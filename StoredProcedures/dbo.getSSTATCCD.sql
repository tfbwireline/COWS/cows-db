USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getSSTATRequests]    Script Date: 09/21/2017 13:43:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		dlp0278
-- Create date: 10/2/2015
-- Description:	Get the details for all STTAT CCD
-- =========================================================
create PROCEDURE [dbo].[getSSTATCCD]
	 @TRAN_ID int 
AS
BEGIN
SET NOCOUNT ON;
Begin Try

	Declare @ORDR_ID int
	
	Select @ORDR_ID = ORDR_ID from dbo.SSTAT_REQ with (nolock) where TRAN_ID = @TRAN_ID
	
	SELECT DISTINCT TRAN_ID	
					,f.FTN
					,DEVICE_ID   
					,ISNULL(ord.CUST_CMMT_DT,f.CUST_CMMT_DT)  AS [CCD]
					
		FROM		dbo.SSTAT_REQ sr	WITH (NOLOCK)
			INNER JOIN ORDR ord WITH (NOLOCK) on sr.ORDR_ID = ord.ORDR_ID
			INNER JOIN FSA_ORDR f WITH (NOLOCK) ON ord.ORDR_ID = f.ORDR_ID
			INNER JOIN (SELECT DISTINCT ORDR_ID, DEVICE_ID from FSA_ORDR_CPE_LINE_ITEM 
							where ORDR_ID = @ORDR_ID)itm ON itm.ORDR_ID = sr.ORDR_ID
		WHERE sr.TRAN_ID = @TRAN_ID
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

