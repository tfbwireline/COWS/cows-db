USE [COWS]
GO
_CreateObject 'SP','dbo','CheckPendingDeviceReturn'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Kyle Wichert
-- Create date: 07/24/2012
-- Description:	14bvi:  COWS will systematically change the event 
--				status from �Pending Device Return� to �Complete� 
--				when the event has been in a status of �Pending 
--				Device Return� for more than 21 days IF the event 
--				has not been manually set to �Complete� by the MNS 
--				Activator. 
--				07/26/2012 - Set status to Cancelled
-- ================================================================

ALTER PROCEDURE [dbo].[CheckPendingDeviceReturn]
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		BEGIN TRANSACTION

		DECLARE	@Events	TABLE
		(
			eventID	int
		)

		INSERT INTO		@Events
		SELECT			ud.EVENT_ID
		FROM			dbo.FEDLINE_EVENT_USER_DATA		ud WITH (NOLOCK)
			INNER JOIN	dbo.FEDLINE_EVENT_TADPOLE_DATA	td WITH (NOLOCK)	ON	ud.EVENT_ID		=	td.EVENT_ID
															AND	td.REC_STUS_ID	=	1
				WHERE	DATEADD(DAY,21,td.CREAT_DT) < GETDATE()
					AND	ud.EVENT_STUS_ID	=	16


		UPDATE			ud
		SET				EVENT_STUS_ID	=	13
		FROM			@Events							e
			INNER JOIN	dbo.FEDLINE_EVENT_USER_DATA		ud	WITH (ROWLOCK) ON	e.eventID	=	ud.EVENT_ID


		INSERT INTO EVENT_HIST	(EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, PRE_CFG_CMPLT_CD)
		SELECT	e.eventID, 50, 'Event in Pending Device Return status for 21 days, status updated to Cancelled.', 1, GETDATE(), 'N'
		FROM			@Events	e

		COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION
		EXEC dbo.insertErrorInfo
	END CATCH
	
END
