USE [COWS]
GO
_CreateObject 'SP','dbo','CompleteBillActivationTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <10/25/2011>
-- Description:	<Auto Completing IPL Bill Activation Ready task >
-- Alter date:	<12/08/2011>
-- Description: Rename SP, add in FSA Change orders
-- =============================================
ALTER PROCEDURE [dbo].[CompleteBillActivationTask]
@ORDR_ID	INT,
@TaskId		SMALLINT,
@TaskStatus	TINYINT = 0,
@Comments	Varchar(max) = ''	
AS
BEGIN
BEGIN TRY
DECLARE @NTE VARCHAR(max) = ''

	IF EXISTS
		(
			SELECT 'X'
				FROM	dbo.ORDR odr WITH (NOLOCK)
			LEFT JOIN	dbo.LK_PPRT lp WITH (NOLOCK) ON lp.PPRT_ID = odr.PPRT_ID
				WHERE	odr.ORDR_ID		=	@ORDR_ID
					AND	(
							(odr.ORDR_CAT_ID	IN	(1,5))
							OR
							((odr.ORDR_CAT_ID	=	4) AND (ISNULL(lp.ORDR_TYPE_ID,0)	= 8))
						 )
						
		)
		BEGIN
			IF ((SELECT COUNT(1) FROM dbo.ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID AND ORDR_CAT_ID = 1) > 0)
				SET @NTE = 'Auto Completing IPL Bill Activation Ready task.'
			ELSE IF ((SELECT COUNT(1) FROM dbo.ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID AND ORDR_CAT_ID = 5) > 0)
				SET @NTE = 'Auto Completing DPL Bill Activation Ready task.'
			ELSE 
				SET @NTE = 'Auto Completing NCCO Bill Activation Ready task.'
			Exec dbo.CompleteActiveTask @ORDR_ID,@TaskId,2,@NTE
			RETURN
		END


	--Issue identified: MDS Review reloading BAR pending task when it's bill activated (INTL Transport Orders)
	IF EXISTS
		(
			SELECT	'X'
				FROM	dbo.ACT_TASK WITH (NOLOCK)
				WHERE	ORDR_ID =	@ORDR_ID
					AND	TASK_ID	=	1001
		)
		BEGIN
			UPDATE dbo.ACT_TASK	WITH (ROWLOCK) SET STUS_ID = 2, MODFD_DT = GETDATE() WHERE ORDR_ID = @ORDR_ID AND TASK_ID = 1000
			RETURN
		END


	IF EXISTS
		(
			SELECT 'X'
				FROM	dbo.ORDR o WITH (NOLOCK) 
			INNER JOIN	dbo.LK_PPRT lp WITH (NOLOCK) ON lp.PPRT_ID = o.PPRT_ID 
				WHERE	lp.ORDR_TYPE_ID	=	8
					AND o.ORDR_ID = @ORDR_ID
		)
		BEGIN
			Exec dbo.CompleteActiveTask @ORDR_ID,1000,2,'Auto Completing Cancel Bill Activation Ready Task.'
			-- Carrier Ethernet NID IP Release on CPE Cancels, if there was a NID assignment done on parent CPE CE order
			DECLARE @PREV_IP_ACTY_ID INT = -1, @IP_MSTR_ID INT = -1,  @PREV_NID_ACTY_ID INT = -1, @NID_SERIAL_NBR VARCHAR(100), @NID_HOST_NME VARCHAR(100), @FTN VARCHAR(50), @PRNT_FTN VARCHAR(50), @LINE_ITEM_ID INT, 
			@H6 VARCHAR(9), @NID_ACTY_ID INT, @IP_ADR VARCHAR(15), @PRNT_ORDR_ID INT, @INSTL_EVENT_ID INT = -1
			SELECT TOP 1 @PREV_IP_ACTY_ID = ia.IP_ACTY_ID, @IP_MSTR_ID = ia.IP_MSTR_ID, @IP_ADR = im.IP_ADR, @PREV_NID_ACTY_ID = na.NID_ACTY_ID, @NID_SERIAL_NBR = na.NID_SERIAL_NBR,
			@NID_HOST_NME = na.NID_HOST_NME, @FTN = fo.FTN, @PRNT_FTN = fo.PRNT_FTN, @PRNT_ORDR_ID = fo2.ORDR_ID, @LINE_ITEM_ID = fcl.FSA_CPE_LINE_ITEM_ID, @H6 = ord.H5_H6_CUST_ID, @INSTL_EVENT_ID = na.EVENT_ID
			FROM dbo.NID_ACTY na WITH (NOLOCK) INNER JOIN
			dbo.IP_ACTY ia WITH (NOLOCK) ON ia.NID_ACTY_ID=na.NID_ACTY_ID INNER JOIN
			dbo.IP_MSTR im WITH (NOLOCK) ON im.IP_MSTR_ID=ia.IP_MSTR_ID INNER JOIN
			dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.PRNT_FTN=na.M5_ORDR_NBR INNER JOIN
			dbo.FSA_ORDR fo2 WITH (NOLOCK) ON fo.PRNT_FTN=fo2.FTN INNER JOIN
			dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID=ord.ORDR_ID INNER JOIN
			dbo.FSA_ORDR_CPE_LINE_ITEM fcl ON fcl.ORDR_ID=fo.ORDR_ID AND fcl.CMPNT_FMLY='NID' AND fcl.SPRINT_MNTD_FLG='Y'
			WHERE ord.ORDR_ID=@ORDR_ID
			 AND na.REC_STUS_ID=251
			 AND ia.REC_STUS_ID=251

			IF (@PREV_NID_ACTY_ID > -1)
			BEGIN
				INSERT INTO dbo.NID_ACTY (NID_SERIAL_NBR, NID_HOST_NME, FSA_CPE_LINE_ITEM_ID, M5_ORDR_NBR, H6, REC_STUS_ID, CREAT_BY_USER_ID, CREAT_DT)
				SELECT @NID_SERIAL_NBR, @NID_HOST_NME, @LINE_ITEM_ID, @FTN, @H6, 251, 1, GETDATE()

				SET @NID_ACTY_ID = SCOPE_IDENTITY()

				INSERT INTO dbo.IP_ACTY (IP_MSTR_ID, NID_ACTY_ID, REC_STUS_ID, CREAT_BY_USER_ID, CREAT_DT)
				SELECT @IP_MSTR_ID, @NID_ACTY_ID, 251, 1, GETDATE()

				UPDATE dbo.IP_MSTR SET REC_STUS_ID=254, MODFD_BY_USER_ID=1, MODFD_DT=GETDATE() WHERE IP_MSTR_ID = @IP_MSTR_ID
				UPDATE dbo.NID_ACTY SET REC_STUS_ID=252, MODFD_BY_USER_ID=1, MODFD_DT=GETDATE() WHERE NID_ACTY_ID=@PREV_NID_ACTY_ID
				UPDATE dbo.IP_ACTY SET REC_STUS_ID=252, MODFD_BY_USER_ID=1, MODFD_DT=GETDATE() WHERE IP_ACTY_ID=@PREV_IP_ACTY_ID
				SET @NTE = 'NID IP Address : '+ @IP_ADR + ' assigned to NID S/N : ' + @NID_SERIAL_NBR + ' under Parent FTN - '+@PRNT_FTN+' has been moved to 90day hold because of the Cancel Order - '+@FTN
				exec dbo.insertOrderNotes @PRNT_ORDR_ID,9,@NTE,1
				
				IF (@INSTL_EVENT_ID>-1)
					BEGIN
						INSERT INTO dbo.EVENT_HIST (
							EVENT_ID
							,ACTN_ID
							,CMNT_TXT
							,CREAT_BY_USER_ID
							,CREAT_DT
							)
						VALUES (
							@INSTL_EVENT_ID
							,20
							,@NTE
							,1
							,GETDATE()
							)
					END
			END
		END
		--512390 FSA wants a completion message.
	--IF EXISTS
	--	(
	--		SELECT 'X'
	--			FROM	dbo.FSA_ORDR WITH (NOLOCK)
	--			WHERE	ORDR_ID			=	@ORDR_ID
	--				AND	ORDR_TYPE_CD	=	'CH'
	--				AND PROD_TYPE_CD	IN	('DO','SO','MO')
	--	)
	--	BEGIN
	--		--Previous Task Rule sets the status to 3 unless there is a completed event.  Set the status to 0 so CompleteTask will work.
	--		Update dbo.ACT_TASK Set STUS_ID = 0 Where ORDR_ID = @ORDR_ID AND TASK_ID = 1000
	--		Exec dbo.CompleteActiveTask @ORDR_ID,@TaskId,2,'Auto Completing Change Order Bill Activation Ready task.'
	--	END

END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH	
    
END
