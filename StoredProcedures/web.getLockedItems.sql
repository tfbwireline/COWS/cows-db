USE COWS
GO
_CreateObject 'SP','web','getLockedItems'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [web].[getLockedItems]   
 @FTN Varchar(20) = '',
 @EVENTID varchar(10) = '',  
 @REDID varchar(10) = '',
 @CPT Varchar(13) = ''   
AS  
BEGIN  
 SET NOCOUNT ON;  
  
 IF (@FTN != '')
 BEGIN
	 SELECT  
	  [t0].[ORDR_ID],  
	  [t1].[LOCK_BY_USER_ID],  
	  [t4].[FULL_NME],  
	  CONVERT(VARCHAR,t0.FTN) AS FTN,  
	  [t2].[PROD_TYPE_DES],  
	  [t3].[ORDR_TYPE_DES]  
	 FROM [dbo].[FSA_ORDR] AS [t0] WITH (NOLOCK)  
	  INNER JOIN [dbo].[ORDR_REC_LOCK] AS [t1] WITH (NOLOCK) ON [t0].[ORDR_ID] = [t1].[ORDR_ID]  
	  INNER JOIN [dbo].[LK_PROD_TYPE] AS [t2] WITH (NOLOCK) ON [t0].[PROD_TYPE_CD] = [t2].[FSA_PROD_TYPE_CD]  
	  INNER JOIN [dbo].[LK_ORDR_TYPE] AS [t3] WITH (NOLOCK) ON [t0].[ORDR_TYPE_CD] = [t3].[FSA_ORDR_TYPE_CD]  
	  INNER JOIN [dbo].[LK_USER] AS [t4] WITH (NOLOCK) ON [t1].[LOCK_BY_USER_ID] = [t4].[USER_ID]  
	 WHERE CONVERT(VARCHAR,t0.FTN) = @FTN  
	 UNION  
	 SELECT  
	  [t0].[ORDR_ID],  
	  [t1].[LOCK_BY_USER_ID],  
	  [t4].[FULL_NME],  
	  CONVERT(VARCHAR,t0.ORDR_ID) AS FTN,  
	  [t2].[PROD_TYPE_DES],  
	  [t3].[ORDR_TYPE_DES]  
	 FROM [dbo].[ORDR] AS [t0] WITH (NOLOCK)  
	  LEFT JOIN [dbo].[LK_PPRT] lp WITH (NOLOCK) ON [lp].[PPRT_ID] = [t0].[PPRT_ID]  
	  INNER JOIN [dbo].[ORDR_REC_LOCK] AS [t1] WITH (NOLOCK) ON [t0].[ORDR_ID] = [t1].[ORDR_ID]  
	  INNER JOIN [dbo].[LK_PROD_TYPE] AS [t2] WITH (NOLOCK)ON [lp].[PROD_TYPE_ID] = ([t2].[PROD_TYPE_ID])  
	  INNER JOIN [dbo].[LK_ORDR_TYPE] AS [t3] WITH (NOLOCK) ON [lp].[ORDR_TYPE_ID] = [t3].[ORDR_TYPE_ID]  
	  INNER JOIN [dbo].[LK_USER] AS [t4] WITH (NOLOCK) ON  [t1].[LOCK_BY_USER_ID] = [t4].[USER_ID]  
	 WHERE CONVERT(VARCHAR,t0.ORDR_ID) = @FTN  
 END
 
 IF(@EVENTID !='') 
 BEGIN
    SELECT 
		[t0].[EVENT_ID]  AS [ORDR_ID], 
		[t1].[LOCK_BY_USER_ID], 
		[t3].[FULL_NME],
		'' AS [FTN], 
		[t2].[EVENT_TYPE_NME] AS [PROD_TYPE_DES],
		'' AS [ORDR_TYPE_DES]  
    FROM [dbo].[EVENT] AS [t0] WITH (NOLOCK)  
		INNER JOIN [dbo].[EVENT_REC_LOCK] AS [t1] WITH (NOLOCK) ON [t0].[EVENT_ID] = [t1].[EVENT_ID]   
		INNER JOIN [dbo].[LK_EVENT_TYPE] AS [t2] WITH (NOLOCK) ON [t0].[EVENT_TYPE_ID] = [t2].[EVENT_TYPE_ID]  
		INNER JOIN [dbo].[LK_USER] AS [t3] WITH (NOLOCK) ON [t3].[USER_ID] = [t1].[LOCK_BY_USER_ID]  
    WHERE [t1].[EVENT_ID] = CAST(@EVENTID as int)
END 
IF(@REDID !='') 
 BEGIN    
    SELECT 
		 [t0].[REDSGN_ID] AS [ORDR_ID], 
		 [t1].[LOCK_BY_USER_ID], 
		 [t3].[FULL_NME], 
		 [t0].[REDSGN_NBR] AS [FTN],
		 [t2].[REDSGN_TYPE_NME] AS [PROD_TYPE_DES],
		 '' AS [ORDR_TYPE_DES] 
    FROM [dbo].[REDSGN] AS [t0] WITH (NOLOCK)  
		INNER JOIN [dbo].[REDSGN_REC_LOCK] AS [t1] WITH (NOLOCK) ON [t0].[REDSGN_ID] = [t1].[REDSGN_ID]   
		INNER JOIN [dbo].[LK_REDSGN_TYPE] AS [t2] WITH (NOLOCK) ON [t0].[REDSGN_TYPE_ID] = [t2].[REDSGN_TYPE_ID]  
		INNER JOIN [dbo].[LK_USER] AS [t3] WITH (NOLOCK) ON [t3].[USER_ID] = [t1].[LOCK_BY_USER_ID]  
    WHERE [t0].[REDSGN_NBR] = @REDID
 END  
 IF(@CPT !='') 
 BEGIN       
	 SELECT 
		  [t0].[CPT_ID] AS [ORDR_ID], ---// OrdEvID  
		  [t1].[LOCK_BY_USER_ID], ---// UserID  
		  [t2].[FULL_NME], ---// UserName 
		  [t0].[CPT_CUST_NBR] AS [FTN],  ---// FTN
		  [t0].[CUST_SHRT_NME] AS [PROD_TYPE_DES],---// ProdType  
		  '' AS [ORDR_TYPE_DES] --// OrderType 
	 FROM [dbo].[CPT] AS [t0] WITH (NOLOCK)  
		  INNER JOIN [dbo].[CPT_REC_LOCK] AS [t1] WITH (NOLOCK) ON [t0].[CPT_ID] = [t1].[CPT_ID]   
		  INNER JOIN [dbo].[LK_USER] AS [t2] WITH (NOLOCK) ON [t1].[LOCK_BY_USER_ID] = [t2].[USER_ID]  
	 WHERE CONVERT(VARCHAR,t0.CPT_CUST_NBR) = @CPT  
 END
 ELSE if(@FTN = '' and @EVENTID = '' and @REDID = '' and @CPT = '')
 BEGIN
 SELECT  
	  '' [ORDR_ID],  
	  '' [LOCK_BY_USER_ID],  
	  '' [FULL_NME],  
	  '' [FTN],  
	  '' [PROD_TYPE_DES],  
	  '' [ORDR_TYPE_DES]
 END
END 

 