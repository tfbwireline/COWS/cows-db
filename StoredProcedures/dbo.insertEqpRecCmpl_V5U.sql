USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertEqpRecCmpl_V5U]    Script Date: 01/23/2020 11:08:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	David Phillips
-- Create date: 7/8/2015
-- Description:	This SP insures all the received items have been sent to SCM and queues the items
--              that have not been sent previously.
-- =============================================
ALTER PROCEDURE [dbo].[insertEqpRecCmpl_V5U] 
	@ADID           VARCHAR(10) = null,
 	@ORDR_ID        INT = 0 
 	
AS
BEGIN TRY

	DECLARE @PREV_QTY  INT,
			@RCVD_QTY INT,
			@FSA_CPE_LINE_ITEM_ID INT
			

	DECLARE @IDS table (FSA_CPE_LINE_ITEM_ID INT, RCVD_QTY INT)
	
	INSERT INTO @IDS (FSA_CPE_LINE_ITEM_ID, RCVD_QTY) 
		 (SELECT FSA_CPE_LINE_ITEM_ID, RCVD_QTY FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
							WHERE ORDR_ID = @ORDR_ID) 
	
	WHILE EXISTS (SELECT * FROM @IDS)
		BEGIN
		  SELECT TOP 1 @FSA_CPE_LINE_ITEM_ID = FSA_CPE_LINE_ITEM_ID, @RCVD_QTY = RCVD_QTY
					FROM @IDS
		  SELECT @PREV_QTY = ISNULL(SUM(RCVD_QTY),0) FROM dbo.PS_RCPT_QUEUE WITH (NOLOCK)
				WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
				GROUP BY FSA_CPE_LINE_ITEM_ID
		
	      SET @PREV_QTY = ISNULL(@PREV_QTY,0)

		  IF @RCVD_QTY > @PREV_QTY 
			AND EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM 
						WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
							AND ISNULL(PRCH_ORDR_NBR,'')<> '')  
			BEGIN
				INSERT INTO [dbo].[PS_RCPT_QUEUE]
					   ([PRCH_ORDR_NBR],[ORDR_ID],[REQSTN_NBR],[FSA_CPE_LINE_ITEM_ID]
					   ,[EQPT_RCVD_BY_ADID],[RCVD_QTY],[PS_RCVD_STUS],[PS_SENT_DT]
					   ,[CREAT_DT],[RCVD_DT],[BATCH_SEQ])

					   (SELECT Distinct PRCH_ORDR_NBR
							   ,ORDR_ID
							   ,PLSFT_RQSTN_NBR
							   ,@FSA_CPE_LINE_ITEM_ID
							   ,@ADID
							   ,@RCVD_QTY - @PREV_QTY
							   ,NULL, NULL, GETDATE(), GETDATE(),NULL           
					   FROM dbo.FSA_ORDR_CPE_LINE_ITEM WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID)
					   
				
			END
			DELETE @IDS WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
			
		END

	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH



