USE [COWS]
GO
_CreateObject 'SP','dbo','insertSSTATResponse'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 10/05/2015
-- Description:	Inserts an SSTAT Response into the SSTAT_RSPN table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertSSTATResponse]
		@TRAN_ID			int
       ,@ORDR_ID			varchar(50)
       ,@DEVICE_ID			varchar(25)
       ,@ORDER_ACTION		varchar(50)
       ,@COMPLETION_DATE	varchar(100)
       ,@NOTE               varchar(500)
       ,@ERROR_MSG          varchar(500)

AS

BEGIN
SET NOCOUNT ON;


BEGIN TRY
		DECLARE @COMPLTNDT DATETIME
		IF (LEN(@COMPLETION_DATE) > 0)
			SET @COMPLTNDT = CONVERT(DATETIME,@COMPLETION_DATE)
		----------------------------------------------------------
		-- Load the SSTAT_RSPN Table with appropriate info.
		----------------------------------------------------------
		INSERT INTO [dbo].[SSTAT_RSPN]
		   ([TRAN_ID],[ORDR_ID],[DEVICE_ID],[ORDER_ACTION],[COMPLETION_DATE],[NOTE]
           ,[ERROR_MSG],[CREAT_DT],[ACK_CD],[ACT_CD])
		VALUES
           (@TRAN_ID,@ORDR_ID,@DEVICE_ID,@ORDER_ACTION,@COMPLTNDT,@NOTE
           ,@ERROR_MSG,GETDATE(),0,0)
						
		return 
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
	return 1
END CATCH
END
