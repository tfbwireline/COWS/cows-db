USE [COWS]
GO
_CreateObject 'SP','web','insertView'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--================================================================================================  
-- Author:  csb9923  
-- Create date: 4/11/2011  
-- Description: inserts the dynamic view  
-- sbg9814	09/09/2011	Changed this to dynamically extract the Views for Orders.
--================================================================================================  
ALTER PROCEDURE [web].[insertView]   
	@USERID INT,
	@VIEWNAME VARCHAR(20),
	@SITE_CNTNT_ID INT,
	@DFLTFLAG CHAR(1),
	@PBLCFLAG CHAR(1),
	@FLTRCOLOPR1 VARCHAR(10),
	@FLTRCOLOPR2 VARCHAR(10),
	@FLTRCOL1 INT,
	@FLTRCOL2 INT,
	@FLTRCOLTXT1VAL VARCHAR(50),
	@FLTRCOLTXT2VAL VARCHAR(50),
	@FLTRFLAG CHAR(1),
	@SORTBYCOL1 INT,
	@SORTBYCOL1ORDER CHAR(1),
	@SORTBYCOL2 INT,
	@SORTBYCOL2ORDER CHAR(1),
	@LOGICOPR VARCHAR(10),
	@sCOLIDS VARCHAR(500),
	@sPOS VARCHAR(500)
AS
BEGIN  
SET NOCOUNT ON;  

DECLARE @SITE_CNTNT_COL_VW_CD	CHAR(1)
DECLARE @DSPL_VW_ID				INT
DECLARE @ViewDesc				VARCHAR(200)
SET		@SITE_CNTNT_COL_VW_CD	=	'N'
SET		@ViewDesc				=	''	

BEGIN TRY  

	SELECT @DSPL_VW_ID = MAX(DSPL_VW_ID) + 1
	FROM dbo.DSPL_VW WITH (NOLOCK)

	IF NOT EXISTS (SELECT 'X' FROM [COWS].[dbo].[DSPL_VW] WHERE [USER_ID]=@USERID AND [DSPL_VW_NME]=@VIEWNAME AND [SITE_CNTNT_ID]=@SITE_CNTNT_ID)
	BEGIN
		INSERT INTO [COWS].[dbo].[DSPL_VW]
			   ([DSPL_VW_ID]
			   ,[USER_ID]
			   ,[DSPL_VW_NME]
			   ,[SITE_CNTNT_ID]
			   ,[SITE_CNTNT_COL_VW_CD]
			   ,[DSPL_VW_DES]
			   ,[DSPL_VW_WEB_ADR]
			   ,[DFLT_VW_CD]
			   ,[PBLC_VW_CD]
			   ,[FILTR_COL_1_OPR_ID]
			   ,[FILTR_COL_2_OPR_ID]
			   ,[FILTR_COL_1_ID]
			   ,[FILTR_COL_2_ID]
			   ,[FILTR_COL_1_VALU_TXT]
			   ,[FILTR_COL_2_VALU_TXT]
			   ,[FILTR_ON_CD]
			   ,[SORT_BY_COL_1_ID]
			   ,[SORT_BY_COL_2_ID]
			   ,[GRP_BY_COL_1_ID]
			   ,[GRP_BY_COL_2_ID]
			   ,[CREAT_BY_USER_ID]
			   ,[MODFD_BY_USER_ID]
			   ,[MODFD_DT]
			   ,[CREAT_DT]
			   ,[FILTR_LOGIC_OPR_ID]
				,[SORT_BY_COL_1_ASC_ORDR_CD]
			   ,[SORT_BY_COL_2_ASC_ORDR_CD])
		 VALUES
			   (@DSPL_VW_ID
			   ,@USERID
			   ,@VIEWNAME
			   ,@SITE_CNTNT_ID
			   ,@SITE_CNTNT_COL_VW_CD
			   ,@ViewDesc
			   ,null
			   ,@DFLTFLAG
			   ,@PBLCFLAG
			   ,@FLTRCOLOPR1
			   ,@FLTRCOLOPR2
			   ,@FLTRCOL1
			   ,@FLTRCOL2
			   ,@FLTRCOLTXT1VAL
			   ,@FLTRCOLTXT2VAL
			   ,@FLTRFLAG
			   ,@SORTBYCOL1
			   ,@SORTBYCOL2
			   ,null
			   ,null
			   ,@USERID
			   ,@USERID
			   ,GETDATE()
			   ,GETDATE()
			   ,@LOGICOPR
			   ,@SORTBYCOL1ORDER
			   ,@SORTBYCOL2ORDER)	
	 END

	IF @DFLTFLAG='Y'
	  BEGIN
		UPDATE [dbo].[DSPL_VW] WITH (ROWLOCK) SET DFLT_VW_CD='N' WHERE USER_ID=@USERID AND DSPL_VW_ID<>@DSPL_VW_ID
	  END
		
	DECLARE @DSPL_COL_IDS TABLE (COLID INT, IDENT INT IDENTITY(1,1))
	DECLARE @DSPL_COL_POS TABLE (COLPOS INT, IDENT INT IDENTITY(1,1))	

	DECLARE @Ctr INT
	DECLARE @Cnt INT
	DECLARE @DSPL_COL_ID INT
	DECLARE @DSPL_COL_PO INT
	SET @Ctr = 1

	INSERT INTO @DSPL_COL_IDS
		SELECT IntegerID FROM web.ParseCommaSeparatedIntegers(@sCOLIDS,',')

	INSERT INTO @DSPL_COL_POS
		SELECT IntegerID FROM web.ParseCommaSeparatedIntegers(@sPOS,',')	

	SET @Cnt = (SELECT COUNT(1) FROM @DSPL_COL_IDS)

	WHILE (@Cnt >= @Ctr)
		BEGIN
			SELECT TOP 1 @DSPL_COL_ID = COLID FROM @DSPL_COL_IDS WHERE IDENT = @Ctr
			SELECT TOP 1 @DSPL_COL_PO = COLPOS FROM @DSPL_COL_POS WHERE IDENT = @Ctr

			INSERT INTO [COWS].[dbo].[DSPL_VW_COL]
			   ([DSPL_VW_ID]
			   ,[DSPL_COL_ID]
			   ,[DSPL_POS_FROM_LEFT_NBR]
			   ,[CREAT_DT])
			VALUES
			   (@DSPL_VW_ID
			   ,@DSPL_COL_ID
			   ,@DSPL_COL_PO
			   ,GETDATE())

			SET @Ctr = @Ctr + 1
		END
END TRY  

BEGIN CATCH  
	EXEC [dbo].[insertErrorInfo]  
END CATCH  
  
END