USE [COWS]
GO
_CreateObject 'SP','dbo','InsertBPMRedesignTransaction'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================================================
-- Author:		<Sarah Sandoval>
-- Create date: <02/25/2021>
-- Description:	<SP to insert record on BPM_REDSGN_TXN table for every status update of BPM Redesign>
-- ======================================================================================================
ALTER PROCEDURE [dbo].[InsertBPMRedesignTransaction]
	@REDSGN_ID		Int
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY
		DECLARE @BPM_REDSGN_NBR VARCHAR(20) = '', @BPM_DATA_TXT VARCHAR(MAX) = ''
		SELECT @BPM_REDSGN_NBR = BPM_REDSGN_NBR FROM [dbo].[REDSGN] WITH (NOLOCK) WHERE REDSGN_ID = @REDSGN_ID
		
		IF (ISNULL(@BPM_REDSGN_NBR,'') != '')
			BEGIN
				SET @BPM_DATA_TXT = (SELECT
					(SELECT r.BPM_REDSGN_NBR AS PR_DD_NBR
							,r.REDSGN_NBR AS MNS_DD_NBR
							,r.H1_CD AS H1_ID
							,s.STUS_DES AS STUS_NME
							,rn.NOTES AS CMNT_TXT
							,rn.CRETD_DT AS CREATE_DT
							,r.SUBMIT_DT AS SUBMIT_DT
							,r.SLA_DT AS SLA_DT
							,r.EXPRTN_DT AS EXPIRY_DT
						FROM [dbo].[REDSGN] r WITH (NOLOCK)
						JOIN [dbo].[LK_STUS] s WITH (NOLOCK) ON r.STUS_ID = s.STUS_ID
						JOIN (SELECT TOP 1 * FROM REDSGN_NOTES WITH (NOLOCK) WHERE REDSGN_ID = @REDSGN_ID AND REDSGN_NTE_TYPE_ID = 2 ORDER BY CRETD_DT DESC) AS rn ON r.REDSGN_ID = rn.REDSGN_ID
						WHERE r.REDSGN_ID = @REDSGN_ID
						FOR XML PATH('BPMInfo'),Type)
					FOR XML PATH(''), ROOT('BPMData') )
				
				INSERT INTO [dbo].[BPM_REDSN_TXN] (REDSGN_ID, BPM_DATA_TXT, REDSGN_NOTES_ID, REC_STUS_ID, CREAT_BY_USER_ID, CREAT_DT)
				SELECT TOP 1 @REDSGN_ID, @BPM_DATA_TXT, REDSGN_NOTES_ID, 263, 1, GETDATE()
				FROM REDSGN_NOTES WITH (NOLOCK) WHERE REDSGN_ID = @REDSGN_ID AND REDSGN_NTE_TYPE_ID = 2 ORDER BY CRETD_DT DESC
			END
	END TRY

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
END