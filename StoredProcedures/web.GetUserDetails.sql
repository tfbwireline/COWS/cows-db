USE COWS
GO
_CreateObject 'SP','web','GetUserDetails'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jagannath Gangi
-- Create date: 04-20-2011
-- Description:	This will store into the Cache for the user Full name and Email
-- =============================================

ALTER PROCEDURE [web].[GetUserDetails]
@sUserName	VARCHAR(10) =	NULL
AS
BEGIN
	
	BEGIN TRY
	
		SELECT ISNULL(FULL_NME,'') AS FullName,
			   ISNULL(USER_ADID,'') AS UserName,
			   ISNULL(EMAIL_ADR,'') AS EMail
		FROM dbo.LK_USER WITH(NOLOCK)
		WHERE USER_ADID = ISNULL(@sUserName,USER_ADID)
		
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH

END

GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
