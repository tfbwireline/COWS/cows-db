﻿USE [COWS]
GO
_CreateObject 'SP','dbo','addVendorEmailAttachments'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 16th Oct 2011
-- Description:	Adds email attachments to Vendor Order
-- ================================================================

ALTER PROCEDURE [dbo].[addVendorEmailAttachments]
	@EmailID INT,
	@H5DocumentIDs VARCHAR(3000),
	@FormIDs VARCHAR(3000),
	@UserID INT
AS
BEGIN

BEGIN TRY	
	INSERT INTO [COWS].[dbo].[VNDR_ORDR_EMAIL_ATCHMT]
           ([VNDR_ORDR_EMAIL_ID]
           ,[FILE_NME]
           ,[FILE_CNTNT]
           ,[FILE_SIZE_QTY]
           ,[CREAT_DT]
           ,[CREAT_BY_USER_ID]
           ,[REC_STUS_ID])
     SELECT
			@EmailID
           ,FILE_NME
           ,FILE_CNTNT
           ,FILE_SIZE_QTY
           ,GETDATE()
           ,@UserID
           ,1
	 FROM	dbo.H5_DOC WITH (NOLOCK)
	 WHERE	H5_DOC_ID IN (SELECT IntegerID FROM web.ParseCommaSeparatedIntegers(@H5DocumentIDs,','))
	 UNION
	 SELECT
			@EmailID
           ,FILE_NME
           ,FILE_CNTNT
           ,FILE_SIZE_QTY
           ,GETDATE()
           ,@UserID
           ,1
	 FROM	dbo.VNDR_ORDR_FORM WITH (NOLOCK)
	 WHERE	VNDR_ORDR_FORM_ID IN (SELECT IntegerID FROM web.ParseCommaSeparatedIntegers(@FormIDs,','))
END TRY
BEGIN Catch
       exec  [dbo].[insertErrorInfo]
END Catch
 
END