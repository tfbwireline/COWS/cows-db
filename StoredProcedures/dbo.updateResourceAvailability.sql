USE [COWS]
GO
_CreateObject 'SP','dbo','updateResourceAvailability'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <10/26/2013>
-- Description:	<To update resource availability>
-- =============================================
ALTER PROCEDURE dbo.updateResourceAvailability
	@updResultset	varchar(max),
	@modUserID	int
AS
BEGIN
	BEGIN TRY
	
		DECLARE @csv Table (updResult varchar(100),ResourceAvailID Int, AllowedFTEvents Int)
		Insert into @csv (updResult,ResourceAvailID,AllowedFTEvents)
		SELECT StringID,0,0 FROM [web].[ParseCommaSeparatedStrings](@updResultset)
	
		Update @csv
			Set ResourceAvailID = SUBSTRING(updResult,0,CHARINDEX('|',updResult)),
				AllowedFTEvents = SUBSTRING(updResult,CHARINDEX('|',updResult) + 1,LEN(updResult))
		
		Update e
			Set		e.MAX_AVAL_TME_SLOT_CAP_CNT = csv.AllowedFTEvents,
					e.MODFD_BY_USER_ID	=	@modUserID
			FROM	dbo.EVENT_TYPE_RSRC_AVLBLTY e
		Inner Join	@csv csv ON csv.ResourceAvailID = e.EVENT_TYPE_RSRC_AVLBLTY_ID		
		
		
	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
END
GO
