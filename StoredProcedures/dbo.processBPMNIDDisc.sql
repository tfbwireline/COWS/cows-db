USE [COWS]
GO
_CreateObject 'SP','dbo','processBPMNIDDisc'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		jrg7298
-- Create date: 06/01/2020
-- Description:	Nightly job to process all NID IP disconnects from BPM view based on customer want date
-- =========================================================
ALTER PROCEDURE [dbo].[processBPMNIDDisc]
AS
BEGIN
SET NOCOUNT ON;
Begin Try

DECLARE @BPM_COWS_NID TABLE
	
		(
			ID				INT IDENTITY(1,1) NOT NULL,
			NID_DETL_ID		INT,
			H6_ID			Varchar(9),
			ORDR_ID			Int,
			CUST_WANT_DT    Date,
			NID_SERIAL_NBR  varchar(100),
			NID_IP			varchar(15),
			FLAG_CD			BIT DEFAULT 0
		)
	DECLARE @Cnt Int = 0
	DECLARE @Ctr Int = 1
	DECLARE @NID_SERIAL_NBR VARCHAR(100), @NID_IP VARCHAR(15), @CUST_WNT_DT DATE, @FTN VARCHAR(50), @ORDR_ID INT, @H6 CHAR(9),@NID_DETL_ID INT
	DECLARE @now Varchar(10)
	SET @now = CONVERT(VARCHAR,CONVERT(DATE,GETDATE()))
	DECLARE @CPE_DEV_ID Varchar(50)
	DECLARE @IsCCDEXPDOrder int = 0
	DECLARE @IsRTSHold Bit = 0
	
	DECLARE @SQL nVarchar(max)
	SET @SQL = ''
	
	SET @SQL = 'SELECT NSF_COWS_NID_DETL_ID, H6_ID,ORDR_ID,CUST_WANT_DT,NID_SERIAL_NBR,IP_ADR
					FROM OPENQUERY (NRMBPM,		
				''SELECT DISTINCT NSF_COWS_NID_DETL_ID, H6_ID,ORDR_ID,CUST_WANT_DT,NID_SERIAL_NBR,IP_ADR
					FROM BPMF_OWNER.NSF_COWS_NID_DETL  
					WHERE	(XTRCT_CD = ''''N'''')
						AND ((XTRCT_DT IS NULL) OR (to_char(XTRCT_DT,''''yyyy-mm-dd'''')  <=  ''''' + @now +'''''))
						AND (to_char(CUST_WANT_DT,''''yyyy-mm-dd'''')  <=  ''''' + @now +''''')
					ORDER BY ORDR_ID 
					'') '	
	INSERT INTO @BPM_COWS_NID (NID_DETL_ID, H6_ID,ORDR_ID,CUST_WANT_DT,NID_SERIAL_NBR,NID_IP)	
	EXEC sp_executesql @SQL
	--select * from @BPM_COWS_NID
	SELECT @Cnt = COUNT(1) FROM @BPM_COWS_NID
	WHILE (@Ctr<=@Cnt)
	BEGIN
		SELECT TOP 1 @NID_DETL_ID = NID_DETL_ID, @ORDR_ID = ORDR_ID,
		@NID_IP=NID_IP,
		@NID_SERIAL_NBR=NID_SERIAL_NBR,
		@CUST_WNT_DT = CUST_WANT_DT
		FROM @BPM_COWS_NID
		WHERE ID=@Ctr
		 AND FLAG_CD=0
		 --select @NID_IP, @NID_SERIAL_NBR, @NID_DETL_ID, @ORDR_ID
	declare @x nvarchar(max) = 'select @ftn =order_nbr from openquery(m5, ''select order_nbr from mach5.v_v5u_ordr where ordr_id='+ convert(varchar,@ORDR_ID) + ' '')'
	select @x
	exec sp_executesql @x, N'@ftn varchar(50) OUTPUT', @ftn=@ftn output


-- Carrier Ethernet NID IP Release on CPE Cancels, if there was a NID assignment done on parent CPE CE order
			DECLARE @PREV_IP_ACTY_ID INT = -1, @IP_MSTR_ID INT = -1,  @PREV_NID_ACTY_ID INT = -1, @NID_ACTY_ID INT, @IP_ADR VARCHAR(15), @COWS_ORDR_ID INT=-1, @PRNT_ORDR_ID INT=-1, @NTE VARCHAR(1000), @COWS_NID_SN VARCHAR(100), @PRNT_FTN VARCHAR(50), @INSTL_EVENT_ID INT = -1
			
			SELECT TOP 1 @PREV_IP_ACTY_ID = ia.IP_ACTY_ID, @IP_MSTR_ID = ia.IP_MSTR_ID, @IP_ADR = im.IP_ADR, @PREV_NID_ACTY_ID = na.NID_ACTY_ID, @PRNT_ORDR_ID = fo.ORDR_ID, @COWS_NID_SN = na.NID_SERIAL_NBR, @PRNT_FTN=na.M5_ORDR_NBR, @COWS_ORDR_ID = fo2.ORDR_ID, @INSTL_EVENT_ID = na.EVENT_ID
			FROM dbo.NID_ACTY na WITH (NOLOCK) INNER JOIN
			dbo.IP_ACTY ia WITH (NOLOCK) ON ia.NID_ACTY_ID=na.NID_ACTY_ID INNER JOIN
			dbo.IP_MSTR im WITH (NOLOCK) ON im.IP_MSTR_ID=ia.IP_MSTR_ID INNER JOIN
			dbo.FSA_ORDR_CPE_LINE_ITEM fcl WITH (NOLOCK) ON na.FSA_CPE_LINE_ITEM_ID=fcl.FSA_CPE_LINE_ITEM_ID INNER JOIN
			dbo.FSA_ORDR_CPE_LINE_ITEM fcl2 WITH (NOLOCK) ON fcl2.DEVICE_ID=fcl.DEVICE_ID AND fcl2.CMPNT_FMLY='NID' AND fcl2.ITM_STUS=401 INNER JOIN
			dbo.FSA_ORDR fo WITH (NOLOCK) ON fcl.ORDR_ID=fo.ORDR_ID AND na.M5_ORDR_NBR=fo.FTN INNER JOIN
			dbo.FSA_ORDR fo2 WITH (NOLOCK) ON fcl2.ORDR_ID=fo2.ORDR_ID AND fo2.FTN=@FTN
			WHERE (im.IP_ADR = @NID_IP)
			 AND na.REC_STUS_ID=251
			 AND ia.REC_STUS_ID=251
			 AND im.REC_STUS_ID=253

			IF (@PREV_NID_ACTY_ID > -1)
			BEGIN
				--SELECT @PREV_IP_ACTY_ID, @PREV_NID_ACTY_ID, @PRNT_FTN, @PRNT_ORDR_ID, @NID_IP, @NID_SERIAL_NBR, @FTN, @H6, 251, 1, GETDATE()
				INSERT INTO dbo.NID_ACTY (NID_SERIAL_NBR, M5_ORDR_NBR, H6, REC_STUS_ID, CREAT_BY_USER_ID, CREAT_DT)
				SELECT @NID_SERIAL_NBR, @ftn, @H6, 251, 1, GETDATE()

				SET @NID_ACTY_ID = SCOPE_IDENTITY()

				INSERT INTO dbo.IP_ACTY (IP_MSTR_ID, NID_ACTY_ID, REC_STUS_ID, CREAT_BY_USER_ID, CREAT_DT)
				SELECT @IP_MSTR_ID, @NID_ACTY_ID, 251, 1, GETDATE()

				UPDATE dbo.IP_MSTR SET REC_STUS_ID=254, MODFD_DT=GETDATE(), MODFD_BY_USER_ID=1 WHERE IP_MSTR_ID = @IP_MSTR_ID
				UPDATE dbo.NID_ACTY SET REC_STUS_ID=252, MODFD_DT=GETDATE(), MODFD_BY_USER_ID=1 WHERE NID_ACTY_ID=@PREV_NID_ACTY_ID
				UPDATE dbo.IP_ACTY SET REC_STUS_ID=252, MODFD_DT=GETDATE(), MODFD_BY_USER_ID=1 WHERE IP_ACTY_ID=@PREV_IP_ACTY_ID
				SET @NTE = 'NID IP Address : '+ @IP_ADR + ' assigned to BPM NID S/N : ' + @NID_SERIAL_NBR + ', COWS NID S/N : ' + @COWS_NID_SN + ' under FTN - '+@PRNT_FTN+' has been moved to 90day hold because of the BPM disconnect Order - '+@FTN
				exec dbo.insertOrderNotes @PRNT_ORDR_ID,9,@NTE,1
				IF (@INSTL_EVENT_ID>-1)
				BEGIN
					INSERT INTO dbo.EVENT_HIST (
						EVENT_ID
						,ACTN_ID
						,CMNT_TXT
						,CREAT_BY_USER_ID
						,CREAT_DT
						)
					VALUES (
						@INSTL_EVENT_ID
						,20
						,@NTE
						,1
						,GETDATE()
						)
				END
				if (@COWS_ORDR_ID>-1)
					exec dbo.insertOrderNotes @COWS_ORDR_ID,9,@NTE,1
				SET @SQL = 'UPDATE OPENQUERY (nrmbpm, ''SELECT XTRCT_CD, XTRCT_DT FROM BPMF_OWNER.NSF_COWS_NID_DETL WHERE NSF_COWS_NID_DETL_ID ='+ convert(varchar,@NID_DETL_ID) + ' '') SET XTRCT_CD = ''Y'', XTRCT_DT='''+@now+''' ';
				exec sp_executesql @SQL
			END
			ELSE
			BEGIN
				SET @SQL = 'UPDATE OPENQUERY (nrmbpm, ''SELECT XTRCT_CD, XTRCT_DT FROM BPMF_OWNER.NSF_COWS_NID_DETL WHERE NSF_COWS_NID_DETL_ID ='+ convert(varchar,@NID_DETL_ID) + ' '') SET XTRCT_CD = ''X'', XTRCT_DT='''+@now+''' ';
				exec sp_executesql @SQL
			END

			UPDATE @BPM_COWS_NID SET FLAG_CD=1 WHERE FLAG_CD=0 AND ID=@Ctr
			SET @Ctr=@Ctr+1
	END

	--Release all 90day hold IPs back into the Active Pool
	UPDATE dbo.IP_MSTR SET REC_STUS_ID=251, MODFD_DT=GETDATE(), MODFD_BY_USER_ID=1 WHERE datediff(dd,MODFD_DT,getdate())>90 AND REC_STUS_ID=254
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END