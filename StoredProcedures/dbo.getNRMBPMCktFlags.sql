USE [COWS]
GO
_CreateObject 'SP','dbo','getNRMBPMCktFlags'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		jrg7298
-- Create date: 03/25/2018
-- Description:	Gets the Circuit Flags from NRMBPM interface using H6s.
-- =========================================================
ALTER PROCEDURE [dbo].[getNRMBPMCktFlags] --'928631597,928386119'
	@H6s VARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	DECLARE @H6List VARCHAR(MAX)

	IF (LEN(@H6s)>0)
	BEGIN
				select @H6List = ISNULL(@H6List,'') + ISNULL(StringID,'') + ', '
				from web.ParseCommaSeparatedStrings(@H6s)
				if(len(@H6List)>2)
					set  @H6List = substring(@H6List, 1, len(@H6List)-1)
		
		declare @cktinfo nvarchar(max) = 'select *
					from openquery(NRMBPM,''select DISTINCT H6_ID, OPT_IN_H_CD, OPT_IN_CKT_CD, READY_BEGIN_FLG_NME
											from BPMF_OWNER.V_BPMF_MONTR_COWS_CKT
											WHERE H6_ID IN (''''' + @H6List + ''''') '')'
		exec sp_executesql @cktinfo
	END

End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END
