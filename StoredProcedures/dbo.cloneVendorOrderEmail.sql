USE [COWS]
GO
_CreateObject 'SP','dbo','cloneVendorOrderEmail'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 2nd Sep 2011
-- Description:	Creates Vendor Order Email from an existing one
-- kh946640: Capturing [VNDR_EMAIL_TYPE_ID] column while inserting into [VNDR_ORDR_EMAIL] table.
-- ================================================================

ALTER PROCEDURE [dbo].[cloneVendorOrderEmail]
	@VendorOrderEmailID INT, 
	@AttachmentsExcluded VARCHAR(3000),
	@UserID INT
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY

BEGIN TRANSACTION
DECLARE @VendorOrderID INT
DECLARE @EmailID INT

SET @EmailID = 0

-- Get Vendor Order 
SELECT
	@VendorOrderID = VNDR_ORDR_ID
FROM dbo.VNDR_ORDR_EMAIL	WITH (NOLOCK)
WHERE VNDR_ORDR_EMAIL_ID = @VendorOrderEmailID


-- Create new Vendor email from Vendor Order
INSERT INTO [dbo].[VNDR_ORDR_EMAIL]
           ([VNDR_ORDR_ID]
           ,[VER_NBR]
           ,[FROM_EMAIL_ADR]
           ,[TO_EMAIL_ADR]
           ,[EMAIL_SUBJ_TXT]
           ,[EMAIL_BODY]
           ,[CC_EMAIL_ADR]
           ,[EMAIL_STUS_ID]
           ,[CREAT_DT]
           ,[CREAT_BY_USER_ID]
           ,[REC_STUS_ID]
		   ,[VNDR_EMAIL_TYPE_ID]) 
SELECT VNDR_ORDR_ID
      ,ISNULL((SELECT MAX(VOE.VER_NBR) FROM dbo.VNDR_ORDR_EMAIL VOE WITH (NOLOCK) WHERE VOE.VNDR_ORDR_ID = @VendorOrderID),0) + 1
      ,FROM_EMAIL_ADR
      ,TO_EMAIL_ADR
      ,EMAIL_SUBJ_TXT
      ,EMAIL_BODY
      ,CC_EMAIL_ADR
      ,13
      ,GETDATE()
      ,@UserID
      ,1
	  ,VNDR_EMAIL_TYPE_ID
FROM dbo.VNDR_ORDR_EMAIL WITH (NOLOCK)
WHERE VNDR_ORDR_EMAIL_ID = @VendorOrderEmailID
SET @EmailID = SCOPE_IDENTITY()

IF @@ERROR = 0
BEGIN	
		-- Add attachments for the Vendor email
		INSERT INTO [dbo].[VNDR_ORDR_EMAIL_ATCHMT]
				   ([VNDR_ORDR_EMAIL_ID]
				   ,[FILE_NME]
				   ,[FILE_CNTNT]
				   ,[FILE_SIZE_QTY]
				   ,[CREAT_DT]
				   ,[CREAT_BY_USER_ID]
				   ,[REC_STUS_ID])
		SELECT 
			@EmailID,
			FILE_NME,
			FILE_CNTNT,
			FILE_SIZE_QTY,
			GETDATE(),
			CREAT_BY_USER_ID,
			1
		FROM dbo.VNDR_ORDR_EMAIL_ATCHMT WITH (NOLOCK)
		WHERE VNDR_ORDR_EMAIL_ID = @VendorOrderEmailID
		AND VNDR_ORDR_EMAIL_ATCHMT_ID NOT IN (SELECT * FROM web.ParseCommaSeparatedIntegers(@AttachmentsExcluded, ':'))
		ORDER BY VNDR_ORDR_EMAIL_ATCHMT_ID
	
		COMMIT TRANSACTION	
END
ELSE
	ROLLBACK TRANSACTION

SELECT @EmailID
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
