USE [COWS]
GO
_CreateObject 'SP','dbo','createRedesignReminderEmails'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <04/11/2016>
-- Description:	<Create a reminder email for 
--	redesigns prior to 30 days of expiration >
-- Modification
--		07/27/2017 km967761 - Added 10-day expiration notice
-- =============================================
ALTER PROCEDURE [dbo].[createRedesignReminderEmails] -- exec [dbo].[createRedesignReminderEmails]
	
AS
BEGIN
	BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @ReminderRDSGNCancellationEmailsTable TABLE (REDSGN_ID INT,FLAG BIT)
		
		INSERT INTO @ReminderRDSGNCancellationEmailsTable
		SELECT r.REDSGN_ID,0
			FROM	dbo.REDSGN r WITH (NOLOCK)
			WHERE	CONVERT(DATE, r.EXPRTN_DT) BETWEEN CONVERT(DATE, GETDATE()+1) AND CONVERT(DATE, GETDATE()+30)
				AND r.STUS_ID	NOT IN (226,227,228)
				AND NOT EXISTS
					(
						SELECT 'X'
							FROM dbo.REDSGN_NOTES rn WITH (NOLOCK)
							WHERE r.REDSGN_ID = rn.REDSGN_ID
								AND rn.NOTES = '30 Day Expiration notice email sent.'
					)
			
	DECLARE @CTR INT = 0, @CNT INT = 0	, @REDSGN_ID INT
		SELECT @CNT = COUNT(1) 
			FROM @ReminderRDSGNCancellationEmailsTable
		
		WHILE (@CTR < @CNT)
			BEGIN
				SELECT @REDSGN_ID = REDSGN_ID
					FROM @ReminderRDSGNCancellationEmailsTable
					WHERE FLAG = 0
				
				INSERT INTO dbo.REDSGN_NOTES (REDSGN_ID,REDSGN_NTE_TYPE_ID,NOTES,CRETD_BY_CD,CRETD_DT)
				VALUES (@REDSGN_ID, 1, '30 Day Expiration notice email sent.', 1, GETDATE()) 
				
				EXEC dbo.insertRedesignEmailData @REDSGN_ID, 229, 0 		
					
				UPDATE @ReminderRDSGNCancellationEmailsTable SET FLAG = 1 WHERE REDSGN_ID = @REDSGN_ID AND FLAG = 0
				SET @CTR = @CTR + 1
			END	
			
		--SELECT r.REDSGN_NBR 
		--	FROM @MoveToCancelStatus t
		--	inner join dbo.REDSGN r on t.REDSGN_ID = r.REDSGN_ID

	-- ADDITIONAL 10-DAY EXPIRATION NOTICE
	DECLARE @ReminderRDSGNCancellationEmailsTable2 TABLE (REDSGN_ID INT,FLAG BIT)
		
		INSERT INTO @ReminderRDSGNCancellationEmailsTable2
		SELECT r.REDSGN_ID,0
			FROM	dbo.REDSGN r WITH (NOLOCK)
			WHERE	CONVERT(DATE, r.EXPRTN_DT) BETWEEN CONVERT(DATE, GETDATE()+1) AND CONVERT(DATE, GETDATE()+10)
				AND r.STUS_ID	NOT IN (226,227,228)
				AND NOT EXISTS
					(
						SELECT 'X'
							FROM dbo.REDSGN_NOTES rn WITH (NOLOCK)
							WHERE r.REDSGN_ID = rn.REDSGN_ID
								AND rn.NOTES = '10 Day Expiration notice email sent.'
					)
			
	DECLARE @CTR2 INT = 0, @CNT2 INT = 0	, @REDSGN_ID2 INT
		SELECT @CNT2 = COUNT(1) 
			FROM @ReminderRDSGNCancellationEmailsTable2
		
		WHILE (@CTR2 < @CNT2)
			BEGIN
				SELECT @REDSGN_ID2 = REDSGN_ID
					FROM @ReminderRDSGNCancellationEmailsTable2
					WHERE FLAG = 0
				
				INSERT INTO dbo.REDSGN_NOTES (REDSGN_ID,REDSGN_NTE_TYPE_ID,NOTES,CRETD_BY_CD,CRETD_DT)
				VALUES (@REDSGN_ID2, 1, '10 Day Expiration notice email sent.', 1, GETDATE()) 
				
				EXEC dbo.insertRedesignEmailData @REDSGN_ID2, 2291, 0 		
					
				UPDATE @ReminderRDSGNCancellationEmailsTable2 SET FLAG = 1 WHERE REDSGN_ID = @REDSGN_ID2 AND FLAG = 0
				SET @CTR2 = @CTR2 + 1
			END
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
		
END
