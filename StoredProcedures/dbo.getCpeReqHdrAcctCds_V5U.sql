USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getCpeReqHdrAcctCds_V5U]    Script Date: 09/23/2016 11:24:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 5/4/2015
	-- Description:	Appian CPE Acct Codes at Header Level.
	---- =============================================
	

 CREATE PROCEDURE [dbo].[getCpeReqHdrAcctCds_V5U] 
		@ORDR_ID INT 
	
	AS
	BEGIN

	
	SELECT TOP 1 
		 pid.BUS_UNT_GL                   AS BUS_UNT_GL
	    ,pid.COST_CNTR                    AS COST_CNTR
	    ,ISNULL(pid.MRKT,'')              AS MRKT
	    ,ISNULL(pid.REGN,'')              AS REGN
	    ,pid.BUS_UNT_PC                   AS BUS_UNT_PC
				 
			

		FROM      dbo.ORDR ord WITH (NOLOCK)
	  INNER JOIN  dbo.LK_CPE_PID pid WITH (NOLOCK)
					ON pid.DMSTC_CD = ord.DMSTC_CD
						AND pid.REC_STUS_ID = 1					
						

	
	WHERE  ord.ORDR_ID = @ORDR_ID
		
	END


