USE [COWS]
GO
_CreateObject 'SP','dbo','getFSAOrderID'
GO
-- =========================================================
-- Author:		sbg9814
-- Create date: 07/11/2011
-- Description:	Check the FTN and then get the OrderID for an FSAOrder.
-- =========================================================
ALTER PROCEDURE [dbo].[getFSAOrderID]
	@ORDR_ID		Int	OUTPUT
	,@ORDR_ACTN_ID	Int
	,@FTN			varchar(20)
	,@ORDR_CAT_ID	smallint
AS
BEGIN
SET NOCOUNT ON;
DECLARE	@OldOrderID	Int
SET	@OldOrderID	=	0

Begin Try
	IF @ORDR_CAT_ID	=	2
		BEGIN
			SELECT		@OldOrderID	=	ISNULL(ORDR_ID, 0)
				FROM	dbo.FSA_ORDR WITH (NOLOCK)
				WHERE	FTN				=	@FTN
					AND	ORDR_ACTN_ID	=	@ORDR_ACTN_ID

			IF	@ORDR_ACTN_ID	=	3
			AND	EXISTS	
				(SELECT 'X' FROM dbo.FSA_ORDR	WITH (NOLOCK)
					WHERE	FTN				=	@FTN
					AND		ORDR_ACTN_ID	=	2)
				BEGIN
					SELECT @ORDR_ID = -1
				END
			ELSE
				BEGIN
					IF	(@ORDR_ACTN_ID	=	1	OR	
						(@OldOrderID	=	0	AND	@ORDR_ACTN_ID !=	1))
						BEGIN
							-----------------------------------------------------------------------------
							--	Delete the old Pre-Submits for this Order.
							-----------------------------------------------------------------------------
							IF	@OldOrderID	!=	0	AND	@ORDR_ACTN_ID	=	1
								BEGIN					
									EXEC	dbo.deletePartialFSAOrder	@OldOrderID
								END
								
							-----------------------------------------------------------------------------
							--	Insert a new record for the FSA_ORDR.
							-----------------------------------------------------------------------------	
							INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID)	
								VALUES (@ORDR_CAT_ID, 1,	1)
							
							SELECT @ORDR_ID = SCOPE_IDENTITY()
							
							UPDATE		dbo.ORDR	WITH (ROWLOCK)
								SET		PRNT_ORDR_ID	=	@ORDR_ID
								WHERE	ORDR_ID			=	@ORDR_ID
						END
					ELSE
						BEGIN
							SELECT @ORDR_ID = 0
						END		
				END				
		END
	ELSE 
		BEGIN
			INSERT INTO dbo.ORDR WITH (ROWLOCK)	(ORDR_CAT_ID, CREAT_BY_USER_ID,	RGN_ID)	
				VALUES (@ORDR_CAT_ID, 1,	1)
			
			SELECT @ORDR_ID = SCOPE_IDENTITY()
			
			UPDATE		dbo.ORDR	WITH (ROWLOCK)
				SET		PRNT_ORDR_ID	=	@ORDR_ID
				WHERE	ORDR_ID			=	@ORDR_ID
		END
	
End Try
Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END