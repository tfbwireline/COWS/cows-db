USE [COWS]
GO
_CreateObject 'SP','dbo','WfmNCITaskRule'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO	

-- =============================================
-- Author:		David Phillips
-- Create date: 08/012/2011
-- Description:	This SP is used to assign xNIC users to orders
-- =============================================
-- Alter:		Kyle Wichert
-- Change Date: 01/15/2012
-- Description: When getting user name, added a join to make sure they have an active role for the group.
-- =============================================


ALTER  PROCEDURE [dbo].[WfmNCITaskRule]
	@OrderID INT, 
	@TaskId SMALLINT, 
	@TaskStatus TINYINT, 
	@Comments VARCHAR(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	SET DEADLOCK_PRIORITY 3

	DECLARE @User_id		INT
	DECLARE @Ordr_cat		SMALLINT  -- 1 = PLS, 2 = FSA, 3 = Vendor
    DECLARE @Ctr			TINYINT	-- counter
	DECLARE @Grp_id			TINYINT
	DECLARE @UsrPrfid		SMALLINT
	DECLARE @Prod_type_id	TINYINT
	DECLARE @Pltfrm_cd		VARCHAR(2)
	DECLARE @Ordr_actn_id   TINYINT
	DECLARE @Orgtng_cty_cd	VARCHAR(2)
	DECLARE @RelativeOrder  INT = 0
	DECLARE	@H5Country		Varchar(2)	=	''
	DECLARE	@Region			Int	=	1

	DECLARE @Ordr_sub_type_cd VARCHAR(7)
	DECLARE @UserName       VARCHAR(100)
	DECLARE @Note           VARCHAR(1000)
	DECLARE @Ordr_Weightage DECIMAL(5,2)  = 0.00
	DECLARE @H5_CUST_ID		Int

	SET @Ctr = 0

	
	BEGIN TRY
	
	--Check to see if order has already been assigned to someone in the NCI group.
	
		SELECT @Ctr = COUNT(ufa.ORDR_ID)
			FROM dbo.USER_WFM_ASMT ufa  WITH (NOLOCK)
			WHERE ufa.ORDR_ID = @OrderID
				AND ((ufa.GRP_ID in (5,6,7)) OR (ufa.USR_PRF_ID IN (14,28,112))) -- 5=Americas, 6=Asia, 7=Europe
	
		IF @Ctr = 0
			BEGIN
	
				SELECT  @Ordr_cat = ord.ORDR_CAT_ID   
					FROM	dbo.ORDR ord	WITH (NOLOCK)			
					WHERE	ord.ORDR_ID		=	@OrderID
			
				IF @Ordr_cat IN (2,6) -- FSA Order
					Begin
						Select @Prod_type_id	= pt.PROD_TYPE_ID
							   ,@Pltfrm_cd      = IsNull(o.PLTFRM_CD,'')
							   ,@Ordr_actn_id	= fo.ORDR_ACTN_ID
							   ,@Grp_id         = r.GRP_ID
							   ,@UsrPrfid		= mwp.WFM_PRF_ID
							   ,@Orgtng_cty_cd	= IsNull(oa.CTRY_CD,'')
							   ,@RelativeOrder	= IsNull(fr.ORDR_ID,0)	
							   ,@H5_CUST_ID		= ISNULL(h5.CUST_ID,0)
						FROM dbo.FSA_ORDR fo	 WITH (NOLOCK)
						JOIN dbo.ORDR o          WITH (NOLOCK)
							ON o.ORDR_ID = fo.ORDR_ID
						LEFT OUTER JOIN dbo.LK_XNCI_RGN r   WITH (NOLOCK)
							ON r.RGN_ID = o.RGN_ID
						LEFT JOIN dbo.MAP_WFM_PRF mwp WITH (NOLOCK) ON mwp.PRNT_PRF_ID=r.USR_PRF_ID
						JOIN dbo.LK_PROD_TYPE pt WITH (NOLOCK)
							ON pt.FSA_PROD_TYPE_CD = fo.PROD_TYPE_CD
						JOIN dbo.ORDR_ADR oa	 WITH (NOLOCK)
							ON oa.ORDR_ID = fo.ORDR_ID
						LEFT OUTER JOIN dbo.FSA_ORDR fr WITH (NOLOCK) on fr.FTN		=	fo.RELTD_FTN	
						LEFT JOIN dbo.H5_FOLDR h5 WITH (NOLOCK) ON h5.H5_FOLDR_ID = o.H5_FOLDR_ID
						WHERE @OrderID = fo.ORDR_ID
							AND oa.CIS_LVL_TYPE IN ('H5','H6')
							
					End
					
				ELSE 
				IF @Ordr_cat = 1 -- IPL Order
					Begin
						Select @Prod_type_id	= ip.PROD_TYPE_ID
							   ,@Ordr_actn_id	= 2
							   ,@Pltfrm_cd      = IsNull(o.PLTFRM_CD,'')
							   ,@Grp_id         = r.GRP_ID
							   ,@UsrPrfid		= mwp.WFM_PRF_ID
							   ,@Orgtng_cty_cd	= IsNull(oa.CTRY_CD,'')
						FROM dbo.IPL_ORDR ip	WITH (NOLOCK)
						JOIN dbo.ORDR o          WITH (NOLOCK)
							ON o.ORDR_ID = ip.ORDR_ID
						LEFT OUTER JOIN dbo.LK_XNCI_RGN r   WITH (NOLOCK)
							ON r.RGN_ID = o.RGN_ID	
						LEFT JOIN dbo.MAP_WFM_PRF mwp WITH (NOLOCK) ON mwp.PRNT_PRF_ID=r.USR_PRF_ID
						JOIN dbo.ORDR_ADR oa	 WITH (NOLOCK)
							ON oa.ORDR_ID = ip.ORDR_ID
						WHERE @OrderID = ip.ORDR_ID 
							AND oa.ADR_TYPE_ID = 12
						
						Set @Ordr_actn_id  = 2
						
						--If the IPL country is blank, use the H5 folder Country, and set the region.
						If @Orgtng_cty_cd	=	''
						Begin
							Select	@H5Country	=	CTRY_CD	
							From	dbo.ORDR	o WITH (NOLOCK)	
								INNER JOIN	dbo.H5_FOLDR	h5 WITH (NOLOCK)	ON	o.H5_FOLDR_ID	=	h5.H5_FOLDR_ID
							Where o.ORDR_ID	=	@OrderID
							
							Select	@Region	=	c.RGN_ID,
									@Grp_id =	r.GRP_ID,
									@UsrPrfid		= CASE WHEN r.USR_PRF_ID = 5 THEN 14 
														WHEN r.USR_PRF_ID = 6 THEN 28
														WHEN r.USR_PRF_ID = 7 THEN 112
														ELSE NULL
												  END
							From	dbo.LK_CTRY	c WITH (NOLOCK)	
								INNER JOIN	dbo.LK_XNCI_RGN	r WITH (NOLOCK)	ON	c.RGN_ID	=	r.RGN_ID
							Where	c.CTRY_CD	=	@H5Country
							
							IF @H5Country <> ''
							Begin
								Update dbo.ORDR_ADR WITH (ROWLOCK) Set	CTRY_CD = @H5Country Where ORDR_ID = @OrderID AND ADR_TYPE_ID = 12
								Update dbo.ORDR WITH (ROWLOCK) Set RGN_ID = @Region Where ORDR_ID = @OrderID
								Set @Orgtng_cty_cd = @H5Country
							End
						End
					End
				ELSE IF @Ordr_cat = 4 -- NCCO Order
					Begin
						Select @Prod_type_id	= ncco.PROD_TYPE_ID
							   ,@Ordr_actn_id	= 2
							   ,@Pltfrm_cd      = IsNull(o.PLTFRM_CD,'')
							   ,@Grp_id         = r.GRP_ID
							   ,@UsrPrfid		= CASE WHEN r.USR_PRF_ID = 5 THEN 14 
														WHEN r.USR_PRF_ID = 6 THEN 28
														WHEN r.USR_PRF_ID = 7 THEN 112
														ELSE NULL
												  END
							   ,@Orgtng_cty_cd	= IsNull(ncco.CTRY_CD,'')
						FROM dbo.NCCO_ORDR	ncco	WITH (NOLOCK)
						JOIN dbo.ORDR		o		WITH (NOLOCK)			ON o.ORDR_ID = ncco.ORDR_ID
						LEFT OUTER JOIN dbo.LK_XNCI_RGN r   WITH (NOLOCK)	ON r.RGN_ID = o.RGN_ID	
						WHERE @OrderID = ncco.ORDR_ID 
							
						Set @Ordr_actn_id  = 2
						
					End
				
				SELECT @Ordr_Weightage  = ISNULL(lot.XNCI_WFM_ORDR_WEIGHTAGE_NBR,0)
					FROM	dbo.ORDR odr WITH (NOLOCK)
				LEFT JOIN	dbo.LK_PPRT lp WITH (NOLOCK) ON lp.PPRT_ID = odr.PPRT_ID
				LEFT JOIN	dbo.LK_ORDR_TYPE lot WITH (NOLOCK) ON lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
					WHERE	odr.ORDR_ID = @OrderID	
				
				If @RelativeOrder > 0
					Begin
						SELECT TOP 1 @User_id	= uwfm.ASN_USER_ID
							FROM dbo.USER_WFM_ASMT uwfm	WITH (NOLOCK) INNER JOIN
								 dbo.LK_USER lu WITH (NOLOCK) ON uwfm.ASN_USER_ID = lu.[USER_ID]
							WHERE ((uwfm.GRP_ID = @Grp_id) OR (uwfm.USR_PRF_ID = @UsrPrfid))
				    			AND uwfm.ORDR_ID	= @RelativeOrder
								AND lu.REC_STUS_ID		= 1
					End
				
				-- IF (@Prod_type_id = 9 AND @Grp_id = 7 AND ISNULL(@User_id,0) = 0)
					-- BEGIN
						-- IF @H5_CUST_ID != 0
							-- BEGIN
								-- SELECT TOP 1 @User_id = USER_ID
									-- FROM	dbo.USER_H5_WFM WITH (NOLOCK)
									-- WHERE	CUST_ID = @H5_CUST_ID
										-- AND REC_STUS_ID = 1
							-- END
					-- END
				-- ELSE 
					-- BEGIN	
						IF @Orgtng_cty_cd = '' and ISNULL(@User_id,0) = 0
							Begin
								--SET @Orgtng_cty_cd = 0
								SET @GRP_ID = 5
								SELECT TOP 1 @User_id	= uwfm.USER_ID
									FROM dbo.USER_WFM uwfm	WITH (NOLOCK)
									LEFT JOIN dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON	uwfm.USER_ID = ugr.USER_ID	AND	uwfm.GRP_ID = ugr.GRP_ID
									LEFT JOIN dbo.MAP_USR_PRF mup WITH (NOLOCK) ON	uwfm.USER_ID = ugr.USER_ID
									JOIN dbo.LK_USER lu		WITH (NOLOCK)
										ON lu.USER_ID = uwfm.USER_ID
									WHERE ((uwfm.GRP_ID = 5) OR (uwfm.USR_PRF_ID = 14))					 -- Default to America
										AND uwfm.PROD_TYPE_ID	= @Prod_type_id
										AND IsNull(uwfm.PLTFRM_CD,'') = @Pltfrm_cd
										AND uwfm.ORDR_ACTN_ID	= @Ordr_actn_id
										AND IsNull(uwfm.ORGTNG_CTRY_CD, '') = @Orgtng_cty_cd -- Only countries not assigned region will have null.
										AND uwfm.REC_STUS_ID	= 1
										AND uwfm.WFM_ASMT_LVL_ID = 1         --Available for systematic assign.
										AND	((ugr.REC_STUS_ID		=	1) OR (mup.REC_STUS_ID=1))
										AND	ugr.GRP_ID			IN	(5,6,7)
										AND lu.REC_STUS_ID		= 1
									ORDER BY lu.USER_ORDR_CNT
							END
						ELSE
							BEGIN
								IF ISNULL(@User_id,0) = 0
									Begin
										SELECT TOP 1 @User_id	= uwfm.USER_ID
											FROM dbo.USER_WFM uwfm		WITH (NOLOCK)
											LEFT JOIN dbo.USER_GRP_ROLE ugr	WITH (NOLOCK) 
												ON	uwfm.USER_ID = ugr.USER_ID	AND	uwfm.GRP_ID = ugr.GRP_ID
											LEFT JOIN dbo.MAP_USR_PRF mup WITH (NOLOCK) ON	uwfm.USER_ID = ugr.USER_ID
											JOIN dbo.LK_USER lu			WITH (NOLOCK)
												ON lu.USER_ID = uwfm.USER_ID
											WHERE ((uwfm.GRP_ID = @Grp_id) OR (uwfm.USR_PRF_ID = @UsrPrfid))
												AND uwfm.PROD_TYPE_ID	= @Prod_type_id
												AND IsNull(uwfm.PLTFRM_CD,'') = @Pltfrm_cd
												AND uwfm.ORDR_ACTN_ID	= @Ordr_actn_id
												AND uwfm.ORGTNG_CTRY_CD = @Orgtng_cty_cd
												AND uwfm.REC_STUS_ID	= 1
												AND uwfm.WFM_ASMT_LVL_ID = 1
												AND	((ugr.REC_STUS_ID		=	1) OR (mup.REC_STUS_ID=1))
												AND	ugr.GRP_ID			IN	(5,6,7)
												AND lu.REC_STUS_ID		= 1
											ORDER BY lu.USER_ORDR_CNT
									End

								IF ISNULL(@User_id,0) = 0
									Begin	
										SELECT TOP 1 @User_id	= uwfm.USER_ID
											FROM dbo.USER_WFM uwfm	WITH (NOLOCK)
											LEFT JOIN dbo.USER_GRP_ROLE ugr	WITH (NOLOCK) 
												ON	uwfm.USER_ID = ugr.USER_ID	AND	uwfm.GRP_ID = ugr.GRP_ID
											LEFT JOIN dbo.MAP_USR_PRF mup WITH (NOLOCK) ON	uwfm.USER_ID = ugr.USER_ID
											JOIN dbo.LK_USER lu		WITH (NOLOCK)
												ON lu.USER_ID = uwfm.USER_ID
											WHERE ((uwfm.GRP_ID = @Grp_id) OR (uwfm.USR_PRF_ID = @UsrPrfid))
												AND uwfm.PROD_TYPE_ID	= @Prod_type_id
												AND IsNull(uwfm.PLTFRM_CD,'') = @Pltfrm_cd
												AND uwfm.ORDR_ACTN_ID	= @Ordr_actn_id

												AND uwfm.REC_STUS_ID	= 1
												AND uwfm.WFM_ASMT_LVL_ID = 1
												AND	((ugr.REC_STUS_ID		=	1) OR (mup.REC_STUS_ID=1))
												AND	ugr.GRP_ID			IN	(5,6,7)
												AND lu.REC_STUS_ID		= 1
												ORDER BY lu.USER_ORDR_CNT
									End					
							END
					-- END
					IF ISNULL(@User_id,0) <> 0
						Begin

							Select @UserName = FULL_NME from dbo.LK_USER WITH (NOLOCK) where USER_ID = @User_id
							Select @Note = 'Order auto-assigned to XNCI User ' + @UserName

							INSERT INTO dbo.ORDR_NTE	(NTE_TYPE_ID,	ORDR_ID,	CREAT_DT,	CREAT_BY_USER_ID,	REC_STUS_ID,	NTE_TXT)
							VALUES						(11,			@OrderID,	GETDATE(),	1,					1,				@Note)
							INSERT INTO dbo.USER_WFM_ASMT
									(ORDR_ID,GRP_ID,ASN_USER_ID,ASN_BY_USER_ID,ASMT_DT,CREAT_DT,ORDR_HIGHLIGHT_CD,ORDR_HIGHLIGHT_MODFD_DT,USR_PRF_ID)
								VALUES
									(@OrderID,@Grp_id,@User_id,1,GETDATE(),GETDATE(),1,GETDATE(),@UsrPrfid)
					
							UPDATE dbo.LK_USER WITH (ROWLOCK)
									SET USER_ORDR_CNT = ISNULL(User_Ordr_cnt,0) + @Ordr_Weightage
								WHERE [USER_ID] = @User_id
						END
						ELSE
						Begin
							INSERT INTO dbo.ORDR_NTE	(NTE_TYPE_ID,	ORDR_ID,	CREAT_DT,	CREAT_BY_USER_ID,	REC_STUS_ID,	NTE_TXT)
							VALUES						(11,			@OrderID,	GETDATE(),	1,					1,				'No user found for xNCI auto-assignment')
						End
						
			End
			

	END TRY

	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH
	SET NOCOUNT OFF
END
GO

