USE [COWS]
GO
_CreateObject 'SP','web','getWGData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:		sbg9814
-- Create date: 09/01/2011
-- Description:	Gets all  the data for the default page based of WGID and UserID.
-- csb9923 5/22/12 changed the sequence of city and country fields in IPL query for fixing IM1046293
-- dlp0278 10/29/12 changed the FSA SOI_CD to get H5/H6 record SOI field instead of H1.
-- kh946640 12/19/2018 updated the query to get distinch PO numbers from Line Items table.
-- ================================================================================
ALTER PROCEDURE [web].[getWGData] 
		@WGID		Int	 
	,@UserID	Int	= 0
	,@OrderID	Int = 0
	,@IsCmplt	Bit = 0
	,@CSGLvlId	TINYINT	= 0
AS
BEGIN
SET NOCOUNT ON;
DECLARE	@Order	TABLE
	(ORDR_ID	Int
	,TaskID		Int
	,StatusID	Int DEFAULT 0
	,GroupID	Int
	,IsCmplt	Bit DEFAULT 0)
	
DECLARE	@RGN_ID	Int
SET	@RGN_ID	=	1
Begin Try
	SELECT		@RGN_ID	=	ISNULL(RGN_ID, 1)
		FROM	dbo.LK_XNCI_RGN	WITH (NOLOCK)
		WHERE	GRP_ID	=	@WGID

IF @OrderID = 0
BEGIN 
	IF	@UserID	=	0
		BEGIN
			IF @IsCmplt = 0
				BEGIN
				IF @WGID = 13
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, StatusID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.STUS_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, at.STUS_ID, mt.GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.GRP_ID				=	@WGID
										AND		at.STUS_ID				IN	(0, 156)
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID			=	6
										--AND ISNULL(fo.CPE_VNDR,'')		<>	'Goodman'
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @WGID = 11
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.GRP_ID				=	@WGID
										AND		at.STUS_ID				=	0
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') in ('UCCH','UCSV','MIPT','UCSM')
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @WGID = 15
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON cli.ORDR_ID = fo.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.GRP_ID				=	@WGID
										AND		at.STUS_ID				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND		od.DMSTC_CD = 0
										AND		od.DMSTC_CD = 0 AND  od.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID = 6
										AND ISNULL(cli.DEVICE_ID,'') <> ''
										AND ISNULL(fo.CPE_VNDR,'') = 'Goodman'
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @WGID = 2
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.GRP_ID				=	@WGID
										AND		at.STUS_ID				=	0
										AND		ISNULL(od.RGN_ID, 1)	=	Case	
																				When	@WGID	IN (5, 6, 7)	Then 	@RGN_ID
																				Else	ISNULL(od.RGN_ID, 1)
																			End
										AND		od.DMSTC_CD				=	Case	
																				When	@WGID	IN (5, 6, 7)	Then 	1
																				Else	od.DMSTC_CD
																			End
										AND		(
													(od.ORDR_STUS_ID IN (0,1)) AND (@WGID NOT IN (5,6,7))
													OR
													mt.GRP_ID = 3
													OR
													(ISNULL(oms.CUST_ACPTC_TURNUP_DT,'') =	'' AND @WGID IN (5,6,7))
												)
									ORDER BY orderByDate DESC	
									
									UNION 
									
									SELECT DISTINCT Top 1000		ord.ORDR_ID,	602, 2, ISNULL(ord.MODFD_DT, ord.CREAT_DT) as orderByDate
									FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
									INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
									INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
									INNER JOIN   dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID

									WHERE  fo.PROD_TYPE_CD = 'CP'
										AND (act.TASK_ID IN (602) AND act.STUS_ID = 0)
										AND fo.ORDR_ID in (SELECT ORDR_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM
																WHERE litm.CNTRC_TYPE_ID  IN ('CUST','INSI'))
																order by 1 desc								
									
									UNION 
									
									SELECT DISTINCT Top 1000		ord.ORDR_ID,	602, 2, ISNULL(ord.MODFD_DT, ord.CREAT_DT) as orderByDate
									FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
									INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
									INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
									INNER JOIN   dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID

									WHERE  fo.PROD_TYPE_CD = 'CP'
										AND (act.TASK_ID IN (602) AND act.STUS_ID = 0)
										AND ISNULL(PRCH_ORDR_NBR,'') <> ''
										AND litm.ITM_STUS = 401						
							)	AS OB	
						END
				ELSE IF @WGID = 3
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.GRP_ID				=	@WGID
										AND		at.STUS_ID				=	0
										AND		ISNULL(od.RGN_ID, 1)	=	Case	
																				When	@WGID	IN (5, 6, 7)	Then 	@RGN_ID
																				Else	ISNULL(od.RGN_ID, 1)
																			End
										AND		(
													(od.ORDR_STUS_ID IN (0,1)) AND (@WGID NOT IN (5,6,7))
													OR
													mt.GRP_ID = 3
													OR
													(ISNULL(oms.CUST_ACPTC_TURNUP_DT,'') =	'' AND @WGID IN (5,6,7))
												)
									ORDER BY orderByDate DESC
								)	AS OB	
								--PRINT 'IN'
						END			
				ELSE
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.GRP_ID				=	@WGID
										AND		at.STUS_ID				=	0
										AND		ISNULL(od.RGN_ID, 1)	=	Case	
																				When	@WGID	IN (5, 6, 7)	Then 	@RGN_ID
																				Else	ISNULL(od.RGN_ID, 1)
																			End
										AND		od.DMSTC_CD				=	1
										AND		(
													(od.ORDR_STUS_ID IN (0,1)) AND (@WGID NOT IN (5,6,7))
													OR
													mt.GRP_ID = 3
													OR
													(ISNULL(oms.CUST_ACPTC_TURNUP_DT,'') =	'' AND @WGID IN (5,6,7))
												)
									ORDER BY orderByDate DESC
								)	AS OB	
								--PRINT 'IN'
						END
				END														
			ELSE
				BEGIN
					IF @WGID = 4
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	IsCmplt)
							SELECT OB.ORDR_ID, 1 FROM (
								SELECT DISTINCT Top 1000	od.ORDR_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
								FROM	dbo.ORDR	od	WITH (NOLOCK)
							INNER JOIN	dbo.ORDR_NTE		ont	WITH (NOLOCK) ON od.ORDR_ID = ont.ORDR_ID
								AND ont.NTE_TYPE_ID	=	14
								AND od.ORDR_STUS_ID	NOT IN (0,1)
								WHERE	@WGID =	4
								ORDER BY orderByDate DESC
							)	AS OB
						END
					ELSE IF @WGID IN (5,6,7)
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	IsCmplt)
							SELECT OB.ORDR_ID, 1 FROM (
								SELECT DISTINCT Top 1000	od.ORDR_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM	dbo.ORDR	od	WITH (NOLOCK)
							INNER JOIN 		dbo.ORDR_MS oms with (nolock) on oms.ORDR_ID = od.ORDR_ID 
									WHERE	ISNULL(oms.CUST_ACPTC_TURNUP_DT,'') <> ''
										AND @WGID		IN (5,6,7)	
										AND	od.DMSTC_CD	=	1
										AND	ISNULL(od.RGN_ID,1)		=	CASE
																			WHEN @WGID	= 5  THEN 1
																			WHEN @WGID  = 7  THEN 2 
																			WHEN @WGID  = 6  THEN 3
																			ELSE  ISNULL(od.RGN_ID,1)
																		END
								ORDER BY orderByDate DESC
							)	AS OB
						END
					ELSE IF @WGID = 13
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, StatusID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.STUS_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, at.STUS_ID, 13 as GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		
										at.STUS_ID				=	0 
										AND at.TASK_ID			= 1001
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID			=	6
									ORDER BY orderByDate DESC
								)	AS OB
						END
					ELSE IF @WGID = 11
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, 11 as GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		
										at.STUS_ID				=	0 
										AND	od.DMSTC_CD			=	0
										AND at.TASK_ID			= 1001
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') in ('UCCH','UCSV','MIPT','UCSM')
									ORDER BY orderByDate DESC
								)	AS OB
						END
					ELSE IF @WGID = 15
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, 15 as GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON cli.ORDR_ID = fo.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		at.STUS_ID				=	0 
										AND     at.TASK_ID				= 1001
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND		od.DMSTC_CD = 0 AND  od.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID = 6
										AND ISNULL(cli.DEVICE_ID,'') <> ''
										AND ISNULL(fo.CPE_VNDR,'') = 'Goodman'
									ORDER BY orderByDate DESC
								)	AS OB
						END
					ELSE
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	IsCmplt)
							SELECT OB.ORDR_ID, 1 FROM (
								SELECT DISTINCT Top 1000	od.ORDR_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
								FROM			dbo.ORDR			od	WITH (NOLOCK)
									LEFT JOIN	dbo.FSA_ORDR		f	WITH (NOLOCK) ON f.ORDR_ID		= od.ORDR_ID 	
									INNER JOIN	dbo.LK_PPRT			p	WITH (NOLOCK) ON od.PPRT_ID		=	p.PPRT_ID	 
									INNER JOIN	dbo.SM				s	WITH (NOLOCK) ON s.SM_ID		=	p.SM_ID
									INNER JOIN	dbo.WG_PTRN_SM		pt	WITH (NOLOCK) ON pt.WG_PTRN_ID	=	s.PRE_REQST_WG_PTRN_ID
									INNER JOIN	dbo.WG_PROF_SM		pf	WITH (NOLOCK) ON pf.WG_PROF_ID	=	pt.PRE_REQST_WG_PROF_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mgt	WITH (NOLOCK) ON mgt.TASK_ID	=	pf.PRE_REQST_TASK_ID
								WHERE	od.ORDR_STUS_ID	NOT IN (0,1)		
									AND (
											(@WGID 	=	1 AND f.ORDR_ACTN_ID = 3)
											OR 
											(mgt.TASK_ID NOT IN (100,101,109)
														AND	mgt.GRP_ID	=	@WGID)
										)
								ORDER BY orderByDate DESC
							)	AS OB
							--PRINT 'IN'
						END									
				END
				
		END
	ELSE
		BEGIN
					IF @WGID = 13
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, StatusID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.STUS_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, at.STUS_ID, mt.GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									LEFT JOIN	dbo.ORDR_NTE		nte	WITH (NOLOCK)	ON	od.ORDR_ID	=	nte.ORDR_ID
									WHERE		mt.GRP_ID				=	@WGID
										AND		at.STUS_ID				IN	(0, 156)
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID			=	6
										--AND ISNULL(fo.CPE_VNDR,'')		<>	'Goodman'
										AND		nte.CREAT_BY_USER_ID = @UserID
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @WGID = 11
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									LEFT JOIN	dbo.ORDR_NTE		nte	WITH (NOLOCK)	ON	od.ORDR_ID	=	nte.ORDR_ID
									WHERE		mt.GRP_ID				=	@WGID
										AND		at.STUS_ID				=	0
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') in ('UCCH','UCSV','MIPT','UCSM')
										AND		nte.CREAT_BY_USER_ID = @UserID
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @WGID = 15
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.GRP_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.GRP_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON cli.ORDR_ID = fo.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									LEFT JOIN	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	ON	od.ORDR_ID	=	uw.ORDR_ID
																			AND	mt.GRP_ID	=	uw.GRP_ID
									LEFT JOIN	dbo.ORDR_NTE		nte	WITH (NOLOCK)	ON	od.ORDR_ID	=	nte.ORDR_ID
									WHERE		mt.GRP_ID				=	@WGID
										AND		at.STUS_ID				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND		od.DMSTC_CD = 0
										AND		od.DMSTC_CD = 0 AND  od.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID = 6
										AND ISNULL(cli.DEVICE_ID,'') <> ''
										AND ISNULL(fo.CPE_VNDR,'') = 'Goodman'
										AND		(uw.ASN_USER_ID			=	@UserID 
										OR nte.CREAT_BY_USER_ID = @UserID)
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE 
					BEGIN
						INSERT INTO @Order	(ORDR_ID,	TaskID, GroupID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.WG_ID FROM (
								SELECT DISTINCT Top 1000	od.ORDR_ID,	at.TASK_ID, Case	@WGID
																						When	0	Then	uw.GRP_ID
																						Else	@WGID
																					End	AS	WG_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									LEFT JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									LEFT JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	ON	od.ORDR_ID	=	uw.ORDR_ID
																						AND	mt.GRP_ID	=	uw.GRP_ID
									WHERE		at.STUS_ID				=	0
										AND		uw.ASN_USER_ID			=	@UserID 
										AND		uw.GRP_ID				=	Case	@WGID
																				When	0	Then	uw.GRP_ID
																				Else	@WGID
																			End
										AND		ISNULL(od.RGN_ID, 1)	=	Case	
																				When	@WGID	IN (5, 6, 7)	Then 	@RGN_ID
																				Else	ISNULL(od.RGN_ID, 1)
																			End
										AND		od.DMSTC_CD				=	Case	
																						When	@WGID	IN (5, 6, 7)	Then 	1
																						Else	od.DMSTC_CD
																					End
									ORDER BY orderByDate DESC
									
							UNION 
													
							SELECT DISTINCT Top 1000	od.ORDR_ID,	at.TASK_ID, Case	@WGID
																					When	0	Then	uw.GRP_ID
																					Else	@WGID
																				End	AS	WG_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
								FROM		dbo.ORDR			od	WITH (NOLOCK)
								LEFT JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
								LEFT JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
								LEFT JOIN	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	ON	od.ORDR_ID	=	uw.ORDR_ID
																					AND	(mt.GRP_ID	=	uw.GRP_ID OR (mt.GRP_ID = 98 AND uw.GRP_ID IN (5, 6, 7)))
								LEFT JOIN	dbo.FSA_ORDR		fsa WITH (NOLOCK)	ON  od.ORDR_ID  =	fsa.ORDR_ID
									WHERE		
											at.STUS_ID				=	0
									AND		uw.ASN_USER_ID			=	@UserID 
									AND		fsa.PROD_TYPE_CD		= 'CP'
									AND		at.TASK_ID = 1000
									AND		uw.GRP_ID				=	Case	@WGID
																			When	0	Then	uw.GRP_ID
																			Else	@WGID
																		End
									AND		ISNULL(od.RGN_ID, 1)	=	Case	
																			When	@WGID	IN (5, 6, 7)	Then 	@RGN_ID
																			Else	ISNULL(od.RGN_ID, 1)
																		End
									AND		od.DMSTC_CD				=	Case	
																					When	@WGID	IN (5, 6, 7)	Then 	1
																					Else	od.DMSTC_CD
																				End

									ORDER BY orderByDate DESC
								)	AS OB
						END
		END
END
ELSE IF (@OrderID > 0)
BEGIN 
			INSERT INTO @Order	(ORDR_ID,	TaskID, GroupID)
				SELECT DISTINCT od.ORDR_ID,	at.TASK_ID,		Case	@WGID
																When	0	Then	mt.GRP_ID
																Else	@WGID
															End
					FROM		dbo.ORDR			od	WITH (NOLOCK)
					LEFT JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
					LEFT JOIN	dbo.MAP_GRP_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
																	AND		at.STUS_ID	in	(0, 156)
					WHERE		od.ORDR_ID				=	@OrderID
						AND		ISNULL(mt.GRP_ID,0)				=	Case	@WGID
																When	0	Then	ISNULL(mt.GRP_ID,0)
																Else	@WGID
															End
						AND		ISNULL(od.RGN_ID, 1)	=	Case	
																When	@WGID	IN (5, 6, 7)	Then 	@RGN_ID
																Else	ISNULL(od.RGN_ID, 1)
															End
						AND		od.DMSTC_CD				=	Case	
																		When	@WGID	IN (5, 6, 7)	Then 	1
																		Else	od.DMSTC_CD
																	End
END 

OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	IF	OBJECT_ID(N'tempdb..#TempResults', N'U') IS NOT NULL 
		DROP TABLE #TempResults
	
	DECLARE @TAC_PRIMARY Varchar(100)
	---------------------------------------------------------------------------------
	--	Get Data for the FSA Orders.		
	---------------------------------------------------------------------------------
	SELECT Distinct Top 1000	od.ORDR_ID
						,od.ORDR_CAT_ID	AS	CAT_ID
						,ISNULL(od.H5_FOLDR_ID, '')					AS	H5_FOLDR_ID
						,CASE 
							WHEN vt.IsCmplt = 0 THEN vt.TaskID									
							WHEN vt.IsCmplt = 1 AND @WGID IN (5,6,7) THEN CASE ISNULL(od.ORDR_STUS_ID,0) 
																					WHEN 0 THEN 1000
																					WHEN 1 THEN 1000
																					ELSE 1001
																			END	
							WHEN vt.IsCmplt = 1 AND @WGID NOT IN (5,6,7) THEN 1001																					
							END AS	TASK_ID
						,fs.ORDR_ACTN_ID							AS	ORDR_ACTN_ID
						,Case	fs.PRNT_FTN
							When	''	Then	NULL
							else	fs.PRNT_FTN
						End											AS	PRNT_FTN
						,fs.FTN										AS	FTN
						,fc.CUST_ID									AS	H1
						,ISNULL(od.H5_H6_CUST_ID, '')				AS	H5_H6_CUST_ID
						,lo.ORDR_ACTN_DES							AS	ORDR_ACTN_DES
						,Case	
							WHEN od.CSG_LVL_ID!=0  AND SUBSTRING(dbo.decryptBinaryData(csdfc.CUST_NME),1,2) = 'O-'	
									Then	'O-Private Customer'
							WHEN od.CSG_LVL_ID!=0  AND SUBSTRING(dbo.decryptBinaryData(csdfc.CUST_NME),1,2) = 'T-'	
									Then	'T-Private Customer'
							WHEN	od.CSG_LVL_ID!=0	Then	'Private Customer'
							Else	[dbo].[RemoveSpecialCharacters](fc.CUST_NME)
						End											AS	CUST_NME
						,Case
							WHEN od.CSG_LVL_ID!=0  AND SUBSTRING(dbo.decryptBinaryData(csdfc.CUST_NME),1,2) = 'O-'	
									Then	'O-Private Customer'
							WHEN od.CSG_LVL_ID!=0  AND SUBSTRING(dbo.decryptBinaryData(csdfc.CUST_NME),1,2) = 'T-'	
									Then	'T-Private Customer'	
							WHEN	od.CSG_LVL_ID!=0	Then	'Private Customer'
							Else	[dbo].[RemoveSpecialCharacters](fc5.CUST_NME)
						End											AS	H5_H6_CUST_NME
						,CASE 
							WHEN lp.ORDR_TYPE_ID = 8 AND fs.ORDR_TYPE_CD != 'CN' THEN lt.FSA_ORDR_TYPE_DES + ' Cancel'
							ELSE lt.FSA_ORDR_TYPE_DES
							END AS	ORDR_TYPE
						,ISNULL(lst.ORDR_SUB_TYPE_DES, '')			AS	ORDR_SUB_TYPE
						,CASE WHEN (od.PROD_ID='CETH') THEN 'Carrier Ethernet'
							  WHEN (od.PROD_ID='CEPL') THEN 'Carrier Ethernet Private Line'
							  WHEN (od.PROD_ID='CEVPL') THEN 'Carrier Ethernet Virtual Private Line'
							  WHEN (od.PROD_ID='CENNI') THEN 'Carrier Ethernet Network to Network Interface'
							  ELSE lf.FSA_PROD_TYPE_DES		END				AS	PROD_TYPE
						,ISNULL(fc.SRVC_SUB_TYPE_ID, '')			AS	SRVC_SUB_TYPE
						,Case	od.DMSTC_CD
							When	0	Then	'DOM'
							Else	'INTL'
						End											AS	DMSTC_CD
						,ISNULL(od.CUST_CMMT_DT, ISNULL(fs.CUST_CMMT_DT, ''))	AS	CUST_CMMT_DT
						,ISNULL(od.CSG_LVL_ID, 0)					AS	CSG_LVL_ID					
						,Case  
							WHEN vt.IsCmplt = 0 
								THEN 
									Case
										When	ls.TASK_ID = 102 AND od.DMSTC_CD = 1 AND fs.PROD_TYPE_CD	= 'CP' Then	'GOM Milestones Pending' 
										When	ls.TASK_ID = 1001 Then	'Completed' 
										Else	ls.TASK_NME + ' Pending'
									End	
							WHEN vt.IsCmplt = 1 AND @WGID NOT IN (5,6,7) THEN los.ORDR_STUS_DES
							WHEN vt.IsCmplt = 1 AND @WGID IN (5,6,7) THEN CASE od.ORDR_STUS_ID 
																				WHEN 0 THEN CASE lt.FSA_ORDR_TYPE_CD
																								WHEN 'DC' THEN 'Disconnected'
																								WHEN 'CN' THEN 'Cancelled'
																								ELSE 'Completed'
																							END
																				WHEN 1 	THEN CASE lt.FSA_ORDR_TYPE_CD
																								WHEN 'DC' THEN 'Disconnected'
																								WHEN 'CN' THEN 'Cancelled'
																								ELSE 'Completed'
																							END
																				ELSE los.ORDR_STUS_DES
																			END
							END	AS	ORDR_STUS
						,ISNULL(fs.TSUP_PRS_QOT_NBR, '')			AS	TSUP_PRS_QOT_NBR
						,ISNULL(fc5.SOI_CD, '')						AS	SOI_CD 
						,Case	ISNULL(od.INSTL_ESCL_CD,ISNULL(fs.INSTL_ESCL_CD, ''))
							When	'Y'	Then	'Yes'
							When	'N'	Then	'No'
							Else	''
						End											AS	INSTL_ESCL_CD
						,Case	ISNULL(od.FSA_EXP_TYPE_CD,ISNULL(fs.FSA_EXP_TYPE_CD, ''))
							When	'IN'	Then	'Internal'
							When	'EX'	Then	'External'
							When	'IE'	Then	'Internal & External'
							Else	''
						End											AS	FSA_EXP_TYPE_CD
						,dbo.getCommaSeparatedUsers(vt.ORDR_ID, @WGID, @UserID)	AS	ASN_USER
						,Case	ISNULL(ht.CCD_HIST_ID, 0)
							When 0	Then	''
							Else	Convert(Varchar(10), ht.OLD_CCD_DT, 101)
						End											AS	CHNGD_CCD
						,ISNULL(ht.OLD_CCD_DT, '')					AS	CHNGD_CCD_ORG
						--,web.getVendorName(od.ORDR_ID)				AS	VNDR_NME
						,CASE lpt.PLTFRM_CD
								WHEN 'SF'	THEN ISNULL(sf.VNDR_NME,ISNULL(rsf.VNDR_NME,ISNULL(web.getVendorName(od.ORDR_ID, od.ORDR_CAT_ID, od.PLTFRM_CD, lot.FSA_ORDR_TYPE_CD),lvo.VNDR_NME)))
								WHEN 'CP'	THEN ISNULL(cp.VNDR_NME,ISNULL(rcp.VNDR_NME,ISNULL(web.getVendorName(od.ORDR_ID, od.ORDR_CAT_ID, od.PLTFRM_CD, lot.FSA_ORDR_TYPE_CD),lvo.VNDR_NME)))
								WHEN 'RS'	THEN ISNULL(rs.VNDR_NME,ISNULL(rrs.VNDR_NME,ISNULL(web.getVendorName(od.ORDR_ID, od.ORDR_CAT_ID, od.PLTFRM_CD, lot.FSA_ORDR_TYPE_CD),lvo.VNDR_NME)))
								ELSE ''
								END AS	VNDR_NME
						--,ISNULL(fs.TTRPT_SPD_OF_SRVC_BDWD_DES,'')	AS	TTRPT_SPD_OF_SRVC_BDWD_DES       --  IM5923588 changed 10/30/2020 dlp0278
						--,ISNULL(CPEP.TTRPT_SPD_OF_SRVC_BDWD_DES,ISNULL(CPEA.TTRPT_SPD_OF_SRVC_BDWD_DES,'')) AS TTRPT_SPD_OF_SRVC_BDWD_DES
						,(SELECT TOP 1 ISNULL(CPEP.TTRPT_SPD_OF_SRVC_BDWD_DES,ISNULL(CPEA.TTRPT_SPD_OF_SRVC_BDWD_DES,''))
								FROM dbo.FSA_ORDR_CPE_LINE_ITEM CPEA WITH (NOLOCK)
								LEFT JOIN dbo.FSA_ORDR_CPE_LINE_ITEM CPEP WITH (NOLOCK) ON fs.ORDR_ID= CPEP.ORDR_ID AND CPEP.LINE_ITEM_CD='PRT' WHERE fs.ORDR_ID= CPEA.ORDR_ID AND CPEA.LINE_ITEM_CD='ACS') AS TTRPT_SPD_OF_SRVC_BDWD_DES
						,CASE WHEN (od.CSG_LVL_ID!=0) THEN ISNULL(dbo.decryptBinaryData(csdad.CTY_NME), '') ELSE ISNULL(ad.CTY_NME,'') END	AS	CTY_NME
						,ISNULL(stt.US_STATE_NAME,'')				AS	STT_NME
						,ISNULL(lc.CTRY_NME,'')						AS	CTRY_NME
						,ISNULL(lpt.PLTFRM_NME,'')					AS	PLTFRM_NME
						,ISNULL(od.SLA_VLTD_CD,0)					AS  SLA_VLTD_CD
						,od.PRNT_ORDR_ID							AS	PRNT_ORDR_ID
						,CASE	When ISNULL(fs.INSTL_ESCL_CD, '') = 'Y' AND ISNULL(fs.FSA_EXP_TYPE_CD, '') <> '' THEN 0
								When ISNULL(fs.INSTL_ESCL_CD, '') IN ('','N') AND ISNULL(fs.FSA_EXP_TYPE_CD, '') = '' THEN 2
								Else 1
						End											AS	Exp_Sort
						,vt.GroupID GRP_ID
						,CASE	WHEN @WGID IN (5,6,7)	THEN	CASE dbo.RTSSLAExceeded(vt.ORDR_ID) WHEN 1 THEN 'Y'
																									WHEN 0 THEN 'N'
																END
								ELSE 'N'
						END											AS	RTSSLA
						,web.retrieveRTSStatus(od.ORDR_ID,1)		AS	RTS_FLG
						,web.retrieveRTSStatus(od.ORDR_ID,0)		AS	RTS_DT
						,web.retrieveRTSStatus(od.ORDR_ID,2)		AS	RTS_DESC
						,CONVERT(Varchar(10), fs.CREAT_DT, 101)		AS	ORDR_RCVD_DT
						,Case	
							When	Len(fs.FSA_EXP_TYPE_CD) > 1		Then	1
							Else	0
						End											AS	IS_EXP
						,CASE WHEN fs.ORDR_SBMT_DT IS NULL THEN fs.CREAT_DT ELSE fs.ORDR_SBMT_DT END	AS	ORDR_SBMT_DT
						,x.NTE_DT									AS	XNCI_UPDT_DT
						,Case When od.CSG_LVL_ID!=0
							Then	'Private Address'
							Else	ISNULL(ad.STREET_ADR_1, '')
						End											AS ADR
						,Case
							When od.CSG_LVL_ID!=0	Then	'66251'
							Else	ISNULL(ad.ZIP_PSTL_CD, '')
						End											AS ZIP_CD
						,cms.TRGT_DLVRY_DT
						,fs.CUST_WANT_DT
						,@TAC_PRIMARY AS TAC_Primary			
						,ISNULL(uwa.ORDR_HIGHLIGHT_CD,0) AS ORDR_HIGHLIGHT_CD
						,dbo.verifyRTSCCDNotification(od.ORDR_ID) AS RTS_CCD_HIGHLIGHT_CD
						,ISNULL(CONVERT(VARCHAR,me.EVENT_ID),'') AS CPE_EVENT_ID
						,ISNULL(convert(varchar,m.STRT_TMST,22),'') AS EVENT_START_TIME  
						,los2.ORDR_STUS_DES AS CPE_ORDR_STUS
						,les.EVENT_STUS_DES
						,(SELECT TOP 1 ISNULL(litm.CNTRC_TYPE_ID,'')
							FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm  
							WHERE litm.ORDR_ID = od.ORDR_ID
							and litm.CNTRC_TYPE_ID IN ('CUST','INSI')
								ORDER BY litm.CREAT_DT DESC) AS CNTRC_TYPE_ID
						--,(SELECT TOP 1 ISNULL(litm.PRCH_ORDR_NBR,'0')
						--	FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm  
						--	WHERE litm.ORDR_ID = od.ORDR_ID
						--	and litm.EQPT_TYPE_ID IN ('MMM','MJH','SVC','MMH','MMS','SPK')) AS PRCH_ORDR_NBR
						,ISNULL(litm.PRCH_ORDR_NBR,'0') AS PRCH_ORDR_NBR
						,(SELECT TOP 1 ISNULL(litm.ITM_STUS,'')
							FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm  
							WHERE litm.ORDR_ID = od.ORDR_ID
								AND litm.ITM_STUS = 401) AS ITM_STUS
						,(SELECT TOP 1 ISNULL(litm.PLSFT_RQSTN_NBR,'0')
							FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm  
							WHERE litm.ORDR_ID = od.ORDR_ID
							ORDER BY litm.CREAT_DT DESC) AS PLSFT_RQSTN_NBR
						,fc5.SITE_ID
						,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_CD,'')
						FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
							 dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
						WHERE oj.ORDR_ID = od.ORDR_ID
							ORDER BY oj.CREAT_DT DESC) AS JPRDY_CD
						,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_DES,'')
							FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
								 dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
							WHERE oj.ORDR_ID = od.ORDR_ID
								ORDER BY oj.CREAT_DT DESC) AS JPRDY_DES
						,(SELECT TOP 1 DATEDIFF(dd,at1.CREAT_DT,GETDATE()) 
							FROM dbo.ACT_TASK at1 WITH (NOLOCK)
							WHERE at1.TASK_ID IN (600,601,602,1000,604)
												  AND at1.STUS_ID = 0
												  AND at1.ORDR_ID = od.ORDR_ID 
												  ORDER BY at1.CREAT_DT DESC) AS INBOX
					    ,CASE WHEN vt.StatusID = 156 THEN 'Y' ELSE 'N' END AS HOLD
					    ,od.DLVY_CLLI as DLVY_CLLI
					    ,fs.CPE_VNDR as CPE_VNDR
			INTO #TempResults
			FROM		@Order					vt
			INNER JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	vt.ORDR_ID		=	od.ORDR_ID
			INNER JOIN	dbo.FSA_ORDR			fs	WITH (NOLOCK)	ON	od.ORDR_ID		=	fs.ORDR_ID
			LEFT JOIN	dbo.LK_ORDR_SUB_TYPE	lst WITH (NOLOCK)	ON	fs.ORDR_SUB_TYPE_CD = lst.ORDR_SUB_TYPE_CD
			INNER JOIN	dbo.LK_FSA_PROD_TYPE	lf	WITH (NOLOCK)	ON	fs.PROD_TYPE_CD =	lf.FSA_PROD_TYPE_CD
			INNER JOIN	dbo.LK_FSA_ORDR_TYPE	lt	WITH (NOLOCK)	ON	fs.ORDR_TYPE_CD =	lt.FSA_ORDR_TYPE_CD
			LEFT JOIN	dbo.LK_TASK				ls	WITH (NOLOCK)	ON	vt.TaskID		=	ls.TASK_ID
			INNER JOIN	dbo.LK_ORDR_ACTN		lo	WITH (NOLOCK)	ON	fs.ORDR_ACTN_ID =	lo.ORDR_ACTN_ID
			LEFT JOIN	dbo.FSA_ORDR_CUST		fc	WITH (NOLOCK)	ON	fs.ORDR_ID		=	fc.ORDR_ID
																	AND	fc.CIS_LVL_TYPE	=	'H1'
			LEFT JOIN	dbo.FSA_ORDR_CUST		fc5	WITH (NOLOCK)	ON	fs.ORDR_ID		=	fc5.ORDR_ID
																	AND	fc5.CIS_LVL_TYPE	IN	('H5', 'H6')																	
			LEFT JOIN	dbo.ORDR_ADR			ad	WITH (NOLOCK)	ON	od.ORDR_ID		=	ad.ORDR_ID
																	AND	ad.CIS_LVL_TYPE	IN	('H5', 'H6')
																	AND ad.adr_type_id	=	18													
			LEFT JOIN	dbo.CUST_SCRD_DATA		csdad WITH (NOLOCK) ON csdad.SCRD_OBJ_ID = ad.ORDR_ADR_ID AND csdad.SCRD_OBJ_TYPE_ID=14
			LEFT JOIN	dbo.CUST_SCRD_DATA		csdfc WITH (NOLOCK) ON csdfc.SCRD_OBJ_ID = fc.FSA_ORDR_CUST_ID AND csdfc.SCRD_OBJ_TYPE_ID=5
			LEFT JOIN	dbo.CUST_SCRD_DATA		csdf5 WITH (NOLOCK) ON csdf5.SCRD_OBJ_ID = fc5.FSA_ORDR_CUST_ID AND csdf5.SCRD_OBJ_TYPE_ID=5
			LEFT JOIN	(SELECT		ORDR_ID, Min(CCD_HIST_ID) AS	CH_ID	
							FROM	dbo.CCD_HIST	WITH (NOLOCK)
							GROUP BY ORDR_ID)	ch					ON	od.ORDR_ID		=	ch.ORDR_ID
			LEFT JOIN	dbo.CCD_HIST			ht	WITH (NOLOCK)	ON	ch.CH_ID		=	ht.CCD_HIST_ID	
			LEFT JOIN	dbo.LK_CTRY				lc	WITH (NOLOCK)	ON	lc.CTRY_CD		=	ad.CTRY_CD
			LEFT JOIN	dbo.LK_US_STT			stt	WITH (NOLOCK)	ON	ad.STT_CD		=	stt.US_STATE_ID	
			LEFT JOIN	dbo.LK_PLTFRM			lpt	WITH (NOLOCK)	ON	lpt.PLTFRM_CD	=	od.PLTFRM_CD
			LEFT JOIN	dbo.LK_ORDR_STUS		los	WITH (NOLOCK)	ON	los.ORDR_STUS_ID =	od.ORDR_STUS_ID	
			LEFT JOIN   dbo.FSA_ORDR_GOM_XNCI GOM WITH (NOLOCK) ON	od.ORDR_ID	=	GOM.ORDR_ID
																AND	GOM.GRP_ID	=	1
			LEFT JOIN	dbo.LK_ORDR_STUS		los2	WITH (NOLOCK)	ON	los2.ORDR_STUS_ID =	GOM.CPE_STUS_ID
			LEFT JOIN   dbo.CKT					ckt	WITH (NOLOCK)   ON	ckt.ORDR_ID		= vt.ORDR_ID
			LEFT JOIN	(SELECT		MAX(VER_ID)	AS	VER_ID,	CKT_ID
							FROM	dbo.CKT_MS	WITH (NOLOCK)	
							GROUP BY	CKT_ID)a					ON	a.CKT_ID		=	ckt.CKT_ID							
			LEFT JOIN	dbo.CKT_MS				cms	WITH (NOLOCK)   ON	cms.CKT_ID		=	a.CKT_ID
																	AND	cms.VER_ID		=	a.VER_ID				
			LEFT JOIN	(SELECT		ORDR_ID,	MAX(CREAT_DT)	AS	NTE_DT
							FROM	dbo.ORDR_NTE	WITH (NOLOCK)
							WHERE	NTE_TYPE_ID	IN	(11, 12)
							GROUP BY	ORDR_ID)x					ON	x.ORDR_ID		=	vt.ORDR_ID
			--LEFT JOIN	dbo.ORDR_CNTCT			oc	WITH (NOLOCK)	ON	oc.ORDR_ID		=	od.ORDR_ID
			--														AND	oc.ROLE_ID		=	13							
			LEFT JOIN	dbo.LK_PPRT				lp	WITH (NOLOCK)	ON	lp.PPRT_ID		=	od.PPRT_ID
			LEFT JOIN	dbo.ORDR_MS				orm	WITH (NOLOCK)	ON	orm.ORDR_ID		=	od.ORDR_ID
			LEFT JOIN	dbo.USER_WFM_ASMT		uwa WITH (NOLOCK)	ON	uwa.ORDR_ID		=	od.ORDR_ID
																	AND	uwa.GRP_ID		=	@WGID
																	AND uwa.ASN_USER_ID		=	@UserID
																	AND ISNULL(uwa.ORDR_HIGHLIGHT_CD,0) = 1
			LEFT JOIN	dbo.LK_VNDR		sf	WITH (NOLOCK)			ON	sf.VNDR_CD		=	fs.CXR_ACCS_CD
			LEFT JOIN	dbo.LK_VNDR		cp	WITH (NOLOCK)			ON	cp.VNDR_CD		=	fs.VNDR_VPN_CD	
			LEFT JOIN	dbo.LK_VNDR		rs	WITH (NOLOCK)			ON	rs.VNDR_CD		=	fs.INSTL_VNDR_CD																
			LEFT JOIN	dbo.FSA_ORDR	rfs	WITH (NOLOCK)			ON	rfs.FTN			=	fs.RELTD_FTN 
			LEFT JOIN	dbo.LK_VNDR		rsf	WITH (NOLOCK)			ON	rsf.VNDR_CD		=	rfs.CXR_ACCS_CD
			LEFT JOIN	dbo.LK_VNDR		rcp	WITH (NOLOCK)			ON	rcp.VNDR_CD		=	rfs.VNDR_VPN_CD	
			LEFT JOIN	dbo.LK_VNDR		rrs	WITH (NOLOCK)			ON	rrs.VNDR_CD		=	rfs.INSTL_VNDR_CD
			LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)			ON	vo.ORDR_ID		=	od.ORDR_ID
			LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)			ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
			LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)			ON	lvo.VNDR_CD = vf.VNDR_CD
			LEFT JOIN	(SELECT MAX(EVENT_ID) AS CPE_EVENT_ID, 
								ecd.CPE_ORDR_ID, 
								fod.ORDR_ID AS ORDR_ID
							FROM	dbo.EVENT_CPE_DEV ecd WITH (NOLOCK) 
							INNER JOIN	dbo.FSA_ORDR fod WITH (NOLOCK) ON fod.FTN = ecd.CPE_ORDR_NBR
							WHERE ecd.REC_STUS_ID = 1
							GROUP BY ecd.CPE_ORDR_ID, fod.ORDR_ID) AS cEventID ON cEventID.ORDR_ID = od.ORDR_ID
			LEFT JOIN	dbo.EVENT_CPE_DEV me WITH (NOLOCK) ON	cEventID.CPE_EVENT_ID	=	me.EVENT_ID
																AND cEventID.CPE_ORDR_ID =	me.CPE_ORDR_ID	
																AND me.REC_STUS_ID = 1
			LEFT JOIN	dbo.MDS_EVENT m WITH (NOLOCK)			ON 	me.EVENT_ID		=	m.EVENT_ID
			LEFT JOIN	dbo.LK_EVENT_STUS les WITH (NOLOCK) 	ON m.EVENT_STUS_ID=les.EVENT_STUS_ID
			LEFT JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON od.ORDR_ID = litm.ORDR_ID
																		 AND litm.EQPT_TYPE_ID IN ('MMM','MJH','SVC','MMH','MMS','SPK')
																		 AND litm.PRCH_ORDR_NBR is not null
			LEFT JOIN 	dbo.LK_ORDR_TYPE lot  WITH (NOLOCK) ON lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
			--LEFT JOIN dbo.FSA_ORDR_CPE_LINE_ITEM CPEA WITH (NOLOCK) ON fs.ORDR_ID= CPEA.ORDR_ID AND CPEA.LINE_ITEM_CD='ACS'
			--LEFT JOIN dbo.FSA_ORDR_CPE_LINE_ITEM CPEP WITH (NOLOCK) ON fs.ORDR_ID= CPEP.ORDR_ID AND CPEP.LINE_ITEM_CD='PRT'
			ORDER by 1 DESC
		---------------------------------------------------------------------------------
		--	Get Data for the IPL Orders.		
		---------------------------------------------------------------------------------	
		INSERT INTO #TempResults
		SELECT Top 500	od.ORDR_ID
					,od.ORDR_CAT_ID	AS	CAT_ID
					,ISNULL(od.H5_FOLDR_ID, '')					AS	H5_FOLDR_ID
					,CASE 
							WHEN vt.IsCmplt = 0 THEN vt.TaskID									
							WHEN vt.IsCmplt = 1 AND @WGID IN (5,6,7) THEN CASE ISNULL(od.ORDR_STUS_ID,0) 
																					WHEN 0 THEN 1000
																					WHEN 1 THEN 1000
																					ELSE 1001
																			END
							WHEN vt.IsCmplt = 1 AND @WGID NOT IN (5,6,7) THEN 1001																					
							END AS	TASK_ID
					,1											AS	ORDR_ACTN_ID
					,NULL										AS	PRNT_FTN
					,CONVERT(VARCHAR,od.ORDR_ID)				AS	FTN
					,''											AS	H1
					,ISNULL(hf.CUST_ID, '')						AS	H5_H6_CUST_ID
					,''											AS	ORDR_ACTN_DES
					,Case	When od.CSG_LVL_ID!=0
							Then	'Private Customer'
							Else	[dbo].[RemoveSpecialCharacters](hf.CUST_NME)
						End											AS	CUST_NME
					,''											AS	H5_H6_CUST_NME
					,CASE ip.ORDR_TYPE_ID
						WHEN 8 THEN 
							CASE 
								WHEN ISNULL(lt2.ORDR_TYPE_DES,'') = '' THEN lt.ORDR_TYPE_DES
								ELSE lt2.ORDR_TYPE_DES + + ' ' + lt.ORDR_TYPE_DES
							END
						ELSE lt.ORDR_TYPE_DES
					END											AS	ORDR_TYPE
					,''											AS	ORDR_SUB_TYPE
					,lf.PROD_TYPE_DES							AS	PROD_TYPE
					,''											AS	SRVC_SUB_TYPE
					,'INTL'										AS	DMSTC_CD
					,ISNULL(od.CUST_CMMT_DT, ISNULL(ip.CUST_CMMT_DT, ''))	AS	CUST_CMMT_DT
					,ISNULL(od.CSG_LVL_ID, 0)					AS	CSG_LVL_ID					
					,Case  
							WHEN vt.IsCmplt = 0	THEN  ls.TASK_NME + ' Pending'
							WHEN vt.IsCmplt = 1 AND @WGID NOT IN (5,6,7) THEN los.ORDR_STUS_DES
							WHEN vt.IsCmplt = 1 AND @WGID IN (5,6,7) THEN CASE od.ORDR_STUS_ID 
																				WHEN 0 THEN CASE lt.FSA_ORDR_TYPE_CD
																								WHEN 'DC' THEN 'Disconnected'
																								WHEN 'CN' THEN 'Cancelled'
																								ELSE 'Completed'
																							END
																				WHEN 1 	THEN CASE lt.FSA_ORDR_TYPE_CD
																								WHEN 'DC' THEN 'Disconnected'
																								WHEN 'CN' THEN 'Cancelled'
																								ELSE 'Completed'
																							END
																				ELSE los.ORDR_STUS_DES
																			END 
							END	AS	ORDR_STUS
					,Case	ISNULL(ip.PRS_PRICE_QOT_NBR, '')
						When	''	Then	ISNULL(ip.GCS_PRICE_QOT_NBR, '')
						Else	ip.PRS_PRICE_QOT_NBR
					End											AS	TSUP_PRS_QOT_NBR
					,SUBSTRING(ISNULL(ip.SOI, ''),1,3)							AS	SOI_CD 
					,Case	ISNULL(od.INSTL_ESCL_CD,'')
						When '' Then	Case	ISNULL(ex.ESCALAT_ORDR_CD, -1)
											When	1	Then	'Yes'
											When	0	Then	'No'
											Else	''
										End	
						Else	Case	ISNULL(od.INSTL_ESCL_CD,'')
									When	'Y'	Then	'Yes'
									When	'N'	Then	'No'
									Else	''
								End					
					End											AS	INSTL_ESCL_CD
					,Case	ISNULL(od.FSA_EXP_TYPE_CD,'')
						When	''	Then	ISNULL(le.EXP_TYPE_DES, '')	
						Else	Case	ISNULL(od.FSA_EXP_TYPE_CD,'')
									When	'IN'	Then	'Internal'
									When	'EX'	Then	'External'
									When	'IE'	Then	'Internal & External'
									Else	''
								End	
					End											AS	FSA_EXP_TYPE_CD
					,dbo.getCommaSeparatedUsers(vt.ORDR_ID, @WGID, @UserID)	AS	ASN_USER
					,''											AS	CHNGD_CCD
					,''											AS	CHNGD_CCD_ORG
					--,web.getVendorName(od.ORDR_ID)				AS	VNDR_NME
					,ISNULL(lv.CXR_NME,ISNULL(lvo.VNDR_NME,'')) AS VNDR_NME
					,''											AS	TTRPT_SPD_OF_SRVC_BDWD_DES
					,CASE WHEN od.CSG_LVL_ID!=0 THEN ISNULL(dbo.decryptBinaryData(csdad.CTY_NME), '') ELSE ISNULL(ad.CTY_NME,'') END	AS	CTY_NME
					,ISNULL(stt.US_STATE_NAME,'')				AS	STT_NME
					,ISNULL(lc.CTRY_NME, '')					AS	CTRY_NME
					,''											AS	PLTFRM_NME
					,ISNULL(od.SLA_VLTD_CD,0)					AS  SLA_VLTD_CD
					,od.PRNT_ORDR_ID							AS	PRNT_ORDR_ID
					,CASE	When ISNULL(ex.ESCALAT_ORDR_CD, 0) = 1 AND ISNULL(le.EXP_TYPE_DES, '') <> '' THEN 0
							When ISNULL(ex.ESCALAT_ORDR_CD, 0) = 0 AND ISNULL(le.EXP_TYPE_DES, '') = '' THEN 2
							Else 1
					End											AS	Exp_Sort
					,vt.GroupID GRP_ID
					,CASE	WHEN @WGID IN (5,6,7)	THEN	CASE dbo.RTSSLAExceeded(vt.ORDR_ID)	WHEN 1 THEN 'Y'
																								WHEN 0 THEN 'N'
															END
								ELSE 'N'
						END										AS	RTSSLA
					,web.retrieveRTSStatus(od.ORDR_ID,1)		AS	RTS_FLG
					,web.retrieveRTSStatus(od.ORDR_ID,0)		AS	RTS_DT
					,web.retrieveRTSStatus(od.ORDR_ID,2)		AS	RTS_DESC
					,CONVERT(Varchar(10), ip.CREAT_DT, 101)		AS	ORDR_RCVD_DT
					,Case	
						When	Len(od.FSA_EXP_TYPE_CD) > 1		Then	1
						Else	0
					End											AS	IS_EXP
					,CASE WHEN ip.CUST_ORDR_SBMT_DT	IS NULL THEN ip.CREAT_DT ELSE 	ip.CUST_ORDR_SBMT_DT END				AS	ORDR_SBMT_DT
					,x.NTE_DT									AS	XNCI_UPDT_DT
					,Case	When od.CSG_LVL_ID!=0
							Then	'Private Address'
							Else	ISNULL(ad.STREET_ADR_1, '')
					End											AS ADR
					,Case	When od.CSG_LVL_ID!=0
							Then	'66251'
							Else	ISNULL(ad.ZIP_PSTL_CD, '')
						End											AS ZIP_CD
					,cms.TRGT_DLVRY_DT
					,ip.CUST_WANT_DT
					,@TAC_PRIMARY AS TAC_Primary													
					,ISNULL(uwa.ORDR_HIGHLIGHT_CD,0) AS ORDR_HIGHLIGHT_CD	
					,dbo.verifyRTSCCDNotification(od.ORDR_ID) AS RTS_CCD_HIGHLIGHT_CD
					,'' AS CPE_EVENT_ID
					,'' AS EVENT_START_TIME  
					,los2.ORDR_STUS_DES AS CPE_ORDR_STUS
					,'' AS EVENT_STUS_DES
					,'' AS CNTRC_TYPE_ID
					,'' AS PRCH_ORDR_NBR
					,'' AS ITM_STUS
					,'' AS PLSFT_RQSTN_NBR
					,'' as SITE_ID
					,'' AS JPRDY_CD
					,'' AS JPRDY_DES
					,'' AS INBOX
					,CASE WHEN vt.StatusID = 156 THEN 'Y' ELSE 'N' END AS HOLD
					,od.DLVY_CLLI as DLVY_CLLI
					,'' as CPE_VNDR
		FROM		@Order					vt
		INNER JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	vt.ORDR_ID		=	od.ORDR_ID
		INNER JOIN	dbo.IPL_ORDR			ip	WITH (NOLOCK)	ON	od.ORDR_ID		=	ip.ORDR_ID
		INNER JOIN	dbo.LK_PROD_TYPE		lf	WITH (NOLOCK)	ON	ip.PROD_TYPE_ID =	lf.PROD_TYPE_ID
		INNER JOIN	dbo.LK_ORDR_TYPE		lt	WITH (NOLOCK)	ON	ip.ORDR_TYPE_ID =	lt.ORDR_TYPE_ID
		LEFT JOIN	dbo.LK_ORDR_TYPE		lt2	WITH (NOLOCK)	ON	lt2.ORDR_TYPE_ID =	ip.PREV_ORDR_TYPE_ID
		LEFT JOIN	dbo.LK_TASK				ls	WITH (NOLOCK)	ON	vt.TaskID		=	ls.TASK_ID
		LEFT JOIN	dbo.H5_FOLDR			hf	WITH (NOLOCK)	ON	od.H5_FOLDR_ID	=	hf.H5_FOLDR_ID
		LEFT JOIN	dbo.ORDR_EXP			ex	WITH (NOLOCK)	ON	od.ORDR_ID		=	ex.ORDR_ID
		LEFT JOIN	dbo.LK_EXP_TYPE			le	WITH (NOLOCK)	ON	ex.EXP_TYPE_ID	=	le.EXP_TYPE_ID
		LEFT JOIN	dbo.ORDR_ADR			ad	WITH (NOLOCK)	ON	od.ORDR_ID		=	ad.ORDR_ID
																AND	ad.ADR_TYPE_ID	=	12
		LEFT JOIN	dbo.CUST_SCRD_DATA		csdad WITH (NOLOCK) ON csdad.SCRD_OBJ_ID = ad.ORDR_ADR_ID AND csdad.SCRD_OBJ_TYPE_ID=14
		LEFT JOIN	dbo.LK_CTRY				lc	WITH (NOLOCK)	ON	ad.CTRY_CD		=	lc.CTRY_CD	
		LEFT JOIN	dbo.LK_US_STT			stt	WITH (NOLOCK)	ON	ad.STT_CD		=	stt.US_STATE_ID	
		LEFT JOIN	dbo.LK_ORDR_STUS		los	WITH (NOLOCK)	ON	los.ORDR_STUS_ID =	od.ORDR_STUS_ID
		LEFT JOIN   dbo.FSA_ORDR_GOM_XNCI GOM WITH (NOLOCK) ON	od.ORDR_ID	=	GOM.ORDR_ID
																AND	GOM.GRP_ID	=	1
		LEFT JOIN	dbo.LK_ORDR_STUS		los2	WITH (NOLOCK)	ON	los2.ORDR_STUS_ID =	GOM.CPE_STUS_ID
		LEFT JOIN   dbo.CKT					ckt	WITH (NOLOCK)   ON	ckt.ORDR_ID		= vt.ORDR_ID
		LEFT JOIN	(SELECT		MAX(VER_ID)	AS	VER_ID,	CKT_ID
						FROM	dbo.CKT_MS	WITH (NOLOCK)	
						GROUP BY	CKT_ID)a					ON	a.CKT_ID		=	ckt.CKT_ID							
		LEFT JOIN	dbo.CKT_MS				cms	WITH (NOLOCK)   ON	cms.CKT_ID		=	a.CKT_ID
																AND	cms.VER_ID		=	a.VER_ID				
		LEFT JOIN	(SELECT		ORDR_ID,	MAX(CREAT_DT)	AS	NTE_DT
						FROM	dbo.ORDR_NTE	WITH (NOLOCK)
						WHERE	NTE_TYPE_ID	IN	(11, 12)
						GROUP BY	ORDR_ID)x					ON	x.ORDR_ID		=	vt.ORDR_ID		
		--LEFT JOIN	dbo.ORDR_CNTCT			oc	WITH (NOLOCK)	ON	oc.ORDR_ID		=	od.ORDR_ID
		--														AND	oc.ROLE_ID		=	13
		LEFT JOIN	dbo.USER_WFM_ASMT		uwa WITH (NOLOCK)	ON	uwa.ORDR_ID		=	od.ORDR_ID
																	AND	uwa.GRP_ID		=	@WGID
																	AND uwa.ASN_USER_ID		=	@UserID
																	AND ISNULL(uwa.ORDR_HIGHLIGHT_CD,0) = 1
		LEFT JOIN	dbo.IPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	od.ORDR_ID		=	la.ORDR_ID
															AND	la.ACCS_TYPE_ID	=	'Terminating'
		LEFT JOIN	dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD	
		LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)				ON	vo.ORDR_ID		=	od.ORDR_ID
		LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)				ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
		LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)				ON	lvo.VNDR_CD = vf.VNDR_CD																	
		ORDER BY vt.ORDR_ID DESC
																	
		INSERT INTO #TempResults
		SELECT Top 500	od.ORDR_ID
					,od.ORDR_CAT_ID	AS	CAT_ID
					,ISNULL(od.H5_FOLDR_ID, '')					AS	H5_FOLDR_ID
					,CASE 
							WHEN vt.IsCmplt = 0 THEN vt.TaskID									
							WHEN vt.IsCmplt = 1 AND @WGID IN (5,6,7) THEN CASE ISNULL(od.ORDR_STUS_ID,0) 
																					WHEN 0 THEN 1000
																					WHEN 1 THEN 1000
																					ELSE 1001
																			END
							WHEN vt.IsCmplt = 1 AND @WGID NOT IN (5,6,7) THEN 1001																					
							END AS	TASK_ID
					,1											AS	ORDR_ACTN_ID
					,NULL										AS	PRNT_FTN
					,CONVERT(VARCHAR,od.ORDR_ID)				AS	FTN
					,''											AS	H1
					,ISNULL(nod.H5_ACCT_NBR, '')				AS	H5_H6_CUST_ID
					,'Submit'									AS	ORDR_ACTN_DES
					,Case	When od.CSG_LVL_ID!=0
							Then	'Private Customer'
							Else	[dbo].[RemoveSpecialCharacters](nod.CUST_NME)
						End											AS	CUST_NME
					,''											AS	H5_H6_CUST_NME
					,lt.ORDR_TYPE_DES							AS	ORDR_TYPE
					,''											AS	ORDR_SUB_TYPE
					,lf.PROD_TYPE_DES							AS	PROD_TYPE
					,''											AS	SRVC_SUB_TYPE
					,'INTL'										AS	DMSTC_CD
					,ISNULL(od.CUST_CMMT_DT, ISNULL(nod.CCS_DT, ''))	AS	CUST_CMMT_DT
					,ISNULL(od.CSG_LVL_ID, 0)					AS	CSG_LVL_ID					
					,Case  
							WHEN vt.IsCmplt = 0	THEN  ls.TASK_NME + ' Pending'
							WHEN vt.IsCmplt = 1 AND @WGID NOT IN (5,6,7) THEN los.ORDR_STUS_DES
							WHEN vt.IsCmplt = 1 AND @WGID IN (5,6,7) THEN CASE od.ORDR_STUS_ID 
																				WHEN 0 THEN CASE lt.FSA_ORDR_TYPE_CD
																								WHEN 'DC' THEN 'Disconnected'
																								WHEN 'CN' THEN 'Cancelled'
																								ELSE 'Completed'
																							END
																				WHEN 1 	THEN CASE lt.FSA_ORDR_TYPE_CD
																								WHEN 'DC' THEN 'Disconnected'
																								WHEN 'CN' THEN 'Cancelled'
																								ELSE 'Completed'
																							END
																				ELSE los.ORDR_STUS_DES
																			END 
							END	AS	ORDR_STUS
					,''	AS	TSUP_PRS_QOT_NBR
					,''	AS	SOI_CD 
					,''	AS	INSTL_ESCL_CD
					,'' AS	FSA_EXP_TYPE_CD
					,dbo.getCommaSeparatedUsers(vt.ORDR_ID, @WGID, @UserID)	AS	ASN_USER
					,''											AS	CHNGD_CCD
					,''											AS	CHNGD_CCD_ORG
					--,web.getVendorName(od.ORDR_ID)				AS	VNDR_NME
					,ISNULL(lv.VNDR_NME,ISNULL(lvo.VNDR_NME,'')) AS VNDR_NME
					,''											AS	TTRPT_SPD_OF_SRVC_BDWD_DES
					,ISNULL(nod.SITE_CITY_NME,'')				AS	CTY_NME
					,''											AS	STT_NME
					,ISNULL(lc.CTRY_NME, '')					AS	CTRY_NME
					,''											AS	PLTFRM_NME
					,ISNULL(od.SLA_VLTD_CD,0)					AS  SLA_VLTD_CD
					,od.PRNT_ORDR_ID							AS	PRNT_ORDR_ID
					,'' AS	Exp_Sort
					,vt.GroupID GRP_ID
					,CASE	WHEN @WGID IN (5,6,7)	THEN	CASE dbo.RTSSLAExceeded(vt.ORDR_ID)	WHEN 1 THEN 'Y'
																								WHEN 0 THEN 'N'
															END
								ELSE 'N'
						END										AS	RTSSLA
					,''		AS	RTS_FLG
					,''		AS	RTS_DT
					,''		AS	RTS_DESC
					,CONVERT(Varchar(10), nod.CREAT_DT,101)		AS	ORDR_RCVD_DT
					,Case	
						When	Len(od.FSA_EXP_TYPE_CD) > 1		Then	1
						Else	0
					End											AS	IS_EXP
					,nod.CREAT_DT						AS	ORDR_SBMT_DT
					,x.NTE_DT									AS	XNCI_UPDT_DT
					,'' AS ADR
					,'' AS ZIP_CD
					,cms.TRGT_DLVRY_DT
					,nod.CWD_DT
					,@TAC_PRIMARY AS TAC_Primary
					,ISNULL(uwa.ORDR_HIGHLIGHT_CD,0) AS ORDR_HIGHLIGHT_CD													
					,dbo.verifyRTSCCDNotification(od.ORDR_ID) AS RTS_CCD_HIGHLIGHT_CD
					,'' AS CPE_EVENT_ID
					,'' AS EVENT_START_TIME  
					,los2.ORDR_STUS_DES AS CPE_ORDR_STUS
					,'' AS EVENT_STUS_DES
					,'' AS CNTRC_TYPE_ID
					,'' AS PRCH_ORDR_NBR
					,'' AS ITM_STUS
					,'' AS PLSFT_RQSTN_NBR
					,'' as SITE_ID
					,'' AS JPRDY_CD
					,'' AS JPRDY_DES
					,'' AS INBOX
					,CASE WHEN vt.StatusID = 156 THEN 'Y' ELSE 'N' END AS HOLD
					,od.DLVY_CLLI as DLVY_CLLI
					,'' as CPE_VNDR
		FROM		@Order					vt
		INNER JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	vt.ORDR_ID		=	od.ORDR_ID
		INNER JOIN	dbo.NCCO_ORDR			nod WITH (NOLOCK)	ON	od.ORDR_ID		=	nod.ORDR_ID
		INNER JOIN	dbo.LK_PROD_TYPE		lf	WITH (NOLOCK)	ON	nod.PROD_TYPE_ID =	lf.PROD_TYPE_ID
		INNER JOIN	dbo.LK_ORDR_TYPE		lt	WITH (NOLOCK)	ON	nod.ORDR_TYPE_ID =	lt.ORDR_TYPE_ID
		LEFT JOIN	dbo.LK_TASK				ls	WITH (NOLOCK)	ON	vt.TaskID		=	ls.TASK_ID
		LEFT JOIN	dbo.LK_CTRY				lc	WITH (NOLOCK)	ON	nod.CTRY_CD		=	lc.CTRY_CD	
		LEFT JOIN	dbo.LK_ORDR_STUS		los	WITH (NOLOCK)	ON	los.ORDR_STUS_ID =	od.ORDR_STUS_ID
		LEFT JOIN   dbo.FSA_ORDR_GOM_XNCI GOM WITH (NOLOCK) ON	od.ORDR_ID	=	GOM.ORDR_ID
																AND	GOM.GRP_ID	=	1
		LEFT JOIN	dbo.LK_ORDR_STUS		los2	WITH (NOLOCK)	ON	los2.ORDR_STUS_ID =	GOM.CPE_STUS_ID
		LEFT JOIN   dbo.CKT					ckt	WITH (NOLOCK)   ON	ckt.ORDR_ID		= vt.ORDR_ID
		LEFT JOIN	(SELECT		MAX(VER_ID)	AS	VER_ID,	CKT_ID
						FROM	dbo.CKT_MS	WITH (NOLOCK)	
						GROUP BY	CKT_ID)a					ON	a.CKT_ID		=	ckt.CKT_ID							
		LEFT JOIN	dbo.CKT_MS				cms	WITH (NOLOCK)   ON	cms.CKT_ID		=	a.CKT_ID
																AND	cms.VER_ID		=	a.VER_ID				
		LEFT JOIN	(SELECT		ORDR_ID,	MAX(CREAT_DT)	AS	NTE_DT
						FROM	dbo.ORDR_NTE	WITH (NOLOCK)
						WHERE	NTE_TYPE_ID	IN	(11, 12)
						GROUP BY	ORDR_ID)x					ON	x.ORDR_ID		=	vt.ORDR_ID		
		LEFT JOIN	dbo.USER_WFM_ASMT		uwa WITH (NOLOCK)	ON	uwa.ORDR_ID		=	od.ORDR_ID
																	AND	uwa.GRP_ID		=	@WGID
																	AND uwa.ASN_USER_ID	=	@UserID
																	AND ISNULL(uwa.ORDR_HIGHLIGHT_CD,0) = 1
		LEFT JOIN	dbo.LK_VNDR		lv	 WITH (NOLOCK)			ON	nod.VNDR_CD	=	lv.VNDR_CD																	
		LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)				ON	vo.ORDR_ID		=	od.ORDR_ID
		LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)				ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
		LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)				ON	lvo.VNDR_CD = vf.VNDR_CD																	
		ORDER BY vt.ORDR_ID DESC

		UPDATE t
			SET FTN = CASE WHEN ISNULL(fcpe.DEVICE_ID,'') = '' THEN t.FTN 
							ELSE FTN + '-' + fcpe.DEVICE_ID
						END
			FROM #TempResults t WITH (NOLOCK)
		INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcpe WITH (NOLOCK) ON fcpe.ORDR_ID = t.ORDR_ID
			WHERE CAT_ID = 6
				AND PROD_TYPE	=	'CPE' OR (PROD_TYPE LIKE 'Carrier Ethernet%')
				--AND DMSTC_CD	=	'DOM'
				
		UPDATE t
			SET t.TAC_Primary = CASE WHEN (t.CSG_LVL_ID = 0) THEN oc.FRST_NME + ' ' + oc.LST_NME ELSE dbo.decryptBinaryData(csd.FRST_NME) + ' ' + dbo.decryptBinaryData(csd.LST_NME) END
			FROM #TempResults t		 				
			LEFT JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON t.ORDR_ID = oc.ORDR_ID
			LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csd.SCRD_OBJ_TYPE_ID=15
			WHERE oc.ROLE_ID = 13
				
		SELECT DISTINCT * FROM #TempResults WHERE ((CSG_LVL_ID = 0) OR ((@CSGLvlId!=0) AND (CSG_LVL_ID >= @CSGLvlId))) ORDER BY ORDR_ID DESC


End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END