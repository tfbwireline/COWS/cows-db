USE [COWS]
GO
_CreateObject 'SP','web','getGOMWFMOrders'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <08/16/2012>
-- Description:	<Get WFM Orders for GOM>
-- =============================================
--Exec [web].[getGOMWFMOrders] 2,' WHERE ASSIGNED_USER = ''Phillips, David L [IT]'' AND  SUB_STATUS = ''Submit''',' ORDER BY ORDR_ID ASC'
ALTER PROCEDURE [web].[getGOMWFMOrders]
@IsOrderInGOMWG TinyInt		 = 1,
@whereClause	Varchar(max) = '',
@sortBy			Varchar(200)		
AS
BEGIN
BEGIN TRY
--	DECLARE @IsOrderInGOMWG TinyInt
--DECLARE @whereClause VARCHAR(MAX)
DECLARE @SQLStr		nVARCHAR(max)
DECLARE @SQLStr1	nVARCHAR(max)
DECLARE @SQLStr2	nVARCHAR(max)
DECLARE @SQLStr3	nVARCHAR(max)
SET @SQLStr		=	''
SET @SQLStr1	=	''
SET @SQLStr2	=	''
SET @SQLStr3	=	''

--SET @IsOrderInGOMWG	=	2
--SET @whereClause = ' WHERE ASSIGNED_USER = ''Phillips, David L [IT]'' AND  SUB_STATUS = ''Submit'''
--SET @whereClause = ' WHERE ORDR_ID LIKE ''%110%'''

IF OBJECT_ID(N'tempdb..#GOMWFMTmp', N'U') IS NOT NULL
	DROP TABLE #GOMWFMTmp
CREATE TABLE #GOMWFMTmp 
(
	ORDR_ID			INT,
	FTN				Varchar(20),
	PROD_TYPE		VARCHAR(100),
	ORDR_TYPE		VARCHAR(100),
	PLTFM_TYPE		VARCHAR(50),
	SUB_STATUS		VARCHAR(250),
	ASSIGNED_USER	VARCHAR(100),
	ASSIGNED_BY		VARCHAR(100),
	MGR				VARCHAR(100),
	ASMT_DT			SMALLDATETIME,
	VNDR_NAME		VARCHAR(50),
	RTSflag			VARCHAR(5),
	ORDR_RCVD_DT	SMALLDATETIME
)

IF @IsOrderInGOMWG = 1
	BEGIN
		SET @SQLStr =	
			'
			INSERT INTO #GOMWFMTmp (ORDR_ID,FTN,PROD_TYPE,ORDR_TYPE,PLTFM_TYPE,SUB_STATUS,
									ASSIGNED_USER,ASSIGNED_BY,MGR,ASMT_DT,
									VNDR_NAME,RTSflag,ORDR_RCVD_DT)
			Select	DISTINCT 
				odr.ORDR_ID								AS	ORDR_ID
				,ISNULL(fsa.FTN,odr.ORDR_ID)			AS	FTN
				,ISNULL(lpt.PROD_TYPE_DES,'''')			AS	PROD_TYPE
				,ISNULL(lot.ORDR_TYPE_DES,'''')			AS	ORDR_TYPE
				,ISNULL(lp.PLTFRM_NME,'''')				AS	PLTFM_TYPE	
				,ISNULL(loa.ORDR_ACTN_DES,''Submit'')	AS SUB_STATUS
				,ISNULL(lu.DSPL_NME,'''')				AS	ASSIGNED_USER
				,ISNULL(lub.DSPL_NME,'''')				AS	ASSIGNED_BY
				,ISNULL(lum.DSPL_NME,'''')				AS	MGR
				,ISNULL(uwa.ASMT_DT,'''')				AS	ASMT_DT
				--,web.getVendorName(odr.ORDR_ID)		AS	VNDR_NME
				,CASE 
					WHEN odr.ORDR_CAT_ID IN (1,5) THEN ISNULL(lv.CXR_NME,ISNULL(lvo.VNDR_NME,'''')) 
					WHEN odr.ORDR_CAT_ID = 4 THEN ISNULL(lncco.VNDR_NME,ISNULL(lvo.VNDR_NME,''''))
					WHEN odr.ORDR_CAT_ID = 2 THEN ISNULL(web.getWFMVendorName(odr.ORDR_ID),lvo.VNDR_NME)
					ELSE '''' 
				END	AS VNDR_NME
				, CASE 
					WHEN atRTS.STUS_ID = 0 THEN ''YES''
					WHEN onte.NTE_ID > 0 THEN ''YES''
					ELSE ''NO''
				END AS RTSflag
				,ISNULL(fsa.CREAT_DT,odr.CREAT_DT) AS ORDR_RCVD_DT
			From	dbo.ORDR	odr WITH (NOLOCK)
		INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	at.ORDR_ID	=	odr.ORDR_ID
		LEFT JOIN	dbo.MAP_GRP_TASK	mgt	WITH (NOLOCK)	ON	mgt.TASK_ID	=	at.TASK_ID	
		LEFT JOIN	dbo.MAP_PRF_TASK	mpt	WITH (NOLOCK)	ON	at.TASK_ID		=	mpt.TASK_ID
		LEFT JOIN	dbo.FSA_ORDR	fsa WITH (NOLOCK) ON odr.ORDR_ID	= fsa.ORDR_ID
		LEFT JOIN	dbo.IPL_ORDR	ipl	WITH (NOLOCK) ON ipl.ORDR_ID	= odr.ORDR_ID
		LEFT JOIN	dbo.NCCO_ORDR	ncco WITH (NOLOCK) ON ncco.ORDR_ID  = odr.ORDR_ID
		INNER JOIN	dbo.LK_PROD_TYPE	lpt	WITH (NOLOCK)	ON	lpt.PROD_TYPE_ID	=	ipl.PROD_TYPE_ID
															OR	lpt.FSA_PROD_TYPE_CD=	fsa.PROD_TYPE_CD
															OR	lpt.PROD_TYPE_ID	=	ncco.PROD_TYPE_ID
		LEFT JOIN	dbo.LK_ORDR_TYPE	lot WITH (NOLOCK)	ON  lot.FSA_ORDR_TYPE_CD = fsa.ORDR_TYPE_CD
															OR 	lot.ORDR_TYPE_ID = ipl.ORDR_TYPE_ID
															OR  lot.ORDR_TYPE_ID = ncco.ORDR_TYPE_ID
		LEFT JOIN	dbo.USER_WFM_ASMT	uwa WITH (NOLOCK)	ON	uwa.ORDR_ID	=	odr.ORDR_ID
															AND	((uwa.GRP_ID in (1)) OR (uwa.USR_PRF_ID IN (126)))
		LEFT JOIN	dbo.LK_ORDR_ACTN	loa	WITH (NOLOCK)	ON	loa.ORDR_ACTN_ID	=	fsa.ORDR_ACTN_ID													
		LEFT JOIN	dbo.LK_USER			lu	WITH (NOLOCK)	ON	lu.[USER_ID]		=	uwa.ASN_USER_ID											
		LEFT JOIN	dbo.LK_USER			lub	WITH (NOLOCK)	ON	lub.[USER_ID]		=	uwa.ASN_BY_USER_ID
		LEFT JOIN	dbo.LK_USER			lum	WITH (NOLOCK)	ON	lum.USER_ADID		=	lu.MGR_ADID
		LEFT JOIN	dbo.LK_PLTFRM		lp	WITH (NOLOCK)	ON	lp.PLTFRM_CD		=	odr.PLTFRM_CD
		LEFT JOIN	dbo.ACT_TASK		atRTS WITH (NOLOCK)	ON	atRTS.ORDR_ID		=	odr.ORDR_ID
															AND	atRTS.TASK_ID		=	500
															AND	atRTS.STUS_ID		=	0
		LEFT JOIN	dbo.ORDR_NTE		onte WITH (NOLOCK) ON	onte.ORDR_ID		=	fsa.ORDR_ID
															AND	fsa.ORDR_ACTN_ID	=	1
															AND	onte.NTE_TXT		=	''GOM user initiated RTS on this order.''
		LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)			ON	vo.ORDR_ID		=	odr.ORDR_ID
		LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)			ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
		LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)			ON	lvo.VNDR_CD = vf.VNDR_CD
		LEFT JOIN	dbo.IPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	odr.ORDR_ID		=	la.ORDR_ID
																AND	la.ACCS_TYPE_ID	=	''Terminating''
		LEFT JOIN	dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD	
		LEFT JOIN	dbo.LK_VNDR				lncco WITH (NOLOCK)			ON	ncco.VNDR_CD	=	lncco.VNDR_CD																														
			WHERE	((mgt.GRP_ID	=	1) OR (mpt.PRNT_PRF_ID=114))
				AND	at.STUS_ID	=	0'
	END
ELSE IF @IsOrderInGOMWG	= 0
	BEGIN
		SET @SQLStr =
		'
		INSERT INTO #GOMWFMTmp (ORDR_ID,FTN,PROD_TYPE,ORDR_TYPE,PLTFM_TYPE,SUB_STATUS,
									ASSIGNED_USER,ASSIGNED_BY,MGR,ASMT_DT,
									VNDR_NAME,RTSflag,ORDR_RCVD_DT)
			Select	DISTINCT 
				odr.ORDR_ID							AS	ORDR_ID
				,ISNULL(fsa.FTN,odr.ORDR_ID)		AS	FTN
				,ISNULL(lpt.PROD_TYPE_DES,'''')		AS	PROD_TYPE
				,ISNULL(lot.ORDR_TYPE_DES,'''')			AS	ORDR_TYPE
				,ISNULL(lp.PLTFRM_NME,'''')			AS	PLTFM_TYPE	
				,ISNULL(loa.ORDR_ACTN_DES,''Submit'')	AS SUB_STATUS
				,ISNULL(lu.DSPL_NME,'''')			AS	ASSIGNED_USER
				,ISNULL(lub.DSPL_NME,'''')			AS	ASSIGNED_BY
				,ISNULL(lum.DSPL_NME,'''')			AS	MGR
				,ISNULL(uwa.ASMT_DT,'''')			AS	ASMT_DT
				--,web.getVendorName(odr.ORDR_ID)	AS	VNDR_NME
				,CASE 
					WHEN odr.ORDR_CAT_ID IN (1,5) THEN ISNULL(lv.CXR_NME,ISNULL(lvo.VNDR_NME,'''')) 
					WHEN odr.ORDR_CAT_ID = 4 THEN ISNULL(lncco.VNDR_NME,ISNULL(lvo.VNDR_NME,''''))
					WHEN odr.ORDR_CAT_ID = 2 THEN ISNULL(web.getWFMVendorName(odr.ORDR_ID),lvo.VNDR_NME)
					ELSE '''' 
				END	AS VNDR_NME
				, CASE 
					WHEN atRTS.STUS_ID = 0 THEN ''YES''
					WHEN onte.NTE_ID > 0 THEN ''YES''
					ELSE ''NO''
				END AS RTSflag
				,ISNULL(fsa.CREAT_DT,odr.CREAT_DT) AS ORDR_RCVD_DT
			From	dbo.ORDR	odr WITH (NOLOCK)
		LEFT JOIN	dbo.FSA_ORDR	fsa WITH (NOLOCK) ON odr.ORDR_ID	= fsa.ORDR_ID
		LEFT JOIN	dbo.IPL_ORDR	ipl	WITH (NOLOCK) ON ipl.ORDR_ID	= odr.ORDR_ID
		LEFT JOIN	dbo.NCCO_ORDR	ncco WITH (NOLOCK) ON ncco.ORDR_ID  = odr.ORDR_ID
		INNER JOIN	dbo.LK_PROD_TYPE	lpt	WITH (NOLOCK)	ON	lpt.PROD_TYPE_ID	=	ipl.PROD_TYPE_ID
															OR	lpt.FSA_PROD_TYPE_CD=	fsa.PROD_TYPE_CD
															OR	lpt.PROD_TYPE_ID	=	ncco.PROD_TYPE_ID
		LEFT JOIN	dbo.LK_ORDR_TYPE	lot WITH (NOLOCK)	ON  lot.FSA_ORDR_TYPE_CD = fsa.ORDR_TYPE_CD
															OR 	lot.ORDR_TYPE_ID = ipl.ORDR_TYPE_ID
															OR  lot.ORDR_TYPE_ID = ncco.ORDR_TYPE_ID
		LEFT JOIN	dbo.USER_WFM_ASMT	uwa WITH (NOLOCK)	ON	uwa.ORDR_ID	=	odr.ORDR_ID
															AND	((uwa.GRP_ID in (1)) OR (uwa.USR_PRF_ID IN (126)))
		--INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	at.ORDR_ID	=	odr.ORDR_ID
		--INNER JOIN	dbo.MAP_GRP_TASK	mgt	WITH (NOLOCK)	ON	mgt.TASK_ID	=	at.TASK_ID	
		LEFT JOIN	dbo.LK_ORDR_ACTN	loa	WITH (NOLOCK)	ON	loa.ORDR_ACTN_ID	=	fsa.ORDR_ACTN_ID													
		LEFT JOIN	dbo.LK_USER			lu	WITH (NOLOCK)	ON	lu.[USER_ID]		=	uwa.ASN_USER_ID											
		LEFT JOIN	dbo.LK_USER			lub	WITH (NOLOCK)	ON	lub.[USER_ID]		=	uwa.ASN_BY_USER_ID
		LEFT JOIN	dbo.LK_USER			lum	WITH (NOLOCK)	ON	lum.USER_ADID		=	lu.MGR_ADID
		LEFT JOIN	dbo.LK_PLTFRM		lp	WITH (NOLOCK)	ON	lp.PLTFRM_CD		=	odr.PLTFRM_CD
		LEFT JOIN	dbo.ACT_TASK		atRTS WITH (NOLOCK)	ON	atRTS.ORDR_ID		=	odr.ORDR_ID
															AND	atRTS.TASK_ID		=	500
															AND	atRTS.STUS_ID		=	0
		LEFT JOIN	dbo.ORDR_NTE		onte WITH (NOLOCK) ON	onte.ORDR_ID		=	fsa.ORDR_ID
															AND	fsa.ORDR_ACTN_ID	=	1
															AND	onte.NTE_TXT		=	''GOM user initiated RTS on this order.''
		LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)			ON	vo.ORDR_ID		=	odr.ORDR_ID
		LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)			ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
		LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)			ON	lvo.VNDR_CD = vf.VNDR_CD
		LEFT JOIN	dbo.IPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	odr.ORDR_ID		=	la.ORDR_ID
																AND	la.ACCS_TYPE_ID	=	''Terminating''
		LEFT JOIN	dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD	
		LEFT JOIN	dbo.LK_VNDR				lncco WITH (NOLOCK)			ON	ncco.VNDR_CD	=	lncco.VNDR_CD																												
		 WHERE	NOT EXISTS
				(
					SELECT ''X''
						FROM	dbo.ACT_TASK		at WITH (NOLOCK)
					LEFT JOIN	dbo.MAP_GRP_TASK	mgt WITH (NOLOCK)	ON	mgt.TASK_ID	=	at.TASK_ID
					LEFT JOIN	dbo.MAP_PRF_TASK	mpt	WITH (NOLOCK)	ON	at.TASK_ID		=	mpt.TASK_ID
						WHERE	at.ORDR_ID	=	odr.ORDR_ID
							AND	((mgt.GRP_ID	=	1) OR (mpt.PRNT_PRF_ID=114))
							AND	at.STUS_ID	=	0
				)
			AND	odr.ORDR_STUS_ID IN (0,1)
			AND odr.ORDR_CAT_ID != 6
				'
	END
ELSE
	BEGIN
		SET @SQLStr1 =
		'
		INSERT INTO #GOMWFMTmp (ORDR_ID,FTN,PROD_TYPE,ORDR_TYPE,PLTFM_TYPE,SUB_STATUS,
									ASSIGNED_USER,ASSIGNED_BY,MGR,ASMT_DT,
									VNDR_NAME,RTSflag,ORDR_RCVD_DT)
		Select	DISTINCT 
				odr.ORDR_ID								AS	ORDR_ID
				,ISNULL(fsa.FTN,odr.ORDR_ID)			AS	FTN
				,ISNULL(lpt.PROD_TYPE_DES,'''')			AS	PROD_TYPE
				,ISNULL(lot.ORDR_TYPE_DES,'''')			AS	ORDR_TYPE
				,ISNULL(lp.PLTFRM_NME,'''')				AS	PLTFM_TYPE	
				,ISNULL(loa.ORDR_ACTN_DES,''Submit'')	AS SUB_STATUS
				,ISNULL(lu.DSPL_NME,'''')		AS	ASSIGNED_USER
				,ISNULL(lub.DSPL_NME,'''')	AS	ASSIGNED_BY
				,ISNULL(lum.DSPL_NME,'''')	AS	MGR
				,ISNULL(uwa.ASMT_DT,'''')		AS	ASMT_DT
				--,web.getVendorName(odr.ORDR_ID)	AS	VNDR_NME
				,CASE 
					WHEN odr.ORDR_CAT_ID IN (1,5) THEN ISNULL(lv.CXR_NME,ISNULL(lvo.VNDR_NME,'''')) 
					WHEN odr.ORDR_CAT_ID = 4 THEN ISNULL(lncco.VNDR_NME,ISNULL(lvo.VNDR_NME,''''))
					WHEN odr.ORDR_CAT_ID = 2 THEN ISNULL(web.getWFMVendorName(odr.ORDR_ID),lvo.VNDR_NME)
					ELSE '''' 
				END	AS VNDR_NME
				, CASE 
					WHEN atRTS.STUS_ID = 0 THEN ''YES''
					WHEN onte.NTE_ID > 0 THEN ''YES''
					ELSE ''NO''
				END AS RTSflag
				,ISNULL(fsa.CREAT_DT,odr.CREAT_DT) AS ORDR_RCVD_DT
			From	dbo.ORDR	odr WITH (NOLOCK)
		INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	at.ORDR_ID	=	odr.ORDR_ID
		LEFT JOIN	dbo.MAP_GRP_TASK	mgt	WITH (NOLOCK)	ON	mgt.TASK_ID	=	at.TASK_ID
		LEFT JOIN	dbo.MAP_PRF_TASK	mpt	WITH (NOLOCK)	ON	at.TASK_ID		=	mpt.TASK_ID
		LEFT JOIN	dbo.FSA_ORDR	fsa WITH (NOLOCK) ON odr.ORDR_ID	= fsa.ORDR_ID
		LEFT JOIN	dbo.IPL_ORDR	ipl	WITH (NOLOCK) ON ipl.ORDR_ID	= odr.ORDR_ID
		LEFT JOIN	dbo.NCCO_ORDR	ncco WITH (NOLOCK) ON ncco.ORDR_ID  = odr.ORDR_ID
		INNER JOIN	dbo.LK_PROD_TYPE	lpt	WITH (NOLOCK)	ON	lpt.PROD_TYPE_ID	=	ipl.PROD_TYPE_ID
															OR	lpt.FSA_PROD_TYPE_CD=	fsa.PROD_TYPE_CD
															OR	lpt.PROD_TYPE_ID	=	ncco.PROD_TYPE_ID
		LEFT JOIN	dbo.LK_ORDR_TYPE	lot WITH (NOLOCK)	ON  lot.FSA_ORDR_TYPE_CD = fsa.ORDR_TYPE_CD
															OR 	lot.ORDR_TYPE_ID = ipl.ORDR_TYPE_ID
															OR  lot.ORDR_TYPE_ID = ncco.ORDR_TYPE_ID
		LEFT JOIN	dbo.USER_WFM_ASMT	uwa WITH (NOLOCK)	ON	uwa.ORDR_ID	=	odr.ORDR_ID
															AND	((uwa.GRP_ID in (1)) OR (uwa.USR_PRF_ID IN (126)))	
		LEFT JOIN	dbo.LK_ORDR_ACTN	loa	WITH (NOLOCK)	ON	loa.ORDR_ACTN_ID	=	fsa.ORDR_ACTN_ID													
		LEFT JOIN	dbo.LK_USER			lu	WITH (NOLOCK)	ON	lu.[USER_ID]		=	uwa.ASN_USER_ID											
		LEFT JOIN	dbo.LK_USER			lub	WITH (NOLOCK)	ON	lub.[USER_ID]		=	uwa.ASN_BY_USER_ID
		LEFT JOIN	dbo.LK_USER			lum	WITH (NOLOCK)	ON	lum.USER_ADID		=	lu.MGR_ADID
		LEFT JOIN	dbo.LK_PLTFRM		lp	WITH (NOLOCK)	ON	lp.PLTFRM_CD		=	odr.PLTFRM_CD
		LEFT JOIN	dbo.ACT_TASK		atRTS WITH (NOLOCK)	ON	atRTS.ORDR_ID		=	odr.ORDR_ID
															AND	atRTS.TASK_ID		=	500
															AND	atRTS.STUS_ID		=	0
		LEFT JOIN	dbo.ORDR_NTE		onte WITH (NOLOCK) ON	onte.ORDR_ID		=	fsa.ORDR_ID
															AND	fsa.ORDR_ACTN_ID	=	1
															AND	onte.NTE_TXT		=	''GOM user initiated RTS on this order.''
		LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)			ON	vo.ORDR_ID		=	odr.ORDR_ID
		LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)			ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
		LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)			ON	lvo.VNDR_CD = vf.VNDR_CD
		LEFT JOIN	dbo.IPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	odr.ORDR_ID		=	la.ORDR_ID
																AND	la.ACCS_TYPE_ID	=	''Terminating''
		LEFT JOIN	dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD	
		LEFT JOIN	dbo.LK_VNDR				lncco WITH (NOLOCK)			ON	ncco.VNDR_CD	=	lncco.VNDR_CD																												
			WHERE	((mgt.GRP_ID	=	1) OR (mpt.PRNT_PRF_ID=114))
				AND	at.STUS_ID	=	0'
				
		SET @SQLStr2 = 
		
		'
		INSERT INTO #GOMWFMTmp (ORDR_ID,FTN,PROD_TYPE,ORDR_TYPE,PLTFM_TYPE,SUB_STATUS,
									ASSIGNED_USER,ASSIGNED_BY,MGR,ASMT_DT,
									VNDR_NAME,RTSflag,ORDR_RCVD_DT)
		Select	DISTINCT 
				odr.ORDR_ID AS ORDR_ID
				,ISNULL(fsa.FTN,odr.ORDR_ID) AS	FTN
				,ISNULL(lpt.PROD_TYPE_DES,'''')		AS	PROD_TYPE
				,ISNULL(lot.ORDR_TYPE_DES,'''')		AS	ORDR_TYPE
				,ISNULL(lp.PLTFRM_NME,'''')			AS	PLTFM_TYPE	
				,ISNULL(loa.ORDR_ACTN_DES,''Submit'')	AS SUB_STATUS
				,ISNULL(lu.DSPL_NME,'''')		AS	ASSIGNED_USER
				,ISNULL(lub.DSPL_NME,'''')	AS	ASSIGNED_BY
				,ISNULL(lum.DSPL_NME,'''')	AS	MGR
				,ISNULL(uwa.ASMT_DT,'''')		AS	ASMT_DT
				--,web.getVendorName(odr.ORDR_ID)	AS	VNDR_NME
				,CASE 
					WHEN odr.ORDR_CAT_ID IN (1,5) THEN ISNULL(lv.CXR_NME,ISNULL(lvo.VNDR_NME,'''')) 
					WHEN odr.ORDR_CAT_ID = 4 THEN ISNULL(lncco.VNDR_NME,ISNULL(lvo.VNDR_NME,''''))
					WHEN odr.ORDR_CAT_ID = 2 THEN ISNULL(web.getWFMVendorName(odr.ORDR_ID),lvo.VNDR_NME)
					ELSE '''' 
				END	AS VNDR_NME
				, CASE 
					WHEN atRTS.STUS_ID = 0 THEN ''YES''
					WHEN onte.NTE_ID > 0 THEN ''YES''
					ELSE ''NO''
				END AS RTSflag
				,ISNULL(fsa.CREAT_DT,odr.CREAT_DT) AS ORDR_RCVD_DT
			From	dbo.ORDR	odr WITH (NOLOCK)
		LEFT JOIN	dbo.FSA_ORDR	fsa WITH (NOLOCK) ON odr.ORDR_ID	= fsa.ORDR_ID
		LEFT JOIN	dbo.IPL_ORDR	ipl	WITH (NOLOCK) ON ipl.ORDR_ID	= odr.ORDR_ID
		LEFT JOIN	dbo.NCCO_ORDR	ncco WITH (NOLOCK) ON ncco.ORDR_ID  = odr.ORDR_ID
		LEFT JOIN	dbo.USER_WFM_ASMT	uwa WITH (NOLOCK)	ON	uwa.ORDR_ID	=	odr.ORDR_ID
															AND	((uwa.GRP_ID in (1)) OR (uwa.USR_PRF_ID IN (126)))	
		--INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	at.ORDR_ID	=	odr.ORDR_ID
		--INNER JOIN	dbo.MAP_GRP_TASK	mgt	WITH (NOLOCK)	ON	mgt.TASK_ID	=	at.TASK_ID	
		INNER JOIN	dbo.LK_PROD_TYPE	lpt	WITH (NOLOCK)	ON	lpt.PROD_TYPE_ID	=	ipl.PROD_TYPE_ID
															OR	lpt.FSA_PROD_TYPE_CD=	fsa.PROD_TYPE_CD
															OR	lpt.PROD_TYPE_ID	=	ncco.PROD_TYPE_ID
		LEFT JOIN	dbo.LK_ORDR_TYPE	lot WITH (NOLOCK)	ON  lot.FSA_ORDR_TYPE_CD = fsa.ORDR_TYPE_CD
															OR 	lot.ORDR_TYPE_ID = ipl.ORDR_TYPE_ID
															OR  lot.ORDR_TYPE_ID = ncco.ORDR_TYPE_ID
		LEFT JOIN	dbo.LK_ORDR_ACTN	loa	WITH (NOLOCK)	ON	loa.ORDR_ACTN_ID	=	fsa.ORDR_ACTN_ID													
		LEFT JOIN	dbo.LK_USER			lu	WITH (NOLOCK)	ON	lu.[USER_ID]		=	uwa.ASN_USER_ID											
		LEFT JOIN	dbo.LK_USER			lub	WITH (NOLOCK)	ON	lub.[USER_ID]		=	uwa.ASN_BY_USER_ID
		LEFT JOIN	dbo.LK_USER			lum	WITH (NOLOCK)	ON	lum.USER_ADID		=	lu.MGR_ADID
		LEFT JOIN	dbo.LK_PLTFRM		lp	WITH (NOLOCK)	ON	lp.PLTFRM_CD		=	odr.PLTFRM_CD
		LEFT JOIN	dbo.ACT_TASK		atRTS WITH (NOLOCK)	ON	atRTS.ORDR_ID		=	odr.ORDR_ID
															AND	atRTS.TASK_ID		=	500
															AND	atRTS.STUS_ID		=	0
		LEFT JOIN	dbo.ORDR_NTE		onte WITH (NOLOCK) ON	onte.ORDR_ID		=	fsa.ORDR_ID
															AND	fsa.ORDR_ACTN_ID	=	1
															AND	onte.NTE_TXT		=	''GOM user initiated RTS on this order.''
		LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)			ON	vo.ORDR_ID		=	odr.ORDR_ID
		LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)			ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
		LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)			ON	lvo.VNDR_CD = vf.VNDR_CD
		LEFT JOIN	dbo.IPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	odr.ORDR_ID		=	la.ORDR_ID
																AND	la.ACCS_TYPE_ID	=	''Terminating''
		LEFT JOIN	dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD	
		LEFT JOIN	dbo.LK_VNDR				lncco WITH (NOLOCK)			ON	ncco.VNDR_CD	=	lncco.VNDR_CD															
		 WHERE	NOT EXISTS
				(
					SELECT ''X''
						FROM	dbo.ACT_TASK		at WITH (NOLOCK)
					LEFT JOIN	dbo.MAP_GRP_TASK	mgt WITH (NOLOCK)	ON	mgt.TASK_ID	=	at.TASK_ID
					LEFT JOIN	dbo.MAP_PRF_TASK	mpt	WITH (NOLOCK)	ON	at.TASK_ID		=	mpt.TASK_ID
						WHERE	at.ORDR_ID	=	odr.ORDR_ID
							AND	((mgt.GRP_ID	=	1) OR (mpt.PRNT_PRF_ID=114))
							AND	at.STUS_ID	=	0
				)
			AND	odr.ORDR_STUS_ID IN (0,1)	
			AND odr.ORDR_CAT_ID != 6
				' 
	END
	
SET @SQLStr3 = '	SELECT COUNT(1) AS CNT,  ASSIGNED_USER, CONCAT(ASSIGNED_USER, '' ('', COUNT(1), '')'') as ASSIGNED_USER_CNT
						FROM #GOMWFMTmp	t WITH (NOLOCK)
					INNER JOIN dbo.LK_USER lu ON t.ASSIGNED_USER = lu.DSPL_NME
						WHERE lu.REC_STUS_ID IN (1,2)
						GROUP BY ASSIGNED_USER	
						ORDER BY ASSIGNED_USER'
		
		IF (@IsOrderInGOMWG <> 2)
			BEGIN
				IF @whereClause <> '' 
					SET @SQLStr = @SQLStr + ' SELECT * FROM #GOMWFMTmp	WITH (NOLOCK) ' + @whereClause + @sortBy
				ELSE
					SET @SQLStr = @SQLStr + ' SELECT * FROM #GOMWFMTmp	WITH (NOLOCK) ' + @sortBy
			END
		ELSE
			BEGIN
				IF @whereClause <> '' 
					SET @SQLStr = @SQLStr1 + @SQLStr2 + ' SELECT * FROM #GOMWFMTmp	WITH (NOLOCK) ' + @whereClause + @sortBy
				ELSE
					SET @SQLStr = @SQLStr1 + @SQLStr2 + ' SELECT * FROM #GOMWFMTmp	WITH (NOLOCK) ' + @sortBy
			END



PRINT @SQLStr
PRINT @SQLStr3
	
EXEC sp_executesql @SQLStr 
EXEC sp_executesql @SQLStr3	
--select * from dbo.USER_WFM

END TRY

BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH

END
