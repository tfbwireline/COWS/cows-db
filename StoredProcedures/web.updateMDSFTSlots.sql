USE COWS
GO
_CreateObject 'SP','web','updateMDSFTSlots'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================                            
-- Author:  Jagannath Gangi
-- Create date: 2016-2-1                            
-- Description: This SP is used to update MDS Event FT Slots Data              
-- =========================================================                            
                            
ALTER PROCEDURE [web].[updateMDSFTSlots]
@EVENT_ID BIGINT,
@MDS_FAST_TRK_TYPE_ID CHAR(1),
@EVENT_TITLE_TXT VARCHAR(255),
@STRT_TMST DATETIME,
@CNFRC_BRDG_NBR VARCHAR(50),
@CNFRC_PIN_NBR VARCHAR(20),
@CREAT_BY_USER_ID INT,
@TME_SLOT_ID CHAR(2),
@WRKFLW_STUS_ID TINYINT,
@EVENT_STUS_ID TINYINT,
@UpdateCd BIT
AS                          
                        
BEGIN                        
 SET NOCOUNT ON;                        
 
 BEGIN TRY                        
  DECLARE @OLD_STRT_TMST DATETIME                      
  DECLARE @OLD_TME_SLOT_ID TinyInt        
  DECLARE @NEW_TME_SLOT_ID TinyInt  
  DECLARE @OLD_WRKFLW_STUS_ID TINYINT
  DECLARE @OLD_MDS_FAST_TRK_TYPE_ID CHAR(1)
  DECLARE @ASN_USER_ID INT
  
  SET @NEW_TME_SLOT_ID =  CONVERT(TINYINT,CASE ISNULL(@TME_SLOT_ID,'0') WHEN '' THEN '0' ELSE @TME_SLOT_ID END)
  
 SELECT @OLD_STRT_TMST = STRT_TMST, @OLD_TME_SLOT_ID = ISNULL(TME_SLOT_ID, 0), @OLD_MDS_FAST_TRK_TYPE_ID = ISNULL(MDS_FAST_TRK_TYPE_ID, ''), @OLD_WRKFLW_STUS_ID = WRKFLW_STUS_ID  FROM dbo.MDS_EVENT WITH (NOLOCK) WHERE (EVENT_ID = @EVENT_ID)
 
 IF (((@WRKFLW_STUS_ID=2) AND (@EVENT_STUS_ID=1)) OR ((@OLD_WRKFLW_STUS_ID = 9) AND (@EVENT_STUS_ID=3) AND (@WRKFLW_STUS_ID=2)))
		BEGIN
			SET @OLD_STRT_TMST = NULL
			SET @OLD_TME_SLOT_ID = 0
		END
 
 IF (@OLD_MDS_FAST_TRK_TYPE_ID = 'A')
	SET @OLD_TME_SLOT_ID = 0
 
IF ((@MDS_FAST_TRK_TYPE_ID = '') AND (@OLD_MDS_FAST_TRK_TYPE_ID != ''))
BEGIN
	EXEC [dbo].[deleteMDSSlotAvailibility]
		0,
		@EVENT_ID,
		@OLD_STRT_TMST,
		@OLD_TME_SLOT_ID
END
                             
IF (ISNULL(@MDS_FAST_TRK_TYPE_ID, '') = 'A')
BEGIN
	IF ((ISNULL(@OLD_MDS_FAST_TRK_TYPE_ID, '') = 'S') AND (@MDS_FAST_TRK_TYPE_ID != ''))
	BEGIN
		EXEC [dbo].[deleteMDSSlotAvailibility]
		0,
		@EVENT_ID,
		@OLD_STRT_TMST,
		@OLD_TME_SLOT_ID
	END

	DELETE FROM dbo.APPT WITH (ROWLOCK) WHERE [EVENT_ID] = @EVENT_ID
	INSERT INTO dbo.APPT WITH (ROWLOCK)    
		   ([EVENT_ID]    
		   ,[SUBJ_TXT]    
		   ,[DES]    
		   ,[STRT_TMST]    
		   ,[END_TMST]    
		   ,[APPT_LOC_TXT]    
		   ,[APPT_TYPE_ID]    
		   ,[RCURNC_DES_TXT]    
		   ,[CREAT_BY_USER_ID]    
		   ,[CREAT_DT]    
		   ,[ASN_TO_USER_ID_LIST_TXT]    
		   ,[RCURNC_CD]
		   ,[REC_STUS_ID])     
		SELECT  @EVENT_ID    
		   ,@EVENT_TITLE_TXT    
		   ,@EVENT_TITLE_TXT 
		   ,(CONVERT(DATETIME, @STRT_TMST, 101) + convert(DATETIME,'00:00:00'))
		   ,(CONVERT(DATETIME, @STRT_TMST, 101) + convert(DATETIME,'01:00:00'))
		   ,@CNFRC_BRDG_NBR + ' - ' + @CNFRC_PIN_NBR    
		   ,5    
		   ,NULL    
		   ,@CREAT_BY_USER_ID    
		   ,getDate()    
		   ,'<ResourceIds>  <ResourceId Type="System.Int32" Value="5001" />  </ResourceIds>'    
		   ,0 
		   ,1
END

                          
IF (ISNULL(@NEW_TME_SLOT_ID, 0) != 0)  AND  (ISNULL(@MDS_FAST_TRK_TYPE_ID, '') != '') AND (ISNULL(@MDS_FAST_TRK_TYPE_ID, '') != 'A') AND ((@WRKFLW_STUS_ID IN (2, 4)) OR ((@WRKFLW_STUS_ID = 3) AND (@EVENT_STUS_ID IN (2,4))) OR ((@WRKFLW_STUS_ID = 8) AND (@EVENT_STUS_ID IN (2,3,4))) OR ((@WRKFLW_STUS_ID = 5) AND (@EVENT_STUS_ID = 5)))      
  BEGIN                      
   -- Randomise()                    
   SELECT TOP 1 @ASN_USER_ID = [USER_ID]                    
   FROM dbo.EVENT_AVAL_USER WITH (NOLOCK)                    
   WHERE TME_SLOT_ID = @NEW_TME_SLOT_ID                    
   ORDER BY NEWID()                    
                    
   EXEC [dbo].[updateMDSSlotAvailibility]                     
     @NEW_TME_SLOT_ID                    
     ,@STRT_TMST                    
     ,@ASN_USER_ID                    
     ,@CREAT_BY_USER_ID                    
     ,@EVENT_TITLE_TXT                    
     ,@CNFRC_BRDG_NBR                    
     ,@CNFRC_PIN_NBR                    
     ,@EVENT_ID                    
     ,@UpdateCd                    
     ,@OLD_STRT_TMST                    
     ,@OLD_TME_SLOT_ID
	 --,@IsReviewer 
	 
  END
  
                        
 END TRY                          
 BEGIN CATCH                          
  EXEC [dbo].[insertErrorInfo]                            
  DECLARE @ErrMsg nVarchar(4000),                             
    @ErrSeverity Int                                  
  SELECT @ErrMsg  = ERROR_MESSAGE(),                                  
         @ErrSeverity = ERROR_SEVERITY()                                  
  RAISERROR(@ErrMsg, @ErrSeverity, 1)                                  
 END CATCH                            
END 