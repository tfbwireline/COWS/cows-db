USE [COWS]
GO
_CreateObject 'SP','dbo','getNCCOOrdersToEmail'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <09/18/2012>
-- Description:	<To email NCCO order upon submission>
-- Author:		<MD M Monir>
-- Create date: <02/21/2017>
-- Description:	 add EMAIL_REQ_TYPE   to the select List as it is being looked at  COWS.EmailSender\EmailSender\EmailSenderIO.cs
--               private void CreateAndSendNCCOOrders(DataRow i)
--              _EmailBody = _XHelper.transformXMLtoHTML(_Email.ToString(), i["EMAIL_REQ_TYPE"].ToString(), 0);

-- =============================================
ALTER PROCEDURE [dbo].[getNCCOOrdersToEmail]
AS
BEGIN
	BEGIN TRY
		
		DECLARE @t Table (ORDR_ID INT)
		Declare @tmpids table (tmpid int)
		INSERT INTO @t
			SELECT DISTINCT ncco.ORDR_ID
			FROM	dbo.NCCO_ORDR ncco WITH (NOLOCK) LEFT JOIN
			dbo.EMAIL_REQ er WITH (NOLOCK) ON er.ORDR_ID =	ncco.ORDR_ID
			WHERE	ISNULL(ncco.EMAIL_ADR,'') != ''
			 AND er.EMAIL_REQ_ID IS NULL  
		
		INSERT INTO dbo.EMAIL_REQ (ORDR_ID,EMAIL_REQ_TYPE_ID,STUS_ID,CREAT_DT) output inserted.EMAIL_REQ_ID into @tmpids
			SELECT ORDR_ID,13,10,GETDATE()
				FROM @t
		
		SELECT	lot.ORDR_TYPE_DES	AS	ORDR_TYPE_DESC,
				lpt.PROD_TYPE_DES	AS 	PROD_TYPE_DES,
				luser.FULL_NME		AS	FULL_NME,
				lctry.CTRY_NME		AS	CTRY_NME,
				lvndr.VNDR_NME		AS	VNDR_NME,
				13					AS	EMAIL_REQ_TYPE_ID,
				ncco.ORDR_ID		AS	ORDR_ID,	
				ncco.CUST_NME	AS	CUST_NME,
				ncco.SITE_CITY_NME	AS	SITE_CITY_NME,
				ncco.H5_ACCT_NBR	AS	H5_ACCT_NBR,
				ncco.SOL_NME	AS	SOL_NME,	
				ncco.OE_NME	AS	OE_NME,
				ncco.SOTS_NBR	AS	SOTS_NBR,
				ncco.BILL_CYC_NBR	AS	BILL_CYC_NBR,
				ncco.CWD_DT	AS	CWD_DT,
				ncco.CCS_DT	AS	CCS_DT,
				REPLACE(ncco.EMAIL_ADR,';',',') AS	EMAIL_ADR,
				ncco.CMNT_TXT	AS	CMNT_TXT,
				tmp.tmpid	AS	EMAIL_REQ_ID
				,'NCCOOrderEmail' AS [EMAIL_REQ_TYPE]
			FROM	dbo.NCCO_ORDR ncco WITH (NOLOCK)
		INNER JOIN	@t	t ON t.ORDR_ID = ncco.ORDR_ID
		INNER JOIN	dbo.LK_ORDR_TYPE lot WITH (NOLOCK) ON ncco.ORDR_TYPE_ID	= lot.ORDR_TYPE_ID
		INNER JOIN	dbo.LK_PROD_TYPE lpt WITH (NOLOCK) ON lpt.PROD_TYPE_ID	=	ncco.PROD_TYPE_ID
		INNER JOIN	dbo.LK_USER	luser WITH (NOLOCK) ON luser.[USER_ID] = ncco.CREAT_BY_USER_ID
		LEFT JOIN	dbo.LK_CTRY	lctry WITH (NOLOCK) ON	ncco.CTRY_CD	=	lctry.CTRY_CD
		LEFT JOIN	dbo.LK_VNDR	lvndr WITH (NOLOCK) ON	lvndr.VNDR_CD	=	ncco.VNDR_CD
		CROSS JOIN	@tmpids tmp
			
	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
END
