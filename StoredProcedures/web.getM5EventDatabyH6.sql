USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getM5EventDatabyH6]    Script Date: 1/18/2022 11:28:33 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  jrg7298            
-- Create date: 07/30/2015         
-- Description: Get the required data for an UCaaS Event when a H6 value is entered.            
-- =============================================            
--927219423, 2015-05-06 00:00:00.000
ALTER PROCEDURE [web].[getM5EventDatabyH6] --'928662703','08/12/2020', 'N'        
 @H6  VARCHAR(9) = '',
 @CCD VARCHAR(10) = '',
 @UCaaSCD CHAR(1) = '',
 @CEFlg BIT = 0,
 @IsNewUI BIT = 0 
AS            
BEGIN            
SET NOCOUNT ON;
BEGIN TRY

	DECLARE @M5_CD BIT = 0
	DECLARE @NRMBPM_CD BIT = 0
	DECLARE @SSTAT_WFM_CD BIT = 0

	BEGIN TRY
		exec sp_testlinkedserver M5
		SET @M5_CD = 1
	END TRY
	BEGIN CATCH
		SET @M5_CD = 0
	END CATCH
	
	BEGIN TRY
		exec sp_testlinkedserver NRMBPM
		SET @NRMBPM_CD = 1
	END TRY
	BEGIN CATCH
		SET @NRMBPM_CD = 0
	END CATCH
	
	BEGIN TRY
		exec sp_testlinkedserver SSTAT_WFM
		SET @SSTAT_WFM_CD = 1
	END TRY
	BEGIN CATCH
		SET @SSTAT_WFM_CD = 0
	END CATCH
	
	IF OBJECT_ID(N'tempdb..#M5CPETable', N'U') IS NOT NULL         
		DROP TABLE #M5CPETable 

	IF OBJECT_ID(N'tempdb..#M5CPEFmlyTbl', N'U') IS NOT NULL         
		DROP TABLE #M5CPEFmlyTbl 

	IF OBJECT_ID(N'tempdb..#M5CUSTTable', N'U') IS NOT NULL         
		DROP TABLE #M5CUSTTable

	IF OBJECT_ID(N'tempdb..#M5ADRTable', N'U') IS NOT NULL         
		DROP TABLE #M5ADRTable
		
	IF OBJECT_ID(N'tempdb..#NRMWIRED', N'U') IS NOT NULL         
		DROP TABLE #NRMWIRED
		
	IF OBJECT_ID(N'tempdb..#M5MNSTABLE', N'U') IS NOT NULL         
		DROP TABLE #M5MNSTABLE

	IF OBJECT_ID(N'tempdb..#M5RELMNSTABLE', N'U') IS NOT NULL         
		DROP TABLE #M5RELMNSTABLE

	IF OBJECT_ID(N'tempdb..#M5SITEIDTABLE', N'U') IS NOT NULL         
		DROP TABLE #M5SITEIDTABLE
		
	IF OBJECT_ID(N'tempdb..#M5PORTTable', N'U') IS NOT NULL         
		DROP TABLE #M5PORTTable 
	
	IF OBJECT_ID(N'tempdb..#M5H1BasedOnH6', N'U') IS NOT NULL         
		DROP TABLE #M5H1BasedOnH6 
		
	CREATE TABLE #M5ADRTable
	(M5_ORDR_NBR VARCHAR(50) NULL,
	CIS_HIER_LVL_CD VARCHAR(2) NULL,
	SITE_ID VARCHAR(20) NULL,
	LINE_1	VARCHAR(255) NULL,
	LINE_2	VARCHAR(255) NULL,
	LINE_3	VARCHAR(255) NULL,
	CTY_NME	VARCHAR(255) NULL,
	ST_PRVN_CD	VARCHAR(20) NULL,
	PSTL_CD	VARCHAR(15) NULL,
	CTRY_CD	VARCHAR(50) NULL,
	BLDG_ID	VARCHAR(255) NULL,
	ROOM_ID	VARCHAR(255) NULL,
	FLOOR_ID	VARCHAR(255) NULL)

	CREATE TABLE #M5CUSTTable
	( M5_ORDR_NBR VARCHAR(50) NULL,
	INST_CNTCT_NME	VARCHAR(511) NULL,
	SRVC_CNTCT_NME	VARCHAR(511) NULL,
	INST_DOM_INTL_IND_CD	CHAR(10) NULL,
	INST_DOM_PHN_NBR	VARCHAR(20) NULL,
	SRVC_DOM_INTL_IND_CD	CHAR(10) NULL,
	SRVC_DOM_PHN_NBR	VARCHAR(20) NULL,
	SRVC_TME_ZN_CD		VARCHAR(100) NULL,
	SRVC_AVLBLTY_HRS	VARCHAR(50) NULL)

	CREATE TABLE #M5CPETable
	 (M5_ORDR_NBR   VARCHAR(50)   NULL
	 ,COWS_ORDR_ID  INT NULL
	 ,ORDR_ID  VARCHAR(20) NULL
	 ,CUST_ACCT_ID INT NULL
	 ,DEVICE_ID VARCHAR(20) NULL
	 ,ASSOC_H6 VARCHAR(20) NULL
	 ,VNDR_PO VARCHAR(20) NULL
	 ,RQSTN_NBR VARCHAR(20) NULL
	 ,CTRCT_TYPE VARCHAR(100) NULL
	 ,CCD VARCHAR(20) NULL
	 ,CMPNT_STUS VARCHAR(255) NULL
	 ,RCPT_STUS VARCHAR(50) NULL
	 ,SITE_ID VARCHAR(50) NULL
	 ,DLVY_CLLI VARCHAR(20) NULL
	 ,CPE_CMPN_FMLY VARCHAR(2000) NULL
	 ,CPE_MFR_PART_NBR VARCHAR(100) NULL
	 ,NID_SERIAL_NBR VARCHAR(511) NULL)  

	  CREATE TABLE #M5CPEFmlyTbl
	 (M5_ORDR_NBR   VARCHAR(50)   NULL            
	 ,ORDR_ID  VARCHAR(20) NULL
	 ,DEVICE_ID VARCHAR(20) NULL
	 ,CPE_CMPN_FMLY VARCHAR(100) NULL) 
	 
	CREATE TABLE #NRMWIRED
	(H6_ID VARCHAR(9) NULL
	,PROD_ID VARCHAR(50) NULL
	,VEN_CIRCUIT_ID VARCHAR(255) NULL
	,VNDR_NME VARCHAR(255) NULL
	,BDWD_NME VARCHAR(255) NULL
	,PLN VARCHAR(255) NULL
	,SPA_NUA VARCHAR(255) NULL
	,MULTILINK_CIRCUIT VARCHAR(255) NULL
	,DEDCTD_ACCS_CD VARCHAR(20) NULL
	,FMS_CKT_ID VARCHAR(20) NULL
	,VLAN_ID VARCHAR(255) NULL
	,IP_NUA VARCHAR(255) NULL
	,SPRINT_MNGE_NME VARCHAR(20) NULL
	,DLCI_VPI VARCHAR(255) NULL
	,READY_BEGIN_FLG_NME CHAR(1) NULL
	,OPT_IN_CKT_CD CHAR(1) NULL
	,OPT_IN_H_CD CHAR(1) NULL
	) 

	CREATE TABLE #M5MNSTABLE
	 (M5_ORDR_NBR  VARCHAR(50) NULL
	 ,M5_ORDR_ID  VARCHAR(20) NULL
	 ,CUST_ACCT_ID INT NULL
	 ,DEVICE_ID VARCHAR(20) NULL
	 ,ORDR_CMPNT_ID INT NULL
	 ,RLTD_CMPNT_ID INT NULL
	 ,MNS_SRVC_TIER VARCHAR(255) NULL
	 ,MDS_SRVC_TIER_ID TINYINT NULL
	 ,ACCS_CXR_NME VARCHAR(255) NULL
	 ,ASSOC_TRNSPRT_TYPE VARCHAR(255) NULL
	 ,CMPNT_STUS VARCHAR(255) NULL
	 ,CMPNT_TYPE_CD CHAR(4) NULL
	 ,NME VARCHAR(255) NULL
	 ) 

	 CREATE TABLE #M5RELMNSTABLE
	 (M5_ORDR_NBR  VARCHAR(50) NULL
	 ,M5_ORDR_ID  VARCHAR(20) NULL
	 ,CUST_ACCT_ID INT NULL
	 ,DEVICE_ID VARCHAR(20) NULL
	 ,ORDR_CMPNT_ID INT NULL
	 ,RLTD_CMPNT_ID INT NULL
	 ,MNS_SRVC_TIER VARCHAR(255) NULL
	 ,MDS_SRVC_TIER_ID TINYINT NULL
	 ,CMPNT_STUS VARCHAR(255) NULL
	 ,CMPNT_TYPE_CD CHAR(4) NULL
	 ,NME VARCHAR(255) NULL
	 ) 

	 CREATE TABLE #M5SITEIDTABLE
	 (
	 SITE_ID VARCHAR(50) NULL
	 )
	 
	 CREATE TABLE #M5PORTTABLE
	 (M5_ORDR_NBR  VARCHAR(50) NULL
	 ,M5_ORDR_CMPNT_ID  INT NULL
	 ,CUST_ACCT_ID INT NULL
	 ,NUA VARCHAR(50) NULL
	 ,PORT_BNDWD VARCHAR(50) NULL
	 )
	 
	 CREATE TABLE #M5H1BasedOnH6
	 (
		CUST_NME VARCHAR(255) NULL
	 )
	
 IF (@M5_CD = 1)
 BEGIN	 

	--Check for the MOVE_TO_ACCT_ID First and if there is no data, then look at PRNT_ACCT_ID
	declare @siteidtbl nvarchar(max) = 'INSERT INTO  #M5SITEIDTABLE (SITE_ID)
	 Select SITE_ID from openquery(M5,''select DISTINCT UPPER(vvca.SITE_ID) as SITE_ID
								from MACH5.V_V5U_ORDR vvo
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'

									--select @siteidtbl
	exec sp_executesql @siteidtbl
	

	declare @siteids varchar(max)
			select @siteids = ISNULL(@siteids,'') + UPPER(ISNULL(SITE_ID,'')) + ''''', '''''
			from #M5SITEIDTABLE
			if(len(@siteids)>2)
			set  @siteids = substring(@siteids, 1, len(@siteids)-6)
			
			--print @siteids
	 
	declare @cpeordr nvarchar(max) = 'INSERT INTO  #M5CPETable (M5_ORDR_NBR,ORDR_ID,CUST_ACCT_ID,DEVICE_ID,ASSOC_H6,CTRCT_TYPE,CCD,CMPNT_STUS,SITE_ID,CPE_MFR_PART_NBR,CPE_CMPN_FMLY)
	 Select ORDER_NBR
	,ORDR_ID
	,CUST_ACCT_ID
	,CPE_DEVICE_ID
	,CIS_CUST_ID
	,CPE_CONTRACT_TYPE
	,CUST_CMMT_DT
	,ORDR_STUS_CD
	,SITE_ID,CPE_MFR_PART_NBR
	,''''  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,vvca.CUST_ACCT_ID
	,voc.CPE_DEVICE_ID
	,vvca.CIS_CUST_ID
	,CASE WHEN voc.CPE_CONTRACT_TYPE = ''''ASAS'''' THEN ''''ASAS'''' ELSE '''''''' END AS CPE_CONTRACT_TYPE
	,vvo.CUST_CMMT_DT
	,vvo.ORDR_STUS_CD
	,vvca.SITE_ID,voc.CPE_MFR_PART_NBR
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.CPE_DEVICE_ID IS NOT NULL'
	IF @UCaaSCD = 'N'
	BEGIN
		SET @cpeordr = @cpeordr + '
				  and ((((voc.CPE_CONTRACT_TYPE != ''''ASAS'''') OR (voc.CPE_CONTRACT_TYPE IS NULL)) and exists (select *
							  from MACH5.V_V5U_ORDR_CMPNT voc2
							  where voc2.ORDR_CMPNT_ID = voc.ORDR_CMPNT_ID and voc.ORDR_ID=voc2.ORDR_ID
							  and (voc2.CPE_MNGD_FLG_CD = ''''Y'''' OR (('+ CONVERT(VARCHAR,@CEFlg) + ' = 1))))) or (voc.CPE_CONTRACT_TYPE = ''''ASAS''''))
				  and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
	END
	ELSE
	BEGIN
		SET @cpeordr = @cpeordr + ' and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
	END
	--select @cpeordr
	exec sp_executesql @cpeordr


	declare @cpefmlyordr nvarchar(max) = 'INSERT INTO  #M5CPEFmlyTbl (M5_ORDR_NBR,ORDR_ID,DEVICE_ID,CPE_CMPN_FMLY)
	 Select ORDER_NBR
	,ORDR_ID
	,CPE_DEVICE_ID
	,ISNULL(CPE_CMPN_FMLY,'''')  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,voc.CPE_DEVICE_ID
	,voc.CPE_CMPN_FMLY
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.CPE_DEVICE_ID IS NOT NULL'
	IF @UCaaSCD = 'N'
	BEGIN
		SET @cpefmlyordr = @cpefmlyordr + '
				  and ((((voc.CPE_CONTRACT_TYPE != ''''ASAS'''') OR (voc.CPE_CONTRACT_TYPE IS NULL)) and exists (select *
							  from MACH5.V_V5U_ORDR_CMPNT voc2
							  where voc2.ORDR_CMPNT_ID = voc.ORDR_CMPNT_ID and voc.ORDR_ID=voc2.ORDR_ID
							  and (voc2.CPE_MNGD_FLG_CD = ''''Y'''' OR (('+ CONVERT(VARCHAR,@CEFlg) + ' = 1))))) or (voc.CPE_CONTRACT_TYPE = ''''ASAS''''))
				  and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
	END
	ELSE
	BEGIN
		SET @cpefmlyordr = @cpefmlyordr + ' and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
	END

	exec sp_executesql @cpefmlyordr


	declare @relordrs nvarchar(max) = 'INSERT INTO  #M5CPETable (M5_ORDR_NBR,ORDR_ID,CUST_ACCT_ID,DEVICE_ID,ASSOC_H6,CTRCT_TYPE,CCD,CMPNT_STUS,SITE_ID,CPE_MFR_PART_NBR,CPE_CMPN_FMLY)
	 Select ORDER_NBR
	,ORDR_ID
	,CUST_ACCT_ID
	,CPE_DEVICE_ID
	,CIS_CUST_ID
	,CPE_CONTRACT_TYPE
	,CUST_CMMT_DT
	,ORDR_STUS_CD
	,SITE_ID,CPE_MFR_PART_NBR
	,'''' FROM (Select ORDER_NBR
	,ORDR_ID
	,CUST_ACCT_ID
	,CPE_DEVICE_ID
	,CIS_CUST_ID
	,CPE_CONTRACT_TYPE
	,CUST_CMMT_DT
	,ORDR_STUS_CD
	,SITE_ID,CPE_MFR_PART_NBR  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,vvca.CUST_ACCT_ID
	,voc.CPE_DEVICE_ID
	,vvca.CIS_CUST_ID
	,CASE WHEN voc.CPE_CONTRACT_TYPE = ''''ASAS'''' THEN ''''ASAS'''' ELSE '''''''' END AS CPE_CONTRACT_TYPE
	,vvo.CUST_CMMT_DT
	,vvo.ORDR_STUS_CD
	,vvca.SITE_ID,voc.CPE_MFR_PART_NBR
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.CPE_DEVICE_ID IS NOT NULL'
	IF @UCaaSCD = 'N'
	BEGIN
		SET @relordrs = @relordrs + '
				  and ((((voc.CPE_CONTRACT_TYPE != ''''ASAS'''') OR (voc.CPE_CONTRACT_TYPE IS NULL)) and exists (select *
							  from MACH5.V_V5U_ORDR_CMPNT voc2
							  where voc2.ORDR_CMPNT_ID = voc.ORDR_CMPNT_ID and voc.ORDR_ID=voc2.ORDR_ID
							  and (voc2.CPE_MNGD_FLG_CD = ''''Y'''' OR (('+ CONVERT(VARCHAR,@CEFlg) + ' = 1))))) or (voc.CPE_CONTRACT_TYPE = ''''ASAS''''))
				  and UPPER(vvca.SITE_ID) IN (''''' + @siteids + ''''') '')) as x WHERE NOT EXISTS (SELECT ''X'' FROM #M5CPETable ct WITH (NOLOCK) WHERE ct.M5_ORDR_NBR=x.ORDER_NBR AND ct.ORDR_ID=x.ORDR_ID AND ct.DEVICE_ID=x.CPE_DEVICE_ID AND ct.SITE_ID=x.SITE_ID)'
	END
	ELSE
	BEGIN
		SET @relordrs = @relordrs + ' and UPPER(vvca.SITE_ID) IN (''''' + @siteids + ''''') '')) as x WHERE NOT EXISTS (SELECT ''X'' FROM #M5CPETable ct WITH (NOLOCK) WHERE ct.M5_ORDR_NBR=x.ORDER_NBR AND ct.ORDR_ID=x.ORDR_ID AND ct.DEVICE_ID=x.CPE_DEVICE_ID AND ct.SITE_ID=x.SITE_ID)'
	END
	--select @relordrs
	exec sp_executesql @relordrs
	

	declare @relordrscpefmly nvarchar(max) = 'INSERT INTO  #M5CPEFmlyTbl (M5_ORDR_NBR,ORDR_ID,DEVICE_ID,CPE_CMPN_FMLY)
	 Select ORDER_NBR
	,ORDR_ID
	,CPE_DEVICE_ID
	,ISNULL(CPE_CMPN_FMLY,'''') FROM (Select ORDER_NBR
	,ORDR_ID
	,CPE_DEVICE_ID
	,CPE_CMPN_FMLY  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,voc.CPE_DEVICE_ID
	,voc.CPE_CMPN_FMLY
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.CPE_DEVICE_ID IS NOT NULL'
	IF @UCaaSCD = 'N'
	BEGIN
		SET @relordrscpefmly = @relordrscpefmly + '
				  and ((((voc.CPE_CONTRACT_TYPE != ''''ASAS'''') OR (voc.CPE_CONTRACT_TYPE IS NULL)) and exists (select *
							  from MACH5.V_V5U_ORDR_CMPNT voc2
							  where voc2.ORDR_CMPNT_ID = voc.ORDR_CMPNT_ID and voc.ORDR_ID=voc2.ORDR_ID
							  and (voc2.CPE_MNGD_FLG_CD = ''''Y'''' OR (('+ CONVERT(VARCHAR,@CEFlg) + ' = 1))))) or (voc.CPE_CONTRACT_TYPE = ''''ASAS''''))
				  and UPPER(vvca.SITE_ID) IN (''''' + @siteids + ''''') '')) as x WHERE NOT EXISTS (SELECT ''X'' FROM #M5CPEFmlyTbl ct WITH (NOLOCK) WHERE ct.M5_ORDR_NBR=x.ORDER_NBR AND ct.ORDR_ID=x.ORDR_ID AND ct.DEVICE_ID=x.CPE_DEVICE_ID)'
	END
	ELSE
	BEGIN
		SET @relordrscpefmly = @relordrscpefmly + ' and UPPER(vvca.SITE_ID) IN (''''' + @siteids + ''''') '')) as x WHERE NOT EXISTS (SELECT ''X'' FROM #M5CPEFmlyTbl ct WITH (NOLOCK) WHERE ct.M5_ORDR_NBR=x.ORDER_NBR AND ct.ORDR_ID=x.ORDR_ID AND ct.DEVICE_ID=x.CPE_DEVICE_ID)'
	END

	exec sp_executesql @relordrscpefmly

	
	set @siteidtbl = 'INSERT INTO  #M5SITEIDTABLE (SITE_ID)
	 Select SITE_ID from openquery(M5,''select DISTINCT UPPER(vvca.SITE_ID) as SITE_ID
								from MACH5.V_V5U_ORDR vvo
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'

									--print @siteidtbl
	exec sp_executesql @siteidtbl
	
	set @siteids = null
			select @siteids = ISNULL(@siteids,'') + UPPER(ISNULL(SITE_ID,'')) + ''''', '''''
			from #M5SITEIDTABLE
			if(len(@siteids)>2)
			set  @siteids = substring(@siteids, 1, len(@siteids)-6)
			
			--print @siteids
	 
	set @cpeordr = 'INSERT INTO  #M5CPETable (M5_ORDR_NBR,ORDR_ID,CUST_ACCT_ID,DEVICE_ID,ASSOC_H6,CTRCT_TYPE,CCD,CMPNT_STUS,SITE_ID,CPE_MFR_PART_NBR,CPE_CMPN_FMLY)
	 Select ORDER_NBR
	,ORDR_ID
	,CUST_ACCT_ID
	,CPE_DEVICE_ID
	,CIS_CUST_ID
	,CPE_CONTRACT_TYPE
	,CUST_CMMT_DT
	,ORDR_STUS_CD
	,SITE_ID,CPE_MFR_PART_NBR
	,''''  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,vvca.CUST_ACCT_ID
	,voc.CPE_DEVICE_ID
	,vvca.CIS_CUST_ID
	,CASE WHEN voc.CPE_CONTRACT_TYPE = ''''ASAS'''' THEN ''''ASAS'''' ELSE '''''''' END AS CPE_CONTRACT_TYPE
	,vvo.CUST_CMMT_DT
	,vvo.ORDR_STUS_CD
	,vvca.SITE_ID,voc.CPE_MFR_PART_NBR
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.CPE_DEVICE_ID IS NOT NULL'
	IF @UCaaSCD = 'N'
	BEGIN
		SET @cpeordr = @cpeordr + '
				  and ((((voc.CPE_CONTRACT_TYPE != ''''ASAS'''') OR (voc.CPE_CONTRACT_TYPE IS NULL)) and exists (select *
							  from MACH5.V_V5U_ORDR_CMPNT voc2
							  where voc2.ORDR_CMPNT_ID = voc.ORDR_CMPNT_ID and voc.ORDR_ID=voc2.ORDR_ID
							  and (voc2.CPE_MNGD_FLG_CD = ''''Y'''' OR (('+ CONVERT(VARCHAR,@CEFlg) + ' = 1))))) or (voc.CPE_CONTRACT_TYPE = ''''ASAS''''))
				  and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
	END
	ELSE
	BEGIN
		SET @cpeordr = @cpeordr + ' and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
	END
	--select @cpeordr
	exec sp_executesql @cpeordr


	set @cpefmlyordr = 'INSERT INTO  #M5CPEFmlyTbl (M5_ORDR_NBR,ORDR_ID,DEVICE_ID,CPE_CMPN_FMLY)
	 Select ORDER_NBR
	,ORDR_ID
	,CPE_DEVICE_ID
	,ISNULL(CPE_CMPN_FMLY,'''')  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,voc.CPE_DEVICE_ID
	,voc.CPE_CMPN_FMLY
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.CPE_DEVICE_ID IS NOT NULL'
	IF @UCaaSCD = 'N'
	BEGIN
		SET @cpefmlyordr = @cpefmlyordr + '
				  and ((((voc.CPE_CONTRACT_TYPE != ''''ASAS'''') OR (voc.CPE_CONTRACT_TYPE IS NULL)) and exists (select *
							  from MACH5.V_V5U_ORDR_CMPNT voc2
							  where voc2.ORDR_CMPNT_ID = voc.ORDR_CMPNT_ID and voc.ORDR_ID=voc2.ORDR_ID
							  and (voc2.CPE_MNGD_FLG_CD = ''''Y'''' OR (('+ CONVERT(VARCHAR,@CEFlg) + ' = 1))))) or (voc.CPE_CONTRACT_TYPE = ''''ASAS''''))
				  and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
	END
	ELSE
	BEGIN
		SET @cpefmlyordr = @cpefmlyordr + ' and vvca.CIS_CUST_ID = ''''' + @H6 + ''''' '')'
	END

	exec sp_executesql @cpefmlyordr


	set @relordrs = 'INSERT INTO  #M5CPETable (M5_ORDR_NBR,ORDR_ID,CUST_ACCT_ID,DEVICE_ID,ASSOC_H6,CTRCT_TYPE,CCD,CMPNT_STUS,SITE_ID,CPE_MFR_PART_NBR,CPE_CMPN_FMLY)
	 Select ORDER_NBR
	,ORDR_ID
	,CUST_ACCT_ID
	,CPE_DEVICE_ID
	,CIS_CUST_ID
	,CPE_CONTRACT_TYPE
	,CUST_CMMT_DT
	,ORDR_STUS_CD
	,SITE_ID,CPE_MFR_PART_NBR
	,'''' FROM (Select ORDER_NBR
	,ORDR_ID
	,CUST_ACCT_ID
	,CPE_DEVICE_ID
	,CIS_CUST_ID
	,CPE_CONTRACT_TYPE
	,CUST_CMMT_DT
	,ORDR_STUS_CD
	,SITE_ID,CPE_MFR_PART_NBR  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,vvca.CUST_ACCT_ID
	,voc.CPE_DEVICE_ID
	,vvca.CIS_CUST_ID
	,CASE WHEN voc.CPE_CONTRACT_TYPE = ''''ASAS'''' THEN ''''ASAS'''' ELSE '''''''' END AS CPE_CONTRACT_TYPE
	,vvo.CUST_CMMT_DT
	,vvo.ORDR_STUS_CD
	,vvca.SITE_ID,voc.CPE_MFR_PART_NBR
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.CPE_DEVICE_ID IS NOT NULL'
	IF @UCaaSCD = 'N'
	BEGIN
		SET @relordrs = @relordrs + '
				  and ((((voc.CPE_CONTRACT_TYPE != ''''ASAS'''') OR (voc.CPE_CONTRACT_TYPE IS NULL)) and exists (select *
							  from MACH5.V_V5U_ORDR_CMPNT voc2
							  where voc2.ORDR_CMPNT_ID = voc.ORDR_CMPNT_ID and voc.ORDR_ID=voc2.ORDR_ID
							  and (voc2.CPE_MNGD_FLG_CD = ''''Y'''' OR (('+ CONVERT(VARCHAR,@CEFlg) + ' = 1))))) or (voc.CPE_CONTRACT_TYPE = ''''ASAS''''))
				  and UPPER(vvca.SITE_ID) IN (''''' + @siteids + ''''') '')) as x WHERE NOT EXISTS (SELECT ''X'' FROM #M5CPETable ct WITH (NOLOCK) WHERE ct.M5_ORDR_NBR=x.ORDER_NBR AND ct.ORDR_ID=x.ORDR_ID AND ct.DEVICE_ID=x.CPE_DEVICE_ID AND ct.SITE_ID=x.SITE_ID)'
	END
	ELSE
	BEGIN
		SET @relordrs = @relordrs + ' and UPPER(vvca.SITE_ID) IN (''''' + @siteids + ''''') '')) as x WHERE NOT EXISTS (SELECT ''X'' FROM #M5CPETable ct WITH (NOLOCK) WHERE ct.M5_ORDR_NBR=x.ORDER_NBR AND ct.ORDR_ID=x.ORDR_ID AND ct.DEVICE_ID=x.CPE_DEVICE_ID AND ct.SITE_ID=x.SITE_ID)'
	END
	--select @relordrs
	exec sp_executesql @relordrs
	

	set @relordrscpefmly = 'INSERT INTO  #M5CPEFmlyTbl (M5_ORDR_NBR,ORDR_ID,DEVICE_ID,CPE_CMPN_FMLY)
	 Select ORDER_NBR
	,ORDR_ID
	,CPE_DEVICE_ID
	,ISNULL(CPE_CMPN_FMLY,'''') FROM (Select ORDER_NBR
	,ORDR_ID
	,CPE_DEVICE_ID
	,CPE_CMPN_FMLY  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,voc.CPE_DEVICE_ID
	,voc.CPE_CMPN_FMLY
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''CPE'''',''''CPEM'''',''''CPEP'''',''''CPES'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.CPE_DEVICE_ID IS NOT NULL'
	IF @UCaaSCD = 'N'
	BEGIN
		SET @relordrscpefmly = @relordrscpefmly + '
				  and ((((voc.CPE_CONTRACT_TYPE != ''''ASAS'''') OR (voc.CPE_CONTRACT_TYPE IS NULL)) and exists (select *
							  from MACH5.V_V5U_ORDR_CMPNT voc2
							  where voc2.ORDR_CMPNT_ID = voc.ORDR_CMPNT_ID and voc.ORDR_ID=voc2.ORDR_ID
							  and (voc2.CPE_MNGD_FLG_CD = ''''Y'''' OR (('+ CONVERT(VARCHAR,@CEFlg) + ' = 1))))) or (voc.CPE_CONTRACT_TYPE = ''''ASAS''''))
				  and UPPER(vvca.SITE_ID) IN (''''' + @siteids + ''''') '')) as x WHERE NOT EXISTS (SELECT ''X'' FROM #M5CPEFmlyTbl ct WITH (NOLOCK) WHERE ct.M5_ORDR_NBR=x.ORDER_NBR AND ct.ORDR_ID=x.ORDR_ID AND ct.DEVICE_ID=x.CPE_DEVICE_ID)'
	END
	ELSE
	BEGIN
		SET @relordrscpefmly = @relordrscpefmly + ' and UPPER(vvca.SITE_ID) IN (''''' + @siteids + ''''') '')) as x WHERE NOT EXISTS (SELECT ''X'' FROM #M5CPEFmlyTbl ct WITH (NOLOCK) WHERE ct.M5_ORDR_NBR=x.ORDER_NBR AND ct.ORDR_ID=x.ORDR_ID AND ct.DEVICE_ID=x.CPE_DEVICE_ID)'
	END

	exec sp_executesql @relordrscpefmly

		UPDATE mc
		SET COWS_ORDR_ID = ISNULL(fo.ORDR_ID,-1),
			VNDR_PO = (SELECT TOP 1 fc.PRCH_ORDR_NBR 
						FROM dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) 
						WHERE fc.ORDR_ID = od.ORDR_ID AND fc.DEVICE_ID = mc.DEVICE_ID
						  AND fc.PRCH_ORDR_NBR IS NOT NULL),
			RQSTN_NBR = (SELECT TOP 1 fc.PLSFT_RQSTN_NBR 
						FROM dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) 
						WHERE fc.ORDR_ID = od.ORDR_ID AND fc.DEVICE_ID = mc.DEVICE_ID
						  AND fc.PLSFT_RQSTN_NBR IS NOT NULL),
			RCPT_STUS = ([dbo].[getOrderStatusForEvent](od.ORDR_ID)),
			DLVY_CLLI = (SELECT TOP 1 odcl.DLVY_CLLI
						 FROM dbo.ORDR odcl WITH (NOLOCK)
						 WHERE odcl.DLVY_CLLI IS NOT NULL
						  AND odcl.ORDR_ID=od.ORDR_ID
						  AND UPPER(odcl.DLVY_CLLI) IN ('MTLDFLTBACP','DLLSTX53ACP','OVPKKSTDACP','RVSDMO09ACP'))
		FROM #M5CPETable mc INNER JOIN
			 dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN = mc.M5_ORDR_NBR INNER JOIN
			 dbo.ORDR od WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID INNER JOIN
			 dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON fc.ORDR_ID=od.ORDR_ID AND fc.DEVICE_ID=mc.DEVICE_ID
		WHERE od.ORDR_STUS_ID NOT IN (3,5)
		  AND fo.ORDR_TYPE_CD != 'DC'
		  AND EXISTS (SELECT 'X'
					  FROM dbo.ORDR od2 WITH (NOLOCK) INNER JOIN
					  (select fo2.ftn, max(fo2.ordr_id) as max_ordr_id
						from dbo.fsa_ordr fo2 with (nolock) INNER JOIN
							 dbo.FSA_ORDR_CPE_LINE_ITEM fc2 WITH (NOLOCK) ON fc2.ORDR_ID=fo2.ORDR_ID AND fc2.DEVICE_ID=mc.DEVICE_ID
						where fo2.ftn = fo.FTN
						  AND fo2.ORDR_TYPE_CD != 'DC'
						group by fo2.ftn) x on x.max_ordr_id=od2.ORDR_ID
					  WHERE od2.ORDR_ID=od.ORDR_ID
					    and od2.ORDR_STUS_ID NOT IN (3,5))
		
	UPDATE mc
	SET CPE_CMPN_FMLY = (select ltrim(rtrim(substring(
			(
				Select CASE WHEN ISNULL(cf.CPE_CMPN_FMLY,'') <> '' THEN ','+cf.CPE_CMPN_FMLY END  AS [text()]
				From #M5CPEFmlyTbl cf
				Where cf.DEVICE_ID = mc.DEVICE_ID
				ORDER BY cf.DEVICE_ID
				For XML PATH ('')
			), 2, 1000)))),
		CMPNT_STUS = CASE CMPNT_STUS WHEN 'CN' THEN 'Cancelled'
										WHEN 'CP' THEN 'Completed'
										WHEN 'HL' THEN 'On Hold'
										WHEN 'PA' THEN 'Pending Approval'
										WHEN 'PV' THEN 'Provisioning'
										WHEN 'WP' THEN 'Work in Progress'
										WHEN 'HP' THEN 'Hold for Pricing Check'
										WHEN 'PC' THEN 'Pending Cancel'
										WHEN 'PD' THEN 'Pending Disconnect'
										WHEN 'PM' THEN 'Pending Modify'
										WHEN 'RJ' THEN 'Rejected' ELSE CMPNT_STUS END
	FROM #M5CPETable mc

	--Get MNS Order Data from M5
	
	declare @srvctier nvarchar(max) = 'INSERT INTO #M5MNSTABLE (M5_ORDR_NBR
	 ,M5_ORDR_ID
	 ,CUST_ACCT_ID
	 ,DEVICE_ID
	 ,ORDR_CMPNT_ID
	 ,RLTD_CMPNT_ID
	 ,MNS_SRVC_TIER
	 ,ASSOC_TRNSPRT_TYPE
	 ,ACCS_CXR_NME
	 ,CMPNT_STUS
	 ,CMPNT_TYPE_CD
	 ,NME) Select ORDER_NBR
	,ORDR_ID
	,CUST_ACCT_ID
	,CPE_DEVICE_ID
	,ORDR_CMPNT_ID
	,RLTD_CMPNT_ID
	,CASE WHEN CMPNT_TYPE_CD LIKE ''MSS%'' THEN ''MSS'' ELSE MNS_SRVC_TIER END AS MNS_SRVC_TIER
	,ISNULL(ASSOC_TRNSPRT_TYPE,''N/A'') AS ASSOC_TRNSPRT_TYPE
	,ISNULL(ACCS_CXR_NME,''N/A'') AS ACCS_CXR_NME
	,STUS_CD
	,CMPNT_TYPE_CD
	,NME  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,vvca.CUST_ACCT_ID
	,voc.CPE_DEVICE_ID
	,voc.ORDR_CMPNT_ID
	,voc.RLTD_CMPNT_ID
	,voc.MNS_SRVC_TIER
	,voc.ASSOC_TRNSPRT_TYPE
	,voc.ACCS_CXR_NME
	,voc.STUS_CD
	,voc.CMPNT_TYPE_CD
	,voc.NME
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''MNST'''',''''MNSS'''',''''MNSS'''',''''MSSS'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and ((vvca.CIS_CUST_ID = ''''' + @H6 + ''''')
				  or (UPPER(vvca.SITE_ID) IN (''''' + @siteids + '''''))) '')'

	exec sp_executesql @srvctier


	UPDATE mt
	SET MDS_SRVC_TIER_ID = lmst.MDS_SRVC_TIER_ID,
		CMPNT_STUS = CASE CMPNT_STUS WHEN 'CN' THEN 'Cancelled'
										WHEN 'CP' THEN 'Completed'
										WHEN 'HL' THEN 'On Hold'
										WHEN 'PA' THEN 'Pending Active'
										WHEN 'PV' THEN 'Provisioning'
										WHEN 'WP' THEN 'Work in Progress'
										WHEN 'HP' THEN 'Hold for Pricing Check'
										WHEN 'PC' THEN 'Pending Cancel'
										WHEN 'PD' THEN 'Pending Disconnect'
										WHEN 'PM' THEN 'Pending Modify'
										WHEN 'RJ' THEN 'Rejected' ELSE CMPNT_STUS END
	FROM #M5MNSTABLE mt INNER JOIN
	dbo.LK_MDS_SRVC_TIER lmst WITH (NOLOCK) ON lmst.MDS_SRVC_TIER_DES=mt.MNS_SRVC_TIER
	WHERE lmst.MDS_SRVC_TIER_CD IS NOT NULL

		UPDATE mt
		SET MDS_SRVC_TIER_ID = lmst.MDS_SRVC_TIER_ID
		FROM #M5MNSTABLE mt INNER JOIN
		dbo.LK_MDS_SRVC_TIER lmst WITH (NOLOCK) ON charindex(lmst.MDS_SRVC_TIER_DES,mt.MNS_SRVC_TIER)=1
		WHERE mt.MDS_SRVC_TIER_ID IS NULL

	
		set @srvctier = 'INSERT INTO #M5MNSTABLE (M5_ORDR_NBR
		 ,M5_ORDR_ID
		 ,CUST_ACCT_ID
		 ,DEVICE_ID
		 ,ORDR_CMPNT_ID
		 ,RLTD_CMPNT_ID
		 ,MNS_SRVC_TIER
		 ,ASSOC_TRNSPRT_TYPE
		 ,ACCS_CXR_NME
		 ,CMPNT_STUS
		 ,CMPNT_TYPE_CD
		 ,NME) SELECT * FROM (Select ORDER_NBR
		,ORDR_ID
		,CUST_ACCT_ID
		,CPE_DEVICE_ID
		,ORDR_CMPNT_ID
		,RLTD_CMPNT_ID
		,CASE WHEN CMPNT_TYPE_CD LIKE ''MSS%'' THEN ''MSS'' ELSE MNS_SRVC_TIER END AS MNS_SRVC_TIER
		,ISNULL(ASSOC_TRNSPRT_TYPE,''N/A'') AS ASSOC_TRNSPRT_TYPE
		,ISNULL(ACCS_CXR_NME,''N/A'') AS ACCS_CXR_NME
		,STUS_CD
		,CMPNT_TYPE_CD
		,NME  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
		,vvo.ORDR_ID
		,vvca.CUST_ACCT_ID
		,voc.CPE_DEVICE_ID
		,voc.ORDR_CMPNT_ID
		,voc.RLTD_CMPNT_ID
		,voc.MNS_SRVC_TIER
		,voc.ASSOC_TRNSPRT_TYPE
		,voc.ACCS_CXR_NME
		,voc.STUS_CD
		,voc.CMPNT_TYPE_CD
		,voc.NME
									from MACH5.V_V5U_ORDR_CMPNT voc
									inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
					  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
									where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
					  and voc.cmpnt_type_cd IN (''''MNST'''',''''MNSS'''',''''MNSS'''',''''MSSS'''')
					  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
					  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
					  and ((vvca.CIS_CUST_ID = ''''' + @H6 + ''''')
					  or (UPPER(vvca.SITE_ID) IN (''''' + @siteids + '''''))) '')) as x
		WHERE NOT EXISTS (SELECT ''X'' FROM #M5MNSTABLE mnst2 WHERE mnst2.M5_ORDR_ID=x.ORDR_ID)'

		exec sp_executesql @srvctier


		UPDATE mt
		SET MDS_SRVC_TIER_ID = lmst.MDS_SRVC_TIER_ID,
			CMPNT_STUS = CASE CMPNT_STUS WHEN 'CN' THEN 'Cancelled'
											WHEN 'CP' THEN 'Completed'
											WHEN 'HL' THEN 'On Hold'
											WHEN 'PA' THEN 'Pending Active'
											WHEN 'PV' THEN 'Provisioning'
											WHEN 'WP' THEN 'Work in Progress'
											WHEN 'HP' THEN 'Hold for Pricing Check'
											WHEN 'PC' THEN 'Pending Cancel'
											WHEN 'PD' THEN 'Pending Disconnect'
											WHEN 'PM' THEN 'Pending Modify'
											WHEN 'RJ' THEN 'Rejected' ELSE CMPNT_STUS END
		FROM #M5MNSTABLE mt INNER JOIN
		dbo.LK_MDS_SRVC_TIER lmst WITH (NOLOCK) ON lmst.MDS_SRVC_TIER_DES=mt.MNS_SRVC_TIER
		WHERE lmst.MDS_SRVC_TIER_CD IS NOT NULL
	
		UPDATE mt
		SET MDS_SRVC_TIER_ID = lmst.MDS_SRVC_TIER_ID
		FROM #M5MNSTABLE mt INNER JOIN
		dbo.LK_MDS_SRVC_TIER lmst WITH (NOLOCK) ON charindex(lmst.MDS_SRVC_TIER_DES,mt.MNS_SRVC_TIER)=1
		WHERE mt.MDS_SRVC_TIER_ID IS NULL

	declare @relmnstier nvarchar(max) = 'INSERT INTO #M5RELMNSTABLE (M5_ORDR_NBR
	 ,M5_ORDR_ID
	 ,CUST_ACCT_ID
	 ,DEVICE_ID
	 ,ORDR_CMPNT_ID
	 ,RLTD_CMPNT_ID
	 ,MNS_SRVC_TIER
	 ,CMPNT_STUS
	 ,CMPNT_TYPE_CD
	 ,NME) Select ORDER_NBR
	,ORDR_ID
	,CUST_ACCT_ID
	,CPE_DEVICE_ID
	,ORDR_CMPNT_ID
	,RLTD_CMPNT_ID
	,CASE WHEN CMPNT_TYPE_CD LIKE ''MSS%'' THEN ''MSS'' ELSE MNS_SRVC_TIER END AS MNS_SRVC_TIER
	,STUS_CD
	,CMPNT_TYPE_CD
	,NME  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,vvca.CUST_ACCT_ID
	,voc.CPE_DEVICE_ID
	,voc.ORDR_CMPNT_ID
	,voc.RLTD_CMPNT_ID
	,voc.MNS_SRVC_TIER
	,voc.STUS_CD
	,voc.CMPNT_TYPE_CD
	,voc.NME
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''MNST'''',''''MNSS'''',''''MNSS'''',''''MSSS'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.MNS_SRVC_TIER IS NOT NULL
				  and voc.RLTD_CMPNT_ID IS NOT NULL
				  and ((vvca.CIS_CUST_ID = ''''' + @H6 + ''''')
				  or (UPPER(vvca.SITE_ID) IN (''''' + @siteids + '''''))) '')'

	exec sp_executesql @relmnstier


	UPDATE mt
	SET MDS_SRVC_TIER_ID = lmst.MDS_SRVC_TIER_ID,
		CMPNT_STUS = CASE CMPNT_STUS WHEN 'CN' THEN 'Cancelled'
										WHEN 'CP' THEN 'Completed'
										WHEN 'HL' THEN 'On Hold'
										WHEN 'PA' THEN 'Pending Active'
										WHEN 'PV' THEN 'Provisioning'
										WHEN 'WP' THEN 'Work in Progress'
										WHEN 'HP' THEN 'Hold for Pricing Check'
										WHEN 'PC' THEN 'Pending Cancel'
										WHEN 'PD' THEN 'Pending Disconnect'
										WHEN 'PM' THEN 'Pending Modify'
										WHEN 'RJ' THEN 'Rejected' ELSE CMPNT_STUS END
	FROM #M5RELMNSTABLE mt INNER JOIN
	dbo.LK_MDS_SRVC_TIER lmst WITH (NOLOCK) ON lmst.MDS_SRVC_TIER_DES=mt.MNS_SRVC_TIER
	WHERE lmst.MDS_SRVC_TIER_CD IS NOT NULL

		UPDATE mt
		SET MDS_SRVC_TIER_ID = lmst.MDS_SRVC_TIER_ID
		FROM #M5RELMNSTABLE mt INNER JOIN
		dbo.LK_MDS_SRVC_TIER lmst WITH (NOLOCK) ON charindex(lmst.MDS_SRVC_TIER_DES,mt.MNS_SRVC_TIER)=1
		WHERE mt.MDS_SRVC_TIER_ID IS NULL

	IF NOT EXISTS(SELECT 'x' FROM #M5RELMNSTABLE)
	BEGIN
		set @relmnstier = 'INSERT INTO #M5RELMNSTABLE (M5_ORDR_NBR
	 ,M5_ORDR_ID
	 ,CUST_ACCT_ID
	 ,DEVICE_ID
	 ,ORDR_CMPNT_ID
	 ,RLTD_CMPNT_ID
	 ,MNS_SRVC_TIER
	 ,CMPNT_STUS
	 ,CMPNT_TYPE_CD
	 ,NME) Select ORDER_NBR
	,ORDR_ID
	,CUST_ACCT_ID
	,CPE_DEVICE_ID
	,ORDR_CMPNT_ID
	,RLTD_CMPNT_ID
	,CASE WHEN CMPNT_TYPE_CD LIKE ''MSS%'' THEN ''MSS'' ELSE MNS_SRVC_TIER END AS MNS_SRVC_TIER
	,STUS_CD
	,CMPNT_TYPE_CD
	,NME  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
	,vvo.ORDR_ID
	,vvca.CUST_ACCT_ID
	,voc.CPE_DEVICE_ID
	,voc.ORDR_CMPNT_ID
	,voc.RLTD_CMPNT_ID
	,voc.MNS_SRVC_TIER
	,voc.STUS_CD
	,voc.CMPNT_TYPE_CD
	,voc.NME
								from MACH5.V_V5U_ORDR_CMPNT voc
								inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
				  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
				  and voc.cmpnt_type_cd IN (''''MNST'''',''''MNSS'''',''''MNSS'''',''''MSSS'''')
				  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''')
				  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
				  and voc.MNS_SRVC_TIER IS NOT NULL
				  and voc.RLTD_CMPNT_ID IS NOT NULL
				  and ((vvca.CIS_CUST_ID = ''''' + @H6 + ''''')
				  or (UPPER(vvca.SITE_ID) IN (''''' + @siteids + '''''))) '')'

	exec sp_executesql @relmnstier


	UPDATE mt
	SET MDS_SRVC_TIER_ID = lmst.MDS_SRVC_TIER_ID,
		CMPNT_STUS = CASE CMPNT_STUS WHEN 'CN' THEN 'Cancelled'
										WHEN 'CP' THEN 'Completed'
										WHEN 'HL' THEN 'On Hold'
										WHEN 'PA' THEN 'Pending Active'
										WHEN 'PV' THEN 'Provisioning'
										WHEN 'WP' THEN 'Work in Progress'
										WHEN 'HP' THEN 'Hold for Pricing Check'
										WHEN 'PC' THEN 'Pending Cancel'
										WHEN 'PD' THEN 'Pending Disconnect'
										WHEN 'PM' THEN 'Pending Modify'
										WHEN 'RJ' THEN 'Rejected' ELSE CMPNT_STUS END
	FROM #M5RELMNSTABLE mt INNER JOIN
	dbo.LK_MDS_SRVC_TIER lmst WITH (NOLOCK) ON lmst.MDS_SRVC_TIER_DES=mt.MNS_SRVC_TIER
	WHERE lmst.MDS_SRVC_TIER_CD IS NOT NULL

		UPDATE mt
		SET MDS_SRVC_TIER_ID = lmst.MDS_SRVC_TIER_ID
		FROM #M5RELMNSTABLE mt INNER JOIN
		dbo.LK_MDS_SRVC_TIER lmst WITH (NOLOCK) ON charindex(lmst.MDS_SRVC_TIER_DES,mt.MNS_SRVC_TIER)=1
		WHERE mt.MDS_SRVC_TIER_ID IS NULL

	END

	--select * from #M5MNSTABLE

	--Get MNS Order Address, Contact Info. from M5
	IF ((EXISTS (SELECT 'x' FROM #M5MNSTABLE))
	OR (EXISTS (SELECT 'x' FROM #M5CPETable)))
	BEGIN
		
			declare @ordrnbrs varchar(max)
			select @ordrnbrs = ISNULL(@ordrnbrs,'') + ISNULL(M5_ORDR_NBR,'') + ''''', '''''
			from (SELECT DISTINCT M5_ORDR_NBR FROM #M5MNSTABLE UNION SELECT DISTINCT M5_ORDR_NBR FROM #M5CPETable) x
			if(len(@ordrnbrs)>2)
			set  @ordrnbrs = substring(@ordrnbrs, 1, len(@ordrnbrs)-6)

			declare @acctids varchar(max)
			select @acctids = ISNULL(@acctids,'') + ISNULL(CONVERT(VARCHAR,CUST_ACCT_ID),'') + ', '
			from (SELECT DISTINCT CUST_ACCT_ID FROM #M5MNSTABLE UNION SELECT DISTINCT CUST_ACCT_ID FROM #M5CPETable) x
			if(len(@acctids)>2)
			set  @acctids = substring(@acctids, 1, len(@acctids)-1)
			--select @acctids
			declare @adrinfo nvarchar(max) = 'INSERT INTO #M5ADRTable (M5_ORDR_NBR,
				CIS_HIER_LVL_CD,
				SITE_ID,
				LINE_1,
				LINE_2,
				LINE_3,
				CTY_NME,
				ST_PRVN_CD,
				PSTL_CD,
				CTRY_CD,
				BLDG_ID,
				ROOM_ID,
				FLOOR_ID) Select ORDER_NBR,
					CIS_HIER_LVL_CD,
					SITE_ID,
					LINE_1,
					LINE_2,
					LINE_3,
					CTY_NME,
					ST_PRVN_CD,
					PSTL_CD,
					CTRY_CD,
					BLDG_ID,
					ROOM_ID,
					FLOOR_ID  from openquery(M5,''Select DISTINCT vvo.ORDER_NBR,
									vvca.CIS_HIER_LVL_CD,
									vvca.SITE_ID,
									vvca.LINE_1,
									vvca.LINE_2,
									vvca.LINE_3,
									vvca.CTY_NME,
									vvca.ST_PRVN_CD,
									vvca.PSTL_CD,
									vvca.CTRY_CD,
									vvca.BLDG_ID,
									vvca.ROOM_ID,
									vvca.FLOOR_ID
												From MACH5.V_V5U_ORDR vvo
								  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
												WHERE vvca.CUST_ACCT_ID IN (' + @acctids + ')
								  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
								  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
										or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
								  and vvo.ORDER_NBR IN (''''' + @ordrnbrs + ''''')
								  UNION
								  Select DISTINCT vvo.ORDER_NBR,
									vvca.CIS_HIER_LVL_CD,
									vvca.SITE_ID,
									vvca.LINE_1,
									vvca.LINE_2,
									vvca.LINE_3,
									vvca.CTY_NME,
									vvca.ST_PRVN_CD,
									vvca.PSTL_CD,
									vvca.CTRY_CD,
									vvca.BLDG_ID,
									vvca.ROOM_ID,
									vvca.FLOOR_ID
												From MACH5.V_V5U_ORDR vvo
								  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								  inner join MACH5.V_V5U_CUST_ACCT vvca2 ON vvca2.H4 = vvca.CIS_CUST_ID
								  WHERE vvca2.CUST_ACCT_ID IN (' + @acctids + ')
								  AND vvca.TYPE_CD=DECODE(vvca.CIS_HIER_LVL_CD,''''H4'''',''''BILL'''',''''PHYS'''',''''PHYY'''')
								  and vvo.ORDER_NBR IN (''''' + @ordrnbrs + ''''')
								  UNION
								  Select DISTINCT vvo.ORDER_NBR,
									vvca.CIS_HIER_LVL_CD,
									vvca.SITE_ID,
									vvca.LINE_1,
									vvca.LINE_2,
									vvca.LINE_3,
									vvca.CTY_NME,
									vvca.ST_PRVN_CD,
									vvca.PSTL_CD,
									vvca.CTRY_CD,
									vvca.BLDG_ID,
									vvca.ROOM_ID,
									vvca.FLOOR_ID
												From MACH5.V_V5U_ORDR vvo
								  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								  inner join MACH5.V_V5U_CUST_ACCT vvca2 ON vvca2.H1 = vvca.CIS_CUST_ID
								  WHERE vvca2.CUST_ACCT_ID IN (' + @acctids + ')
								  AND vvca.TYPE_CD=DECODE(vvca.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''')
								  and vvo.ORDER_NBR IN (''''' + @ordrnbrs + ''''') '')'

								  --select @adrinfo
								  exec sp_executesql @adrinfo
		
			UPDATE ma
			SET CTRY_CD = CTRY_NME
			FROM #M5ADRTable ma INNER JOIN
				 dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = ma.CTRY_CD
			
			declare @continfo nvarchar(max) = 'INSERT INTO #M5CUSTTable (M5_ORDR_NBR,
			INST_CNTCT_NME,
			SRVC_CNTCT_NME,
			INST_DOM_INTL_IND_CD,
			INST_DOM_PHN_NBR,
			SRVC_DOM_INTL_IND_CD,
			SRVC_DOM_PHN_NBR,
			SRVC_TME_ZN_CD,
			SRVC_AVLBLTY_HRS) Select ORDER_NBR
					,INST_CUST_NME
					,SRVC_CUST_NME
					,INST_ISD_CD
					,INST_PHN_NBR
					,SRVC_ISD_CD
					,SRVC_PHN_NBR
					,SRVC_TME_ZN_CD
					,SRVC_AVLBLTY_HRS  from openquery(M5,''Select DISTINCT vvo.ORDER_NBR,
									vatc.FIRST_NME || '''' ''''|| vatc.LAST_NME as SRVC_CUST_NME,
									vatc2.FIRST_NME || '''' ''''|| vatc2.LAST_NME as INST_CUST_NME,
									vatc.INTL_CNTRY_DIAL_CD AS SRVC_ISD_CD,
									vatc2.INTL_CNTRY_DIAL_CD AS INST_ISD_CD,
														CASE	WHEN vatc.DOM_INTL_IND_CD = ''''D'''' THEN vatc.DOM_PHN_NBR
																WHEN vatc.DOM_INTL_IND_CD = ''''I'''' THEN vatc.INTL_PHN_NBR   
														END AS  SRVC_PHN_NBR, 
									  CASE	WHEN vatc2.DOM_INTL_IND_CD = ''''D'''' THEN vatc2.DOM_PHN_NBR
																WHEN vatc2.DOM_INTL_IND_CD = ''''I'''' THEN vatc2.INTL_PHN_NBR
														END AS  INST_PHN_NBR,
									  '''''''' AS SRVC_TME_ZN_CD,
									  '''''''' AS SRVC_AVLBLTY_HRS
												From MACH5.V_V5U_ORDR vvo
								  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								  left outer join Mach5.v_V5U_ACCT_TEAM_CONTACT vatc ON vatc.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								  left outer join Mach5.v_V5U_ACCT_TEAM_CONTACT vatc2 ON vatc2.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
												WHERE vvca.CUST_ACCT_ID IN (' + @acctids + ')
								  and (vatc.ROLE_CD IN (''''SVCA'''')
								  or vatc2.ROLE_CD IN (''''CONT''''))
								  and vvo.ORDER_NBR IN (''''' + @ordrnbrs + ''''')
					UNION
					Select DISTINCT vvo.ORDER_NBR,
									vcc.CUST_NME as SRVC_CUST_NME,
									vcc2.CUST_NME as INST_CUST_NME,
									vcc.INTL_CNTRY_DIAL_CD AS SRVC_ISD_CD,
									vcc2.INTL_CNTRY_DIAL_CD AS INST_ISD_CD,
														CASE	WHEN vcc.DOM_INTL_IND_CD = ''''D'''' THEN vcc.DOM_PHN_NBR
																WHEN vcc.DOM_INTL_IND_CD = ''''I'''' THEN vcc.INTL_PHN_NBR   
														END AS  SRVC_PHN_NBR, 
									  CASE	WHEN vcc2.DOM_INTL_IND_CD = ''''D'''' THEN vcc2.DOM_PHN_NBR
																WHEN vcc2.DOM_INTL_IND_CD = ''''I'''' THEN vcc2.INTL_PHN_NBR
														END AS  INST_PHN_NBR,
									  vcc.TME_ZN_CD AS SRVC_TME_ZN_CD,
									  vcc.AVLBLTY_HR_STRT || '''' - ''''|| vcc.AVLBLTY_HR_END AS SRVC_AVLBLTY_HRS
												From MACH5.V_V5U_ORDR vvo
								  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								  left outer join Mach5.v_V5U_CUST_CONTACT vcc ON vcc.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								  left outer join Mach5.v_V5U_CUST_CONTACT vcc2 ON vcc2.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
												WHERE vvca.CUST_ACCT_ID IN (' + @acctids + ')
								  and (vcc.ROLE_CD IN (''''SVCA'''')
								  or vcc2.ROLE_CD IN (''''CONT''''))
								  and vvo.ORDER_NBR IN (''''' + @ordrnbrs + ''''') 
					UNION
					Select DISTINCT vvo.ORDER_NBR,
									vcc.CUST_NME as SRVC_CUST_NME,
									vcc.CUST_NME as INST_CUST_NME,
									atc.INTL_CNTRY_DIAL_CD AS SRVC_ISD_CD,
									atc.INTL_CNTRY_DIAL_CD AS INST_ISD_CD,
														CASE	WHEN atc.DOM_INTL_IND_CD = ''''D'''' THEN atc.DOM_PHN_NBR
																WHEN atc.DOM_INTL_IND_CD = ''''I'''' THEN atc.INTL_PHN_NBR   
														END AS  SRVC_PHN_NBR, 
									  CASE	WHEN atc.DOM_INTL_IND_CD = ''''D'''' THEN atc.DOM_PHN_NBR
																WHEN atc.DOM_INTL_IND_CD = ''''I'''' THEN atc.INTL_PHN_NBR
														END AS  INST_PHN_NBR,
									  vcc.TME_ZN_CD AS SRVC_TME_ZN_CD,
									  vcc.AVLBLTY_HR_STRT || '''' - ''''|| vcc.AVLBLTY_HR_END AS SRVC_AVLBLTY_HRS
							From MACH5.V_V5U_ORDR vvo
								  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								  left outer join Mach5.v_V5U_CUST_CONTACT vcc ON vcc.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
								  INNER JOIN Mach5.V_V5U_ACCT_TEAM_CONTACT_ORDR atc on atc.ORDR_ID = vvo.ORDR_ID
									    WHERE vvca.CUST_ACCT_ID IN (' + @acctids + ')
												  and vcc.ROLE_CD IN (''''SVCA'''',''''CONT'''')
												  and vvo.ORDER_NBR IN (''''' + @ordrnbrs + ''''') '')'

								
								  exec sp_executesql @continfo
	END

	IF NOT EXISTS(SELECT 'X' FROM #M5ADRTable)
		BEGIN
			SET @adrinfo = 'INSERT INTO #M5ADRTable (M5_ORDR_NBR,
				CIS_HIER_LVL_CD,
				SITE_ID,
				LINE_1,
				LINE_2,
				LINE_3,
				CTY_NME,
				ST_PRVN_CD,
				PSTL_CD,
				CTRY_CD,
				BLDG_ID,
				ROOM_ID,
				FLOOR_ID) Select ORDER_NBR,
					CIS_HIER_LVL_CD,
					SITE_ID,
					LINE_1,
					LINE_2,
					LINE_3,
					CTY_NME,
					ST_PRVN_CD,
					PSTL_CD,
					CTRY_CD,
					BLDG_ID,
					ROOM_ID,
					FLOOR_ID  from openquery(M5,''Select DISTINCT '''''''' AS ORDER_NBR,
									vvca.CIS_HIER_LVL_CD,
									vvca.SITE_ID,
									vvca.LINE_1,
									vvca.LINE_2,
									vvca.LINE_3,
									vvca.CTY_NME,
									vvca.ST_PRVN_CD,
									vvca.PSTL_CD,
									vvca.CTRY_CD,
									vvca.BLDG_ID,
									vvca.ROOM_ID,
									vvca.FLOOR_ID
												From MACH5.V_V5U_CUST_ACCT vvca 
												WHERE vvca.CIS_CUST_ID = ''''' + @H6 + '''''
								  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
								  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
									or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
								  '')'

								  --print @adrinfo
								  exec sp_executesql @adrinfo
		
			UPDATE ma
			SET CTRY_CD = CTRY_NME
			FROM #M5ADRTable ma INNER JOIN
				 dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = ma.CTRY_CD
		END
			

	IF NOT EXISTS (SELECT 'X' FROM #M5CUSTTable)
		BEGIN
			SET @continfo = 'INSERT INTO #M5CUSTTable (M5_ORDR_NBR,
			INST_CNTCT_NME,
			SRVC_CNTCT_NME,
			INST_DOM_INTL_IND_CD,
			INST_DOM_PHN_NBR,
			SRVC_DOM_INTL_IND_CD,
			SRVC_DOM_PHN_NBR,
			SRVC_TME_ZN_CD,
			SRVC_AVLBLTY_HRS) Select ORDER_NBR
					,INST_CUST_NME
					,SRVC_CUST_NME
					,INST_ISD_CD
					,INST_PHN_NBR
					,SRVC_ISD_CD
					,SRVC_PHN_NBR
					,SRVC_TME_ZN_CD
					,SRVC_AVLBLTY_HRS  from openquery(M5,''Select DISTINCT  '''''''' ORDER_NBR,
									vatc.FIRST_NME || '''' ''''|| vatc.LAST_NME as SRVC_CUST_NME,
									vatc2.FIRST_NME || '''' ''''|| vatc2.LAST_NME as INST_CUST_NME,
									vatc.INTL_CNTRY_DIAL_CD AS SRVC_ISD_CD,
									vatc2.INTL_CNTRY_DIAL_CD AS INST_ISD_CD,
														CASE	WHEN vatc.DOM_INTL_IND_CD = ''''D'''' THEN vatc.DOM_PHN_NBR
																WHEN vatc.DOM_INTL_IND_CD = ''''I'''' THEN vatc.INTL_PHN_NBR   
														END AS  SRVC_PHN_NBR, 
									  CASE	WHEN vatc2.DOM_INTL_IND_CD = ''''D'''' THEN vatc2.DOM_PHN_NBR
																WHEN vatc2.DOM_INTL_IND_CD = ''''I'''' THEN vatc2.INTL_PHN_NBR
														END AS  INST_PHN_NBR,
									  '''''''' AS SRVC_TME_ZN_CD,
									  '''''''' AS SRVC_AVLBLTY_HRS
												From MACH5.V_V5U_CUST_ACCT vvca 
								  left outer join Mach5.v_V5U_ACCT_TEAM_CONTACT vatc ON vatc.CUST_ACCT_ID = vvca.CUST_ACCT_ID
								  left outer join Mach5.v_V5U_ACCT_TEAM_CONTACT vatc2 ON vatc2.CUST_ACCT_ID = vvca.CUST_ACCT_ID
												WHERE vvca.CIS_CUST_ID  = ''''' + @H6 + '''''
								  and (vatc.ROLE_CD IN (''''SVCA'''')
								  or vatc2.ROLE_CD IN (''''CONT''''))
								  
					UNION
					Select DISTINCT '''''''' ORDER_NBR,
									vcc.CUST_NME as SRVC_CUST_NME,
									vcc2.CUST_NME as INST_CUST_NME,
									vcc.INTL_CNTRY_DIAL_CD AS SRVC_ISD_CD,
									vcc2.INTL_CNTRY_DIAL_CD AS INST_ISD_CD,
														CASE	WHEN vcc.DOM_INTL_IND_CD = ''''D'''' THEN vcc.DOM_PHN_NBR
																WHEN vcc.DOM_INTL_IND_CD = ''''I'''' THEN vcc.INTL_PHN_NBR   
														END AS  SRVC_PHN_NBR, 
									  CASE	WHEN vcc2.DOM_INTL_IND_CD = ''''D'''' THEN vcc2.DOM_PHN_NBR
																WHEN vcc2.DOM_INTL_IND_CD = ''''I'''' THEN vcc2.INTL_PHN_NBR
														END AS  INST_PHN_NBR,
									  vcc.TME_ZN_CD AS SRVC_TME_ZN_CD,
									  vcc.AVLBLTY_HR_STRT || '''' - ''''|| vcc.AVLBLTY_HR_END AS SRVC_AVLBLTY_HRS
												From MACH5.V_V5U_CUST_ACCT vvca 
								  left outer join Mach5.v_V5U_CUST_CONTACT vcc ON vcc.CUST_ACCT_ID = vvca.CUST_ACCT_ID
								  left outer join Mach5.v_V5U_CUST_CONTACT vcc2 ON vcc2.CUST_ACCT_ID = vvca.CUST_ACCT_ID
												WHERE vvca.CIS_CUST_ID  = ''''' + @H6 + '''''
								  and (vcc.ROLE_CD IN (''''SVCA'''')
								  or vcc2.ROLE_CD IN (''''CONT'''')) '')'

								  --print @continfo
								  exec sp_executesql @continfo
		END		
	
	--Update NID_SERIAL_NBR from COWS Order
	UPDATE cpe
	SET NID_SERIAL_NBR = fc.NID_SERIAL_NBR
	from #M5CPETable cpe inner join 
	dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.FTN=cpe.M5_ORDR_NBR inner join
	dbo.ORDR od WITH (NOLOCK) ON od.ORDR_ID=fo.ORDR_ID inner join
	dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON fc.ORDR_ID=fo.ORDR_ID AND fc.CMPNT_FMLY='NID' AND fc.SPRINT_MNTD_FLG='Y'
	WHERE od.H5_H6_CUST_ID=@H6
	   AND fc.NID_SERIAL_NBR IS NOT NULL
	   
	UPDATE cpe
	SET NID_SERIAL_NBR = fc.NID_SERIAL_NBR
	from #M5CPETable cpe inner join 
	dbo.ORDR od WITH (NOLOCK) on cpe.ASSOC_H6 = od.H5_H6_CUST_ID AND od.H5_H6_CUST_ID=@H6 inner join
	dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON fc.ORDR_ID=od.ORDR_ID AND fc.CMPNT_FMLY='NID' AND fc.SPRINT_MNTD_FLG='Y'
	WHERE fc.NID_SERIAL_NBR IS NOT NULL
	  AND ISNULL(cpe.NID_SERIAL_NBR,'') = ''
	  
		--select * from #M5CPETable
	IF (@UCaaSCD = 'Y')
	select DISTINCT M5_ORDR_NBR as M5OrdrNbr, ORDR_ID as CPEOrdrID, COWS_ORDR_ID as OrdrID, DEVICE_ID as DeviceID, ASSOC_H6 as AssocH6, ISNULL(VNDR_PO,'N/A') as MntVndrPO, ISNULL(RQSTN_NBR,'N/A') as RqstnNbr  from #M5CPETable
	ELSE
	select DISTINCT M5_ORDR_NBR as M5OrdrNbr, ORDR_ID as CPEOrdrID, COWS_ORDR_ID as OrdrID, DEVICE_ID as DeviceID, ASSOC_H6 as AssocH6, ISNULL(VNDR_PO,'N/A') as MntVndrPO, ISNULL(RQSTN_NBR,'N/A') as RqstnNbr,
	CCD, CMPNT_STUS AS CmpntStus, ISNULL(RCPT_STUS,'No Order in COWS') AS RcptStus, ISNULL(DLVY_CLLI,'N/A') AS DLVY_CLLI, ISNULL(CPE_CMPN_FMLY,'N/A') as CPE_CMPN_FMLY, NID_SERIAL_NBR from #M5CPETable

	IF (@UCaaSCD = 'Y')
	select DISTINCT ISNULL(INST_CNTCT_NME,'N/A') as InstallSitePOCName, ISNULL(SRVC_CNTCT_NME,'N/A') as SrvcAssurncePOCName, 
	INST_DOM_PHN_NBR as InstallSitePOCPhn, CASE INST_DOM_INTL_IND_CD WHEN 'D' THEN '' ELSE  INST_DOM_INTL_IND_CD END AS InstallSitePOCPhnCD,
	SRVC_DOM_PHN_NBR as SrvcAssurncePOCPhn, CASE SRVC_DOM_INTL_IND_CD WHEN 'D' THEN '' ELSE  SRVC_DOM_INTL_IND_CD END AS SrvcAssurncePOCPhnCD
	from #M5CUSTTable
	ELSE
	select DISTINCT ISNULL(INST_CNTCT_NME,'N/A') as InstallSitePOCName, ISNULL(SRVC_CNTCT_NME,'N/A') as SrvcAssurncePOCName, 
	INST_DOM_PHN_NBR as InstallSitePOCPhn, CASE INST_DOM_INTL_IND_CD WHEN 'D' THEN '' ELSE  INST_DOM_INTL_IND_CD END AS InstallSitePOCPhnCD,
	SRVC_DOM_PHN_NBR as SrvcAssurncePOCPhn, CASE SRVC_DOM_INTL_IND_CD WHEN 'D' THEN '' ELSE  SRVC_DOM_INTL_IND_CD END AS SrvcAssurncePOCPhnCD, SRVC_TME_ZN_CD	as SrvcAssuranceTimeZone, SRVC_AVLBLTY_HRS as SrvcAssuranceAvlbltyHrs
	from #M5CUSTTable
	ORDER BY SRVC_TME_ZN_CD DESC

	IF (@UCaaSCD = 'Y')
	select DISTINCT ISNULL(SITE_ID,'N/A') as SiteID, CASE WHEN (LEN(ISNULL(LINE_1,'') + ISNULL(LINE_2,'') + ISNULL(LINE_3,'')) > 0) THEN ISNULL(LINE_1,'') + ISNULL(LINE_2,'') + ISNULL(LINE_3,'') ELSE 'N/A' END as StAddr, CASE WHEN (LEN(ISNULL(BLDG_ID,'') + ISNULL(FLOOR_ID,'') + ISNULL(ROOM_ID,'')) > 0) THEN ISNULL(BLDG_ID,'') +'/'+ ISNULL(FLOOR_ID,'') +'/'+ ISNULL(ROOM_ID,'') ELSE 'N/A' END as FlrBldg, 
	ISNULL(ST_PRVN_CD,'N/A') as ProvState, CTY_NME as City, PSTL_CD as ZipCD, CTRY_CD as Ctry  from #M5ADRTable
	ELSE
	select DISTINCT ISNULL(SITE_ID,'N/A') as SiteID, CASE WHEN (LEN(ISNULL(LINE_1,'') + ISNULL(LINE_2,'') + ISNULL(LINE_3,'')) > 0) THEN ISNULL(LINE_1,'') + ISNULL(LINE_2,'') + ISNULL(LINE_3,'') ELSE 'N/A' END as StAddr, CASE WHEN (LEN(ISNULL(BLDG_ID,'') + ISNULL(FLOOR_ID,'') + ISNULL(ROOM_ID,'')) > 0) THEN ISNULL(BLDG_ID,'') +'/'+ ISNULL(FLOOR_ID,'') +'/'+ ISNULL(ROOM_ID,'') ELSE 'N/A' END as FlrBldg, 
	ISNULL(ST_PRVN_CD,'N/A') as ProvState, CTY_NME as City, PSTL_CD as ZipCD, CTRY_CD as Ctry  from #M5ADRTable
	
	IF (@UCaaSCD = 'Y')
	 BEGIN
		declare @h1h6 nvarchar(max) = N'INSERT INTO #M5H1BasedOnH6
		 Select CUST_NME  from openquery(M5,''select Distinct vvca2.CUST_NME
									from MACH5.V_V5U_CUST_ACCT vvca INNER JOIN
									MACH5.V_V5U_CUST_ACCT vvca2 ON vvca.H1 = vvca2.CIS_CUST_ID
									where vvca.CIS_CUST_ID = '+ CONVERT(VARCHAR,@H6) +' '')'; 

		exec sp_executesql @h1h6
		
		SELECT ISNULL(CUST_NME,'N/A') as CustName FROM #M5H1BasedOnH6
	END


	IF (@UCaaSCD != 'Y')
	SELECT DISTINCT M5_ORDR_NBR
	 ,M5_ORDR_ID
	 ,CUST_ACCT_ID
	 ,ISNULL(DEVICE_ID,'0') AS DEVICE_ID
	 ,ORDR_CMPNT_ID
	 ,RLTD_CMPNT_ID
	 ,MNS_SRVC_TIER
	 ,MDS_SRVC_TIER_ID
	 ,ISNULL(ASSOC_TRNSPRT_TYPE,'N/A') AS ASSOC_TRNSPRT_TYPE
	 ,ISNULL(ACCS_CXR_NME, 'N/A') AS ACCS_CXR_NME
	 ,CMPNT_STUS, CMPNT_TYPE_CD, NME FROM #M5MNSTABLE


	--Get Rel MNS Order Data from M5
	
	select DISTINCT M5_ORDR_NBR
	 ,M5_ORDR_ID
	 ,CUST_ACCT_ID
	 ,ISNULL(DEVICE_ID,'0') AS DEVICE_ID
	 ,ORDR_CMPNT_ID
	 ,RLTD_CMPNT_ID
	 ,MNS_SRVC_TIER
	 ,MDS_SRVC_TIER_ID
	 ,CMPNT_STUS, CMPNT_TYPE_CD, NME from #M5RELMNSTABLE

	 
	 IF (@UCaaSCD = 'N')
	 BEGIN
		IF @IsNewUI = 0
		BEGIN
			IF EXISTS (SELECT 'X' FROM #M5CPETable WHERE CTRCT_TYPE='ASAS')
			BEGIN
				SELECT DISTINCT M5_ORDR_NBR
					,ORDR_ID AS M5_ORDR_ID
					,-1 AS CUST_ACCT_ID
					,ISNULL(DEVICE_ID,'0') AS DEVICE_ID
					,NULL AS ORDR_CMPNT_ID
					,NULL AS RLTD_CMPNT_ID
					,CASE CPE_MFR_PART_NBR WHEN 'MNS-COLABS' THEN 'MNS Collaborative Solution' ELSE 'MNS Complete Solution' END AS MNS_SRVC_TIER
					,CASE CPE_MFR_PART_NBR WHEN 'MNS-COLABS' THEN 52 ELSE 51 END AS MDS_SRVC_TIER_ID
					,'N/A' AS ASSOC_TRNSPRT_TYPE
					,'N/A' AS ACCS_CXR_NME
					,CMPNT_STUS, 'MNST' AS CMPNT_TYPE_CD,
					CASE CPE_MFR_PART_NBR WHEN 'MNS-COLABS' THEN 'MNS Collaborative Solution' ELSE 'MNS Complete Solution' END AS NME 
				FROM #M5CPETable
				WHERE CTRCT_TYPE='ASAS' AND CPE_MFR_PART_NBR IN ('MNS-COLABS','MNSCOMPLTS')
			END
			ELSE
				SELECT * FROM #M5CPETable WHERE CTRCT_TYPE='ASAS'
		END
		ELSE
		BEGIN
			SELECT DISTINCT M5_ORDR_NBR
				,ORDR_ID AS M5_ORDR_ID
				,-1 AS CUST_ACCT_ID
				,ISNULL(DEVICE_ID,'0') AS DEVICE_ID
				,NULL AS ORDR_CMPNT_ID
				,NULL AS RLTD_CMPNT_ID
				,CASE CPE_MFR_PART_NBR WHEN 'MNS-COLABS' THEN 'MNS Collaborative Solution' ELSE 'MNS Complete Solution' END AS MNS_SRVC_TIER
				,CASE CPE_MFR_PART_NBR WHEN 'MNS-COLABS' THEN 52 ELSE 51 END AS MDS_SRVC_TIER_ID
				,'N/A' AS ASSOC_TRNSPRT_TYPE
				,'N/A' AS ACCS_CXR_NME
				,CMPNT_STUS, 'MNST' AS CMPNT_TYPE_CD,
				CASE CPE_MFR_PART_NBR WHEN 'MNS-COLABS' THEN 'MNS Collaborative Solution' ELSE 'MNS Complete Solution' END AS NME 
			FROM #M5CPETable
			WHERE CTRCT_TYPE='ASAS' AND CPE_MFR_PART_NBR IN ('MNS-COLABS','MNSCOMPLTS')
		END
	 END
	 
	 IF (@UCaaSCD = 'N')
	 BEGIN
		declare @portordr nvarchar(max) = 'INSERT INTO #M5PORTTABLE (M5_ORDR_NBR, M5_ORDR_CMPNT_ID, CUST_ACCT_ID, NUA, PORT_BNDWD)
		 Select ORDER_NBR
		,ORDR_CMPNT_ID
		,CIS_CUST_ID
		,NTWK_USR_ADR
		,PORT_SPD_CD  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
		,voc.ORDR_CMPNT_ID
		,vvca.CIS_CUST_ID
		,voc.NTWK_USR_ADR
		,voc.PORT_SPD_CD
									from MACH5.V_V5U_ORDR_CMPNT voc
									inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
					  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.MOVE_TO_ACCT_ID
									where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
					  and voc.cmpnt_type_cd = ''''PORT''''
					  and voc.STUS_CD NOT IN (''''CN'''',''''RJ'''')
					  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
					  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
						or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
					  and vvo.ORDR_TYPE_CD = ''''CH''''
					  and (vvca.CIS_CUST_ID = ''''' + @H6 + ''''') '')'

		exec sp_executesql @portordr

		IF NOT EXISTS(SELECT 'x' FROM #M5PORTTABLE)
		BEGIN
			set @portordr = 'INSERT INTO #M5PORTTABLE (M5_ORDR_NBR, M5_ORDR_CMPNT_ID, CUST_ACCT_ID, NUA, PORT_BNDWD)
			 Select ORDER_NBR
			,ORDR_CMPNT_ID
			,CIS_CUST_ID
			,NTWK_USR_ADR
			,PORT_SPD_CD  from openquery(M5,''select DISTINCT vvo.ORDER_NBR
			,voc.ORDR_CMPNT_ID
			,vvca.CIS_CUST_ID
			,voc.NTWK_USR_ADR
			,voc.PORT_SPD_CD
										from MACH5.V_V5U_ORDR_CMPNT voc
										inner join MACH5.V_V5U_ORDR vvo ON vvo.ORDR_ID = voc.ORDR_ID
						  inner join MACH5.V_V5U_CUST_ACCT vvca ON vvca.CUST_ACCT_ID = vvo.PRNT_ACCT_ID
										where TRUNC(vvo.CUST_CMMT_DT)=TO_DATE(''''' + @CCD + ''''',''''mm/dd/yyyy'''')
						  and voc.cmpnt_type_cd = ''''PORT''''
						  and voc.STUS_CD NOT IN (''''CN'''',''''RJ'''')
						  and vvca.CIS_HIER_LVL_CD IN (''''H5'''',''''H6'''') 
						  and ((vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
							or (vvca.TYPE_CD = DECODE(vvca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
						  and vvo.ORDR_TYPE_CD = ''''CH''''
						  and (vvca.CIS_CUST_ID = ''''' + @H6 + ''''') '')'

			exec sp_executesql @portordr
		END

		SELECT * FROM #M5PORTTABLE
	 END
 END
 
	--Get CPE Dispatch Email from SSTAT
	IF (@SSTAT_WFM_CD = 1)
	BEGIN
		DECLARE @ZipCd VARCHAR(255) = ''
		select TOP 1 @ZipCd = CASE WHEN (charindex('-',PSTL_CD) > 0) THEN substring(PSTL_CD,1,5) ELSE PSTL_CD END from #M5ADRTable
		declare @cowspdl nvarchar(max) = 'SELECT ISNULL(COWS_PDL,'''') as [CPEDispatchEmail] from openquery(SSTAT_WFM,''select top 1 COWS_PDL from dbo.vZipCodeCLLIReference where Zip_Code='''''+ @ZipCd + ''''' '')'
		exec sp_executesql @cowspdl
		--SELECT 'Michael.V.Potter@sprint.com' AS [CPEDispatchEmail]
	END
	ELSE
		SELECT '' as [CPEDispatchEmail]
		
	IF ((@UCaaSCD != 'Y') AND (@NRMBPM_CD = 1))
	BEGIN
	/* declare @ordrids varchar(max)
	select @ordrids = ISNULL(@ordrids,'') + ISNULL(M5_ORDR_NBR,'') + ''''', '''''
	from #M5CPETable
	select @ordrids = ISNULL(@ordrids,'') + ISNULL(M5_ORDR_NBR,'') + ''''', '''''
	from #M5MNSTABLE
	if(len(@ordrids)>2)
	set  @ordrids = substring(@ordrids, 1, len(@ordrids)-6) */

	declare @cktinfo nvarchar(max) = 'INSERT INTO #NRMWIRED (H6_ID,PROD_ID
	,VEN_CIRCUIT_ID
	,VNDR_NME
	,BDWD_NME
	--,PLN
	--,SPA_NUA
	--,MULTILINK_CIRCUIT
	--,DEDCTD_ACCS_CD
	--,FMS_CKT_ID
	,VLAN_ID
	,IP_NUA
	,SPRINT_MNGE_NME
	--, DLCI_VPI
	,READY_BEGIN_FLG_NME
	,OPT_IN_CKT_CD, OPT_IN_H_CD) Select DISTINCT H6_ID
	,PROD_ID
	,VEN_CIRCUIT_ID
	,VNDR_NME
	,BDWD_NME
	--,PLN
	--,SPA_NUA
	--,MULTILINK_CIRCUIT
	--,DEDCTD_ACCS_CD
	--,FMS_CKT_ID
	,VLAN_ID
	,IP_NUA
	,SPRINT_MNGE_NME
	--,LAYER_2_ID  
	,READY_BEGIN_FLG_NME
	,OPT_IN_CKT_CD, OPT_IN_H_CD
		from openquery(NRMBPM,''select B.H6_ID, B.PROD_ID
											,B.VEN_CIRCUIT_ID
											,B.VNDR_NME
											,B.BDWD_NME
											--,B.PLN
											--,B.SPA_NUA
											--,B.MULTILINK_CIRCUIT
											--,B.DEDCTD_ACCS_CD
											--,B.FMS_CKT_ID
											,B.VLAN_ID
											,COALESCE(B.IP_NUA,B.SPA_NUA) AS IP_NUA
											,B.SPRINT_MNGE_NME
											--,B.LAYER_2_ID
											,M.READY_BEGIN_FLG_NME
											,M.OPT_IN_CKT_CD
											,M.OPT_IN_H_CD
								from BPMF_OWNER.V_BPMF_COWS_CKT B 
								LEFT JOIN BPMF_OWNER.V_BPMF_MONTR_COWS_CKT M
								ON B.H6_ID = M.H6_ID
								where B.H6_ID = '''''+ @H6 + '''''
								  AND B.VNDR_NME IS NOT NULL
								  AND B.IP_NUA IS NOT NULL	'')'
								 -- modify this to join with  new view and add two new columns -- n.s
									--print @cktinfo
	exec sp_executesql @cktinfo
	END	
	
	IF (@UCaaSCD != 'Y')
		SELECT DISTINCT H6_ID, ISNULL(PROD_ID,'N/A') AS PROD_ID
		,SUBSTRING(ISNULL(VEN_CIRCUIT_ID,'N/A'),1,50) AS VEN_CIRCUIT_ID
		,ISNULL(VNDR_NME,'N/A') AS VNDR_NME
		,ISNULL(BDWD_NME,'N/A') AS BDWD_NME
		--,ISNULL(PLN,'N/A') AS PLN
		--,ISNULL(SPA_NUA,'N/A') AS SPA_NUA
		--,ISNULL(MULTILINK_CIRCUIT,'No') AS MULTILINK_CIRCUIT
		--,ISNULL(DEDCTD_ACCS_CD,'No') AS DEDCTD_ACCS_CD
		--,ISNULL(FMS_CKT_ID,'N/A') AS FMS_CKT_ID
		,ISNULL(VLAN_ID,'N/A') AS VLAN_ID
		,ISNULL(IP_NUA,'N/A') AS IP_NUA
		,ISNULL(SPRINT_MNGE_NME,'No') AS SPRINT_MNGE_NME
		--,ISNULL(DLCI_VPI,'N/A') AS DLCI_VPI, 
		,READY_BEGIN_FLG_NME, OPT_IN_CKT_CD, OPT_IN_H_CD FROM #NRMWIRED
	
END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END