USE [COWS]
GO
_CreateObject 'SP','dbo','loadRTSEmailNotification'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================================
-- Author:		sbg9814
-- Create date: 08/17/2011
-- Description:	Load an Email notification to the SIS-Primary Contact when the order moves to RTS.
-- ==================================================================================
ALTER PROCEDURE [dbo].[loadRTSEmailNotification] 
	@OrderID	Int,
	@TaskID		SmallInt	=	500,
	@TaskStatus	TinyInt		=	0,
	@Comments	Varchar(1000) = NULL
AS
BEGIN
DECLARE	@UserADID	Varchar(10)
DECLARE	@Email		Varchar(50)
DECLARE	@CurrDate	DateTime
SET	@UserADID	=	''
SET	@Email		=	''
SET	@CurrDate	=	getDate()

Begin Try
	------------------------------------------------------------------
	-- Create an Email request notifying the RTS User.
	------------------------------------------------------------------
	EXEC	dbo.insertEmailRequest	@OrderID,	1,	10	
			
	------------------------------------------------------------------
	-- Get the Email Address from the Order.
	------------------------------------------------------------------
	OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0;		
	SELECT Top 1	@Email		=	CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_EMAIL_ADR) ELSE oc.EMAIL_ADR END
		FROM		dbo.ORDR_CNTCT	oc	WITH (NOLOCK) INNER JOIN
					dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID=oc.ORDR_ID LEFT JOIN
					dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csd.SCRD_OBJ_TYPE_ID=15
		WHERE		oc.ORDR_ID	=	@OrderID
		AND			oc.ROLE_ID	=	11
	
	------------------------------------------------------------------
	-- Assign the Order if a valid Email Address is obtained.
	------------------------------------------------------------------
	IF	ISNULL(@Email, '')	!=	''
		BEGIN
			SELECT		@UserADID	=	USER_ADID
				FROM	dbo.LK_USER	WITH (NOLOCK)
				WHERE	EMAIL_ADR	=	@Email	
				
			IF	ISNULL(@UserADID, '')	!=	''
				BEGIN
					EXEC	[dbo].[insertWFMOrderAssignment]	
							@OrderID
							,4
							,@UserADID
							,1
							,@CurrDate
							,0
				END	
		END				
End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END