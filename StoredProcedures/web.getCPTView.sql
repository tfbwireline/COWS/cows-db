USE [COWS]
GO
_CreateObject 'SP','web','getCPTView'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 ================================================================================================  
 Author:		jrg7298  
 Create date:	7/11/2012  
 Description:	Gets the Data for CPT View
 Updated by:	qi931353
 Updated date:	6/19/2018
 Description:	Added parameter to be used in both Search and CPT View
 ================================================================================================  
*/

ALTER PROCEDURE [web].[getCPTView] 
	@CSGLvlId		TINYINT			= 0,
	@IsSearchView	TINYINT			= 0
 AS
BEGIN  
 
 SET NOCOUNT ON;  
  
 BEGIN TRY  

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

		DECLARE @CPTTbl TABLE (CptID INT, CptCustomerNumber VARCHAR(13), CustomerShortName VARCHAR(100), 
								ReturnedToSDE BIT, ReviewedByPM BIT, CompanyName VARCHAR(1000), 
								H1 VARCHAR(9),CntDevices SMALLINT, CreatedDateTime DATETIME, CreatedByUserName VARCHAR(1000))
		

		IF (@IsSearchView = 0)
			BEGIN
				INSERT INTO @CPTTbl
				SELECT DISTINCT cpt.CPT_ID, cpt.CPT_CUST_NBR, cpt.CUST_SHRT_NME, cpt.RETRN_SDE_CD, cpt.REVWD_PM_CD, 
						CASE WHEN (cpt.CSG_LVL_ID = 0) THEN cpt.COMPNY_NME ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'') END AS COMPNY_NME, cpt.H1, cpt.DEV_CNT, cpt.CREAT_DT, usr.DSPL_NME
				FROM dbo.CPT cpt WITH (NOLOCK) INNER JOIN
					 dbo.LK_USER usr WITH (NOLOCK) ON usr.[USER_ID]=cpt.CREAT_BY_USER_ID LEFT JOIN
					 dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID = cpt.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
				WHERE cpt.CPT_STUS_ID NOT IN (308,309,311)
				  AND cpt.REC_STUS_ID=1
				  AND ((cpt.CSG_LVL_ID = 0) OR ((@CSGLvlId!=0) AND (cpt.CSG_LVL_ID >= @CSGLvlId)))
			END
		ELSE
			BEGIN
				INSERT INTO @CPTTbl
				SELECT DISTINCT cpt.CPT_ID, cpt.CPT_CUST_NBR, cpt.CUST_SHRT_NME, cpt.RETRN_SDE_CD, cpt.REVWD_PM_CD, 
						CASE WHEN (cpt.CSG_LVL_ID = 0) THEN cpt.COMPNY_NME ELSE ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'') END AS COMPNY_NME, cpt.H1, cpt.DEV_CNT, cpt.CREAT_DT, usr.DSPL_NME
				FROM dbo.CPT cpt WITH (NOLOCK) INNER JOIN
					 dbo.LK_USER usr WITH (NOLOCK) ON usr.[USER_ID]=cpt.CREAT_BY_USER_ID LEFT JOIN
					 dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID = cpt.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
				WHERE cpt.CPT_STUS_ID NOT IN (308)
				  AND cpt.REC_STUS_ID=1
				  AND ((cpt.CSG_LVL_ID = 0) OR ((@CSGLvlId!=0) AND (cpt.CSG_LVL_ID >= @CSGLvlId)))
			END


		SELECT * FROM @CPTTbl

		
		SELECT DISTINCT cpt.CptID,
						cph.CPT_HIST_ID AS HistoryID,
                        cph.ACTN_ID AS ActionID,
                        act.ACTN_DES AS [Action],
                        cph.CMNT_TXT AS HistComments,
                        lhu.DSPL_NME AS HistCreatedByDspNme,
                        lhu.CREAT_BY_USER_ID AS HistCreatedByUserID,
                        cph.CREAT_DT AS HistCreatedDateTime
		FROM @CPTTbl cpt INNER JOIN
		     dbo.CPT_HIST cph WITH (NOLOCK) ON cph.CPT_ID=cpt.CptID INNER JOIN
			 dbo.LK_ACTN act WITH (NOLOCK) ON act.ACTN_ID = cph.ACTN_ID INNER JOIN
			 dbo.LK_USER lhu WITH (NOLOCK) ON cph.CREAT_BY_USER_ID = lhu.[USER_ID]

		SELECT DISTINCT cpt.CptID,
				ua.ROLE_ID AS AsmtRoleID,
               ua.CPT_USER_ID as AsmtUserID,
               usr.FULL_NME AS AsmtUserName
		FROM @CPTTbl cpt INNER JOIN
		     dbo.CPT_USER_ASMT ua WITH (NOLOCK) ON ua.CPT_ID=cpt.CptID INNER JOIN
			 dbo.LK_USER usr WITH (NOLOCK) ON usr.[USER_ID] = ua.CPT_USER_ID
		WHERE ua.REC_STUS_ID=1

		SELECT DISTINCT cpt.CptID, ldu.FULL_NME AS LockedByUser, crl.LOCK_BY_USER_ID AS LockedByUserID
		FROM @CPTTbl cpt INNER JOIN
		     dbo.CPT_REC_LOCK crl WITH (NOLOCK) ON crl.CPT_ID = cpt.CptID INNER JOIN
			 dbo.LK_USER ldu WITH (NOLOCK) ON ldu.[USER_ID] = crl.LOCK_BY_USER_ID

 END TRY  
  
 BEGIN CATCH  
	EXEC [dbo].[insertErrorInfo]  
 END CATCH  
  
END