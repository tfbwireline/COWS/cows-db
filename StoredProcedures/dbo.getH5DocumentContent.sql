﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getH5DocumentContent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 16th Sep 2011
-- Description:	Returns content of H5 Document
-- ================================================================

ALTER PROCEDURE [dbo].[getH5DocumentContent]
	@H5DocumentID INT 	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		FILE_CNTNT
	FROM 
		dbo.H5_DOC	WITH (NOLOCK)
	WHERE 
		H5_DOC_ID = @H5DocumentID
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END