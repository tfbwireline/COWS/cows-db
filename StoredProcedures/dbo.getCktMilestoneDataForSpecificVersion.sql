USE COWS
GO
_CreateObject 'SP','dbo','getCktMilestoneDataForSpecificVersion'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <09/06/2011>
-- Description:	<Retrieve Ckt Milestone Data for specific version of an order>
-- =============================================
-- Modify:		<Kyle Wichert>
-- Create date: <09/06/2011>
-- Description:	modify to get max version as not all circuits will be on the same version
-- =============================================
ALTER PROCEDURE dbo.getCktMilestoneDataForSpecificVersion 
@ORDR_ID INT,
@VER_ID  SMALLINT	
AS
BEGIN
	BEGIN TRY
		SELECT	ISNULL(c.CKT_ID,0) AS CktID
				,ISNULL(c.ORDR_ID,0) AS OrderID
				,ISNULL(cs.VER_ID,0) AS VersionID
				,ISNULL(c.VNDR_CKT_ID,'') AS VendorCktID
				,ISNULL(c.PL_SEQ_NBR,'') AS PLSeq
				,ISNULL(c.LEC_ID,0) AS LECID
				,ISNULL(dbo.convertOrderMilestonedates(cs.TRGT_DLVRY_DT_RECV_DT),'') AS TDDR
				,ISNULL(dbo.convertOrderMilestonedates(cs.TRGT_DLVRY_DT),'') AS  TDD
				,ISNULL(dbo.convertOrderMilestonedates(cs.ACCS_DLVRY_DT),'') AS [ADD]
				,ISNULL(dbo.convertOrderMilestonedates(cs.ACCS_ACPTC_DT),'') AS AAD
				,ISNULL(dbo.convertOrderMilestonedates(cs.VNDR_CNFRM_DSCNCT_DT),'') AS VCDD
				,ISNULL(vo.TRMTG_CD,0) as IsTerm
		FROM	dbo.CKT c WITH (NOLOCK)
	LEFT JOIN	dbo.CKT_MS cs		WITH (NOLOCK) ON cs.CKT_ID = c.CKT_ID
	LEFT JOIN	dbo.VNDR_ORDR vo	WITH (NOLOCK) ON vo.VNDR_ORDR_ID = c.VNDR_ORDR_ID
		WHERE	c.ORDR_ID = @ORDR_ID
			AND	cs.VER_ID = (SELECT MAX(VER_ID) from dbo.CKT_MS csMax WITH (NOLOCK) WHERE csMax.CKT_ID = cs.CKT_ID)
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	
END
GO
