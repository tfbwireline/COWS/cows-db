USE COWS
GO
_CreateObject 'SP','dbo','insertUpdateCSCInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===========================================================================
-- Created By:		sbg9814
-- Created Date:	08/30/2011
-- Description:		Insert the CSC Info for a given Order.
-- ===========================================================================       
ALTER PROCEDURE [dbo].[insertUpdateCSCInfo]
	@OrderID				Int
	,@SERIAL_NBR			Varchar(30)
	,@MTRL_RQSTN_TXT		Varchar(100)
	,@PRCH_ORDR_TXT			Varchar(30)
	,@INTL_MNTC_SCHRG_TXT	Varchar(100)
	,@USER_ID				Int
AS      
BEGIN
SET DEADLOCK_PRIORITY 5
Begin Try
	IF	NOT EXISTS
		(SELECT 'X' FROM dbo.FSA_ORDR_CSC	WITH (NOLOCK)
			WHERE	ORDR_ID	=	@OrderID)
		BEGIN
			INSERT INTO	dbo.FSA_ORDR_CSC	WITH (ROWLOCK)
						(ORDR_ID
						,SERIAL_NBR
						,MTRL_RQSTN_TXT
						,PRCH_ORDR_TXT
						,INTL_MNTC_SCHRG_TXT
						,CREAT_BY_USER_ID)
				VALUES	(@OrderID
						,@SERIAL_NBR
						,@MTRL_RQSTN_TXT
						,@PRCH_ORDR_TXT
						,@INTL_MNTC_SCHRG_TXT
						,@USER_ID)				
		END
	ELSE
		BEGIN
			UPDATE		dbo.FSA_ORDR_CSC	WITH (ROWLOCK)
				SET		SERIAL_NBR				=	@SERIAL_NBR
						,MTRL_RQSTN_TXT			=	@MTRL_RQSTN_TXT
						,PRCH_ORDR_TXT			=	@PRCH_ORDR_TXT
						,INTL_MNTC_SCHRG_TXT	=	@INTL_MNTC_SCHRG_TXT	
						,MODFD_BY_USER_ID		=	@USER_ID
						,MODFD_DT				=	getDate()
				WHERE	ORDR_ID					=	@OrderID	
		END					
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo] 
End Catch		
END