USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[_CreateObject]    Script Date: 10/05/2010 09:10:35 ******/
--This is used to create a blank db object
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[_CreateObject]
	@ObjectType	varchar(4),
    @ObjectSchema varchar(5),
	@ObjectName varchar(50)
AS
BEGIN
	IF @ObjectType = 'SP'
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema=@ObjectSchema and routine_name=@ObjectName)
		EXEC ('CREATE PROCEDURE ['+@ObjectSchema+'].['+@ObjectName+'] AS BEGIN SELECT ''WARNING: Default Definition''	END')
		RETURN
	END
	IF @ObjectType = 'SP-E'
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema=@ObjectSchema and routine_name=@ObjectName)
		EXEC ('CREATE PROCEDURE ['+@ObjectSchema+'].['+@ObjectName+'] WITH ENCRYPTION AS BEGIN SELECT ''WARNING: Default Definition''	END')
		RETURN
	END
	IF @ObjectType = 'FS'
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema=@ObjectSchema and routine_name=@ObjectName)
		EXEC ('CREATE FUNCTION ['+@ObjectSchema+'].['+@ObjectName+'] () RETURNS VARCHAR(27) AS BEGIN RETURN ''WARNING: Default Definition''	END')
		RETURN
	END
	IF @ObjectType = 'FT'
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema=@ObjectSchema and routine_name=@ObjectName)
		EXEC ('CREATE FUNCTION ['+@ObjectSchema+'].['+@ObjectName+'] () RETURNS TABLE AS RETURN (SELECT ''WARNING: Default Definition'' AS [Warning])') 
		RETURN 
	END
   IF @ObjectType = 'FTV'
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema=@ObjectSchema and routine_name=@ObjectName)
		EXEC ('CREATE FUNCTION ['+@ObjectSchema+'].['+@ObjectName+'] () RETURNS @Values TABLE (Warning nvarchar(27)) AS BEGIN INSERT @Values(Warning) VALUES(''WARNING: Default Definition''); RETURN END')
		RETURN 
	END
	IF @ObjectType = 'V'
	BEGIN
		IF NOT EXISTS (SELECT 1 FROM information_schema.views WHERE table_schema=@ObjectSchema and table_name=@ObjectName)
		EXEC ('CREATE VIEW ['+@ObjectSchema+'].['+@ObjectName+'] AS SELECT ''WARNING: Default Definition'' AS [Warning]')
		RETURN
	END
	IF @ObjectType = 'T'
	BEGIN
		IF EXISTS (SELECT 1 FROM information_schema.tables WHERE table_schema=@ObjectSchema and table_name=@ObjectName)
			RAISERROR(N'Table "%s" already exists. You must use a Data script to modify this table.',10,1,@ObjectName)
		RETURN
	END
	IF @ObjectType = 'S'
	BEGIN
		IF EXISTS (SELECT 1 FROM information_schema.schemata WHERE schema_name=@ObjectSchema)
			RAISERROR(N'Schema "%s" already exists.',10,1,@ObjectSchema)
		RETURN
	END
	IF @ObjectType = 'UT'
	BEGIN
		IF EXISTS (SELECT 1 FROM information_schema.domains WHERE domain_schema=@ObjectSchema and domain_name=@ObjectName)
			RAISERROR(N'Data type "%s" already exists.',10,1,@ObjectName)
		RETURN
	END

	RAISERROR (N'Unknown ObjectType "%s" passed to _CreateObject.',10,1,@ObjectType)
END
