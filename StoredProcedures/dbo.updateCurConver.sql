USE [COWS]
GO
_CreateObject 'SP','dbo','updateCurConver'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Kyle Wichert
-- Create date: 08/17/2011
-- Description:	Update Currency Conversion factors based on 
-- spreadsheet entries in dbo.SRC_CUR_FILE.
-- ================================================================

ALTER PROCEDURE [dbo].[updateCurConver]
@userID INT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRY

		
		DECLARE @currencyConv TABLE 
		(
			curCd	VARCHAR(3),
			conv	DECIMAL(26,16)
		)
		DECLARE	@TOrderParts TABLE
		(
			OrderPart	Varchar(30),
			OrderCtr	Int	IDENTITY(1, 1)
		)
		DECLARE @curCd	VARCHAR(3)
		DECLARE @conv	DECIMAL(26,16)
		DECLARE @cnt	INT
		DECLARE	@ctr	INT
		DECLARE @updStr	VARCHAR(MAX)
		
		--Get string to process from SRC_CUR_File
		SELECT TOP 1 @updStr = PRSD_CUR_LIST_TXT
		FROM dbo.SRC_CUR_FILE WITH (NOLOCK)
		WHERE	REC_STUS_ID = 1
		
		INSERT INTO @TOrderParts 
					(OrderPart)
		SELECT	StringId FROM [web].[ParseStringWithDelimiter](@updStr, '|')
		
		SELECT @cnt = COUNT(1) FROM @TOrderParts
		SET	@ctr = 1
		
		WHILE @ctr <= @cnt
		BEGIN
			SET @curCd	=	''
			SET	@conv	=	0
			
			SELECT @curCd = OrderPart FROM @TOrderParts WHERE OrderCtr = @ctr
			SET @ctr = @ctr + 1
			--SELECT @conv = Convert(Decimal(26,16), LTRIM(RTRIM(OrderPart))) FROM @TOrderParts WHERE OrderCtr = @ctr
			SELECT @conv = LTRIM(RTRIM(STR(OrderPart,26,16))) FROM @TOrderParts WHERE OrderCtr = @ctr
			SET @ctr = @ctr + 1 
			
			INSERT INTO @currencyConv	(curCd,		conv)
			VALUES						(@curCd,	@conv)
		END
		
		UPDATE	cur
		SET		CUR_CNVRSN_FCTR_FROM_USD_QTY = cc.conv,
				MODFD_DT = GETDATE(),
				MODFD_BY_USER_ID = @userID,
				CREAT_DT = GETDATE()
		FROM			dbo.LK_CUR		cur	WITH (ROWLOCK)
			INNER JOIN	@currencyConv	cc					ON	cur.CUR_ID	=	cc.curCd
		WHERE	ISNULL(cc.conv,0) <> 0
			
		UPDATE	scf
		SET		REC_STUS_ID = 0,
				PRCSD_DT	= GETDATE()
		FROM	dbo.SRC_CUR_FILE	scf	WITH (ROWLOCK)
		WHERE	REC_STUS_ID	=	1
		
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
	
END
