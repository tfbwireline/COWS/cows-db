USE [COWS]
GO
_CreateObject 'SP','dbo','completeFSAInitialTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		sbg9814
-- Create date: 07/30/2011
-- Description:	Complete the Initial Tasks for incoming FSA Orders.
-- =========================================================
ALTER PROCEDURE [dbo].[completeFSAInitialTask]
	@ORDR_ID	Int
AS
BEGIN
SET NOCOUNT ON;

DECLARE	@IsIntl			Bit
DECLARE	@H5FolderId		Int
DECLARE	@FSAActionId	Int
DECLARE	@FTN			varchar(20)
DECLARE	@H1				Int
DECLARE @RelOrderId		Int
DECLARE @PrntOrderId	Int
DECLARE @PrntFTN		varchar(20)
DECLARE @RelFTN			varchar(20)
DECLARE @PropId			Int
DECLARE	@OrderType		Varchar(2)
DECLARE @TaskId			Int
DECLARE	@PSDate			SmallDateTime
DECLARE	@SDate			SmallDateTime
DECLARE @UserWFM		TABLE (GRP_ID SMALLINT, ASN_USER_ID INT, ASN_BY_USER_ID INT, USR_PRF_ID SMALLINT)
DECLARE @PRNT_PPRT_ID	Int
DECLARE @ORDR_CAT_ID	TinyInt
DECLARE @PROD_TYPE_CD	Varchar(2)
--DECLARE @NO_PRE_SBMT	Bit

SET	@TaskId		=	100
SET	@PropId		=	0
SET	@PrntFTN	=	''
SET	@RelFTN		=	''
SET	@PrntOrderId=	0
--SET @NO_PRE_SBMT =  0

Begin Try
	SELECT			@FSAActionId	=	fo.ORDR_ACTN_ID
					,@FTN			=	fo.FTN
					,@IsIntl		=	od.DMSTC_CD
					,@H5FolderId	=	od.H5_FOLDR_ID
					,@H1			=	fc.CUST_ID
					,@OrderType		=	fo.ORDR_TYPE_CD
					,@PropId		=	od.PPRT_ID
					,@SDate			=	fo.CREAT_DT
					,@PrntFTN		=	ISNULL(fo.PRNT_FTN, '')
					,@RelFTN		=	ISNULL(fo.RELTD_FTN, '')
					,@RelOrderId	=	ISNULL(fp.ORDR_ID, 0)
					,@PSDate		=	fp.CREAT_DT
					,@ORDR_CAT_ID	=	od.ORDR_CAT_ID
					,@PROD_TYPE_CD	=	fo.PROD_TYPE_CD	
		FROM		dbo.ORDR			od	WITH (NOLOCK)	
		INNER JOIN	dbo.FSA_ORDR		fo	WITH (NOLOCK)	ON	od.ORDR_ID		=	fo.ORDR_ID
		LEFT JOIN	dbo.FSA_ORDR_CUST	fc	WITH (NOLOCK)	ON	fo.ORDR_ID		=	fc.ORDR_ID
															AND	fc.CIS_LVL_TYPE	=	'H1'
		LEFT JOIN	dbo.FSA_ORDR		fp	WITH (NOLOCK)	ON	fo.FTN			=	fp.FTN
															AND	fp.ORDR_ACTN_ID	=	1
		WHERE		od.ORDR_ID		=	@ORDR_ID		
	
	IF @ORDR_CAT_ID = 2
		BEGIN
			EXEC	dbo.insertOrderNotes	@ORDR_ID,	9,	'COWS received the order from FSA.', 1
		END
	ELSE IF @ORDR_CAT_ID = 6
		BEGIN
			EXEC	dbo.insertOrderNotes	@ORDR_ID,	9,	'COWS received the order from Mach5.', 1
		END	
			
	IF	@FSAActionId	=	2
		BEGIN
			-----------------------------------------------------------------------------
			-- For Move Orders the PRNT_FTN will be populated from the FTN on Pre-Submit.
			-----------------------------------------------------------------------------	
			IF	@RelOrderId =	0	AND	@PrntFTN != '' AND	@RelFTN	!=	''
				BEGIN
					SELECT		@RelOrderId		=	ISNULL(ORDR_ID, 0)
						FROM	dbo.FSA_ORDR	WITH (NOLOCK)	
						WHERE	FTN				=	@PrntFTN			
						AND		ORDR_ACTN_ID	=	1
				END
			
			
			-----------------------------------------------------------------------------
			--	Load the Pre-Submit Date and Submit Dates in the ORDR_MS Table.
			-----------------------------------------------------------------------------
			IF	@IsIntl	=	1
				AND	EXISTS
					(SELECT 'X' FROM dbo.LK_PPRT	WITH (NOLOCK)
							WHERE	PPRT_ID	=	@PropId
							AND		TRPT_CD	=	1)
				AND	NOT EXISTS	
					(SELECT 'X' FROM dbo.ORDR_MS WITH (NOLOCK)
							WHERE	ORDR_ID = @ORDR_ID)
				BEGIN
					INSERT INTO	dbo.ORDR_MS	WITH (ROWLOCK)
								(ORDR_ID
								,VER_ID
								,PRE_SBMT_DT
								,SBMT_DT
								,CREAT_BY_USER_ID)
						VALUES	(@ORDR_ID
								,1			
								,@PSDate
								,@SDate
								,1)						
				END		
						
			IF	ISNULL(@RelOrderId, 0) != 0
				BEGIN		

					INSERT INTO @UserWFM
					SELECT		GRP_ID,		ASN_USER_ID,	ASN_BY_USER_ID, USR_PRF_ID
						FROM	dbo.USER_WFM_ASMT	uwa WITH (NOLOCK) INNER JOIN
								dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID] = uwa.ASN_USER_ID
						WHERE	uwa.ORDR_ID		=	@RelOrderId			
						  AND	lu.REC_STUS_ID  =	1		

					-----------------------------------------------------------------------------
					--	Delete the Pre-Submit once the Submit is received.
					-----------------------------------------------------------------------------
					DECLARE	@PreSubmitCreateDt	SMALLDATETIME
					SELECT	@PreSubmitCreateDt	=	CREAT_DT
					FROM	dbo.FSA_ORDR WITH (NOLOCK)
					WHERE	ORDR_ID	=	@RelOrderId

					UPDATE	dbo.FSA_ORDR WITH (ROWLOCK) SET PRE_SBMT_CREAT_DT = @PreSubmitCreateDt WHERE ORDR_ID = @ORDR_ID

					EXEC	dbo.deletePartialFSAOrder	@RelOrderId
					
					-----------------------------------------------------------------------------
					--	Reload the WFM table with the data from the Pre-Submit.
					-----------------------------------------------------------------------------
					DELETE FROM		dbo.USER_WFM_ASMT WITH (ROWLOCK) 
							WHERE	ORDR_ID		=	@ORDR_ID	
							AND		(SELECT COUNT(1) FROM @UserWFM) > 0
							
					INSERT INTO dbo.USER_WFM_ASMT	
											(ORDR_ID
											,GRP_ID
											,ASN_USER_ID
											,ASN_BY_USER_ID
											,ASMT_DT
											,CREAT_DT,USR_PRF_ID)
						SELECT	DISTINCT	@ORDR_ID
											,GRP_ID
											,ASN_USER_ID
											,ASN_BY_USER_ID
											,GETDATE()
											,@PreSubmitCreateDt,USR_PRF_ID
							FROM			@UserWFM

					IF (SELECT COUNT(1) FROM @UserWFM) <= 0
					EXEC	dbo.insertOrderNotes	@ORDR_ID,	9,	'Related User not found for WFM Assignment.', 1
				END
			--ELSE
			--	BEGIN
			--		SET @NO_PRE_SBMT = 1
			--	END

			---------------------------------------------------------------------
			--	Associate the ParentID	by H5/H6 CUST_ID & CCD.
			---------------------------------------------------------------------
			-- UPDATE			o1
				-- SET			o1.PRNT_ORDR_ID	=	ISNULL(o2.ORDR_ID, o1.ORDR_ID)
				-- FROM		dbo.ORDR		o1	WITH (ROWLOCK)
				-- INNER JOIN	dbo.ORDR		o2	WITH (NOLOCK)	ON	o1.H5_H6_CUST_ID	=	o2.H5_H6_CUST_ID
				-- INNER JOIN	dbo.FSA_ORDR	f1	WITH (NOLOCK)	ON	o1.ORDR_ID			=	f1.ORDR_ID
				-- INNER JOIN	dbo.FSA_ORDR	f2	WITH (NOLOCK)	ON	o2.ORDR_ID			=	f2.ORDR_ID
				-- LEFT JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK)	ON	o2.PPRT_ID			=	lp.PPRT_ID
				-- WHERE		o1.ORDR_ID					=	@ORDR_ID
				-- AND			o1.ORDR_ID					<>	o2.ORDR_ID
				-- --AND			f1.FTN						<>	f2.FTN
				-- AND			f1.ORDR_ACTN_ID				=	f2.ORDR_ACTN_ID
				-- AND			f1.CUST_CMMT_DT				=	ISNULL(o2.CUST_CMMT_DT, f2.CUST_CMMT_DT)
				-- AND			f1.ORDR_ACTN_ID				=	2
				-- AND			((ISNULL(lp.ORDR_TYPE_ID,0)	<>	8) AND (o2.ORDR_STUS_ID <> 4)) --Eliminating cancels
				-- AND			NOT EXISTS
							-- (SELECT 'X' FROM dbo.ACT_TASK	WITH (NOLOCK)
								-- WHERE	ORDR_ID	=	o2.ORDR_ID
									-- AND	TASK_ID	IN	(1000, 1001)
									-- AND	STUS_ID	=	2)
				
			---------------------------------------------------------------------
			--	Load the ODIE Request for all Orders except Billing & Discos.
			---------------------------------------------------------------------	
			IF	(@OrderType	NOT IN ('BC', 'DC', 'CN'))
			AND	EXISTS
				(SELECT 'X' FROM	dbo.LK_PPRT	WITH (NOLOCK)	
							WHERE	PPRT_ID	=	@PropId
							AND		MDS_CD	=	1)
				BEGIN
					INSERT INTO dbo.ODIE_REQ 
							(ORDR_ID,	ODIE_MSG_ID,STUS_MOD_DT,H1_CUST_ID,	STUS_ID)
						VALUES	
							(@ORDR_ID,	1,			GETDATE(),	@H1,		10)

					EXEC dbo.insertOrderNotes @ORDR_ID,9,'Order Information successfully sent to ODIE.',1
				END	
		END
	
	---------------------------------------------------------------------
	--	Load the Email Request for all Cancel Orders.
	---------------------------------------------------------------------
	IF	(@OrderType	= 'CN')
		BEGIN
			IF @ORDR_CAT_ID	=	6 AND @PROD_TYPE_CD = 'CP'
				BEGIN
					EXEC dbo.ProcessMach5CPECancels @ORDR_ID 
				END
			ELSE
				BEGIN
					EXEC dbo.ProcessNonTransportCancels @ORDR_ID	
				END
			

			--EXEC	dbo.insertEmailRequest	@ORDR_ID,	2,	10
			
			--SELECT		@PrntOrderId	=	ISNULL(fo.ORDR_ID, 0),
			--			@PRNT_PPRT_ID	=	ISNULL(o.PPRT_ID,0)
			--	FROM	dbo.FSA_ORDR	fo	WITH (NOLOCK)
			--INNER JOIN	dbo.ORDR		o	WITH (NOLOCK) ON fo.ORDR_ID = o.ORDR_ID
			--	WHERE	FTN				=	@PrntFTN
					
				
			--EXEC [dbo].[deleteActiveTasks]	@PrntOrderId
			
			--UPDATE		dbo.ORDR	WITH (ROWLOCK)
			--	SET		ORDR_STUS_ID	=	4
			--	WHERE	ORDR_ID			=	@PrntOrderId
				
			--UPDATE		dbo.ORDR	WITH (ROWLOCK)
			--	SET		ORDR_STUS_ID	=	4,
			--			PPRT_ID			=	@PRNT_PPRT_ID -- To display cancelled order in grp specific completed view
			--	WHERE	ORDR_ID			=	@ORDR_ID

			----To remove related order association if exists & insert notes.
			--Exec dbo.removeRelatedOrderAssociation @PrntOrderId
				
		END
	ELSE IF @FSAActionId	=	3
		BEGIN
			SELECT		@PrntOrderId	=	ISNULL(ORDR_ID, 0)						
				FROM	dbo.FSA_ORDR	fo	WITH (NOLOCK)
				WHERE	FTN				=	@FTN
				AND		ORDR_ACTN_ID	!=	3

			UPDATE		dbo.ORDR	WITH (ROWLOCK)
				SET		ORDR_STUS_ID	=	6
				WHERE	ORDR_ID			=	@PrntOrderId	
				
			EXEC	dbo.deleteActiveTasks	@PrntOrderId
			EXEC	dbo.insertOrderNotes	@PrntOrderId,	9,	'Delete received on this FTN. Moving the order to Presubmit Delete status.', 1
			
			IF @PrntOrderId !=	0
				EXEC	dbo.deletePartialFSAOrder	@ORDR_ID
				
			UPDATE		dbo.FSA_ORDR	WITH (ROWLOCK)
				SET		ORDR_ACTN_ID	=	3
				WHERE	ORDR_ID			=	@PrntOrderId		
		END

	IF (@IsIntl	=	1) AND (@OrderType	NOT IN	('CN','AD'))
		BEGIN
			IF	 @H5FolderId IS NULL
				BEGIN
					EXEC	dbo.CompleteActiveTask	@ORDR_ID, @TaskId,	1, 'Unable to associate H5 Folder. Moving the order to Error.'
				END
			ELSE
				BEGIN
					EXEC	dbo.insertOrderNotes	@ORDR_ID,	9,	'H5 folder association completed successfully.', 1
				END
		END	

	
	---------------------------------------------------------------------
	--	Complete the GOM task for Submits only at the end.
	---------------------------------------------------------------------
	IF	(@FSAActionID = 2) 
	AND ((@IsIntl = 0) OR (@IsIntl = 1  AND (ISNULL(@H5FolderId,0) <> 0) OR @OrderType = 'AD'))		
	AND	EXISTS
		(SELECT 'X' FROM dbo.ACT_TASK WITH (NOLOCK)
			WHERE	ORDR_ID	=	@ORDR_ID
			AND		TASK_ID	=	@TaskId
			AND		STUS_ID	=	0)
		BEGIN
			--IF @NO_PRE_SBMT = 1
			--	BEGIN
			--		UPDATE	lu
			--		SET		lu.USER_ORDR_CNT	=	lu.USER_ORDR_CNT	-	1
			--		FROM	dbo.ORDR odr WITH (NOLOCK)  
			--	INNER JOIN	dbo.USER_WFM_ASMT uwa WITH (NOLOCK) ON odr.ORDR_ID	=	uwa.ORDR_ID
			--	INNER JOIN	dbo.LK_USER	lu	WITH (ROWLOCK)		ON lu.USER_ID	=	uwa.ASN_USER_ID
			--	LEFT JOIN	dbo.LK_PPRT lp WITH (NOLOCK)		ON lp.PPRT_ID = odr.PPRT_ID
			--	LEFT JOIN	dbo.LK_ORDR_TYPE lot WITH (NOLOCK)	ON lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
			--	WHERE	uwa.ORDR_ID					=	@ORDR_ID	
			--			AND		uwa.ASN_USER_ID		!=	0
			--			AND		lu.USER_ORDR_CNT	>	0
			--			AND		uwa.GRP_ID			=	1
			--	END
			EXEC	dbo.CompleteActiveTask	@ORDR_ID, @TaskId,	2, 'Completing GOM Submit Task.'	
		END	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

