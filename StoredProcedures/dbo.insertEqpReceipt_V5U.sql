USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertEqpReceipt_V5U]    Script Date: 01/23/2020 11:10:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
-- Author:	David Phillips
-- Create date: 7/8/2015
-- Description:	Updates Equip Received Date for CPE Line Items
--              when entered in Appian Eqp Receipts.
-- =============================================
ALTER PROCEDURE [dbo].[insertEqpReceipt_V5U] 
	@FSA_CPE_LINE_ITEM_ID	int ,
 	@PARTIALQTY		INT = 0,
 	@ACTION         VARCHAR(1), -- C:Complete, S:Send to SCM, I:Insert Date, D:Delete Date 
 	@ADID           VARCHAR(10) = null,
 	@ORDR_ID        INT = 0 
 	
AS
BEGIN TRY

	DECLARE @PreviousQty INT,
			@MDS_DES varchar(50),
			@Sent_Qty INT,
			@Sent_DT datetime
	
	SELECT @PreviousQty = ISNULL(RCVD_QTY,0) FROM dbo.FSA_ORDR_CPE_LINE_ITEM 
								WHERE FSA_CPE_LINE_ITEM_ID =  @FSA_CPE_LINE_ITEM_ID

	SELECT @MDS_DES = MDS_DES FROM dbo.FSA_ORDR_CPE_LINE_ITEM
							    WHERE  FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
	SELECT @Sent_Qty = ISNULL(SUM(RCVD_QTY),0) FROM dbo.PS_RCPT_QUEUE WITH (NOLOCK)
				WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
				GROUP BY FSA_CPE_LINE_ITEM_ID

	SET NOCOUNT ON;

	IF @ACTION = 'I'
	  BEGIN
			UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM
			SET EQPT_ITM_RCVD_DT =  GETDATE(),
				EQPT_RCVD_BY_ADID = @ADID,
				RCVD_QTY = CASE
								WHEN @PARTIALQTY = ORDR_QTY  THEN ORDR_QTY
								WHEN ISNULL(@PARTIALQTY,0) = 0		 THEN ORDR_QTY
								WHEN @PARTIALQTY > @PreviousQty THEN @PARTIALQTY
								WHEN @PARTIALQTY < @PreviousQty
										AND @PARTIALQTY >= @Sent_Qty  THEN @PARTIALQTY
								WHEN @PARTIALQTY < @PreviousQty
										AND @PARTIALQTY < @Sent_Qty  THEN @Sent_Qty
								ELSE @PARTIALQTY
							END  	
			WHERE FSA_CPE_LINE_ITEM_ID =  @FSA_CPE_LINE_ITEM_ID
				
      END  
    ELSE IF @ACTION = 'D'
			BEGIN
				IF ISNULL(@Sent_Qty,0) = 0
					 BEGIN
						 UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM
							SET EQPT_ITM_RCVD_DT =  NULL,
								EQPT_RCVD_BY_ADID = NULL,
								RCVD_QTY = NULL	
							WHERE FSA_CPE_LINE_ITEM_ID =  @FSA_CPE_LINE_ITEM_ID
					 END
				ELSE
					BEGIN
						 UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM
							SET EQPT_ITM_RCVD_DT =  (SELECT TOP 1 CREAT_DT FROM dbo.PS_RCPT_QUEUE WITH (NOLOCK)
													WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
													ORDER BY PO_RCVD_ID DESC)
								,EQPT_RCVD_BY_ADID = (SELECT TOP 1 EQPT_RCVD_BY_ADID FROM dbo.PS_RCPT_QUEUE WITH (NOLOCK)
													WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
													ORDER BY PO_RCVD_ID DESC)
								,RCVD_QTY = @Sent_Qty	
							WHERE FSA_CPE_LINE_ITEM_ID =  @FSA_CPE_LINE_ITEM_ID
					 END
						
			END
		ELSE IF @ACTION = 'S'
			BEGIN
				UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM
				SET EQPT_ITM_RCVD_DT =  GETDATE(),
					EQPT_RCVD_BY_ADID = @ADID,
					RCVD_QTY = CASE
									WHEN @PARTIALQTY = ORDR_QTY  THEN ORDR_QTY
									WHEN ISNULL(@PARTIALQTY,0) = 0		 THEN ORDR_QTY
									WHEN @PARTIALQTY > @PreviousQty THEN @PARTIALQTY
									WHEN @PARTIALQTY < @PreviousQty
											AND @PARTIALQTY >= @Sent_Qty  THEN @PARTIALQTY
									WHEN @PARTIALQTY < @PreviousQty
											AND @PARTIALQTY < @Sent_Qty  THEN @Sent_Qty
									ELSE @PARTIALQTY
								END     
				WHERE FSA_CPE_LINE_ITEM_ID =  @FSA_CPE_LINE_ITEM_ID
				
				IF (@PARTIALQTY > @PreviousQty) OR ISNULL(@PARTIALQTY,0) = 0
					BEGIN
						IF ISNULL((SELECT [PRCH_ORDR_NBR] FROM dbo.FSA_ORDR_CPE_LINE_ITEM
							 WHERE FSA_CPE_LINE_ITEM_ID =  @FSA_CPE_LINE_ITEM_ID),0) <> 0
							
							BEGIN 
						
								INSERT INTO [dbo].[PS_RCPT_QUEUE]
							   ([PRCH_ORDR_NBR],[ORDR_ID],[REQSTN_NBR],[FSA_CPE_LINE_ITEM_ID]
							   ,[EQPT_RCVD_BY_ADID],[RCVD_QTY],[PS_RCVD_STUS],[PS_SENT_DT]
							   ,[CREAT_DT],[RCVD_DT],[BATCH_SEQ])

							   (SELECT DISTINCT PRCH_ORDR_NBR
									   ,ORDR_ID
									   ,PLSFT_RQSTN_NBR
									   ,@FSA_CPE_LINE_ITEM_ID
									   ,@ADID
									   ,CASE
											WHEN @PARTIALQTY = ORDR_QTY  THEN ORDR_QTY
											WHEN ISNULL(@PARTIALQTY,0) = 0		 THEN ORDR_QTY
											WHEN @PARTIALQTY > @PreviousQty THEN @PARTIALQTY
											WHEN @PARTIALQTY < @PreviousQty
													AND @PARTIALQTY >= @Sent_Qty  THEN @PARTIALQTY
											WHEN @PARTIALQTY < @PreviousQty
													AND @PARTIALQTY < @Sent_Qty  THEN @Sent_Qty
											ELSE @PARTIALQTY
										END AS           RCVD_QTY
									   ,NULL, NULL, GETDATE(), GETDATE(),NULL           
							   FROM dbo.FSA_ORDR_CPE_LINE_ITEM WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID)
							END
					IF ISNULL(@PARTIALQTY,0) <> 0 AND @PARTIALQTY > @PreviousQty
					   BEGIN
							
							INSERT INTO [dbo].[ORDR_NTE]
								([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT],[MODFD_BY_USER_ID],[MODFD_DT])
							VALUES
								(6,@ORDR_ID,getdate(),1,1,'Partial Qty Received for  ' +
									@MDS_DES + ' -- Qty received was ' + CONVERT(varchar(4),(@PARTIALQTY - @PreviousQty)) +
									'  by ' + @ADID ,null,null)
					   END		
					END
				END
			
			ELSE IF @ACTION = 'C'
			  BEGIN
				UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM
				SET EQPT_ITM_RCVD_DT =  GETDATE(),
					EQPT_RCVD_BY_ADID = @ADID,
					RCVD_QTY = CASE
									WHEN @PARTIALQTY = ORDR_QTY  THEN ORDR_QTY
									WHEN ISNULL(@PARTIALQTY,0) = 0	 THEN ORDR_QTY
									WHEN @PARTIALQTY > @PreviousQty THEN @PARTIALQTY
									WHEN @PARTIALQTY < @PreviousQty
											AND @PARTIALQTY >= @Sent_Qty  THEN @PARTIALQTY
									WHEN @PARTIALQTY < @PreviousQty
											AND @PARTIALQTY < @Sent_Qty  THEN @Sent_Qty
									ELSE @PARTIALQTY
								END     
				WHERE FSA_CPE_LINE_ITEM_ID =  @FSA_CPE_LINE_ITEM_ID
				
				IF (@PARTIALQTY > @PreviousQty) OR ISNULL(@PARTIALQTY,0) = 0
					BEGIN
					
						IF ISNULL((SELECT [PRCH_ORDR_NBR] FROM dbo.FSA_ORDR_CPE_LINE_ITEM
							 WHERE FSA_CPE_LINE_ITEM_ID =  @FSA_CPE_LINE_ITEM_ID),0) <> 0
							
							BEGIN	
						
								INSERT INTO [dbo].[PS_RCPT_QUEUE]
							   ([PRCH_ORDR_NBR],[ORDR_ID],[REQSTN_NBR],[FSA_CPE_LINE_ITEM_ID]
							   ,[EQPT_RCVD_BY_ADID],[RCVD_QTY],[PS_RCVD_STUS],[PS_SENT_DT]
							   ,[CREAT_DT],[RCVD_DT],[BATCH_SEQ])

							   (SELECT DISTINCT PRCH_ORDR_NBR
									   ,ORDR_ID
									   ,PLSFT_RQSTN_NBR
									   ,@FSA_CPE_LINE_ITEM_ID
									   ,@ADID
									   ,CASE
											WHEN @PARTIALQTY = ORDR_QTY  THEN ORDR_QTY
											WHEN ISNULL(@PARTIALQTY,0) = 0		 THEN ORDR_QTY
											WHEN @PARTIALQTY > @PreviousQty THEN (@PARTIALQTY - @PreviousQty)
											WHEN @PARTIALQTY = @PreviousQty THEN ORDR_QTY
											ELSE @PARTIALQTY
											END AS           RCVD_QTY
									   ,NULL, NULL, GETDATE(), GETDATE(),NULL           
							   FROM dbo.FSA_ORDR_CPE_LINE_ITEM WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID)
							END
					IF ISNULL(@PARTIALQTY,0) <> 0 AND @PARTIALQTY > @PreviousQty
					   BEGIN
							INSERT INTO [dbo].[ORDR_NTE]
								([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT],[MODFD_BY_USER_ID],[MODFD_DT])
							VALUES
								(6,@ORDR_ID,getdate(),1,1,'Partial Qty Received for  ' +
									@MDS_DES + ' -- Qty received was ' + CONVERT(varchar(4),(@PARTIALQTY - @PreviousQty)) +
									'  by ' + @ADID ,null,null)
					   END		
					END

				END

	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH




