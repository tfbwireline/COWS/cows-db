USE [COWS]
GO
_CreateObject 'SP','web','insertMDSEventChanges'
GO
USE [COWS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================          
-- Author:  Suman Chitemella          
-- Create date: 2011-8-06          
-- Description: This SP is used to insert MDS Event Data Changes  
-- =========================================================          
          
ALTER PROCEDURE [web].[insertMDSEventChanges]      
 @iEventID BIGINT,         
 @EventChanges NVARCHAR(MAX) = NULL,        
 @ID INT OUTPUT         
AS        
     DECLARE @hDoc   INT     
     SET @ID = 0
BEGIN      
 SET NOCOUNT ON;      
       
 BEGIN TRY     
   
   IF (LEN(@EventChanges) > 10)            
   BEGIN            
    EXEC sp_xml_preparedocument @hDoc OUTPUT, @EventChanges            
  
 SELECT @iEventID, CHNG_TYPE_NME, GETDATE(), CREAT_BY_USER_ID     
    FROM OPENXML (@hDoc, '/DocumentElement/EventChanges', 2)            
    WITH (MDS_EVENT_ID INT,            
       CHNG_TYPE_NME VARCHAR(500),            
       CREAT_BY_USER_ID INT  
       )       
                   
    INSERT INTO MDS_EVENT_FORM_CHNG (MDS_EVENT_ID, CHNG_TYPE_NME, CREAT_DT, CREAT_BY_USER_ID)       
    SELECT @iEventID, CHNG_TYPE_NME, GETDATE(), CREAT_BY_USER_ID     
    FROM OPENXML (@hDoc, '/DocumentElement/EventChanges', 2)            
    WITH (MDS_EVENT_ID INT,            
       CHNG_TYPE_NME VARCHAR(500),            
       CREAT_BY_USER_ID INT  
       )            

      SET @ID = SCOPE_IDENTITY()           
   EXEC sp_xml_removedocument @hDoc              
   END -- End IF (LEN(@EventChanges) > 10)    
      
        
 END TRY        
 BEGIN CATCH        
  EXEC [dbo].[insertErrorInfo]          
  DECLARE @ErrMsg nVarchar(4000),           
    @ErrSeverity Int                
  SELECT @ErrMsg  = ERROR_MESSAGE(),                
         @ErrSeverity = ERROR_SEVERITY()                
  RAISERROR(@ErrMsg, @ErrSeverity, 1)                
 END CATCH          
END