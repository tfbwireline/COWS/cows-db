USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getWFMUserAssignments]    Script Date: 10/07/2020 6:27:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================  
-- Author:  kh946640  
-- Create date: 02/06/2019  
-- Description: Pulls WFM User Assignments based on User ID and User Profile ID.  
-- =========================================================  
ALTER PROCEDURE [web].[getWFMUserAssignments] 
  @USER_ID int 
 ,@USR_PRF_ID Int = 0
AS  
BEGIN  
SET NOCOUNT ON;  
Begin Try 
 
IF @USR_PRF_ID = 0
BEGIN 
	select wfm.WFM_ID as WfmId, wfm.USER_ID as UserId, wfm.GRP_ID as GrpId, wfm.ROLE_ID as RoleId, wfm.ORDR_TYPE_ID as OrdrTypeId, ot.ORDR_TYPE_DES as OrdrTypeDes, wfm.PROD_TYPE_ID as ProdTypeId, pt.PROD_TYPE_DES as ProdTypeDes,
	wfm.PLTFRM_CD as PltfrmCd, pl.PLTFRM_NME as PltfrmNme, wfm.VNDR_CD as VndrCd, wfm.ORGTNG_CTRY_CD as OrgtngCtryCd, c.CTRY_NME as OrgtngCtryNme, wfm.IPL_TRMTG_CTRY_CD as IplTrmtgCtryCd, c.CTRY_NME as IplTrmtgCtryNme, wfm.ORDR_ACTN_ID as OrdrActnId, oac.ORDR_ACTN_DES as OrdrActnDes, 
	wfm.WFM_ASMT_LVL_ID as WfmAsmtLvlId, u.DSPL_NME as DsplNme, wfm.USR_PRF_ID as UsrPrfId from dbo.USER_WFM wfm WITH (NOLOCK) 
	join dbo.LK_USER u WITH (NOLOCK) on  wfm.CREAT_BY_USER_ID = u.USER_ID
	left outer join dbo.LK_ORDR_ACTN oac WITH (NOLOCK) on wfm.ORDR_ACTN_ID = oac.ORDR_ACTN_ID
	left outer join dbo.LK_PROD_TYPE pt WITH (NOLOCK) on wfm.PROD_TYPE_ID = pt.PROD_TYPE_ID
	left outer join dbo.LK_PLTFRM pl WITH (NOLOCK) on wfm.PLTFRM_CD = pl.PLTFRM_CD
	left outer join dbo.LK_CTRY c WITH (NOLOCK) on wfm.ORGTNG_CTRY_CD = c.CTRY_CD
	left outer join dbo.LK_ORDR_TYPE ot WITH (NOLOCK) on wfm.ORDR_TYPE_ID = ot.ORDR_TYPE_ID
	where wfm.USER_ID = @USER_ID and wfm.REC_STUS_ID = 1
	order by oac.ORDR_ACTN_DES, pt.PROD_TYPE_DES, pl.PLTFRM_NME
END

ELSE IF (@USR_PRF_ID > 0)
BEGIN
	select wfm.WFM_ID as WfmId, wfm.USER_ID as UserId, wfm.GRP_ID as GrpId, wfm.ROLE_ID as RoleId, wfm.ORDR_TYPE_ID as OrdrTypeId, ot.ORDR_TYPE_DES as OrdrTypeDes, wfm.PROD_TYPE_ID as ProdTypeId, pt.PROD_TYPE_DES as ProdTypeDes,
	wfm.PLTFRM_CD as PltfrmCd, pl.PLTFRM_NME as PltfrmNme, wfm.VNDR_CD as VndrCd, wfm.ORGTNG_CTRY_CD as OrgtngCtryCd, c.CTRY_NME as OrgtngCtryNme, wfm.IPL_TRMTG_CTRY_CD as IplTrmtgCtryCd, c.CTRY_NME as IplTrmtgCtryNme, wfm.ORDR_ACTN_ID as OrdrActnId, oac.ORDR_ACTN_DES as OrdrActnDes, 
	wfm.WFM_ASMT_LVL_ID as WfmAsmtLvlId, u.DSPL_NME as DsplNme, wfm.USR_PRF_ID as UsrPrfId from dbo.USER_WFM wfm WITH (NOLOCK) 
	join dbo.LK_USER u WITH (NOLOCK) on  wfm.CREAT_BY_USER_ID = u.USER_ID
	left outer join dbo.LK_ORDR_ACTN oac WITH (NOLOCK) on wfm.ORDR_ACTN_ID = oac.ORDR_ACTN_ID
	left outer join dbo.LK_PROD_TYPE pt WITH (NOLOCK) on wfm.PROD_TYPE_ID = pt.PROD_TYPE_ID
	left outer join dbo.LK_PLTFRM pl WITH (NOLOCK) on wfm.PLTFRM_CD = pl.PLTFRM_CD
	left outer join dbo.LK_CTRY c WITH (NOLOCK) on wfm.ORGTNG_CTRY_CD = c.CTRY_CD
	left outer join dbo.LK_ORDR_TYPE ot WITH (NOLOCK) on wfm.ORDR_TYPE_ID = ot.ORDR_TYPE_ID
	where wfm.USER_ID = @USER_ID and wfm.USR_PRF_ID = @USR_PRF_ID and wfm.REC_STUS_ID = 1
	order by oac.ORDR_ACTN_DES, pt.PROD_TYPE_DES, pl.PLTFRM_NME
END
End Try  
  
Begin Catch  
 EXEC [dbo].[insertErrorInfo]  
End Catch  
END