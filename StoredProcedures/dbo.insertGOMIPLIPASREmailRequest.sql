USE [COWS]
GO
_CreateObject 'SP','dbo','insertGOMIPLIPASREmailRequest'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <10/20/2014>
-- Description:	<to send email for GOM IPL+IP ASR completed tasks.>
-- =============================================
ALTER PROCEDURE dbo.insertGOMIPLIPASREmailRequest
	@OrderID INT, 
	@TaskId SMALLINT, 
	@TaskStatus TINYINT, 
	@Comments VARCHAR(1000) = NULL
AS
BEGIN
	BEGIN TRY
		IF EXISTS
			(
				SELECT 'X'
					FROM	dbo.FSA_ORDR	fo	WITH (NOLOCK)
				INNER JOIN	dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
					WHERE	fo.ORDR_ID				=	@OrderID
						AND	fo.PROD_TYPE_CD			IN	('MP','DN')
						AND	ISNULL(oa.CTRY_CD,'')	IN	('GU','MP','PR','VI','US')
			)
			BEGIN
				DECLARE @EMAIL_LIST_TXT VARCHAR(1000)= ''		
				DECLARE @EMAIL_SUBJ_TXT	VARCHAR(1000) = 'GOM IPL+IP ASR Notice'
				DECLARE @EMAIL_BODY_TXT VARCHAR(1000) = ''
				
				SELECT @EMAIL_LIST_TXT = ISNULL(PRMTR_VALU_TXT,'')
					FROM	dbo.LK_SYS_CFG WITH (NOLOCK)
					WHERE	PRMTR_NME = 'GOMIPLIPASREmailList'
					
				SELECT	TOP 1 @EMAIL_BODY_TXT = NTE_TXT
					FROM	dbo.ORDR_NTE WITH (NOLOCK)
					WHERE	ORDR_ID = @OrderID
						AND NTE_TYPE_ID = 22
					ORDER BY NTE_ID DESC

				INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
						(ORDR_ID
						,EMAIL_REQ_TYPE_ID
						,STUS_ID
						,EMAIL_LIST_TXT
						,EMAIL_CC_TXT
						,EMAIL_SUBJ_TXT
						,EMAIL_BODY_TXT)
				SELECT	@OrderID
					,17 
					,10 
					,@EMAIL_LIST_TXT
					,'' AS EMAIL_CC_TXT
					,'FTN ' + FTN + ': ASR detail document for IPL+IP'
					,@EMAIL_BODY_TXT
				FROM	dbo.FSA_ORDR f WITH (NOLOCK)
				
				WHERE	ORDR_ID = @OrderID	
			END
		ELSE
			BEGIN
				DELETE 
					FROM ORDR_NTE WITH (ROWLOCK)
					WHERE NTE_TXT = ''
						AND ORDR_ID = @OrderID
			END
		
			
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END
GO
