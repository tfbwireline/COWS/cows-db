USE [COWS]
GO
_CreateObject 'SP','dbo','getVendorOrdersByOrder'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 1st Sep 2011
-- Description:	Returns all Vendor Orders for a given Order
-- ================================================================

ALTER PROCEDURE [dbo].[getVendorOrdersByOrder]
	@OrderID INT
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
  SELECT VO.[VNDR_ORDR_ID],
		VO.[VNDR_FOLDR_ID],
		LV.VNDR_NME,
		LC.CTRY_NME,
		VO.[ORDR_ID],
		FO.FTN,
		HF.H5_FOLDR_ID,
		HF.CUST_ID,
		VO.[CREAT_DT],
		LU.FULL_NME     
  FROM [dbo].[VNDR_ORDR] VO WITH (NOLOCK)
  INNER JOIN [dbo].[VNDR_FOLDR] VF WITH (NOLOCK) ON VO.VNDR_FOLDR_ID = VF.VNDR_FOLDR_ID
  INNER JOIN [dbo].[LK_VNDR] LV WITH (NOLOCK) ON VF.VNDR_CD = LV.VNDR_CD 
  LEFT OUTER JOIN [dbo].[LK_CTRY] LC WITH (NOLOCK) ON VF.CTRY_CD = LC.CTRY_CD
  INNER JOIN [dbo].[LK_USER] LU WITH (NOLOCK) ON VO.CREAT_BY_USER_ID = LU.[USER_ID]
  LEFT OUTER JOIN [dbo].[ORDR] O WITH (NOLOCK) ON VO.VNDR_ORDR_ID = O.ORDR_ID  
  LEFT OUTER JOIN [dbo].[H5_FOLDR] HF WITH (NOLOCK) ON O.H5_FOLDR_ID = HF.H5_FOLDR_ID
  LEFT OUTER JOIN [dbo].[FSA_ORDR] FO WITH (NOLOCK) ON VO.ORDR_ID = FO.ORDR_ID
  WHERE VO.ORDR_ID = @OrderID  
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
