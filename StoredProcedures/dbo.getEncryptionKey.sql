USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getSSTATKey]    Script Date: 05/09/2018 07:34:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 5/09/2018
	-- Description:	Gets encryption key for Application MQ message encryption.
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getEncryptionKey]
				@APPL_ID varchar(5)

	AS
	BEGIN

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;	
	
	SELECT	top 1 CONVERT(VARCHAR, DecryptByKey(ENCRYPTION_KEY)) [KEY]	 
		FROM dbo.LK_KEYS WITH (NOLOCK)
		WHERE KEY_APPL_ID = @APPL_ID
	

	END

