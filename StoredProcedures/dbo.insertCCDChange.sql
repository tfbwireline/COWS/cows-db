USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertCCDChange]    Script Date: 11/15/2021 10:15:42 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ============================================================================
-- Cereated By:	Kyle Wichert
-- Create date:	08/24/2011 
-- Description:	Perform CCD Change.  Add note.  Insert old values into history table, 
--				clear new values, load new tasks.
-- ============================================================================
--				Remove MS dates.  Add task directly instead of calling insert
--				initial task.  
-- ============================================================================


ALTER PROCEDURE [dbo].[insertCCDChange]
	--@parentOrderID	INT,
	@OrdrIDs		VARCHAR(1000),
	@newCCD			DATETIME,
	@reasonCSV		VARCHAR(MAX),
	@notes			VARCHAR(500),
	@UserID			INT,
	@Out			INT OUTPUT
AS
BEGIN
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE	@Orders TABLE 
		(
			orderID			INT,
			prnt_ordr_id	INT,
			H5_H6_ID		INT,
			CCDOld			DATETIME,
			noteID			INT,
			PPRT_ID			INT
		)
		
		DECLARE @Reasons TABLE
		(
			reasonID	SMALLINT
		)
		
		DECLARE @ORDRID TABLE
		(
			ORDR_ID	Varchar(50)
		)
		
		INSERT INTO @Reasons SELECT IntegerID FROM [web].[ParseCommaSeparatedIntegers](@reasonCSV, ',')
		INSERT INTO @ORDRID SELECT StringID FROM [web].[ParseCommaSeparatedStrings](@OrdrIDs)
		
		IF EXISTS (SELECT 'x' FROM @ORDRID WHERE (isnumeric(ORDR_ID)=0))
		BEGIN
			--Insert FSA orders
			INSERT INTO @Orders
			SELECT		DISTINCT
						o.ORDR_ID,
						ISNULL(o.PRNT_ORDR_ID,0),
						ISNULL(o.H5_H6_CUST_ID,foc.CUST_ID),
						ISNULL(o.CUST_CMMT_DT,fo.CUST_CMMT_DT),
						0, --noteid
						ISNULL(o.PPRT_ID,0) AS PPRT_ID		
			FROM			dbo.ORDR		o	WITH (NOLOCK)	
				INNER JOIN	dbo.FSA_ORDR	fo	WITH (NOLOCK) ON o.ORDR_ID = fo.ORDR_ID
				LEFT JOIN	dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON o.ORDR_ID = foc.ORDR_ID
																AND foc.CIS_LVL_TYPE IN ('H5','H6')
			--WHERE		o.PRNT_ORDR_ID	=	@parentOrderID
				WHERE	fo.FTN IN (SELECT ORDR_ID FROM @ORDRID)
				AND		o.ORDR_STUS_ID	<>	4
		END
		ELSE
		BEGIN
			--Insert FSA orders
			INSERT INTO @Orders
			SELECT		DISTINCT
						o.ORDR_ID,
						ISNULL(o.PRNT_ORDR_ID,0),
						ISNULL(o.H5_H6_CUST_ID,foc.CUST_ID),
						ISNULL(o.CUST_CMMT_DT,fo.CUST_CMMT_DT),
						0, --noteid
						ISNULL(o.PPRT_ID,0) AS PPRT_ID		
			FROM			dbo.ORDR		o	WITH (NOLOCK)	
				INNER JOIN	dbo.FSA_ORDR	fo	WITH (NOLOCK) ON o.ORDR_ID = fo.ORDR_ID
				LEFT JOIN	dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON o.ORDR_ID = foc.ORDR_ID
																AND foc.CIS_LVL_TYPE IN ('H5','H6')
			--WHERE		o.PRNT_ORDR_ID	=	@parentOrderID
				WHERE	fo.ORDR_ID IN (SELECT ORDR_ID FROM @ORDRID)
				AND		o.ORDR_STUS_ID	<>	4
		END
		--UNION
		
		----Insert related FSA orders
		--SELECT		DISTINCT
		--			rop.ORDR_ID,
		--			ISNULL(rop.CUST_CMMT_DT,pf.CUST_CMMT_DT),
		--			0, --noteid
		--			ISNULL(rop.PPRT_ID,0) AS PPRT_ID		
		--FROM			dbo.ORDR		o	WITH (NOLOCK)	
		--	INNER JOIN	dbo.FSA_ORDR	fo	WITH (NOLOCK) ON o.ORDR_ID		=	fo.ORDR_ID
		--	INNER JOIN	dbo.FSA_ORDR	rfo	WITH (NOLOCK) ON fo.RELTD_FTN	=	rfo.FTN
		--	INNER JOIN	dbo.ORDR		ro	WITH (NOLOCK) ON rfo.ORDR_ID	=	ro.ORDR_ID
		--	INNER JOIN	dbo.ORDR		rop	WITH (NOLOCK) ON ro.PRNT_ORDR_ID=	rop.PRNT_ORDR_ID
		--	INNER JOIN	dbo.FSA_ORDR	pf	WITH (NOLOCK) ON rop.ORDR_ID	=	pf.ORDR_ID
		--WHERE		o.PRNT_ORDR_ID	=	@parentOrderID
		--	AND		o.ORDR_STUS_ID	<>	4


		--Insert IPL && NCCO orders
		INSERT INTO @Orders
		SELECT	o.ORDR_ID,
				ISNULL(o.PRNT_ORDR_ID,0),
				ISNULL(o.H5_H6_CUST_ID,h5.CUST_ID),
				ISNULL(o.CUST_CMMT_DT,ipo.CUST_CMMT_DT),
				0, --noteid
				ISNULL(o.PPRT_ID,0) AS PPRT_ID				
		FROM			dbo.ORDR		o	WITH (NOLOCK)	
			INNER JOIN	dbo.IPL_ORDR	ipo	 WITH (NOLOCK) ON o.ORDR_ID = ipo.ORDR_ID
			LEFT JOIN	dbo.H5_FOLDR	h5 WITH (NOLOCK) ON h5.H5_FOLDR_ID = o.H5_FOLDR_ID
		WHERE		CONVERT(VARCHAR,o.ORDR_ID)	IN (SELECT ORDR_ID FROM @ORDRID)

		
		
		INSERT INTO @Orders
		SELECT o.ORDR_ID,
					ISNULL(o.PRNT_ORDR_ID,0),
					ISNULL(o.H5_H6_CUST_ID,ncco.H5_ACCT_NBR),
					ISNULL(o.CUST_CMMT_DT,ncco.CCS_DT),
					0, --noteid
					ISNULL(o.PPRT_ID,0) AS PPRT_ID				
		FROM			dbo.ORDR		o	WITH (NOLOCK)	
			INNER JOIN	dbo.NCCO_ORDR	ncco WITH (NOLOCK) ON ncco.ORDR_ID = o.ORDR_ID
		WHERE		CONVERT(VARCHAR,o.ORDR_ID)	IN (SELECT ORDR_ID FROM @ORDRID)

		
		
		--Dissociate parent order ID if they choose to update ccd on partial FTNs and update new parent order id to unchecked FTNs  
		declare @prnt_ordr_id_unchecked int = 0
		declare @prnt_ordr_id_checked int = 0
		SELECT @prnt_ordr_id_unchecked = MIN(odr.ORDR_ID)
			From	dbo.ORDR odr 
		Inner Join	@Orders  o ON o.prnt_ordr_id = odr.PRNT_ORDR_ID
			Where	NOT EXISTS
					(
						SELECT 'X'
							FROM	@Orders t
							WHERE	t.prnt_ordr_id	=	odr.PRNT_ORDR_ID
								AND	t.orderID		=	odr.ORDR_ID
					)
		
		SELECT @prnt_ordr_id_checked = MIN(odr.ORDR_ID)
			From	dbo.ORDR odr 
		Inner Join	@Orders  o ON o.prnt_ordr_id = odr.PRNT_ORDR_ID
			Where	EXISTS
					(
						SELECT 'X'
							FROM	@Orders t
							WHERE	t.prnt_ordr_id	=	odr.PRNT_ORDR_ID
								AND	t.orderID		=	odr.ORDR_ID
					)
					
		if(@prnt_ordr_id_unchecked != 0 AND @prnt_ordr_id_checked != 0 AND @prnt_ordr_id_checked != @prnt_ordr_id_unchecked)
			begin
				Update odr
				SET odr.PRNT_ORDR_ID = @prnt_ordr_id_unchecked 					
				From	dbo.ORDR odr WITH (ROWLOCK)
			Inner Join	@Orders  o ON o.prnt_ordr_id = odr.PRNT_ORDR_ID
				Where	NOT EXISTS
						(
							SELECT 'X'
								FROM	@Orders t
								WHERE	t.prnt_ordr_id	=	odr.PRNT_ORDR_ID
									AND	t.orderID		=	odr.ORDR_ID
						)
				
				Update odr
				SET odr.PRNT_ORDR_ID = @prnt_ordr_id_checked 					
				From	dbo.ORDR odr WITH (ROWLOCK)
			Inner Join	@Orders  o ON o.prnt_ordr_id = odr.PRNT_ORDR_ID
				Where	EXISTS
						(
							SELECT 'X'
								FROM	@Orders t
								WHERE	t.prnt_ordr_id	=	odr.PRNT_ORDR_ID
									AND	t.orderID		=	odr.ORDR_ID
						)
						
				INSERT INTO dbo.ORDR_NTE	
						(NTE_TYPE_ID,	
						ORDR_ID,	
						CREAT_DT,	
						CREAT_BY_USER_ID,	
						REC_STUS_ID,	
						NTE_TXT) 
					SELECT		15,
								o.ORDR_ID,	
								GETDATE(),	
								@UserID,			
								1,				
								'Removing association for this FTN as per user request to update ccd on partial FTNs'
					FROM	ORDR o WITH (NOLOCK)
					WHERE	o.prnt_ordr_id IN (@prnt_ordr_id_unchecked,@prnt_ordr_id_checked)
			end	
		
		
		--Re-associate parent order ID to other order ID's within same category based on H5/H6 and new CCD 
		DECLARE @orig_prnt_ordr_id INT = 0
		Select @orig_prnt_ordr_id = odr.PRNT_ORDR_ID
			From	dbo.ORDR odr WITH (NOLOCK)
		Inner Join	dbo.FSA_ORDR fsa WITH (NOLOCK) On odr.ORDR_ID = fsa.ORDR_ID
		Inner Join	dbo.FSA_ORDR_CUST foc WITH (NOLOCK) On foc.ORDR_ID = fsa.ORDR_ID
														And foc.CIS_LVL_TYPE in ('H5','H6')
			Where	odr.ORDR_STUS_ID IN (0,1)
				AND Exists
					(
						SELECT 'X'
							FROM	@Orders	t
							WHERE	t.H5_H6_ID	=	ISNULL(odr.H5_H6_CUST_ID,foc.CUST_ID)
								AND	@newCCD		=	ISNULL(odr.CUST_CMMT_DT,fsa.CUST_CMMT_DT)
					)														 
		
		
		IF @orig_prnt_ordr_id <> 0
			BEGIN
				DECLARE @prnt_ordr_id INT = 0
				Select @prnt_ordr_id = prnt_ordr_id
					From @Orders 
				
				if(@prnt_ordr_id <> 0)
					begin 
						if(@prnt_ordr_id < @orig_prnt_ordr_id)
							begin
								update dbo.ORDR 
									set PRNT_ORDR_ID = @prnt_ordr_id
									where PRNT_ORDR_ID = @orig_prnt_ordr_id
							end
						else
							begin
								update dbo.ORDR 
									set PRNT_ORDR_ID = @orig_prnt_ordr_id
									where PRNT_ORDR_ID = @prnt_ordr_id
							end
							
						INSERT INTO dbo.ORDR_NTE	
						(NTE_TYPE_ID,	
						ORDR_ID,	
						CREAT_DT,	
						CREAT_BY_USER_ID,	
						REC_STUS_ID,	
						NTE_TXT) 
						SELECT		15,
									o.ORDR_ID,	
									GETDATE(),	
									@UserID,			
									1,				
									'Re-associating FTNs as per user request to update ccd which matches with H5/H6 and new CCD requested for this partial FTN.'
						FROM	ORDR o WITH (NOLOCK)
						WHERE	o.prnt_ordr_id IN (@prnt_ordr_id,@orig_prnt_ordr_id)
					end	
			END
		
		
		
		--Create Note
		DECLARE @OID	INT
		DECLARE @noteID	INT

		WHILE EXISTS (SELECT 'X' FROM @Orders WHERE noteID = 0)
		BEGIN
			SET @noteID = -1
			SELECT TOP 1 @OID = orderID FROM @Orders WHERE noteID = 0
			
			INSERT INTO dbo.ORDR_NTE	
						(NTE_TYPE_ID,	
						ORDR_ID,	
						CREAT_DT,	
						CREAT_BY_USER_ID,	
						REC_STUS_ID,	
						NTE_TXT) 
			SELECT		15,
						o.orderID,	
						GETDATE(),	
						@UserID,			
						1,				
						'CCD Changed from ' + ISNULL(CONVERT(VARCHAR(25),o.CCDOld),'previous value') + ' to ' + CONVERT(VARCHAR(25),@newCCD) + '.  ' + @notes
			FROM	@Orders o
			WHERE	o.orderID = @OID
			
			--update @Orders table with new noteID
			SELECT @noteID = SCOPE_IDENTITY()
			UPDATE @Orders SET noteID = @noteID WHERE orderID = @OID
		END
		
		--Insert all Orders into history table
		INSERT INTO dbo.CCD_HIST
		SELECT	DISTINCT
				o.orderID,
				ISNULL(o.CCDOld,0),
				@newCCD,
				o.noteID,
				NULL,--o.VndrSent,
				NULL,--o.VndrAck,
				@UserID,
				GETDATE()
		FROM @Orders o
		
		--Set CCD Flag in ORDR table
		UPDATE ordr
		SET CCD_UPDT_CD = 1
		FROM			@Orders		o
			INNER JOIN	dbo.ORDR	ordr WITH (ROWLOCK) ON ordr.ORDR_ID = o.orderID
			
		--Set new CCD Date all
		UPDATE	ord
		SET		CUST_CMMT_DT	=	@newCCD
		FROM			@Orders o
			INNER JOIN 	dbo.ORDR	ord	WITH (ROWLOCK) ON o.orderID = ord.ORDR_ID
			
		--Load reasonIDs
		INSERT INTO dbo.CCD_HIST_REAS
		SELECT		DISTINCT
					ch.CCD_HIST_ID,
					r.reasonID,
					GETDATE()
		FROM			@Orders			o
			INNER JOIN	dbo.CCD_HIST	ch	WITH (NOLOCK)	ON	o.orderID = ch.ORDR_ID
															AND o.noteID  = ch.NTE_ID
			JOIN		@Reasons	r						ON	1=1
			
		--Load new tasks once they are created.
		UPDATE	at
		SET		STUS_ID = 0, MODFD_DT = GETDATE()
		FROM			dbo.ACT_TASK	at
			INNER JOIN	@Orders			o	ON at.ORDR_ID	=	o.orderID
			INNER JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK)	ON o.PPRT_ID = lp.PPRT_ID
			INNER JOIN	dbo.ORDR		odr WITH (NOLOCK)	ON odr.ORDR_ID = o.orderID
		WHERE	at.TASK_ID	IN	(106, 211) --CCD Ready
			AND	at.STUS_ID	!=	0
			AND ((lp.INTL_CD	=	1 AND	lp.TRPT_CD	=	1) OR (odr.ORDR_CAT_ID IN (1,4)))
			AND odr.ORDR_CAT_ID != 6
		
		
		UPDATE	at
		SET		STUS_ID = 0, MODFD_DT = GETDATE()
		FROM			dbo.ACT_TASK	at
			INNER JOIN	@Orders			o	ON at.ORDR_ID	=	o.orderID
			INNER JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK)	ON o.PPRT_ID = lp.PPRT_ID
			INNER JOIN	dbo.ORDR		odr WITH (NOLOCK)	ON odr.ORDR_ID = o.orderID
		WHERE	at.TASK_ID	IN	(211) --CCD Ready
			AND	at.STUS_ID	!=	0
			AND lp.INTL_CD	=	1 
			AND	lp.TRPT_CD	=	1
			AND odr.ORDR_CAT_ID = 6
			AND odr.ORDR_STUS_ID not in (2,4,5)	-- dlp0278 6/21/21 stopped creating pending task for completed orders.  Lajan/Aladin asked for this
					
			
		INSERT INTO dbo.ACT_TASK (ORDR_ID, TASK_ID, STUS_ID, WG_PROF_ID, CREAT_DT)
		SELECT o.orderID, 211, 0, 202, GETDATE()
		FROM	@Orders o
		INNER JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK)	ON o.PPRT_ID = lp.PPRT_ID
		WHERE (
					(lp.INTL_CD	=	1 AND	lp.TRPT_CD	=	1) 
					OR 
					EXISTS
						(SELECT 'X'
							FROM dbo.ORDR odr WITH (NOLOCK)
							WHERE odr.ORDR_ID = o.orderID
								AND odr.ORDR_CAT_ID IN (1,4))
				)
			AND	NOT EXISTS (SELECT TOP 1 'X' FROM dbo.ACT_TASK at WITH (NOLOCK) WHERE at.ORDR_ID = o.orderID AND at.TASK_ID = 211)
			AND NOT EXISTS (SELECT TOP 1 'X' FROM dbo.ORDR WITH (NOLOCK) where ORDR_ID = o.orderID  and ORDR_STUS_ID in (2,4,5)) -- dlp0278 6/21/21 stopped creating pending task for completed orders.  Lajan/Aladin asked for this
			
		INSERT INTO dbo.ACT_TASK (ORDR_ID, TASK_ID, STUS_ID, WG_PROF_ID, CREAT_DT)
		SELECT o.orderID, 106, 0, 202, GETDATE()
		FROM	@Orders o
		INNER JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK)	ON o.PPRT_ID = lp.PPRT_ID
		WHERE	(
					(lp.INTL_CD	=	1
						AND	lp.TRPT_CD	=	1
						AND lp.ORDR_CAT_ID != 6)
					OR EXISTS
					(
						SELECT 'X'
							From dbo.ORDR odr WITH (NOLOCK)
							WHERE odr.ORDR_ID = o.orderID
								AND odr.ORDR_CAT_ID IN (1,4)
					)
				)
			AND	NOT EXISTS (SELECT TOP 1 'X' FROM dbo.ACT_TASK at WITH (NOLOCK) WHERE at.ORDR_ID = o.orderID AND at.TASK_ID = 106)
			AND NOT EXISTS (SELECT TOP 1 'X' FROM dbo.ORDR WITH (NOLOCK) where ORDR_ID = o.orderID  and ORDR_STUS_ID in (2,4,5)) -- dlp0278 6/21/21 stopped creating pending task for completed orders.  Lajan/Aladin asked for this
		
		
		 -- Notify SSTAT of the CCD change
		 
		INSERT INTO dbo.SSTAT_REQ (ORDR_ID, SSTAT_MSG_ID,STUS_MOD_DT, CREAT_DT,STUS_ID,FTN)
		SELECT o.orderID, 5, null, GETDATE(),10,f.FTN
		FROM	@Orders o
		INNER JOIN	dbo.FSA_ORDR f	WITH (NOLOCK)	ON o.orderID = f.ORDR_ID
		WHERE 	EXISTS
					(SELECT 'X'
							FROM dbo.ACT_TASK at WITH (NOLOCK)
							WHERE at.ORDR_ID = o.orderID
								AND (at.TASK_ID = 1000 and at.STUS_ID = 0))
				
			AND	EXISTS (SELECT 'X' FROM dbo.SSTAT_REQ sr WITH (NOLOCK) 
							WHERE sr.ORDR_ID = o.orderID AND sr.SSTAT_MSG_ID = 1)
		
		
			
		SET @Out = 1
	COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
	ROLLBACK TRANSACTION
		SET @Out = 0
		EXEC dbo.insertErrorInfo
	END CATCH
	
END

