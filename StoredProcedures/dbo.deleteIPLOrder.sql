USE [COWS]
GO
_CreateObject 'SP','dbo','deleteIPLOrder'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <07/24/2012>
-- Description:	<to delete IPL Order (drafts only)>
-- =============================================
ALTER PROCEDURE dbo.deleteIPLOrder
	@orderID	INT,
	@draft		BIT = 0
AS
BEGIN
	BEGIN TRY
		IF @draft = 1
			BEGIN
				delete From dbo.ORDR_EXP with (rowlock) where ORDR_ID = @orderID
				delete From dbo.ORDR_CNTCT with (rowlock) where ORDR_ID = @orderID
				delete from dbo.ORDR_ADR with (rowlock) where ORDR_ID = @orderID
				delete from IPL_ORDR_ACCS_INFO with (rowlock) where ORDR_ID = @orderID
				delete from IPL_ORDR_CKT with (rowlock) where ORDR_ID = @orderID
				delete from IPL_ORDR_EQPT with (rowlock) where ORDR_ID = @orderID
				delete From dbo.IPL_ORDR with (rowlock) where ORDR_ID = @orderID
				delete from dbo.ORDR with (rowlock) where ORDR_ID = @orderID
			END
	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
END
GO
