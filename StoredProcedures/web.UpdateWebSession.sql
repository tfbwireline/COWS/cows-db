USE [COWS]
GO
_CreateObject 'SP','web','UpdateWebSession'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
 --=====================================================================================================    
 -- Author: Jagan Gangi
 -- Create Date: 11/6/2012
 -- Description : To Update web session details to maintain a log
 --=====================================================================================================    
    
 ALTER PROCEDURE [web].[UpdateWebSession]     
  @UADID VARCHAR(20),    
  @IPAdr VARCHAR(39),    
  @SID  VARCHAR(100),
  @IsOldUI BIT = NULL
 AS    
 BEGIN    
  SET NOCOUNT ON    
  BEGIN TRY    
   
   IF ((@UADID != '') AND (@SID != ''))  
   BEGIN
		IF NOT EXISTS (SELECT 'X'
					   FROM dbo.WEB_SESSN_LOG WITH (NOLOCK)
					   WHERE (USER_AD_ID = @UADID) 
						AND	 (SESSN_ID = @SID)
						AND  (REC_STUS_ID=1))
		BEGIN
		   UPDATE dbo.WEB_SESSN_LOG WITH (ROWLOCK) SET REC_STUS_ID=0 WHERE (USER_AD_ID = @UADID) AND  (REC_STUS_ID=1)
		   
		   INSERT INTO dbo.WEB_SESSN_LOG (USER_AD_ID, SESSN_ID, IP_ADR, IsOldUI) VALUES (@UADID, @SID, @IPAdr, @IsOldUI)
		   UPDATE dbo.LK_USER WITH (ROWLOCK) SET LST_LOGGDIN_DT=GETDATE() WHERE USER_ADID=@UADID
		END
   END
     
  END TRY    
  BEGIN CATCH    
   EXEC [dbo].[insertErrorInfo]      
   DECLARE @ErrMsg nVarchar(4000),       
     @ErrSeverity Int            
   SELECT @ErrMsg  = ERROR_MESSAGE(),            
    @ErrSeverity = ERROR_SEVERITY()            
   RAISERROR(@ErrMsg, @ErrSeverity, 1)     
  END CATCH    
    
 END