USE [COWS]
GO
_CreateObject 'SP','dbo','updateEmailRequest'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sbg9814
-- Create date: 08/16/2011
-- Description:	Update the status of an Email Request.
-- =============================================
ALTER PROCEDURE [dbo].[updateEmailRequest] 
	@EMAIL_REQ_ID INT
	,@ORDR_ID	Int
	,@STUS_ID	Int
	,@EVENT_ID  Int
	,@REDESIGN_ID Int = 0
AS
BEGIN
Begin Try

IF @ORDR_ID > 0
	BEGIN
		UPDATE		dbo.EMAIL_REQ	WITH (ROWLOCK)
			SET		STUS_ID	=	@STUS_ID,
					SENT_DT =   CASE @STUS_ID WHEN 11 THEN GETDATE() ELSE NULL END
			WHERE	ORDR_ID	=	@ORDR_ID
			  AND   EMAIL_REQ_ID = @EMAIL_REQ_ID
	END
Else
	
	IF @EVENT_ID > 0
		BEGIN

			UPDATE		dbo.EMAIL_REQ	WITH (ROWLOCK)
				SET		STUS_ID	=	@STUS_ID,
						SENT_DT =   CASE @STUS_ID WHEN 11 THEN GETDATE() ELSE NULL END
				WHERE	EVENT_ID	=	@EVENT_ID
				  AND   EMAIL_REQ_ID = @EMAIL_REQ_ID
		END
Else
	IF @REDESIGN_ID > 0
		BEGIN
			UPDATE		dbo.EMAIL_REQ	WITH (ROWLOCK)
				SET		STUS_ID	=	@STUS_ID,
						SENT_DT =   CASE @STUS_ID WHEN 11 THEN GETDATE() ELSE NULL END
				WHERE	EMAIL_REQ_ID = @EMAIL_REQ_ID	
		END
Else
	BEGIN
		UPDATE		dbo.EMAIL_REQ	WITH (ROWLOCK)
				SET		STUS_ID	=	@STUS_ID,
						SENT_DT =   CASE @STUS_ID WHEN 11 THEN GETDATE() ELSE NULL END
				WHERE	EMAIL_REQ_ID = @EMAIL_REQ_ID
	END

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
GO
