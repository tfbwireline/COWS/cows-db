USE [COWS]
GO
/****** Object:  StoredProcedure [web].[chkForAppointment]    Script Date: 04/12/2021 9:23:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 11/08/2011
-- Description:	Get an Appointment
-- =========================================================
-- jrg7298, Added logic to ignore the current EventID slot
-- kh946640, Updated to pull Non Recurring Appointments greater than Appt Types 16
-- km967761, Added ISNULL(nme.SOFT_ASSIGN_CD, 0) <> 1 condition for soft assigned events

-- exec [web].[chkForAppointment] 6243, '2021-04-16 11:00:00', '2021-04-16 12:30:00', 0
ALTER PROCEDURE [web].[chkForAppointment]
	@User_ID	varchar(8),
	@StartTime	DateTime,
	@EndTime	DateTime,
	@EventID	INT = 0
AS
BEGIN
SET NOCOUNT ON;
Begin Try

		IF (@User_ID <> '5001')
		BEGIN
			DECLARE @Appt TABLE --#Appt
			(
				ApptID			INT IDENTITY(1,1),
				StartTime		SMALLDATETIME,
				EndTime			SMALLDATETIME,
				ApptType		INT
			)

		--Get all Shift appointments that are non-recurring
			INSERT INTO @Appt
			SELECT a.STRT_TMST, a.END_TMST, APPT_TYPE_ID
			FROM	dbo.APPT	a	WITH (NOLOCK)
			-- km967761 - ADDED CONDITION "ISNULL(nme.SOFT_ASSIGN_CD, 0) <> 1" for SOFT ASSIGN EXCEMPTIONS
			LEFT OUTER JOIN dbo.MDS_EVENT nme  WITH (NOLOCK) on nme.EVENT_ID = a.EVENT_ID AND ISNULL(nme.SOFT_ASSIGN_CD, 0) <> 1
			LEFT OUTER JOIN dbo.UCaaS_EVENT uc  WITH (NOLOCK) on uc.EVENT_ID = a.EVENT_ID
			WHERE	((a.EVENT_ID <> @EventID) AND
					(((((@StartTime between a.STRT_TMST AND a.END_TMST) AND (@StartTime <> a.END_TMST))
					 OR ((@EndTime between a.STRT_TMST AND a.END_TMST) AND (@EndTime <> a.STRT_TMST)))
					 AND ASN_TO_USER_ID_LIST_TXT LIKE '%="' + @User_ID +'"%'
					 AND nme.EVENT_STUS_ID IN (5,2,4,9,10)-- status 'inprogress - 5', 'pending - 2','published - 4','fulfilling-9','shipped-10'
					 AND (nme.EVENT_ID <> @EventID)
					 AND RCURNC_CD	= 0)
							OR
					((((@StartTime between a.STRT_TMST AND a.END_TMST) AND (@StartTime <> a.END_TMST))
					 OR ((@EndTime between a.STRT_TMST AND a.END_TMST) AND (@EndTime <> a.STRT_TMST)))
					 AND ASN_TO_USER_ID_LIST_TXT LIKE '%="' + @User_ID +'"%'
					 AND uc.EVENT_STUS_ID IN (5,2,4)-- status 'inprogress - 5', 'pending - 2','published - 4' 
					 AND (uc.EVENT_ID <> @EventID)
					 AND RCURNC_CD	= 0)
							OR
					((((@StartTime between a.STRT_TMST AND a.END_TMST) AND (@StartTime <> a.END_TMST))
					 OR ((@EndTime between a.STRT_TMST AND a.END_TMST) AND (@EndTime <> a.STRT_TMST)))
					 AND ASN_TO_USER_ID_LIST_TXT LIKE '%="' + @User_ID +'"%'
					 AND RCURNC_CD	= 0
					 AND ISNULL(a.EVENT_ID,1) = 1))) -- Personal appts has NULL for Event_ID's.
				 
			--Get all recurring appointments into temp table
			INSERT INTO @Appt
			SELECT STRT_TMST, END_TMST, APPT_TYPE_ID
			FROM	dbo.APPT_RCURNC_DATA		WITH (NOLOCK)
			WHERE	APPT_TYPE_ID >	16
				AND APPT_TYPE_ID NOT IN (31, 33)
				AND ASN_USER_ID = @User_ID

				UNION

			SELECT STRT_TMST, END_TMST, APPT_TYPE_ID
			FROM	dbo.APPT		WITH (NOLOCK)
			WHERE	APPT_TYPE_ID >	16
				AND APPT_TYPE_ID NOT IN (31, 33)
				AND RCURNC_CD	= 0
				AND ASN_TO_USER_ID_LIST_TXT LIKE '%="' + @User_ID +'"%'

			--Return any appointments found for the User during timeslot.

			IF EXISTS(SELECT ApptID from @Appt a 
				WHERE (((@StartTime between a.StartTime AND a.EndTime) AND (@StartTime <> a.EndTime))
						 OR ((@EndTime between a.StartTime AND a.EndTime) AND (@EndTime <> a.StartTime))))
			BEGIN
				SELECT DSPL_NME
				FROM dbo.LK_USER WITH (NOLOCK)
				WHERE [USER_ID]=@User_ID
			END
			ELSE
				SELECT ''
	END
	ELSE
	BEGIN
		SELECT ''
	END			 	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END