USE [COWS]
GO
_CreateObject 'SP','dbo','CmplCPEOrdrFrmEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	Jagannath Gangi
-- Create date: 5/1/2017
-- Description:	This SP completes the devices/order and sends ODIE and M5 messages.
-- =============================================
ALTER PROCEDURE [dbo].[CmplCPEOrdrFrmEvent] 
	@ORDR_ID        INT = 0,
	@DEVICE_ID         VARCHAR(25) = NULL,
	@NOTE           VARCHAR (1000) = NULL,
	@USERID			INT = 1
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY

	DECLARE @FSA_CPE_LINE_ITEM_ID INT	
			,@FTN VARCHAR(50)
			,@ORDR_TYPE_CD VARCHAR(2)
	DECLARE @IDS table (FSA_CPE_LINE_ITEM_ID INT)
	
	IF ((@DEVICE_ID IS NULL) AND (@ORDR_ID > 0))
	BEGIN
		SELECT TOP 1 @DEVICE_ID = li.DEVICE_ID
				FROM dbo.ACT_TASK a WITH (NOLOCK)
				INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM li WITH (NOLOCK)
							ON li.ORDR_ID = a.ORDR_ID
				WHERE ((a.TASK_ID = 1000 AND a.STUS_ID IN (0,3))
						OR (a.TASK_ID = 604 AND a.STUS_ID IN (0,3)))
					AND li.ORDR_ID = @ORDR_ID 
					AND li.ITM_STUS = 401
	END

	IF ((@ORDR_ID = 0) AND (@DEVICE_ID IS NOT NULL))
		BEGIN
			SELECT DISTINCT @ORDR_ID = a.ORDR_ID 
				FROM dbo.ACT_TASK a WITH (NOLOCK)
				INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM li WITH (NOLOCK)
							ON li.ORDR_ID = a.ORDR_ID
				WHERE ((a.TASK_ID = 1000 AND a.STUS_ID IN (0,3))
						OR (a.TASK_ID = 604 AND a.STUS_ID IN (0,3)))
					AND li.DEVICE_ID = @DEVICE_ID 
		END

	--If the order is Goodman or Ericsson, DO NOT COMPLETE
	IF EXISTS (SELECT 'X'
			   FROM dbo.FSA_ORDR WITH (NOLOCK)
			   WHERE UPPER(CPE_VNDR) IN ('GOODMAN','ERICSSON')
			     AND ORDR_ID=@ORDR_ID)
	RETURN;
	
	IF ((ISNULL(@ORDR_ID,0) <> 0)
	     AND (EXISTS (SELECT 'X' FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) WHERE ORDR_ID=@ORDR_ID AND ITM_STUS = 401)))	
		BEGIN				
					SELECT @ORDR_TYPE_CD = ORDR_TYPE_CD, @FTN = FTN FROM dbo.FSA_ORDR WITH (NOLOCK) 
										WHERE ORDR_ID = @ORDR_ID
					IF @ORDR_TYPE_CD = 'DC'
						BEGIN
							UPDATE dbo.ORDR 
								SET ORDR_STUS_ID = 5,
									MODFD_DT = GETDATE(),
									MODFD_BY_USER_ID = @USERID
							WHERE ORDR_ID = @ORDR_ID
						END
					ELSE
						BEGIN
							UPDATE dbo.ORDR 
								SET ORDR_STUS_ID = 2,
									MODFD_DT = GETDATE(),
									MODFD_BY_USER_ID = @USERID
							WHERE ORDR_ID = @ORDR_ID
						END
					
					EXEC dbo.CompleteActiveTask @ORDR_ID,604,2,@NOTE,1,6
					EXEC dbo.CompleteActiveTask @ORDR_ID,1000,2,@NOTE,1,6
					
					INSERT INTO @IDS (FSA_CPE_LINE_ITEM_ID) 
						 (SELECT FSA_CPE_LINE_ITEM_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
											WHERE ORDR_ID = @ORDR_ID AND CMPL_DT IS NULL
											AND DEVICE_ID = @DEVICE_ID AND ITM_STUS = 401) 
					--SELECT * FROM @IDS 
					WHILE EXISTS (SELECT * FROM @IDS)
						BEGIN
						  SELECT TOP 1 @FSA_CPE_LINE_ITEM_ID = FSA_CPE_LINE_ITEM_ID
									FROM @IDS
						  
						  UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM 
								SET CMPL_DT =  GETDATE()
								WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
										AND DEVICE_ID = @DEVICE_ID		

						  DELETE @IDS WHERE FSA_CPE_LINE_ITEM_ID = @FSA_CPE_LINE_ITEM_ID
										
						END
						
					IF @DEVICE_ID NOT IN (SELECT DEVICE FROM dbo.M5_ORDR_MSG WITH (NOLOCK)
												 WHERE ORDR_ID = @ORDR_ID AND M5_MSG_ID = 1)
							BEGIN
							
								INSERT INTO [dbo].[M5_ORDR_MSG]
									([ORDR_ID],[M5_MSG_ID],[NTE],[MSG], [DEVICE],[CMPNT_ID],[STUS_ID],[CREAT_DT],[SENT_DT])     
								VALUES (@ORDR_ID,1,@NOTE,null,@DEVICE_ID,null,10,GETDATE(),NULL)	
								
							END

					IF EXISTS (SELECT * FROM ODIE_REQ WITH (NOLOCK)
								WHERE @ORDR_ID = ORDR_ID AND ODIE_MSG_ID = 5)							
							BEGIN
								INSERT INTO [COWS].[dbo].[ODIE_REQ]
								   ([ORDR_ID],[ODIE_MSG_ID],[STUS_MOD_DT],[H1_CUST_ID],[CREAT_DT]
								   ,[STUS_ID],[MDS_EVENT_ID],[TAB_SEQ_NBR],[CUST_NME],[ODIE_CUST_ID]
								   ,[CPT_ID],[ODIE_CUST_INFO_REQ_CAT_TYPE_ID],[DEV_FLTR])
								VALUES
								   (@ORDR_ID,6,GETDATE(),NULL,GETDATE()
								   ,10,NULL,NULL,NULL,NULL
								   ,NULL,NULL,@DEVICE_ID)

								EXEC dbo.insertOrderNotes @ORDR_ID,9,'Order Information successfully sent to ODIE.',1
							END

				END
			
	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH
END