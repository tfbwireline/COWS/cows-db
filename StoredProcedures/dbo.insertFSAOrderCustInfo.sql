USE [COWS]
GO
_CreateObject 'SP','dbo','insertFSAOrderCustInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/11/2011
-- Description:	Inserts the FSA record into the FSA_ORDR_CUST table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertFSAOrderCustInfo]
	@ORDR_ID				Int
	,@CIS_LVL_TYPE			Varchar(2)
	,@CUST_ID				Int
	,@CUST_NME				Varchar(50)
	,@BR_CD					Varchar(2)
	,@CURR_BILL_CYC_CD		Varchar(2)
	,@FUT_BILL_CYC_CD		Varchar(2)
	,@SRVC_SUB_TYPE_ID		Varchar(6)
	,@SOI_CD				Varchar(3)
	,@SALS_PERSN_PRIM_CID	Varchar(9)
	,@SALS_PERSN_SCNDY_CID	Varchar(9)
	,@CLLI_CD				Varchar(50)	
	
	,@ORDR_CNTCT_ID			Int
	,@CNTCT_TYPE_ID			Int
	,@FRST_NME				Varchar(50)
	,@LST_NME				Varchar(50)
	,@NME					Varchar(50)	=	NULL
	,@PHN_NBR				Varchar(20)
	,@EMAIL_ADR				Varchar(200)
	,@TME_ZONE_ID			Varchar(5)
	,@INTPRTR_CD			Bit
	,@CREAT_BY_USER_ID		Int
	,@FAX_NBR				Varchar(20)
	,@ROLE_ID				TinyInt
	,@NPA					Varchar(3)	=	NULL
	,@NXX					Varchar(3)	=	NULL
	,@STN_NBR				Varchar(4)	=	NULL
	,@CTY_CD				Varchar(5)	=	NULL
	,@ISD_CD				Varchar(3)	=	NULL
	,@PHN_EXT_NBR			Varchar(10)	=	NULL
	
	,@ORDR_ADR_ID		Int
	,@ADR_TYPE_ID		TinyInt
	,@STREET_ADR_1		Varchar(50)
	,@STREET_ADR_2		Varchar(50)
	,@CTY_NME			Varchar(20)
	,@PRVN_NME			Varchar(200)
	,@STT_CD			Varchar(2)
	,@ZIP_PSTL_CD		Varchar(20)
	,@CTRY_CD			Varchar(2)
	,@FSA_MDUL_ID		Varchar(3)
	,@HIER_LVL_CD		Bit			=	0
	
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY

OPEN SYMMETRIC KEY FS@K3y 
    DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
    
	INSERT INTO dbo.FSA_ORDR_CUST WITH (ROWLOCK)
					(ORDR_ID
					,CIS_LVL_TYPE
					,CUST_ID
					,CUST_NME
					,BR_CD
					,CURR_BILL_CYC_CD
					,FUT_BILL_CYC_CD
					,SRVC_SUB_TYPE_ID
					,SOI_CD
					,SALS_PERSN_PRIM_CID
					,SALS_PERSN_SCNDY_CID
					,CLLI_CD
					)
		VALUES		(@ORDR_ID
					,@CIS_LVL_TYPE
					,@CUST_ID
					,dbo.encryptString(@CUST_NME)
					,@BR_CD
					,@CURR_BILL_CYC_CD
					,@FUT_BILL_CYC_CD
					,@SRVC_SUB_TYPE_ID
					,@SOI_CD
					,@SALS_PERSN_PRIM_CID
					,@SALS_PERSN_SCNDY_CID
					,@CLLI_CD)
					
	EXEC dbo.insertOrderContactInfo
					@ORDR_CNTCT_ID OUT
					,@ORDR_ID				
					,@CNTCT_TYPE_ID			
					,@FRST_NME				
					,@LST_NME	
					,@NME		
					,@PHN_NBR			
					,@EMAIL_ADR			
					,@TME_ZONE_ID		
					,@INTPRTR_CD		
					,@CREAT_BY_USER_ID	
					,@FAX_NBR				
					,@ROLE_ID				
					,@CIS_LVL_TYPE
					,@FSA_MDUL_ID
					,@NPA
					,@NXX
					,@STN_NBR
					,@CTY_CD
					,@ISD_CD
					,@PHN_EXT_NBR
								
	EXEC  dbo.insertOrderAddressInfo
					@ORDR_ADR_ID	OUT
					,@ORDR_ID			
					,@ADR_TYPE_ID		
					,@STREET_ADR_1		
					,@STREET_ADR_2	
					,@CTY_NME		
					,@PRVN_NME	
					,@STT_CD		
					,@ZIP_PSTL_CD	
					,NULL --@BLDG_NME			
					,NULL --@FLR_ID		
					,NULL --@RM_NBR		
					,@CREAT_BY_USER_ID	
					,@CTRY_CD		
					,@CIS_LVL_TYPE	
					,@FSA_MDUL_ID
					,@HIER_LVL_CD
					
					
					
	IF	@CIS_LVL_TYPE	IN	('H5', 'H6')
		BEGIN			
			-- Populate the CUST_ID, DMSTC_CD & H5_FOLDR_ID for H5/H6 records.
			UPDATE			o
				SET			o.H5_H6_CUST_ID	=	@CUST_ID
							,o.DMSTC_CD		=	Case	@CIS_LVL_TYPE
													When	'H5'	Then	1
													Else	0
												End
							,o.H5_FOLDR_ID	=	hf.H5_FOLDR_ID					
				FROM		dbo.ORDR		o	WITH (ROWLOCK)
				LEFT JOIN	dbo.H5_FOLDR	hf	WITH (NOLOCK)	ON	@CUST_ID	=	hf.CUST_ID
				WHERE		o.ORDR_ID		=	@ORDR_ID
		END
	ELSE IF @CIS_LVL_TYPE	=	'H1'
		BEGIN
			-- Populate the Secured Code flag for H1 records.
			UPDATE		dbo.ORDR	WITH (ROWLOCK)
				SET		SCURD_CD	=	1
				WHERE	ORDR_ID		=	@ORDR_ID
					AND	(@FUT_BILL_CYC_CD	IN	('43', '75', '76')
					OR	@CURR_BILL_CYC_CD	IN	('43', '75', '76'))
		END	
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END

GO