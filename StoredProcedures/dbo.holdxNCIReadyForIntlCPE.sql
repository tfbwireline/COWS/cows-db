USE [COWS]
GO
_CreateObject 'SP','dbo','holdxNCIReadyForIntlCPE'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <08/02/2012>
-- Description:	<update xNCI Ready status to hold for Intl CPE orders orders till GOM populates certain key fields>
-- =============================================
Alter PROCEDURE dbo.holdxNCIReadyForIntlCPE
	@OrderID INT, 
	@TaskId SMALLINT, 
	@TaskStatus TINYINT, 
	@Comments VARCHAR(1000) = NULL
AS
BEGIN
	
	BEGIN TRY
		IF EXISTS
			(
			SELECT	'X'
				FROM	dbo.ORDR		o WITH (NOLOCK)
			Inner Join	dbo.FSA_ORDR	f WITH (NOLOCK) ON f.ORDR_ID = o.ORDR_ID
				WHERE	o.DMSTC_CD		=	1
					AND	f.PROD_TYPE_CD	=	'CP'
					AND	o.ORDR_ID		=	@OrderID
					AND ISNULL(o.RGN_ID,1) IN (1,3)
					AND o.ORDR_CAT_ID	=	2
			)
			BEGIN
				Delete dbo.ACT_TASK WITH (ROWLOCK) WHERE ORDR_ID = @OrderID AND TASK_ID = 210 AND STUS_ID = 0
			END
	END TRY
	BEGIN CATCH
		Exec dbo.insertErrorInfo
	END CATCH
END
GO
