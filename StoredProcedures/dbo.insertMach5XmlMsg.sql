USE [COWS]
GO
_CreateObject 'SP','dbo','insertMach5XmlMsg'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		mjl0081
-- Create date: 04/30/2015
-- Description:	PJ8006 SP to insert the received Mach5 webservice 
--				XML into the [dbo].[M5_XML_MSG_CMCTN] table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertMach5XmlMsg]
	 @MSG_XML_TXT				VARCHAR(MAX)
	,@CREAT_DT					DATETIME
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY

	INSERT INTO dbo.M5_XML_MSG_CMCTN
				(MSG_XML_TXT
				,CREAT_DT
				,IS_REJ_CD
				)
	VALUES		(@MSG_XML_TXT
				,@CREAT_DT
				,0 --Use Reject Code = 0 to flag this record for pickup.  TODO: rename the IS_REJ_CD column to a more appropriate name that reflects how it is being used.
				)		
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
