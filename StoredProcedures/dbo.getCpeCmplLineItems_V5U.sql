USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getCpeCmplLineItems_V5U]    Script Date: 05/25/2016 09:36:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 5/4/2015
	-- Description:	Appian CPE LINE ITEM Info.
	---- =============================================
	

	create PROCEDURE [dbo].[getCpeCmplLineItems_V5U]  --301945
		@ORDR_ID INT

	AS
	BEGIN

	SELECT	distinct	 litm.FSA_CPE_LINE_ITEM_ID 
				,fo.ORDR_ID      
				,fo.FTN                          											   
				,litm.ORDR_CMPNT_ID AS CMPNT_ID
                ,litm.EQPT_TYPE_ID
                ,litm.MDS_DES
                ,litm.EQPT_ID	
                ,litm.MFR_NME
                ,litm.CNTRC_TYPE_ID
                ,litm.MNTC_CD	
				,litm.SRVC_LINE_ITEM_CD
				,litm.INSTLN_CD
				,litm.DISC_PL_NBR
				,litm.DISC_FMS_CKT_NBR
				,litm.DISC_NW_ADR
				,litm.LINE_ITEM_CD
				,litm.CXR_CKT_ID
				,litm.TTRPT_ACCS_TYPE_CD
				,litm.TTRPT_SPD_OF_SRVC_BDWD_DES
				,litm.TTRPT_ACCS_TYPE_DES 
				,litm.PLSFT_RQSTN_NBR
				,litm.RQSTN_DT
				,litm.PRCH_ORDR_NBR
				,litm.EQPT_ITM_RCVD_DT
				,litm.ORDR_QTY  -- changed name
				,litm.MATL_CD
				,litm.UNIT_MSR 
				-- new fields
				,litm.UNIT_PRICE
				,litm.MANF_PART_CD
				,litm.VNDR_CD
				, ' ' AS VNDR_NME
				, litm.MANF_DISCNT_CD     
				,CASE
					WHEN ISNULL(litm.MANF_DISCNT_CD,'') <> '' THEN 'Y'
					ELSE ''
					END							   AS DEAL_ID
				, litm.EQPT_RCVD_BY_ADID		   AS EQPT_RCVD_BY_ADID
				,litm.ITM_STUS                     AS ITM_STUS
				,ISNULL(litm.CMPL_DT,GETDATE())    AS CMPL_DT
				,litm.PO_LN_NBR                    AS PO_LN_NBR
				,litm.RCVD_QTY					   AS RCVD_QTY
				,litm.PS_RCVD_STUS				   AS PS_RCVD_STUS
				,litm.PID						   AS PID
				,litm.DROP_SHP					   AS DROP_SHP
				,litm.DEVICE_ID					   AS DEVICE_ID
				,litm.SUPPLIER                  AS SUPPLIER
				,litm.CMPNT_FMLY                AS CMPNT_FMLY
				,litm.RLTD_CMPNT_ID				AS RLTD_CMPNT_ID
				,CASE 
					WHEN ISNULL(litm.CNTRC_TYPE_ID,'') <> '' THEN Substring(litm.CNTRC_TYPE_ID,1,1)
					WHEN ISNULL(litm.CNTRC_TYPE_ID,'') = '' THEN  SUBSTRING(ritm.CNTRC_TYPE_ID,1,1) 
				END AS CT
    
				
      	

	FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
	INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
	INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
	INNER JOIN   dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID
	LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM ritm WITH (NOLOCK) 
				ON ritm.CMPNT_ID = litm.RLTD_CMPNT_ID 


	WHERE  fo.ORDR_ID = @ORDR_ID	
		AND litm.CMPL_DT IS NULL 
		AND ((fo.ORDR_TYPE_CD <> 'CN' AND litm.ITM_STUS = 401)
								OR
			(fo.ORDR_TYPE_CD = 'CN' AND litm.ITM_STUS = 402))
		

	END


