USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getCalendarViewDetails]    Script Date: 11/10/2020 8:26:12 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/04/2011
-- Description:	Get the details of a given Calendar View!
-- =========================================================
-- Modify:		kaw4664
-- Create date: 04/11/2012
-- Description:	Add Fedline Events
-- =========================================================
ALTER PROCEDURE [web].[getCalendarViewDetails]
	@DSPL_VW_ID		Int 
	,@APPT_TYPE_ID	Int 
	,@SelDate		DateTime	=	NULL
	,@SelType		Varchar(50)	=	''
	,@SelUser		Varchar(50)	=	''
	,@SelGroup		Varchar(50)	=	''
	,@CSGLvlId	TINYINT	= 0
AS
BEGIN
SET NOCOUNT ON;
Begin Try
				DECLARE @TempView TABLE 
		(Iden			Int	IDENTITY(1, 1)
		,EVENT_ID		Int
		,StartTime		DateTime
		,EndTime		DateTime
		,CSG_LVL_ID		TINYINT
		,Subject		Varchar(1000)
		--,Description	Varchar(1000)
		,Location		Varchar(500)
		,AppointmentType	Varchar(50)
		,CreatedByUID		Int
		,GroupName			Varchar(max)
		,CreatedBy			Varchar(50)
		,ModifiedBy			varchar(50)
		,ModifiedDate		SmallDateTime
		,CreateDate			SmallDateTime
		,AssignedUsersXML		XML
		,AssignedUsers		varchar(max)
		,Flag				Bit
		)
	
	DECLARE	@ApptTypes	TABLE
		(ApptType	SmallInt)
	IF	@APPT_TYPE_ID	=	200
		BEGIN
			INSERT INTO 	@ApptTypes	(ApptType)
				SELECT		APPT_TYPE_ID
					FROM	dbo.LK_APPT_TYPE	WITH (NOLOCK)
					WHERE	(APPT_TYPE_ID	>	8	AND	APPT_TYPE_ID	<	17) OR APPT_TYPE_ID IN (31,32) --Fedline/MVS Shift ID
		END
	ELSE IF @APPT_TYPE_ID	=	100	
		BEGIN
			INSERT INTO 	@ApptTypes	(ApptType)
				SELECT		APPT_TYPE_ID
					FROM	dbo.LK_APPT_TYPE	WITH (NOLOCK)
					WHERE	APPT_TYPE_ID	<	9	
						OR	(APPT_TYPE_ID	>	16	AND APPT_TYPE_ID NOT IN (31, 32))
		END
	ELSE
		BEGIN		
			INSERT INTO 	@ApptTypes	(ApptType)
				VALUES		(@APPT_TYPE_ID)
		END	
	
	IF	@SelType != ''
		BEGIN
			DELETE FROM @ApptTypes WHERE ApptType NOT IN 
				(SELECT StringID FROM web.ParseStringWithDelimiter(@SelType, ','))	
		END
		
	IF	@SelDate IS NULL
		BEGIN
			IF @APPT_TYPE_ID = 30
			BEGIN
				OPEN SYMMETRIC KEY FS@K3y 
				DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

				INSERT INTO @TempView	(EVENT_ID		,StartTime	,EndTime	,CSG_LVL_ID	,Subject	,Location	,AppointmentType
										,CreatedByUID	,GroupName	,CreatedBy	,ModifiedBy	,ModifiedDate	,CreateDate	,AssignedUsersXML,	Flag	)		
					SELECT			a.EVENT_ID
									,STRT_TMST					AS	StartTime
									,END_TMST					AS	EndTime
									,2							AS  CSG_LVL_ID 
									,dbo.decryptBinaryData(csd.EVENT_TITLE_TXT) AS	Subject
									--,[DES]						AS	Description
									,'Installation Contact Phone: ' + cn.PHN_NBR					AS	Location
									,lt.APPT_TYPE_DES			AS	AppointmentType
									,a.CREAT_BY_USER_ID			AS	CreatedByUID
									,''							AS	GroupName
									,lc.FULL_NME				AS	CreatedBy
									,ISNULL(lm.FULL_NME, '')	AS	ModifiedBy
									,a.MODFD_DT					AS	ModifiedDate
									,a.CREAT_DT					AS	CreateDate
									,CONVERT(XML,ASN_TO_USER_ID_LIST_TXT)	AS	AssignedUsers	
									,0							AS	Flag
						FROM		dbo.APPT						a	WITH (NOLOCK)
						INNER JOIN	dbo.LK_APPT_TYPE				lt	WITH (NOLOCK)	ON	a.APPT_TYPE_ID		=	lt.APPT_TYPE_ID	
						INNER JOIN	dbo.FEDLINE_EVENT_USER_DATA		fe  WITH (NOLOCK)	ON	a.EVENT_ID			=	fe.EVENT_ID
						INNER JOIN	dbo.FEDLINE_EVENT_TADPOLE_DATA	td  WITH (NOLOCK)	ON	a.EVENT_ID			=	td.EVENT_ID	AND	td.REC_STUS_ID	=	1
						INNER JOIN	dbo.FEDLINE_EVENT_CNTCT			cn  WITH (NOLOCK)	ON	td.FEDLINE_EVENT_ID	=	cn.FEDLINE_EVENT_ID
						INNER JOIN	dbo.LK_USER			lc	WITH (NOLOCK)	ON	a.CREAT_BY_USER_ID	=	lc.USER_ID
						INNER JOIN  dbo.CUST_SCRD_DATA csd WITH (NOLOCK)   ON csd.SCRD_OBJ_ID=fe.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=25
						LEFT JOIN	dbo.LK_USER			lm	WITH (NOLOCK)	ON	a.MODFD_BY_USER_ID	=	lm.USER_ID
						WHERE		STRT_TMST			>=	GETDATE() - 90
								AND	cn.ROLE_ID			=	1
								AND	cn.CNTCT_TYPE_ID	=	1
								AND (@CSGLvlId IN (1,2))
			END
			ELSE
			BEGIN
				INSERT INTO @TempView	(EVENT_ID		,StartTime	,EndTime	,CSG_LVL_ID	,Subject	,Location	,AppointmentType
										,CreatedByUID	,GroupName	,CreatedBy	,ModifiedBy	,ModifiedDate	,CreateDate	,AssignedUsersXML,	Flag	)		
					SELECT			a.EVENT_ID
									,STRT_TMST					AS	StartTime
									,END_TMST					AS	EndTime
									,ev.CSG_LVL_ID				AS	CSG_LVL_ID
									,SUBJ_TXT					AS	Subject
									--,[DES]						AS	Description
									,APPT_LOC_TXT				AS	Location
									,lt.APPT_TYPE_DES			AS	AppointmentType
									,a.CREAT_BY_USER_ID			AS	CreatedByUID
									,''							AS	GroupName
									,lc.FULL_NME				AS	CreatedBy
									,ISNULL(lm.FULL_NME, '')	AS	ModifiedBy
									,a.MODFD_DT					AS	ModifiedDate
									,a.CREAT_DT					AS	CreateDate
									,CONVERT(XML,ASN_TO_USER_ID_LIST_TXT)	AS	AssignedUsers	
									,0							AS	Flag
						FROM		dbo.APPT			a	WITH (NOLOCK)
						INNER JOIN	dbo.LK_APPT_TYPE	lt	WITH (NOLOCK)	ON	a.APPT_TYPE_ID		=	lt.APPT_TYPE_ID	
						INNER JOIN	@ApptTypes			vt					ON	a.APPT_TYPE_ID		=	vt.ApptType
						INNER JOIN	dbo.LK_USER			lc	WITH (NOLOCK)	ON	a.CREAT_BY_USER_ID	=	lc.USER_ID
						LEFT JOIN	dbo.LK_USER			lm	WITH (NOLOCK)	ON	a.MODFD_BY_USER_ID	=	lm.USER_ID
						LEFT JOIN	dbo.[EVENT]			ev	WITH (NOLOCK)	ON	ev.EVENT_ID			=	a.EVENT_ID
						WHERE		(STRT_TMST			>=	GETDATE() - 90)
							AND ((a.REC_STUS_ID IS NULL) OR a.REC_STUS_ID IN (0,1))
							AND (a.RCURNC_CD = 0)

				INSERT INTO @TempView	(EVENT_ID		,StartTime	,EndTime	,CSG_LVL_ID	,Subject	,Location	,AppointmentType
										,CreatedByUID	,GroupName	,CreatedBy	,ModifiedBy	,ModifiedDate	,CreateDate	,AssignedUsersXML,	Flag	)		
					SELECT			a.EVENT_ID
									,a.STRT_TMST					AS	StartTime
									,a.END_TMST					AS	EndTime
									,ev.CSG_LVL_ID				AS	CSG_LVL_ID
									,SUBJ_TXT					AS	Subject
									--,[DES]						AS	Description
									,APPT_LOC_TXT				AS	Location
									,lt.APPT_TYPE_DES			AS	AppointmentType
									,a.CREAT_BY_USER_ID			AS	CreatedByUID
									,''							AS	GroupName
									,lc.FULL_NME				AS	CreatedBy
									,ISNULL(lm.FULL_NME, '')	AS	ModifiedBy
									,a.MODFD_DT					AS	ModifiedDate
									,a.CREAT_DT					AS	CreateDate
									,CONVERT(XML,ASN_TO_USER_ID_LIST_TXT)	AS	AssignedUsers	
									,0							AS	Flag
						FROM		dbo.APPT			a	WITH (NOLOCK)
						INNER JOIN  dbo.APPT_RCURNC_DATA ar WITH (NOLOCK)	ON	a.APPT_ID = ar.APPT_ID
						INNER JOIN	dbo.LK_APPT_TYPE	lt	WITH (NOLOCK)	ON	a.APPT_TYPE_ID		=	lt.APPT_TYPE_ID	
						INNER JOIN	@ApptTypes			vt					ON	a.APPT_TYPE_ID		=	vt.ApptType
						INNER JOIN	dbo.LK_USER			lc	WITH (NOLOCK)	ON	a.CREAT_BY_USER_ID	=	lc.USER_ID
						LEFT JOIN	dbo.LK_USER			lm	WITH (NOLOCK)	ON	a.MODFD_BY_USER_ID	=	lm.USER_ID
						LEFT JOIN	dbo.[EVENT]			ev	WITH (NOLOCK)	ON	ev.EVENT_ID			=	a.EVENT_ID
						WHERE		(ar.STRT_TMST			>=	GETDATE() - 90)
							AND ((a.REC_STUS_ID IS NULL) OR a.REC_STUS_ID IN (0,1))
							AND (a.RCURNC_CD = 1)
			END
		END	
	ELSE
		BEGIN
			DECLARE	@SelDateV	Varchar(10)
			SET	@SelDateV	=	Convert(Varchar(10), @SelDate, 101)
			IF @APPT_TYPE_ID = 30
			BEGIN
				OPEN SYMMETRIC KEY FS@K3y 
				DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

				INSERT INTO @TempView	(EVENT_ID		,StartTime	,EndTime	,CSG_LVL_ID	,Subject	,Location	,AppointmentType
										,CreatedByUID	,GroupName	,CreatedBy	,ModifiedBy	,ModifiedDate	,CreateDate	,AssignedUsersXML,	Flag	)		
					SELECT			a.EVENT_ID
									,STRT_TMST					AS	StartTime
									,END_TMST					AS	EndTime
									,2							AS  CSG_LVL_ID
									,dbo.decryptBinaryData(csd.EVENT_TITLE_TXT)					AS	Subject
									--,[DES]						AS	Description
									,'Installation Contact Phone: ' + cn.PHN_NBR					AS	Location
									,lt.APPT_TYPE_DES			AS	AppointmentType
									,a.CREAT_BY_USER_ID			AS	CreatedByUID
									,''							AS	GroupName
									,lc.FULL_NME				AS	CreatedBy
									,ISNULL(lm.FULL_NME, '')	AS	ModifiedBy
									,a.MODFD_DT					AS	ModifiedDate
									,a.CREAT_DT					AS	CreateDate
									,CONVERT(XML,ASN_TO_USER_ID_LIST_TXT)	AS	AssignedUsers	
									,0							AS	Flag
						FROM		dbo.APPT						a	WITH (NOLOCK)
						INNER JOIN	dbo.LK_APPT_TYPE				lt	WITH (NOLOCK)	ON	a.APPT_TYPE_ID		=	lt.APPT_TYPE_ID	
						INNER JOIN	dbo.FEDLINE_EVENT_USER_DATA		fe	WITH (NOLOCK)	ON	a.EVENT_ID			=	fe.EVENT_ID
						INNER JOIN	dbo.FEDLINE_EVENT_TADPOLE_DATA	td	WITH (NOLOCK)	ON	a.EVENT_ID			=	td.EVENT_ID	AND	td.REC_STUS_ID	=	1
						INNER JOIN	dbo.FEDLINE_EVENT_CNTCT			cn	WITH (NOLOCK)	ON	td.FEDLINE_EVENT_ID	=	cn.FEDLINE_EVENT_ID
						INNER JOIN	dbo.LK_USER			lc	WITH (NOLOCK)	ON	a.CREAT_BY_USER_ID	=	lc.USER_ID
						INNER JOIN  dbo.CUST_SCRD_DATA csd WITH (NOLOCK)   ON csd.SCRD_OBJ_ID=fe.EVENT_ID AND csd.SCRD_OBJ_TYPE_ID=25
						LEFT JOIN	dbo.LK_USER			lm	WITH (NOLOCK)	ON	a.MODFD_BY_USER_ID	=	lm.USER_ID
						WHERE		STRT_TMST			>=	GETDATE() - 90
								AND	cn.ROLE_ID			=	1
								AND	cn.CNTCT_TYPE_ID	=	1
								AND (@CSGLvlId IN (1,2))
			END
			ELSE
			BEGIN
				INSERT INTO @TempView	(EVENT_ID		,StartTime	,EndTime	,CSG_LVL_ID	,Subject	,Location	,AppointmentType
										,CreatedByUID	,GroupName	,CreatedBy	,ModifiedBy	,ModifiedDate	,CreateDate	,AssignedUsersXML,	Flag	)		
					SELECT			a.EVENT_ID
									,STRT_TMST					AS	StartTime
									,END_TMST					AS	EndTime
									,ev.CSG_LVL_ID				AS  CSG_LVL_ID
									,SUBJ_TXT					AS	Subject
									--,[DES]						AS	Description
									,APPT_LOC_TXT				AS	Location
									,lt.APPT_TYPE_DES			AS	AppointmentType
									,a.CREAT_BY_USER_ID			AS	CreatedByUID
									,''							AS	GroupName
									,lc.FULL_NME				AS	CreatedBy
									,ISNULL(lm.FULL_NME, '')	AS	ModifiedBy
									,a.MODFD_DT					AS	ModifiedDate
									,a.CREAT_DT					AS	CreateDate
									,CONVERT(XML,ASN_TO_USER_ID_LIST_TXT)	AS	AssignedUsers	
									,0							AS	Flag
						FROM		dbo.APPT			a	WITH (NOLOCK)
						INNER JOIN	dbo.LK_APPT_TYPE	lt	WITH (NOLOCK)	ON	a.APPT_TYPE_ID		=	lt.APPT_TYPE_ID	
						INNER JOIN	@ApptTypes			vt					ON	a.APPT_TYPE_ID		=	vt.ApptType
						INNER JOIN	dbo.LK_USER			lc	WITH (NOLOCK)	ON	a.CREAT_BY_USER_ID	=	lc.USER_ID
						LEFT JOIN	dbo.LK_USER			lm	WITH (NOLOCK)	ON	a.MODFD_BY_USER_ID	=	lm.USER_ID
						LEFT JOIN	dbo.[EVENT]			ev	WITH (NOLOCK)	ON	ev.EVENT_ID			=	a.EVENT_ID
						WHERE		(Convert(Varchar(10), STRT_TMST, 101)	=	@SelDateV)
							AND ((a.REC_STUS_ID IS NULL) OR a.REC_STUS_ID IN (0,1))
							AND (a.RCURNC_CD = 0)
						

				INSERT INTO @TempView	(EVENT_ID		,StartTime	,EndTime	,CSG_LVL_ID	,Subject	,Location	,AppointmentType
										,CreatedByUID	,GroupName	,CreatedBy	,ModifiedBy	,ModifiedDate	,CreateDate	,AssignedUsersXML,	Flag	)		
					SELECT			a.EVENT_ID
									,a.STRT_TMST					AS	StartTime
									,a.END_TMST					AS	EndTime
									,ev.CSG_LVL_ID				AS  CSG_LVL_ID
									,SUBJ_TXT					AS	Subject
									--,[DES]						AS	Description
									,APPT_LOC_TXT				AS	Location
									,lt.APPT_TYPE_DES			AS	AppointmentType
									,a.CREAT_BY_USER_ID			AS	CreatedByUID
									,''							AS	GroupName
									,lc.FULL_NME				AS	CreatedBy
									,ISNULL(lm.FULL_NME, '')	AS	ModifiedBy
									,a.MODFD_DT					AS	ModifiedDate
									,a.CREAT_DT					AS	CreateDate
									,CONVERT(XML,ASN_TO_USER_ID_LIST_TXT)	AS	AssignedUsers	
									,0							AS	Flag
						FROM		dbo.APPT			a	WITH (NOLOCK)
						INNER JOIN  dbo.APPT_RCURNC_DATA ar WITH (NOLOCK)	ON	a.APPT_ID = ar.APPT_ID
						INNER JOIN	dbo.LK_APPT_TYPE	lt	WITH (NOLOCK)	ON	a.APPT_TYPE_ID		=	lt.APPT_TYPE_ID	
						INNER JOIN	@ApptTypes			vt					ON	a.APPT_TYPE_ID		=	vt.ApptType
						INNER JOIN	dbo.LK_USER			lc	WITH (NOLOCK)	ON	a.CREAT_BY_USER_ID	=	lc.USER_ID
						LEFT JOIN	dbo.LK_USER			lm	WITH (NOLOCK)	ON	a.MODFD_BY_USER_ID	=	lm.USER_ID
						LEFT JOIN	dbo.[EVENT]			ev	WITH (NOLOCK)	ON	ev.EVENT_ID			=	a.EVENT_ID
						WHERE		(Convert(Varchar(10), ar.STRT_TMST, 101)	=	@SelDateV)
							AND ((a.REC_STUS_ID IS NULL) OR a.REC_STUS_ID IN (0,1))
							AND (a.RCURNC_CD = 1)

			END
		END
		
	DELETE		a
		FROM	@TempView	a
		WHERE	(	EXISTS	(SELECT 'X'	FROM dbo.MDS_EVENT				WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND ((EVENT_STUS_ID NOT IN (2, 3, 4, 5, 6, 7, 9, 10)) OR ((EVENT_STUS_ID=3) AND (WRKFLW_STUS_ID NOT IN (3,5,8)))))
				OR	EXISTS	(SELECT 'X'	FROM dbo.UCaaS_EVENT				WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND ((EVENT_STUS_ID NOT IN (2, 3, 4, 5, 6, 7, 9, 10)) OR ((EVENT_STUS_ID=3) AND (WRKFLW_STUS_ID NOT IN (3,5,8)))))
				OR	EXISTS	(SELECT 'X'	FROM dbo.SIPT_EVENT					WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND ((EVENT_STUS_ID NOT IN (2, 3, 4, 5, 6, 7)) OR ((EVENT_STUS_ID=3) AND (WRKFLW_STUS_ID NOT IN (3,5,8)))))
				OR	EXISTS	(SELECT 'X'	FROM dbo.FEDLINE_EVENT_USER_DATA	WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND EVENT_STUS_ID NOT IN (2, 5, 6, 9, 10, 14, 15, 16, 17))
				)	
		
	DELETE		a
		FROM	@TempView	a
		WHERE	@APPT_TYPE_ID	=	100
		AND		StartTime < GETDATE() - 2
		AND		(	EXISTS	(SELECT 'X'	FROM dbo.MDS_EVENT				WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND EVENT_STUS_ID = 6)
				OR	EXISTS	(SELECT 'X'	FROM dbo.UCaaS_EVENT					WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND EVENT_STUS_ID = 6)
				OR	EXISTS	(SELECT 'X'	FROM dbo.SIPT_EVENT					WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND EVENT_STUS_ID = 6)
				OR	EXISTS	(SELECT 'X'	FROM dbo.FEDLINE_EVENT_USER_DATA	WITH (NOLOCK) WHERE EVENT_ID	=	a.EVENT_ID AND (EVENT_STUS_ID = 6 OR EVENT_STUS_ID = 14))
				)

	UPDATE @TempView
	SET AssignedUsers = dbo.[getCommaSepAssignedUsersFromApptXML](AssignedUsersXML),
	GroupName = dbo.[getCommaSepGroupsFromUserID](CreatedByUID)
			
	IF	(@SelGroup	!=	'')	
		BEGIN
			DELETE FROM @TempView	WHERE	GroupName	=	''
		END
		
	SELECT DISTINCT	vt.EVENT_ID
					,CASE WHEN ev.EVENT_TYPE_ID IN (5,6,9,10,19) THEN es.EVENT_STUS_DES
					      ELSE '' END	AS	EventStatus
					--,ve.EVENT_STUS_DES	AS	EventStatus .. dlp0278 11/10/2020 changed way to get status for performance issue.
					,vt.StartTime	AS	StartTime
					,vt.EndTime	AS	EndTime
					,vt.CSG_LVL_ID
					,CASE WHEN vt.CSG_LVL_ID>0 THEN 'Private Customer, Event ID : '+CONVERT(VARCHAR,ev.EVENT_ID) ELSE vt.[Subject] END AS [Subject]
					--,vt.Description
					,CASE WHEN vt.CSG_LVL_ID>0 THEN '6200 Sprint Parkway' ELSE vt.Location END AS Location
					,vt.AppointmentType
					,CASE vt.AppointmentType	WHEN 'Fedline Shift' THEN 'IPSD' ELSE vt.GroupName END as GroupName
					,vt.CreatedBy	
					,vt.ModifiedBy	
					,vt.ModifiedDate	
					,vt.CreateDate		
					,vt.AssignedUsers 
					,ev.EVENT_TYPE_ID
		FROM		@TempView	vt
		LEFT JOIN	dbo.[EVENT]	ev	WITH (NOLOCK)	ON	vt.EVENT_ID	=	ev.EVENT_ID
		--LEFT JOIN	dbo.V_EVENT	ve	WITH (NOLOCK)	ON	ev.EVENT_ID	=	ve.EVENT_ID .. removed 11/10/2020 dlp0278 for performance issues.
		LEFT JOIN dbo.MDS_EVENT mde WITH (NOLOCK) ON ev.EVENT_ID = mde.EVENT_ID -- dlp0278 added 11/10/2020
		LEFT JOIN dbo.UCaaS_EVENT ue WITH (NOLOCK) ON ev.EVENT_ID = ue.EVENT_ID -- dlp0278 added 11/10/2020
		LEFT JOIN dbo.SIPT_EVENT sp WITH (NOLOCK) ON ev.EVENT_ID = sp.EVENT_ID -- dlp0278 added 11/10/2020
		LEFT JOIN dbo.FEDLINE_EVENT_USER_DATA	fu	WITH (NOLOCK) ON	ev.EVENT_ID = fu.EVENT_ID-- dlp0278 added 11/10/2020
		LEFT JOIN dbo.LK_EVENT_STUS es WITH (NOLOCK) ON ((mde.EVENT_STUS_ID = es.EVENT_STUS_ID) OR (fu.EVENT_STUS_ID = es.EVENT_STUS_ID) OR (sp.EVENT_STUS_ID = es.EVENT_STUS_ID) OR (ue.EVENT_STUS_ID = es.EVENT_STUS_ID)) -- dlp0278 added 11/10/2020
		--LEFT JOIN  dbo.LK_EVENT_STUS	evs	WITH (NOLOCK)	ON	evs.EVENT_STUS_ID	=	fu.EVENT_STUS_ID-- dlp0278 added 11/10/2020
		WHERE		((@SelUser	=	'')
			OR		((@SelUser	!=	'')	AND	(AssignedUsers	!=	'')))
			AND		((ISNULL(ev.CSG_LVL_ID,0) = 0) OR ((@CSGLvlId!=0) AND (ev.CSG_LVL_ID >= @CSGLvlId)))
		ORDER BY	vt.EVENT_ID DESC, vt.StartTime, vt.CreateDate DESC
End Try
Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END