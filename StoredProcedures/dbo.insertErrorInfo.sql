USE [COWS]
GO
_CreateObject 'SP','dbo','insertErrorInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		jrg7298
-- Create date: 09/01/2010
-- Description:	Inserts Error Info (http://msdn2.microsoft.com/en-us/library/ms175976.aspx)
-- =============================================
ALTER PROCEDURE [dbo].[insertErrorInfo]
AS
BEGIN
SET NOCOUNT ON

  -- Test XACT_STATE:
        -- If 1, the transaction is committable.
        -- If -1, the transaction is uncommittable and should 
        --     be rolled back.
        -- XACT_STATE = 0 means that there is no transaction and
        --     a commit or rollback operation would generate an error.

	DECLARE @XACT_STATE SMALLINT = XACT_STATE()
	IF (@XACT_STATE = -1)
	 ROLLBACK TRANSACTION;
	IF (@XACT_STATE = 1)
	 COMMIT TRANSACTION; 
	DECLARE @ErrorNum INT = ERROR_NUMBER()
	DECLARE @ErrorMsg VARCHAR(4000) = ERROR_MESSAGE()
	DECLARE @TransInfoTxt VARCHAR(500) = CASE WHEN (@XACT_STATE = -1) THEN 'The transaction is in an uncommittable state.' +'Transaction rolled back.'
											  WHEN (@XACT_STATE = 1)  THEN 'The transaction is in a committable state.' +'Transaction committed.'
											  WHEN (@XACT_STATE = 0)  THEN 'Not in Transaction mode.'
											  ELSE 'Not in Transaction mode. ' + CONVERT(VARCHAR,@XACT_STATE) END

	IF (charindex('^|^',@ErrorMsg) > 0)
	BEGIN
		SET @ErrorNum = substring(@ErrorMsg,charindex('^|^',@ErrorMsg)+3,len(@ErrorMsg))
		SET @ErrorMsg = substring(@ErrorMsg,0,charindex('^|^',@ErrorMsg))
	END
			 INSERT INTO 
					dbo.SQL_ERROR
				(	Error_Nbr, Error_Svrty_CD, Error_State_CD,
					Error_Line_Nbr, Error_Prc_Name, Error_Msg_Txt, 	[Host_Name] , Tran_Info_Txt)
			   VALUES
				(    @ErrorNum,
					ERROR_SEVERITY() ,
					ERROR_STATE() ,
					ERROR_LINE () ,
					ERROR_PROCEDURE(),
					@ErrorMsg,
					host_name() ,
					@TransInfoTxt
				)
   
SET NOCOUNT OFF
END
GO