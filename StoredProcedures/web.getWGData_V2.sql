USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getWGData_V2]    Script Date: 02/10/2021 11:08:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================================
-- Author:		jrg7298
-- Create date: 11/03/2019
-- Description:	Gets all the data for the default page based of PRF_ID and UserID.
-- km967761 - 08/14/2020 - GET SECURED DATA IF VETTED USER
-- ================================================================================
 --[web].[getWGData] 7,0,0,0,0
ALTER PROCEDURE [web].[getWGData_V2] --86,0,0,0,0
	 @USR_PRF_ID		SMALLInt 	
	,@UserID	Int	= 0
	,@OrderID	Int = 0
	,@IsCmplt	Bit = 0
	,@CSGLvlId	TINYINT	= 0
AS
BEGIN
SET NOCOUNT ON;
DECLARE	@Order	TABLE
	(ORDR_ID	Int
	,TaskID		Int
	,StatusID	Int DEFAULT 0
	,UsrPrfID	SmallInt
	,IsCmplt	Bit DEFAULT 0)
	
DECLARE	@RGN_ID	Int
SET	@RGN_ID	=	1
Begin Try
	SELECT		@RGN_ID	=	ISNULL(RGN_ID, 1)
		FROM	dbo.LK_XNCI_RGN	WITH (NOLOCK)
		WHERE	USR_PRF_ID	=	@USR_PRF_ID

IF @OrderID = 0
BEGIN 
	IF	@UserID	=	0
		BEGIN
			IF @IsCmplt = 0
				BEGIN
				IF @USR_PRF_ID = 86
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, StatusID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.STUS_ID, OB.PRNT_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, at.STUS_ID, mt.PRNT_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.PRNT_PRF_ID				=	@USR_PRF_ID
										AND		at.STUS_ID				IN	(0, 156)
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID			=	6
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @USR_PRF_ID = 170
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.PRNT_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.PRNT_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.PRNT_PRF_ID				=	@USR_PRF_ID
										AND		at.STUS_ID				=	0
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') in ('UCCH','UCSV','MIPT','UCSM')
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @USR_PRF_ID = 44
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.PRNT_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.PRNT_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON cli.ORDR_ID = fo.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.PRNT_PRF_ID				=	@USR_PRF_ID
										AND		at.STUS_ID				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND		od.DMSTC_CD = 0
										AND		od.DMSTC_CD = 0 AND  od.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID = 6
										AND ISNULL(cli.DEVICE_ID,'') <> ''
										AND ISNULL(fo.CPE_VNDR,'') = 'Goodman'
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @USR_PRF_ID = 128
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.PRNT_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.PRNT_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.PRNT_PRF_ID				=	@USR_PRF_ID
										AND		at.STUS_ID				=	0
										AND		ISNULL(od.RGN_ID, 1)	=	Case	
																				When	@USR_PRF_ID	IN (2, 16, 100)	Then 	@RGN_ID
																				Else	ISNULL(od.RGN_ID, 1)
																			End
										AND		od.DMSTC_CD				=	Case	
																				When	@USR_PRF_ID	IN (2, 16, 100)	Then 	1
																				Else	od.DMSTC_CD
																			End
										AND		(
													(od.ORDR_STUS_ID IN (0,1)) AND (@USR_PRF_ID NOT IN (2, 16, 100))
													OR
													mt.PRNT_PRF_ID = 58
													OR
													(ISNULL(oms.CUST_ACPTC_TURNUP_DT,'') =	'' AND @USR_PRF_ID IN (2, 16, 100))
												)
									ORDER BY orderByDate DESC	
									
									UNION 
									
									SELECT DISTINCT Top 1000		ord.ORDR_ID,	602, 128, ISNULL(ord.MODFD_DT, ord.CREAT_DT) as orderByDate
									FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
									INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
									INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
									INNER JOIN   dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID

									WHERE  fo.PROD_TYPE_CD = 'CP'
										AND (act.TASK_ID IN (602) AND act.STUS_ID = 0)
										AND fo.ORDR_ID in (SELECT ORDR_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
																WHERE litm.CNTRC_TYPE_ID  IN ('CUST','INSI'))
																order by 1 desc								
									
									UNION 
									
									SELECT DISTINCT Top 1000		ord.ORDR_ID,	602, 128, ISNULL(ord.MODFD_DT, ord.CREAT_DT) as orderByDate
									FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
									INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
									INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
									INNER JOIN   dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID

									WHERE  fo.PROD_TYPE_CD = 'CP'
										AND (act.TASK_ID IN (602) AND act.STUS_ID = 0)
										AND ISNULL(PRCH_ORDR_NBR,'') <> ''
										AND litm.ITM_STUS = 401						
							)	AS OB	
						END
				ELSE IF @USR_PRF_ID = 58
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.PRNT_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.PRNT_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.PRNT_PRF_ID				=	@USR_PRF_ID
										AND		at.STUS_ID				=	0
										AND		ISNULL(od.RGN_ID, 1)	=	Case	
																				When	@USR_PRF_ID	IN (2, 16, 100)	Then 	@RGN_ID
																				Else	ISNULL(od.RGN_ID, 1)
																			End
										AND		(
													(od.ORDR_STUS_ID IN (0,1)) AND (@USR_PRF_ID NOT IN (2, 16, 100))
													OR
													mt.PRNT_PRF_ID = 58
													OR
													(ISNULL(oms.CUST_ACPTC_TURNUP_DT,'') =	'' AND @USR_PRF_ID IN (2, 16, 100))
												)
									ORDER BY orderByDate DESC
								)	AS OB	
								--PRINT 'IN'
						END			
				ELSE
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.PRNT_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.PRNT_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		mt.PRNT_PRF_ID				=	@USR_PRF_ID
										AND		at.STUS_ID				=	0
										AND		ISNULL(od.RGN_ID, 1)	=	Case	
																				When	@USR_PRF_ID	IN (2, 16, 100)	Then 	@RGN_ID
																				Else	ISNULL(od.RGN_ID, 1)
																			End
										AND		od.DMSTC_CD				=	1
										AND		(
													(od.ORDR_STUS_ID IN (0,1)) AND (@USR_PRF_ID NOT IN (2, 16, 100))
													OR
													mt.PRNT_PRF_ID = 58
													OR
													(ISNULL(oms.CUST_ACPTC_TURNUP_DT,'') =	'' AND @USR_PRF_ID IN (2, 16, 100))
												)
									ORDER BY orderByDate DESC
								)	AS OB	
								--PRINT 'IN'
						END
				END														
			ELSE
				BEGIN
					IF @USR_PRF_ID = 156
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	IsCmplt)
							SELECT OB.ORDR_ID, 1 FROM (
								SELECT DISTINCT Top 1000	od.ORDR_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
								FROM	dbo.ORDR	od	WITH (NOLOCK)
							INNER JOIN	dbo.ORDR_NTE		ont	WITH (NOLOCK) ON od.ORDR_ID = ont.ORDR_ID
								AND ont.NTE_TYPE_ID	=	14
								AND od.ORDR_STUS_ID	NOT IN (0,1)
								WHERE	@USR_PRF_ID =	156
								ORDER BY orderByDate DESC
							)	AS OB
						END
					ELSE IF @USR_PRF_ID IN (2,16,100)
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	IsCmplt)
							SELECT OB.ORDR_ID, 1 FROM (
								SELECT DISTINCT Top 1000	od.ORDR_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM	dbo.ORDR	od	WITH (NOLOCK)
							INNER JOIN 		dbo.ORDR_MS oms with (nolock) on oms.ORDR_ID = od.ORDR_ID 
									WHERE	ISNULL(oms.CUST_ACPTC_TURNUP_DT,'') <> ''
										AND @USR_PRF_ID		IN (2, 16, 100)	
										AND	od.DMSTC_CD	=	1
										AND	ISNULL(od.RGN_ID,1)		=	CASE
																			WHEN @USR_PRF_ID  = 2  THEN 1
																			WHEN @USR_PRF_ID  = 100  THEN 2 
																			WHEN @USR_PRF_ID  = 16  THEN 3
																			ELSE  ISNULL(od.RGN_ID,1)
																		END
								ORDER BY orderByDate DESC
							)	AS OB
						END
					ELSE IF @USR_PRF_ID = 86
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, StatusID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.STUS_ID, OB.USR_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, at.STUS_ID, 13 as USR_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		
										at.STUS_ID				=	0 
										AND at.TASK_ID			= 1001
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID			=	6
									ORDER BY orderByDate DESC
								)	AS OB
						END
					ELSE IF @USR_PRF_ID = 170
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.USR_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, 11 as USR_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		
										at.STUS_ID				=	0 
										AND	od.DMSTC_CD			=	0
										AND at.TASK_ID			= 1001
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') in ('UCCH','UCSV','MIPT','UCSM')
									ORDER BY orderByDate DESC
								)	AS OB
						END
					ELSE IF @USR_PRF_ID = 44
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.USR_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, 15 as USR_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON cli.ORDR_ID = fo.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									WHERE		at.STUS_ID				=	0 
										AND     at.TASK_ID				= 1001
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND		od.DMSTC_CD = 0 AND  od.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID = 6
										AND ISNULL(cli.DEVICE_ID,'') <> ''
										AND ISNULL(fo.CPE_VNDR,'') = 'Goodman'
									ORDER BY orderByDate DESC
								)	AS OB
						END
					ELSE
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	IsCmplt)
							SELECT OB.ORDR_ID, 1 FROM (
								SELECT DISTINCT Top 1000	od.ORDR_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
								FROM			dbo.ORDR			od	WITH (NOLOCK)
									LEFT JOIN	dbo.FSA_ORDR		f	WITH (NOLOCK) ON f.ORDR_ID		= od.ORDR_ID 	
									INNER JOIN	dbo.LK_PPRT			p	WITH (NOLOCK) ON od.PPRT_ID		=	p.PPRT_ID	 
									INNER JOIN	dbo.SM				s	WITH (NOLOCK) ON s.SM_ID		=	p.SM_ID
									INNER JOIN	dbo.WG_PTRN_SM		pt	WITH (NOLOCK) ON pt.WG_PTRN_ID	=	s.PRE_REQST_WG_PTRN_ID
									INNER JOIN	dbo.WG_PROF_SM		pf	WITH (NOLOCK) ON pf.WG_PROF_ID	=	pt.PRE_REQST_WG_PROF_ID
									INNER JOIN	dbo.MAP_PRF_TASK	mgt	WITH (NOLOCK) ON mgt.TASK_ID	=	pf.PRE_REQST_TASK_ID
								WHERE	od.ORDR_STUS_ID	NOT IN (0,1)		
									AND (
											(@USR_PRF_ID 	=	1 AND f.ORDR_ACTN_ID = 3)
											OR 
											(mgt.TASK_ID NOT IN (100,101,109)
														AND	mgt.PRNT_PRF_ID	=	@USR_PRF_ID)
										)
								ORDER BY orderByDate DESC
							)	AS OB
							--PRINT 'IN'
						END									
				END
				
		END
	ELSE
		BEGIN
					IF @USR_PRF_ID = 86
						BEGIN
							INSERT INTO @Order	(ORDR_ID, TaskID, StatusID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.STUS_ID, OB.PRNT_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, at.STUS_ID, mt.PRNT_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									LEFT JOIN	dbo.ORDR_NTE		nte	WITH (NOLOCK)	ON	od.ORDR_ID	=	nte.ORDR_ID
									WHERE		mt.PRNT_PRF_ID				=	@USR_PRF_ID
										AND		at.STUS_ID				IN	(0, 156)
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID			=	6
										--AND ISNULL(fo.CPE_VNDR,'')		<>	'Goodman'
										AND		nte.CREAT_BY_USER_ID = @UserID
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @USR_PRF_ID = 170
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.PRNT_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.PRNT_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									INNER JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									LEFT JOIN	dbo.ORDR_NTE		nte	WITH (NOLOCK)	ON	od.ORDR_ID	=	nte.ORDR_ID
									WHERE		mt.PRNT_PRF_ID				=	@USR_PRF_ID
										AND		at.STUS_ID				=	0
										AND		od.DMSTC_CD				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND ISNULL(od.PROD_ID,'') in ('UCCH','UCSV','MIPT','UCSM')
										AND		nte.CREAT_BY_USER_ID = @UserID
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE IF @USR_PRF_ID = 44
						BEGIN
							INSERT INTO @Order	(ORDR_ID,	TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.PRNT_PRF_ID FROM (
								SELECT DISTINCT Top 1000		od.ORDR_ID,	at.TASK_ID, mt.PRNT_PRF_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									INNER JOIN  dbo.FSA_ORDR        fo	WITH (NOLOCK)   ON	fo.ORDR_ID	=	od.ORDR_ID
									LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON cli.ORDR_ID = fo.ORDR_ID
									INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									LEFT JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.MAP_WFM_PRF		mwp	WITH (NOLOCK)	ON	mwp.PRNT_PRF_ID = mt.PRNT_PRF_ID
									LEFT JOIN	dbo.ORDR_MS			oms WITH (NOLOCK)	ON	oms.ORDR_ID =	od.ORDR_ID
																						AND	oms.VER_ID	=	(SELECT MAX(VER_ID) FROM dbo.ORDR_MS om WITH (NOLOCK) WHERE om.ORDR_ID = oms.ORDR_ID)
									LEFT JOIN	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	ON	od.ORDR_ID	=	uw.ORDR_ID
																			AND	mwp.WFM_PRF_ID	=	uw.USR_PRF_ID
									LEFT JOIN	dbo.ORDR_NTE		nte	WITH (NOLOCK)	ON	od.ORDR_ID	=	nte.ORDR_ID
									WHERE		mt.PRNT_PRF_ID				=	@USR_PRF_ID
										AND		at.STUS_ID				=	0
										AND		fo.PROD_TYPE_CD			=  'CP'
										AND		od.DMSTC_CD = 0
										AND		od.DMSTC_CD = 0 AND  od.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')
										AND		od.ORDR_CAT_ID = 6
										AND ISNULL(cli.DEVICE_ID,'') <> ''
										AND ISNULL(fo.CPE_VNDR,'') = 'Goodman'
										AND		(uw.ASN_USER_ID			=	@UserID 
										OR nte.CREAT_BY_USER_ID = @UserID)
									ORDER BY orderByDate DESC
								)	AS OB
						END
				ELSE 
					BEGIN
						INSERT INTO @Order	(ORDR_ID,	TaskID, UsrPrfID)
							SELECT OB.ORDR_ID, OB.TASK_ID, OB.WG_ID FROM (
								SELECT DISTINCT Top 1000	od.ORDR_ID,	at.TASK_ID, Case	@USR_PRF_ID
																						When	0	Then	uw.USR_PRF_ID
																						Else	@USR_PRF_ID
																					End	AS	WG_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
									FROM		dbo.ORDR			od	WITH (NOLOCK)
									LEFT JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
									LEFT JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
									LEFT JOIN	dbo.MAP_WFM_PRF		mwp	WITH (NOLOCK)	ON	mwp.PRNT_PRF_ID = mt.PRNT_PRF_ID
									LEFT JOIN	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	ON	od.ORDR_ID	=	uw.ORDR_ID
																						AND	mwp.WFM_PRF_ID	=	uw.USR_PRF_ID
									WHERE		at.STUS_ID				=	0
										AND		uw.ASN_USER_ID			=	@UserID 
										AND		mt.PRNT_PRF_ID				=	Case	@USR_PRF_ID
																				When	0	Then	mwp.PRNT_PRF_ID
																				Else	@USR_PRF_ID
																			End
										AND		ISNULL(od.RGN_ID, 1)	=	Case	
																				When	@USR_PRF_ID	IN (2, 16, 100)	Then 	@RGN_ID
																				Else	ISNULL(od.RGN_ID, 1)
																			End
										AND		od.DMSTC_CD				=	Case	
																						When	@USR_PRF_ID	IN (2, 16, 100)	Then 	1
																						Else	od.DMSTC_CD
																					End
									ORDER BY orderByDate DESC
									
							UNION 
													
							SELECT DISTINCT Top 1000	od.ORDR_ID,	at.TASK_ID, Case	@USR_PRF_ID
																					When	0	Then	uw.USR_PRF_ID
																					Else	@USR_PRF_ID
																				End	AS	WG_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
								FROM		dbo.ORDR			od	WITH (NOLOCK)
								LEFT JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
								LEFT JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
								LEFT JOIN	dbo.MAP_WFM_PRF		mwp	WITH (NOLOCK)	ON	mwp.PRNT_PRF_ID = mt.PRNT_PRF_ID
								LEFT JOIN	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	ON	od.ORDR_ID	=	uw.ORDR_ID
																					AND	(mt.PRNT_PRF_ID = 184)
								LEFT JOIN	dbo.FSA_ORDR		fsa WITH (NOLOCK)	ON  od.ORDR_ID  =	fsa.ORDR_ID
									WHERE	@USR_PRF_ID != 114 AND	
											at.STUS_ID				=	0
									AND		uw.ASN_USER_ID			=	@UserID 
									AND		fsa.PROD_TYPE_CD		= 'CP'
									AND		at.TASK_ID = 1000
									--AND		uw.USR_PRF_ID				=	Case	@USR_PRF_ID
									--										When	0	Then	uw.USR_PRF_ID
									--										Else	@USR_PRF_ID
									--									End
									AND		ISNULL(od.RGN_ID, 1)	=	Case	
																			When	@USR_PRF_ID	IN (2, 16, 100)	Then 	@RGN_ID
																			Else	ISNULL(od.RGN_ID, 1)
																		End
									AND		od.DMSTC_CD				=	Case	
																					When	@USR_PRF_ID	IN (2, 16, 100)	Then 	1
																					Else	od.DMSTC_CD
																				End

									ORDER BY orderByDate DESC
							UNION 
													
							SELECT DISTINCT Top 1000	od.ORDR_ID,	at.TASK_ID, Case	'114'
																					When	0	Then	uw.USR_PRF_ID
																					Else	@USR_PRF_ID
																				End	AS	WG_ID, ISNULL(od.MODFD_DT, od.CREAT_DT) as orderByDate
								FROM		dbo.ORDR			od	WITH (NOLOCK)
								INNER JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID
								LEFT JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID 
								LEFT JOIN	dbo.MAP_WFM_PRF		mwp	WITH (NOLOCK)	ON	mwp.PRNT_PRF_ID = mt.PRNT_PRF_ID AND mwp.PRNT_PRF_ID=114
								LEFT JOIN	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	ON	od.ORDR_ID	=	uw.ORDR_ID AND uw.USR_PRF_ID=mwp.WFM_PRF_ID
								LEFT JOIN	dbo.FSA_ORDR		fsa WITH (NOLOCK)	ON  od.ORDR_ID  =	fsa.ORDR_ID
									WHERE	114 = 114 AND
											at.STUS_ID				=	0 AND at.TASK_ID NOT IN (1000,1001)
									AND		uw.ASN_USER_ID			=	@UserID 
									AND		fsa.PROD_TYPE_CD		= 'CP'
									ORDER BY orderByDate DESC
								)	AS OB
						END
		END
END
ELSE IF (@OrderID > 0)
BEGIN 
			INSERT INTO @Order	(ORDR_ID,	TaskID, UsrPrfID)
				SELECT DISTINCT od.ORDR_ID,	at.TASK_ID,		Case	@USR_PRF_ID
																When	0	Then	mt.PRNT_PRF_ID
																Else	@USR_PRF_ID
															End
					FROM		dbo.ORDR			od	WITH (NOLOCK)
					LEFT JOIN	dbo.ACT_TASK		at	WITH (NOLOCK)	ON	od.ORDR_ID	=	at.ORDR_ID AND		at.STUS_ID	IN	(0, 156)
					LEFT JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
					--LEFT JOIN USER_WFM_ASMT uw ON uw.ORDR_ID = od.ORDR_ID
					--LEFT JOIN MAP_WFM_PRF mwp ON mwp.WFM_PRF_ID = uw.USR_PRF_ID
																	
					WHERE		od.ORDR_ID				=	@OrderID
						AND		ISNULL(mt.PRNT_PRF_ID,0)				=	Case	@USR_PRF_ID
																When	0	Then	ISNULL(mt.PRNT_PRF_ID,0)
																Else	@USR_PRF_ID
															End
						AND		ISNULL(od.RGN_ID, 1)	=	Case	
																When	@USR_PRF_ID	IN (2, 16, 100)	Then 	@RGN_ID
																Else	ISNULL(od.RGN_ID, 1)
															End
						AND		od.DMSTC_CD				=	Case	
																		When	@USR_PRF_ID	IN (2, 16, 100)	Then 	1
																		Else	od.DMSTC_CD
																	End
END 

OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	IF	OBJECT_ID(N'tempdb..#TempResults', N'U') IS NOT NULL 
		DROP TABLE #TempResults
	
	DECLARE @TAC_PRIMARY Varchar(100)
	---------------------------------------------------------------------------------
	--	Get Data for the FSA Orders.		
	---------------------------------------------------------------------------------
	SELECT Distinct Top 1000	od.ORDR_ID
						,od.ORDR_CAT_ID	AS	CAT_ID
						,ISNULL(od.H5_FOLDR_ID, '')					AS	H5_FOLDR_ID
						,CASE 
							WHEN vt.IsCmplt = 0 THEN vt.TaskID									
							WHEN vt.IsCmplt = 1 AND @USR_PRF_ID IN (2, 16, 100) THEN CASE ISNULL(od.ORDR_STUS_ID,0) 
																					WHEN 0 THEN 1000
																					WHEN 1 THEN 1000
																					ELSE 1001
																			END	
							WHEN vt.IsCmplt = 1 AND @USR_PRF_ID NOT IN (2, 16, 100) THEN 1001																					
							END AS	TASK_ID
						,fs.ORDR_ACTN_ID							AS	ORDR_ACTN_ID
						,Case	fs.PRNT_FTN
							When	''	Then	NULL
							else	fs.PRNT_FTN
						End											AS	PRNT_FTN
						,fs.FTN										AS	FTN
						,fc.CUST_ID									AS	H1
						,ISNULL(od.H5_H6_CUST_ID, '')				AS	H5_H6_CUST_ID
						,lo.ORDR_ACTN_DES							AS	ORDR_ACTN_DES,
						CASE
							WHEN ISNULL(od.CSG_LVL_ID,0) = 0 THEN fc.CUST_NME
							WHEN @CSGLvlId > 0 AND @CSGLvlId <= ISNULL(od.CSG_LVL_ID,0) THEN dbo.decryptBinaryData(csdfc.CUST_NME)
							ELSE ''
						END AS CUST_NME,
						CASE
							WHEN ISNULL(od.CSG_LVL_ID,0) = 0 THEN fc5.CUST_NME
							WHEN @CSGLvlId > 0 AND @CSGLvlId <= ISNULL(od.CSG_LVL_ID,0) THEN dbo.decryptBinaryData(csdf5.CUST_NME)
							ELSE ''
						END AS H5_H6_CUST_NME
						--,Case	
						--	WHEN ISNULL(od.CSG_LVL_ID,0)!=0  AND SUBSTRING(dbo.decryptBinaryData(csdfc.CUST_NME),1,2) = 'O-'	
						--			Then	'O-Private Customer'
						--	WHEN ISNULL(od.CSG_LVL_ID,0)!=0  AND SUBSTRING(dbo.decryptBinaryData(csdfc.CUST_NME),1,2) = 'T-'	
						--			Then	'T-Private Customer'
						--	WHEN	ISNULL(od.CSG_LVL_ID,0)!=0	Then	'Private Customer'
						--	Else	[dbo].[RemoveSpecialCharacters](fc.CUST_NME)
						--End											AS	CUST_NME
						--,Case
						--	WHEN ISNULL(od.CSG_LVL_ID,0)!=0  AND SUBSTRING(dbo.decryptBinaryData(csdfc.CUST_NME),1,2) = 'O-'	
						--			Then	'O-Private Customer'
						--	WHEN ISNULL(od.CSG_LVL_ID,0)!=0  AND SUBSTRING(dbo.decryptBinaryData(csdfc.CUST_NME),1,2) = 'T-'	
						--			Then	'T-Private Customer'	
						--	WHEN	ISNULL(od.CSG_LVL_ID,0)!=0	Then	'Private Customer'
						--	Else	[dbo].[RemoveSpecialCharacters](fc5.CUST_NME)
						--End											AS	H5_H6_CUST_NME
						,CASE 
							WHEN lp.ORDR_TYPE_ID = 8 AND fs.ORDR_TYPE_CD != 'CN' THEN lt.FSA_ORDR_TYPE_DES + ' Cancel'
							ELSE lt.FSA_ORDR_TYPE_DES
							END AS	ORDR_TYPE
						,ISNULL(lst.ORDR_SUB_TYPE_DES, '')			AS	ORDR_SUB_TYPE
						,CASE WHEN (od.PROD_ID='CETH') THEN 'Carrier Ethernet'
							  WHEN (od.PROD_ID='CEPL') THEN 'Carrier Ethernet Private Line'
							  WHEN (od.PROD_ID='CEVPL') THEN 'Carrier Ethernet Virtual Private Line'
							  WHEN (od.PROD_ID='CENNI') THEN 'Carrier Ethernet Network to Network Interface'
							  ELSE lf.FSA_PROD_TYPE_DES		END				AS	PROD_TYPE
						,ISNULL(fc.SRVC_SUB_TYPE_ID, '')			AS	SRVC_SUB_TYPE
						,Case	od.DMSTC_CD
							When	0	Then	'DOM'
							Else	'INTL'
						End											AS	DMSTC_CD
						,ISNULL(od.CUST_CMMT_DT, ISNULL(fs.CUST_CMMT_DT, null))	AS	CUST_CMMT_DT
						,ISNULL(od.CSG_LVL_ID,0)					AS	CSG_LVL_ID					
						,Case  
							WHEN vt.IsCmplt = 0 
								THEN 
									Case
										When	ls.TASK_ID = 102 AND od.DMSTC_CD = 1 AND fs.PROD_TYPE_CD	= 'CP' Then	'GOM Milestones Pending' 
										When	ls.TASK_ID = 1001 Then	'Completed' 
										Else	ls.TASK_NME + ' Pending'
									End	
							WHEN vt.IsCmplt = 1 AND @USR_PRF_ID NOT IN (2, 16, 100) THEN los.ORDR_STUS_DES
							WHEN vt.IsCmplt = 1 AND @USR_PRF_ID IN (2, 16, 100) THEN CASE od.ORDR_STUS_ID 
																				WHEN 0 THEN CASE lt.FSA_ORDR_TYPE_CD
																								WHEN 'DC' THEN 'Disconnected'
																								WHEN 'CN' THEN 'Cancelled'
																								ELSE 'Completed'
																							END
																				WHEN 1 	THEN CASE lt.FSA_ORDR_TYPE_CD
																								WHEN 'DC' THEN 'Disconnected'
																								WHEN 'CN' THEN 'Cancelled'
																								ELSE 'Completed'
																							END
																				ELSE los.ORDR_STUS_DES
																			END
							END	AS	ORDR_STUS
						,ISNULL(fs.TSUP_PRS_QOT_NBR, '')			AS	TSUP_PRS_QOT_NBR
						,ISNULL(fc5.SOI_CD, '')						AS	SOI_CD 
						,Case	ISNULL(od.INSTL_ESCL_CD,ISNULL(fs.INSTL_ESCL_CD, ''))
							When	'Y'	Then	'Yes'
							When	'N'	Then	'No'
							Else	''
						End											AS	INSTL_ESCL_CD
						,Case	ISNULL(od.FSA_EXP_TYPE_CD,ISNULL(fs.FSA_EXP_TYPE_CD, ''))
							When	'IN'	Then	'Internal'
							When	'EX'	Then	'External'
							When	'IE'	Then	'Internal & External'
							Else	''
						End											AS	FSA_EXP_TYPE_CD
						,dbo.getCommaSeparatedUsers_V2(vt.ORDR_ID, @USR_PRF_ID, @UserID)	AS	ASN_USER
						,Case	ISNULL(ht.CCD_HIST_ID, 0)
							When 0	Then	''
							Else	Convert(Varchar(10), ht.OLD_CCD_DT, 101)
						End											AS	CHNGD_CCD
						,ISNULL(ht.OLD_CCD_DT, '')					AS	CHNGD_CCD_ORG
						--,web.getVendorName(od.ORDR_ID)				AS	VNDR_NME
						,CASE lpt.PLTFRM_CD
								WHEN 'SF'	THEN ISNULL(sf.VNDR_NME,ISNULL(rsf.VNDR_NME,ISNULL(web.getVendorName(od.ORDR_ID, od.ORDR_CAT_ID, od.PLTFRM_CD, lot.FSA_ORDR_TYPE_CD),lvo.VNDR_NME)))
								WHEN 'CP'	THEN ISNULL(cp.VNDR_NME,ISNULL(rcp.VNDR_NME,ISNULL(web.getVendorName(od.ORDR_ID, od.ORDR_CAT_ID, od.PLTFRM_CD, lot.FSA_ORDR_TYPE_CD),lvo.VNDR_NME)))
								WHEN 'RS'	THEN ISNULL(rs.VNDR_NME,ISNULL(rrs.VNDR_NME,ISNULL(web.getVendorName(od.ORDR_ID, od.ORDR_CAT_ID, od.PLTFRM_CD, lot.FSA_ORDR_TYPE_CD),lvo.VNDR_NME)))
								ELSE ''
								END AS	VNDR_NME
						--,ISNULL(fs.TTRPT_SPD_OF_SRVC_BDWD_DES,'')	AS	TTRPT_SPD_OF_SRVC_BDWD_DES         -- IM5923588 changed 10/30/2020 dlp0278
						--,ISNULL(CPEP.TTRPT_SPD_OF_SRVC_BDWD_DES,ISNULL(CPEA.TTRPT_SPD_OF_SRVC_BDWD_DES,'')) AS TTRPT_SPD_OF_SRVC_BDWD_DES
						,(SELECT TOP 1 ISNULL(CPEP.TTRPT_SPD_OF_SRVC_BDWD_DES,ISNULL(CPEA.TTRPT_SPD_OF_SRVC_BDWD_DES,''))
								FROM dbo.FSA_ORDR_CPE_LINE_ITEM CPEA WITH (NOLOCK)
								LEFT JOIN dbo.FSA_ORDR_CPE_LINE_ITEM CPEP WITH (NOLOCK) ON fs.ORDR_ID= CPEP.ORDR_ID AND CPEP.LINE_ITEM_CD='PRT' WHERE fs.ORDR_ID= CPEA.ORDR_ID AND CPEA.LINE_ITEM_CD='ACS') AS TTRPT_SPD_OF_SRVC_BDWD_DES
						,(SELECT TOP 1 COALESCE(
							(SELECT TOP 1 TTRPT_ACCS_BDWD_TYPE FROM FSA_ORDR_CPE_LINE_ITEM focli WHERE focli.ORDR_ID = od.ORDR_ID and focli.LINE_ITEM_CD = 'PRT'),
							(SELECT TOP 1 TTRPT_ACCS_BDWD_TYPE FROM FSA_ORDR_CPE_LINE_ITEM focli WHERE focli.ORDR_ID = od.ORDR_ID and focli.LINE_ITEM_CD = 'ACS'))) as 'AccessBandwidth'
						,CASE
							WHEN ISNULL(od.CSG_LVL_ID,0) = 0 THEN ad.CTY_NME
							WHEN @CSGLvlId > 0 AND @CSGLvlId <= ISNULL(od.CSG_LVL_ID,0) THEN dbo.decryptBinaryData(csdad.CTY_NME)
							ELSE ''
						END AS CTY_NME
						--,CASE WHEN (ISNULL(od.CSG_LVL_ID,0)!=0) THEN ISNULL(dbo.decryptBinaryData(csdad.CTY_NME), '') ELSE ISNULL(ad.CTY_NME,'') END	AS	CTY_NME
						,ISNULL(stt.US_STATE_NAME,'')				AS	STT_NME
						,ISNULL(lc.CTRY_NME,'')						AS	CTRY_NME
						,ISNULL(lpt.PLTFRM_NME,'')					AS	PLTFRM_NME
						,ISNULL(od.SLA_VLTD_CD,0)					AS  SLA_VLTD_CD
						,od.PRNT_ORDR_ID							AS	PRNT_ORDR_ID
						,CASE	When ISNULL(fs.INSTL_ESCL_CD, '') = 'Y' AND ISNULL(fs.FSA_EXP_TYPE_CD, '') <> '' THEN 0
								When ISNULL(fs.INSTL_ESCL_CD, '') IN ('','N') AND ISNULL(fs.FSA_EXP_TYPE_CD, '') = '' THEN 2
								Else 1
						End											AS	Exp_Sort
						,vt.UsrPrfID USR_PRF_ID
						,CASE	WHEN @USR_PRF_ID IN (2, 16, 100)	THEN	CASE dbo.RTSSLAExceeded(vt.ORDR_ID) WHEN 1 THEN 'Y'
																									WHEN 0 THEN 'N'
																END
								ELSE 'N'
						END											AS	RTSSLA
						,web.retrieveRTSStatus(od.ORDR_ID,1)		AS	RTS_FLG
						,web.retrieveRTSStatus(od.ORDR_ID,0)		AS	RTS_DT
						,web.retrieveRTSStatus(od.ORDR_ID,2)		AS	RTS_DESC
						,CONVERT(Varchar(10), fs.CREAT_DT, 101)		AS	ORDR_RCVD_DT
						,Case	
							When	Len(fs.FSA_EXP_TYPE_CD) > 1		Then	'Y'
							Else	'N'
						End											AS	IS_EXP
						,CASE WHEN fs.ORDR_SBMT_DT IS NULL THEN fs.CREAT_DT ELSE fs.ORDR_SBMT_DT END	AS	ORDR_SBMT_DT
						,x.NTE_DT									AS	XNCI_UPDT_DT,
						CASE
							WHEN ISNULL(od.CSG_LVL_ID,0) = 0 THEN fc.CUST_NME
							WHEN @CSGLvlId > 0 AND @CSGLvlId <= ISNULL(od.CSG_LVL_ID,0) THEN dbo.decryptBinaryData(csdad.STREET_ADR_1)
							ELSE ''
						END AS ADR,
						CASE
							WHEN ISNULL(od.CSG_LVL_ID,0) = 0 THEN ad.ZIP_PSTL_CD
							WHEN @CSGLvlId > 0 AND @CSGLvlId <= ISNULL(od.CSG_LVL_ID,0) THEN dbo.decryptBinaryData(csdad.ZIP_PSTL_CD)
							ELSE ''
						END AS ZIP_CD
						--,Case When ISNULL(od.CSG_LVL_ID,0)!=0
						--	Then	'Private Address'
						--	Else	ISNULL(ad.STREET_ADR_1, '')
						--End											AS ADR
						--,Case
						--	When ISNULL(od.CSG_LVL_ID,0)!=0	Then	'66251'
						--	Else	ISNULL(ad.ZIP_PSTL_CD, '')
						--End											AS ZIP_CD
						,cms.TRGT_DLVRY_DT
						,fs.CUST_WANT_DT
						,@TAC_PRIMARY AS TAC_Primary			
						,ISNULL(uwa.ORDR_HIGHLIGHT_CD,0) AS ORDR_HIGHLIGHT_CD
						,dbo.verifyRTSCCDNotification(od.ORDR_ID) AS RTS_CCD_HIGHLIGHT_CD
						,ISNULL(CONVERT(VARCHAR,me.EVENT_ID),'') AS CPE_EVENT_ID
						,ISNULL(convert(varchar,m.STRT_TMST,22),'') AS EVENT_START_TIME  
						,los2.ORDR_STUS_DES AS CPE_ORDR_STUS
						,les.EVENT_STUS_DES
						,(SELECT TOP 1 ISNULL(litm.CNTRC_TYPE_ID,'')
							FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK)
							WHERE litm.ORDR_ID = od.ORDR_ID
							and litm.CNTRC_TYPE_ID IN ('CUST','INSI')
								ORDER BY litm.CREAT_DT DESC) AS CNTRC_TYPE_ID
						--,(SELECT TOP 1 ISNULL(litm.PRCH_ORDR_NBR,'0')
						--	FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm  
						--	WHERE litm.ORDR_ID = od.ORDR_ID
						--	and litm.EQPT_TYPE_ID IN ('MMM','MJH','SVC','MMH','MMS','SPK')) AS PRCH_ORDR_NBR
						,ISNULL(litm.PRCH_ORDR_NBR,'0') AS PRCH_ORDR_NBR
						,(SELECT TOP 1 ISNULL(litm.ITM_STUS,'')
							FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK)  
							WHERE litm.ORDR_ID = od.ORDR_ID
								AND litm.ITM_STUS = 401) AS ITM_STUS
						,(SELECT TOP 1 ISNULL(litm.PLSFT_RQSTN_NBR,'0')
							FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK)  
							WHERE litm.ORDR_ID = od.ORDR_ID
							ORDER BY litm.CREAT_DT DESC) AS PLSFT_RQSTN_NBR
						,fc5.SITE_ID
						,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_CD,'')
						FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
							 dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
						WHERE oj.ORDR_ID = od.ORDR_ID
							ORDER BY oj.CREAT_DT DESC) AS JPRDY_CD
						,(SELECT TOP 1 ISNULL(lj.CPE_JPRDY_DES,'')
							FROM dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
								 dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
							WHERE oj.ORDR_ID = od.ORDR_ID
								ORDER BY oj.CREAT_DT DESC) AS JPRDY_DES
						,(SELECT TOP 1 DATEDIFF(dd,at1.CREAT_DT,GETDATE()) 
							FROM dbo.ACT_TASK at1 WITH (NOLOCK)
							WHERE at1.TASK_ID IN (600,601,602,1000,604)
												  AND at1.STUS_ID = 0
												  AND at1.ORDR_ID = od.ORDR_ID 
												  ORDER BY at1.CREAT_DT DESC) AS INBOX
					    ,CASE WHEN vt.StatusID = 156 THEN 'Y' ELSE 'N' END AS HOLD
					    ,od.DLVY_CLLI as DLVY_CLLI
					    ,fs.CPE_VNDR as CPE_VNDR
			INTO #TempResults
			FROM		@Order					vt
			INNER JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	vt.ORDR_ID		=	od.ORDR_ID
			INNER JOIN	dbo.FSA_ORDR			fs	WITH (NOLOCK)	ON	od.ORDR_ID		=	fs.ORDR_ID
			LEFT JOIN	dbo.LK_ORDR_SUB_TYPE	lst WITH (NOLOCK)	ON	fs.ORDR_SUB_TYPE_CD = lst.ORDR_SUB_TYPE_CD
			INNER JOIN	dbo.LK_FSA_PROD_TYPE	lf	WITH (NOLOCK)	ON	fs.PROD_TYPE_CD =	lf.FSA_PROD_TYPE_CD
			INNER JOIN	dbo.LK_FSA_ORDR_TYPE	lt	WITH (NOLOCK)	ON	fs.ORDR_TYPE_CD =	lt.FSA_ORDR_TYPE_CD
			LEFT JOIN	dbo.LK_TASK				ls	WITH (NOLOCK)	ON	vt.TaskID		=	ls.TASK_ID
			INNER JOIN	dbo.LK_ORDR_ACTN		lo	WITH (NOLOCK)	ON	fs.ORDR_ACTN_ID =	lo.ORDR_ACTN_ID
			LEFT JOIN	dbo.FSA_ORDR_CUST		fc	WITH (NOLOCK)	ON	fs.ORDR_ID		=	fc.ORDR_ID
																	AND	fc.CIS_LVL_TYPE	=	'H1'
			LEFT JOIN	dbo.FSA_ORDR_CUST		fc5	WITH (NOLOCK)	ON	fs.ORDR_ID		=	fc5.ORDR_ID
																	AND	fc5.CIS_LVL_TYPE	IN	('H5', 'H6')																	
			LEFT JOIN	dbo.ORDR_ADR			ad	WITH (NOLOCK)	ON	od.ORDR_ID		=	ad.ORDR_ID
																	AND	ad.CIS_LVL_TYPE	IN	('H5', 'H6')
																	AND ad.adr_type_id	=	18													
			LEFT JOIN	dbo.CUST_SCRD_DATA		csdad WITH (NOLOCK) ON csdad.SCRD_OBJ_ID = ad.ORDR_ADR_ID AND csdad.SCRD_OBJ_TYPE_ID=14
			LEFT JOIN	dbo.CUST_SCRD_DATA		csdfc WITH (NOLOCK) ON csdfc.SCRD_OBJ_ID = fc.FSA_ORDR_CUST_ID AND csdfc.SCRD_OBJ_TYPE_ID=5
			LEFT JOIN	dbo.CUST_SCRD_DATA		csdf5 WITH (NOLOCK) ON csdf5.SCRD_OBJ_ID = fc5.FSA_ORDR_CUST_ID AND csdf5.SCRD_OBJ_TYPE_ID=5
			LEFT JOIN	(SELECT		ORDR_ID, Min(CCD_HIST_ID) AS	CH_ID	
							FROM	dbo.CCD_HIST	WITH (NOLOCK)
							GROUP BY ORDR_ID)	ch					ON	od.ORDR_ID		=	ch.ORDR_ID
			LEFT JOIN	dbo.CCD_HIST			ht	WITH (NOLOCK)	ON	ch.CH_ID		=	ht.CCD_HIST_ID	
			LEFT JOIN	dbo.LK_CTRY				lc	WITH (NOLOCK)	ON	lc.CTRY_CD		=	ad.CTRY_CD
			LEFT JOIN	dbo.LK_US_STT			stt	WITH (NOLOCK)	ON	ad.STT_CD		=	stt.US_STATE_ID	
			LEFT JOIN	dbo.LK_PLTFRM			lpt	WITH (NOLOCK)	ON	lpt.PLTFRM_CD	=	od.PLTFRM_CD
			LEFT JOIN	dbo.LK_ORDR_STUS		los	WITH (NOLOCK)	ON	los.ORDR_STUS_ID =	od.ORDR_STUS_ID	
			LEFT JOIN   dbo.FSA_ORDR_GOM_XNCI GOM WITH (NOLOCK) ON	od.ORDR_ID	=	GOM.ORDR_ID
																AND	(GOM.USR_PRF_ID	in (114,126) OR GOM.GRP_ID = 1)
			LEFT JOIN	dbo.LK_ORDR_STUS		los2	WITH (NOLOCK)	ON	los2.ORDR_STUS_ID =	GOM.CPE_STUS_ID
			LEFT JOIN   dbo.CKT					ckt	WITH (NOLOCK)   ON	ckt.ORDR_ID		= vt.ORDR_ID
			LEFT JOIN	(SELECT		MAX(VER_ID)	AS	VER_ID,	CKT_ID
							FROM	dbo.CKT_MS	WITH (NOLOCK)	
							GROUP BY	CKT_ID)a					ON	a.CKT_ID		=	ckt.CKT_ID							
			LEFT JOIN	dbo.CKT_MS				cms	WITH (NOLOCK)   ON	cms.CKT_ID		=	a.CKT_ID
																	AND	cms.VER_ID		=	a.VER_ID				
			LEFT JOIN	(SELECT		ORDR_ID,	MAX(CREAT_DT)	AS	NTE_DT
							FROM	dbo.ORDR_NTE	WITH (NOLOCK)
							WHERE	NTE_TYPE_ID	IN	(11, 12)
							GROUP BY	ORDR_ID)x					ON	x.ORDR_ID		=	vt.ORDR_ID
			--LEFT JOIN	dbo.ORDR_CNTCT			oc	WITH (NOLOCK)	ON	oc.ORDR_ID		=	od.ORDR_ID
			--														AND	oc.ROLE_ID		=	13							
			LEFT JOIN	dbo.LK_PPRT				lp	WITH (NOLOCK)	ON	lp.PPRT_ID		=	od.PPRT_ID
			LEFT JOIN	dbo.ORDR_MS				orm	WITH (NOLOCK)	ON	orm.ORDR_ID		=	od.ORDR_ID
			LEFT JOIN	dbo.MAP_PRF_TASK	mt	WITH (NOLOCK)	ON	vt.TaskID	=	mt.TASK_ID
			LEFT JOIN	dbo.MAP_WFM_PRF		mwp	WITH (NOLOCK)	ON	mwp.PRNT_PRF_ID = mt.PRNT_PRF_ID AND mwp.PRNT_PRF_ID = @USR_PRF_ID
			LEFT JOIN	dbo.USER_WFM_ASMT		uwa WITH (NOLOCK)	ON	uwa.ORDR_ID		=	od.ORDR_ID
																	AND	((uwa.USR_PRF_ID		=	mwp.WFM_PRF_ID) OR uwa.USR_PRF_ID IN (2,14,16,28,100,112))
																	AND uwa.ASN_USER_ID		=	@UserID
																	AND ISNULL(uwa.ORDR_HIGHLIGHT_CD,0) = 1
			LEFT JOIN	dbo.LK_VNDR		sf	WITH (NOLOCK)			ON	sf.VNDR_CD		=	fs.CXR_ACCS_CD
			LEFT JOIN	dbo.LK_VNDR		cp	WITH (NOLOCK)			ON	cp.VNDR_CD		=	fs.VNDR_VPN_CD	
			LEFT JOIN	dbo.LK_VNDR		rs	WITH (NOLOCK)			ON	rs.VNDR_CD		=	fs.INSTL_VNDR_CD																
			LEFT JOIN	dbo.FSA_ORDR	rfs	WITH (NOLOCK)			ON	rfs.FTN			=	fs.RELTD_FTN 
			LEFT JOIN	dbo.LK_VNDR		rsf	WITH (NOLOCK)			ON	rsf.VNDR_CD		=	rfs.CXR_ACCS_CD
			LEFT JOIN	dbo.LK_VNDR		rcp	WITH (NOLOCK)			ON	rcp.VNDR_CD		=	rfs.VNDR_VPN_CD	
			LEFT JOIN	dbo.LK_VNDR		rrs	WITH (NOLOCK)			ON	rrs.VNDR_CD		=	rfs.INSTL_VNDR_CD
			LEFT JOIN	dbo.VNDR_ORDR	vo	WITH (NOLOCK)			ON	vo.ORDR_ID		=	od.ORDR_ID
			LEFT JOIN	dbo.VNDR_FOLDR	vf	WITH (NOLOCK)			ON	vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
			LEFT JOIN	dbo.LK_VNDR		lvo WITH (NOLOCK)			ON	lvo.VNDR_CD = vf.VNDR_CD
			LEFT JOIN	(SELECT MAX(EVENT_ID) AS CPE_EVENT_ID, 
								ecd.CPE_ORDR_ID, 
								fod.ORDR_ID AS ORDR_ID
							FROM	dbo.EVENT_CPE_DEV ecd WITH (NOLOCK) 
							INNER JOIN	dbo.FSA_ORDR fod WITH (NOLOCK) ON fod.FTN = ecd.CPE_ORDR_NBR
							WHERE ecd.REC_STUS_ID = 1
							GROUP BY ecd.CPE_ORDR_ID, fod.ORDR_ID) AS cEventID ON cEventID.ORDR_ID = od.ORDR_ID
			LEFT JOIN	dbo.EVENT_CPE_DEV me WITH (NOLOCK) ON	cEventID.CPE_EVENT_ID	=	me.EVENT_ID
																AND cEventID.CPE_ORDR_ID =	me.CPE_ORDR_ID	
																AND me.REC_STUS_ID = 1
			LEFT JOIN	dbo.MDS_EVENT m WITH (NOLOCK)			ON 	me.EVENT_ID		=	m.EVENT_ID
			LEFT JOIN	dbo.LK_EVENT_STUS les WITH (NOLOCK) 	ON m.EVENT_STUS_ID=les.EVENT_STUS_ID
			LEFT JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON od.ORDR_ID = litm.ORDR_ID
																		 AND litm.EQPT_TYPE_ID IN ('MMM','MJH','SVC','MMH','MMS','SPK')
																		 AND litm.PRCH_ORDR_NBR is not null
			LEFT JOIN 	dbo.LK_ORDR_TYPE lot  WITH (NOLOCK) ON lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
			--LEFT JOIN dbo.FSA_ORDR_CPE_LINE_ITEM CPEA WITH (NOLOCK) ON fs.ORDR_ID= CPEA.ORDR_ID AND CPEA.LINE_ITEM_CD='ACS'
			--LEFT JOIN dbo.FSA_ORDR_CPE_LINE_ITEM CPEP WITH (NOLOCK) ON fs.ORDR_ID= CPEP.ORDR_ID AND CPEP.LINE_ITEM_CD='PRT'
			ORDER by 1 DESC
		
		--UPDATE TRGT_DLVRY_DT when NULL
		UPDATE t
		SET t.TRGT_DLVRY_DT = t2.TRGT_DLVRY_DT
		FROM #TempResults t  INNER JOIN
		#TempResults t2 WITH (NOLOCK) ON t.FTN=t2.FTN AND t.ORDR_STUS=t2.ORDR_STUS AND t.ORDR_TYPE=t2.ORDR_TYPE
		WHERE t.TRGT_DLVRY_DT IS NULL
		  AND t2.TRGT_DLVRY_DT IS NOT NULL

		UPDATE t
			SET FTN = CASE WHEN ISNULL(fcpe.DEVICE_ID,'') = '' THEN t.FTN 
							ELSE FTN + '-' + fcpe.DEVICE_ID
						END
			FROM #TempResults t WITH (NOLOCK)
		INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM fcpe WITH (NOLOCK) ON fcpe.ORDR_ID = t.ORDR_ID
			WHERE CAT_ID = 6
				AND PROD_TYPE	=	'CPE' OR (PROD_TYPE LIKE 'Carrier Ethernet%')
				-- AND DMSTC_CD	=	'DOM'
				
		UPDATE t
			SET 
				
				t.TAC_Primary = CASE
					WHEN ISNULL(t.CSG_LVL_ID,0) = 0 THEN oc.FRST_NME + ' ' + oc.LST_NME
					WHEN @CSGLvlId > 0 AND @CSGLvlId <= ISNULL(t.CSG_LVL_ID,0) THEN dbo.decryptBinaryData(csd.FRST_NME) + ' ' + dbo.decryptBinaryData(csd.LST_NME)
					ELSE ''
				END 
				--t.TAC_Primary = CASE 
				--	WHEN (ISNULL(t.CSG_LVL_ID,0) = 0) THEN oc.FRST_NME + ' ' + oc.LST_NME
				--	ELSE dbo.decryptBinaryData(csd.FRST_NME) + ' ' + dbo.decryptBinaryData(csd.LST_NME)
				--END
			FROM #TempResults t		 				
			LEFT JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON t.ORDR_ID = oc.ORDR_ID
			LEFT JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csd.SCRD_OBJ_TYPE_ID=15
			WHERE oc.ROLE_ID = 13
		
		IF (@UserID != 0)
			SELECT DISTINCT * FROM #TempResults WHERE ((ISNULL(CSG_LVL_ID,0) = 0) OR ((@CSGLvlId!=0) AND (ISNULL(CSG_LVL_ID,0) >= @CSGLvlId))) ORDER BY CUST_CMMT_DT ASC, ORDR_ID DESC
		ELSE
			SELECT DISTINCT * FROM #TempResults WHERE ((ISNULL(CSG_LVL_ID,0) = 0) OR ((@CSGLvlId!=0) AND (ISNULL(CSG_LVL_ID,0) >= @CSGLvlId))) ORDER BY ORDR_ID DESC


End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END