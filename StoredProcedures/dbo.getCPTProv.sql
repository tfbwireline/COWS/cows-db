USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getCPTProv]    Script Date: 1/18/2022 2:46:27 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		jrg7298
-- Create date: 09/02/2015
-- Description:	Update the status of a given CPT
-- Update date: 01/10/2022
-- Description: Include 'Needed' column
-- =========================================================
--1	Spectrum
--2	Voyancy
--3	QIP
--4	TACACS
--5	ODIE
-----------------------------------------------------
ALTER PROCEDURE [dbo].[getCPTProv]
@CPTID INT = -1,
@Type SMALLINT = -1
AS
BEGIN
SET NOCOUNT ON;
Begin Try
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
IF ((@CPTID = -1) AND (@Type = -1))
BEGIN
	SELECT cp.CPT_PRVSN_ID AS CPTProvisionID, cp.CPT_ID AS CPTID, lcpt.CPT_PRVSN_TYPE AS ProvisionType,cp.PRVSN_NTE_TXT AS Notes, cp.SRVR_NME AS ServerName, cp.CREAT_DT AS CreatedDate, 
	ls.STUS_DES AS Status,
	CASE WHEN cp.PRVSN_STUS_CD = 1 THEN 'Yes' ELSE 'No' END AS Needed,
	c.[CUST_SHRT_NME] AS ShortName,
	c.[SUBNET_IP_ADR] AS SubnetIPAddress,
	c.[START_IP_ADR] AS StartIPAddress,
	c.[END_IP_ADR]	AS EndIPAddress,
	c.[QIP_SUBNET_NME] AS QIPSubnetName,
	c.[PRE_SHRD_KEY] AS PreSharedKey,
	ISNULL(CASE WHEN (c.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE c.[COMPNY_NME] END, '') AS CustomerName,
	c.[DEV_CNT] AS NoOfDevices,
	dbo.getCommaSepPrimSite(c.CPT_ID) AS PrimeSite,
	dbo.[getCommaSepCPTPrimaryTprt](c.CPT_ID) AS PrimaryTransport
	FROM dbo.CPT c WITH (NOLOCK) INNER JOIN
	dbo.CPT_PRVSN cp WITH (NOLOCK) ON c.CPT_ID = cp.CPT_ID INNER JOIN
	dbo.LK_CPT_PRVSN_TYPE lcpt WITH (NOLOCK) ON lcpt.CPT_PRVSN_TYPE_ID = cp.CPT_PRVSN_TYPE_ID INNER JOIN
	dbo.LK_STUS ls WITH (NOLOCK) ON ls.STUS_ID = cp.REC_STUS_ID LEFT JOIN
	dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=c.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
	WHERE cp.PRVSN_STUS_CD = 1
	  AND c.CPT_STUS_ID NOT IN (308,309,311)
	  AND c.REC_STUS_ID = 1
END
ELSE
IF ((@CPTID != -1) AND (@Type = -1))
BEGIN
	SELECT cp.CPT_PRVSN_ID AS CPTProvisionID, cp.CPT_ID AS CPTID, lcpt.CPT_PRVSN_TYPE AS ProvisionType,cp.PRVSN_NTE_TXT AS Notes, cp.SRVR_NME AS ServerName, cp.CREAT_DT AS CreatedDate, 
	ls.STUS_DES AS Status,
	CASE WHEN cp.PRVSN_STUS_CD = 1 THEN 'Yes' ELSE 'No' END AS Needed,
	c.[CUST_SHRT_NME] AS ShortName,
	c.[SUBNET_IP_ADR] AS SubnetIPAddress,
	c.[START_IP_ADR] AS StartIPAddress,
	c.[END_IP_ADR]	AS EndIPAddress,
	c.[QIP_SUBNET_NME] AS QIPSubnetName,
	c.[PRE_SHRD_KEY] AS PreSharedKey,
	ISNULL(CASE WHEN (c.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE c.[COMPNY_NME] END, '') AS CustomerName,
	c.[DEV_CNT] AS NoOfDevices,
	dbo.getCommaSepPrimSite(c.CPT_ID) AS PrimeSite,
	dbo.[getCommaSepCPTPrimaryTprt](c.CPT_ID) AS PrimaryTransport
	FROM dbo.CPT c WITH (NOLOCK) INNER JOIN
	dbo.CPT_PRVSN cp WITH (NOLOCK) ON c.CPT_ID = cp.CPT_ID INNER JOIN
	dbo.LK_CPT_PRVSN_TYPE lcpt WITH (NOLOCK) ON lcpt.CPT_PRVSN_TYPE_ID = cp.CPT_PRVSN_TYPE_ID INNER JOIN
	dbo.LK_STUS ls WITH (NOLOCK) ON ls.STUS_ID = cp.REC_STUS_ID LEFT JOIN
	dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=c.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
	WHERE cp.CPT_ID=@CPTID
	  AND c.CPT_STUS_ID NOT IN (308,309,311)
	  AND c.REC_STUS_ID = 1
END
ELSE
IF ((@CPTID = -1) AND (@Type != -1))
BEGIN
	SELECT cp.CPT_PRVSN_ID AS CPTProvisionID, cp.CPT_ID AS CPTID, lcpt.CPT_PRVSN_TYPE AS ProvisionType,cp.PRVSN_NTE_TXT AS Notes, cp.SRVR_NME AS ServerName, cp.CREAT_DT AS CreatedDate, 
	ls.STUS_DES AS Status,
	CASE WHEN cp.PRVSN_STUS_CD = 1 THEN 'Yes' ELSE 'No' END AS Needed,
	c.[CUST_SHRT_NME] AS ShortName,
	c.[SUBNET_IP_ADR] AS SubnetIPAddress,
	c.[START_IP_ADR] AS StartIPAddress,
	c.[END_IP_ADR]	AS EndIPAddress,
	c.[QIP_SUBNET_NME] AS QIPSubnetName,
	c.[PRE_SHRD_KEY] AS PreSharedKey,
	ISNULL(CASE WHEN (c.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE c.[COMPNY_NME] END, '') AS CustomerName,
	c.[DEV_CNT] AS NoOfDevices,
	dbo.getCommaSepPrimSite(c.CPT_ID) AS PrimeSite,
	dbo.[getCommaSepCPTPrimaryTprt](c.CPT_ID) AS PrimaryTransport
	FROM dbo.CPT c WITH (NOLOCK) INNER JOIN
	dbo.CPT_PRVSN cp WITH (NOLOCK) ON c.CPT_ID = cp.CPT_ID INNER JOIN
	dbo.LK_CPT_PRVSN_TYPE lcpt WITH (NOLOCK) ON lcpt.CPT_PRVSN_TYPE_ID = cp.CPT_PRVSN_TYPE_ID INNER JOIN
	dbo.LK_STUS ls WITH (NOLOCK) ON ls.STUS_ID = cp.REC_STUS_ID LEFT JOIN
	dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=c.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
	WHERE cp.CPT_PRVSN_TYPE_ID=@Type
	  AND cp.PRVSN_STUS_CD = 1
	  AND cp.REC_STUS_ID IN (301,304)
	  AND c.REC_STUS_ID = 1
	  AND c.CPT_STUS_ID NOT IN (308,309,311)
END


End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

