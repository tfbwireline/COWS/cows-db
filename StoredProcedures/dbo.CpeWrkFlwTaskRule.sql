USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[CpeWrkFlwTaskRule]    Script Date: 1/18/2022 2:47:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 12/29/2015
-- Description:	Task Rule for CPE workflow.

-- exec [dbo].[CpeWrkFlwTaskRule] 16160,601,0,''

-- =========================================================
ALTER PROCEDURE [dbo].[CpeWrkFlwTaskRule] 
	@ORDR_ID INT,
	@TaskId SMALLINT, 
	@TaskStatus TINYINT , 
	@Comments VARCHAR(1000) = ''
	
AS

BEGIN
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @DROP_SHIP_FLG	bit	= 0 
			,@VOICE_FLG		bit	= 0 
			,@DMSTC_FLG		bit = 0	
			,@INTL_FLG		bit = 0
			,@RECORDS_ONLY  bit	= 0
			,@FULLCANCEL    bit = 0
			,@PARTCANCEL    bit = 0
			,@INSTALL       bit = 0
			,@DISCONNECT    bit = 0
			,@ASGN_ID		int
			,@Nte_txt       varchar(1000)
			,@RELATED_ORDR  int = null
			,@CancelItems   varchar(max) = ''
			,@FTN           varchar(50)
			,@DEVICE_ID     varchar(25)
			,@RTS_ORDR_ID   int = null
			,@PRNT_FTN      varchar (50) = null
			,@CPE_VNDR varchar(50)
			,@SERVICE_ONLY_FLG    bit = 0
			,@NTE_ID INT

			
	 DECLARE	@Device	TABLE
		(DEVICE_ID   varchar(25))
		
	IF EXISTS (SELECT TOP 1 'X' FROM FSA_ORDR fo WITH (NOLOCK)
					INNER JOIN FSA_ORDR_CPE_LINE_ITEM itm WITH (NOLOCK)
						ON fo.ORDR_ID = itm.ORDR_ID AND itm.MFR_NME = 'ZSCA'
				WHERE fo.ORDR_ID = @ORDR_ID 
					AND fo.ORDR_TYPE_CD in ('DC','CN'))
		BEGIN
			IF EXISTS (SELECT 'X' FROM ACT_TASK WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID
							AND (TASK_ID = 600 AND STUS_ID = 0))
				BEGIN
					EXEC dbo.processZsclrDiscCncl @ORDR_ID, 600, 0,'Zscaler Disc/Can Order processed thru EQ Review.'
				END
			RETURN
		END 

--	Setting all the processing flags

	IF EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
						WHERE ORDR_ID = @ORDR_ID AND ISNULL(DROP_SHP,'N') = 'Y')
			BEGIN							
				SET @DROP_SHIP_FLG = 1
				exec dbo.insertOrderNotes @ORDR_ID,9,'Order is Drop-Ship and will auto-process thru COWS workflow.',1
			END
	ELSE 
			SET @DROP_SHIP_FLG = 0
			
	IF EXISTS (SELECT * FROM ORDR WITH (NOLOCK) 
						WHERE PROD_ID in ('UCCH','UCSV','MIPT','UCSM')
							AND @ORDR_ID = ORDR_ID)  
																	
			SET @VOICE_FLG = 1
	ELSE 
			SET @VOICE_FLG = 0
	
	IF EXISTS (SELECT * FROM ORDR WITH (NOLOCK)
						WHERE ORDR_ID = @ORDR_ID AND DMSTC_CD = 0)
		BEGIN				 
			SET @DMSTC_FLG = 1
			SET @INTL_FLG  = 0
		END
	ELSE
		BEGIN
			SET @DMSTC_FLG = 0
			SET @INTL_FLG  = 1
		END
	
	IF EXISTS (SELECT * FROM FSA_ORDR WITH (NOLOCK)
						WHERE ORDR_ID = @ORDR_ID AND ISNULL(CPE_REC_ONLY_CD,'N') = 'Y') 
		SET @RECORDS_ONLY = 1
	ELSE
		SET @RECORDS_ONLY = 0
		
	
	
-- Below logic is to handle line items cancelled while returned to sales and resent with the same
-- Mach5 order ID.	
	
	-- Removed logic below on 9/27/2017 10:42 a.m.  No longer needed because all items are set to 
	-- 402 status when RTS in insertRTS_V5U.
	
	--UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM WITH (ROWLOCK)
	--	SET ITM_STUS = 402, STUS_CD = 'CN'
	--	WHERE  CMPNT_ID IN (SELECT CMPNT_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
	--						WHERE ORDR_ID = @ORDR_ID AND STUS_CD = 'CN')
			--AND ORDR_ID = @ORDR_ID 8/21/2017 changed 1:30 pm
	
	SELECT TOP 1  @RELATED_ORDR = cp.ORDR_ID, @PRNT_FTN = f.PRNT_FTN 
			FROM dbo.FSA_ORDR f WITH (NOLOCK)
			--INNER JOIN FSA_ORDR fo WITH (NOLOCK) ON f.PRNT_FTN = fo.FTN
			INNER JOIN dbo.ORDR o WITH (NOLOCK) ON o.ORDR_ID = f.ORDR_ID AND o.ORDR_CAT_ID = 6 -- added this for the conversion orders
			INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM c WITH (NOLOCK) ON f.ORDR_ID = c.ORDR_ID
			INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cp WITH (NOLOCK) 
				ON  cp.ORDR_CMPNT_ID = c.ORDR_CMPNT_ID 	 
			WHERE f.ORDR_ID = @ORDR_ID
				and cp.ORDR_ID <> @ORDR_ID -- Changed 8/21/2017 1:30 pm
			ORDER by cp.ORDR_ID DESC
	
	
	
	IF EXISTS (SELECT * FROM dbo.FSA_ORDR WITH (NOLOCK)
						WHERE ORDR_ID = @ORDR_ID AND ORDR_TYPE_CD = 'CN')
		BEGIN
			
			IF  NOT EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
							 WHERE @RELATED_ORDR = ORDR_ID AND ITM_STUS = 401
							 AND MDS_DES != 'CPE Order Detail')-- added 12/29/17 dlp0278 
				BEGIN	
											
					SET @FULLCANCEL = 1
				END
			ELSE
				IF EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) 
						 WHERE @RELATED_ORDR = ORDR_ID AND ITM_STUS = 401
						 AND MDS_DES != 'CPE Order Detail')-- added 12/29/17 dlp0278 
					BEGIN
						
						SET @PARTCANCEL = 1
					END 
		END

	IF EXISTS (SELECT * FROM dbo.ORDR WITH (NOLOCK)
						WHERE ORDR_ID = @ORDR_ID AND ORDR_STUS_ID = 4)
		BEGIN 
			SET @FULLCANCEL = 1
		END
	
	IF EXISTS (SELECT * FROM dbo.FSA_ORDR WITH (NOLOCK)
						WHERE ORDR_ID = @ORDR_ID AND ORDR_TYPE_CD = 'IN')
		BEGIN
			SET @INSTALL = 1
		END
	
	IF EXISTS (SELECT * FROM dbo.FSA_ORDR WITH (NOLOCK)
						WHERE ORDR_ID = @ORDR_ID AND ORDR_TYPE_CD = 'DC')
		BEGIN
			SET @DISCONNECT = 1
		END
	
	IF @FULLCANCEL = 0 and @PARTCANCEL = 0
		BEGIN	
			IF NOT EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
						WHERE (EQPT_TYPE_ID in ('MJH','MMH','MMM','MMS','IPH') AND ITM_STUS = 401) AND ORDR_ID = @ORDR_ID)
				SET @SERVICE_ONLY_FLG = 1
			ELSE 
				SET @SERVICE_ONLY_FLG = 0
		END
	ELSE
		BEGIN
			IF @FULLCANCEL = 1 or @PARTCANCEL = 1
				BEGIN
					IF NOT EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
								WHERE (EQPT_TYPE_ID in ('MJH','MMH','MMM','MMS') AND ITM_STUS = 402) AND ORDR_ID = @ORDR_ID)
						SET @SERVICE_ONLY_FLG = 1
					ELSE 
						SET @SERVICE_ONLY_FLG = 0
				END
		
		END
	
-- Task processing

	SELECT  @FTN = FTN FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID
	
	INSERT INTO @Device (DEVICE_ID)
	SELECT DISTINCT DEVICE_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID
	
	SELECT top 1  @RTS_ORDR_ID = fsa.ORDR_ID  
			FROM dbo.FSA_ORDR fsa WITH (NOLOCK)
			INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM li WITH (NOLOCK) ON fsa.ORDR_ID = li.ORDR_ID
			INNER JOIN dbo.ACT_TASK at WITH (NOLOCK) ON at.ORDR_ID = fsa.ORDR_ID
			WHERE fsa.FTN	=	@FTN
				AND li.DEVICE_ID in (SELECT DEVICE_ID from @Device)
				AND (at.TASK_ID = 603 AND at.STUS_ID = 0)
				
				order by fsa.ORDR_ID desc
	
	IF ISNULL(@RTS_ORDR_ID,0) <> 0
		BEGIN
			UPDATE dbo.ACT_TASK WITH (ROWLOCK)
			SET STUS_ID = 2
			WHERE ORDR_ID = @RTS_ORDR_ID  AND (TASK_ID in (603,604) AND STUS_ID = 0)
			
			UPDATE dbo.ORDR_NTE WITH (ROWLOCK)
			SET ORDR_ID = @ORDR_ID
			WHERE ORDR_ID = @RTS_ORDR_ID 
			
			UPDATE dbo.ORDR WITH (ROWLOCK)
			SET ORDR_STUS_ID = 3 --Reject status because orig order RTS
			WHERE ORDR_ID = @RTS_ORDR_ID

			UPDATE dbo.ORDR_JPRDY WITH (ROWLOCK)
			SET ORDR_ID = @ORDR_ID
			WHERE ORDR_ID = @RTS_ORDR_ID			
														
			EXEC dbo.insertOrderNotes @RTS_ORDR_ID,9,'Order status changed to Reject due to order released in Mach5',1

			IF EXISTS (SELECT 'x' from USER_WFM_ASMT WITH (NOLOCK) WHERE ORDR_ID = @RTS_ORDR_ID)
				BEGIN
					UPDATE dbo.USER_WFM_ASMT 
					SET ORDR_ID = @ORDR_ID
					WHERE ORDR_ID = @RTS_ORDR_ID

					EXEC dbo.insertOrderNotes @ORDR_ID,9,'Order returned from Sales.  Order auto-assigned to previous user.',1
				END
			
			UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM WITH (ROWLOCK)
			SET ITM_STUS = 402
			WHERE ORDR_ID =  @RTS_ORDR_ID				
		
		-- Code below to move the purchasing fields from any RTS order to new order.
			DECLARE @iCnt Int = 0,@iCtr Int = 0, @ORDR_CMPNT_ID int, @PLSFT_RQSTN_NBR varchar (15),@RQSTN_DT smalldatetime,
				@PRCH_ORDR_NBR varchar(15), @EQPT_ITM_RCVD_DT smalldatetime,
				@EQPT_RCVD_BY_ADID varchar(10), @RCVD_QTY int,@SUPPLIER varchar(50)
		
			DECLARE  @RTS_Items TABLE (ORDR_CMPNT_ID int, PLSFT_RQSTN_NBR varchar (15),RQSTN_DT smalldatetime,
										PRCH_ORDR_NBR varchar(15), EQPT_ITM_RCVD_DT smalldatetime,
										EQPT_RCVD_BY_ADID varchar(10), RCVD_QTY int, SUPPLIER varchar(50))
			INSERT INTO @RTS_Items
			SELECT DISTINCT ORDR_CMPNT_ID, 
							PLSFT_RQSTN_NBR,
							RQSTN_DT,
							PRCH_ORDR_NBR, 
							EQPT_ITM_RCVD_DT,
							EQPT_RCVD_BY_ADID, 
							RCVD_QTY,SUPPLIER
			 FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
			 WHERE ISNULL(PLSFT_RQSTN_NBR, '') != '' 
				 AND ORDR_CMPNT_ID IN (SELECT ORDR_CMPNT_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) 
										WHERE ORDR_ID = @ORDR_ID)			 
				 -- AND ORDR_ID =  @RTS_ORDR_ID  removed 5/13/19 @ 11:30 
												-- Disconnects with same device id was being selected instead of install orders.
												-- This change moves any component with Mat Req over by Component id.			
			 
			 SET @iCnt = (SELECT COUNT(*) FROM @RTS_Items)
			 
			 IF (@iCnt > 0)
					BEGIN
						WHILE (@iCtr < @iCnt)
							BEGIN
								SELECT TOP 1 @ORDR_CMPNT_ID = ORDR_CMPNT_ID
											,@PLSFT_RQSTN_NBR = PLSFT_RQSTN_NBR
											,@RQSTN_DT = RQSTN_DT
											,@PRCH_ORDR_NBR = PRCH_ORDR_NBR 
											,@EQPT_ITM_RCVD_DT = EQPT_ITM_RCVD_DT
											,@EQPT_RCVD_BY_ADID = EQPT_RCVD_BY_ADID
											,@RCVD_QTY = RCVD_QTY
											,@SUPPLIER = SUPPLIER
								FROM @RTS_Items
								
								UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM
									SET PLSFT_RQSTN_NBR = @PLSFT_RQSTN_NBR,
										RQSTN_DT = @RQSTN_DT,
										PRCH_ORDR_NBR = @PRCH_ORDR_NBR,
										EQPT_ITM_RCVD_DT = @EQPT_ITM_RCVD_DT,
										EQPT_RCVD_BY_ADID = @EQPT_RCVD_BY_ADID,
										RCVD_QTY = @RCVD_QTY,
										SUPPLIER = @SUPPLIER
								WHERE ORDR_CMPNT_ID = @ORDR_CMPNT_ID AND ORDR_ID = @ORDR_ID
							
								DELETE @RTS_Items where ORDR_CMPNT_ID = @ORDR_CMPNT_ID

								SET @iCtr = @iCtr + 1
							END
					END 

		END
		
	DECLARE @ZIP varchar (12)
	DECLARE @DLVY_CLLI varchar (14)
	DECLARE @EQ_CLLI varchar (14)						  
	DECLARE @USER_ID INT

	IF @TaskId = 600   --  Equipment Review Processing
		BEGIN
			
			EXEC dbo.insertBpmAdr_V5U @ORDR_ID
		
			IF @INSTALL = 1 OR @PARTCANCEL = 1 OR @DISCONNECT = 1
					BEGIN
					
						--print 'here1'
						IF (@DROP_SHIP_FLG = 1 OR @VOICE_FLG = 1 OR @SERVICE_ONLY_FLG = 1) -- DS and Voice orders skip ODIE.
							BEGIN
							  EXEC dbo.CompleteActiveTask @ORDR_ID,600,2,'ODIE message not required, system completed Equip Review Task',1,9
							END
						IF @DMSTC_FLG = 1 AND @VOICE_FLG = 0 -- ASSIGN DMSTC USER BY USING ZIP CODE and populate ORDR.DLVY_CLLI
							AND @DROP_SHIP_FLG = 0 and  @SERVICE_ONLY_FLG = 0
							BEGIN
								OPEN SYMMETRIC KEY FS@K3y 
									DECRYPTION BY CERTIFICATE S3cFS@CustInf0
								--print 'Here2'	

								SELECT @ZIP = --LEFT(ZIP_PSTL_CD,5)
								  ISNULL(LEFT(oa.ZIP_PSTL_CD,5), LEFT(dbo.decryptbinarydata(csdA.ZIP_PSTL_CD),5))
								 	FROM dbo.ORDR_ADR oa WITH (NOLOCK)
									 INNER JOIN ORDR ord WITH (NOLOCK) on oa.ORDR_ID = ord.ORDR_ID 
									LEFT JOIN dbo.CUST_SCRD_DATA csdA WITH (NOLOCK) ON csdA.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdA.SCRD_OBJ_TYPE_ID=14
									WHERE oa.ORDR_ID = @ORDR_ID 
											AND oa.ADR_TYPE_ID = 18 
											AND	oa.CIS_LVL_TYPE in ('H6','H5')
								
								IF  EXISTS (SELECT * FROM dbo.LK_SSTAT_ASMT WITH (NOLOCK)
												WHERE @ZIP = Zip_Code)
									BEGIN
							 
										SELECT @DLVY_CLLI = si.[DLVY_CLLI], @USER_ID = lu.USER_ID,
												@CPE_VNDR = si.VENDOR_COMPANY, @EQ_CLLI = si.CLLI_CODE
											FROM dbo.LK_SSTAT_ASMT si WITH (NOLOCK)
											 LEFT OUTER JOIN dbo.LK_USER lu WITH (NOLOCK) ON si.AD_ID = lu.USER_ACF2_ID
											 WHERE @ZIP = si.ZIP_CODE
									END
								
								IF ISNULL(@CPE_VNDR,'') <> '' AND @VOICE_FLG = 0
										AND @DMSTC_FLG = 1
									BEGIN
											UPDATE dbo.FSA_ORDR 
											SET CPE_VNDR = @CPE_VNDR
											WHERE ORDR_ID = @ORDR_ID
											
											SELECT @Nte_txt = 'CPE Vendor Assigned as ' + @CPE_VNDR
												
											exec dbo.insertOrderNotes @ORDR_ID,9,@Nte_txt,1
									END
								
								
									
								IF ISNULL(@DLVY_CLLI,'') <> ''
									BEGIN
										IF @ORDR_ID in (SELECT ORDR_ID from dbo.ORDR WITH (NOLOCK)
														where ORDR_ID = @ORDR_ID AND ORDR_CAT_ID = 6
															AND ISNULL(DLVY_CLLI,'') = '')
											BEGIN 		 						
												IF @CPE_VNDR = 'Goodman'
													BEGIN
														UPDATE dbo.ORDR with (ROWLOCK)
															SET DLVY_CLLI = 'AUSTTX03CPE', CPE_CLLI = @EQ_CLLI
														WHERE ORDR_ID = @ORDR_ID AND ORDR_CAT_ID = 6
														
														SELECT @Nte_txt = 'CPE Vendor is Goodman, Delivery CLLI Defaulted to AUSTTX03CPE.  CPE CLLI auto assigned to:' + @DLVY_CLLI
														
														exec dbo.insertOrderNotes @ORDR_ID,9,@Nte_txt,1
														
													END	
												ELSE		
													BEGIN 						
														UPDATE dbo.ORDR WITH (ROWLOCK)
															SET DLVY_CLLI = @DLVY_CLLI, CPE_CLLI = @EQ_CLLI
														WHERE ORDR_ID = @ORDR_ID AND ORDR_CAT_ID = 6
														
														--UPDATE dbo.FSA_ORDR   moved above 6/13/17 @ 1 p.m.
														--	SET CPE_VNDR = @CPE_VNDR
														--WHERE ORDR_ID = @ORDR_ID
														
														SELECT @Nte_txt = 'Delivery CLLI assigned to:  ' + @DLVY_CLLI + '   and CPE Serving CLLI auto assigned to:  ' + @EQ_CLLI
														
														exec dbo.insertOrderNotes @ORDR_ID,9,@Nte_txt,1
													END
											END
										ELSE 
											BEGIN 
												IF @ORDR_ID in (SELECT ORDR_ID from dbo.ORDR WITH (NOLOCK)
														where ORDR_ID = @ORDR_ID AND ORDR_CAT_ID = 6
															AND ISNULL(CPE_CLLI,'') = '')
													BEGIN			
															 						
														UPDATE dbo.ORDR WITH (ROWLOCK) 
															SET CPE_CLLI = @EQ_CLLI
														WHERE ORDR_ID = @ORDR_ID AND ORDR_CAT_ID = 6
														
														SELECT @Nte_txt = 'CPE Serving CLLI auto assigned to:  ' + @EQ_CLLI
														
														exec dbo.insertOrderNotes @ORDR_ID,9,@Nte_txt,1
													END
											END
										IF ISNULL(@USER_ID,'') <> ''
											AND @ORDR_ID not in (select ORDR_ID from dbo.USER_WFM_ASMT with (NOLOCK)
																	WHERE ORDR_ID = @ORDR_ID)
										 BEGIN
										 
											IF EXISTS -- This is temporary until a few orders are processed.  Assigning Peggy.
												(SELECT 'x' FROM dbo.FSA_ORDR WITH (NOLOCK) where 
															ORDR_ID = @ORDR_ID AND ORDR_SUB_TYPE_CD  IN ('ROCP'))
												BEGIN
													SET @USER_ID = 1405
												END
										 
										 
										   INSERT INTO dbo.USER_WFM_ASMT
												([ORDR_ID],[GRP_ID],[ASN_USER_ID],[ASN_BY_USER_ID],[ASMT_DT]
												,[CREAT_DT],[ORDR_HIGHLIGHT_CD],[ORDR_HIGHLIGHT_MODFD_DT],[USR_PRF_ID])
											VALUES
												(@ORDR_ID,13,@USER_ID,1,GETDATE(),GETDATE(),null,null,98)
												
											exec dbo.insertOrderNotes @ORDR_ID,9,'User auto assigned.',@USER_ID
										 END
										 ELSE
											IF ISNULL(@USER_ID,'') <> ''
											 BEGIN
												exec dbo.insertOrderNotes @ORDR_ID,9,'No user found to auto assign.',1
											 END
									END	
								ELSE
									exec dbo.insertOrderNotes @ORDR_ID,9,'Zip not found in SSTAT for auto assignment.',1
							 
							END	
						IF @PARTCANCEL = 1  --Send Email to PeopleSoft
								BEGIN
									IF EXISTS (SELECT * FROM dbo.PS_REQ_LINE_ITM_QUEUE WITH (NOLOCK)
												WHERE CMPNT_ID IN
												(SELECT ORDR_CMPNT_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
													WHERE ORDR_ID = @ORDR_ID) )
										BEGIN
											DECLARE @ReqLineNbrs TABLE (REQSTN_NBR varchar (10),CMPNT_ID int, REQ_LINE_NBR int)
											DECLARE @Req     varchar(50)
											DECLARE @Line    int
											
											
											INSERT INTO @ReqLineNbrs (REQSTN_NBR,CMPNT_ID, REQ_LINE_NBR)
											 SELECT REQSTN_NBR, CMPNT_ID, REQ_LINE_NBR FROM dbo.PS_REQ_LINE_ITM_QUEUE WITH (NOLOCK)
												WHERE CMPNT_ID IN (SELECT ORDR_CMPNT_ID FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
												                     WHERE ORDR_ID = @ORDR_ID)   
										END

										
										WHILE EXISTS (SELECT * FROM @ReqLineNbrs)
											BEGIN
												 SELECT TOP 1 @Req = REQSTN_NBR  FROM @ReqLineNbrs
												 SELECT TOP 1 @Line = REQ_LINE_NBR FROM @ReqLineNbrs
												
												SET @CancelItems = @CancelItems + (@Req + '/' + CONVERT(varchar(5), @Line)) + ' *** '											
													
													
												DELETE @ReqLineNbrs WHERE REQSTN_NBR = @Req AND REQ_LINE_NBR = @Line									
											END
								
										IF (ISNULL(@CancelItems,'') <> '')
											BEGIN
												INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
											(ORDR_ID,EMAIL_REQ_TYPE_ID,STUS_ID,EMAIL_LIST_TXT,EMAIL_CC_TXT,EMAIL_SUBJ_TXT,EMAIL_BODY_TXT)
											VALUES	(@ORDR_ID,34,10,'Peggy.Wheaton@sprint.com,Karen.J.Vicchiarelli@sprint.com,intlcpe@sprint.com',
											' ',
												'Partial Order Cancellation',
												'The following Req Number/Req Line Number(s) have been cancelled:  ' + @CancelItems 
												+ '  M5 Order ID:  ' + @FTN)
											END	
											
										IF EXISTS (SELECT * FROM dbo.SSTAT_REQ WITH (NOLOCK)
											WHERE ORDR_ID = @RELATED_ORDR AND SSTAT_MSG_ID = 1)
											BEGIN
												INSERT INTO [dbo].[SSTAT_REQ]
													([ORDR_ID],[SSTAT_MSG_ID],[STUS_MOD_DT],[CREAT_DT],[STUS_ID],[FTN])
												VALUES
													(@ORDR_ID,1 ,null,GETDATE(),10,(SELECT FTN from FSA_ORDR WHERE ORDR_ID = @ORDR_ID))	
											END	
								END 
							
						END				 
				 
			IF @FULLCANCEL = 1
					BEGIN
						DECLARE @origOrdr_ID int
						DECLARE @origFTN   varchar (50)
									
						
						EXEC dbo.CompleteActiveTask @ORDR_ID,600,2,'System Completed Equip Review Task',1,9
						DECLARE @OrigOrdrTbl TABLE (ID INT IDENTITY(1,1), OrigOrdrID INT, OrigFTN VARCHAR(50), Flag BIT)
						SET @iCtr=1
						SET @iCnt=0
						INSERT INTO @OrigOrdrTbl (OrigOrdrID, OrigFTN, Flag)
						SELECT DISTINCT oli.ORDR_ID, o.FTN, 0
								from dbo.FSA_ORDR f WITH (NOLOCK)
								INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM li WITH (NOLOCK) ON li.ORDR_ID = f.ORDR_ID
								INNER JOIN dbo.FSA_ORDR o WITH (NOLOCK) ON o.FTN = f.PRNT_FTN
								INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM oli WITH (NOLOCK) ON o.ORDR_ID = oli.ORDR_ID
																	AND li.ORDR_CMPNT_ID = oli.ORDR_CMPNT_ID
								WHERE f.ORDR_ID = @ORDR_ID
								ORDER by oli.ORDR_ID desc

						SELECT @iCnt = COUNT(1) FROM @OrigOrdrTbl

						WHILE (@iCtr<=@iCnt)
						BEGIN

						SELECT top 1 @origOrdr_ID = OrigOrdrID, @origFTN = OrigFTN 
						FROM @OrigOrdrTbl
						WHERE ID=@iCtr
						AND Flag=0
						
						UPDATE dbo.ORDR WITH (ROWLOCK)
							SET ORDR_STUS_ID = 4
						WHERE ORDR_ID = @origOrdr_ID
						
						SELECT @Nte_txt = 'Order cancelled because full cancel received from Mach5: ' + @FTN
						exec dbo.insertOrderNotes @origOrdr_ID,9,@Nte_txt,1
						
						UPDATE dbo.ACT_TASK WITH (ROWLOCK)
							set STUS_ID = 2
						WHERE ORDR_ID =  @origOrdr_ID  and TASK_ID in (600,601,602,604,603,1000)
						
						DELETE BPM_ORDR_ADR WHERE ORDR_ID = @origOrdr_ID
						
						-- Send Email to Peoplesoft if original Order completed Material Req task	
						IF EXISTS (SELECT * FROM dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK)
									WHERE ORDR_ID = @origOrdr_ID) 														
						   BEGIN
								DECLARE @reqNbr    Varchar (10)
								DECLARE @LK_MatReq table (PLSFT_RQSTN_NBR VARCHAR(10))
								INSERT INTO @LK_MatReq (PLSFT_RQSTN_NBR) 
								 (SELECT DISTINCT f.PLSFT_RQSTN_NBR
										 FROM dbo.FSA_ORDR_CPE_LINE_ITEM f WITH (NOLOCK)
									WHERE ISNULL(f.PLSFT_RQSTN_NBR,'') <> '' 
										AND f.ORDR_ID = @origOrdr_ID)
										
								WHILE EXISTS (SELECT * FROM @LK_MatReq)
									BEGIN
										SELECT TOP 1 @reqNbr = PLSFT_RQSTN_NBR FROM @LK_MatReq
										
										INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
											(ORDR_ID,EMAIL_REQ_TYPE_ID,STUS_ID,EMAIL_LIST_TXT,EMAIL_CC_TXT,EMAIL_SUBJ_TXT,EMAIL_BODY_TXT)
										VALUES	(@ORDR_ID,34,10,'Peggy.Wheaton@sprint.com,Karen.J.Vicchiarelli@sprint.com,intlcpe@sprint.com',
										' ',
											'PR# ' +  @reqNbr +' Order Cancellation',
											'Order Cancellation of ' + @reqNbr + 
											'        M5 Order IDs:  ' + @PRNT_FTN + ' / ' + @FTN)
									
										DELETE @LK_MatReq WHERE @reqNbr = PLSFT_RQSTN_NBR
									END
						   END
						   
						   -- Send cancel status to ODIE if original order has completed Equip Review task
						   IF EXISTS (SELECT * FROM ODIE_REQ WITH (NOLOCK)
												WHERE ORDR_ID = @origOrdr_ID)
								BEGIN
									DECLARE @deviceID    Varchar (25)
									DECLARE @LK_Device table (DEVICE_ID VARCHAR(25))
									INSERT INTO @LK_Device (DEVICE_ID) 
									 (SELECT DISTINCT f.DEVICE_ID
											 FROM dbo.FSA_ORDR_CPE_LINE_ITEM f WITH (NOLOCK)
										WHERE ISNULL(f.DEVICE_ID,'') <> '' 
											AND f.ORDR_ID = @origOrdr_ID)
											
									WHILE EXISTS (SELECT * FROM @LK_Device)
										BEGIN
											SELECT TOP 1 @deviceID = DEVICE_ID FROM @LK_Device
										
										INSERT INTO [COWS].[dbo].[ODIE_REQ]
										   ([ORDR_ID],[ODIE_MSG_ID],[STUS_MOD_DT],[H1_CUST_ID],[CREAT_DT],[STUS_ID],[MDS_EVENT_ID]
										   ,[TAB_SEQ_NBR],[CUST_NME],[ODIE_CUST_ID],[CPT_ID],[ODIE_CUST_INFO_REQ_CAT_TYPE_ID]
										   ,[DEV_FLTR])
										VALUES
										   (@origOrdr_ID,6,GETDATE(),null,GETDATE(),10,null,null,null,null,null,null,@deviceID)

										EXEC dbo.insertOrderNotes @origOrdr_ID,9,'Order Information successfully sent to ODIE.',1

											DELETE @LK_Device WHERE @deviceID = DEVICE_ID
										END	
								END
								
							-- Send cancel status to SSTAT if original order has completed Equip Receipts		
							IF EXISTS (SELECT * FROM dbo.SSTAT_REQ WITH (NOLOCK)
										WHERE ORDR_ID = @origOrdr_ID )
								BEGIN
									INSERT INTO [dbo].[SSTAT_REQ]
										([ORDR_ID],[SSTAT_MSG_ID],[STUS_MOD_DT],[CREAT_DT],[STUS_ID],[FTN])
									VALUES
										(@origOrdr_ID,2 ,null,GETDATE(),10,@origFTN)	
					
								END	
						UPDATE @OrigOrdrTbl SET Flag=1 WHERE ID=@iCtr AND Flag=0
						SET @iCtr=@iCtr+1
					 END
					 EXEC dbo.CompleteActiveTask @ORDR_ID,604,2,'Completed Tech Assign Task',1,9
					 EXEC dbo.CompleteActiveTask @ORDR_ID,1000,2,'System Completed BAR-Pending Task',1,9
					 
					 DELETE BPM_ORDR_ADR WHERE ORDR_ID = @ORDR_ID
			END
		END
	ELSE IF @TaskId = 601 -- Material Requisitioning
		BEGIN
			DECLARE @SMRT_ACCT_DOMN_TXT Varchar(100),
							@VRTL_ACCT_DOMN_TXT Varchar(100),
							@Contract_Type      VARCHAR(20),
							@NOTE VARCHAR (450) = ''

			IF (@RECORDS_ONLY = 1 OR @SERVICE_ONLY_FLG = 1) -- Records Only orders skip Matl Req.
				BEGIN
				  EXEC dbo.CompleteActiveTask @ORDR_ID,601,2,'No Mat Req required, system completed Mat Req Task.',1,9
				END
			ELSE IF @DROP_SHIP_FLG = 1 -- DS sends Mat Req and moves thru task
				BEGIN
				 
				  EXEC dbo.insertMatlReqDropShip_V5U @ORDR_ID
				  EXEC dbo.CompleteActiveTask @ORDR_ID,601,2,'System Completed Mat Req Task for Drop-Ship.',1,9	
				END		
				
			IF NOT EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) 
							WHERE ORDR_ID = @ORDR_ID 
							AND (ISNULL(CPE_REUSE_CD,0) = 0  AND EQPT_TYPE_ID <> 'SVC'))
				BEGIN 
					EXEC dbo.CompleteActiveTask @ORDR_ID,601,2,'System Completed Mat Req Task for Reuse Order',1,9
				END 

			IF EXISTS (SELECT 'X' FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) WHERE MDS_DES like '%Meraki%' 
																	AND ORDR_ID = @ORDR_ID
																	AND ISNULL(CPE_REUSE_CD,0) = 0 
																	AND ISNULL(DROP_SHP,'N') = 'N')
				BEGIN
					DECLARE @CnctNme Varchar(100),
							@CnctEmail Varchar(150),
							@CnctTn Varchar(25),
							
							@IPMEmail Varchar(150)
							
					SELECT @NOTE = '' 

					SELECT @CnctNme = ISNULL(oc.CNTCT_NME,DECRYPTBYKEY(csd.CUST_CNTCT_NME)),
						   @CnctEmail = REPLACE(ISNULL(oc.EMAIL_ADR,DECRYPTBYKEY(csd.CUST_EMAIL_ADR)),'@', 'AT'),
						   @CnctTn = oc.NPA + oc.NXX + oc.STN_NBR 
						FROM dbo.ORDR_CNTCT oc WITH (NOLOCK)
							LEFT OUTER JOIN dbo.CUST_SCRD_DATA csd WITH (NOLOCK) on csd.SCRD_OBJ_ID = oc.ORDR_CNTCT_ID
										AND CNTCT_TYPE_ID = 1 AND ROLE_ID = 94						
						WHERE  oc.CNTCT_TYPE_ID = 1 AND oc.ROLE_ID = 94 AND oc.ORDR_ID = @ORDR_ID
								AND CIS_LVL_TYPE = 'OD' -- changed PJ025845 from 'H6' 
				
					SELECT @IPMEmail = REPLACE(oc.EMAIL_ADR,'@', 'AT')
						FROM dbo.ORDR_CNTCT oc WITH (NOLOCK)
						WHERE  oc.CNTCT_TYPE_ID = 1 AND oc.ROLE_ID = 99 AND oc.ORDR_ID = @ORDR_ID
							

				
					SELECT @NOTE = 'Send Meraki License Key to ' + @IPMEmail + ' and ' + 'IPServicesVendorSupportATsprint.com.  '
						+ 'Contact Info.   ' + RTRIM(@CnctNme) + '  ' +  RTRIM(@CnctEmail) + '   ' + RTRIM(@CnctTn)
						
					EXEC dbo.insertOrderNotes @ORDR_ID, 27, @NOTE, 1

				END
			 ELSE IF EXISTS (SELECT 'X' FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) WHERE MFR_NME = 'CSCO' AND MDS_DES not like '%Meraki%' 
																	AND ISNULL(CPE_REUSE_CD,0) = 0 AND ORDR_ID = @ORDR_ID)
				BEGIN
					SELECT @NOTE = ''
					
					IF EXISTS (SELECT 'X' FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) WHERE MFR_NME = 'CSCO' AND MDS_DES not like '%Meraki%' 
																	AND ISNULL(CPE_REUSE_CD,0) = 0 AND ORDR_ID = @ORDR_ID
																	AND ISNULL(DROP_SHP,'N') = 'N'
																	AND ISNULL(SMRT_ACCT_DOMN_TXT,'') <> ''	
																	--AND CNTRC_TYPE_ID IN ('PRCH','INST')
																	)	
						BEGIN	
								
								SELECT TOP 1 @SMRT_ACCT_DOMN_TXT = SMRT_ACCT_DOMN_TXT,
											 @VRTL_ACCT_DOMN_TXT = VRTL_ACCT_DOMN_TXT
									FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
											WHERE MFR_NME = 'CSCO' AND MDS_DES not like '%Meraki%' 
													AND ISNULL(CPE_REUSE_CD,0) = 0 AND ORDR_ID = @ORDR_ID
													AND ISNULL(DROP_SHP,'N') = 'N'	AND ISNULL(SMRT_ACCT_DOMN_TXT,'') <> ''	
													--AND CNTRC_TYPE_ID IN ('PRCH','INST')											
				
								SELECT @NOTE = 'Smart Account Domain Identifier  ' + @SMRT_ACCT_DOMN_TXT + 
										',     Virtual account  ' + @VRTL_ACCT_DOMN_TXT
						
								EXEC dbo.insertOrderNotes @ORDR_ID, 27, @NOTE, 1
						END
					-- Removed per Peggy's request 6/18/2020  populate when SMART Account is given in above code.
					--IF EXISTS (SELECT 'X' FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) WHERE MFR_NME = 'CSCO' AND MDS_DES not like '%Meraki%' 
					--												AND ISNULL(CPE_REUSE_CD,0) = 0 AND ORDR_ID = @ORDR_ID
					--												AND ISNULL(DROP_SHP,'N') = 'N'
					--												AND CNTRC_TYPE_ID IN ('RNTL','LEAS'))	
					--	BEGIN	
								
					--			SELECT TOP 1 @SMRT_ACCT_DOMN_TXT = SMRT_ACCT_DOMN_TXT,
					--						 @VRTL_ACCT_DOMN_TXT = VRTL_ACCT_DOMN_TXT
					--				FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
					--						WHERE MFR_NME = 'CSCO' AND MDS_DES not like '%Meraki%' 
					--								AND ISNULL(CPE_REUSE_CD,0) = 0 AND ORDR_ID = @ORDR_ID
					--								AND ISNULL(DROP_SHP,'N') = 'N'	AND ISNULL(SMRT_ACCT_DOMN_TXT,'') <> '' 
					--								AND CNTRC_TYPE_ID IN ('RNTL','LEAS')												
				
					--			SELECT @NOTE = @NOTE + '   Customer Rental of Router or Switch/Sprint Managed.    Smart Account Domain Identifier  ' + @SMRT_ACCT_DOMN_TXT + 
					--					', Virtual account  ' + @VRTL_ACCT_DOMN_TXT
						
					--			EXEC dbo.insertOrderNotes @ORDR_ID, 27, @NOTE, 1
					--	END
				END

			IF EXISTS (SELECT 'X' FROM ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID AND PROD_ID in ('SUPPB','SUPPT'))
				BEGIN
					
					SELECT @NOTE =  @NOTE + '   SYNNEX Note   SHIP TO INFO.  Sprint MNS Pre-Configuration   ATTN. Kelly Tatro   851 Trafalgar Ct. Suite 114    Maitland FL 32751   Phone 3212809851' 
					
					EXEC dbo.insertOrderNotes @ORDR_ID, 27, @NOTE, 1
				END
		
		
		END
	ELSE IF @TaskId = 602 -- Equip Receipts
		BEGIN
			IF (@RECORDS_ONLY = 1 OR @DROP_SHIP_FLG = 1 OR @INTL_FLG = 1 OR @SERVICE_ONLY_FLG = 1) 
			   BEGIN
				  EXEC dbo.CompleteActiveTask @ORDR_ID,602,2,'No receipt required, system completed Equip Receipts Task.',1,9
			   END  
			IF NOT EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK) 
							WHERE ORDR_ID = @ORDR_ID AND ISNULL(CPE_REUSE_CD,0) = 0 AND EQPT_TYPE_ID <> 'SVC')
				BEGIN 
					EXEC dbo.CompleteActiveTask @ORDR_ID,602,2,'System Completed Equip Receipts Task for Reuse Order',1,9
				END 
			--IF @DMSTC_FLG = 1 AND @VOICE_FLG = 0 AND @DROP_SHIP_FLG = 0	
			--	BEGIN
					
			--		SELECT @CPE_VNDR = ISNULL(VENDOR_COMPANY,'') FROM dbo.LK_SSTAT_ASMT s WITH (NOLOCK)
			--			INNER JOIN dbo.ORDR o WITH (NOLOCK) ON s.CLLI_Code = o.CPE_CLLI
			--			where o.ORDR_ID = @ORDR_ID
						
			--		 UPDATE FSA_ORDR
			--			SET CPE_VNDR = ISNULL(@CPE_VNDR,'')
			--		 WHERE ORDR_ID = @ORDR_ID
			--   END
		
		END	
	ELSE IF @TaskId = 604 -- TECH Assignment for 3rd Party orders.
		BEGIN
			IF EXISTS 
				(SELECT 'X' FROM dbo.FSA_ORDR WITH (NOLOCK) where ORDR_ID = @ORDR_ID AND
									 ORDR_SUB_TYPE_CD  IN ('ISMV'))
				 BEGIN
					OPEN SYMMETRIC KEY FS@K3y 
									DECRYPTION BY CERTIFICATE S3cFS@CustInf0
								
					SELECT @ZIP = --LEFT(ZIP_PSTL_CD,5)
								ISNULL(LEFT(oa.ZIP_PSTL_CD,5), LEFT(dbo.decryptbinarydata(csdA.ZIP_PSTL_CD),5))
								 	FROM dbo.ORDR_ADR oa WITH (NOLOCK)
									 INNER JOIN ORDR ord WITH (NOLOCK) on oa.ORDR_ID = ord.ORDR_ID 
									LEFT JOIN dbo.CUST_SCRD_DATA csdA WITH (NOLOCK) ON csdA.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdA.SCRD_OBJ_TYPE_ID=14
									WHERE oa.ORDR_ID = @ORDR_ID 
											AND oa.ADR_TYPE_ID = 18 
											AND	oa.CIS_LVL_TYPE in ('H6','H5')
					
					IF  EXISTS (SELECT * FROM dbo.LK_SSTAT_ASMT WITH (NOLOCK) WHERE @ZIP = Zip_Code)
						BEGIN
				 
							SELECT @DLVY_CLLI = si.[DLVY_CLLI], @USER_ID = lu.USER_ID,
							@CPE_VNDR = VENDOR_COMPANY, @EQ_CLLI = CLLI_CODE
								FROM LK_SSTAT_ASMT si 
								 LEFT OUTER JOIN LK_USER lu WITH (NOLOCK) ON si.AD_ID = lu.USER_ACF2_ID
								 WHERE @ZIP = si.ZIP_CODE
						END
					IF ISNULL(@DLVY_CLLI,'') <> ''
						BEGIN
							IF @ORDR_ID in (SELECT ORDR_ID from dbo.ORDR WITH (NOLOCK)
											where ORDR_ID = @ORDR_ID AND ORDR_CAT_ID = 6
												AND ISNULL(DLVY_CLLI,'') = '')
								BEGIN 		 						
									UPDATE dbo.ORDR WITH (ROWLOCK) 
										SET DLVY_CLLI = @DLVY_CLLI, CPE_CLLI = @EQ_CLLI
									WHERE ORDR_ID = @ORDR_ID AND ORDR_CAT_ID = 6
									
									UPDATE FSA_ORDR 
									SET CPE_VNDR = @CPE_VNDR
									WHERE ORDR_ID = @ORDR_ID
									
									SELECT @Nte_txt = 'Delivery CLLI assigned to: ' + @DLVY_CLLI + '   and CPE Serving CLLI auto assigned to:  ' + @EQ_CLLI
									
									exec dbo.insertOrderNotes @ORDR_ID,9,@Nte_txt,1
								END
						END
						
					--IF @DMSTC_FLG = 1 AND @VOICE_FLG = 0 AND @DROP_SHIP_FLG = 0 	
					--	BEGIN
					--		SELECT @CPE_VNDR = ISNULL(VENDOR_COMPANY,'') FROM dbo.V_V5U_SSTAT_INFO s WITH (NOLOCK)
					--			INNER JOIN ORDR o WITH (NOLOCK) ON s.CLLI_Code = o.CPE_CLLI
					--			where o.ORDR_ID = @ORDR_ID
								
					--		 UPDATE dbo.FSA_ORDR WITH (ROWLOCK)
					--			SET CPE_VNDR = ISNULL(@CPE_VNDR,'')
					--		 WHERE ORDR_ID = @ORDR_ID
					--	END	
				
				END
				
			IF (@VOICE_FLG = 1 OR @DROP_SHIP_FLG = 1 OR @INTL_FLG  = 1 OR @SERVICE_ONLY_FLG = 1)
	
				BEGIN
					EXEC dbo.CompleteActiveTask @ORDR_ID,604,2,'No dispatch required, system completed Tech Assign Task.',1,9
				END
									
			ELSE IF EXISTS (select 'X' from dbo.FSA_ORDR WITH (NOLOCK) where ORDR_ID = @ORDR_ID
										AND CPE_VNDR in ('Ericsson','Sprint'))
					
					BEGIN
						IF EXISTS (SELECT 'X' FROM FSA_ORDR WITH (NOLOCK) 
										WHERE ORDR_ID = @ORDR_ID AND SUBSTRING(FTN,1,2) = 'DC' )
							AND NOT EXISTS (SELECT 'X' FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
												WHERE ORDR_ID = @ORDR_ID AND CNTRC_TYPE_ID = 'RNTL'
												AND EQPT_TYPE_ID = 'MJH' )
								BEGIN
									EXEC dbo.insertOrderNotes @ORDR_ID,26,'No Rental Equipment found on order.  Did not send to SSTAT.',1
									
									

									SELECT TOP 1 @NTE_ID = NTE_ID FROM dbo.ORDR_NTE WITH (NOLOCK) 
									WHERE ORDR_ID = @ORDR_ID AND NTE_TYPE_ID = 26 ORDER BY CREAT_DT DESC
								
									INSERT INTO [dbo].[ORDR_JPRDY]
									  ([ORDR_ID],[JPRDY_CD],[NTE_ID],[CREAT_DT])
									 VALUES
										   (@ORDR_ID,'Z27',@NTE_ID,GETDATE())
								END

					 				  
						 ELSE  IF NOT EXISTS (SELECT 'X' FROM dbo.SSTAT_REQ s WITH (NOLOCK) 
										WHERE @ORDR_ID = ORDR_ID AND SSTAT_MSG_ID = 1
												AND STUS_ID IN (10,11))
							BEGIN
								IF NOT EXISTS (SELECT 'X' FROM dbo.ORDR WITH (NOLOCK)
										WHERE ORDR_ID = @ORDR_ID 
										AND DLVY_CLLI  IN ('MTLDFLTBACP','DLLSTX53ACP','OVPKKSTDACP','RVSDMO09ACP'))
									BEGIN
										INSERT INTO [dbo].[SSTAT_REQ]
											([ORDR_ID],[SSTAT_MSG_ID],[STUS_MOD_DT],[CREAT_DT],[STUS_ID],[FTN])
										VALUES
											(@ORDR_ID,1 ,null,GETDATE(),10,@FTN)
		  
									IF EXISTS (SELECT 'X' FROM FSA_ORDR WITH (NOLOCK) 
											WHERE ORDR_ID = @ORDR_ID AND SUBSTRING(FTN,1,2) = 'DC' )
											AND EXISTS (SELECT 'X' FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
												WHERE ORDR_ID = @ORDR_ID AND CNTRC_TYPE_ID = 'RNTL'
												AND EQPT_TYPE_ID = 'MJH' )

												BEGIN 
													EXEC dbo.insertOrderNotes @ORDR_ID,26,'Only disconnected rental items sent to SSTAT.',1
												END			
																									
									END

							
					
															   
								ELSE IF EXISTS (SELECT 'X' FROM dbo.ORDR WITH (NOLOCK)
										WHERE ORDR_ID = @ORDR_ID 
										AND DLVY_CLLI  IN ('MTLDFLTBACP','DLLSTX53ACP','OVPKKSTDACP','RVSDMO09ACP'))
											BEGIN
												EXEC dbo.insertOrderNotes @ORDR_ID,26,'Pre-Staged order.  Auto completion of Tech Assignments.',1
											END	   																					  
							END
	 
						EXEC dbo.CompleteActiveTask @ORDR_ID,604,2,'Complete Tech Assign Task',1,9	

					  
				   END 
				   
			ELSE IF EXISTS  (select 'X' from dbo.FSA_ORDR WITH (NOLOCK) where ORDR_ID = @ORDR_ID
										AND CPE_VNDR = 'Goodman')
					AND EXISTS (SELECT 'X' FROM FSA_ORDR WITH (NOLOCK) 
										WHERE ORDR_ID = @ORDR_ID AND SUBSTRING(FTN,1,2) = 'DC' )
					AND NOT EXISTS (SELECT 'X' FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
												WHERE ORDR_ID = @ORDR_ID AND CNTRC_TYPE_ID = 'RNTL'
												AND EQPT_TYPE_ID = 'MJH' )
				   BEGIN

						EXEC dbo.CompleteActiveTask @ORDR_ID,604,2,'No rental equipment found on this Goodman order.  Removed Goodman as the Vendor.',1,9
									
						EXEC dbo.insertOrderNotes @ORDR_ID,26,'No rental equipment found on this Goodman order.  Removed Goodman as the Vendor.',1
						
						SELECT TOP 1 @NTE_ID = NTE_ID FROM dbo.ORDR_NTE WITH (NOLOCK) 
								WHERE ORDR_ID = @ORDR_ID AND NTE_TYPE_ID = 26 ORDER BY CREAT_DT DESC
								
						INSERT INTO [dbo].[ORDR_JPRDY]
							([ORDR_ID],[JPRDY_CD],[NTE_ID],[CREAT_DT])
							VALUES
								(@ORDR_ID,'Z27',@NTE_ID,GETDATE())
						
						UPDATE FSA_ORDR
							SET CPE_VNDR = NULL
							WHERE ORDR_ID = @ORDR_ID
				   
				   END

			
			-- Move order which needs IPM to coordinate Shipping of Equipment

			IF @DMSTC_FLG = 1 AND EXISTS (SELECT 'x'  FROM FSA_ORDR WITH (NOLOCK)
												WHERE ORDR_ID = @ORDR_ID 
													AND SUBSTRING(FTN,1,2) = 'CH' 
													AND ORDR_TYPE_CD = 'DC')

				BEGIN
					
					SELECT @FTN = FTN from FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID

					Declare @ICLLI varchar(20) = (SELECT TOP 1 isnuLL(CPE_CLLI,'') FROM ORDR o WITH (NOLOCK)
									INNER JOIN FSA_ORDR f WITH (NOLOCK) ON o.ORDR_ID = f.ORDR_ID
									WHERE f.FTN = @FTN and f.ORDR_TYPE_CD = 'IN')
					Declare @DCLLI varchar(20) = (SELECT TOP 1 isnuLL(CPE_CLLI,'') FROM ORDR o WITH (NOLOCK)
									INNER JOIN FSA_ORDR f WITH (NOLOCK) ON o.ORDR_ID = f.ORDR_ID
									WHERE f.FTN = @FTN and f.ORDR_TYPE_CD = 'DC')

					IF @DCLLI <> '' AND @DCLLI <> @ICLLI
							BEGIN
								
								EXEC dbo.insertOrderNotes @ORDR_ID,26,'Email sent to IPM to coordinate shipping.',1

								DECLARE @IPM VARCHAR(max), @IS VARCHAR(1000), @Subject varchar(1000),@Body varchar(max), @IPMH1 VARCHAR(max)
								
								SET @Subject = @FTN + '– Coordinate CPE Shipping'
								SET @IPM = (SELECT TOP 1 EMAIL_ADR FROM dbo.ORDR_CNTCT with (nolock)
												WHERE CIS_LVL_TYPE = 'H6' and ORDR_ID = @ORDR_ID
													AND ROLE_ID = 99)
								SET @IS = (SELECT TOP 1 EMAIL_ADR FROM dbo.ORDR_CNTCT with (nolock)
												WHERE CIS_LVL_TYPE = 'H6' and ORDR_ID = @ORDR_ID
													AND ROLE_ID = 101)
								SET @IPMH1 = (SELECT TOP 1 EMAIL_ADR FROM dbo.ORDR_CNTCT with (nolock)
												WHERE CIS_LVL_TYPE = 'H1' and ORDR_ID = @ORDR_ID
													AND ROLE_ID = 99)

								SET @Body = 'CPE move activity for this order requires the coordination of shipping the customer premise equipment between CPE regions.  Please arrange shipment and advise the CPE-Provisioning (PDL) once arrangements have been made.'


								INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
									(ORDR_ID,EMAIL_REQ_TYPE_ID,STUS_ID,EMAIL_LIST_TXT,EMAIL_CC_TXT ,EMAIL_SUBJ_TXT,EMAIL_BODY_TXT)
								VALUES	(@ORDR_ID,43,10,ISNULL(@IPM,@IPMH1),@IS,@Subject,@Body)
							
							
							
							END	
									
				END
				

		END		

END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH

END

