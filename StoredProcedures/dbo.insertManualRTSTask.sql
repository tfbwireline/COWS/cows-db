USE [COWS]
GO
_CreateObject 'SP','dbo','insertManualRTSTask'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 10/01/2011
-- Description:	Inserts the RTS Task manually outside the SM.
-- =========================================================
ALTER PROCEDURE [dbo].[insertManualRTSTask]
	@OrderID		Int	
AS
BEGIN
SET NOCOUNT ON;

Begin Try
	IF	EXISTS
		(SELECT 'X' FROM dbo.ACT_TASK	WITH (NOLOCK)
			WHERE	ORDR_ID	=	@OrderID
				AND	TASK_ID	=	500)
		BEGIN
			UPDATE		dbo.ACT_TASK	WITH (ROWLOCK)
				SET		STUS_ID		=	0,
						WG_PROF_ID	=	0,
						MODFD_DT = GETDATE()
				WHERE	ORDR_ID		=	@OrderID
					AND	TASK_ID		=	500
					AND STUS_ID		<>	0
		END		
	ELSE
		BEGIN
			INSERT INTO dbo.ACT_TASK	WITH (ROWLOCK)
						(ORDR_ID	,TASK_ID	,STUS_ID	,WG_PROF_ID)
				VALUES	(@OrderID	,500		,0			,0)	
		END
	
	---------------------------------------------------------------
	-- Call the Taskrule to send Email Notification.
	---------------------------------------------------------------
	EXEC	dbo.loadRTSEmailNotification @OrderID ,500 ,0 ,''
End Try

Begin Catch
	EXEC dbo.insertErrorInfo
End Catch
END
GO