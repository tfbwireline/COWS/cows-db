USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getEmailRequests]    Script Date: 07/22/2021 10:04:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		sbg9814
-- Create date: 08/16/2011
-- Description:	Get all Email Requests to be sent out.
-- Updated by:	qi931353
-- Updated Date:08/09/2017
-- Description:	Added SDE Opportunity Email
-- =============================================
ALTER PROCEDURE [dbo].[getEmailRequests] 
AS
BEGIN
SET NOCOUNT ON
Begin Try

DECLARE @Email	TABLE
			(EMAIL_REQ_ID	INT
			,ORDR_ID			Int
			,EMAIL_REQ_TYPE		VARCHAR(50)
			,EMAIL_REQ_TYPE_ID	Int
			,REL_ORDR_ID		Int
			,ATC_EMAIL			Varchar(1000)	NULL
			,FULL_NME			Varchar(100)	NULL
			,H1_CUST_ID			Int				NULL	
			,H5_H6_CUST_ID		Int				NULL
			,FTN				varchar(20)		NULL
			,MDS_CD				Bit				
			,CUST_NME			Varchar(50)		NULL
			,CUST_CMMT_DT		Varchar(25)		NULL
			,H5_H6_ADR			Varchar(1000)	NULL
			,DCD				Varchar(25)		NULL
			,FSA_ORDR_TYPE_DES	Varchar(25)		NULL
			,ISIPL				BIT
			,PRIM_TAC_EMAIL		Varchar (50)	NULL)
			
IF	EXISTS
	(SELECT 'X' FROM dbo.EMAIL_REQ	WITH (NOLOCK)
		WHERE	ORDR_ID	IS	NOT NULL
			AND	STUS_ID	=	10)
	BEGIN

		INSERT INTO @Email	(EMAIL_REQ_ID, ORDR_ID,	EMAIL_REQ_TYPE, EMAIL_REQ_TYPE_ID,	REL_ORDR_ID,	ATC_EMAIL,		MDS_CD, ISIPL)
				SELECT	DISTINCT	em.EMAIL_REQ_ID, em.ORDR_ID,	le.EMAIL_REQ_TYPE, em.EMAIL_REQ_TYPE_ID,	em.ORDR_ID, em.EMAIL_LIST_TXT, 0,	0
					FROM	dbo.EMAIL_REQ 	em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	(em.EMAIL_REQ_TYPE_ID	IN	(1, 2)
					AND		em.STUS_ID				=	10
					AND		em.ORDR_ID				IS	NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL)
							or
					(em.EMAIL_REQ_TYPE_ID	IN	(9)
					AND		em.STUS_ID				=	10
					AND		em.ORDR_ID				IS	NOT NULL)
							or 
					(em.EMAIL_REQ_TYPE_ID	IN	(17)
					AND		em.STUS_ID				=	10
					AND		em.ORDR_ID				IS	NOT NULL)
				


					
		IF (SELECT Count(1) FROM @Email)	>	0
			BEGIN
				UPDATE			vt
					SET			vt.REL_ORDR_ID	=	ISNULL(f2.ORDR_ID, vt.ORDR_ID)		
					FROM		@Email	vt
					INNER JOIN	dbo.FSA_ORDR	f1	WITH (NOLOCK)	ON	vt.ORDR_ID	=	f1.ORDR_ID
					INNER JOIN	dbo.FSA_ORDR	f2	WITH (NOLOCK)	ON	f1.PRNT_FTN	=	f2.FTN
					WHERE		f2.ORDR_ACTN_ID			=	2
					AND			vt.EMAIL_REQ_TYPE_ID	=	2
					
				OPEN SYMMETRIC KEY FS@K3y 
					DECRYPTION BY CERTIFICATE S3cFS@CustInf0;		

				UPDATE			vt
					SET			vt.H1_CUST_ID		=	fc.CUST_ID
								,vt.H5_H6_CUST_ID	=	od.H5_H6_CUST_ID 
								,vt.FTN				=	fo.FTN
								,vt.CUST_NME		=	Case When	(od.CSG_LVL_ID>0)	Then	'Private Customer'
															Else	fc.CUST_NME
														End
					--			,vt.ATC_EMAIL		=	dbo.decryptBinaryData(oc.EMAIL_ADR)	
								,vt.FULL_NME		=	Case	When	(od.CSG_LVL_ID>0)	Then	'Private Customer'
															Else	oc.FRST_NME	+ ' ' + oc.LST_NME
														End
								,vt.CUST_CMMT_DT	=	Convert(Varchar(12), ISNULL(od.CUST_CMMT_DT, fo.CUST_CMMT_DT))
								,vt.H5_H6_ADR		=	Case	When	(od.CSG_LVL_ID>0)	Then	'Private Address'
															Else	ad.STREET_ADR_1 + ' ' 
																	+ ad.STREET_ADR_2 + ', ' 
																	+ ad.CTY_NME	+ ' ' 
																	+ ad.PRVN_NME + ', ' 
																	+ ad.STT_CD + ', ' 
																	+ ad.ZIP_PSTL_CD + ', '
																	+ ad.CTRY_CD
														End
								,vt.MDS_CD			=	lp.MDS_CD						
								,vt.DCD				=	Case	vt.EMAIL_REQ_TYPE_ID
															When	1	Then	''
															Else	Convert(Varchar(12), getDate())
														End 	 
								,vt.FSA_ORDR_TYPE_DES=	lo.FSA_ORDR_TYPE_DES
								,vt.PRIM_TAC_EMAIL			=   Case	When	(od.CSG_LVL_ID>0)	Then	''
															Else	ordc.EMAIL_ADR END 
					FROM		@Email					vt	
					INNER JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	vt.REL_ORDR_ID	=	od.ORDR_ID
					INNER JOIN	dbo.FSA_ORDR			fo	WITH (NOLOCK)	ON	od.ORDR_ID		=	fo.ORDR_ID
					INNER JOIN	dbo.FSA_ORDR_CUST		fc	WITH (NOLOCK)	ON	fo.ORDR_ID		=	fc.ORDR_ID	
																			AND	fc.CIS_LVL_TYPE =	'H1'
					INNER JOIN	dbo.LK_FSA_ORDR_TYPE	lo	WITH (NOLOCK)	ON	fo.ORDR_TYPE_CD	=	lo.FSA_ORDR_TYPE_CD
					INNER JOIN	dbo.LK_PPRT				lp	WITH (NOLOCK)	ON	od.PPRT_ID		=	lp.PPRT_ID
					LEFT JOIN	dbo.ORDR_CNTCT			oc	WITH (NOLOCK)	ON	fo.ORDR_ID		=	oc.ORDR_ID	
																			AND	oc.ROLE_ID		=	11
					LEFT JOIN	dbo.ORDR_ADR			ad	WITH (NOLOCK)	ON	fo.ORDR_ID		=	ad.ORDR_ID	
																			AND	ad.CIS_LVL_TYPE	IN	('H5', 'H6')
					LEFT JOIN	dbo.ORDR_CNTCT		ordc	WITH (NOLOCK)	ON	od.ORDR_ID		=	ordc.ORDR_ID	
																			AND	ordc.ROLE_ID	=	13		

				UPDATE			vt
					SET			vt.H5_H6_CUST_ID	=	h5.CUST_ID
								,vt.FTN				=	od.ORDR_ID
								,vt.CUST_NME		=	ipl.ORGTNG_CO_ID
					--			,vt.ATC_EMAIL		=	dbo.decryptBinaryData(oc.EMAIL_ADR)	
								,vt.FULL_NME		=	Case	When	(od.CSG_LVL_ID>0)	Then	'Private Customer'
														Else oc.FRST_NME	+ ' ' + oc.LST_NME END
								,vt.CUST_CMMT_DT	=	Convert(Varchar(12), ISNULL(od.CUST_CMMT_DT, ipl.CUST_CMMT_DT))
								,vt.H5_H6_ADR		=	Case	When	(od.CSG_LVL_ID>0)	Then	'Private Address'
															Else	ad.STREET_ADR_1 + ' ' 
																	+ ad.STREET_ADR_2 + ', ' 
																	+ ad.CTY_NME	+ ' ' 
																	+ ad.PRVN_NME --don't put a comma between province and state, only 1 is pop + ', ' 
																	+ ad.STT_CD + ', ' 
																	+ ad.ZIP_PSTL_CD + ', '
																	+ ad.CTRY_CD
														End
								,vt.MDS_CD			=	lp.MDS_CD
								,vt.DCD				=	Case	vt.EMAIL_REQ_TYPE_ID
															When	1	Then	''
															Else	Convert(Varchar(12), getDate())
														End 	 
								,vt.FSA_ORDR_TYPE_DES=	lo.ORDR_TYPE_DES
								, vt.ISIPL			=	1
								,vt.PRIM_TAC_EMAIL			=   Case	When	(od.CSG_LVL_ID>0)	Then	''
															Else ordc.EMAIL_ADR End
					FROM		@Email					vt	
					INNER JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	vt.REL_ORDR_ID	=	od.ORDR_ID
					INNER JOIN	dbo.IPL_ORDR			ipl	WITH (NOLOCK)	ON	od.ORDR_ID		=	ipl.ORDR_ID
					INNER JOIN	dbo.H5_FOLDR			h5	WITH (NOLOCK)	ON	od.H5_FOLDR_ID	=	h5.H5_FOLDR_ID
					INNER JOIN	dbo.LK_ORDR_TYPE		lo	WITH (NOLOCK)	ON	lo.ORDR_TYPE_ID	=	ipl.ORDR_TYPE_ID
					INNER JOIN	dbo.LK_PPRT				lp	WITH (NOLOCK)	ON	od.PPRT_ID		=	lp.PPRT_ID
					LEFT JOIN	dbo.ORDR_CNTCT			oc	WITH (NOLOCK)	ON	od.ORDR_ID		=	oc.ORDR_ID	
																			AND	oc.ROLE_ID		=	11
					LEFT JOIN	dbo.ORDR_ADR			ad	WITH (NOLOCK)	ON	od.ORDR_ID		=	ad.ORDR_ID	
																			AND ad.ADR_TYPE_ID	=	11
					LEFT JOIN	dbo.ORDR_CNTCT		ordc	WITH (NOLOCK)	ON	od.ORDR_ID		=	ordc.ORDR_ID	
																			AND	ordc.ROLE_ID	=	13	
																		
			END				
	END	
	SELECT * FROM @Email

	-- ****************************************************************
	-- Below table is for the processing of the Event emails.
	--*****************************************************************
	
	SELECT		em.EMAIL_REQ_ID, em.EVENT_ID,	le.EMAIL_REQ_TYPE, em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT AS ATC_MAIL, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT
					FROM	dbo.EMAIL_REQ em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	NOT IN	(1, 2, 6, 18, 19)
					AND		em.STUS_ID				=	10
					AND		em.Event_ID			IS	NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL

	Exec dbo.getNCCOOrdersToEmail

	SELECT		em.EMAIL_REQ_ID, le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_BODY_TXT
					FROM	dbo.EMAIL_REQ em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	= 15
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
					
	
	-- ****************************************************************
	-- Below table is for the processing of the Custid's to User Assignment emails.
	--*****************************************************************
				
	SELECT		em.EMAIL_REQ_ID, le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_BODY_TXT
					FROM	dbo.EMAIL_REQ em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	= 16
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
	
	--For MDS Guest Wi-fi Emails
	
	SELECT		em.EMAIL_REQ_ID, le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.EVENT_ID
					FROM	dbo.EMAIL_REQ em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (18,19)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
	
	

	--For ASR Emails
	SELECT em.EMAIL_REQ_ID,	le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.ORDR_ID
		FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
				dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
		WHERE	em.EMAIL_REQ_TYPE_ID	=	20
			AND em.STUS_ID	=	10
			

	--For Redesign 
	SELECT		em.EMAIL_REQ_ID,	le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.REDSGN_ID 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	=	21
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL

	-- For CPT 24 hour Notification Email
	SELECT		em.EMAIL_REQ_ID,	le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.CPT_ID, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
							-- Added by Sarah Sandoval (06262018) to not send email for CPT in Deleted, Completed and Cancelled status
							INNER JOIN dbo.CPT cpt WITH (NOLOCK) ON cpt.CPT_ID = em.CPT_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (27,28,29)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
					AND		DATEADD(DAY, 1, em.CREAT_DT) <= GETDATE()
					AND		cpt.CPT_STUS_ID			NOT IN (308, 309, 311)
	
	SELECT		em.EMAIL_REQ_ID, em.ORDR_ID, em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) 
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (31)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL

	-- For CPT Notification Email
	SELECT		em.EMAIL_REQ_ID,	le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.CPT_ID, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (32,33)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
	
	SELECT		em.EMAIL_REQ_ID, em.ORDR_ID, em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) 
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (34)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL

	-- For SDE Opportunity Email
	SELECT		em.EMAIL_REQ_ID, le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID, em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.CPT_ID, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (36)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
					
	-- For SDE Account Setup Email Notification
	SELECT		em.EMAIL_REQ_ID, em.EMAIL_REQ_TYPE_ID,	em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) 
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (37)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL	

	-- For CPE CLLI Email
	SELECT		em.EMAIL_REQ_ID, le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID, em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.EMAIL_BODY_TXT, em.CPT_ID, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (38)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_BODY_TXT		IS NOT NULL
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
	
	-- For CPE Zscaler Disconnects Email
	SELECT		em.EMAIL_REQ_ID, em.ORDR_ID, le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID, em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (39)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
	
	-- For IS Bill Only Emails notification that an order is in Task 110 and a bill only order is needed.
	SELECT		em.EMAIL_REQ_ID, em.ORDR_ID, le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID, em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_CC_TXT, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (42)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
	-- For IPM Notification that Shipping need coordinated on CPE move order.
	SELECT		em.EMAIL_REQ_ID, em.ORDR_ID, le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID, em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_BODY_TXT, em.EMAIL_CC_TXT, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (43)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL
	
	-- For Asia International orders for Disc to have Vendor pick up the Equipment
	SELECT		em.EMAIL_REQ_ID, em.ORDR_ID, le.EMAIL_REQ_TYPE,	em.EMAIL_REQ_TYPE_ID, em.EMAIL_LIST_TXT, em.EMAIL_SUBJ_TXT, em.EMAIL_BODY_TXT, em.EMAIL_CC_TXT, em.CREAT_DT 
					FROM	dbo.EMAIL_REQ  em	WITH (NOLOCK) INNER JOIN
							dbo.LK_EMAIL_REQ_TYPE le WITH (NOLOCK) ON le.EMAIL_REQ_TYPE_ID = em.EMAIL_REQ_TYPE_ID
					WHERE	em.EMAIL_REQ_TYPE_ID	IN (44)
					AND		em.STUS_ID				=	10
					AND		em.EMAIL_LIST_TXT		IS	NOT NULL



End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
