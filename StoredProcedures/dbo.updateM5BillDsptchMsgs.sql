USE [COWS]
GO
_CreateObject 'SP','dbo','updateM5BillDsptchMsgs'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		jrg7298
-- Create date: 10/01/2019
-- Description:	Get all Billable Dispatch messages for Mach5 Sender
-- =========================================================
ALTER PROCEDURE [dbo].[updateM5BillDsptchMsgs]
	 @TRAN_ID		Int
	,@BillDisptchID Int
	,@STUS_ID		Int
	,@ERRMSG			VARCHAR(MAX)
AS
BEGIN
SET NOCOUNT ON;
Begin Try

UPDATE		dbo.[M5_BILL_DSPTCH_MSG]	WITH (ROWLOCK)
	SET		STUS_ID		=	@STUS_ID, SENT_DT = GETDATE(), ERR_MSG=CASE WHEN LEN(@ERRMSG)>0 THEN @ERRMSG ELSE NULL END
	WHERE	[M5_BILL_DSPTCH_MSG_ID]		=	@TRAN_ID
	  AND  STUS_ID=10

UPDATE		dbo.[BILL_DSPTCH]	WITH (ROWLOCK)
	SET		STUS_ID		= CASE WHEN (@STUS_ID = 21)	THEN 242 ELSE 243 END, MODFD_DT = GETDATE(), MODFD_BY_USER_ID = 1
	WHERE	BILL_DSPTCH_ID		=	@BillDisptchID

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END