USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getODIECPEMsgInfo]    Script Date: 1/18/2022 9:51:19 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

	-- =============================================
	-- Author:		David Phillips
	-- Create date: 7/29/2015
	-- Description:	Retrieves ODIE CPE Info.
	-- kh946640 03/26/18 COWS DB Restructuring as columns have been moved from fsa_ordr->fsa_ordr_cpe_line_item table for DualPort project(PJ020783 - CR93)
	-- kh946640 04/17/18 Fix for pulling data based on CSG_LVL_ID 
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getODIECPEMsgInfo] 
		@REQ_ID int  

	AS
	BEGIN


	BEGIN TRY
	
	DECLARE @ORDR_ID int  -- 302519
	
	SELECT @ORDR_ID = ORDR_ID FROM dbo.ODIE_REQ WHERE REQ_ID = @REQ_ID
	
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	-- Order Info.
	SELECT  DISTINCT
			ISNULL(fsa.FTN,'')                                     AS [OrderID],
			ISNULL(lot.ORDR_TYPE_DES,'')                           AS [OrderType],
			'Pending'                                              AS [OrderAction],
			ISNULL(lpt.PROD_TYPE_DES,'')                           AS [ProductType],
			CASE
				WHEN ord.DMSTC_CD = 1                   THEN 'I'
				ELSE 'D'
			END                                                   AS [DomesticInternationalFlag],
			ISNULL(ord.CSG_LVL_ID,0)							  AS [CSGLevel],		
			ISNULL(fsa.PRNT_FTN,'')                               AS [ParentOrderID],
			ISNULL(fsa.RELTD_FTN,'')                              AS [RelatedOrderID],
			ISNULL(ord.CUST_CMMT_DT, fsa.CUST_CMMT_DT)            AS [CustomerCommitDate],
			ISNULL(fsa.CUST_PRMS_OCPY_CD,'')                      AS [CustomerPremiseCurrentlyOccupiedFlag],
			ISNULL(fc.SITE_ID,'')                                 AS [SiteID]
			
	
	FROM dbo.FSA_ORDR fsa WITH (NOLOCK)
	INNER JOIN dbo.ORDR	ord WITH (NOLOCK) ON ord.ORDR_ID = fsa.ORDR_ID
	INNER JOIN dbo.LK_ORDR_TYPE lot WITH (NOLOCK) ON lot.FSA_ORDR_TYPE_CD = fsa.ORDR_TYPE_CD
	LEFT OUTER JOIN dbo.LK_PROD_TYPE lpt WITH (NOLOCK) ON fsa.PROD_TYPE_CD = lpt.FSA_PROD_TYPE_CD
	LEFT OUTER JOIN dbo.FSA_ORDR_CUST fc WITH (NOLOCK) ON fc.ORDR_ID = fsa.ORDR_ID
					AND fc.CIS_LVL_TYPE IN ('H5','H6')
	WHERE fsa.ORDR_ID = @ORDR_ID
		
			
	--CustomerInfo/CISHierarchyInfo Section
	
	SELECT	DISTINCT
			ISNULL(fc.CIS_LVL_TYPE,'')                                 AS [LevelType],
			ISNULL(fc.CUST_ID,0)								       AS [CustomerID],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdFC.CUST_NME) ELSE fc.CUST_NME END,'') AS [CustomerName],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.STREET_ADR_1) ELSE oa.STREET_ADR_1 END,'') AS [AddressLine1],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.STREET_ADR_2) ELSE oa.STREET_ADR_2 END,'') AS [AddressLine2],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.STREET_ADR_3) ELSE oa.STREET_ADR_3 END,'') AS [AddressLine3],
			''													       AS [Suite],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.BLDG_NME) ELSE oa.BLDG_NME END,'') AS [Building],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.FLR_ID) ELSE oa.FLR_ID END,'') AS [Floor],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.RM_NBR) ELSE oa.RM_NBR END,'') AS [Room],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.CTY_NME) ELSE oa.CTY_NME END,'') AS [City],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.STT_CD) ELSE oa.STT_CD END,'') AS [StateCode],
			ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.ZIP_PSTL_CD) ELSE oa.ZIP_PSTL_CD END,'') AS [ZIPPostalCode],	
			ISNULL(oa.CTRY_CD,'')                                      AS [CntryCode],
			ISNULL(oa.PRVN_NME,'')     AS [ProvinceMuncipality],
			'Phone'													   AS [PhoneType],
			ISNULL(oc.ISD_CD,'')                                       AS [CountryCode],
			ISNULL(oc.NPA,'')		                                   AS [NPA],
			ISNULL(oc.NXX,'')		                                   AS [NXX],
			ISNULL(oc.STN_NBR,'')	                                   AS [Station],
			ISNULL(oc.CTY_CD,'')	                                   AS [CityCode],
			ISNULL(oc.PHN_NBR,'')                                      AS [Number]

			FROM dbo.ORDR ord 
				INNER JOIN dbo.FSA_ORDR_CUST fc WITH (NOLOCK) on fc.ORDR_ID = ord.ORDR_ID
				LEFT JOIN dbo.ORDR_ADR oa WITH (NOLOCK)	on	oa.ORDR_ID = fc.ORDR_ID  
											AND	oa.CIS_LVL_TYPE in ('H6','H5','H1')
											AND	oa.HIER_LVL_CD	=	1
				LEFT JOIN dbo.ORDR_CNTCT oc	WITH (NOLOCK) on oc.ORDR_ID	= fc.ORDR_ID 
											AND	oc.CIS_LVL_TYPE  in ('H6','H5','H1')
											AND	oc.CNTCT_TYPE_ID	=	17
				LEFT JOIN dbo.LK_CNTCT_TYPE	lc WITH (NOLOCK) on lc.CNTCT_TYPE_ID = oc.CNTCT_TYPE_ID
				LEFT JOIN dbo.CUST_SCRD_DATA csdFC WITH (NOLOCK) ON csdFC.SCRD_OBJ_ID=fc.FSA_ORDR_CUST_ID AND csdFC.SCRD_OBJ_TYPE_ID=5
				LEFT JOIN dbo.CUST_SCRD_DATA csdA WITH (NOLOCK) ON csdA.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdA.SCRD_OBJ_TYPE_ID=14
		   WHERE fc.ORDR_ID = @ORDR_ID 
				AND	fc.CIS_LVL_TYPE  in ('H6','H5','H1') 	


	
	--CustomerInfo/ContactInfo Section
	SELECT		DISTINCT
				ISNULL(lc.CNTCT_TYPE_DES,'') AS [Type],
				ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.FRST_NME) ELSE oc.FRST_NME END,'') AS [FirstName],
				ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.LST_NME) ELSE oc.LST_NME END,'') AS [LastName],
				ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.CUST_CNTCT_NME) ELSE oc.CNTCT_NME END,'') AS [Name],
				ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdC.CUST_EMAIL_ADR) ELSE oc.EMAIL_ADR END,'') AS [EmailAddress],
				ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.STREET_ADR_1) ELSE oa.STREET_ADR_1 END,'') AS [AddressLine1],
				ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.STREET_ADR_2) ELSE oa.STREET_ADR_2 END,'') AS [AddressLine2],
				
				ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.CTY_NME) ELSE oa.CTY_NME END,'') AS [City],
				ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.STT_CD) ELSE oa.STT_CD END,'') AS [StateCode],
				ISNULL(CASE WHEN (ord.CSG_LVL_ID>0) THEN dbo.decryptbinarydata(csdA.ZIP_PSTL_CD) ELSE oa.ZIP_PSTL_CD END,'') AS [ZIPPostalCode],
				ISNULL(oa.CTRY_CD,'')									AS [CntryCode],
				ISNULL(oa.PRVN_NME,'')	AS [ProvinceMuncipality],
				
				'Phone'													AS [PhoneType],
				ISNULL(oc.ISD_CD,'')                                    AS [CountryCode],
				ISNULL(oc.NPA,'')										AS	[NPA],
				ISNULL(oc.NXX,'')										AS	[NXX],
				ISNULL(oc.STN_NBR,'')									AS	[Station],
				ISNULL(oc.CTY_CD,'')									AS	[CityCode],
				ISNULL(oc.PHN_NBR,'')									AS [Number],
				ISNULL(oc.PHN_EXT_NBR,'')                               AS [Extension]
        FROM	dbo.ORDR ord
			INNER JOIN  dbo.ORDR_ADR		oa	WITH (NOLOCK)	on oa.ORDR_ID = ord.ORDR_ID	
			LEFT JOIN	dbo.LK_ADR_TYPE		la	WITH (NOLOCK)	on la.ADR_TYPE_ID = oa.ADR_TYPE_ID
			LEFT JOIN	dbo.ORDR_CNTCT		oc	WITH (NOLOCK)	on oc.ORDR_ID	= oa.ORDR_ID 
																AND	oc.CIS_LVL_TYPE in ('OD')-- PJ025845 changed from 'H6','H5'
																AND	oc.CNTCT_TYPE_ID	!=	17
			LEFT JOIN	dbo.LK_CNTCT_TYPE	lc	WITH (NOLOCK)	on lc.CNTCT_TYPE_ID = oc.CNTCT_TYPE_ID
			LEFT JOIN dbo.CUST_SCRD_DATA csdA WITH (NOLOCK) ON csdA.SCRD_OBJ_ID=oa.ORDR_ADR_ID AND csdA.SCRD_OBJ_TYPE_ID=14
			LEFT JOIN dbo.CUST_SCRD_DATA csdC WITH (NOLOCK) ON csdC.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csdC.SCRD_OBJ_TYPE_ID=15
       WHERE	oa.ORDR_ID = @ORDR_ID  
			AND	oa.CIS_LVL_TYPE in ('H6','H5') 
						
	
	-- CPE ORDER Level Section
	SELECT 
			ISNULL(fsa.CPE_CPE_ORDR_TYPE_CD,'')                        AS [CPEOrderTypeCode],
			ISNULL(fsa.CPE_EQPT_ONLY_CD,'')                            AS [EquipmentOnlyFlagCode],
			ISNULL(fsa.CPE_REC_ONLY_CD,'')                             AS [RecordsOnlyOrderFlagCode],
			ISNULL(fsa.CPE_ACCS_PRVDR_CD,'')                           AS [AccessProviderCode],
			ISNULL(fsa.CPE_PHN_NBR_TYPE_CD,'')                         AS [Type],
			ISNULL(fsa.CPE_PHN_NBR, '')                                AS [CPEPhoneNumber],
			ISNULL(fsa.CPE_TST_TN_NBR, '')                             AS [SprintTestPhoneNumber],
			ISNULL(fsa.CPE_ECCKT_ID, '')                               AS [ECCKTIdentifier],
			ISNULL(fsa.CPE_REC_ONLY_CD,'')                             AS [RecordsOnlyOrderFlagCode],
			ISNULL(fsa.CPE_MSCP_CD,'')                                 AS [ManagedServicesChannelProgramCode],
			ISNULL(fsa.CPE_DLVRY_DUTY_ID, '')                          AS [DeliveryDuties]
	
		FROM FSA_ORDR fsa WITH (NOLOCK)
		WHERE fsa.ORDR_ID = @ORDR_ID
	
	
	-- CPE Line Items Section	
	
	SELECT
		CASE
			WHEN litm.ITM_STUS = 401		THEN 'Pending' 
		    WHEN litm.ITM_STUS = 402        THEN 'Removed'
		 END                                                           AS [ItemAction],
		ISNULL(litm.EQPT_TYPE_ID, '')                                   AS [EquipmentTypeCode],
		ISNULL(litm.MATL_CD,'')                                         AS [MatCode],
		ISNULL(litm.MDS_DES,'')                                         AS [Description],
		ISNULL(litm.MFR_NME,'')                                         AS [Manufacturer],
		ISNULL(litm.DEVICE_ID,'')                                        AS [DeviceID],
		''						                                        AS [ProjectID],
		ISNULL(litm.ORDR_QTY,0)                                        AS [Quantity],
		ISNULL(litm.CNTRC_TYPE_ID,'')                                   AS [ContractType],
		ISNULL(litm.CNTRC_TERM_ID,'')                                   AS [ContractTerm],
		ISNULL(litm.INSTLN_CD,'')                                       AS [InstallationFlagCode],
		CASE
			WHEN litm.CNTRC_TYPE_ID = 'ASAS'      THEN 'Y'
			ELSE ISNULL(litm.MNTC_CD,'')
		END									                            AS [MaintenanceFlagCode],
		ISNULL(litm.FMS_CKT_NBR,ISNULL(DISC_FMS_CKT_NBR,0))            AS [FMSCktNbr],
		ISNULL(litm.CMPNT_FMLY,'')                                      AS [CompFamily],
		ISNULL(litm.CMPNT_ID,0)                                         AS [ComponentID],
		CASE
			WHEN ISNULL(litm.CPE_REUSE_CD,0) = 0 THEN 'N'
			WHEN ISNULL (litm.CPE_REUSE_CD,0)<> 0 THEN 'Y'
		END																AS [Reuse]
			
		
		
	FROM dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK)
		INNER JOIN FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = litm.ORDR_ID
		WHERE litm.ORDR_ID = @ORDR_ID
		AND litm.EQPT_TYPE_ID = 'MJH'
		AND ((fo.ORDR_TYPE_CD <> 'CN' AND litm.ITM_STUS = 401)
								OR
			(fo.ORDR_TYPE_CD = 'CN' AND litm.ITM_STUS = 402))
	
	
	--Notes
	
	SELECT 
		ISNULL(NTE_TXT,'')                                              AS [NoteText]
	
	FROM dbo.ORDR_NTE WITH (NOLOCK)
	WHERE ORDR_ID = @ORDR_ID
		AND NTE_TYPE_ID = 6
	
	RETURN
			
	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH

	END
	



