USE [COWS]
GO
_CreateObject 'SP','dbo','removeRelatedOrderAssociation'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <07/02/2012>
-- Description:	<To remove association from related orders list for FSA orders>
-- =============================================
Alter PROCEDURE dbo.removeRelatedOrderAssociation 
	@CNCL_ORDR_ID INT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Cnt INT
	SET @Cnt = 0
	
	
    SELECT @Cnt = COUNT(1)
		FROM	dbo.ORDR WITH (NOLOCK)
		WHERE	PRNT_ORDR_ID = @CNCL_ORDR_ID
		
	IF @Cnt > 1
		BEGIN
			DECLARE @PRNT_ORDR_ID INT
			SET @PRNT_ORDR_ID = 0
			
			--Get next ORDR_ID 
			SELECT TOP 1 @PRNT_ORDR_ID = ORDR_ID
				FROM	dbo.ORDR WITH (NOLOCK)
				WHERE	PRNT_ORDR_ID = @CNCL_ORDR_ID 
					AND ORDR_ID	<> @CNCL_ORDR_ID
					
			IF @PRNT_ORDR_ID <> 0
				BEGIN
					UPDATE	dbo.ORDR WITH (ROWLOCK)
						SET		PRNT_ORDR_ID = @PRNT_ORDR_ID
						WHERE	PRNT_ORDR_ID = @CNCL_ORDR_ID
							AND	ORDR_ID		<> @CNCL_ORDR_ID
				END		
		END
	ELSE IF @Cnt = 0
		BEGIN
			UPDATE	dbo.ORDR WITH (ROWLOCK)
				SET		PRNT_ORDR_ID = @CNCL_ORDR_ID
				WHERE	ORDR_ID		= @CNCL_ORDR_ID
		END
	
	DECLARE @CNCL_FTN varchar(20)
	SET @CNCL_FTN = ''
	
	SELECT @CNCL_FTN  = FTN
		FROM	dbo.FSA_ORDR WITH (NOLOCK)
		WHERE	PRNT_FTN IN (SELECT FTN FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @CNCL_ORDR_ID)
		
	IF @CNCL_FTN <> '' 
		BEGIN	
			--Enter notes
			INSERT INTO dbo.ORDR_NTE (NTE_TYPE_ID,ORDR_ID,CREAT_DT,CREAT_BY_USER_ID,REC_STUS_ID,NTE_TXT)
			VALUES (9,@CNCL_ORDR_ID,GETDATE(),1,1,'This FTN is cancelled. Cancelled FTN received from FSA: ' + CAST(@CNCL_FTN AS VARCHAR))
		END
	
    
END
GO
