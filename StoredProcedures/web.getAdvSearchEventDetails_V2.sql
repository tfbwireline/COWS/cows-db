USE [COWS]
GO
_CreateObject 'SP','web','getAdvSearchEventDetails_V2'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Cereated By:	Naidu Vattigunta	
-- Create date:	08/11/2011
-- Description:	Advanced Search
-- =======================================================


-- =======================================================
-- Modify By:	Kyle Wichert
-- Create date:	02/06/2012
-- Description:	Rewrite to take use new view, no more dynamic SQL.
--				Idea is to do a search of the view for the first criteria given and store those event IDs.
--				Then for the next criteria passed in search the subset of events found in the first criteria, and again for the next etc.
-- =======================================================
-- Modified By:		sbg9814
-- Modified Date:	05/31/2012



-- Description:		Rewrote to inprove performance and eliminated the use of V_EVENT.
-- =======================================================
ALTER PROCEDURE [web].[getAdvSearchEventDetails_V2]
	@FTNs			Varchar(1000)	= '',
	@SOWSIds		Varchar(1000)	= '',
	@AssignedUsers	Varchar(50)		= '',
	@RequestorNames	Varchar(50)		= '',
	@EventTypes		Varchar(1000)	= '0',
	@EventIDs		Varchar(100)	= '',
	@ADType			Varchar(1000)	= '',
	@CustName		Varchar(1000)	= '',
	@FMSCkt			Varchar(100)	= '',
	@EventStatus	Varchar(100)	= '',
	@ActionType		Varchar(100)	= '',
	@Device			Varchar(100)	= '',
	@H5_H6			Varchar(100)	= '',
	@StrtDt			DateTime		= NULL,
	@EndDt			DateTime		= NULL,
	@FRBID			Varchar(10)		= '',
	@Serial			Varchar(60)		= '',
	@DesignType		Varchar(1)		= '',
	@OrgID			Varchar(10)		= '',
	@ActType		Varchar(20)		= '',
	@Status			Varchar(1)		= '',
	@ReDesign		Varchar(50)		= '',
	@apptStartOp	Varchar(2)		= '0',
	@apptStartOption Varchar(2)		=	'0',
	@apptEndOp		Varchar(2)		=	'0',
	@apptEndOption	Varchar(2)		=	'0',
	@MDSActivityType Varchar(2)		=	'0',
	@MDSMACType		 Varchar(100)	=	'',
	@REQ_BY_USR_ID	 INT			=	1,
	@RptSchedule	CHAR(1)			=	'D',
	@AdhocEmailChk  VARCHAR(10)		=	'FALSE',
	@CSGLvlId		TINYINT			= 0,
	@MDSNtwkActyType VARCHAR(100)	= ''
AS
BEGIN
Begin Try
DECLARE @OrdrBy VARCHAR(50)
DECLARE @DropTbl NVARCHAR(400)				
DECLARE @CrtTbl NVARCHAR(4000)
DECLARE @InsrtTbl VARCHAR(100)
DECLARE @FinalSel NVARCHAR(4000)

			    
SET NOCOUNT ON;  
	SET @OrdrBy = ' ORDER BY ev.[EVENT_ID] DESC '
	SET @DropTbl = ' IF OBJECT_ID(N''tempdb..#Results'', N''U'') IS NOT NULL         
				DROP TABLE #Results '
	SET @CrtTbl = 	' CREATE TABLE #Results (EVENT_ID VARCHAR(100), 
		EVENT_TYPE_NME VARCHAR(100), 
		EVENT_STUS_DES VARCHAR(100), 
		FTN VARCHAR(4000), 
		ASN_TO VARCHAR(1000), 
		REQ_BY VARCHAR(500), 
		STRT_DT DATETIME, 
		MODFD_DT DATETIME, 
		MDS_Type VARCHAR(10), 
		CSG_LVL_ID TINYINT,
		H5_H6 VARCHAR(MAX),
		H1 VARCHAR(9),
		CUST_NME VARCHAR(200),
		City VARCHAR(100),
		STT VARCHAR(100),
		DEV_NME VARCHAR(1000),
		REDS_NBR VARCHAR(1000)) '
	SET @InsrtTbl = ' INSERT INTO #Results '
	SET @FinalSel = ' SELECT EVENT_ID AS  ''Event ID'', 
		EVENT_TYPE_NME AS  ''Event Type'', 
		EVENT_STUS_DES AS	''Event Status'', 
		FTN , 
		ASN_TO AS  ''Assigned To'', 
		REQ_BY AS	''Requested By'', 
		STRT_DT AS ''Start Date'', 
		MODFD_DT AS ''Modified Date'', 
		MDS_Type AS  ''MDS_Type'', 
		CSG_LVL_ID,
		H5_H6 AS	''H5 H6'',
		H1,
		CUST_NME AS	''Customer Name'',
		City,
		STT AS ''State'',
		DEV_NME AS  ''Device Name'',
		REDS_NBR AS	''Redesign Number''
		FROM #Results '
    SET @FTNs           = LTRIM(RTRIM(@FTNs))
    SET @SOWSIds        = LTRIM(RTRIM(@SOWSIds))
    SET @AssignedUsers  = LTRIM(RTRIM(@AssignedUsers))
    SET @RequestorNames = LTRIM(RTRIM(@RequestorNames))
    SET @EventTypes     = LTRIM(RTRIM(@EventTypes))
    SET @EventIDs       = LTRIM(RTRIM(@EventIDs))
    SET @ADType         = LTRIM(RTRIM(@ADType))
    SET @CustName		= LTRIM(RTRIM(@CustName))
    SET @FMSCkt			= LTRIM(RTRIM(@FMSCkt))
	SET @EventStatus	= LTRIM(RTRIM(@EventStatus))
	SET @ActionType		= LTRIM(RTRIM(@ActionType))
	SET @Device			= LTRIM(RTRIM(@Device))
	SET @H5_H6			= LTRIM(RTRIM(@H5_H6))
	SET @FRBID			= LTRIM(RTRIM(@FRBID))
	SET @Serial			= LTRIM(RTRIM(@Serial))
	SET @DesignType		= LTRIM(RTRIM(@DesignType))
	SET @OrgID			= LTRIM(RTRIM(@OrgID))
	SET @ActType		= LTRIM(RTRIM(@ActType))
	SET @Status			= LTRIM(RTRIM(@Status))
	SET @ReDesign		= LTRIM(RTRIM(@ReDesign))
	SET @MDSActivityType = LTRIM(RTRIM(@MDSActivityType))
	SET @MDSMACType		= LTRIM(RTRIM(@MDSMACType))
	IF (RIGHT(@MDSNtwkActyType, 1) = ',')
			set @MDSNtwkActyType = LEFT(@MDSNtwkActyType, LEN(@MDSNtwkActyType) - 1)

    -- Baseline '0' & null to '', except for FRB Request ID.  That will be 0 for Headend Events
    IF    ISNULL(@FTNs,'0')             =     '0'

          SET @FTNs                     =     ''
    IF    ISNULL(@SOWSIds, '0')         =     '0'

          SET @SOWSIds                  =     ''
    IF    ISNULL(@AssignedUsers, '0')   =     '0'
          SET @AssignedUsers            =     ''
    IF    ISNULL(@RequestorNames, '0')  =     '0'
          SET @RequestorNames           =     ''
    IF    ISNULL(@EventTypes, '0')      =     '0'
          SET @EventTypes               =     ''
    IF    ISNULL(@EventIDs, '0')        =     '0'
		  SET @EventIDs                 =     ''
    IF    ISNULL(@ADType, '0')          =     '0'

          SET @ADType                   =     ''
    IF    ISNULL(@CustName, '0')        =     '0'

          SET @CustName                 =     ''
    IF	  ISNULL(@FMSCkt, '0')			=	  '0'
		  SET @FMSCkt					=	  ''     
	IF	  ISNULL(@EventStatus, '0')		=     '0'
		  SET @EventStatus				=	  ''      
	IF	  ISNULL(@ActionType, '0')		=	  '0'
		  SET @ActionType				=	  ''      
	IF	  ISNULL(@Device, '0')			=	  '0'
		  SET @Device					=	  ''      
	IF	  ISNULL(@H5_H6, '0')			=	  '0'
		  SET @H5_H6					=	  '' 
	IF	  ISNULL(@FRBID, '-1')			=	  '-1'
		  SET @FRBID					=	  '' 
	IF	  ISNULL(@Serial, '0')			=	  '0'
		  SET @Serial					=	  '' 
	IF	  ISNULL(@DesignType, '0')		=	  '0'
		  SET @DesignType				=	  '' 
	IF	  ISNULL(@OrgID, '0')			=	  '0'
		  SET @OrgID					=	  '' 
	IF	  ISNULL(@ActType, '0')			=	  '0'
		  SET @ActType					=	  ''
	IF	  ISNULL(@ReDesign, '0')		=	  '0'
		  SET @ReDesign					=	  ''
	IF	  ISNULL(@MDSActivityType, '0')	=	  '0'
		  SET @MDSActivityType			=	  ''
	IF	  ISNULL(@MDSMACType, '0')		=	  '0'
		  SET @MDSMACType				=	  ''
	--Don't baseline Status because 0 is a valid value.
	IF OBJECT_ID(N'tempdb..#Events', N'U') IS NOT NULL         
				DROP TABLE #Events   
			    CREATE TABLE #Events         
				(RowID INT Identity(1,1), EventID	Varchar(100)) 
				
	IF OBJECT_ID(N'tempdb..#MDSNtwkActy', N'U') IS NOT NULL         
				DROP TABLE #MDSNtwkActy   
			    CREATE TABLE #MDSNtwkActy         
				(EVENT_ID INT, NTWK_ACTY VARCHAR(100))

INSERT INTO #MDSNtwkActy
		SELECT EVENT_ID, dbo.[GetCommaSepMDSNtwkActys](EVENT_ID,1) FROM dbo.MDS_EVENT_NTWK_ACTY WITH (NOLOCK)
		IF (LEN(@MDSNtwkActyType)>0)
		BEGIN
			DELETE FROM
			#MDSNtwkActy WHERE NTWK_ACTY!=@MDSNtwkActyType
		END
	


	DECLARE @e_sqlEvent NVARCHAR(MAX)
	DECLARE @e_where	NVARCHAR(max)	 = ''
	DECLARE @e_finalSQL NVARCHAR(MAX)
	SET @e_sqlEvent = 'INSERT INTO #Events (EventID)        
							SELECT DISTINCT Event_ID
							FROM dbo.APPT WITH(NOLOCK) 
							WHERE 1 = 1 '


	IF @StrtDt IS NOT NULL
		BEGIN
			IF @apptStartOp != '0'
				BEGIN
					IF @apptStartOp = '1'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,STRT_TMST) >= ''' + CONVERT(varchar,@StrtDt) + '''' + ' AND CONVERT(DATE,STRT_TMST) <=''' + CONVERT(Varchar,DATEADD(day,180,@StrtDt)) + ''''  

						END
					ELSE IF @apptStartOp = '2'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,STRT_TMST) >= ''' + CONVERT(varchar,DATEADD(day,-180,@StrtDt)) + '''' + ' AND CONVERT(DATE,STRT_TMST) <=''' + CONVERT(Varchar,@StrtDt) + ''''  

						END
					ELSE IF @apptStartOp = '3'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,STRT_TMST) = ''' + CONVERT(Varchar,@StrtDt) + ''''  


						END
				END
			ELSE
				BEGIN
					SET @e_where += ' AND CONVERT(DATE,STRT_TMST) = ''' + CONVERT(Varchar,@StrtDt) + ''''  


				END

		END
	ELSE
		BEGIN
			IF @apptStartOption != '0'
				BEGIN
					IF @apptStartOption = '1'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,STRT_TMST) >= '''  + CONVERT(varchar,(GETDATE() - 30)) +  '''' + ' AND CONVERT(DATE,STRT_TMST) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 

						END
					ELSE IF @apptStartOption = '2'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,STRT_TMST) >= '''  + CONVERT(varchar,(GETDATE() - 60)) +  '''' + ' AND CONVERT(DATE,STRT_TMST) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 

						END
					ELSE IF @apptStartOption = '3'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,STRT_TMST) >= '''  + CONVERT(varchar,(GETDATE() - 90)) +  '''' + ' AND CONVERT(DATE,STRT_TMST) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 

						END
					ELSE IF @apptStartOption = '4'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,STRT_TMST) >= '''  + CONVERT(varchar,(GETDATE() - 180)) +  '''' + ' AND CONVERT(DATE,STRT_TMST) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 


						END
				END
		END


	IF @EndDt IS NOT NULL
		BEGIN
			IF @apptEndOp != '0'
				BEGIN
					IF @apptEndOp = '1'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,END_TMST) >= ''' + CONVERT(varchar,@EndDt) + '''' + ' AND CONVERT(DATE,END_TMST) <=''' + CONVERT(Varchar,DATEADD(day,180,@EndDt)) + ''''  

						END
					ELSE IF @apptEndOp = '2'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,END_TMST) >= ''' + CONVERT(varchar,DATEADD(day,-180,@EndDt)) + '''' + ' AND CONVERT(DATE,END_TMST) <=''' + CONVERT(Varchar,@EndDt) + ''''  

						END
					ELSE IF @apptEndOp = '3'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,END_TMST) = ''' + CONVERT(Varchar,@EndDt) + ''''  


						END
				END
			ELSE
				BEGIN
					SET @e_where += ' AND CONVERT(DATE,END_TMST) = ''' + CONVERT(Varchar,@EndDt) + ''''  


				END

		END
	ELSE
		BEGIN
			IF @apptEndOption != '0'
				BEGIN
					IF @apptEndOption = '1'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,END_TMST) >= '''  + CONVERT(varchar,(GETDATE() - 30)) +  '''' + ' AND CONVERT(DATE,END_TMST) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 

						END
					ELSE IF @apptEndOption = '2'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,END_TMST) >= '''  + CONVERT(varchar,(GETDATE() - 60)) +  '''' + ' AND CONVERT(DATE,END_TMST) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 

						END
					ELSE IF @apptEndOption = '3'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,END_TMST) >= '''  + CONVERT(varchar,(GETDATE() - 90)) +  '''' + ' AND CONVERT(DATE,END_TMST) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 

						END
					ELSE IF @apptEndOption = '4'
						BEGIN
							SET @e_where += ' AND CONVERT(DATE,END_TMST) >= '''  + CONVERT(varchar,(GETDATE() - 180)) +  '''' + ' AND CONVERT(DATE,END_TMST) <= '''  + CONVERT(varchar,(GETDATE())) +  '''' 


						END
				END
		END



	
	SET @e_finalSQL = @e_sqlEvent + @e_where
	IF @e_finalSQL != ''
		EXEC sp_executesql @e_finalSQL    

	 
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;


    IF (@FTNs = '' AND @SOWSIds = '' AND @AssignedUsers = '' AND @RequestorNames = '' AND @EventTypes = '' AND @EventIDs = '' AND @ADType = '' AND @CustName = '' 
	AND @FMSCkt = '' AND @EventStatus = '' AND @ActionType = '' AND @Device = '' AND @H5_H6 = '' AND @StrtDt = NULL AND @EndDt = Null AND @FRBID = ''
	AND @apptStartOp = '0' AND @apptStartOption = '0' AND @apptEndOp = '0' AND @apptEndOption = '0')

        BEGIN
              SELECT      TOP 0 '1' AS 'Event ID', '1' AS 'Event Type', '1' AS 'Event Status','1' AS 'FTN', '1' AS 'Assigned To',
                          '1' AS 'H1', '1' AS 'H5_H6', '1' AS 'CustName', '1' AS 'Customer Name', '1' AS 'SOWS Event ID',
                          '1' AS 'Requested By', '1' AS 'Page', '1' AS 'Start Date', '1' AS 'ModifiedDt', '' AS 'MDS_Type', '0' AS 'CSG_LVL_ID',
						  '1' AS 'CUST_NME', '1' AS 'City', '1' AS 'State'




              RETURN
        END
	ELSE

        BEGIN
			DECLARE @IsFilter	Bit
			DECLARE	@Select		nVarchar(Max)
			DECLARE @Filter		nVarchar(Max)
			DECLARE @SQL		nVarchar(Max)

			
			DECLARE	@MDSSelect	nVarchar(Max)
			DECLARE	@NewMDSSelect	nVarchar(Max)
			DECLARE	@FedSelect	nVarchar(Max)
			DECLARE	@RestSelect	nVarchar(Max)	
			DECLARE @MPLSSelect	nVarchar(Max)		
			DECLARE @UCaaSSelect nVarchar(Max)		
			DECLARE @SIPTSelect nVarchar(Max)		

			
			DECLARE @MDSFrom	nVarchar(Max)
			DECLARE @NewMDSFrom	nVarchar(Max)
			DECLARE	@ADFrom		nVarchar(Max)
			DECLARE	@MPLSFrom	nVarchar(Max)
			DECLARE	@NGVNFrom	nVarchar(Max)
			DECLARE	@SLFrom		nVarchar(Max)
			DECLARE	@FedFrom	nVarchar(Max)
			DECLARE	@UCaaSFrom	nVarchar(Max)
			DECLARE	@SIPTFrom	nVarchar(Max)

			
			DECLARE @NewMDSWhere	nVarchar(Max)
			DECLARE @MDSWhere	nVarchar(Max)
			DECLARE	@ADWhere	nVarchar(Max)
			DECLARE	@MPLSWhere	nVarchar(Max)
			DECLARE	@NGSLWhere	nVarchar(Max)
			DECLARE	@FedWhere	nVarchar(Max)
			DECLARE	@UCaaSWhere	nVarchar(Max)
			DECLARE	@SIPTWhere	nVarchar(Max)

			
			SET @IsFilter	=	0		
			SET	@Select		=	''
			SET	@SQL		=	''
			SET	@Filter		=	''

			
			SET	@NewMDSSelect	=	''
			SET	@MDSSelect	=	''
			SET	@RestSelect	=	''
			SET @FedSelect	=	''
			SET @MPLSSelect =	''
			SET @UCaaSSelect =	''
			SET @SIPTSelect =	''

			
			SET	@NewMDSFrom	=	''
			SET	@MDSFrom	=	''
			SET	@ADFrom		=	''
			SET	@MPLSFrom	=	''
			SET @NGVNFrom	=	''
			SET @SLFrom		=	''
			SET	@FedFrom	=	''
			SET	@UCaaSFrom	=	''
			SET	@SIPTFrom	=	''

			
			SET	@NewMDSWhere	=	''
			SET	@MDSWhere	=	''
			SET	@ADWhere	=	''
			SET	@MPLSWhere	=	''
			SET @NGSLWhere	=	''
			SET	@FedWhere	=	''
			SET	@UCaaSWhere	=	''
			SET	@SIPTWhere	=	''

			
			SET	@Select		=	'SELECT DISTINCT TOP 100	ev.EVENT_ID				AS  ''Event ID'',
															lt.EVENT_TYPE_NME		AS  ''Event Type'',
															es.EVENT_STUS_DES		AS	''Event Status'',
															[dbo].[GetCommaSepEventFTNs](ev.EVENT_ID) 		AS	FTN,
															[dbo].[GetSemiColonSepAssignUsers](ev.EVENT_ID)	AS  ''Assigned To'',	
															lr.DSPL_NME				AS	''Requested By'',
															ev.STRT_TMST	AS ''Start Date'',
															ev.MODFD_DT		AS ''Modified Date''


								'
			
			SET	@SIPTSelect		=	'SELECT DISTINCT TOP 100	ev.EVENT_ID				AS  ''Event ID'',
															lt.EVENT_TYPE_NME		AS  ''Event Type'',
															es.EVENT_STUS_DES		AS	''Event Status'',
															[dbo].[GetCommaSepEventFTNs](ev.EVENT_ID) 		AS	FTN,
															[dbo].[GetSemiColonSepAssignUsers](ev.EVENT_ID)	AS  ''Assigned To'',	
															lr.DSPL_NME				AS	''Requested By'',
															''''	AS ''Start Date'',
															ev.MODFD_DT		AS ''Modified Date''


								'

			SET	@MDSSelect	=	'							,''OLD'' AS ''MDS_Type''
															,ev1.CSG_LVL_ID
															,CASE WHEN ((SELECT COUNT(1)
															 FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1) THEN ''Multiple'' ELSE fe.H5_H6_CUST_ID END AS	''H5 H6''	
															,ev.H1_ID AS	''H1''
															,Case	When (ev1.CSG_LVL_ID>0)
																Then	''Private Customer''
																Else	ev.CUST_NME
															End 					AS	''Customer Name'' 
															,CASE WHEN ((ev.MDS_ACTY_TYPE_ID != 3) AND ((SELECT COUNT(1)
															 FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1)) THEN ''Multiple'' 
															 WHEN ((ev.MDS_ACTY_TYPE_ID != 3) AND ((SELECT COUNT(1)
															 FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) <= 1)) THEN Case	When (ev1.CSG_LVL_ID>0) Then ''Private'' Else fe.CTY_NME End 
															 WHEN ((ev.MDS_ACTY_TYPE_ID = 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_EVENT_DISCO WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1)) THEN ''Multiple'' 
															 WHEN ((ev.MDS_ACTY_TYPE_ID = 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_EVENT_DISCO WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) <= 1)) THEN Case	When (ev1.CSG_LVL_ID>0) Then ''Private'' Else '''' End
															 ELSE fe.CTY_NME END AS	''City''	
															,CASE WHEN ((ev.MDS_ACTY_TYPE_ID != 3) AND ((SELECT COUNT(1)
															 FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1)) THEN ''Multiple''
															 WHEN ((ev.MDS_ACTY_TYPE_ID != 3) AND ((SELECT COUNT(1)
															 FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) <= 1)) THEN Case	When (ev1.CSG_LVL_ID>0) Then ''Private'' Else fe.STT_PRVN_NME End
															 WHEN ((ev.MDS_ACTY_TYPE_ID = 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_EVENT_DISCO WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1)) THEN ''Multiple'' 
															 WHEN ((ev.MDS_ACTY_TYPE_ID = 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_EVENT_DISCO WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) <= 1)) THEN Case	When (ev1.CSG_LVL_ID>0) Then ''Private'' Else '''' End
															 ELSE fe.STT_PRVN_NME END	AS	''State''
															,CASE WHEN ((ev.MDS_ACTY_TYPE_ID != 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_MNGD_ACT_NEW WITH (NOLOCK)
															 WHERE event_id = ev.event_id) > 1)) THEN ''Multiple''
															 WHEN ((ev.MDS_ACTY_TYPE_ID != 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_MNGD_ACT_NEW WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) <= 1)) THEN mm.ODIE_DEV_NME 
															 WHEN ((ev.MDS_ACTY_TYPE_ID = 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_EVENT_DISCO WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1)) THEN ''Multiple'' 
															 WHEN ((ev.MDS_ACTY_TYPE_ID = 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_EVENT_DISCO WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) <= 1)) THEN med.ODIE_DEV_NME  
															 ELSE mm.ODIE_DEV_NME END	AS	''Device Name''
															 ,CASE WHEN ((SELECT COUNT(1)
															 FROM dbo.MDS_MNGD_ACT_NEW WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1) THEN dbo.getCommaSepRDSNs(ev.EVENT_ID, ''MDS'') ELSE mm.RDSN_NBR END	AS	''Redesign Number'''


			SET	@NewMDSSelect	=	'						,''NEW'' AS ''MDS_Type''
															,ev1.CSG_LVL_ID
															,CASE WHEN (ev.MDS_ACTY_TYPE_ID != 3) THEN ev.H6 ELSE dbo.getCommaSepDiscoEventH6s(ev.EVENT_ID) END AS	''H5 H6''
															,ev.H1 AS	''H1''
															,Case When	(ev1.CSG_LVL_ID>0)
																Then	''Private Customer''
																Else	ev.CUST_NME
															End 					AS	''Customer Name'' 
															,CASE WHEN (ev.MDS_ACTY_TYPE_ID != 3) THEN Case When (ev1.CSG_LVL_ID>0) Then ''Private'' Else ev.CTY_NME End 
															 WHEN (ev.MDS_ACTY_TYPE_ID = 3) THEN Case	When (ev1.CSG_LVL_ID>0) Then ''Private'' Else '''' End
															 ELSE medd.SITE_ID END AS	''City''	
															 ,CASE WHEN (ev.MDS_ACTY_TYPE_ID != 3) THEN Case When (ev1.CSG_LVL_ID>0) Then ''Private'' Else ev.STT_PRVN_NME End 
															 WHEN (ev.MDS_ACTY_TYPE_ID = 3) THEN Case	When (ev1.CSG_LVL_ID>0) Then ''Private'' Else '''' End
															 ELSE '''' END AS	''State''	
															,CASE WHEN ((ev.MDS_ACTY_TYPE_ID != 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_EVENT_ODIE_DEV WITH (NOLOCK)
															 WHERE event_id = ev.event_id) > 1)) THEN ''Multiple''
															 WHEN ((ev.MDS_ACTY_TYPE_ID != 3) AND ((SELECT COUNT(1)
															 FROM dbo.MDS_EVENT_ODIE_DEV WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) <= 1)) THEN meod.ODIE_DEV_NME 
															 WHEN ((ev.MDS_ACTY_TYPE_ID = 3) AND ((SELECT COUNT(1)
															 FROM dbo.EVENT_DISCO_DEV WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1)) THEN ''Multiple'' 
															 WHEN ((ev.MDS_ACTY_TYPE_ID = 3) AND ((SELECT COUNT(1)
															 FROM dbo.EVENT_DISCO_DEV WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) <= 1)) THEN medd.ODIE_DEV_NME  
															 ELSE meod.ODIE_DEV_NME END	AS	''Device Name''
															 ,CASE WHEN ((SELECT COUNT(1)
															 FROM dbo.MDS_EVENT_ODIE_DEV WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1) THEN dbo.getCommaSepRDSNs(ev.EVENT_ID, ''MDS'') ELSE meod.RDSN_NBR END	AS	''Redesign Number'''


			SET	@MPLSSelect	=	'							,'''' AS ''MDS_Type''
															,ev1.CSG_LVL_ID
															,ev.H6					AS	''H5 H6''
															,ev.H1					AS	''H1''
															,Case	When	(ev1.CSG_LVL_ID>0)
																Then	''Private Customer''
																Else	ev.CUST_NME
															End 					AS	''Customer Name''  
															,CASE WHEN ((SELECT COUNT(1)
															 FROM dbo.MPLS_EVENT_ACCS_TAG WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1) THEN ''Multiple'' ELSE tg.LOC_CTY_NME END AS	''City''	




															,CASE WHEN ((SELECT COUNT(1)
															 FROM dbo.MPLS_EVENT_ACCS_TAG WITH (NOLOCK)
															 WHERE EVENT_ID = ev.EVENT_ID) > 1) THEN ''Multiple'' ELSE tg.LOC_STT_NME END	AS	''State''



															 ,''''					AS  ''Device Name''
															 ,''''					AS  ''Redesign Number'''

			
			SET	@UCaaSSelect	=	'						,'''' AS ''MDS_Type''
															,ev1.CSG_LVL_ID
															,ev.H6					AS	''H5 H6''
															,ev.H1					AS	''H1''
															,Case	When	(ev1.CSG_LVL_ID>0)
																Then	''Private Customer''
																Else	ev.CUST_NME
															End 					AS	''Customer Name''
															,Case	When (ev1.CSG_LVL_ID>0) Then ''Private''
															 ELSE ev.CTY_NME END	AS	''City''	
															,Case	When (ev1.CSG_LVL_ID>0) Then ''Private''
															 ELSE ev.STT_PRVN_NME END AS	''State''
															,dbo.getCommaSepODIEDev(ev.EVENT_ID, ''UCaaS'')	AS  ''Device Name''
															,dbo.getCommaSepRDSNs(ev.EVENT_ID, ''UCaaS'') AS  ''Redesign Number''	'

															
			SET	@RestSelect	=	'							,'''' AS ''MDS_Type''
															,ev1.CSG_LVL_ID
															,ev.H6					AS	''H5 H6''
															,ev.H1					AS	''H1''
															,Case When	(ev1.CSG_LVL_ID>0)
																Then	''Private Customer''
																Else	ev.CUST_NME
															End 					AS	''Customer Name''
															,''''					AS	''City''	
															,''''					AS	''State''
															,''''					AS  ''Device Name''
															,''''					AS  ''Redesign Number''	'

															
			SET	@FedSelect	=	'SELECT DISTINCT TOP 1000	ev.EVENT_ID				AS  ''Event ID'',
															lt.EVENT_TYPE_NME		AS  ''Event Type'',
															es.EVENT_STUS_DES		AS	''Event Status'',

															''''					AS	FTN,
															''COWS Fedline Activators''	AS  ''Assigned To'',
															''FRM''					AS	''Requested By'',
															td.STRT_TME			AS	''Start Date'',
															td.REQ_RECV_DT			AS	''Modified Date'',
															''''					AS	''MDS_Type'',
															ev.CSG_LVL_ID			AS	''CSG_LVL_ID'',
															''''					AS	''H5 H6'',
															''''					AS	''H1'',
															''Private Customer''	AS	CUST_NME,
															''''					AS	''City'',
															''''					AS	''State'',
															''''					AS  ''Device Name'',
															ISNULL(fu.RDSN_NBR,'''')	AS  ''Redesign Number'''								

											   
			SET @MDSFrom		=	' FROM		dbo.MDS_EVENT_NEW				ev  WITH (NOLOCK)
									 LEFT JOIN	dbo.FSA_MDS_EVENT_NEW			fe	WITH (NOLOCK)	ON	ev.EVENT_ID			=	fe.EVENT_ID
									 INNER JOIN	dbo.EVENT						ev1	WITH (NOLOCK)	ON	ev.EVENT_ID			=	ev1.EVENT_ID
									 INNER JOIN	dbo.LK_EVENT_TYPE				lt	WITH (NOLOCK)	ON	ev1.EVENT_TYPE_ID	=	lt.EVENT_TYPE_ID
									 INNER JOIN	dbo.LK_EVENT_STUS				es	WITH (NOLOCK)	ON	ev.EVENT_STUS_ID	=	es.EVENT_STUS_ID
									 LEFT JOIN	dbo.MDS_EVENT_ACCS				fm	WITH (NOLOCK)	ON	fe.FSA_MDS_EVENT_ID	=	fm.FSA_MDS_EVENT_ID
									 LEFT JOIN	dbo.ORDR						od	WITH (NOLOCK)	ON	fm.ORDR_ID			=	od.ORDR_ID
									 LEFT JOIN	dbo.MDS_EVENT_ODIE_DEV_NME		md	WITH (NOLOCK)	ON	md.FSA_MDS_EVENT_ID	=	fe.FSA_MDS_EVENT_ID
									 LEFT JOIN	dbo.MDS_EVENT_MNS				oe	WITH (NOLOCK)	ON	oe.FSA_MDS_EVENT_ID	=	fe.FSA_MDS_EVENT_ID
									 LEFT JOIN	dbo.MDS_EVENT_WIRED_TRPT		wt	WITH (NOLOCK)	ON	wt.FSA_MDS_EVENT_ID	=	fe.FSA_MDS_EVENT_ID
									 LEFT JOIN	dbo.MDS_EVENT_CPE				cp	WITH (NOLOCK)	ON	cp.FSA_MDS_EVENT_ID	=	fe.FSA_MDS_EVENT_ID
									 LEFT JOIN	dbo.MDS_MNGD_ACT_NEW			mm	WITH (NOLOCK)	ON	mm.EVENT_ID			=	ev.EVENT_ID
									 LEFT JOIN	dbo.LK_USER						lr	WITH (NOLOCK)	ON	ev.REQOR_USER_ID	=	lr.USER_ID
									 LEFT JOIN	dbo.EVENT_ASN_TO_USER			ea	WITH (NOLOCK)	ON	ea.EVENT_ID			=	ev.EVENT_ID
																									AND	ea.REC_STUS_ID		=	1
									 LEFT JOIN	dbo.LK_USER						la	WITH (NOLOCK)	ON	ea.ASN_TO_USER_ID	=	la.USER_ID
									 LEFT JOIN	dbo.MDS_EVENT_DISCO				med WITH (NOLOCK)	ON  med.EVENT_ID = ev.EVENT_ID
									 LEFT JOIN	dbo.MDS_EVENT_MAC_ACTY			mmact WITH (NOLOCK) ON  ev.EVENT_ID	= mmact.EVENT_ID																									
									 LEFT JOIN   dbo.CUST_SCRD_DATA  csdev WITH (NOLOCK) ON csdev.SCRD_OBJ_ID=ev.EVENT_ID AND csdev.SCRD_OBJ_TYPE_ID=8
									 LEFT JOIN   dbo.CUST_SCRD_DATA  csdfe WITH (NOLOCK) ON csdfe.SCRD_OBJ_ID=fe.FSA_MDS_EVENT_ID AND csdfe.SCRD_OBJ_TYPE_ID=3
									 WHERE		ev.EVENT_STUS_ID	!=	8	'

									 
			SET @NewMDSFrom		=	' FROM		dbo.MDS_EVENT					ev  WITH (NOLOCK)
									 LEFT JOIN	dbo.MDS_EVENT_ODIE_DEV			meod	WITH (NOLOCK)	ON	ev.EVENT_ID			=	meod.EVENT_ID
									 INNER JOIN	dbo.EVENT						ev1	WITH (NOLOCK)	ON	ev.EVENT_ID			=	ev1.EVENT_ID
									 INNER JOIN	dbo.LK_EVENT_TYPE				lt	WITH (NOLOCK)	ON	ev1.EVENT_TYPE_ID	=	lt.EVENT_TYPE_ID
									 INNER JOIN	dbo.LK_EVENT_STUS				es	WITH (NOLOCK)	ON	ev.EVENT_STUS_ID	=	es.EVENT_STUS_ID
									 INNER JOIN #MDSNtwkActy					tmna WITH (NOLOCK) 	ON tmna.EVENT_ID 		=   ev.EVENT_ID
									 LEFT JOIN	dbo.EVENT_CPE_DEV				ecd	WITH (NOLOCK)	ON	ecd.EVENT_ID = ev.EVENT_ID AND ecd.REC_STUS_ID=1
									 LEFT JOIN	dbo.FSA_ORDR					fo	WITH (NOLOCK)	ON	fo.FTN			= ecd.CPE_ORDR_ID
									 LEFT JOIN	dbo.MDS_EVENT_SITE_SRVC			mess	WITH (NOLOCK)	ON	mess.EVENT_ID	=	ev.EVENT_ID
									 LEFT JOIN	dbo.MDS_EVENT_DSL_SBIC_CUST_TRPT medsc	WITH (NOLOCK)	ON	medsc.EVENT_ID = ev.EVENT_ID AND medsc.REC_STUS_ID=1
									 LEFT JOIN	dbo.MDS_EVENT_SLNK_WIRED_TRPT mesw	WITH (NOLOCK)	ON	mesw.EVENT_ID = ev.EVENT_ID AND mesw.REC_STUS_ID=1
									 LEFT JOIN	dbo.EVENT_DEV_CMPLT		edc	WITH (NOLOCK)	ON	edc.EVENT_ID = ev.EVENT_ID
									 LEFT JOIN	dbo.EVENT_DEV_SRVC_MGMT edsm	WITH (NOLOCK)	ON	edsm.EVENT_ID = ev.EVENT_ID AND edsm.REC_STUS_ID=1
									 LEFT JOIN	dbo.EVENT_DISCO_DEV			medd	WITH (NOLOCK)	ON	medd.EVENT_ID			=	ev.EVENT_ID
									 LEFT JOIN	dbo.LK_USER						lr	WITH (NOLOCK)	ON	ev.REQOR_USER_ID	=	lr.USER_ID
									 LEFT JOIN	dbo.EVENT_ASN_TO_USER			ea	WITH (NOLOCK)	ON	ea.EVENT_ID			=	ev.EVENT_ID
																									AND	ea.REC_STUS_ID		=	1
									 LEFT JOIN	dbo.LK_USER						la	WITH (NOLOCK)	ON	ea.ASN_TO_USER_ID	=	la.USER_ID
									 LEFT JOIN	dbo.MDS_EVENT_WRLS_TRPT			mewt WITH (NOLOCK)	ON  mewt.EVENT_ID = ev.EVENT_ID
									 LEFT JOIN	dbo.MDS_EVENT_MAC_ACTY			mmact WITH (NOLOCK) ON  ev.EVENT_ID	= mmact.EVENT_ID
									 LEFT JOIN   dbo.CUST_SCRD_DATA  csdev WITH (NOLOCK) ON csdev.SCRD_OBJ_ID=ev.EVENT_ID AND csdev.SCRD_OBJ_TYPE_ID=7
									 WHERE		ev.EVENT_STUS_ID	!=	8	'

									 
			SET @ADFrom		=	'	 FROM		dbo.AD_EVENT			ev  WITH (NOLOCK)
									 INNER JOIN	dbo.EVENT				ev1	WITH (NOLOCK)	ON	ev.EVENT_ID			=	ev1.EVENT_ID
									 INNER JOIN	dbo.LK_EVENT_TYPE		lt	WITH (NOLOCK)	ON	ev1.EVENT_TYPE_ID	=	lt.EVENT_TYPE_ID
									 INNER JOIN	dbo.LK_EVENT_STUS		es	WITH (NOLOCK)	ON	ev.EVENT_STUS_ID	=	es.EVENT_STUS_ID
									 LEFT JOIN	dbo.FSA_ORDR			fs	WITH (NOLOCK)	ON	ev.FTN				=	fs.FTN
									 LEFT JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	fs.ORDR_ID			=	od.ORDR_ID
									 LEFT JOIN	dbo.AD_EVENT_ACCS_TAG	tg	WITH (NOLOCK)	ON	ev.EVENT_ID			=	tg.EVENT_ID
									 LEFT JOIN	dbo.LK_USER				lr	WITH (NOLOCK)	ON	ev.REQOR_USER_ID	=	lr.USER_ID
									 LEFT JOIN	dbo.EVENT_ASN_TO_USER	ea	WITH (NOLOCK)	ON	ea.EVENT_ID			=	ev.EVENT_ID
																							AND	ea.REC_STUS_ID		=	1
									 LEFT JOIN	dbo.LK_USER				la	WITH (NOLOCK)	ON	ea.ASN_TO_USER_ID	=	la.USER_ID
									 LEFT JOIN	dbo.LK_ENHNC_SRVC		le	WITH (NOLOCK)	ON	ev.ENHNC_SRVC_ID	=	le.ENHNC_SRVC_ID 
									 LEFT JOIN   dbo.CUST_SCRD_DATA  csdev WITH (NOLOCK) ON csdev.SCRD_OBJ_ID=ev.EVENT_ID AND csdev.SCRD_OBJ_TYPE_ID=1
									 WHERE		ev.EVENT_STUS_ID	!=	8	'	

									 
			SET @MPLSFrom	=	'	 FROM		dbo.MPLS_EVENT			ev  WITH (NOLOCK)
									 INNER JOIN	dbo.EVENT				ev1	WITH (NOLOCK)	ON	ev.EVENT_ID			=	ev1.EVENT_ID
									 INNER JOIN	dbo.LK_EVENT_TYPE		lt	WITH (NOLOCK)	ON	ev1.EVENT_TYPE_ID	=	lt.EVENT_TYPE_ID
									 INNER JOIN	dbo.LK_EVENT_STUS		es	WITH (NOLOCK)	ON	ev.EVENT_STUS_ID	=	es.EVENT_STUS_ID
									 LEFT JOIN	dbo.FSA_ORDR			fs	WITH (NOLOCK)	ON	ev.FTN				=	fs.FTN
									 LEFT JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	fs.ORDR_ID			=	od.ORDR_ID
									 LEFT JOIN	dbo.MPLS_EVENT_ACCS_TAG	tg	WITH (NOLOCK)	ON	ev.EVENT_ID			=	tg.EVENT_ID

									 LEFT JOIN	dbo.LK_USER				lr	WITH (NOLOCK)	ON	ev.REQOR_USER_ID	=	lr.USER_ID
									 LEFT JOIN	dbo.EVENT_ASN_TO_USER	ea	WITH (NOLOCK)	ON	ea.EVENT_ID			=	ev.EVENT_ID
																							AND	ea.REC_STUS_ID		=	1
									 LEFT JOIN	dbo.LK_USER				la	WITH (NOLOCK)	ON	ea.ASN_TO_USER_ID	=	la.USER_ID
									 LEFT JOIN   dbo.CUST_SCRD_DATA  csdev WITH (NOLOCK) ON csdev.SCRD_OBJ_ID=ev.EVENT_ID AND csdev.SCRD_OBJ_TYPE_ID=9
									 WHERE		ev.EVENT_STUS_ID	!=	8	'	


			SET @NGVNFrom	=	'	 FROM		dbo.NGVN_EVENT			ev  WITH (NOLOCK)
									 INNER JOIN	dbo.EVENT				ev1	WITH (NOLOCK)	ON	ev.EVENT_ID			=	ev1.EVENT_ID
									 INNER JOIN	dbo.LK_EVENT_TYPE		lt	WITH (NOLOCK)	ON	ev1.EVENT_TYPE_ID	=	lt.EVENT_TYPE_ID
									 INNER JOIN	dbo.LK_EVENT_STUS		es	WITH (NOLOCK)	ON	ev.EVENT_STUS_ID	=	es.EVENT_STUS_ID
									 LEFT JOIN	dbo.FSA_ORDR			fs	WITH (NOLOCK)	ON	ev.FTN				=	fs.FTN
									 LEFT JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	fs.ORDR_ID			=	od.ORDR_ID
									 LEFT JOIN	dbo.LK_USER				lr	WITH (NOLOCK)	ON	ev.REQOR_USER_ID	=	lr.USER_ID
									 LEFT JOIN	dbo.EVENT_ASN_TO_USER	ea	WITH (NOLOCK)	ON	ea.EVENT_ID			=	ev.EVENT_ID
																							AND	ea.REC_STUS_ID		=	1
									 LEFT JOIN	dbo.LK_USER				la	WITH (NOLOCK)	ON	ea.ASN_TO_USER_ID	=	la.USER_ID
									 LEFT JOIN   dbo.CUST_SCRD_DATA  csdev WITH (NOLOCK) ON csdev.SCRD_OBJ_ID=ev.EVENT_ID AND csdev.SCRD_OBJ_TYPE_ID=11
									 WHERE		ev.EVENT_STUS_ID	!=	8	'


			SET @UCaaSFrom	=	'	 FROM		dbo.UCaaS_EVENT			ev  WITH (NOLOCK)
									 INNER JOIN	dbo.EVENT				ev1	WITH (NOLOCK)	ON	ev.EVENT_ID			=	ev1.EVENT_ID
									 INNER JOIN	dbo.LK_EVENT_TYPE		lt	WITH (NOLOCK)	ON	ev1.EVENT_TYPE_ID	=	lt.EVENT_TYPE_ID
									 INNER JOIN	dbo.LK_EVENT_STUS		es	WITH (NOLOCK)	ON	ev.EVENT_STUS_ID	=	es.EVENT_STUS_ID
									 LEFT JOIN	dbo.EVENT_CPE_DEV		uc	WITH (NOLOCK)	ON	ev.EVENT_ID	=	uc.EVENT_ID AND uc.REC_STUS_ID=1
									 LEFT JOIN	dbo.UCaaS_EVENT_ODIE_DEV		uo	WITH (NOLOCK)	ON	ev.EVENT_ID	=	uo.EVENT_ID
									 LEFT JOIN	dbo.LK_USER				lr	WITH (NOLOCK)	ON	ev.REQOR_USER_ID	=	lr.USER_ID
									 LEFT JOIN	dbo.EVENT_ASN_TO_USER	ea	WITH (NOLOCK)	ON	ea.EVENT_ID			=	ev.EVENT_ID
																							AND	ea.REC_STUS_ID		=	1
									 LEFT JOIN	dbo.LK_USER				la	WITH (NOLOCK)	ON	ea.ASN_TO_USER_ID	=	la.USER_ID
									 LEFT JOIN   dbo.CUST_SCRD_DATA  csdev WITH (NOLOCK) ON csdev.SCRD_OBJ_ID=ev.EVENT_ID AND csdev.SCRD_OBJ_TYPE_ID=20
									 WHERE		ev.EVENT_STUS_ID	!=	8	'

			
			SET @SIPTFrom	=	'	 FROM		dbo.SIPT_EVENT			ev  WITH (NOLOCK)

									 INNER JOIN	dbo.EVENT				ev1	WITH (NOLOCK)	ON	ev.EVENT_ID			=	ev1.EVENT_ID
									 INNER JOIN	dbo.LK_EVENT_TYPE		lt	WITH (NOLOCK)	ON	ev1.EVENT_TYPE_ID	=	lt.EVENT_TYPE_ID
									 INNER JOIN	dbo.LK_EVENT_STUS		es	WITH (NOLOCK)	ON	ev.EVENT_STUS_ID	=	es.EVENT_STUS_ID
									 LEFT JOIN	dbo.LK_USER				lr	WITH (NOLOCK)	ON	ev.REQOR_USER_ID	=	lr.USER_ID
									 LEFT JOIN	dbo.EVENT_ASN_TO_USER	ea	WITH (NOLOCK)	ON	ea.EVENT_ID			=	ev.EVENT_ID
																							AND	ea.REC_STUS_ID		=	1
									 LEFT JOIN	dbo.LK_USER				la	WITH (NOLOCK)	ON	ea.ASN_TO_USER_ID	=	la.USER_ID
									 LEFT JOIN   dbo.CUST_SCRD_DATA  csdev WITH (NOLOCK) ON csdev.SCRD_OBJ_ID=ev.EVENT_ID AND csdev.SCRD_OBJ_TYPE_ID=18
									 WHERE		ev.EVENT_STUS_ID	!=	8	'
									 
			
			SET @SLFrom		=	'	 FROM		dbo.SPLK_EVENT			ev  WITH (NOLOCK)
									 INNER JOIN	dbo.EVENT				ev1	WITH (NOLOCK)	ON	ev.EVENT_ID			=	ev1.EVENT_ID
									 INNER JOIN	dbo.LK_EVENT_TYPE		lt	WITH (NOLOCK)	ON	ev1.EVENT_TYPE_ID	=	lt.EVENT_TYPE_ID
									 INNER JOIN	dbo.LK_EVENT_STUS		es	WITH (NOLOCK)	ON	ev.EVENT_STUS_ID	=	es.EVENT_STUS_ID
									 LEFT JOIN	dbo.FSA_ORDR			fs	WITH (NOLOCK)	ON	ev.FTN				=	fs.FTN
									 LEFT JOIN	dbo.ORDR				od	WITH (NOLOCK)	ON	fs.ORDR_ID			=	od.ORDR_ID
									 LEFT JOIN	dbo.LK_USER				lr	WITH (NOLOCK)	ON	ev.REQOR_USER_ID	=	lr.USER_ID
									 LEFT JOIN	dbo.EVENT_ASN_TO_USER	ea	WITH (NOLOCK)	ON	ea.EVENT_ID			=	ev.EVENT_ID
																							AND	ea.REC_STUS_ID		=	1
									 LEFT JOIN	dbo.LK_USER				la	WITH (NOLOCK)	ON	ea.ASN_TO_USER_ID	=	la.USER_ID
									 LEFT JOIN   dbo.CUST_SCRD_DATA  csdev WITH (NOLOCK) ON csdev.SCRD_OBJ_ID=ev.EVENT_ID AND csdev.SCRD_OBJ_TYPE_ID=19
									 WHERE		ev.EVENT_STUS_ID	!=	8	'		 
			
			SET	@FedFrom	=	'	FROM		dbo.EVENT						ev	WITH (NOLOCK)
									INNER JOIN	dbo.LK_EVENT_TYPE				lt	WITH (NOLOCK)	ON	ev.EVENT_TYPE_ID	=	lt.EVENT_TYPE_ID	
									INNER JOIN	dbo.FEDLINE_EVENT_USER_DATA		fu	WITH (NOLOCK)	ON	ev.EVENT_ID			=	fu.EVENT_ID AND fu.EVENT_STUS_ID <> 8
									INNER JOIN	dbo.FEDLINE_EVENT_TADPOLE_DATA	td	WITH (NOLOCK)	ON	ev.EVENT_ID			=	td.EVENT_ID
																									AND	td.REC_STUS_ID		=	1
									INNER JOIN  dbo.LK_EVENT_STUS				es	WITH (NOLOCK)	ON	es.EVENT_STUS_ID	=	fu.EVENT_STUS_ID 
									INNER JOIN	dbo.LK_FEDLINE_ORDR_TYPE		fot	WITH (NOLOCK)	ON	td.ORDR_TYPE_CD		=	fot.ORDR_TYPE_CD
									
									WHERE		1 = 1	'

			---------------------------------------------------------------
			--	FTN Filter 
			---------------------------------------------------------------
			IF    @FTNs <> ''
				BEGIN
				  SET @FTNs = REPLACE(@FTNs, '*', '%')
				  SET @FTNs = REPLACE(@FTNs, '\r\n', ',')
				  SET @IsFilter = 1 

				  --If there is a comma, do the comma method
				  IF CHARINDEX(',',@FTNs) > 0 
						BEGIN
							SET @FTNs = REPLACE(@FTNs, ',', ''',''')
							SET @ADWhere 	+=	' AND ev.FTN IN (''' + @FTNs + ''') '	
							SET @MPLSWhere	+=	' AND ev.FTN IN (''' + @FTNs + ''') '	
							SET @NGSLWhere	+=	' AND ev.FTN IN (''' + @FTNs + ''') '
							SET @MDSWhere	+=	' AND ( cp.CPE_FTN_NBR IN (''' + @FTNs + ''') OR oe.MNS_FTN_NBR IN (''' + @FTNs + ''') OR fm.ACCS_FTN_NBR IN (''' + @FTNs + ''') )'
							SET @NewMDSWhere	+=	' AND ( ecd.CPE_ORDR_ID IN (''' + @FTNs + ''') OR edsm.MNS_ORDR_NBR IN (''' + @FTNs + ''') )'
							SET @FedWhere	+=	' AND 1 = 0'
							SET @UCaaSWhere	+=	' AND 1 = 0'
							SET @SIPTWhere	+=	' AND ev.M5_ORDR_NBR IN (''' + @FTNs + ''') '
						END
				  --Else if we have a wildcard
				  ELSE IF CHARINDEX('%',@FTNs) > 0
						BEGIN
							SET @ADWhere	+=	' AND ev.FTN LIKE (''' + @FTNs + ''') '
							SET @MPLSWhere	+=	' AND ev.FTN LIKE (''' + @FTNs + ''') '
							SET @NGSLWhere	+=	' AND ev.FTN LIKE (''' + @FTNs + ''') '
							SET @MDSWhere	+=	' AND ( cp.CPE_FTN_NBR LIKE (''' + @FTNs + ''') OR oe.MNS_FTN_NBR LIKE (''' + @FTNs + ''') OR fm.ACCS_FTN_NBR LIKE (''' + @FTNs + ''') )'	
							SET @NewMDSWhere	+=	' AND ( ecd.CPE_ORDR_ID LIKE (''' + @FTNs + ''') OR edsm.MNS_ORDR_NBR LIKE (''' + @FTNs + ''') )'							
							SET @FedWhere	+=	' AND 1 = 0'
							SET @UCaaSWhere	+=	' AND 1 = 0'
							SET @SIPTWhere	+=	' AND ev.M5_ORDR_NBR LIKE (''' + @FTNs + ''') '
						END
						
				  ELSE
					BEGIN
							SET @ADWhere	+=	' AND ev.FTN IN (''' + @FTNs + ''') '
							SET @MPLSWhere	+=	' AND ev.FTN IN (''' + @FTNs + ''') '
							SET @NGSLWhere	+=	' AND ev.FTN IN (''' + @FTNs + ''') '
							SET @MDSWhere	+=	' AND ( cp.CPE_FTN_NBR IN (''' + @FTNs + ''') OR oe.MNS_FTN_NBR IN (''' + @FTNs + ''') OR fm.ACCS_FTN_NBR IN (''' + @FTNs + ''') )'
							SET @NewMDSWhere	+=	' AND ( ecd.CPE_ORDR_ID IN (''' + @FTNs + ''') OR edsm.MNS_ORDR_NBR IN (''' + @FTNs + ''') )'
							SET @FedWhere	+=	' AND 1 = 0'
							SET @UCaaSWhere	+=	' AND 1 = 0'
							SET @SIPTWhere	+=	' AND ev.M5_ORDR_NBR IN (''' + @FTNs + ''') '
					END
             END
            
			---------------------------------------------------------------
			--	EVENT_ID Filter 
			---------------------------------------------------------------
			IF	@EventIDs	<>	''
				BEGIN
					SET @Filter		+=	' AND ev.EVENT_ID IN (' + @EventIDs + ') '
					SET @FedWhere	+=	' AND ev.EVENT_ID IN (' + @EventIDs + ') '
					SET @UCaaSWhere	+=	' AND ev.EVENT_ID IN (' + @EventIDs + ') '
					SET @SIPTWhere	+=	' AND ev.EVENT_ID IN (' + @EventIDs + ') '
					SET @IsFilter = 1
				END
				
			---------------------------------------------------------------
			--	SOWS_EVENT_ID Filter 
			---------------------------------------------------------------
			IF	@SOWSIds	<>	''
				BEGIN
					SET @Filter	+=	' AND ev.SOWS_EVENT_ID IN (' + @SOWSIds + ') '		
					SET @FedWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @IsFilter = 1						
				END	
				
			 ---------------------------------------------------------------
			--	EVENT_TYPE_ID Filter 
			---------------------------------------------------------------
			IF @EventTypes <> ''
				BEGIN

					IF	@EventTypes	=	6
						BEGIN
							SET @MDSWhere	+=	' AND	ev.MDS_FAST_TRK_CD	=	1	AND	lt.EVENT_TYPE_ID = (5) '	
							SET @MDSWhere	+=	' AND	ISNULL(ev.MDS_FAST_TRK_TYPE_ID,'') <> '' AND	lt.EVENT_TYPE_ID = (5) '	
							SET @ADWhere	+=	' AND 1 = 0' 	
							SET @MPLSWhere	+=	' AND 1 = 0' 
							SET @NGSLWhere	+=	' AND 1 = 0' 					
							SET @FedWhere	+=	' AND 1 = 0'
							SET @UCaaSWhere	+=	' AND 1 = 0'
							SET @SIPTWhere	+=	' AND 1 = 0'
						END
					ELSE  
						SET @Filter		+=	' AND	lt.EVENT_TYPE_ID = (' + @EventTypes + ') '
						SET @FedWhere	+=	' AND	lt.EVENT_TYPE_ID = (' + @EventTypes + ') '	
						SET @UCaaSWhere	+=	' AND	lt.EVENT_TYPE_ID = (' + @EventTypes + ') '	
						SET @SIPTWhere	+=	' AND	lt.EVENT_TYPE_ID = (' + @EventTypes + ') '	
					SET @IsFilter = 1					                  
				END
				
			---------------------------------------------------------------
			--	Assigned User Filter 
			---------------------------------------------------------------
			IF	@AssignedUsers	<>	''
				BEGIN
					SET @Filter	+=	' AND CHARINDEX(''' + @AssignedUsers + ''', la.DSPL_NME) > 0 '
					IF CHARINDEX(@AssignedUsers, 'COWS Fedline Activators') > 0
						SET @FedWhere	+=	' AND 1 = 1'
					ELSE
						SET @FedWhere	+=	' AND 1 = 0'


					SET @IsFilter = 1
				END	
				
			---------------------------------------------------------------
			--	Requestor User Filter 
			---------------------------------------------------------------
			IF	@RequestorNames	<>	''
				BEGIN
					SET @Filter	+=	' AND CHARINDEX(''' + @RequestorNames + ''', lr.DSPL_NME) > 0 '
					IF CHARINDEX(@RequestorNames, 'FRM') > 0
						SET @FedWhere	+=	' AND 1 = 1'
					ELSE
						SET @FedWhere	+=	' AND 1 = 0'
					SET @IsFilter = 1								
				END		
				
			---------------------------------------------------------------
			--	AD Type Filter 
			---------------------------------------------------------------
			IF	@ADType	<>	''
				BEGIN
				
					SET @ADWhere	+=	' AND le.ENHNC_SRVC_NME LIKE ''%' + @ADType + '%'' AND   ev.EVENT_STUS_ID  != 6	'
					SET @MDSWhere	+=	' AND 1 = 0' 	
					SET @NewMDSWhere	+=	' AND 1 = 0' 	
					SET @MPLSWhere	+=	' AND 1 = 0' 
					SET @NGSLWhere	+=	' AND 1 = 0' 	
					SET @FedWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'




					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @IsFilter = 1
				END	
				
			---------------------------------------------------------------
			--	Customer Name Filter 
			---------------------------------------------------------------
			IF	@CustName	<>	''
				BEGIN
					SET @MDSWhere	+=	' AND CHARINDEX(''' + @CustName + ''', CASE WHEN (ev1.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csdev.CUST_NME) ELSE ev.CUST_NME END) > 0 '
					SET @NewMDSWhere	+=	' AND CHARINDEX(''' + @CustName + ''', CASE WHEN (ev1.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csdev.CUST_NME) ELSE ev.CUST_NME END) > 0 '
					SET @ADWhere	+=	' AND CHARINDEX(''' + @CustName + ''', CASE WHEN (ev1.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csdev.CUST_NME) ELSE ev.CUST_NME END) > 0 '
					SET @MPLSWhere	+=	' AND CHARINDEX(''' + @CustName + ''', CASE WHEN (ev1.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csdev.CUST_NME) ELSE ev.CUST_NME END) > 0 '
					SET @NGSLWhere	+=	' AND CHARINDEX(''' + @CustName + ''', CASE WHEN (ev1.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csdev.CUST_NME) ELSE ev.CUST_NME END) > 0 '
					SET @FedWhere	+=	' AND CHARINDEX(''' + @CustName + ''', dbo.decryptBinaryData(td.CUST_NME)) > 0 '
					SET @UCaaSWhere	+=	' AND CHARINDEX(''' + @CustName + ''', CASE WHEN (ev1.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csdev.CUST_NME) ELSE ev.CUST_NME END) > 0 '



					SET @SIPTWhere	+=	' AND CHARINDEX(''' + @CustName + ''', CASE WHEN (ev1.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csdev.CUST_NME) ELSE ev.CUST_NME END) > 0 '
					SET @IsFilter = 1
				END		
              
            ---------------------------------------------------------------
			--	FMS Circuit Filter 
			---------------------------------------------------------------
			IF	@FMSCkt	<>	''
				BEGIN
					SET @MDSWhere	+=	' AND CHARINDEX (''' + @FMSCkt + ''', wt.FMS_CKT_ID) > 0'
					SET @NewMDSWhere	+=	' AND ((CHARINDEX (''' + @FMSCkt + ''', medsc.VNDR_PRVDR_TRPT_CKT_ID) > 0) OR (CHARINDEX (''' + @FMSCkt + ''', mesw.VNDR_PRVDR_TRPT_CKT_ID) > 0))'
					SET @ADWhere	+=	' AND CHARINDEX (''' + @FMSCkt + ''', tg.CKT_ID) > 0'	
					SET @MPLSWhere	+=	' AND CHARINDEX (''' + @FMSCkt + ''', tg.PL_DAL_CKT_NBR) > 0'
					SET @NGSLWhere	+=	' AND 1 = 0'	
					SET @FedWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @IsFilter = 1


				END

			---------------------------------------------------------------
			--	Event Status Filter 
			---------------------------------------------------------------
			IF	@EventStatus	<>	''
				BEGIN
					SET @Filter		+=	' AND CHARINDEX (''' + @EventStatus + ''', es.EVENT_STUS_DES) > 0'	
					SET @FedWhere	+=	' AND CHARINDEX (''' + @EventStatus + ''', es.EVENT_STUS_DES) > 0'
					SET @IsFilter	=	1


				END
			
			---------------------------------------------------------------
			--	Action Type Filter 
			---------------------------------------------------------------
			IF	@ActionType	<>	''
				BEGIN
					IF @ActionType = 'Standard'
						BEGIN
							SET @ActionType = 'S'


						END
					ELSE 
						IF @ActionType = 'At Will'
						BEGIN
							SET @ActionType = 'A'


						END
						
					SET @MDSWhere	+=	' AND CHARINDEX (''' + @ActionType + ''', ev.MDS_FAST_TRK_TYPE_ID) > 0'	
					SET @NewMDSWhere	+=	' AND CHARINDEX (''' + @ActionType + ''', ev.MDS_FAST_TRK_TYPE_ID) > 0'	
					SET @ADWhere	+=	' AND 1 = 0' 	
					SET @MPLSWhere	+=	' AND 1 = 0' 	
					SET @NGSLWhere	+=	' AND 1 = 0'	
					SET @FedWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @IsFilter = 1


				END
			
			---------------------------------------------------------------
			--	Device Filter 
			---------------------------------------------------------------
			IF	@Device	<>	''
				BEGIN
					SET @MDSWhere	+=	' AND ((CHARINDEX (''' + @Device + ''', mm.ODIE_DEV_NME) > 0) OR (CHARINDEX (''' + @Device + ''', med.ODIE_DEV_NME) > 0))'	
					SET @NewMDSWhere	+=	' AND ((CHARINDEX (''' + @Device + ''', meod.ODIE_DEV_NME) > 0) OR (CHARINDEX (''' + @Device + ''', medd.ODIE_DEV_NME) > 0) OR (CHARINDEX (''' + @Device + ''', edsm.ODIE_DEV_NME) > 0) OR (CHARINDEX (''' + @Device + ''', ecd.ODIE_DEV_NME) > 0))'

					SET @ADWhere	+=	' AND 1 = 0' 
					SET @MPLSWhere	+=	' AND 1 = 0' 	
					SET @NGSLWhere	+=	' AND 1 = 0'	
					SET @FedWhere	+=	' AND CHARINDEX (''' + @Device + ''', td.DEV_NME) > 0'
					SET @UCaaSWhere	+=	' AND ((CHARINDEX (''' + @Device + ''', uc.ODIE_DEV_NME) > 0) OR (CHARINDEX (''' + @Device + ''', uo.ODIE_DEV_NME) > 0))'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @IsFilter = 1


				END
			
			---------------------------------------------------------------
			--	H5/H6 CUST_ID Filter 
			---------------------------------------------------------------
			IF	@H5_H6	<>	''
				BEGIN
					SET @MDSWhere	+=	' AND CHARINDEX (''' + @H5_H6 + ''', fe.H5_H6_CUST_ID) > 0'	
					SET @NewMDSWhere	+=	' AND CHARINDEX (''' + @H5_H6 + ''', ev.H6) > 0'	
					SET @ADWhere	+=	' AND CHARINDEX (''' + @H5_H6 + ''', ev.H6) > 0'	
					SET @MPLSWhere	+=	' AND CHARINDEX (''' + @H5_H6 + ''', ev.H6) > 0'
					SET @NGSLWhere	+=	' AND CHARINDEX (''' + @H5_H6 + ''', ev.H6) > 0'	
					SET @UCaaSWhere	+=	' AND CHARINDEX (''' + @H5_H6 + ''', ev.H6) > 0'
					SET @SIPTWhere	+=	' AND CHARINDEX (''' + @H5_H6 + ''', ev.H6) > 0'
					SET @FedWhere	+=	' AND 1 = 0'
					SET @IsFilter = 1


				END
			
			---------------------------------------------------------------
			--	Start Date and/or End Date Filter 
			---------------------------------------------------------------
			IF @StrtDt IS NOT NULL or @EndDt IS NOT NULL OR ISNULL(@apptStartOption,'0') != '0' OR ISNULL(@apptEndOption,'0') != '0'
				BEGIN
					SET @Filter		+=	' AND ev.EVENT_ID IN (SELECT EventID FROM #Events)'	
					SET @FedWhere	+=	' AND ev.EVENT_ID IN (SELECT EventID FROM #Events)'
					SET @IsFilter	=	1


				END

			---------------------------------------------------------------
			--	FRB ID Filter 
			---------------------------------------------------------------
			IF	@FRBID	<>	''
				BEGIN
					SET @MDSWhere	+=	' AND 1 = 0'	
					SET @NewMDSWhere	+=	' AND 1 = 0'	
					SET @ADWhere	+=	' AND 1 = 0'
					SET @MPLSWhere	+=	' AND 1 = 0'
					SET @NGSLWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @FedWhere	+=	' AND td.FRB_REQ_ID = ' + @FRBID
					SET @IsFilter = 1


				END

			---------------------------------------------------------------
			--	Device Serial Number Filter 
			---------------------------------------------------------------
			IF	@Serial	<>	''
				BEGIN
					SET @MDSWhere	+=	' AND 1 = 0'	
					SET @NewMDSWhere	+=	' AND 1 = 0'	
					SET @ADWhere	+=	' AND 1 = 0'
					SET @MPLSWhere	+=	' AND 1 = 0'
					SET @NGSLWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @FedWhere	+=	' AND td.DEV_SERIAL_NBR = ''' + @Serial + ''''
					SET @IsFilter = 1

				END
			---------------------------------------------------------------
			--	FRB Design Type Filter 
			---------------------------------------------------------------
			IF	@DesignType	<>	''
				BEGIN
					SET @MDSWhere	+=	' AND 1 = 0'	
					SET @NewMDSWhere	+=	' AND 1 = 0'	
					SET @ADWhere	+=	' AND 1 = 0'
					SET @MPLSWhere	+=	' AND 1 = 0'
					SET @NGSLWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @FedWhere	+=	' AND td.DSGN_TYPE_CD = ''' + @DesignType + ''''
					SET @IsFilter = 1


				END

			---------------------------------------------------------------
			--	Org ID Filter 
			---------------------------------------------------------------
			IF	@OrgID	<>	''
				BEGIN
					SET @MDSWhere	+=	' AND 1 = 0'	
					SET @NewMDSWhere	+=	' AND 1 = 0'	
					SET @ADWhere	+=	' AND 1 = 0'
					SET @MPLSWhere	+=	' AND 1 = 0'
					SET @NGSLWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @FedWhere	+=	' AND td.ORG_ID = ' + @OrgID
					SET @IsFilter = 1


				END

			IF @Status <> ''
				BEGIN
					IF (@Status = '1')
					BEGIN
						SET @MDSWhere	+=	' AND (ev.EVENT_STUS_ID = 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @NewMDSWhere	+=	' AND (ev.EVENT_STUS_ID = 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @ADWhere	+=	' AND (ev.EVENT_STUS_ID = 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @MPLSWhere	+=	' AND (ev.EVENT_STUS_ID = 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @NGSLWhere	+=	' AND (ev.EVENT_STUS_ID = 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @UCaaSWhere	+=	' AND (ev.EVENT_STUS_ID = 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @SIPTWhere	+=	' AND (ev.EVENT_STUS_ID = 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @FedWhere	+=	' AND (fu.EVENT_STUS_ID IN (6, 14)) AND (td.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @IsFilter = 1

					END
					ELSE
					BEGIN
						SET @MDSWhere	+=	' AND (ev.EVENT_STUS_ID <> 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @NewMDSWhere	+=	' AND (ev.EVENT_STUS_ID <> 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -45, GETDATE())) + ''')'
						SET @ADWhere	+=	' AND (ev.EVENT_STUS_ID <> 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @MPLSWhere	+=	' AND (ev.EVENT_STUS_ID <> 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @NGSLWhere	+=	' AND (ev.EVENT_STUS_ID <> 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @UCaaSWhere	+=	' AND (ev.EVENT_STUS_ID <> 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @SIPTWhere	+=	' AND (ev.EVENT_STUS_ID <> 6) AND (ev.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @FedWhere	+=	' AND fu.EVENT_STUS_ID NOT IN (6, 14, 13) AND (td.CREAT_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @IsFilter = 1



					END
				END

			IF @ReDesign <> ''
			BEGIN
					SET @MDSWhere	+=	' AND CHARINDEX (''' + @ReDesign + ''', mm.RDSN_NBR) > 0'
					SET @NewMDSWhere	+=	' AND CHARINDEX (''' + @ReDesign + ''', meod.RDSN_NBR) > 0'
					SET @ADWhere	+=	' AND 1 = 0'
					SET @MPLSWhere	+=	' AND 1 = 0'
					SET @NGSLWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND CHARINDEX (''' + @ReDesign + ''', uo.RDSN_NBR) > 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @FedWhere	+=	' AND CHARINDEX (''' + @ReDesign + ''', fu.RDSN_NBR) > 0'
					SET @IsFilter = 1
			END


			---------------------------------------------------------------
			--	FRB Activity Type Filter 
			---------------------------------------------------------------
			IF	@ActType	<>	''
				BEGIN
					SET @MDSWhere	+=	' AND 1 = 0'	
					SET @NewMDSWhere	+=	' AND 1 = 0'	
					SET @ADWhere	+=	' AND 1 = 0'
					SET @MPLSWhere	+=	' AND 1 = 0'
					SET @NGSLWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @FedWhere	+=	' AND fot.PRNT_ORDR_TYPE_DES = ''' + @ActType + ''''
					SET @IsFilter = 1


				END

			------------------------------------------------------------------
			-- MDS Activity Type && MDS MAC Type
			------------------------------------------------------------------
			IF @MDSActivityType <> ''
				BEGIN
					SET @ADWhere	+=	' AND 1 = 0'
					SET @MPLSWhere	+=	' AND 1 = 0'
					SET @NGSLWhere	+=	' AND 1 = 0'
					SET @FedWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @MDSWhere	+= ' AND ev.MDS_ACTY_TYPE_ID IN (''' + @MDSActivityType + ''')'
					SET @NewMDSWhere	+= ' AND ev.MDS_ACTY_TYPE_ID IN (''' + @MDSActivityType + ''')'
					SET @IsFilter = 1


				END

			IF @MDSMACType <> ''
				BEGIN
					SET @ADWhere	+=	' AND 1 = 0'
					SET @MPLSWhere	+=	' AND 1 = 0'
					SET @NGSLWhere	+=	' AND 1 = 0'
					SET @FedWhere	+=	' AND 1 = 0'
					SET @UCaaSWhere	+=	' AND 1 = 0'
					SET @SIPTWhere	+=	' AND 1 = 0'
					SET @MDSWhere	+= ' AND mmact.MDS_MAC_ACTY_ID IN (' + @MDSMACType + ')'
					SET @NewMDSWhere	+= ' AND mmact.MDS_MAC_ACTY_ID IN (' + @MDSMACType + ')'
					SET @IsFilter = 1
				END
				
			IF (LEN(@MDSNtwkActyType)>0)
			BEGIN
				SET @IsFilter = 1
			END

			
			SET @SQL	=		@DropTbl + 	@CrtTbl	+ 
						CASE WHEN (@MDSNtwkActyType='1' OR @MDSNtwkActyType='') THEN @InsrtTbl +	@Select		+	@MDSSelect	+	@MDSFrom	+	@MDSWhere	+	@Filter + @OrdrBy ELSE '' END
						+   @InsrtTbl 	+	@Select		+	@NewMDSSelect	+	@NewMDSFrom	+	@NewMDSWhere	+	@Filter + @OrdrBy
						--+	@InsrtTbl	+	@Select		+	@RestSelect	+	@ADFrom		+	@ADWhere	+	@Filter + @OrdrBy
						--+	@InsrtTbl	+	@Select		+	@MPLSSelect	+	@MPLSFrom	+	@MPLSWhere	+	@Filter + @OrdrBy
						--+	@InsrtTbl	+	@Select		+	@RestSelect	+	@NGVNFrom	+	@NGSLWhere	+	@Filter + @OrdrBy
						--+	@InsrtTbl	+	@Select		+	@RestSelect	+	@SLFrom		+	@NGSLWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@Select		+	@UCaaSSelect	+	@UCaaSFrom	+	@UCaaSWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@SIPTSelect		+	@RestSelect	+	@SIPTFrom	+	@SIPTWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@FedSelect	+	@FedFrom	+	@FedWhere  + @OrdrBy
						+	@FinalSel	+  ' WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ORDER BY EVENT_ID DESC'


			-- Remove CAND Events on Search result set
			-- Include them on Archive
			IF @REQ_BY_USR_ID > 1
				BEGIN
					DECLARE @PROFILECNT	INT = 0
					DECLARE @ISARCHIVE	BIT = 0
					SET @PROFILECNT = (SELECT COUNT(USR_PRF_ID)  FROM MAP_USR_PRF WHERE USER_ID = @REQ_BY_USR_ID AND REC_STUS_ID = 1)
					SET @ISARCHIVE = (SELECT COUNT(USR_PRF_ID)  FROM MAP_USR_PRF WHERE USER_ID = @REQ_BY_USR_ID AND USR_PRF_ID = 228 AND REC_STUS_ID = 1)

					IF (@PROFILECNT = 1 AND @ISARCHIVE = 1)
					BEGIN
						SET @SQL	=		@DropTbl + 	@CrtTbl	+ 
						CASE WHEN (@MDSNtwkActyType='1' OR @MDSNtwkActyType='') THEN @InsrtTbl +	@Select		+	@MDSSelect	+	@MDSFrom	+	@MDSWhere	+	@Filter + @OrdrBy ELSE '' END
						+   @InsrtTbl 	+	@Select		+	@NewMDSSelect	+	@NewMDSFrom	+	@NewMDSWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@Select		+	@RestSelect	+	@ADFrom		+	@ADWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@Select		+	@MPLSSelect	+	@MPLSFrom	+	@MPLSWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@Select		+	@RestSelect	+	@NGVNFrom	+	@NGSLWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@Select		+	@RestSelect	+	@SLFrom		+	@NGSLWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@Select		+	@UCaaSSelect	+	@UCaaSFrom	+	@UCaaSWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@SIPTSelect		+	@RestSelect	+	@SIPTFrom	+	@SIPTWhere	+	@Filter + @OrdrBy
						+	@InsrtTbl	+	@FedSelect	+	@FedFrom	+	@FedWhere  + @OrdrBy
						+	@FinalSel	+  ' WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ORDER BY EVENT_ID DESC'
					END
				END
			
			--select @SQL

			
            IF @IsFilter = 1
				EXEC sp_executesql @SQL
	

		END
End Try


Begin Catch
	--EXEC [dbo].[insertErrorInfo]
End Catch
END
