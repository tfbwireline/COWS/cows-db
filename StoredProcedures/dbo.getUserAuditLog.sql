USE [COWS]
GO
_CreateObject 'SP','dbo','getUserAuditLog'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================  
-- Author:  vn370313  
-- Create date: 01/23/2018  
-- Description: Gets data for RuleSetProcess Service.  
-- =========================================================  
ALTER PROCEDURE [dbo].[getUserAuditLog]  
        
AS  
BEGIN TRY  
      
	select	USER_AD_ID as ADID, 
			SESSN_ID as 'SessionID', 
			IP_ADR as 'IP Address', 
			'Login' as 'EventType', 
			'Success' as 'Action', 
			'WebLogin' as 'ResourceID', 
			SESSN_STRT_TME as 'DateTime'
	from	dbo.WEB_SESSN_LOG with (nolock) 
	WHERE	REC_STUS_ID=1 
		AND	SESSN_STRT_TME>=convert(datetime,convert(varchar, GETDATE()-1, 101) + ' 22:50:00')
	UNION 
	select	wsl.USER_AD_ID as ADID, 
			wsl.SESSN_ID as 'SessionID', 
			wsl.IP_ADR as 'IP Address', 
			lwa.WEB_ACTY_TYPE_NME as 'EventType', 
			CASE WHEN wal.SUCCS_FAIL_CD='S' THEN 'Success' ELSE 'Failure' END as 'Action', 
			CONVERT(VARCHAR,wal.ACTY_VALU) as 'ResourceID',
			wal.CREAT_DT as 'DateTime' 
	from	dbo.WEB_ACTY_LOG wal with (nolock) 
			inner join dbo.WEB_SESSN_LOG wsl WITH (NOLOCK) ON wsl.LOG_ID=wal.LOG_ID 
			inner join dbo.LK_WEB_ACTY_TYPE lwa WITH (NOLOCK) ON lwa.WEB_ACTY_TYPE_ID=wal.WEB_ACTY_TYPE_ID 
	WHERE	wal.CREAT_DT>=convert(datetime,convert(varchar, GETDATE()-1, 101) + ' 22:50:00')

      
    
END TRY  
  
BEGIN CATCH  
 EXEC [dbo].[insertErrorInfo]  
END CATCH  
  
  