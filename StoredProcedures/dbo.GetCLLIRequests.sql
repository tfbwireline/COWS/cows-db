USE [COWS]
GO
_CreateObject 'SP','dbo','GetCLLIRequests'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bolano, Joshua Judiel
-- Create date: 08/17/2017
-- Description:	Get CLLI requests by REC_STUS_ID
-- Updated:
--		08/29/2017 - Add search functionality for CLLI_ID
--		09/14/2017 - Add search functionality for ZIP_CD
-- =============================================
ALTER PROCEDURE [dbo].[GetCLLIRequests]  -- exec GETCLLIRequests '2'
	-- Add the parameters for the stored procedure here
	@StusID VARCHAR(5),
	@CLLI_ID VARCHAR(11) = '',
	@ZIP_CD VARCHAR(10) = ''
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF @StusID = ''
	BEGIN
		SELECT 
			CPE_CLLI_ID [ID],
			CLLI_ID [CLLI ID],
			--NTWK_SITE_CD,
			SITE_NME [SITE NAME],
			--CASCD_ID,
			--SITE_ID,
			ADDR_TXT [ADDRESS],
			STT_ID [STATE],
			ZIP_CD [ZIP CODE],
			CASE
				WHEN CPE_CLLI_STUS_ID = 0 THEN 'Pending'
				WHEN CPE_CLLI_STUS_ID = 2 THEN 'Completed'
			END [STATUS],
			CASE
				WHEN REC_STUS_ID = 1 THEN 'Active'
				WHEN REC_STUS_ID = 0 THEN 'Inactive'
			END [ACTIVE/INACTIVE]
		FROM dbo.CPE_CLLI WITH (NOLOCK)
		WHERE CLLI_ID LIKE '%' + @CLLI_ID + '%'
			AND ZIP_CD LIKE '%' + @ZIP_CD + '%'
		ORDER BY CREAT_DT DESC, CPE_CLLI_ID DESC
	END
	ELSE
	BEGIN
		SELECT 
			CPE_CLLI_ID [ID],
			CLLI_ID [CLLI ID],
			--NTWK_SITE_CD,
			SITE_NME [SITE NAME],
			--CASCD_ID,
			--SITE_ID,
			ADDR_TXT [ADDRESS],
			STT_ID [STATE],
			ZIP_CD [ZIP CODE],
			CASE
				WHEN CPE_CLLI_STUS_ID = 0 THEN 'Pending'
				WHEN CPE_CLLI_STUS_ID = 2 THEN 'Completed'
			END [STATUS],
			CASE
				WHEN REC_STUS_ID = 1 THEN 'Active'
				WHEN REC_STUS_ID = 0 THEN 'Inactive'
			END [ACTIVE/INACTIVE]
		FROM dbo.CPE_CLLI WITH (NOLOCK)
		WHERE CLLI_ID LIKE '%' + @CLLI_ID + '%'
			AND ZIP_CD LIKE '%' + @ZIP_CD + '%'
			AND CPE_CLLI_STUS_ID = @StusID
		ORDER BY CREAT_DT DESC, CPE_CLLI_ID DESC
	END
END
