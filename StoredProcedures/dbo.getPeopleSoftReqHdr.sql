USE [COWS]
GO
_CreateObject 'SP','dbo','getPeopleSoftReqHdr'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 08/11/2015
-- Description:	Gets data for PeopleSoft Batch process.
-- =========================================================
ALTER PROCEDURE [dbo].[getPeopleSoftReqHdr]
        
AS
BEGIN TRY
				
	SELECT  ORDR_ID
		   ,REQSTN_NBR	
		   ,CUST_ELID
		   ,CASE
				WHEN DLVY_CLLI in ('PHNXAZ9865','OMAHNE9895','NWYKNY9855') THEN DLVY_CLLI
				ELSE LEFT(ISNULL(DLVY_CLLI,''), 8)
			END        	                    AS [DLVY_CLLI]
		   ,LEFT(ISNULL(dbo.RemoveIntlCharacters(DLVY_NME),''),30)	AS [DLVY_NME]
           ,LEFT(ISNULL(dbo.RemoveIntlCharacters(DLVY_ADDR1),''),35)			AS [DLVY_ADDR1]
           ,LEFT(ISNULL(dbo.RemoveIntlCharacters(DLVY_ADDR2),''),35)			AS [DLVY_ADDR2]
           ,LEFT(ISNULL(dbo.RemoveIntlCharacters(DLVY_ADDR3),''),35)			AS [DLVY_ADDR3]
           ,LEFT(ISNULL(dbo.RemoveIntlCharacters(DLVY_ADDR4),''),35)			AS [DLVY_ADDR4]
           ,LEFT(ISNULL(dbo.RemoveIntlCharacters(DLVY_CTY),''),30)			AS [DLVY_CTY]
           ,LEFT(ISNULL(DLVY_CNTY,''),3)			AS [DLVY_CNTY]
           ,LEFT(ISNULL(DLVY_ST,''),2)			AS [DLVY_ST]
           ,LEFT(ISNULL(DLVY_ZIP,''),12)				AS [DLVY_ZIP]
           ,LEFT(ISNULL(DLVY_PHN_NBR,''),24)			AS [DLVY_PHN_NBR]
           ,LEFT(ISNULL(INST_CLLI,''),10)			AS [INST_CLLI]
           ,LEFT(ISNULL(MRK_PKG,''),20)				AS [MRK_PKG]
           ,Convert(varchar(8),REQSTN_DT,112) AS [REQSTN_DT]
           ,LEFT(ISNULL(REF_NBR,''),12)				AS [REF_NBR]      
           ,LEFT(ISNULL(dbo.RemoveSpecialCharacters(SHIP_CMMTS),''),490)			AS [SHIP_CMMTS]
           ,LEFT(ISNULL(RFQ_INDCTR,''),1)			AS [RFQ_INDCTR]
           ,REC_STUS_ID
           ,CREAT_DT
           ,SENT_DT
           ,'Bldg:' + DLVY_BLDG				AS [DLVY_BLDG]
           ,'Flr:' + DLVY_FLR				AS [DLVY_FLR]
           ,'Rm:' + DLVY_RM					AS [DLVY_RM]
           ,CASE
				WHEN CSG_LVL = '00000'    THEN ''
				ELSE 'CSG:' + CSG_LVL
			END				AS [CSG_LVL]
    FROM dbo.PS_REQ_HDR_QUEUE WITH (NOLOCK)
    WHERE ISNULL(SENT_DT,'') = ''	
    
		
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH


