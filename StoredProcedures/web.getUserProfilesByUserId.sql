USE [COWS]
GO
_CreateObject 'SP','web','getUserProfilesByUserId'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================  
-- Author:  kh946640  
-- Create date: 02/06/2019  
-- Description: Gets all the User Profiles based on UserID.
-- =========================================================  
ALTER PROCEDURE [web].[getUserProfilesByUserId]  --40, 6241
  @User_Id int,
  @LoggedInUserId int 
AS  
BEGIN  
SET NOCOUNT ON;  
Begin Try 
	
	--select @User_Id as UserId, lup.USR_PRF_ID as UsrPrfId, lup.USR_PRF_DES as UsrPrfDes, lup.REC_STUS_ID as RecStusId
	--from LK_USR_PRF lup with (nolock)
	--join MAP_USR_PRF mup with (nolock) on lup.USR_PRF_ID = mup.USR_PRF_ID
	--where mup.USER_ID = @User_Id
	--union
	--select @User_Id as UserId, lup1.USR_PRF_ID as UsrPrfId, lup1.USR_PRF_DES as UsrPrfDes, 0 as RecStusId
	--from LK_USR_PRF lup
	--join MAP_USR_PRF mup on lup.USR_PRF_ID = mup.USR_PRF_ID
	--join LK_PRF_HRCHY lph on lup.USR_PRF_ID = lph.PRNT_PRF_ID
	--join LK_USR_PRF lup1 on lph.CHLD_PRF_ID = lup1.USR_PRF_ID
	--where lup1.USR_PRF_ID not in (select lup2.USR_PRF_ID
	--from LK_USR_PRF lup2 with (nolock)
	--join MAP_USR_PRF mup2 with (nolock) on lup2.USR_PRF_ID = mup2.USR_PRF_ID
	--where mup2.USER_ID = @LoggedInUserId)

	--Profiles of searchuser where the loggedinuser is not an admin of that profile will be returned as 2
	select mup.USER_ID as UserId, lup.USR_PRF_ID as UsrPrfId, lup.USR_PRF_DES as UsrPrfDes,CASE WHEN mup2.USR_PRF_ID IS NULL THEN 2 ELSE lup.REC_STUS_ID END as RecStusId,  CASE WHEN mup2.USR_PRF_ID IS NULL and lup.USR_PRF_DES LIKE 'MDS%' THEN 2 ELSE mup.POOL_CD END as PoolCd
	from LK_USR_PRF lup with (nolock)
	join MAP_USR_PRF mup with (nolock) on lup.USR_PRF_ID = mup.USR_PRF_ID
	join LK_PRF_HRCHY lph on lup.USR_PRF_ID = lph.CHLD_PRF_ID
	left join MAP_USR_PRF mup2 with (nolock) on ((lph.PRNT_PRF_ID = mup2.USR_PRF_ID) OR (lph.PRNT_PRF_ID IN (2,16,100) AND mup2.USR_PRF_ID IN (2,16,100))) and mup2.USER_ID=@LoggedInUserId and mup2.REC_STUS_ID = 1
	where mup.USER_ID = @User_Id and mup.REC_STUS_ID = 1
	union--All other profiles from loggedinuser that can be assigned to searchuser
	select @User_Id as UserId, lup1.USR_PRF_ID as UsrPrfId, lup1.USR_PRF_DES as UsrPrfDes, 0 as RecStusId, CASE WHEN lup1.USR_PRF_NME IN ('MDSEActivator', 'MDSEReviewer', 'MDSCPTManager', 'MDSCPTGateKeeper', 'MDSCPTCRM') THEN 0 ELSE NULL END as PoolCd 
	from LK_USR_PRF lup
	join MAP_USR_PRF mup on lup.USR_PRF_ID = mup.USR_PRF_ID
	join LK_PRF_HRCHY lph on lup.USR_PRF_ID = lph.PRNT_PRF_ID
	join LK_USR_PRF lup1 on lph.CHLD_PRF_ID = lup1.USR_PRF_ID  and lup1.REC_STUS_ID = 1
	where lup1.USR_PRF_ID not in (select lup2.USR_PRF_ID 
	from LK_USR_PRF lup2 with (nolock)
	join MAP_USR_PRF mup2 with (nolock) on lup2.USR_PRF_ID = mup2.USR_PRF_ID
	where mup2.USER_ID = @User_Id and mup2.REC_STUS_ID = 1)
	and mup.USER_ID = @LoggedInUserId and mup.REC_STUS_ID = 1

End Try  
  
Begin Catch  
 EXEC [dbo].[insertErrorInfo]  
End Catch  
END