USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getCpeReqLineItems_V5U]    Script Date: 12/13/2017 13:58:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	-- =============================================
	-- Author:		David Phillips
	-- Create date: 5/4/2015
	-- Description:	Appian CPE LINE ITEM Info.
	---- =============================================
	--  [dbo].[getCpeReqLineItems_V5U] 11132

	ALTER PROCEDURE [dbo].[getCpeReqLineItems_V5U]  --11817
		@ORDR_ID INT 
		
		
	AS
	BEGIN

	SELECT	distinct litm.FSA_CPE_LINE_ITEM_ID
				,fo.ORDR_ID      
				,litm.ORDR_CMPNT_ID			AS CMPNT_ID
				,litm.MATL_CD				AS MAT_CD
				,litm.MDS_DES				AS ITM_DES
				,litm.MANF_PART_CD			AS MANF_PART_NBR
				,litm.MFR_PS_ID				AS MANF_ID
				,litm.ORDR_QTY				AS ORDR_QTY
				,ISNULL(litm.UNIT_MSR,'EA') AS UNT_MSR
				,litm.UNIT_PRICE			AS UNT_PRICE
				,ISNULL(pid.ACCT,'')        AS ACCT
				,ISNULL(pid.PRODCT,'')      AS PRODCT 
				,ISNULL(pid.PROJ_ID,'') 	AS PROJ_ID 
				,ISNULL(pid.ACTVY,'')       AS ACTVY
				,ISNULL(pid.SOURCE_TYP,'')  AS SOURCE_TYP
				,ISNULL(pid.RSRC_CAT,'')  	AS RSRC_CAT
				,litm.MANF_DISCNT_CD
				,litm.DEVICE_ID
	FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
	INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
	INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
	LEFT OUTER JOIN  dbo.LK_CPE_PID pid WITH (NOLOCK)
					ON pid.DMSTC_CD = ord.DMSTC_CD
						AND pid.REC_STUS_ID = 1
						AND litm.CNTRC_TYPE_ID = pid.PID_CNTRCT_TYPE	
						AND ISNULL(litm.DROP_SHP,'N') = pid.DROP_SHP
	WHERE  fo.ORDR_ID = @ORDR_ID
		AND litm.EQPT_TYPE_ID <> 'SPK'
		AND litm.ITM_STUS = 401
		AND ISNULL(CPE_REUSE_CD,0) = 0
		AND litm.CNTRC_TYPE_ID IN ('PRCH','RNTL', 'LEAS','INST','ASAS')
		AND NOT EXISTS (SELECT 'X' 
						FROM dbo.PS_REQ_LINE_ITM_QUEUE psrl WITH (NOLOCK)
						WHERE psrl.CMPNT_ID = litm.ORDR_CMPNT_ID)
		AND NOT EXISTS (SELECT 'X'
						FROM dbo.FSA_ORDR_CPE_LINE_ITEM l WITH (NOLOCK)
						 INNER JOIN dbo.ORDR o WITH (NOLOCK) ON o.ORDR_ID = l.ORDR_ID
							WHERE ord.DMSTC_CD = 0
							AND ord.PROD_ID NOT in ('UCCH','UCSV','MIPT','UCSM') 
							AND litm.EQPT_TYPE_ID = 'SVC'
							AND l.FSA_CPE_LINE_ITEM_ID = litm.FSA_CPE_LINE_ITEM_ID)
	END