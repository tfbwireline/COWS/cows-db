USE [COWS]
GO
_CreateObject 'SP','web','getContactDetails'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ==============================================================================================================================            
-- Author:  jrg7298            
-- Create date: 5/10/2021
-- Description: Get Contact Details from Mach5 and ODIE using HierID, HierLvl : H1/H2/H4/H6/OD, ObjType : 'E','R','C'
-- If HierLvl, HierID are blank then pull all contact information from Mach5 and ODIE as given below
--  ODIE : Role codes of MNSPM, MNSNTE, MSSSE
--  Mach5 : Role codes of IPMP, IPMC, IPMS, IPMD, CSM, CSMC
-- jbolano15 - 09/14/2021 - Added additional condition if the record was created by current logged in user
-- jbolano15 - 10/20/2021 - Update condition of getting data from upstream
-- jbolano15 - 11/12/2021 - Return CREAT_BY_USER_ID = 1 if from upstream
-- jbolano15 - 11/12/2021 - For non-completed Redesign, always Upstream over Manual
-- jbolano15 - 11/16/2021 - For non-completed Redesign, deactivate non-persisting upstream records
-- jbolano15 - 12/02/2021 - Updated SP to accommodate Job run
-- jbolano15 - 12/02/2021 - Add filter for CUSTOMER_H1.CUST_ID; Only for Events/Redesign
-- jbolano15 - 12/16/2021 - Adding MNS-PM filter for both Events and CPT; Now applies to all Redesign/CPT/Events
-- jbolano15 - 01/07/2022 - Replace EMAIL_ADR = NULL into EMAIL_ADR = ''
-- jbolano15 - 01/11/2022 - Include non-completed Events for the process of deactivating non-persisting upstream records
-- jbolano15 - 01/20/2022 - Allow H6 value for generic H1 events (999999999)
-- jbolano15 - 01/26/2022 - Remove CSM/CSMC for M5 query; retain manual records at all times; Network ONLY events will not query against ODIE
-- ssandov3 - 02/04/2022 - Updated PHN_NBR from NOT NULL to NULL
-- jbolano15 - 02/16/2022 - Remove recursioin part for generic H1
-- ==============================================================================================================================          
ALTER PROCEDURE [web].[getContactDetails] -- exec web.getContactDetails 0, '999999999', 'H1', 'R', 'cows-only', 6244, 0, ''
	 @ObjID INT					-- CPTID, RedesignID, EventID
	,@HierID CHAR(9)			-- 9 digit H1/H2/H4/H6/OD values
	,@HierLvl CHAR(2)			-- H1/H2/H4/H6/OD
	,@ObjType CHAR(1)			-- E','R','C',
	,@CustId VARCHAR(10)		-- CUSTOMER_H1.CUST_ID
	,@LoggedInUser INT = 0
	,@IsViaJob INT = 0
	,@IsNetworkOnly BIT = 0
AS
BEGIN
	BEGIN TRY
	-- DECLARE @ObjID INT = 0					-- CPTID, RedesignID, EventID
	--,@HierID CHAR(9) = '928663277'			-- 9 digit H1/H2/H4/H6/OD values
	--,@HierLvl CHAR(2) = 'H1'			-- H1/H2/H4/H6/OD
	--,@ObjType CHAR(1) = 'E'			-- E','R','C',
	--,@CustId VARCHAR(10)		-- CUSTOMER_H1.CUST_ID
	--,@LoggedInUser INT = 0
	--,@IsViaJob INT = 0
	--,@H6 CHAR(9) = NULL
	--,@IsNetworkOnly BIT = 0
		-- ROLE LEGENDS
		-- 
		-- LK_ROLE <-> ODIE
		-- MNS-PM	=	MNS PM
		-- MNS-NTE	=	NTE
		-- MNS-NIM	=	MNS PM
		-- SE		=	Sec Design Eng
		-- 
		-- LK_ROLE <-> MACH5
		-- IPMP		=	IPMP	= Implementation Project Manager
		-- IPMS		=	IPMS	= Secondary Implementation Project Manager
		-- IPMC		=	IPMC	= Customer Support Manager
		-- IPMD		=	IPMD	= Secondary Customer Support Manager
		-- CSM		=	CSM		= Sr. Manager, Implementation
		-- CSMC		=	CSMC	= Manager, Customer Support
		-- SE		=	SE		= 

		-- For Testing:
		--DECLARE @ObjID INT = 12228, @HierID CHAR(9) = '927164289', @HierLvl CHAR(2) = 'H1', @ObjType CHAR(1) = 'E'
		--DECLARE @ObjID INT = 0, @HierID CHAR(9) = '926787326', @HierLvl CHAR(2) = 'H1', @ObjType CHAR(1) = 'E'
		--DECLARE @ObjID INT = 12462, @HierID CHAR(9) = '999999999', @HierLvl CHAR(2) = '', @ObjType CHAR(1) = 'E'
		--SELECT * FROM SQL_ERROR ORDER BY CREAT_DT DESC
		
		-- CREATE TEMP TABLE HOLDER FOR FINAL OUTPUT
		IF OBJECT_ID(N'tempdb..#ContactList', N'U') IS NOT NULL         
			DROP TABLE #ContactList

		CREATE TABLE #ContactList (
			OBJ_ID INT NOT NULL
			,ID INT NOT NULL
			,HIER_ID VARCHAR(9) NOT NULL
			,HIER_LVL_CD VARCHAR(2) NOT NULL
			,ROLE_ID TINYINT NOT NULL
			,ROLE_NME VARCHAR(MAX) NOT NULL
			,EMAIL_ADR VARCHAR(MAX) NOT NULL
			,PHN_NBR VARCHAR(20) NULL
			,EMAIL_CD VARCHAR(100) NOT NULL
			,AUTO_RFRSH_CD BIT NOT NULL
			,CREAT_BY_USER_ID INT NOT NULL
			,SUPRS_EMAIL BIT NOT NULL)

		-- Get odie contacts from their view and load into #table; use the hierid, hierlvl if available
		DECLARE @odie_sql nvarchar(max) = 'SELECT *
				FROM OPENQUERY(D1ODIE1,''SELECT DISTINCT S.USERID, S.EMAIL, S.PHONE, S.STAFF_ROLE,
												CH.H1_ID, ''''H1''''
										FROM	STAFF S
										LEFT JOIN CUST_ACCT_TEAM CAT ON CAT.STAFF_ID = S.USERID
										LEFT JOIN CUSTOMER_H1 CH ON CH.CUST_ID = CAT.CUST_ID
									WHERE 1 = 1
										AND S.STAFF_ROLE IN (''''NTE'''',''''MNS PM'''',''''Sec Design Eng'''')'

		DECLARE @m5_sql nvarchar(max) = 'SELECT *
			FROM OPENQUERY(M5, ''SELECT DISTINCT atc.USR_ID, atc.EMAIL_ADR,
					CASE
						WHEN atc.DOM_INTL_IND_CD = ''''D'''' THEN atc.DOM_PHN_NBR
						WHEN atc.DOM_INTL_IND_CD = ''''I'''' THEN atc.INTL_PHN_NBR
						ELSE ''''''''
					END AS PHN_NBR, atc.ROLE_CD, ca.CIS_CUST_ID, ca.CIS_HIER_LVL_CD
				FROM MACH5.V_V5U_CUST_ACCT ca
					LEFT JOIN MACH5.v_V5U_ACCT_TEAM_CONTACT atc ON atc.CUST_ACCT_ID = ca.CUST_ACCT_ID
				WHERE
					1 = 1'
					
		DECLARE @NEW_H1 CHAR(9) = @HierID
		DECLARE @GET_UPSTREAM BIT = 0
		DECLARE @H6 CHAR(9) = ''

		IF @ObjID > 0
		BEGIN
			-- Merged Contact Details for ODIE/M5 for Redesign/Event NON-FINAL Status (like Completed/Deleted/Cancelled etc)
			IF @ObjType = 'R'
			BEGIN
				SELECT @GET_UPSTREAM = 1,
					@HierID = H1_CD,
					@CustId = CASE
						WHEN ISNULL(@CustId, '') = '' THEN ODIE_CUST_ID
						ELSE @CustId END
				FROM dbo.REDSGN WITH (NOLOCK)
				WHERE
					REDSGN_ID = @ObjID
					AND STUS_ID NOT IN (226,227,228,229)
			END
			ELSE IF @ObjType = 'E'
			BEGIN
				-- Commented for now to proceed with PROD Deployment (1/21/2022)
				-- TO DO: Joshua need to revisit this portion
				SELECT @GET_UPSTREAM = 1,
					@HierID = CASE
						WHEN H1 = '999999999' THEN H6
						ELSE COALESCE(H1, NTWK_H1) END,
					@HierLvl = CASE
						WHEN H1 = '999999999' THEN 'H6'
						ELSE 'H1' END,
					@CustId = CASE
						WHEN ISNULL(@CustId, '') = '' THEN ODIE_CUST_ID
						ELSE @CustId END,
					@IsNetworkOnly = CASE
						WHEN EXISTS (SELECT 1 FROM MDS_EVENT_NTWK_ACTY WHERE EVENT_ID = @ObjID AND NTWK_ACTY_TYPE_ID NOT IN (2,3,4)) THEN 0
						ELSE 1 END
				FROM dbo.MDS_EVENT WITH (NOLOCK)
				WHERE 
					EVENT_ID = @ObjID
					AND EVENT_STUS_ID NOT IN (6,8,13)
			END
			ELSE IF @ObjType = 'C'
			BEGIN
				SELECT @GET_UPSTREAM = 1, @HierID = H1 FROM dbo.CPT WITH (NOLOCK) WHERE  CPT_ID = @ObjID AND CPT_STUS_ID NOT IN (308,309,311)
			END

			-- Use previous value of @HierID (currently saved on @NEW_H1) when sp is called with @ObjID > 0 AND @HierID is not equal to current H1/H6 of the Object
			-- In UI perspective, to use new H1 value inputted by user on Edit/Update process
			IF @NEW_H1 <> '' AND @NEW_H1 <> @HierID
			BEGIN
				SET @HierID = @NEW_H1
			END
		END

		-- For ROLE_CD filter
		IF @ObjType = 'E'
		BEGIN
			-- Filter via chosen ODIE_CUST_ID
			IF ISNULL(@CustId, '') <> ''
			BEGIN
				SET @odie_sql = @odie_sql + ' AND CH.CUST_ID = ''''' + @CustId + ''''''
			END

			-- Get H1 from M5 via H6
			IF @HierLvl = 'H6'
			BEGIN
				SET @H6 = @HierID
				DECLARE @H1_VIA_H6 NVARCHAR(MAX) = 'SELECT @H1 = H1 FROM OPENQUERY(M5, ''SELECT H1 FROM V_V5U_CUST_ACCT WHERE CIS_HIER_LVL_CD = ''''H6'''' AND CIS_CUST_ID = ''''' + @H6 + ''''''')'
				EXEC sp_executeSQL @H1_VIA_H6, N'@H1 VARCHAR(MAX) OUT', @HierID OUT
			END

			SET @m5_sql = @m5_sql + ' AND atc.ROLE_CD IN (''''IPMP'''',''''IPMC'''',''''IPMS'''',''''IPMD'''')'
		END
		ELSE IF @ObjType = 'R' 
		BEGIN
			-- Filter via chosen ODIE_CUST_ID
			IF ISNULL(@CustId, '') <> ''
			BEGIN
				SET @odie_sql = @odie_sql + ' AND CH.CUST_ID = ''''' + @CustId + ''''''
			END

			SET @m5_sql = @m5_sql + ' AND atc.ROLE_CD IN (''''IPMP'''',''''IPMC'''',''''IPMS'''',''''IPMD'''',''''SE'''')'
		END
		ELSE IF @ObjType = 'C'
		BEGIN
			SET @m5_sql = @m5_sql + ' AND atc.ROLE_CD IN (''''IPMP'''',''''SLSP'''')'
		END

		-- CALLED WHEN ONLOAD
		IF @ObjID > 0
		BEGIN
			IF @GET_UPSTREAM = 1
			BEGIN
				SET @odie_sql = @odie_sql + ' AND CH.H1_ID = ''''' + CONVERT(VARCHAR,@HierID) + ''''' '')'
				SET @m5_sql = @m5_sql + ' AND ca.H1 = ''''' + CONVERT(VARCHAR,@HierID) + ''''' AND ca.CIS_HIER_LVL_CD IN (''''H1'''', ''''H2'''') '')'
			END
			ELSE
			BEGIN
				DECLARE @I INT = 0
				DECLARE @HIER_ID NVARCHAR(MAX)
				DECLARE @HIER_LVL_CD NVARCHAR(MAX)
				DECLARE CNTCT_DETL_CURSOR CURSOR FOR
				SELECT DISTINCT HIER_LVL_CD, HIER_ID
				FROM dbo.CNTCT_DETL
				WHERE
					OBJ_ID = @ObjID
					AND OBJ_TYP_CD = @ObjType
					AND REC_STUS_ID = 1

				OPEN CNTCT_DETL_CURSOR
				FETCH NEXT FROM CNTCT_DETL_CURSOR INTO @HIER_LVL_CD, @HIER_ID
			
				WHILE @@FETCH_STATUS = 0
				BEGIN
					IF @I = 0
					BEGIN
						SET @odie_sql = @odie_sql + ' AND ('
						SET @m5_sql = @m5_sql + ' AND ('
					END
					ELSE
					BEGIN
						SET @odie_sql = @odie_sql + ' OR '
						SET @m5_sql = @m5_sql + ' OR '
					END
				
					SET @odie_sql = @odie_sql + 'CH.H1_ID = ''''' + CONVERT(VARCHAR,@HIER_ID) + ''''''
					SET @m5_sql = @m5_sql + '(ca.H1 = ''''' + CONVERT(VARCHAR,@HIER_ID) + ''''' AND ca.CIS_HIER_LVL_CD = ''''' + CONVERT(VARCHAR,@HIER_LVL_CD) + ''''')'
					SET @I = @I + 1

					FETCH NEXT FROM CNTCT_DETL_CURSOR INTO @HIER_LVL_CD, @HIER_ID
				END
			
				IF @I > 0
				BEGIN
					SET @odie_sql = @odie_sql + ')'')'
					SET @m5_sql = @m5_sql + ')'')'
				END
				-- SHOULD NOT RETURN ANY DATA FROM ODIE/M5 DUE TO @OBJ_ID > 0 AND NO ACTIVE RECORD ON CNTCT_DETL WHERE OBJ_ID = @OBJ_ID
				ELSE
				BEGIN
					SET @odie_sql = @odie_sql + ' AND 1 = 0'')'
					SET @m5_sql = @m5_sql + ' AND 1 = 0'')'
				END

				CLOSE CNTCT_DETL_CURSOR
				DEALLOCATE CNTCT_DETL_CURSOR
			END
		END
		-- CALLED VIA REFRESH BUTTON
		ELSE
		BEGIN
				SET @odie_sql = @odie_sql + ' AND CH.H1_ID = ''''' + CONVERT(VARCHAR,@HierID) + ''''' '')'
				-- Updated to query to return M5 contacts for H1/H2 and change ca.CIS_CUST_ID to ca.H1
				-- ca.H1 is used to get M5 Contact Details for create/drft/visible status
				-- Once submitted, looping for auto refresh data will looked through ca.CIS_CUST_ID
				SET @m5_sql = @m5_sql + ' AND ca.H1 = ''''' + CONVERT(VARCHAR,@HierID) + ''''' AND ca.CIS_HIER_LVL_CD IN (''''H1'''', ''''H2'''') '')'
		END

		-- Create a temp table ContactList to store for both odie and mach5
		DECLARE @tempContactTable TABLE (
			USER_ADID				VARCHAR(10)
			,EMAIL_ADR				VARCHAR(200)
			,PHN_NBR				VARCHAR(20)
			,ROLE_CD				VARCHAR(30)
			--,CUST_ID				VARCHAR(100)
			,HIER_ID				CHAR(9)
			,HIER_LVL				CHAR(2))
			--,DATA_VOICE_INDCR_CD	CHAR(1))

		IF @IsNetworkOnly = 0
		BEGIN
			INSERT INTO @tempContactTable
			EXEC sp_executeSQL @odie_sql
			--PRINT @odie_sql
		END
		
		INSERT INTO @tempContactTable
		EXEC sp_executeSQL @m5_sql
		--PRINT @m5_sql

		--SELECT * FROM @tempContactTable
		
		-- SYNC LK_ROLE.ROLE_CD VS STAFF.STAFF_ROLE
		UPDATE @tempContactTable SET ROLE_CD = 'MNS-PM' WHERE ROLE_CD = 'MNS PM'
		UPDATE @tempContactTable SET ROLE_CD = 'SDE' WHERE ROLE_CD = 'Sec Design Eng'

		-- REPLACE NULL EMAIL_ADR INTO ''
		UPDATE @tempContactTable SET EMAIL_ADR = ISNULL(EMAIL_ADR, '') WHERE EMAIL_ADR IS NULL
		
		IF @ObjID = 0
		BEGIN
			INSERT INTO #ContactList
			SELECT DISTINCT @ObjID AS OBJ_ID, 0 AS ID, tmp.HIER_ID AS HIER_ID, tmp.HIER_LVL AS HIER_LVL_CD,
							lr.ROLE_ID AS ROLE_ID, ISNULL(lr.ROLE_CD, '') + ' - ' + lr.ROLE_NME AS ROLE_NME,
							tmp.EMAIL_ADR, tmp.PHN_NBR, 
					CASE @ObjType
						WHEN 'E' THEN '2,4,10,3,6'
						WHEN 'R' THEN '221,223,225,226,227,230'
						WHEN 'C' THEN '305,307,309,310,311'
						ELSE NULL
					END AS EMAIL_CD, 1 AS AUTO_RFRSH_CD, 1 AS CREAT_BY_USER_ID, 0 AS SUPRS_EMAIL
			FROM @tempContactTable tmp
				INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = tmp.ROLE_CD
		END
		ELSE
		BEGIN
			IF @GET_UPSTREAM = 1
			BEGIN
				-- Merge #table contents with dbo.CNTCT_DETL based on auto-refresh flag, h-level, h-id, role_cd, email_adr;
				-- Return the #table to the UI. If rec_stus_id = 0 then dont return to UI; if autorefresh = 0 then dont refresh the data coming from odie/mach5
				--SELECT * FROM CNTCT_DETL WHERE OBJ_ID = 12751 ORDER BY REC_STUS_ID DESC
				--SELECT * FROM CNTCT_DETL WHERE OBJ_ID = 12751 AND REC_STUS_ID = 1
				--UPDATE CNTCT_DETL SET REC_STUS_ID = 1, CREAT_BY_USER_ID = 1, SUPRS_EMAIL = 1 WHERE ID IN (2004)
				--DELETE FROM CNTCT_DETL WHERE ID IN (716)
				IF @ObjType = 'R' OR @ObjType = 'E'
				BEGIN
					UPDATE CNTCT_DETL SET
						REC_STUS_ID = 0,
						MODFD_BY_USER_ID = 1,
						MODFD_DT = GETDATE()
					WHERE
						ID IN (SELECT DISTINCT cd.ID
							FROM @tempContactTable tmp
								INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = tmp.ROLE_CD
								RIGHT JOIN dbo.CNTCT_DETL cd WITH (NOLOCK) ON cd.OBJ_ID = @ObjID AND tmp.HIER_LVL = cd.HIER_LVL_CD AND tmp.HIER_ID = cd.HIER_ID AND tmp.ROLE_CD = lr.ROLE_CD AND tmp.EMAIL_ADR = cd.EMAIL_ADR
							WHERE
								cd.OBJ_ID = @ObjID
								AND cd.REC_STUS_ID = 1
								AND cd.CREAT_BY_USER_ID = 1
								AND tmp.USER_ADID IS NULL)
				END

				INSERT INTO #ContactList
				SELECT DISTINCT
					@ObjID AS OBJ_ID,
					cd.ID,
					cd.HIER_ID,
					cd.HIER_LVL_CD,
					cd.ROLE_ID, 
					ISNULL(lr.ROLE_CD, '') + ' - ' + lr.ROLE_NME AS ROLE_NME,
					cd.EMAIL_ADR,
					COALESCE(tct.PHN_NBR, cd.PHN_NBR, '') AS PHN_NBR,
					cd.EMAIL_CD,
					cd.AUTO_RFRSH_CD,
					cd.CREAT_BY_USER_ID,
					cd.SUPRS_EMAIL
				FROM dbo.CNTCT_DETL cd WITH (NOLOCK)
					INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_ID = cd.ROLE_ID
					LEFT JOIN @tempContactTable tct ON tct.HIER_LVL = cd.HIER_LVL_CD AND tct.HIER_ID = cd.HIER_ID AND tct.EMAIL_ADR = cd.EMAIL_ADR
				WHERE cd.OBJ_ID = @ObjID AND cd.OBJ_TYP_CD = @ObjType AND cd.REC_STUS_ID = 1
				UNION
				SELECT DISTINCT @ObjID AS OBJ_ID, 0 AS ID, tmp.HIER_ID AS HIER_ID, tmp.HIER_LVL AS HIER_LVL_CD,
								lr.ROLE_ID AS ROLE_ID, ISNULL(lr.ROLE_CD, '') + ' - ' + lr.ROLE_NME AS ROLE_NME,
								tmp.EMAIL_ADR, tmp.PHN_NBR, 
					CASE @ObjType
						WHEN 'E' THEN '2,4,10,3,6'
						WHEN 'R' THEN '221,223,225,226,227,230'
						WHEN 'C' THEN '305,307,309,310,311'
						ELSE NULL
					END AS EMAIL_CD, 1 AS AUTO_RFRSH_CD, 1 AS CREAT_BY_USER_ID, 0 AS SUPRS_EMAIL
				FROM @tempContactTable tmp
					INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_CD = tmp.ROLE_CD
					LEFT JOIN dbo.CNTCT_DETL cd WITH (NOLOCK) ON cd.OBJ_ID = @ObjID AND tmp.HIER_LVL = cd.HIER_LVL_CD AND tmp.HIER_ID = cd.HIER_ID AND tmp.EMAIL_ADR = cd.EMAIL_ADR AND cd.CREAT_BY_USER_ID = 1 AND cd.REC_STUS_ID = 1
				WHERE
					cd.ID IS NULL
			END
			ELSE
			BEGIN
				INSERT INTO #ContactList
				SELECT DISTINCT
					@ObjID AS OBJ_ID,
					cd.ID,
					cd.HIER_ID,
					cd.HIER_LVL_CD,
					cd.ROLE_ID, 
					ISNULL(lr.ROLE_CD, '') + ' - ' + lr.ROLE_NME AS ROLE_NME,
					cd.EMAIL_ADR,
					cd.PHN_NBR,
					cd.EMAIL_CD,
					cd.AUTO_RFRSH_CD,
					cd.CREAT_BY_USER_ID,
					cd.SUPRS_EMAIL
				FROM dbo.CNTCT_DETL cd WITH (NOLOCK)
					INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON lr.ROLE_ID = cd.ROLE_ID
					LEFT JOIN @tempContactTable tct ON tct.HIER_LVL = cd.HIER_LVL_CD AND tct.HIER_ID = cd.HIER_ID AND tct.EMAIL_ADR = cd.EMAIL_ADR
				WHERE cd.OBJ_ID = @ObjID AND cd.OBJ_TYP_CD = @ObjType AND cd.REC_STUS_ID = 1
			END

			-- EXECUTE THIS ONLY IF SP RAN VIA JOB RefreshContactDetails_30min
			-- This will add new entry from ODIE/M5 and insert into our Table dbo.CNTCT_DETL
			IF @IsViaJob = 1
			BEGIN
				INSERT INTO CNTCT_DETL
				SELECT OBJ_ID, @ObjType, HIER_LVL_CD, HIER_ID, ROLE_ID, EMAIL_ADR, PHN_NBR, AUTO_RFRSH_CD, EMAIL_CD, 1, 1, NULL, GETDATE(), NULL, SUPRS_EMAIL
				FROM #ContactList
				WHERE ID = 0

				RETURN
			END
		END

		--IF NOT EXISTS (SELECT * FROM #ContactList) AND @HierID = '999999999'
		--BEGIN
		--	EXEC web.getContactDetails @ObjId, @H6, 'H6', @ObjType, '', @LoggedInUser, 0, NULL, @IsNetworkOnly
		--END
		--ELSE
		--BEGIN
			SELECT * FROM #ContactList
		--END

	END TRY

	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END