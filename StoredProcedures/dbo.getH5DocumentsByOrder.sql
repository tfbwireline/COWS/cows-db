﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getH5DocumentsByOrder'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 16th September 2011
-- Description:	Returns all H5 Documents for given Order
-- ================================================================

ALTER PROCEDURE [dbo].[getH5DocumentsByOrder]
	@OrderID INT 	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		HD.H5_DOC_ID
		,OHD.ORDR_ID
		,HD.FILE_NME
		,HD.FILE_SIZE_QTY
		,HD.CREAT_DT
		,LU.FULL_NME
		,HD.CREAT_BY_USER_ID
	FROM 
		[dbo].[ORDR_H5_DOC] OHD	WITH (NOLOCK)
	INNER JOIN
		[dbo].[H5_DOC] HD WITH (NOLOCK) ON OHD.H5_DOC_ID = HD.H5_DOC_ID
	INNER JOIN
		[dbo].[LK_USER] LU WITH (NOLOCK) ON HD.[CREAT_BY_USER_ID] = LU.[USER_ID]
	WHERE 
		OHD.ORDR_ID = @OrderID
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
