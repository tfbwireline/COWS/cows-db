USE [COWS]
GO
_CreateObject 'SP','dbo','UpdateNUACkt'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 05/04/2020
-- Description:	Updates the NUA/Ckt on FSA_ORDR.  Used by Domestic CPE provisioning.      
-- =========================================================
ALTER PROCEDURE [dbo].[UpdateNUACkt]
	@ORDR_ID	int,
	@NuaCkt	varchar (200),
	@User	varchar (30)


AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @oldNuaCkt  Varchar (200)

	SELECT @oldNuaCkt = ISNULL(NUA,CKT) from FSA_ORDR with (nolock) where ORDR_ID = @ORDR_ID

	DECLARE @NTE  varchar(max) = 'NUA/Ckt was updated to ' + @NuaCkt + ' from ' + @oldNuaCkt
	
	BEGIN TRY

		Update FSA_ORDR 
		SET CKT = @NuaCkt
		WHERE ORDR_ID = @ORDR_ID
		
		EXEC dbo.insertOrderNotes_V5U @ORDR_ID, 6, @NTE, @User 
		
			
	END TRY


	BEGIN CATCH
		EXEC [dbo].[insertErrorInfo]
	END CATCH


END

