USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertRTS_V5U]    Script Date: 02/10/2020 11:04:46 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:	David Phillips
-- Create date: 7/8/2015
-- Description:	Performs the RTS activities in DB for Domestic CPE Orders.
--
-- Changes:
-- =============================================
ALTER PROCEDURE [dbo].[insertRTS_V5U] 
	@ORDR_ID        INT ,
	@DEVICE_ID      VARCHAR(25),
	@JPRDY_CD        VARCHAR(200),
	@NTE             Varchar(Max)	=	NULL
 	
 	
AS
BEGIN TRY

	DECLARE @TASK_ID INT, @NTE_ID INT, @device VARCHAR (25)
	

	SELECT @TASK_ID = TASK_ID FROM dbo.ACT_TASK WITH (NOLOCK)
		WHERE (ORDR_ID = @ORDR_ID AND TASK_ID IN (600,601,602,604,1000) AND STUS_ID = 0) 
		
	--EXEC [dbo].[CompleteActiveTask] @ORDR_ID, @TASK_ID,1,'Order Returned To Sales - Hold',1,26
	
	UPDATE dbo.ACT_TASK WITH (ROWLOCK)
	SET STUS_ID = 1
	WHERE ORDR_ID = @ORDR_ID AND TASK_ID = @TASK_ID
						
	EXEC dbo.insertOrderNotes @ORDR_ID,26,'Order Returned To Sales - Hold',1
	
	IF NOT EXISTS (SELECT 'X' FROM dbo.ACT_TASK WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID AND TASK_ID = 603)					
	BEGIN
		INSERT INTO dbo.ACT_TASK (ORDR_ID,TASK_ID,STUS_ID,WG_PROF_ID,CREAT_DT) 
						VALUES (@ORDR_ID,603,0,600,GETDATE())
		
		UPDATE FSA_ORDR_CPE_LINE_ITEM
		SET ITM_STUS = 402
		WHERE ORDR_ID = @ORDR_ID
		
	END

	IF ISNULL(@DEVICE_ID,'') = ''
		BEGIN
		  SELECT TOP 1 @device = DEVICE_ID  FROM dbo.FSA_ORDR_CPE_LINE_ITEM with (nolock)
			WHERE ORDR_ID = @ORDR_ID AND ISNULL(DEVICE_ID,'') <> '' 
		END

	
	SELECT @JPRDY_CD = SUBSTRING(LTRIM(RTRIM(@JPRDY_CD)),1,3)
	SELECT @NTE = SUBSTRING(LTRIM(RTRIM(ISNULL(@NTE,''))),1,500)
	
	--print @JPRDY_CD
	
	SELECT TOP 1 @NTE_ID = NTE_ID FROM dbo.ORDR_NTE WITH (NOLOCK) 
		WHERE ORDR_ID = @ORDR_ID AND NTE_TYPE_ID = 26
		ORDER BY CREAT_DT DESC
	
	--print @NTE_ID
	
	INSERT INTO [dbo].[ORDR_JPRDY]
           ([ORDR_ID],[JPRDY_CD],[NTE_ID],[CREAT_DT])
     VALUES
           (@ORDR_ID,@JPRDY_CD,@NTE_ID,GETDATE())
		
	INSERT INTO [dbo].[M5_ORDR_MSG]
		([ORDR_ID],[M5_MSG_ID],[NTE],[MSG], [DEVICE],[CMPNT_ID],[STUS_ID],[CREAT_DT],[SENT_DT])     
	VALUES (@ORDR_ID,4,@NTE,null,@DEVICE_ID,null,10,GETDATE(),NULL)
	
	IF EXISTS (SELECT * FROM dbo.SSTAT_REQ WITH (NOLOCK) WHERE (ORDR_ID = @ORDR_ID AND SSTAT_MSG_ID = 1 AND STUS_ID = 10))
		BEGIN
			UPDATE dbo.SSTAT_REQ WITH (ROWLOCK)
			 SET STUS_ID = 9
			WHERE (ORDR_ID = @ORDR_ID AND SSTAT_MSG_ID = 1 AND STUS_ID = 10)
		END

	IF EXISTS (SELECT * from dbo.BPM_ORDR_ADR WITH (NOLOCK)
								where @ORDR_ID = ORDR_ID)
		BEGIN
			DELETE dbo.BPM_ORDR_ADR WHERE ORDR_ID = @ORDR_ID
		END

END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH



