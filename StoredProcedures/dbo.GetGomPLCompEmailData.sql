-- =============================================      
-- Author:  dlp0278     
-- Create date: 08/16/2011      
-- Description: Get the required data for an MDS Event when a H5/H6 value is entered. 
 -- Update date: <06/06/2018>
 -- Updated by: Md M Monir  
 -- Description: <To get ORDR_CNTCT Data>  EMAIL_ADDR     
-- =============================================      
ALTER PROCEDURE [dbo].[GetGomPLCompEmailData]       
 @OrderID  Varchar(20) = ''      
   
AS      
BEGIN      
SET NOCOUNT ON      
Begin Try      
     
 OPEN SYMMETRIC KEY FS@K3y       
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;      
    
 IF @OrderID != ''     
  BEGIN     
   SELECT  DISTINCT   
     f.FTN ,   
     ISNULL(nc.PORT_ASMT_DES,'') AS PORT_ASMT_DES,   
     ISNULL(nc.TOC_NME,'')  AS TOC_NME,   
     ISNULL(nc.FMS_CKT_ID,'') AS FMS_CKT_ID,   
     ISNULL(nc.NUA_ADR,'')  AS NUA_ADR,   
     ISNULL(fogx.PLN_NME,ISNULL(nc.PLN_NME,''))  AS PLN_NME,   
     ISNULL(nc.PLN_SEQ_NBR,'') AS PLN_SEQ_NBR,   
     --IsNull(dbo.decryptBinaryData(ordc.EMAIL_ADR),'') AS PAC,  
     --IsNull(dbo.decryptBinaryData(oc.EMAIL_ADR),'') AS SIS, 
     ISNULL(CASE WHEN (O.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_EMAIL_ADR) ELSE ordc.EMAIL_ADR  END,'') AS PAC,
     ISNULL(CASE WHEN (O.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd2.CUST_EMAIL_ADR) ELSE oc.EMAIL_ADR  END,'') AS SIS,  
     ISNULL(er.EMAIL_CC_TXT,'') AS EMAIL_CC  
   FROM dbo.FSA_ORDR f WITH (NOLOCK)  
  LEFT JOIN dbo.FSA_ORDR_GOM_XNCI fogx WITH (NOLOCK) ON f.ORDR_ID = fogx.ORDR_ID  
                AND fogx.GRP_ID = 1    
  LEFT JOIN dbo.NRM_CKT nc WITH (NOLOCK) on f.FTN = nc.FTN  
  LEFT JOIN dbo.ORDR_CNTCT ordc WITH (NOLOCK) ON f.ORDR_ID  = ordc.ORDR_ID   
              AND ordc.ROLE_ID = 13   
  LEFT JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON f.ORDR_ID  = oc.ORDR_ID   
              AND oc.ROLE_ID  = 11  
  INNER JOIN dbo.EMAIL_REQ er WITH (NOLOCK) ON f.ORDR_ID = er.ORDR_ID
  LEFT JOIN COWS.dbo.[CUST_SCRD_DATA] csd WITH (NOLOCK) ON csd.[SCRD_OBJ_ID]=ordc.[ORDR_CNTCT_ID] and csd.[SCRD_OBJ_TYPE_ID]=15 
  LEFT JOIN COWS.dbo.[CUST_SCRD_DATA] csd2 WITH (NOLOCK) ON csd2.[SCRD_OBJ_ID]=oc.[ORDR_CNTCT_ID] and csd2.[SCRD_OBJ_TYPE_ID]=15
  LEFT JOIN COWS.dbo.ORDR O WITH (NOLOCK) ON O.ORDR_ID=f.ORDR_ID                   
   WHERE
    f.ORDR_ID = @OrderID   AND
    er.EMAIL_REQ_TYPE_ID = 9  AND  
    er.STUS_ID = 10  
  END  
End Try      
      
Begin Catch      
 EXEC [dbo].[insertErrorInfo]      
End Catch      
END 