USE [COWS]
GO
_CreateObject 'SP','dbo','insertReqHdr_V5U'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 06/16/2015
-- Description:	Inserts an SCM Header Request into the PS_REQ_HDR table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertReqHdr_V5U]
			@ORDR_ID int  
           ,@REQSTN_NBR varchar(10)
           ,@CUST_ELID varchar(10) = null
           ,@DLVY_CLLI varchar(10)= null
           ,@DLVY_NME varchar(30) = null
           ,@DLVY_ADDR1 varchar(35) = null
           ,@DLVY_ADDR2 varchar(35) = null
           ,@DLVY_ADDR3 varchar(35) = null
           ,@DLVY_ADDR4 varchar(35) = null
           ,@DLVY_CTY varchar(30) = null
           ,@DLVY_CNTY varchar(3) = null
           ,@DLVY_ST varchar(2) = null
           ,@DLVY_ZIP varchar(12) = null
           ,@DLVY_PHN_NBR varchar(24) = null
           ,@INST_CLLI varchar(10) = null
           ,@MRK_PKG varchar(20) = null
           ,@REF_NBR varchar(12) = null
           ,@SHIP_CMMTS varchar(529) = null
           ,@RFQ_INDCTR varchar(1) = null
           ,@DLVY_BLDG varchar(10) = null
           ,@DLVY_FLR varchar(5) = null
           ,@DLVY_RM varchar(5) = null
           ,@RAS_DT  Datetime
AS
BEGIN
SET NOCOUNT ON;




BEGIN TRY

		DECLARE @CSG_LVL varchar (5)
		
		SELECT @CSG_LVL = ISNULL(lcl.CSG_LVL_CD,'00000')
		FROM dbo.ORDR ordr WITH (NOLOCK) LEFT JOIN
		dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_ID=ordr.CSG_LVL_ID
		WHERE ordr.ORDR_ID = @ORDR_ID



		INSERT INTO [dbo].[PS_REQ_HDR_QUEUE]
			   ([ORDR_ID]
			   ,[REQSTN_NBR]
			   ,[CUST_ELID]
			   ,[DLVY_CLLI]
			   ,[DLVY_NME]
			   ,[DLVY_ADDR1]
			   ,[DLVY_ADDR2]
			   ,[DLVY_ADDR3]
			   ,[DLVY_ADDR4]
			   ,[DLVY_CTY]
			   ,[DLVY_CNTY]
			   ,[DLVY_ST]
			   ,[DLVY_ZIP]
			   ,[DLVY_PHN_NBR]
			   ,[INST_CLLI]
			   ,[MRK_PKG]
			   ,[REQSTN_DT]
			   ,[REF_NBR]
			   ,[SHIP_CMMTS]
			   ,[RFQ_INDCTR]
			   ,[REC_STUS_ID]
			   ,[CREAT_DT]
			   ,[SENT_DT]
			   ,[DLVY_BLDG]
			   ,[DLVY_FLR]
			   ,[DLVY_RM]
			   ,[CSG_LVL])
		 VALUES
			   (@ORDR_ID
			   ,@REQSTN_NBR
			   ,@CUST_ELID
			   ,@DLVY_CLLI
			   ,@DLVY_NME
			   ,@DLVY_ADDR1
			   ,@DLVY_ADDR2
			   ,@DLVY_ADDR3
			   ,@DLVY_ADDR4
			   ,@DLVY_CTY
			   ,UPPER(@DLVY_CNTY)
			   ,UPPER(@DLVY_ST)
			   ,@DLVY_ZIP
			   ,@DLVY_PHN_NBR
			   ,@INST_CLLI
			   ,@MRK_PKG
			   ,getdate()
			   ,@REF_NBR
			   ,@SHIP_CMMTS
			   ,@RFQ_INDCTR
			   ,1 --REC_STUS_ID
			   ,getdate() --CREAT_DT
			   ,null --SENT_DT
			   ,@DLVY_BLDG
			   ,@DLVY_FLR
			   ,@DLVY_RM
			   ,@CSG_LVL 
			   )
			   
			   
	UPDATE dbo.PS_REQ_LINE_ITM_QUEUE
	SET RAS_DT = @RAS_DT
	WHERE REQSTN_NBR = @REQSTN_NBR
	
	UPDATE dbo.ORDR
	SET RAS_DT = @RAS_DT
	WHERE ORDR_ID = @ORDR_ID
	
		
END TRY

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch

END

