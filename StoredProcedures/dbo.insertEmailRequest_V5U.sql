USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertEmailRequest_V5U]    Script Date: 02/03/2016 15:39:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		dlp0278
-- Create date: 07/23/2015
-- Description:	Inserts an BPM Email Request to be send.
-- =============================================
CREATE PROCEDURE [dbo].[insertEmailRequest_V5U] 
	@ORDR_ID			Int
	,@DEVICE_ID         varchar (25)
	,@EMAIL_REQ_TYPE_ID	Int
	,@STUS_ID			Int
	,@FTN               varchar(50)
	,@EMAIL_LIST_TXT	Varchar(Max)	=	NULL
	,@EMAIL_CC_TXT      Varchar(Max)    =   NULL
	,@EMAIL_NTE   		Varchar(Max)	=	NULL
	,@EMAIL_JPRDY_NTE   Varchar(Max)	=	NULL	

AS
BEGIN
Begin Try

	
	INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
				(ORDR_ID
				,EMAIL_REQ_TYPE_ID
				,STUS_ID
				,EMAIL_LIST_TXT
				,EMAIL_CC_TXT 
				,EMAIL_SUBJ_TXT
				,EMAIL_BODY_TXT)
		VALUES	(@ORDR_ID
				,@EMAIL_REQ_TYPE_ID
				,10
				,@EMAIL_LIST_TXT
				,@EMAIL_CC_TXT 
				,'Returned To Sales Order: ' + @FTN + ' -  DEVICE: ' + @DEVICE_ID
				,@EMAIL_JPRDY_NTE + '  :  ' + @EMAIL_NTE)
					
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

