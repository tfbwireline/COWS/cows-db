USE [COWS]
GO
_CreateObject 'SP','web','UnlockItems'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================  
-- Author:  vn370313  
-- Create date: 08/26/2011  
-- Description: Locks/Unlocks the 0=Orders  1=Events 2=Redsgn  3=CPT.  
-- =========================================================  
ALTER PROCEDURE [web].[UnlockItems]  
  @ORDR_ID int  
 ,@EVENT_ID int  
 ,@USER_ID Int  
 ,@IsOrder int  
 ,@Unlock int   
 ,@IsLocked Int   
 ,@ItemType Int  
AS  
BEGIN  
SET NOCOUNT ON;  
Begin Try 
Declare @Success varchar(10)='Failed'
IF (@ItemType=0)  /*0=Ordr*/
BEGIN
	IF @Unlock = 1  
	 BEGIN  	   
	   DELETE FROM dbo.ORDR_REC_LOCK WITH (ROWLOCK)  
		WHERE LOCK_BY_USER_ID = @USER_ID  
		Set @Success ='Success'
	  END  
	 ELSE   
	  BEGIN  	  
	   -----------------------------------------------------------  
	   -- Check the Order locks and then if NOT Locked lock it.  
	   -----------------------------------------------------------  
		
		 IF NOT EXISTS  
		  (SELECT 'X' FROM dbo.ORDR_REC_LOCK WITH (NOLOCK)  
		   WHERE ORDR_ID   = @ORDR_ID)  
		  BEGIN  
		   INSERT INTO dbo.ORDR_REC_LOCK WITH (ROWLOCK)  
			  (ORDR_ID  
			  ,STRT_REC_LOCK_TMST  
			  ,LOCK_BY_USER_ID)  
			VALUES (@ORDR_ID  
			  ,getDate()  
			  ,@USER_ID) 
			Set @Success ='Success' 
		    
		END        
	  END  
END 
IF (@ItemType=1)  /* 1=Event*/
BEGIN
 IF @Unlock = 1  
  BEGIN  
		DELETE FROM dbo.EVENT_REC_LOCK WITH (ROWLOCK)  
		WHERE LOCK_BY_USER_ID = @USER_ID  
   Set @Success ='Success' 
  END  
 ELSE   
  BEGIN       
      -----------------------------------------------------------  
	   -- Check the Event locks and then if NOT Locked lock it.  
	   -----------------------------------------------------------  
	           
		 IF NOT EXISTS  
		  (SELECT 'X' FROM dbo.EVENT_REC_LOCK WITH (NOLOCK)  
		   WHERE EVENT_ID  = @EVENT_ID)  
		  BEGIN  
		   INSERT INTO dbo.EVENT_REC_LOCK WITH (ROWLOCK)  
			  (EVENT_ID  
			  ,STRT_REC_LOCK_TMST  
			  ,LOCK_BY_USER_ID)  
		   VALUES (@EVENT_ID  
			 ,getDate()  
			 ,@USER_ID) 
		   Set @Success ='Success'        
		  END    		
  
  END  
END
IF (@ItemType=2)  /* 2=REdsgn*/
BEGIN
 IF @Unlock = 1  
  BEGIN  
   DELETE FROM REDSGN_REC_LOCK WITH (ROWLOCK)  
    WHERE LOCK_BY_USER_ID = @USER_ID 
   Set @Success ='Success' 
  END  
 ELSE   
  BEGIN  
     
     SELECT  @IsLocked = 1  
      WHERE EXISTS  
        (SELECT 'X' FROM dbo.REDSGN_REC_LOCK AS [t1] WITH (NOLOCK)  
         INNER JOIN dbo.REDSGN AS [t0] WITH (NOLOCK) ON  [t0].[REDSGN_ID] = [t1].[REDSGN_ID]   
         WHERE [t0].[REDSGN_ID]   = @ORDR_ID  
          AND [t1].[LOCK_BY_USER_ID] != @USER_ID)  
            
     IF @IsLocked = 0  
     AND NOT EXISTS  
      (SELECT 'X' FROM dbo.REDSGN_REC_LOCK AS [t1] WITH (NOLOCK)  
         INNER JOIN dbo.REDSGN AS [t0] WITH (NOLOCK) ON  [t0].[REDSGN_ID] = [t1].[REDSGN_ID]   
         WHERE [t0].[REDSGN_ID]   = @ORDR_ID  
        AND LOCK_BY_USER_ID = @USER_ID)  
      BEGIN  
       INSERT INTO dbo.REDSGN_REC_LOCK WITH (ROWLOCK)  
          (REDSGN_ID  
          ,STRT_REC_LOCK_TMST  
          ,LOCK_BY_USER_ID,CREAT_DT)  
        SELECT [REDSGN_ID], GETDATE(), @USER_ID,GETDATE()  
        FROM dbo.REDSGN  WITH (NOLOCK)   
        WHERE [REDSGN_ID]   = @ORDR_ID  
        Set @Success ='Success' 
      END   
  
  END  
END
IF (@ItemType=3)  /* 2=CPT*/
BEGIN
	IF @Unlock = 1  
  BEGIN  
   DELETE FROM CPT_REC_LOCK WITH (ROWLOCK)  
    WHERE CPT_ID = @ORDR_ID   /**For CPT it will be CPTID*/ 
    Set @Success ='Success' 
  END 
END   

 SELECT  
	  @ORDR_ID [ORDR_ID],  
	  1 [LOCK_BY_USER_ID],  
	  '' [FULL_NME],  
	  @Success [FTN],  
	  '' [PROD_TYPE_DES],  
	  '' [ORDR_TYPE_DES]  
End Try  
  
Begin Catch  
 EXEC [dbo].[insertErrorInfo]  
End Catch  
END


  /*
-- =========================================================  
-- Author:  sbg9814  
-- Create date: 08/26/2011  
-- Description: Locks/Unlocks the Orders and Events.  
-- =========================================================  
CREATE PROCEDURE [dbo].[lockUnlockOrdersEvents]  
 @ORDR_ID Int = 0  
 ,@EVENT_ID Int = 0  
 ,@USER_ID Int  
 ,@IsOrder Bit  
 ,@Unlock Bit   
 ,@IsLocked Int   
   
AS  
BEGIN  
SET NOCOUNT ON;  
Begin Try  
 IF @Unlock = 1  
  BEGIN  
   DELETE FROM dbo.EVENT_REC_LOCK WITH (ROWLOCK)  
    WHERE LOCK_BY_USER_ID = @USER_ID  
   DELETE FROM dbo.ORDR_REC_LOCK WITH (ROWLOCK)  
    WHERE LOCK_BY_USER_ID = @USER_ID  
  END  
 ELSE   
  BEGIN  
   -----------------------------------------------------------  
   -- Check the Event locks and then if NOT Locked lock it.  
   -----------------------------------------------------------  
   IF (@IsOrder = 0)  
    BEGIN            
     IF NOT EXISTS  
      (SELECT 'X' FROM dbo.EVENT_REC_LOCK WITH (NOLOCK)  
       WHERE EVENT_ID  = @EVENT_ID)  
      BEGIN  
       INSERT INTO dbo.EVENT_REC_LOCK WITH (ROWLOCK)  
          (EVENT_ID  
          ,STRT_REC_LOCK_TMST  
          ,LOCK_BY_USER_ID)  
       VALUES (@EVENT_ID  
         ,getDate()  
         ,@USER_ID)         
      END    
    END     
   ELSE  
   -----------------------------------------------------------  
   -- Check the Order locks and then if NOT Locked lock it.  
   -----------------------------------------------------------  
    BEGIN  
     IF NOT EXISTS  
      (SELECT 'X' FROM dbo.ORDR_REC_LOCK WITH (NOLOCK)  
       WHERE ORDR_ID   = @ORDR_ID)  
      BEGIN  
       INSERT INTO dbo.ORDR_REC_LOCK WITH (ROWLOCK)  
          (ORDR_ID  
          ,STRT_REC_LOCK_TMST  
          ,LOCK_BY_USER_ID)  
        VALUES (@ORDR_ID  
          ,getDate()  
          ,@USER_ID)  
      END   
    END        
  END  
    
 SELECT @IsLocked  
End Try  
  
Begin Catch  
 EXEC [dbo].[insertErrorInfo]  
End Catch  
END

*/