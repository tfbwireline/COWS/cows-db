USE [COWS]
GO
_CreateObject 'SP','web','InsertMDSEventInfoData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================          
-- Author:  Suman Chitemella          
-- Create date: 2012-7-12          
-- Description: This SP is used to insert MDS Event Data          
--********************************************************    
-- This is the New MDS rewrite    
--********************************************************  
-- Updated By:  MD M Monir  VN370313          
-- Create date: 2017-03-03          
-- Description: NO Redesign Related Action on [dbo].[LK_MDS_MAC_ACTY].MDS_MAC_ACTY_ID      10(other) and 4 (SPS)
-- =========================================================          
          
ALTER PROCEDURE [web].[InsertMDSEventInfoData]         
 @EventID BIGINT,         
 @EventData NVARCHAR(MAX),        
 @ODIEDevInfo NVARCHAR(MAX) = NULL,        
 @AccessInfo NVARCHAR(MAX) = NULL,        
 @CPEInfo NVARCHAR(MAX),        
 @MDSMNSInfo NVARCHAR(MAX) = NULL,        
 @Wired NVARCHAR(MAX) = NULL,        
 @SpAETransport NVARCHAR(MAX) = NULL,        
 @DSLTransport NVARCHAR(MAX) = NULL,        
 @SpRFTransport NVARCHAR(MAX) = NULL,      
 @VLAN NVARCHAR(MAX) = NULL,    
 @Wireless NVARCHAR(MAX) = NULL,        
 @PVC NVARCHAR(MAX) = NULL,
 @SrvcTbl NVARCHAR(MAX) = NULL,        
 @ID INT OUTPUT         
AS        
 OPEN SYMMETRIC KEY FS@K3y         
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;        
 DECLARE @hDoc   INT             
 DECLARE @DoRedesgn   INT  /*vn370313*/         
DECLARE @FSAEventData TABLE          
     (     
 FSA_MDS_EVENT_ID int ,    
 TAB_NME varchar(200) ,    
 ORDR_EVENT_STUS_ID tinyint ,    
 TAB_SEQ_NBR tinyint ,    
 --H5_H6_CUST_ID dbo.H6 ,    
     
 --TAB_DLTD_CD bit ,    
 INSTL_SITE_POC_NME VARCHAR(200) ,    
 INSTL_SITE_POC_INTL_PHN_CD varchar(50) ,    
 INSTL_SITE_POC_PHN_NBR varchar(50) ,    
 INSTL_SITE_POC_INTL_CELL_PHN_CD varchar(16) ,    
 INSTL_SITE_POC_CELL_PHN_NBR varchar(16) ,    
 SRVC_ASSRN_POC_NME VARCHAR(200) ,    
 SRVC_ASSRN_POC_INTL_PHN_CD varchar(50) ,    
 SRVC_ASSRN_POC_PHN_NBR varchar(50) ,    
 SRVC_ASSRN_POC_INTL_CELL_PHN_CD varchar(20) ,    
 SRVC_ASSRN_POC_CELL_PHN_NBR varchar(20) ,   
 SA_POC_HR_NME VARCHAR(50), 
 TME_ZONE_ID VARCHAR(50),
 US_INTL_ID VARCHAR(200) ,    
 SITE_ADR VARCHAR(200) ,    
 FLR_BLDG_NME VARCHAR(200) ,    
 CTY_NME VARCHAR(200) ,    
 STT_PRVN_NME VARCHAR(200) ,    
 CTRY_RGN_NME VARCHAR(200) ,    
 ZIP_CD VARCHAR(200) ,    
 WIRED_DEV_TRNSPRT_REQR_CD bit ,    
 WRLS_TRNSPRT_REQR_CD bit ,    
 VRTL_CNCTN_CD bit ,    
 SPRF_DEV_TRNSPRT_REQR_CD bit,    
 SPAE_TRNSPRT_REQR_CD bit,
 
 DSL_TRNSPRT_REQR_CD bit,  
 MACH5_SRVC_REQR_CD bit,  
 CREAT_BY_USER_ID int ,    
 EVENT_SCHED_NTE_TXT varchar(255),    
 CMPLTD_CD bit ,    
 EVENT_ID int )        
           
  DECLARE @ODIEDevNme TABLE          
     (FSA_MDS_EVENT_ID int  ,    
  ODIE_DEV_NME varchar(200)    
  , TAB_SEQ_NBR SMALLINT)    
       
  DECLARE @MDS_EVENT_ACCSS TABLE          
     (FSA_MDS_EVENT_ID int ,    
  ACCS_FTN_NBR varchar(20) ,    
  ORDR_ID int    
  , TAB_SEQ_NBR SMALLINT)    
       
  DECLARE @MDS_EVENT_CPE TABLE          
  (FSA_MDS_EVENT_ID int ,    
  CPE_FTN_NBR varchar(20) ,    
  EQPT_DES varchar(250) ,    
  EQPT_VNDR_NME varchar(250) ,    
  ORDR_ID int    
  , TAB_SEQ_NBR SMALLINT    
  )      
     
 DECLARE @MDS_EVENT_MNS TABLE          
     (FSA_MDS_EVENT_ID int ,    
  MNS_FTN_NBR varchar(20) ,    
  MDS_SRVC_TIER_ID tinyint ,    
  MDS_ENTLMNT_OE_ID varchar(250) ,    
  ORDR_ID int    
  , TAB_SEQ_NBR SMALLINT)     
          
  DECLARE @MDS_EVENT_WIRED_TRPT TABLE          
     (FSA_MDS_EVENT_ID int ,    
  MDS_TRPT_TYPE_ID tinyint ,    
  PRIM_BKUP_CD char(1) ,    
  BDWD_CHNL_NME varchar(100) ,    
  FMS_CKT_ID varchar(100) ,    
  PL_NBR varchar(20) ,    
  IP_NUA_ADR varchar(25) ,    
  OLD_CKT_ID varchar(30) ,    
  MULTI_LINK_CKT_CD char(1) ,    
  SPRINT_MNGD_CD char(1) ,    
  TELCO_ID tinyint ,    
  FOC_DT datetime, TAB_SEQ_NBR SMALLINT)     
     
  DECLARE @MDS_EVENT_SPAE_TRPT TABLE          
     (FSA_MDS_EVENT_ID int ,    
  MDS_TRPT_TYPE_ID tinyint ,    
  PRIM_BKUP_CD char(1) ,    
  BDWD_CHNL_NME varchar(100) ,    
  --FMS_CKT_ID varchar(100) ,    
  ECCKT_ID varchar(20) ,    
  UNI_NUA_ADR varchar(25) ,    
  OLD_CKT_ID varchar(30) ,    
  SPRINT_MNGD_CD char(1) ,    
  TELCO_ID tinyint ,    
  FOC_DT datetime, TAB_SEQ_NBR SMALLINT)     

  DECLARE @MDS_EVENT_DSL_TRPT TABLE          
     (FSA_MDS_EVENT_ID int ,     
  PRIM_BKUP_CD char(1) ,    
  IP_ADDR varchar(100) ,    
  SUBNET_MASK varchar(100) ,    
  NHOP_GWY varchar(100) ,    
  PrvdrCKTID varchar(100) ,    
  SPRINT_CUST_CD char(1) ,    
  SPRINT_MNGD_CD char(1) ,    
  TELCO_ID tinyint ,    
  TAB_SEQ_NBR SMALLINT) 
    
  DECLARE @MDS_EVENT_SPRINT_LINK_ATM_TRPT TABLE          
     (FSA_MDS_EVENT_ID int ,    
  MDS_TRPT_TYPE_ID tinyint ,    
  PRIM_BKUP_CD char(1) ,    
  BDWD_CHNL_NME varchar(100) ,    
  FMS_CKT_ID varchar(100) ,    
  PL_NBR varchar(20) ,    
  IP_NUA_ADR varchar(25) ,    
  OLD_CKT_ID varchar(30) ,    
  --MULTI_LINK_CKT_CD char(1) ,    
  SPRINT_MNGD_CD char(1) ,    
  --TELCO_ID tinyint ,    
  FOC_DT datetime, TAB_SEQ_NBR SMALLINT)     
      
 DECLARE @VLAN_Temp TABLE    
 (FSA_MDS_EVENT_ID int,    
  VLAN_NBR varchar(10),    
  SPA_NUA_ADR varchar(20), TAB_SEQ_NBR SMALLINT)      
      
  DECLARE @MDS_EVENT_WRLS_TRPT TABLE          
     (FSA_MDS_EVENT_ID int ,    
   PRIM_BKUP_CD char(1) ,    
   ESN_MAC_ID varchar(30), TAB_SEQ_NBR SMALLINT)      
    
  DECLARE @MDS_EVENT_VRTL_CNCTN TABLE          
     (FSA_MDS_EVENT_ID int ,    
  FMS_NME varchar(200) ,    
  DLCI_VPI_CD varchar(200), TAB_SEQ_NBR SMALLINT)     
/*---------------------------------------*/             
    
DECLARE @MDS_EVENT_WIRED_TRNSPRT_DEV TABLE          
     ( TAB_SEQ_NBR SMALLINT,        
      FSA_MDS_EVENT_ID INT,        
      TRPT_FTN   Varchar(20),        
      MDS_TRNSPRT_TYPE_ID TINYINT,        
      TELCO_ID TINYINT,        
      PRIM_BKUP_CD VARCHAR(1),        
      FMS_CKT_ID VARCHAR(10),        
      PL_NBR VARCHAR(20),        
      NUA_449_ADR VARCHAR(25),        
      OLD_CKT_ID VARCHAR(10),        
      MULTI_LINK_CKT_CD VARCHAR(1),        
      NEW_FMS_CKT_ID   VARCHAR(10),        
      NEW_NUA VARCHAR(25),        
      SPRINT_MNGD_CD VARCHAR(1),        
      FOC_DT DATETIME,        
      CREAT_DT DATETIME)     
          
  DECLARE @MDS_EVENT_MNS_OE TABLE          
     ( TAB_SEQ_NBR SMALLINT,        
      FSA_MDS_EVENT_ID INT,        
      MNS_OE_FTN VARCHAR(20), -- Col length Change in actual table        
      MNS_OE_TYPE_ID TINYINT,        
      MNS_OE_ID TINYINT,        
      MDS_SRVC_TIER_ID TINYINT,        
      MDS_ENTLMNT_ID TINYINT,        
      MDS_ENTLMNT_OE_ID VARCHAR(1000),        
      ORDR_ID INT,        
      CREAT_DT DATETIME)        
              
  DECLARE @MDS_EVENT_PVC TABLE          
     ( TAB_SEQ_NBR SMALLINT,        
      FSA_MDS_EVENT_ID INT,        
      DLCI_ID VARCHAR(25),         
      CREAT_DT DATETIME)        
          
  DECLARE @MDS_EVENT_WRLS_TRNSPRT_DEV TABLE          
     ( TAB_SEQ_NBR SMALLINT,        
      FSA_MDS_EVENT_ID INT,        
      PRIM_BKUP_CD VARCHAR(1),         
      ESN VARCHAR(30),         
      CREAT_DT DATETIME)        
          
  DECLARE @MDS_EVENT_MNGD_DEV TABLE        
  (TAB_SEQ_NBR SMALLINT,        
      FSA_MDS_EVENT_ID INT,        
      FTN VARCHAR(20),        
      DEV_NME VARCHAR(200),        
      EQPT_DES VARCHAR(50),        
      EQPT_VNDR_NME  VARCHAR(50),        
      MANF_ID SMALLINT,        
      DEV_MODEL_ID SMALLINT,        
     
      ORDR_ID INT)      
	  
  DECLARE @MDS_EVENT_SRVC TABLE
  (FSA_MDS_EVENT_ID INT,        
      MACH5_H6_SRVC_ID INT,        
      SRVC_TYPE_ID TINYINT,        
      THRD_PARTY_VNDR_ID TINYINT,        
      ThirdParty  VARCHAR(20),        
      THRD_PARTY_SRVC_LVL_ID TINYINT,        
      ACTV_DT SMALLDATETIME,        
      CMNT_TXT VARCHAR(2000),
	  EMAIL_CD BIT,
	  TAB_SEQ_NBR TINYINT)
	  
  DECLARE @OldSrvcTbl TABLE
  (EVENT_ID INT,
   TAB_SEQ_NBR TINYINT,
   MACH5_H6_SRVC_ID INT,
   SRVC_TYPE_ID TINYINT,
   ACTV_DT SMALLDATETIME,
   EMAIL_CD BIT)
        
BEGIN        
 SET NOCOUNT ON;        
 BEGIN TRY        
  BEGIN TRAN insertNewMDSEventData        
          
  IF (LEN(@EventData) > 10)        
  BEGIN        
    
  INSERT INTO @OldSrvcTbl
  SELECT DISTINCT fme.Event_ID, fme.TAB_SEQ_NBR, mes.MACH5_H6_SRVC_ID, mes.SRVC_TYPE_ID, mes.ACTV_DT, mes.EMAIL_CD
  FROM dbo.MDS_EVENT_SRVC mes WITH (NOLOCK) INNER JOIN
       dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON mes.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID
  WHERE (mes.EMAIL_CD = 1)
    AND fme.EVENT_ID = @EventID
    
  delete from dbo.MDS_EVENT_ODIE_DEV_NME WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)               
  delete from dbo.MDS_EVENT_ACCS WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)               
  delete from dbo.MDS_EVENT_CPE WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)               
  delete from dbo.MDS_EVENT_MNS WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)               
  delete from dbo.MDS_EVENT_WIRED_TRPT WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)        
  delete from dbo.MDS_EVENT_SPAE_TRPT WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)        
  delete from dbo.MDS_EVENT_DSL_TRPT WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)        
  delete from dbo.MDS_EVENT_SPRINT_LINK_ATM_TRPT WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)        
  delete from dbo.MDS_EVENT_VLAN WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)        
  delete from dbo.MDS_EVENT_WRLS_TRPT WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)        
  delete from dbo.MDS_EVENT_VRTL_CNCTN WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)        
  delete from dbo.MDS_EVENT_SRVC WITH (ROWLOCK) where FSA_MDS_EVENT_ID in  (SELECT FSA_MDS_EVENT_ID FROM dbo.FSA_MDS_EVENT_NEW WITH (NOLOCK) WHERE EVENT_ID = @EventID)
  delete from dbo.FSA_MDS_EVENT_ORDR WITH (ROWLOCK) where event_id = @EventID    
  delete from dbo.FSA_MDS_EVENT_NEW WITH (ROWLOCK) where event_id = @EventID        
          
   EXEC sp_xml_preparedocument @hDoc OUTPUT, @EventData        
           
   SET @ID = -1        
    
           
    INSERT INTO @FSAEventData        
 SELECT *, @EventID AS EVENT_ID    
  FROM OPENXML (@hDoc, '/DocumentElement/EventData', 2)        
  WITH (    
    FSA_MDS_EVENT_ID int ,    
    TAB_NME varchar(200) ,    
    ORDR_EVENT_STUS_ID tinyint ,    
 TAB_SEQ_NBR tinyint ,    
 --substring(TAB_NME, 1, 9) VARCHAR(200) ,    
     
 --TAB_DLTD_CD bit ,    
 INSTL_SITE_POC_NME VARCHAR(200) ,    
 INSTL_SITE_POC_INTL_PHN_CD varchar(50) ,    
 INSTL_SITE_POC_PHN_NBR varchar(50) ,    
 INSTL_SITE_POC_INTL_CELL_PHN_CD varchar(16) ,    
 INSTL_SITE_POC_CELL_PHN_NBR varchar(16) ,    
 SRVC_ASSRN_POC_NME VARCHAR(200) ,    
 SRVC_ASSRN_POC_INTL_PHN_CD varchar(50) ,    
 SRVC_ASSRN_POC_PHN_NBR varchar(50) ,    
 SRVC_ASSRN_POC_INTL_CELL_PHN_CD varchar(20) ,   
 SRVC_ASSRN_POC_CELL_PHN_NBR varchar(20) ,
 SA_POC_HR_NME VARCHAR(50),    
 TME_ZONE_ID VARCHAR(50),
 US_INTL_ID VARCHAR(200) ,    
 SITE_ADR VARCHAR(200) ,    
 FLR_BLDG_NME VARCHAR(200) ,    
 CTY_NME VARCHAR(200) ,    
 STT_PRVN_NME VARCHAR(200) ,    
 CTRY_RGN_NME VARCHAR(200) ,    
 ZIP_CD VARCHAR(200) ,    
 WIRED_DEV_TRNSPRT_REQR_CD bit ,    
 WRLS_TRNSPRT_REQR_CD bit ,    
 VRTL_CNCTN_CD bit ,    
 SPRF_DEV_TRNSPRT_REQR_CD bit,    
 SPAE_TRNSPRT_REQR_CD bit, 
 
 DSL_TRNSPRT_REQR_CD bit, 
 MACH5_SRVC_REQR_CD bit,  
 CREAT_BY_USER_ID int ,    
 EVENT_SCHED_NTE_TXT varchar(255),    
 CMPLTD_CD bit )          
    
  INSERT INTO FSA_MDS_EVENT_NEW        
  (EVENT_ID  ,    
 TAB_NME  ,    
 ORDR_EVENT_STUS_ID ,    
 TAB_SEQ_NBR ,    
 H5_H6_CUST_ID,      
 --TAB_DLTD_CD  ,    
 INSTL_SITE_POC_NME  ,    
 INSTL_SITE_POC_INTL_PHN_CD,    
 INSTL_SITE_POC_PHN_NBR  ,    
 INSTL_SITE_POC_INTL_CELL_PHN_CD  ,    
 INSTL_SITE_POC_CELL_PHN_NBR  ,    
 SRVC_ASSRN_POC_NME  ,    
 SRVC_ASSRN_POC_INTL_PHN_CD  ,    
 SRVC_ASSRN_POC_PHN_NBR  ,    
 SRVC_ASSRN_POC_INTL_CELL_PHN_CD  ,    
 SRVC_ASSRN_POC_CELL_NBR  ,    
 SA_POC_HR_NME,
 TME_ZONE_ID,
 US_INTL_ID  ,    
 SITE_ADR  ,    
 FLR_BLDG_NME  ,    
 CTY_NME  ,    
 STT_PRVN_NME  ,    
 CTRY_RGN_NME  ,    
 ZIP_CD  ,    
 WIRED_DEV_TRPT_REQR_CD  ,    
 WRLS_TRPT_REQR_CD  ,    
 VRTL_CNCTN_CD  ,    
 SPRF_DEV_TRNSPRT_REQR_CD,    
 SPAE_TRNSPRT_REQR_CD,
 
 DSL_TRNSPRT_REQR_CD,   
 MACH5_SRVC_REQR_CD, 
 CREAT_BY_USER_ID  ,    
 CREAT_DT ,    
 EVENT_SCHED_NTE_TXT ,    
 CMPLTD_CD           
   )        
  SELECT @EventID,    
   TAB_NME,    
      ORDR_EVENT_STUS_ID,    
      TAB_SEQ_NBR,    
   substring(TAB_NME, 1, 9),    
   --CMPLTD_CD,    
   --TAB_DLTD_CD,    
   dbo.encryptString(INSTL_SITE_POC_NME),    
   INSTL_SITE_POC_INTL_PHN_CD,    
   INSTL_SITE_POC_PHN_NBR  ,    
      INSTL_SITE_POC_INTL_CELL_PHN_CD  ,    
   INSTL_SITE_POC_CELL_PHN_NBR  ,    
   dbo.encryptString(SRVC_ASSRN_POC_NME),    
   SRVC_ASSRN_POC_INTL_PHN_CD  ,    
      SRVC_ASSRN_POC_PHN_NBR  ,    
      SRVC_ASSRN_POC_INTL_CELL_PHN_CD  ,    
      SRVC_ASSRN_POC_CELL_PHN_NBR  , 
	  SA_POC_HR_NME,   
	  TME_ZONE_ID,
   CASE   
  WHEN CTRY_RGN_NME IN ('usaa','USA/West','USA/SW','USA/Pacific','usa/north east','USA/NE','usa/eastern','USA/East Coast','USA/EAST',  
        'usa/ sw','USA/ North East','usa/ america','USA West','USA /East coast','USA /East','USA / West','USA / Eastern / Central / Central',  
        'USA / Eastern','USA / East','USA / DOMESTIC','USA / Central','USA - Northwest','USA & International','USA',  
        'US1`','US/SE','US/PST','US/Pacific','us/ne','US/mountain','US/Easternb','US/Eastern','US/East','US/domestic',  
        'US/Central','US/ North East','US / Pacific','US / Central','US','Unities States','United States of Georgia',  
        'United States of America','UNITED STATES','United Staes','U.S.A.','U.S.','U*SA','U S','Eastern USA'  
        ) THEN dbo.encryptString('D')  
  ELSE dbo.encryptString('I')  
  END AS US_INTL_ID,      
   dbo.encryptString(SITE_ADR),     
   dbo.encryptString(FLR_BLDG_NME),     
   dbo.encryptString(CTY_NME),     
   dbo.encryptString(STT_PRVN_NME),     
   dbo.encryptString(CTRY_RGN_NME),    
   dbo.encryptString(ZIP_CD),    
   CASE COALESCE(WIRED_DEV_TRNSPRT_REQR_CD, 0) WHEN 0 THEN 0 ELSE WIRED_DEV_TRNSPRT_REQR_CD END AS WIRED_DEV_TRPT_REQR_CD ,     
   CASE COALESCE(WRLS_TRNSPRT_REQR_CD, 0) WHEN 0 THEN 0 ELSE WRLS_TRNSPRT_REQR_CD END AS WRLS_TRPT_REQR_CD ,     
   CASE COALESCE(VRTL_CNCTN_CD, 0) WHEN 0 THEN 0 ELSE VRTL_CNCTN_CD END AS VRTL_CNCTN_CD ,    
   CASE COALESCE(SPRF_DEV_TRNSPRT_REQR_CD, 0) WHEN 0 THEN 0 ELSE SPRF_DEV_TRNSPRT_REQR_CD END AS SPRF_DEV_TRNSPRT_REQR_CD ,    
   CASE COALESCE(SPAE_TRNSPRT_REQR_CD, 0) WHEN 0 THEN 0 ELSE SPAE_TRNSPRT_REQR_CD END AS SPAE_TRNSPRT_REQR_CD ,
   
   CASE COALESCE(DSL_TRNSPRT_REQR_CD, 0) WHEN 0 THEN 0 ELSE DSL_TRNSPRT_REQR_CD END AS DSL_TRNSPRT_REQR_CD ,  
   CASE COALESCE(MACH5_SRVC_REQR_CD, 0) WHEN 0 THEN 0 ELSE MACH5_SRVC_REQR_CD END AS MACH5_SRVC_REQR_CD ,  
   CREAT_BY_USER_ID,     
   GETDATE(),    
   EVENT_SCHED_NTE_TXT,    
   CMPLTD_CD      
  FROM @FSAEventData        
  WHERE FSA_MDS_EVENT_ID = 0        
          
  IF EXISTS         
   (SELECT 'X' FROM dbo.FSA_MDS_EVENT_NEW fde WITH (NOLOCK) JOIN @FSAEventData fed ON  fde.TAB_SEQ_NBR = fed.TAB_SEQ_NBR AND fde.EVENT_ID = @EVENTID)        
    SET @ID = 1        
                 
  EXEC sp_xml_removedocument @hDoc         
    
 -- ODIE Dev Info: ODIEDevInfo    
 IF (LEN(@ODIEDevInfo) > 10)    
 BEGIN    
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @ODIEDevInfo        
      
  INSERT INTO @ODIEDevNme      
  SELECT FSA_MDS_EVENT_ID     
   ,ODIE_DEV_NME    
   ,TAB_SEQ_NBR    
  FROM OPENXML(@hDoc, '/DocumentElement/ODIEDevInfo', 2)    
  WITH(FSA_MDS_EVENT_ID int     
    ,ODIE_DEV_NME varchar(200)      
    , TAB_SEQ_NBR SMALLINT    
    )    
    EXEC sp_xml_removedocument @hDoc         
        
     UPDATE odn                                                
      SET odn.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @ODIEDevNme odn         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = odn.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID         
           
    INSERT INTO dbo.MDS_EVENT_ODIE_DEV_NME     
  (FSA_MDS_EVENT_ID, ODIE_DEV_NME, TAB_SEQ_NBR)        
    SELECT FSA_MDS_EVENT_ID, ODIE_DEV_NME, TAB_SEQ_NBR        
  FROM @ODIEDevNme      
 END    
     
 -- Access Info    
 IF (LEN(@AccessInfo) > 10)    
 BEGIN    
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @AccessInfo        
      
  INSERT INTO @MDS_EVENT_ACCSS      
  SELECT FSA_MDS_EVENT_ID     
   ,ACCESS_FTN AS ACCS_FTN_NBR    
   ,ORDR_ID    
   ,TAB_SEQ_NBR    
  FROM OPENXML(@hDoc, '/DocumentElement/AccessInfo', 2)    
  WITH(FSA_MDS_EVENT_ID int   
    ,ACCESS_FTN varchar(20)    
    ,ORDR_ID int      
    , TAB_SEQ_NBR SMALLINT    
    )    
    EXEC sp_xml_removedocument @hDoc         
        
     UPDATE mea                                                
   SET mea.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_ACCSS mea         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = mea.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID         
           
    INSERT INTO dbo.MDS_EVENT_ACCS     
  (FSA_MDS_EVENT_ID     
   ,ACCS_FTN_NBR    
   ,ORDR_ID    
   , TAB_SEQ_NBR)        
    SELECT FSA_MDS_EVENT_ID     
   ,CASE COALESCE(ACCS_FTN_NBR, '') WHEN '' THEN '0' ELSE ACCS_FTN_NBR END AS ACCS_FTN_NBR    
   ,ORDR_ID    
   , TAB_SEQ_NBR        
  FROM @MDS_EVENT_ACCSS     
 END    
     
 -- CPE FTN Info    
 IF (LEN(@CPEInfo) > 10)    
 BEGIN    
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @CPEInfo       
       
  INSERT INTO @MDS_EVENT_CPE     
  SELECT FSA_MDS_EVENT_ID     
  ,CPE_FTN AS CPE_FTN_NBR    
  ,Equip_Desc AS EQPT_DES    
  ,Equip_Vend AS EQPT_VNDR_NME    
  ,ORDR_ID     
  ,TAB_SEQ_NBR     
  FROM OPENXML(@hDoc, '/DocumentElement/CPEInfo', 2)    
  WITH (MDS_EVENT_CPE_ID int,    
   FSA_MDS_EVENT_ID int ,
   CPE_FTN varchar(20) ,    
   Equip_Desc varchar(250) ,    
   Equip_Vend  varchar(250) ,    
   ORDR_ID int    
   , TAB_SEQ_NBR SMALLINT    
   )    
      
  EXEC sp_xml_removedocument @hDoc         
      
  UPDATE mec                                                
      SET mec.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_CPE mec         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = mec.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID     
      
  INSERT INTO dbo.MDS_EVENT_CPE     
  (FSA_MDS_EVENT_ID     
   ,CPE_FTN_NBR    
   ,EQPT_DES    
   ,EQPT_VNDR_NME    
   ,ORDR_ID     
   ,TAB_SEQ_NBR     
   )        
    SELECT FSA_MDS_EVENT_ID     
    ,CASE COALESCE(CPE_FTN_NBR, '') WHEN '' THEN '0' ELSE CPE_FTN_NBR END AS CPE_FTN_NBR    
    ,EQPT_DES    
    ,EQPT_VNDR_NME    
    ,ORDR_ID     
    ,TAB_SEQ_NBR        
  FROM @MDS_EVENT_CPE       
 END    
     
 -- MDS MNS Info    
 IF (LEN(@MDSMNSInfo) > 10)    
 BEGIN    
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @MDSMNSInfo        
      
  INSERT INTO @MDS_EVENT_MNS     
  SELECT FSA_MDS_EVENT_ID    
   ,MDS_MNS_FTN AS MNS_FTN_NBR     
   ,MDS_SRVC_TIER_ID     
   ,MDS_ENTLMNT_OE_ID     
   ,ORDR_ID     
   ,TAB_SEQ_NBR    
   FROM OPENXML(@hDoc, '/DocumentElement/MDSMNSInfo', 2)    
  WITH(FSA_MDS_EVENT_ID int
   ,MDS_MNS_FTN varchar(20)     
   ,MDS_SRVC_TIER_ID tinyint     
   ,MDS_ENTLMNT_OE_ID varchar(250)     
   ,ORDR_ID int    
   ,TAB_SEQ_NBR TINYINT    
   )    
  EXEC sp_xml_removedocument @hDoc         
      
  UPDATE mem                                                
      SET mem.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_MNS mem         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = mem.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID    
     
  INSERT INTO dbo.MDS_EVENT_MNS    
   (FSA_MDS_EVENT_ID    
    ,MNS_FTN_NBR     
    ,MDS_SRVC_TIER_ID     
    ,MDS_ENTLMNT_ID     
    ,ORDR_ID     
    ,TAB_SEQ_NBR    
   )    
  SELECT FSA_MDS_EVENT_ID    
   ,CASE COALESCE(MNS_FTN_NBR, '') WHEN '' THEN '0' ELSE MNS_FTN_NBR END AS MNS_FTN_NBR    
   ,CASE MDS_SRVC_TIER_ID WHEN 0 THEN NULL ELSE MDS_SRVC_TIER_ID END AS MDS_SRVC_TIER_ID     
   ,MDS_ENTLMNT_OE_ID     
   ,ORDR_ID     
   ,TAB_SEQ_NBR    
  FROM @MDS_EVENT_MNS          
 END    
     
 -- Transport: Wired    
 IF (LEN(@Wired) > 10)    
 BEGIN    
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @Wired        
      
  INSERT INTO @MDS_EVENT_WIRED_TRPT    
  SELECT  FSA_MDS_EVENT_ID    
   ,MDS_TRNSPRT_TYPE_ID AS MDS_TRPT_TYPE_ID    
   ,PRIM_BKUP_CD    
   ,BDWL_CHNL_NME AS BDWD_CHNL_NME    
   ,FMS_CKT_ID    
   ,PL_NBR    
   ,NUA_449_ADR AS IP_NUA_ADR    
   ,OLD_CKT_ID    
   ,MULTI_LINK_CKT_CD    
   ,SPRINT_MNGD_CD    
   ,TELCO_ID    
   ,FOC_DT    
   ,TAB_SEQ_NBR    
   FROM OPENXML(@hDoc, '/DocumentElement/Wired', 2)    
  WITH(FSA_MDS_EVENT_ID int     
   ,MDS_TRNSPRT_TYPE_ID tinyint     
   ,PRIM_BKUP_CD char(1)     
   ,BDWL_CHNL_NME varchar(100)     
   ,FMS_CKT_ID varchar(100)     
   ,PL_NBR varchar(20)     
   ,NUA_449_ADR varchar(25)     
   ,OLD_CKT_ID varchar(30)    
   ,MULTI_LINK_CKT_CD char(1)     
   ,SPRINT_MNGD_CD char(1)     
   ,TELCO_ID tinyint     
   ,FOC_DT datetime    
   ,TAB_SEQ_NBR SMALLINT    
   )     
      
  EXEC sp_xml_removedocument @hDoc         
      
   UPDATE mewt                                                
      SET mewt.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_WIRED_TRPT mewt         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = mewt.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID     
     
  INSERT INTO dbo.MDS_EVENT_WIRED_TRPT    
   (FSA_MDS_EVENT_ID    
   ,MDS_TRPT_TYPE_ID    
   ,PRIM_BKUP_CD    
   ,BDWD_CHNL_NME    
   ,FMS_CKT_ID    
   ,PL_NBR    
   ,IP_NUA_ADR    
   ,OLD_CKT_ID    
   ,MULTI_LINK_CKT_CD    
   ,SPRINT_MNGD_CD    
   ,TELCO_ID    
   ,FOC_DT    
   ,TAB_SEQ_NBR    
   )     
  SELECT FSA_MDS_EVENT_ID    
    ,MDS_TRPT_TYPE_ID    
    ,PRIM_BKUP_CD    
    --,CASE COALESCE(BDWD_CHNL_NME, 0) WHEN 0 THEN ' ' ELSE BDWD_CHNL_NME END AS BDWD_CHNL_NME    
    ,BDWD_CHNL_NME    
    ,FMS_CKT_ID    
    ,PL_NBR    
    ,CASE COALESCE(IP_NUA_ADR, '') WHEN '' THEN '0' ELSE IP_NUA_ADR END AS IP_NUA_ADR    
    ,OLD_CKT_ID    
    ,0    
    ,SPRINT_MNGD_CD    
    ,CASE TELCO_ID WHEN 0 THEN NULL ELSE TELCO_ID END AS TELCO_ID    
    ,FOC_DT    
    ,TAB_SEQ_NBR    
 FROM @MDS_EVENT_WIRED_TRPT        
 END    
      
 -- DSL Transport: MDS_EVENT_DSL_TRPT    
 IF (LEN(@DSLTransport) > 10)    
 BEGIN    
     
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @DSLTransport        
      
  INSERT INTO  @MDS_EVENT_DSL_TRPT     
  SELECT FSA_MDS_EVENT_ID    
   ,PRIM_BKUP_CD    
   ,IP_ADDR    
   ,SUBNET_MASK    
   ,NHOP_GWY    
   ,PrvdrCKTID
   ,SPRINT_CUST_CD    
   ,SPRINT_MNGD_CD    
   ,TELCO_ID    
   ,TAB_SEQ_NBR     
  FROM OPENXML(@hDoc, '/DocumentElement/DSLTransport', 2)    
  WITH(FSA_MDS_EVENT_ID int     
   ,PRIM_BKUP_CD char(1)     
   ,IP_ADDR varchar(100)     
   ,SUBNET_MASK varchar(100)     
   ,NHOP_GWY varchar(100)     
   ,PrvdrCKTID varchar(100)     
   ,SPRINT_CUST_CD char(1)     
   ,SPRINT_MNGD_CD char(1)    
   ,TELCO_ID tinyint     
   ,TAB_SEQ_NBR TINYINT    
   )     
  EXEC sp_xml_removedocument @hDoc    
      
  UPDATE medt                                                
      SET medt.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_DSL_TRPT medt         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = medt.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID     
       
      
  INSERT INTO dbo.MDS_EVENT_DSL_TRPT    
   (FSA_MDS_EVENT_ID        
   ,PRIM_BKUP_CD    
   ,IP_ADR    
   ,SUBNET_MASK_ADR    
   ,NXTHOP_GTWY_ADR    
   ,PRVDR_CKT_ID    
   ,SPRINT_CUST_CD    
   ,SPRINT_MNGD_CD    
   ,TELCO_ID      
   ,TAB_SEQ_NBR    
   )     
  SELECT     
   FSA_MDS_EVENT_ID       
   ,PRIM_BKUP_CD    
   ,IP_ADDR      
   ,SUBNET_MASK    
   ,NHOP_GWY    
   ,PrvdrCKTID    
   ,SPRINT_CUST_CD    
   ,SPRINT_MNGD_CD    
   ,CASE TELCO_ID WHEN 0 THEN NULL ELSE TELCO_ID END AS TELCO_ID    
   ,TAB_SEQ_NBR     
  FROM  @MDS_EVENT_DSL_TRPT           
 END    

 -- Transport: MDS_EVENT_SPAE_TRPT    
 IF (LEN(@SpAETransport) > 10)    
 BEGIN    
     
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @SpAETransport        
      
  INSERT INTO  @MDS_EVENT_SPAE_TRPT     
  SELECT FSA_MDS_EVENT_ID    
   ,MDS_TRNSPRT_TYPE_ID AS MDS_TRPT_TYPE_ID    
   ,PRIM_BKUP_CD    
   ,BDWL_CHNL_NME AS BDWD_CHNL_NME    
   --,FMS_CKT_ID    
   ,ECCKT_ID    
   ,UNI_NUA_ADDR AS UNI_NUA_ADR    
   ,OLD_CKT_ID    
   ,SPRINT_MNGD_CD    
   ,TELCO_ID    
   ,FOC_DT    
   ,TAB_SEQ_NBR     
  FROM OPENXML(@hDoc, '/DocumentElement/SpAETransport', 2)    
  WITH(FSA_MDS_EVENT_ID int     
   ,MDS_TRNSPRT_TYPE_ID tinyint     
   ,PRIM_BKUP_CD char(1)     
   ,BDWL_CHNL_NME varchar(100)     
   --,FMS_CKT_ID varchar(100)     
   ,ECCKT_ID varchar(20)     
   ,UNI_NUA_ADDR varchar(25)     
   ,OLD_CKT_ID varchar(30)     
   ,SPRINT_MNGD_CD char(1)    
   ,TELCO_ID tinyint     
   ,FOC_DT datetime    
   ,TAB_SEQ_NBR TINYINT    
   )     
  EXEC sp_xml_removedocument @hDoc    
      
  UPDATE mest                                                
      SET mest.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_SPAE_TRPT mest         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = mest.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID     
       
      
  INSERT INTO dbo.MDS_EVENT_SPAE_TRPT    
   (FSA_MDS_EVENT_ID    
   ,MDS_TRPT_TYPE_ID    
   ,PRIM_BKUP_CD    
   ,BDWD_CHNL_NME    
   --,FMS_CKT_ID    
   ,ECCKT_ID    
   ,UNI_NUA_ADR    
   ,OLD_CKT_ID    
   ,SPRINT_MNGD_CD    
   ,TELCO_ID    
   ,FOC_DT    
   ,TAB_SEQ_NBR    
   )     
  SELECT     
   FSA_MDS_EVENT_ID    
   ,NULL    
   ,PRIM_BKUP_CD    
   --,CASE COALESCE(BDWD_CHNL_NME, 0) WHEN 0 THEN ' ' ELSE BDWD_CHNL_NME END AS BDWD_CHNL_NME  
   ,BDWD_CHNL_NME      
   --,FMS_CKT_ID    
   ,ECCKT_ID    
   ,UNI_NUA_ADR    
   ,OLD_CKT_ID    
   ,SPRINT_MNGD_CD    
   ,CASE TELCO_ID WHEN 0 THEN NULL ELSE TELCO_ID END AS TELCO_ID    
   ,FOC_DT    
   ,TAB_SEQ_NBR     
  FROM  @MDS_EVENT_SPAE_TRPT           
 END  
     
 -- Transport: MDS_EVENT_SPRINT_LINK_ATM_TRPT    
 IF (LEN(@SpRFTransport) > 10)    
 BEGIN    
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @SpRFTransport        
      
  INSERT INTO @MDS_EVENT_SPRINT_LINK_ATM_TRPT     
  SELECT FSA_MDS_EVENT_ID    
   ,MDS_TRNSPRT_TYPE_ID AS MDS_TRPT_TYPE_ID    
   ,PRIM_BKUP_CD    
   ,BDWL_CHNL_NME AS BDWD_CHNL_NME    
   ,FMS_CKT_ID    
   ,PL_NBR    
   ,NUA_449_ADR AS IP_NUA_ADR    
   ,OLD_CKT_ID    
   --,MULTI_LINK_CKT_CD    
   ,SPRINT_MNGD_CD    
   --,TELCO_ID    
   ,FOC_DT    
   ,TAB_SEQ_NBR TINYINT    
  FROM OPENXML(@hDoc, '/DocumentElement/SpRFTransport', 2)    
  WITH(FSA_MDS_EVENT_ID int     
   ,MDS_TRNSPRT_TYPE_ID tinyint     
   ,PRIM_BKUP_CD char(1)     
   ,BDWL_CHNL_NME varchar(100)     
   ,FMS_CKT_ID varchar(100)     
   ,PL_NBR varchar(20)     
   ,NUA_449_ADR varchar(25)     
   ,OLD_CKT_ID varchar(30)     
   --,MULTI_LINK_CKT_CD char(1)     
   ,SPRINT_MNGD_CD char(1)     
   --,TELCO_ID tinyint     
   ,FOC_DT datetime    
   ,TAB_SEQ_NBR TINYINT    
   )    
      
  EXEC sp_xml_removedocument @hDoc       
      
  UPDATE meslat                                                
      SET meslat.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_SPRINT_LINK_ATM_TRPT meslat         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = meslat.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID     
     
 INSERT INTO dbo.MDS_EVENT_SPRINT_LINK_ATM_TRPT    
   (FSA_MDS_EVENT_ID    
     ,MDS_TRPT_TYPE_ID    
     ,PRIM_BKUP_CD    
     ,BDWD_CHNL_NME    
     ,FMS_CKT_ID    
     ,PL_NBR    
     ,IP_NUA_ADR    
     ,OLD_CKT_ID    
     ,MULTI_LINK_CKT_CD    
     ,SPRINT_MNGD_CD    
     --,TELCO_ID    
     ,FOC_DT    
     ,TAB_SEQ_NBR    
   )    
 SELECT FSA_MDS_EVENT_ID    
   ,MDS_TRPT_TYPE_ID    
   ,PRIM_BKUP_CD    
   --,CASE COALESCE(BDWD_CHNL_NME, 0) WHEN 0 THEN ' ' ELSE BDWD_CHNL_NME END AS BDWD_CHNL_NME    
   ,BDWD_CHNL_NME    
   ,FMS_CKT_ID    
   ,PL_NBR    
   ,IP_NUA_ADR    
   ,OLD_CKT_ID    
   ,0    
   ,''    
   --,CASE TELCO_ID WHEN 0 THEN NULL ELSE TELCO_ID END AS TELCO_ID    
   ,FOC_DT    
   ,TAB_SEQ_NBR TINYINT    
 FROM @MDS_EVENT_SPRINT_LINK_ATM_TRPT    
         
 END    
     
 --Virtual LAN    
 IF (LEN(@VLAN) > 10)    
 BEGIN    
 EXEC sp_xml_preparedocument @hDoc OUTPUT, @VLAN    
         
 INSERT INTO @VLAN_Temp     
 SELECT FSA_MDS_EVENT_ID    
   ,VLAN AS VLAN_NBR     
   ,SPA_NUA AS SPA_NUA_ADR    
   ,TAB_SEQ_NBR    
 FROM OPENXML(@hDoc, '/DocumentElement/VLAN', 2)    
 WITH (FSA_MDS_EVENT_ID int    
   ,VLAN varchar(10)    
   ,SPA_NUA varchar(20)    
   ,TAB_SEQ_NBR TINYINT    
   )    
      
  EXEC sp_xml_removedocument @hDoc    
      
  UPDATE vlt                                                
      SET vlt.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @VLAN_Temp vlt         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = vlt.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID     
       
 INSERT INTO dbo.MDS_EVENT_VLAN     
  (FSA_MDS_EVENT_ID    
   ,VLAN_NBR     
   ,SPA_NUA_ADR    
   ,TAB_SEQ_NBR)    
 SELECT FSA_MDS_EVENT_ID    
   ,VLAN_NBR     
   ,SPA_NUA_ADR    
   ,TAB_SEQ_NBR    
 FROM @VLAN_Temp    
       
 END    
     
     
 -- Transport: Wireless    
 IF (LEN(@Wireless) > 10)    
 BEGIN    
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @Wireless        
      
  INSERT INTO @MDS_EVENT_WRLS_TRPT     
  SELECT FSA_MDS_EVENT_ID    
   ,PRIM_BKUP_CD    
   ,ESN AS ESN_MAC_ID     
   ,TAB_SEQ_NBR     
  FROM OPENXML(@hDoc, '/DocumentElement/WirelessTransport', 2)    
  WITH  (FSA_MDS_EVENT_ID int     
   ,PRIM_BKUP_CD char(1)     
   ,ESN varchar(30)    
   ,TAB_SEQ_NBR TINYINT    
   )    
       
  EXEC sp_xml_removedocument @hDoc    
      
  UPDATE mewt              
      SET mewt.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_WRLS_TRPT mewt         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = mewt.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID     
           
   INSERT INTO dbo.MDS_EVENT_WRLS_TRPT     
    (FSA_MDS_EVENT_ID    
     ,PRIM_BKUP_CD    
     ,ESN_MAC_ID     
     ,TAB_SEQ_NBR     
    )    
   SELECT FSA_MDS_EVENT_ID    
   ,PRIM_BKUP_CD    
   ,CASE COALESCE(ESN_MAC_ID, '' ) WHEN '' THEN '' ELSE ESN_MAC_ID END AS ESN_MAC_ID    
   ,TAB_SEQ_NBR     
   FROM @MDS_EVENT_WRLS_TRPT    
       
 END    
     
 -- Virtual Connections    
 IF (LEN(@PVC) > 10)    
 BEGIN    
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @PVC        
      
  INSERT INTO  @MDS_EVENT_VRTL_CNCTN    
  SELECT FSA_MDS_EVENT_ID       ,FMS_NME    
   ,DLCI_VPI_CD    
   ,TAB_SEQ_NBR    
  FROM OPENXML(@hDoc, '/DocumentElement/VirtualConnections', 2)    
  WITH  (FSA_MDS_EVENT_ID int     
   ,FMS_NME varchar(200)     
   ,DLCI_VPI_CD varchar(200)    
   ,TAB_SEQ_NBR TINYINT    
      )    
      
  EXEC sp_xml_removedocument @hDoc       
      
   UPDATE mevc                                                
      SET mevc.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_VRTL_CNCTN mevc         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = mevc.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID       
       
  INSERT INTO dbo.MDS_EVENT_VRTL_CNCTN    
     (FSA_MDS_EVENT_ID    
      ,FMS_NME    
      ,DLCI_VPI_CD    
      ,TAB_SEQ_NBR    
     )    
  SELECT FSA_MDS_EVENT_ID    
    ,FMS_NME    
    ,DLCI_VPI_CD    
    ,TAB_SEQ_NBR    
  FROM @MDS_EVENT_VRTL_CNCTN    
 END    
    
-- Service Table
 IF (LEN(@SrvcTbl) > 10)    
 BEGIN    
     
  EXEC sp_xml_preparedocument @hDoc OUTPUT, @SrvcTbl        
      
  INSERT INTO  @MDS_EVENT_SRVC     
  SELECT FSA_MDS_EVENT_ID
   ,MACH5_H6_SRVC_ID
   ,SRVC_TYPE_ID
   ,THRD_PARTY_VNDR_ID
   ,ThirdParty
   ,THRD_PARTY_SRVC_LVL_ID
   ,ACTV_DT
   ,CMNT_TXT
   ,EMAIL_CD
   ,TAB_SEQ_NBR    
  FROM OPENXML(@hDoc, '/DocumentElement/ServiceTbl', 2)    
  WITH(FSA_MDS_EVENT_ID int     
   ,MACH5_H6_SRVC_ID int       
   ,SRVC_TYPE_ID tinyint
   ,THRD_PARTY_VNDR_ID tinyint
   ,ThirdParty varchar(20)
   ,THRD_PARTY_SRVC_LVL_ID tinyint
   ,ACTV_DT smalldatetime    
   ,CMNT_TXT varchar(2000)
   ,EMAIL_CD BIT
   ,TAB_SEQ_NBR TINYINT    
   )     
  EXEC sp_xml_removedocument @hDoc    
      
  UPDATE mes                                                
      SET mes.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID         
   FROM @MDS_EVENT_SRVC mes         
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON  fme.TAB_SEQ_NBR = mes.TAB_SEQ_NBR         
   WHERE fme.EVENT_ID = @EventID 
   
  UPDATE mes
     SET mes.EMAIL_CD = CASE WHEN ((ost.ACTV_DT IS NULL) AND (mes.ACTV_DT IS NOT NULL)) THEN 0 ELSE ost.EMAIL_CD END
  FROM @MDS_EVENT_SRVC mes         
     INNER JOIN @OldSrvcTbl ost ON mes.TAB_SEQ_NBR = ost.TAB_SEQ_NBR 
								AND mes.MACH5_H6_SRVC_ID = ost.MACH5_H6_SRVC_ID AND mes.SRVC_TYPE_ID = ost.SRVC_TYPE_ID
   WHERE ost.EVENT_ID = @EventID
       
      
  INSERT INTO dbo.MDS_EVENT_SRVC    
   (FSA_MDS_EVENT_ID   
   ,MACH5_H6_SRVC_ID 
   ,SRVC_TYPE_ID
   ,THRD_PARTY_VNDR_ID
   ,THRD_PARTY_ID    
   ,THRD_PARTY_SRVC_LVL_ID
   ,ACTV_DT
   ,CMNT_TXT
   ,EMAIL_CD
   ,TAB_SEQ_NBR   
   )     
  SELECT     
   FSA_MDS_EVENT_ID   
   ,MACH5_H6_SRVC_ID 
   ,CASE SRVC_TYPE_ID WHEN 0 THEN NULL ELSE SRVC_TYPE_ID END AS SRVC_TYPE_ID
   ,CASE THRD_PARTY_VNDR_ID WHEN 0 THEN NULL ELSE THRD_PARTY_VNDR_ID END AS THRD_PARTY_VNDR_ID
   ,ThirdParty
   ,CASE THRD_PARTY_SRVC_LVL_ID WHEN 0 THEN NULL ELSE THRD_PARTY_SRVC_LVL_ID END AS THRD_PARTY_SRVC_LVL_ID
   ,CASE WHEN (ACTV_DT = '1900-01-01 00:00:00') THEN NULL ELSE ACTV_DT END AS ACTV_DT
   ,CMNT_TXT
   ,EMAIL_CD
   ,TAB_SEQ_NBR
  FROM  @MDS_EVENT_SRVC           
 END      
          
  --Updating FSA_MDS_EVENT_ORDR        
  INSERT INTO FSA_MDS_EVENT_ORDR (ORDR_ID, EVENT_ID, TAB_SEQ_NBR, CMPLTD_CD, CREAT_DT)        
   (SELECT DISTINCT ORDR_ID, EVENT_ID, TAB_SEQ_NBR, CMPLTD_CD, getdate()        
    FROM        
    (SELECT DISTINCT oe.ORDR_ID AS ORDR_ID,         
         @EventID AS EVENT_ID,         
         fe.TAB_SEQ_NBR AS TAB_SEQ_NBR,         
         fe.CMPLTD_CD AS CMPLTD_CD        
     FROM dbo.FSA_MDS_EVENT_NEW fe WITH (NOLOCK)        
      INNER JOIN dbo.MDS_EVENT_ACCS oe WITH (NOLOCK) ON fe.FSA_MDS_EVENT_ID = oe.FSA_MDS_EVENT_ID        
      WHERE fe.EVENT_ID = @EventID        
         AND oe.ORDR_ID IS NOT NULL 
         AND oe.ORDR_ID NOT IN (0)        
     UNION         
     SELECT DISTINCT md.ORDR_ID AS ORDR_ID,         
         @EventID AS EVENT_ID,         
         fe.TAB_SEQ_NBR AS TAB_SEQ_NBR,         
         fe.CMPLTD_CD  AS CMPLTD_CD        
     FROM dbo.FSA_MDS_EVENT_NEW fe WITH (NOLOCK)        
      INNER JOIN dbo.MDS_EVENT_CPE md WITH (NOLOCK) ON fe.FSA_MDS_EVENT_ID = md.FSA_MDS_EVENT_ID        
      WHERE fe.EVENT_ID = @EventID        
        AND md.ORDR_ID IS NOT NULL
        AND md.ORDR_ID NOT IN (0)    
    UNION         
     SELECT DISTINCT mn.ORDR_ID AS ORDR_ID,         
         @EventID AS EVENT_ID,         
         fe.TAB_SEQ_NBR AS TAB_SEQ_NBR,         
         fe.CMPLTD_CD  AS CMPLTD_CD        
     FROM dbo.FSA_MDS_EVENT_NEW fe WITH (NOLOCK)        
      INNER JOIN dbo.MDS_EVENT_MNS mn WITH (NOLOCK) ON fe.FSA_MDS_EVENT_ID = mn.FSA_MDS_EVENT_ID        
      WHERE fe.EVENT_ID = @EventID        
        AND mn.ORDR_ID IS NOT NULL 
        AND mn.ORDR_ID NOT IN (0)) a_tbl        
    )        
	
	DECLARE @DevCmpltDt DATETIME = GETDATE()
	--Complete Redesign Tables and also send Mach5/ODIE messages
	/*
	
	Added by Monir Vn370313  from Checking if 
	Scenerio  1     :     Mac Activity  chosen    anything other than   10/4     Redesign Related Update Happens  (YES)
	Scenerio 2      :     Mac Activity  chosen    only    10/4     Redesign Related Update Happens   (NO)
	Scenerio 3      :     Mac Activity  chosen    any other  ID   ALONG WITH    10/4     (example  9 and 10   or  11 and 4)    Redesign Related Update Happens   (YES)
	*** 10(other) and 4 (SPS)
	*/
	SET @DoRedesgn = 0
	IF EXISTS (Select * from dbo.MDS_EVENT_MAC_ACTY WITH (NOLOCK) where Event_ID=@EventID )
	BEGIN
		IF EXISTS (Select * from dbo.MDS_EVENT_MAC_ACTY WITH (NOLOCK) where Event_ID=@EventID and MDS_MAC_ACTY_ID NOT IN (10,4) )
		BEGIN
		     SET @DoRedesgn = 1
		END
		ELSE
		BEGIN
			SET @DoRedesgn = 0
		END
	END
	ELSE
	BEGIN
		SET @DoRedesgn = 1
	END

	IF (@DoRedesgn >0 )
	BEGIN
			UPDATE rdi
			SET DEV_BILL_DT = @DevCmpltDt,
				DEV_CMPLTN_CD = 1,
				H6_CUST_ID = SUBSTRING(fm.TAB_NME,1,9)
			from dbo.FSA_MDS_EVENT_NEW fm with (nolock) inner join
			dbo.MDS_EVENT_ODIE_DEV_NME modn with (nolock) on modn.FSA_MDS_EVENT_ID = fm.FSA_MDS_EVENT_ID inner join
			dbo.MDS_MNGD_ACT_NEW mma with (nolock) on mma.ODIE_DEV_NME = modn.ODIE_DEV_NME and fm.event_id = mma.event_id inner join
			dbo.REDSGN_DEVICES_INFO rdi on mma.ODIE_DEV_NME = rdi.DEV_NME inner join
			dbo.REDSGN rd with (nolock) on rd.REDSGN_NBR=mma.RDSN_NBR and rd.redsgn_id=rdi.redsgn_id
			where fm.event_id=@EventID
			  AND fm.CMPLTD_CD = 1
			  AND rdi.REC_STUS_ID = 1
	  
			-- Insert into REDSGN_DEV_EVENT_HIST
			INSERT INTO dbo.REDSGN_DEV_EVENT_HIST (EVENT_ID, CMPLTD_DT, REDSGN_DEV_ID)
			SELECT DISTINCT @EventID, @DevCmpltDt, rdi.REDSGN_DEV_ID
			from dbo.FSA_MDS_EVENT_NEW fm with (nolock) inner join
			dbo.MDS_EVENT_ODIE_DEV_NME modn with (nolock) on modn.FSA_MDS_EVENT_ID = fm.FSA_MDS_EVENT_ID inner join
			dbo.MDS_MNGD_ACT_NEW mma with (nolock) on mma.ODIE_DEV_NME = modn.ODIE_DEV_NME and fm.event_id = mma.event_id inner join
			dbo.REDSGN_DEVICES_INFO rdi with (nolock) on mma.ODIE_DEV_NME = rdi.DEV_NME inner join
			dbo.REDSGN rd with (nolock) on rd.REDSGN_NBR=mma.RDSN_NBR and rd.redsgn_id=rdi.redsgn_id
			where fm.event_id=@EventID
			  AND fm.CMPLTD_CD = 1
			  AND rdi.REC_STUS_ID = 1
	
	  
			--Update the NTE_CHRG_CD on the first redesign device
			UPDATE dbo.REDSGN_DEVICES_INFO
			SET NTE_CHRG_CD = 1
			WHERE REDSGN_DEV_ID = (select TOP 1 rdi.REDSGN_DEV_ID
			from dbo.FSA_MDS_EVENT_NEW fm with (nolock) inner join
				dbo.MDS_EVENT_ODIE_DEV_NME modn with (nolock) on modn.FSA_MDS_EVENT_ID = fm.FSA_MDS_EVENT_ID inner join
				dbo.MDS_MNGD_ACT_NEW mma with (nolock) on mma.ODIE_DEV_NME = modn.ODIE_DEV_NME and fm.event_id = mma.event_id inner join
				dbo.REDSGN_DEVICES_INFO rdi with (nolock) on mma.ODIE_DEV_NME = rdi.DEV_NME inner join
				dbo.REDSGN rd with (nolock) on rd.REDSGN_NBR=mma.RDSN_NBR and rd.redsgn_id=rdi.redsgn_id
				where fm.event_id=@EventID
				  AND fm.CMPLTD_CD = 1
				  AND rdi.REC_STUS_ID = 1
				  AND NOT EXISTS (SELECT 'X'
								  FROM dbo.REDSGN_DEVICES_INFO rdi2 WITH (NOLOCK)
								  WHERE (rdi.redsgn_id = rdi2.redsgn_id) AND (rdi2.NTE_CHRG_CD = 1)))
	
			--Check all associated redesign devices and complete status on redesign
			EXEC [web].[updateRdsnCmpltFromEvent] @EventID
	END
        
    EXEC [dbo].[completeMDSEvent] @EventID        
         
  END -- end IF (LEN(@EventData) > 10)        
  COMMIT TRAN insertNewMDSEventData        
 END TRY        
 BEGIN CATCH        
  ROLLBACK TRAN insertNewMDSEventData        
  EXEC [dbo].[insertErrorInfo]          
  DECLARE @ErrMsg nVarchar(4000),           
       @ErrSeverity Int                
  SELECT @ErrMsg  = ERROR_MESSAGE(),                
      @ErrSeverity = ERROR_SEVERITY()                
  RAISERROR(@ErrMsg, @ErrSeverity, 1)          
 END CATCH        
        
END