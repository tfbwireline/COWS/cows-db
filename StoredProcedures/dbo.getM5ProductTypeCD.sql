USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getM5ProductTypeCD]    Script Date: 1/18/2022 2:39:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[getM5ProductTypeCD]
	@ORDR_TYPE_ID Varchar(5)  ,
	@M5_ORDR_ID Int ,
	@M5_PROD_ID Varchar(3) ,
	@ORDR_SUB_TYPE_CD CHAR(2) ,
	@PROD_TYPE_CD Varchar(2) OUTPUT
	
AS
BEGIN
BEGIN TRY

	--DECLARE @ORDR_TYPE_ID Varchar(2) = 'IN'
	--DECLARE @M5_ORDR_ID Int = 2056
	--DECLARE @M5_PROD_ID Varchar(3) = 'IPL'
	--DECLARE @PROD_TYPE_CD Varchar(3) 
	DECLARE @TBL_INDICATOR TABLE (NET_INDICATOR Varchar(3),PROD_CD Varchar(5)) 


	DECLARE @sql nvarchar(max)
	SET @sql = ''

	IF @ORDR_TYPE_ID = 'IN' --IN ('IN','CN') 8/15/17 changed
		BEGIN
			SET @sql = 'SELECT INTL_NW_TYPE, PROD_CD FROM  OPENQUERY ' + 
				  '(M5,' + 
					 '''SELECT  c.INTL_NW_TYPE, c.PROD_CD
								FROM	MACH5.V_V5U_ORDR b
									JOIN MACH5.V_V5U_ORDR_CMPNT c ON b.ORDR_ID = c.ORDR_ID
								WHERE	b.ORDR_ID =' +  CONVERT(VARCHAR,@M5_ORDR_ID) +     
									' AND c.CMPNT_TYPE_CD IN (''''ACCS'''',''''ACST'''',''''PORT'''') '')'
			--Select @sql
			INSERT INTO @TBL_INDICATOR
				Exec sp_executesql @SQL								

		END
	ELSE IF @ORDR_TYPE_ID IN ('CH','DC')
		BEGIN
			SET @sql = 'SELECT INTL_NW_TYPE, PROD_CD FROM  OPENQUERY ' + 
				  '(M5,' + 
					 '''SELECT c.INTL_NW_TYPE, c.PROD_CD
								FROM	MACH5.V_V5U_ORDR b
									JOIN	MACH5.V_V5U_ORDR_DISC_CMPNT d ON b.ORDR_ID = d.ORDR_ID
									JOIN	MACH5.V_V5U_NVTRY c ON d.NVTRY_CMPNT_ID = c.NVTRY_ID
								WHERE	b.ORDR_ID =' +  CONVERT(VARCHAR,@M5_ORDR_ID) +     
									' AND c.CMPNT_TYPE_CD IN ( ''''ACCS'''',''''ACST'''',''''PORT'''') '')'
			--Select @sql
			INSERT INTO @TBL_INDICATOR
				Exec sp_executesql @SQL							
		END
	ELSE IF @ORDR_TYPE_ID IN ('ACRF','ACRN','RO')-- modified 6/18/2020 for ACRF/ACRN transactions.  Was looking at order views. dlp0278
		BEGIN
			SET @sql = 'SELECT  INTL_NW_TYPE, PROD_CD FROM  OPENQUERY ' + 
						'(M5,' + 
						'''SELECT c.INTL_NW_TYPE, c.PROD_CD
								FROM	mach5.V_V5U_CHNG_TXN b
								    JOIN mach5.V_V5U_CHNG_TXN b2 ON b2.CHNG_TXN_ID=b.CHNG_TXN_ID
									LEFT JOIN	MACH5.V_V5U_NVTRY c ON b.CUST_ACCT_ID = c.CUST_ACCT_ID AND c.CMPNT_TYPE_CD IN ( ''''ACCS'''',''''ACST'''',''''PORT'''')
							
								WHERE	b.CHNG_TXN_ID = ' +  CONVERT(VARCHAR,@M5_ORDR_ID) +     
									'  '')'
			--Select @sql
			INSERT INTO @TBL_INDICATOR
				Exec sp_executesql @SQL							
		END
	ELSE IF @ORDR_TYPE_ID in ('CN','CNI','CND') -- added 8/15/17 dlp0278  Modified to handle outside moves Dec 13 Rel 2019
		BEGIN
			SET @sql = 'SELECT  INTL_NW_TYPE, PROD_CD  FROM  OPENQUERY 
				  (M5,''SELECT DISTINCT n.INTL_NW_TYPE, n.PROD_CD
								FROM	MACH5.V_V5U_ORDR b
									JOIN	MACH5.V_V5U_ORDR_CNCL_CMPNT c ON b.ORDR_ID = c.ORDR_ID
									JOIN	MACH5.V_V5U_ORDR_CMPNT oc ON c.ORDR_CMPNT_ID = oc.ORDR_CMPNT_ID
									JOIN	MACH5.V_V5U_NVTRY n ON n.NVTRY_ID = oc.NVTRY_ID
								WHERE	b.ORDR_ID =' +  CONVERT(VARCHAR,@M5_ORDR_ID) +     
									' AND n.CMPNT_TYPE_CD IN ( ''''ACCS'''',''''ACST'''',''''PORT'''') 
									
						UNION 	
						
						SELECT DISTINCT n.INTL_NW_TYPE, n.PROD_CD
								FROM	MACH5.V_V5U_ORDR b
									JOIN	MACH5.V_V5U_ORDR_CNCL_CMPNT c ON b.ORDR_ID = c.ORDR_ID
									JOIN	Mach5.V_V5U_ORDR_DISC_CMPNT oc ON c.ORDR_CMPNT_ID = oc.ORDR_DISC_CMPNT_ID
									JOIN	MACH5.V_V5U_NVTRY n ON n.NVTRY_ID = oc.NVTRY_CMPNT_ID
								WHERE	b.ORDR_ID =' +  CONVERT(VARCHAR,@M5_ORDR_ID) +     
									' AND n.CMPNT_TYPE_CD IN ( ''''ACCS'''',''''ACST'''',''''PORT'''') 			
									'')'
			--Select @sql
			INSERT INTO @TBL_INDICATOR
				Exec sp_executesql @SQL		
		END
	DECLARE @I_ID Varchar(3)
	SELECT top 1 @I_ID = NET_INDICATOR, @M5_PROD_ID = SUBSTRING(PROD_CD,1,3) FROM @TBL_INDICATOR WHERE ISNULL(PROD_CD,'') <> ''
	--Select @I_ID
	SELECT @PROD_TYPE_CD = 
					CASE @M5_PROD_ID
						WHEN 'DIA' THEN
							CASE @I_ID
								WHEN 'OFN' THEN 'DO'
								WHEN 'ONN' THEN 'DN'
								ELSE (CASE WHEN @ORDR_SUB_TYPE_CD IN ('ACRF','ACRN','RO') THEN 'DO' ELSE 'DN' END)
							END
						WHEN 'SLF' THEN
							CASE @I_ID 
								WHEN 'OFN' THEN 'SO'
								WHEN 'ONN' THEN 'SN'
								ELSE (CASE WHEN @ORDR_SUB_TYPE_CD IN ('ACRF','ACRN','RO') THEN 'SO' ELSE 'SN' END)
							END
						WHEN 'CEV' THEN
							CASE @I_ID 
								WHEN 'OFN' THEN 'MO'
								WHEN 'ONN' THEN 'MP'
								ELSE (CASE WHEN @ORDR_SUB_TYPE_CD IN ('ACRF','ACRN','RO') THEN 'MO' ELSE 'MP' END)
							END
						WHEN 'CEP' THEN
							CASE @I_ID 
								WHEN 'OFN' THEN 'MO'
								WHEN 'ONN' THEN 'MP'
								ELSE (CASE WHEN @ORDR_SUB_TYPE_CD IN ('ACRF','ACRN','RO') THEN 'MO' ELSE 'MP' END)
							END
						WHEN 'CEV' THEN
							CASE @I_ID 
								WHEN 'OFN' THEN 'MO'
								WHEN 'ONN' THEN 'MP'
								ELSE (CASE WHEN @ORDR_SUB_TYPE_CD IN ('ACRF','ACRN','RO') THEN 'MO' ELSE 'MP' END)
							END
						WHEN 'CEN' THEN
							CASE @I_ID 
								WHEN 'OFN' THEN 'MO'
								WHEN 'ONN' THEN 'MP'
								ELSE (CASE WHEN @ORDR_SUB_TYPE_CD IN ('ACRF','ACRN','RO') THEN 'MO' ELSE 'MP' END)
							END
						WHEN 'MPL' THEN
							CASE @I_ID 
								WHEN 'OFN' THEN 'MO'
								WHEN 'ONN' THEN 'MP'
								ELSE (CASE WHEN @ORDR_SUB_TYPE_CD IN ('ACRF','ACRN','RO') THEN 'MO' ELSE 'MP' END)
							END
						WHEN 'IPL' THEN
							CASE @I_ID 
								WHEN 'OFN' THEN 'IO'
								WHEN 'ONN' THEN 'IN'
								ELSE (CASE WHEN @ORDR_SUB_TYPE_CD IN ('ACRF','ACRN','RO') THEN 'IO' ELSE 'IN' END)
							END
						WHEN 'SLP' THEN -- Put as temporary for old product to disconnect. 6/6/17 10:30 a.m.
							CASE @I_ID 
								WHEN 'OFN' THEN 'SO'
								WHEN 'ONN' THEN 'SN'
								ELSE (CASE WHEN @ORDR_SUB_TYPE_CD IN ('ACRF','ACRN','RO') THEN 'SO' ELSE 'SN' END)
							END
						END

Select 	@PROD_TYPE_CD
END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH
END