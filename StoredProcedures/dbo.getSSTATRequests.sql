USE [COWS]
GO
_CreateObject 'SP'
	,'dbo'
	,'getSSTATRequests'
GO
/****** Object:  StoredProcedure [dbo].[getSSTATRequests]    Script Date: 10/07/2015 09:17:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		dlp0278
-- Create date: 10/2/2015
-- Description:	Get the details for all STTAT requests
-- kh946640 07/23/18 Added CSG_LVL_ID column.
-- km967761 - 08/23/2021 - Added EMAIL_REQ_ID for CPE Dispatch Email
-- =========================================================
ALTER PROCEDURE [dbo].[getSSTATRequests]
AS
BEGIN
SET NOCOUNT ON;
Begin Try

	SELECT DISTINCT TRAN_ID	
					,sst.ORDR_ID
					,SSTAT_MSG_ID
					,sst.STUS_ID
					,FTN
					,COALESCE(ord.CSG_LVL_ID, evt.CSG_LVL_ID, 0) AS [CSG_LVL_ID],
					sst.EMAIL_REQ_ID
		FROM		dbo.SSTAT_REQ sst	WITH (NOLOCK)
			LEFT JOIN   dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = sst.ORDR_ID
			LEFT JOIN dbo.EMAIL_REQ er WITH (NOLOCK) ON er.EMAIL_REQ_ID = sst.EMAIL_REQ_ID
			LEFT JOIN dbo.EVENT evt WITH (NOLOCK) ON evt.EVENT_ID = er.EVENT_ID
		WHERE		sst.STUS_ID		=	10
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

