USE [COWS]
GO

/****** Object:  StoredProcedure [dbo].[updatePS_REQ_HDR]    Script Date: 10/01/2015 11:07:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 08/11/2015
-- Description:	Gets last File Sequence for PeopleSoft Batch process.
-- =========================================================
CREATE PROCEDURE [dbo].[updatePS_REQ_HDR]
	@ORDR_ID  int,
	@REQSTN_NBR varchar (10),
	@BATCH_SEQ int
        
AS
BEGIN TRY
				
	UPDATE dbo.PS_REQ_HDR_QUEUE
		SET SENT_DT = GETDATE(),
			BATCH_SEQ = @BATCH_SEQ 
	WHERE ORDR_ID = @ORDR_ID
	    AND REQSTN_NBR = @REQSTN_NBR
	
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH


GO


