USE [COWS]
GO
_CreateObject 'SP','dbo','GetFSADisconnectInformation'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE [dbo].[GetFSADisconnectInformation]
	@OrderID Int	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT 
		RoleName = ISNULL(lr.ROLE_NME,''),
        ContactType = ISNULL(lc.CNTCT_TYPE_DES,''),
		FrstNme = CASE WHEN (csdoc.FRST_NME IS NULL) THEN ISNULL(oc.FRST_NME,'') ELSE ISNULL(CONVERT(VARCHAR, DecryptByKey(csdoc.FRST_NME)),'') END,
		LstNme = CASE WHEN (csdoc.LST_NME IS NULL) THEN ISNULL(oc.LST_NME,'') ELSE ISNULL(CONVERT(VARCHAR, DecryptByKey(csdoc.LST_NME)),'') END,
		Nme = CASE WHEN (csdoc.CUST_CNTCT_NME IS NULL) THEN ISNULL(oc.CNTCT_NME,'') ELSE ISNULL(CONVERT(VARCHAR, DecryptByKey(csdoc.CUST_CNTCT_NME)),'') END,
		EmailAdr = CASE WHEN (csdoc.CUST_EMAIL_ADR IS NULL) THEN ISNULL(oc.EMAIL_ADR,'') ELSE ISNULL(CONVERT(VARCHAR, DecryptByKey(csdoc.CUST_EMAIL_ADR)),'') END,
        ISNULL(oc.ISD_CD,'')	AS	IsdCd,
		ISNULL(oc.STN_NBR,'')	AS	StnNbr,
		ISNULL(oc.CTY_CD,'')	AS	CtyCd,
		ISNULL(oc.NPA,'')		AS	Npa,
		ISNULL(oc.NXX,'')		AS	Nxx,
        PhnNbr = ISNULL(oc.PHN_NBR,'')
	FROM	dbo.ORDR_CNTCT		oc WITH (NOLOCK)	
			INNER JOIN dbo.ORDR odr WITH (NOLOCK) ON odr.ORDR_ID = oc.ORDR_ID
			LEFT JOIN	dbo.LK_ROLE			lr WITH (NOLOCK)		on oc.ROLE_ID = lr.ROLE_ID
			LEFT JOIN	dbo.LK_CNTCT_TYPE	lc WITH (NOLOCK)		on oc.CNTCT_TYPE_ID = lc.CNTCT_TYPE_ID
			LEFT JOIN dbo.CUST_SCRD_DATA csdoc WITH (NOLOCK)  ON csdoc.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID AND csdoc.SCRD_OBJ_TYPE_ID=15
	WHERE	oc.ORDR_ID = @OrderID
        AND	(
				(oc.FSA_MDUL_ID = 'DSC')
				OR 
				(odr.ORDR_CAT_ID = 6 AND oc.ROLE_ID = 99)
			)
END
