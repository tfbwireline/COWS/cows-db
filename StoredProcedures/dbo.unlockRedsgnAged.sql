USE [COWS]
GO
_CreateObject 'SP','dbo','unlockRedsgnAged'
GO
/****** Object:  StoredProcedure [dbo].[getSSTATRequests]    Script Date: 10/10/2016 13:49:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 10/10/2016
-- Description:	Unlocks Redesigns that are Aged.
-- =========================================================

ALTER PROCEDURE [dbo].[unlockRedsgnAged]

AS
BEGIN

SET NOCOUNT ON;
Begin Try

	DELETE dbo.REDSGN_REC_LOCK
		WHERE DATEADD(mi,-15,getdate()) > STRT_REC_LOCK_TMST
		
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END

