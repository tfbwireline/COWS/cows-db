USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[UpdateReportActyLog]    Script Date: 11/6/2013 10:24:08 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='UpdateReportActyLog')
EXEC ('CREATE PROCEDURE [dbo].[UpdateReportActyLog] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

 --=====================================================================================================    
 -- Author: Jagan Gangi
 -- Create Date: 11/6/2012
 -- Description : To Update web session details to maintain a log
 --=====================================================================================================    
    
 ALTER PROCEDURE [dbo].[UpdateReportActyLog]     
  @RptID INT,
  @RptTypeCd CHAR(1),
  @Sessionid  VARCHAR(100),
  @IpAdr VARCHAR(39),
  @UAdid VARCHAR(20)
 AS    
 BEGIN    
  SET NOCOUNT ON    
  BEGIN TRY    
   
   IF ((@UAdid != '') AND (@Sessionid != ''))  
   BEGIN
		   
		   INSERT INTO dbo.RPT_ACTY_LOG (RPT_ID, RPT_TYPE, USER_AD_ID, SESSN_ID, IP_ADR, STRT_TME) VALUES (@RptID, @RptTypeCd, @UAdid, @Sessionid, @IPAdr, GETDATE())
   END
     
  END TRY    
  BEGIN CATCH      
  END CATCH    
    
 END