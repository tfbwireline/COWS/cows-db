USE [COWS]
GO
_CreateObject 'SP','dbo','getConferenceBridgeInfo'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ===============================================================================
-- Author:		sbg9814
-- Create date: 09/29/2011
-- Description:	Gets the Conference brdige info for a given User.
-- ===============================================================================
ALTER PROCEDURE [dbo].[getConferenceBridgeInfo]
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	SELECT			CNFRC_BRDG_NBR
					,CNFRC_PIN_NBR
					,ONLINE_MEETING_ADR
					,CREAT_BY_USER_ID
		FROM		dbo.LK_CNFRC_BRDG	WITH (NOLOCK)
		WHERE		REC_STUS_ID			=	1
		ORDER BY	CNFRC_BRDG_ID	DESC	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
