USE [COWS]
GO
_CreateObject 'SP','dbo','getDeCryptValue'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jagannath Gangi
-- Create date: 06/30/2011
-- Description:	Gets the decrypted value of a encrypted column data
-- Update Date: 05/07/2019
-- Updated By:	Sarah Sandoval
-- Description: For the new COWS Rewrite 
-- =============================================
ALTER PROCEDURE [dbo].[GetDeCryptValue] --0x000F0F9BDF0C5347B3ABEA0F27002A9901000000E35276FEC54ADF5D67335FABB76FC78AF11CADDB2DA54CABFD9E9BF49F6A4B6B
(
	@EncryptData VARBINARY(MAX)
)
AS
BEGIN
BEGIN TRY

			OPEN SYMMETRIC KEY FS@K3y 
			DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
			
			SELECT  CONVERT(varchar(Max), DecryptByKey(@EncryptData)) AS Item
	
END TRY
BEGIN CATCH
EXEC dbo.insertErrorInfo
END CATCH
END