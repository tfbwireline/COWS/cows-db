USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getISBillOnly]    Script Date: 04/25/2019 8:54:16 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By:		David Phillips
-- Create date: 04/25/2019
-- Description:	This SP is used to get IS initiate Bill Only order email info.
-- =============================================
ALTER PROCEDURE [dbo].[getISBillOnlyEmailInfo] 			 
  @ORDR_ID INT 

  AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRY

	
	SELECT DISTINCT	fo.FTN,ord.H5_H6_CUST_ID 
		FROM dbo.FSA_ORDR fo WITH (NOLOCK)
		INNER JOIN dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
		WHERE fo.ORDR_ID = @ORDR_ID


	 SELECT DISTINCT          
			cct.CKT_CHG_TYPE_DES
			,cc.CHG_NRC_AMT
			,cc.TAX_RT_PCT_QTY
			,lc.CUR_NME
			,cc.CHG_NRC_IN_USD_AMT
			,cc.NTE_TXT
			,ls.STUS_DES
			
		FROM  dbo.ORDR_CKT_CHG cc WITH (NOLOCK)
			INNER JOIN dbo.LK_CKT_CHG_TYPE cct WITH (NOLOCK) ON cct.CKT_CHG_TYPE_ID = cc.CKT_CHG_TYPE_ID
			INNER JOIN dbo.LK_CUR lc WITH (NOLOCK) ON lc.CUR_ID = cc.CHG_CUR_ID
			INNER JOIN dbo.LK_STUS ls WITH (NOLOCK) ON ls.STUS_ID = cc.SALS_STUS_ID		
		
		WHERE cc.ORDR_ID = @ORDR_ID	
			AND cc.SALS_STUS_ID = 30 
			AND cc.CHG_NRC_AMT > 0
			AND cc.REQR_BILL_ORDR_CD = 1
			
			
	END TRY
	BEGIN CATCH
		exec [dbo].[insertErrorInfo]
	END CATCH
END

