USE COWS
GO
_CreateObject 'SP','dbo','getMDSGuestWifiEmailData'
GO
-- ==================================================================================
-- Author:		jrg7298
-- Create date: 03/22/2015
-- Description:	Gets the data for the Guest Wifi Emails.
-- Modification date: 04/14/2015
-- Description:	dbo.LK_MDS_3RDPARTYID  Not exist so modified the SP
-- ==================================================================================
ALTER PROCEDURE dbo.getMDSGuestWifiEmailData 
	@EVENT_ID	Int	
AS
BEGIN
SET NOCOUNT ON;
Begin Try

OPEN SYMMETRIC KEY FS@K3y       
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;     

SELECT CASE mes.ACTV_DT WHEN NULL THEN '' ELSE CONVERT(VARCHAR,mes.ACTV_DT,101) END AS ACTV_DT, COALESCE(mes.CMNT_TXT,'') AS CMNT_TXT, mes.THRD_PARTY_ID AS THIRD_PARTY_ID, 
COALESCE(lur.FULL_NME, '') AS REQOR_NME, COALESCE(lur.PHN_NBR, '') AS REQOR_PHN_NBR,
ISNULL(dbo.decryptBinaryData(mn.CUST_EMAIL_ADR),'') AS CUST_EMAIL_ADR,
CASE WHEN (mes.ACTV_DT IS NOT NULL) THEN 'Managed Guest Wi-Fi Successful Activation'
     WHEN ((mes.ACTV_DT IS NULL) AND (COALESCE(mes.CMNT_TXT,'')<> '')) THEN 'Managed Guest Wi-Fi Unsuccessful Activation'
	 END AS SUBJ, mn.EVENT_STUS_ID, CASE mes.EMAIL_CD WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS EMAIL_CD
FROM dbo.MDS_EVENT_NEW mn WITH (NOLOCK) INNER JOIN
dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON mn.EVENT_ID = fme.EVENT_ID INNER JOIN
dbo.MDS_EVENT_SRVC mes WITH (NOLOCK) ON mes.FSA_MDS_EVENT_ID = fme.FSA_MDS_EVENT_ID LEFT JOIN
--dbo.LK_MDS_3RDPARTYID lm3p WITH (NOLOCK) ON mes.THRD_PARTYID_ID = lm3p.THRD_PARTYID_ID LEFT JOIN  /* dbo.LK_MDS_3RDPARTYID  Not exist*/
dbo.LK_USER lur WITH (NOLOCK) ON lur.[USER_ID] = mn.REQOR_USER_ID
WHERE fme.EVENT_ID = @EVENT_ID
AND mes.SRVC_TYPE_ID = 3 --Guest Wi-fi
AND ((mes.ACTV_DT IS NOT NULL) OR (mes.CMNT_TXT IS NOT NULL))

SELECT PRMTR_NME, PRMTR_VALU_TXT
FROM dbo.LK_SYS_CFG WITH (NOLOCK)
WHERE PRMTR_NME IN ('MDSGuestWifiActivationEmail','MDSGuestWifiCommentsEmail')
AND REC_STUS_ID = 1

End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END
