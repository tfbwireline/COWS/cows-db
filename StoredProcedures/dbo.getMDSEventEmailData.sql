USE [COWS]
GO
_CreateObject 'SP','dbo','getMDSEventEmailData'
GO
/****** Object:  StoredProcedure [web].[getMDSEventData]    Script Date: 08/10/2012 17:13:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================      
-- Author:  su302037      
-- Create date: 06/30/2012      
-- Description: Get the required data for an MDS Event when a H5/H6 value is entered.  
-- kh946640 03/22/18 COWS DB Restructuring as columns have been moved from fsa_ordr->fsa_ordr_cpe_line_item table for DualPort project(PJ020783 - CR93)    
-- =============================================      
ALTER PROCEDURE [dbo].[getMDSEventEmailData]  --'0', 6414, '', 5018, 1
 @H5_H6_CUST_ID  Varchar(20) = ''    
 ,@EVENT_ID   Int    
 ,@TAB_NME   Varchar(50)    
 ,@FSA_MDS_EVENT_ID Int    
 ,@Tab_Seq_Nbr TinyInt    
AS      
BEGIN      
SET NOCOUNT ON      
Begin Try      
      
OPEN SYMMETRIC KEY FS@K3y       
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;      
      
DECLARE @Table TABLE      
 (ORDR_ID   Int   NULL      
 ,PROD_TYPE_CD  VARCHAR(2) NULL      
 ,EVENT_ID   Int   NULL      
 ,FSA_MDS_EVENT_ID Int   NULL)      
      
IF @H5_H6_CUST_ID != 0      
 BEGIN      
  INSERT INTO @Table (ORDR_ID,PROD_TYPE_CD,  EVENT_ID, FSA_MDS_EVENT_ID)      
    SELECT                  od.ORDR_ID, fo.PROD_TYPE_CD, 0,                 0      
                        FROM        dbo.ORDR    od    WITH (NOLOCK)      
                                          INNER JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID      
                                          INNER JOIN dbo.ACT_TASK at WITH (NOLOCK) on  od.ORDR_ID = at.ORDR_ID      
                        WHERE       fo.PROD_TYPE_CD != 'CN'      
                                         AND ((at.TASK_ID = 1000 AND at.STUS_ID = 3)      
                                          OR (at.TASK_ID in (300, 301) AND at.STUS_ID = 2))      
                                          AND fo.ordr_actn_id = 2      
                                          AND od.H5_H6_CUST_ID  = @H5_H6_CUST_ID      
                                          AND od.ORDR_STUS_ID = 1      
                                          AND od.ordr_cat_id = 2          
 END      
ELSE      
 BEGIN      
  INSERT INTO @Table (ORDR_ID, PROD_TYPE_CD, EVENT_ID, FSA_MDS_EVENT_ID)      
   SELECT   0, 0, fm.EVENT_ID, FSA_MDS_EVENT_ID      
		FROM  dbo.FSA_MDS_EVENT_NEW  fm WITH (NOLOCK)      
		WHERE  fm.FSA_MDS_EVENT_ID  = @FSA_MDS_EVENT_ID      
 END        
      
IF (SELECT Count(1) FROM @Table) > 0      
 BEGIN       
  IF @H5_H6_CUST_ID != 0      
   BEGIN      	
	
	     
	-----------------------------------------------------------------------      
    -- Table 00 - MNS Info FTN Table.      
    -----------------------------------------------------------------------  
    SELECT DISTINCT @Tab_Seq_Nbr      					 AS TAB_SEQ_NBR,   
					fo.FTN			  					 AS MDS_MNS_FTN,
					ISNULL(ls.MDS_SRVC_TIER_DES, ' ')	 AS MDS_SRVC_TIER_DES,      
					ISNULL(le.MDS_ENTLMNT_DES, ' ')		 AS MDS_ENTLMNT_DES
		FROM  @Table   tt INNER JOIN 
			  dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = tt.ORDR_ID LEFT JOIN 
			  dbo.MDS_EVENT_MNS oe WITH (NOLOCK) ON fo.ORDR_ID = oe.ORDR_ID LEFT JOIN 
			  dbo.LK_MDS_SRVC_TIER ls WITH (NOLOCK) ON RTRIM(LTRIM(fo.INSTL_SRVC_TIER_CD)) = ls.MDS_SRVC_TIER_ID cross apply 
			  [web].[parseStringWithDelimiter] (oe.MDS_ENTLMNT_ID, ';')  AS ps
			  LEFT JOIN dbo.LK_MDS_ENTLMNT  le WITH (NOLOCK) ON ps.StringID = le.MDS_ENTLMNT_ID     
		WHERE  fo.PROD_TYPE_CD IN ('MN', 'SE')      
			   AND  fo.ORDR_TYPE_CD IN ('IN')
			   
	-----------------------------------------------------------------------      
    -- Table 01 - CPE Info FTN Table.      
    -----------------------------------------------------------------------      	
	SELECT DISTINCT @Tab_Seq_Nbr				 AS TAB_SEQ_NBR,
					fo.FTN						 AS FTN_CPE,
					ISNULL(odn.ODIE_DEV_NME, 0)				  AS DEV_MODEL_NME,
					ISNULL(ldm.DEV_MODEL_NME, 0)			  AS DEV_NME,
					ISNULL(lm.MANF_NME, 0)					  AS MANF_NME    
		FROM @Table tt INNER JOIN 
			 dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = tt.ORDR_ID LEFT JOIN 
			 dbo.ORDR od WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID LEFT JOIN
			 dbo.MDS_EVENT_ODIE_DEV_NME AS odn WITH (NOLOCK) ON odn.FSA_MDS_EVENT_ID = tt.FSA_MDS_EVENT_ID  LEFT JOIN
			 dbo.MDS_MNGD_ACT_NEW AS mn WITH (NOLOCK) ON  mn.EVENT_ID = tt.EVENT_ID  LEFT JOIN
			 dbo.LK_DEV_MODEL ldm WITH (NOLOCK) ON ldm.DEV_MODEL_ID = mn.DEV_MODEL_ID LEFT JOIN   
			 dbo.LK_DEV_MANF lm WITH (NOLOCK) ON lm.MANF_ID = mn.MANF_ID
		WHERE  fo.ORDR_TYPE_CD IN ('IN')      
				AND  ( (od.DMSTC_CD = 1 AND fo.PROD_TYPE_CD = 'CP')      
						OR ( od.DMSTC_CD       = 0          
						AND fo.PROD_TYPE_CD      IN ('CP', 'IP')       
						AND ISNULL(fo.CPE_CPE_ORDR_TYPE_CD, '') != '')      
					 ) 
					 
	
	-----------------------------------------------------------------------      
    -- Table 02 - Wired Device Transport Info Table.      
    -----------------------------------------------------------------------  
     SELECT DISTINCT @Tab_Seq_Nbr							AS TAB_SEQ_NBR, 
					ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '')	AS WdTransportType,       
			        ''										AS WdPrimaryBackup,
					''										AS FMS_CKT_ID,      
					''										AS PL_NBR,      
					ISNULL(fo.TTRPT_NW_ADR, '')			    AS NUA_449_ADR,      
					''										AS OLD_CKT_ID,      
					0										AS MULTI_LINK_CKT,      
					''										AS WdSprintManaged,
					' '										AS WdTelcoCallout,
					''										AS FOC_DT,
					' '										AS NEW_FMS_CKT_ID,
					' '										AS NEW_NUA,
					fo.FTN									AS ACCESS_FTN
		 FROM  @Table AS tt LEFT JOIN 
				dbo.FSA_ORDR AS fo WITH (NOLOCK) ON tt.ORDR_ID = fo.ORDR_ID LEFT JOIN      
				dbo.ORDR AS od WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID LEFT JOIN 
				dbo.LK_MDS_TRNSPRT_TYPE AS lm WITH (NOLOCK) ON fo.INSTL_TRNSPRT_TYPE_CD = lm.MDS_TRNSPRT_CD LEFT JOIN
				dbo.FSA_ORDR_CPE_LINE_ITEM AS CPED with (NOLOCK) ON fo.ORDR_ID = CPED.ORDR_ID      
		 WHERE  fo.ORDR_TYPE_CD IN  ('IN')      
				AND  ( (od.DMSTC_CD = 1 AND fo.PROD_TYPE_CD NOT IN ('MN', 'CP', 'SE'))      
						OR ( od.DMSTC_CD       = 0          
						AND fo.PROD_TYPE_CD      IN ('CP', 'IP')       
						AND ISNULL(CPED.TTRPT_ACCS_TYPE_CD, '') != '')      
					 ) 
				AND ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0) <> 0	
	
					 
	-----------------------------------------------------------------------      
    -- Table 03 - Wireless Transport Data. 
    -----------------------------------------------------------------------      
    SELECT DISTINCT @Tab_Seq_Nbr							AS TAB_SEQ_NBR, 
					''										AS WlPrimaryBackup,          
					ISNULL(md.ESN_MAC_ID, '')						AS ESN      
		FROM  dbo.MDS_EVENT_WRLS_TRPT md WITH (NOLOCK)       
		WHERE  1 != 1      
          
    -----------------------------------------------------------------------      
    -- Table 04 - Virtual Connection PVC Data.
    -----------------------------------------------------------------------      
    SELECT DISTINCT @Tab_Seq_Nbr							AS TAB_SEQ_NBR, 
					ISNULL(DLCI_VPI_CD, '')					AS DLCI_VPI_CD      
	     FROM  dbo.MDS_EVENT_VRTL_CNCTN WITH (NOLOCK)       
		 WHERE  1 != 1      					 	 	 					
          
    -----------------------------------------------------------------------      
    -- Table 05 - Get the remaining data for a given MNS Event Data.      
    -----------------------------------------------------------------------      
    IF EXISTS(SELECT 'X' 
			  FROM    @Table    vt        
				INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID        
				INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID        
				LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID        
				LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID        
				WHERE  (oc.CNTCT_TYPE_ID = 1 and oc.role_id = 6)      
					and (sc.CNTCT_TYPE_ID = 3       
					and (LEN(ISNULL(sc.FRST_NME, '')) <> 0)      
					)
				)      
	 BEGIN            
			SELECT DISTINCT -- CPE Section        
					 0					AS WIRED_DEV_TRPT_REQR_CD
					,0					AS SPAE_TRNSPRT_REQR_CD
					,0					AS DSL_TRNSPRT_REQR_CD      
					,0					AS SPRF_DEV_TRNSPRT_REQR_CD
					,0					AS WRLS_TRNSPRT_REQR_CD
					,0					AS VRTL_CNCTN_CD    
					,0					AS CMPLTD_CD        
					,ISNULL(oc.FRST_NME, '') + ' ' +        
							ISNULL(oc.LST_NME, '')  
										AS  INSTL_SITE_POC_NME
					,oc.ISD_CD			AS  INSTL_SITE_POC_INTL_PHN_CD
					,CASE ISNULL(oc.ISD_CD, '') WHEN '' THEN '' ELSE oc.ISD_CD + '-' END        
						+ CASE ISNULL(oc.PHN_NBR, '')        
							WHEN '' THEN ISNULL(oc.CTY_CD, '')         
						+ ISNULL(oc.NPA, '')          
						+ ISNULL(oc.NXX, '')          
						+ ISNULL(oc.STN_NBR, '')         
						ELSE ISNULL(oc.CTY_CD, '')   + oc.PHN_NBR        
						END        
								  AS  INSTL_SITE_POC_PHN_NBR         
					,''			  AS  INSTL_SITE_POC_INTL_CELL_PHN_CD
					,''           AS  INSTL_SITE_POC_CELL_PHN_NBR        
					,ISNULL(sc.FRST_NME, '') + ' ' +        
						ISNULL(sc.LST_NME, '')  
								 AS  SRVC_ASSRN_POC_NME        
					, sc.ISD_CD	 AS  SRVC_ASSRN_POC_INTL_PHN_CD
					, CASE ISNULL(sc.ISD_CD, '') WHEN '' THEN '' ELSE sc.ISD_CD + '-' END        
						+ CASE ISNULL(sc.PHN_NBR, '')        
							WHEN '' THEN ISNULL(sc.CTY_CD, '')         
						+ ISNULL(sc.NPA, '')          
						+ ISNULL(sc.NXX, '')          
						+ ISNULL(sc.STN_NBR, '')         
						ELSE ISNULL(sc.CTY_CD, '')  + sc.PHN_NBR        
						END   
								AS  SRVC_ASSRN_POC_PHN_NBR                 
					,''		    AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD
					,''         AS  SRVC_ASSRN_POC_CELL_NBR  
					,ISNULL(sc.CNTCT_HR_TXT, '')        AS SA_POC_HR_NME  
					,ISNULL(sc.FSA_TME_ZONE_CD, '')			AS TME_ZONE_ID      
			    ,@Tab_Seq_Nbr        AS TAB_SEQ_NBR 
			    ,las.SRVC_ASSRN_SITE_SUPP_DES
				,fmds.CPE_DSPCH_EMAIL_ADR
				,fmds.CPE_DSPCH_CMNT_TXT  
				,lsc.SPRINT_CPE_NCR_DES     
			FROM  @Table    vt        
				INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID        
				INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID        
				LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID        
				LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID   
				LEFT JOIN dbo.MDS_EVENT_NEW fmds WITH (NOLOCK) ON fmds.EVENT_ID= vt.EVENT_ID    
				LEFT JOIN dbo.lk_SRVC_ASSRN_SITE_SUPP las WITH (NOLOCK) ON las.SRVC_ASSRN_SITE_SUPP_ID = fmds.SRVC_ASSRN_SITE_SUPP_ID
				LEFT JOIN dbo.lk_SPRINT_CPE_NCR lsc WITH (NOLOCK) ON lsc.SPRINT_CPE_NCR_ID = fmds.SPRINT_CPE_NCR_ID
			WHERE  (oc.CNTCT_TYPE_ID = 1 and oc.role_id = 6)      
				    AND (sc.CNTCT_TYPE_ID = 3       
						   AND (LEN(ISNULL(sc.FRST_NME, '')) <> 0)      
						)      
	END       
    ELSE IF EXISTS( SELECT 'X' FROM  @Table    vt        
					 INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID        
					 INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID        
					 LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID        
					 LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID        
					WHERE  (oc.CNTCT_TYPE_ID = 1 and oc.role_id = 6)      
				  )           
    BEGIN       
			SELECT DISTINCT       
					0					AS WIRED_DEV_TRPT_REQR_CD
					,0					AS SPAE_TRNSPRT_REQR_CD
					,0					AS DSL_TRNSPRT_REQR_CD      
					,0					AS SPRF_DEV_TRNSPRT_REQR_CD
					,0					AS WRLS_TRNSPRT_REQR_CD
					,0					AS VRTL_CNCTN_CD    
					,0					AS CMPLTD_CD        
					,ISNULL(oc.FRST_NME, '') 
							+ ' ' 
							+ ISNULL(oc.LST_NME, '')  
								AS  INSTL_SITE_POC_NME        
					, CASE od.DMSTC_CD         
							WHEN 1 THEN CASE ISNULL(oc.ISD_CD, '') 
											WHEN '' THEN ''
											ELSE oc.ISD_CD 
										END 
							END  
							AS INSTL_SITE_POC_INTL_PHN_CD
					, CASE ISNULL(oc.PHN_NBR, '')        
						 WHEN '' THEN ISNULL(oc.ISD_CD, '')        
									+ ISNULL(oc.CTY_CD, '')         
									+ ISNULL(oc.NPA, '')          
									+ ISNULL(oc.NXX, '')          
									+ ISNULL(oc.STN_NBR, '')        
						 ELSE oc.PHN_NBR        
						 END        
							 AS  INSTL_SITE_POC_PHN_NBR         
				,''		     AS  INSTL_SITE_POC_INTL_CELL_PHN_CD
				,''          AS  INSTL_SITE_POC_CELL_PHN_NBR        
				,ISNULL(sc.FRST_NME, '') + ' ' +        
						ISNULL(sc.LST_NME, '')  
								 AS  SRVC_ASSRN_POC_NME        
					, sc.ISD_CD	 AS  SRVC_ASSRN_POC_INTL_PHN_CD
					, CASE ISNULL(sc.ISD_CD, '') WHEN '' THEN '' ELSE sc.ISD_CD + '-' END        
						+ CASE ISNULL(sc.PHN_NBR, '')        
							WHEN '' THEN ISNULL(sc.CTY_CD, '')         
						+ ISNULL(sc.NPA, '')          
						+ ISNULL(sc.NXX, '')          
						+ ISNULL(sc.STN_NBR, '')         
						ELSE ISNULL(sc.CTY_CD, '')  + sc.PHN_NBR        
						END   
								AS  SRVC_ASSRN_POC_PHN_NBR                 
				,''          AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD
				,''          AS  SRVC_ASSRN_POC_CELL_NBR        
				,ISNULL(sc.CNTCT_HR_TXT, '')        AS SA_POC_HR_NME  
				,ISNULL(sc.FSA_TME_ZONE_CD, '')			AS TME_ZONE_ID
				,@Tab_Seq_Nbr        AS TAB_SEQ_NBR    
				,las.SRVC_ASSRN_SITE_SUPP_DES
				,fmds.CPE_DSPCH_EMAIL_ADR
				,fmds.CPE_DSPCH_CMNT_TXT
				,lsc.SPRINT_CPE_NCR_DES
        FROM  @Table    vt        
			INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID        
			INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID        
			LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID        
			LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID  
			LEFT JOIN dbo.MDS_EVENT_NEW fmds WITH (NOLOCK) ON fmds.EVENT_ID= vt.EVENT_ID    
			LEFT JOIN dbo.lk_SRVC_ASSRN_SITE_SUPP las WITH (NOLOCK) ON las.SRVC_ASSRN_SITE_SUPP_ID = fmds.SRVC_ASSRN_SITE_SUPP_ID 
			LEFT JOIN dbo.lk_SPRINT_CPE_NCR lsc WITH (NOLOCK) ON lsc.SPRINT_CPE_NCR_ID = fmds.SPRINT_CPE_NCR_ID     
        WHERE  (oc.CNTCT_TYPE_ID = 1 and oc.role_id = 6)      
    END      
    ELSE       
    BEGIN       
       SELECT DISTINCT 
					 0					AS WIRED_DEV_TRPT_REQR_CD
					,0					AS SPAE_TRNSPRT_REQR_CD
					,0					AS DSL_TRNSPRT_REQR_CD      
					,0					AS SPRF_DEV_TRNSPRT_REQR_CD
					,0					AS WRLS_TRNSPRT_REQR_CD
					,0					AS VRTL_CNCTN_CD    
					,0					AS CMPLTD_CD        
					,ISNULL(oc.FRST_NME, '') + ' ' +        
						ISNULL(oc.LST_NME, '')  
										AS  INSTL_SITE_POC_NME        
					,CASE od.DMSTC_CD         
						WHEN 1 THEN CASE ISNULL(oc.ISD_CD, '') 
										WHEN '' THEN '' ELSE oc.ISD_CD + '-' 
									END 
					END 
						AS   INSTL_SITE_POC_INTL_PHN_CD     
					, CASE ISNULL(oc.PHN_NBR, '')        
						WHEN '' THEN ISNULL(oc.CTY_CD, '')         
									+ ISNULL(oc.NPA, '')          
									+ ISNULL(oc.NXX, '')          
									+ ISNULL(oc.STN_NBR, '')         
						ELSE oc.PHN_NBR        
					END                      
						 		AS  INSTL_SITE_POC_PHN_NBR         
					,''		     AS  INSTL_SITE_POC_INTL_CELL_PHN_CD
					,''          AS  INSTL_SITE_POC_CELL_PHN_NBR        
					,ISNULL(sc.FRST_NME, '') + ' ' +        
						ISNULL(sc.LST_NME, '')  
								 AS  SRVC_ASSRN_POC_NME        
					, sc.ISD_CD	 AS  SRVC_ASSRN_POC_INTL_PHN_CD
					, CASE ISNULL(sc.ISD_CD, '') WHEN '' THEN '' ELSE sc.ISD_CD + '-' END        
						+ CASE ISNULL(sc.PHN_NBR, '')        
							WHEN '' THEN ISNULL(sc.CTY_CD, '')         
						+ ISNULL(sc.NPA, '')          
						+ ISNULL(sc.NXX, '')          
						+ ISNULL(sc.STN_NBR, '')         
						ELSE ISNULL(sc.CTY_CD, '')  + sc.PHN_NBR        
						END   
								AS  SRVC_ASSRN_POC_PHN_NBR
					,''          AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD
					,''          AS  SRVC_ASSRN_POC_CELL_NBR
					,ISNULL(sc.CNTCT_HR_TXT, '')        AS SA_POC_HR_NME  
					,ISNULL(sc.FSA_TME_ZONE_CD, '')			AS TME_ZONE_ID  
					,@Tab_Seq_Nbr        AS TAB_SEQ_NBR
					,las.SRVC_ASSRN_SITE_SUPP_DES
					,fmds.CPE_DSPCH_EMAIL_ADR
					,fmds.CPE_DSPCH_CMNT_TXT
					,lsc.SPRINT_CPE_NCR_DES        
        FROM  @Table    vt        
			INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID        
			INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID        
			LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID        
			LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID
			LEFT JOIN dbo.MDS_EVENT_NEW fmds WITH (NOLOCK) ON fmds.EVENT_ID= vt.EVENT_ID    
			LEFT JOIN dbo.lk_SRVC_ASSRN_SITE_SUPP las WITH (NOLOCK) ON las.SRVC_ASSRN_SITE_SUPP_ID = fmds.SRVC_ASSRN_SITE_SUPP_ID
			LEFT JOIN dbo.lk_SPRINT_CPE_NCR lsc WITH (NOLOCK) ON lsc.SPRINT_CPE_NCR_ID = fmds.SPRINT_CPE_NCR_ID        
         WHERE   (sc.CNTCT_TYPE_ID = 3       
             and (LEN(ISNULL(sc.FRST_NME, '')) <> 0)      
            )      
               
     END         
           
     -----------------------------------------------------------------------      
     -- Table 06 - Get the Address elements for the order .      
     -----------------------------------------------------------------------      
     OPEN SYMMETRIC KEY FS@K3y       
     DECRYPTION BY CERTIFICATE S3cFS@CustInf0;      
     SELECT DISTINCT      
        -- Site Location Info      
        ISNULL(oa.STREET_ADR_1, '') + ' ' +      
         ISNULL(oa.STREET_ADR_2, '') AS  ADR      
        ,ISNULL(oa.BLDG_NME, '')   AS  FLR_BLDG_NME      
        ,ISNULL(oa.CTY_NME, '')   AS  CTY_NME      
        ,ISNULL(oa.STT_CD, '') +        
         ISNULL(oa.PRVN_NME, '')  AS  STT_PRVN_NME      
        ,ISNULL(lc.CTRY_NME, '')        AS  CTRY_RGN_NME      
        ,ISNULL(oa.CTRY_CD, '')         AS  CTRY_CD      
        ,ISNULL(oa.ZIP_PSTL_CD, '')  AS  ZIP_CD      
        ,CASE od.DMSTC_CD      
         WHEN NULL THEN 'US'      
				 WHEN 0 THEN 'US'      
				 ELSE 'International'            
        END              AS US_INTL_ID      
     FROM @Table    vt      
       INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID      
       INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID      
       LEFT JOIN dbo.ORDR_ADR  oa WITH (NOLOCK) ON fo.ORDR_ID = oa.ORDR_ID      
       LEFT JOIN dbo.LK_CTRY   lc WITH (NOLOCK) ON oa.CTRY_CD = lc.CTRY_CD      
      WHERE   oa.ADR_TYPE_ID  = 18    


	----------------------------------------------------------------------      
     -- Table 07 - Get the LOC Data.      
     ----------------------------------------------------------------------- 
	  SELECT DISTINCT  ISNULL(lm.MDS_LOC_CAT_DES, '')       AS MDS_LOC_CAT_DES    
         ,ISNULL(lt.MDS_LOC_TYPE_DES, '')     AS MDS_LOC_TYPE_DES     
         ,ISNULL(ml.MDS_LOC_NBR, '')       AS MDS_LOC_NBR    
         ,ISNULL(Convert(Varchar(10), MDS_LOC_RAS_DT, 101), '') AS MDS_LOC_RAS_DT    
         ,0             AS MDSLOCID      
      FROM  @Table    vt      
		  INNER JOIN dbo.MDS_EVENT_LOC ml WITH (NOLOCK) ON ml.EVENT_ID = vt.EVENT_ID
		  LEFT JOIN dbo.LK_MDS_LOC_CAT lm WITH (NOLOCK) ON ml.MDS_LOC_CAT_ID = lm.MDS_LOC_CAT_ID    
		  LEFT JOIN dbo.LK_MDS_LOC_TYPE lt WITH (NOLOCK) ON ml.MDS_LOC_TYPE_ID = lt.MDS_LOC_TYPE_ID
     
       
   END      
  ELSE      
   BEGIN      

	-----------------------------------------------------------------------      
    -- Table 00 - MNS Info FTN Table.      
    -----------------------------------------------------------------------  
        
    SELECT DISTINCT ISNULL(em.TAB_SEQ_NBR, @Tab_Seq_Nbr)     AS TAB_SEQ_NBR
					,CASE em.MNS_FTN_NBR 
						WHEN '0' THEN '' 
						ELSE MNS_FTN_NBR 
					 END 
															 AS MDS_MNS_FTN
					,ISNULL(ls.MDS_SRVC_TIER_DES, ' ')		 AS MDS_SRVC_TIER_DES
					,ISNULL(le.MDS_ENTLMNT_DES, ' ') AS MDS_ENTLMNT_DES
		FROM  @Table   tt 
				 INNER JOIN dbo.MDS_EVENT_MNS AS em WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = em.FSA_MDS_EVENT_ID
				 LEFT JOIN dbo.LK_MDS_SRVC_TIER AS ls WITH (NOLOCK) ON em.MDS_SRVC_TIER_ID = ls.MDS_SRVC_TIER_ID      
				 cross apply [web].[parseStringWithDelimiter] (em.MDS_ENTLMNT_ID, ';')  AS ps  --ON 1=1
				 LEFT JOIN dbo.LK_MDS_ENTLMNT  le WITH (NOLOCK) ON ps.StringID = le.MDS_ENTLMNT_ID 
				 
	-----------------------------------------------------------------------      
    -- Table 01 - CPE Info FTN Table.      
    -----------------------------------------------------------------------     
    SELECT DISTINCT ISNULL(ec.TAB_SEQ_NBR, @Tab_Seq_Nbr)     AS TAB_SEQ_NBR
					,CASE ec.CPE_FTN_NBR 
						WHEN '0' THEN '' 
						ELSE CPE_FTN_NBR 
					 END 
															 AS FTN_CPE
					,ISNULL(odn.ODIE_DEV_NME, 0)				  AS DEV_MODEL_NME
					,ISNULL(ldm.DEV_MODEL_NME, 0)			  AS DEV_NME
					,ISNULL(lm.MANF_NME, 0)					  AS MANF_NME
		FROM @Table tt INNER JOIN 
			 dbo.MDS_EVENT_CPE AS ec WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = ec.FSA_MDS_EVENT_ID LEFT JOIN
			 dbo.MDS_EVENT_ODIE_DEV_NME AS odn WITH (NOLOCK) ON odn.FSA_MDS_EVENT_ID = tt.FSA_MDS_EVENT_ID  LEFT JOIN
			 dbo.MDS_MNGD_ACT_NEW AS mn WITH (NOLOCK) ON  mn.EVENT_ID = tt.EVENT_ID  LEFT JOIN
			 dbo.LK_DEV_MODEL ldm WITH (NOLOCK) ON ldm.DEV_MODEL_ID = mn.DEV_MODEL_ID LEFT JOIN   
			 dbo.LK_DEV_MANF lm WITH (NOLOCK) ON lm.MANF_ID = mn.MANF_ID  
	
	-----------------------------------------------------------------------      
    -- Table 02 - Wired Device Transport Info Table.      
    -----------------------------------------------------------------------  
     SELECT DISTINCT ISNULL(wt.TAB_SEQ_NBR, @Tab_Seq_Nbr)    AS TAB_SEQ_NBR
					,ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '')	 AS WdTransportType
					,CASE ISNULL(wt.PRIM_BKUP_CD, '')       
						 WHEN 'P' THEN 'Primary'      
						 WHEN 'B' THEN 'Backup'      
						 ELSE ''      
						END								     AS WdPrimaryBackup  
			        ,wt.PRIM_BKUP_CD						 AS PRIM_BKUP_CD
					,wt.FMS_CKT_ID							 AS FMS_CKT_ID
					,wt.PL_NBR								 AS PL_NBR
					,ISNULL(wt.IP_NUA_ADR, '')			     AS NUA_449_ADR
					,wt.OLD_CKT_ID							 AS OLD_CKT_ID
					,' '									 AS MULTI_LINK_CKT    
					,CASE ISNULL(wt.SPRINT_MNGD_CD, '')      
						 WHEN 'S' THEN 'Sprint'      
						 WHEN 'C' THEN 'Customer'       
						 ELSE ''         
						END									 AS WdSprintManaged      
					,ISNULL(wt.SPRINT_MNGD_CD, '')			 AS SPRINT_MNGD_CD
					,ISNULL(wt.TELCO_ID, '')		AS TELCO_ID    
					,ISNULL(lt.TELCO_NME, '') 
							+ ':' +       
							ISNULL(lt.TELCO_CNTCT_PHN_NBR, '')		AS WdTelcoCallout  
					,ISNULL(Convert(Varchar(10), FOC_DT, 101), '')	AS FOC_DT  
					,' '									 AS NEW_FMS_CKT_ID
					,' '									 AS NEW_NUA
					,mea.ACCS_FTN_NBR						 AS ACCESS_FTN
		 FROM  @Table   tt 
				 LEFT JOIN dbo.MDS_EVENT_WIRED_TRPT AS wt WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = wt.FSA_MDS_EVENT_ID 
				 LEFT JOIN dbo.LK_MDS_TRNSPRT_TYPE   lm WITH (NOLOCK)  ON wt.MDS_TRPT_TYPE_ID = lm.MDS_TRNSPRT_TYPE_ID               
				 LEFT JOIN dbo.LK_TELCO     lt WITH (NOLOCK) ON wt.TELCO_ID    = lt.TELCO_ID  
				 LEFT JOIN dbo.MDS_EVENT_ACCS mea WITH (NOLOCK) ON mea.FSA_MDS_EVENT_ID = tt.FSA_MDS_EVENT_ID
	
					 
	-----------------------------------------------------------------------      
    -- Table 03 - Wireless Transport Data. 
    -----------------------------------------------------------------------  
        
    SELECT DISTINCT ISNULL(md.TAB_SEQ_NBR, @Tab_Seq_Nbr)		AS TAB_SEQ_NBR
					,CASE ISNULL(md.PRIM_BKUP_CD, '')       
						 WHEN 'P' THEN 'Primary'      
						 WHEN 'B' THEN 'Backup'      
						 ELSE ''      
						END										AS WlPrimaryBackup      
					,ISNULL(md.ESN_MAC_ID, '')					AS ESN      
		FROM  @Table   tt 
				 INNER JOIN dbo.MDS_EVENT_WRLS_TRPT md WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = md.FSA_MDS_EVENT_ID 
		
          
    -----------------------------------------------------------------------      
    -- Table 04 - Virtual Connection PVC Data.
    -----------------------------------------------------------------------      
    
    SELECT DISTINCT ISNULL(md.TAB_SEQ_NBR, @Tab_Seq_Nbr)	AS TAB_SEQ_NBR
					,ISNULL(md.DLCI_VPI_CD, '')				AS DLCI_VPI_CD      
	     FROM  @Table   tt 
				 INNER JOIN dbo.MDS_EVENT_VRTL_CNCTN md WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = md.FSA_MDS_EVENT_ID 
		 
    -----------------------------------------------------------------------      
    -- Table 05 - Get the remaining data for a given MNS Event Data.      
    -----------------------------------------------------------------------      
SELECT DISTINCT ISNULL(fm.TAB_SEQ_NBR, @Tab_Seq_Nbr)	AS TAB_SEQ_NBR      
					,ISNULL(fm.FSA_MDS_EVENT_ID, 0)			AS FSA_MDS_EVENT_ID      
					-- CPE Section      
					,fm.WIRED_DEV_TRPT_REQR_CD				AS WIRED_DEV_TRPT_REQR_CD
					,fm.SPAE_TRNSPRT_REQR_CD				AS SPAE_TRNSPRT_REQR_CD
					,fm.DSL_TRNSPRT_REQR_CD				    AS DSL_TRNSPRT_REQR_CD
					,fm.SPRF_DEV_TRNSPRT_REQR_CD			AS SPRF_DEV_TRNSPRT_REQR_CD
					,fm.WRLS_TRPT_REQR_CD					AS WRLS_TRNSPRT_REQR_CD
					,fm.VRTL_CNCTN_CD						AS VRTL_CNCTN_CD      
					
					,ISNULL(fm.INSTL_SITE_POC_NME, '')   AS  INSTL_SITE_POC_NME      
					,ISNULL(fm.INSTL_SITE_POC_INTL_PHN_CD, '' )					AS  INSTL_SITE_POC_INTL_PHN_CD
					,ISNULL(fm.INSTL_SITE_POC_PHN_NBR, '')						AS  INSTL_SITE_POC_PHN_NBR      
					,ISNULL(fm.INSTL_SITE_POC_INTL_CELL_PHN_CD, '')				AS  INSTL_SITE_POC_INTL_CELL_PHN_CD
					,ISNULL(fm.INSTL_SITE_POC_CELL_PHN_NBR, '')					AS  INSTL_SITE_POC_CELL_PHN_NBR      
					,ISNULL(fm.SRVC_ASSRN_POC_NME, '')   AS  SRVC_ASSRN_POC_NME      
					,ISNULL(fm.SRVC_ASSRN_POC_INTL_PHN_CD, '')					AS  SRVC_ASSRN_POC_INTL_PHN_CD
					,ISNULL(fm.SRVC_ASSRN_POC_PHN_NBR, '')						AS  SRVC_ASSRN_POC_PHN_NBR      
					,ISNULL(fm.SRVC_ASSRN_POC_INTL_CELL_PHN_CD, '')				AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD
					,ISNULL(fm.SRVC_ASSRN_POC_CELL_NBR, '')  AS  SRVC_ASSRN_POC_CELL_NBR      
					,ISNULL(fm.SA_POC_HR_NME, '')  AS  SA_POC_HR_NME 
					,ISNULL(fm.TME_ZONE_ID, '')			AS TME_ZONE_ID           
					,fm.CMPLTD_CD        AS CMPLTD_CD
					,las.SRVC_ASSRN_SITE_SUPP_DES
					,fmds.CPE_DSPCH_EMAIL_ADR
					,fmds.CPE_DSPCH_CMNT_TXT
					,lsc.SPRINT_CPE_NCR_DES      
     FROM  @Table    vt      
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fm WITH (NOLOCK) ON vt.FSA_MDS_EVENT_ID = fm.FSA_MDS_EVENT_ID   
     LEFT JOIN dbo.MDS_EVENT_NEW fmds WITH (NOLOCK) ON fmds.EVENT_ID= vt.EVENT_ID    
	 LEFT JOIN dbo.lk_SRVC_ASSRN_SITE_SUPP las WITH (NOLOCK) ON las.SRVC_ASSRN_SITE_SUPP_ID = fmds.SRVC_ASSRN_SITE_SUPP_ID
	 LEFT JOIN dbo.lk_SPRINT_CPE_NCR lsc WITH (NOLOCK) ON lsc.SPRINT_CPE_NCR_ID = fmds.SPRINT_CPE_NCR_ID     
        
     
     ----------------------------------------------------------------------      
     -- Table 06 - Get the FTN's for Transport Order.      
     -----------------------------------------------------------------------      
     SELECT DISTINCT      
     -- Site Location Info      
        ISNULL(fm.SITE_ADR, '')		AS  ADR
        ,ISNULL(fm.FLR_BLDG_NME, '') AS  FLR_BLDG_NME      
        ,ISNULL(fm.CTY_NME, '')		AS  CTY_NME      
        ,ISNULL(fm.STT_PRVN_NME, '') AS  STT_PRVN_NME      
        ,ISNULL(fm.CTRY_RGN_NME, '') AS  CTRY_RGN_NME      
        ,ISNULL(fm.ZIP_CD, '')		AS  ZIP_CD      
        ,CASE ISNULL(fm.US_INTL_ID, NULL)      
				 WHEN NULL THEN 'US'      
				 WHEN 'D' THEN 'US'      
				 ELSE 'International'      
				END											AS US_INTL_ID       
     FROM  @Table    vt      
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fm WITH (NOLOCK) ON vt.FSA_MDS_EVENT_ID = fm.FSA_MDS_EVENT_ID   
	 
	 ----------------------------------------------------------------------      
     -- Table 07 - Get the LOC Data.      
     ----------------------------------------------------------------------- 
	  SELECT DISTINCT  ISNULL(lm.MDS_LOC_CAT_DES, '')       AS MDS_LOC_CAT_DES    
         ,ISNULL(lt.MDS_LOC_TYPE_DES, '')     AS MDS_LOC_TYPE_DES     
         ,ISNULL(ml.MDS_LOC_NBR, '')       AS MDS_LOC_NBR    
         ,ISNULL(Convert(Varchar(10), MDS_LOC_RAS_DT, 101), '') AS MDS_LOC_RAS_DT    
         ,0             AS MDSLOCID      
      FROM  @Table    vt      
		  INNER JOIN dbo.MDS_EVENT_LOC ml WITH (NOLOCK) ON ml.EVENT_ID = vt.EVENT_ID
		  LEFT JOIN dbo.LK_MDS_LOC_CAT lm WITH (NOLOCK) ON ml.MDS_LOC_CAT_ID = lm.MDS_LOC_CAT_ID    
		  LEFT JOIN dbo.LK_MDS_LOC_TYPE lt WITH (NOLOCK) ON ml.MDS_LOC_TYPE_ID = lt.MDS_LOC_TYPE_ID

   END           
 END      
END Try      
      
Begin Catch      
 EXEC [dbo].[insertErrorInfo]      
END Catch      
END