USE [COWS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='web' and routine_name='getAdvSearchRedesignInfo')
EXEC ('CREATE PROCEDURE [web].[getAdvSearchRedesignInfo] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO
--sp_helptext '[web].[getAdvSearchRedesignInfo]'

-- =======================================================
-- Created By:	Sarah Sandoval
-- Create date:	02/01/2017
-- Description:	Advanced Search
-- Modified By:	MD M Monir   VN370313
-- Modified date:	02/01/2017
-- Description:	Modified as per Instruction from Pramod and Sarah
-- Modified By:	MD M Monir   VN370313
-- Modified date:	02/10/2017
-- Description:	ModifiedEeventID Filter  ----Added by Monir  02102017
-- sp_Helptext 'web.getAdvSearchRedesignInfo'
-- Modified By: Sarah Sandoval
-- Modified Date: 03/29/2017
-- Description: Added Convert(Varchar) to @RedesignCat and @RedesignStus Filter
--		Removed extra "r." on Redesign Type Filter
--		Added new parameter @Status
-- =======================================================
--EXEC [web].[getAdvSearchRedesignInfo_TEST_Monir] 'R%','A'
--EXEC [web].[getAdvSearchRedesignInfo] 'R%','A'
--EXEC [web].[getAdvSearchRedesignInfo] 'R%',''
ALTER PROCEDURE [web].[getAdvSearchRedesignInfo]
	@RedesignNumbers	VARCHAR(1000)	= '',
	@CustomerNames		VARCHAR(1000)	= '',
	@NTE			VARCHAR(1000)	= '',
	@ODIEDevices		VARCHAR(1000)	= '',
	@PM                 	VARCHAR (1000)  = '',
	@H1			VARCHAR (1000)  = '',
	@RedesignCat		INT	=	0,
	@RedesignType		VARCHAR (1000)  = '',
	@RedesignStus		INT =	0,
	@EventID		INT =	0,
	@MSSSE			VARCHAR (1000)  = '',
	@NE			VARCHAR (1000)  = '',
	@SubmittedDate		DATETIME	=	NULL,
	@SLADueDate		DATETIME	=	NULL,
	@ExpirationDate		DATETIME	=	NULL,
	@Status			VARCHAR (1)		= '',
	@CSGLvlId		TINYINT			= 0
	
AS
BEGIN
Begin Try
DECLARE @OrdrBy VARCHAR(50)
DECLARE @DropTbl NVARCHAR(400)				
DECLARE @CrtTbl NVARCHAR(4000)
DECLARE @InsrtTbl VARCHAR(2000)
DECLARE @FinalSel NVARCHAR(4000)
			    
SET NOCOUNT ON;  
	SET @OrdrBy  = ' ORDER BY r.[REDSGN_ID] DESC '
	--SET @DropTbl = ' IF OBJECT_ID(N''tempdb..#RedesignResults'', N''U'') IS NOT NULL DROP TABLE #RedesignResults '
	SET @DropTbl = ' '
	SET @CrtTbl  = ' DECLARE @RedesignResults  TABLE
		(
			REDSGN_ID			VARCHAR(100), 
			Redesign_Number		VARCHAR(100), 
			CUST_NME			VARCHAR(200),
			H1					VARCHAR(9), 
			Redesign_Cat_ID		INT,
			Redesign_Cat		VARCHAR(100), 
			Redesign_Type_ID	INT,
			Redesign_Type		VARCHAR(100), 
			Redesign_Status_ID	INT,
			Redesign_Status		VARCHAR(50),
			Device_Name			VARCHAR(1000),
			Assigned_NTE		VARCHAR(100), 
			Assigned_PM			VARCHAR(100),
			EventID				INT,
			Assigned_MSSSE		VARCHAR(100), 
			Assigned_NE			VARCHAR(100),
			Submitted_Date		DATETIME, 
			SLA_Due_Date		DATETIME,
			Expiration_Date		DATETIME,
			FT_CD				BIT,
			Dispatch_Ready_CD	BIT,
			Completion_CD		BIT,
			CSG_LVL_ID			TINYINT
		)'
	SET @InsrtTbl = ' INSERT INTO @RedesignResults (REDSGN_ID,Redesign_Number,CUST_NME,H1,Redesign_Cat_ID,Redesign_Cat,Redesign_Type_ID,Redesign_Type, 
Redesign_Status_ID	,Redesign_Status,Device_Name,Assigned_NTE,Assigned_PM,EventID,Assigned_MSSSE, 
Assigned_NE,Submitted_Date,SLA_Due_Date,Expiration_Date,FT_CD,Dispatch_Ready_CD,Completion_CD,CSG_LVL_ID) '
	SET @FinalSel = ' 
	SELECT	DISTINCT
	        REDSGN_ID           AS ''Redesign ID'',
			Redesign_Number		AS ''Redesign Number'',
			Expiration_Date		AS ''Expiration Date'', 
			H1					AS ''H1'', 
			CASE WHEN (CSG_LVL_ID>0) THEN ''Private Customer'' ELSE CUST_NME END AS	''Customer Name'',
			Device_Name			AS ''Device Name'',
			FT_CD				AS ''Fast Track Flag'',
			Dispatch_Ready_CD	AS ''Dispatch Ready Flag'',
			Completion_CD		AS ''Completion Flag'',
			EventID				AS ''Event ID'',
			CSG_LVL_ID			
			/*
			Redesign_Cat_ID		AS ''Redesign Category ID'',
			Redesign_Cat		AS ''Redesign Category'',
			Redesign_Type_ID	AS ''Redesign Type ID'',
			Redesign_Type		AS ''Redesign Type'',
			Redesign_Status_ID	AS ''Redesign Status ID'',
			Redesign_Status		AS ''Redesign Status'',
			Assigned_NTE		AS ''Assigned NTE'', 
			Assigned_PM			AS ''Assigned PM'', 
			Assigned_MSSSE		AS ''Assigned MSS SE'', 
			Assigned_NE			AS ''Assigned NE'', 
			Submitted_Date		AS ''Submitted Date'', 
			SLA_Due_Date		AS ''SLA Due Date'',
			*/
		FROM @RedesignResults '
    SET @RedesignNumbers	= LTRIM(RTRIM(@RedesignNumbers))
    SET @CustomerNames		= LTRIM(RTRIM(@CustomerNames))
    SET @NTE				= LTRIM(RTRIM(@NTE))
    SET @ODIEDevices		= LTRIM(RTRIM(@ODIEDevices))
    SET @PM					= LTRIM(RTRIM(@PM))
    SET @H1					= LTRIM(RTRIM(@H1))
    SET @RedesignType		= LTRIM(RTRIM(@RedesignType))
    SET @MSSSE				= LTRIM(RTRIM(@MSSSE))
    SET @NE					= LTRIM(RTRIM(@NE))
    SET @Status				= LTRIM(RTRIM(@Status))
    
    -- Baseline '0' & null to '', 
    IF    ISNULL(@RedesignNumbers,'0')	=     '0'
          SET @RedesignNumbers			=     ''
    IF    ISNULL(@CustomerNames, '0')	=     '0'
          SET @CustomerNames			=     ''
    IF    ISNULL(@NTE, '0')				=     '0'
          SET @NTE						=     ''
	IF    ISNULL(@ODIEDevices, '0')		=     '0'
          SET @ODIEDevices				=     ''
	IF    ISNULL(@PM, '0')				=     '0'
          SET @PM 						=     ''
	IF    ISNULL(@H1, '0')				=     '0'
          SET @H1 						=     ''
	IF    ISNULL(@RedesignType, '0')	=     '0'
          SET @RedesignType 			=     ''
	IF    ISNULL(@MSSSE, '0')			=     '0'
          SET @MSSSE 					=     ''
	IF    ISNULL(@NE, '0')				=     '0'
          SET @NE 						=     ''
    --IF    ISNULL(@EventID, '0')			=     0
    --      SET @EventID 					=     ''   
		           
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
		BEGIN
			DECLARE @IsFilter	BIT
			DECLARE	@Select		NVARCHAR(Max)
			DECLARE @Filter		NVARCHAR(Max)
			DECLARE @SQL		NVARCHAR(Max)
			DECLARE @From		NVARCHAR(Max)
			DECLARE @Where		NVARCHAR(Max)
			
			SET @IsFilter	=	0		
			SET	@Select		=	''
			SET	@SQL		=	''
			SET	@Filter		=	''
			SET	@From		=	''
			SET @Where		=	''
			
			SET	@Select		=	
			'
				SELECT DISTINCT 
				r.REDSGN_ID AS ''REDSGN_ID'',
				r.REDSGN_NBR AS	''Redesign_Number'',
				CASE WHEN (r.CSG_LVL_ID>0) THEN LEFT(dbo.decryptBinaryData(csd.CUST_NME),200) ELSE r.CUST_NME END AS ''Customer_Name'', 
				r.H1_CD AS ''H1'',
				r.REDSGN_CAT_ID AS ''Redesign_Cat_ID'',
				lrc.REDSGN_CAT_DES AS ''Redesign_Cat'',
				r.REDSGN_TYPE_ID AS ''Redesign_Type_ID'',
				lrt.REDSGN_TYPE_NME AS ''Redesign_Type'',
				r.STUS_ID AS ''Redesign_Status_ID'',
				ls.STUS_DES AS ''Redesign_Status'',
				rd.DEV_NME AS ''ODIE_Device_Name'',
				lut.DSPL_NME AS ''Assigned_NTE'',
				lup.DSPL_NME AS ''Assigned_PM'',
				rdH.EVENT_ID AS ''EVENTID'',
				lum.DSPL_NME AS ''Assigned_MSSSE'',
				lun.DSPL_NME AS ''Assigned_NE'',
				r.SUBMIT_DT AS ''Submitted_Date'' ,
				r.SLA_DT AS ''SLA_Due_Date'',
				r.EXPRTN_DT AS ''Expiration_Date'',
				isnull(rd.FAST_TRK_CD,0) AS ''Fast_Track_Flag'',
				isnull(rd.DSPCH_RDY_CD,0) AS ''Dispatch_Ready_Flag'',
				CASE rd.DEV_CMPLTN_CD WHEN 1 THEN 1  ELSE 0 END  AS ''Completion_Flag'',
				r.CSG_LVL_ID AS ''CSG_LVL_ID''

			'
											   
			SET @From		=	
			'	
				FROM		dbo.REDSGN r WITH (NOLOCK)
				INNER JOIN	dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = r.CRETD_BY_CD
				INNER JOIN	dbo.LK_STUS ls WITH (NOLOCK) ON ls.STUS_ID = r.STUS_ID
				INNER JOIN	dbo.LK_REDSGN_TYPE lrt WITH (NOLOCK) ON lrt.REDSGN_TYPE_ID = r.REDSGN_TYPE_ID 
				INNER JOIN	dbo.LK_REDSGN_CAT lrc WITH (NOLOCK) ON lrc.REDSGN_CAT_ID = r.REDSGN_CAT_ID 
				LEFT JOIN  dbo.CUST_SCRD_DATA csd WITH (NOLOCK)   ON csd.SCRD_OBJ_ID=r.REDSGN_ID AND csd.SCRD_OBJ_TYPE_ID=16
				LEFT JOIN	dbo.REDSGN_DEVICES_INFO rd WITH (NOLOCK) ON r.REDSGN_ID = rd.REDSGN_ID AND rd.REC_STUS_ID = 1
				LEFT JOIN	dbo.REDSGN_DEV_EVENT_HIST rdH WITH (NOLOCK) ON RDH.REDSGN_DEV_ID = RD.REDSGN_DEV_ID
				LEFT JOIN	dbo.LK_USER lut WITH (NOLOCK) ON r.NTE_ASSIGNED = lut.USER_ADID 
				LEFT JOIN	dbo.LK_USER lup WITH (NOLOCK) ON r.PM_ASSIGNED = lup.USER_ADID
				LEFT JOIN	dbo.LK_USER lun WITH (NOLOCK) ON r.NE_ASN_NME = lun.USER_ADID
				LEFT JOIN	dbo.LK_USER lum WITH (NOLOCK) ON r.SDE_ASN_NME = lum.USER_ADID
				WHERE		1 = 1	
			'	
									 
			--select @SQL

			---------------------------------------------------------------
			--	Redesign Number Filter 
			---------------------------------------------------------------
			IF    @RedesignNumbers <> ''
				BEGIN
				  SET @RedesignNumbers = REPLACE(@RedesignNumbers, '*', '%')
				  SET @RedesignNumbers = REPLACE(@RedesignNumbers, '\r\n', ',')
				  SET @IsFilter = 1 

				  --If there is a comma, do the comma method
				  IF CHARINDEX(',',@RedesignNumbers) > 0 
						BEGIN
							SET @RedesignNumbers = REPLACE(@RedesignNumbers, ',', ''',''')
							SET @Where 	+=	' AND r.REDSGN_NBR IN (''' + @RedesignNumbers + ''') '	
							
						END
				  --Else if we have a wildcard
				  ELSE IF CHARINDEX('%',@RedesignNumbers) > 0
						BEGIN
							SET @Where	+=	' AND r.REDSGN_NBR LIKE (''' + @RedesignNumbers + ''') '
							
						END
						
				  ELSE
					BEGIN
							SET @Where	+=	' AND r.REDSGN_NBR IN (''' + @RedesignNumbers + ''') '
					END
             END
			
			---------------------------------------------------------------
			--	EventID Filter  ----Added by Monir  02102017
			---------------------------------------------------------------	
			IF LEN(@EventID) > 0 AND  @EventID <> 0--- IS NOT NULL
					BEGIN
						SET @Where	+=	' AND rdh.Event_ID LIKE (''%' +  CONVERT(VARCHAR,@EventID) + '%'') AND rd.REC_STUS_ID = 1 '
						SET @IsFilter = 1
					END	
			---------------------------------------------------------------
			--	Customer Name Filter 
			---------------------------------------------------------------
			IF	@CustomerNames	<>	''
				BEGIN
					SET @Where	+=	' AND CHARINDEX(''' + @CustomerNames + ''', CASE WHEN (r.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE r.CUST_NME END) > 0 '
					SET @IsFilter = 1
				END		
			
			---------------------------------------------------------------
			--	NTE Filter 
			---------------------------------------------------------------
			IF @NTE	<> ''
				BEGIN
					SET @Where	+=	' AND lut.DSPL_NME LIKE (''%' + @NTE + '%'') '
					SET @IsFilter = 1
				END
              
            ---------------------------------------------------------------
			--	ODIE Device Filter 
			---------------------------------------------------------------
			IF    @ODIEDevices <> ''
				BEGIN
					SET @Where	+=	' AND rd.DEV_NME LIKE (''%' + @ODIEDevices + '%'') AND rd.REC_STUS_ID = 1 '
					SET @IsFilter = 1
				END
			
			---------------------------------------------------------------
			--	PM Filter 
			---------------------------------------------------------------	
			IF    @PM <> ''
				BEGIN
					SET @Where	+=	' AND lup.DSPL_NME LIKE (''%' + @PM + '%'') AND rd.REC_STUS_ID = 1 '
					SET @IsFilter = 1
				END
				
			---------------------------------------------------------------
			--	H1 Filter 
			---------------------------------------------------------------	
			IF    @H1 <> ''
				BEGIN
					SET @Where	+=	' AND r.H1_CD LIKE (''%' + @H1 + '%'') '
					SET @IsFilter = 1
				END
				
			---------------------------------------------------------------
			--	Redesign Category Filter 
			---------------------------------------------------------------	
			IF LEN(@RedesignCat) > 0 AND  @RedesignCat <> 0--- IS NOT NULL
				BEGIN
					SET @Where	+=	' AND r.REDSGN_CAT_ID IN (' + CONVERT(VARCHAR,@RedesignCat) + ') '
					SET @IsFilter = 1
				END
			
			---------------------------------------------------------------
			--	Redesign Type Filter 
			---------------------------------------------------------------
			IF	@RedesignType	<>	''
				BEGIN
					SET @Where	+=	' AND lrt.REDSGN_TYPE_NME LIKE (''%' + @RedesignType + '%'') '
					SET @IsFilter = 1
				END
				
			---------------------------------------------------------------
			--	Redesign Status Filter 
			---------------------------------------------------------------	
			IF LEN(@RedesignStus) > 0 AND  @RedesignStus <> 0--- IS NOT NULL
				BEGIN
					SET @Where	+=	' AND r.STUS_ID IN (' + CONVERT(VARCHAR,@RedesignStus) + ') '
					SET @IsFilter = 1
				END
				
				
			---------------------------------------------------------------
			--	MSS SE Filter 
			---------------------------------------------------------------	
			IF    @MSSSE <> ''
				BEGIN
					SET @Where	+=	' AND lum.DSPL_NME LIKE (''%' + @MSSSE + '%'') AND rd.REC_STUS_ID = 1 '
					SET @IsFilter = 1
				END
				
			---------------------------------------------------------------
			--	NE Filter 
			---------------------------------------------------------------	
			IF    @NE <> ''
				BEGIN
					SET @Where	+=	' AND lun.DSPL_NME LIKE (''%' + @NE + '%'') AND rd.REC_STUS_ID = 1 '
					SET @IsFilter = 1
				END

			---------------------------------------------------------------
			--	Submission Date Filter 
			---------------------------------------------------------------	
			IF @SubmittedDate IS NOT NULL
					BEGIN
						SET @Where	+=	' AND CONVERT(DATE,r.SUBMIT_DT) = ''' + CONVERT(VARCHAR,@SubmittedDate) + ''''
						SET @IsFilter = 1
					END

			---------------------------------------------------------------
			--	SLA Due Date Filter 
			---------------------------------------------------------------	
			IF @SLADueDate IS NOT NULL
					BEGIN
						SET @Where	+=	' AND CONVERT(DATE,r.SLA_DT) = ''' + CONVERT(VARCHAR,@SLADueDate) + ''''
						SET @IsFilter = 1
					END

			---------------------------------------------------------------
			--	Expiration Date Filter 
			---------------------------------------------------------------	
			IF @ExpirationDate IS NOT NULL
					BEGIN
						SET @Where	+=	' AND CONVERT(DATE,r.EXPRTN_DT) = ''' + CONVERT(VARCHAR,@ExpirationDate) + ''''
						SET @IsFilter = 1
					END

			
			---------------------------------------------------------------
			--	Status Filter 
			---------------------------------------------------------------	
			IF @Status <> ''
				BEGIN
					IF (@Status = '1')
					BEGIN
						SET @Where	+=	' AND (r.STUS_ID = 227) AND (r.CRETD_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @IsFilter = 1
					END
					ELSE
					BEGIN
						SET @Where	+=	' AND (r.STUS_ID <> 227) AND (r.CRETD_DT >= '''+ CONVERT(VARCHAR(101), DATEADD(day, -90, GETDATE())) + ''')'
						SET @IsFilter = 1
					END
				END
					


			SET @SQL	=		@DropTbl + 	@CrtTbl	+ @InsrtTbl +	@Select	 + @From	+	@Where	+ @OrdrBy
						+	@FinalSel +  ' WHERE ((CSG_LVL_ID = 0) OR (('+CONVERT(CHAR,@CSGLvlId)+'!=0) AND (CSG_LVL_ID >= '+CONVERT(CHAR,@CSGLvlId)+'))) ORDER BY REDSGN_ID DESC'
		

            IF @IsFilter = 1
				BEGIN
					--select @SQL
					EXEC sp_executesql @SQL
				END
			END
		
END TRY

BEGIN CATCH
--	EXEC [dbo].[insertErrorInfo]
END CATCH
END
