USE [COWS]
GO
_CreateObject 'SP','dbo','GetCPTAssignmentUsers'
GO
USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[GetCPTPooledUsers]    Script Date: 09/27/2015 22:05:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:	qi931353
-- Create date: 09/27/2015
-- Description:	Get pooled users for CPT Assignment
-- =========================================================
ALTER PROCEDURE [dbo].[GetCPTAssignmentUsers]
@ROLEID INT
AS
BEGIN
SET NOCOUNT ON;
Begin Try
		
	SELECT usr.USER_ID, usr.FULL_NME, usr.EMAIL_ADR,
		usr.CPT_QTY, grpRole.GRP_ID, grpRole.POOL_CD,
		SUM(cpt.DEV_CNT) as DEVICES, COUNT(cpt.CPT_CUST_NBR) as TASKS
	FROM LK_USER usr
	JOIN USER_GRP_ROLE grpRole
	ON (grpRole.USER_ID = usr.USER_ID AND grpRole.ROLE_ID = 152 
		AND grpRole.REC_STUS_ID = 1)
	LEFT JOIN CPT_USER_ASMT cptusr
	ON (cptusr.CPT_USER_ID = usr.USER_ID AND cptusr.ROLE_ID = grpRole.ROLE_ID 
		AND cptusr.REC_STUS_ID = 1)
	LEFT JOIN CPT cpt
	ON (cpt.CPT_ID = cptusr.CPT_ID AND cpt.REC_STUS_ID = 1 
		AND (CPT_STUS_ID != 308 OR CPT_STUS_ID != 309) 
		AND DATEADD(day, 30, cpt.CREAT_DT) > GETDATE())
	WHERE usr.REC_STUS_ID = 1
	AND (usr.USER_ID != 1 OR usr.USER_ID != 3) -- COWS System or NTE Pool
	GROUP BY usr.USER_ID, usr.FULL_NME, usr.EMAIL_ADR, usr.CPT_QTY, grpRole.GRP_ID, grpRole.POOL_CD
	ORDER BY usr.CPT_QTY, usr.FULL_NME

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
