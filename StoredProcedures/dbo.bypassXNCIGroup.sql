USE [COWS]
GO
_CreateObject 'SP','dbo','bypassXNCIGroup'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <03/21/2013>
-- Description:	<This SP will bypass xnci workgroup 
--	for domestic orders entered with INTL product types>
-- =============================================
ALTER PROCEDURE dbo.bypassXNCIGroup
@OrderID	Int,
@TaskID		SmallInt,
@TaskStatus	TinyInt,
@Comments	Varchar(1000) = NULL
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @Notes Varchar(1000) = ''
	IF @TaskID != 0
		BEGIN
			SELECT @Notes = 'bypassing ' + TASK_NME + ' milestone task.'
				FROM dbo.LK_TASK WITH (NOLOCK)
				WHERE TASK_ID = @TaskID
		END
    BEGIN TRY
		IF EXISTS
			(
				SELECT 'X'
					FROM	dbo.FSA_ORDR	fo	WITH (NOLOCK)
				INNER JOIN	dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
					WHERE	fo.ORDR_ID				=	@OrderID
						AND	fo.PROD_TYPE_CD			IN	('MP','DN')
						AND	ISNULL(oa.CTRY_CD,'')	IN	('GU','MP','PR','VI','US')
			)
			BEGIN
				EXEC dbo.CompleteActiveTask @OrderID,@TaskID,2,@Notes,1
			END
    END TRY
    BEGIN CATCH
		EXEC dbo.insertErrorInfo
    END CATCH
END
GO
