USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertSSTATReq]    Script Date: 09/06/2018 14:09:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 10/05/2015
-- Description:	Inserts an SSTAT Response into the SSTAT_REQ table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertSSTATReq]
		@ORDR_ID			INT
       ,@MSG_ID				INT
       ,@FTN				VARCHAR(50)
AS

BEGIN
SET NOCOUNT ON;

BEGIN TRY

		DECLARE @PRNT_ORDR_ID	INT
		DECLARE @PRNT_FTN		Varchar(50)
		DECLARE @ORDR_TYPE_CD   Varchar(2)       
		
		SELECT @PRNT_FTN = ISNULL(PRNT_FTN,''), @ORDR_TYPE_CD = ORDR_TYPE_CD
				 FROM [dbo].[FSA_ORDR] WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID
		
		IF @PRNT_FTN <> '' AND @ORDR_TYPE_CD = 'CN'
			BEGIN	
				SELECT @PRNT_ORDR_ID = ORDR_ID 
					FROM [dbo].[FSA_ORDR] WITH (NOLOCK) WHERE FTN = @PRNT_FTN
					 
				IF EXISTS (SELECT ORDR_ID FROM [dbo].[SSTAT_REQ] WITH (NOLOCK) WHERE ORDR_ID = @PRNT_ORDR_ID)
					BEGIN 
						INSERT INTO [dbo].[SSTAT_REQ]
						   ([ORDR_ID],[SSTAT_MSG_ID],[STUS_MOD_DT],[CREAT_DT],[STUS_ID],[FTN])
						VALUES
						   (@ORDR_ID,@MSG_ID,NULL,GETDATE(),10,@FTN)
					END
			END	
			
		 ELSE IF (@MSG_ID in (2,3,4,5) 
						AND @ORDR_ID IN (SELECT ORDR_ID FROM [dbo].[SSTAT_REQ] WITH (NOLOCK) 
													WHERE ORDR_ID = @ORDR_ID AND  SSTAT_MSG_ID = 1))
					BEGIN					
						INSERT INTO [dbo].[SSTAT_REQ]
						   ([ORDR_ID],[SSTAT_MSG_ID],[STUS_MOD_DT],[CREAT_DT],[STUS_ID],[FTN])
						VALUES
						   (@ORDR_ID,@MSG_ID,NULL,GETDATE(),10,@FTN)
					END
			   ELSE IF @MSG_ID in (1) AND 
						EXISTS (SELECT ORDR_ID FROM ORDR WITH (NOLOCK)
										WHERE CPE_CLLI in (SELECT CLLI_Code FROM V_V5U_SSTAT_INFO
															 WHERE VENDOR_COMPANY in ('Ericsson','Sprint'))
							AND ORDR_ID = @ORDR_ID
							AND ISNULL(DLVY_CLLI,'') not in ('MTLDFLTBACP','DLLSTX53ACP','OVPKKSTDACP','RVSDMO09ACP'))
							AND @ORDR_ID NOT IN (SELECT ORDR_ID FROM [dbo].[SSTAT_REQ] WITH (NOLOCK) 
													WHERE ORDR_ID = @ORDR_ID AND  SSTAT_MSG_ID = 1
															AND STUS_ID IN (9,10,11))
							
									BEGIN
											INSERT INTO [dbo].[SSTAT_REQ]
											   ([ORDR_ID],[SSTAT_MSG_ID],[STUS_MOD_DT],[CREAT_DT],[STUS_ID],[FTN])
											VALUES
											   (@ORDR_ID,@MSG_ID,NULL,GETDATE(),10,@FTN)
									END
						
						 
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
	return 1
END CATCH
END
