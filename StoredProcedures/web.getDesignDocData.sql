USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getDesignDocData]    Script Date: 1/18/2022 2:50:41 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  jrg7298            
-- Create date: 11/24/2018       
-- Description: Get the Design data from NRMBPM using H6/DesignDoc#/NUA        
--@VASCEFlg => 0:Network,1:VAS,2:Network+VAS,3:Network+CE,4:VAS+CE,5:Network+CE+VAS  
-- =============================================
-- Modified By:		qi931353
-- Modified Date:	04/06/2021
-- Description:		Removed the following columns upon changes made on BPM View (V_BPMF_COWS_ACTV_EVENT)
--					VRF_NME, ROUTG_TYPE_NME, FUT_PIM_BSR_BORDER_NME, MULTPATH_SRC_NME
--					MULTCST_IPV4_ADR, MULTCST_IPV6_ADR, DET_ROUTG_COMM_STRNG_NME
-- jbolano15 - 06/30/2021 - Added H1_CUST_NME on #M5CANDTable; H1_CUST_NME on @ordr
-- JCarana1	 - 10/29/2021 - Added ITM_STUS = 401 filter
-- Jagan	 - 11/01/2021 - Making sure there is no latest cpe order with NID for this H6
-- jbolano15 - 11/02/2021 - Use base FTN for FTN with -DC suffix
-- jbolano15 - 11/02/2021 - Use H6 for getting data from Mach5 instead of M5_ORDR_NBR
-- jbolano15 - 11/03/2021 - Filter out "Complete" and "Cancelled" on BPMF_OWNER.V_BPMF_COWS_ACTV_EVENT, BPMF_OWNER.V_BPMF_VAS_COWS_ACTV_EVENT,
--							BPMF_OWNER.V_BPMF_CE_COWS_ACTV_EVENT, BPMF_OWNER.V_BPMF_COWS_REFLAG_ACTV_EVENT
-- jbolano15 - 11/12/2021 - Fixed VAS+CE issue via removing @VASCEFlg = 4 condition
-- jbolano15 - 11/15/2021 - Allow H6 value for CE Change
-- jbolano15 - 11/23/2021 - Added "Installed" filter on BPMF_OWNER.V_BPMF_COWS_ACTV_EVENT, BPMF_OWNER.V_BPMF_VAS_COWS_ACTV_EVENT,
--							BPMF_OWNER.V_BPMF_CE_COWS_ACTV_EVENT, BPMF_OWNER.V_BPMF_COWS_REFLAG_ACTV_EVENT
-- =============================================            
ALTER PROCEDURE [web].[getDesignDocData] --exec web.getDesignDocData '928551588', '', 3, 0, 0
 @H6  VARCHAR(max),
 @DD VARCHAR(100) = '',
 @VASCEFlg TINYINT = 0,
 @CEChngFlg	  BIT = 0,
 @Rows INT = 0
AS            
BEGIN            
SET NOCOUNT ON;
BEGIN TRY

IF OBJECT_ID(N'tempdb..#M5CANDTable', N'U') IS NOT NULL         
	DROP TABLE #M5CANDTable 
CREATE TABLE #M5CANDTable
 (M5_ORDR_ID   VARCHAR(20)   NOT NULL
 ,ORDR_ID INT NOT NULL 
 ,CHARS_ID VARCHAR(9) NULL
 ,SITE_ID VARCHAR(50) NULL
 ,CCD VARCHAR(20) NULL
 ,H1 VARCHAR(9) NULL
 ,H1_CUST_NME VARCHAR(255) NULL
 ,H6 VARCHAR(9) NULL
 ,CUST_NME	VARCHAR(255) NULL
 ,CUST_CNTCT_NME VARCHAR(511) NULL
 ,PHN_NBR	VARCHAR(30) NULL
 ,EMAIL_ADR VARCHAR(255) NULL
 ,DESIGN_DOC_NBR VARCHAR(20) NULL
 ,CKT_ID VARCHAR(9) NULL
 ,CSG_LVL VARCHAR(5) NULL
 ,CUST_ACCT_ID INT NULL
 ,[ISD_CD] [varchar](3) NULL
 ,[PHN_EXT_NBR] [varchar](10) NULL
 ,[ROLE_CD] VARCHAR(10) NULL)

declare @H6s varchar(max), @CEServiceIDs varchar(max), @ordr nvarchar(max)
IF OBJECT_ID(N'tempdb..#h6tbl', N'U') IS NOT NULL         
	DROP TABLE #h6tbl 
	IF OBJECT_ID(N'tempdb..#cesrvctbl', N'U') IS NOT NULL         
	DROP TABLE #cesrvctbl 
create table #h6tbl (h6 char(9))
create table #cesrvctbl (cesrvcid varchar(511))

if (@CEChngFlg = 1)
begin
insert into #cesrvctbl
select * from web.ParseCommaSeparatedStrings(@H6)
select @CEServiceIDs = ISNULL(@CEServiceIDs,'') + ISNULL(cesrvcid,'') + ''''', '''''
from (SELECT DISTINCT cesrvcid FROM #cesrvctbl ) x
if(len(@CEServiceIDs)>2)
set @CEServiceIDs = substring(@CEServiceIDs, 1, len(@CEServiceIDs)-6)
-- @CEServiceIDs will either hold CE Service ID or H6
set @ordr = 'INSERT INTO #h6tbl(h6)SELECT H6_ID FROM OPENQUERY(NRMBPM,''select DISTINCT H6_ID from BPMF_OWNER.V_BPMF_CE_COWS_ACTV_EVENT
							where (CE_SRVC_ID IN (''''' + @CEServiceIDs + ''''') OR H6_ID IN (''''' + @CEServiceIDs + ''''')) AND 
							((ACTY_TYPE_NME = ''''Change'''') OR (ACTY_TYPE_NME LIKE ''''%Redesign%'''') OR (ACTY_TYPE_NME LIKE ''''%Install%'''') OR (ACTY_TYPE_NME LIKE ''''%Disconnect%'''')) AND TASK_STUS_NME != ''''Cancelled'''' '')'
							
exec sp_executesql @ordr
end
else
begin
insert into #h6tbl
select * from web.ParseCommaSeparatedStrings(@H6)
end

select @H6s = ISNULL(@H6s,'') + ISNULL(H6,'') + ''''', '''''
from (SELECT DISTINCT H6 FROM #h6tbl ) x
if(len(@H6s)>2)
set @H6s = substring(@H6s, 1, len(@H6s)-6)


set @ordr = 'INSERT INTO #M5CANDTable (M5_ORDR_ID, ORDR_ID, CHARS_ID, SITE_ID, H1, H1_CUST_NME, H6, CUST_NME, CUST_CNTCT_NME, PHN_NBR, EMAIL_ADR,
								CSG_LVL, CUST_ACCT_ID,ISD_CD,PHN_EXT_NBR,ROLE_CD) Select DISTINCT
							'''' AS ORDER_NBR, -1 AS ORDR_ID, CHARS_CUST_ID, SITE_ID, H1, H1_CUST_NME, H6, CUST_NME, CUST_CNTCT_NME, PHN_NBR, EMAIL_ADR,
								CSG_LVL, CUST_ACCT_ID, ISD_CD, PHN_EXT_NBR, ROLE_CD from openquery(M5,''select DISTINCT 
								vbca.CHARS_CUST_ID, vbca.SITE_ID, vbca.H1, b.CUST_NME AS H1_CUST_NME, vbca.H6, vbca.CUST_NME, vbcc.CUST_NME as CUST_CNTCT_NME,
								CASE	WHEN vbcc.DOM_INTL_IND_CD = ''''D'''' THEN vbcc.DOM_PHN_NBR
															WHEN vbcc.DOM_INTL_IND_CD = ''''I'''' THEN vbcc.INTL_PHN_NBR   
													END AS  PHN_NBR,
								vbcc.EMAIL_ADR, 
								vbca.CSG_LVL, vbca.CUST_ACCT_ID,vbcc.INTL_CNTRY_DIAL_CD AS ISD_CD,			
									vbcc.PHN_EXT AS PHN_EXT_NBR, vbcc.ROLE_CD
							from
							MACH5.V_V5U_CUST_CONTACT vbcc,
							MACH5.V_V5U_CUST_ACCT vbca
								INNER JOIN MACH5.V_V5U_CUST_ACCT b ON b.CIS_CUST_ID = vbca.H1 AND b.CIS_HIER_LVL_CD = ''''H1''''
							where vbca.CIS_HIER_LVL_CD IN (''''H5'''', ''''H6'''') 
							and ((vbca.TYPE_CD = DECODE(vbca.CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL''''))
								or (vbca.TYPE_CD = DECODE(vbca.CIS_HIER_LVL_CD,''''H5'''',''''PHYS'''',''''PHYY'''',''''BILL'''')))
							AND vbca.CUST_ACCT_ID = vbcc.CUST_ACCT_ID
							AND vbcc.ROLE_CD IN (''''CONT'''',''''SVCA'''',''''CONP'''',''''CON2'''')
							AND vbca.CIS_CUST_ID IN (''''' + @H6s + ''''') '')'

exec sp_executesql @ordr

SELECT DISTINCT c.SITE_ID as SITEID, c.H1, c.H1_CUST_NME, c.H6, c.CUST_NME as CustomerName, ISNULL(lcl.CSG_LVL_ID,0) AS CSG_LVL_ID
FROM #M5CANDTable c LEFT JOIN dbo.LK_CSG_LVL lcl WITH (NOLOCK) ON lcl.CSG_LVL_CD=c.CSG_LVL

SELECT DISTINCT c.H6,
c.CUST_CNTCT_NME as CustomerContactName, 
CASE WHEN (LEN(c.ISD_CD)>0 AND LEN(c.PHN_EXT_NBR)>0) THEN c.ISD_CD + '-'+ c.PHN_NBR + '+' + c.PHN_EXT_NBR 
	 WHEN (LEN(c.ISD_CD)>0 AND LEN(c.PHN_EXT_NBR)=0) THEN c.ISD_CD + '-'+ c.PHN_NBR
	 WHEN (LEN(c.ISD_CD)=0 AND LEN(c.PHN_EXT_NBR)>0) THEN c.PHN_NBR + '+' + c.PHN_EXT_NBR  ELSE c.PHN_NBR END as CustomerContactPhone, c.EMAIL_ADR as CustomerEmail, lr.ROLE_NME as RoleNme
FROM #M5CANDTable c INNER JOIN dbo.LK_ROLE lr WITH (NOLOCK) ON c.ROLE_CD = lr.ROLE_CD

declare @nuatbl table (DD_NBR varchar(511) null, H6_H5_ID VARCHAR(511) NULL, NUA_ADR VARCHAR(511) NULL, FTN_NBR VARCHAR(511) NULL)

-- IF (charindex('''',@H6) <=0)
-- BEGIN
-- insert into #h6tbl
-- select * from web.ParseCommaSeparatedStrings(@H6)

-- select @H6s = ISNULL(@H6s,'') + ISNULL(H6,'') + ''''', '''''
-- from (SELECT DISTINCT H6 FROM #h6tbl ) x
-- if(len(@H6s)>2)
-- set @H6s = substring(@H6s, 1, len(@H6s)-6)
-- END

IF OBJECT_ID(N'tempdb..#RltdCEEvents', N'U') IS NOT NULL         
	DROP TABLE #RltdCEEvents

CREATE TABLE #RltdCEEvents
(CE_SRVC_ID VARCHAR(511) NOT NULL,
EVENT_ID INT NOT NULL)

IF OBJECT_ID(N'tempdb..#DesignDocTable', N'U') IS NOT NULL         
	DROP TABLE #DesignDocTable 

CREATE TABLE #DesignDocTable
(DDTblID INT IDENTITY(1,1) NOT NULL,
DD_NBR VARCHAR(511) NULL,
ACCT_ROLE_NME VARCHAR(511) NULL,
WRK_PHN_NBR VARCHAR(511) NULL,
EMAIL_ADR VARCHAR(511) NULL,
PROD_ID VARCHAR(511) NULL,
ACCS_VNDR_NME VARCHAR(511) NULL,
FTN_NBR VARCHAR(511) NULL,
CUST_NME VARCHAR(511) NULL,
SRVC_ORDR_STUS_NME VARCHAR(511) NULL,
H6_H5_ID VARCHAR(511) NULL,
BDWD_NME VARCHAR(511) NULL,
SCA_NBR VARCHAR(511) NULL,
CUST_ROUTR_TAG_NME VARCHAR(511) NULL,
CUST_PRVD_IP_NME VARCHAR(511) NULL,
VLAN_ID VARCHAR(511) NULL,
VRF_NME VARCHAR(511) NULL,
ROUTG_TYPE_NME VARCHAR(511) NULL,
ADR_FMLY_TYPE_NME VARCHAR(511) NULL,
COS_CD VARCHAR(511) NULL,
MGT_RTE_TRGT_NME VARCHAR(511) NULL,
FUT_PIM_BSR_BORDER_NME VARCHAR(511) NULL,
MULTPATH_SRC_NME VARCHAR(511) NULL,
MULTCST_IPV4_ADR VARCHAR(511) NULL,
MULTCST_IPV6_ADR VARCHAR(511) NULL,
DET_ROUTG_COMM_STRNG_NME VARCHAR(511) NULL,
GRP_NME VARCHAR(511) NULL,
NUA_ADR VARCHAR(511) NULL,
LOC_CITY VARCHAR(511) NULL,
LOC_STT VARCHAR(511) NULL,
LOC_CTRY VARCHAR(511) NULL,
TASK_ID VARCHAR(100) NULL,
TASK_NME VARCHAR(255) NULL,
DMSTC_CD VARCHAR(10) NULL,
SUB_TYPE VARCHAR(100) NULL,
VAS_CD BIT NULL,
CE_SRVC_ID VARCHAR(511) NULL,
NID_HOST_NME VARCHAR(511) NULL,
NID_SERIAL_NBR VARCHAR(100) NULL,
NID_IP_ADR VARCHAR(15) NULL
)

CREATE CLUSTERED INDEX IDX_C_Temp_DDTbl ON #DesignDocTable(DDTblID)   
CREATE INDEX IDX_ARID_Temp_DDTbl ON #DesignDocTable(FTN_NBR)

declare @ddtbl nvarchar(max) = 'INSERT INTO  #DesignDocTable (DD_NBR,
ACCT_ROLE_NME,
WRK_PHN_NBR,
EMAIL_ADR,
PROD_ID,
ACCS_VNDR_NME,
FTN_NBR,
CUST_NME,
SRVC_ORDR_STUS_NME,
H6_H5_ID,
BDWD_NME,
SCA_NBR,
CUST_ROUTR_TAG_NME,
CUST_PRVD_IP_NME,
VLAN_ID,
ADR_FMLY_TYPE_NME,
COS_CD,
MGT_RTE_TRGT_NME,
GRP_NME,
NUA_ADR,
LOC_CITY,
LOC_STT,
LOC_CTRY,
TASK_ID,
TASK_NME,
DMSTC_CD,
SUB_TYPE,
VAS_CD,
CE_SRVC_ID,
NID_HOST_NME,
NID_SERIAL_NBR
)
Select DD_NBR,
ACCT_ROLE_NME,
WRK_PHN_NBR,
EMAIL_ADR,
PROD_ID,
ACCS_VNDR_NME,
FTN_NBR,
CUST_NME,
SRVC_ORDR_STUS_NME,
H6_H5_ID,
BDWD_NME,
SCA_NBR,
CUST_ROUTR_TAG_NME,
CUST_PRVD_IP_NME,
VLAN_ID,
ADR_FMLY_TYPE_NME,
COS_CD,
MGT_RTE_TRGT_NME,
ISNULL(GRP_NME,''SPRINT'') AS GRP_NME,
NUA_ADR,'''','''','''',TASK_ID,
QUEUE_NME,
DMSTC_CD,SUB_TYPE, VAS_CD,CE_SRVC_ID,NID_HOST_NME,''''   from openquery(NRMBPM,'' '

IF ((@VASCEFlg=0) OR (@VASCEFlg=2) OR (@VASCEFlg=3) OR (@VASCEFlg=5))
BEGIN
set  @ddtbl = @ddtbl + 'select DISTINCT DD_NBR,
ACCT_ROLE_NME,
WRK_PHN_NBR,
EMAIL_ADR,
PROD_ID,
ACCS_VNDR_NME,
FTN_NBR,
CUST_NME,
SRVC_ORDR_STUS_NME,
H6_H5_ID,
BDWD_NME,
SCA_NBR,
CUST_ROUTR_TAG_NME,
CUST_PRVD_IP_NME,
VLAN_ID,
ADR_FMLY_TYPE_NME,
COS_CD,
MGT_RTE_TRGT_NME,
''''SPRINT'''' AS GRP_NME,
NUA_ADR,
TASK_ID,
QUEUE_NME,
DMSTC_CD,
'''''''' AS SUB_TYPE,0 AS VAS_CD,
'''''''' AS CE_SRVC_ID,'''''''' AS NID_HOST_NME
							from BPMF_OWNER.V_BPMF_COWS_ACTV_EVENT
							where H6_H5_ID IN (''''' + @H6s + ''''')
								AND TASK_STUS_NME NOT IN (''''Complete'''',''''Cancelled'''',''''Installed'''') '

IF (LEN(@DD)>0)
SET @ddtbl = @ddtbl + ' and DD_NBR = ''''' + @DD + ''''' '
END

-- km967761 - 11/12/2021 - Removed @VASCEFlg = 4
IF ((@VASCEFlg=2) OR (@VASCEFlg=5))
SET @ddtbl = @ddtbl + ' UNION '
IF ((@VASCEFlg=1) OR (@VASCEFlg=2) OR (@VASCEFlg=4) OR (@VASCEFlg=5))
BEGIN 
SET @ddtbl = @ddtbl + '
select DISTINCT DD_NBR,
ACCT_ROLE_NME,
WRK_PHN_NBR,
EMAIL_ADR,
PROD_ID,
'''''''' as ACCS_VNDR_NME,
FTN_NBR,
CUST_NME,
SRVC_ORDR_STUS_NME,
H6_H5_ID,
BDWD_NME,
SCA_NBR,
CUST_ROUTR_TAG_NME,
CUST_PRVD_IP_NME,
VLAN_ID,
ADR_FMLY_TYPE_NME,
'''''''' AS COS_CD,
'''''''' as MGT_RTE_TRGT_NME,
''''SPRINT'''' AS GRP_NME,
NUA_ADR,
TASK_ID,
QUEUE_NME,
DMSTC_CD,
SUB_TYPE,1 AS VAS_CD,
'''''''' AS CE_SRVC_ID,'''''''' AS NID_HOST_NME
							from BPMF_OWNER.V_BPMF_VAS_COWS_ACTV_EVENT
							where H6_H5_ID IN (''''' + @H6s + ''''')
								AND TASK_STUS_NME NOT IN (''''Complete'''',''''Cancelled'''',''''Installed'''') '
IF (LEN(@DD)>0)
SET @ddtbl = @ddtbl + ' and DD_NBR = ''''' + @DD + ''''' '
END

IF ((@VASCEFlg=3) OR (@VASCEFlg=4) OR (@VASCEFlg=5))
SET @ddtbl = @ddtbl + ' UNION '
IF ((@VASCEFlg=3) OR (@VASCEFlg=4) OR (@VASCEFlg=5))
BEGIN
SET @ddtbl = @ddtbl + '
select DISTINCT DD_NBR,
ACCT_ROLE_NME,
WRK_PHN_NBR,
EMAIL_ADR,
PROD_TYPE_NME as PROD_ID,
ACCS_VNDR_NME,
FTN_NBR,
CUST_NME,
'''''''' AS SRVC_ORDR_STUS_NME,
H6_ID as H6_H5_ID,
BDWD_NME,
SCA_NBR,
CUST_ROUTR_TAG_NME,
'''''''' AS CUST_PRVD_IP_NME,
'''''''' AS VLAN_ID,
'''''''' AS ADR_FMLY_TYPE_NME,
'''''''' AS COS_CD,
'''''''' AS MGT_RTE_TRGT_NME,
''''SPRINT'''' AS GRP_NME,
NUA_ADR,
TASK_ID,
QUEUE_NME,
'''''''' AS DMSTC_CD,
'''''''' AS SUB_TYPE,0 AS VAS_CD,CE_SRVC_ID,NID_HOST_NME
							from BPMF_OWNER.V_BPMF_CE_COWS_ACTV_EVENT
							where
								TASK_STUS_NME NOT IN (''''Complete'''',''''Cancelled'''',''''Installed'''') AND '
IF (@CEChngFlg = 1)
	SET @ddtbl = @ddtbl + ' (CE_SRVC_ID IN (''''' + @CEServiceIDs + ''''') OR H6_ID IN (''''' + @CEServiceIDs + ''''')) AND ((ACTY_TYPE_NME = ''''Change'''') OR (ACTY_TYPE_NME LIKE ''''%Redesign%'''') OR (ACTY_TYPE_NME LIKE ''''%Install%'''') OR (ACTY_TYPE_NME LIKE ''''%Disconnect%''''))'
ELSE 
	SET @ddtbl = @ddtbl + ' H6_ID IN (''''' + @H6s + ''''') '
IF (LEN(@DD)>0)
SET @ddtbl = @ddtbl + ' and DD_NBR = ''''' + @DD + ''''' '
END
							
SET @ddtbl = @ddtbl + '	'')'
--select @ddtbl
exec sp_executesql @ddtbl

IF NOT EXISTS(SELECT 'X' FROM #DesignDocTable)
BEGIN
set @ddtbl = 'INSERT INTO  #DesignDocTable (DD_NBR,
ACCT_ROLE_NME,
WRK_PHN_NBR,
EMAIL_ADR,
PROD_ID,
ACCS_VNDR_NME,
FTN_NBR,
CUST_NME,
SRVC_ORDR_STUS_NME,
H6_H5_ID,
BDWD_NME,
SCA_NBR,
CUST_ROUTR_TAG_NME,
CUST_PRVD_IP_NME,
VLAN_ID,
ADR_FMLY_TYPE_NME,
COS_CD,
MGT_RTE_TRGT_NME,
GRP_NME,
NUA_ADR,
LOC_CITY,
LOC_STT,
LOC_CTRY,
TASK_ID,
TASK_NME,
DMSTC_CD,
SUB_TYPE,
VAS_CD,
CE_SRVC_ID,
NID_HOST_NME,
NID_SERIAL_NBR
)
Select DD_NBR,
ACCT_ROLE_NME,
WRK_PHN_NBR,
EMAIL_ADR,
PROD_ID,
ACCS_VNDR_NME,
FTN_NBR,
CUST_NME,
SRVC_ORDR_STUS_NME,
H6_H5_ID,
BDWD_NME,
SCA_NBR,
CUST_ROUTR_TAG_NME,
CUST_PRVD_IP_NME,
VLAN_ID,
ADR_FMLY_TYPE_NME,
COS_CD,
MGT_RTE_TRGT_NME,
ISNULL(GRP_NME,''SPRINT'') AS GRP_NME,
NUA_ADR,'''','''','''',TASK_ID,
QUEUE_NME,
DMSTC_CD,SUB_TYPE, VAS_CD,CE_SRVC_ID,NID_HOST_NME,''''   from openquery(NRMBPM,''select DISTINCT DD_NBR,
ACCT_ROLE_NME,
WRK_PHN_NBR,
EMAIL_ADR,
PROD_ID,
'''''''' AS ACCS_VNDR_NME,
FTN_NBR,
CUST_NME,
SRVC_ORDR_STUS_NME,
H6_H5_ID,
BDWD_NME,
SCA_NBR,
CUST_ROUTR_TAG_NME,
CUST_PRVD_IP_NME,
'''''''' AS VLAN_ID,
'''''''' AS ADR_FMLY_TYPE_NME,
'''''''' AS COS_CD,
'''''''' AS MGT_RTE_TRGT_NME,
''''SPRINT'''' AS GRP_NME,
NUA_ADR,
TASK_ID,
QUEUE_NME,
DMSTC_CD,
'''''''' AS SUB_TYPE,0 AS VAS_CD,
'''''''' AS CE_SRVC_ID,'''''''' AS NID_HOST_NME
							from BPMF_OWNER.V_BPMF_COWS_REFLAG_ACTV_EVENT
							where H6_H5_ID IN (''''' + @H6s + ''''') 
								AND TASK_STUS_NME NOT IN (''''Complete'''',''''Cancelled'''',''''Installed'''') '

IF (LEN(@DD)>0)
SET @ddtbl = @ddtbl + ' and DD_NBR = ''''' + @DD + ''''' '

SET @ddtbl = @ddtbl + '	'')'
--select @ddtbl
exec sp_executesql @ddtbl
END

set @ddtbl = 'update dd
set LOC_CITY = x.cty_nme,
LOC_STT = x.st_prvn_cd,
LOC_CTRY = x.ctry_cd
from #DesignDocTable dd 

inner join (select * from openquery(m5,  ''Select DISTINCT 
					CIS_CUST_ID,CTRY_CD,CTY_NME,ST_PRVN_CD
				From Mach5.V_V5U_CUST_ACCT
				Where
					CIS_CUST_ID IN (''''' + CONVERT(VARCHAR,@H6s) + ''''')
					AND TYPE_CD = DECODE(CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')'')
			) as x on dd.H6_H5_ID=x.CIS_CUST_ID'

exec sp_executesql @ddtbl

set @ddtbl = 'update dd set
	EMAIL_ADR = x.EMAIL_ADR,
	WRK_PHN_NBR = x.DOM_PHN_NBR
from #DesignDocTable dd
	inner join (select * from openquery(m5,  ''Select DISTINCT 
			vvca.CIS_CUST_ID,vvat.EMAIL_ADR,vvat.DOM_PHN_NBR
		From Mach5.V_V5U_CUST_ACCT vvca
			LEFT JOIN Mach5.V_V5U_ACCT_TEAM_CONTACT vvat ON vvat.CUST_ACCT_ID = vvca.CUST_ACCT_ID AND vvat.ROLE_CD = ''''SE''''
		Where
			CIS_CUST_ID IN (''''' + CONVERT(VARCHAR,@H6s) + ''''')
			AND TYPE_CD = DECODE(CIS_HIER_LVL_CD,''''H6'''',''''PHYS'''',''''PHYY'''',''''BILL'''')'')
	) as x on dd.H6_H5_ID=x.CIS_CUST_ID
WHERE ((ISNULL(dd.EMAIL_ADR,'''')=''''
	OR ISNULL(dd.WRK_PHN_NBR,'''')='''')
	AND (ISNULL(x.EMAIL_ADR,'''')!='''')
	AND (ISNULL(x.DOM_PHN_NBR,'''')!='''')) '

		
exec sp_executesql @ddtbl


SET @H6s = REPLACE(@H6s, '''''','''')
set @ddtbl = 'update dd
set NID_SERIAL_NBR = x.NID_SERIAL_NBR,
NID_IP_ADR = x.IP_ADR
from #DesignDocTable dd with (nolock) inner join 
(select distinct ord.H5_H6_CUST_ID, nac.NID_SERIAL_NBR, ims.IP_ADR
FROM dbo.ORDR ord WITH (NOLOCK) inner join
dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON fc.ORDR_ID=ord.ORDR_ID AND fc.CMPNT_FMLY=''NID'' AND fc.SPRINT_MNTD_FLG=''Y'' AND FC.ITM_STUS = 401 inner join
dbo.NID_ACTY nac WITH (NOLOCK) ON nac.FSA_CPE_LINE_ITEM_ID = fc.FSA_CPE_LINE_ITEM_ID AND nac.REC_STUS_ID=251 inner join
dbo.IP_ACTY iac WITH (NOLOCK) ON nac.NID_ACTY_ID = iac.NID_ACTY_ID AND iac.REC_STUS_ID=251 inner join
dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID = iac.IP_MSTR_ID AND ims.REC_STUS_ID = 253
WHERE convert(varchar,ord.H5_H6_CUST_ID) IN (''' + @H6s + ''')
and not exists (select ''x'' from dbo.ordr ord2 with (nolock) inner join 
dbo.FSA_ORDR_CPE_LINE_ITEM fcl2 with (nolock) on fcl2.ORDR_ID=ord2.ORDR_ID AND fcl2.CMPNT_FMLY=''NID'' AND fcl2.SPRINT_MNTD_FLG=''Y'' AND fcl2.ITM_STUS = 401 
where convert(varchar,ord2.H5_H6_CUST_ID) IN (''' + @H6s + ''') and ord2.ordr_id>ord.ordr_id )) as x on x.H5_H6_CUST_ID=dd.H6_H5_ID ' --Making sure there is no latest cpe order with NID for this H6

exec sp_executesql @ddtbl

INSERT INTO @nuatbl (DD_NBR, H6_H5_ID, NUA_ADR, FTN_NBR)
SELECT DISTINCT DD_NBR, H6_H5_ID, NUA_ADR, FTN_NBR
FROM #DesignDocTable WITH (NOLOCK)

UPDATE dd
SET NUA_ADR = (select ltrim(rtrim(substring(
			(
				Select CASE WHEN ISNULL(nu.NUA_ADR,'') <> '' THEN ','+nu.NUA_ADR END  AS [text()]
				From @nuatbl nu
				Where nu.DD_NBR = dd.DD_NBR
				  AND nu.H6_H5_ID = dd.H6_H5_ID
				  AND nu.FTN_NBR = dd.FTN_NBR
				ORDER BY nu.DD_NBR, nu.H6_H5_ID, nu.NUA_ADR
				For XML PATH ('')
			), 2, 8000))))
FROM #DesignDocTable dd

IF ((@Rows=0) AND (LEN(@DD)>0))
SELECT DISTINCT x.*, (SELECT TOP 1 CUST_NME FROM #DesignDocTable WHERE H6_H5_ID=x.H6_H5_ID) AS CUST_NME FROM
(SELECT DISTINCT DD_NBR, ACCT_ROLE_NME, WRK_PHN_NBR, EMAIL_ADR, PROD_ID, ACCS_VNDR_NME, FTN_NBR, SRVC_ORDR_STUS_NME, H6_H5_ID, SCA_NBR, CUST_ROUTR_TAG_NME, CUST_PRVD_IP_NME, VLAN_ID, VRF_NME, ROUTG_TYPE_NME, ADR_FMLY_TYPE_NME, COS_CD, MGT_RTE_TRGT_NME, FUT_PIM_BSR_BORDER_NME, MULTPATH_SRC_NME, MULTCST_IPV4_ADR, MULTCST_IPV6_ADR, DET_ROUTG_COMM_STRNG_NME, GRP_NME, NUA_ADR, LOC_CITY, ISNULL(LOC_STT,'N/A') as LOC_STT, LOC_CTRY, SUB_TYPE, VAS_CD, CE_SRVC_ID, CASE WHEN LEN(NID_SERIAL_NBR)>0 THEN NID_HOST_NME ELSE NULL END AS NID_HOST_NME, NID_SERIAL_NBR, NID_IP_ADR, BDWD_NME, TASK_ID, TASK_NME, DMSTC_CD FROM #DesignDocTable) as x
ELSE IF (@Rows=0)
SELECT DISTINCT x.*, (SELECT TOP 1 CUST_NME FROM #DesignDocTable WHERE H6_H5_ID=x.H6_H5_ID) AS CUST_NME FROM
(SELECT DISTINCT DD_NBR, ACCT_ROLE_NME, WRK_PHN_NBR, EMAIL_ADR, PROD_ID, ACCS_VNDR_NME, FTN_NBR, H6_H5_ID, SCA_NBR,
VLAN_ID, ADR_FMLY_TYPE_NME, GRP_NME, NUA_ADR, LOC_CITY, ISNULL(LOC_STT,'N/A') as LOC_STT, LOC_CTRY, SUB_TYPE, VAS_CD, CE_SRVC_ID, CASE WHEN LEN(NID_SERIAL_NBR)>0 THEN NID_HOST_NME ELSE NULL END AS NID_HOST_NME, NID_SERIAL_NBR, NID_IP_ADR, BDWD_NME FROM #DesignDocTable) as x
ELSE
SELECT TOP 1 DD_NBR, ACCT_ROLE_NME, WRK_PHN_NBR, EMAIL_ADR, PROD_ID, ACCS_VNDR_NME, FTN_NBR, CUST_NME, H6_H5_ID, SCA_NBR,
VLAN_ID, ADR_FMLY_TYPE_NME, GRP_NME, NUA_ADR, LOC_CITY, ISNULL(LOC_STT,'N/A') as LOC_STT, LOC_CTRY, SUB_TYPE, VAS_CD, CE_SRVC_ID, CASE WHEN LEN(NID_SERIAL_NBR)>0 THEN NID_HOST_NME ELSE NULL END AS NID_HOST_NME, NID_SERIAL_NBR, NID_IP_ADR, BDWD_NME FROM #DesignDocTable


INSERT INTO #RltdCEEvents (CE_SRVC_ID, EVENT_ID)
SELECT DISTINCT dd.CE_SRVC_ID, ment.EVENT_ID
FROM #DesignDocTable dd WITH (NOLOCK) INNER JOIN
dbo.MDS_EVENT_NTWK_TRPT ment WITH (NOLOCK) ON ment.CE_SRVC_ID=dd.CE_SRVC_ID AND ment.REC_STUS_ID=1 INNER JOIN
dbo.MDS_EVENT me WITH (NOLOCK) ON me.EVENT_ID=ment.EVENT_ID
WHERE LEN(dd.CE_SRVC_ID)>0
  AND me.EVENT_STUS_ID != 8

SELECT * FROM #RltdCEEvents

SELECT DISTINCT nac.H6, nac.NID_SERIAL_NBR
FROM #DesignDocTable dd WITH (NOLOCK) INNER JOIN
dbo.NID_ACTY nac WITH (NOLOCK) ON nac.H6=dd.H6_H5_ID INNER join
dbo.IP_ACTY iac WITH (NOLOCK) ON nac.NID_ACTY_ID = iac.NID_ACTY_ID AND iac.REC_STUS_ID = 251 INNER join
dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID = iac.IP_MSTR_ID
WHERE nac.REC_STUS_ID=251
AND ims.REC_STUS_ID IN (251,254)


END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END    
    
