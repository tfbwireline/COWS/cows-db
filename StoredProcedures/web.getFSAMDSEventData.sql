USE [COWS]
GO
_CreateObject 'SP','web','getFSAMDSEventData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  su302037            
-- Create date: 06/30/2012            
-- Description: Get the required data for an MDS Event when a H5/H6 value is entered.   
-- kh946640 03/26/18 COWS DB Restructuring as columns have been moved from fsa_ordr->fsa_ordr_cpe_line_item table for DualPort project(PJ020783 - CR93)         
-- =============================================            
ALTER PROCEDURE [web].[getFSAMDSEventData]             
 @H5_H6_CUST_ID  Varchar(20) = ''                  
 ,@Tab_Seq_Nbr TinyInt            
AS            
BEGIN            
SET NOCOUNT ON            
Begin Try            
            
OPEN SYMMETRIC KEY FS@K3y             
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;            
            
DECLARE @Table TABLE            
 (ORDR_ID   Int   NULL            
 ,PROD_TYPE_CD  VARCHAR(2) NULL            
 ,EVENT_ID   Int   NULL            
 ,FSA_MDS_EVENT_ID Int   NULL)            
            
IF @H5_H6_CUST_ID != 0            
 BEGIN            
  INSERT INTO @Table (ORDR_ID,PROD_TYPE_CD,  EVENT_ID, FSA_MDS_EVENT_ID)            
    SELECT        DISTINCT          od.ORDR_ID, fo.PROD_TYPE_CD, 0,                 0            
                        FROM        dbo.ORDR    od    WITH (NOLOCK)            
                                          INNER JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID            
                                          --INNER JOIN dbo.ACT_TASK at WITH (NOLOCK) on  od.ORDR_ID = at.ORDR_ID            
                        WHERE       fo.ORDR_TYPE_CD NOT IN ('CN', 'DC')            
                                         --AND ((at.TASK_ID = 1000 AND at.STUS_ID = 3)            
                                         -- OR (at.TASK_ID in (300, 301) AND at.STUS_ID = 2))            
                                          AND fo.ordr_actn_id = 2            
                                          AND od.H5_H6_CUST_ID  = @H5_H6_CUST_ID            
                                          AND od.ORDR_STUS_ID = 1            
                                          AND od.ordr_cat_id = 2
										  AND (
												(fo.PROD_TYPE_CD IN ('MN','SE'))
												OR (
													(fo.PROD_TYPE_CD NOT IN ('MN','SE')) 
														AND (
																(ISNULL(fo.CPE_CPE_ORDR_TYPE_CD,'') = 'MNS') 
																OR (ISNULL(fo.TTRPT_MNGD_DATA_SRVC_CD,'') = 'Y')
															)
												   )
												)          
                                          --AND od.ORDR_ID = 2192            
 END            
            
IF (SELECT Count(1) FROM @Table) > 0            
 BEGIN             
  IF @H5_H6_CUST_ID != 0            
   BEGIN            
 -----------------------------------------------------------------------            
    -- Table 00 - ODIE Dev Name.            
    -----------------------------------------------------------------------            
 SELECT DISTINCT @Tab_Seq_Nbr      AS TAB_SEQ_NBR,      
     0      AS FSA_MDS_EVENT_ID,      
     ' '      AS ODIE_DEV_NME,      
     0      AS dtMDSODIENmeInfo      
 FROM @Table tt INNER JOIN       
   dbo.MDS_EVENT_ODIE_DEV_NME AS od WITH (NOLOCK) ON od.FSA_MDS_EVENT_ID = tt.FSA_MDS_EVENT_ID      
       
 -----------------------------------------------------------------------            
    -- Table 01 - Access Info FTN Table.            
    -----------------------------------------------------------------------             
 SELECT DISTINCT @Tab_Seq_Nbr    AS TAB_SEQ_NBR,      
     0       AS FSA_MDS_EVENT_ID,      
     fo.FTN      AS ACCESS_FTN,      
     ISNULL(tt.ORDR_ID, 0)    AS ORDR_ID,      
     0       AS AccessInfoID         
 FROM @Table tt INNER JOIN       
   dbo.FSA_ORDR AS fo WITH (NOLOCK) ON fo.ORDR_ID = tt.ORDR_ID INNER JOIN       
   dbo.ACT_TASK at WITH (NOLOCK) ON at.ORDR_ID = fo.ORDR_ID            
 WHERE fo.PROD_TYPE_CD NOT IN ('MN', 'CP', 'SE')            
   AND fo.ORDR_TYPE_CD in ('IN')      
       
 -----------------------------------------------------------------------            
    -- Table 02 - CPE Info FTN Table.            
    -----------------------------------------------------------------------             
 SELECT DISTINCT @Tab_Seq_Nbr     AS TAB_SEQ_NBR,      
     0        AS FSA_MDS_EVENT_ID,      
     fo.FTN       AS CPE_FTN,     
     ISNULL(tt.ORDR_ID, 0)   AS ORDR_ID,      
     ISNULL(fob.LINE_ITEM_DES,'') AS EQUIP_DESC,            
     --ISNULL(foc.MFR_NME, '')   AS EQUIP_VNDR,    
     '' AS EQUIP_VNDR,        
     0        AS CPEInfoID            
  FROM @Table tt INNER JOIN       
    dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = tt.ORDR_ID INNER JOIN       
    dbo.ORDR  od WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID LEFT JOIN       
    dbo.FSA_ORDR_CPE_LINE_ITEM foc WITH (NOLOCK) ON fo.ORDR_ID = foc.ORDR_ID LEFT JOIN       
    dbo.FSA_ORDR_BILL_LINE_ITEM fob WITH (NOLOCK) ON foc.FSA_CPE_LINE_ITEM_ID = fob.FSA_CPE_LINE_ITEM_ID            
  WHERE  fo.ORDR_TYPE_CD IN ('IN')            
    AND  ( (od.DMSTC_CD = 1 AND fo.PROD_TYPE_CD = 'CP')            
      OR ( od.DMSTC_CD       = 0                
      AND fo.PROD_TYPE_CD      IN ('CP', 'IP')             
      AND ISNULL(fo.CPE_CPE_ORDR_TYPE_CD, '') != '')            
      )            
 -----------------------------------------------------------------------            
    -- Table 03 - MNS Info FTN Table.            
    -----------------------------------------------------------------------        
    SELECT DISTINCT @Tab_Seq_Nbr            AS TAB_SEQ_NBR,      
     0            AS FSA_MDS_EVENT_ID,            
     fo.FTN           AS MDS_MNS_FTN,      
     ISNULL(ls.MDS_SRVC_TIER_DES, ' ')  AS MDS_SRVC_TIER_DES,            
     ISNULL(ls.MDS_SRVC_TIER_ID, 0)   AS MDS_SRVC_TIER_ID,            
     --ISNULL(le.MDS_ENTLMNT_DES, ' ')
	 ' '   AS MDS_ENTLMNT_DES,            
     --ISNULL(oe.MDS_ENTLMNT_ID, 0)
	 0   AS MDS_ENTLMNT_ID,            
     --ISNULL(oe.MDS_ENTLMNT_OE_ID, ' ')
	 ' '  AS MDS_ENTLMNT_OE_ID,            
     0          AS MDSMNSInfoID,            
     ISNULL(tt.ORDR_ID, 0)     AS ORDR_ID            
  FROM  @Table   tt INNER JOIN       
     dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = tt.ORDR_ID LEFT JOIN       
     --dbo.MDS_EVENT_MNS_OE oe WITH (NOLOCK) ON fo.ORDR_ID = oe.ORDR_ID LEFT JOIN       
     --dbo.LK_MNS_OE_TYPE  lm WITH (NOLOCK) ON oe.MNS_OE_TYPE_ID = lm.MNS_OE_TYPE_ID LEFT JOIN       
     dbo.LK_MDS_SRVC_TIER ls WITH (NOLOCK) ON RTRIM(LTRIM(fo.INSTL_SRVC_TIER_CD)) = ls.MDS_SRVC_TIER_ID 
	 --LEFT JOIN       
     --dbo.LK_MDS_ENTLMNT  le WITH (NOLOCK) ON oe.MDS_ENTLMNT_ID = le.MDS_ENTLMNT_ID            
  WHERE  fo.PROD_TYPE_CD IN ('MN', 'SE')            
      AND  fo.ORDR_TYPE_CD IN ('IN')      
       
 -----------------------------------------------------------------------            
    -- Table 04 - Wired Device Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS WdID,            
     ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '') AS WdTransportType,             
     ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0)  AS MDS_TRNSPRT_TYPE_ID,            
     '0'          AS PRIM_BKUP_CD,      
           ''          AS WdPrimaryBackup,      
           ''          AS BDWL_CHNL_NME,                  
     ''          AS FMS_CKT_ID,            
     ''          AS PL_NBR,            
     ISNULL(fo.TTRPT_NW_ADR, '')       AS NUA_449_ADR,            
     ''          AS OLD_CKT_ID,            
     0          AS MULTI_LINK_CKT,            
     --''          AS WdMultilinkCircuit,      
     ''          AS WdSprintManaged,      
     '0'          AS SPRINT_MNGD_CD,      
     0          AS TELCO_ID,      
     ' '          AS WdTelcoCallout,      
     ''          AS FOC_DT,      
     0          AS GRPWdID,      
     ' '          AS NEW_FMS_CKT_ID,      
     ' '          AS NEW_NUA      
   FROM  @Table AS tt LEFT JOIN       
    dbo.FSA_ORDR AS fo WITH (NOLOCK) ON tt.ORDR_ID = fo.ORDR_ID LEFT JOIN            
    dbo.ORDR AS od WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID LEFT JOIN       
    dbo.LK_MDS_TRNSPRT_TYPE AS lm WITH (NOLOCK) ON fo.INSTL_TRNSPRT_TYPE_CD = lm.MDS_TRNSPRT_CD LEFT JOIN
    dbo.FSA_ORDR_CPE_LINE_ITEM CPED WITH (NOLOCK) ON fo.ORDR_ID= CPED.ORDR_ID
   WHERE  fo.ORDR_TYPE_CD IN  ('IN')            
    AND  ( (od.DMSTC_CD = 1 AND fo.PROD_TYPE_CD NOT IN ('MN', 'CP', 'SE'))            
      OR ( od.DMSTC_CD       = 0                
   AND fo.PROD_TYPE_CD      IN ('CP', 'IP')             
      AND ISNULL(CPED.TTRPT_ACCS_TYPE_CD, '') != '')            
      )       
    AND ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0) <> 0      
       
 -----------------------------------------------------------------------            
    -- Table 05 - SPA-E Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS SpID,            
     ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '') AS WdTransportType,             
     ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0)  AS MDS_TRNSPRT_TYPE_ID,            
     '0'          AS PRIM_BKUP_CD,      
           ''          AS WdPrimaryBackup,      
           ''          AS BDWL_CHNL_NME,                  
     ''          AS FMS_CKT_ID,            
     ''          AS ECCKT_ID,            
     ISNULL(fo.TTRPT_NW_ADR, '')       AS UNI_NUA_ADDR,            
     ''          AS OLD_CKT_ID,            
     --0          AS MULTI_LINK_CKT_CD,            
     --''          AS SpMultilinkCircuit,      
     ''          AS WdSprintManaged,      
     '0'          AS SPRINT_MNGD_CD,      
     0          AS TELCO_ID,      
     ' '          AS WdTelcoCallout,      
     ''          AS FOC_DT,      
     0          AS GrpWdID,      
     0          AS SpaEID      
   FROM  @Table AS tt LEFT JOIN       
    dbo.FSA_ORDR AS fo WITH (NOLOCK) ON tt.ORDR_ID = fo.ORDR_ID LEFT JOIN            
    dbo.ORDR AS od WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID LEFT JOIN       
    dbo.LK_MDS_TRNSPRT_TYPE AS lm WITH (NOLOCK) ON fo.INSTL_TRNSPRT_TYPE_CD = lm.MDS_TRNSPRT_CD LEFT JOIN
    dbo.FSA_ORDR_CPE_LINE_ITEM CPED WITH (NOLOCK) ON fo.ORDR_ID= CPED.ORDR_ID          
   WHERE  fo.ORDR_TYPE_CD IN  ('IN')            
    AND  ( (od.DMSTC_CD = 1 AND fo.PROD_TYPE_CD NOT IN ('MN', 'CP', 'SE'))            
      OR ( od.DMSTC_CD       = 0                
      AND fo.PROD_TYPE_CD      IN ('CP', 'IP')             
      AND ISNULL(CPED.TTRPT_ACCS_TYPE_CD, '') != '')            
      )      
            
 -----------------------------------------------------------------------            
    -- Table 06 - Sprint Link & ATM Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS SLID,            
     ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '') AS WdTransportType,             
     ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0)  AS MDS_TRNSPRT_TYPE_ID,            
     '0'          AS PRIM_BKUP_CD,      
           ''          AS WdPrimaryBackup,      
           ''          AS BDWL_CHNL_NME,                  
     ''          AS FMS_CKT_ID,            
     ''          AS PL_NBR,            
     ISNULL(fo.TTRPT_NW_ADR, '')       AS NUA_449_ADR,            
     ''          AS OLD_CKT_ID,            
     ''          AS WdSprintManaged,      
     '0'          AS SPRINT_MNGD_CD,      
     0          AS TELCO_ID,      
     ' '          AS WdTelcoCallout,      
     ''          AS FOC_DT,      
     0          AS GrpWdID,      
     0          AS WdID      
   FROM  @Table AS tt LEFT JOIN       
    dbo.FSA_ORDR AS fo WITH (NOLOCK) ON tt.ORDR_ID = fo.ORDR_ID LEFT JOIN            
    dbo.ORDR AS od WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID LEFT JOIN       
    dbo.LK_MDS_TRNSPRT_TYPE AS lm WITH (NOLOCK) ON fo.INSTL_TRNSPRT_TYPE_CD = lm.MDS_TRNSPRT_CD LEFT JOIN
    dbo.FSA_ORDR_CPE_LINE_ITEM CPED WITH (NOLOCK) ON fo.ORDR_ID= CPED.ORDR_ID           
   WHERE  fo.ORDR_TYPE_CD IN  ('IN')            
    AND  ( (od.DMSTC_CD = 1 AND fo.PROD_TYPE_CD NOT IN ('MN', 'CP', 'SE'))            
      OR ( od.DMSTC_CD       = 0                
      AND fo.PROD_TYPE_CD      IN ('CP', 'IP')    
      AND ISNULL(CPED.TTRPT_ACCS_TYPE_CD, '') != '')            
      )            
            
 -----------------------------------------------------------------------            
    -- Table 07 - Wireless Transport Data.       
    -----------------------------------------------------------------------            
    SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS WlID,            
     ''         AS WlPrimaryBackup,            
     ISNULL(md.PRIM_BKUP_CD, '')    AS PRIM_BKUP_CD,            
     ISNULL(md.ESN_MAC_ID, '')      AS ESN            
  FROM  dbo.MDS_EVENT_WRLS_TRPT md WITH (NOLOCK)             
  WHERE  1 != 1            
                
    -----------------------------------------------------------------------            
    -- Table 08 - Virtual Connection PVC Data.      
    -----------------------------------------------------------------------            
    SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS VirtualConnVCID,            
     ' '          AS FMS_NME,      
     ISNULL(DLCI_VPI_CD, '')     AS DLCI_VPI_CD            
      FROM  dbo.MDS_EVENT_VRTL_CNCTN WITH (NOLOCK)             
   WHERE  1 != 1                           
                
    -----------------------------------------------------------------------            
    -- Table 09 - Get the remaining data for a given MNS Event Data.            
    -----------------------------------------------------------------------            
    IF EXISTS(SELECT 'X'       
     FROM    @Table    vt              
    INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID              
    INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID              
    LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID              
    LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID              
    WHERE  (oc.CNTCT_TYPE_ID = 1 and oc.role_id = 6)            
     and (sc.CNTCT_TYPE_ID = 3             
     and (LEN(ISNULL(sc.FRST_NME, '')) <> 0)            
     )      
    )            
  BEGIN                  
   SELECT DISTINCT -- CPE Section              
      0     AS WIRED_DEV_TRPT_REQR_CD      
     ,0     AS SPAE_TRNSPRT_REQR_CD      
	 ,0     AS DSL_TRNSPRT_REQR_CD      
     ,0     AS SPRF_DEV_TRNSPRT_REQR_CD      
     ,0     AS WRLS_TRNSPRT_REQR_CD      
     ,0     AS VRTL_CNCTN_CD          
     ,0     AS CMPLTD_CD              
     ,ISNULL(oc.FRST_NME, '') + ' ' +              
       ISNULL(oc.LST_NME, '')        
          AS  INSTL_SITE_POC_NME      
     ,ISNULL(oc.ISD_CD,'') AS  INSTL_SITE_POC_INTL_PHN_CD      
     ,CASE ISNULL(oc.PHN_NBR, '')              
       WHEN '' THEN ISNULL(oc.CTY_CD, '')               
      + ISNULL(oc.NPA, '')                
      + ISNULL(oc.NXX, '')                
      + ISNULL(oc.STN_NBR, '')               
      ELSE ISNULL(oc.CTY_CD, '') + oc.PHN_NBR              
      END              
          AS  INSTL_SITE_POC_PHN_NBR               
     ,''     AS  INSTL_SITE_POC_INTL_CELL_PHN_CD      
     ,''           AS  INSTL_SITE_POC_CELL_PHN_NBR              
     ,ISNULL(sc.FRST_NME, '') + ' ' +              
      ISNULL(sc.LST_NME, '')        
         AS  SRVC_ASSRN_POC_NME          
         ,ISNULL(sc.ISD_CD,'') AS  SRVC_ASSRN_POC_INTL_PHN_CD
     , CASE ISNULL(sc.PHN_NBR, '')              
       WHEN '' THEN ISNULL(sc.CTY_CD, '')               
      + ISNULL(sc.NPA, '')                
      + ISNULL(sc.NXX, '')                
      + ISNULL(sc.STN_NBR, '')               
      ELSE ISNULL(sc.CTY_CD, '') + sc.PHN_NBR              
      END         
        AS  SRVC_ASSRN_POC_PHN_NBR      
     ,''      AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD      
     ,''         AS  SRVC_ASSRN_POC_CELL_NBR 
	 ,ISNULL(sc.CNTCT_HR_TXT, '')        AS SA_POC_HR_NME  
					,ISNULL(sc.FSA_TME_ZONE_CD, '')			AS TME_ZONE_ID
       ,@Tab_Seq_Nbr        AS TAB_SEQ_NBR              
   FROM  @Table    vt              
    INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID              
    INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID              
    LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID              
    LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID              
   WHERE  (oc.CNTCT_TYPE_ID = 1 and oc.role_id = 6)            
        AND (sc.CNTCT_TYPE_ID = 3             
         AND (LEN(ISNULL(sc.FRST_NME, '')) <> 0)            
      )            
 END             
    ELSE IF EXISTS( SELECT 'X' FROM  @Table    vt              
      INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID              
      INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID              
      LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID              
      LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID              
     WHERE  (oc.CNTCT_TYPE_ID = 1 and oc.role_id = 6)            
      )                 
    BEGIN             
   SELECT DISTINCT             
     0     AS WIRED_DEV_TRPT_REQR_CD      
     ,0     AS SPAE_TRNSPRT_REQR_CD   
	 ,0     AS DSL_TRNSPRT_REQR_CD         
     ,0     AS SPRF_DEV_TRNSPRT_REQR_CD      
     ,0     AS WRLS_TRNSPRT_REQR_CD      
     ,0     AS VRTL_CNCTN_CD          
     ,0     AS CMPLTD_CD              
     ,ISNULL(oc.FRST_NME, '')       
       + ' '       
       + ISNULL(oc.LST_NME, '')        
        AS  INSTL_SITE_POC_NME              
     , CASE od.DMSTC_CD               
       WHEN 1 THEN CASE ISNULL(oc.ISD_CD, '')       
           WHEN '' THEN ''      
           ELSE oc.ISD_CD       
          END       
       END        
       AS INSTL_SITE_POC_INTL_PHN_CD      
     , CASE ISNULL(oc.PHN_NBR, '')              
       WHEN '' THEN ISNULL(oc.CTY_CD, '')               
         + ISNULL(oc.NPA, '')                
         + ISNULL(oc.NXX, '')                
         + ISNULL(oc.STN_NBR, '')              
       ELSE ISNULL(oc.CTY_CD, '') + oc.PHN_NBR              
       END              
        AS  INSTL_SITE_POC_PHN_NBR               
    ,''       AS  INSTL_SITE_POC_INTL_CELL_PHN_CD      
    ,''          AS  INSTL_SITE_POC_CELL_PHN_NBR              
    ,ISNULL(sc.FRST_NME, '') + ' ' +        
						ISNULL(sc.LST_NME, '')  
								 AS  SRVC_ASSRN_POC_NME        
					, sc.ISD_CD	 AS  SRVC_ASSRN_POC_INTL_PHN_CD
					, CASE ISNULL(sc.ISD_CD, '') WHEN '' THEN '' ELSE sc.ISD_CD + '-' END        
						+ CASE ISNULL(sc.PHN_NBR, '')        
							WHEN '' THEN ISNULL(sc.CTY_CD, '')         
						+ ISNULL(sc.NPA, '')          
						+ ISNULL(sc.NXX, '')          
						+ ISNULL(sc.STN_NBR, '')         
						ELSE ISNULL(sc.CTY_CD, '')  + sc.PHN_NBR        
						END   
								AS  SRVC_ASSRN_POC_PHN_NBR                       
    ,''          AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD      
    ,''          AS  SRVC_ASSRN_POC_CELL_NBR   
	,ISNULL(sc.CNTCT_HR_TXT, '')        AS SA_POC_HR_NME  
					,ISNULL(sc.FSA_TME_ZONE_CD, '')			AS TME_ZONE_ID                
    ,@Tab_Seq_Nbr        AS TAB_SEQ_NBR              
        FROM  @Table    vt              
   INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID              
   INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID              
   LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID              
   LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID              
        WHERE  (oc.CNTCT_TYPE_ID = 1 and oc.role_id = 6)            
    END            
    ELSE             
    BEGIN             
       SELECT DISTINCT       
      0     AS WIRED_DEV_TRPT_REQR_CD      
     ,0     AS SPAE_TRNSPRT_REQR_CD  
	 ,0     AS DSL_TRNSPRT_REQR_CD          
     ,0     AS SPRF_DEV_TRNSPRT_REQR_CD      
     ,0     AS WRLS_TRNSPRT_REQR_CD      
     ,0     AS VRTL_CNCTN_CD          
     ,0     AS CMPLTD_CD              
     ,ISNULL(oc.FRST_NME, '') + ' ' +              
      ISNULL(oc.LST_NME, '')        
          AS  INSTL_SITE_POC_NME              
     ,CASE od.DMSTC_CD               
      WHEN 1 THEN CASE ISNULL(oc.ISD_CD, '')       
          WHEN '' THEN '' ELSE oc.ISD_CD       
         END       
     END       
      AS   INSTL_SITE_POC_INTL_PHN_CD           
     , CASE ISNULL(oc.PHN_NBR, '')              
      WHEN '' THEN ISNULL(oc.CTY_CD, '')               
         + ISNULL(oc.NPA, '')                
         + ISNULL(oc.NXX, '')                
         + ISNULL(oc.STN_NBR, '')               
      ELSE ISNULL(oc.CTY_CD, '') + oc.PHN_NBR              
     END                            
         AS  INSTL_SITE_POC_PHN_NBR               
     ,''       AS  INSTL_SITE_POC_INTL_CELL_PHN_CD      
     ,''          AS  INSTL_SITE_POC_CELL_PHN_NBR              
     ,ISNULL(sc.FRST_NME, '') + ' ' +        
						ISNULL(sc.LST_NME, '')  
								 AS  SRVC_ASSRN_POC_NME        
					, sc.ISD_CD	 AS  SRVC_ASSRN_POC_INTL_PHN_CD
					, CASE ISNULL(sc.ISD_CD, '') WHEN '' THEN '' ELSE sc.ISD_CD + '-' END        
						+ CASE ISNULL(sc.PHN_NBR, '')        
							WHEN '' THEN ISNULL(sc.CTY_CD, '')         
						+ ISNULL(sc.NPA, '')          
						+ ISNULL(sc.NXX, '')          
						+ ISNULL(sc.STN_NBR, '')         
						ELSE ISNULL(sc.CTY_CD, '')  + sc.PHN_NBR        
						END   
								AS  SRVC_ASSRN_POC_PHN_NBR                       
     ,''          AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD      
     ,''          AS  SRVC_ASSRN_POC_CELL_NBR
	 ,ISNULL(sc.CNTCT_HR_TXT, '')        AS SA_POC_HR_NME  
					,ISNULL(sc.FSA_TME_ZONE_CD, '')			AS TME_ZONE_ID
     ,@Tab_Seq_Nbr        AS TAB_SEQ_NBR              
        FROM  @Table    vt              
   INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID              
   INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID              
   LEFT JOIN dbo.ORDR_CNTCT  sc WITH (NOLOCK) ON fo.ORDR_ID = sc.ORDR_ID              
   LEFT JOIN dbo.ORDR_CNTCT  oc WITH (NOLOCK) ON fo.ORDR_ID = oc.ORDR_ID              
         WHERE   (sc.CNTCT_TYPE_ID = 3             
             and (LEN(ISNULL(sc.FRST_NME, '')) <> 0)            
            )            
                     
     END            
     -----------------------------------------------------------------------            
     -- Table 09 - Get the Order IDs for a given MNS Event Data.            
     -----------------------------------------------------------------------            
     SELECT DISTINCT @Tab_Seq_Nbr      AS TAB_SEQ_NBR            
             ,fo.ORDR_ID            
     FROM  @Table   vt            
     LEFT JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON vt.ORDR_ID = fo.ORDR_ID            
                 
     -----------------------------------------------------------------------            
     -- Table 10 - Get the Address elements for the order .            
     -----------------------------------------------------------------------            
     OPEN SYMMETRIC KEY FS@K3y             
     DECRYPTION BY CERTIFICATE S3cFS@CustInf0;            
     SELECT DISTINCT            
        -- Site Location Info            
        ISNULL(oa.STREET_ADR_1, '') + ' ' +            
         ISNULL(oa.STREET_ADR_2, '') AS  SITE_ADR            
        ,ISNULL(oa.BLDG_NME, '')   AS  FLR_BLDG_NME            
        ,ISNULL(oa.CTY_NME, '')   AS  CTY_NME            
        ,ISNULL(oa.STT_CD, '') +              
         ISNULL(oa.PRVN_NME, '')  AS  STT_PRVN_NME            
        ,ISNULL(lc.CTRY_NME, '')        AS  CTRY_RGN_NME            
        ,ISNULL(oa.CTRY_CD, '')         AS  CTRY_CD            
        ,ISNULL(oa.ZIP_PSTL_CD, '')  AS  ZIP_CD            
        ,CASE od.DMSTC_CD            
         WHEN NULL THEN 'D'            
         WHEN 0 THEN 'D'            
         ELSE 'I'            
        END              AS US_INTL_ID            
     FROM @Table    vt            
       INNER JOIN dbo.ORDR   od WITH (NOLOCK) ON vt.ORDR_ID = od.ORDR_ID            
       INNER JOIN dbo.FSA_ORDR  fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID            
       LEFT JOIN dbo.ORDR_ADR  oa WITH (NOLOCK) ON fo.ORDR_ID = oa.ORDR_ID            
       LEFT JOIN dbo.LK_CTRY   lc WITH (NOLOCK) ON oa.CTRY_CD = lc.CTRY_CD            
      WHERE   oa.ADR_TYPE_ID  = 18          
            
      ----------------------------------------------------------------------            
     -- Table 12 - Get the VLAN data            
     -----------------------------------------------------------------------                   
           
       SELECT DISTINCT @Tab_Seq_Nbr  AS TAB_SEQ_NBR      
     ,0     AS FSA_MDS_EVENT_ID      
     ,0     AS VLANID      
     ,' '    AS SPA_NUA      
     ,' '    AS VLAN      
      FROM  dbo.MDS_EVENT_VLAN WITH (NOLOCK)       
      WHERE 1 != 1      
	  
	  
	  ----------------------------------------------------------------------            
    -- Table 13 - DSL Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT @Tab_Seq_Nbr       AS TAB_SEQ_NBR,       
     0          AS FSA_MDS_EVENT_ID,      
     0          AS SpID,                     
     '0'          AS PRIM_BKUP_CD,      
           ''          AS PrimaryBackup,      
           ''          AS IP_ADDR,                  
     ''          AS SUBNET_MASK,            
     ''          AS NHOP_GWY,            
     ''       AS PrvdrCKTID,             
     ''          AS SprintManaged,      
     '0'          AS SPRINT_MNGD_CD,
	 ''          AS SprintCustomer,      
     '0'          AS SPRINT_CUST_CD,      
     0          AS TELCO_ID,      
     ' '          AS TelcoCallout,           
     0          AS GrpWdID,      
     0          AS DSLID      
   FROM  @Table AS tt LEFT JOIN       
    dbo.FSA_ORDR AS fo WITH (NOLOCK) ON tt.ORDR_ID = fo.ORDR_ID LEFT JOIN            
    dbo.ORDR AS od WITH (NOLOCK) ON fo.ORDR_ID = od.ORDR_ID LEFT JOIN       
    dbo.LK_MDS_TRNSPRT_TYPE AS lm WITH (NOLOCK) ON fo.INSTL_TRNSPRT_TYPE_CD = lm.MDS_TRNSPRT_CD LEFT JOIN
    dbo.FSA_ORDR_CPE_LINE_ITEM CPED WITH (NOLOCK) ON fo.ORDR_ID= CPED.ORDR_ID            
   WHERE  fo.ORDR_TYPE_CD IN  ('IN')            
    AND  ( (od.DMSTC_CD = 1 AND fo.PROD_TYPE_CD NOT IN ('MN', 'CP', 'SE'))            
      OR ( od.DMSTC_CD       = 0                
      AND fo.PROD_TYPE_CD      IN ('CP', 'IP')             
      AND ISNULL(CPED.TTRPT_ACCS_TYPE_CD, '') != '')            
      ) 
	  
	--MACH5 SERVICE TABLE
   SELECT TOP 1 @Tab_Seq_Nbr    AS TAB_SEQ_NBR      
     ,0    AS FSA_MDS_EVENT_ID      
	 ,0    AS MACH5_H6_SRVC_ID
     ,0    AS SRVC_TYPE_ID          
     ,''  AS SrvcType   
	 ,0    AS THRD_PARTY_VNDR_ID          
     ,''  AS ThirdPartyVndr     
	 ,''    AS ThirdParty
     ,0       AS THRD_PARTY_SRVC_LVL_ID
	 ,''  AS ThirdPartySrvcLvl
     ,'' AS ACTV_DT
	 ,'' AS CMNT_TXT
	 ,'FALSE' AS EMAIL_CD
     ,0     AS SrvcTblID      
   FROM  dbo.MDS_EVENT_SRVC WITH (NOLOCK)
   WHERE 1 != 1
             
   END            
 END            
END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END 