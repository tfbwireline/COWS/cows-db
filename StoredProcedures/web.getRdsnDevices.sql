USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getRdsnDevices]    Script Date: 11/28/2016 09:18:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		dlp0278
-- Create date: 10/20/2016
-- Description:	Indicates the Devices are in ODIE
-- =========================================================
ALTER PROCEDURE [web].[getRdsnDevices] 
	@redesignID			int

AS
BEGIN
SET NOCOUNT ON;

DECLARE @isChecked bit = 1

Begin Try
	
		SELECT Distinct
			DEV_NME
			,rdi.DSPCH_RDY_CD
			,rdi.FAST_TRK_CD
			,rdi.SC_CD
			,SEQ_NBR
			,REC_STUS_ID
			,REDSGN_DEV_ID
			,ISNULL(DEV_CMPLTN_CD,0) AS DEV_CMPLTN_CD
			,rdi.H6_CUST_ID
			,@isChecked AS IsChecked
		FROM dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK)
		INNER JOIN dbo.ODIE_RSPN_INFO ori WITH (NOLOCK)
			ON rdi.DEV_NME = ori.DEV_ID 
				AND ori.RSPN_INFO_DT > DATEADD(day,-1, getdate())
		WHERE rdi.REDSGN_ID = @redesignID
			AND rdi.REC_STUS_ID = 1
	
	
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END