USE [COWS]
GO
_CreateObject 'SP','web','LogWebActivity'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
      
 --=====================================================================================================    
 -- Author: Jagan Gangi
 -- Create Date: 08/10/2017
 -- Description : Log activities from website
 --=====================================================================================================    
    
 ALTER PROCEDURE [web].[LogWebActivity]     
  @actyType TINYINT,    
  @actyVal VARCHAR(max),    
  @userADID  VARCHAR(10),
  @userCSGLvl TINYINT = 0,
  @ObjCSGLvl TINYINT = 0
 AS    
 BEGIN    
  SET NOCOUNT ON    
  BEGIN TRY    
   
   IF ((ISNULL(@userADID,'') != '') AND (ISNULL(@actyVal,'') != '') AND (ISNULL(@actyType,0)>0))  
   BEGIN
		DECLARE @LogID BIGINT, @SuccsFailCd CHAR(1)
		SELECT TOP 1 @LogID = LOG_ID FROM dbo.WEB_SESSN_LOG WITH (NOLOCK) WHERE [USER_AD_ID]=@userADID AND REC_STUS_ID=1 ORDER BY SESSN_STRT_TME DESC
		   
		IF ((@userCSGLvl>0) AND (@ObjCSGLvl>0) AND (@userCSGLvl <= @ObjCSGLvl))
			SET @SuccsFailCd = 'S'
		ELSE
			SET @SuccsFailCd = 'F'
			
		INSERT INTO dbo.[WEB_ACTY_LOG] ([WEB_ACTY_TYPE_ID], [ACTY_VALU], [LOG_ID], [CREAT_DT], [SUCCS_FAIL_CD]) VALUES (@actyType, @actyVal, @LogID, GETDATE(), @SuccsFailCd)
   END
     
  END TRY    
  BEGIN CATCH    
   EXEC [dbo].[insertErrorInfo]      
   DECLARE @ErrMsg nVarchar(4000),       
     @ErrSeverity Int            
   SELECT @ErrMsg  = ERROR_MESSAGE(),            
    @ErrSeverity = ERROR_SEVERITY()            
   RAISERROR(@ErrMsg, @ErrSeverity, 1)     
  END CATCH    
    
 END
