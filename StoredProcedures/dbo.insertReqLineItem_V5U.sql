USE [COWS]
GO
_CreateObject 'SP','dbo','insertReqLineItem_V5U'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		qi931353
-- Create date: 11/16/2017
-- Description:	Inserts an SCM Line Item Request into the PS_REQ_LINE_ITM table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertReqLineItem_V5U]
			@ORDR_ID int
		   ,@CMPNT_ID int = null
           ,@REQSTN_NBR varchar(10)
           ,@MAT_CD varchar(10) = null
           ,@DLVY_CLLI varchar(20)= null
           ,@ITM_DES varchar(254) = null
           ,@MANF_PART_NBR varchar(20) = null
           ,@MANF_ID varchar(100) = null
           ,@ORDR_QTY int = null
           ,@UNT_MSR varchar(3) = null
           ,@UNT_PRICE decimal(15,5) = null
           ,@RAS_DT datetime = null
           ,@VNDR_NME varchar(30) = null
           ,@BUS_UNT_GL varchar(5) = null
           ,@ACCT varchar(6) = null
		   ,@COST_CNTR varchar(10) = null
		   ,@PRODCT varchar(6) = null
           ,@MRKT varchar(4) = null
           ,@AFFLT varchar(5) = null
		   ,@REGN varchar(6) = null
		   ,@PROJ_ID varchar(15) = null
		   ,@BUS_UNT_PC varchar(5) = null
		   ,@ACTVY varchar(15) = null
		   ,@SOURCE_TYP varchar(5) = null
		   ,@RSRC_CAT varchar(5) = null
		   ,@RSRC_SUB varchar(5) = null
		   ,@CNTRCT_ID varchar(25) = null
		   ,@CNTRCT_LN_NBR int = null
		   ,@AXLRY_ID varchar(24) = null
		   ,@INST_CD varchar(10) = null
		   ,@SENT_DT datetime = null
		   ,@FSA_CPE_LINE_ITEM_ID int = null
           ,@EQPT_TYPE_ID varchar(20) = null
           ,@COMMENTS varchar(512) = null
		   ,@MANF_DISCNT_CD varchar(500) = null
		   ,@REQ_LINE_NBR int = null
           
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY

		INSERT INTO [dbo].[PS_REQ_LINE_ITM_QUEUE]
			   ([ORDR_ID]
			   ,[CMPNT_ID]
			   ,[REQSTN_NBR]
			   ,[MAT_CD]
			   ,[DLVY_CLLI]
			   ,[ITM_DES]
			   ,[MANF_PART_NBR]
			   ,[MANF_ID]
			   ,[ORDR_QTY]
			   ,[UNT_MSR]
			   ,[UNT_PRICE]
			   ,[RAS_DT]
			   ,[VNDR_NME]
			   ,[BUS_UNT_GL]
			   ,[ACCT]
			   ,[COST_CNTR]
			   ,[PRODCT]
			   ,[MRKT]
			   ,[AFFLT]
			   ,[REGN]
			   ,[PROJ_ID]
			   ,[BUS_UNT_PC]
			   ,[ACTVY]
			   ,[SOURCE_TYP]
			   ,[RSRC_CAT]
			   ,[RSRC_SUB]
			   ,[CNTRCT_ID]
			   ,[CNTRCT_LN_NBR]
			   ,[AXLRY_ID]
			   ,[INST_CD]
			   ,[REC_STUS_ID]
			   ,[CREAT_DT]
			   ,[SENT_DT]
			   ,[FSA_CPE_LINE_ITEM_ID]
			   ,[EQPT_TYPE_ID]
			   ,[COMMENTS]
			   ,[MANF_DISCNT_CD]
			   ,[REQ_LINE_NBR])
		 VALUES
			   (@ORDR_ID
			   ,@CMPNT_ID
			   ,@REQSTN_NBR
			   ,@MAT_CD
			   ,@DLVY_CLLI
			   ,@ITM_DES
			   ,@MANF_PART_NBR
			   ,@MANF_ID
			   ,@ORDR_QTY
			   ,@UNT_MSR
			   ,@UNT_PRICE
			   ,@RAS_DT
			   ,@VNDR_NME
			   ,@BUS_UNT_GL
			   ,@ACCT
			   ,@COST_CNTR
			   ,@PRODCT
			   ,@MRKT
			   ,@AFFLT
			   ,@REGN
			   ,@PROJ_ID 
			   ,@BUS_UNT_PC
			   ,@ACTVY 
			   ,@SOURCE_TYP
			   ,@RSRC_CAT
			   ,@RSRC_SUB
			   ,@CNTRCT_ID
			   ,@CNTRCT_LN_NBR
			   ,@AXLRY_ID
			   ,@INST_CD
			   ,1 --REC_STUS_ID
			   ,getdate() --CREAT_DT
			   ,null --SENT_DT
			   ,@FSA_CPE_LINE_ITEM_ID 
			   ,@EQPT_TYPE_ID
			   ,@COMMENTS
			   ,@MANF_DISCNT_CD
			   ,@REQ_LINE_NBR
			   )
			   
END TRY

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch

END

