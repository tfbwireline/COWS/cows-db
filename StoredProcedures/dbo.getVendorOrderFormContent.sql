﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getVendorOrderFormContent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 12th July 2011
-- Description:	Returns content of Vendor Order Form
-- ================================================================

ALTER PROCEDURE [dbo].[getVendorOrderFormContent]
	@VendorOrderFormID INT 	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		FILE_CNTNT
	FROM 
		dbo.VNDR_ORDR_FORM WITH (NOLOCK)
	WHERE 
		VNDR_ORDR_FORM_ID = @VendorOrderFormID
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END


