USE [COWS]
GO
_CreateObject 'SP','dbo','insertOrderVLAN'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 10/30/2011
-- Description:	Inserts the FSA VLAN data into the ORDR_VLAN table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertOrderVLAN]
	@ORDR_ID			Int	
	,@VLAN_ID			Varchar(20)
	,@VLAN_SRC_NME		Varchar(5)
	,@VLAN_PCT_QTY		Varchar(3)
	,@CREAT_BY_USER_ID	Int
	
AS
BEGIN
SET NOCOUNT ON;

Begin Try
	INSERT INTO dbo.ORDR_VLAN WITH (ROWLOCK)
					(ORDR_ID
					,VLAN_ID
					,VLAN_SRC_NME
					,VLAN_PCT_QTY
					,CREAT_BY_USER_ID
					)
		VALUES		(@ORDR_ID
					,@VLAN_ID
					,@VLAN_SRC_NME
					,@VLAN_PCT_QTY
					,@CREAT_BY_USER_ID)
End Try
Begin Catch
	EXEC dbo.insertErrorInfo
End Catch
END
GO