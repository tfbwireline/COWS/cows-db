
-- =============================================            
-- Author:  su302037            
-- Create date: 06/30/2012  
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME           
-- Description: Get the required data for an MDS Event when a H5/H6 value is entered.            
-- =============================================            
ALTER PROCEDURE [web].[getMDSEventData]             
 @FSA_MDS_EVENT_ID Int            
 ,@Tab_Seq_Nbr TinyInt            
AS            
BEGIN            
SET NOCOUNT ON            
Begin Try            
            
OPEN SYMMETRIC KEY FS@K3y             
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;            
            
DECLARE @Table TABLE            
 (ORDR_ID   Int   NULL            
 ,PROD_TYPE_CD  VARCHAR(2) NULL            
 ,EVENT_ID   Int   NULL            
 ,FSA_MDS_EVENT_ID Int   NULL)            
            
  INSERT INTO @Table (ORDR_ID, PROD_TYPE_CD, EVENT_ID, FSA_MDS_EVENT_ID)            
   SELECT   0, 0, fm.EVENT_ID, FSA_MDS_EVENT_ID            
  FROM  dbo.FSA_MDS_EVENT_NEW  fm WITH (NOLOCK)            
  WHERE  fm.FSA_MDS_EVENT_ID  = @FSA_MDS_EVENT_ID            
            
IF (SELECT Count(1) FROM @Table) > 0            
 BEGIN                        
   -----------------------------------------------------------------------            
    -- Table 00 - ODIE Dev Name.            
    -----------------------------------------------------------------------            
 SELECT DISTINCT ISNULL(fe.TAB_SEQ_NBR, @Tab_Seq_Nbr)      AS TAB_SEQ_NBR      
     ,ISNULL(od.FSA_MDS_EVENT_ID, 0)     AS FSA_MDS_EVENT_ID      
     ,ISNULL(od.ODIE_DEV_NME, 0)      AS ODIE_DEV_NME      
     , 0            AS dtMDSODIENmeInfo      
 FROM @Table tt INNER JOIN       
   dbo.FSA_MDS_EVENT_NEW AS fe WITH (NOLOCK) ON fe.FSA_MDS_EVENT_ID = tt.FSA_MDS_EVENT_ID INNER JOIN      
   dbo.MDS_EVENT_ODIE_DEV_NME AS od WITH (NOLOCK) ON od.FSA_MDS_EVENT_ID = tt.FSA_MDS_EVENT_ID      
       
 -----------------------------------------------------------------------            
    -- Table 01 - Access Info FTN Table.            
    -----------------------------------------------------------------------             
 SELECT DISTINCT ISNULL(fe.TAB_SEQ_NBR, @Tab_Seq_Nbr)      AS TAB_SEQ_NBR      
     ,ISNULL(ea.FSA_MDS_EVENT_ID, 0)     AS FSA_MDS_EVENT_ID      
     ,ISNULL(ea.ACCS_FTN_NBR, '') AS ACCESS_FTN      
     ,ISNULL(ea.ORDR_ID, 0)       AS ORDR_ID         
     , 0            AS AccessInfoID      
 FROM @Table tt INNER JOIN       
   dbo.FSA_MDS_EVENT_NEW AS fe WITH (NOLOCK) ON fe.FSA_MDS_EVENT_ID = tt.FSA_MDS_EVENT_ID INNER JOIN      
   dbo.MDS_EVENT_ACCS AS ea WITH (NOLOCK) ON ea.FSA_MDS_EVENT_ID = tt.FSA_MDS_EVENT_ID       
       
 -----------------------------------------------------------------------            
    -- Table 02 - CPE Info FTN Table.            
    -----------------------------------------------------------------------             
 SELECT DISTINCT ISNULL(ec.TAB_SEQ_NBR, @Tab_Seq_Nbr)     AS TAB_SEQ_NBR      
     ,ISNULL(ec.FSA_MDS_EVENT_ID, 0)    AS FSA_MDS_EVENT_ID      
     ,ISNULL(ec.CPE_FTN_NBR, '')   AS CPE_FTN      
     ,ISNULL(ec.ORDR_ID, 0)      AS ORDR_ID      
     ,ISNULL(ec.EQPT_DES,'')      AS Equip_Desc      
     ,ISNULL(ec.EQPT_VNDR_NME, '')    AS Equip_Vend            
     , 0           AS CPEInfoID      
  FROM @Table tt INNER JOIN       
    dbo.MDS_EVENT_CPE AS ec WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = ec.FSA_MDS_EVENT_ID      
             
 -----------------------------------------------------------------------            
    -- Table 03 - MNS Info FTN Table.            
    -----------------------------------------------------------------------        
              
    SELECT DISTINCT ISNULL(em.TAB_SEQ_NBR, @Tab_Seq_Nbr)     AS TAB_SEQ_NBR      
     ,ISNULL(em.FSA_MDS_EVENT_ID, 0)    AS FSA_MDS_EVENT_ID      
     ,ISNULL(em.MNS_FTN_NBR, '')        AS MDS_MNS_FTN      
     ,ISNULL(ls.MDS_SRVC_TIER_DES, ' ')   AS MDS_SRVC_TIER_DES      
     ,ISNULL(ls.MDS_SRVC_TIER_ID, 0)    AS MDS_SRVC_TIER_ID          
     --,ISNULL(le.MDS_ENTLMNT_DES, ' ')   AS MDS_ENTLMNT_DES      
     ,' '   AS MDS_ENTLMNT_DES      
     ,ISNULL(em.MDS_ENTLMNT_ID, 0)    AS MDS_ENTLMNT_ID          
     ,ISNULL(em.MDS_ENTLMNT_ID, ' ')    AS MDS_ENTLMNT_OE_ID            
     ,0           AS MDSMNSInfoID      
     ,ISNULL(em.ORDR_ID, 0)      AS ORDR_ID            
  FROM  @Table   tt       
     INNER JOIN dbo.MDS_EVENT_MNS AS em WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = em.FSA_MDS_EVENT_ID      
     LEFT JOIN dbo.LK_MDS_SRVC_TIER AS ls WITH (NOLOCK) ON em.MDS_SRVC_TIER_ID = ls.MDS_SRVC_TIER_ID            
     --LEFT JOIN dbo.LK_MDS_ENTLMNT  AS le  ON em.MDS_ENTLMNT_ID = le.MDS_ENTLMNT_ID            
       
 -----------------------------------------------------------------------            
    -- Table 04 - Wired Device Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT ISNULL(wt.TAB_SEQ_NBR, @Tab_Seq_Nbr)    AS TAB_SEQ_NBR      
     ,ISNULL(wt.FSA_MDS_EVENT_ID, 0)    AS FSA_MDS_EVENT_ID      
     ,ISNULL(wt.MDS_TRPT_TYPE_ID, 0)    AS MDS_TRNSPRT_TYPE_ID          
     ,ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '')  AS WdTransportType      
     ,ISNULL(lm.MDS_TRNSPRT_TYPE_ID, 0)   AS MDS_TRNSPRT_TYPE_ID      
     ,CASE ISNULL(wt.PRIM_BKUP_CD, '')             
       WHEN 'P' THEN 'Primary'            
       WHEN 'B' THEN 'Backup'            
       ELSE ''            
      END             AS WdPrimaryBackup        
           ,wt.PRIM_BKUP_CD       AS PRIM_BKUP_CD      
           ,wt.BDWD_CHNL_NME                        AS BDWL_CHNL_NME      
     ,wt.FMS_CKT_ID        AS FMS_CKT_ID      
     ,wt.PL_NBR         AS PL_NBR      
     ,ISNULL(wt.IP_NUA_ADR, '')        AS NUA_449_ADR      
     ,wt.OLD_CKT_ID        AS OLD_CKT_ID      
     --,wt.MULTI_LINK_CKT_CD      AS MULTI_LINK_CKT            
     ,' '          AS MULTI_LINK_CKT          
     ,CASE ISNULL(wt.SPRINT_MNGD_CD, '')            
       WHEN 'S' THEN 'Sprint'            
       WHEN 'C' THEN 'Customer'             
       ELSE ''               
      END          AS WdSprintManaged            
     ,ISNULL(wt.SPRINT_MNGD_CD, '')    AS SPRINT_MNGD_CD      
     ,ISNULL(wt.TELCO_ID, '')  AS TELCO_ID          
     ,ISNULL(lt.TELCO_NME, '')       
       + ':' +             
       ISNULL(lt.TELCO_CNTCT_PHN_NBR, '')  AS WdTelcoCallout        
     ,ISNULL(Convert(Varchar(10), FOC_DT, 101), '') AS FOC_DT        
     ,0           AS GRPWdID       
     ,0           AS WdID      
     ,' '          AS NEW_FMS_CKT_ID      
     ,' '          AS NEW_NUA      
   FROM  @Table   tt       
     INNER JOIN dbo.MDS_EVENT_WIRED_TRPT AS wt WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = wt.FSA_MDS_EVENT_ID       
     LEFT JOIN dbo.LK_MDS_TRNSPRT_TYPE   lm WITH (NOLOCK)  ON wt.MDS_TRPT_TYPE_ID = lm.MDS_TRNSPRT_TYPE_ID                     
     LEFT JOIN dbo.LK_TELCO     lt WITH (NOLOCK) ON wt.TELCO_ID    = lt.TELCO_ID        
       
 -----------------------------------------------------------------------            
    -- Table 05 - SPA-E Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT ISNULL(swt.TAB_SEQ_NBR, @Tab_Seq_Nbr)    AS TAB_SEQ_NBR      
     ,ISNULL(swt.FSA_MDS_EVENT_ID, 0)    AS FSA_MDS_EVENT_ID      
     ,ISNULL(swt.MDS_TRPT_TYPE_ID, 0)    AS MDS_TRNSPRT_TYPE_ID          
     ,ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '')  AS WdTransportType      
     ,CASE ISNULL(swt.PRIM_BKUP_CD, '')             
       WHEN 'P' THEN 'Primary'            
       WHEN 'B' THEN 'Backup'            
       ELSE ''            
      END             AS WdPrimaryBackup        
           ,swt.PRIM_BKUP_CD       AS PRIM_BKUP_CD      
           ,swt.BDWD_CHNL_NME                        AS BDWL_CHNL_NME      
     --,swt.FMS_CKT_ID        AS FMS_CKT_ID      
     ,swt.ECCKT_ID         AS ECCKT_ID      
     ,ISNULL(swt.UNI_NUA_ADR, '')        AS UNI_NUA_ADDR      
     ,swt.OLD_CKT_ID        AS OLD_CKT_ID      
     ,CASE ISNULL(swt.SPRINT_MNGD_CD, '')            
       WHEN 'S' THEN 'Sprint'            
       WHEN 'C' THEN 'Customer'             
       ELSE ''               
      END          AS WdSprintManaged            
     ,ISNULL(swt.SPRINT_MNGD_CD, '')    AS SPRINT_MNGD_CD      
     ,ISNULL(swt.TELCO_ID, '')  AS TELCO_ID          
     ,ISNULL(lt.TELCO_NME, '')       
       + ':' +             
       ISNULL(lt.TELCO_CNTCT_PHN_NBR, '')  AS WdTelcoCallout        
     ,ISNULL(Convert(Varchar(10), FOC_DT, 101), '') AS FOC_DT        
     ,0           AS GRPWdID          
     ,0           AS SpaEID      
     ,0          AS SpID      
   FROM  @Table   tt       
     INNER JOIN dbo.MDS_EVENT_SPAE_TRPT AS swt WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = swt.FSA_MDS_EVENT_ID       
     LEFT JOIN dbo.LK_MDS_TRNSPRT_TYPE   lm WITH (NOLOCK)  ON swt.MDS_TRPT_TYPE_ID = lm.MDS_TRNSPRT_TYPE_ID                     
     LEFT JOIN dbo.LK_TELCO     lt WITH (NOLOCK) ON swt.TELCO_ID    = lt.TELCO_ID          
            
 -----------------------------------------------------------------------            
    -- Table 06 - SPA-RF Transport Info Table.            
    -----------------------------------------------------------------------        
      SELECT DISTINCT ISNULL(slwt.TAB_SEQ_NBR, @Tab_Seq_Nbr)  AS TAB_SEQ_NBR      
     ,ISNULL(slwt.FSA_MDS_EVENT_ID, 0)    AS FSA_MDS_EVENT_ID      
     ,ISNULL(slwt.MDS_TRPT_TYPE_ID, 0)    AS MDS_TRNSPRT_TYPE_ID          
     ,ISNULL (lm.MDS_TRNSPRT_TYPE_DES, '')   AS WdTransportType      
     ,CASE ISNULL(slwt.PRIM_BKUP_CD, '')             
       WHEN 'P' THEN 'Primary'            
       WHEN 'B' THEN 'Backup'            
       ELSE ''            
      END           AS WdPrimaryBackup        
           ,slwt.PRIM_BKUP_CD        AS PRIM_BKUP_CD      
           ,slwt.BDWD_CHNL_NME        AS BDWL_CHNL_NME      
     ,slwt.FMS_CKT_ID        AS FMS_CKT_ID      
     ,slwt.PL_NBR         AS PL_NBR      
     ,ISNULL(slwt.IP_NUA_ADR, '')     AS NUA_449_ADR      
     ,slwt.OLD_CKT_ID        AS OLD_CKT_ID      
     --,slwt.MULTI_LINK_CKT_CD      AS MULTI_LINK_CKT            
     /*,CASE ISNULL(slwt.SPRINT_MNGD_CD, '')            
       WHEN 'S' THEN 'Sprint'            
       WHEN 'C' THEN 'Customer'             
       ELSE ''               
      END           AS WdSprintManaged            
     ,ISNULL(slwt.SPRINT_MNGD_CD, '')    AS SPRINT_MNGD_CD      
     ,ISNULL(slwt.TELCO_ID, '')      AS TELCO_ID          
     ,ISNULL(lt.TELCO_NME, '')       
       + ':' +             
       ISNULL(lt.TELCO_CNTCT_PHN_NBR, '')  AS WdTelcoCallout*/        
     ,ISNULL(Convert(Varchar(10), FOC_DT, 101), '') AS FOC_DT        
     ,0            AS GRPWdID       
     ,0            AS WdID         
     ,0            AS SLID         
   FROM  @Table   tt       
     INNER JOIN dbo.MDS_EVENT_SPRINT_LINK_ATM_TRPT AS slwt WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = slwt.FSA_MDS_EVENT_ID       
     LEFT JOIN dbo.LK_MDS_TRNSPRT_TYPE   lm WITH (NOLOCK)  ON slwt.MDS_TRPT_TYPE_ID = lm.MDS_TRNSPRT_TYPE_ID                     
     LEFT JOIN dbo.LK_TELCO     lt WITH (NOLOCK) ON slwt.TELCO_ID    = lt.TELCO_ID      
            
 -----------------------------------------------------------------------            
    -- Table 07 - Wireless Transport Data.       
    -----------------------------------------------------------------------        
              
    SELECT DISTINCT ISNULL(md.TAB_SEQ_NBR, @Tab_Seq_Nbr)  AS TAB_SEQ_NBR      
     ,ISNULL(md.FSA_MDS_EVENT_ID, 0)    AS FSA_MDS_EVENT_ID      
     ,0           AS WlID      
     ,CASE ISNULL(md.PRIM_BKUP_CD, '')             
       WHEN 'P' THEN 'Primary'            
     WHEN 'B' THEN 'Backup'            
       ELSE ''            
      END          AS WlPrimaryBackup            
     ,ISNULL(md.PRIM_BKUP_CD, '')    AS PRIM_BKUP_CD       
     ,ISNULL(md.ESN_MAC_ID, '')     AS ESN            
  FROM  @Table   tt       
     INNER JOIN dbo.MDS_EVENT_WRLS_TRPT md WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = md.FSA_MDS_EVENT_ID       
        
                
    -----------------------------------------------------------------------            
    -- Table 08 - Virtual Connection PVC Data.      
    -----------------------------------------------------------------------            
          
    SELECT DISTINCT ISNULL(md.TAB_SEQ_NBR, @Tab_Seq_Nbr) AS TAB_SEQ_NBR      
     ,ISNULL(md.FSA_MDS_EVENT_ID, 0)   AS FSA_MDS_EVENT_ID      
     ,0          AS VirtualConnVCID      
     ,md.FMS_NME        AS FMS_NME      
     ,ISNULL(md.DLCI_VPI_CD, '')    AS DLCI_VPI_CD            
      FROM  @Table   tt       
     INNER JOIN dbo.MDS_EVENT_VRTL_CNCTN md WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = md.FSA_MDS_EVENT_ID       
         
    -----------------------------------------------------------------------            
    -- Table 09 - Get the remaining data for a given MNS Event Data.            
    -----------------------------------------------------------------------            
SELECT DISTINCT ISNULL(fm.TAB_SEQ_NBR, @Tab_Seq_Nbr) AS TAB_SEQ_NBR            
     ,ISNULL(fm.FSA_MDS_EVENT_ID, 0)   AS FSA_MDS_EVENT_ID   
     ,fm.H5_H6_CUST_ID AS H5_H6         
     -- CPE Section            
     ,fm.WIRED_DEV_TRPT_REQR_CD    AS WIRED_DEV_TRPT_REQR_CD      
     ,fm.SPAE_TRNSPRT_REQR_CD    AS SPAE_TRNSPRT_REQR_CD      
	 ,fm.DSL_TRNSPRT_REQR_CD    AS DSL_TRNSPRT_REQR_CD      
     ,fm.SPRF_DEV_TRNSPRT_REQR_CD   AS SPRF_DEV_TRNSPRT_REQR_CD      
     ,fm.WRLS_TRPT_REQR_CD     AS WRLS_TRNSPRT_REQR_CD      
     ,fm.VRTL_CNCTN_CD      AS VRTL_CNCTN_CD            
           
     --,ISNULL(dbo.decryptBinaryData(fm.INSTL_SITE_POC_NME), '')   AS  INSTL_SITE_POC_NME
     ,ISNULL(CASE WHEN (e.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_CNTCT_NME)    ELSE fm.INSTL_SITE_POC_NME END,'') AS INSTL_SITE_POC_NME /*Monir 06062018*/                        
     ,ISNULL(fm.INSTL_SITE_POC_INTL_PHN_CD, '' )     AS  INSTL_SITE_POC_INTL_PHN_CD      
     ,ISNULL(fm.INSTL_SITE_POC_PHN_NBR, '')      AS  INSTL_SITE_POC_PHN_NBR            
     ,ISNULL(fm.INSTL_SITE_POC_INTL_CELL_PHN_CD, '')    AS  INSTL_SITE_POC_INTL_CELL_PHN_CD      
     ,ISNULL(fm.INSTL_SITE_POC_CELL_PHN_NBR, '')     AS  INSTL_SITE_POC_CELL_PHN_NBR            
     --,ISNULL(dbo.decryptBinaryData(fm.SRVC_ASSRN_POC_NME), '')   AS  SRVC_ASSRN_POC_NME 
     ,ISNULL(CASE WHEN (e.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.SRVC_ASSRN_POC_NME)    ELSE fm.SRVC_ASSRN_POC_NME END,'') AS SRVC_ASSRN_POC_NME /*Monir 06062018*/            
     ,ISNULL(fm.SRVC_ASSRN_POC_INTL_PHN_CD, '')     AS  SRVC_ASSRN_POC_INTL_PHN_CD      
     ,ISNULL(fm.SRVC_ASSRN_POC_PHN_NBR, '')      AS  SRVC_ASSRN_POC_PHN_NBR            
     ,ISNULL(fm.SRVC_ASSRN_POC_INTL_CELL_PHN_CD, '')    AS  SRVC_ASSRN_POC_INTL_CELL_PHN_CD      
     ,ISNULL(fm.SRVC_ASSRN_POC_CELL_NBR, '')  AS  SRVC_ASSRN_POC_CELL_NBR            
	 ,ISNULL(fm.SA_POC_HR_NME, '')  AS  SA_POC_HR_NME     
	 ,ISNULL(fm.TME_ZONE_ID, '')  AS  TME_ZONE_ID
     ,fm.CMPLTD_CD        AS CMPLTD_CD            
     FROM  @Table    vt            
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fm WITH (NOLOCK) ON vt.FSA_MDS_EVENT_ID = fm.FSA_MDS_EVENT_ID              
     LEFT JOIN  cows.dbo.EVENT e WITH (NOLOCK) ON e.EVENT_ID = fm.EVENT_ID  
     LEFT JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=vt.FSA_MDS_EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=4       
     -----------------------------------------------------------------------            
     -- Table 10 - Get the Order IDs for a given MNS Event Data.            
     -----------------------------------------------------------------------            
     SELECT DISTINCT @Tab_Seq_Nbr      AS TAB_SEQ_NBR            
             ,vt.ORDR_ID            
     FROM  @Table   vt            
           
     ----------------------------------------------------------------------            
     -- Table 11 - Get the FTN's for Transport Order.            
     -----------------------------------------------------------------------            
     SELECT DISTINCT            
     -- Site Location Info            
        -- ISNULL(dbo.decryptBinaryData(fm.SITE_ADR), '')  AS  SITE_ADR      
        --,ISNULL(dbo.decryptBinaryData(fm.FLR_BLDG_NME), '') AS  FLR_BLDG_NME            
        --,ISNULL(dbo.decryptBinaryData(fm.CTY_NME), '')  AS  CTY_NME            
        --,ISNULL(dbo.decryptBinaryData(fm.STT_PRVN_NME), '') AS  STT_PRVN_NME            
        --,ISNULL(dbo.decryptBinaryData(fm.CTRY_RGN_NME), '') AS  CTRY_RGN_NME            
        --,ISNULL(dbo.decryptBinaryData(fm.ZIP_CD), '')  AS  ZIP_CD 
         CASE WHEN (e.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.SITE_ADR)    ELSE fm.SITE_ADR		END AS SITE_ADR /*Monir 06062018*/ 
        ,CASE WHEN (e.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.BLDG_NME)    ELSE fm.FLR_BLDG_NME	END AS FLR_BLDG_NME /*Monir 06062018*/ 
        ,CASE WHEN (e.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME)     ELSE fm.CTY_NME			END AS CTY_NME /*Monir 06062018*/ 
        ,CASE WHEN (e.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.STT_PRVN_NME)ELSE fm.STT_PRVN_NME	END AS STT_PRVN_NME /*Monir 06062018*/ 
        ,CASE WHEN (e.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTRY_RGN_NME)ELSE fm.CTRY_RGN_NME	END AS CTRY_RGN_NME /*Monir 06062018*/ 
        ,CASE WHEN (e.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.ZIP_PSTL_CD) ELSE fm.ZIP_CD			END AS ZIP_CD /*Monir 06062018*/           
        ,CASE ISNULL(fm.US_INTL_ID, NULL)            
     WHEN NULL THEN 'D'            
     WHEN 'D' THEN 'D'            
     ELSE 'I'            
    END           AS US_INTL_ID           
     FROM  @Table    vt            
     INNER JOIN dbo.FSA_MDS_EVENT_NEW fm WITH (NOLOCK) ON vt.FSA_MDS_EVENT_ID = fm.FSA_MDS_EVENT_ID   
     LEFT JOIN  cows.dbo.EVENT e WITH (NOLOCK) ON e.EVENT_ID = fm.EVENT_ID  
     LEFT JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=vt.FSA_MDS_EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=4          
       
  ----------------------------------------------------------------------            
     -- Table 12 - Get the VLAN data            
     -----------------------------------------------------------------------                   
           
       SELECT DISTINCT ISNULL(md.TAB_SEQ_NBR, @Tab_Seq_Nbr) AS TAB_SEQ_NBR      
     ,ISNULL(md.FSA_MDS_EVENT_ID, 0)   AS FSA_MDS_EVENT_ID      
     ,0          AS VLANID      
     ,md.SPA_NUA_ADR       AS SPA_NUA      
     ,md.VLAN_NBR       AS VLAN      
      FROM  @Table   tt       
     INNER JOIN dbo.MDS_EVENT_VLAN md WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = md.FSA_MDS_EVENT_ID       

	 --------------------------------------------------------------------            
    -- Table 13 - DSL Transport Info Table.            
    -----------------------------------------------------------------------        
     SELECT DISTINCT ISNULL(mdt.TAB_SEQ_NBR, @Tab_Seq_Nbr)    AS TAB_SEQ_NBR      
     ,ISNULL(mdt.FSA_MDS_EVENT_ID, 0)    AS FSA_MDS_EVENT_ID      
     ,CASE ISNULL(mdt.PRIM_BKUP_CD, '')             
       WHEN 'P' THEN 'Primary'            
       WHEN 'B' THEN 'Backup'            
       ELSE ''            
      END             AS PrimaryBackup        
           ,mdt.PRIM_BKUP_CD       AS PRIM_BKUP_CD      
           ,mdt.IP_ADR                        AS IP_ADDR      
     ,mdt.SUBNET_MASK_ADR        AS SUBNET_MASK      
     ,mdt.NXTHOP_GTWY_ADR         AS NHOP_GWY      
     ,mdt.PRVDR_CKT_ID        AS PrvdrCKTID      
     ,CASE ISNULL(mdt.SPRINT_CUST_CD, '')            
       WHEN 'S' THEN 'Sprint'            
       WHEN 'C' THEN 'Customer'             
       ELSE ''               
      END          AS SprintCustomer            
     ,ISNULL(mdt.SPRINT_CUST_CD, '')    AS SPRINT_CUST_CD      
     ,CASE ISNULL(mdt.SPRINT_MNGD_CD, '')            
       WHEN 'S' THEN 'Sprint'            
       WHEN 'M' THEN 'Managed'             
       ELSE ''               
      END          AS SprintManaged            
     ,ISNULL(mdt.SPRINT_MNGD_CD, '')    AS SPRINT_MNGD_CD      
     ,ISNULL(mdt.TELCO_ID, '')  AS TELCO_ID          
     ,ISNULL(lt.TELCO_NME, '')       
       + ':' +             
       ISNULL(lt.TELCO_CNTCT_PHN_NBR, '')  AS TelcoCallout             
     ,0           AS GRPWdID          
     ,0           AS DSLID      
     ,0          AS SpID      
   FROM  @Table   tt       
     INNER JOIN dbo.MDS_EVENT_DSL_TRPT AS mdt WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = mdt.FSA_MDS_EVENT_ID                         
     LEFT JOIN dbo.LK_TELCO     lt WITH (NOLOCK) ON mdt.TELCO_ID    = lt.TELCO_ID

	 ------------------------------------------------------------------------------------
	-- MACH5 Service Table
	------------------------------------------------------------------------------------
	SELECT DISTINCT COALESCE(mes.TAB_SEQ_NBR, @Tab_Seq_Nbr)    AS TAB_SEQ_NBR      
     ,COALESCE(mes.FSA_MDS_EVENT_ID, 0)    AS FSA_MDS_EVENT_ID      
	 ,COALESCE(mes.MACH5_H6_SRVC_ID, 0)    AS MACH5_H6_SRVC_ID
     ,COALESCE(mes.SRVC_TYPE_ID, 0)    AS SRVC_TYPE_ID          
     ,COALESCE (lst.SRVC_TYPE_DES, '')  AS SrvcType   
	 ,COALESCE(mes.THRD_PARTY_VNDR_ID, 0)    AS THRD_PARTY_VNDR_ID          
     ,COALESCE (lm3v.THRD_PARTY_VNDR_DES, '')  AS ThirdPartyVndr     
	 ,COALESCE(mes.THRD_PARTY_ID, '')    AS ThirdParty
     ,COALESCE(mes.THRD_PARTY_SRVC_LVL_ID, 0)       AS THRD_PARTY_SRVC_LVL_ID
	 ,COALESCE(lm3s.THRD_PARTY_SRVC_LVL_DES, '')  AS ThirdPartySrvcLvl
     ,COALESCE(Convert(Varchar(10), mes.ACTV_DT, 101), '') AS ACTV_DT
	 ,COALESCE(mes.CMNT_TXT,'') AS CMNT_TXT
	 ,CASE mes.EMAIL_CD WHEN 1 THEN 'TRUE' ELSE 'FALSE' END AS EMAIL_CD
     ,0           AS SrvcTblID      
   FROM  @Table   tt       
     INNER JOIN dbo.MDS_EVENT_SRVC AS mes WITH (NOLOCK) ON tt.FSA_MDS_EVENT_ID = mes.FSA_MDS_EVENT_ID       
     LEFT JOIN dbo.LK_MDS_3RDPARTY_VNDR   lm3v WITH (NOLOCK)  ON mes.THRD_PARTY_VNDR_ID = lm3v.THRD_PARTY_VNDR_ID                     
     LEFT JOIN dbo.LK_MDS_SRVC_TYPE     lst WITH (NOLOCK) ON mes.SRVC_TYPE_ID    = lst.SRVC_TYPE_ID 
	 LEFT JOIN dbo.LK_MDS_3RDPARTY_SRVC_LVL lm3s WITH (NOLOCK) ON lm3s.THRD_PARTY_SRVC_LVL_ID = mes.THRD_PARTY_SRVC_LVL_ID	 
	 

   END                             
END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END 

