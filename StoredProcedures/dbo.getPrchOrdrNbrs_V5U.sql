USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getPrchOrdrNbrs_V5U]    Script Date: 05/11/2016 15:09:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 5/4/2015
	-- Description:	Appian CPE PO Numbers Info.
	---- =============================================
	

	CREATE PROCEDURE [dbo].[getPrchOrdrNbrs_V5U]

	AS
	BEGIN

	SELECT		 
			DISTINCT	 fo.ORDR_ID 
						,litm.PRCH_ORDR_NBR     
						,fo.FTN                          											   
						,litm.PLSFT_RQSTN_NBR

						
	FROM         dbo.FSA_ORDR fo WITH (NOLOCK)
	INNER JOIN   dbo.ORDR ord WITH (NOLOCK) ON fo.ORDR_ID = ord.ORDR_ID
	INNER JOIN   dbo.FSA_ORDR_CPE_LINE_ITEM litm WITH (NOLOCK) ON litm.ORDR_ID = fo.ORDR_ID
	INNER JOIN   dbo.ACT_TASK act WITH (NOLOCK) ON act.ORDR_ID = fo.ORDR_ID

	WHERE  fo.PROD_TYPE_CD = 'CP'
		AND (act.TASK_ID IN (602) AND act.STUS_ID = 0)
		AND ISNULL(PRCH_ORDR_NBR,'') <> ''
		AND litm.ITM_STUS = 401

	END


