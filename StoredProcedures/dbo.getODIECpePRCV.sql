USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getODIECPEStus]    Script Date: 12/05/2016 13:05:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


	-- =============================================
	-- Author:		David Phillillps
	-- Create date: 12/05/2016
	-- Description:	Retrieves ODIE CPE InSide Move Transaction Data.
	---- =============================================
	

	ALTER PROCEDURE [dbo].[getODIECpePRCV] 
		@REQ_ID int  

	AS
	BEGIN


	BEGIN TRY
	
	DECLARE @ORDR_ID int  
	DECLARE @DEVICE_ID varchar (25)
	
	SELECT @ORDR_ID = ORDR_ID FROM dbo.ODIE_REQ WHERE REQ_ID = @REQ_ID
	
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	-- Order Info.
	SELECT  DISTINCT			
			ISNULL(fsa.FTN,'')											AS OrderID,
			GETDATE()													AS StatusDate,
			DEVICE_ID													AS DeviceID,
			cpe.CNTRC_TYPE_ID											AS CntrcType,
			cpe.CNTRC_TERM_ID											AS CntrcTerm                      
					
	FROM dbo.FSA_ORDR fsa WITH (NOLOCK)
	INNER JOIN dbo.ORDR	ord WITH (NOLOCK) ON ord.ORDR_ID = fsa.ORDR_ID
	INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM cpe ON cpe.ORDR_ID = fsa.ORDR_ID
	WHERE fsa.ORDR_ID = @ORDR_ID
		
	--Notes
	
	SELECT 
		ISNULL(NTE_TXT,'')                                              AS [NoteText]
	
	FROM dbo.ORDR_NTE WITH (NOLOCK)
	WHERE ORDR_ID = @ORDR_ID
		AND NTE_TYPE_ID = 6
	
	RETURN
			
	END TRY

	BEGIN CATCH
		EXEC	[dbo].[insertErrorInfo]
	END CATCH

	END
	




