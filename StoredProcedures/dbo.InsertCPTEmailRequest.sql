USE [COWS]
GO
_CreateObject 'SP','dbo','InsertCPTEmailRequest'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================================
-- Author:			<Sarah Sandoval>
-- Create date:		09/16/2015
-- Description:		Inserts a CPT Email Request to be send.
-- Updated by:		<Sarah Sandoval>
-- Updated date:	06/26/2018
-- Description:		Added condition to insert email request for CPT not in
--					Deleted (308), Completed (309) and Cancelled (311) status.
-- =====================================================================================
ALTER PROCEDURE [dbo].[InsertCPTEmailRequest] 
	@CPT_ID			Int
	,@EMAIL_REQ_TYPE_ID	Int
	,@STUS_ID		Int		=	NULL
	,@EMAIL_LIST_TXT	Varchar(Max)	=	NULL	
	,@EMAIL_CC_TXT		Varchar(1000)	=	NULL	
	,@EMAIL_SUBJ_TXT	Varchar(1000)	=	NULL	
	,@EMAIL_BODY_TXT	Varchar(Max)	=	NULL	

AS
BEGIN
BEGIN TRY
	DECLARE	@EMAIL_REQ_ID	Int			=	0
	
	IF (@CPT_ID > 0 AND 
		(SELECT COUNT(1) FROM CPT WITH (NOLOCK) WHERE CPT_ID = @CPT_ID AND CPT_STUS_ID NOT IN (308,309,311)) > 0)
		BEGIN
			INSERT INTO	dbo.EMAIL_REQ	WITH (ROWLOCK)
					(EMAIL_REQ_TYPE_ID
					,STUS_ID
					,CPT_ID
					,EMAIL_LIST_TXT
					,EMAIL_CC_TXT
					,EMAIL_SUBJ_TXT
					,EMAIL_BODY_TXT
					,CREAT_DT)
			VALUES	(@EMAIL_REQ_TYPE_ID
					,@STUS_ID
					,@CPT_ID
					,@EMAIL_LIST_TXT
					,@EMAIL_CC_TXT
					,@EMAIL_SUBJ_TXT
					,@EMAIL_BODY_TXT
					,GETDATE())
				
			SELECT	@EMAIL_REQ_ID	=	SCOPE_IDENTITY()		
		END

END TRY

BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END