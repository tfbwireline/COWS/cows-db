USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[CompleteCPESrvcOnly]    Script Date: 10/13/2020 8:16:51 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 04/10/2017
-- Description:	Completes CPE Service Only orders.
-- =========================================================
ALTER PROCEDURE [dbo].[CompleteCPESrvcOnly]
	@ORDR_ID INT ,
	@TaskId SMALLINT , 
	@TaskStatus TINYINT, 
	@Comments VARCHAR(1000) = ''
AS

BEGIN
SET NOCOUNT ON;

	DECLARE @device_id varchar (25)

BEGIN TRY

	IF (NOT EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
				WHERE (EQPT_TYPE_ID in ('MJH','MMH','MMM','MMS','IPH') AND ITM_STUS = 401) AND ORDR_ID = @ORDR_ID )
		AND EXISTS (SELECT * FROM dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
				WHERE (EQPT_TYPE_ID in ('SVC') AND ITM_STUS = 401) AND ORDR_ID = @ORDR_ID )
		AND EXISTS (SELECT * FROM ORDR WITH (NOLOCK)
				WHERE ORDR_CAT_ID = 6 AND ORDR_ID = @ORDR_ID))
		
		BEGIN
		
			EXEC dbo.CompleteActiveTask @ORDR_ID,1000,2,'System Completed BAR-Pending Task for Services-Only order',1,9
		
			SELECT DISTINCT top 1 @device_id = DEVICE_ID FROM FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
				WHERE ORDR_ID = @ORDR_ID
				
				IF ISNULL(@device_ID,'') != ''
					BEGIN
						INSERT INTO dbo.M5_ORDR_MSG(ORDR_ID,M5_MSG_ID,NTE,MSG,DEVICE,CMPNT_ID,STUS_ID,CREAT_DT,SENT_DT,PRCH_ORDR_NBR)
						VALUES(@ORDR_ID,1,null,null,@device_id,null,10,GETDATE(),null,null)
					END	
					
			DELETE BPM_ORDR_ADR WHERE ORDR_ID = @ORDR_ID		
		END
				
END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH

END