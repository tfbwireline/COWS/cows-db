USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[completeOrderActivity]    Script Date: 09/19/2017 13:03:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==================================================================================
-- Author:		sbg9814
-- Create date: 08/10/2011
-- Description:	Complete the Order Activity when Bill Activated is loaded.
-- ==================================================================================
ALTER PROCEDURE [dbo].[completeOrderActivity] 
	@OrderID	Int ,
	@TaskID		SmallInt	=	1001,
	@TaskStatus	TinyInt		=	0,
	@Comments	Varchar(1000) = NULL
AS
BEGIN
Begin Try
DECLARE @OrderStatus INT
SET @OrderStatus = 0
	IF EXISTS
		(
			SELECT 'X'
				FROM	dbo.ORDR		odr	WITH (NOLOCK)
			LEFT JOIN	dbo.FSA_ORDR	f	WITH (NOLOCK)	ON	odr.ORDR_ID	=	f.ORDR_ID
			LEFT JOIN	dbo.FSA_ORDR	fp	WITH (NOLOCK)	ON	fp.PRNT_FTN	=	f.FTN
			LEFT JOIN	dbo.LK_PPRT		lp	WITH (NOLOCK)	ON	odr.PPRT_ID	=	lp.PPRT_ID
				WHERE	odr.ORDR_ID		=	@OrderID
				    AND odr.ORDR_CAT_ID <> 6
					AND	((ISNULL(fp.ORDR_TYPE_CD,'') = 'CN') OR (ISNULL(lp.ORDR_TYPE_ID,0) = 8))
				
		)
		SET @OrderStatus = 4
	
	IF @OrderStatus = 0
		BEGIN
			SELECT  @OrderStatus = CASE t.FSA_ORDR_TYPE_CD
										WHEN 'DC' THEN 5
										WHEN 'CN' THEN 4
										ELSE 2
									END
				FROM dbo.ORDR o WITH (NOLOCK)
					LEFT JOIN dbo.FSA_ORDR f WITH (NOLOCK) ON o.ORDR_ID = f.ORDR_ID
					LEFT JOIN dbo.IPL_ORDR i WITH (NOLOCK) ON o.ORDR_ID = i.ORDR_ID
					LEFT JOIN dbo.NCCO_ORDR ncco WITH (NOLOCK) ON ncco.ORDR_ID = o.ORDR_ID
					INNER JOIN dbo.LK_ORDR_TYPE t WITH (NOLOCK) ON ((f.ORDR_TYPE_CD = t.FSA_ORDR_TYPE_CD) OR (i.ORDR_TYPE_ID = t.ORDR_TYPE_ID) OR (ncco.ORDR_TYPE_ID = t.ORDR_TYPE_ID))
				WHERE o.ORDR_ID = @OrderID 
		END
		
	IF EXISTS -- added 9/19/17 1:24 pm to change status to cancelled instead of complete for Intl orders.
		(
			SELECT 'X' FROM  ORDR o WITH (NOLOCK)
					inner join FSA_ORDR f WITH (NOLOCK) ON o.ORDR_ID = f.ORDR_ID
					inner join LK_PPRT p WITH (NOLOCK) ON p.PPRT_ID = o.PPRT_ID
						WHERE o.ORDR_ID = @OrderID
						AND p.ORDR_TYPE_ID = 8 
		)
		BEGIN
			SET @OrderStatus = 4
		END 
				
			 
	IF @OrderStatus <> 0
		BEGIN
			UPDATE		dbo.ORDR	WITH (ROWLOCK)
				SET		ORDR_STUS_ID	=	@OrderStatus
				WHERE	ORDR_ID			=	@OrderID

			IF (@OrderStatus = 2)
			BEGIN
				INSERT INTO dbo.M5_ORDR_MSG ([ORDR_ID], [M5_MSG_ID], [STUS_ID], [DEVICE], [CREAT_DT])
				SELECT DISTINCT od.ORDR_ID, 1, 10, fc.DEVICE_ID, GETDATE()
				FROM dbo.ORDR od WITH (NOLOCK) INNER JOIN
				dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON fc.ORDR_ID=od.ORDR_ID
				WHERE od.ORDR_ID=@OrderID
				  AND od.ORDR_CAT_ID=6
				  AND od.PPRT_ID IN (629,630)
				  AND fc.DEVICE_ID IS NOT NULL
			END
		END
		
	--IF EXISTS
	--	(
	--		SELECT 'X'
	--			FROM	dbo.ACT_TASK WITH (NOLOCK)
	--			WHERE	ORDR_ID = @OrderID
	--				AND	TASK_ID	=	220
	--				AND	STUS_ID	=	0
	--	)
	--	BEGIN
	--		DELETE 
	--			FROM	dbo.ACT_TASK WITH (ROWLOCK) 
	--			WHERE	ORDR_ID = @OrderID
	--				AND	TASK_ID	=	220
	--				AND	STUS_ID	=	0
	--	END
End Try

Begin Catch
	EXEC	[dbo].[insertErrorInfo]
End Catch
END
