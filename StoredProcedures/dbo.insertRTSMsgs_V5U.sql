USE [COWS]
GO

/****** Object:  StoredProcedure [dbo].[insertRTSMsgs_V5U]    Script Date: 10/01/2015 09:41:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		dlp0278
-- Create date: 07/22/2015
-- Description:	Add BPM Complete/RTS messages to send to MACH 5
-- =========================================================
CREATE PROCEDURE [dbo].[insertRTSMsgs_V5U]
		@ORDR_ID		INT,
		@STUS			VARCHAR(50),
		@Comments		VARCHAR(1000) = NULL,
		@ADID			VARCHAR(10)  = NULL
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
		
		
		INSERT INTO [dbo].[M5_ORDR_MSG]
				([ORDR_ID],[M5_MSG_ID],[STUS_ID],[CREAT_DT],[EVENT_ID],[SENT_DT],[FSA_CPE_LINE_ITEM_ID])
		VALUES (@ORDR_ID,8,10,GETDATE(),NULL, NULL, NULL)           
		
		DECLARE @TaskID INT 
		DECLARE	@USER_ID INT
		
		SELECT @TaskID = TASK_ID FROM dbo.LK_TASK WITH (NOLOCK)
			WHERE TASK_NME = @STUS
		
		SELECT @USER_ID = USER_ID FROM dbo.LK_USER WITH (NOLOCK)
			WHERE USER_ACF2_ID = @ADID
		
		SET @Comments = @ADID + ' returned order to Sales -- ' + @Comments
		
		IF ISNULL(@USER_ID,0) = 0
			SET @USER_ID = 1	 
		

		EXEC [dbo].[CompleteActiveTask]  @ORDR_ID, @TaskID, 1, @Comments, @USER_ID, 6   

			
	
END TRY

BEGIN Catch
	EXEC [dbo].[insertErrorInfo]
END Catch

END

GO


