USE [COWS]
GO
_CreateObject 'SP','dbo','updateDmstcWFM_V5U'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		dlp0278
-- Create date: 09/24/2015
-- Description:	Updates Assigned User on Domestic CPE Orders.
--kh946640: Updated SP to support COWS ReWrite App by updating value in [USER_WFM_ASMT] table with column "USR_PRF_ID"
-- =========================================================
ALTER PROCEDURE [dbo].[updateDmstcWFM_V5U]
		@ORDR_ID		INT,
		@ADID			VARCHAR(10),
		@ASSIGNER_ADID  VARCHAR(10),
		@COMMENTS       VARCHAR(MAX) = NULL 
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY

		DECLARE	@ASN_USER_ID INT
		DECLARE	@ASSIGNER_USER_ID INT
		DECLARE @NTE_TXT VARCHAR(1000)
		
		
		SELECT @ASN_USER_ID = USER_ID FROM dbo.LK_USER WITH (NOLOCK)
			WHERE USER_ACF2_ID = @ADID
		
		SELECT @ASSIGNER_USER_ID = USER_ID FROM dbo.LK_USER WITH (NOLOCK)
			WHERE USER_ACF2_ID = @ASSIGNER_ADID
			
		IF ISNULL(@ASN_USER_ID,0) = 0
			BEGIN
				SET @NTE_TXT = @ADID + ' was entered to assign order to but ADID was not found to be a valid user. '
				
				INSERT INTO [dbo].[ORDR_NTE]
				   ([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT],[MODFD_BY_USER_ID],[MODFD_DT])
					VALUES
				   (6,@ORDR_ID,GETDATE(),ISNULL(@ASSIGNER_USER_ID,1),1,@NTE_TXT,NULL,NULL)
			END
		ELSE
			BEGIN	
				DELETE [dbo].[USER_WFM_ASMT] WHERE ORDR_ID = @ORDR_ID AND ((GRP_ID = 13) OR (USR_PRF_ID=98))
				
				INSERT INTO [dbo].[USER_WFM_ASMT]
				   ([ORDR_ID],[GRP_ID],[ASN_USER_ID],[ASN_BY_USER_ID],[ASMT_DT],[CREAT_DT],[ORDR_HIGHLIGHT_CD],[ORDR_HIGHLIGHT_MODFD_DT],USR_PRF_ID)
				VALUES
				   (@ORDR_ID,13,@ASN_USER_ID,@ASSIGNER_USER_ID,GETDATE(),GETDATE(),null,null,98)
				
				SET @NTE_TXT = @ADID + ' was assigned order by ' + @ASSIGNER_ADID + ' -- ' + @COMMENTS
				
				INSERT INTO [dbo].[ORDR_NTE]
				   ([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT],[MODFD_BY_USER_ID],[MODFD_DT])
					VALUES
				   (6,@ORDR_ID,GETDATE(),@ASSIGNER_USER_ID,1,@NTE_TXT,NULL,NULL)
			END
 	
	
END TRY

BEGIN Catch
	EXEC [dbo].[insertErrorInfo]
END Catch

END