USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getOrdrData]    Script Date: 02/05/2021 1:36:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--[web].[getOrdrData] '11692031' , null
--[web].[getOrdrData] 'IN3VOM302511' , null
--[web].[getOrdrData] 'IN3VOM302511' , 928633745
--[web].[getOrdrData] null, 928633745
--[web].[getOrdrData] null , null
-- =============================================
-- Author:		<Md M Monir>
-- Create date: <01/22/2019,>
-- Description:	<To get ORDR information based on FTN/ H5_H6_CUST_ID>
-- =============================================
ALTER PROCEDURE [web].[getOrdrData]
	@FTN varchar(50),
	@H5_H6_CUST_ID varchar(9)
AS
BEGIN TRY
BEGIN
	--SET NOCOUNT ON;
	OPEN SYMMETRIC KEY FS@K3y    
    DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
	SELECT DISTINCT
		FTN = B.FTN,
		H5_H6 = A.H5_H6_CUST_ID,
		ORDR_TYPE = F.ORDR_TYPE_DES,
		PROD_TYPE = E.PROD_TYPE_DES,
		CUST_NME = ISNULL(CONVERT(varchar, DecryptByKey(G.CUST_NME)),C.CUST_NME) ,
		CCD= ISNULL(CONVERT(varchar, A.CUST_CMMT_DT,101),CONVERT(varchar,B.CUST_CMMT_DT,101)),
		NOTES=''
	FROM 
		dbo.ORDR  A WITH (NOLOCK) LEFT JOIN  
		dbo.FSA_ORDR   B WITH (NOLOCK) ON A.ORDR_ID=B.ORDR_ID LEFT JOIN
		dbo.FSA_ORDR_CUST   C WITH (NOLOCK) ON C.ORDR_ID=B.ORDR_ID LEFT JOIN
		dbo.LK_PPRT   D WITH (NOLOCK) ON D.PPRT_ID=A.PPRT_ID LEFT JOIN
		dbo.LK_PROD_TYPE   E WITH (NOLOCK) ON B.PROD_TYPE_CD=E.FSA_PROD_TYPE_CD LEFT JOIN
		dbo.LK_ORDR_TYPE   F WITH (NOLOCK) ON F.FSA_ORDR_TYPE_CD=B.ORDR_TYPE_CD LEFT JOIN
		dbo.CUST_SCRD_DATA   G WITH (NOLOCK) ON  G.SCRD_OBJ_ID=C.FSA_ORDR_CUST_ID LEFT JOIN
		dbo.LK_SCRD_OBJ_TYPE H WITH (NOLOCK) ON H.SCRD_OBJ_TYPE_ID=G.SCRD_OBJ_TYPE_ID AND H.SCRD_OBJ_TYPE='FSA_ORDR_CUST'
	WHERE
		 --(B.FTN=ISNULL(@FTN,B.FTN)OR A.H5_H6_CUST_ID=ISNULL(@H5_H6_CUST_ID,A.H5_H6_CUST_ID)) 
		 (B.FTN=@FTN OR A.H5_H6_CUST_ID=@H5_H6_CUST_ID)
		 AND C.CIS_LVL_TYPE = 'H1'
         AND A.ORDR_STUS_ID = 1
         AND D.ORDR_TYPE_ID != 8	
		
	CLOSE SYMMETRIC KEY FS@K3y 
END
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch

