USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getZsclrDiscInfo]    Script Date: 11/15/2021 10:14:37 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Created By:		David Phillips
-- Create date: 02/07/2017
-- Description:	This SP is used to get Zscaler Disconnect Email Info.
-- =============================================
ALTER PROCEDURE [dbo].[getZsclrDiscInfo]
					 
  @ORDR_ID INT 
AS
BEGIN

	SET NOCOUNT ON;

	BEGIN TRY
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	 SELECT DISTINCT 
			CASE
				WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN foc.CUST_NME
				ELSE ISNULL(CONVERT(VARCHAR (max), DecryptByKey(sa.CUST_NME)),'') 
			END										AS Customer
			,CASE
				WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN oa.CTY_NME
				ELSE ISNULL(CONVERT(VARCHAR (max), DecryptByKey(sa.CTY_NME)),'') 
			END                                     AS City
			,CASE
				WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN oa.STT_CD
				ELSE CONVERT(VARCHAR (max), DecryptByKey(sa.STT_CD))  
			END										AS State
			,CASE
				WHEN ISNULL(ord.CSG_LVL_ID,0) = 0 THEN ISNULL(oc.FRST_NME + ' ' + oc.LST_NME,oc1.FRST_NME + ' ' + oc1.LST_NME)
				ELSE ISNULL(CONVERT(VARCHAR (max), DecryptByKey(sc.FRST_NME)) + ' ' + CONVERT(VARCHAR (max), DecryptByKey(sc.LST_NME))
					 ,CONVERT(VARCHAR (max), DecryptByKey(sc1.FRST_NME)) + ' ' + CONVERT(VARCHAR (max), DecryptByKey(sc1.LST_NME))) 
			END                                     AS IPM
			,'(' + ISNULL(oc.NPA,oc1.NPA) + ') ' + ISNULL(oc.NXX,oc1.NXX) + '-'+ ISNULL(oc.STN_NBR,oc1.STN_NBR)     AS Phone
			,li.ZSCLR_QUOTE_ID                                 AS QuoteID
			,CONVERT(VARCHAR,fo.CUST_CMMT_DT,101)              AS CCD
			,nte.NTE_TXT                                       AS Note
		FROM dbo.FSA_ORDR fo WITH (NOLOCK)
			INNER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM li WITH (NOLOCK) ON 
							li.ORDR_ID = fo.ORDR_ID
			INNER JOIN dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = fo.ORDR_ID
			INNER JOIN dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.ORDR_ID = fo.ORDR_ID
													AND foc.CIS_LVL_TYPE in ('H6')
			INNER JOIN dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.ORDR_ID = fo.ORDR_ID
													AND oa.CIS_LVL_TYPE in  ('H6')
			LEFT OUTER JOIN dbo.CUST_SCRD_DATA sa WITH (NOLOCK) 
				ON sa.SCRD_OBJ_ID = oa.ORDR_ADR_ID and sa.SCRD_OBJ_TYPE_ID = 14
			
			LEFT OUTER JOIN dbo.ORDR_CNTCT oc WITH (NOLOCK) ON oc.ORDR_ID = fo.ORDR_ID											
													AND oc.ROLE_ID = 99
													AND oc.CIS_LVL_TYPE in  ('H6')
			LEFT OUTER JOIN dbo.CUST_SCRD_DATA sc WITH (NOLOCK) 
				ON sc.SCRD_OBJ_ID = oc.ORDR_CNTCT_ID and sc.SCRD_OBJ_TYPE_ID = 15
			
			LEFT OUTER JOIN dbo.ORDR_CNTCT oc1 WITH (NOLOCK) ON oc1.ORDR_ID = fo.ORDR_ID											
													AND oc1.ROLE_ID = 99
													AND oc1.CIS_LVL_TYPE in  ('H1')
			LEFT OUTER JOIN dbo.CUST_SCRD_DATA sc1 WITH (NOLOCK) 
				ON sc1.SCRD_OBJ_ID = oc1.ORDR_CNTCT_ID and sc1.SCRD_OBJ_TYPE_ID = 15
			LEFT OUTER JOIN (SELECT top 1 ORDR_ID, NTE_TYPE_ID, NTE_TXT from dbo.ORDR_NTE WITH (NOLOCK)
				where ORDR_ID = @ORDR_ID AND NTE_TYPE_ID = 27 order by CREAT_DT DESC)nte on nte.ORDR_ID = fo.ORDR_ID 
					
			
				
		WHERE fo.ORDR_ID = @ORDR_ID	and ISNULL(li.ZSCLR_QUOTE_ID,'') <> ''  
		
		
		SELECT DISTINCT MDS_DES, ORDR_QTY from FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
				 WHERE MFR_NME = 'ZSCA'
						AND ORDR_ID = @ORDR_ID
						AND ITM_STUS = 401		 
			
	END TRY
	BEGIN CATCH
		exec [dbo].[insertErrorInfo]
	END CATCH
END

