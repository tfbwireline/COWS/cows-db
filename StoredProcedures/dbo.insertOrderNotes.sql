USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[insertOrderNotes]    Script Date: 01/07/2020 12:41:03 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/13/2011
-- Description:	Inserts the FSA NOtes into the ORDR_NTE table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertOrderNotes]
	@ORDR_ID		Int	
	,@NTE_TYPE_ID	TinyInt
	,@NTE_TXT		Varchar(Max) = ''
	,@CREAT_BY_USER_ID	INT = NULL	
	
AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY

	IF ISNULL(@CREAT_BY_USER_ID,0) = 0
		SET @CREAT_BY_USER_ID = 1
		 
	INSERT INTO dbo.ORDR_NTE WITH (ROWLOCK)
					(ORDR_ID
					,NTE_TYPE_ID
					,NTE_TXT
					,CREAT_BY_USER_ID)
		VALUES		(@ORDR_ID
					,@NTE_TYPE_ID
					,@NTE_TXT
					,@CREAT_BY_USER_ID)
END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH					
END