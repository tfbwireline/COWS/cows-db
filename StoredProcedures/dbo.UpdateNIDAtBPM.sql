USE [COWS]
GO

_CreateObject 'SP'
	,'dbo'
	,'UpdateNIDAtBPM'
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================            
-- Author:  jrg7298            
-- Create date: 12/01/2019
-- Description: Update NID Serial Number in BPM using FTN/NUA
-- km967761 - 01/28/2021 - Accommodate multiple FSA_CPE_LINE_ITEM via @NidActy Table
-- =============================================            
ALTER PROCEDURE [dbo].[UpdateNIDAtBPM] @OrdrId INT = - 1
	,@H6 CHAR(9) = ''
	,@NIDSerialNbr VARCHAR(100)
	,@EventID INT = - 1
	,@ModfdByUserID INT
AS
BEGIN
	BEGIN TRY
	
		DECLARE @dSQL NVARCHAR(MAX) = ''
		DECLARE @NTE_TXT VARCHAR(4000) = ''
			,@M5OrdrNbr VARCHAR(100) = ''
		DECLARE @IP_ADR VARCHAR(15), @IP_MSTR_ID INT = -1, @CNCL_NID_ACTY_ID INT = -1, @CNCL_IP_ACTY_ID INT = -1

		IF (@OrdrId > 0)
		BEGIN
			SELECT TOP 1 @M5OrdrNbr = fo.FTN
				,@H6 = CONVERT(VARCHAR, od.H5_H6_CUST_ID)
			FROM dbo.FSA_ORDR fo WITH (NOLOCK)
			INNER JOIN dbo.ORDR od WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			WHERE od.ORDR_ID = @OrdrId
		END

		--IF a IP is already assigned then bypass
		IF EXISTS(SELECT 'x' FROM dbo.IP_MSTR ims WITH (NOLOCK) INNER JOIN
					dbo.IP_ACTY iac WITH (NOLOCK) ON iac.IP_MSTR_ID=ims.IP_MSTR_ID INNER JOIN
					dbo.NID_ACTY na WITH (NOLOCK) ON na.NID_ACTY_ID = iac.NID_ACTY_ID
					WHERE na.H6=@H6
					  AND ims.REC_STUS_ID=253--Used
					  AND na.NID_SERIAL_NBR=@NIDSerialNbr
					  AND na.REC_STUS_ID=251
					  AND iac.REC_STUS_ID=251)
			RETURN;

		
		SELECT TOP 1 @IP_ADR = ims.IP_ADR, @IP_MSTR_ID = ims.IP_MSTR_ID, @CNCL_NID_ACTY_ID = na.NID_ACTY_ID, @CNCL_IP_ACTY_ID = iac.IP_ACTY_ID
		FROM dbo.IP_MSTR ims WITH (NOLOCK) INNER JOIN
		dbo.IP_ACTY iac WITH (NOLOCK) ON iac.IP_MSTR_ID=ims.IP_MSTR_ID INNER JOIN
		dbo.NID_ACTY na WITH (NOLOCK) ON na.NID_ACTY_ID = iac.NID_ACTY_ID INNER JOIN
		dbo.FSA_ORDR_CPE_LINE_ITEM fcl WITH (NOLOCK) ON na.FSA_CPE_LINE_ITEM_ID=fcl.FSA_CPE_LINE_ITEM_ID and fcl.ITM_STUS=402
		WHERE na.H6=@H6
		  AND ims.REC_STUS_ID IN (251,254)--90dayHold/cancelled/Back to Active after 90days release
		  AND na.NID_SERIAL_NBR=@NIDSerialNbr
		  AND na.M5_ORDR_NBR!=@M5OrdrNbr
		  AND na.REC_STUS_ID=251
		ORDER BY ims.MODFD_DT DESC
		
		--Assign IP Address for NID
		IF (ISNULL(@IP_MSTR_ID,-1)=-1)
		BEGIN
			SELECT TOP 1 @IP_ADR = ims.IP_ADR, @IP_MSTR_ID = ims.IP_MSTR_ID
			FROM dbo.IP_MSTR ims WITH (NOLOCK) INNER JOIN
			dbo.LK_IP_MSTR lim WITH (NOLOCK) ON convert(tinyint,PARSENAME(ims.IP_ADR,4))>=convert(tinyint,PARSENAME(lim.STRT_IP_ADR,4)) AND convert(tinyint,PARSENAME(ims.IP_ADR,4))<=PARSENAME(lim.END_IP_ADR,4) AND convert(tinyint,PARSENAME(ims.IP_ADR,3))>=convert(tinyint,PARSENAME(lim.STRT_IP_ADR,3)) AND convert(tinyint,PARSENAME(ims.IP_ADR,3))<=PARSENAME(lim.END_IP_ADR,3)
			WHERE ims.REC_STUS_ID=251
			  AND lim.REC_STUS_ID=251
			  AND lim.SYSTM_CD='NID-CE'
			order by convert(tinyint,PARSENAME(ims.IP_ADR,4)) asc, convert(tinyint,PARSENAME(ims.IP_ADR,3)) asc, convert(tinyint,PARSENAME(ims.IP_ADR,2)) asc, convert(tinyint,PARSENAME(ims.IP_ADR,1)) asc
		END

		DECLARE @NidActy TABLE(NidActyId INT NOT NULL)

		IF (@EventID != -1)
		BEGIN
			INSERT INTO @NidActy
			SELECT NID_ACTY_ID
				FROM dbo.NID_ACTY nac WITH (NOLOCK)
				WHERE EVENT_ID = @EventID
				  AND H6 = @H6
				  AND NID_SERIAL_NBR = @NIDSerialNbr
				  AND REC_STUS_ID = 251
				ORDER BY NID_ACTY_ID DESC
		END
		ELSE
		BEGIN
			INSERT INTO @NidActy
			SELECT NID_ACTY_ID
			FROM dbo.NID_ACTY nac WITH (NOLOCK)
			WHERE M5_ORDR_NBR = @M5OrdrNbr
			  AND NID_SERIAL_NBR = @NIDSerialNbr
			  AND REC_STUS_ID = 251
			ORDER BY NID_ACTY_ID DESC
			
			UPDATE dbo.NID_ACTY
			SET H6 = @H6
			WHERE NID_ACTY_ID IN (SELECT NidActyId FROM @NidActy)
		END
		
		IF (((SELECT COUNT(1) FROM @NidActy) > 0) AND (@IP_MSTR_ID>-1))
		BEGIN
			
			IF (LEN(@NIDSerialNbr) > 0)
			BEGIN
				SET @dSQL = 'EXECUTE ( ''begin BPMF_OWNER.PKG_BPMF_COWS.proc_BPM_COWS_NID_DETAILS_UPDT(''''' + @H6 + ''''',''''' + @NIDSerialNbr + ''''',''''' + @IP_ADR + '''''); end;'') AT NRMBPM;'

				EXEC sp_executesql @dSQL
				
				INSERT INTO dbo.IP_ACTY (IP_MSTR_ID, NID_ACTY_ID, REC_STUS_ID, CREAT_BY_USER_ID, CREAT_DT)
				SELECT DISTINCT @IP_MSTR_ID, NidActyId, 251, @ModfdByUserID, GETDATE() FROM @NidActy

				IF (ISNULL(@CNCL_IP_ACTY_ID,-1) != -1)
				BEGIN
					UPDATE dbo.NID_ACTY SET REC_STUS_ID=252, MODFD_BY_USER_ID=@ModfdByUserID, MODFD_DT=GETDATE() WHERE NID_ACTY_ID = @CNCL_NID_ACTY_ID
					UPDATE dbo.IP_ACTY SET REC_STUS_ID=252, MODFD_BY_USER_ID=@ModfdByUserID, MODFD_DT=GETDATE() WHERE IP_ACTY_ID = @CNCL_IP_ACTY_ID
				END
				UPDATE dbo.IP_MSTR SET REC_STUS_ID=253, MODFD_BY_USER_ID=@ModfdByUserID, MODFD_DT=GETDATE() WHERE IP_MSTR_ID = @IP_MSTR_ID

				--If there are active NID Acty, IP Acty, IPs on same Mach5 order #,device and Diff S/N, then update those statuses to 252,254
				DECLARE @PrevNIDSerialNbr VARCHAR(100)='', @PrevIPADR VARCHAR(15)='', @PrevNIDACTYID INT=-1, @PrevIPACTYID INT =-1, @PrevIPMSTRID INT = -1, @INSTL_EVENT_ID INT = -1
				SELECT TOP 1 @PrevNIDSerialNbr = na.NID_SERIAL_NBR, @PrevIPADR =  ims.IP_ADR, @PrevNIDACTYID = na.NID_ACTY_ID, @PrevIPACTYID = ia.IP_ACTY_ID, @PrevIPMSTRID=ia.IP_MSTR_ID, @INSTL_EVENT_ID = na.EVENT_ID
				FROM dbo.NID_ACTY na WITH (NOLOCK) INNER JOIN
				dbo.IP_ACTY ia WITH (NOLOCK) ON na.NID_ACTY_ID=ia.NID_ACTY_ID INNER JOIN
				dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID=ia.IP_MSTR_ID
				WHERE na.NID_SERIAL_NBR != @NIDSerialNbr
					AND na.REC_STUS_ID = 251
					AND ia.REC_STUS_ID = 251
					AND ims.REC_STUS_ID = 253
					AND M5_ORDR_NBR = @M5OrdrNbr
				
				IF ((@PrevNIDACTYID > -1) AND (@PrevIPMSTRID > -1))
				BEGIN
					SET @NTE_TXT = 'NID Serial Number - ' + @PrevNIDSerialNbr + ' with NID IP - ' + @PrevIPADR + ' is put to 90day Hold due to NID S/N change by Tech from Order/H6 - ' + @M5OrdrNbr + '/' + @H6
					INSERT INTO dbo.ORDR_NTE
					WITH (ROWLOCK) (
							ORDR_ID
							,NTE_TYPE_ID
							,NTE_TXT
							,CREAT_BY_USER_ID
							)
					VALUES (
						@OrdrId
						,6
						,@NTE_TXT
						,@ModfdByUserID
						)
					UPDATE dbo.NID_ACTY
					SET REC_STUS_ID=252,
					MODFD_BY_USER_ID=@ModfdByUserID,
					MODFD_DT=GETDATE()
					WHERE NID_ACTY_ID=@PrevNIDACTYID

					UPDATE dbo.IP_ACTY
					SET REC_STUS_ID=252,
					MODFD_BY_USER_ID=@ModfdByUserID,
					MODFD_DT=GETDATE()
					WHERE IP_ACTY_ID=@PrevIPACTYID
				
					UPDATE dbo.IP_MSTR
					SET REC_STUS_ID=254,--90dayhold
					MODFD_BY_USER_ID=@ModfdByUserID,
					MODFD_DT=GETDATE()
					WHERE IP_MSTR_ID=@PrevIPMSTRID
					
					IF (@INSTL_EVENT_ID>-1)
					BEGIN
						INSERT INTO dbo.EVENT_HIST (
							EVENT_ID
							,ACTN_ID
							,CMNT_TXT
							,CREAT_BY_USER_ID
							,CREAT_DT
							)
						VALUES (
							@INSTL_EVENT_ID
							,20
							,@NTE_TXT
							,@ModfdByUserID
							,GETDATE()
							)
					END
				END
			END

			IF (@EventID > 0)
			SET @NTE_TXT = 'NID Serial Number - ' + @NIDSerialNbr + ', NID IP - ' + @IP_ADR + ' updated in BPM from MDS Event - ' + CONVERT(VARCHAR, @EventID)
			ELSE IF (@OrdrId > 0)
			SET @NTE_TXT = 'NID Serial Number - ' + @NIDSerialNbr + ', NID IP - ' + @IP_ADR + ' updated in BPM from Order/H6 - ' + @M5OrdrNbr + '/' + @H6

			IF (
					(@EventID > 0)
					AND (LEN(@NIDSerialNbr) > 0)
					)
			BEGIN
				INSERT INTO dbo.EVENT_HIST (
					EVENT_ID
					,ACTN_ID
					,CMNT_TXT
					,CREAT_BY_USER_ID
					,CREAT_DT
					)
				VALUES (
					@EventID
					,20
					,@NTE_TXT
					,@ModfdByUserID
					,GETDATE()
					)
			END
			ELSE IF (
					(@OrdrId > 0)
					AND (LEN(@NIDSerialNbr) > 0)
					)
			BEGIN
				INSERT INTO dbo.ORDR_NTE
				WITH (ROWLOCK) (
						ORDR_ID
						,NTE_TYPE_ID
						,NTE_TXT
						,CREAT_BY_USER_ID
						)
				VALUES (
					@OrdrId
					,6
					,@NTE_TXT
					,@ModfdByUserID
					)
			END
		
		END
	END TRY

	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END