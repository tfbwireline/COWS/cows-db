USE [COWS]
GO
_CreateObject 'SP','dbo','updateMach5Response'
GO
/****** Object:  StoredProcedure [dbo].[updateMach5Response]    Script Date: 03/02/2016 09:47:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		dlp0278
-- Create date: 03/30/2015
-- Description:	SP to update status of MACH5 outbound messages
-- =========================================================
ALTER PROCEDURE [dbo].[updateMach5Response]
	@TRAN_ID		Int
	,@ORDR_ID		Int
	,@STUS_ID		Int
	,@ERRMSG			VARCHAR(MAX)
AS
BEGIN

SET NOCOUNT ON;

Begin Try

	UPDATE		dbo.M5_ORDR_MSG	WITH (ROWLOCK)
	SET		STUS_ID		=	@STUS_ID, SENT_DT = GETDATE()
	WHERE	M5_TRAN_ID		=	@TRAN_ID
	  AND ORDR_ID = @ORDR_ID
	  AND  STUS_ID=10
	  
	UPDATE		dbo.M5_EVENT_MSG	WITH (ROWLOCK)
	SET		STUS_ID		=	@STUS_ID, SENT_DT = GETDATE()
	WHERE	M5_EVENT_MSG_ID		=	@TRAN_ID
	  AND M5_ORDR_ID = @ORDR_ID
	  AND  STUS_ID=10

End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch

END

