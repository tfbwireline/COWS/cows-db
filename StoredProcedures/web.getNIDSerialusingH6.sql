USE [COWS]
GO
/****** Object:  StoredProcedure [web].[getNIDSerialusingH6]    Script Date: 1/18/2022 1:48:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================            
-- Author:  jrg7298           
-- Create date: 3/2/2020       
-- Description: Get the latest NID Serial #s using H6s
-- km967761 - 01/14/2021 - Updated result to synced NRMBPM NID_HOST_NME
-- km967761 - 03/09/2021 - Updated result to consider MANUAL EDITED VALUE from NID_ACTY
-- km967761 - 03/10/2021 - Added ment.EVENT_ID condition to filter out specific event records
-- km967761 - 07/07/2021 - Updated NID fetching to group by H6 with MAX(NID_ACTY_ID)
-- =============================================            
ALTER PROCEDURE [web].[getNIDSerialusingH6] -- exec web.getNIDSerialusingH6 13325, '928668103,928668094'
	@EVENT_ID INT,
	@H6  VARCHAR(max)
AS               
BEGIN            
SET NOCOUNT ON;
BEGIN TRY

IF OBJECT_ID(N'tempdb..#H6Table', N'U') IS NOT NULL         
	DROP TABLE #H6Table 

CREATE TABLE #H6Table
(
h6 CHAR(9) NULL,
nidactyid int NULL,
nidserialnbr VARCHAR(100) NULL,
nidhostname VARCHAR(100) NULL,
nidipadr VARCHAR(15) NULL
)

--insert into #H6Table (h6)
--select * from web.ParseCommaSeparatedStrings(@H6)

--update h6t
--set nidactyid=x.NID_ACTY_ID, nidserialnbr=x.NID_SERIAL_NBR, nidhostname=x.NID_HOST_NME, nidipadr=x.IP_ADR
--from #H6Table h6t inner join
--(
----select distinct ment.ASSOC_H6, nac.NID_SERIAL_NBR, nac.NID_HOST_NME, ims.IP_ADR, nac.CREAT_DT from 
----dbo.mds_event_ntwk_trpt ment with (nolock) inner join
----#H6Table ht on ht.h6=ment.ASSOC_H6 inner join
----dbo.ORDR ord WITH (NOLOCK) on convert(varchar,ord.H5_H6_CUST_ID)=ment.ASSOC_H6 inner join
----dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON fc.ORDR_ID=ord.ORDR_ID AND fc.CMPNT_FMLY='NID' AND fc.SPRINT_MNTD_FLG='Y' AND fc.ITM_STUS=401 inner join
----dbo.NID_ACTY nac WITH (NOLOCK) ON nac.FSA_CPE_LINE_ITEM_ID = fc.FSA_CPE_LINE_ITEM_ID AND nac.REC_STUS_ID=251 left join
----dbo.IP_ACTY iac WITH (NOLOCK) ON nac.NID_ACTY_ID = iac.NID_ACTY_ID AND iac.REC_STUS_ID=251 left join
----dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID = iac.IP_MSTR_ID AND ims.REC_STUS_ID IN (251,253)
----WHERE ment.REC_STUS_ID = 1
----	AND ment.EVENT_ID = @EVENT_ID
--select distinct ment.ASSOC_H6, nac.NID_ACTY_ID, ims.IP_ADR, nac.NID_SERIAL_NBR, nac.NID_HOST_NME, nac.CREAT_DT from 
--dbo.mds_event_ntwk_trpt ment with (nolock) inner join
--#H6Table ht on ht.h6=ment.ASSOC_H6 inner join
--dbo.ORDR ord WITH (NOLOCK) on convert(varchar,ord.H5_H6_CUST_ID)=ment.ASSOC_H6 inner join
--dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON fc.ORDR_ID=ord.ORDR_ID AND fc.CMPNT_FMLY='NID' AND fc.SPRINT_MNTD_FLG='Y' AND fc.ITM_STUS=401 inner join
--dbo.NID_ACTY nac WITH (NOLOCK) ON (nac.FSA_CPE_LINE_ITEM_ID = fc.FSA_CPE_LINE_ITEM_ID AND nac.REC_STUS_ID=251 AND nac.M5_ORDR_NBR NOT LIKE 'CN%' AND nac.M5_ORDR_NBR NOT LIKE 'DC%') inner join
--dbo.IP_ACTY iac WITH (NOLOCK) ON nac.NID_ACTY_ID = iac.NID_ACTY_ID AND iac.REC_STUS_ID=251 inner join
--dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID = iac.IP_MSTR_ID AND ims.REC_STUS_ID =253
--WHERE ment.REC_STUS_ID = 1
--	AND ment.EVENT_ID = @EVENT_ID
--) as x on x.ASSOC_H6=h6t.h6

INSERT INTO #H6Table (h6, nidactyid)
select nac.H6, MAX(nac.NID_ACTY_ID)
from dbo.mds_event_ntwk_trpt ment with (nolock)
	inner join (select StringID as ASSOC_H6 from web.ParseCommaSeparatedStrings(@H6)) ht on ht.ASSOC_H6=ment.ASSOC_H6
	inner join dbo.ORDR ord WITH (NOLOCK) on convert(varchar,ord.H5_H6_CUST_ID)=ment.ASSOC_H6
	inner join dbo.FSA_ORDR_CPE_LINE_ITEM fc WITH (NOLOCK) ON fc.ORDR_ID=ord.ORDR_ID AND fc.CMPNT_FMLY='NID' AND fc.SPRINT_MNTD_FLG='Y' AND fc.ITM_STUS=401
	inner join dbo.NID_ACTY nac WITH (NOLOCK) ON (nac.FSA_CPE_LINE_ITEM_ID = fc.FSA_CPE_LINE_ITEM_ID AND nac.REC_STUS_ID=251 AND nac.M5_ORDR_NBR NOT LIKE 'CN%' AND nac.M5_ORDR_NBR NOT LIKE 'DC%')
	--inner join dbo.IP_ACTY iac WITH (NOLOCK) ON nac.NID_ACTY_ID = iac.NID_ACTY_ID AND iac.REC_STUS_ID=251
	--inner join dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID = iac.IP_MSTR_ID AND ims.REC_STUS_ID =253
	and not exists (select 'x' from dbo.ordr ord2 with (nolock) inner join 
	dbo.FSA_ORDR_CPE_LINE_ITEM fcl2 with (nolock) on fcl2.ORDR_ID=ord2.ORDR_ID AND fcl2.CMPNT_FMLY='NID' AND fcl2.SPRINT_MNTD_FLG='Y' AND fcl2.ITM_STUS = 401 
	where convert(varchar,ord2.H5_H6_CUST_ID)=ment.ASSOC_H6 and ord2.ordr_id>ord.ordr_id ) --Making sure there is no latest cpe order with NID for this H6
WHERE ment.REC_STUS_ID = 1
	AND ment.EVENT_ID = @EVENT_ID
GROUP BY nac.H6


--SELECT * FROM #H6Table
update h6t
set nidactyid=x.NID_ACTY_ID, nidserialnbr=x.NID_SERIAL_NBR, nidhostname=x.NID_HOST_NME, nidipadr=x.IP_ADR
from #H6Table h6t inner join
(
	SELECT nac.H6, nac.NID_ACTY_ID, nac.NID_SERIAL_NBR, nac.NID_HOST_NME, ims.IP_ADR
	FROM NID_ACTY nac
		inner join dbo.IP_ACTY iac WITH (NOLOCK) ON nac.NID_ACTY_ID = iac.NID_ACTY_ID AND iac.REC_STUS_ID=251
		inner join dbo.IP_MSTR ims WITH (NOLOCK) ON ims.IP_MSTR_ID = iac.IP_MSTR_ID AND ims.REC_STUS_ID =253
) as x on x.NID_ACTY_ID=h6t.nidactyid

if exists(select 'x' from dbo.mds_event me with (nolock) inner join dbo.MPLS_EVENT_ACTY_TYPE mea with (nolock) on mea.EVENT_ID=me.EVENT_ID where mea.MPLS_ACTY_TYPE_ID=21 and me.event_id=@EVENT_ID)
begin
update h6t
set nidactyid=x.NID_ACTY_ID, nidserialnbr=x.NID_SERIAL_NBR, nidhostname=x.NID_HOST_NME, nidipadr=x.IP_ADR
from #H6Table h6t inner join
(
select distinct nac.NID_ACTY_ID, ment.ASSOC_H6, NULL as IP_ADR, nac.NID_SERIAL_NBR, nac.NID_HOST_NME, nac.CREAT_DT from 
dbo.mds_event_ntwk_trpt ment with (nolock) inner join
#H6Table ht on ht.h6=ment.ASSOC_H6 inner join
dbo.NID_ACTY nac WITH (NOLOCK) ON (nac.h6=ht.h6 and  nac.EVENT_ID = ment.EVENT_ID AND nac.REC_STUS_ID=251 AND nac.FSA_CPE_LINE_ITEM_ID IS NULL)
WHERE ment.REC_STUS_ID = 1
	AND ment.EVENT_ID = @EVENT_ID
) as x on x.ASSOC_H6=h6t.h6 and isnull(h6t.nidserialnbr,'') = ''
end

--select distinct * from #H6Table
DECLARE @H6s VARCHAR(MAX)
select @H6s = ISNULL(@H6s,'') + ISNULL(H6,'') + ''''', '''''
from (SELECT DISTINCT H6 FROM #H6Table ) x
if(len(@H6s)>2)
set @H6s = substring(@H6s, 1, len(@H6s)-6)

declare @nid nvarchar(max) = 'UPDATE a SET nidhostname = b.NID_HOST_NME FROM #H6Table a LEFT JOIN (SELECT * from openquery(NRMBPM, ''SELECT DISTINCT NID_HOST_NME, H6_ID from BPMF_OWNER.V_BPMF_CE_COWS_ACTV_EVENT WHERE H6_ID IN ('''''+@H6s+''''') '')) AS b ON b.H6_ID = a.h6 WHERE ISNULL(a.nidserialnbr,'''') != '''' '
exec sp_executesql @nid

--update h6t
--set nidserialnbr=x.NID_SERIAL_NBR, nidhostname=ISNULL(nidhostname,x.NID_HOST_NME), nidipadr=x.IP_ADR
--from #H6Table h6t inner join
--(
--select distinct ment.ASSOC_H6, nac.NID_SERIAL_NBR, nac.NID_HOST_NME, NULL AS [IP_ADR], nac.CREAT_DT from 
--dbo.mds_event_ntwk_trpt ment with (nolock) inner join
--#H6Table ht on ht.h6=ment.ASSOC_H6 left join
--dbo.NID_ACTY nac WITH (NOLOCK) ON nac.H6 = ht.h6 AND nac.EVENT_ID = ment.EVENT_ID AND nac.REC_STUS_ID=251
--WHERE ment.REC_STUS_ID = 1
--	AND ment.EVENT_ID = @EVENT_ID
--) as x on x.ASSOC_H6=h6t.h6
--WHERE h6t.nidserialnbr IS NULL

select distinct * from #H6Table

END Try            
            
Begin Catch            
 EXEC [dbo].[insertErrorInfo]            
END Catch            
END  
