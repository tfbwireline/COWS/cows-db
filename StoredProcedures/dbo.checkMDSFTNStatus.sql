USE [COWS]
GO
_CreateObject 'SP','dbo','checkMDSFTNStatus'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		sbg9814
-- Create date: 09/29/2011
-- Description:	Checks the Status of an FTN and checks to see if it is in Review Complete State or NOT.
-- =========================================================
ALTER PROCEDURE [dbo].[checkMDSFTNStatus]
	@FTN		varchar(20)
	,@RetVal	Int		OUTPUT
AS
BEGIN
SET NOCOUNT ON;
Begin Try
DECLARE	@OrderID	Int
SET	@OrderID	=	0

IF EXISTS
	(
		SELECT 'X'
			FROM	dbo.FSA_ORDR WITH (NOLOCK)
			WHERE	FTN	= @FTN
				AND ORDR_TYPE_CD IN ('DC','CN')
	)
	BEGIN
		SET @RetVal = -2
		
	END
ELSE
	BEGIN
		SELECT		@OrderID	=	ISNULL(fo.ORDR_ID, 0)
			FROM	dbo.FSA_ORDR	fo	WITH (NOLOCK) INNER JOIN
					dbo.ORDR		od	WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			WHERE	fo.FTN	=	@FTN
				AND od.ORDR_STUS_ID = 1            
				AND od.ordr_cat_id  IN (2,6)
				AND fo.ORDR_TYPE_CD NOT IN ('CN', 'DC')
											   AND fo.ordr_actn_id = 2
												AND		((fo.PROD_TYPE_CD IN ('MN','SE'))
														OR (
															(fo.PROD_TYPE_CD NOT IN ('MN','SE')) 
																AND (
																		(ISNULL(fo.CPE_CPE_ORDR_TYPE_CD,'') = 'MNS') 
																		OR (ISNULL(fo.TTRPT_MNGD_DATA_SRVC_CD,'') = 'Y')
																	)
														   ))
			
			IF	@OrderID	=	0
			OR	EXISTS
				(SELECT 'X' FROM dbo.ACT_TASK	ct	WITH (NOLOCK)
					INNER JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID=ct.ORDR_ID
					WHERE		ct.ORDR_ID	=	@OrderID
						AND 	fo.ORDR_TYPE_CD != 'BC'
						AND		(	(ct.TASK_ID	=	1000		AND		ct.STUS_ID	=	3)
								OR	(ct.TASK_ID	IN	(300, 301)	AND		ct.STUS_ID	=	2)
								)
				)
			OR	EXISTS
				(SELECT 'X' FROM dbo.ACT_TASK	ct	WITH (NOLOCK)
					INNER JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID=ct.ORDR_ID
					WHERE		ct.ORDR_ID	=	@OrderID
						AND 	fo.ORDR_TYPE_CD = 'BC'
						AND		(ct.TASK_ID	=	1000		AND		ct.STUS_ID	=	0)
				)
			OR EXISTS
				(SELECT 'X' FROM dbo.ORDR od WITH (NOLOCK)
				   INNER JOIN dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID=od.ORDR_ID
					WHERE od.ORDR_ID = @OrderID
						AND 	fo.ORDR_TYPE_CD != 'BC'
						AND ((od.ORDR_STUS_ID = 4)
							 OR (EXISTS (SELECT 'X'
										 FROM dbo.LK_PPRT lp WITH (NOLOCK)
										 WHERE lp.PPRT_ID = od.PPRT_ID
										  AND  lp.PPRT_NME LIKE '%Cancel%')))
				)	
				BEGIN
					SET	@RetVal	=	@OrderID
				END
			ELSE
				BEGIN
					SET	@RetVal	=	-1
				END
	END
End Try

Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
END
