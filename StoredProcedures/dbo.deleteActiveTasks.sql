USE [COWS]
GO
_CreateObject 'SP','dbo','deleteActiveTasks'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 07/14/2011
-- Description:	Deletes the Active Tasks for a given Order ID.
-- =========================================================
ALTER PROCEDURE [dbo].[deleteActiveTasks]
	@ORDR_ID	Int
AS
BEGIN
SET NOCOUNT ON;
Begin Try
	-----------------------------------------------------------------------------
	--	WFM Changes.
	-----------------------------------------------------------------------------
	EXEC dbo.WFMComplete @ORDR_ID, 1, 0, ''

	-----------------------------------------------------------------------------
	--	Delete the Pre-Submit data from the tables.
	-----------------------------------------------------------------------------
	DELETE  FROM dbo.ACT_TASK		WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
	DELETE  FROM dbo.WG_PROF_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID
	DELETE  FROM dbo.WG_PTRN_STUS	WITH (ROWLOCK) WHERE ORDR_ID = @ORDR_ID

End Try

Begin Catch
	EXEC dbo.insertErrorInfo
End Catch	
END