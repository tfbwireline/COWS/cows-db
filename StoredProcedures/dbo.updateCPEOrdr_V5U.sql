USE [COWS]
GO
_CreateObject 'SP','dbo','updateCPEOrdr_V5U'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		dlp0278
-- Create date: 04/5/2016
-- Description:	Modifies Domestic CPE Orders.
-- exec [dbo].[updateCPEOrdr_V5U] 10823, 4,'dlp0278',null, null, 'aurco9811'

-- =========================================================
ALTER PROCEDURE [dbo].[updateCPEOrdr_V5U] 
		@ORDR_ID		INT,
		@MOD_TYP        INT,
		@ADID			VARCHAR(10),
		@JCODE          VARCHAR(100) = null,
		@COMMENTS       VARCHAR(MAX) = 'NOTE',
		@CLLI			VARCHAR(20) = null

AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY

		DECLARE	@USER_ID INT
		DECLARE @NTE_TXT VARCHAR(1000)
		DECLARE @ORIG_CLLI VARCHAR(20)
		DECLARE @NTE_ID INT
		
		
		SELECT @USER_ID = USER_ID FROM dbo.LK_USER WITH (NOLOCK)
			WHERE USER_ACF2_ID = @ADID
		
	
		IF @MOD_TYP = 1 -- ADD Note
			BEGIN
				SET @NTE_TXT = @COMMENTS 
				
				INSERT INTO [dbo].[ORDR_NTE]
				   ([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT],[MODFD_BY_USER_ID],[MODFD_DT])
					VALUES
				   (6,@ORDR_ID,GETDATE(),ISNULL(@USER_ID,1),1,@NTE_TXT,NULL,NULL)
			END
		IF @MOD_TYP = 2 -- ADD Hold
			BEGIN	
				SET @NTE_TXT = @JCODE + ' -- ' + ISNULL(@COMMENTS,'')
				
				INSERT INTO [dbo].[ORDR_NTE]
				   ([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT],[MODFD_BY_USER_ID],[MODFD_DT])
					VALUES
				   (6,@ORDR_ID,GETDATE(),@USER_ID,1,@NTE_TXT,NULL,NULL)
				   
				 UPDATE ACT_TASK
				 SET STUS_ID = 156, MODFD_DT = GETDATE()
				 WHERE ORDR_ID = @ORDR_ID AND (STUS_ID = 0 AND TASK_ID IN (600, 601, 602, 1000))
				 
				 
				 SELECT @NTE_ID = MAX(NTE_ID) FROM dbo.ORDR_NTE WITH (NOLOCK)
					WHERE  ORDR_ID = @ORDR_ID AND NTE_TYPE_ID = 6
				 
				 INSERT INTO [COWS].[dbo].[ORDR_JPRDY]
					([ORDR_ID],[JPRDY_CD],[NTE_ID],[CREAT_DT])
				 VALUES
					(@ORDR_ID,LEFT(@JCODE,3),@NTE_ID,GETDATE())		 
				 
			END
			
		IF @MOD_TYP = 3 -- Remove Jeopardy Code
			BEGIN	
				SET @NTE_TXT = ' Hold lifted --  ' + ISNULL(@COMMENTS,'')
				
				INSERT INTO [dbo].[ORDR_NTE]
				   ([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT],[MODFD_BY_USER_ID],[MODFD_DT])
					VALUES
				   (6,@ORDR_ID,GETDATE(),@USER_ID,1,@NTE_TXT,NULL,NULL)  
				   
				 UPDATE ACT_TASK
				 SET STUS_ID = 0, MODFD_DT = GETDATE()
				 WHERE ORDR_ID = @ORDR_ID AND (STUS_ID = 156 AND TASK_ID IN (600, 601, 602, 1000))
			END
			
		IF @MOD_TYP = 4 -- Update Serving CLLI
			BEGIN
				SELECT @ORIG_CLLI = ISNULL(CPE_CLLI,'no CLLI ') FROM dbo.ORDR WITH (NOLOCK)
					WHERE ORDR_ID = @ORDR_ID 
				
				SET @NTE_TXT = 'Original CLLI ' + @ORIG_CLLI + ' was changed to ' + @CLLI + '.  --' + ISNULL(@COMMENTS,'')
				
				INSERT INTO [dbo].[ORDR_NTE]
				   ([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT],[MODFD_BY_USER_ID],[MODFD_DT])
				VALUES
				   (6,@ORDR_ID,GETDATE(),@USER_ID,1,@NTE_TXT,NULL,NULL)
				   
				UPDATE ORDR 
				SET CPE_CLLI = @CLLI
				WHERE ORDR_ID = @ORDR_ID 
			END
		IF @MOD_TYP = 5 -- ADD JCODE
			BEGIN	
				SET @NTE_TXT = @JCODE + ' -- ' + ISNULL(@COMMENTS,'')
				
				INSERT INTO [dbo].[ORDR_NTE]
				   ([NTE_TYPE_ID],[ORDR_ID],[CREAT_DT],[CREAT_BY_USER_ID],[REC_STUS_ID],[NTE_TXT],[MODFD_BY_USER_ID],[MODFD_DT])
					VALUES
				   (6,@ORDR_ID,GETDATE(),@USER_ID,1,@NTE_TXT,NULL,NULL)
		 				 
				 SELECT @NTE_ID = MAX(NTE_ID) FROM dbo.ORDR_NTE WITH (NOLOCK)
					WHERE  ORDR_ID = @ORDR_ID AND NTE_TYPE_ID = 6
				 
				 INSERT INTO [COWS].[dbo].[ORDR_JPRDY]
					([ORDR_ID],[JPRDY_CD],[NTE_ID],[CREAT_DT])
				 VALUES
					(@ORDR_ID,LEFT(@JCODE,3),@NTE_ID,GETDATE())		 
				 
			END
 	
	
END TRY

BEGIN Catch
	EXEC [dbo].[insertErrorInfo]
END Catch

END

