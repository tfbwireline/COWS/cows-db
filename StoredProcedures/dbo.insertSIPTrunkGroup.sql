USE [COWS]
GO
_CreateObject 'SP','dbo','insertSIPTrunkGroup'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 03/05/2012
-- Description:	Inserts a record into the SIP_TRUNK_GRP Table.
-- =========================================================
ALTER PROCEDURE [dbo].[insertSIPTrunkGroup]
	@SIP_TRNK_GRP_ID	Int	OUTPUT
	,@ORDR_ID			Int
	,@CMPNT_ID			Int
	,@IS_INSTL_CD		Bit
	,@SIP_TRNK_QTY		Int
	,@NW_USER_ADR		Varchar(15)	=	NULL
AS
BEGIN
SET NOCOUNT ON;

Begin Try
	INSERT INTO	dbo.SIP_TRNK_GRP	WITH (ROWLOCK)
				(ORDR_ID
				,CMPNT_ID
				,IS_INSTL_CD
				,SIP_TRNK_QTY
				,NW_USER_ADR)	
		VALUES	(@ORDR_ID
				,@CMPNT_ID
				,@IS_INSTL_CD
				,@SIP_TRNK_QTY
				,@NW_USER_ADR)
			
	SELECT	@SIP_TRNK_GRP_ID	=	SCOPE_IDENTITY()		

End Try
Begin Catch
	EXEC [dbo].[insertErrorInfo]
End Catch
					
END
GO