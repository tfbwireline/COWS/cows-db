USE [COWS]
GO
_CreateObject 'SP','dbo','insertCustScrdLog'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--- =============================================
---- Author:		Jagannath Gangi
---- Create date: 7/18/2018
---- Description:	Insert logs for secured customers into CUST_SCRD_LOG
-- kh946640 07/26/18 Added logic to split the input log message if it is more 
-- than 7500 chars in length and concatinate encrypted message to save.
------ =============================================
	

ALTER PROCEDURE [dbo].[insertCustScrdLog] 
@SCRD_OBJ_ID int,
@SCRD_OBJ_TYPE_ID tinyint,
@LogMsg VARCHAR(MAX)

AS
BEGIN
SET NOCOUNT ON;
BEGIN TRY

OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

select @LogMsg = LTRIM(RTRIM(@LogMsg))

DECLARE @index INT = 1
DECLARE @len INT = LEN(@LogMsg)
DECLARE @LogMsgPartial Varbinary(max) = 0x
DECLARE @char Varchar(8000)


WHILE @index <= @len
BEGIN
	set @char = SUBSTRING(@LogMsg, @index, 7500)
	select @LogMsgPartial =  @LogMsgPartial + dbo.encryptString(@char)
	
	SET @index = @index + 7500
END

IF (@len > 7500)
	BEGIN		
		INSERT INTO dbo.CUST_SCRD_LOG (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, LOG_MSG, CREAT_DT)
		SELECT @SCRD_OBJ_ID, @SCRD_OBJ_TYPE_ID, @LogMsgPartial, GETDATE()
	END	
ELSE
	BEGIN
		INSERT INTO dbo.CUST_SCRD_LOG (SCRD_OBJ_ID, SCRD_OBJ_TYPE_ID, LOG_MSG, CREAT_DT)
		SELECT @SCRD_OBJ_ID, @SCRD_OBJ_TYPE_ID, dbo.encryptString(@LogMsg), GETDATE()

	END	

END TRY

BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END

