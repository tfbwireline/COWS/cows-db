USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[GetDashBoardData]    Script Date: 11/6/2013 10:21:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='GetDashBoardData')
EXEC ('CREATE PROCEDURE [dbo].[GetDashBoardData] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

-- =============================================================================
-- Author:		jrg7298
-- Create date: 03/20/2013
-- Description:	Gets Data for DashBoard.
-- =============================================================================
ALTER PROCEDURE [dbo].[GetDashBoardData] --'ALL'
@Type VARCHAR(10)
AS
BEGIN
SET NOCOUNT ON;
Begin Try

IF ((@Type = 'CHARGE') OR (@Type = 'ALL'))
BEGIN
	DECLARE @CostTable TABLE (MonthYear VARCHAR(8), AMNCIMRC MONEY NULL, AMNCINRC MONEY NULL, ENCIMRC MONEY NULL, ENCINRC MONEY NULL, ANCIMRC MONEY NULL, ANCINRC MONEY NULL)
	INSERT INTO @CostTable (MonthYear, AMNCIMRC, AMNCINRC)
	SELECT SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,x.[Year]) + '-' + cast(x.[Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,x.[Year]), 3, 2) AS [MonthYear], SUM(CONVERT(MONEY, MRC_CHG_AMT)) as 'AMNCI MRC', SUM(CONVERT(MONEY, NRC_CHG_AMT)) as 'AMNCI NRC'
		FROM (
			SELECT fob.MRC_CHG_AMT, fob.NRC_CHG_AMT, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.[ORDR_STUS_ID], od.[CREAT_DT]
			FROM COWS.[dbo].[FSA_ORDR_BILL_LINE_ITEM] AS fob WITH (NOLOCK)
			INNER JOIN COWS.[dbo].[ORDR] AS od WITH (NOLOCK) ON fob.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE od.RGN_ID = 1
			  AND (od.[ORDR_STUS_ID] = 2) 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND ((fob.MRC_CHG_AMT IS NOT NULL) OR (fob.NRC_CHG_AMT IS NOT NULL))
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			) AS x
		GROUP BY x.[Month], x.[Year]
		ORDER BY x.Year asc, x.Month asc

	UPDATE ct
	SET ct.ENCIMRC = y.ENCIMRC,
		ct.ENCINRC = y.ENCINRC
	FROM @CostTable ct INNER JOIN
		(SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,x.[Year]) + '-' + cast(x.[Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,x.[Year]), 3, 2) AS [MonthYear], SUM(CONVERT(MONEY, MRC_CHG_AMT)) as 'ENCIMRC', SUM(CONVERT(MONEY, NRC_CHG_AMT)) as 'ENCINRC'
		FROM (
			SELECT fob.MRC_CHG_AMT, fob.NRC_CHG_AMT, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.[ORDR_STUS_ID], od.[CREAT_DT]
			FROM COWS.[dbo].[FSA_ORDR_BILL_LINE_ITEM] AS fob WITH (NOLOCK)
			INNER JOIN COWS.[dbo].[ORDR] AS od WITH (NOLOCK) ON fob.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE od.RGN_ID = 2
			  AND (od.[ORDR_STUS_ID] = 2) 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND ((fob.MRC_CHG_AMT IS NOT NULL) OR (fob.NRC_CHG_AMT IS NOT NULL))
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			) AS x
		GROUP BY x.[Month], x.[Year]) y ON y.[MonthYear] = ct.MonthYear

	UPDATE ct
	SET ct.ANCIMRC = y.ANCIMRC,
		ct.ANCINRC = y.ANCINRC
	FROM @CostTable ct INNER JOIN
		(SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,x.[Year]) + '-' + cast(x.[Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,x.[Year]), 3, 2) AS [MonthYear], SUM(CONVERT(MONEY, MRC_CHG_AMT)) as 'ANCIMRC', SUM(CONVERT(MONEY, NRC_CHG_AMT)) as 'ANCINRC'
		FROM (
			SELECT fob.MRC_CHG_AMT, fob.NRC_CHG_AMT, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.[ORDR_STUS_ID], od.[CREAT_DT]
			FROM COWS.[dbo].[FSA_ORDR_BILL_LINE_ITEM] AS fob WITH (NOLOCK)
			INNER JOIN COWS.[dbo].[ORDR] AS od WITH (NOLOCK) ON fob.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE od.RGN_ID = 3
			  AND (od.[ORDR_STUS_ID] = 2) 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND ((fob.MRC_CHG_AMT IS NOT NULL) OR (fob.NRC_CHG_AMT IS NOT NULL))
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			) AS x
		GROUP BY x.[Month], x.[Year]) y ON y.[MonthYear] = ct.MonthYear


	--INSERT INTO @CostTable (MonthYear, AMNCIMRC, AMNCINRC)
	--SELECT  CONVERT(VARCHAR,x.[Month]) +'/'+ CONVERT(VARCHAR,x.[Year]) AS [Month/Year], SUM(CONVERT(MONEY, ACCS_CUST_MRC_IN_USD_AMT)) as 'AMNCI MRC', SUM(CONVERT(MONEY, [ACCS_CUST_NRC_IN_USD_AMT])) as 'AMNCI NRC'
	--	FROM (
	--		SELECT cc.[ACCS_CUST_NRC_IN_USD_AMT], cc.ACCS_CUST_MRC_IN_USD_AMT, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.[ORDR_STUS_ID], od.[CREAT_DT]
	--		FROM [dbo].[CKT] AS [ct]
	--		INNER JOIN [dbo].[CKT_COST] AS [cc] ON [ct].[CKT_ID] = [cc].[CKT_ID]
	--		INNER JOIN [dbo].[ORDR] AS od WITH (NOLOCK) ON ct.[ORDR_ID] = (od.[ORDR_ID])
	--		WHERE od.RGN_ID = 1
	--		  AND (od.[ORDR_STUS_ID] = 2) 
	--		  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
	--			AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
	--		  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	--		) AS x
	--	GROUP BY x.[Month], x.[Year]
	--	ORDER BY x.Year asc, x.Month asc

	--UPDATE ct
	--SET ct.ENCIMRC = y.ENCIMRC,
	--	ct.ENCINRC = y.ENCINRC
	--FROM @CostTable ct INNER JOIN
	--	(SELECT  CONVERT(VARCHAR,x.[Month]) +'/'+ CONVERT(VARCHAR,x.[Year]) AS [Month/Year], SUM(CONVERT(MONEY, ACCS_CUST_MRC_IN_USD_AMT)) as 'ENCIMRC', SUM(CONVERT(MONEY, [ACCS_CUST_NRC_IN_USD_AMT])) as 'ENCINRC'
	--	FROM (
	--		SELECT cc.[ACCS_CUST_NRC_IN_USD_AMT], cc.ACCS_CUST_MRC_IN_USD_AMT, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.[ORDR_STUS_ID], od.[CREAT_DT]
	--		FROM [dbo].[CKT] AS [ct]
	--		INNER JOIN [dbo].[CKT_COST] AS [cc] ON [ct].[CKT_ID] = [cc].[CKT_ID]
	--		INNER JOIN [dbo].[ORDR] AS od WITH (NOLOCK) ON ct.[ORDR_ID] = (od.[ORDR_ID])
	--		WHERE od.RGN_ID = 2
	--		  AND (od.[ORDR_STUS_ID] = 2) 
	--		  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
	--			AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
	--		  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	--		) AS x
	--	GROUP BY x.[Month], x.[Year]) y ON y.[Month/Year] = ct.MonthYear

	--UPDATE ct
	--SET ct.ANCIMRC = y.ANCIMRC,
	--	ct.ANCINRC = y.ANCINRC
	--FROM @CostTable ct INNER JOIN
	--	(SELECT  CONVERT(VARCHAR,x.[Month]) +'/'+ CONVERT(VARCHAR,x.[Year]) AS [Month/Year], SUM(CONVERT(MONEY, ACCS_CUST_MRC_IN_USD_AMT)) as 'ANCIMRC', SUM(CONVERT(MONEY, [ACCS_CUST_NRC_IN_USD_AMT])) as 'ANCINRC'
	--	FROM (
	--		SELECT cc.[ACCS_CUST_NRC_IN_USD_AMT], cc.ACCS_CUST_MRC_IN_USD_AMT, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.[ORDR_STUS_ID], od.[CREAT_DT]
	--		FROM [dbo].[CKT] AS [ct]
	--		INNER JOIN [dbo].[CKT_COST] AS [cc] ON [ct].[CKT_ID] = [cc].[CKT_ID]
	--		INNER JOIN [dbo].[ORDR] AS od WITH (NOLOCK) ON ct.[ORDR_ID] = (od.[ORDR_ID])
	--		WHERE od.RGN_ID = 3
	--		  AND (od.[ORDR_STUS_ID] = 2) 
	--		  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
	--			AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
	--		  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	--		) AS x
	--	GROUP BY x.[Month], x.[Year]) y ON y.[Month/Year] = ct.MonthYear
	 
	SELECT MonthYear as 'MonthYear',
		   COALESCE(AMNCIMRC,0) as 'AMNCIMRC',
		   COALESCE(AMNCINRC,0) as 'AMNCINRC',
		   COALESCE(ENCIMRC,0) as 'ENCIMRC',
		   COALESCE(ENCINRC,0) as 'ENCINRC',
		   COALESCE(ANCIMRC,0) as 'ANCIMRC',
		   COALESCE(ANCINRC,0) as 'ANCINRC'
	FROM @CostTable  


END

IF ((@Type = 'INSTINT') OR (@Type = 'ALL'))
BEGIN
	--[dbo].[sp_AMNCIRptSprintFacilitySubmitted]
	--[dbo].[sp_AMNCIRptSprintFacilityRevenueDataInsert]
	--[dbo].[sp_AMNCIRptSprintFacilityRevenueSummary]
	SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,x.[Year]) + '-' + cast(x.[Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,x.[Year]), 3, 2) AS [MonthYear], COALESCE(AVG(NoOfDays),0) AS [AverageInstallationInterval]
		FROM (
			SELECT SUM(DATEDIFF(day, fo.ORDR_SBMT_DT, at.CREAT_DT)) AS NoOfDays, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.ORDR_ID
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN COWS.dbo.ACT_TASK AS at WITH (NOLOCK) ON od.ORDR_ID = at.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			LEFT OUTER JOIN COWS.[dbo].[H5_FOLDR] h5 ON od.H5_FOLDR_ID = h5.H5_FOLDR_ID 
			LEFT OUTER JOIN COWS.[dbo].[LK_CTRY] lkc ON h5.ctry_cd = lkc.ctry_cd
			WHERE (od.[ORDR_STUS_ID] = 2) 
			  AND at.TASK_ID = 1001
			  AND od.DMSTC_CD = 1
			  AND fo.ORDR_ACTN_ID = 2
			  AND lkc.CTRY_CD IN ('UK', 'DE', 'FR', 'IT', 'NL')
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			  GROUP BY DATEPART(Month, od.[CREAT_DT]), DATEPART(Year, od.[CREAT_DT]), od.ORDR_ID
			) AS x
		GROUP BY x.[Month], x.[Year]
		ORDER BY x.Year asc, x.Month asc
END

IF ((@Type = 'INSTVOL') OR (@Type = 'ALL'))
BEGIN
	SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,[Year]) + '-' + cast([Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,[Year]), 3, 2) as [MonthYear], [1] AS 'AMNCI', [2] AS 'ENCI', [3] AS 'ANCI'
		FROM(
			SELECT od.ORDR_ID, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) as [Month], DATEPART(Year, od.[CREAT_DT]) as [Year]
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE (fo.ordr_type_cd = 'IN') 
			  AND (od.RGN_ID IN (1,2,3))
			  AND od.DMSTC_CD = 1
			  AND fo.ORDR_ACTN_ID = 2
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			  --GROUP BY DATEPART(Month, od.[CREAT_DT]), DATEPART(Year, od.[CREAT_DT]), od.RGN_ID
			) AS x
			PIVOT (COUNT(ORDR_ID) FOR RGN_ID IN ([1],[2],[3])) as pvt
			ORDER BY pvt.[Year] asc, pvt.[Month] asc
END

IF ((@Type = 'NEWVOL') OR (@Type = 'ALL'))
BEGIN
	SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,[Year]) + '-' + cast([Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,[Year]), 3, 2) as [MonthYear], [1] AS 'AMNCI', [2] AS 'ENCI', [3] AS 'ANCI'
		FROM(
			SELECT od.ORDR_ID, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) as [Month], DATEPART(Year, od.[CREAT_DT]) as [Year]
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) INNER JOIN
				 COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
				 INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE (od.RGN_ID IN (1,2,3))
			  AND od.DMSTC_CD = 1
			  AND fo.ORDR_ACTN_ID = 2
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			) AS x
			PIVOT (COUNT(ORDR_ID) FOR RGN_ID IN ([1],[2],[3])) as pvt
			ORDER BY pvt.[Year] asc, pvt.[Month] asc
END

IF (@Type = 'DUMP')
BEGIN
	SELECT fo.FTN, fob.MRC_CHG_AMT AS 'MRCAmount', fob.NRC_CHG_AMT AS 'NRCAmount', rg.RGN_DES AS 'Region', od.[CREAT_DT] AS 'OrderSubmissionDate'
			FROM COWS.[dbo].[FSA_ORDR_BILL_LINE_ITEM] AS fob WITH (NOLOCK)
			INNER JOIN COWS.[dbo].[ORDR] AS od WITH (NOLOCK) ON fob.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON fo.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.LK_XNCI_RGN AS rg WITH (NOLOCK) ON rg.RGN_ID = od.RGN_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
	WHERE (od.[ORDR_STUS_ID] = 2) 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND ((fob.MRC_CHG_AMT IS NOT NULL) OR (fob.NRC_CHG_AMT IS NOT NULL))
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	ORDER BY od.CREAT_DT DESC

	SELECT fo.FTN, rg.RGN_DES AS 'Region', lkc.CTRY_NME AS 'Country', od.[CREAT_DT] AS 'OrderSubmissionDate', DATEDIFF(day, fo.ORDR_SBMT_DT, at.CREAT_DT) AS NoOfDays
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN COWS.dbo.ACT_TASK AS at WITH (NOLOCK) ON od.ORDR_ID = at.ORDR_ID
			INNER JOIN COWS.dbo.LK_XNCI_RGN AS rg WITH (NOLOCK) ON rg.RGN_ID = od.RGN_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			LEFT OUTER JOIN COWS.[dbo].[H5_FOLDR] h5 ON od.H5_FOLDR_ID = h5.H5_FOLDR_ID 
			LEFT OUTER JOIN COWS.[dbo].[LK_CTRY] lkc ON h5.ctry_cd = lkc.ctry_cd
	WHERE (od.[ORDR_STUS_ID] = 2) 
			  AND at.TASK_ID = 1001
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND lkc.CTRY_CD IN ('UK', 'DE', 'FR', 'IT', 'NL')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	ORDER BY od.CREAT_DT DESC

	SELECT fo.FTN, rg.RGN_DES AS 'Region', lt.ORDR_TYPE_DES AS 'OrderType', od.[CREAT_DT] AS 'OrderSubmissionDate'
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN COWS.dbo.LK_XNCI_RGN AS rg WITH (NOLOCK) ON rg.RGN_ID = od.RGN_ID
			INNER JOIN COWS.dbo.LK_ORDR_TYPE AS lt WITH (NOLOCK) ON lt.FSA_ORDR_TYPE_CD = fo.ORDR_TYPE_CD
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
	WHERE (fo.ORDR_TYPE_CD = 'IN') 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND (od.RGN_ID IN (1,2,3))
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	ORDER BY od.CREAT_DT DESC

	SELECT fo.FTN, rg.RGN_DES AS 'Region', lt.ORDR_TYPE_DES AS 'OrderType', od.[CREAT_DT] AS 'OrderSubmissionDate'
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN COWS.dbo.LK_XNCI_RGN AS rg WITH (NOLOCK) ON rg.RGN_ID = od.RGN_ID
			INNER JOIN COWS.dbo.LK_ORDR_TYPE AS lt WITH (NOLOCK) ON lt.FSA_ORDR_TYPE_CD = fo.ORDR_TYPE_CD
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
	WHERE fo.ORDR_ACTN_ID = 2
			AND od.DMSTC_CD = 1 
			AND (od.RGN_ID IN (1,2,3))
			AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	ORDER BY od.CREAT_DT DESC
END

IF (@Type = 'DUMPALL')
BEGIN

	DECLARE @CostTable2 TABLE (MonthYear VARCHAR(8), AMNCIMRC MONEY NULL, AMNCINRC MONEY NULL, ENCIMRC MONEY NULL, ENCINRC MONEY NULL, ANCIMRC MONEY NULL, ANCINRC MONEY NULL)
	INSERT INTO @CostTable2 (MonthYear, AMNCIMRC, AMNCINRC)
	SELECT SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,x.[Year]) + '-' + cast(x.[Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,x.[Year]), 3, 2) AS [MonthYear], SUM(CONVERT(MONEY, MRC_CHG_AMT)) as 'AMNCI MRC', SUM(CONVERT(MONEY, NRC_CHG_AMT)) as 'AMNCI NRC'
		FROM (
			SELECT fob.MRC_CHG_AMT, fob.NRC_CHG_AMT, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.[ORDR_STUS_ID], od.[CREAT_DT]
			FROM COWS.[dbo].[FSA_ORDR_BILL_LINE_ITEM] AS fob WITH (NOLOCK)
			INNER JOIN COWS.[dbo].[ORDR] AS od WITH (NOLOCK) ON fob.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE od.RGN_ID = 1
			  AND (od.[ORDR_STUS_ID] = 2) 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND ((fob.MRC_CHG_AMT IS NOT NULL) OR (fob.NRC_CHG_AMT IS NOT NULL))
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			) AS x
		GROUP BY x.[Month], x.[Year]
		ORDER BY x.Year asc, x.Month asc

	UPDATE ct
	SET ct.ENCIMRC = y.ENCIMRC,
		ct.ENCINRC = y.ENCINRC
	FROM @CostTable2 ct INNER JOIN
		(SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,x.[Year]) + '-' + cast(x.[Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,x.[Year]), 3, 2) AS [MonthYear], SUM(CONVERT(MONEY, MRC_CHG_AMT)) as 'ENCIMRC', SUM(CONVERT(MONEY, NRC_CHG_AMT)) as 'ENCINRC'
		FROM (
			SELECT fob.MRC_CHG_AMT, fob.NRC_CHG_AMT, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.[ORDR_STUS_ID], od.[CREAT_DT]
			FROM COWS.[dbo].[FSA_ORDR_BILL_LINE_ITEM] AS fob WITH (NOLOCK)
			INNER JOIN COWS.[dbo].[ORDR] AS od WITH (NOLOCK) ON fob.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE od.RGN_ID = 2
			  AND (od.[ORDR_STUS_ID] = 2) 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND ((fob.MRC_CHG_AMT IS NOT NULL) OR (fob.NRC_CHG_AMT IS NOT NULL))
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			) AS x
		GROUP BY x.[Month], x.[Year]) y ON y.[MonthYear] = ct.MonthYear

	UPDATE ct
	SET ct.ANCIMRC = y.ANCIMRC,
		ct.ANCINRC = y.ANCINRC
	FROM @CostTable2 ct INNER JOIN
		(SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,x.[Year]) + '-' + cast(x.[Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,x.[Year]), 3, 2) AS [MonthYear], SUM(CONVERT(MONEY, MRC_CHG_AMT)) as 'ANCIMRC', SUM(CONVERT(MONEY, NRC_CHG_AMT)) as 'ANCINRC'
		FROM (
			SELECT fob.MRC_CHG_AMT, fob.NRC_CHG_AMT, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.[ORDR_STUS_ID], od.[CREAT_DT]
			FROM COWS.[dbo].[FSA_ORDR_BILL_LINE_ITEM] AS fob WITH (NOLOCK)
			INNER JOIN COWS.[dbo].[ORDR] AS od WITH (NOLOCK) ON fob.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE od.RGN_ID = 3
			  AND (od.[ORDR_STUS_ID] = 2) 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND ((fob.MRC_CHG_AMT IS NOT NULL) OR (fob.NRC_CHG_AMT IS NOT NULL))
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			) AS x
		GROUP BY x.[Month], x.[Year]) y ON y.[MonthYear] = ct.MonthYear

	SELECT MonthYear as 'MonthYear',
		   COALESCE(AMNCIMRC,0) as 'AMNCIMRC',
		   COALESCE(AMNCINRC,0) as 'AMNCINRC',
		   COALESCE(ENCIMRC,0) as 'ENCIMRC',
		   COALESCE(ENCINRC,0) as 'ENCINRC',
		   COALESCE(ANCIMRC,0) as 'ANCIMRC',
		   COALESCE(ANCINRC,0) as 'ANCINRC'
	FROM @CostTable2

	SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,x.[Year]) + '-' + cast(x.[Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,x.[Year]), 3, 2) AS [MonthYear], COALESCE(AVG(NoOfDays),0) AS [AverageInstallationInterval]
		FROM (
			SELECT SUM(DATEDIFF(day, fo.ORDR_SBMT_DT, at.CREAT_DT)) AS NoOfDays, DATEPART(Month, od.[CREAT_DT]) AS [Month], DATEPART(Year, od.[CREAT_DT]) AS [Year], od.ORDR_ID
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN COWS.dbo.ACT_TASK AS at WITH (NOLOCK) ON od.ORDR_ID = at.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			LEFT OUTER JOIN COWS.[dbo].[H5_FOLDR] h5 ON od.H5_FOLDR_ID = h5.H5_FOLDR_ID 
			LEFT OUTER JOIN COWS.[dbo].[LK_CTRY] lkc ON h5.ctry_cd = lkc.ctry_cd
			WHERE (od.[ORDR_STUS_ID] = 2) 
			  AND at.TASK_ID = 1001
			  AND od.DMSTC_CD = 1
			  AND fo.ORDR_ACTN_ID = 2
			  AND lkc.CTRY_CD IN ('UK', 'DE', 'FR', 'IT', 'NL')
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			  GROUP BY DATEPART(Month, od.[CREAT_DT]), DATEPART(Year, od.[CREAT_DT]), od.ORDR_ID
			) AS x
		GROUP BY x.[Month], x.[Year]
		ORDER BY x.Year asc, x.Month asc

	SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,[Year]) + '-' + cast([Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,[Year]), 3, 2) as [MonthYear], [1] AS 'AMNCI', [2] AS 'ENCI', [3] AS 'ANCI'
		FROM(
			SELECT od.ORDR_ID, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) as [Month], DATEPART(Year, od.[CREAT_DT]) as [Year]
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE (fo.ordr_type_cd = 'IN') 
			  AND (od.RGN_ID IN (1,2,3))
			  AND od.DMSTC_CD = 1
			  AND fo.ORDR_ACTN_ID = 2
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			  --GROUP BY DATEPART(Month, od.[CREAT_DT]), DATEPART(Year, od.[CREAT_DT]), od.RGN_ID
			) AS x
			PIVOT (COUNT(ORDR_ID) FOR RGN_ID IN ([1],[2],[3])) as pvt
			ORDER BY pvt.[Year] asc, pvt.[Month] asc

	SELECT  SUBSTRING(DateName(MM, convert(datetime, CONVERT(VARCHAR,[Year]) + '-' + cast([Month] as varchar) + '-01', 120)), 1, 3) + '''' + SUBSTRING(CONVERT(VARCHAR,[Year]), 3, 2) as [MonthYear], [1] AS 'AMNCI', [2] AS 'ENCI', [3] AS 'ANCI'
		FROM(
			SELECT od.ORDR_ID, od.RGN_ID, DATEPART(Month, od.[CREAT_DT]) as [Month], DATEPART(Year, od.[CREAT_DT]) as [Year]
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) INNER JOIN
				 COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
				 INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			WHERE (od.RGN_ID IN (1,2,3))
			  AND od.DMSTC_CD = 1
			  AND fo.ORDR_ACTN_ID = 2
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
			) AS x
			PIVOT (COUNT(ORDR_ID) FOR RGN_ID IN ([1],[2],[3])) as pvt
			ORDER BY pvt.[Year] asc, pvt.[Month] asc


	SELECT fo.FTN, fob.MRC_CHG_AMT AS 'MRCAmount', fob.NRC_CHG_AMT AS 'NRCAmount', rg.RGN_DES AS 'Region', od.[CREAT_DT] AS 'OrderSubmissionDate'
			FROM COWS.[dbo].[FSA_ORDR_BILL_LINE_ITEM] AS fob WITH (NOLOCK)
			INNER JOIN COWS.[dbo].[ORDR] AS od WITH (NOLOCK) ON fob.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON fo.[ORDR_ID] = (od.[ORDR_ID])
			INNER JOIN COWS.dbo.LK_XNCI_RGN AS rg WITH (NOLOCK) ON rg.RGN_ID = od.RGN_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
	WHERE (od.[ORDR_STUS_ID] = 2) 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND ((fob.MRC_CHG_AMT IS NOT NULL) OR (fob.NRC_CHG_AMT IS NOT NULL))
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	ORDER BY od.CREAT_DT DESC

	SELECT fo.FTN, rg.RGN_DES AS 'Region', lkc.CTRY_NME AS 'Country', od.[CREAT_DT] AS 'OrderSubmissionDate', DATEDIFF(day, fo.ORDR_SBMT_DT, at.CREAT_DT) AS NoOfDays
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN COWS.dbo.ACT_TASK AS at WITH (NOLOCK) ON od.ORDR_ID = at.ORDR_ID
			INNER JOIN COWS.dbo.LK_XNCI_RGN AS rg WITH (NOLOCK) ON rg.RGN_ID = od.RGN_ID
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
			LEFT OUTER JOIN COWS.[dbo].[H5_FOLDR] h5 ON od.H5_FOLDR_ID = h5.H5_FOLDR_ID 
			LEFT OUTER JOIN COWS.[dbo].[LK_CTRY] lkc ON h5.ctry_cd = lkc.ctry_cd
	WHERE (od.[ORDR_STUS_ID] = 2) 
			  AND at.TASK_ID = 1001
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND lkc.CTRY_CD IN ('UK', 'DE', 'FR', 'IT', 'NL')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	ORDER BY od.CREAT_DT DESC

	SELECT fo.FTN, rg.RGN_DES AS 'Region', lt.ORDR_TYPE_DES AS 'OrderType', od.[CREAT_DT] AS 'OrderSubmissionDate'
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN COWS.dbo.LK_XNCI_RGN AS rg WITH (NOLOCK) ON rg.RGN_ID = od.RGN_ID
			INNER JOIN COWS.dbo.LK_ORDR_TYPE AS lt WITH (NOLOCK) ON lt.FSA_ORDR_TYPE_CD = fo.ORDR_TYPE_CD
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
	WHERE (fo.ORDR_TYPE_CD = 'IN') 
			  AND fo.ORDR_ACTN_ID = 2
			  AND od.DMSTC_CD = 1 
			  AND (od.RGN_ID IN (1,2,3))
			  AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	ORDER BY od.CREAT_DT DESC

	SELECT fo.FTN, rg.RGN_DES AS 'Region', lt.ORDR_TYPE_DES AS 'OrderType', od.[CREAT_DT] AS 'OrderSubmissionDate'
			FROM COWS.[dbo].[ORDR] AS od WITH (NOLOCK) 
			INNER JOIN COWS.dbo.FSA_ORDR AS fo WITH (NOLOCK) ON od.ORDR_ID = fo.ORDR_ID
			INNER JOIN COWS.dbo.LK_XNCI_RGN AS rg WITH (NOLOCK) ON rg.RGN_ID = od.RGN_ID
			INNER JOIN COWS.dbo.LK_ORDR_TYPE AS lt WITH (NOLOCK) ON lt.FSA_ORDR_TYPE_CD = fo.ORDR_TYPE_CD
			INNER JOIN	COWS.dbo.ORDR_ADR	oa WITH (NOLOCK) ON	oa.ORDR_ID			=	fo.ORDR_ID
																AND	oa.CIS_LVL_TYPE	IN	('H5','H6')
	WHERE fo.ORDR_ACTN_ID = 2
			AND od.DMSTC_CD = 1 
			AND (od.RGN_ID IN (1,2,3))
			AND	ISNULL(oa.CTRY_CD,'')	NOT IN	('GU','MP','PR','VI','US')
			  AND (((DATEPART(Month, od.[CREAT_DT]) > DATEPART(Month, GETDATE())) 
				AND ((DATEPART(Year, GETDATE())-1) = DATEPART(Year, od.[CREAT_DT])))
			  OR (DATEPART(Year, od.[CREAT_DT]) = DATEPART(Year, GETDATE())))
	ORDER BY od.CREAT_DT DESC
END

End Try

Begin Catch
	--EXEC [dbo].[insertErrorInfo]
End Catch
END
