USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getASREmailData]    Script Date: 04/15/2021 7:53:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <04/14/2015>
-- Description:	<To retrieve ASR email data>
-- =============================================
ALTER PROCEDURE [dbo].[getASREmailData]
	@orderID int
AS
BEGIN
	SET NOCOUNT ON;
 

	DECLARE @VLANlist VARCHAR(max)   
	
	SELECT @VLANlist = COALESCE(@VLANlist + ',', '') + CAST(VLAN_ID AS VARCHAR(200)) 
			FROM	(SELECT DISTINCT VLAN_ID
						FROM dbo.ORDR_VLAN WITH (NOLOCK) 
							WHERE ORDR_ID = @orderID) v


BEGIN TRY


	SELECT TOP 1 ASR_ID AS ASR_ID, 
			ORDR_ID AS OrderID,
			ASR_TYPE_ID AS ASR_TYPE_ID,
			ISNULL(IP_NODE_TXT,'') AS IP_NODE_TXT,
			ISNULL(ACCS_BDWD_DES, '') AS ACCS_BDWD_DES,
			ISNULL(ACCS_CTY_NME_SITE_CD,'') AS ACCS_CTY_NME_SITE_CD,
			ISNULL(TRNSPNT_CD,'') AS TRNSPNT_CD,
			ISNULL(LEC_NNI_NBR, 0) AS LEC_NNI_NBR,
			ISNULL(ENTRNC_ASMT_TXT, '') AS ENTRNC_ASMT_TXT,
			ISNULL(MSTR_FTN_CD,'') AS MSTR_FTN_CD,
			ISNULL(DLCI_DES,'') AS DLCI_DES,
			ISNULL(H1_MATCH_MSTR_VAS_CD,'') AS H1_MATCH_MSTR_VAS_CD,
			ISNULL(ASR_NOTES_TXT,'') AS ASR_NOTES_TXT,
			ISNULL(PTNR_CXR_CD,'') AS PTNR_CXR_CD,
			ISNULL(@VLANlist,'')  AS VLAN_IDs,
			ISNULL(INTL_DOM_EMAIL_CD,'') AS INTL_DOM_EMAIL_CD
		FROM	dbo.ASR WITH (NOLOCK)
		WHERE	STUS_ID	=	10
			AND ORDR_ID = @orderID
		
END TRY
BEGIN CATCH
	EXEC dbo.insertErrorInfo
END CATCH
    
END
