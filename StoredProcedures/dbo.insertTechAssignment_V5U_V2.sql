USE [COWS]
GO
_CreateObject 'SP','dbo','insertTechAssignment_V5U_V2'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		kh946640
-- Create date: 12/10/2019
-- Description:	Assignes the 3rd Party Tech to the order for new COWS app.
-- =========================================================
ALTER PROCEDURE [dbo].[insertTechAssignment_V5U_V2]
			@ORDR_ID int        
			,@TECH varchar(100)
			,@DSPTCH_TM  datetime
			,@EVENT_ID  int
			,@ASN_BY_USER_ADID  VARCHAR(10) 
        
AS
BEGIN
SET NOCOUNT ON;


DECLARE	@Tech_USER_ID int, @ASN_BY_USER_ID int, @Tech_ADID varchar(10)

BEGIN TRY

	SELECT @Tech_ADID = USER_ADID, @Tech_USER_ID = [USER_ID] from dbo.LK_USER WITH (NOLOCK)
			 WHERE FULL_NME = @TECH
	
	SELECT @ASN_BY_USER_ID = [USER_ID] FROM dbo.LK_USER WITH (NOLOCK)
			WHERE USER_ADID = @ASN_BY_USER_ADID  	

	IF NOT EXISTS (SELECT 'X' FROM dbo.USER_WFM_ASMT WITH (NOLOCK) WHERE ORDR_ID=@ORDR_ID AND GRP_ID=15 AND ASN_USER_ID=@Tech_USER_ID)
	BEGIN
		INSERT INTO [COWS].[dbo].[USER_WFM_ASMT]
				   ([ORDR_ID],[GRP_ID],[ASN_USER_ID],[ASN_BY_USER_ID],[ASMT_DT]
				   ,[CREAT_DT],[ORDR_HIGHLIGHT_CD],[ORDR_HIGHLIGHT_MODFD_DT],[DSPTCH_TM]
				   ,[INRTE_TM],[ONSITE_TM],[CMPLT_TM],[EVENT_ID],[USR_PRF_ID])
			 VALUES
				   (@ORDR_ID,15,@Tech_USER_ID,@ASN_BY_USER_ID,GETDATE(),GETDATE()
				   ,null,null,@DSPTCH_TM,null,null,null,@EVENT_ID,56)
	END

END TRY

BEGIN CATCH
	EXEC	[dbo].[insertErrorInfo]
END CATCH
END
GO


