﻿USE [COWS]
GO
_CreateObject 'SP','dbo','getVendorOrderForms'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Lakshmi Kadiyala
-- Create date: 12th July 2011
-- Description:	Returns all Vendor Forms for a Vendor Order
-- ================================================================

ALTER PROCEDURE [dbo].[getVendorOrderForms]
	@VendorOrderID INT 	
AS
BEGIN
SET NOCOUNT ON;

BEGIN TRY
	-- Get Vendor Order Templates
	SELECT 
		VOF.VNDR_ORDR_FORM_ID
		,VOF.VNDR_ORDR_ID
		,VOF.FILE_NME
		,VOF.FILE_SIZE_QTY
		,VOF.CREAT_DT
		,LU.FULL_NME
		,VOF.CREAT_BY_USER_ID
	FROM 
		[dbo].[VNDR_ORDR_FORM] VOF WITH (NOLOCK)
	INNER JOIN
		[dbo].[LK_USER] LU WITH (NOLOCK) ON VOF.[CREAT_BY_USER_ID] = LU.[USER_ID]
	WHERE 
		VOF.VNDR_ORDR_ID = @VendorOrderID
	AND
		VOF.TMPLT_CD = 1

	-- Get Vendor Order Forms
	SELECT
		VOF.TMPLT_ID 
		,VOF.VNDR_ORDR_FORM_ID
		,VOF.VNDR_ORDR_ID
		,VOF.FILE_NME
		,VOF.FILE_SIZE_QTY
		,VOF.CREAT_DT
		,LU.FULL_NME
		,VOF.CREAT_BY_USER_ID
	FROM 
		[dbo].[VNDR_ORDR_FORM] VOF WITH (NOLOCK)
	INNER JOIN
		[dbo].[LK_USER] LU WITH (NOLOCK) ON VOF.[CREAT_BY_USER_ID] = LU.[USER_ID]
	WHERE 
		VOF.VNDR_ORDR_ID = @VendorOrderID
	AND
		VOF.TMPLT_CD = 0
END TRY
BEGIN CATCH
	EXEC [dbo].[insertErrorInfo]
END CATCH
END
