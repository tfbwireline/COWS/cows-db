USE [COWS]
GO
_CreateObject 'SP','dbo','bypassNRMPLForSPAEOrders'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <11/16/2011>
-- Description:	<To Bypass following steps for SPA E Orders
--		1. NRM: Pending OE & PL
--		2. GOM Private Line
-- kh946640 03/22/18 COWS DB Restructuring as columns have been moved from fsa_ordr->fsa_ordr_cpe_line_item table for DualPort project(PJ020783 - CR93)
-- =============================================
ALTER PROCEDURE dbo.bypassNRMPLForSPAEOrders
	@ORDR_ID	INT,
	@TaskId		SMALLINT = 0,
	@TaskStatus	TINYINT = 0,
	@Comments	Varchar(1000) = ''		
AS
BEGIN
	BEGIN TRY
		DECLARE @TSK_NME	VARCHAR(100)
		DECLARE @RELTD_FTN	VARCHAR(20)
		DECLARE	@byPass		BIT = 0
		DECLARE @FSA_ORGNL_INSTL_ORDR_ID	VARCHAR(20)
		DECLARE @INST_ORDR_ID INT
		SET @TSK_NME = ''
		SET @INST_ORDR_ID =	0
		SELECT	@TSK_NME = SUBSTRING(ISNULL(TASK_NME,''),0,100)
			FROM	dbo.LK_TASK WITH (NOLOCK)
			WHERE	TASK_ID = @TaskId 
		SET @Comments = 'Bypassing ' + @TSK_NME + ' step for SPA E Orders. Private Line information is not needed for this product.'
		--Disconnect Orders Logic
		IF EXISTS
			(
				SELECT 'X'
					FROM	dbo.FSA_ORDR WITH (NOLOCK)
					WHERE	ORDR_ID			=	@ORDR_ID
						AND	ORDR_TYPE_CD	=	'DC'
			)
			BEGIN
				SELECT	@FSA_ORGNL_INSTL_ORDR_ID	=	ISNULL(foc.FSA_ORGNL_INSTL_ORDR_ID,''),
						@RELTD_FTN					=	ISNULL(RELTD_FTN,'')
					FROM	dbo.FSA_ORDR	f	WITH (NOLOCK)
				LEFT JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM foc with (nolock) on f.ORDR_ID = foc.ORDR_ID
																		AND	foc.LINE_ITEM_CD IN ('DSC','MDS')
					WHERE	f.ORDR_ID	=	@ORDR_ID
					
				IF @FSA_ORGNL_INSTL_ORDR_ID <> ''
					BEGIN
						SELECT @INST_ORDR_ID = ISNULL(ORDR_ID,0) 
							FROM	dbo.FSA_ORDR WITH (NOLOCK)
							WHERE	FTN	=	@FSA_ORGNL_INSTL_ORDR_ID
					END
				ELSE IF @RELTD_FTN <> ''
					BEGIN
						SELECT @INST_ORDR_ID = ISNULL(ORDR_ID,0) 
							FROM	dbo.FSA_ORDR WITH (NOLOCK)
							WHERE	FTN	=	@RELTD_FTN
					END
			END
		
		IF @ORDR_ID <> 0
			BEGIN
				IF EXISTS
					(
						SELECT		'X'
							FROM	dbo.FSA_ORDR od WITH (NOLOCK)
							join dbo.FSA_ORDR_CPE_LINE_ITEM CPED on od.ORDR_ID = CPED.ORDR_ID
							WHERE	od.ORDR_ID					IN (@ORDR_ID,@INST_ORDR_ID)
								AND	CPED.TTRPT_ACCS_TYPE_CD		=	'IP1E'
								AND	CPED.TTRPT_ACCS_ARNGT_CD		=	'SPA'
								--AND	TTRPT_ACCS_PRVDR_CD		=	'SPRT'
								--AND	ISNULL(SPA_ACCS_INDCR_DES,'') IN ('Dedicated','Aggregated')			
					)
					BEGIN 
						IF @TaskId > 0 
							EXEC dbo.CompleteActiveTask @ORDR_ID,@TaskId,2,@Comments
					END
			END
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END
GO
