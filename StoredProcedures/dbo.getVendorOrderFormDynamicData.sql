USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[getVendorOrderFormDynamicData]    Script Date: 1/18/2022 9:52:41 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ================================================================
-- Author:		Chakri Boggavarapu, Lakshmi Kadiyala, Jagannath Gangi
-- Create date: 21st Sep 2011
-- Description:	Returns basic details for given order
-- ================================================================
-- Modify:		Kyle Wichert - Modify Disconnects to pull indicated 
--				fields from last completed Install FTN.  Modify moves
--				to pull all but Section 11 from the Install half, and 
--				section 11 from the disconnect half.  And comments.
--				Per Emily, use Site instead of CustomerOnSite address
-- kh946640 03/22/18 COWS DB Restructuring as columns have been moved from fsa_ordr->fsa_ordr_cpe_line_item table for DualPort project(PJ020783 - CR93)
-- ================================================================

ALTER PROCEDURE [dbo].[getVendorOrderFormDynamicData] --1076
	@OrderID INT 	
AS
BEGIN
	BEGIN TRY
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
		DECLARE @FTN				varchar(20)
		DECLARE @CategoryID			TINYINT
		DECLARE @ORDR_TYPE_CD		CHAR(2)
		DECLARE @PRNT_ORDR_ID		INT
		DECLARE @REL_FTN			varchar(20)
		DECLARE @H5H6CustID			INT
		DECLARE @lastInstOrderID	INT
		DECLARE @lastCompletedFTN	varchar(20)
		DECLARE @Order_Sub_Type		VARCHAR(10)
		DECLARE @Order_Sub_Type_Des	VARCHAR(50)
		DECLARE @IsCancel			BIT
		DECLARE	@CancelFTN			varchar(20)

		SET @lastCompletedFTN	=	''
		SET @lastInstOrderID	=	0
		SET @CancelFTN			=	''

		--Cases
		--For a Disconnect/Change, we need to find Last Install Order and pull most information from it
		--For a Move/Up/Dn we need to present the information from the Install Half, pull one set of information from Disco
		--half regardless of which order ID is passed in
		--For Cancel, we need to take action on the order ID passed to us, then in the BF layer convert the FTNs around to display differently

		--If the Disco side of an FSA order is passed, swap out to the actual install side
		SELECT	@OrderID = f2.ORDR_ID
		FROM			dbo.FSA_ORDR	f1	WITH (NOLOCK)
			INNER JOIN	dbo.FSA_ORDR	f2	WITH (NOLOCK) ON f1.FTN = f2.RELTD_FTN
		WHERE	f1.ORDR_ID			=	@OrderID
			AND	f1.ORDR_SUB_TYPE_CD IN	('HCDN','COLODN','HCUP','COLOUP','HCMV','COLOMV','CPEHCMV','CPEREMV')
			AND	f1.ORDR_TYPE_CD		=	'DC'
			AND f2.ORDR_TYPE_CD		=	'IN'

		--Get general information
		SELECT @CategoryID = od.ORDR_CAT_ID,
			   @ORDR_TYPE_CD = fo.ORDR_TYPE_CD,
			   @PRNT_ORDR_ID = od.PRNT_ORDR_ID,
			   @REL_FTN = COALESCE(fo.RELTD_FTN, ''),
			   @H5H6CustID = ISNULL(od.H5_H6_CUST_ID,0),
			   @Order_Sub_Type = fo.ORDR_SUB_TYPE_CD,
			   @IsCancel = CASE ot.FSA_ORDR_TYPE_CD WHEN 'CN' THEN 1 ELSE 0 END
		FROM			dbo.ORDR			od	WITH (NOLOCK) 
			INNER JOIN	dbo.FSA_ORDR		fo	WITH (NOLOCK) ON od.ORDR_ID			= fo.ORDR_ID
			INNER JOIN	dbo.LK_PPRT			p	WITH (NOLOCK) ON p.PPRT_ID			= od.PPRT_ID
			INNER JOIN	dbo.LK_ORDR_TYPE	ot	WITH (NOLOCK) ON ot.ORDR_TYPE_ID	= p.ORDR_TYPE_ID 
		WHERE	od.ORDR_ID = @OrderID

		

		--Get Order Type Description
		SELECT	@Order_Sub_Type_Des =	CASE ISNULL(st.ORDR_SUB_TYPE_CD, '')	WHEN	'HCDN'		THEN	st.ORDR_SUB_TYPE_DES
																				WHEN	'COLODN'	THEN	st.ORDR_SUB_TYPE_DES
																				WHEN	'HCUP'		THEN	st.ORDR_SUB_TYPE_DES
																				WHEN	'COLOUP'	THEN	st.ORDR_SUB_TYPE_DES
																				WHEN	'HCMV'		THEN	st.ORDR_SUB_TYPE_DES
																				WHEN	'COLOMV'	THEN	st.ORDR_SUB_TYPE_DES
																				WHEN	'CPEHCMV'	THEN	st.ORDR_SUB_TYPE_DES
																				WHEN	'CPEREMV'	THEN	st.ORDR_SUB_TYPE_DES
																				ELSE	lo.ORDR_TYPE_DES
										END										
		FROM			dbo.FSA_ORDR			f	WITH (NOLOCK)
			INNER JOIN	dbo.LK_ORDR_TYPE		lo	WITH (NOLOCK) ON f.ORDR_TYPE_CD		= lo.FSA_ORDR_TYPE_CD
			LEFT JOIN	dbo.LK_ORDR_SUB_TYPE	st	WITH (NOLOCK) ON f.ORDR_SUB_TYPE_CD	= st.ORDR_SUB_TYPE_CD
		WHERE	f.ORDR_ID = @OrderID


		--Get a good order description with sub type and isCancel factored in
		IF @IsCancel = 1
		BEGIN
			SET	@Order_Sub_Type_Des = 'Cancel ' +	@Order_Sub_Type_Des

			SELECT	@CancelFTN	=	f2.FTN
			FROM			dbo.FSA_ORDR	f1	WITH (NOLOCK)
				INNER JOIN	dbo.FSA_ORDR	f2	WITH (NOLOCK) ON f1.FTN	=	f2.PRNT_FTN
			WHERE	f1.ORDR_ID	=	@OrderID
		END

		--The property flag gives only Install/Disconnect for Moves/Up/Dn.  Set the actual order type
		SET @ORDR_TYPE_CD =	CASE @Order_Sub_Type	WHEN	'HCDN'		THEN	'DN'
													WHEN	'COLODN'	THEN	'DN'
													WHEN	'HCUP'		THEN	'UP'
													WHEN	'COLOUP'	THEN	'UP'
													WHEN	'HCMV'		THEN	'MV'
													WHEN	'COLOMV'	THEN	'MV'
													WHEN	'CPEHCMV'	THEN	'MV'
													WHEN	'CPEREMV'	THEN	'MV'
													ELSE	@ORDR_TYPE_CD
							END
		

		IF(@CategoryID = 2 OR @CategoryID = 6)	--FSA
		BEGIN
				IF (@ORDR_TYPE_CD IN ('CH','DC','CN'))
				BEGIN

					--Find the Last completed order to pull data from
					--FSA *Should* be sending this information to us.  If not, check manually for completed order.  If still not found, use this order
					SELECT TOP 1	@lastCompletedFTN	=	fol.FSA_ORGNL_INSTL_ORDR_ID
					FROM	dbo.FSA_ORDR_CPE_LINE_ITEM	fol	WITH (NOLOCK) 
					WHERE	fol.ORDR_ID	= @OrderID
						AND	ISNULL(fol.FSA_ORGNL_INSTL_ORDR_ID, '') <> ''
					
					--The Disconnect has a CPE line item, but the change order gets this value saved directly into the FSA table.
					IF @lastCompletedFTN = ''
					BEGIN
						SELECT	@lastCompletedFTN	=	ISNULL(fo.FSA_ORGNL_INSTL_ORDR_ID,0)
						FROM	dbo.FSA_ORDR	fo	WITH (NOLOCK)
						WHERE	fo.ORDR_ID	=	@OrderID
							AND	@ORDR_TYPE_CD = 'CH'
					END
					
					--Get Order ID for the supplied Completed FTN
					SELECT	@lastInstOrderID	=	ORDR_ID
					FROM	dbo.FSA_ORDR WITH (NOLOCK)
					WHERE	FTN = @lastCompletedFTN

					--If the Supplied Last Install FTN is not supplied, or we don't have that FTN in our 
					--database find the last completed order ID to use in the join below.  Keep the current FTN if it was supplied
					IF @lastInstOrderID	=	0
					BEGIN
						SELECT TOP 1	@lastInstOrderID = ISNULL(o.ORDR_ID, 0),
										@lastCompletedFTN= CASE @lastCompletedFTN WHEN '' THEN ISNULL(fo.FTN, '') ELSE @lastCompletedFTN END
						FROM			dbo.ORDR		o	WITH (NOLOCK)
							INNER JOIN	dbo.FSA_ORDR	fo	WITH (NOLOCK) ON o.ORDR_ID	= fo.ORDR_ID
						WHERE	fo.ORDR_TYPE_CD	=	'IN'
							AND	o.ORDR_STUS_ID	=	2
							AND @H5H6CustID = o.H5_H6_CUST_ID
						ORDER BY ISNULL(o.CUST_CMMT_DT, fo.CUST_CMMT_DT) DESC
					END

					--If we don't have any completed orders for this customer at all, use the data from the current order itself.
					IF @lastInstOrderID = 0
					BEGIN
						SET @lastInstOrderID =	@OrderID
					END

					SELECT TOP 1
						FO.FTN, 
						FO.CUST_CMMT_DT, 
						FO.CUST_WANT_DT, 
						CASE @ORDR_TYPE_CD WHEN 'DC' THEN FO.CUST_SIGNED_DT ELSE FOP.CUST_SIGNED_DT END AS CUST_SIGNED_DT,																		--Original Install FTN when NOT disconnect
						FO.CUST_ACPT_ERLY_SRVC_CD, 
						FO.MULT_CUST_ORDR_CD,
						FO.CUST_ORDR_SBMT_DT,
						FOP.TSP_CD,																				--Original Install FTN
						ISNULL(FOCH5.CUST_NME,'')					AS 'CUST_NME',
						ISNULL(OC.FRST_NME,'')						AS 'FRST_NME',
						ISNULL(OC.LST_NME,'')						AS 'LST_NME',
						ISNULL(OC.CNTCT_NME,'') 						AS 'CNTCT_NME',
						ISNULL(OA.STREET_ADR_1,'')					AS 'STREET_ADR_1',
						ISNULL(OA.STREET_ADR_2,'')					AS 'STREET_ADR_2',
						ISNULL(OA.CTY_NME,'')						AS 'CTY_NME',
						ISNULL(OA.ZIP_PSTL_CD,'')					AS 'ZIP_PSTL_CD',
						ISNULL(OA.PRVN_NME,'') +'/'+ ISNULL(OA.STT_CD,'') PRVN_NME_STT_CD,
						LC.CTRY_NME,
						OCP.NPA,																				--Original Install FTN
						OCP.NXX,																				--Original Install FTN
						OCP.PHN_NBR															AS 'SitePhn',		--Original Install FTN
						CASE ISNULL(OC.PHN_NBR, '') 
							WHEN '' THEN OC.NPA + OC.NXX + OC.STN_NBR
							ELSE	OC.PHN_NBR
						END																	AS 'PHN_NBR',
						ISNULL(OC.EMAIL_ADR,'')						AS 'EMAIL_ADR',
						ISNULL(OCA.FRST_NME,'')						AS 'ALT_FRST_NME',	--Original Install FTN
						ISNULL(OCA.LST_NME,'')						AS 'ALT_LST_NME',	--Original Install FTN
						ISNULL(OCA.CNTCT_NME,'')						AS 'ALT_CNTCT_NME',	--Original Install FTN
						CASE ISNULL(OCA.PHN_NBR, '') 
							WHEN '' THEN OCA.NPA + OCA.NXX + OCA.STN_NBR
							ELSE  	OCA.PHN_NBR
						END																	AS 'ALT_CNTCT_PHN',	--Original Install FTN
						ISNULL(OCA.EMAIL_ADR,'')						AS 'ALT_EMAIL_ADR',	--Original Install FTN
						FOP.CUST_PRMS_OCPY_CD,																	--Original Install FTN
						@Order_Sub_Type_Des													AS 'ORDR_TYPE_DES',
						FO.FSA_EXP_TYPE_CD,
						ocli.INSTL_DSGN_DOC_NBR,																	--Original Install FTN
						FOP.PROD_TYPE_CD,																		--Original Install FTN
						CASE FOP.PROD_TYPE_CD WHEN 'DO' THEN 'Dedicated IP'
											 WHEN 'SO' THEN 'SprintLink Frame Relay'
											 WHEN 'MP' THEN 'MPLS'
											 WHEN 'MO' THEN 'MPLS'
											 ELSE FOP.PROD_TYPE_CD 
						END																	AS 'PROD_SELECTION',--Original Install FTN
						CASE ISNULL(ocli.TTRPT_ACCS_TYPE_DES,'') 
							WHEN  '' THEN CASE ISNULL(FO.TTRPT_ACCS_TYPE_DES,'') 
											WHEN '' THEN FOP.TTRPT_ACCS_TYPE_DES
											ELSE FO.TTRPT_ACCS_TYPE_DES
										  END	
							ELSE ocli.TTRPT_ACCS_TYPE_DES
						END																	AS 'LoopSpeed',
						FOP.TSUP_TELCO_DEMARC_BLDG_NME										AS BLDG,
						FOP.TSUP_TELCO_DEMARC_FLR_ID										AS FLR,
						FOP.TSUP_TELCO_DEMARC_RM_NBR										AS ROOM,
						CASE ISNULL(FOP.TSUP_TELCO_DEMARC_BLDG_NME,'')	
							WHEN '' THEN ''
							ELSE 'Bldg: ' + FOP.TSUP_TELCO_DEMARC_BLDG_NME + '  '
						END
						+ CASE ISNULL(FOP.TSUP_TELCO_DEMARC_FLR_ID,'')	
							WHEN '' THEN ''
							ELSE 'Floor: ' + FOP.TSUP_TELCO_DEMARC_FLR_ID + '  '
						END
						+ CASE ISNULL(FOP.TSUP_TELCO_DEMARC_RM_NBR,'')	
							WHEN '' THEN ''
							ELSE 'Room: ' + FOP.TSUP_TELCO_DEMARC_RM_NBR + '  '
						END																	AS 'TelcoDemarc',	--Original Install FTN
						--CPE_ACCS_PRVDR_CD AS 'CPEProvidedBy',
						FOP.TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD									AS 'CSUDSU',		--Original Install FTN
						--TPORT_CUST_ROUTR_TAG_TXT + ' ' + TPORT_CUST_ROUTR_AUTO_NEGOT_CD AS 'RouterInfo',
						ocli.TTRPT_ENCAP_CD													AS 'ENCAP',			--Original Install FTN
						ocli.TTRPT_SPD_OF_SRVC_BDWD_DES										AS 'PortSpeed',
						FOP.TTRPT_ROUTG_TYPE_CD,																--Original Install FTN
						FOP.TSUP_PRS_QOT_NBR,																	--Original Install FTN
						FOP.SCA_NBR,																			--Original Install FTN
						FOP.TTRPT_MNGD_DATA_SRVC_CD,															--Original Install FTN
						FOCH5.CUST_ID														AS 'H5_CUST_ID',
						FOCH6.CUST_ID														AS 'H6_CUST_ID',
						''																	AS 'CNTRC_TERM',
						''																	AS 'DNS_SRVC_FSTNAME',
						''																	AS 'DNS_SRVC_LSTNAME',
						FOP.TTRPT_LINK_PRCOL_CD												AS 'INTF_PROT',		--Original Install FTN
						ocli.CXR_ACCS_CD,																		--Original Install FTN
						FOP.TSUP_NEAREST_CROSS_STREET_NME,														--Original Install FTN
						ocli.TPORT_VLAN_QTY													AS 'VLANQTY',
						CASE VAS.VAS_TYPE_CD
							WHEN 'COS' THEN 'Y'
							ELSE 'N' END													AS 'VAS',			--Original Install FTN	
						ocli.TPORT_IP_VER_TYPE_CD											AS 'IPVER',			--Original Install FTN
						ocli.TPORT_IPV6_ADR_QTY												AS 'PROVIDEQTY',	--Original Install FTN
						ocli.TPORT_IPV4_ADR_PRVDR_CD 										AS 'IPV4PROVIDER',	--Original Install FTN
						ocli.TPORT_IPV6_ADR_PRVDR_CD 										AS 'IPV6PROVIDER',	--Original Install FTN
						FO.MULT_ORDR_INDEX_NBR 												AS 'INDXNBR',
						FO.MULT_ORDR_TOT_CNT 												AS 'TOTNBR',
						ISNULL(OCDA.FRST_NME,'')						AS 'DNSFSTNME',		--Original Install FTN
						ISNULL(OCDA.LST_NME,'')						AS 'DNSLSTNME',		--Original Install FTN
						ISNULL(OCDA.CNTCT_NME,'') 					AS 'DNSNME',		--Original Install FTN
						CASE ISNULL(OCDA.PHN_NBR, '') 
							WHEN '' THEN OCDA.NPA + OCDA.NXX + OCDA.STN_NBR
							ELSE	OCDA.PHN_NBR
						END																	AS 'DNSPHN',		--Original Install FTN
						ISNULL(OCDA.EMAIL_ADR,'')					AS 'DNSEMAIL',		--Original Install FTN
						FOP.NRFC_PRCOL_CD													AS 'INTPROTOCOL',	--Original Install FTN
						FOP.DOMN_NME 														AS 'DOMAIN',		--Original Install FTN
						FOP.ADDL_IP_ADR_CD													AS 'ADDIPADR',		--Original Install FTN
						FOP.TPORT_XST_IP_CNCTN_CD 											AS 'EXISTIPFLG',	--Original Install FTN
						STUFF((SELECT ',' + IP_ADR FROM dbo.FSA_IP_ADR IA 
								WHERE @PRNT_ORDR_ID = IA.ORDR_ID FOR XML PATH('')), 1, 1,'') AS 'IPLIST',
						FOP.TPORT_CURR_INET_PRVDR_CD 										AS 'IPPROVIDER',	--For Disco/Chan, supposed to display the old provider.  Set it here, instead of using if conditions in BF layer
						FOP.VNDO_CNTRC_TERM_ID												AS 'VNDRTERM',		--Original Install FTN
						CASE @ORDR_TYPE_CD	WHEN 'CH' THEN FO.ORDR_CHNG_DES
											ELSE NT.NTE_TXT
						END		 															AS 'COMMENTS',
						FOP.TTRPT_ROUTG_TYPE_CD 											AS 'PROTOCOLS',		--Original Install FTN
						--FOP.ORANGE_CMNTY_NME,																	--Original Install FTN
						--FOP.ORANGE_SITE_ID,																		--Original Install FTN
						FOP.ORANGE_OFFR_CD,																		--Original Install FTN
						ISNULL(ISNULL(OCS.CNTCT_NME,OCSP.CNTCT_NME),'')						AS 'SRVCNTNME',
						CASE ISNULL(OCS.PHN_NBR, OCSP.PHN_NBR)
							WHEN '' THEN ISNULL(OCS.NPA,OCSP.NPA) + ISNULL(OCS.NXX,OCSP.NXX) + ISNULL(OCS.STN_NBR,OCSP.STN_NBR)
							ELSE	ISNULL(OCS.PHN_NBR, OCSP.PHN_NBR)
						END																	AS 'SRVCNTPHN',
						FOP.TTRPT_JACK_NRFC_TYPE_CD 										AS 'INTERFACETYPE',	--Original Install FTN
						CASE @lastCompletedFTN WHEN '' THEN '' ELSE 	@lastCompletedFTN END	AS 'RelatedFTN',	--This is the last completed order FTN here
						@CancelFTN															AS 'PARENTFTN',		--This is the Cancel FTN (the one we don't use in workflow)
						CASE ISNULL(ocli.CXR_SRVC_ID,'') 
							WHEN  '' THEN CASE ISNULL(FO.CXR_SRVC_ID,'') 
											WHEN '' THEN FOP.CXR_SRVC_ID
											ELSE FO.CXR_SRVC_ID
										  END	
							ELSE ocli.CXR_SRVC_ID
						END																	AS 'ORIGSRVID',		--Original Install FTN					
						ocli.TPORT_IPV4_ADR_QTY 												AS 'IPV4QTY',
						FO.CUST_FRWL_CD 													AS 'Firewall',
						OCDA.PHN_EXT_NBR 													AS 'DNS_PHN_EXT',	--Original Install FTN
						OT.ORDR_TYPE_ID 													AS 'ORDER_TYPE',
						FOP.IPV4_SUBNET_MASK_ADR 											AS 'SUBMASK',		--Original Install FTN
						'' RD_CUST_NME,
						'' RD_FRST_NME,
						'' RD_LST_NME,
						'' 																	RD_CNTCT_NME,
						'' RD_STREET_ADR_1,
						'' RD_STREET_ADR_2,
						'' RD_CTY_NME,
						'' RD_ZIP_PSTL_CD,
						'' RD_PRVN_NME_STT_CD,
						'' RD_CTRY_NME,
						'' RD_PHN_NBR,
						'' RD_ALT_FRST_NME,
						'' RD_ALT_LST_NME,
						'' RD_ALT_CNTCT_NME,
						'' RD_ALT_CNTCT_PHN
					--For Disconnects, grab several fields from previous order.  LEFT JOIN on @lastInstOrderID = FOP.OrderID
					FROM			dbo.FSA_ORDR				FO		WITH (NOLOCK)
					INNER JOIN		dbo.ORDR					OD		WITH (NOLOCK) ON FO.ORDR_ID		= OD.ORDR_ID
					LEFT OUTER JOIN dbo.FSA_ORDR				FOP		WITH (NOLOCK) ON FOP.ORDR_ID	= @lastInstOrderID								--Original Install FTN
					INNER JOIN		dbo.LK_PPRT					P		WITH (NOLOCK) ON P.PPRT_ID		= OD.PPRT_ID
					INNER JOIN		dbo.LK_ORDR_TYPE			OT		WITH (NOLOCK) ON P.ORDR_TYPE_ID = OT.ORDR_TYPE_ID
					LEFT OUTER JOIN	dbo.FSA_ORDR_CUST			FOC		WITH (NOLOCK) ON FOC.ORDR_ID	= OD.PRNT_ORDR_ID	AND FOC.CIS_LVL_TYPE='H1'
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST			FOCH5	WITH (NOLOCK) ON FOCH5.ORDR_ID	= OD.ORDR_ID		AND FOCH5.CIS_LVL_TYPE IN ('H5','H6')
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST			FOCH6	WITH (NOLOCK) ON FOCH6.ORDR_ID	= OD.ORDR_ID		AND FOCH6.CIS_LVL_TYPE IN ('H5','H6')
					LEFT OUTER JOIN dbo.ORDR_CNTCT				OC		WITH (NOLOCK) ON OD.ORDR_ID		= OC.ORDR_ID		AND OC.ROLE_ID = 6 			AND OC.CIS_LVL_TYPE	IN ('H5','H6')
					LEFT OUTER JOIN dbo.ORDR_CNTCT				OCP		WITH (NOLOCK) ON FOP.ORDR_ID	= OCP.ORDR_ID		AND	OCP.CIS_LVL_TYPE	IN ('OD')	AND	OCP.CNTCT_TYPE_ID	=	17	--Original Install FTN
					LEFT OUTER JOIN dbo.ORDR_CNTCT				OCA		WITH (NOLOCK) ON FOP.ORDR_ID	= OCA.ORDR_ID		AND OCA.ROLE_ID = 7			--Original Install FTN
					LEFT OUTER JOIN dbo.ORDR_CNTCT				OCDA	WITH (NOLOCK) ON FOP.ORDR_ID	= OCDA.ORDR_ID		AND OCDA.ROLE_ID = 9		--Original Install FTN
					LEFT OUTER JOIN dbo.ORDR_CNTCT				OCS		WITH (NOLOCK) ON OD.ORDR_ID		= OCS.ORDR_ID		AND OCS.ROLE_ID = 8			
					LEFT OUTER JOIN dbo.ORDR_CNTCT				OCSP	WITH (NOLOCK) ON FOP.ORDR_ID	= OCSP.ORDR_ID		AND OCSP.ROLE_ID = 8			--Original Install FTN
					LEFT OUTER JOIN dbo.ORDR_ADR				OA		WITH (NOLOCK) ON OD.ORDR_ID		= OA.ORDR_ID		AND OA.ADR_TYPE_ID = 18
					LEFT OUTER JOIN dbo.LK_CTRY					LC		WITH (NOLOCK) ON OA.CTRY_CD		= LC.CTRY_CD
					LEFT OUTER JOIN dbo.FSA_ORDR_VAS			VAS		WITH (NOLOCK) ON FOP.ORDR_ID	= VAS.ORDR_ID									--Original Install FTN
					LEFT OUTER JOIN dbo.ORDR_NTE				NT		WITH (NOLOCK) ON FO.ORDR_ID		= NT.ORDR_ID		AND NT.NTE_TYPE_ID = 19
					LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM	ocli	WITH (NOLOCK) ON FO.ORDR_ID		= ocli.ORDR_ID
					WHERE FO.ORDR_ID = @OrderID	
				END
				ELSE IF (@ORDR_TYPE_CD IN ('UP','DN','MV'))
				BEGIN
					--Find the Last completed order to pull data from
					--FSA *Should* be sending this information to us.  If not, check manually for completed order.  If still not found, use this Disconnect side of the order
					SELECT TOP 1	@lastCompletedFTN	=	fol.FSA_ORGNL_INSTL_ORDR_ID
					FROM			dbo.FSA_ORDR				fo	WITH (NOLOCK)
						INNER JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM	fol	WITH (NOLOCK) ON fo.ORDR_ID	=	fol.ORDR_ID
					WHERE	fo.FTN = @REL_FTN
						AND	ISNULL(fol.FSA_ORGNL_INSTL_ORDR_ID, 0) <> 0

					--Get Order ID for the supplied Completed FTN
					SELECT	@lastInstOrderID	=	ORDR_ID
					FROM	dbo.FSA_ORDR WITH (NOLOCK)
					WHERE	FTN = @lastCompletedFTN

					--If the Last Install FTN is not supplied, or we don't have that FTN in our 
					--database find the last completed order ID to use in the join below.  Keep the current FTN if it was supplied
					IF @lastInstOrderID	=	0
					BEGIN
						SELECT TOP 1	@lastInstOrderID = ISNULL(o.ORDR_ID, 0),
										@lastCompletedFTN= CASE @lastCompletedFTN WHEN '' THEN ISNULL(fo.FTN, 0) ELSE @lastCompletedFTN END
						FROM			dbo.ORDR		o	WITH (NOLOCK)
							INNER JOIN	dbo.ORDR		od	WITH (NOLOCK) ON o.H5_H6_CUST_ID	=	od.H5_H6_CUST_ID
							INNER JOIN	dbo.FSA_ORDR	fo	WITH (NOLOCK) ON o.ORDR_ID			=	fo.ORDR_ID
							INNER JOIN	dbo.FSA_ORDR	fod	WITH (NOLOCK) ON od.ORDR_ID			=	fod.ORDR_ID
						WHERE	fo.ORDR_TYPE_CD	=	'IN'
							AND	o.ORDR_STUS_ID	=	2
							AND fod.FTN			=	@REL_FTN
						ORDER BY ISNULL(o.CUST_CMMT_DT, fo.CUST_CMMT_DT) DESC
					END

					IF @lastInstOrderID	=	0
					BEGIN
						SELECT	@lastInstOrderID	=	ISNULL(fo.ORDR_ID, 0)
						FROM	dbo.FSA_ORDR	fo	WITH (NOLOCK)
						WHERE	fo.FTN	=	@REL_FTN
					END


					SELECT TOP 1
						FO.FTN, 
						FO.CUST_CMMT_DT, 
						FO.CUST_WANT_DT, 
						FO.CUST_SIGNED_DT, 
						FO.CUST_ACPT_ERLY_SRVC_CD, 
						FO.MULT_CUST_ORDR_CD,
						FO.CUST_ORDR_SBMT_DT,
						FO.TSP_CD,
						ISNULL(FOCH5.CUST_NME,'')					AS 'CUST_NME',
						ISNULL(OC.FRST_NME,'')						AS 'FRST_NME',
						ISNULL(OC.LST_NME,'')						AS 'LST_NME',
						ISNULL(OC.CNTCT_NME,'') 						AS 'CNTCT_NME',
						ISNULL(OA.STREET_ADR_1,'')					AS 'STREET_ADR_1',
						ISNULL(OA.STREET_ADR_2,'')					AS 'STREET_ADR_2',
						ISNULL(OA.CTY_NME,'')						AS 'CTY_NME',
						ISNULL(OA.ZIP_PSTL_CD,'')					AS 'ZIP_PSTL_CD',
						ISNULL(OA.PRVN_NME,'') +'/'+ ISNULL(OA.STT_CD,'') AS 'PRVN_NME_STT_CD',
						LC.CTRY_NME,
						ESP.NPA,
						ESP.NXX,
						ESP.PHN_NBR															AS 'SitePhn',
						CASE ISNULL(OC.PHN_NBR, '')
							WHEN '' THEN OC.NPA + OC.NXX + OC.STN_NBR
							ELSE	OC.PHN_NBR
						END																	AS 'PHN_NBR',
						ISNULL(OC.EMAIL_ADR,'')						AS 'EMAIL_ADR',
						ISNULL(OCA.FRST_NME,'')						AS 'ALT_FRST_NME',
						ISNULL(OCA.LST_NME,'')						AS 'ALT_LST_NME',	
						ISNULL(OCA.CNTCT_NME,'')						AS 'ALT_CNTCT_NME',
						CASE ISNULL(OCA.PHN_NBR, '')
							WHEN '' THEN OCA.NPA + OCA.NXX + OCA.STN_NBR
							ELSE	OCA.PHN_NBR
						END																	AS 'ALT_CNTCT_PHN',			
						ISNULL(OCA.EMAIL_ADR,'')						AS 'ALT_EMAIL_ADR',
						FO.CUST_PRMS_OCPY_CD,
						@Order_Sub_Type_Des													AS 'ORDR_TYPE_DES',
						FO.FSA_EXP_TYPE_CD,
						CPED.INSTL_DSGN_DOC_NBR,
						FO.PROD_TYPE_CD,
						CASE FO.PROD_TYPE_CD WHEN 'DO' THEN 'Dedicated IP'
											 WHEN 'SO' THEN 'SprintLink Frame Relay'
											 WHEN 'MP' THEN 'MPLS'
											 WHEN 'MO' THEN 'MPLS'
											 ELSE FO.PROD_TYPE_CD 
						END																	AS 'PROD_SELECTION',
						FO.TTRPT_ACCS_TYPE_DES												AS 'LoopSpeed',
						FO.TSUP_TELCO_DEMARC_BLDG_NME										AS BLDG,
						FO.TSUP_TELCO_DEMARC_FLR_ID											AS FLR,
						FO.TSUP_TELCO_DEMARC_RM_NBR											AS ROOM,
						CASE ISNULL(FO.TSUP_TELCO_DEMARC_BLDG_NME,'')	
							WHEN '' THEN ''
							ELSE 'Bldg: ' + FO.TSUP_TELCO_DEMARC_BLDG_NME + '  '
						END
						+ CASE ISNULL(FO.TSUP_TELCO_DEMARC_FLR_ID,'')	
							WHEN '' THEN ''
							ELSE 'Floor: ' + FO.TSUP_TELCO_DEMARC_FLR_ID + '  '
						END
						+ CASE ISNULL(FO.TSUP_TELCO_DEMARC_RM_NBR,'')	
							WHEN '' THEN ''
							ELSE 'Room: ' + FO.TSUP_TELCO_DEMARC_RM_NBR + '  '
						END																	AS 'TelcoDemarc',
						FO.TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD									AS 'CSUDSU',
						CPED.TTRPT_ENCAP_CD													AS 'ENCAP',
						CPED.TTRPT_SPD_OF_SRVC_BDWD_DES										AS 'PortSpeed',
						FO.TTRPT_ROUTG_TYPE_CD,
						FO.TSUP_PRS_QOT_NBR,
						FO.SCA_NBR,
						FO.TTRPT_MNGD_DATA_SRVC_CD,
						FOCH5.CUST_ID														AS 'H5_CUST_ID',
						FOCH6.CUST_ID														AS 'H6_CUST_ID',
						''																	AS 'CNTRC_TERM',
						'' 																	AS 'DNS_SRVC_FSTNAME',
						'' 																	AS 'DNS_SRVC_LSTNAME',
						FO.TTRPT_LINK_PRCOL_CD 												AS 'INTF_PROT',
						CPED.CXR_ACCS_CD,
						FO.TSUP_NEAREST_CROSS_STREET_NME,
						
						CPED.TPORT_VLAN_QTY 													AS 'VLANQTY',
						CASE VAS.VAS_TYPE_CD
							WHEN 'COS' THEN 'Y'
							ELSE 'N' 
						END 																AS 'VAS',			
						CPED.TPORT_IP_VER_TYPE_CD												AS 'IPVER',
						CPED.TPORT_IPV6_ADR_QTY 												AS 'PROVIDEQTY',
						CPED.TPORT_IPV4_ADR_PRVDR_CD 											AS 'IPV4PROVIDER',
						CPED.TPORT_IPV6_ADR_PRVDR_CD 											AS 'IPV6PROVIDER',
						FO.MULT_ORDR_INDEX_NBR 												AS 'INDXNBR',
						FO.MULT_ORDR_TOT_CNT 												AS 'TOTNBR',
						ISNULL(OCDA.FRST_NME,'')						AS 'DNSFSTNME',
						ISNULL(OCDA.LST_NME,'')						AS 'DNSLSTNME',
						ISNULL(OCDA.CNTCT_NME,'') 					AS 'DNSNME',
						CASE ISNULL(OCDA.PHN_NBR, '')
							WHEN '' THEN OCDA.NPA + OCDA.NXX + OCDA.STN_NBR
							ELSE	OCDA.PHN_NBR
						END																	AS 'DNSPHN',
						ISNULL(OCDA.EMAIL_ADR,'')					AS 'DNSEMAIL',
						FO.NRFC_PRCOL_CD 													AS 'INTPROTOCOL',
						FO.DOMN_NME 														AS 'DOMAIN',
						FO.ADDL_IP_ADR_CD 													AS 'ADDIPADR',
						FO.TPORT_XST_IP_CNCTN_CD 											AS 'EXISTIPFLG',
						STUFF((SELECT ',' + IP_ADR FROM dbo.FSA_IP_ADR IA 
								WHERE @PRNT_ORDR_ID = IA.ORDR_ID FOR XML PATH('')), 1, 1,'') AS 'IPLIST',
						FO.TPORT_CURR_INET_PRVDR_CD 										AS 'IPPROVIDER',
						FO.VNDO_CNTRC_TERM_ID 												AS 'VNDRTERM',
						NT.NTE_TXT 															AS 'COMMENTS',
						FO.TTRPT_ROUTG_TYPE_CD 												AS 'PROTOCOLS',
						FO.ORANGE_OFFR_CD,
						ISNULL(ISNULL(OCS.CNTCT_NME,OCSP.CNTCT_NME),'')	AS 'SRVCNTNME',
						CASE ISNULL(OCS.PHN_NBR, OCSP.PHN_NBR)
							WHEN '' THEN ISNULL(OCS.NPA,OCSP.NPA) + ISNULL(OCS.NXX,OCSP.NXX) + ISNULL(OCS.STN_NBR,OCSP.STN_NBR)
							ELSE	ISNULL(OCS.PHN_NBR, OCSP.PHN_NBR)
						END																	AS 'SRVCNTPHN',
						FO.TTRPT_JACK_NRFC_TYPE_CD 											AS 'INTERFACETYPE',
						CASE @IsCancel 
							WHEN 1 THEN ISNULL(CanCF.FTN,FO.RELTD_FTN) 
							ELSE ISNULL(CPED.FSA_ORGNL_INSTL_ORDR_ID,0) 
						END																	AS 'RelatedFTN',	--Disconnect side FTN here
						CASE @IsCancel WHEN 1 THEN @CancelFTN ELSE FOP.PRNT_FTN END			AS 'PARENTFTN', -- Cancel pending order FTN, Disc/change Orig. Access FTN
						CPED.CXR_SRVC_ID 													AS 'ORIGSRVID',		--Disconnect side here
						CPED.TPORT_IPV4_ADR_QTY 												AS 'IPV4QTY',
						FO.CUST_FRWL_CD 													AS 'Firewall',
						OCDA.PHN_EXT_NBR 													AS 'DNS_PHN_EXT',
						OT.ORDR_TYPE_ID 													AS 'ORDER_TYPE',
						FO.IPV4_SUBNET_MASK_ADR 											AS 'SUBMASK', 
						
						--Disconnect Site Info
						ISNULL(FOCD.CUST_NME,'')						AS 'RD_CUST_NME',
						ISNULL(OCD.FRST_NME,'')						AS 'RD_FRST_NME',
						ISNULL(OCD.LST_NME,'')						AS 'RD_LST_NME',
						ISNULL(OCD.CNTCT_NME,'')						AS 'RD_CNTCT_NME',
						ISNULL(OAD.STREET_ADR_1,'')					AS 'RD_STREET_ADR_1',
						ISNULL(OAD.STREET_ADR_2,'')					AS 'RD_STREET_ADR_2',
						ISNULL(OAD.CTY_NME,'')						AS 'RD_CTY_NME',
						ISNULL(OAD.ZIP_PSTL_CD,'')					AS 'RD_ZIP_PSTL_CD',
						ISNULL(OAD.PRVN_NME,'') 
							+ '/'+ ISNULL(OAD.STT_CD,'')				AS 'RD_PRVN_NME_STT_CD',
						LCD.CTRY_NME														AS 'RD_CTRY_NME',
						CASE ISNULL(OCD.PHN_NBR, '')
							WHEN '' THEN OCD.NPA + OCD.NXX + OCD.STN_NBR
							ELSE OCD.PHN_NBR
						END																	AS 'RD_PHN_NBR',
						ISNULL(OCAD.FRST_NME,'')						AS 'RD_ALT_FRST_NME',
						ISNULL(OCAD.LST_NME,'')						AS 'RD_ALT_LST_NME',
						ISNULL(OCAD.CNTCT_NME,'')					AS 'RD_ALT_CNTCT_NME',
						CASE ISNULL(OCAD.PHN_NBR, '')
							WHEN '' THEN OCAD.NPA + OCAD.NXX + OCAD.STN_NBR
							ELSE 	OCAD.PHN_NBR
						END																	AS 'RD_ALT_CNTCT_PHN'
					--Disco Site Info comes from related Disco.  All other sections come from Install
					FROM			dbo.FSA_ORDR		FO		WITH (NOLOCK)
					INNER JOIN		dbo.ORDR			OD		WITH (NOLOCK) ON FO.ORDR_ID		= OD.ORDR_ID
					LEFT OUTER JOIN dbo.FSA_ORDR		FOP		WITH (NOLOCK) ON FOP.ORDR_ID	= @lastInstOrderID		--Disco Half (or last install order)
					INNER JOIN		dbo.LK_PPRT			P		WITH (NOLOCK) ON P.PPRT_ID		= OD.PPRT_ID
					INNER JOIN		dbo.LK_ORDR_TYPE	OT		WITH (NOLOCK) ON @ORDR_TYPE_CD	= OT.FSA_ORDR_TYPE_CD
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST	FOC		WITH (NOLOCK) ON FOC.ORDR_ID	= OD.ORDR_ID		AND FOC.CIS_LVL_TYPE='H1'
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST	FOCD	WITH (NOLOCK) ON FOP.ORDR_ID	= FOCD.ORDR_ID		AND FOCD.CIS_LVL_TYPE IN ('H5','H6')	--From Disco Half
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST	FOCH5	WITH (NOLOCK) ON FOCH5.ORDR_ID	= OD.ORDR_ID		AND FOCH5.CIS_LVL_TYPE IN ('H5','H6')
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST	FOCH6	WITH (NOLOCK) ON FOCH6.ORDR_ID	= OD.ORDR_ID		AND FOCH6.CIS_LVL_TYPE IN ('H5','H6')
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OC		WITH (NOLOCK) ON FO.ORDR_ID		= OC.ORDR_ID		AND OC.ROLE_ID = 6  		AND OC.CIS_LVL_TYPE	IN ('H5','H6')
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OCD		WITH (NOLOCK) ON FOP.ORDR_ID	= OCD.ORDR_ID		AND OCD.ROLE_ID = 6			AND OCD.CIS_LVL_TYPE	IN ('H5','H6')--From Disco Half
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OCA		WITH (NOLOCK) ON FO.ORDR_ID		= OCA.ORDR_ID		AND OCA.ROLE_ID = 7
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OCAD	WITH (NOLOCK) ON FOP.ORDR_ID	= OCAD.ORDR_ID		AND OCAD.ROLE_ID = 7			--From Disco Half
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OCDA	WITH (NOLOCK) ON FO.ORDR_ID		= OCDA.ORDR_ID		AND OCDA.ROLE_ID = 9
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OCS		WITH (NOLOCK) ON OD.ORDR_ID		= OCS.ORDR_ID		AND OCS.ROLE_ID = 8			
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OCSP	WITH (NOLOCK) ON FOP.ORDR_ID	= OCSP.ORDR_ID		AND OCSP.ROLE_ID = 8			--Original Install FTN
					LEFT OUTER JOIN dbo.ORDR_ADR		OA		WITH (NOLOCK) ON FO.ORDR_ID		= OA.ORDR_ID		AND OA.ADR_TYPE_ID = 18
					LEFT OUTER JOIN dbo.ORDR_ADR		OAD		WITH (NOLOCK) ON FOP.ORDR_ID	= OAD.ORDR_ID		AND OAD.ADR_TYPE_ID = 18		--From Disco Half
					LEFT OUTER JOIN dbo.LK_CTRY			LC		WITH (NOLOCK) ON OA.CTRY_CD		= LC.CTRY_CD
					LEFT OUTER JOIN dbo.LK_CTRY			LCD		WITH (NOLOCK) ON OAD.CTRY_CD	= LCD.CTRY_CD									--From Disco Half
					LEFT OUTER JOIN dbo.FSA_ORDR_VAS	VAS		WITH (NOLOCK) ON FO.ORDR_ID		= VAS.ORDR_ID 
					LEFT OUTER JOIN dbo.ORDR_NTE		NT		WITH (NOLOCK) ON FO.ORDR_ID		= NT.ORDR_ID		AND NT.NTE_TYPE_ID = 19
					LEFT OUTER JOIN dbo.FSA_ORDR		CanCF	WITH (NOLOCK) ON FOP.FTN		= CanCF.PRNT_FTN
					LEFT OUTER JOIN dbo.FSA_ORDR		rFO		WITH (NOLOCK) ON rFO.FTN		= FO.RELTD_FTN
					LEFT OUTER JOIN	dbo.ORDR_CNTCT		ESP		WITH (NOLOCK) ON FO.ORDR_ID		= ESP.ORDR_ID		AND	ESP.CIS_LVL_TYPE	IN ('H5','H6')	AND	ESP.CNTCT_TYPE_ID	=	17
					LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM CPED WITH (NOLOCK) ON rFO.ORDR_ID= CPED.ORDR_ID	
					WHERE FO.ORDR_ID = @OrderID
				END
				ELSE IF ((@REL_FTN <> '') AND (@ORDR_TYPE_CD = 'IN'))
				BEGIN
				PRINT 'I don''t think we''ll ever get here'
					SELECT TOP 1
						FO.FTN, 
						FO.CUST_CMMT_DT, 
						FO.CUST_WANT_DT, 
						FOP.CUST_SIGNED_DT, 
						FO.CUST_ACPT_ERLY_SRVC_CD, 
						FO.MULT_CUST_ORDR_CD,
						FO.CUST_ORDR_SBMT_DT,
						FO.TSP_CD,
						ISNULL(FOCH5.CUST_NME,'') CUST_NME,
						ISNULL(OC.FRST_NME,'') FRST_NME,
						ISNULL(OC.LST_NME,'') LST_NME,
						ISNULL(OC.CNTCT_NME,'') 	CNTCT_NME,
						ISNULL(OA.STREET_ADR_1,'') STREET_ADR_1,
						ISNULL(OA.STREET_ADR_2,'') STREET_ADR_2,
						ISNULL(OA.CTY_NME,'') CTY_NME,
						ISNULL(OA.ZIP_PSTL_CD,'') ZIP_PSTL_CD,
						ISNULL(OA.PRVN_NME,'') +'/'+ ISNULL(OA.STT_CD,'') PRVN_NME_STT_CD,
						LC.CTRY_NME,
						ESP.NPA,
						ESP.NXX,
						ESP.PHN_NBR															AS 'SitePhn',		--Original Install FTN
						CASE ISNULL(OC.PHN_NBR, '')
							WHEN '' THEN OC.NPA + OC.NXX + OC.STN_NBR
							ELSE	OC.PHN_NBR
						END																	AS 'PHN_NBR',
						ISNULL(OC.EMAIL_ADR,'') EMAIL_ADR,
						ISNULL(OCA.FRST_NME,'') ALT_FRST_NME,
						ISNULL(OCA.LST_NME,'') ALT_LST_NME,	
						ISNULL(OCA.CNTCT_NME,'')						AS 'ALT_CNTCT_NME',	
						CASE ISNULL(OCA.PHN_NBR, '')
							WHEN '' THEN OCA.NPA + OCA.NXX + OCA.STN_NBR
							ELSE	OCA.PHN_NBR
						END																	ALT_CNTCT_PHN,			
						ISNULL(OCA.EMAIL_ADR,'')						ALT_EMAIL_ADR,
						FOP.CUST_PRMS_OCPY_CD,
						@Order_Sub_Type_Des													AS 'ORDR_TYPE_DES',
						FO.FSA_EXP_TYPE_CD,
						CPED.INSTL_DSGN_DOC_NBR,
						FOP.PROD_TYPE_CD,
						CASE FOP.PROD_TYPE_CD WHEN 'DO' THEN 'Dedicated IP'
											 WHEN 'SO' THEN 'SprintLink Frame Relay'
											 WHEN 'MP' THEN 'MPLS'
											 WHEN 'MO' THEN 'MPLS'
											 ELSE FOP.PROD_TYPE_CD END AS PROD_SELECTION,
						FOP.TTRPT_ACCS_TYPE_DES as 'LoopSpeed',
						FOP.TSUP_TELCO_DEMARC_BLDG_NME										AS BLDG,
						FOP.TSUP_TELCO_DEMARC_FLR_ID										AS FLR,
						FOP.TSUP_TELCO_DEMARC_RM_NBR										AS ROOM,
						CASE ISNULL(FOP.TSUP_TELCO_DEMARC_BLDG_NME,'')	WHEN '' THEN ''
																		ELSE 'Bldg: ' + FOP.TSUP_TELCO_DEMARC_BLDG_NME + '  '
						END
						+ CASE ISNULL(FOP.TSUP_TELCO_DEMARC_FLR_ID,'')	WHEN '' THEN ''
																		ELSE 'Floor: ' + FOP.TSUP_TELCO_DEMARC_FLR_ID + '  '
						END
						+ CASE ISNULL(FOP.TSUP_TELCO_DEMARC_RM_NBR,'')	WHEN '' THEN ''
																		ELSE 'Room: ' + FOP.TSUP_TELCO_DEMARC_RM_NBR + '  '
						END																	AS 'TelcoDemarc',
						--CPE_ACCS_PRVDR_CD AS 'CPEProvidedBy',
						FOP.TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD AS 'CSUDSU',
						--TPORT_CUST_ROUTR_TAG_TXT + ' ' + TPORT_CUST_ROUTR_AUTO_NEGOT_CD AS 'RouterInfo',
						CPED.TTRPT_ENCAP_CD AS 'ENCAP',
						CPED.TTRPT_SPD_OF_SRVC_BDWD_DES as 'PortSpeed',
						FOP.TTRPT_ROUTG_TYPE_CD,
						FOP.TSUP_PRS_QOT_NBR,
						FOP.SCA_NBR,
						FOP.TTRPT_MNGD_DATA_SRVC_CD,
						FOCH5.CUST_ID AS H5_CUST_ID,
						FOCH6.CUST_ID AS H6_CUST_ID,
						'' AS CNTRC_TERM,
						'' AS 'DNS_SRVC_FSTNAME',
						'' AS 'DNS_SRVC_LSTNAME',
						ISNULL(OCDA.FRST_NME,'') AS 'DNS_SRVC_FSTNAME',
						ISNULL(OCDA.LST_NME,'') AS 'DNS_SRVC_LSTNAME',
						ISNULL(OCDA.CNTCT_NME,'') AS 'DNS_SRVC_NAME',
						FOP.TTRPT_LINK_PRCOL_CD as 'INTF_PROT',
						CPED.CXR_ACCS_CD,
						FOP.TSUP_NEAREST_CROSS_STREET_NME,						
						CPED.TPORT_VLAN_QTY AS 'VLANQTY',
						CASE VAS.VAS_TYPE_CD
							WHEN 'COS' THEN 'Y'
							ELSE 'N' END AS 'VAS',			
						CPED.TPORT_IP_VER_TYPE_CD AS 'IPVER',
						CPED.TPORT_IPV6_ADR_QTY AS 'PROVIDEQTY',
						CPED.TPORT_IPV4_ADR_PRVDR_CD AS 'IPV4PROVIDER',
						CPED.TPORT_IPV6_ADR_PRVDR_CD AS 'IPV6PROVIDER',
						FO.MULT_ORDR_INDEX_NBR AS 'INDXNBR',
						FO.MULT_ORDR_TOT_CNT AS 'TOTNBR',
						ISNULL(OCDA.FRST_NME,'') AS 'DNSFSTNME',
						ISNULL(OCDA.LST_NME,'') AS 'DNSLSTNME',
						ISNULL(OCDA.CNTCT_NME,'') AS 'DNSNME',
						CASE ISNULL(OCDA.PHN_NBR, '')
							WHEN '' THEN OCDA.NPA + OCDA.NXX + OCDA.STN_NBR
							ELSE OCDA.PHN_NBR
						END												 AS 'DNSPHN',
						ISNULL(OCDA.EMAIL_ADR,'') AS 'DNSEMAIL',
						FO.NRFC_PRCOL_CD AS 'INTPROTOCOL',
						FO.DOMN_NME AS 'DOMAIN',
						FO.ADDL_IP_ADR_CD AS 'ADDIPADR',
						FOP.TPORT_XST_IP_CNCTN_CD AS 'EXISTIPFLG',
						STUFF((SELECT ',' + IP_ADR FROM dbo.FSA_IP_ADR IA 
								WHERE @OrderID = IA.ORDR_ID FOR XML PATH('')), 1, 1,'') AS IPLIST,
						FO.TPORT_CURR_INET_PRVDR_CD AS 'IPPROVIDER',
						FO.VNDO_CNTRC_TERM_ID AS 'VNDRTERM',
						NT.NTE_TXT AS 'COMMENTS',
						FO.TTRPT_ROUTG_TYPE_CD AS 'PROTOCOLS',
						--FO.ORANGE_CMNTY_NME,
						--FO.ORANGE_SITE_ID,
						FO.ORANGE_OFFR_CD,
						ISNULL(OCS.CNTCT_NME,'') AS 'SRVCNTNME',
						CASE ISNULL(OCS.PHN_NBR, '') 
							WHEN '' THEN OCS.NPA + OCS.NXX + OCS.STN_NBR
							ELSE	OCS.PHN_NBR
						END												AS 'SRVCNTPHN',
						FO.TTRPT_JACK_NRFC_TYPE_CD AS 'INTERFACETYPE',
						ISNULL(FO.RELTD_FTN,'') AS 'RelatedFTN',
						CASE @IsCancel WHEN 1 THEN @CancelFTN ELSE '' END AS 'PARENTFTN', -- Cancel pending order FTN, Disc/change Orig. Access FTN
						OT.ORDR_TYPE_ID AS 'ORDER_TYPE',					
						'' AS 'ORIGSRVID',
						CPED.TPORT_IPV4_ADR_QTY AS 'IPV4QTY',
						FO.CUST_FRWL_CD AS 'Firewall',
						OCDA.PHN_EXT_NBR AS 'DNS_PHN_EXT',
						FO.IPV4_SUBNET_MASK_ADR AS 'SUBMASK',
						'' RD_CUST_NME,
						'' RD_FRST_NME,
						'' RD_LST_NME,
						'' RD_CNTCT_NME,
						'' RD_STREET_ADR_1,
						'' RD_STREET_ADR_2,
						'' RD_CTY_NME,
						'' RD_ZIP_PSTL_CD,
						'' RD_PRVN_NME_STT_CD,
						'' RD_CTRY_NME,
						'' RD_PHN_NBR,
						'' RD_ALT_FRST_NME,
						'' RD_ALT_LST_NME,
						'' RD_ALT_CNTCT_NME,
						'' RD_ALT_CNTCT_PHN										
					FROM dbo.FSA_ORDR FO WITH (NOLOCK)
					INNER JOIN dbo.FSA_ORDR FOP WITH (NOLOCK) ON FOP.ORDR_ID <> FO.ORDR_ID AND COALESCE(FOP.RELTD_FTN,'') = '' AND FOP.ORDR_TYPE_CD = 'IN' AND FOP.ORDR_ID < FO.ORDR_ID
					INNER JOIN dbo.ORDR ODP WITH (NOLOCK) ON ODP.ORDR_ID = FOP.ORDR_ID 
					INNER JOIN dbo.ORDR ODI WITH (NOLOCK) ON ODI.H5_H6_CUST_ID = ODP.H5_H6_CUST_ID AND ODI.ORDR_ID = FO.ORDR_ID
					INNER JOIN dbo.LK_PPRT P WITH (NOLOCK) ON P.PPRT_ID = ODP.PPRT_ID
					INNER JOIN dbo.LK_ORDR_TYPE OT WITH (NOLOCK) ON P.ORDR_TYPE_ID = OT.ORDR_TYPE_ID
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST FOC WITH (NOLOCK) ON FOC.ORDR_ID = FOP.ORDR_ID AND FOC.CIS_LVL_TYPE ='H1'
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST FOCH5 WITH (NOLOCK) ON FOCH5.ORDR_ID = FOP.ORDR_ID AND FOCH5.CIS_LVL_TYPE IN ('H5','H6')
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST FOCH6 WITH (NOLOCK) ON FOCH6.ORDR_ID = FOP.ORDR_ID AND FOCH6.CIS_LVL_TYPE IN ('H5','H6')
					LEFT OUTER JOIN dbo.ORDR_CNTCT OC WITH (NOLOCK) ON FOP.ORDR_ID = OC.ORDR_ID AND OC.ROLE_ID = 6  			AND OC.CIS_LVL_TYPE	IN ('H5','H6')
					LEFT OUTER JOIN dbo.ORDR_CNTCT OCA WITH (NOLOCK) ON FOP.ORDR_ID = OCA.ORDR_ID AND OCA.ROLE_ID = 7
					LEFT OUTER JOIN dbo.ORDR_CNTCT OCDA WITH (NOLOCK) ON FOP.ORDR_ID = OCDA.ORDR_ID AND OCDA.ROLE_ID = 9
					LEFT OUTER JOIN dbo.ORDR_CNTCT OCS WITH (NOLOCK) ON FO.ORDR_ID = OCS.ORDR_ID AND OCS.ROLE_ID = 8
					LEFT OUTER JOIN dbo.ORDR_ADR OA WITH (NOLOCK) ON FOP.ORDR_ID = OA.ORDR_ID AND OA.ADR_TYPE_ID = 18
					LEFT OUTER JOIN dbo.LK_CTRY LC WITH (NOLOCK) ON OA.CTRY_CD = LC.CTRY_CD
					LEFT OUTER JOIN dbo.FSA_ORDR_VAS VAS WITH (NOLOCK) ON FO.ORDR_ID = VAS.ORDR_ID 
					LEFT OUTER JOIN dbo.ORDR_NTE NT WITH (NOLOCK) ON FO.ORDR_ID = NT.ORDR_ID AND NT.NTE_TYPE_ID = 19
					LEFT OUTER JOIN	dbo.ORDR_CNTCT		ESP		WITH (NOLOCK) ON FO.ORDR_ID	= ESP.ORDR_ID		AND	ESP.CIS_LVL_TYPE	IN ('H5','H6')	AND	ESP.CNTCT_TYPE_ID	=	17
					LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM CPED WITH (NOLOCK) ON FO.ORDR_ID= CPED.ORDR_ID	
					WHERE FO.ORDR_ID = @OrderID	
										ORDER BY FOP.ORDR_ID DESC
				END
				ELSE
				BEGIN
					SELECT DISTINCT
					FO.FTN, 
					FO.CUST_CMMT_DT, 
					FO.CUST_WANT_DT, 
					FO.CUST_SIGNED_DT, 
					FO.CUST_ACPT_ERLY_SRVC_CD, 
					FO.MULT_CUST_ORDR_CD,
					FO.CUST_ORDR_SBMT_DT,
					FO.TSP_CD,
					ISNULL(FOCH5.CUST_NME,'')						CUST_NME,
					ISNULL(OC.FRST_NME,'')							FRST_NME,
					ISNULL(OC.LST_NME,'') 							LST_NME,
					ISNULL(OC.CNTCT_NME,'') 							CNTCT_NME,
					ISNULL(OA.STREET_ADR_1,'') 						STREET_ADR_1,
					ISNULL(OA.STREET_ADR_2,'') 						STREET_ADR_2,
					ISNULL(OA.CTY_NME,'') 							CTY_NME,
					ISNULL(OA.ZIP_PSTL_CD,'') 						ZIP_PSTL_CD,
					ISNULL(OA.PRVN_NME,'') 
						+ '/' + ISNULL(OA.STT_CD,'')					PRVN_NME_STT_CD,
					LC.CTRY_NME,
					ESP.NPA,
					ESP.NXX,
					ESP.PHN_NBR																AS 'SitePhn',		--Original Install FTN
					CASE ISNULL(OC.PHN_NBR, '')
						WHEN '' THEN OC.NPA + OC.NXX + OC.STN_NBR
						ELSE	OC.PHN_NBR
					END																		AS 'PHN_NBR',
					ISNULL(OC.EMAIL_ADR,'')							EMAIL_ADR,
					ISNULL(OCA.FRST_NME,'') 							ALT_FRST_NME,
					ISNULL(OCA.LST_NME,'') 							ALT_LST_NME,	
					ISNULL(OCA.CNTCT_NME,'')							AS 'ALT_CNTCT_NME',
					CASE ISNULL(OCA.PHN_NBR, '')
						WHEN '' THEN OCA.NPA + OCA.NXX + OCA.STN_NBR
						ELSE	OCA.PHN_NBR
					END																		ALT_CNTCT_PHN,			
					ISNULL(OCA.EMAIL_ADR,'') 						ALT_EMAIL_ADR,
					FO.CUST_PRMS_OCPY_CD,
					@Order_Sub_Type_Des														AS 'ORDR_TYPE_DES',
					FO.FSA_EXP_TYPE_CD,
					CPED.INSTL_DSGN_DOC_NBR,
					FO.PROD_TYPE_CD,
					CASE FO.PROD_TYPE_CD WHEN 'DO' THEN 'Dedicated IP'
										 WHEN 'SO' THEN 'SprintLink Frame Relay'
										 WHEN 'MP' THEN 'MPLS'
										 WHEN 'MO' THEN 'MPLS'
										 ELSE FO.PROD_TYPE_CD 
					END 																	AS PROD_SELECTION,
					FO.TTRPT_ACCS_TYPE_DES as 'LoopSpeed',
					FO.TSUP_TELCO_DEMARC_BLDG_NME											AS BLDG,
					FO.TSUP_TELCO_DEMARC_FLR_ID												AS FLR,
					FO.TSUP_TELCO_DEMARC_RM_NBR												AS ROOM,
					CASE ISNULL(FO.TSUP_TELCO_DEMARC_BLDG_NME,'')	
						WHEN '' THEN ''
						ELSE 'Bldg: ' + FO.TSUP_TELCO_DEMARC_BLDG_NME + '  '
					END
					+ CASE ISNULL(FO.TSUP_TELCO_DEMARC_FLR_ID,'')	
						WHEN '' THEN ''
						ELSE 'Floor: ' + FO.TSUP_TELCO_DEMARC_FLR_ID + '  '
					END
					+ CASE ISNULL(FO.TSUP_TELCO_DEMARC_RM_NBR,'')	
						WHEN '' THEN ''
						ELSE 'Room: ' + FO.TSUP_TELCO_DEMARC_RM_NBR + '  '
					END																		AS 'TelcoDemarc',
					--CPE_ACCS_PRVDR_CD AS 'CPEProvidedBy',
					FO.TTRPT_CSU_DSU_PRVDD_BY_VNDR_CD 										AS 'CSUDSU',
					--TPORT_CUST_ROUTR_TAG_TXT + ' ' + TPORT_CUST_ROUTR_AUTO_NEGOT_CD AS 'RouterInfo',
					CPED.TTRPT_ENCAP_CD 														AS 'ENCAP',
					CPED.TTRPT_SPD_OF_SRVC_BDWD_DES 											as 'PortSpeed',
					FO.TTRPT_ROUTG_TYPE_CD,
					FO.TSUP_PRS_QOT_NBR,
					FO.SCA_NBR,
					FO.TTRPT_MNGD_DATA_SRVC_CD,
					FOCH5.CUST_ID 															AS H5_CUST_ID,
					FOCH6.CUST_ID 															AS H6_CUST_ID,
					'' 																		AS CNTRC_TERM,
					'' 																		AS 'DNS_SRVC_FSTNAME',
					'' 																		AS 'DNS_SRVC_LSTNAME',
					FO.TTRPT_LINK_PRCOL_CD 													as 'INTF_PROT',
					CPED.CXR_ACCS_CD,
					FO.TSUP_NEAREST_CROSS_STREET_NME,					
					CPED.TPORT_VLAN_QTY 														AS 'VLANQTY',
					CASE VAS.VAS_TYPE_CD
						WHEN 'COS' THEN 'Y'
						ELSE 'N' 
					END 																	AS 'VAS',			
					CPED.TPORT_IP_VER_TYPE_CD 												AS 'IPVER',
					CPED.TPORT_IPV6_ADR_QTY 													AS 'PROVIDEQTY',
					CPED.TPORT_IPV4_ADR_PRVDR_CD 												AS 'IPV4PROVIDER',
					CPED.TPORT_IPV6_ADR_PRVDR_CD 												AS 'IPV6PROVIDER',
					FO.MULT_ORDR_INDEX_NBR 													AS 'INDXNBR',
					FO.MULT_ORDR_TOT_CNT 													AS 'TOTNBR',
					ISNULL(OCDA.FRST_NME,'') 						AS 'DNSFSTNME',
					ISNULL(OCDA.LST_NME,'') 							AS 'DNSLSTNME',
					ISNULL(OCDA.CNTCT_NME,'') 						AS 'DNSNME',
					CASE ISNULL(OCDA.PHN_NBR, '')
						WHEN '' THEN OCDA.NPA + OCDA.NXX + OCDA.STN_NBR
						ELSE	OCDA.PHN_NBR
					END																		AS 'DNSPHN',
					ISNULL(OCDA.EMAIL_ADR,'') 						AS 'DNSEMAIL',
					FO.NRFC_PRCOL_CD 														AS 'INTPROTOCOL',
					FO.DOMN_NME 															AS 'DOMAIN',
					FO.ADDL_IP_ADR_CD 														AS 'ADDIPADR',
					FO.TPORT_XST_IP_CNCTN_CD 												AS 'EXISTIPFLG',
					STUFF((SELECT ',' + IP_ADR FROM dbo.FSA_IP_ADR IA 
							WHERE @OrderID = IA.ORDR_ID FOR XML PATH('')), 1, 1,'')			AS IPLIST,
					FO.TPORT_CURR_INET_PRVDR_CD 											AS 'IPPROVIDER',
					FO.VNDO_CNTRC_TERM_ID 													AS 'VNDRTERM',
					NT.NTE_TXT 																AS 'COMMENTS',
					FO.TTRPT_ROUTG_TYPE_CD 													AS 'PROTOCOLS',
					--FO.ORANGE_CMNTY_NME,
					--FO.ORANGE_SITE_ID,
					FO.ORANGE_OFFR_CD,
					ISNULL(OCS.CNTCT_NME,'')							AS 'SRVCNTNME',
					CASE ISNULL(OCS.PHN_NBR, '')
						WHEN '' THEN OCS.NPA + OCS.NXX + OCS.STN_NBR
						ELSE	OCS.PHN_NBR
					END																		AS 'SRVCNTPHN',
					FO.TTRPT_JACK_NRFC_TYPE_CD 												AS 'INTERFACETYPE',
					'' 																		AS 'RelatedFTN',
					CASE @IsCancel WHEN 1 THEN @CancelFTN ELSE '' END 						AS 'PARENTFTN', -- Cancel pending order FTN, Disc/change Orig. Access FTN
					OT.ORDR_TYPE_ID 														AS 'ORDER_TYPE',
					'' 																		AS 'ORIGSRVID',
					CPED.TPORT_IPV4_ADR_QTY 													AS 'IPV4QTY',
					FO.CUST_FRWL_CD 														AS 'Firewall',
					OCDA.PHN_EXT_NBR 														AS 'DNS_PHN_EXT',
					FO.IPV4_SUBNET_MASK_ADR 												AS 'SUBMASK',
					'' 																		RD_CUST_NME,
					'' 																		RD_FRST_NME,
					'' 																		RD_LST_NME,
					'' 																		RD_CNTCT_NME,
					'' 																		RD_STREET_ADR_1,
					'' 																		RD_STREET_ADR_2,
					'' 																		RD_CTY_NME,
					'' 																		RD_ZIP_PSTL_CD,
					'' 																		RD_PRVN_NME_STT_CD,
					'' 																		RD_CTRY_NME,
					'' 																		RD_PHN_NBR,
					'' 																		RD_ALT_FRST_NME,
					'' 																		RD_ALT_LST_NME,
					''																		RD_ALT_CNTCT_NME,
					'' 																		RD_ALT_CNTCT_PHN				
					FROM			dbo.FSA_ORDR		FO		WITH (NOLOCK)
					INNER JOIN		ORDR				OD		WITH (NOLOCK) ON FO.ORDR_ID		= OD.ORDR_ID
					INNER JOIN		dbo.LK_PPRT			P		WITH (NOLOCK) ON P.PPRT_ID		= OD.PPRT_ID
					INNER JOIN		dbo.LK_ORDR_TYPE	OT		WITH (NOLOCK) ON P.ORDR_TYPE_ID = OT.ORDR_TYPE_ID
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST	FOC		WITH (NOLOCK) ON FOC.ORDR_ID	= FO.ORDR_ID		AND FOC.CIS_LVL_TYPE='H1'
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST	FOCH5	WITH (NOLOCK) ON FOCH5.ORDR_ID	= FO.ORDR_ID		AND FOCH5.CIS_LVL_TYPE IN ('H5','H6')
					LEFT OUTER JOIN dbo.FSA_ORDR_CUST	FOCH6	WITH (NOLOCK) ON FOCH6.ORDR_ID	= FO.ORDR_ID		AND FOCH6.CIS_LVL_TYPE IN ('H5','H6')
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OC		WITH (NOLOCK) ON FO.ORDR_ID		= OC.ORDR_ID		AND OC.ROLE_ID = 6			AND OC.CIS_LVL_TYPE	IN ('OD')
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OCA		WITH (NOLOCK) ON FO.ORDR_ID		= OCA.ORDR_ID		AND OCA.ROLE_ID = 7
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OCDA	WITH (NOLOCK) ON FO.ORDR_ID		= OCDA.ORDR_ID		AND OCDA.ROLE_ID = 9
					LEFT OUTER JOIN dbo.ORDR_CNTCT		OCS		WITH (NOLOCK) ON FO.ORDR_ID		= OCS.ORDR_ID		AND OCS.ROLE_ID = 8
					LEFT OUTER JOIN	dbo.ORDR_CNTCT		ESP		WITH (NOLOCK) ON FO.ORDR_ID		= ESP.ORDR_ID		AND	ESP.CIS_LVL_TYPE	IN ('H5','H6')	AND	ESP.CNTCT_TYPE_ID	=	17
					LEFT OUTER JOIN dbo.ORDR_ADR		OA		WITH (NOLOCK) ON FO.ORDR_ID		= OA.ORDR_ID		AND OA.ADR_TYPE_ID = 18
					LEFT OUTER JOIN dbo.LK_CTRY			LC		WITH (NOLOCK) ON OA.CTRY_CD		= LC.CTRY_CD
					LEFT OUTER JOIN dbo.FSA_ORDR_VAS	VAS		WITH (NOLOCK) ON FO.ORDR_ID		= VAS.ORDR_ID 
					LEFT OUTER JOIN dbo.ORDR_NTE		NT		WITH (NOLOCK) ON FO.ORDR_ID		= NT.ORDR_ID		AND NT.NTE_TYPE_ID = 19
					LEFT OUTER JOIN dbo.FSA_ORDR_CPE_LINE_ITEM CPED WITH (NOLOCK) ON FO.ORDR_ID= CPED.ORDR_ID
					WHERE FO.ORDR_ID = @OrderID
				END
		END
	END TRY
	BEGIN CATCH
		EXEC dbo.insertErrorInfo
	END CATCH
END
