USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_ANCIRptWklyBillClear]    Script Date: 09/06/2018 23:01:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


  
--======================================================================================     
-- Author:   David Phillilps   
-- Date:   06/27/2016  
-- Description:    
--          ANCI Weekly Bill Clear/Install Date Report   
--======================================================================================  
CREATE Procedure [dbo].[sp_ANCIRptWklyBillClear]      
 @Secured   INT=0
AS  
BEGIN TRY  
   
 SET NOCOUNT ON;  
   
 DECLARE @startDate  Datetime  
 DECLARE @endDate  Datetime  
 DECLARE @today   Datetime  
 DECLARE @startDtStr  VARCHAR(10)

 
 IF DATEPART(DD,GETDATE()) < 7
	BEGIN   
    	SET @today=DATEADD(MONTH,-1,GETDATE())        
		SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));         
		SET @startDate=cast(@startDtStr AS datetime)     
		SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
	END
ELSE 
	BEGIN  
		SET @today=GETDATE()        
		SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));         
		SET @startDate=cast(@startDtStr AS datetime)     
		SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
	END
   
  
 SELECT 
	ISNULL(fsa.FTN,odr.ORDR_ID) AS 'FTN#',
	lc.CTRY_NME AS 'Country',
	strd.STDI_REAS_DES AS 'STDI' ,
	lpt.PROD_NME AS 'Product type',
	lot.ORDR_TYPE_DES AS 'Order Type',
	ost.ORDR_SUB_TYPE_DES AS 'Order Sub Type',
	osh.STDI_DT AS 'Standard Interval Date(s)',
	ccdh.OLD_CCD_DT AS 'Origial CCD',
	ISNULL(odr.CUST_CMMT_DT,fsa.CUST_CMMT_DT) AS 'Latest CCD',
	CASE 
		WHEN odr.ORDR_STUS_ID = 2 THEN oMS.ORDR_BILL_CLEAR_INSTL_DT 
		WHEN odr.ORDR_STUS_ID = 5 THEN ORDR_DSCNCT_DT 
		END  'Order Bill Clear Date',
	stdiC.count_STDI 'STDI Change',
		CASE
		WHEN odr.ORDR_STUS_ID = 2 AND DATEDIFF(day,oMS.ORDR_BILL_CLEAR_INSTL_DT,osh.STDI_DT) <= 1 THEN 0
		WHEN odr.ORDR_STUS_ID = 5 AND DATEDIFF(day,oMS.ORDR_DSCNCT_DT,osh.STDI_DT) <= 1 THEN 0
		ELSE 1
	END 'STDI Met',
	u.FULL_NME 'User Assigned'	
From COWS.dbo.ORDR odr WITH (NOLOCK)
INNER JOIN COWS.dbo.FSA_ORDR fsa WITH (NOLOCK) ON odr.ORDR_ID = fsa.ORDR_ID
INNER JOIN COWS.dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.ORDR_ID = odr.ORDR_ID AND oa.CIS_LVL_TYPE IN ('H5','H6')
INNER JOIN COWS.dbo.LK_CTRY lc WITH (NOLOCK) ON lc.CTRY_CD = oa.CTRY_CD
LEFT JOIN (SELECT MAX(ORDR_STDI_ID) AS ORDR_STDI_ID, 
			ORDR_ID AS ORDR_ID FROM COWS.dbo.ORDR_STDI_HIST WITH (NOLOCK) GROUP BY ORDR_ID) 
			stdiM ON stdiM.ORDR_ID = odr.ORDR_ID
LEFT JOIN COWS.dbo.ORDR_STDI_HIST osh WITH (NOLOCK) ON osh.ORDR_STDI_ID = stdiM.ORDR_STDI_ID
INNER JOIN COWS.dbo.LK_PPRT lp WITH (NOLOCK) ON lp.PPRT_ID = odr.PPRT_ID
INNER JOIN COWS.dbo.LK_PROD_TYPE lpt WITH (NOLOCK) ON lpt.PROD_TYPE_ID = lp.PROD_TYPE_ID
INNER JOIN COWS.dbo.LK_ORDR_TYPE lot WITH (NOLOCK) ON lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
LEFT JOIN COWS.dbo.ORDR_MS oMS WITH (NOLOCK) ON oMS.ORDR_ID = odr.ORDR_ID
INNER JOIN COWS.dbo.LK_STDI_REAS strd WITH (NOLOCK) ON strd.STDI_REAS_ID = osh.STDI_REAS_ID
LEFT JOIN (SELECT COUNT(ORDR_ID) AS COUNT_STDI, 
			ORDR_ID AS ORDR_ID FROM COWS.dbo.ORDR_STDI_HIST WITH (NOLOCK) GROUP BY ORDR_ID) 
			stdiC ON stdiC.ORDR_ID = odr.ORDR_ID
INNER JOIN COWS.dbo.LK_ORDR_SUB_TYPE ost WITH (NOLOCK) ON ost.ORDR_SUB_TYPE_CD = fsa.ORDR_SUB_TYPE_CD
LEFT JOIN (Select MIN (CCD_HIST_ID) AS CCD_MIN_ID, 
			ORDR_ID AS ORDR_ID from COWS.dbo.CCD_HIST WITH (NOLOCK) GROUP BY ORDR_ID )
			ccdhM ON ccdhM.ORDR_ID = odr.ORDR_ID 
LEFT JOIN COWS.dbo.CCD_HIST ccdh WITH (NOLOCK) ON ccdh.CCD_HIST_ID = ccdhM.CCD_MIN_ID
INNER JOIN COWS.dbo.USER_WFM_ASMT ufa WITH (NOLOCK) ON ufa.ORDR_ID = odr.ORDR_ID
INNER JOIN COWS.dbo.LK_USER u WITH (NOLOCK) ON u.USER_ID = ufa.ASN_USER_ID
WHERE odr.RGN_ID = 3
     AND ((odr.ORDR_STUS_ID = 2
     AND oMS.ORDR_BILL_CLEAR_INSTL_DT BETWEEN @startDate and @endDate)
     OR (odr.ORDR_STUS_ID = 5 AND oMS.ORDR_DSCNCT_DT BETWEEN @startDate and @endDate))
   
 RETURN 0;  
   
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_ANCIRptWklyBillClear '    
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  

GO


