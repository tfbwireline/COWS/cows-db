USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptMNSEscalationReport]    Script Date: 11/21/2019 1:06:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
--==================================================================================  
-- Project:   PJ004672 - COWS Reporting   
-- Author:   Sudarat Vongchumpit   
-- Date:   06/17/2011  
-- Description:    
--     Extract MDS events that have been submitted as escalations.  
--     QueryNumber: 1 = Daily  
--         2 = Weekly  
--         3 = Monthly  
--         0 = Specify start date & end date: 'mm/dd/yyyy'  
--  
-- Note:   MDS Activity Type of Disconnect DOES NOT have FSA_MDS_EVENT record.  
--   
-- Modifications:  
--  
-- 01/25/2012  sxv0766: Updated to match the OnDemand SP.   
-- 03/29/2012  sxv0766: IM952822-Removed resulting string expression from   
--     Event Submit Date and Event Schedule Date columns  
--
-- 11/23/2016		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked 
----EXEC COWS_Reporting.dbo.sp_MNSRptMNSEscalationReport 0,0,'01/01/2014','11/11/2016'      
--==================================================================================  
*/
ALTER PROCEDURE [dbo].[sp_MNSRptMNSEscalationReport]   
 @QueryNumber  INT,  
 @Secured   INT=0,   
 @inStartDtStr  VARCHAR(10)='',  
 @inEndDtStr   VARCHAR(10)=''   
AS  
BEGIN TRY  
  
 SET NOCOUNT ON;  
   
 DECLARE @startDate  Datetime  
 DECLARE @endDate  Datetime  
 DECLARE @today   Datetime  
 DECLARE @startDtStr  VARCHAR(10)   
 DECLARE @dayOfWeek  INT  
  
 IF @QueryNumber=0  
  BEGIN  
  
   SET @startDate=CONVERT(datetime, @inStartDtStr, 101)  
   SET @endDate=CONVERT(datetime, @inEndDtStr, 101)      
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @endDate=DATEADD(day, 1, @endDate)  
     
  END    
 ELSE IF @QueryNumber=1  
     BEGIN       
      -- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000  
    
   SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)  
   SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)     
  END  
 ELSE IF @QueryNumber=2  
  BEGIN  
   -- e.g. Run date is '2011-06-21' 3rd day of the week  
   -- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000  
     
   SET @today=GETDATE()  
   SET @dayOfWeek=DATEPART(dw, @today)  
   --SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+6), @today), 112) As DateTime)  
   --SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS Datetime)  
   SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)  
   SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)  
   --SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -7, GETDATE()), 112) As DateTime)  
   --SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS Datetime)    
      
  END  
 ELSE IF @QueryNumber=3  
  BEGIN      
     SET @today=DATEADD(MONTH,-1,GETDATE())  
       
      -- e.g. Run date is the 1st day of each month   
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000  
        
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));       
   SET @startDate=cast(@startDtStr AS datetime)  
   SET @endDate=cast((cast(SUBSTRING(CONVERT(VARCHAR(10), getdate(), 101),1,2) + '/01/' + CAST(YEAR(getdate()) AS VARCHAR(4)) as datetime) -1) AS datetime)  
  END  
      
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;  
   
  SELECT CASE         
    --WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''  
    --WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'  
    --ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
    
    WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
	WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'')
    END 'Customer Name',  
    mds.EVENT_ID 'COWS Event ID',   
    mds.CREAT_DT 'Event Submit Date',  
   --REPLACE(LEFT(CONVERT(VARCHAR(10),mds.CREAT_DT,101),1), '0', '') +  
   -- SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 2, 2) +  
   -- REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 4, 1), '0', '') +  
   -- RIGHT(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 6) 'Event Submit Date',  
   mds.STRT_TMST 'Event Schedule Date',  
   --REPLACE(LEFT(CONVERT(VARCHAR(10),mds.STRT_TMST,101),1), '0', '') +  
   -- SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 2, 2) +  
   -- REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 4, 1), '0', '') +  
   -- RIGHT(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 6) 'Event Schedule Date',      
   UPPER(evst.EVENT_STUS_DES) 'Event Status',  
   CASE mds.SPRINT_CPE_NCR_ID  
    WHEN 3 THEN 'NO'  
    ELSE 'YES'   
    END 'CPE Requested',  
   mact.MDS_ACTY_TYPE_DES 'Event Type',   
   CASE  
    WHEN luser1.DSPL_NME is NULL THEN ''     
    ELSE luser1.DSPL_NME END 'IPM Name',   
   CASE  
    WHEN luser2.DSPL_NME is NULL THEN ''      
    ELSE luser2.DSPL_NME END 'Assigned Acct MNS PM', 
   'MDS Only'			AS [Network Activity Type],
	''                              AS [S/C Flag]
 FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)   
  JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID   
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID   
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID        
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
  --LEFT JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst   
  --   FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)  
  --   WHERE REC_STUS_ID = 1  
  --   GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID  
  --LEFT JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID   
  -- AND lasg.maxRecTmst = easg.CREAT_DT    
  --LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON easg.ASN_TO_USER_ID = luser2.USER_ID    
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID  
    
 WHERE mds.ESCL_CD = 1 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) AND mds.STRT_TMST >= @startDate AND mds.STRT_TMST < @endDate   
   
 --WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND mds.ESCL_CD = 1 AND mds.STRT_TMST >= @startDate AND mds.STRT_TMST < @endDate   
 /*New MDS EVENT Start*/
 
 UNION ALL
 SELECT CASE         
    --WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''  
    --WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'  
    --ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME))
    WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
	WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
    END 'Customer Name',  
    mds.EVENT_ID 'COWS Event ID',   
    mds.CREAT_DT 'Event Submit Date',  
   --REPLACE(LEFT(CONVERT(VARCHAR(10),mds.CREAT_DT,101),1), '0', '') +  
   -- SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 2, 2) +  
   -- REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 4, 1), '0', '') +  
   -- RIGHT(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 6) 'Event Submit Date',  
   mds.STRT_TMST 'Event Schedule Date',  
   --REPLACE(LEFT(CONVERT(VARCHAR(10),mds.STRT_TMST,101),1), '0', '') +  
   -- SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 2, 2) +  
   -- REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 4, 1), '0', '') +  
   -- RIGHT(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 6) 'Event Schedule Date',      
   UPPER(evst.EVENT_STUS_DES) 'Event Status',  
   CASE mds.SPRINT_CPE_NCR_ID  
    WHEN 3 THEN 'NO'  
    ELSE 'YES'   
    END 'CPE Requested',  
   mact.MDS_ACTY_TYPE_DES 'Event Type',   
   CASE  
    WHEN luser1.DSPL_NME is NULL THEN ''     
    ELSE luser1.DSPL_NME END 'IPM Name',   
   CASE  
    WHEN luser2.DSPL_NME is NULL THEN ''      
    ELSE luser2.DSPL_NME END 'Assigned Acct MNS PM',
   lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type],
			CASE	
						WHEN eod.SC_CD = 'C'        THEN 'Complex'
						WHEN eod.SC_CD = 'S'        THEN 'Simple'
					    ELSE ''
						END                                     AS [S/C Flag]
 FROM COWS.dbo.MDS_EVENT mds with (nolock)   
  JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID   
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID   
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID        
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = mds.NTWK_ACTY_TYPE_ID
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
  --LEFT JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst   
  --   FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)  
  --   WHERE REC_STUS_ID = 1  
  --   GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID  
  --LEFT JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID   
  -- AND lasg.maxRecTmst = easg.CREAT_DT    
  --LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON easg.ASN_TO_USER_ID = luser2.USER_ID    
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID 
  LEFT OUTER JOIN [COWS].[dbo].[MDS_EVENT_ODIE_DEV] eod WITH (NOLOCK) ON eod.EVENT_ID = mds.EVENT_ID
    
 WHERE mds.ESCL_CD = 1 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) AND mds.STRT_TMST >= @startDate AND mds.STRT_TMST < @endDate 


 ORDER By mds.EVENT_ID
/*New MDS EVENT Ends */
      
 CLOSE SYMMETRIC KEY FS@K3y;   
   
 RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptMNSEscalationReport '  + CAST(@QueryNumber AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
 

