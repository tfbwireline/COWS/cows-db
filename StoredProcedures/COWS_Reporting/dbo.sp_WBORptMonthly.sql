USE [COWS_Reporting]
GO
/*
-- =============================================
-- Author:		
-- Create date: 
-- Description: Extract WBS Monthly report data
--					QueryNumber: 1 = Domestic Code 1
--								 0 = International Code 0
-- =============================================
*/

--DROP [dbo].[sp_WBORptweekly]
ALTER PROCEDURE [dbo].[sp_WBORptMonthly]
--	 Add the parameters for the stored procedure here
	    @QueryNumber		INT = 0,
	    @SecuredUser		bit = 0
AS
BEGIN TRY

	SET NOCOUNT ON;
	
--DECLARE @QueryNumber		INT = 1
--DECLARE @SecuredUser		bit = 0	

	
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
	
IF @QueryNumber = 0  /*Domestic Code  0*/
	BEGIN
	---WBO Weekly International Report
		SELECT	 DISTINCT
				 A.[FTN]   AS [FTN] 
				,A.[PRNT_FTN] as [Related FTN]
				,B.[ORDR_TYPE_DES] AS  [Order Type]
				,D.[ORDR_STUS_DES] AS  [Order Status]
				,H1.[CUST_ID] AS  [H1 Customer Number]
				--,F.[ORDR_BILL_CLEAR_INSTL_DT] AS  [Bill Clear Date]
				,tk.[CREAT_DT]   AS  [Bill Clear Date]
				--,E.[CUST_NME]  AS [End Customer Name]
				,CASE
							--WHEN @SecuredUser = 0 and C.SCURD_CD = 0 THEN CONVERT(varchar, DecryptByKey(h5.CUST_NME))	
							--WHEN @SecuredUser = 0 and C.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(h5.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
							--WHEN @SecuredUser = 0 and C.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(h5.CUST_NME)) IS NULL THEN NULL 
							--ELSE CONVERT(varchar, DecryptByKey(h5.CUST_NME))	
					WHEN @SecuredUser = 0 and C.CSG_LVL_ID =0 THEN h5.CUST_NME
					WHEN @SecuredUser = 0 and C.CSG_LVL_ID >0 AND  h5.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and C.CSG_LVL_ID >0 AND  h5.CUST_NME2 IS NULL THEN NULL 
					ELSE COALESCE(h5.CUST_NME2,h5.CUST_NME,'')		
					END [End Customer Name] --Secured
				,G.[ORDR_SUB_TYPE_DES] AS  [Order SubType]
				,C.[H5_H6_CUST_ID] AS  [H5/H6 ID]
				--,h5.CUST_ID AS  [H5/H6 ID]
				,A.[DMSTC_CD] AS  [Domestic Flag]
				,ISNULL(C.[CUST_CMMT_DT],A.[CUST_CMMT_DT]) AS  [CCD Date]
		FROM	(SELECT bb.FTN, bb.ORDR_ID, bb.ORDR_ACTN_ID, fcpl.TPORT_ETHRNT_NRFC_INDCR, 
						bb.TTRPT_ALT_ACCS_PRVDR_CD,
						fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES, bb.TPORT_BRSTBL_USAGE_TYPE_CD,bb.ORDR_SUB_TYPE_CD, 
						bb.TPORT_PORT_TYPE_ID, fcpl.CXR_ACCS_CD,
						bb.ORDR_TYPE_CD, bb.PROD_TYPE_CD, bb.CPE_PHN_NBR,
						bb.CPE_REC_ONLY_CD,bb.[CUST_CMMT_DT],bb.[PRNT_FTN],cc.[DMSTC_CD]
						FROM	[COWS].[dbo].[FSA_ORDR] bb JOIN
							[COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
							LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = bb.ORDR_ID
						WHERE	bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
						FROM	(SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
						WHERE	bb.FTN = fsa.FTN)  AND CC.ORDR_STUS_ID in (2,1)  /*AND CC.CREAT_DT >= DATEADD(day,-365,CC.CREAT_DT)
				 UNION
				 SELECT bb.FTN, bb.ORDR_ID, bb.ORDR_ACTN_ID, bb.TPORT_ETHRNT_NRFC_INDCR, 
						bb.TTRPT_ALT_ACCS_PRVDR_CD,
						bb.TTRPT_SPD_OF_SRVC_BDWD_DES, bb.TPORT_BRSTBL_USAGE_TYPE_CD,bb.ORDR_SUB_TYPE_CD, 
						bb.TPORT_PORT_TYPE_ID, bb.CXR_ACCS_CD,
						bb.ORDR_TYPE_CD, bb.PROD_TYPE_CD, bb.CPE_PHN_NBR,
						bb.CPE_REC_ONLY_CD,bb.[CUST_CMMT_DT],bb.[PRNT_FTN],cc.[DMSTC_CD]
						FROM	[COWS].[dbo].[FSA_ORDR] bb JOIN
							[COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE	bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
						FROM	(SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
						WHERE	bb.FTN = fsa.FTN)  AND CC.ORDR_STUS_ID =1  */
				) A     
				INNER JOIN [COWS].[dbo].ORDR C WITH(NOLOCK) ON A.ORDR_ID=C.ORDR_ID 
				LEFT JOIN [COWS].[dbo].LK_ORDR_SUB_TYPE G WITH(NOLOCK) ON A.ORDR_SUB_TYPE_CD=G.ORDR_SUB_TYPE_CD 
				LEFT JOIN [COWS].[dbo].LK_ORDR_TYPE B WITH(NOLOCK) ON A.[ORDR_TYPE_CD]=B.FSA_ORDR_TYPE_CD 
				LEFT JOIN [COWS].[dbo].LK_ORDR_STUS D WITH(NOLOCK) ON C.ORDR_STUS_ID=D.ORDR_STUS_ID LEFT JOIN 
				(SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE bb.CIS_LVL_TYPE = 'H1') h1 on A.ORDR_ID = h1.ORDR_ID  LEFT JOIN
				(SELECT bb.CUST_ID, bb.ORDR_ID, bb.SOI_CD, bb.CUST_NME, bb.CURR_BILL_CYC_CD, 
								bb.SRVC_SUB_TYPE_ID ,
						        CONVERT(varchar, DecryptByKey(csd.CUST_NME)) AS  [CUST_NME2] 
								FROM [COWS].[dbo].[FSA_ORDR_CUST] bb with (nolock) JOIN 
								[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID LEFT OUTER JOIN 
                        [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5 
								WHERE bb.CIS_LVL_TYPE in( 'H5','H6','ES') AND bb.CUST_ID IS NOT NULL
								AND LEN(bb.CUST_ID) <> 0 AND bb.CURR_BILL_CYC_CD <> '43' ) h5
								on A.ORDR_ID = h5.ORDR_ID   LEFT JOIN
				(SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID 
 								FROM [COWS].[dbo].[ACT_TASK] bb with (nolock) JOIN
								[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
								WHERE bb.TASK_ID = 1001) tk on A.ORDR_ID = tk.ORDR_ID 
		WHERE A.DMSTC_CD = 0
			  --AND C.CREAT_DT >= DATEADD(day,-365,C.CREAT_DT)    --(CONVERT(datetime,@Date)
			  --AND A.[FTN]='11477605'
		ORDER BY A.[FTN]  ASC			
END
	ELSE   /*Domestic Code  1*/
		BEGIN
		---WBO Weekly Domestic Report
		SELECT	 DISTINCT
				 A.[FTN]   AS [FTN] 
				--,A.[PRNT_FTN] as [Related FTN]
				,B.[ORDR_TYPE_DES] AS  [Order Type]
				,D.[ORDR_STUS_DES] AS  [Order Status]
				,H1.[CUST_ID] AS  [H1 Customer Number]
				--,F.[ORDR_BILL_CLEAR_INSTL_DT] AS  [Bill Clear Date]
				,tk.[CREAT_DT]   AS  [Bill Clear Date]
				--,E.[CUST_NME]  AS [End Customer Name]
				,CASE
					--WHEN @SecuredUser = 0 and C.SCURD_CD = 0 THEN CONVERT(varchar, DecryptByKey(h5.CUST_NME))	
					--WHEN @SecuredUser = 0 and C.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(h5.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					--WHEN @SecuredUser = 0 and C.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(h5.CUST_NME)) IS NULL THEN NULL 
					--ELSE CONVERT(varchar, DecryptByKey(h5.CUST_NME))	
					WHEN @SecuredUser = 0 and C.CSG_LVL_ID =0 THEN h5.CUST_NME
					WHEN @SecuredUser = 0 and C.CSG_LVL_ID >0 AND  h5.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and C.CSG_LVL_ID >0 AND  h5.CUST_NME2 IS NULL THEN NULL 
					ELSE COALESCE(h5.CUST_NME2,h5.CUST_NME,'')
				END [End Customer Name] --Secured
				,G.[ORDR_SUB_TYPE_DES] AS  [Order SubType]
				,C.[H5_H6_CUST_ID] AS  [H5/H6 ID]
				--,h5.CUST_ID AS  [H5/H6 ID]
				,C.[DMSTC_CD] AS  [Domestic Flag]
				,ISNULL(C.[CUST_CMMT_DT],A.[CUST_CMMT_DT]) AS  [CCD Date]
		FROM	(SELECT bb.FTN, bb.ORDR_ID, bb.ORDR_ACTN_ID, fcpl.TPORT_ETHRNT_NRFC_INDCR, 
						bb.TTRPT_ALT_ACCS_PRVDR_CD,
						fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES, bb.TPORT_BRSTBL_USAGE_TYPE_CD,bb.ORDR_SUB_TYPE_CD, 
						bb.TPORT_PORT_TYPE_ID, fcpl.CXR_ACCS_CD,
						bb.ORDR_TYPE_CD, bb.PROD_TYPE_CD, bb.CPE_PHN_NBR,
						bb.CPE_REC_ONLY_CD,bb.[CUST_CMMT_DT],bb.[PRNT_FTN],cc.[DMSTC_CD]
						FROM	[COWS].[dbo].[FSA_ORDR] bb JOIN
							[COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
							LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = bb.ORDR_ID
						WHERE	bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
						FROM	(SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
						WHERE	bb.FTN = fsa.FTN)  AND CC.ORDR_STUS_ID in(2,1)  /*AND CC.CREAT_DT >= DATEADD(day,-365,CC.CREAT_DT)
				 UNION
				 SELECT bb.FTN, bb.ORDR_ID, bb.ORDR_ACTN_ID, bb.TPORT_ETHRNT_NRFC_INDCR, 
						bb.TTRPT_ALT_ACCS_PRVDR_CD,
						bb.TTRPT_SPD_OF_SRVC_BDWD_DES, bb.TPORT_BRSTBL_USAGE_TYPE_CD,bb.ORDR_SUB_TYPE_CD, 
						bb.TPORT_PORT_TYPE_ID, bb.CXR_ACCS_CD,
						bb.ORDR_TYPE_CD, bb.PROD_TYPE_CD, bb.CPE_PHN_NBR,
						bb.CPE_REC_ONLY_CD,bb.[CUST_CMMT_DT],bb.[PRNT_FTN],cc.[DMSTC_CD]
						FROM	[COWS].[dbo].[FSA_ORDR] bb JOIN
							[COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE	bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
						FROM	(SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
						WHERE	bb.FTN = fsa.FTN)  AND CC.ORDR_STUS_ID =1  */
				) A     
				INNER JOIN [COWS].[dbo].ORDR C WITH(NOLOCK) ON A.ORDR_ID=C.ORDR_ID 
				LEFT JOIN [COWS].[dbo].LK_ORDR_SUB_TYPE G WITH(NOLOCK) ON A.ORDR_SUB_TYPE_CD=G.ORDR_SUB_TYPE_CD 
				LEFT JOIN [COWS].[dbo].LK_ORDR_TYPE B WITH(NOLOCK) ON A.[ORDR_TYPE_CD]=B.FSA_ORDR_TYPE_CD 
				LEFT JOIN [COWS].[dbo].LK_ORDR_STUS D WITH(NOLOCK) ON C.ORDR_STUS_ID=D.ORDR_STUS_ID LEFT JOIN 
				(SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE bb.CIS_LVL_TYPE = 'H1') h1 on A.ORDR_ID = h1.ORDR_ID  LEFT JOIN
				(SELECT bb.CUST_ID, bb.ORDR_ID, bb.SOI_CD, bb.CUST_NME, bb.CURR_BILL_CYC_CD, 
								bb.SRVC_SUB_TYPE_ID  ,
						        CONVERT(varchar, DecryptByKey(csd.CUST_NME)) AS  [CUST_NME2]
								FROM [COWS].[dbo].[FSA_ORDR_CUST] bb with (nolock) JOIN 
								[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID LEFT OUTER JOIN 
                                [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5 
								WHERE bb.CIS_LVL_TYPE in( 'H5','H6','ES') AND bb.CUST_ID IS NOT NULL
								AND LEN(bb.CUST_ID) <> 0 AND bb.CURR_BILL_CYC_CD <> '43' ) h5
								on A.ORDR_ID = h5.ORDR_ID   LEFT JOIN
				(SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID 
 								FROM [COWS].[dbo].[ACT_TASK] bb with (nolock) JOIN
								[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
								WHERE bb.TASK_ID = 1001) tk on A.ORDR_ID = tk.ORDR_ID 
		WHERE c.DMSTC_CD = 1
			  --AND C.CREAT_DT >= DATEADD(day,-365,C.CREAT_DT)    --(CONVERT(datetime,@Date)
			  --AND A.[FTN]='11566784'
		ORDER BY A.[FTN]  ASC

		END
CLOSE SYMMETRIC KEY FS@K3y 

	END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_WBORptMonthly '  + CAST(@QueryNumber AS VARCHAR(4)) + ', '
	SET @Desc=@Desc + CONVERT(VARCHAR(1), @QueryNumber) + ', ' + CONVERT(VARCHAR(1), @SecuredUser)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH








