USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptPerCustomerQueueReportOD]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_MNSRptPerCustomerQueueReportOD')
EXEC ('CREATE PROCEDURE [dbo].[sp_MNSRptPerCustomerQueueReportOD] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			08/23/2011
-- Description:		
--					Extract opened orders for a specified parameters(s);
-- 					DateRange
--
--					Use run date to get # of days passed Customer Commitment Date.
--
-- Note:			Contains no sensitive data.
--
--==================================================================================
ALTER Procedure [dbo].[sp_MNSRptPerCustomerQueueReportOD]
	@SecuredUser		INT=0,	
	@getSensData		CHAR(1)='N',
	@startDate			Datetime=NULL,
	@endDate			Datetime=NULL	
AS
BEGIN TRY

	SET NOCOUNT ON;
		
	IF @startDate is NOT NULL AND @endDate is NOT NULL
		BEGIN
	
			SELECT
				CASE
					WHEN luser.FULL_NME is NULL THEN 'Default Queue (No MNS PM)' 
					ELSE luser.FULL_NME END 'Assigned MNS PM' , 
				CASE
					WHEN (ltsk.ORDR_STUS_ID = 210 OR ltsk.ORDR_STUS_ID = 211) THEN 'Return To Sales'
					ELSE stus.STUS_DES END 'COWS Order Status',	
				CASE		
					WHEN DATEDIFF(day, ford.cust_cmmt_dt, GETDATE()) >= 31 THEN 1  
			 		WHEN (DATEDIFF(day, ford.cust_cmmt_dt, GETDATE()) >= 5 AND DATEDIFF(day, ford.cust_cmmt_dt, GETDATE()) < 31) THEN 2 
					END 'CCD Priority'
	
			FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)
				--LEFT JOIN COWS.dbo.FSA_MDS_EVENT fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 														
				LEFT JOIN COWS.dbo.FSA_MDS_EVENT_ORDR fmord with (nolock) ON mds.EVENT_ID = fmord.EVENT_ID				
				LEFT JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON fmord.ORDR_ID = ford.ORDR_ID
				LEFT JOIN COWS.dbo.ACT_TASK actt with (nolock) ON ford.ORDR_ID = actt.ORDR_ID
				LEFT JOIN COWS.dbo.LK_TASK ltsk with (nolock) ON actt.TASK_ID = ltsk.TASK_ID		
				LEFT JOIN COWS.dbo.LK_STUS stus with (nolock) ON ltsk.ORDR_STUS_ID = stus.STUS_ID
				LEFT JOIN (SELECT ORDR_ID, ASN_USER_ID 
							FROM USER_WFM_ASMT with (nolock)
							WHERE GRP_ID = 2 ) as uwasm ON ford.ORDR_ID = uwasm.ORDR_ID														
				LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON uwasm.ASN_USER_ID = luser.USER_ID		
				
			-- STUS_ID: 0 = Pending, 150 = Approved, 210 = RTS Pending, 211 = RTS Complete
			WHERE ltsk.ORDR_STUS_ID in (0, 150, 210, 211) AND DATEDIFF(day, ford.cust_cmmt_dt, GETDATE()) >= 5
				AND mds.STRT_TMST >= @startDate AND mds.STRT_TMST < DATEADD(day, 1, @endDate)
	
		END
	ELSE IF @startDate is NULL AND @endDate is NULL
		BEGIN
		
			SELECT
				CASE
					WHEN luser.FULL_NME is NULL THEN 'Default Queue (No MNS PM)' 
					ELSE luser.FULL_NME END 'Assigned MNS PM' , 
				CASE
					WHEN (ltsk.ORDR_STUS_ID = 210 OR ltsk.ORDR_STUS_ID = 211) THEN 'Return To Sales'
					ELSE stus.STUS_DES END 'COWS Order Status',	
				CASE		
					WHEN DATEDIFF(day, ford.cust_cmmt_dt, GETDATE()) >= 31 THEN 1  
			 		WHEN (DATEDIFF(day, ford.cust_cmmt_dt, GETDATE()) >= 5 AND DATEDIFF(day, ford.cust_cmmt_dt, GETDATE()) < 31) THEN 2 
					END 'CCD Priority'
	
			--FROM COWS.dbo.FSA_MDS_EVENT fmds with (nolock) 				
			--	LEFT JOIN COWS.dbo.FSA_MDS_EVENT_ORDR fmord with (nolock) ON fmds.EVENT_ID = fmord.EVENT_ID
			FROM COWS.dbo.FSA_MDS_EVENT_ORDR fmord with (nolock)
				LEFT JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON fmord.ORDR_ID = ford.ORDR_ID
				LEFT JOIN COWS.dbo.ACT_TASK actt with (nolock) ON ford.ORDR_ID = actt.ORDR_ID
				LEFT JOIN COWS.dbo.LK_TASK ltsk with (nolock) ON actt.TASK_ID = ltsk.TASK_ID		
				LEFT JOIN COWS.dbo.LK_STUS stus with (nolock) ON ltsk.ORDR_STUS_ID = stus.STUS_ID
				LEFT JOIN (SELECT ORDR_ID, ASN_USER_ID 
							FROM USER_WFM_ASMT with (nolock)
							WHERE GRP_ID = 2 ) as uwasm ON ford.ORDR_ID = uwasm.ORDR_ID				
				LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON uwasm.ASN_USER_ID = luser.USER_ID		
				
			-- STUS_ID: 0 = Pending, 150 = Approved, 210 = RTS Pending, 211 = RTS Complete
			WHERE ltsk.ORDR_STUS_ID in (0, 150, 210, 211) AND DATEDIFF(day, ford.cust_cmmt_dt, GETDATE()) >= 5		
		END
	
	
	RETURN 0;
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptPerCustomerQueueReportOD ' 
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH




GO
