USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptEventsWorked]    Script Date: 11/21/2019 1:50:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
--==========================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			11/21/2011
-- Description:		
--					Extract Fast Track and MDS event(s) in "Completed" & "Returned To Work".  
--
--					QueryNumber: 1 = Daily
--								 2 = Weekly
--								 3 = Monthly
--								 0 = Specify start date & end date: 'mm/dd/yyyy'
--
-- Note:			MDS Activity Type of Disconnect DOES NOT have FSA_MDS_EVENT record.
--
-- Modifications:
--
-- 01/24/2012		sxv0766: Specified VARCHAR size of 100 for Cust Name decryption,
--							 Use Yesterday date for the daily report.
-- 02/21/2012		sxv0766: Added Reschedule Failed ACTN ID from 31 to 43
-- 03/29/2012		sxv0766: IM952822-Removed resulting string expression from YearMonth
--					and Date And Time columns
-- 11/23/2016		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked
-- 05/11/2017		vn370313: record duplicates due to event_Hist table having some events with 2 identical record 1 with comment  other none
--					goal herer is to merge those 2 identical column having only comment field concat
----EXEC COWS_Reporting.dbo.sp_MNSRptEventsWorked 1,0,'01/01/2014','11/11/2016'
----EXEC COWS_Reporting.dbo.sp_MNSRptEventsWorked 1
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--==========================================================================================
*/
ALTER PROCEDURE [dbo].[sp_MNSRptEventsWorked]
	@QueryNumber		INT,	
	@Secured			INT=0,	
	@inStartDtStr		VARCHAR(10)='',
	@inEndDtStr			VARCHAR(10)=''
AS
BEGIN
--BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDate		Datetime
	DECLARE @endDate		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)	
	DECLARE @dayOfWeek		INT
	
	IF @QueryNumber=0
		BEGIN

			SET @startDate=CONVERT(datetime, @inStartDtStr, 101)
			SET @endDate=CONVERT(datetime, @inEndDtStr, 101)				

			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			SET @endDate=DATEADD(day, 1, @endDate) 
												
		END	
	ELSE IF @QueryNumber=1
    	BEGIN    	
    		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
		
			--SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			--SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)   
			
			-- Today's report shows a list of events worked/completed yesterday 
			SET @startDate=cast(CONVERT(varchar(8), DATEADD(day, -1, GETDATE()), 112) AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			
		END
	ELSE IF @QueryNumber=2
		BEGIN
			-- e.g. Run date is '2011-06-21' 3rd day of the week
			-- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000

			SET @today=GETDATE()
			SET @dayOfWeek=DATEPART(dw, @today)
			SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)			
				
		END
	ELSE IF @QueryNumber=3
		BEGIN			 
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month 
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
			SET @startDate=cast(@startDtStr AS datetime)
			SET @endDate=cast((cast(SUBSTRING(CONVERT(VARCHAR(10), getdate(), 101),1,2) + '/01/' + CAST(YEAR(getdate()) AS VARCHAR(4)) as datetime) -1) AS datetime)  
		END
				
	OPEN SYMMETRIC KEY FS@K3y DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SELECT	CASE mds.MDS_FAST_TRK_CD
				WHEN 1 THEN 'MDS Fast Track'
				WHEN 0 THEN 'MDS' END 'Event',
			mds.EVENT_ID 'Event ID',
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name',	
			luser1.DSPL_NME 'Author', 
			mds.STRT_TMST 'YearMonth', 
			--SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) 'YearMonth', 
			CASE
				WHEN actn.ACTN_DES is NULL THEN ''		
				ELSE actn.ACTN_DES END 'Event Action',
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',
 			evhi.CREAT_DT 'Date And Time',	 	
 			--REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',								
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END 'MDS Install Activity',			
			CASE
				WHEN luser2.DSPL_NME is NULL THEN '' 
				ELSE luser2.DSPL_NME END 'Performed By',	
 			CASE
 				WHEN evhi.CMNT_TXT is NULL THEN ''
 				ELSE evhi.CMNT_TXT END 'Comment',				
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
			''                             AS [S/C Flag], 
			'MDS Only'			AS [Network Activity Type]			
				
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)
		--LEFT JOIN COWS.dbo.FSA_MDS_EVENT fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID 
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		--LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID AND mds.MDS_ACTY_TYPE_ID = 2
		LEFT JOIN ( /*SELECT EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) */
					SELECT	EVENT_ID, ACTN_ID, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID=ISNULL(FAIL_REAS_ID,'') , 
					STUFF(
						(SELECT DISTINCT ',' + CMNT_TXT
						FROM COWS.dbo.EVENT_HIST B with (nolock)
						WHERE 
							B.EVENT_ID=A.EVENT_ID AND 
							B.ACTN_ID=A.ACTN_ID   AND 
							B.CREAT_BY_USER_ID=A.CREAT_BY_USER_ID AND 
							B.CREAT_DT=A.CREAT_DT  AND
							--B.FAIL_REAS_ID=A.FAIL_REAS_ID
							ISNULL(B.FAIL_REAS_ID,'')=ISNULL(A.FAIL_REAS_ID,'')
						FOR XML PATH ('')) , 1, 1, '')  AS CMNT_TXT

			FROM	COWS.dbo.EVENT_HIST A with (nolock)
			WHERE	(A.ACTN_ID in (15, 17) OR A.ACTN_ID between 31 and 43)  --AND A.EVENT_ID=184104 
			GROUP BY A.EVENT_ID, A.ACTN_ID, A.CREAT_BY_USER_ID, A.CREAT_DT,ISNULL(A.FAIL_REAS_ID,'')-- A.FAIL_REAS_ID
					
				  )as evhi ON mds.EVENT_ID = evhi.EVENT_ID		
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID			
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID		
	
	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000			
	-- EVENT_STUS_ID: 3 = Rework, 6 = Completed
	WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND mds.EVENT_STUS_ID in (3, 6) AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate
	
	UNION ALL
	
	SELECT	CASE mds.MDS_FAST_TRK_CD
				WHEN 1 THEN 'MDS Fast Track'
				WHEN 0 THEN 'MDS' END 'Event',
			mds.EVENT_ID 'Event ID',
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name',	
			luser1.DSPL_NME 'Author', 
			mds.STRT_TMST 'YearMonth',
			--SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) 'YearMonth', 
			CASE
				WHEN actn.ACTN_DES is NULL THEN ''		
				ELSE actn.ACTN_DES END 'Event Action',
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',
 			evhi.CREAT_DT 'Date And Time',
 			--REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',								
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END 'MDS Install Activity',				
			CASE
				WHEN luser2.DSPL_NME is NULL THEN '' 
				ELSE luser2.DSPL_NME END 'Performed By',	
 			CASE
 				WHEN evhi.CMNT_TXT is NULL THEN ''
 				ELSE evhi.CMNT_TXT END 'Comment',				
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
			''                                     AS [S/C Flag], 
			'MDS Only'			AS [Network Activity Type]			
				
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN ( /*SELECT EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) */
					SELECT	EVENT_ID, ACTN_ID, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID=ISNULL(FAIL_REAS_ID,'') , 
					STUFF(
						(SELECT DISTINCT ',' + CMNT_TXT
						FROM COWS.dbo.EVENT_HIST B with (nolock)
						WHERE 
							B.EVENT_ID=A.EVENT_ID AND 
							B.ACTN_ID=A.ACTN_ID   AND 
							B.CREAT_BY_USER_ID=A.CREAT_BY_USER_ID AND 
							B.CREAT_DT=A.CREAT_DT  AND
							--B.FAIL_REAS_ID=A.FAIL_REAS_ID
							ISNULL(B.FAIL_REAS_ID,'')=ISNULL(A.FAIL_REAS_ID,'')
						FOR XML PATH ('')) , 1, 1, '')  AS CMNT_TXT

			FROM	COWS.dbo.EVENT_HIST A with (nolock)
			WHERE	(A.ACTN_ID in (15, 17) OR A.ACTN_ID between 31 and 43)  --AND A.EVENT_ID=184104 
			GROUP BY A.EVENT_ID, A.ACTN_ID, A.CREAT_BY_USER_ID, A.CREAT_DT,ISNULL(A.FAIL_REAS_ID,'')-- A.FAIL_REAS_ID 
					
				  )as evhi ON mds.EVENT_ID = evhi.EVENT_ID		
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID			
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID		
	
	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000			
	-- EVENT_STUS_ID: 3 = Rework, 6 = Completed
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (3, 6) AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate	
	
	/*ORDER BY mds.EVENT_ID*/

	/*New MDS Events Code Start */
	UNION ALL

	SELECT CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END 'Event',
			mds.EVENT_ID 'Event ID',
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name',	
			luser1.DSPL_NME 'Author', 
			mds.STRT_TMST 'YearMonth', 
			--SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) 'YearMonth', 
			CASE
				WHEN actn.ACTN_DES is NULL THEN ''		
				ELSE actn.ACTN_DES END 'Event Action',
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',
 			evhi.CREAT_DT 'Date And Time',	 	
 			--REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',								
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END 'MDS Install Activity',			
			CASE
				WHEN luser2.DSPL_NME is NULL THEN '' 
				ELSE luser2.DSPL_NME END 'Performed By',	
 			CASE
 				WHEN evhi.CMNT_TXT is NULL THEN ''
 				ELSE evhi.CMNT_TXT END 'Comment',				
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
			CASE	
						WHEN eod.SC_CD = 'C'        THEN 'Complex'
						WHEN eod.SC_CD = 'S'        THEN 'Simple'
					    ELSE ''
						END                                     AS [S/C Flag],
			lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type]				
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)
		--LEFT JOIN COWS.dbo.FSA_MDS_EVENT fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID 
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = mds.NTWK_ACTY_TYPE_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
		--LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID AND mds.MDS_ACTY_TYPE_ID = 2
		LEFT JOIN ( /*SELECT EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) */

					SELECT	EVENT_ID, ACTN_ID, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID=ISNULL(FAIL_REAS_ID,'') , 
					STUFF(
						(SELECT DISTINCT ',' + CMNT_TXT
						FROM COWS.dbo.EVENT_HIST B with (nolock)
						WHERE 
							B.EVENT_ID=A.EVENT_ID AND 
							B.ACTN_ID=A.ACTN_ID   AND 
							B.CREAT_BY_USER_ID=A.CREAT_BY_USER_ID AND 
							B.CREAT_DT=A.CREAT_DT  AND
							--B.FAIL_REAS_ID=A.FAIL_REAS_ID
							ISNULL(B.FAIL_REAS_ID,'')=ISNULL(A.FAIL_REAS_ID,'')
						FOR XML PATH ('')) , 1, 1, '')  AS CMNT_TXT

			FROM	COWS.dbo.EVENT_HIST A with (nolock)
			WHERE	(A.ACTN_ID in (15, 17) OR A.ACTN_ID between 31 and 43)  --AND A.EVENT_ID=184104 
			GROUP BY A.EVENT_ID, A.ACTN_ID, A.CREAT_BY_USER_ID, A.CREAT_DT,ISNULL(A.FAIL_REAS_ID,'')-- A.FAIL_REAS_ID
					
				  )as evhi ON mds.EVENT_ID = evhi.EVENT_ID		
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID			
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID
		LEFT OUTER JOIN [COWS].[dbo].[MDS_EVENT_ODIE_DEV] eod WITH (NOLOCK) ON eod.EVENT_ID = mds.EVENT_ID
	
	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000			
	-- EVENT_STUS_ID: 3 = Rework, 6 = Completed
	WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND ((mds.EVENT_STUS_ID in (3, 6)) OR ((mds.EVENT_STUS_ID = 4) AND (mds.WRKFLW_STUS_ID = 4)))  AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate

	UNION ALL
	
	SELECT	CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END 'Event',
			mds.EVENT_ID 'Event ID',
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name',	
			luser1.DSPL_NME 'Author', 
			mds.STRT_TMST 'YearMonth',
			--SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) 'YearMonth', 
			CASE
				WHEN actn.ACTN_DES is NULL THEN ''		
				ELSE actn.ACTN_DES END 'Event Action',
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',
 			evhi.CREAT_DT 'Date And Time',
 			--REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',								
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END 'MDS Install Activity',				
			CASE
				WHEN luser2.DSPL_NME is NULL THEN '' 
				ELSE luser2.DSPL_NME END 'Performed By',	
 			CASE
 				WHEN evhi.CMNT_TXT is NULL THEN ''
 				ELSE evhi.CMNT_TXT END 'Comment',				
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
			''                                     AS [S/C Flag],
			lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type]				
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = mds.NTWK_ACTY_TYPE_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
		LEFT JOIN ( /*SELECT EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) */
					SELECT	EVENT_ID, ACTN_ID, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID=ISNULL(FAIL_REAS_ID,'') , 
					STUFF(
						(SELECT DISTINCT ',' + CMNT_TXT
						FROM COWS.dbo.EVENT_HIST B with (nolock)
						WHERE 
							B.EVENT_ID=A.EVENT_ID AND 
							B.ACTN_ID=A.ACTN_ID   AND 
							B.CREAT_BY_USER_ID=A.CREAT_BY_USER_ID AND 
							B.CREAT_DT=A.CREAT_DT  AND
							--B.FAIL_REAS_ID=A.FAIL_REAS_ID
							ISNULL(B.FAIL_REAS_ID,'')=ISNULL(A.FAIL_REAS_ID,'')
						FOR XML PATH ('')) , 1, 1, '')  AS CMNT_TXT

			FROM	COWS.dbo.EVENT_HIST A with (nolock)
			WHERE	(A.ACTN_ID in (15, 17) OR A.ACTN_ID between 31 and 43)  --AND A.EVENT_ID=184104 
			GROUP BY A.EVENT_ID, A.ACTN_ID, A.CREAT_BY_USER_ID, A.CREAT_DT,ISNULL(A.FAIL_REAS_ID,'')-- A.FAIL_REAS_ID
					
					)as evhi ON mds.EVENT_ID = evhi.EVENT_ID		
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID			
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID		
		
	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000			
	-- EVENT_STUS_ID: 3 = Rework, 6 = Completed
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND ((mds.EVENT_STUS_ID in (3, 6)) OR ((mds.EVENT_STUS_ID = 4) AND (mds.WRKFLW_STUS_ID = 4)))  AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate	


	ORDER BY mds.EVENT_ID
	/*New MDS Eevnts Code Ends  */
	
	CLOSE SYMMETRIC KEY FS@K3y;
	
	SET NOCOUNT OFF;
	--RETURN 0;		
	
--END TRY

--BEGIN CATCH
--	DECLARE @Desc VARCHAR(200)
--	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptEventsWorked ' + CAST(@QueryNumber AS VARCHAR(4))
--	SET @Desc=@Desc + ',' + CAST(@Secured AS VARCHAR(2)) + ': '
--	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)	
--	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
--	RETURN 1;
--END CATCH


END