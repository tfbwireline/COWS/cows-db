  
  
--exec sp_MPLSAccsEventCircuitOD '' ,'12-01-2009','12-31-2012'  
  
--==================================================================================  
-- Project:   Maintainance  
-- Author:   Arti Mashalkar  
-- Date:   01/02/2013 
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME  
-- Description:    
--     Extract MPLS ACCs Event Circuit Information for specified parameter(s);  
--     Date Range and EventID  
--==================================================================================  
-- 2/25/2013 axm3320 removed rec_stus_id from mplseventaccess table select  
  
ALTER Procedure [dbo].[sp_MPLSAccsEventCircuitOD]     
 @SecuredUser  INT=0,  
 @getSensData  CHAR(1)='N',     
 @startDate   Datetime=NULL,    
 @endDate   Datetime=NULL      
AS    
BEGIN TRY    
    
 SET NOCOUNT ON;    
   
 declare @SQLQuery varchar(max);  
 declare @SQLWhere varchar(max);  
 declare @SQLOrder varchar(max);  
   
   
 DECLARE @startDateStr VARCHAR(20)    
 DECLARE @endDateStr  VARCHAR(20)     
   
select @SQLQuery = 'select mplsat.PL_DAL_CKT_NBR as [FMS_NBR] , mplsat.TRS_NET_449_ADR as [NUA_NBR]  
            , ''  '' as [STORE_ID]  
            , mplsat.LOC_CTY_NME as [CITY]  
            , mplsat.LOC_STT_NME as [STATE]
            , CASE WHEN (evl.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE mplsat.LOC_CTY_NME  END AS LOC_CTY_NME /*Monir 06062018*/
	        , CASE WHEN (evl.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.STT_PRVN_NME) ELSE mplsat.LOC_STT_NME  END AS LOC_STT_NME   /*Monir 06062018*/  
            , mplsat.EVENT_ID as [EVENT]  
            , mpls.STRT_TMST  as [EVENT_Date]  
            , usr.full_nme as [ASSIGNED TO]  
            , '' '' as [LANE OF FAILURE(Rescheduled)]  
            , '' '' as [First Time Turn-UP]  
            , '' '' as [Pre-Config]  
            , mpls.DES_CMNT_TXT as [Notes]  
            , '' '' as [Service Ticket]  
            , mplsat.vas_sol_nbr as [VAS_FTN]  
            , mplsat.trnsprt_oe_ftn_nbr as [Transport_FTN]  
from COWS.dbo.mpls_event mpls with (nolock)   
join COWS.dbo.MPLS_EVENT_ACCS_TAG mplsat with (nolock) on mpls.event_id = mplsat.event_id   
join COWS.dbo.event_asn_to_user eatu with (nolock) on mpls.event_id = eatu.event_id   
join COWS.dbo.LK_USER usr with (nolock) on eatu.asn_to_user_id = usr.user_id  
LEFT OUTER JOIN COWS.dbo.EVENT evl WITH (NOLOCK) ON evl.EVENT_ID = mpls.EVENT_ID
LEFT JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mpls.MPLS_EVENT_ACCS_TAG_ID  AND csd.SCRD_OBJ_TYPE_ID=28              
where eatu.rec_stus_id = 1 AND mpls.EVENT_STUS_ID <> 8 '  
              
              
  
 IF @startDate is NOT NULL  AND @endDate is NOT NULL    
  BEGIN    
   -- Do the conversion before passing them to @aSQL    
   SET @startDateStr = @startDate    
   SET @endDateStr = @endDate    
     
   SET @SQLQuery=@SQLQuery + ' AND mpls.STRT_TMST >= ''' + @startDateStr + ''' AND mpls.STRT_TMST < DATEADD(day, 1, ''' + @endDateStr + ''') '          
       
  END     
    
  SET @SQLOrder = ' order by event_date , event , city , state , fms_nbr'  
  
--print @SQLQuery + @SQLOrder  
  
exec ( @SQLQuery + @SQLOrder)  
  
  
    
 RETURN 0;    
     
END TRY    
    
BEGIN CATCH    
 DECLARE @Desc VARCHAR(200)    
 SET @Desc='EXEC COWS_Reporting.dbo.sp_MPLSAccsEventCircuitOD, '      
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)    
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;    
 RETURN 1;    
END CATCH    
  
  
  
  