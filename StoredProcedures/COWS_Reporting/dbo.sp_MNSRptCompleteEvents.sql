USE [COWS_Reporting]
GO
/*
--=========================================================================================
-- Project:			Maintenance 
-- Author:			Jagan	
-- Date:			01/27/2014
-- Description:		
-- 01/27/2017		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked	
--[dbo].[sp_MNSRptCompleteEvents] 0
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--=========================================================================================
*/
ALTER Procedure [dbo].[sp_MNSRptCompleteEvents] 
	@secured		BIT = 0
		
AS

BEGIN TRY

OPEN SYMMETRIC KEY FS@K3y             
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

SELECT DISTINCT
dbo.[GetEventReviewerName] (me.EVENT_ID) as 'MNS Reviewer',
dbo.[GetEventSubmitterName] (me.EVENT_ID) as 'Event Submitter', 
CASE 
	--WHEN @Secured = 0 AND ev.SCURD_CD = 1 AND CONVERT(VARCHAR(100), DecryptByKey(me.CUST_NME)) is NOT NULL 
	--THEN 'Private Customer'
	--ELSE ISNULL(CONVERT(VARCHAR(100), DecryptByKey(me.CUST_NME)),'') 
	WHEN @secured = 0 and ev.CSG_LVL_ID =0 THEN me.CUST_NME
	WHEN @secured = 0 and ev.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @secured = 0 and ev.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),me.CUST_NME,'') 
	END [Customer Name],
me.EVENT_ID as 'Event ID',
me.OPT_OUT_REAS_TXT as 'Reason for Opt Out',
me.STRT_TMST as 'Event Date',
COWS.[dbo].[getTimeSlotForEventID] (me.EVENT_ID) as 'Event Time Block',
CASE WHEN lmat.MDS_ACTY_TYPE_ID IN (2,5) THEN lmat.MDS_ACTY_TYPE_DES + ' - ' + (dbo.[GetCommaSepMDSMACActys] (me.EVENT_ID)) ELSE lmat.MDS_ACTY_TYPE_DES END as 'Event type',
COALESCE(scn.SPRINT_CPE_NCR_DES,'') as 'CPE designation',
CASE me.ESCL_CD WHEN 1 THEN 'Yes' ELSE 'No' END as 'Escalation flag'
FROM [COWS].[dbo].[EVENT] ev  WITH (NOLOCK) INNER JOIN
	 COWS.dbo.MDS_EVENT_NEW me WITH (NOLOCK) ON me.EVENT_ID = ev.EVENT_ID INNER JOIN
     COWS.dbo.EVENT_HIST ehst WITH (NOLOCK) ON me.EVENT_ID = ehst.EVENT_ID INNER JOIN
     COWS.dbo.LK_MDS_ACTY_TYPE lmat WITH (NOLOCK) ON me.MDS_ACTY_TYPE_ID = lmat.MDS_ACTY_TYPE_ID LEFT JOIN
     COWS.dbo.MDS_EVENT_MAC_ACTY mma WITH (NOLOCK) ON me.EVENT_ID = mma.EVENT_ID LEFT JOIN
     COWS.dbo.LK_MDS_MAC_ACTY lmm WITH (NOLOCK) ON lmm.MDS_MAC_ACTY_ID = mma.MDS_MAC_ACTY_ID LEFT JOIN
     COWS.dbo.LK_SPRINT_CPE_NCR scn WITH (NOLOCK) ON scn.SPRINT_CPE_NCR_ID = me.SPRINT_CPE_NCR_ID LEFT JOIN
     [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=me.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
WHERE DATEPART(month,ehst.CREAT_DT) = DATEPART(month, dateadd(MM, -1, getdate()))
	and DATEPART(YEAR,ehst.CREAT_DT) = DATEPART(YEAR, DATEADD(mm, -1, GETDATE()))
  and ehst.ACTN_ID = 17
  and me.MDS_ACTY_TYPE_ID != 3
  and LEN(COALESCE(me.OPT_OUT_REAS_TXT, '')) > 0

/*New MDS Events Code Start */
	UNION ALL
SELECT DISTINCT
dbo.[GetEventReviewerName] (me.EVENT_ID) as 'MNS Reviewer',
dbo.[GetEventSubmitterName] (me.EVENT_ID) as 'Event Submitter', 
CASE 
	--WHEN @Secured = 0 AND ev.SCURD_CD = 1 AND CONVERT(VARCHAR(100), DecryptByKey(me.CUST_NME)) is NOT NULL 
	--THEN 'Private Customer'
	--ELSE ISNULL(CONVERT(VARCHAR(100), DecryptByKey(me.CUST_NME)),'') 	
	WHEN @secured = 0 and ev.CSG_LVL_ID =0 THEN me.CUST_NME
	WHEN @secured = 0 and ev.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @secured = 0 and ev.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),me.CUST_NME,'') 
	END [Customer Name],
me.EVENT_ID as 'Event ID',
ORS.OPT_OUT_REAS_DES as 'Reason for Opt Out',
me.STRT_TMST as 'Event Date',
COWS.[dbo].[getTimeSlotForEventID] (me.EVENT_ID) as 'Event Time Block',
CASE WHEN lmat.MDS_ACTY_TYPE_ID IN (2,5) THEN lmat.MDS_ACTY_TYPE_DES + ' - ' + (dbo.[GetCommaSepMDSMACActys] (me.EVENT_ID)) ELSE lmat.MDS_ACTY_TYPE_DES END as 'Event type',
COALESCE(scn.SPRINT_CPE_NCR_DES,'') as 'CPE designation',
CASE me.ESCL_CD WHEN 1 THEN 'Yes' ELSE 'No' END as 'Escalation flag'
FROM [COWS].[dbo].[EVENT] ev  WITH (NOLOCK) INNER JOIN
	 COWS.dbo.MDS_EVENT me WITH (NOLOCK) ON me.EVENT_ID = ev.EVENT_ID INNER JOIN
     COWS.dbo.EVENT_HIST ehst WITH (NOLOCK) ON me.EVENT_ID = ehst.EVENT_ID INNER JOIN
     COWS.dbo.LK_MDS_ACTY_TYPE lmat WITH (NOLOCK) ON me.MDS_ACTY_TYPE_ID = lmat.MDS_ACTY_TYPE_ID LEFT JOIN
     COWS.dbo.MDS_EVENT_MAC_ACTY mma WITH (NOLOCK) ON me.EVENT_ID = mma.EVENT_ID LEFT JOIN
     COWS.dbo.LK_MDS_MAC_ACTY lmm WITH (NOLOCK) ON lmm.MDS_MAC_ACTY_ID = mma.MDS_MAC_ACTY_ID LEFT JOIN
     COWS.dbo.LK_SPRINT_CPE_NCR scn WITH (NOLOCK) ON scn.SPRINT_CPE_NCR_ID = me.SPRINT_CPE_NCR_ID LEFT JOIN
	 COWS.[dbo].[LK_OPT_OUT_REAS] ORS WITH (NOLOCK) ON me.OPT_OUT_REAS_ID=ORS.OPT_OUT_REAS_ID  LEFT JOIN
	 [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=me.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
WHERE DATEPART(month,ehst.CREAT_DT) = DATEPART(month, dateadd(MM, -1, getdate()))
	and DATEPART(YEAR,ehst.CREAT_DT) = DATEPART(YEAR, DATEADD(mm, -1, GETDATE()))
  and ehst.ACTN_ID = 17
  and me.MDS_ACTY_TYPE_ID != 3
  and LEN(COALESCE(me.OPT_OUT_REAS_ID, '')) > 0
/*New MDS Events Code OLD */
ORDER BY me.EVENT_ID DESC
		
  	RETURN 0;
  	  	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(100)
	SET @Desc='EXEC COWS_Reporting.dbo.MNSRptCompleteEvents '
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH
