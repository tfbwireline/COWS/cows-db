USE [COWS_Reporting]
GO
/*
---- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Notes:
-- 9/4/2012 : Got latest version from Prod and Creating new SP for PJ007370
-- =============================================
-- axm3320: 10/29/2012 added H5- Address Fields,H5 PH_NBR
--axm3320: 10/30/2012 fix added for dupliacte records for VNDR_CNFRM_DSCNCT_DT
--kh946640: 04/21/2017 added new columns. 
--kh946640: 07/11/2017 added 1 new column [CPE_User_Assigned]. 
--kh946640: 07/26/2017 added 1 new column [COWS_Vendor_Circuit_ID]. 
--vn370313: 08/04/2017 Added 2 columns CPE_VNDR_NME is Carrier Name  SHPMT_TRK_NBR is Ship Track # from FSA_ORDR_GOM_XNCI  table
--Updated By:   Md M Monir
--Updated Date: 03/19/2018
--Updated Reason: SCURD_CD/CSG_LVL_CD
*/
ALTER PROCEDURE [dbo].[sp_LassieView_New] 
----Add the parameters for the stored procedure here
AS
BEGIN

	SET NOCOUNT ON;
	
TRUNCATE TABLE [COWS_Reporting].[dbo].[LassieOrderData]

DECLARE @ProcessDateTime DATETIME = GETDATE()
SET @ProcessDateTime = DATEADD(mm,-18,@ProcessDateTime)
	
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 

IF OBJECT_ID(N'tempdb..#OrdrList', N'U') IS NOT NULL
			DROP TABLE #OrdrList
CREATE TABLE #OrdrList (ID INT IDENTITY(1,1), ORDR_ID INT, FTN VARCHAR(20), DMSTC_CD BIT, ORDR_TYPE_CD CHAR(2), ORDR_ACTN_ID TINYINT)
INSERT INTO #OrdrList (ORDR_ID, FTN, DMSTC_CD, ORDR_TYPE_CD, ORDR_ACTN_ID)
select DISTINCT od.ordr_id, fs.FTN, od.DMSTC_CD, fs.ORDR_TYPE_CD, fs.ORDR_ACTN_ID
from [COWS].[dbo].[ORDR] od with (nolock) LEFT OUTER JOIN
[COWS].[dbo].[FSA_ORDR] fs with (nolock) ON fs.ORDR_ID=od.ORDR_ID LEFT OUTER JOIN
[COWS].[dbo].[IPL_ORDR] ipo with (nolock) ON ipo.ORDR_ID=od.ORDR_ID
WHERE od.CREAT_DT>=@ProcessDateTime
AND ((ipo.ordr_stus_id is null) or (ipo.ORDR_STUS_ID !=20))
AND ((od.ORDR_CAT_ID IN (1,5)) OR ((od.ORDR_CAT_ID IN (2,6)) AND ((od.DMSTC_CD = 1) OR (od.DMSTC_CD = 0
			AND (fs.PROD_TYPE_CD = 'MN' 
			OR fs.PROD_TYPE_CD = 'SE' OR fs.PROD_TYPE_CD ='CP' or ((fs.PROD_TYPE_CD = 'IP' 
			AND (fs.CPE_CPE_ORDR_TYPE_CD = 'MNS' OR ISNULL(fs.TTRPT_MNGD_DATA_SRVC_CD,'') = 'Y'))
			))))))
CREATE INDEX IDX_#OrdrList ON #OrdrList(ORDR_ID)
CREATE INDEX IDX_#OrdrList_2 ON #OrdrList(FTN)

IF OBJECT_ID(N'tempdb..#MaxActTask', N'U') IS NOT NULL
			DROP TABLE #MaxActTask

CREATE TABLE #MaxActTask (ID INT IDENTITY(1,1), MAX_ACT_TASK_ID INT, MIN_ACT_TASK_ID INT, ORDR_ID INT, STUS_ID TINYINT)
INSERT INTO #MaxActTask (MAX_ACT_TASK_ID, MIN_ACT_TASK_ID, ORDR_ID, STUS_ID)
select max(act_task_id) as max_act_task_id, min(ACT_TASK_ID) as MIN_ACT_TASK_ID, tsk.ordr_id, STUS_ID
from [COWS].[dbo].[ACT_TASK] tsk with (nolock) INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=tsk.ORDR_ID
group by tsk.ordr_id, STUS_ID
CREATE INDEX IDX_#MaxActTask ON #MaxActTask(ORDR_ID)

IF OBJECT_ID(N'tempdb..#MaxUWA', N'U') IS NOT NULL
			DROP TABLE #MaxUWA
CREATE TABLE #MaxUWA (ID INT IDENTITY(1,1), MAX_ASN_USER_ID INT, MIN_ASN_USER_ID INT, MAX_ASMT_DT SMALLDATETIME, MIN_ASMT_DT SMALLDATETIME, ORDR_ID INT)
INSERT INTO #MaxUWA (MAX_ASN_USER_ID, MAX_ASMT_DT, MIN_ASN_USER_ID, MIN_ASMT_DT, ORDR_ID)
select max(ASN_USER_ID), max(ASMT_DT), min(ASN_USER_ID), min(ASMT_DT), uwa.ordr_id
FROM [COWS].[dbo].[USER_WFM_ASMT] uwa with (nolock) INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=uwa.ORDR_ID
group by uwa.ORDR_ID
CREATE INDEX IDX_#MaxUWA ON #MaxUWA(ORDR_ID)

IF OBJECT_ID(N'tempdb..#MaxSOICD', N'U') IS NOT NULL
			DROP TABLE #MaxSOICD

CREATE TABLE #MaxSOICD (ID INT IDENTITY(1,1), MAX_SOI_CD VARCHAR(100), ORDR_ID INT)
INSERT INTO #MaxSOICD (MAX_SOI_CD, ORDR_ID)
select max(SOI_CD), foc.ordr_id
FROM [COWS].[dbo].[FSA_ORDR_CUST] foc with (nolock) INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=foc.ORDR_ID
WHERE SOI_CD IS NOT NULL
group by foc.ORDR_ID
CREATE INDEX IDX_#MaxSOICD ON #MaxSOICD(ORDR_ID)

IF OBJECT_ID(N'tempdb..#MaxNTEID', N'U') IS NOT NULL
			DROP TABLE #MaxNTEID

CREATE TABLE #MaxNTEID (ID INT IDENTITY(1,1), MAX_NTE_ID INT, ORDR_ID INT)
INSERT INTO #MaxNTEID (MAX_NTE_ID, ORDR_ID)
select max(NTE_ID), nt.ordr_id
FROM [COWS].[dbo].[ORDR_NTE] nt with (nolock) INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=nt.ORDR_ID
group by nt.ORDR_ID
CREATE INDEX IDX_#MaxNTEID ON #MaxNTEID(ORDR_ID)

IF OBJECT_ID(N'tempdb..#MaxNRMCKTID', N'U') IS NOT NULL
			DROP TABLE #MaxNRMCKTID

CREATE TABLE #MaxNRMCKTID (ID INT IDENTITY(1,1), MAX_NRM_CKT_ID INT, FTN VARCHAR(20))
INSERT INTO #MaxNRMCKTID (MAX_NRM_CKT_ID, FTN)
select max(NRM_CKT_ID), nc.FTN
FROM [COWS].[dbo].[NRM_CKT] nc with (nolock) INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.FTN=nc.FTN
WHERE nc.FTN IS NOT NULL AND nc.NRM_CKT_ID IS NOT NULL
group by nc.FTN
CREATE INDEX IDX_#MaxNRMCKTID ON #MaxNRMCKTID(FTN)

IF OBJECT_ID(N'tempdb..#MaxCCDHIST', N'U') IS NOT NULL
			DROP TABLE #MaxCCDHIST

CREATE TABLE #MaxCCDHIST (ID INT IDENTITY(1,1), MAX_CCD_HIST_ID INT, ORDR_ID INT)
INSERT INTO #MaxCCDHIST (MAX_CCD_HIST_ID, ORDR_ID)
select max(CCD_HIST_ID), ch.ORDR_ID
FROM [COWS].[dbo].[CCD_HIST] ch with (nolock) INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=ch.ORDR_ID
group by ch.ORDR_ID
CREATE INDEX IDX_#MaxCCDHIST ON #MaxCCDHIST(ORDR_ID)

--IF OBJECT_ID(N'tempdb..#MaxCKT', N'U') IS NOT NULL
--			DROP TABLE #MaxCKT

--CREATE TABLE #MaxCKT (ID INT IDENTITY(1,1), CKT_ID INT, ORDR_ID INT,VNDR_CKT_ID VARCHAR(200))
--INSERT INTO #MaxCKT (CKT_ID, ORDR_ID, VNDR_CKT_ID)
--select max(CKT_ID), c.ORDR_ID, c.VNDR_CKT_ID
--FROM [COWS].[dbo].[CKT] c with (nolock) INNER JOIN
--#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=c.ORDR_ID 
--WHERE CKT_ID IS NOT NULL
--AND ol.DMSTC_CD = 1 and ol.ORDR_TYPE_CD = 'IN' and ol.ORDR_ACTN_ID <>3
--group by c.ORDR_ID, c.VNDR_CKT_ID
--CREATE INDEX IDX_#MaxCKT ON #MaxCKT(ORDR_ID)

IF OBJECT_ID(N'tempdb..#MaxCKTMS', N'U') IS NOT NULL
			DROP TABLE #MaxCKTMS

CREATE TABLE #MaxCKTMS (ID INT IDENTITY(1,1), ORDR_ID INT, CKT_ID INT, VER_ID SMALLINT, VNDR_CKT_ID VARCHAR(200))
INSERT INTO #MaxCKTMS (ORDR_ID, CKT_ID, VNDR_CKT_ID, VER_ID)
select c.ORDR_ID, cm.CKT_ID, c.VNDR_CKT_ID, max(cm.VER_ID)
FROM [COWS].[dbo].[CKT_MS] cm with (nolock) INNER JOIN
[COWS].[dbo].[CKT] c with (nolock) ON c.CKT_ID=cm.CKT_ID INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=c.ORDR_ID
WHERE cm.CKT_ID IS NOT NULL AND cm.VER_ID IS NOT NULL
AND ol.DMSTC_CD = 1 and ol.ORDR_TYPE_CD = 'DC' and ol.ORDR_ACTN_ID <>3
group by c.ORDR_ID, cm.CKT_ID, c.VNDR_CKT_ID
CREATE INDEX IDX_#MaxCKTMS ON #MaxCKTMS(ORDR_ID, CKT_ID, VNDR_CKT_ID)

IF OBJECT_ID(N'tempdb..#MaxVndrOrdrMSI', N'U') IS NOT NULL
			DROP TABLE #MaxVndrOrdrMSI

CREATE TABLE #MaxVndrOrdrMSI (ID INT IDENTITY(1,1), MAX_VNDR_ORDR_MS_ID INT, VNDR_ORDR_ID INT, ORDR_ID INT)
INSERT INTO #MaxVndrOrdrMSI (MAX_VNDR_ORDR_MS_ID, VNDR_ORDR_ID, ORDR_ID)
select max(vo.VNDR_ORDR_MS_ID), vo.VNDR_ORDR_ID, v.ORDR_ID
FROM [COWS].[dbo].[VNDR_ORDR_MS] vo with (nolock) INNER JOIN
[COWS].[dbo].[VNDR_ORDR] v with (nolock) ON vo.VNDR_ORDR_ID=v.VNDR_ORDR_ID INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=v.ORDR_ID
WHERE vo.VNDR_ORDR_ID IS NOT NULL
AND ol.DMSTC_CD = 1 and ol.ORDR_TYPE_CD = 'IN' and ol.ORDR_ACTN_ID <>3
group by vo.VNDR_ORDR_ID, v.ORDR_ID
CREATE INDEX IDX_#MaxVndrOrdrMSI ON #MaxVndrOrdrMSI(VNDR_ORDR_ID, ORDR_ID)

IF OBJECT_ID(N'tempdb..#MaxVndrOrdrMSD', N'U') IS NOT NULL
			DROP TABLE #MaxVndrOrdrMSD

CREATE TABLE #MaxVndrOrdrMSD (ID INT IDENTITY(1,1), MAX_VNDR_ORDR_MS_ID INT, VNDR_ORDR_ID INT, ORDR_ID INT)
INSERT INTO #MaxVndrOrdrMSD (MAX_VNDR_ORDR_MS_ID, VNDR_ORDR_ID, ORDR_ID)
select max(vo.VNDR_ORDR_MS_ID), vo.VNDR_ORDR_ID, v.ORDR_ID
FROM [COWS].[dbo].[VNDR_ORDR_MS] vo with (nolock) INNER JOIN
[COWS].[dbo].[VNDR_ORDR] v with (nolock) ON vo.VNDR_ORDR_ID=v.VNDR_ORDR_ID INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=v.ORDR_ID
WHERE vo.VNDR_ORDR_ID IS NOT NULL
AND ol.DMSTC_CD = 1 and ol.ORDR_TYPE_CD = 'DC' and ol.ORDR_ACTN_ID <>3
group by vo.VNDR_ORDR_ID, v.ORDR_ID
CREATE INDEX IDX_#MaxVndrOrdrMSD ON #MaxVndrOrdrMSD(VNDR_ORDR_ID, ORDR_ID)

IF OBJECT_ID(N'tempdb..#MaxOrdrMS', N'U') IS NOT NULL
			DROP TABLE #MaxOrdrMS

CREATE TABLE #MaxOrdrMS (ID INT IDENTITY(1,1), MAX_VER_ID SMALLINT, ORDR_ID INT)
INSERT INTO #MaxOrdrMS (MAX_VER_ID, ORDR_ID)
select max(om.VER_ID), om.ORDR_ID
FROM [COWS].[dbo].[ORDR_MS] om with (nolock) INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.ORDR_ID=om.ORDR_ID
WHERE om.VER_ID IS NOT NULL
AND ol.DMSTC_CD = 1 and ol.ORDR_TYPE_CD = 'DC' and ol.ORDR_ACTN_ID <>3
group by om.ORDR_ID
CREATE INDEX IDX_#MaxOrdrMS ON #MaxOrdrMS(ORDR_ID)

IF OBJECT_ID(N'tempdb..#MaxNRMSrvc', N'U') IS NOT NULL
			DROP TABLE #MaxNRMSrvc

CREATE TABLE #MaxNRMSrvc (ID INT IDENTITY(1,1), FTN VARCHAR(20), MAX_MODFD_DT SMALLDATETIME)
INSERT INTO #MaxNRMSrvc (FTN, MAX_MODFD_DT)
select nsi.FTN, max(nsi.MODFD_DT)
FROM [COWS].[dbo].NRM_SRVC_INSTC nsi with (nolock) INNER JOIN
#OrdrList ol WITH (NOLOCK) on ol.FTN=nsi.FTN
WHERE nsi.MODFD_DT IS NOT NULL
AND ol.DMSTC_CD = 1 and ol.ORDR_TYPE_CD in ('DC','IN') and ol.ORDR_ACTN_ID <>3
group by nsi.FTN
CREATE INDEX IDX_#MaxNRMSrvc ON #MaxNRMSrvc(FTN)


INSERT INTO [COWS_Reporting].[dbo].[LassieOrderData]
           ([Order_ID],[ORDR_ID],[H1_ID],[H4_ID],[Parent_FTN],[H5_ID],[H6_ID],[COWS_IPM],[COWS_IPM_AD_ID]
           ,[FSA_IPM],[FSA_IPM_Email_Address],[Last_Modified_Date_time],[NUA],[PL#],[CCD],[Bill_Clear_Date]
           ,[Customer_Name],[Escalation_Flag],[Expedite],[Order_Type],[Order_Sub_Type],[Product_Type]
           ,[SOI],[Platform_Name],[Order_Status],[Jeopardy_Code],[Jeopardy_Description],[COWS_Notes]
           ,[System_Order_Status],[StatusDt],[DMSTC_CD],[CHARS_ID],[ORDR_ACTN_DES],[TTRPT_SPD_OF_SRVC_BDWD_DES]
           ,[TTRPT_ACCS_ARNGT_CD],[TTRPT_PRLEL_PRCS_CKT_ID],[TTRPT_MNGD_DATA_SRVC_CD],[TPORT_CUST_IP_ADR]
           ,[CUST_ORDR_SBMT_DT],[SCA_NBR],[H6_CTRY_CD],[H6_CTY_NME],[H6_PRVN_NME],[H6_STREET_ADR_1]
           ,[H6_STREET_ADR_2],[H6_STT_CD],[H5_CTRY_CD],[H5_CTY_NME],[H5_PRVN_NME],[H5_STREET_ADR_1]
           ,[H5_STREET_ADR_2],[H5_STT_CD],[H6_EMAIL_ADR],[H6_PHN_NBR],[H5_PH_NBR],[H6_CUST_NME]
           ,[H6_SOI],[CUST_DLVRY_DT],[HDWR_DLVRD_DT],[VNDR_SHIP_DT],[new_ccd_dt],[ACCS_ACPTC_DT]
           ,[ACCS_DLVRY_DT],[TRGT_DLVRY_DT],[VNDR_CNFRM_DSCNCT_DT],[SENT_TO_VNDR_DT_I],[SENT_TO_VNDR_DT_D]
           ,[ORDR_DSCNCT_DT],[NRM_ORDR_STUS_DES],[MRC_CHG_AMT],[NRC_CHG_AMT],[H6_ZIP_PSTL_CD],[SITE_ID]
           ,[COWS_Close_Date],[COWS_Validated_Date],[COWS_Order_Type],[COWS_Product_Type],[COWS_PL],[COWS_Vendor_ACK_Date]
           ,[COWS_LECID],[BPM_Assigned_To],[BPM_Tech_Assigned],[BPM_Tech_Email],[BPM_Tech_Phone],[BPM_Dispatch_Date]
           ,[BPM_Dispatch_Time],[BPM_Onsite_Date],[BPM_Onsite_Time],[BPM_Inroute_Date],[BPM_Inroute_Time]
           ,[BPM_Completed_Date],[BPM_Completed_Time],[CPE_User_Assigned],[COWS_Vendor_Circuit_ID],[SHPMT_TRK_NBR],
           [Carrier_Name])

SELECT DISTINCT
   CASE 
		WHEN ordr.ORDR_CAT_ID IN (1,5) THEN CONVERT(varchar(50),z.ORDR_ID)
		ELSE fsa.FTN 
		END [Order_ID],
   ordr.ORDR_ID,		
   h1.CUST_ID [H1_ID], 
   h4.CUST_ID [H4_ID], 
   fsa.PRNT_FTN [Parent_FTN],
    
   CASE 
		WHEN ordr.ORDR_CAT_ID = 6 THEN h5.CUST_ID
		WHEN ordr.ORDR_CAT_ID = 2 THEN h5.CUST_ID END [H5_ID],
		
   h6.CUST_ID [H6_ID],
   x.FULL_NME [COWS_IPM],	 	 	 
   x.USER_ADID [COWS_IPM_AD_ID],
   	
   tac.FRST_NME + tac.LST_NME [FSA_IPM],	
   tac.EMAIL_ADR [FSA_IPM_Email_Address],
   	
   CASE 
		WHEN tk.CREAT_DT > nt.CREAT_DT THEN tk.CREAT_DT
    ELSE nt.CREAT_DT END [Last_Modified_Date_time],
    
    nm.NUA_ADR [NUA],
    nm.PLN_NME [PL#],
    
	CASE 
		WHEN ordr.ORDR_CAT_ID IN (1,5) AND 	cd.NEW_CCD_DT IS NULL THEN z.CUST_CMMT_DT
		WHEN ordr.ORDR_CAT_ID = 2 AND 	cd.NEW_CCD_DT IS NULL THEN fsa.CUST_CMMT_DT
		WHEN ordr.ORDR_CAT_ID = 6 AND 	cd.NEW_CCD_DT IS NULL THEN fsa.CUST_CMMT_DT 
		WHEN cd.NEW_CCD_DT IS NOT NULL THEN cd.NEW_CCD_DT END [CCD],
		
      CASE
		  WHEN tk.TASK_ID = 1001 THEN tk.CREAT_DT ELSE NULL END [Bill_Clear_Date],
		  
 	  CASE
		WHEN ordr.CSG_LVL_ID =0  AND ordr.ORDR_CAT_ID IN (1,5) THEN ISNULL( aa.CUST_NME, '')
		WHEN ordr.CSG_LVL_ID =0  AND ordr.ORDR_CAT_ID = 2 THEN si.CUST_NME
		WHEN ordr.CSG_LVL_ID =0  AND ordr.ORDR_CAT_ID = 6 THEN si.CUST_NME
		WHEN ordr.CSG_LVL_ID >0  AND ordr.ORDR_CAT_ID = 6 THEN 'PRIVATE CUSTOMER' 
		WHEN ordr.CSG_LVL_ID >0  AND ordr.ORDR_CAT_ID = 2 THEN 'PRIVATE CUSTOMER' 
		END  [Customer_Name],
		
	CASE 
		WHEN ordr.ORDR_CAT_ID IN (1,5) AND h.ESCALAT_ORDR_CD = 0 THEN 'N'
		WHEN ordr.ORDR_CAT_ID IN (1,5) AND h.ESCALAT_ORDR_CD = 1 THEN 'Y'
		WHEN ordr.ORDR_CAT_ID = 2 THEN fsa.INSTL_ESCL_CD
		WHEN ordr.ORDR_CAT_ID = 6 THEN fsa.INSTL_ESCL_CD END [Escalation_Flag],
		
	u.EXP_TYPE_DES [Expedite],
	
	CASE 
		WHEN ordr.ORDR_CAT_ID IN (1,5) THEN m.ORDR_TYPE_DES
		WHEN ordr.ORDR_CAT_ID = 6 THEN s.FSA_ORDR_TYPE_DES
		WHEN ordr.ORDR_CAT_ID = 2 THEN s.FSA_ORDR_TYPE_DES END [Order_Type],
		
    fsa.ORDR_SUB_TYPE_CD [Order_Sub_Type],
    
 	CASE 
		WHEN ordr.ORDR_CAT_ID IN (1,5) THEN t.PROD_TYPE_DES
		WHEN ordr.ORDR_CAT_ID = 6 THEN  i.PROD_TYPE_DES
		WHEN ordr.ORDR_CAT_ID = 2 THEN  i.PROD_TYPE_DES END [Product_Type],
		
 	CASE 
		WHEN ordr.ORDR_CAT_ID IN (1,5) THEN  z.SOI
		WHEN ordr.ORDR_CAT_ID = 6 THEN  si.SOI_CD
		WHEN ordr.ORDR_CAT_ID = 2 THEN  si.SOI_CD END [SOI],
       
    j.PLTFRM_NME [Platform_Name],
    o.ORDR_STUS_DES [Order_Status],
    q.JPRDY_CD [Jeopardy_Code] ,
    ISNULL(jep.CPE_JPRDY_DES,r.JPRDY_DES) [Jeopardy_Description],
    SUBSTRING(nt.NTE_TXT,1, 500)  [COWS_Notes],

    CASE WHEN ordr.ORDR_STUS_ID  = 1 then -- Pending
           CASE when fsa.PROD_TYPE_CD = 'CP' and ordr.DMSTC_CD = 1 Then
             Case when tk.TASK_ID = 102 then 'GOM Milestones Pending'
             else 
               Case when ISNULL((CASE WHEN lt1.TASK_ID  = 1001 THEN 'Completed' ELSE lt1.TASK_NME + '-' + s1.STUS_DES END),'') = '' then (CASE WHEN lt2.TASK_ID  = 1001 THEN 'Completed' ELSE lt2.TASK_NME + '-' + s2.STUS_DES END)
                else  (CASE WHEN lt1.TASK_ID  = 1001 THEN 'Completed' ELSE lt1.TASK_NME + '-' + s1.STUS_DES END)
               END  
             END
           Else 
            Case when ISNULL((CASE WHEN lt1.TASK_ID  = 1001 THEN 'Completed' ELSE lt1.TASK_NME + '-' + s1.STUS_DES END),'') = '' then (CASE WHEN lt2.TASK_ID  = 1001 THEN 'Completed' ELSE lt2.TASK_NME + '-' + s2.STUS_DES END)
               else  (CASE WHEN lt1.TASK_ID  = 1001 THEN 'Completed' ELSE lt1.TASK_NME + '-' + s1.STUS_DES END)
             END  
           END
     Else o.ORDR_STUS_DES -- Completed      
     END [System_Order_Status],  
     acttsk1.CREAT_DT [StatusDt],
     ordr.DMSTC_CD [DMSTC_CD],
     ordr.CHARS_ID [CHARS_ID],
     'Submit'  [ORDR_ACTN_DES],
     fsa.TTRPT_SPD_OF_SRVC_BDWD_DES,
     focl.TTRPT_ACCS_ARNGT_CD,
     fsa.TTRPT_PRLEL_PRCS_CKT_ID,
     fsa.TTRPT_MNGD_DATA_SRVC_CD,
     fsa.TPORT_CUST_IP_ADR,
     CASE When ordr.ORDR_CAT_ID IN (1,5) then z.CUST_ORDR_SBMT_DT
     WHEN ordr.ORDR_CAT_ID = 6 Then fsa.CUST_ORDR_SBMT_DT 
     WHEN ordr.ORDR_CAT_ID = 2 Then fsa.CUST_ORDR_SBMT_DT END [ CUST_ORDR_SBMT_DT],
     CASE When ordr.ORDR_CAT_ID IN (1,5) then z.SCA_NBR
     WHEN ordr.ORDR_CAT_ID = 6 Then fsa.SCA_NBR  
     WHEN ordr.ORDR_CAT_ID = 2 Then fsa.SCA_NBR END [SCA_NBR],
           
     adh6.CTRY_CD as H6_CTRY_CD,
     CASE
		WHEN adh6.CTY_NME IS NULL THEN NULL
		WHEN ordr.CSG_LVL_ID =0 THEN  adh6.CTY_NME
		ELSE 'OVERLAND PARK'
	 END  [H6_CTY_NME],
	 CASE
	    WHEN adh6.PRVN_NME IS NULL THEN NULL
		WHEN ordr.CSG_LVL_ID = 0 THEN  adh6.PRVN_NME
		ELSE NULL
	 END   [H6_PRVN_NME],
	 CASE
	    WHEN adh6.STREET_ADR_1 IS NULL THEN NULL
		WHEN ordr.CSG_LVL_ID = 0 THEN  adh6.STREET_ADR_1
		ELSE '6200 SPRINT PARKWAY'
	 END   [H6_STREET_ADR_1],
	 CASE
	    WHEN adh6.STREET_ADR_2 IS NULL THEN NULL
		WHEN ordr.CSG_LVL_ID = 0 THEN  adh6.STREET_ADR_2
		ELSE NULL
	 END   [H6_STREET_ADR_2],
     CASE
        WHEN adh6.STT_CD IS NULL THEN NULL
		WHEN ordr.CSG_LVL_ID = 0 THEN  adh6.STT_CD
		ELSE 'KS'
	 END   [H6_STT_CD],

     CASE WHEN adh5.CTRY_CD IS NULL THEN (CASE WHEN (ISNULL(hf.CTRY_CD,'')!='') THEN hf.CTRY_CD ELSE NULL END)
	 ELSE adh5.CTRY_CD
	 END as H5_CTRY_CD,
     CASE
        WHEN adh5.CTY_NME IS NULL THEN (CASE WHEN (ISNULL(hf.CUST_CTY_NME,'')!='') THEN hf.CUST_CTY_NME ELSE NULL END)
		WHEN ordr.CSG_LVL_ID = 0 THEN  adh5.CTY_NME
		ELSE 'OVERLAND PARK'
	 END   [H5_CTY_NME],
     CASE
        WHEN adh5.PRVN_NME IS NULL THEN NULL
		WHEN ordr.CSG_LVL_ID = 0 THEN  adh5.PRVN_NME
		ELSE NULL
	 END   [H5_PRVN_NME],
     CASE
        WHEN adh5.STREET_ADR_1 IS NULL THEN NULL
		WHEN ordr.CSG_LVL_ID = 0 THEN  adh5.STREET_ADR_1
		ELSE '6200 SPRINT PARKWAY'
	 END   [H5_STREET_ADR_1],
	 CASE
	    WHEN adh5.STREET_ADR_2 IS NULL THEN NULL
		WHEN ordr.CSG_LVL_ID = 0 THEN  adh5.STREET_ADR_2
		ELSE NULL
	 END   [H5_STREET_ADR_2],
     CASE
        WHEN adh5.STT_CD IS NULL THEN NULL
		WHEN ordr.CSG_LVL_ID = 0 THEN  adh5.STT_CD
		ELSE 'KS'
	 END   [H5_STT_CD],
     CASE 
		WHEN ordr.ORDR_CAT_ID = 6 THEN CntctH6M.EMAIL_ADR
		ELSE CntctH6.EMAIL_ADR
		END [H6_EMAIL_ADR],
	 CASE 
		WHEN ordr.ORDR_CAT_ID = 6 THEN CntctH6M.PHN_NBR
		ELSE CntctH6.PHN_NBR
		END [H6_PHN_NBR],

     CntctH5.PHN_NBR as H5_PH_NBR, 
     CASE WHEN (h6.CUST_NME IS NULL) THEN hf.CUST_NME
	 ELSE h6.CUST_NME END AS CUST_NME,
     CASE 
		WHEN ordr.ORDR_CAT_ID = 6 THEN  h6.SOI_CD
		WHEN ordr.ORDR_CAT_ID = 2 THEN  h6.SOI_CD END [H6_SOI],
		fsagom.CUST_DLVRY_DT,
		fsagom.HDWR_DLVRD_DT,
		fsagom.PRCH_ORDR_BACK_ORDR_SHIP_DT,
		
		CASE When cd.new_ccd_dt IS NULL THEN
				  CASE 
					   WHEN ordr.ORDR_CAT_ID = 6 THEN fsa.CUST_CMMT_DT	
					   WHEN ordr.ORDR_CAT_ID = 2 THEN fsa.CUST_CMMT_DT END 
             When cd.new_ccd_dt IS NOT NULL THEN cd.new_ccd_dt END [new_ccd_dt],
             
             cm.ACCS_ACPTC_DT,
             cm.ACCS_DLVRY_DT,
             cm.TRGT_DLVRY_DT,
             cmd.VNDR_CNFRM_DSCNCT_DT, 
             VNDR_SENT_DT_I.SENT_TO_VNDR_DT as SENT_TO_VNDR_DT_I,             
             VNDR_SENT_DT_D.SENT_TO_VNDR_DT as SENT_TO_VNDR_DT_D,
             oms.ORDR_DSCNCT_DT,
             nrmsrvc.ORDR_STUS_DES [NRM_ORDR_STUS_DES],
             ISNULL(fob.MRC_CHG_AMT,0) AS [MRC_CHG_AMT],                
             ISNULL(fob.NRC_CHG_AMT,0) AS [NRC_CHG_AMT],
        CASE
			WHEN adh6.ZIP_PSTL_CD IS NULL THEN NULL
			WHEN ordr.CSG_LVL_ID = 0 THEN  adh6.ZIP_PSTL_CD
			ELSE NULL
		END   [H6_ZIP_PSTL_CD],
		
		h6.SITE_ID,
		
		-- gautam: Addition of NEW COLUMNS
		oms.ORDR_DSCNCT_DT as [COWS_Close_Date], -- this column already exist
		oms.VLDTD_DT as [COWS_Validated_Date],
		fsa.ORDR_TYPE_CD as [COWS_Order_Type],
		fsa.PROD_TYPE_CD as [COWS_Product_Type],
		fsa.PL_NBR as [COWS_PL],
		ISNULL(VNDR_SENT_DT_I.ACK_BY_VNDR_DT, VNDR_SENT_DT_D.ACK_BY_VNDR_DT) as [COWS_Vendor_ACK_Date],
		ISNULL(cm.CKT_ID, cmd.CKT_ID) as [COWS_LECID],
		
		x.USER_ID [BPM_Assigned_To],
		x.FULL_NME [BPM_Tech_Assigned], -- this column already exist
		x.EMAIL_ADR [BPM_Tech_Email],
		x.PHN_NBR [BPM_Tech_Phone],
		ur.BPM_Dispatch_Date,
		ur.BPM_Dispatch_Time,
		ur.BPM_Onsite_Date,
		ur.BPM_Onsite_Time,
		ur.BPM_Inroute_Date,
		ur.BPM_Inroute_Time,
		ur.BPM_Completed_Date,
		ur.BPM_Completed_Time,
		x1.FULL_NME	as [CPE_User_Assigned],
		ISNULL(mcktms.VNDR_CKT_ID, mcktms2.VNDR_CKT_ID) as [COWS_Vendor_Circuit_ID],
		fsagom.[SHPMT_TRK_NBR],
		fsagom.[CPE_VNDR_NME]
      
from  #OrdrList odlist WITH (NOLOCK) INNER JOIN
  	  [COWS].[dbo].[ORDR] ordr WITH (NOLOCK) ON ordr.ORDR_ID=odlist.ORDR_ID LEFT OUTER JOIN
  	  [COWS].[dbo].[FSA_ORDR] fsa WITH (NOLOCK) ON fsa.ORDR_ID=ordr.ORDR_ID LEFT OUTER JOIN
	  [COWS].[dbo].[IPL_ORDR] z with (nolock) on ordr.ORDR_ID = z.ORDR_ID LEFT OUTER JOIN
	  [COWS].[dbo].[H5_FOLDR] hf with (nolock) on ordr.H5_FOLDR_ID = hf.H5_FOLDR_ID LEFT OUTER JOIN
	  [COWS].[dbo].[LK_ORDR_TYPE] m with (nolock) on z.ORDR_TYPE_ID = m.ORDR_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_PROD_TYPE] t with (nolock) on z.PROD_TYPE_ID = t.PROD_TYPE_ID LEFT OUTER JOIN
	  [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] focl WITH (NOLOCK) ON fsa.ORDR_ID=focl.ORDR_ID LEFT OUTER JOIN
	  [COWS].[dbo].[ORDR_CNTCT] tac WITH (NOLOCK) ON  ordr.ORDR_ID  = tac.ORDR_ID AND tac.ROLE_ID = 13 LEFT OUTER JOIN		
      [COWS].[dbo].[LK_CNTCT_TYPE] e with (nolock) on  tac.CNTCT_TYPE_ID = e.CNTCT_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[TRPT_ORDR] g with (nolock) on ordr.ORDR_ID = g.ORDR_ID LEFT OUTER JOIN
      [COWS].[dbo].[ORDR_EXP] h with (nolock) on ordr.ORDR_ID = h.ORDR_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_EXP_TYPE] l with (nolock) on h.EXP_TYPE_ID = l.EXP_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_PROD_TYPE] i with (nolock) on fsa.PROD_TYPE_CD = i.FSA_PROD_TYPE_CD LEFT OUTER JOIN
   	  [COWS].[dbo].[lk_FSA_ORDR_TYPE] s with (nolock) on fsa.ORDR_TYPE_CD = s.FSA_ORDR_TYPE_CD LEFT OUTER JOIN
      [COWS].[dbo].[PLTFRM_MAPNG] k with (nolock) on ordr.[PLTFRM_CD] = k.PLTFRM_CD LEFT OUTER JOIN
      [COWS].[dbo].[LK_PLTFRM] j with (nolock) on k.PLTFRM_CD = j.PLTFRM_CD LEFT OUTER JOIN
      (SELECT atk.ACT_TASK_ID, atk.TASK_ID, atk.ORDR_ID, atk.CREAT_DT
	  FROM [COWS].[dbo].[ACT_TASK] atk WITH (NOLOCK) INNER JOIN
	  #MaxActTask mat WITH (NOLOCK) ON mat.ORDR_ID=atk.ORDR_ID AND atk.ACT_TASK_ID=mat.MAX_ACT_TASK_ID) as tk ON tk.ORDR_ID=ordr.ORDR_ID LEFT OUTER JOIN			
      [COWS].[dbo].[LK_TASK] n with (nolock) on tk.TASK_ID = n.TASK_ID LEFT OUTER JOIN
       [COWS].[dbo].[LK_ORDR_STUS]o with (nolock) on ordr.ORDR_STUS_ID = o.ORDR_STUS_ID LEFT OUTER JOIN
      [COWS].[dbo].[ORDR_JPRDY] q with (nolock) on ordr.ORDR_ID = q.ORDR_ID LEFT OUTER JOIN 
      [COWS].[dbo].[LK_JPRDY] r with (nolock) on q.JPRDY_CD = r.JPRDY_CD LEFT OUTER JOIN
      [COWS].[dbo].[LK_CPE_JPRDY] jep with (nolock) on q.JPRDY_CD = jep.CPE_JPRDY_CD LEFT OUTER JOIN
  	  (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID, 
  	  CONVERT(date, bb.DSPTCH_TM) as [BPM_Dispatch_Date],
  	  CONVERT(char(10), bb.DSPTCH_TM, 108) as [BPM_Dispatch_Time],
  	  CONVERT(date, bb.ONSITE_TM) as [BPM_Onsite_Date],
  	  CONVERT(char(10), bb.ONSITE_TM, 108) as [BPM_Onsite_Time],
  	  CONVERT(date, bb.INRTE_TM) as [BPM_Inroute_Date],
  	  CONVERT(char(10), bb.INRTE_TM, 108) as [BPM_Inroute_Time],
  	  CONVERT(date, bb.CMPLT_TM) as [BPM_Completed_Date],
  	  CONVERT(char(10), bb.CMPLT_TM, 108) as [BPM_Completed_Time]
  		FROM [COWS].[dbo].[USER_WFM_ASMT] bb with (nolock) INNER JOIN
		#MaxUWA muwa with (nolock) ON muwa.MAX_ASMT_DT=bb.ASMT_DT and muwa.MAX_ASN_USER_ID=bb.ASN_USER_ID and muwa.ordr_id=bb.ORDR_ID) ur 
			on  ordr.ORDR_ID = ur.ORDR_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_USER] x with (nolock) on ur.ASN_USER_ID =  x.[USER_ID] LEFT OUTER JOIN
      
      (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID  	  
  		FROM [COWS].[dbo].[USER_WFM_ASMT] bb with (nolock) INNER JOIN
		#MaxUWA muwa with (nolock) ON muwa.MIN_ASMT_DT=bb.ASMT_DT and muwa.MIN_ASN_USER_ID=bb.ASN_USER_ID and muwa.ordr_id=bb.ORDR_ID) ur1 
			on  ordr.ORDR_ID = ur1.ORDR_ID LEFT OUTER JOIN			
      [COWS].[dbo].[LK_USER] x1 with (nolock) on ur1.ASN_USER_ID =  x1.[USER_ID] LEFT OUTER JOIN
      
      [COWS].[dbo].[LK_ROLE]y with (nolock) on tac.ROLE_ID = y.ROLE_ID   LEFT OUTER JOIN
      [COWS].[dbo].[FSA_ORDR_CUST] h1 with (nolock) on h1.ORDR_ID=ordr.ORDR_ID and h1.CIS_LVL_TYPE='H1' LEFT OUTER JOIN
	  [COWS].[dbo].[FSA_ORDR_CUST] h4 with (nolock) on h4.ORDR_ID=ordr.ORDR_ID and h4.CIS_LVL_TYPE='H4'  LEFT OUTER JOIN
	  [COWS].[dbo].[FSA_ORDR_CUST] h5 with (nolock) on h5.ORDR_ID=ordr.ORDR_ID and h5.CIS_LVL_TYPE = 'H5' LEFT OUTER JOIN
	  [COWS].[dbo].[FSA_ORDR_CUST] h6 with (nolock) on h6.ORDR_ID=ordr.ORDR_ID and h6.CIS_LVL_TYPE IN ('H5','H6') LEFT OUTER JOIN
 	  (SELECT bb.ORDR_ID, bb.SOI_CD, bb.CUST_NME [CUST_NME]
 	   FROM [COWS].[dbo].[FSA_ORDR_CUST] bb with (nolock) INNER JOIN
	   #MaxSOICD msc with (nolock) on msc.ORDR_ID=bb.ORDR_ID and msc.MAX_SOI_CD=bb.SOI_CD) si on  ordr.ORDR_ID = si.ORDR_ID LEFT OUTER JOIN
										
	  (SELECT bb.NTE_ID, bb.ORDR_ID, bb.NTE_TXT, bb.CREAT_DT
	    FROM [COWS].[dbo].[ORDR_NTE] bb with (nolock) INNER JOIN 
		#MaxNTEID mni with (nolock) on mni.MAX_NTE_ID=bb.NTE_ID and mni.ORDR_ID=bb.ORDR_ID) nt on  ordr.ORDR_ID = nt.ORDR_ID LEFT OUTER JOIN
		
	  (SELECT bb.NRM_CKT_ID, bb.FTN, bb.NUA_ADR, bb.PLN_NME
	  FROM [COWS].[dbo].[NRM_CKT] bb with (nolock) INNER JOIN 
	  #MaxNRMCKTID mrc with (nolock) on mrc.MAX_NRM_CKT_ID=bb.NRM_CKT_ID and bb.FTN=mrc.FTN) nm on  fsa.FTN = nm.FTN LEFT OUTER JOIN

      [COWS].[dbo].[LK_EXP_TYPE] u with (nolock) on l.EXP_TYPE_ID = u.EXP_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[H5_FOLDR] aa with (nolock) on ordr.H5_FOLDR_ID  = aa.H5_FOLDR_ID LEFT OUTER JOIN
      
            (SELECT bb.ORDR_ID, bb.NEW_CCD_DT, bb.CCD_HIST_ID
 	   FROM [COWS].[dbo].[CCD_HIST] bb WITH (NOLOCK) INNER JOIN
		#MaxCCDHIST mch WITH (NOLOCK) ON mch.MAX_CCD_HIST_ID=bb.CCD_HIST_ID AND bb.ORDR_ID=mch.ORDR_ID
		) cd on  ordr.ORDR_ID = cd.ORDR_ID 
	LEFT OUTER JOIN COWS.Dbo.ACT_TASK acttsk1 with (nolock) ON acttsk1.ORDR_ID=ordr.ORDR_ID AND acttsk1.STUS_ID not in (1,2)
	LEFT OUTER JOIN #MaxActTask mact1 with (nolock) on mact1.MIN_ACT_TASK_ID=acttsk1.ACT_TASK_ID and acttsk1.ORDR_ID=mact1.ORDR_ID
    LEFT OUTER JOIN COWS.dbo.MAP_GRP_TASK mgt1 with(nolock) ON mgt1.TASK_ID = acttsk1.TASK_ID AND mgt1.GRP_ID in (4,11)--system
	LEFT OUTER join COWS.dbo.LK_TASK lt1 with(nolock) on lt1.TASK_ID = acttsk1.TASK_ID 
	LEFT OUTER join COWS.dbo.LK_STUS s1 with(nolock) on s1.STUS_ID = acttsk1.STUS_ID
	LEFT OUTER JOIN COWS.Dbo.ACT_TASK acttsk2 with (nolock) ON acttsk2.ORDR_ID=ordr.ORDR_ID AND acttsk2.STUS_ID not in (1,2)
	LEFT OUTER JOIN #MaxActTask mact2 with (nolock) on mact2.MIN_ACT_TASK_ID=acttsk2.ACT_TASK_ID and acttsk2.ORDR_ID=mact2.ORDR_ID
    LEFT OUTER JOIN COWS.dbo.MAP_GRP_TASK mgt2 with(nolock) ON mgt2.TASK_ID = acttsk2.TASK_ID AND mgt2.GRP_ID not in (4,11)--nonsystem
	LEFT OUTER join COWS.dbo.LK_TASK lt2 with(nolock) on lt2.TASK_ID = acttsk2.TASK_ID 
	LEFT OUTER join COWS.dbo.LK_STUS s2 with(nolock) on s2.STUS_ID = acttsk2.STUS_ID 
	LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] adh6 with (nolock) on adh6.CIS_LVL_TYPE in ('H6') and adh6.REC_STUS_ID =1 and adh6.ORDR_ID=ordr.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] adh5 with (nolock) on adh5.CIS_LVL_TYPE in ('H5') and adh5.REC_STUS_ID =1 and adh5.ORDR_ID=ordr.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].[ORDR_CNTCT] CntctH6 with (nolock) ON CntctH6.CIS_LVL_TYPE in ('H6') and  CntctH6.CNTCT_TYPE_ID  = 1 and CntctH6.ORDR_ID=ordr.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].[ORDR_CNTCT] CntctH6M with (nolock) ON CntctH6M.CIS_LVL_TYPE in ('H6') and  CntctH6M.CNTCT_TYPE_ID  = 1 and CntctH6M.ROLE_ID in (94,90) and CntctH6M.ORDR_ID=ordr.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].[ORDR_CNTCT] CntctH5 with (nolock) ON CntctH5.CIS_LVL_TYPE in ('H5') and  CntctH5.CNTCT_TYPE_ID  = 1 and CntctH5.ORDR_ID=ordr.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].FSA_ORDR_GOM_XNCI fsagom with (nolock) on fsagom.ORDR_ID=ordr.ORDR_ID and ordr.DMSTC_CD = 1 and fsa.ORDR_TYPE_CD in( 'IN','CP') and FSA.ORDR_ACTN_ID <>3 and fsagom.GRP_ID = 1
	LEFT OUTER JOIN #MaxCKTMS mcktms ON mcktms.ORDR_ID=odlist.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].[CKT_MS] cm with (nolock) ON cm.CKT_ID=mcktms.CKT_ID
	LEFT OUTER JOIN #MaxCKTMS mcktms2 ON mcktms2.ORDR_ID=odlist.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].[CKT_MS] cmd with (nolock) ON cmd.CKT_ID=mcktms2.CKT_ID
	LEFT OUTER JOIN #MaxVndrOrdrMSI mvomi with (nolock) on mvomi.ORDR_ID=odlist.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR_MS] VNDR_SENT_DT_I with (nolock) on VNDR_SENT_DT_I.VNDR_ORDR_MS_ID=mvomi.MAX_VNDR_ORDR_MS_ID AND mvomi.VNDR_ORDR_ID=VNDR_SENT_DT_I.VNDR_ORDR_ID
	LEFT OUTER JOIN #MaxVndrOrdrMSD mvomd with (nolock) on mvomd.ORDR_ID=odlist.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR_MS] VNDR_SENT_DT_D with (nolock) on VNDR_SENT_DT_D.VNDR_ORDR_MS_ID=mvomd.MAX_VNDR_ORDR_MS_ID AND mvomd.VNDR_ORDR_ID=VNDR_SENT_DT_D.VNDR_ORDR_ID
	LEFT OUTER JOIN #MaxOrdrMS moms with (nolock) on moms.ORDR_ID=odlist.ORDR_ID
	LEFT OUTER JOIN [COWS].[dbo].[ORDR_MS] oms with (nolock) on oms.ORDR_ID=moms.ORDR_ID and moms.MAX_VER_ID=oms.VER_ID
	left outer join #maxnrmsrvc mnrms with (nolock) on mnrms.ftn=fsa.ftn
	left outer join cows.dbo.nrm_srvc_instc nrmsrvc with(nolock) on nrmsrvc.ftn=mnrms.ftn and nrmsrvc.modfd_dt=mnrms.max_modfd_dt
	left join (select sum( cows_reporting.dbo.convertinputtofloat(mrc_chg_amt)) as mrc_chg_amt,sum(cows_reporting.dbo.convertinputtofloat(nrc_chg_amt)) as nrc_chg_amt, fobli.ordr_id
				from [cows].[dbo].[fsa_ordr_bill_line_item] fobli with (nolock) inner join
				#ordrlist odlist with (nolock) on odlist.ordr_id=fobli.ordr_id
				group by fobli.ordr_id) fob on fob.ordr_id = ordr.ordr_id


CLOSE SYMMETRIC KEY FS@K3y 


END
