USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_Lassie_CPE_Nte_View]    Script Date: 9/26/2016 4:03:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: dlp0278
-- Create date: 3/21/2016 
-- Description:  SP used for Lassie CPE Notes View
-- =============================================
ALTER PROCEDURE [dbo].[sp_Lassie_CPE_Nte_View] 

AS
BEGIN TRY

SET NOCOUNT ON;

TRUNCATE TABLE [COWS_Reporting].[dbo].[LassieCpeNteData]

INSERT INTO [COWS_Reporting].[dbo].[LassieCpeNteData]
           ([Order_ID],[ORDR_ID],[SITE_ID],[DEVICE_ID],[NTE_TYPE_DES],[NTE_ID],[NTE_TXT],[CREAT_DT],[CREAT_BY_USER_ID])


SELECT distinct fsa.FTN AS Order_ID
			,fsa.ORDR_ID 
		   ,foc.SITE_ID
		   ,itm.DEVICE_ID
		   ,typ.NTE_TYPE_DES
		   ,nte.NTE_ID
		   ,nte.NTE_TXT
		   ,nte.CREAT_DT
		   ,nte.CREAT_BY_USER_ID

	FROM [COWS].[dbo].[FSA_ORDR] fsa WITH (NOLOCK)
	INNER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc WITH (NOLOCK) ON foc.ORDR_ID = fsa.ORDR_ID
	INNER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] itm WITH (NOLOCK) 
					ON itm.ORDR_ID = fsa.ORDR_ID
					AND ISNULL(itm.DEVICE_ID,'')<> ''
	INNER JOIN [COWS].[dbo].[ORDR] ord WITH (NOLOCK) 
					ON ord.ORDR_ID = fsa.ORDR_ID
	INNER JOIN [COWS].[dbo].[ORDR_NTE] nte WITH (NOLOCK) 
					ON nte.ORDR_ID = fsa.ORDR_ID 
	LEFT OUTER JOIN [COWS].[dbo].[LK_NTE_TYPE] typ WITH (NOLOCK)
					ON nte.NTE_TYPE_ID = typ.NTE_TYPE_ID

	WHERE ord.ORDR_CAT_ID = 6 AND nte.NTE_TYPE_ID IN (6,26,9) 

END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_Lassie_CPE_Nte_View ' + ': '
	SET @Desc=@Desc 
	EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
	
END CATCH