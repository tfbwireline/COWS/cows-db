/*  
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD  
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME 
*/  
ALTER Proc [dbo].[sp_App_ADHOCL2PINFO1]  
as  
-- Note: orderID in all order tables is the same from ORDR table except CKT table where need to get CKTID from Orderid  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
OPEN SYMMETRIC KEY FS@K3y   
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
    -- Insert statements for procedure here  
      
select distinct ordr.ORDR_ID,  
ordr.ORDR_CAT_ID,   
  
--IPL Order related Fields  
 d.CKT_TYPE_CD,  
d.H5_CUST_ACCT_NBR,  
d.CUST_CNTRC_SIGN_DT ,  
ai.FRMNG_ID,  
ct.LINE_CODNG_TYPE_DES,   
  
-- FSA Related Fields  
a.FTN,  
a.RELTD_FTN,  
a.TTRPT_JACK_NRFC_TYPE_CD,  
b.FSA_ORDR_TYPE_DES,  
c.FSA_PROD_TYPE_DES,  
a.FSA_EXP_TYPE_CD,  
a.TSUP_RFQ_NBR,  
a.CUST_WANT_DT,  
a.CUST_CMMT_DT,  
a.TTRPT_ACCS_CNTRC_TERM_CD,  
a.TPORT_CNCTR_TYPE_ID,  
a.TSUP_PRS_QOT_NBR,  
a.CPE_ACCS_PRVDR_CD,  
bl.MRC_CHG_AMT,  
bl.NRC_CHG_AMT,  
(cast(bl.MRC_CHG_AMT as money) + cktc.ACCS_CUST_MRC_AMT) as TOTAL_CUST_BILL,  
os.REC_STUS_DES,  
--FSA ORDR CUST  
i.SALS_PERSN_PRIM_CID,  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN i.CUST_NME   
  WHEN ordr.CSG_LVL_ID > 0 AND i.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND i.CUST_NME2 IS NULL THEN NULL END [CUST_NME] ,  
  
  
a.TTRPT_SPD_OF_SRVC_BDWD_DES,  
  
---- NRM_CKT fields  
n.PLN_NME,  
n.PLN_SEQ_NBR,  
n.FMS_CKT_ID,  
n.NUA_ADR,  
n.TOC_ADR,  
  
--NRM_VNDR  
nv.LEC_ID,  
  
--TRPT_ORDR fields  
t.CMNT_TXT,  
u.FULL_NME ,  
m.CCD_MISSD_REAS_DES,  
t.ORDR_ASN_VW_DT  ,  
t.CUST_ACPTD_DT ,  
t.BILL_CLEAR_DT,  
t.CKT_LGTH_IN_KMS_QTY,  
t.ACCS_IN_MB_VALU_QTY,  
t.A_END_NEW_CKT_CD,  
t.A_END_SPCL_CUST_ACCS_DETL_TXT,  
t.A_END_REGION,  
t.B_END_NEW_CKT_CD,  
t.B_END_SPCL_CUST_ACCS_DETL_TXT,   
t.IPT_AVLBLTY_ID,              
cu.CUR_NME,  
t.VNDR_LOCAL_ACCS_PRVDR_GRP_NME,  
vt.VNDR_TYPE_DES,  
s.SALS_CHNL_DES,  
  
--ORDR_MS Related Fields  
o.PRE_SBMT_DT,  
--o.ACK_BY_VNDR_DT,  
--o.SENT_TO_VNDR_DT,  
o.VLDTD_DT AS ACK_BY_VNDR_DT,  
o.VLDTD_DT AS SENT_TO_VNDR_DT,  
o.VLDTD_DT,  
--Commented as field missing  
oh.HOLD_DT as ORDR_HOLD_DT,  
  
--CKT_MS fields  
cm.ACCS_ACPTC_DT,  
cm.ACCS_DLVRY_DT,  
cm.CNTRC_TERM_ID,  
cm.TRGT_DLVRY_DT,  
cm.TRGT_DLVRY_DT_RECV_DT,  
cm.VNDR_CNFRM_DSCNCT_DT,  
  
--ORDR_ADR fields  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.BLDG_NME   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.BLDG_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.BLDG_NME2 IS NULL THEN NULL END [BLDG_NME] ,  
  
ad.CTRY_CD as [CTRY_CD] ,  
  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.CTY_NME   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.CTY_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.CTY_NME2 IS NULL THEN NULL END [CTY_NME] ,  
  
-- ad.FLR_ID,  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.FLR_ID   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.FLR_ID2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.FLR_ID2 IS NULL THEN NULL END [FLR_ID] ,  
  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.PRVN_NME   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.PRVN_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.PRVN_NME2 IS NULL THEN NULL END [PRVN_NME] ,  
  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.RM_NBR   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.RM_NBR2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.RM_NBR2 IS NULL THEN NULL END [RM_NBR] ,  
    
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN ad.STREET_ADR_1   
WHEN ordr.CSG_LVL_ID > 0 AND ad.STREET_ADR_12 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND ad.STREET_ADR_12 IS NULL THEN NULL END [STREET_ADR_1] ,  
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN ad.STT_CD   
WHEN ordr.CSG_LVL_ID > 0 AND ad.STT_CD2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND ad.STT_CD2 IS NULL THEN NULL END [STT_CD] ,  
    
    
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN ad.ZIP_PSTL_CD   
WHEN ordr.CSG_LVL_ID > 0 AND ad.ZIP_PSTL_CD2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND ad.ZIP_PSTL_CD2 IS NULL THEN NULL END [ZIP_PSTL_CD] ,  
  
-- ORDR_CNTCT  
oc.PHN_NBR,  
  
--VNDR_FOLDR_CNTCT fields  
fc.CNTCT_FRST_NME,  
fc.CNTCT_LST_NME,  
fc.CNTCT_PHN_NBR,  
fc.CNTCT_EMAIL_ADR  ,  
--VNDR_FOLDR  
f.VNDR_CD ,  
--VNDR_ORDR  
e.VNDR_ORDR_ID ,  
  
  
-- SPLK_EVENT_ACCS  
sp.NEW_PORT_SPEED_DES,  
  
-- CKT_COST fields  
cktc.ACCS_CUST_MRC_AMT,  
  
cktc.VNDR_MRC_IN_USD_AMT,  
cktc.VNDR_NRC_IN_USD_AMT,  
cktc.ASR_MRC_IN_USD_AMT,  
cktc.ASR_NRC_IN_USD_AMT,  
cktc.ACCS_CUST_MRC_IN_USD_AMT,  
cktc.ACCS_CUST_NRC_IN_USD_AMT,  
cktc.ACCS_CUST_NRC_AMT,  
cktc.ASR_MRC_ATY,  
cktc.QOT_NBR,  
cktc.VNDR_CUR_ID,   
ckt.VNDR_CNTRC_TERM_END_DT ,  
ckt.VNDR_CKT_ID,  
  
(cktc.VNDR_MRC_IN_USD_AMT + cktc.VNDR_NRC_IN_USD_AMT + cktc.ASR_MRC_IN_USD_AMT + cktc.ASR_NRC_IN_USD_AMT + cktc.ACCS_CUST_MRC_IN_USD_AMT + cktc.ACCS_CUST_NRC_IN_USD_AMT) as COSTTYPE,  
  
h.H1_CUST_ID,  
--ORDR_VLAN   
  
vl.VLAN_ID,  
vl.VLAN_PCT_QTY,  
a.SPA_ACCS_INDCR_DES,  
a.TPORT_BRSTBL_USAGE_TYPE_CD,  
a.TPORT_ETHRNT_NRFC_INDCR,  
a.TPORT_CUST_ROUTR_TAG_TXT,  
a.TPORT_CUST_ROUTR_AUTO_NEGOT_CD,  
acs.ACCS_CTY_NME_SITE_CD                   
    
    
from COWS.dbo.ORDR ordr with (nolock)   
  
-- FSA Related Fields  
  
  
   
left outer join (  
  SELECT bb.*  
   --fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES,  
   --fcpl.SPA_ACCS_INDCR_DES,  
   --fcpl.TPORT_ETHRNT_NRFC_INDCR,  
   --fcpl.TPORT_CUST_ROUTR_TAG_TXT,  
   --fcpl.TPORT_CUST_ROUTR_AUTO_NEGOT_CD   
  FROM [COWS].[dbo].[FSA_ORDR] bb LEFT OUTER JOIN  
  [COWS].[dbo].[ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = bb.ORDR_ID  
  WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)  
  FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] ) fsa   
  WHERE fsa.FTN = bb.FTN)) a on a.ORDR_ID = ordr.ORDR_ID  
  
left outer join COWS.dbo.LK_REC_STUS os with(nolock) on os.REC_STUS_ID = ordr.REC_STUS_ID  
  
left outer join COWS.dbo.LK_FSA_ORDR_TYPE b with(nolock) on b.FSA_ORDR_TYPE_CD = a.ORDR_TYPE_CD  
  
left outer join COWS.dbo.LK_FSA_PROD_TYPE c with(nolock) on c.FSA_PROD_TYPE_CD = a.PROD_TYPE_CD  
  
  
left outer join (SELECT bli.* FROM COWS.dbo.FSA_ORDR_BILL_LINE_ITEM bli LEFT OUTER JOIN  
  [COWS].[dbo].[ORDR] cc on bli.ORDR_ID = cc.ORDR_ID    
  WHERE bli.FSA_ORDR_BILL_LINE_ITEM_ID  = (SELECT MAX(FSA_ORDR_BILL_LINE_ITEM_ID)  
  FROM (SELECT FSA_ORDR_BILL_LINE_ITEM_ID,ORDR_ID FROM COWS.dbo.FSA_ORDR_BILL_LINE_ITEM il where il.MRC_CHG_AMT <> '' and il.NRC_CHG_AMT <> '') i   
  WHERE i.ORDR_ID = bli.ORDR_ID)) bl on bl.ORDR_ID = ordr.ORDR_ID  
  
  
  
  
-- FSA_ORDR_CUST  
  
  LEFT OUTER JOIN (SELECT bb.ORDR_ID,bb.SALS_PERSN_PRIM_CID, bb.SOI_CD, CONVERT(varchar, DecryptByKey(csd.CUST_NME)) [CUST_NME2], bb.CUST_NME  
     FROM [COWS].[dbo].[FSA_ORDR_CUST] bb LEFT OUTER JOIN  
  [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
  WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
   FROM (SELECT SOI_CD, ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] ) soi  
   WHERE soi.ORDR_ID = bb.ORDR_ID)) i on i.ORDR_ID = a.ORDR_ID   
  
left outer join (
SELECT  DISTINCT 
nn.ftn,
nn.PLN_NME,  
nn.PLN_SEQ_NBR,  
nn.FMS_CKT_ID,  
nn.NUA_ADR,  
CASE WHEN (o.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.[SITE_ADR]) ELSE nn.[TOC_ADR]  END AS [TOC_ADR]----nn.TOC_ADR

FROM [COWS].[dbo].[NRM_CKT] nn with (nolock)  
LEFT OUTER JOIN COWS.dbo.[CUST_SCRD_DATA] csd WITH (NOLOCK) ON csd.[SCRD_OBJ_ID]=nn.[NRM_CKT_ID] and csd.[SCRD_OBJ_TYPE_ID]=29
LEFT OUTER JOIN COWS.DBO.FSA_ORDR f WITH(Nolock) ON f.FTN=nn.ftn
LEFT JOIN COWS.dbo.ORDR O WITH (NOLOCK) ON O.ORDR_ID=f.ORDR_ID
WHERE nn.NRM_CKT_ID  = (SELECT MAX(NRM_CKT_ID)  
FROM (SELECT NRM_CKT_ID, FTN FROM [COWS].[dbo].[NRM_CKT] with (nolock) ) nrm   
WHERE nrm.FTN = nn.FTN)) n on n.FTN = a.FTN   
    
    --SPLK_EVENT_ACCS  
 left outer join (SELECT se.NEW_PORT_SPEED_DES,sl.FTN FROM [COWS].[dbo].[SPLK_EVENT_ACCS] se  
        join [COWS].[dbo].[SPLK_EVENT] sl on sl.EVENT_ID = se.EVENT_ID ) sp   
  on sp.FTN = a.FTN   
    
-- NRM_VNDR  
  
left outer join (SELECT nvn.* FROM [COWS].[dbo].[NRM_VNDR] nvn  
WHERE nvn.NMS_VNDR_ID  = (SELECT MAX(NMS_VNDR_ID)  
FROM (SELECT NMS_VNDR_ID, FTN FROM [COWS].[dbo].[NRM_VNDR] ) nrm   
WHERE nrm.FTN = nvn.FTN)) nv on nv.FTN = a.FTN   
    
  
-- IPLOrdr related fields  
  
left outer join COWS.dbo.IPL_ORDR d with (nolock) on d.ORDR_ID = ordr.ORDR_ID   
-- 2 records returned for each orderid  
left outer join (Select * from COWS.dbo.IPL_ORDR_ACCS_INFO oai where oai.ACCS_TYPE_ID = 'Originating')  ai on ai.ORDR_ID = d.ORDR_ID  
  
left outer join COWS.dbo.LK_LINE_CODNG_TYPE ct with (nolock) on ct.LINE_CODNG_TYPE_ID = ai.LINE_CODNG_TYPE_ID  
  
  
--TRPT_ORDR fields   
 left outer join COWS.dbo.TRPT_ORDR t with(nolock) on t.ORDR_ID = ordr.ORDR_ID   
 left outer join COWS.dbo.LK_USER u with (nolock) on u.USER_ID = t.XNCI_ASN_MGR_ID   
 left outer join COWS.dbo.LK_CCD_MISSD_REAS m with (nolock) on m.CCD_MISSD_REAS_ID  = t.CCD_MISSD_REAS_ID   
 left outer join COWS.dbo.LK_CUR cu with (nolock) on cu.CUR_ID = t.CUST_BILL_CUR_ID   
 left outer join COWS.dbo.LK_VNDR_TYPE vt with (nolock) on vt.VNDR_TYPE_ID  = t.VNDR_TYPE_ID   
 left outer join COWS.dbo.LK_SALS_CHNL s with (nolock) on s.SALS_CHNL_ID = t.SALS_CHNL_ID   
  left outer join COWS.dbo.LK_ACCS_CTY_SITE acs with (nolock) on acs.ACCS_CTY_SITE_ID  = t.ACCS_CTY_SITE_ID   
  
 --ORDR_MS fields -- multiple rows returned**  
   
 left outer join (SELECT oo.* FROM [COWS].[dbo].[ORDR_MS] oo LEFT OUTER JOIN  
  [COWS].[dbo].[ORDR] cc on oo.ORDR_ID = cc.ORDR_ID    
  WHERE oo.VER_ID = (SELECT MAX(VER_ID)  
  FROM (SELECT VER_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_MS] ) oms   
  WHERE oms.ORDR_ID = oo.ORDR_ID)) o on o.ORDR_ID = ordr.ORDR_ID  
   
-- ORDR_HOLD_MS  
   
  
left outer join (SELECT ohm.* FROM [COWS].[dbo].[ORDR_HOLD_MS] ohm LEFT OUTER JOIN  
  [COWS].[dbo].[ORDR] cc on ohm.ORDR_ID = cc.ORDR_ID    
  WHERE ohm.VER_ID = (SELECT MAX(VER_ID)  
  FROM (SELECT VER_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_HOLD_MS] ) oms   
  WHERE oms.ORDR_ID = ohm.ORDR_ID)) oh on oh.ORDR_ID = ordr.ORDR_ID  
   
    
 --CKT, CKT_MS Fields -- multiple rows returned**  
   
 left outer join (SELECT ck.* FROM [COWS].[dbo].[CKT] ck   
  WHERE ck.ORDR_ID  = (SELECT MAX(ORDR_ID)  
  FROM (SELECT ORDR_ID FROM [COWS].[dbo].[CKT] ) ock   
  WHERE ock.ORDR_ID = ck.ORDR_ID)) ckt on ckt.ORDR_ID = ordr.ORDR_ID  
   
   
 left outer join (SELECT ckm.* FROM [COWS].[dbo].[CKT_MS] ckm LEFT OUTER JOIN  
  [COWS].[dbo].[CKT] cc on ckm.CKT_ID = cc.CKT_ID    
  WHERE ckm.VER_ID  = (SELECT MAX(VER_ID)  
  FROM (SELECT VER_ID,CKT_ID FROM [COWS].[dbo].[CKT_MS] ) ock   
  WHERE ock.CKT_ID = ckm.CKT_ID)) cm on cm.CKT_ID  = ckt.CKT_ID   
  
-- CKT_COST fields  
  
left outer join (SELECT ckto.* FROM [COWS].[dbo].[CKT_COST] ckto LEFT OUTER JOIN  
  [COWS].[dbo].[CKT] cc on ckto.CKT_ID = cc.CKT_ID    
  WHERE ckto.VER_ID  = (SELECT MAX(VER_ID)  
  FROM (SELECT VER_ID,CKT_ID FROM [COWS].[dbo].[CKT_COST] ) ock   
  WHERE ock.CKT_ID = ckto.CKT_ID)) cktc on cktc.CKT_ID  = ckt.CKT_ID   
  
    
--ORDR_ADR fields ORDR.orderid  
-- Get address of Type Primary  
left outer join (SELECT adr.ORDR_ID,  
    CONVERT(varchar, DecryptByKey(csd.CTY_NME)) [CTY_NME2],  
    CONVERT(varchar, DecryptByKey(csd.BLDG_NME)) [BLDG_NME2],  
    CONVERT(varchar, DecryptByKey(csd.FLR_ID)) [FLR_ID2],  
    CONVERT(varchar, DecryptByKey(csd.STT_PRVN_NME)) [PRVN_NME2],  
    CONVERT(varchar, DecryptByKey(csd.RM_NBR)) [RM_NBR2],  
    CONVERT(varchar, DecryptByKey(csd.STREET_ADR_1)) [STREET_ADR_12],  
    CONVERT(varchar, DecryptByKey(csd.ZIP_PSTL_CD)) [ZIP_PSTL_CD2],  
    CONVERT(varchar, DecryptByKey(csd.STT_CD)) [STT_CD2],   
      
    adr.CTY_NME AS [CTY_NME],  
    adr.BLDG_NME AS [BLDG_NME],  
    adr.CTRY_CD as [CTRY_CD],  
    adr.FLR_ID AS [FLR_ID],  
    adr.PRVN_NME AS [PRVN_NME],  
    adr.RM_NBR AS [RM_NBR],  
    adr.STREET_ADR_1 AS [STREET_ADR_1],  
    adr.ZIP_PSTL_CD AS [ZIP_PSTL_CD],  
    adr.STT_CD AS [STT_CD]   
      
    FROM [COWS].[dbo].[ORDR_ADR] adr   
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=adr.ORDR_ADR_ID  AND csd.SCRD_OBJ_TYPE_ID=14  
    where adr.ADR_TYPE_ID in ( 2,11) )ad on ad.ORDR_ID = ordr.ORDR_ID  
  
-- ORDR_CNTCT  
-- Get Contact of Type Primary  
  
left outer join (SELECT oct.* FROM [COWS].[dbo].[ORDR_CNTCT] oct JOIN  
  [COWS].[dbo].[ORDR] cc on oct.ORDR_ID = cc.ORDR_ID    
  WHERE oct.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)  
  FROM (SELECT ORDR_CNTCT_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_CNTCT] ct where ct.CNTCT_TYPE_ID = 1 ) omc   
  WHERE omc.ORDR_ID = oct.ORDR_ID)) oc on oc.ORDR_ID = ordr.ORDR_ID   
  
--VNDR_FOLDR  
  
left outer join (select vo.* from [COWS].[dbo].[VNDR_ORDR] vo join   
                 [COWS].[dbo].[ORDR] cc on vo.ORDR_ID = cc.ORDR_ID    
                 where vo.VNDR_ORDR_ID =(select MAX(VNDR_ORDR_ID) from (SELECT VNDR_ORDR_ID,ORDR_ID FROM [COWS].[dbo].[VNDR_ORDR] vo1 where vo1.TRMTG_CD =0 ) vo2   
  WHERE vo2.ORDR_ID = vo.ORDR_ID  )) e on e.ORDR_ID = ordr.ORDR_ID  
    
left outer join COWS.dbo.VNDR_FOLDR f with(nolock) on f.VNDR_FOLDR_ID = e.VNDR_FOLDR_ID   
left outer join COWS.dbo.VNDR_FOLDR_CNTCT  fc with(nolock) on fc.VNDR_FOLDR_ID = e.VNDR_FOLDR_ID   
  
  
-- ODIE_REQ  
-- multiple records with same orderid  
  
left outer join (SELECT orq.* FROM [COWS].[dbo].[ODIE_REQ] orq JOIN  
  [COWS].[dbo].[ORDR] cc on orq.ORDR_ID = cc.ORDR_ID    
  WHERE orq.REQ_ID = (SELECT MAX(REQ_ID)  
  FROM (SELECT REQ_ID,ORDR_ID FROM [COWS].[dbo].[ODIE_REQ] ) oms   
  WHERE oms.ORDR_ID = orq.ORDR_ID)) h on h.ORDR_ID = ordr.ORDR_ID  
  
--ORDR_VLAN fields assuming there will be 1 ORDR_VLAN_ID per ORDR_ID  
  
  
  
left outer join (SELECT vln.* FROM [COWS].[dbo].[ORDR_VLAN] vln JOIN  
  [COWS].[dbo].[ORDR] cc on vln.ORDR_ID = cc.ORDR_ID    
  WHERE vln.ORDR_VLAN_ID = (SELECT MAX(ORDR_VLAN_ID)  
  FROM (SELECT ORDR_VLAN_ID,ORDR_ID,VLAN_SRC_NME FROM [COWS].[dbo].[ORDR_VLAN] ) vla   
  WHERE vla.ORDR_ID = vln.ORDR_ID and vla.VLAN_SRC_NME = 'FSA')) vl on vl.ORDR_ID = ordr.ORDR_ID  
  
  
END  