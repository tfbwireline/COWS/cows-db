USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_CPTReport]    Script Date: 06/23/2018 01:28:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
--=======================================================================================
-- Project:			Pj017930 - CPT Report
-- Author:			Md mahmudur Rahman Monir	
-- Date:			03/10/2016
-- Description:		
--					CPT Report
-- Updated by:     vn370313
-- Updated For:    Added PM/NTE/MSSE Eng/ CRM  PM=22 and 23=NTE
-- Updated Date:   01202017
-- Updated by:     vn370313
-- Updated For:    Added Condition RecStatus_ID of resource must be 1
-- Updated Date:   04282017
-- Updated by:     vn370313
-- Updated For:    Customer name wont Show
-- Updated Date:   06242018

-- [dbo].[sp_MNSRptCPT] 1			
-- QueryNumber: 	
	----1 - CPT Provisioning			
	----2 - MNS PM
	----3 - Manager
-- Notes:			
	----
--
-- Modifications:
-- NA 
----TEST---------------------------------------------------------------------------------
EXEC [dbo].[sp_CPTReport] 1,1
EXEC [dbo].[sp_CPTReport] 1,0
EXEC [dbo].[sp_CPTReport] 2,1
EXEC [dbo].[sp_CPTReport] 2,0
EXEC [dbo].[sp_CPTReport] 3,1
EXEC [dbo].[sp_CPTReport] 3,0
---ALTER TABLE [COWS].[dbo].[CPT] ADD [COMPNY_NME] [varbinary](max) NULL
Sp_helptext 'getReportListByUserID' 
Select 	* from RAD_Reporting.[rad].[REPORTQUEUE_NEW]
-----------------------------------------------------------------------------------------
--=======================================================================================
*/
ALTER PROCEDURE [dbo].[sp_CPTReport]
 	@QueryNumber		   INT,
  	@Secured			   INT=0
AS

BEGIN TRY
BEGIN
	SET NOCOUNT ON;
    OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
		IF @QueryNumber=1
		BEGIN
			----1 - CPT Provisioning
			SELECT	DISTINCT
					'CPT Number'= CPT.CPT_CUST_NBR ,
					/*
					CASE   WHEN CPT.COMPNY_NME is NULL THEN ''
							WHEN @Secured = 0  AND CPT.COMPNY_NME is NOT NULL THEN 'Private Company Name'
							ELSE CPT.COMPNY_NME END 'Company Name',
							--ELSE CONVERT(varchar, DecryptByKey(CPT.COMPNY_NME)) END 'Company Name',
							--ISNULL(CASE WHEN (c.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE c.[COMPNY_NME] END, '')
					*/		
					CASE WHEN ((@Secured=1) AND (cpt.CSG_LVL_ID>0)) THEN ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'')
						 WHEN ((@Secured=0) AND (cpt.CSG_LVL_ID>0)) THEN 'Private Company Name'
					     WHEN (cpt.CSG_LVL_ID=0) THEN ISNULL(cpt.COMPNY_NME,'') END AS [Company Name],
							
					'Date Submitted'=ISNULL(CPT.CREAT_DT, ''),
					CASE 						
						WHEN CPT.CUST_SHRT_NME is NULL THEN ''
						WHEN @Secured = 0  AND CPT.CUST_SHRT_NME is NOT NULL THEN 'Private Customer Name'
						ELSE CPT.CUST_SHRT_NME END 'Customer Short Name',
					--'Company Name'=CPT.COMPNY_NME,
					--'Date Submitted'=ISNULL(CPT.CREAT_DT, ''),
					--'Customer Short Name'=ISNULL(CPT.CUST_SHRT_NME, ''),
					'Start IP Address'=ISNULL(CPT.START_IP_ADR, ''),
					'End IP Address'=ISNULL(CPT.END_IP_ADR, ''),
					'Pre-Shared Key'=ISNULL(CPT.PRE_SHRD_KEY, '') , 
					'Gatekeeper'=ISNULL(U.FULL_NME, '') , --ASMT.CPT_USER_ID,
					'SLO for Gatekeeper'=(Select DATEADD(DAY,4,MAX(MODFD_DT)) FROM COWS.dbo.CPT_PRVSN WITH (NOLOCK) WHERE CPT.CPT_ID=CPT_ID AND PRVSN_STUS_CD = 1 /*AND A.REC_STUS_ID = 303*/ ),
					--Add 4 days to CREAT_DT once the CPT_PRVSN Table is filled.
					'QIP'=ISNULL((Select SRVR_NME FROM COWS.dbo.CPT_PRVSN WITH (NOLOCK) WHERE CPT.CPT_ID=CPT_ID AND PRVSN_STUS_CD = 1 and CPT_PRVSN_TYPE_ID =3),'NA'),
					'ODIE'=ISNULL(CPT.CUST_SHRT_NME, ''),
					'Spectrum'=ISNULL((Select SRVR_NME FROM COWS.dbo.CPT_PRVSN WITH (NOLOCK) WHERE CPT.CPT_ID=CPT_ID AND PRVSN_STUS_CD = 1 and CPT_PRVSN_TYPE_ID =1),'NA'),
					'Voyence'=ISNULL((Select SRVR_NME FROM COWS.dbo.CPT_PRVSN WITH (NOLOCK) WHERE CPT.CPT_ID=CPT_ID AND PRVSN_STUS_CD = 1 and CPT_PRVSN_TYPE_ID =2),'NA'),
					'TACACS'=ISNULL((Select ISNULL(S.STUS_DES,'NA') FROM COWS.dbo.CPT_PRVSN  A WITH (NOLOCK) INNER JOIN COWS.dbo.LK_STUS S WITH (NOLOCK) ON A.REC_STUS_ID=S.STUS_ID  WHERE CPT.CPT_ID=A.CPT_ID AND PRVSN_STUS_CD = 1 and CPT_PRVSN_TYPE_ID =4 /*AND A.REC_STUS_ID = 303*/ AND S.STUS_TYPE_ID='CPT'),'NA') ,
					'Return to SDE'=CASE RETRN_SDE_CD  WHEN 1 THEN  'Yes' ELSE 'No' END , --,PRVSN.PRVSN_STUS_CD,PRVSN.CPT_PRVSN_TYPE_ID
					ISNULL(nteu.FULL_NME,'') AS [NTE],
					ISNULL(pmu.FULL_NME,'')  AS [PM],
					--ISNULL(mgru.FULL_NME,'') AS  [Manager],
					ISNULL(mssu.FULL_NME,'') AS [MSS Eng],
					ISNULL(crmu.FULL_NME,'') AS [CRM]
			FROM	COWS.dbo.CPT  CPT WITH (NOLOCK)     
					LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT ASMT WITH (NOLOCK)  ON
					CPT.CPT_ID=ASMT.CPT_ID   AND ASMT.[ROLE_ID]=152 AND ASMT.[REC_STUS_ID]=1  
					LEFT OUTER JOIN COWS.dbo.CPT_PRVSN    PRVSN WITH (NOLOCK)  ON
					CPT.CPT_ID=PRVSN.CPT_ID AND PRVSN.PRVSN_STUS_CD = 1 and PRVSN.CPT_PRVSN_TYPE_ID in( 1,2,3,4)
					LEFT JOIN COWS.dbo.LK_USER U WITH (NOLOCK) ON ASMT.CPT_USER_ID=U.[USER_ID]
					LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT nte ON cpt.CPT_ID = nte.CPT_ID AND nte.ROLE_ID = 23 AND nte.REC_STUS_ID=1  --- 22 before
					LEFT OUTER JOIN COWS.dbo.LK_USER nteu ON nte.CPT_USER_ID = nteu.USER_ID
					LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT pm ON cpt.CPT_ID = pm.CPT_ID AND pm.ROLE_ID = 22  AND PM.REC_STUS_ID=1  --- 23 Before
					LEFT OUTER JOIN COWS.dbo.LK_USER pmu ON pm.CPT_USER_ID = pmu.USER_ID
					LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT mss ON cpt.CPT_ID = mss.CPT_ID AND mss.ROLE_ID = 82  AND mss.REC_STUS_ID=1
					LEFT OUTER JOIN COWS.dbo.LK_USER mssu ON mss.CPT_USER_ID = mssu.USER_ID
					LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT mgr ON cpt.CPT_ID = mgr.CPT_ID AND mgr.ROLE_ID = 151 AND mgr.REC_STUS_ID=1
					LEFT OUTER JOIN COWS.dbo.LK_USER mgru ON mgr.CPT_USER_ID = mgru.USER_ID
					LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT crm ON cpt.CPT_ID = crm.CPT_ID AND crm.ROLE_ID = 153 AND crm.REC_STUS_ID=1
					LEFT OUTER JOIN COWS.dbo.LK_USER crmu ON crm.CPT_USER_ID = crmu.USER_ID
					LEFT JOIN COWS.dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=cpt.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
			WHERE   cpt.REC_STUS_ID = 1 --Active CPT
					AND cpt.CPT_STUS_ID NOT IN (308, 309) --CPT Status not Deleted or Completed
					AND DATEADD(day, 30, cpt.CREAT_DT) > GETDATE() 
					--AND CPT_CUST_NBR='C021816152520'
					--AND cpt.CPT_ID=48
			ORDER BY CPT.CPT_CUST_NBR ASC
		END

		IF @QueryNumber=2
		BEGIN
				----2 - MNS PM
				SELECT	DISTINCT
						'CPT Number'= CPT.CPT_CUST_NBR ,
						/*
						CASE 						
							WHEN CPT.COMPNY_NME is NULL THEN ''
							WHEN @Secured = 0  AND CPT.COMPNY_NME is NOT NULL THEN 'Private Company Name'
							--ELSE CONVERT(varchar, DecryptByKey(CPT.COMPNY_NME)) END 'Company Name',
							ELSE CPT.COMPNY_NME END 'Company Name',
							--ISNULL(CASE WHEN (c.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE c.[COMPNY_NME] END, '')
							*/
						CASE WHEN ((@Secured=1) AND (cpt.CSG_LVL_ID>0)) THEN ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'')
						 WHEN ((@Secured=0) AND (cpt.CSG_LVL_ID>0)) THEN 'Private Company Name'
					     WHEN (cpt.CSG_LVL_ID=0) THEN ISNULL(cpt.COMPNY_NME,'') END AS [Company Name],
						CASE 						
							WHEN CPT.CUST_SHRT_NME is NULL THEN ''
							WHEN @Secured = 0  AND CPT.CUST_SHRT_NME is NOT NULL THEN 'Private Customer Name'
							ELSE CPT.CUST_SHRT_NME END 'Customer Short Name',
						--'Customer Short Name'=ISNULL(CPT.CUST_SHRT_NME, ''),
						'Assigned Resource'=ISNULL(U.FULL_NME, ''),
						'Acknowledge SLO Date'=ISNULL(DATEADD(DAY,2,ASMT.CREAT_DT) , NULL)
				FROM	COWS.dbo.CPT  CPT WITH (NOLOCK)     
						LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT ASMT WITH (NOLOCK)  ON
						CPT.CPT_ID=ASMT.CPT_ID   AND ASMT.[ROLE_ID]=22 AND ASMT.[REC_STUS_ID]=1  
						LEFT JOIN COWS.dbo.LK_USER U WITH (NOLOCK) ON ASMT.CPT_USER_ID=U.[USER_ID]
						LEFT JOIN COWS.dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=cpt.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
				WHERE   cpt.REC_STUS_ID = 1 --Active CPT
						AND cpt.CPT_STUS_ID NOT IN (308, 309) --CPT Status not Deleted or Completed
						AND DATEADD(day, 30, cpt.CREAT_DT) > GETDATE()
						--AND CPT_CUST_NBR='C021816152520'
						--AND cpt.CPT_ID=48
				ORDER BY CPT.CPT_CUST_NBR ASC
	END

	IF @QueryNumber=3
	BEGIN
		-----3 - Manager
		SELECT	DISTINCT
				'CPT Number'= CPT.CPT_CUST_NBR ,
				/*
				CASE 						
					WHEN CPT.COMPNY_NME is NULL THEN ''
					WHEN @Secured = 0  AND CPT.COMPNY_NME is NOT NULL THEN 'Private Company Name'
					--ELSE CONVERT(varchar, DecryptByKey(CPT.COMPNY_NME)) END 'Company Name',
					ELSE CPT.COMPNY_NME END 'Company Name',
				--ISNULL(CASE WHEN (c.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CUST_NME) ELSE c.[COMPNY_NME] END, '') 
				*/
				CASE WHEN ((@Secured=1) AND (cpt.CSG_LVL_ID>0)) THEN ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'')
						 WHEN ((@Secured=0) AND (cpt.CSG_LVL_ID>0)) THEN 'Private Company Name'
					     WHEN (cpt.CSG_LVL_ID=0) THEN ISNULL(cpt.COMPNY_NME,'') END AS [Company Name],
				CASE 						
					WHEN CPT.CUST_SHRT_NME is NULL THEN ''
					WHEN @Secured = 0  AND CPT.CUST_SHRT_NME is NOT NULL THEN 'Private Customer Name'
					ELSE CPT.CUST_SHRT_NME END 'Customer Short Name',
				--'Customer Short Name'=ISNULL(CPT.CUST_SHRT_NME, ''),
				'Assigned Resource'=ISNULL(U.FULL_NME, ''),
				'Acknowledge SLO Date'=ISNULL(DATEADD(DAY,1,ASMT.CREAT_DT) , NULL)
		FROM	COWS.dbo.CPT  CPT WITH (NOLOCK)     
				LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT ASMT WITH (NOLOCK)  ON
				CPT.CPT_ID=ASMT.CPT_ID   AND ASMT.[ROLE_ID]=151 AND ASMT.[REC_STUS_ID]=1  
				LEFT JOIN COWS.dbo.LK_USER U WITH (NOLOCK) ON ASMT.CPT_USER_ID=U.[USER_ID]
				LEFT JOIN COWS.dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=cpt.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
		WHERE   cpt.REC_STUS_ID = 1 --Active CPT
				AND cpt.CPT_STUS_ID NOT IN (308, 309) --CPT Status not Deleted or Completed
				AND DATEADD(day, 30, cpt.CREAT_DT) > GETDATE()
				--AND CPT_CUST_NBR='C021816152520'
				--AND cpt.CPT_ID=48
		ORDER BY CPT.CPT_CUST_NBR ASC
	END

CLOSE SYMMETRIC KEY FS@K3y; 
RETURN 0;
END
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_CPTReport ' + CAST(@QueryNumber AS VARCHAR(4)) + ', ' 
	SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;   --- Open later during deployment
	RETURN 1;
END CATCH


--Select * from [dbo].[LK_STUS]
--Select RETRN_SDE_CD,*  FROM CPT
--Select *  FROM CPT_PRVSN  where CPT_ID=48
--Select CREAT_DT,* FROM COWS.dbo.CPT WHERE  REC_STUS_ID = 1 AND CPT_STUS_ID NOT IN (308, 309)
--Select CREAT_DT,* FROM COWS.dbo.CPT WHERE  REC_STUS_ID = 1 AND CPT_STUS_ID NOT IN (308, 309) AND DATEADD(day, 30, cpt.CREAT_DT) > GETDATE()
-- UPDATE COWS.dbo.CPT  SET COMPNY_NME=CMPNY_NME_2 


--Select  CPT.*
--FROM	CPT  CPT WITH (NOLOCK)     LEFT OUTER JOIN CPT_USER_ASMT ASMT WITH (NOLOCK)  ON
--        CPT.CPT_ID=ASMT.CPT_ID     LEFT OUTER JOIN CPT_PRVSN    PRVSN WITH (NOLOCK)  ON
--        CPT.CPT_ID=PRVSN.CPT_ID AND PRVSN.PRVSN_STUS_CD = 1 and PRVSN.CPT_PRVSN_TYPE_ID in( 1,2,4)
--WHERE   cpt.REC_STUS_ID = 1 --Active CPT
--		AND cpt.CPT_STUS_ID NOT IN (308, 309) --CPT Status not Deleted or Completed
--		AND ASMT.[ROLE_ID]=152 AND ASMT.[REC_STUS_ID]=1
--		--AND PRVSN.PRVSN_STUS_CD = 1 
--		--and PRVSN.CPT_PRVSN_TYPE_ID in( 1,2,4)
--		AND DATEADD(day, 30, cpt.CREAT_DT) > GETDATE()
--		AND CPT_CUST_NBR='C020916144100'

--Sp_Helptext'dbo.GetCPTReportingDetails'
--EXEC [dbo].[GetCPTReportingDetails]

GO


