-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- dlp0289 8/21/13: added PRS Quote Number to end of select.  
-- EXEC COWS_Reporting.dbo.sp_AMNCIRptSubmitted   
-- Modificatios:  
-- ci554013 10/21/2013 - changed report to back only 13 months and   
--  commented out the code that restricted output by MGR_ADID  
-- ci554013 01/21/2014 - Added 3 columns: [Order Received Date],[STDI Date],[H5/H6/ES City Name]  
--  ci554013 01/23/2014 - Changed date format so pivot table can sort  
-- Updated By:   Md M Monir  
-- Updated Date: 03/14/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME   
-- [dbo].[sp_AMNCIRptSubmittedOD] 0, 'N', '1/1/2015', '12/07/2017'  
--- Changed SP 12082017  Remove double quote from Customer name   
-- [dbo].[sp_AMNCIRptSubmittedOD] 1, 'N', '1/1/2017', '12/07/2018'  
-- =============================================  
ALTER PROCEDURE [dbo].[sp_AMNCIRptSubmittedOD] --0, 'N', '1/1/2015', '12/07/2017'  
 @SecuredUser  INT=0,  
 @getSensData  CHAR(1)='N',  
 @startDate   Datetime=NULL,  
 @endDate   Datetime=NULL  
  
AS  
BEGIN TRY  
  
SET NOCOUNT ON;  
  
 IF ((@startDate IS NULL) AND (@endDate IS NULL))  
 BEGIN  
  SET @endDate = GETDATE()  
  SET @startDate = DATEADD(mm,-13,CONVERT(VARCHAR(25),DATEADD(DD,-(DAY(GETDATE())-1),GETDATE()),101))  
 END  
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
  
 SELECT DISTINCT  
 i.FTN [FTN],  
 --si.CUST_NME  [Customer Name],  
 CASE  
 WHEN @SecuredUser = 0 and z.CSG_LVL_ID =0 THEN si.CUST_NME  
 WHEN @SecuredUser = 0 and z.CSG_LVL_ID >0 AND si.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
 WHEN @SecuredUser = 0 and z.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(si.CUST_NME2)) IS NULL THEN NULL   
 ELSE coalesce(si.CUST_NME,si.CUST_NME2,'') END [Customer Name], --Secured  
 CONVERT(VARCHAR(10),tk.CREAT_DT,101) [Installed Date],  
 --tac.FRST_NME + ' ' + tac.LST_NME [Program Manager],  
 --sis.FRST_NME + ' '  + sis.LST_NME [RAM],  
 coalesce(tac.NME, tac.NME2,'') AS [Program Manager],  
 coalesce(sis.NME, sis.NME2,'') [RAM],  
 g.FULL_NME [CPM],  
 c.VLDTD_DT [Sprint Target Delivery Date],  
 h.PROD_TYPE_DES [Product],  
 CONVERT(VARCHAR(10),i.CUST_CMMT_DT,101) [Customer Commit Date],  
 x.RGN_DES [XNCI Region],  
 --u.CTRY_NME [H5 Country],
 ISNULL(CASE WHEN (t.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE u.CTRY_NME END,'') [H5 Country], /*Monir 06062018*/   
 o.ORDR_STUS_DES [Order Status],  
 mg.FULL_NME [Manager],  
 CASE  
  WHEN cd.NEW_CCD_DT <= i.CUST_CMMT_DT THEN 1   
  WHEN cd.NEW_CCD_DT  >   i.CUST_CMMT_DT  THEN 0   
  ELSE 0 END [CCD Met #],  
 CASE    
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 1   
   THEN CONVERT(VARCHAR(10),ISNULL(gq.[GOM Receives Order], i.CREAT_DT) + 3, 101)  
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 2   
   THEN CONVERT(VARCHAR(10),ISNULL(gq.[GOM Receives Order], i.CREAT_DT) + 2, 101)  
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 3   
   THEN  CONVERT(VARCHAR(10),ISNULL(gq.[GOM Receives Order], i.CREAT_DT) + 1, 101)  
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 5   
   THEN  CONVERT(VARCHAR(10),ISNULL(gq.[GOM Receives Order], i.CREAT_DT) - 1, 101)  
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 6   
   THEN  CONVERT(VARCHAR(10),ISNULL(gq.[GOM Receives Order], i.CREAT_DT) - 2, 101)  
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 7   
   THEN  CONVERT(VARCHAR(10),ISNULL(gq.[GOM Receives Order], i.CREAT_DT) - 3, 101)  
  ELSE  CONVERT(VARCHAR(10),ISNULL(gq.[GOM Receives Order], i.CREAT_DT), 101) END  [Week],  
   
 CONVERT(DATE,DATEADD(dd,-(DAY(ISNULL(gq.[GOM Receives Order], i.CREAT_DT))-1),ISNULL(gq.[GOM Receives Order], i.CREAT_DT)),101) [Month],   
 h.PROD_NME [Product2],  
 CASE WHEN tk.TASK_ID = 1001  then tk.CREAT_DT END [Bill Clear Date],  
 fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES [Access Speed],  
 n.ORDR_TYPE_DES [Order Type],  
 s.ORDR_SUB_TYPE_DES [Order Sub Type],  
 ac.ACCS_ACPTD_DT [Access Accepted Date],  
 ac.ACCS_DLVRY_DT [Access Delivery Date],  
 vl.VNDR_NME [Vendor],  
 1 [CCD Met All],  
 b5.MRC_CHG_AMT [ MRC],  
 b5.NRC_CHG_AMT [ NRC],  
 vd.ACCS_CUST_MRC [Access Customer MRC],  
 vd.ACCS_CUST_NRC [Access Customer NRC],  
 vd.ASR_MRC [ASR MRC],  
 vd.ASR_NRC [ASR NRC],  
 vd.VNDR_MRC [Vendor MRC],  
 vd.VNDR_NRC [Vendor NRC],  
 ISNULL(gq.[GOM Receives Order], i.CREAT_DT) [GOM Receives Order],  
 ISNULL(i.TSUP_PRS_QOT_NBR,'') [PRS Quote Number],  
 z.CREAT_DT [Order Received Date],  
 sh.STDI_DT [STDI Date],  
 CASE   
     /*  
  WHEN @SecuredUser = 0 AND z.SCURD_CD = 1 AND CONVERT(VARCHAR(100), DecryptByKey(oa.CTY_NME)) is NOT NULL   
  THEN 'Private Customer'  
  WHEN @SecuredUser = 1 AND @getSensData='N' AND z.SCURD_CD = 1 AND CONVERT(VARCHAR(100), DecryptByKey(oa.CTY_NME)) is NOT NULL   
  THEN 'Private Customer'  
  ELSE ISNULL(CONVERT(VARCHAR(100), DecryptByKey(oa.CTY_NME)),'')  
  */  
  WHEN @SecuredUser = 0 AND oa.CTY_NME<>''  THEN coalesce(oa.CTY_NME,'PRIVATE CUSTOMER')  
  WHEN @SecuredUser = 0 AND oa.CTY_NME =''  THEN 'PRIVATE CUSTOMER'   
  WHEN @SecuredUser = 0 AND oa.CTY_NME IS NULL  THEN 'PRIVATE CUSTOMER'   
  WHEN @SecuredUser = 1 AND @getSensData='N'AND oa.CTY_NME<>''  THEN coalesce(oa.CTY_NME,'PRIVATE CUSTOMER')  
  WHEN @SecuredUser = 1 AND @getSensData='N'AND oa.CTY_NME =''  THEN 'PRIVATE CUSTOMER'    
  WHEN @SecuredUser = 1 AND @getSensData='N'AND oa.CTY_NME IS NULL  THEN 'PRIVATE CUSTOMER'    
  ELSE coalesce( CONVERT(VARCHAR(100), DecryptByKey(csd.CTY_NME)),oa.CTY_NME,'')   
 END  [H5/H6/ES City Name]  
    
 FROM [COWS].[dbo].[ORDR] z WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] i WITH (NOLOCK) ON z.ORDR_ID = i.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = i.ORDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[IPL_ORDR] w WITH (NOLOCK) ON z.ORDR_ID = w.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_GOM_XNCI] a WITH (NOLOCK) ON z.ORDR_ID = a.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] b WITH (NOLOCK) ON z.ORDR_ID = b.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] d WITH (NOLOCK) ON i.PROD_TYPE_CD = d.FSA_PROD_TYPE_CD  
  LEFT OUTER JOIN  [COWS].[dbo].[ORDR_MS] c WITH (NOLOCK) ON a.ORDR_ID = c.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] h WITH (NOLOCK) ON i.PROD_TYPE_CD = h.FSA_PROD_TYPE_CD   
    
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID,   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) [FRST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [LST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) + ' '+ CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [NME2],  
     CONVERT(varchar, DecryptByKey(csd2.CUST_EMAIL_ADR)) [EMAIL_ADR2],   
       
     bb.FRST_NME  as [FRST_NME],   
     bb.LST_NME   as [LST_NME],   
     bb.FRST_NME  + ' '+ bb.LST_NME   as [NME],  
     bb.EMAIL_ADR as [EMAIL_ADR],   
  bb.CNTCT_TYPE_ID   
  FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
  [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID  
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd2.SCRD_OBJ_TYPE_ID=15    
  WHERE bb.ROLE_ID = 13) tac on z.ORDR_ID  = tac.ORDR_ID   
  
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID,   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) [FRST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [LST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) + ' '+ CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [NME2],  
     CONVERT(varchar, DecryptByKey(csd2.CUST_EMAIL_ADR)) [EMAIL_ADR2],   
       
     bb.FRST_NME  as [FRST_NME],   
     bb.LST_NME   as [LST_NME],   
     bb.FRST_NME  + ' '+ bb.LST_NME   as [NME],  
     bb.EMAIL_ADR as [EMAIL_ADR],   
  bb.CNTCT_TYPE_ID   
  FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
  [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd2.SCRD_OBJ_TYPE_ID=15   
  WHERE bb.ROLE_ID = 11) sis on z.ORDR_ID  = sis.ORDR_ID  
     
  LEFT OUTER JOIN (SELECT ORDR_ID, MIN(at.CREAT_DT) as [GOM Receives Order]  
   FROM [COWS].dbo.ACT_TASK at WITH (NOLOCK)  
   JOIN [COWS].dbo.MAP_GRP_TASK mgt WITH (NOLOCK) on mgt.TASK_ID=at.TASK_ID  
   WHERE mgt.GRP_ID=1 AND at.TASK_ID NOT IN (101,102)  
   GROUP BY ORDR_ID) gq ON gq.ORDR_ID = a.ORDR_ID        
  LEFT OUTER JOIN (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID   
     FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
     LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
    FROM (SELECT DISTINCT ASMT_DT, ORDR_ID   
     FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
    WHERE usr.ORDR_ID  = bb.ORDR_ID )  
    AND bb.ASN_USER_ID  IN (SELECT ASN_USER_ID  
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
     WHERE uas.ORDR_ID = bb.ORDR_ID)) ur ON ur.ORDR_ID = i.ORDR_ID   
  LEFT JOIN [COWS].[dbo].[LK_USER] g WITH (NOLOCK) ON ur.ASN_USER_ID = g.USER_ID   
     JOIN [COWS].[dbo].[LK_USER] mg WITH (NOLOCK) ON g.MGR_ADID = mg.USER_ADID   
  LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] k WITH (NOLOCK) ON z.ORDR_ID = k.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_SALS_CHNL] j WITH (NOLOCK) ON j.SALS_CHNL_ID = k.SALS_CHNL_ID     
  LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.STUS_ID, bb.CREAT_DT, bb.ORDR_ID   
   FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
    FROM (SELECT ACT_TASK_ID, ORDR_ID   
     FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
    WHERE tsk.ORDR_ID = bb.ORDR_ID AND bb.TASK_ID = 1001) ) tk ON tk.ORDR_ID = i.ORDR_ID     
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_STUS] o WITH (NOLOCK) ON z.ORDR_STUS_ID = o.ORDR_STUS_ID   
  LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] p WITH (NOLOCK) ON p.ORDR_ID = a.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_CCD_MISSD_REAS] q WITH (NOLOCK) ON p.CCD_MISSD_REAS_ID = q.CCD_MISSD_REAS_ID   
  LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] r WITH (NOLOCK) ON r.FTN = i.FTN   
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON  r.TOC_CTRY_CD = l.CTRY_CD  
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.SOI_CD,   
    --CONVERT(varchar, REPLACE(CONVERT(varchar, DecryptByKey(bb.CUST_NME)), '"', '')) [CUST_NME]  /*12082017 remove ""  from customer name*/  
    CONVERT(varchar, DecryptByKey(csd.CUST_NME)) [CUST_NME2],  
    bb.CUST_NME AS [CUST_NME]  
    FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  WITH (NOLOCK)  
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID  
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5   
   WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
    FROM (SELECT SOI_CD, ORDR_ID   
     FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi  
    WHERE soi.ORDR_ID = bb.ORDR_ID)) si ON Z.ORDR_ID  = si.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] m WITH (NOLOCK) on i.ORDR_ID = m.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] n WITH (NOLOCK) on i.ORDR_TYPE_CD = n.FSA_ORDR_TYPE_CD  
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_SUB_TYPE] s WITH (NOLOCK) on i.ORDR_SUB_TYPE_CD = s.ORDR_SUB_TYPE_CD  
  LEFT OUTER JOIN [COWS].[dbo].[H5_FOLDR] t WITH (NOLOCK) on z.H5_FOLDR_ID = t.H5_FOLDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] u WITH (NOLOCK) on t.CTRY_CD = u.CTRY_CD
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd3 WITH (NOLOCK) ON csd3.SCRD_OBJ_ID=t.H5_FOLDR_ID  AND csd3.SCRD_OBJ_TYPE_ID=6  
  LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd3.CTRY_CD)) = lcs.CTRY_CD   
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.NEW_CCD_DT, bb.CCD_HIST_ID  
    FROM [COWS].[dbo].[CCD_HIST] bb WITH (NOLOCK) LEFT OUTER JOIN  
   [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)  
   FROM (SELECT CCD_HIST_ID, ORDR_ID FROM [COWS].[dbo].[CCD_HIST] WITH (NOLOCK) ) ccd  
   WHERE ccd.ORDR_ID = bb.ORDR_ID)) cd on  z.ORDR_ID = cd.ORDR_ID     
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, SUM(CAST(bb.MRC_CHG_AMT AS MONEY)) MRC_CHG_AMT, SUM(CAST (bb.NRC_CHG_AMT AS MONEY)) NRC_CHG_AMT   
   FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
   JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
   GROUP by bb.ORDR_ID) b5 on b.ORDR_ID = b5.ORDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR] vnd WITH (NOLOCK) ON b.ORDR_ID = vnd.ORDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] vf WITH (NOLOCK) ON vnd.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] vl WITH (NOLOCK) ON vf.VNDR_CD = vl.VNDR_CD  
  LEFT OUTER JOIN [COWS].[dbo].[LK_XNCI_RGN] x WITH (NOLOCK) ON z.RGN_ID = x.RGN_ID  
  LEFT OUTER JOIN (SELECT bb.[ORDR_ID], MIN(aa.ACCS_DLVRY_DT) ACCS_DLVRY_DT, MIN(aa.ACCS_ACPTC_DT) ACCS_ACPTD_DT  
    FROM [COWS].[dbo].[CKT_MS] aa WITH (NOLOCK)  
    JOIN COWS.dbo.CKT bb WITH (NOLOCK) on aa.CKT_ID = bb.CKT_ID  
    JOIN [COWS].[dbo].[ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = CC.ORDR_ID  
    GROUP BY bb.ORDR_ID) ac on b.ORDR_ID = ac.ORDR_ID  
  LEFT OUTER JOIN (SELECT bb.[ORDR_ID], SUM(CAST([VNDR_MRC_IN_USD_AMT] AS MONEY)) as VNDR_MRC  
   ,SUM(CAST ([VNDR_NRC_IN_USD_AMT] AS MONEY)) AS VNDR_NRC, SUM(CAST (aa.ACCS_CUST_MRC_IN_USD_AMT AS MONEY)) ACCS_CUST_MRC,   
   SUM(CAST (aa.ACCS_CUST_NRC_IN_USD_AMT AS MONEY)) ACCS_CUST_NRC,  
   SUM(CAST (aa.ASR_MRC_IN_USD_AMT AS MONEY))ASR_MRC, SUM(CAST (aa.ASR_NRC_IN_USD_AMT AS MONEY))ASR_NRC  
   FROM [COWS].[dbo].[CKT_COST] aa WITH (NOLOCK) JOIN COWS.dbo.CKT bb WITH (NOLOCK) on aa.CKT_ID = bb.CKT_ID  
   JOIN [COWS].[dbo].[ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = CC.ORDR_ID  
   GROUP by bb.ORDR_ID) vd on b.ORDR_ID = vd.ORDR_ID    
 LEFT JOIN COWS.dbo.ORDR_ADR oa with (nolock) ON z.ORDR_ID = oa.ORDR_ID AND oa.cis_lvl_type in ('h5','h6')  
 LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oa.ORDR_ADR_ID  AND csd.SCRD_OBJ_TYPE_ID=14  
 LEFT JOIN (SELECT aa.ORDR_ID[ORDR_ID], aa.STDI_DT,ROW_NUMBER() OVER (PARTITION BY aa.ORDR_ID ORDER BY aa.CREAT_DT DESC) AS ROWNUM    
     FROM [COWS].[dbo].[ORDR_STDI_HIST] aa WITH (NOLOCK)) sh  on z.ordr_id = sh.ordr_id AND ROWNUM = 1  
 WHERE Z.RGN_ID = 1 AND i.CREAT_DT BETWEEN @startDate AND @endDate  
  
  
 CLOSE SYMMETRIC KEY FS@K3y   
        
 RETURN 0;  
  
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptSubmittedOD' + ',' + CAST(@startDate AS VARCHAR(12)) + ': '  
 + CAST(@endDate AS VARCHAR(12)) + ' '  
 SET @Desc=@Desc   
 EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH   
   
    
  
  
  