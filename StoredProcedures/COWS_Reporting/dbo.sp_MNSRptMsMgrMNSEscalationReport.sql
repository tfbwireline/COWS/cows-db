USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptMsMgrMNSEscalationReport]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_MNSRptMsMgrMNSEscalationReport')
EXEC ('CREATE PROCEDURE [dbo].[sp_MNSRptMsMgrMNSEscalationReport] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			06/21/2011
-- Description:		
--					MS management summary report of all events that were submitted as 
--					an escalation for specified date range with any event status 
--					(Pending, Published, Completed, or Rework status)
--
-- QueryNumber: 	 
--				 	1 = Data: Count of Escalation Totals By MNS PM including TOTALS line  								  
--				 	2 = Data: Count of Escalation Totals By Event Status including TOTALS line		
--				 	3 = Column Headers of Count of Escalation Totals By MNS PM table
--				 	4 = Text: Count of Escalation Totals By MNS PM
--				 	5 = Text: Count of Escalation Totals By Event Status
--
-- FrequencyId: 	1 = Daily
--				 	2 = Weekly
--				 	3 = Monthly
--				 	4 = Specify start date & end date: 'mm/dd/yyyy'

--===================================================================================

ALTER Procedure [dbo].[sp_MNSRptMsMgrMNSEscalationReport]
	@QueryNumber		INT,
	@FrequencyId		INT=0,
	@startDate			DATETIME=NULL,
	@endDate			DATETIME=NULL,
	@SecuredUser		INT=0,
	@getSensData		CHAR(1)='N'
		
AS
BEGIN TRY

	SET NOCOUNT ON;

	DECLARE @stDt		Datetime
	DECLARE @endDt		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)
	
	DECLARE @stDtStr	VARCHAR(20)
	DECLARE @endDtStr		VARCHAR(20)
	
	DECLARE @dayOfWeek		INT

	DECLARE @SQL			VARCHAR(MAX)
	
	DECLARE @pivotColHdrs	VARCHAR(MAX)
	DECLARE @pivotTableSQL 	NVARCHAR(MAX)
	DECLARE @script			VARCHAR(MAX)
	DECLARE @script_prepare	VARCHAR(MAX)
	
	DECLARE	@firstMNSPM		VARCHAR(1000)
	DECLARE @colList		VARCHAR(MAX)
	
	
	IF @FrequencyId=1
    	BEGIN    	
    		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
		
			SET @stDt=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			SET @endDt=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)   
		END
	ELSE IF @FrequencyId=2
		BEGIN
			-- e.g. Run date is '2011-06-20' 2nd day of the week
			-- STRT_TMST >= 2011-06-13 00:00:00.000 and STRT_TMST < 2011-06-20 00:00:00.000
			
			SET @today=GETDATE()
			SET @dayOfWeek=DATEPART(dw, @today)
			SET @stDt=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)
			SET @endDt=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)
						
		END
	ELSE IF @FrequencyId=3
		BEGIN			 
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month 
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
			SET @stDt=cast(@startDtStr AS datetime)
			SET @endDt=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			
		END
	ELSE IF @FrequencyId=4
			BEGIN
	
				SET @stDt=CONVERT(datetime, @startDate, 101)
				SET @endDt=CONVERT(datetime, @endDate, 101)				
	
				-- Add 1 day to End Date in order to cover the End Date 24-hr period
				SET @endDt=DATEADD(day, 1, @endDt)		
			
		END
		
  	-- Count of Escalation Totals By MNS PM including Total line
  	IF @QueryNumber=1
 		BEGIN
 			
 			-- Create temp table to host column header, which is the last name portion of user display name
 			CREATE TABLE #tmp_user_dspl (DSPL VARCHAR(100));
			-- add UNION section for Disconnect MDS Activity on 10/07/2011
			
			INSERT INTO #tmp_user_dspl
		  		SELECT distinct luser.DSPL_NME	
					FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)
					JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
            		JOIN (SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
						FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
						WHERE REC_STUS_ID=1
						GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID
					JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
						AND lasg.maxRecTmst = easg.CREAT_DT
					JOIN COWS.dbo.LK_USER luser with (nolock) ON easg.ASN_TO_USER_ID = luser.USER_ID
					WHERE mds.ESCL_CD = 1 AND mds.MDS_ACTY_TYPE_ID <> 3 AND mds.STRT_TMST >= @stDt AND mds.STRT_TMST < @endDt 
						AND (CHARINDEX('[', DSPL_NME) > 0)
				UNION
		  		SELECT distinct luser.DSPL_NME	
					FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)
            		JOIN (SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
						FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
						WHERE REC_STUS_ID=1
						GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID
					JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
						AND lasg.maxRecTmst = easg.CREAT_DT
					JOIN COWS.dbo.LK_USER luser with (nolock) ON easg.ASN_TO_USER_ID = luser.USER_ID
					WHERE mds.ESCL_CD = 1 AND mds.MDS_ACTY_TYPE_ID = 3 AND mds.STRT_TMST >= @stDt AND mds.STRT_TMST < @endDt 
						AND (CHARINDEX('[', DSPL_NME) > 0)				
			--=============================================================
			-- Create temp table to host data and add dynamic columns to it 
			--=============================================================
 			CREATE TABLE #tmp_escl_pm (IPM_Name VARCHAR(100));
 
			SET @script_prepare = 'ALTER TABLE #tmp_escl_pm ADD [?]] INT;'
			SET @script = ''
 
			SELECT @script = @script + Replace(@script_prepare, '?', DSPL)			
			FROM #tmp_user_dspl
			ORDER BY DSPL
			
			-- Add dynamic columns
			EXEC (@script) 	
  	
			--=================================			
			-- Get NOT NULL Header columns list 
			--=================================
																										
			SELECT @pivotColHdrs =			
			STUFF
			(
				(			
					SELECT ',' + quotename(DSPL)
  				 	FROM #tmp_user_dspl	ORDER BY DSPL			
																		
					FOR XML PATH('')
               ), 1, 1, ''
			)
			
			--There could be more than one EVENT_ASN_TO_USER record of the same EVENT_ID (EVENT assigned to different user_id)  
			--Use the one with the highest timestamp

			--==================================================
			-- Convert to VARCHAR before using it in dynamic SQL 
			--==================================================
			SET @stDtStr=@stDt
			SET @endDtStr=@endDt
			
			--=============================================================
			-- Execute Dynamic Pivot Table to populate data into temp table
			--=============================================================			
			-- add UNION section for Disconnect MDS Activity on 10/07/2011
			
			SET @pivotTableSQL = '
			INSERT INTO #tmp_escl_pm
			SELECT * 
  			  FROM 
  			  (
    		  	  SELECT mds.EVENT_ID, luser1.DSPL_NME ''MNS_PM'', luser2.DSPL_NME ''IPM Name''
					FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)
						JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
						JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
								FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
								WHERE REC_STUS_ID=1
								GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID
						JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
							AND lasg.maxRecTmst = easg.CREAT_DT										
						JOIN COWS.dbo.LK_USER luser1 with (nolock) ON easg.ASN_TO_USER_ID = luser1.USER_ID
						JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.CREAT_BY_USER_ID = luser2.USER_ID
					WHERE mds.ESCL_CD = 1 AND mds.MDS_ACTY_TYPE_ID <> 3 AND mds.STRT_TMST >= ''' + @stDtStr + ''' AND mds.STRT_TMST < ''' + @endDtStr + '''
				  UNION
    		  	  SELECT mds.EVENT_ID, luser1.DSPL_NME ''MNS_PM'', luser2.DSPL_NME ''IPM Name''
					FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)
						JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
								FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
								WHERE REC_STUS_ID=1
								GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID
						JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
							AND lasg.maxRecTmst = easg.CREAT_DT										
						JOIN COWS.dbo.LK_USER luser1 with (nolock) ON easg.ASN_TO_USER_ID = luser1.USER_ID
						JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.CREAT_BY_USER_ID = luser2.USER_ID
					WHERE mds.ESCL_CD = 1 AND mds.MDS_ACTY_TYPE_ID = 3 AND mds.STRT_TMST >= ''' + @stDtStr + ''' AND mds.STRT_TMST < ''' + @endDtStr + '''					
  			  )mds1
  			  PIVOT 
  			  (
    		 	  COUNT(EVENT_ID)
    			  FOR MNS_PM IN (' + @pivotColHdrs + ')
    				
  			  )pvt1
  			  order by ''IPM Name'''  	
  	
  			-- Populate counts in temp table
  			EXEC (@pivotTableSQL)
  			--======================================
  			-- Add TotalCount column and populate it
  			--======================================
  			EXEC ('ALTER Table #tmp_escl_pm ADD [TotalCount] INT;')
  	
  			SET @script='UPDATE #tmp_escl_pm SET TotalCount=(['
  			
  			SELECT @firstMNSPM = DSPL FROM (SELECT top 1 DSPL FROM #tmp_user_dspl ORDER BY DSPL)tbl
  			
  			SET @script=@script + @firstMNSPM + ']]'
			-- Prepare MNS PM Total Count column  			
  			SET @script_prepare = '+[?]]'

  			SET @colList=''
  			SELECT @colList = @colList + Replace(@script_prepare, '?', DSPL)			
			FROM #tmp_user_dspl  			
			WHERE DSPL > @firstMNSPM
			ORDER BY DSPL
			
  			SET @script=@script + @colList + ')'
  			
  			EXEC (@script)
  			
  			--===============================================
  			-- Prepare SQL statement to SELECT data and Total
 			--===============================================
 			SET @script='SELECT IPM_NAME ''IPM Name'''
  			SET @script_prepare=', [?]]'

			-- Prepare MNS PM column list  	
  			SET @colList=''
  			SELECT @colList=@colList + Replace(@script_prepare, '?', DSPL)			
			FROM #tmp_user_dspl
			ORDER BY DSPL

  			SET @script=@script + @colList + ', TotalCount ''TOTAL Escalation Count'' FROM #tmp_escl_pm UNION SELECT ''TOTALS'' ''IPM Name'''
  			
  			SET @script_prepare = ', SUM([?]]) ''?'''
  	
  			SET @colList=''		
  			SELECT @colList = @colList + Replace(@script_prepare, '?', DSPL)			
			FROM #tmp_user_dspl  	
			  			
  			SET @script=@script + @colList + ', CASE WHEN SUM(TotalCount) is NULL THEN 0 ELSE SUM(TotalCount) END ''TOTAL Escalation Count'' FROM #tmp_escl_pm ORDER BY ''IPM Name'''
  			
  			EXEC (@script)
  			 			
  			--=========
  			-- Clean up
  			--=========
  			IF OBJECT_ID('tempdb..#tmp_user_dspl', 'U') IS NOT NULL	
				drop table #tmp_user_dspl;
  			IF OBJECT_ID('tempdb..#tmp_escl_pm', 'U') IS NOT NULL	
				drop table #tmp_escl_pm;  	
  	
  		END
	-- Count of Escalation Totals By Event Status including TOTALS line  		
  	ELSE IF @QueryNumber=2
  		BEGIN
	
			CREATE TABLE #tmp_escl_event
			(
				IPM_Name	VARCHAR(100),
				Pending		INT,
				Published	INT,
				Rework		INT,
				Completed	INT,
				TotalCount	INT
			)
			
			-- INSERT 2 blank lines and table title line
			
			INSERT INTO #tmp_escl_event  		
  			SELECT DSPL_NME, Pending, Published, Rework, Completed, (Pending+Published+Rework+Completed)		
  			FROM
  			(
				SELECT DSPL_NME, [2] 'Pending', [4] 'Published', [3] 'Rework', [6] 'Completed' 
				FROM
				( 
					SELECT mds.EVENT_ID, mds.EVENT_STUS_ID, luser.DSPL_NME 
					FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
						JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
						JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.CREAT_BY_USER_ID = luser.USER_ID 
					WHERE mds.ESCL_CD = 1 AND mds.MDS_ACTY_TYPE_ID <> 3 AND mds.STRT_TMST >= @stDt AND mds.STRT_TMST < @endDt 
					UNION
					SELECT mds.EVENT_ID, mds.EVENT_STUS_ID, luser.DSPL_NME 
					FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
						JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.CREAT_BY_USER_ID = luser.USER_ID 
					WHERE mds.ESCL_CD = 1 AND mds.MDS_ACTY_TYPE_ID = 3 AND mds.STRT_TMST >= @stDt AND mds.STRT_TMST < @endDt					
				)mds2
				PIVOT
				(
					COUNT(EVENT_ID)
					FOR EVENT_STUS_ID in ([2], [4], [3], [6])
	
				)AS pvt2
				
  			)evStat
  		
  			SELECT IPM_Name 'IPM Name', Pending, Published, Rework, Completed, TotalCount 'TOTAL Escalation Count'
  				FROM #tmp_escl_event
  			UNION
  			SELECT 'TOTALS' 'IPM Name', 
  					CASE WHEN SUM(Pending) is NULL THEN 0 ELSE SUM(Pending) END 'Pending', 
  					CASE WHEN SUM(Published) is NULL THEN 0 ELSE SUM(Published) END 'Published', 
  					CASE WHEN SUM(Rework) is NULL THEN 0 ELSE SUM(Rework) END 'Rework', 
  					CASE WHEN SUM(Completed) is NULL THEN 0 ELSE SUM(Completed) END 'Completed', 
  					CASE WHEN SUM(TotalCount) is NULL THEN 0 ELSE SUM(TotalCount) END 'TOTAL Escalation Count' 
  				FROM #tmp_escl_event
  				
  			--=========
  			-- Clean up
  			--=========
  			IF OBJECT_ID('tempdb..#tmp_escl_event', 'U') IS NOT NULL	
				drop table #tmp_escl_event;  		
  		
  		
  		END
  	ELSE IF	@QueryNumber=3
  		BEGIN
  		
 			-- Create temp table to host column headers
 			CREATE TABLE #tmp_dspl_nme (DSPL VARCHAR(50), DSPL_L VARCHAR(100));  		
 			
 			INSERT INTO #tmp_dspl_nme (DSPL, DSPL_L)
				SELECT	distinct LEFT(luser.DSPL_NME, (CHARINDEX(',', luser.DSPL_NME)-1)) 'DSPL', 
					luser.DSPL_NME 'DSPL_L'	
				FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)
				JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
				JOIN (SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
						FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
						GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID
				JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
					AND lasg.maxRecTmst = easg.CREAT_DT
				JOIN COWS.dbo.LK_USER luser with (nolock) ON easg.ASN_TO_USER_ID = luser.USER_ID
				WHERE mds.ESCL_CD = 1 AND mds.STRT_TMST >= @stDt AND mds.STRT_TMST < @endDt
				ORDER BY DSPL
 			 			
 			SET @script='SELECT ''IPM Name'''
 			SET @script_prepare=',''?'''
 			
 			SET @colList=''
 			
 			SELECT @colList = @colList + Replace(@script_prepare, '?', DSPL_L)			
			FROM #tmp_dspl_nme
			
			SET @script=@script + @colList
			
			EXEC (@script)
			
  			--=========
  			-- Clean up
  			--=========
  			IF OBJECT_ID('tempdb..#tmp_dspl_nme', 'U') IS NOT NULL	
				drop table #tmp_dspl_nme;  		
  		
  		END
	ELSE IF	@QueryNumber IN (4, 5)
		BEGIN
			SET @SQL=CASE @QueryNumber        		
        		WHEN 4 THEN 'SELECT ''Count of Escalation Totals By MNS PM'''
        		WHEN 5 THEN 'SELECT ''Count of Escalation Totals By Event Status'''        		        		
        		END
      		EXEC (@SQL)		
		END
  
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptMsMgrMNSEscalationReport ' + CAST(@QueryNumber AS VARCHAR(4))
	SET @Desc=@Desc + ', ' + CAST(@FrequencyId AS VARCHAR(4)) + ', ' + CONVERT(VARCHAR(10), @stDt, 101) 
	SET @Desc=@Desc + ', ' + CONVERT(VARCHAR(10), @endDt, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH




GO
