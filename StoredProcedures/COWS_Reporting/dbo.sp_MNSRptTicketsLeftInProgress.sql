USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptTicketsLeftInProgress]    Script Date: 11/21/2019 1:23:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--============================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			11/16/2011
-- Description:		
--					Extract all MDS & MDS Fasttrack events that were placed into "In Progress".
--
--					QueryNumber: 1 = Daily
--								 2 = Weekly
--								 3 = Monthly
--								 0 = Specify start date & end date: 'mm/dd/yyyy'			
--
-- Modifications:			
--
-- 03/28/2012		sxv0766: IM952822-Removed resulting string expression from Event Start Date
--					and Event End Date.
-- 04/24/2012		sxv0766: IM1004557-Get the last "In Progress" action of a given EVENT_ID 
--					from EVENT_HIST table.
--
-- 11/23/2016		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked
----EXEC COWS_Reporting.dbo.sp_MNSRptTicketsLeftInProgress 1,'01/01/2014','11/11/2016'
--============================================================================================
ALTER PROCEDURE [dbo].[sp_MNSRptTicketsLeftInProgress]
	@QueryNumber		INT,		
	@inStartDtStr		VARCHAR(10)='',
	@inEndDtStr			VARCHAR(10)=''
AS
BEGIN TRY

	SET NOCOUNT ON;

	DECLARE @startDate		Datetime
	DECLARE @endDate		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)
	DECLARE @dayOfWeek		INT

	IF @QueryNumber=0
		BEGIN

			SET @startDate=CONVERT(datetime, @inStartDtStr, 101)
			SET @endDate=CONVERT(datetime, @inEndDtStr, 101)			
			
			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			SET @endDate=DATEADD(day, 1, @endDate)
			
		END
	ELSE IF @QueryNumber=1
    	BEGIN    	
    		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
		
			SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)   
			
		END
	ELSE IF @QueryNumber=2
		BEGIN
			-- e.g. Run date is '2011-06-21' 3rd day of the week
			-- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000
			
			SET @today=GETDATE()
			SET @dayOfWeek=DATEPART(dw, @today)
			--SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+6), @today), 112) As DateTime)
			--SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS Datetime)			
			SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)			
							
		END
	ELSE IF @QueryNumber=3
		BEGIN			 
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month 
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
			SET @startDate=cast(@startDtStr AS datetime)
			SET @endDate=cast((cast(SUBSTRING(CONVERT(VARCHAR(10), getdate(), 101),1,2) + '/01/' + CAST(YEAR(getdate()) AS VARCHAR(4)) as datetime) -1) AS datetime)  
		END
		
 	SELECT	mds.EVENT_ID 'Event ID',
			CASE mds.MDS_FAST_TRK_CD
				WHEN 1 THEN 'MDS Fast Track'
				WHEN 0 THEN 'MDS' END 'Event',	
			mds.STRT_TMST 'Event Start Date',	
			--REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Event Start Date',
			mds.END_TMST 'Event End Date',
			--REPLACE(SUBSTRING(CONVERT(char, mds.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 25, 2) 'Event End Date',
			--mact.MDS_ACTY_TYPE_DES 'Event Type',
			CASE
				WHEN mds.MDS_FAST_TRK_TYPE_ID IS NULL THEN ''
				ELSE mftk.MDS_FAST_TRK_TYPE_DES END 'Event Type',			
			UPPER(evst.EVENT_STUS_DES) 'Event Status',
			CASE
				WHEN luser1.DSPL_NME is NULL THEN '' 
				ELSE luser1.DSPL_NME END 'Assigned To',
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 'Author',
			CASE
				WHEN luser3.DSPL_NME is NULL THEN ''
				ELSE luser3.DSPL_NME END 'Editor', 
			'MDS Only'			AS [Network Activity Type],
			''                                     AS [S/C Flag]
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)	
		LEFT JOIN ( SELECT ehis.EVENT_HIST_ID, ehis.EVENT_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT 
						FROM COWS.dbo.EVENT_HIST ehis with (nolock)
						WHERE ehis.EVENT_HIST_ID = (SELECT max(EVENT_HIST_ID)
													FROM (SELECT EVENT_HIST_ID, EVENT_ID
															FROM COWS.dbo.EVENT_HIST with (nolock)
															WHERE ACTN_ID in (19))ehst 
													WHERE ehst.EVENT_ID = ehis.EVENT_ID)
				  )evhi ON mds.EVENT_ID = evhi.EVENT_ID 		
		--JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
		LEFT JOIN COWS.dbo.MDS_FAST_TRK_TYPE mftk with (nolock) ON mds.MDS_FAST_TRK_TYPE_ID = mftk.MDS_FAST_TRK_TYPE_ID
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
					FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
					WHERE REC_STUS_ID = 1
					GROUP BY EVENT_ID)lasg ON mds.EVENT_ID = lasg.EVENT_ID
		LEFT JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
			AND lasg.maxRecTmst = easg.CREAT_DT				
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON easg.ASN_TO_USER_ID = luser1.USER_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.CREAT_BY_USER_ID = luser2.USER_ID 
		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID
		
		-- EVENT_STUS_ID: 5 = InProgress 
		
	WHERE mds.EVENT_STUS_ID = 5 AND mds.END_TMST < @startDate
	/*New MDS EVENT Start*/

	UNION ALL

	SELECT	mds.EVENT_ID 'Event ID',
			CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END 'Event',	
			mds.STRT_TMST 'Event Start Date',	
			--REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Event Start Date',
			mds.END_TMST 'Event End Date',
			--REPLACE(SUBSTRING(CONVERT(char, mds.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 25, 2) 'Event End Date',
			--mact.MDS_ACTY_TYPE_DES 'Event Type',
			CASE
				WHEN mds.MDS_FAST_TRK_TYPE_ID IS NULL THEN ''
				ELSE mftk.MDS_FAST_TRK_TYPE_DES END 'Event Type',			
			UPPER(evst.EVENT_STUS_DES) 'Event Status',
			CASE
				WHEN luser1.DSPL_NME is NULL THEN '' 
				ELSE luser1.DSPL_NME END 'Assigned To',
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 'Author',
			CASE
				WHEN luser3.DSPL_NME is NULL THEN ''
				ELSE luser3.DSPL_NME END 'Editor',
			lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type],
			CASE	
						WHEN eod.SC_CD = 'C'        THEN 'Complex'
						WHEN eod.SC_CD = 'S'        THEN 'Simple'
					    ELSE ''
						END                                     AS [S/C Flag]
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)	
		LEFT JOIN ( SELECT ehis.EVENT_HIST_ID, ehis.EVENT_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT 
						FROM COWS.dbo.EVENT_HIST ehis with (nolock)
						WHERE ehis.EVENT_HIST_ID = (SELECT max(EVENT_HIST_ID)
													FROM (SELECT EVENT_HIST_ID, EVENT_ID
															FROM COWS.dbo.EVENT_HIST with (nolock)
															WHERE ACTN_ID in (19))ehst 
													WHERE ehst.EVENT_ID = ehis.EVENT_ID)
				  )evhi ON mds.EVENT_ID = evhi.EVENT_ID 		
		--JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
		LEFT JOIN COWS.dbo.MDS_FAST_TRK_TYPE mftk with (nolock) ON mds.MDS_FAST_TRK_TYPE_ID = mftk.MDS_FAST_TRK_TYPE_ID
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
					FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
					WHERE REC_STUS_ID = 1
					GROUP BY EVENT_ID)lasg ON mds.EVENT_ID = lasg.EVENT_ID
		LEFT JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
			AND lasg.maxRecTmst = easg.CREAT_DT				
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON easg.ASN_TO_USER_ID = luser1.USER_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.CREAT_BY_USER_ID = luser2.USER_ID 
		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID
		LEFT OUTER JOIN [COWS].[dbo].[MDS_EVENT_ODIE_DEV] eod WITH (NOLOCK) ON eod.EVENT_ID = mds.EVENT_ID
		INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = mds.NTWK_ACTY_TYPE_ID
		-- EVENT_STUS_ID: 5 = InProgress 
		
	WHERE mds.EVENT_STUS_ID = 5 AND mds.END_TMST < @startDate

	/*New MDS EVENT Ends */
	ORDER BY mds.STRT_TMST DESC
  
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptTicketsLeftInProgress ' + CAST(@QueryNumber AS VARCHAR(4)) + ': '	
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101) 	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH

