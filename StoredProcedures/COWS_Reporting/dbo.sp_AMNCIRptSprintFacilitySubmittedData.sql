  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD  
-- EXEC COWS_Reporting.dbo.sp_AMNCIRptSprintFacilitySubmittedData 0  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_AMNCIRptSprintFacilitySubmittedData]   
     @secured   bit = 0,  
     @inStartDtStr    VARCHAR(10)= '',  
     @inEndDtStr   VARCHAR(10)= ''  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
  
  
--DELETE FROM [COWS_Reporting].[dbo].[Sprint_Facility_Submitted_Report_data]  WHERE  
--[COWS_Reporting].[dbo].[Sprint_Facility_Submitted_Report_data].[Order Tracking No] in  
--(SELECT [Order Tracking No] FROM [COWS_Reporting].[dbo].[Sprint_Facility_Submitted_Report_data] as sfs   
--INNER JOIN [COWS].[dbo].[FSA_ORDR] as sf on sfs.[Order Tracking No] = sf.FTN  
--LEFT OUTER JOIN [COWS].[dbo].[ORDR] as odr  
--on odr.ORDR_ID = sf.ORDR_ID  
--WHERE odr.DMSTC_CD = 1 )   
  
 SET NOCOUNT ON;  
 DECLARE @startDate  Datetime  
 DECLARE @endDate  Datetime  
 DECLARE @today   Datetime  
 DECLARE @dayOfWeek  INT  
  
  
 SET @startDate=CONVERT(datetime, @inStartDtStr, 101)  
 SET @endDate=CONVERT(datetime, @inEndDtStr, 101)  
  
 -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @today=GETDATE()  
   SET @dayOfWeek=DATEPART(dw, @today)  
   SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+90), @today), 112) As DateTime)  
   SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS Datetime)  
  
OPEN SYMMETRIC KEY FS@K3y   
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
--INSERT INTO [COWS_Reporting].[dbo].[Sprint_Facility_Submitted_Report_data]   
  
--DECLARE @secured bit = 0 --for testing  
  
SELECT DISTINCT   
a.FTN [Order Tracking No],   
CASE  
   WHEN @secured = 0 AND si.CUST_NME<>''  THEN coalesce(si.CUST_NME,'PRIVATE CUSTOMER')
   WHEN @secured = 0 AND si.CUST_NME =''  THEN 'PRIVATE CUSTOMER'
   WHEN @secured = 0 AND si.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'    
   ELSE coalesce(si.CUST_NME2,si.CUST_NME,'') 
 END [Customer Name], --Secured  
--b.CUST_NME [Customer Name],    
'' [Customer Reference Number],    
CASE  
WHEN tk.TASK_ID = 1001 THEN tk.CREAT_DT END [Installed Date],    
gom.ORDR_CNTCT_ID,  
 gom.FRST_NME + ' ' + gom.LST_NME [Implementation Contact],  
--'Private data' [Implementation Contact],  
cm.ORDR_CNTCT_ID,    
 cm.FRST_NME + ' ' + cm.LST_NME [Customer Project Manager],   
--'Private data' [Customer Project Manager],    
a.CUST_WANT_DT [Sprint Target Delivery Date],    
d.PROD_TYPE_DES [Product],    
a.CUST_CMMT_DT [Customer Commit Date],    
f.STUS_DES [Order Status],    
g.RQSTN_DT [Received Service Request],    
g.CREAT_DT [Received by GOM],    
i.SALS_CHNL_DES [Sales Region],    
'N/A' [Sales Sub-Region],    
 ipm.FRST_NME + ' ' + ipm.LST_NME [ISS Program Manager],    
--'Private data' [ISS Program Manager],   
NULL  [Service Request Type changed],   
j.ORDR_TYPE_DES [Service Request Type],    
a.CREAT_DT [GOM Receives Order],    
h.A_END_REGION [Asset Region],  
CASE    
WHEN DATEPART(DW, a.CREAT_DT) = 1 THEN CONVERT(nvarchar(10), (a.CREAT_DT + 3),101)  
WHEN DATEPART(DW, a.CREAT_DT) = 2 THEN CONVERT(nvarchar(10), (a.CREAT_DT + 2),101)  
WHEN DATEPART(DW, a.CREAT_DT) = 3 THEN CONVERT(nvarchar(10), (a.CREAT_DT + 1),101)  
WHEN DATEPART(DW, a.CREAT_DT) = 5 THEN CONVERT(nvarchar(10), (a.CREAT_DT - 1),101)  
WHEN DATEPART(DW, a.CREAT_DT) = 5 THEN CONVERT(nvarchar(10), (a.CREAT_DT - 2),101)  
WHEN DATEPART(DW, a.CREAT_DT) = 7 THEN CONVERT(nvarchar(10), (a.CREAT_DT - 3),101)  
ELSE CONVERT(nvarchar(10), GETDATE(), 101) END  [Week],  
fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES [F21],  
h.A_END_REGION [F22],  
 'N/A' [F23],  
l.LINE_ITEM_DES [F24],  
 '' [F25],  
 '' [F26],  
 '' [F27],  
l.MRC_CHG_AMT [F28],  
l.NRC_CHG_AMT [F29]            
FROM   
[COWS].[dbo].[FSA_ORDR] a 
LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = a.ORDR_ID
LEFT OUTER JOIN  
[COWS].[dbo].[ORDR] b on a.ORDR_ID = b.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[ORDR_MS] c on a.ORDR_ID = c.ORDR_ID   
LEFT OUTER JOIN (SELECT bb.ORDR_CNTCT_ID,bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME)) [FRST_NME],   
 CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR] FROM [COWS].[dbo].[ORDR_CNTCT] bb   
 LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
 WHERE bb.ROLE_ID = 13  
  AND bb.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)   
  FROM (SELECT ORDR_CNTCT_ID, ORDR_ID   
   FROM [COWS].[dbo].[ORDR_CNTCT] WHERE ROLE_ID = 13) oc   
 WHERE oc.ORDR_ID = bb.ORDR_ID)) gom on gom.ORDR_ID = a.ORDR_ID   
  
LEFT OUTER JOIN (SELECT bb.ORDR_CNTCT_ID, bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME)) [FRST_NME],   
   CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]   
  FROM [COWS].[dbo].[ORDR_CNTCT] bb   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ROLE_ID = 6   
  AND bb.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)   
  FROM (SELECT ORDR_CNTCT_ID, ORDR_ID   
   FROM [COWS].[dbo].[ORDR_CNTCT] WHERE ROLE_ID = 6) oc   
  WHERE oc.ORDR_ID = bb.ORDR_ID)) cm on cm.ORDR_ID = a.ORDR_ID  
  
  
LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] d on a.PROD_TYPE_CD = d.FSA_PROD_TYPE_CD  
LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID   
 FROM [COWS].[dbo].[ACT_TASK] bb  
 LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
 WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
 FROM (SELECT ACT_TASK_ID, ORDR_ID   
  FROM [COWS].[dbo].[ACT_TASK]) tsk   
  WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk on tk.ORDR_ID = a.ORDR_ID  
LEFT OUTER JOIN [COWS].[dbo].[LK_TASK] e  on tk.TASK_ID = e.TASK_ID  
LEFT OUTER JOIN [COWS].[dbo].[LK_STUS] f on e.ORDR_STUS_ID = f.STUS_ID  
LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_GOM_XNCI] g on a.ORDR_ID = g.ORDR_ID  
LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] h on a.ORDR_ID = h.ORDR_ID  
LEFT OUTER JOIN [COWS].[dbo].[LK_SALS_CHNL] i on h.SALS_CHNL_ID = i.SALS_CHNL_ID  
LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME)) [FRST_NME],   
 CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar,   
 DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR] FROM [COWS].[dbo].[ORDR_CNTCT] bb   
 LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
 WHERE bb.ROLE_ID = 35   
   AND bb.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)   
  FROM (SELECT ORDR_CNTCT_ID, ORDR_ID   
   FROM [COWS].[dbo].[ORDR_CNTCT] WHERE ROLE_ID = 35) oc   
  WHERE oc.ORDR_ID = bb.ORDR_ID)) ipm on ipm.ORDR_ID = a.ORDR_ID   
  
   
   
   
LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] j on j.FSA_ORDR_TYPE_CD = a.ORDR_TYPE_CD   
LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] k on b.RGN_ID = k.RGN_ID   
LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] l on a.ORDR_ID = l.ORDR_ID   
  
LEFT OUTER JOIN (SELECT 
	bb.ORDR_ID, 
	bb.SOI_CD, 
	bb.CIS_LVL_TYPE, 
	CONVERT(VARCHAR, DecryptByKey(csd.CUST_NME)) [CUST_NME2],
	bb.CUST_NME as  [CUST_NME]   
 FROM [COWS].[dbo].[FSA_ORDR_CUST] bb   
 LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID  
 LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5     
 WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
 FROM (SELECT SOI_CD, ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST]) soi  
 WHERE soi.ORDR_ID = bb.ORDR_ID)) si on si.ORDR_ID = a.ORDR_ID   
WHERE  
  b.DMSTC_CD = 1  
  AND b.RGN_ID = 1 --AMNCI  
  AND b.PLTFRM_CD = 'SF'  
  AND si.CIS_LVL_TYPE = 'H5'  
  ---AND a.FTN = 11132337  
ORDER BY a.FTN  
  
--SELECT * FROM [COWS_Reporting].[dbo].[Sprint_Facility_Submitted_Report_data]   
  
  
CLOSE SYMMETRIC KEY FS@K3y   
  
END  
  