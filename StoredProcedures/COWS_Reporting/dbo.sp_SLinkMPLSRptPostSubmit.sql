USE [COWS_Reporting]
GO
/*
--=========================================================================================
-- Project:			Maintenance 
-- Author:			Jagan	
-- Date:			01/27/2014
-- Description:	
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD	
--		[dbo].[sp_SLinkMPLSRptPostSubmit] --0, 'N', '07/01/2014', '07/24/2014'
--=========================================================================================
*/

ALTER Procedure [dbo].[sp_SLinkMPLSRptPostSubmit] 
 @SecuredUser		INT=0,
 @getSensData		CHAR(1)='N',   
 @startDate   Datetime=NULL,  
 @endDate   Datetime=NULL  
AS

BEGIN TRY

	--DECLARE @startDate		DATETIME
	--DECLARE @endDate		DATETIME
	DECLARE	@today			DATETIME
	DECLARE @dayOfWeek		INT
	IF OBJECT_ID(N'tempdb..#ResultsTbl', N'U') IS NOT NULL         
				DROP TABLE #ResultsTbl

	CREATE TABLE #ResultsTbl 
	(EventID INT, 
	PLDALCkt VARCHAR(1000), 
	TrptFTN VARCHAR(1000), 
	PortSpeedBwdth VARCHAR(1000),
	NUA VARCHAR(1000), 
	EventStatus VARCHAR(100), 
	WrkflwStatus VARCHAR(100),
	Title VARCHAR(1000),
	EventType VARCHAR(1000),
	CustName VARCHAR(1000),
	[Action] VARCHAR(250),
	ModfdDt VARCHAR(100),
	ModfdBy VARCHAR(100),
	ActvCmmnt VARCHAR(4000),
	DescCmmnt VARCHAR(2000),
	IPVer VARCHAR(100),
	IsCP VARCHAR(10),
	IsEsc VARCHAR(10),
	IsMDS VARCHAR(10),
	IsNDD VARCHAR(10),
	IsWP VARCHAR(10),
	RelCmpCD VARCHAR(10),
	RelCmpNCRNme VARCHAR(12))
	

	--SET @startDate=CONVERT(DATETIME, @inStartDtStr, 101)
	--SET @endDate=CONVERT(DATETIME, @inEndDtStr, 101)
	IF ((@startDate IS NULL) AND (@endDate IS NULL))
	BEGIN
		SET @today=GETDATE()
		SET @dayOfWeek=DATEPART(dw, @today)
		SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+90), @today), 112) As DATETIME)
		SET @endDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS DATETIME)
	END


OPEN SYMMETRIC KEY FS@K3y             
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

INSERT INTO #ResultsTbl
SELECT
mp.EVENT_ID as 'Event ID',
[dbo].[GetCommaSepEventValues] ('dbo.MPLS_EVENT_ACCS_TAG', 'PL_DAL_CKT_NBR', mp.EVENT_ID) as 'PL/DAL Circuit',
[dbo].[GetCommaSepEventValues] ('dbo.MPLS_EVENT_ACCS_TAG', 'TRNSPRT_OE_FTN_NBR', mp.EVENT_ID) as 'Transport FTN',
[dbo].[GetCommaSepEventValues] ('dbo.MPLS_EVENT_ACCS_TAG', 'MPLS_ACCS_BDWD_ID', mp.EVENT_ID) as 'PortSpeedBwdth',
[dbo].[GetCommaSepEventValues] ('dbo.MPLS_EVENT_ACCS_TAG', 'TRS_NET_449_ADR', mp.EVENT_ID) as 'NUA',
les.EVENT_STUS_DES as 'Event Status',
lws.WRKFLW_STUS_DES as 'WorkFlow Status',
CASE 
	   --WHEN @SecuredUser = 0 AND  ev.SCURD_CD = 1 AND COALESCE(CONVERT(VARCHAR(2000), DecryptByKey(mp.EVENT_TITLE_TXT)),'') <> '' THEN 'Private Customer'
	   --WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.SCURD_CD = 1 AND COALESCE(CONVERT(VARCHAR(2000), DecryptByKey(mp.EVENT_TITLE_TXT)), '') <> '' THEN 'Private Customer'
    --   ELSE ISNULL(CONVERT(VARCHAR(2000), DecryptByKey(mp.EVENT_TITLE_TXT)),'') 
       
        WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NOT NULL THEN 'Private Customer'
		WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NULL THEN mp.EVENT_TITLE_TXT
		WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  = 0  THEN mp.EVENT_TITLE_TXT
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NOT NULL THEN 'Private Customer'
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NULL THEN mp.EVENT_TITLE_TXT
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  = 0 THEN mp.EVENT_TITLE_TXT
		ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) ,mp.EVENT_TITLE_TXT,'')
       END [Title],
'MPLS - ' + lme.MPLS_EVENT_TYPE_DES as 'Event Type',
CASE 
	   --WHEN @SecuredUser = 0 AND  ev.SCURD_CD = 1 AND COALESCE(CONVERT(VARCHAR(255), DecryptByKey(mp.CUST_NME)),'') <> '' THEN 'Private Customer'
	   --WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.SCURD_CD = 1 AND COALESCE(CONVERT(VARCHAR(255), DecryptByKey(mp.CUST_NME)), '') <> '' THEN 'Private Customer'
       --ELSE ISNULL(CONVERT(VARCHAR(255), DecryptByKey(mp.CUST_NME)),'') 
        WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NOT NULL THEN 'Private Customer'
		WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NULL THEN mp.CUST_NME
		WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  = 0  THEN mp.CUST_NME
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NOT NULL THEN 'Private Customer'
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NULL THEN mp.CUST_NME
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  = 0 THEN mp.CUST_NME
		ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)) ,mp.CUST_NME,'')
       END [Customer Name],
la.ACTN_DES as 'Action',
CONVERT(VARCHAR, ehst.CREAT_DT, 120) as 'Modified Date',
lu.FULL_NME as 'Modified By',
SUBSTRING(COALESCE(ehst.CMNT_TXT,''),1,4000) as 'Activator Comments',
mp.DES_CMNT_TXT as 'Description and Comments',
liv.IP_VER_NME as 'IP Version',
CASE WHEN (mp.CXR_PTNR_CD = 1) THEN 'TRUE' ELSE 'FALSE' END as 'Is Carrier Partner',
CASE WHEN (mp.ESCL_CD = 1) THEN 'TRUE' ELSE 'FALSE' END as 'Is Escalation',
CASE WHEN (mp.MDS_MNGD_CD = 1) THEN 'TRUE' ELSE 'FALSE' END as 'Is MDS Managed',
CASE WHEN (mp.NDD_UPDTD_CD = 1) THEN 'TRUE' ELSE 'FALSE' END as 'Is NDD Updated',
CASE WHEN (mp.WHLSL_PTNR_CD = 1) THEN 'TRUE' ELSE 'FALSE' END as 'Is Wholesale Partner',
CASE WHEN (mp.RELTD_CMPS_NCR_CD = 1) THEN 'TRUE' ELSE 'FALSE' END as 'Is Related Compass NCR',
mp.RELTD_CMPS_NCR_NME as 'Related Compass NCR Name'
FROM [COWS].[dbo].[EVENT] ev  WITH (NOLOCK) INNER JOIN
       COWS.dbo.MPLS_EVENT mp WITH (NOLOCK) ON mp.EVENT_ID = ev.EVENT_ID INNER JOIN
	   COWS.dbo.LK_MPLS_EVENT_TYPE lme WITH (NOLOCK) ON lme.MPLS_EVENT_TYPE_ID = mp.MPLS_EVENT_TYPE_ID INNER JOIN
       COWS.dbo.LK_EVENT_STUS les WITH (NOLOCK) ON les.EVENT_STUS_ID = mp.EVENT_STUS_ID INNER JOIN
       COWS.dbo.LK_WRKFLW_STUS lws WITH (NOLOCK) ON lws.WRKFLW_STUS_ID = mp.WRKFLW_STUS_ID INNER JOIN
	   COWS.dbo.EVENT_HIST ehst WITH (NOLOCK) ON mp.EVENT_ID = ehst.EVENT_ID INNER JOIN
	   COWS.dbo.LK_USER lu WITH (NOLOCK) ON ehst.CREAT_BY_USER_ID = lu.[USER_ID] INNER JOIN
	   COWS.dbo.LK_ACTN la WITH (NOLOCK) ON la.ACTN_ID = ehst.ACTN_ID INNER JOIN
	   COWS.dbo.LK_IP_VER liv WITH (NOLOCK) ON mp.IP_VER_ID = liv.IP_VER_ID  LEFT OUTER JOIN 
	   [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mp.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=9
WHERE ehst.CREAT_DT >= @startDate
       and ehst.CREAT_DT <= @endDate
  and mp.EVENT_STUS_ID in (3, 4, 5, 6, 7, 9, 10) and (mp.wrkflw_stus_id <> 3)
  and la.ACTN_ID not in (2,4,6,7,8,14,18,20)

INSERT INTO #ResultsTbl
SELECT
se.EVENT_ID as 'Event ID',
[dbo].[GetCommaSepEventValues] ('dbo.SPLK_EVENT_ACCS', 'NEW_PL_NBR', se.EVENT_ID) as 'PL/DAL Circuit',
se.FTN as 'Transport FTN',
[dbo].[GetCommaSepEventValues] ('dbo.SPLK_EVENT_ACCS', 'NEW_PORT_SPEED_DES', se.EVENT_ID) as 'PortSpeedBwdth',
'' as 'NUA',
les.EVENT_STUS_DES as 'Event Status',
lws.WRKFLW_STUS_DES as 'WorkFlow Status',
CASE 
	   --WHEN @SecuredUser = 0 AND  ev.SCURD_CD = 1 AND COALESCE(CONVERT(VARCHAR(2000), DecryptByKey(se.EVENT_TITLE_TXT)),'') <> '' THEN 'Private Customer'
	   --WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.SCURD_CD = 1 AND COALESCE(CONVERT(VARCHAR(2000), DecryptByKey(se.EVENT_TITLE_TXT)), '') <> '' THEN 'Private Customer'
       --ELSE ISNULL(CONVERT(VARCHAR(2000), DecryptByKey(se.EVENT_TITLE_TXT)),'')
       
        WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NOT NULL THEN 'Private Customer'
		WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NULL THEN se.EVENT_TITLE_TXT
		WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  = 0  THEN se.EVENT_TITLE_TXT
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NOT NULL THEN 'Private Customer'
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NULL THEN se.EVENT_TITLE_TXT
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  = 0 THEN se.EVENT_TITLE_TXT
		ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) ,se.EVENT_TITLE_TXT,'')
		 
       END [Title],
'SprintLink - ' + lse.SPLK_EVENT_TYPE_DES as 'Event Type',
CASE 
	   --WHEN @SecuredUser = 0 AND  ev.SCURD_CD = 1 AND COALESCE(CONVERT(VARCHAR(255), DecryptByKey(se.CUST_NME)),'') <> '' THEN 'Private Customer'
	   --WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.SCURD_CD = 1 AND COALESCE(CONVERT(VARCHAR(255), DecryptByKey(se.CUST_NME)), '') <> '' THEN 'Private Customer'
       --ELSE ISNULL(CONVERT(VARCHAR(255), DecryptByKey(se.CUST_NME)),'') 
       
        WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NOT NULL THEN 'Private Customer'
		WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NULL THEN se.CUST_NME
		WHEN @SecuredUser = 0 AND ev.CSG_LVL_ID  = 0  THEN se.CUST_NME
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NOT NULL THEN 'Private Customer'
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NULL THEN se.CUST_NME
		WHEN @SecuredUser = 1 AND @getSensData='N' AND ev.CSG_LVL_ID  = 0 THEN se.CUST_NME
		ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)) ,se.CUST_NME,'')
       END [Customer Name],
la.ACTN_DES as 'Action',
CONVERT(VARCHAR, ehst.CREAT_DT, 120) as 'Modified Date',
lu.FULL_NME as 'Modified By',
SUBSTRING(COALESCE(ehst.CMNT_TXT,''),1,4000) as 'Activator Comments',
se.DSGN_CMNT_TXT as 'Description and Comments',
liv.IP_VER_NME as 'IP Version',
'' as 'Is Carrier Partner',
CASE WHEN (se.ESCL_CD = 1) THEN 'TRUE' ELSE 'FALSE' END as 'Is Escalation',
CASE WHEN (se.MDS_MNGD_CD = 1) THEN 'TRUE' ELSE 'FALSE' END as 'Is MDS Managed',
'' as 'Is NDD Updated',
'' as 'Is Wholesale Partner',
CASE WHEN (se.RELTD_CMPS_NCR_CD = 1) THEN 'TRUE' ELSE 'FALSE' END as 'Is Related Compass NCR',
se.RELTD_CMPS_NCR_NME as 'Related Compass NCR Name'
FROM [COWS].[dbo].[EVENT] ev  WITH (NOLOCK) INNER JOIN
       COWS.dbo.SPLK_EVENT se WITH (NOLOCK) ON se.EVENT_ID = ev.EVENT_ID INNER JOIN
	   COWS.dbo.LK_SPLK_EVENT_TYPE lse WITH (NOLOCK) ON lse.SPLK_EVENT_TYPE_ID = se.SPLK_EVENT_TYPE_ID INNER JOIN
       COWS.dbo.LK_EVENT_STUS les WITH (NOLOCK) ON les.EVENT_STUS_ID = se.EVENT_STUS_ID INNER JOIN
       COWS.dbo.LK_WRKFLW_STUS lws WITH (NOLOCK) ON lws.WRKFLW_STUS_ID = se.WRKFLW_STUS_ID INNER JOIN
	   COWS.dbo.EVENT_HIST ehst WITH (NOLOCK) ON se.EVENT_ID = ehst.EVENT_ID INNER JOIN
	   COWS.dbo.LK_USER lu WITH (NOLOCK) ON ehst.CREAT_BY_USER_ID = lu.[USER_ID] INNER JOIN
	   COWS.dbo.LK_ACTN la WITH (NOLOCK) ON la.ACTN_ID = ehst.ACTN_ID INNER JOIN
	   COWS.dbo.LK_IP_VER liv WITH (NOLOCK) ON se.IP_VER_ID = liv.IP_VER_ID LEFT OUTER JOIN 
	   [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=se.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=19
WHERE ehst.CREAT_DT >= @startDate
       and ehst.CREAT_DT <= @endDate
  and se.EVENT_STUS_ID in (3, 4, 5, 6, 7, 9, 10) and (se.wrkflw_stus_id <> 3)
  and la.ACTN_ID not in (2,4,6,7,8,14,18,20)

CREATE INDEX IDX_ResultsTbl_EventID ON #ResultsTbl(EventID)
CREATE INDEX IDX_ResultsTbl_ModfdDt ON #ResultsTbl(ModfdDt)

SELECT
	DISTINCT 
	EventID as 'Event ID',
	PLDALCkt as 'PL/DAL Circuit', 
	TrptFTN as 'Transport FTN', 
	PortSpeedBwdth as 'PortSpeed/Bandwidth',
	NUA as 'NUA', 
	EventStatus as 'Event Status',
	WrkflwStatus as 'WorkFlow Status',
	Title,
	EventType as 'Event Type',
	CustName as 'Customer Name',
	[Action] as 'Action',
	ModfdDt as 'Modified Date',
	ModfdBy as 'Modified By',
	ActvCmmnt as 'Activator Comments',
	DescCmmnt as 'Description and Comments',
	IPVer as 'IP Version',
	IsCP as 'Is Carrier Partner',
	IsEsc as 'Is Escalation',
	IsMDS as 'Is MDS Managed',
	IsNDD as 'Is NDD Updated',
	IsWP as 'Is Wholesale Partner',
	RelCmpCD as 'Is Related Compass NCR',
	RelCmpNCRNme as 'Related Compass NCR Name'
FROM #ResultsTbl
ORDER BY EventID DESC, ModfdDt DESC
		
  	RETURN 0;
  	  	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(100)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_SLinkMPLSRptPostSubmit '
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH

