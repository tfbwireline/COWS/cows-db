/*  
 --=============================================  
 --Author:  <Author,,Name>  
 --Create date: <Create Date,,>  
 --Description: <Description,,>  
 -- Modification:   
 -- Updated By:   Md M Monir  
 -- Updated Date: 03/19/2018  
 -- Updated Reason: SCURD_CD/CSG_LVL_CD  
 --  ci554013 10/14/2013   Added Decrypt to Customer Name for H5_folder change 
 -- Update date: <06/06/2018>
 -- Updated by: Md M Monir  
 -- Description: <To get H5 Data>  CTY_NME 
 -- [dbo].[sp_ENCIRptRegionalPendingDisconnectKPI] 1  
 --=============================================  
 */  
   
ALTER PROCEDURE [dbo].[sp_ENCIRptRegionalPendingDisconnectKPI]  
     @secured   bit = 0,  
     @interval   CHAR = 'M',  
     @inStartDtStr    VARCHAR(10)= '',  
     @inEndDtStr   VARCHAR(10)= ''  
AS  
BEGIN  
  --SET NOCOUNT ON added to prevent extra result sets from  
  --interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
 DECLARE @startDate  Datetime  
 DECLARE @endDate  Datetime  
 DECLARE @today   Datetime  
 DECLARE @LastMonth      INT  
 DECLARE @yearLastMonth  INT  
--DECLARE @interval           CHAR = 'M'  
--DECLARE @secured   bit = 1  
--DECLARE @inStartDtStr    VARCHAR(10)= '09/01/2011'  
--DECLARE @inEndDtStr   VARCHAR(10)= '01/31/2012'  
  
  
 SET @startDate=CONVERT(datetime, @inStartDtStr, 101)  
 SET @endDate=CONVERT(datetime, @inEndDtStr, 101)  
 SET @startDate=CONVERT(datetime, @inStartDtStr, 101)  
 SET @endDate=CONVERT(datetime, @inEndDtStr, 101)  
 SET @today = GETDATE()  
 SET @LastMonth = DATEPART(M, DATEADD(M, -1, @today))  
 SET @yearLastMonth  = DATEPART(YEAR, DATEADD(M, -1, @today))  
  
IF (@inStartDtStr = '' AND @interval = 'M') BEGIN  
   SET @startDate =  cast(@LastMonth as varchar(2)) + '/01/' + cast(@yearLastMonth   as varchar(4))  
   SET @endDate= cast(CONVERT(VARCHAR(8), DATEADD(M, 1, @startDate), 112) As DateTime)  
END   
   
IF (@inStartDtStr = '' AND @interval = 'W') BEGIN  
   SET @startDate =  cast(@LastMonth as varchar(2)) + '/01/' + cast(@yearLastMonth   as varchar(4))  
   SET @endDate= @today  
END   
  
-- ENCI Regional Order Pending KPI Report  
  
OPEN SYMMETRIC KEY FS@K3y   
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
SELECT DISTINCT  
  
  
  
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN convert(varchar(50),b.ORDR_ID)  
 WHEN b.ORDR_CAT_ID = 4 THEN convert(varchar(50),b.ORDR_ID)  
 WHEN b.ORDR_CAT_ID = 5 THEN convert(varchar(50),b.ORDR_ID)  
--Start of code changes by Anitha  
 --WHEN b.ORDR_CAT_ID = 2 THEN c.FTN END [Order Tracking No],   
 WHEN b.ORDR_CAT_ID = 6 THEN c.FTN  
 WHEN b.ORDR_CAT_ID = 2 THEN c.FTN END [FTN],   
nc.NUA_ADR [NUA],   
--End of code changes by Anitha  
  
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN t.PRE_XST_PL_NBR  
 WHEN b.ORDR_CAT_ID = 4 THEN t.PRE_XST_PL_NBR  
 WHEN b.ORDR_CAT_ID = 5 THEN t.PRE_XST_PL_NBR  
 WHEN b.ORDR_CAT_ID = 6 THEN NC.PLN_NME  
 WHEN b.ORDR_CAT_ID = 2 THEN NC.PLN_NME END [Private Line Number],  
   
CASE  
 WHEN @secured = 0 and b.CSG_LVL_ID =0 THEN p.CUST_NME  
 WHEN @secured = 0 and b.CSG_LVL_ID >0 AND p.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
 WHEN @secured = 0 and b.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd2.CUST_NME)) IS NULL THEN NULL   
 ELSE coalesce(p.CUST_NME,CONVERT(varchar, DecryptByKey(csd2.CUST_NME)),'') END [Customer Name], --Secured  
   
CASE  
 WHEN b.ORDR_CAT_ID = 1 THEN u.PROD_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 4 THEN u.PROD_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 5 THEN u.PROD_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 6 THEN d.PROD_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 2 THEN d.PROD_TYPE_DES END  [Product],   
   
--p.CUST_CTY_NME [H5 City],  
CASE WHEN (p.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd2.CTY_NME) ELSE p.CUST_CTY_NME END AS  [H5 City], /*Monir 06062018*/
--lc.CTRY_NME  [H5 Country], 
CASE WHEN (p.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE lc.CTRY_NME END [H5 Country], /*Monir 06062018*/   
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN CONVERT(varchar,t.CKT_SPD_QTY)  
 WHEN b.ORDR_CAT_ID = 4 THEN CONVERT(varchar,t.CKT_SPD_QTY)  
 WHEN b.ORDR_CAT_ID = 5 THEN CONVERT(varchar,t.CKT_SPD_QTY)  
 WHEN b.ORDR_CAT_ID = 6 THEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES  
 WHEN b.ORDR_CAT_ID = 2 THEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES   
 --Start of code changes by Anitha  
 --END  [Order Bandwidth],   
 END  [Speed of Service],  
 --End of code changes by Anitha   
--Start of code deletion by Anitha  
--o.SALS_CHNL_DES [Sales Region],   
--End of code deletion by Anitha  
f.ORDR_STUS_DES [Order Status],  
ISNULL(CAST(b5.MRC_CHG_AMT AS MONEY), 0) [Monthly Recurring Charge (MRC)],  
     
   
ISNULL(CAST(b5.NRC_CHG_AMT AS MONEY), 0) [Non-Recurring Charge (NRC)],  
      
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN t.CUST_CNTRC_SIGN_DT  
 WHEN b.ORDR_CAT_ID = 4 THEN t.CUST_CNTRC_SIGN_DT  
 WHEN b.ORDR_CAT_ID = 5 THEN t.CUST_CNTRC_SIGN_DT  
 WHEN b.ORDR_CAT_ID = 6 THEN c.CUST_SIGNED_DT  
 WHEN b.ORDR_CAT_ID = 2 THEN c.CUST_SIGNED_DT   
 --Start of code changes by Anitha   
 --END [Contract Signed Date],   
 END [Customer Signed Date],   
 --End of code changes by Anitha  
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN t.CREAT_DT  
 WHEN b.ORDR_CAT_ID = 4 THEN t.CREAT_DT  
 WHEN b.ORDR_CAT_ID = 5 THEN t.CREAT_DT  
 WHEN b.ORDR_CAT_ID = 6 THEN c.CREAT_DT  
 WHEN b.ORDR_CAT_ID = 2 THEN c.CREAT_DT   
 --Start of code changes by Anitha   
 --END [Received Service Request],   
 END [Order Submit Date],  
 --End of code changes by Anitha   
/*Start of code removal by Anitha  
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 2 THEN c.ORDR_SBMT_DT  END [Complete order was received],  
End of code removal by Anitha*/   
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 4 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 5 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 6 THEN c.CUST_WANT_DT  
 WHEN b.ORDR_CAT_ID = 2 THEN c.CUST_WANT_DT    
 --Start of code changes by Anitha  
 --END [Order CWD],  
 END [Customer Want Date],  
 --End of code changes by Anitha  
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN t.CUST_CMMT_DT  
 WHEN b.ORDR_CAT_ID = 4 THEN t.CUST_CMMT_DT  
 WHEN b.ORDR_CAT_ID = 5 THEN t.CUST_CMMT_DT  
 WHEN b.ORDR_CAT_ID = 6 THEN c.CUST_CMMT_DT  
 WHEN b.ORDR_CAT_ID = 2 THEN c.CUST_CMMT_DT  END [Customer Commit Date],   
   
--Start of code changes by Anitha   
--tk.CREAT_DT [Installed Date],   
tk.CREAT_DT [Order Bill clear / install date],   
--End of code changes by Anitha  
  
/*Start of code removal by Anitha  
td.TRGT_DLVRY_DT [Sprint Target Delivery Date],   
  
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN y.VNDR_NME  
 WHEN b.ORDR_CAT_ID = 2 THEN y.VNDR_NME  END  [Origination Vendor],  
End of code removal by Anitha*/  
    
CASE   
 WHEN b.ORDR_CAT_ID = 1 THEN ab.VNDR_NME  
 WHEN b.ORDR_CAT_ID = 4 THEN ab.VNDR_NME  
 WHEN b.ORDR_CAT_ID = 5 THEN ab.VNDR_NME  
 WHEN b.ORDR_CAT_ID = 6 THEN ab.VNDR_NME  
 WHEN b.ORDR_CAT_ID = 2 THEN ab.VNDR_NME   
 --Start of code changes by Anitha  
 --END   [Termination Vendor],   
 END   [Vendor Name],  
 --End of code changes by Anitha   
CASE  
 WHEN @secured = 0 and b.CSG_LVL_ID =0 THEN lu.FULL_NME  
 WHEN @secured = 0 and b.CSG_LVL_ID >0 AND lu.FULL_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
 WHEN @secured = 0 and b.CSG_LVL_ID >0 AND lu.FULL_NME IS NULL THEN NULL   
 ELSE lu.FULL_NME   
 END [Assigned user in xNCI WFM],  
  
/*Start of code removal by Anitha  
'' [Regional Account Manager],   
CASE  
 WHEN @secured = 0 and b.SCURD_CD = 0 THEN svm.FRST_NME + ' ' + svm.LST_NME  
 WHEN @secured = 0 and b.SCURD_CD = 1 AND svm.FRST_NME + ' ' + svm.LST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
 WHEN @secured = 0 and b.SCURD_CD = 1 AND svm.FRST_NME + ' ' + svm.LST_NME IS NULL THEN NULL  
 ELSE svm.FRST_NME + ' ' + svm.LST_NME   
 END [Service Manager],   
CASE  
 WHEN @secured = 0 and b.SCURD_CD = 0 THEN ipm.FRST_NME + ' ' + ipm.LST_NME   
 WHEN @secured = 0 and b.SCURD_CD = 1 AND ipm.FRST_NME + ' ' + ipm.LST_NME  IS NOT NULL THEN 'PRIVATE CUSTOMER'   
 WHEN @secured = 0 and b.SCURD_CD = 1 AND ipm.FRST_NME + ' ' + ipm.LST_NME  IS NULL THEN NULL  
 ELSE ipm.FRST_NME + ' ' + ipm.LST_NME  END [Customer Project Manager (CPM)],  
    
CASE  
 WHEN b.ORDR_CAT_ID = 1 THEN t.SALS_PERSN_NME  
 WHEN b.ORDR_CAT_ID = 2 THEN  cst.SALS_PERSN_PRIM_CID END [Quota Bearing Sales],  
   
b.MODFD_DT [Last Updated],  
End of code removal by Anitha*/  
   
CASE  
 WHEN b.ORDR_CAT_ID = 6 THEN j.ORDR_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 2 THEN j.ORDR_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 4 THEN  m.ORDR_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 5 THEN  m.ORDR_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 1 THEN  m.ORDR_TYPE_DES END [Service Request Type],  
  
/*Start of code removal by Anitha    
CASE  
 WHEN b.ORDR_CAT_ID = 1 THEN  ds.SRVC_CKT_DES  
 WHEN b.ORDR_CAT_ID = 2 THEN  ISNULL(b1.LINE_ITEM_DES, '') + ' ' + ISNULL(b2.LINE_ITEM_DES, '')   
  + ' ' + ISNULL(b3.LINE_ITEM_DES, '') + ' ' + ISNULL(b4.LINE_ITEM_DES, '') + ' ' + ISNULL(b5.LINE_ITEM_DES, '')   
  + ' ' + ISNULL(b6.LINE_ITEM_DES, '') + ' ' + ISNULL(b7.LINE_ITEM_DES, '') + ' ' + ISNULL(b8.LINE_ITEM_DES, '')   
  + ' ' + ISNULL(b9.LINE_ITEM_DES, '') END [Order Description]  
End of code removal by Anitha*/   
--Start of code addition by Anitha  
gq.[GOM Receives Order] [GOM Receives Order Date]  
--End of code addition by Anitha  
   
FROM  
[COWS].[dbo].[ORDR] b LEFT OUTER JOIN  
[COWS].[dbo].[FSA_ORDR] c with (nolock) on b.ORDR_ID = c.ORDR_ID   
LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = c.ORDR_ID  
LEFT OUTER JOIN  
[COWS].[dbo].[IPL_ORDR] t with (nolock) on b.ORDR_ID = t.ORDR_ID LEFT OUTER JOIN  
  
[COWS].[dbo].[LK_PROD_TYPE] d with (nolock) on c.PROD_TYPE_CD = d.FSA_PROD_TYPE_CD LEFT OUTER JOIN  
[COWS].[dbo].[LK_PROD_TYPE] u with (nolock) on  t.PROD_TYPE_ID = u.PROD_TYPE_ID LEFT OUTER JOIN  
  
--(SELECT  ORDR_ID,  CONVERT(varchar, DecryptByKey(bb.CTY_NME)) [CTY_NME]  FROM   [COWS].[dbo].[ORDR_ADR] bb WHERE bb.CIS_LVL_TYPE in ('H5', 'H6')) e on b.ORDR_ID = e.ORDR_ID LEFT OUTER JOIN  
--(SELECT ORDR_ID,  CONVERT(varchar, DecryptByKey(bb.CTY_NME)) [CTY_NME] FROM   [COWS].[dbo].[ORDR_ADR] bb WHERE bb.ADR_TYPE_ID = 11) iplorig on b.ORDR_ID = iplorig.ORDR_ID LEFT OUTER JOIN  
--(SELECT ORDR_ID,  CONVERT(varchar, DecryptByKey(bb.CTY_NME)) [CTY_NME] FROM   [COWS].[dbo].[ORDR_ADR] bb WHERE bb.ADR_TYPE_ID = 12) iplterm on b.ORDR_ID = iplterm.ORDR_ID LEFT OUTER JOIN  
  
[COWS].[dbo].[LK_ORDR_STUS] f with (nolock) on b.ORDR_STUS_ID = f.ORDR_STUS_ID LEFT OUTER JOIN  
[COWS].[dbo].[ORDR_MS] g with (nolock) on b.ORDR_ID = g.ORDR_ID JOIN  
  
(SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID, cc.ORDR_STUS_ID  
 FROM [COWS].[dbo].[ACT_TASK] bb LEFT OUTER JOIN  
 [COWS].[dbo].[ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
 WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
 FROM (SELECT ACT_TASK_ID, ORDR_ID, TASK_ID   
  FROM [COWS].[dbo].[ACT_TASK] ) tsk   
  WHERE tsk.ORDR_ID = bb.ORDR_ID and cc.ORDR_STUS_ID in (1))) tk on  b.ORDR_ID = tk.ORDR_ID LEFT OUTER JOIN  
    
[COWS].[dbo].[LK_TASK] h with (nolock) on tk.TASK_ID = h.TASK_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_STUS]i with (nolock) on h.ORDR_STUS_ID = i.STUS_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_ORDR_TYPE] j with (nolock) on  c.ORDR_TYPE_CD = j.FSA_ORDR_TYPE_CD LEFT OUTER JOIN  
[COWS].[dbo].[LK_ORDR_TYPE] m with (nolock) on  t.ORDR_TYPE_ID = m.ORDR_TYPE_ID LEFT OUTER JOIN  
  
--(SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME))[FRST_NME] , CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar, DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]  
-- FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
-- [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
-- WHERE bb.ROLE_ID = 13) ipm on  b.ORDR_ID  = ipm.ORDR_ID LEFT OUTER JOIN  
  
--(SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME))[FRST_NME] , CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar, DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]  
-- FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
-- [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
-- WHERE bb.ROLE_ID = 31) rpm on b.ORDR_ID = rpm.ORDR_ID  LEFT OUTER JOIN  
  
--(SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME))[FRST_NME] , CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar, DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]  
-- FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
-- [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
-- WHERE bb.ROLE_ID = 34) svm on  b.ORDR_ID = svm.ORDR_ID LEFT OUTER JOIN  
  
--(SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.CTY_CD)) [CTY_CD]   
-- FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
-- [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
-- WHERE bb.CNTCT_TYPE_ID = 15) org on b.ORDR_ID  = org.ORDR_ID LEFT OUTER JOIN  
   
--(SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME)) [FRST_NME], CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar, DecryptByKey(bb.EMAIL_ADR))[EMAIL_ADR]  
-- FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
-- [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
-- WHERE bb.ROLE_ID = 30) qbs on  b.ORDR_ID = qbs.ORDR_ID LEFT OUTER JOIN  
   
--(SELECT bb.ORDR_ID, CONVERT(varchar, DecryptByKey(bb.CUST_NME)) [CUST_NME], CIS_LVL_TYPE,  
-- bb.SALS_PERSN_PRIM_CID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb join  
-- [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
-- WHERE CIS_LVL_TYPE = 'H5') cst on  b.ORDR_ID = cst.ORDR_ID LEFT OUTER JOIN  
  
  
[COWS].[dbo].[H5_FOLDR] cust on b.H5_FOLDR_ID = cust.H5_FOLDR_ID LEFT OUTER JOIN   
[COWS].[dbo].[LK_VNDR] l with (nolock) on c.INSTL_VNDR_CD = l.VNDR_CD   LEFT OUTER JOIN  
[COWS].[dbo].[TRPT_ORDR]  n with (nolock) on b.ORDR_ID = n.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_SALS_CHNL] o with (nolock) on n.SALS_CHNL_ID = o.SALS_CHNL_ID LEFT OUTER JOIN  
[COWS].[dbo].[CKT] q on b.ORDR_ID = q.ORDR_ID LEFT OUTER JOIN  
  
(SELECT DISTINCT bb.CKT_ID, bb.VER_ID, bb.TRGT_DLVRY_DT, cc.PL_SEQ_NBR, cc.ORDR_ID from [COWS].[dbo].[CKT_MS] bb   
join [COWS].[dbo].[CKT] cc on cc.CKT_ID = bb.CKT_ID   
where bb.TRGT_DLVRY_DT = (SELECT MAX(TRGT_DLVRY_DT) from (SELECT  
        TRGT_DLVRY_DT, CKT_ID FROM [COWS].[dbo].[CKT_COST]) ee where ee.CKT_ID = bb.CKT_ID ))td on b.ORDR_ID = td.ORDR_ID LEFT OUTER JOIN  
  
(SELECT DISTINCT bb.TRMTG_CD, bb.ORDR_ID, bb.VNDR_FOLDR_ID FROM [COWS].[dbo].[VNDR_ORDR] bb JOIN [COWS].[dbo].[ORDR] cc   
 on cc.ORDR_ID = bb.ORDR_ID WHERE TRMTG_CD = 0)  w on b.ORDR_ID = w.ORDR_ID LEFT OUTER JOIN  
   
[COWS].[dbo].[VNDR_FOLDR] x with (nolock) on w.VNDR_FOLDR_ID = x.VNDR_FOLDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_VNDR] y with (nolock) on x.VNDR_CD = y.VNDR_CD LEFT OUTER JOIN  
  
(SELECT DISTINCT bb.TRMTG_CD, bb.ORDR_ID, bb.VNDR_FOLDR_ID FROM [COWS].[dbo].[VNDR_ORDR] bb JOIN [COWS].[dbo].[ORDR] cc   
 on cc.ORDR_ID = bb.ORDR_ID WHERE TRMTG_CD = 1) z on b.ORDR_ID = z.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[VNDR_FOLDR] aa on z.VNDR_FOLDR_ID = aa.VNDR_FOLDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_VNDR] ab on aa.VNDR_CD = ab.VNDR_CD LEFT OUTER JOIN  
  
(SELECT ad.ORDR_ID, ac.SRVC_CKT_DES FROM [COWS].[dbo].[IPL_PROD_SRVC_CKT] ac join [COWS].[dbo].[IPL_ORDR] ad on ad.PROD_TYPE_ID = ac.PROD_TYPE_ID   
WHERE ad.SRVC_TYPE_CD = ac.SRVC_TYPE_CD and ad.CKT_TYPE_CD = ac.CKT_TYPE_CD) ds on t.ORDR_ID = ds.ORDR_ID LEFT OUTER JOIN  
   
  
(SELECT  DISTINCT bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID  
     FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
     LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
      WHERE   
      bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
    FROM (SELECT DISTINCT ASMT_DT, ORDR_ID   
     FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
    WHERE bb.ORDR_ID = usr.ORDR_ID )  
    AND bb.ASN_USER_ID = (SELECT  MAX(ASN_USER_ID)  
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
     WHERE bb.ORDR_ID = uas.ORDR_ID)   
   ) ufm ON b.ORDR_ID =  ufm.ORDR_ID LEFT OUTER JOIN     
  
[COWS].[dbo].[LK_USER] lu on ufm.ASN_USER_ID = lu.USER_ID LEFT OUTER JOIN  
  
(SELECT DISTINCT [FTN],[FMS_CKT_ID] ,[NUA_ADR] ,[PLN_NME]  
  FROM [COWS].[dbo].[NRM_CKT] ) nc on c.FTN = nc.FTN LEFT OUTER JOIN  
  
[COWS].[dbo].[H5_FOLDR] p on b.H5_FOLDR_ID = p.H5_FOLDR_ID LEFT OUTER JOIN  
[COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=p.H5_FOLDR_ID  AND csd2.SCRD_OBJ_TYPE_ID=6  LEFT OUTER JOIN  
[COWS].[dbo].[LK_CTRY] lc on p.CTRY_CD = lc.CTRY_CD LEFT OUTER JOIN  
[COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd2.CTRY_CD)) = lcs.CTRY_CD   LEFT JOIN 
(SELECT bb.ORDR_ID, SUM(CAST(bb.MRC_CHG_AMT AS MONEY)) MRC_CHG_AMT,   
  
      SUM(CAST (bb.NRC_CHG_AMT AS MONEY)) NRC_CHG_AMT   
  
      FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
  
      JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
  
      GROUP by bb.ORDR_ID) b5 on b.ORDR_ID = b5.ORDR_ID LEFT OUTER JOIN  
  
(SELECT ORDR_ID, CONVERT(VARCHAR(10),MIN(at.CREAT_DT),101) as [GOM Receives Order]  
  
      FROM [COWS].dbo.ACT_TASK at WITH (NOLOCK)  
  
      JOIN [COWS].dbo.MAP_GRP_TASK mgt WITH (NOLOCK) on mgt.TASK_ID=at.TASK_ID  
  
      WHERE mgt.GRP_ID=1 AND at.TASK_ID NOT IN (101,102)  
  
      GROUP BY ORDR_ID) gq ON c.ORDR_ID = gq.ORDR_ID    
  
WHERE  
c.ORDR_TYPE_CD = 'DC' AND b.RGN_ID = 2 -- ENCI Region  
  
CLOSE SYMMETRIC KEY FS@K3y   
  
END  
  