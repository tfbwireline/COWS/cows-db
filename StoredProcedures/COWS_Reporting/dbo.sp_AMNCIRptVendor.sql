USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_AMNCIRptVendor]    Script Date: 10/08/2018 20:03:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_AMNCIRptVendor]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_AMNCIRptVendor]
GO

USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_AMNCIRptVendor]    Script Date: 10/08/2018 20:03:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
-- =============================================  
-- Author:  MD M Monir 
-- Create date: 06282018
-- Description: AMNCI Report With Summary  
  
-- Updated By:   Md M Monir  
-- Updated Date: 06/28/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD  

-- Updated By:   km967761
-- Updated Date: 08/23/2018  
-- Updated Reason:
--		AVG SLA only counts OrderSubType IN ('Full Install', 'Access Upgrade', 'Downgrade')
--		Fixed CKT_MS SentToVendorDate and AccessAcceptedDate NULL values
-- Updated By:   Md M monir
-- Updated Date: 10/16/2018  
-- Updated Reason:
--              "Summary" Tab (Construction, Vendor Customer columns)  in the Current Report   was only count  
--              (full Install, access upgrade, access downgrade) 
--              Report is One monthly Only
-- Updated By:   vn370313
-- Updated Date: 09/06/2018 
-- Updated By:   vn370313
-- Updated Date: 09/19/2018 
-- Updated Reason:  Remove Distinct  from query3
-- Updated Date: 10/08/2018 
-- Updated Reason:  Changed Order received Date to Bill Clear date for Extracting Report Data
-- Updated By:   vn370313
-- Updated Date: 01/10/2019
-- Updated Reason:  "9  LEC Construction W/ Permits"
----EXEC RAD_Reporting.rad.sp_ReportRefreshQueue_NEW 'N','172'
--Select * from RAD_Reporting.rad.ReportProcessLog Order By LogDateTime DESC
--Select 	* from RAD_Reporting.[rad].[REPORTQUEUE_NEW] with (nolock) where ReportID IN(172,0)

-- Updated By:   vn370313
-- Updated Date: 01/28/2019
-- Updated Reason:  summary and Commitment Vs Delivery both tab Grouping Changed from Vendor Country to H5 Country
					So for the 2nd and 3rd Tab The @AMNCIVendor2  and @AMNCIVendor2 Temp Table Column 
					VendorCountry is going to carry H5Country
					
-- Updated By:   vn370313
-- Updated Date: 05/16/2019
-- Updated Reason: 	add Aladin, Mohammed
-- [dbo].[sp_AMNCIRptVendor] 1,0
-- [dbo].[sp_AMNCIRptVendor] 2,0 
-- [dbo].[sp_AMNCIRptVendor] 3,0
-- [dbo].[sp_AMNCIRptVendor] 1,1
-- [dbo].[sp_AMNCIRptVendor] 2,1
-- [dbo].[sp_AMNCIRptVendor] 3,1
-- [dbo].[sp_AMNCIRptVendor] 7,0
-- [dbo].[sp_AMNCIRptVendor] 7,1
-- =============================================  

*/
CREATE PROCEDURE [dbo].[sp_AMNCIRptVendor] 
  @QueryNumber		   INT,  
  @secured  BIT = 0  
  
  
AS  
BEGIN TRY  
--DECLARE @secured BIT=1 -- for testing purposes only  
  
  
    SET NOCOUNT ON;  
    --SET ANSI_WARNINGS OFF;
    DECLARE @startDate DATE  
	DECLARE @endDate DATE 
	--DECLARE @QueryNumber INT = 2
	--DECLARE @secured BIT = 0 

	SET @endDate = GETDATE()  
	SET @startDate = DATEADD(mm, -1, CONVERT(VARCHAR(25), DATEADD(DD, -(DAY(GETDATE()) - 1), GETDATE()), 101))  
    ---Last # Years Data Only As instructed by Pramod 06132018
	--SET @endDate = DATEADD(dd,-1,GETDATE())  
	--SET @startDate = DATEADD(mm, -1, CONVERT(VARCHAR(25), DATEADD(DD, -(DAY(GETDATE()) - 1), GETDATE()), 101))  

	Print @endDate  
	Print @startDate 
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   

--Drop Table #AMNCIVendor
--CREATE TABLE  #AMNCIVendor
	DECLARE @AMNCIVendor TABLE
	(
		FTNOrderID          varchar(50),
		CustomerName        varchar(MAX),
		CPMContact          varchar(100),
		SprintTargetDeliveryDate  smalldatetime,
		Product             varchar(100),
		CustomerCommitDate  smalldatetime,
		xNCIRegion          varchar(25),
		H5City              varchar(50),
		H5Country           varchar(50),
		VendorCountry       varchar(50),
		OrderStatus         varchar(25),
		CCDMissedReason     varchar(50),
		Manager             varchar(100),
		BillClearDate		smalldatetime,
		AccessSpeed         varchar(50),
		OrderType           varchar(100),
		OrderSubType        varchar(100),
		SenttoVendorDate	smalldatetime,
		PresubmitDate		smalldatetime,
		SubmitDate			smalldatetime,
		Vendor              varchar(50),
		AccessAcceptedDate  smalldatetime,
		AccessDeliveryDate  smalldatetime,
		TargetDeliveryDate  smalldatetime,
		STDIDATE			smalldatetime,
		STDIREASONID        varchar(50),
		STDICOMMENTS        varchar(500),
		CustomerSignDate    smalldatetime,
		STDI_REAS_ID        int

	) 
	DECLARE @AMNCIVendor2 TABLE
	(
		FTNOrderID          varchar(50),
		CustomerName        varchar(MAX),
		CPMContact          varchar(100),
		VendorCountry       varchar(50),    /*VendorCountry is going to carry H5Country New request 01282019*/
		OrderType           varchar(100),
		SenttoVendorDate	smalldatetime,
		Vendor              varchar(50),
		AccessAcceptedDate  smalldatetime,
		STDI_REAS_ID        int,
		TotalDays           int,
		OrderSubType		varchar(100)
    )
    DECLARE @AMNCIVendor3 TABLE
	(
			VendorName              varchar(50),
			VendorCountry			varchar(50), /*VendorCountry is going to carry H5Country New request 01282019*/
			STDIntvlSLA				int,
			Opportunities			int,
			Install					int,
			Disconnect				int,
			Change					int,
			MET						int,
			Missed					int,
			NoDate					int,
			Construction			int,
			Vendor					int,
			Customer				int,
			TotalDays               int,
			TotalNoOfOrders			int,
			OrderSubType			varchar(100),
			AVESLA					NUMERIC(10,2)
    )  
	DECLARE @AMNCIVendor4 TABLE
	(
		TotalInstall        int,
		TotDaysIntsall      NUMERIC(10,2),
		METInstallCount1    NUMERIC(10,2),
		METInstallCount2    NUMERIC(10,2),
		METInstallCount3    NUMERIC(10,2),
		MetConst1           NUMERIC(10,2),
		MetConst2           NUMERIC(10,2),
		MetConst3           NUMERIC(10,2),
		OrderType           varchar(100),
		NoDate              int
	)

	DECLARE @AMNCIVendor5 TABLE
	(
		[Country]   varchar(100),
		[Total_Vendor_Opportunities] NUMERIC(10,2),
		[Average_Install_SLA] NUMERIC(10,2),
		[%_Met_WO_Const_Cust] NUMERIC(10,2),
		[% Met_WO_Const_Vendor] NUMERIC(10,2),
		[% Met_WO_Const] NUMERIC(10,2),
		[Target] varchar(10)
	)
 INSERT INTO @AMNCIVendor
 --INSERT INTO #AMNCIVendor 
  
 SELECT DISTINCT  
		cast (ISNULL(i.FTN, ord.ORDR_ID) as varchar(20)) [FTN/Order ID],  

		CASE  
		WHEN @secured = 0 and ord.CSG_LVL_ID =0 THEN t.CUST_NME  
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 AND t.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd2.CUST_NME)) IS NULL THEN NULL   
		ELSE coalesce(t.CUST_NME,CONVERT(varchar, DecryptByKey(csd2.CUST_NME)),'') END [Customer Name], --Secured  
		CASE   
		WHEN ord.ORDR_CAT_ID = 1 THEN w.CLCM_NME  
		WHEN ord.ORDR_CAT_ID = 4 THEN w.CLCM_NME  
		WHEN ord.ORDR_CAT_ID = 5 THEN w.CLCM_NME  
		WHEN ord.ORDR_CAT_ID = 6 THEN g.FULL_NME  
		WHEN ord.ORDR_CAT_ID = 2 THEN g.FULL_NME END [CPM Contact],  
		CASE   
		WHEN ord.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), w.CUST_WANT_DT), 101)  
		WHEN ord.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), w.CUST_WANT_DT), 101)  
		WHEN ord.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), w.CUST_WANT_DT), 101)  
		WHEN ord.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), i.CUST_WANT_DT), 101)  
		WHEN ord.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), i.CUST_WANT_DT), 101)  END [Sprint Target Delivery Date],  
		CASE   
		WHEN ord.ORDR_CAT_ID = 1 THEN lp.PROD_TYPE_DES  
		WHEN ord.ORDR_CAT_ID = 4 THEN lp.PROD_TYPE_DES  
		WHEN ord.ORDR_CAT_ID = 5 THEN lp.PROD_TYPE_DES  
		WHEN ord.ORDR_CAT_ID = 6 THEN h.PROD_TYPE_DES  
		WHEN ord.ORDR_CAT_ID = 2 THEN h.PROD_TYPE_DES END [Product],  
		CASE   
		WHEN ord.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, w.CUST_CMMT_DT), 101)  
		WHEN ord.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, w.CUST_CMMT_DT), 101)  
		WHEN ord.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, w.CUST_CMMT_DT), 101)  
		WHEN ord.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, i.CUST_CMMT_DT), 101)  
		WHEN ord.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, i.CUST_CMMT_DT), 101) END [Customer Commit Date],  
		xr.RGN_DES [xNCI Region],  
		oa.CTY_NME [H5 City],  
		CASE WHEN (t.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE u.CTRY_NME END [H5 Country], /*Monir 06062018*/ 
		lcv.CTRY_NME as [Vendor Country], 
		o.ORDR_STUS_DES [Order Status],  
		q.CCD_MISSD_REAS_DES [CCD Missed Reason],  
		mg.FULL_NME [Manager],  
		CASE   
		WHEN tk.TASK_ID = 1001 and ord.ORDR_STUS_ID <> 1 then CONVERT(VARCHAR(10), tk.CREAT_DT, 101) END [Bill Clear Date],  	 
		CASE   
		WHEN ord.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), w.CKT_SPD_QTY, 101)  
		WHEN ord.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), w.CKT_SPD_QTY, 101)  
		WHEN ord.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), w.CKT_SPD_QTY, 101)  
		WHEN ord.ORDR_CAT_ID = 6 THEN i.TTRPT_SPD_OF_SRVC_BDWD_DES  
		WHEN ord.ORDR_CAT_ID = 2 THEN i.TTRPT_SPD_OF_SRVC_BDWD_DES END [Access Speed],  
		CASE   
		WHEN ord.ORDR_CAT_ID = 1 THEN lo.ORDR_TYPE_DES  
		WHEN ord.ORDR_CAT_ID = 4 THEN lo.ORDR_TYPE_DES  
		WHEN ord.ORDR_CAT_ID = 5 THEN lo.ORDR_TYPE_DES  
		WHEN ord.ORDR_CAT_ID = 6 THEN n.ORDR_TYPE_DES  
		WHEN ord.ORDR_CAT_ID = 2 THEN n.ORDR_TYPE_DES END [Order Type],  
		s.ORDR_SUB_TYPE_DES [Order Sub Type],  
		[Sent to Vendor Date]=ISNULL( (SELECT CONVERT(VARCHAR(10),MIN(voe.[SENT_DT]),101)  FROM [COWS].[dbo].[VNDR_ORDR_EMAIL] voe WITH (NOLOCK) WHERE vnd.VNDR_ORDR_ID = voe.VNDR_ORDR_ID) ,NULL)      ,   /*Change to MIN from MAX due to email of Aladin*/    
		CONVERT(VARCHAR(10), om.PRE_SBMT_DT, 101) [Pre-submit Date],  
		CONVERT(VARCHAR(10), om.SBMT_DT, 101) [Submit Date],  
		vl.VNDR_NME [Vendor],  
		CONVERT(VARCHAR(10), cm.ACCS_ACPTC_DT, 101) [Access Accepted Date],  
		CONVERT(VARCHAR(10), cm.ACCS_DLVRY_DT, 101) [Access Delivery Date],  
		CONVERT(VARCHAR(10), cm.TRGT_DLVRY_DT, 101) [Target Delivery Date],   
		osh.STDI_DT [STDI DATE],  
		osh.STDI_REAS_DES [STDI REASON ID],  
		osh.CMNT_TXT [STDI COMMENTS],    
		CONVERT(VARCHAR(10), i.CUST_SIGNED_DT, 101) [Customer Sign Date],
		osh.STDI_REAS_ID
		

 FROM  (SELECT * FROM [COWS].[dbo].[ORDR] WITH (NOLOCK)) ord   
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] i WITH (NOLOCK) ON ord.ORDR_ID = i.ORDR_ID   
		LEFT OUTER JOIN [COWS].[dbo].[IPL_ORDR] w WITH (NOLOCK) ON ord.ORDR_ID = w.ORDR_ID   
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_GOM_XNCI] a WITH (NOLOCK) ON ord.ORDR_ID = a.ORDR_ID   
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] b WITH (NOLOCK) ON ord.ORDR_ID = b.ORDR_ID   
 
		LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] h WITH (NOLOCK) ON i.PROD_TYPE_CD = h.FSA_PROD_TYPE_CD   

		LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID,   
		CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) [FRST_NME2],   
		CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [LST_NME2],   
		CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) + ' '+ CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [NME2],  
		CONVERT(varchar, DecryptByKey(csd2.CUST_EMAIL_ADR)) [EMAIL_ADR2],   

		bb.FRST_NME  as [FRST_NME],   
		bb.LST_NME   as [LST_NME],   
		bb.FRST_NME  + ' '+ bb.LST_NME   as [NME],  
		bb.EMAIL_ADR as [EMAIL_ADR],   
		bb.CNTCT_TYPE_ID   
		FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
		[COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID  
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd2.SCRD_OBJ_TYPE_ID=15   
		WHERE bb.ROLE_ID = 13  
		) tac on ord.ORDR_ID  = tac.ORDR_ID   

		LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID,   
		CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) [FRST_NME2],   
		CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [LST_NME2],   
		CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) + ' '+ CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [NME2],  
		CONVERT(varchar, DecryptByKey(csd2.CUST_EMAIL_ADR)) [EMAIL_ADR2],   

		bb.FRST_NME  as [FRST_NME],   
		bb.LST_NME   as [LST_NME],   
		bb.FRST_NME  + ' '+ bb.LST_NME   as [NME],  
		bb.EMAIL_ADR as [EMAIL_ADR],    
		bb.CNTCT_TYPE_ID   

		FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK) LEFT OUTER JOIN   
		[COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd2.SCRD_OBJ_TYPE_ID=15  
		WHERE bb.ROLE_ID = 11  
		) sis on ord.ORDR_ID  = sis.ORDR_ID   


		LEFT OUTER JOIN (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID   
			FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
			LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
			WHERE bb.ASN_USER_ID  = (SELECT MAX(ASN_USER_ID)  
									FROM (SELECT TOP 1 ASMT_DT, ORDR_ID, ASN_USER_ID   
									FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)
									WHERE ORDR_ID=bb.ORDR_ID
									GROUP BY ASMT_DT,ORDR_ID,ASN_USER_ID
									ORDER BY ASMT_DT DESC
									)uas  
									WHERE uas.ORDR_ID = bb.ORDR_ID )  
									) ur ON ur.ORDR_ID = i.ORDR_ID   
		LEFT OUTER JOIN [COWS].[dbo].[LK_USER] g WITH (NOLOCK) ON ur.ASN_USER_ID = g.USER_ID   
		LEFT OUTER JOIN [COWS].[dbo].[LK_USER] mg WITH (NOLOCK) ON g.MGR_ADID = mg.USER_ADID   
		LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] k WITH (NOLOCK) ON ord.ORDR_ID = k.ORDR_ID   
		LEFT OUTER JOIN [COWS].[dbo].[LK_SALS_CHNL] j WITH (NOLOCK) ON j.SALS_CHNL_ID = k.SALS_CHNL_ID   

		LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.STUS_ID, bb.CREAT_DT, bb.ORDR_ID, cc.ORDR_STUS_ID  
		FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
		LEFT OUTER JOIN [COWS].[dbo].[ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
		WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
		FROM (SELECT ACT_TASK_ID, ORDR_ID   
		FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
		WHERE bb.ORDR_ID = tsk.ORDR_ID  )) tk ON ord.ORDR_ID = tk.ORDR_ID  
		  
		LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_STUS] o WITH (NOLOCK) ON ord.ORDR_STUS_ID = o.ORDR_STUS_ID   
		LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] p WITH (NOLOCK) ON ord.ORDR_ID = p.ORDR_ID   

		LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.NEW_CCD_DT, bb.CCD_HIST_ID  
		FROM [COWS].[dbo].[CCD_HIST] bb LEFT OUTER JOIN  
		[COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID   
		WHERE bb.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)  
		FROM (SELECT CCD_HIST_ID, ORDR_ID FROM [COWS].[dbo].[CCD_HIST] WITH (NOLOCK) ) ccd  
		WHERE ccd.ORDR_ID = bb.ORDR_ID)) cd on  ord.ORDR_ID = cd.ORDR_ID   

		LEFT OUTER JOIN [COWS].[dbo].[CCD_HIST_REAS] cch WITH (NOLOCK) on cd.CCD_HIST_ID = cch.CCD_HIST_ID  
		LEFT OUTER JOIN [COWS].[dbo].[LK_CCD_MISSD_REAS] q WITH (NOLOCK) ON cch.CCD_MISSD_REAS_ID = q.CCD_MISSD_REAS_ID   
		LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] r WITH (NOLOCK) ON r.FTN = i.FTN   
		LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON  r.TOC_CTRY_CD = l.CTRY_CD  

		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] m WITH (NOLOCK) on i.ORDR_ID = m.ORDR_ID   

		LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] n WITH (NOLOCK) on i.ORDR_TYPE_CD = n.FSA_ORDR_TYPE_CD  
		LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_SUB_TYPE] s WITH (NOLOCK) on i.ORDR_SUB_TYPE_CD = s.ORDR_SUB_TYPE_CD  


		LEFT OUtER JOIN [COWS].[dbo].[H5_FOLDR] t WITH (NOLOCK) on ord.H5_FOLDR_ID = t.H5_FOLDR_ID  
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=t.H5_FOLDR_ID  AND csd2.SCRD_OBJ_TYPE_ID=6  
		LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] u  WITH (NOLOCK)on t.CTRY_CD = u.CTRY_CD  
		LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs WITH (NOLOCK) on CONVERT(varchar, DecryptByKey(csd2.CTRY_CD)) = lcs.CTRY_CD
		LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR] vnd WITH (NOLOCK) ON ord.ORDR_ID = vnd.ORDR_ID  
		LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] vf WITH (NOLOCK) ON vnd.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID  
		LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] vl WITH (NOLOCK) ON vf.VNDR_CD = vl.VNDR_CD  
        LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] lcv  WITH (NOLOCK) ON vf.CTRY_CD = lcv.CTRY_CD

		LEFT OUTER JOIN (SELECT ORDR_ID, MIN(at.CREAT_DT) as [GOM Receives Order]  
		FROM [COWS].dbo.ACT_TASK at WITH (NOLOCK)  
		JOIN [COWS].dbo.MAP_GRP_TASK mgt WITH (NOLOCK) on mgt.TASK_ID=at.TASK_ID  
		WHERE mgt.GRP_ID=1 AND at.TASK_ID NOT IN (101,102)  
		GROUP BY ORDR_ID) gq ON ord.ORDR_ID = gq.ORDR_ID  
		LEFT OUTER JOIN [COWS].[dbo].[LK_XNCI_RGN] xr WITH (NOLOCK) on ord.RGN_ID = xr.RGN_ID  


		LEFT OUTER JOIN COWS.dbo.ORDR_MS om  WITH (NOLOCK) on ord.ORDR_ID = om.ORDR_ID   

		LEFT OUTER JOIN (SELECT  DISTINCT aa.ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT] aa   
		JOIN [COWS].[dbo].[ORDR] bb on  aa.ORDR_ID = bb.ORDR_ID where  
		CKT_ID = (SELECT  MAX(CKT_ID) FROM (SELECT DISTINCT ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT]) ck      
		WHERE bb.ORDR_ID = ck.ORDR_ID)) ckt ON ord.ORDR_ID = ckt.ORDR_ID  


		LEFT OUTER JOIN (
			SELECT DISTINCT aa.CKT_ID, aa.ACCS_DLVRY_DT, aa.ACCS_ACPTC_DT, aa.TRGT_DLVRY_DT   
			FROM [COWS].[dbo].[CKT_MS] aa WITH (NOLOCK)
			WHERE
				VER_ID = (
					SELECT TOP 1 ck.VER_ID
					FROM [COWS].[dbo].[CKT_MS] ck WITH (NOLOCK)
					WHERE ck.CKT_ID = aa.CKT_ID
					ORDER BY ck.CKT_ID, ck.VER_ID DESC
				)
		) cm on ckt.CKT_ID = cm.CKT_ID   


		LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] lp  WITH (NOLOCK) on w.PROD_TYPE_ID = lp.PROD_TYPE_ID  
		LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] lo  WITH (NOLOCK) on w.ORDR_TYPE_ID = lo.ORDR_TYPE_ID  

		LEFT OUTER JOIN (
		SELECT DISTINCT ORDR_ID, ISNULL(bb.CTY_NME, CONVERT(VARCHAR, DecryptByKey(csd.CTY_NME))) [CTY_NME]  
		FROM [COWS].[dbo].[ORDR_ADR] bb WITH (NOLOCK) 
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.ORDR_ADR_ID  AND csd.SCRD_OBJ_TYPE_ID=14
		WHERE CIS_LVL_TYPE = 'H5'
		) oa ON oa.ORDR_ID = ord.ORDR_ID  
		 
		OUTER APPLY (SELECT Top 1 ORDR_ID, sh.STDI_DT,sr.STDI_REAS_DES, CMNT_TXT,sr.STDI_REAS_ID  FROM [COWS].[dbo].[ORDR_STDI_HIST] sh WITH (NOLOCK)  
		INNER JOIN [COWS].[dbo].[LK_STDI_REAS] sr WITH (NOLOCK) ON sh.STDI_REAS_ID = sr.STDI_REAS_ID  
		where ord.ORDR_ID = ORDR_ID   
		ORDER BY sh.CREAT_DT DESC 
		) osh    
     
	WHERE	--i.CREAT_DT BETWEEN @startDate AND @endDate AND							/*Changed Order Received Date to Bill Clear Date */
			ord.DMSTC_CD = 1 AND ord.RGN_ID = 1  
			AND tk.TASK_ID = 1001 and ord.ORDR_STUS_ID <> 1								/*Changed Order Received Date to Bill Clear Date */
			AND CONVERT(VARCHAR(10), tk.CREAT_DT, 101) BETWEEN @startDate AND @endDate  /*Changed Order Received Date to Bill Clear Date */
			AND i.ORDR_TYPE_CD IN ('IN','CH','DC') 
			AND(ord.ORDR_STUS_ID in (2,5) or tk.TASK_ID = 1000 or (tk.TASK_ID = 213 and tk.STUS_ID = 2) or tk.TASK_ID = 212 )  
			AND ord.H5_FOLDR_ID IS NOT NULL 
			AND ISNULL(W.PROD_TYPE_ID,0) NOT IN (9,27)
			AND h.PROD_TYPE_ID  NOT IN (9,27)
			AND i.PROD_TYPE_CD <>'CP'
			AND vl.VNDR_CD<>'RES-GCI'  ---vl.VNDR_NME     <>'GCI'
			AND ur.ASN_USER_ID NOT IN (124,7177,118)   -- CPM CONTACT 'Virginia LaJan Howard', Trisha Jensen,angelina pirandello
			--AND ISNULL(w.CLCM_NME,'') NOT IN ('Virginia LaJan Howard','Trisha Jensen','angelina pirandello')
			AND (ISNULL(g.FULL_NME,'') IN ('Darlene Franz','Netra Ghising','Susy Tobin','Xabier K Mugica','Rogelio Vazquez Juarez','Aladin H Mahmoud') OR 
				ISNULL(w.CLCM_NME,'')  IN ('Darlene Franz','Netra Ghising','Susy Tobin','Xabier K Mugica','Rogelio Vazquez Juarez','Aladin H Mahmoud') )
			--AND  cast (ISNULL(i.FTN, ord.ORDR_ID) as varchar(20))='DC3O11175255'  
			--AND (i.PROD_TYPE_CD NOT IN ('MN') OR i.PROD_TYPE_CD IS NULL )  
			 
	  
	ORDER BY cast (ISNULL(i.FTN, ord.ORDR_ID) as varchar(20))
    CLOSE SYMMETRIC KEY FS@K3y 
	--SELECT * FROM @AMNCIVendor	

 IF @QueryNumber=1
 BEGIN
		 SELECT
			[FTN/Order ID]=ISNULL(FTNOrderID ,'')     ,
			[Customer Name]=ISNULL(CustomerName ,'')        ,
			[CPM Contact]=ISNULL(CPMContact  ,'')         ,
			[Sprint Target Delivery Date]=ISNULL(CONVERT(VARCHAR(10),SprintTargetDeliveryDate ,101) ,'')  ,
			[Product]=ISNULL(Product ,'')             ,
			[Customer Commit Date]=ISNULL(CONVERT(VARCHAR(10),CustomerCommitDate ,101) ,'')   ,
			[xNCI Region]=ISNULL(xNCIRegion ,'')          ,
			[H5 City]=ISNULL(H5City ,'')              ,
			[H5 Country]=ISNULL(H5Country  ,'')          ,
			[Vendor Country]=ISNULL(VendorCountry ,'')       ,
			[Order Status]=ISNULL(OrderStatus ,'')         ,
			[CCD Missed Reason]=ISNULL(CCDMissedReason ,'')     ,
			[Manager]=ISNULL(Manager  ,'')            ,
			[Bill Clear Date]=ISNULL(CONVERT(VARCHAR(10),BillClearDate ,101) ,'') 		,
			[Access Speed]=ISNULL(AccessSpeed ,'')         ,
			[Order Type]=ISNULL(OrderType ,'')           ,
			[Order Sub Type]=ISNULL(OrderSubType  ,'')       ,
			[Sent to Vendor Date]=ISNULL(CONVERT(VARCHAR(10),SenttoVendorDate ,101) ,'') 	,
			[Presubmit Date]=ISNULL(CONVERT(VARCHAR(10),PresubmitDate ,101) ,'') 		,
			[Submit Date]=ISNULL(CONVERT(VARCHAR(10),SubmitDate ,101)  ,'')			,
			[Vendor]=ISNULL(Vendor  ,'')             ,
			[Access Accepted Date]=ISNULL(CONVERT(VARCHAR(10),AccessAcceptedDate ,101) ,'')   ,
			[Access Delivery Date]=ISNULL(CONVERT(VARCHAR(10),AccessDeliveryDate ,101) ,'')   ,
			[Target Delivery Date]=ISNULL(CONVERT(VARCHAR(10),TargetDeliveryDate ,101) ,'')   ,
			[STDI DATE]=ISNULL(STDIDATE ,'')			,
			[STDI REASON]=ISNULL(STDIREASONID  ,'')       ,
			[STDI COMMENTS]=ISNULL(STDICOMMENTS  ,'')       ,
			[Customer Sign Date]=ISNULL(CONVERT(VARCHAR(10),CustomerSignDate ,101),'')    
			--,STDI_REAS_ID        
		 FROM  @AMNCIVendor 
		 WHERE Product <>'CPE'  AND
		       Vendor  <>'GCI'
 END
 IF @QueryNumber=2
 BEGIN
		INSERT INTO  @AMNCIVendor2 (FTNOrderID,CustomerName,CPMContact,VendorCountry,OrderType,SenttoVendorDate,Vendor,AccessAcceptedDate,STDI_REAS_ID,TotalDays,OrderSubType)
		SELECT DISTINCT FTNOrderID,CustomerName,CPMContact,H5Country,OrderType,SenttoVendorDate,
					Vendor,AccessAcceptedDate,STDI_REAS_ID, 
					ISNULL(CASE OrderType WHEN 'Disconnect' THEN 0 ELSE Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate) END,0),
					OrderSubType
		FROM @AMNCIVendor  
        /*VendorCountry is going to carry H5Country New request 01282019*/
        --SELECT * FROM @AMNCIVendor2
        
        UPDATE @AMNCIVendor2  SET TotalDays=0 WHERE TotalDays iS NULL
		INSERT INTO  @AMNCIVendor3 (VendorName,VendorCountry,STDIntvlSLA,Opportunities,Install,Disconnect,Change,
									MET,Missed,NoDate,Construction,Vendor,Customer,TotalDays ,TotalNoOfOrders,OrderSubType)
		Select	VendorName=Vendor , 	
				VendorCountry  , 
				STDIntvlSLA=62 , 
				Opportunities=COUNT(1) ,
				Install=(Select COUNT(1) FROM @AMNCIVendor2 Where OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')and Vendor=My.Vendor and VendorCountry=My.VendorCountry  ),
				Disconnect=(Select COUNT(1) FROM @AMNCIVendor2 Where OrderType='Disconnect' and Vendor=My.Vendor  and VendorCountry=My.VendorCountry ),
				Change=(Select COUNT(1) FROM @AMNCIVendor2 Where OrderType='Change' and Vendor=My.Vendor and VendorCountry=My.VendorCountry ),
				MET=(Select COUNT(1) FROM @AMNCIVendor2 Where  Vendor=My.Vendor AND Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry ),
				Missed=(Select COUNT(1) FROM @AMNCIVendor2 Where  Vendor=My.Vendor AND Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)>62 and VendorCountry=My.VendorCountry ),
				NoDate=(Select COUNT(1) FROM @AMNCIVendor2 Where  Vendor=My.Vendor AND (AccessAcceptedDate is null OR SenttoVendorDate is null) and VendorCountry=My.VendorCountry ),
				Construction=(Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID in (10,9) and Vendor=My.Vendor and VendorCountry=My.VendorCountry AND OrderType IN ('Install') AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')), ---  "10 Lec Construction" "9  LEC Construction W/ Permits"
				Vendor=(Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID=11 and Vendor=My.Vendor and VendorCountry=My.VendorCountry AND OrderType IN ('Install') AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),  --- "Lec Delay"
				Customer=(Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID in(4,8) and Vendor=My.Vendor and VendorCountry=My.VendorCountry AND OrderType IN ('Install') AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),    --- "Customer Delay" / "Customer request CCD change"
				TotalDays=ISNULL((Select SUM(TotalDays) FROM @AMNCIVendor2 Where  Vendor=My.Vendor and VendorCountry=My.VendorCountry AND OrderType IN ('Install') AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') ),0),
				TotalNoOfOrders=ISNULL((Select COUNT(TotalDays) FROM @AMNCIVendor2 Where Vendor=My.Vendor and VendorCountry=My.VendorCountry AND OrderType IN ('Install') AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') ),0),
				OrderSubType=''
		 FROM	@AMNCIVendor2 My
		 WHERE  Vendor iS NOT NULL 
		 GROUP BY 
				My.Vendor,My.VendorCountry
		 ORDER BY 
				My.Vendor
		/*VendorCountry is going to carry H5Country New request 01282019*/	
		--SELECT * FROM @AMNCIVendor2
		--SELECT * FROM @AMNCIVendor3
		UPDATE  @AMNCIVendor3  SET  TotalDays=0  Where ISNULL(TotalDays ,0) <0
		UPDATE  @AMNCIVendor3  SET  TotalNoOfOrders=(Opportunities-NoDate), AVESLA=TotalDays/TotalNoOfOrders  WHERE TotalNoOfOrders >0
		UPDATE  @AMNCIVendor3  SET  AVESLA=0  WHERE AVESLA IS NULL
		--SELECT * FROM @AMNCIVendor3

		
		Select  VendorName              ,
				VendorCountry	AS 	H5Country	,
				[Standard Interval] =STDIntvlSLA				,
				Opportunities			,
				Install					,
				Disconnect				,
				Change					,
				MET						,
				Missed					,
				NoDate					,
				Construction			,
				Vendor					,
				Customer				,
				[AVG SLA]=AVESLA      
		from  
		@AMNCIVendor3
		/*VendorCountry is going to carry H5Country New request 01282019*/
 END  
 IF @QueryNumber=3
 BEGIN
		 Select CPMContact AS [CPM Contact],
				/*COUNT(DISTINCT FTNOrderID) AS [# of orders]*/  /* they want me to remove distinct*/
				COUNT(FTNOrderID) AS [# of orders]
		 FROM	@AMNCIVendor My
		 WHERE  CPMContact iS NOT NULL 
		 GROUP BY 
				CPMContact
 END  
 
 IF @QueryNumber=7
 BEGIN
 
		INSERT INTO  @AMNCIVendor2 (FTNOrderID,CustomerName,CPMContact,VendorCountry,OrderType,SenttoVendorDate,Vendor,AccessAcceptedDate,STDI_REAS_ID,TotalDays,OrderSubType)
		SELECT DISTINCT FTNOrderID,CustomerName,CPMContact,H5Country,OrderType,SenttoVendorDate,
					Vendor,AccessAcceptedDate,STDI_REAS_ID, 
					CASE OrderType WHEN 'Disconnect' THEN 0 ELSE Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate) END,OrderSubType
		FROM @AMNCIVendor  
		WHERE Vendor iS NOT NULL 
		--Select * FROM @AMNCIVendor2 Where  VendorCountry = 'CANADA' and OrderType='Install'
		/*VendorCountry is going to carry H5Country New request 01282019*/
		DELETE @AMNCIVendor4
		INSERT INTO  @AMNCIVendor4
		Select	TotalInstall=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') and VendorCountry=My.VendorCountry  ),0),
		        TotDaysIntsall=ISNULL((Select SUM(TotalDays) FROM @AMNCIVendor2 Where  VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),0),
		        METInstallCount1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where  Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') and STDI_REAS_ID NOT in(4,9,10)),0),
		        METInstallCount1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where  Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') and STDI_REAS_ID NOT in(9,10,11)),0),
		        METInstallCount1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where  Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') and STDI_REAS_ID NOT in(9,10)),0),
		        MetConst1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID NOT in(4,9,10) and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') ),0),
		        MetConst2=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID NOT in(9,10,11) and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') ),0),
		        MetConst3=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID NOT in(9,10) and VendorCountry=My.VendorCountry and OrderType='Install'  AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),0)
		        ,'INSTALL',
		        NoDate=(Select COUNT(1) FROM @AMNCIVendor2 Where   (AccessAcceptedDate is null OR SenttoVendorDate is null) and VendorCountry=My.VendorCountry  and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') )
		 FROM	@AMNCIVendor2 My
		 WHERE  VendorCountry='CANADA'  AND OrderType='INSTALL'
		 GROUP BY 
				My.VendorCountry
		--Select * from @AMNCIVendor4
				
		 INSERT INTO  @AMNCIVendor5
		 SELECT 'CANADA', 
				[Total Vendor Opportunities]=TotalInstall,
		        [Average Install SLA]= CASE TotalInstall WHEN 0 THEN 0 ELSE TotDaysIntsall/(TotalInstall-Nodate) END,
		        [% Met W/O Const/ Cust]=CASE  MetConst1 WHEN 0 THEN 0 ELSE (METInstallCount1/MetConst1)*100 END,
		        [% Met W/O Const/ Vendor]=CASE MetConst2 WHEN 0 THEN 0 ELSE (METInstallCount2/MetConst2)*100 END,
		        [% Met W/O Const]=CASE MetConst3 WHEN 0 THEN 0 ELSE (METInstallCount3/MetConst3)*100 END,
		        [Target]= '80%'
		 FROM   @AMNCIVendor4
		
		
		--Select * from @AMNCIVendor4
		DELETE @AMNCIVendor4
		INSERT INTO  @AMNCIVendor4
		Select	TotalInstall=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') and VendorCountry=My.VendorCountry  ),0),
		        TotDaysIntsall=ISNULL((Select SUM(TotalDays) FROM @AMNCIVendor2 Where  VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') ),0),
		        METInstallCount1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where  Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') and STDI_REAS_ID NOT in(4,9,10)),0),
		        METInstallCount1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where  Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') and STDI_REAS_ID NOT in(9,10,11)),0),
		        METInstallCount1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where  Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') and STDI_REAS_ID NOT in(9,10)),0),
		        MetConst1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID NOT in(4,9,10) and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),0),
		        MetConst2=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID NOT in(9,10,11) and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),0),
		        MetConst3=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID NOT in(9,10) and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),0)
		        ,'INSTALL',
		        NoDate=(Select COUNT(1) FROM @AMNCIVendor2 Where  (AccessAcceptedDate is null OR SenttoVendorDate is null) and VendorCountry=My.VendorCountry  and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') )
		 FROM	@AMNCIVendor2 My
		 WHERE  VendorCountry='MEXICO'  AND OrderType='INSTALL'
		 GROUP BY 
				My.VendorCountry
				
		 INSERT INTO  @AMNCIVendor5
		 SELECT 'MEXICO',
				[Total Vendor Opportunities]=TotalInstall,
		        [Average Install SLA]= CASE TotalInstall WHEN 0 THEN 0 ELSE TotDaysIntsall/(TotalInstall-Nodate) END,
		        [% Met W/O Const/ Cust]=CASE  MetConst1 WHEN 0 THEN 0 ELSE (METInstallCount1/MetConst1)*100 END,
		        [% Met W/O Const/ Vendor]=CASE MetConst2 WHEN 0 THEN 0 ELSE (METInstallCount2/MetConst2)*100 END,
		        [% Met W/O Const]=CASE MetConst3 WHEN 0 THEN 0 ELSE (METInstallCount3/MetConst3)*100 END,
		        [Target]= '80%'
		 FROM   @AMNCIVendor4
		 
		--Select * from @AMNCIVendor4
		DELETE @AMNCIVendor4
		INSERT INTO  @AMNCIVendor4
		Select	TotalInstall=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade') and VendorCountry=My.VendorCountry  ),0),
			TotDaysIntsall=ISNULL((Select SUM(TotalDays) FROM @AMNCIVendor2 Where  VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),0),
			METInstallCount1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where  Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')and STDI_REAS_ID NOT in(4,9,10)),0),
			METInstallCount1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where  Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')and STDI_REAS_ID NOT in(9,10,11)),0),
			METInstallCount1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where  Cows.dbo.CalculateBusinessDays(AccessAcceptedDate,SenttoVendorDate)<=62  and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')and STDI_REAS_ID NOT in(9,10)),0),
			MetConst1=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID NOT in(4,9,10) and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),0),
			MetConst2=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID NOT in(9,10,11) and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),0),
			MetConst3=ISNULL((Select COUNT(1) FROM @AMNCIVendor2 Where STDI_REAS_ID NOT in(9,10) and VendorCountry=My.VendorCountry and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade')),0)
			,'INSTALL',
			NoDate=(Select COUNT(1) FROM @AMNCIVendor2 Where   (AccessAcceptedDate is null OR SenttoVendorDate is null) and VendorCountry=My.VendorCountry  and OrderType='Install' AND OrderSubType IN ('Full Install', 'Access Upgrade', 'Access Downgrade'))
		FROM	@AMNCIVendor2 My
		WHERE  VendorCountry NOT IN ('CANADA','MEXICO')  AND OrderType='INSTALL'
		GROUP BY 
			My.VendorCountry
	
		--Select * from @AMNCIVendor2
		--Select * from @AMNCIVendor4
		INSERT INTO  @AMNCIVendor5
		SELECT 'Rest of South AMERICA',
			[Total Vendor Opportunities]=TotalInstall,
			[Average Install SLA]= CASE TotalInstall WHEN 0 THEN 0 ELSE TotDaysIntsall/(TotalInstall-Nodate) END,
			[% Met W/O Const/ Cust]=CASE  MetConst1 WHEN 0 THEN 0 ELSE (METInstallCount1/MetConst1)*100 END,
			[% Met W/O Const/ Vendor]=CASE MetConst2 WHEN 0 THEN 0 ELSE (METInstallCount2/MetConst2)*100 END,
			[% Met W/O Const]=CASE MetConst3 WHEN 0 THEN 0 ELSE (METInstallCount3/MetConst3)*100 END,
			[Target]= '80%'
		FROM   (
			 SELECT 
				SUM(TotalInstall ) AS TotalInstall        ,
				SUM(NoDate ) AS NoDate        ,
				SUM(TotDaysIntsall) AS TotDaysIntsall      ,
				SUM(METInstallCount1)  AS METInstallCount1   ,
				SUM(METInstallCount2)  AS METInstallCount2   ,
				SUM(METInstallCount3) AS METInstallCount3    ,
				SUM(MetConst1)  AS MetConst1          ,
				SUM(MetConst2)  AS MetConst2          ,
				SUM(MetConst3)  AS MetConst3          
			 FROM @AMNCIVendor4
			 GROUP BY OrderType
		) A

		SELECT  
		[Country]   ,
		[Total Vendor Opportunities]= CAST([Total_Vendor_Opportunities] AS int) ,
		[Average Install SLA]=[Average_Install_SLA],
		[% Met W/O Const/ Cust]=[%_Met_WO_Const_Cust] ,
		[% Met W/O Const/ Vendor]=[% Met_WO_Const_Vendor] ,
		[% Met W/O Const]=[% Met_WO_Const] ,
		[Target]
		
		FROM @AMNCIVendor5
 END   
  
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptVendor ' + ',' + CAST(@secured AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc   
 EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  
  

GO


