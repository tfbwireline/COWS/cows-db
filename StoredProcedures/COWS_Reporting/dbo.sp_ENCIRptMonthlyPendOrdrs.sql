USE [COWS_Reporting]
GO
/*
 --=============================================
 --Author:		<Author,,Name>
 --Create date: <Create Date,,>
 --Description:	<Description,,>
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
 ----EXEC COWS_Reporting.dbo.sp_ENCIRptMonthlyPendOrdrs 1
 
 --=============================================
 */
ALTER PROCEDURE [dbo].[sp_ENCIRptMonthlyPendOrdrs]
	    @secured			BIT = 0,
		@inStartDtStr  		VARCHAR(10)= '',
	    @inEndDtStr			VARCHAR(10)= ''

AS
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @startDate		DATETIME
	DECLARE @endDate		DATETIME
	DECLARE	@today			DATETIME
	DECLARE @dayOfWeek		INT
	
--DECLARE @secured		BIT = 0

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 


	SELECT DISTINCT 
	a.FTN [FTN],
	CASE
		WHEN @secured = 0 and ord.CSG_LVL_ID =0 THEN foc5.CUST_NME
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' 
		WHEN @secured = 0 and ord.CSG_LVL_ID >1 AND CONVERT(varchar, DecryptByKey(csd1.CUST_NME)) IS NULL THEN NULL 
		ELSE coalesce(foc5.CUST_NME,CONVERT(varchar, DecryptByKey(csd1.CUST_NME)),'') END [Sprint Customer Name], --Secured
	
	CASE
		WHEN @secured = 0 and ord.CSG_LVL_ID =0 THEN oa.CTY_NME
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' 
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd2.CTY_NME)) IS NULL THEN NULL 
		ELSE coalesce(oa.CTY_NME,CONVERT(varchar, DecryptByKey(csd2.CTY_NME)),'') END [City], --Secured

	lc.CTRY_NME [H6 Country],
	fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES [Speed of Service],
	lv.VNDR_NME [Vendor Name],
	cm.ACCS_ACPTC_DT [Access Accepted Date],
	cc.VNDR_MRC_IN_USD_AMT  [Vendor MRC $],
	cc.VNDR_NRC_IN_USD_AMT [Vendor NRC $],
	pfm.PLTFRM_NME [Platform],
	pt.PROD_TYPE_DES [Product],
	wa.ASMT_DT [Assign Date]
	
	
	
	
	FROM [COWS].[dbo].[ORDR] ord 
		INNER JOIN      [COWS].[dbo].[ORDR_ADR] oa WITH (NOLOCK) ON ord.ORDR_ID = oa.ORDR_ID AND oa.CIS_LVL_TYPE = ('H5')
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=oa.ORDR_ADR_ID  AND csd2.SCRD_OBJ_TYPE_ID=14
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] a WITH (NOLOCK) ON ord.ORDR_ID = a.ORDR_ID
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = a.ORDR_ID
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc5 WITH (NOLOCK) ON ord.ORDR_ID = foc5.ORDR_ID AND foc5.CIS_LVL_TYPE = 'H5'
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd1 WITH (NOLOCK) ON csd1.SCRD_OBJ_ID=foc5.FSA_ORDR_CUST_ID  AND csd1.SCRD_OBJ_TYPE_ID=5
		LEFT OUTER JOIN [COWS].[dbo].[CKT] ckt WITH (NOLOCK) on ckt.ORDR_ID = ord.ORDR_ID
		LEFT OUTER JOIN [COWS].[dbo].[CKT_COST] cc WITH (NOLOCK) on ckt.CKT_ID = cc.CKT_ID
					AND cc.VER_ID = (SELECT MAX(VER_ID) FROM [COWS].[dbo].[CKT_MS] cm1
									 WHERE ckt.CKT_ID =cm1.CKT_ID)
		LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] lc WITH (NOLOCK) ON oa.CTRY_CD = lc.CTRY_CD
		LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR] vo WITH (NOLOCK) ON vo.ORDR_ID = ord.ORDR_ID
		LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] vf WITH (NOLOCK) ON vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID
		LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] lv WITH (NOLOCK) ON vf.VNDR_CD = lv.VNDR_CD
		LEFT OUTER JOIN [COWS].[dbo].[CKT_MS] cm WITH (NOLOCK) ON cm.CKT_ID = cc.CKT_ID
					AND cm.VER_ID = (SELECT MAX(VER_ID) FROM [COWS].[dbo].[CKT_MS] c
									 WHERE ckt.CKT_ID =c.CKT_ID)
		LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] pt WITH (NOLOCK) ON pt.FSA_PROD_TYPE_CD = a.PROD_TYPE_CD
		LEFT OUTER JOIN [COWS].[dbo].[LK_PLTFRM] pfm WITH (NOLOCK) ON pfm.PLTFRM_CD = ord.PLTFRM_CD
		LEFT OUTER JOIN [COWS].[dbo].[USER_WFM_ASMT] wa WITH (NOLOCK)
							ON wa.ORDR_ID = ord.ORDR_ID AND wa.GRP_ID = 7
							AND ASMT_DT = (SELECT MAX(ASMT_DT) FROM [COWS].[dbo].[USER_WFM_ASMT] w WITH (NOLOCK)
											WHERE w.GRP_ID = 7 and w.ORDR_ID = ord.ORDR_ID)  	
		
	WHERE ord.ORDR_STUS_ID = 1 
		AND a.ORDR_ACTN_ID = 2
		AND ord.RGN_ID = 2
	
	ORDER BY a.FTN	
							
	CLOSE SYMMETRIC KEY FS@K3y 
						
	RETURN 0;
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_ENCIRptMonthlyPendOrdrs '  + ',' + CAST(@secured AS VARCHAR(4)) + ': '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH





