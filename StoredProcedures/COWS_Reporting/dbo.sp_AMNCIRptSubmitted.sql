-- =============================================    
-- Author: < Author, , Name >    
-- Create date: < Create Date, , >    
-- Description: < Description, , >    
-- dlp0289 8 / 21 / 13: added PRS Quote Number to end of select.    
-- EXEC COWS_Reporting.dbo.sp_AMNCIRptSubmitted    
-- Modificatios:    
-- ci554013 10 / 21 / 2013 - changed report to back only 13 months and    
-- commented out the code that restricted output by MGR_ADID    
-- ci554013 01 / 21 / 2014 - Added 3 columns: [Order Received Date], [STDI Date], [H5 / H6 / ES City Name]    
-- ci554013 01 / 23 / 2014 - Changed date format so pivot table can sort    
-- km967761 06 / 26 / 2017 - Changed joined table 'vd' to select only the latest VER_ID    
-- vn370313 08 / 07 / 2017 - Added field  TTRPT_ACCS_TYPE_CD  from FSA_ORDR    
-- vn370313 11 / 03 / 2017 - Howard, Lajan L [CTO]  User Request and Pramod    Sent: Thursday, November 02, 2017 9:51 AM    
/*       Could we pull the �Updated CCD� into the report as well    
       Please add     
       Access City/Site Code,   ACCS_CTY_SITE_ID    dbo.TRPT_ORDR.    
       and from the Prequal Data we would like to pull in as many of these     
       Prequal fields as possible.  At a minimum we need     
       Prequal Site ID,    TPORT_PREQUAL_SITE_ID   FSA_ORDR    
       Prequal Line Item ID, and  TPORT_PREQUAL_LINE_ITEM_ID  FSA_ORDR    
       Vendor Raw MRC and    TPORT_VNDR_RAW_MRC_IN_CUR_AMT FSA_ORDR    
       Vendor Raw NRC, and    TPORT_VNDR_RAW_NRC_IN_CUR_AMT FSA_ORDR    
       Calculated Rate USD MRC and  TPORT_CALC_RT_MRC_USD_AMT  FSA_ORDR    
       Calculated Rate USD NRC.  TPORT_CALC_RT_NRC_USD_AMT  FSA_ORDR    
--vn370313 12112017       -  ACCESS city / Site Code should be name not Code   so i use look up table K1.ACCS_CTY_NME_SITE_CD    
--vn370313 12132017        Howard lajan    
         You wanted ADD       
       1.  H5/H6 ID    
       2.  Mach5 Bill Activation    
       3.  Vendor Sent Date    
       4.  Access Delivery Date   ( It is already in the report)    
       Modify    
       And you are saying    Sprint Target Delivery Date   not picking up for some FTN/Orders    
--vn370313 12152017   Vendor Sent Date and Target Delivery Date  Corrected 
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME  
-- Updated Reason: For TTRPT_ACCS_Type_CD, use the record which has line_item_cd='ACS'
--                 For TTRPT_SPD_OFSRVC_BDWD_DES, use the record with line_item_cd='PRT'  
  [dbo].[sp_AMNCIRptSubmitted]  1  
*/      
-- =============================================    
ALTER PROCEDURE [dbo].[sp_AMNCIRptSubmitted]    
 @secured BIT = 0    
    
  AS    
  BEGIN TRY    
  SET NOCOUNT ON;    
  --DECLARE @Interval char(1) = 'M' /* Not being used at this time */    
  DECLARE @startDate DATE    
  DECLARE @endDate DATE    
    
  SET @endDate = GETDATE()    
  SET @startDate = DATEADD(mm, -13, CONVERT(VARCHAR(25), DATEADD(DD, -(DAY(GETDATE()) - 1), GETDATE()), 101))    
  /*    
  SET @endDate = DATEADD(dd,-15,GETDATE())    
  SET @startDate = DATEADD(mm, -13, CONVERT(VARCHAR(25), DATEADD(DD, -(DAY(GETDATE()) - 1), GETDATE()), 101))    
      
  Print @endDate    
  Print @startDate    
  */    
       
  OPEN SYMMETRIC KEY FS@K3y    
  DECRYPTION BY CERTIFICATE S3cFS@CustInf0;    
    
    
  SELECT DISTINCT    
   i.FTN[FTN],    
      ---si.CUST_NME[Customer Name],   
      CASE  
  WHEN @secured = 0 and z.CSG_LVL_ID =0 THEN si.CUST_NME  
  WHEN @secured = 0 and z.CSG_LVL_ID >0 AND  si.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN @secured = 0 and z.CSG_LVL_ID >0 AND  si.CUST_NME2 IS NULL THEN ''   
  ELSE COALESCE(si.CUST_NME,si.CUST_NME2,'')   
  END  
  AS [Customer Name],   
      CONVERT(VARCHAR(10), tk.CREAT_DT, 101)[Installed Date],    
      --tac.FRST_NME + ' ' + tac.LST_NME[Program Manager],  
      --sis.FRST_NME + ' ' + sis.LST_NME[RAM],   
      CASE  
  WHEN @secured = 0 and z.CSG_LVL_ID =0 THEN tac.FRST_NME + ' ' + tac.LST_NME  
  WHEN @secured = 0 and z.CSG_LVL_ID >0 AND  tac.FRST_NME2 IS NOT NULL THEN 'PRIVATE DATA'   
  WHEN @secured = 0 and z.CSG_LVL_ID >0 AND  tac.FRST_NME2 IS NULL THEN ''   
  ELSE COALESCE(tac.FRST_NME + ' ' + tac.LST_NME,tac.FRST_NME2 + ' ' + tac.LST_NME2,'')   
  END  
  AS [Program Manager],  
   CASE  
  WHEN @secured = 0 and z.CSG_LVL_ID =0 THEN sis.FRST_NME + ' ' + sis.LST_NME  
  WHEN @secured = 0 and z.CSG_LVL_ID >0 AND  sis.FRST_NME2 IS NOT NULL THEN 'PRIVATE DATA'   
  WHEN @secured = 0 and z.CSG_LVL_ID >0 AND  sis.FRST_NME2 IS NULL THEN ''   
  ELSE COALESCE(sis.FRST_NME + ' ' + sis.LST_NME,sis.FRST_NME2 + ' ' + sis.LST_NME2,'')   
  END  
  AS [RAM],     
         
      g.FULL_NME[CPM],    
      /*c.VLDTD_DT[Sprint Target Delivery Date],*/    
      ac.TARGET_DLVRY_DT [Sprint Target Delivery Date],    
      h.PROD_TYPE_DES[Product],    
      CONVERT(VARCHAR(10), i.CUST_CMMT_DT, 101)[Customer Commit Date],    
      x.RGN_DES[XNCI Region],    
      --u.CTRY_NME[H5 Country], 
      CASE WHEN (t.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE u.CTRY_NME END [H5 Country], /*Monir 06062018*/     
      o.ORDR_STUS_DES[Order Status],    
      mg.FULL_NME[Manager],    
      CASE    
  WHEN cd.NEW_CCD_DT <= i.CUST_CMMT_DT THEN 1    
  WHEN cd.NEW_CCD_DT > i.CUST_CMMT_DT THEN 0    
  ELSE 0 END[CCD Met#],    
      CASE    
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 1    
  THEN CONVERT(VARCHAR(10), ISNULL(gq.[GOM Receives Order], i.CREAT_DT) + 3, 101)    
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 2    
  THEN CONVERT(VARCHAR(10), ISNULL(gq.[GOM Receives Order], i.CREAT_DT) + 2, 101)    
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 3    
  THEN CONVERT(VARCHAR(10), ISNULL(gq.[GOM Receives Order], i.CREAT_DT) + 1, 101)    
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 5    
  THEN CONVERT(VARCHAR(10), ISNULL(gq.[GOM Receives Order], i.CREAT_DT) - 1, 101)    
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 6    
  THEN CONVERT(VARCHAR(10), ISNULL(gq.[GOM Receives Order], i.CREAT_DT) - 2, 101)    
  WHEN DATEPART(DW, ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) = 7    
  THEN CONVERT(VARCHAR(10), ISNULL(gq.[GOM Receives Order], i.CREAT_DT) - 3, 101)    
  ELSE CONVERT(VARCHAR(10), ISNULL(gq.[GOM Receives Order], i.CREAT_DT), 101) END[Week],    
      /* Changed date format so pivot table can sort*/    
      CONVERT(DATE, DATEADD(dd, -(DAY(ISNULL(gq.[GOM Receives Order], i.CREAT_DT)) - 1), ISNULL(gq.[GOM Receives Order], i.CREAT_DT)), 101)[Month],    
      h.PROD_NME[Product2],    
      CASE    
  WHEN tk.TASK_ID = 1001 then tk.CREAT_DT END[Bill Clear Date],    
      /*i.CUST_WANT_DT[CWD], */   
      fcpl2.TTRPT_SPD_OF_SRVC_BDWD_DES[Access Speed],    
      n.ORDR_TYPE_DES[Order Type],    
      s.ORDR_SUB_TYPE_DES[Order Sub Type],    
      ac.ACCS_ACPTD_DT[Access Accepted Date],    
      ac.ACCS_DLVRY_DT[Access Delivery Date],    
      vl.VNDR_NME[Vendor],    
      1[CCD Met All],    
      b5.MRC_CHG_AMT[MRC],    
      b5.NRC_CHG_AMT[NRC],    
      vd.ACCS_CUST_MRC[Access Customer MRC],    
      vd.ACCS_CUST_NRC[Access Customer NRC],    
      vd.ASR_MRC[ASR MRC],    
      vd.ASR_NRC[ASR NRC],    
      vd.VNDR_MRC[Vendor MRC],    
      vd.VNDR_NRC[Vendor NRC],    
      ISNULL(gq.[GOM Receives Order], i.CREAT_DT)[GOM Receives Order],    
      ISNULL(i.TSUP_PRS_QOT_NBR, '')[PRS Quote Number],    
      z.CREAT_DT[Order Received Date],    
      sh.STDI_DT[STDI Date],   
  /*   
  CASE    
  WHEN @Secured = 0 AND z.SCURD_CD = 1 AND CONVERT(VARCHAR(100), DecryptByKey(oa.CTY_NME)) is NOT NULL    
  THEN 'Private Customer'    
  ELSE ISNULL(CONVERT(VARCHAR(100), DecryptByKey(oa.CTY_NME)), '')   
  */  
  CASE    
    WHEN @Secured = 0 AND oaa.CTY_NME<>''  THEN coalesce(oaa.CTY_NME,'PRIVATE CITY')  
    WHEN @Secured = 0 AND oaa.CTY_NME =''  THEN 'PRIVATE CITY'  
    WHEN @Secured = 0 AND oaa.CTY_NME IS NULL  THEN 'PRIVATE CITY'      
    ELSE coalesce(oaa.CTY_NME2,oaa.CTY_NME,'')      
  END        as [H5 / H6 / ES City Name],      
  fcpl.TTRPT_ACCS_TYPE_CD    as [ACCESS TYPE CODE],    
  CONVERT(VARCHAR(10), z.CUST_CMMT_DT,101)     as [Updated CCD],    
  K1.ACCS_CTY_NME_SITE_CD   as [Access City/Site Code],    
  i.TPORT_PREQUAL_SITE_ID   as [Prequal Site ID],    
  i.TPORT_PREQUAL_LINE_ITEM_ID  as [Prequal Line Item ID],    
  i.TPORT_VNDR_RAW_MRC_QOT_CUR_AMT as [Vendor Raw MRC],    
  i.TPORT_VNDR_RAW_NRC_QOT_CUR_AMT as [Vendor Raw NRC],    
  i.TPORT_CALC_RT_MRC_USD_AMT  as [Calculated Rate USD MRC],    
  i.TPORT_CALC_RT_NRC_USD_AMT  as [Calculated Rate USD NRC],    
  z.[H5_H6_CUST_ID]     as [H5/H6 ID],    
  [Vendor Sent Date]=ISNULL( (SELECT CONVERT(VARCHAR(10),MAX(voe.[SENT_DT]),101)  FROM [COWS].[dbo].[VNDR_ORDR_EMAIL] voe WITH (NOLOCK) WHERE vnd.VNDR_ORDR_ID = voe.VNDR_ORDR_ID) ,'')      ,    
  [Mach5 Bill Activation]= ISNULL((SELECT top 1 CONVERT(VARCHAR(10), msg.SENT_DT, 101) FROM  COWS.dbo.m5_ordr_msg  msg with (nolock) Where z.ORDR_ID=msg.ORDR_ID ),'')    
  FROM[COWS].[dbo].[ORDR] z WITH(NOLOCK)    
  LEFT OUTER JOIN[COWS].[dbo].[FSA_ORDR] i WITH(NOLOCK) ON z.ORDR_ID = i.ORDR_ID   
  --LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = i.ORDR_ID
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock)  on fcpl.ORDR_ID = i.ORDR_ID AND fcpl.line_item_cd='ACS'
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl2 with (nolock) on fcpl2.ORDR_ID = i.ORDR_ID AND fcpl2.line_item_cd='PRT'
	   
  LEFT OUTER JOIN[COWS].[dbo].[IPL_ORDR] w WITH(NOLOCK) ON z.ORDR_ID = w.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[FSA_ORDR_GOM_XNCI] a WITH(NOLOCK) ON z.ORDR_ID = a.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[FSA_ORDR_CUST] b WITH(NOLOCK) ON z.ORDR_ID = b.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[LK_PROD_TYPE] d WITH(NOLOCK) ON i.PROD_TYPE_CD = d.FSA_PROD_TYPE_CD    
  LEFT OUTER JOIN[COWS].[dbo].[ORDR_MS] c WITH(NOLOCK) ON z.ORDR_ID = c.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[LK_PROD_TYPE] h WITH(NOLOCK) ON i.PROD_TYPE_CD = h.FSA_PROD_TYPE_CD    
    
  LEFT OUTER JOIN  
   (SELECT DISTINCT   
          bb.ORDR_ID, bb.ROLE_ID,   
    bb.FRST_NME as [FRST_NME],    
    bb.LST_NME as [LST_NME],   
    bb.EMAIL_ADR as [EMAIL_ADR],   
    CONVERT(varchar, DecryptByKey(csd.FRST_NME))[FRST_NME2],    
    CONVERT(varchar, DecryptByKey(csd.LST_NME))[LST_NME2],   
    CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR))[EMAIL_ADR2],   
    bb.CNTCT_TYPE_ID   
      FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN[COWS].[dbo].[FSA_ORDR] cc WITH(NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
      LEFT OUTER JOIN [COWS].[dbo].[CUST_SCRD_DATA] csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15  
      WHERE bb.ROLE_ID = 13) tac on z.ORDR_ID = tac.ORDR_ID    
    
  LEFT OUTER JOIN  
     (SELECT DISTINCT  
    bb.ORDR_ID, bb.ROLE_ID,   
    bb.FRST_NME as [FRST_NME],    
    bb.LST_NME as [LST_NME],   
    bb.EMAIL_ADR as [EMAIL_ADR],   
    CONVERT(varchar, DecryptByKey(csd.FRST_NME))[FRST_NME2],    
    CONVERT(varchar, DecryptByKey(csd.LST_NME))[LST_NME2],   
    CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR))[EMAIL_ADR2],    
    bb.CNTCT_TYPE_ID   
      FROM[COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH(NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
      LEFT OUTER JOIN [COWS].[dbo].[CUST_SCRD_DATA] csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15  
      WHERE bb.ROLE_ID = 11) sis on z.ORDR_ID = sis.ORDR_ID    
    
  LEFT OUTER JOIN(SELECT ORDR_ID, MIN(at.CREAT_DT) as[GOM Receives Order]   
   FROM[COWS].dbo.ACT_TASK at WITH(NOLOCK) JOIN [COWS].dbo.MAP_GRP_TASK mgt WITH(NOLOCK) on mgt.TASK_ID = at.TASK_ID   
   WHERE mgt.GRP_ID = 1 AND at.TASK_ID NOT IN(101, 102)   
      GROUP BY ORDR_ID) gq ON gq.ORDR_ID = a.ORDR_ID    
    
  LEFT OUTER JOIN(SELECT bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID FROM[COWS].[dbo].[USER_WFM_ASMT] bb WITH(NOLOCK) LEFT OUTER JOIN[COWS].[dbo].[FSA_ORDR] cc WITH(NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT) FROM(SELECT DISTINCT
  
    ASMT_DT, ORDR_ID FROM[COWS].[dbo].[USER_WFM_ASMT] WITH(NOLOCK)) usr WHERE usr.ORDR_ID = bb.ORDR_ID) AND bb.ASN_USER_ID IN(SELECT ASN_USER_ID FROM(SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID FROM[COWS].[dbo].[USER_WFM_ASMT] WITH(NOLOCK)) uas   
    WHERE uas.ORDR_ID = bb.ORDR_ID)) ur ON ur.ORDR_ID = i.ORDR_ID    
  LEFT JOIN [COWS].[dbo].[LK_USER] g WITH(NOLOCK) ON ur.ASN_USER_ID = g.USER_ID    
  JOIN [COWS].[dbo].[LK_USER] mg WITH(NOLOCK) ON g.MGR_ADID = mg.USER_ADID     
  LEFT OUTER JOIN[COWS].[dbo].[TRPT_ORDR] k WITH(NOLOCK) ON z.ORDR_ID = k.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[LK_ACCS_CTY_SITE] k1 WITH(NOLOCK) ON K.[ACCS_CTY_SITE_ID] = K1.[ACCS_CTY_SITE_ID]    
  LEFT OUTER JOIN[COWS].[dbo].[LK_SALS_CHNL] j WITH(NOLOCK) ON j.SALS_CHNL_ID = k.SALS_CHNL_ID    
  LEFT OUTER JOIN(SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.STUS_ID, bb.CREAT_DT, bb.ORDR_ID FROM[COWS].[dbo].[ACT_TASK] bb WITH(NOLOCK) LEFT OUTER JOIN[COWS].[dbo].[FSA_ORDR] cc WITH(NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID) FROM(SELECT ACT_TASK_ID, ORDR_ID FROM[COWS].[dbo].[ACT_TASK] WITH(NOLOCK)) tsk WHERE tsk.ORDR_ID = bb.ORDR_ID AND bb.TASK_ID = 1001)) tk ON tk.ORDR_ID = i.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[LK_ORDR_STUS] o WITH(NOLOCK) ON z.ORDR_STUS_ID = o.ORDR_STUS_ID    
  LEFT OUTER JOIN[COWS].[dbo].[TRPT_ORDR] p WITH(NOLOCK) ON p.ORDR_ID = a.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[LK_CCD_MISSD_REAS] q WITH(NOLOCK) ON p.CCD_MISSD_REAS_ID = q.CCD_MISSD_REAS_ID    
  LEFT OUTER JOIN[COWS].[dbo].[NRM_CKT] r WITH(NOLOCK) ON r.FTN = i.FTN    
  LEFT OUTER JOIN[COWS].[dbo].[LK_CTRY] l WITH(NOLOCK) ON r.TOC_CTRY_CD = l.CTRY_CD    
    
  LEFT OUTER JOIN  
 ( SELECT DISTINCT bb.ORDR_ID, bb.SOI_CD,bb.CUST_NME as [CUST_NME] , CONVERT(varchar, DecryptByKey(csd.CUST_NME))[CUST_NME2]   
  FROM[COWS].[dbo].[FSA_ORDR_CUST] bb WITH(NOLOCK) LEFT OUTER JOIN[COWS].[dbo].[FSA_ORDR] cc WITH(NOLOCK) ON   
  bb.ORDR_ID = cc.ORDR_ID   
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
  WHERE bb.SOI_CD = (SELECT MAX(SOI_CD) FROM(SELECT SOI_CD, ORDR_ID   
       FROM[COWS].[dbo].[FSA_ORDR_CUST] WITH(NOLOCK)) soi   
       WHERE soi.ORDR_ID = bb.ORDR_ID)) si ON Z.ORDR_ID = si.ORDR_ID    
         
  LEFT OUTER JOIN[COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] m on i.ORDR_ID = m.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[LK_ORDR_TYPE] n on i.ORDR_TYPE_CD = n.FSA_ORDR_TYPE_CD    
  LEFT OUTER JOIN[COWS].[dbo].[LK_ORDR_SUB_TYPE] s on i.ORDR_SUB_TYPE_CD = s.ORDR_SUB_TYPE_CD    
  LEFT OUTER JOIN[COWS].[dbo].[H5_FOLDR] t on z.H5_FOLDR_ID = t.H5_FOLDR_ID
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=t.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6  
  LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD    
  LEFT OUTER JOIN[COWS].[dbo].[LK_CTRY] u on t.CTRY_CD = u.CTRY_CD    
  LEFT OUTER JOIN(SELECT bb.ORDR_ID, bb.NEW_CCD_DT, bb.CCD_HIST_ID FROM[COWS].[dbo].[CCD_HIST] bb LEFT OUTER JOIN  
  [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID WHERE bb.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)   
  FROM(SELECT CCD_HIST_ID, ORDR_ID FROM[COWS].[dbo].[CCD_HIST]) ccd WHERE ccd.ORDR_ID = bb.ORDR_ID)) cd on z.ORDR_ID = cd.ORDR_ID    
  LEFT OUTER JOIN(SELECT bb.ORDR_ID, SUM(CAST(bb.MRC_CHG_AMT AS MONEY)) MRC_CHG_AMT, SUM(CAST(bb.NRC_CHG_AMT AS MONEY)) NRC_CHG_AMT FROM[COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH(NOLOCK) JOIN[COWS].dbo.[ORDR] cc WITH(NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID
  
 GROUP by bb.ORDR_ID) b5 on b.ORDR_ID = b5.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[VNDR_ORDR] vnd WITH(NOLOCK) ON b.ORDR_ID = vnd.ORDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[VNDR_FOLDR] vf WITH(NOLOCK) ON vnd.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID    
  LEFT OUTER JOIN[COWS].[dbo].[LK_VNDR] vl WITH(NOLOCK) ON vf.VNDR_CD = vl.VNDR_CD    
  LEFT OUTER JOIN[COWS].[dbo].[LK_XNCI_RGN] x WITH(NOLOCK) ON z.RGN_ID = x.RGN_ID    
  LEFT OUTER JOIN(SELECT bb.[ORDR_ID], MIN(aa.ACCS_DLVRY_DT) ACCS_DLVRY_DT, MIN(aa.ACCS_ACPTC_DT) ACCS_ACPTD_DT, MAX(aa.TRGT_DLVRY_DT) TARGET_DLVRY_DT FROM[COWS].[dbo].[CKT_MS] aa JOIN COWS.dbo.CKT bb on aa.CKT_ID = bb.CKT_ID JOIN[COWS].[dbo].[ORDR] cc on
  
 bb.ORDR_ID = CC.ORDR_ID GROUP BY bb.ORDR_ID) ac on b.ORDR_ID = ac.ORDR_ID    
  LEFT OUTER JOIN(SELECT bb.[ORDR_ID], SUM(CAST([VNDR_MRC_IN_USD_AMT] AS MONEY)) as VNDR_MRC, SUM(CAST([VNDR_NRC_IN_USD_AMT] AS MONEY)) AS VNDR_NRC, SUM(CAST(aa.ACCS_CUST_MRC_IN_USD_AMT AS MONEY)) ACCS_CUST_MRC,    
      SUM(CAST(aa.ACCS_CUST_NRC_IN_USD_AMT AS MONEY)) ACCS_CUST_NRC,    
      SUM(CAST(aa.ASR_MRC_IN_USD_AMT AS MONEY)) ASR_MRC, SUM(CAST(aa.ASR_NRC_IN_USD_AMT AS MONEY)) ASR_NRC FROM[COWS].[dbo].[CKT_COST] aa JOIN COWS.dbo.CKT bb on aa.CKT_ID = bb.CKT_ID JOIN[COWS].[dbo].[ORDR] cc on bb.ORDR_ID = CC.ORDR_ID JOIN(    
          SELECT bb.[ORDR_ID],    
          aa.CKT_ID,    
          MAX(aa.VER_ID) AS LATEST_VER_ID FROM[COWS].[dbo].[CKT_COST] aa JOIN COWS.dbo.CKT bb on aa.CKT_ID = bb.CKT_ID GROUP by bb.ORDR_ID, aa.CKT_ID    
      ) dd ON dd.ORDR_ID = cc.ORDR_ID AND dd.CKT_ID = bb.CKT_ID AND dd.LATEST_VER_ID = aa.VER_ID GROUP by bb.ORDR_ID) vd on b.ORDR_ID = vd.ORDR_ID    
  LEFT JOIN   
   (SELECT DISTINCT oa.CTY_NME,CONVERT(varchar, DecryptByKey(csd.CTY_NME)) as CTY_NME2,  oa.ORDR_ID, oa.cis_lvl_type  FROM COWS.dbo.ORDR_ADR oa with(nolock)   
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oa.ORDR_ADR_ID  AND csd.SCRD_OBJ_TYPE_ID=14   
    WHERE oa.cis_lvl_type in ('h5', 'h6')   
    )oaa ON z.ORDR_ID = oaa.ORDR_ID   
  LEFT JOIN(SELECT aa.ORDR_ID[ORDR_ID], aa.STDI_DT, ROW_NUMBER() OVER(PARTITION BY aa.ORDR_ID ORDER BY aa.CREAT_DT DESC) AS ROWNUM FROM[COWS].[dbo].[ORDR_STDI_HIST] aa) sh on z.ordr_id = sh.ordr_id AND   
  ROWNUM = 1    
  WHERE Z.RGN_ID = 1 AND i.CREAT_DT BETWEEN @startDate AND @endDate   /*AND i.FTN IN('IN3NVJ142025','IN3NVU148435')*/    
  
  ORDER BY i.ftn 
    
    
  CLOSE SYMMETRIC KEY FS@K3y    
    
  RETURN 0;    
    
  END TRY    
    
  BEGIN CATCH    
  DECLARE @Desc VARCHAR(200)    
  SET @Desc = 'EXEC COWS_Reporting.dbo.sp_AMNCIRptSubmitted ' + ': '    
  SET @Desc = @Desc    
  EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;    
  RETURN 1;    
  END CATCH    
    