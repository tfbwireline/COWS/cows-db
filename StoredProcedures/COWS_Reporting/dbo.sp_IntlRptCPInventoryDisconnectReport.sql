--==================================================================================  
-- Project:   PJ004672 - COWS Reporting   
-- Author:   Brad Settle  
-- Date:   08/03/2011  
-- Description:    
--     Intl Carrier Partner Inventory Disconnect Report.  
--     Group Type:  Intl  
--     QueryNumber: 1 = Daily  
--         2 = Weekly  
--         3 = Monthly  
--         0 = Specify start date & end date: 'mm/dd/yyyy'  
--  
-- TEST: EXEC COWS_Reporting.dbo.sp_IntlRptCPInventoryDisconnectReport 1, 3, '09/01/2011', '10/30/2011'  
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME 
-- Updated Reason: SCURD_CD/CSG_LVL_CD  
--==================================================================================  
ALTER Procedure [dbo].[sp_IntlRptCPInventoryDisconnectReport]   
 @secured   BIT=0,  
 @QueryNumber  INT,  
 @inStartDtStr  VARCHAR(10)='',  
 @inEndDtStr   VARCHAR(10)=''  
AS  
BEGIN TRY  
  
 SET NOCOUNT ON;  
   
 DECLARE @startDate  Datetime  
 DECLARE @endDate  Datetime  
 DECLARE @today   Datetime  
 DECLARE @startDtStr  VARCHAR(10)   
 DECLARE @dayOfWeek  INT  
  
OPEN SYMMETRIC KEY FS@K3y   
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
    
 IF @QueryNumber=0  
  BEGIN  
  
   SET @startDate=CONVERT(datetime, @inStartDtStr, 101)  
   SET @endDate=CONVERT(datetime, @inEndDtStr, 101)      
  
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @endDate=DATEADD(day, 1, @endDate)   
              
  END   
 ELSE IF @QueryNumber=1  
     BEGIN       
      -- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000  
    
   SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)  
   SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)     
  END  
 ELSE IF @QueryNumber=2  
  BEGIN  
   -- e.g. Run date is '2011-06-21' 3rd day of the week  
   -- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000  
     
   SET @today=GETDATE()  
   SET @dayOfWeek=DATEPART(dw, @today)  
   SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+6), @today), 112) As DateTime)  
   SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS Datetime)  
  
   --SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -7, GETDATE()), 112) As DateTime)  
   --SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS Datetime)    
      
  END  
 ELSE IF @QueryNumber=3  
  BEGIN      
     SET @today=DATEADD(MONTH,-1,GETDATE())  
       
      -- e.g. Run date is the 1st day of each month   
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000  
        
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));       
   SET @startDate=cast(@startDtStr AS datetime)  
   SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)  
  END  
  
  
SELECT DISTINCT  
 fo.[FTN],  
 ISNULL(VAS_SOL_NBR,'') AS [SOL],  
 '' AS [OE],  
  
 CASE  
  --WHEN @secured = 0 and o.SCURD_CD = 0 THEN CONVERT(VARCHAR, DecryptByKey(si.CUST_NME))   
  --WHEN @secured = 0 and o.SCURD_CD = 1 AND CONVERT(VARCHAR, DecryptByKey(si.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  --WHEN @secured = 0 and o.SCURD_CD = 1 AND CONVERT(VARCHAR, DecryptByKey(si.CUST_NME)) IS NULL THEN NULL   
  --ELSE CONVERT(VARCHAR, DecryptByKey(si.CUST_NME))   
  WHEN @secured = 0 and o.CSG_LVL_ID =0 THEN si.CUST_NME  
  WHEN @secured = 0 and o.CSG_LVL_ID >0 AND si.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN @secured = 0 and o.CSG_LVL_ID >0 AND si.CUST_NME2 IS NULL THEN NULL   
  ELSE COALESCE(si.CUST_NME2,si.CUST_NME,'')   
  END [Customer Name], --Secured  
  
  
 lv.VNDR_NME AS [Carrier Partner],  
 lfpt.FSA_PROD_TYPE_DES AS [TYPE],  
 ISNULL(nc.TOC_CTY_NME,'') AS [NNI IP NODE],  
 ISNULL(nc.CKT_NME,'') AS [Carrier Partner Circuit ID],  
 nsi.NUA_ADR AS [NUA],  
 ISNULL(MDS_DLCI,'') AS [DLCI],  
 ISNULL(nc.PORT_ASMT_DES,'') AS [Router],  
 '' AS [Sub-Interface],  
 nsi.ORDR_STUS_DES AS [Status],  
 RGN_DES AS [Region],  
 --ISNULL(nc.TOC_CTRY_NME,'') AS [Country] 
 CASE WHEN (o.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd2.[CTRY_RGN_NME]) ELSE nc.CTRY_NME END [Country] /*Monir 06062018*/ 
FROM   
COWS.dbo.ORDR o WITH (NOLOCK)  
LEFT OUTER JOIN COWS.dbo.ORDR_ADR oa WITH (NOLOCK) ON oa.ORDR_ID=o.ORDR_ID  
LEFT OUTER JOIN COWS.dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID=o.ORDR_ID  
LEFT OUTER JOIN COWS.dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.ORDR_ID=o.ORDR_ID  
LEFT OUTER JOIN COWS.dbo.MPLS_EVENT mpe WITH (NOLOCK) ON mpe.FTN = fo.FTN  
LEFT OUTER JOIN COWS.dbo.MPLS_EVENT_ACCS_TAG meat WITH (NOLOCK) ON meat.EVENT_ID=mpe.EVENT_ID  
LEFT OUTER JOIN COWS.dbo.NRM_CKT nc WITH (NOLOCK) ON nc.FTN=fo.FTN 
LEFT OUTER JOIN COWS.dbo.[CUST_SCRD_DATA] csd2 WITH (NOLOCK) ON csd2.[SCRD_OBJ_ID]=nc.[NRM_CKT_ID] and csd2.[SCRD_OBJ_TYPE_ID]=29 
LEFT OUTER JOIN COWS.dbo.NRM_SRVC_INSTC nsi WITH (NOLOCK) ON nsi.FTN=fo.FTN  
LEFT OUTER JOIN COWS.dbo.LK_FSA_PROD_TYPE lfpt WITH (NOLOCK) ON lfpt.FSA_PROD_TYPE_CD=fo.PROD_TYPE_CD  
LEFT OUTER JOIN COWS.dbo.LK_WHLSL_PTNR lwp WITH (NOLOCK) ON lwp.WHLSL_PTNR_ID=mpe.WHLSL_PTNR_ID  
LEFT OUTER JOIN COWS.dbo.LK_VNDR lv WITH (NOLOCK) ON lv.VNDR_CD=fo.INSTL_VNDR_CD  
LEFT OUTER JOIN COWS.dbo.LK_XNCI_RGN lxr WITH (NOLOCK) ON lxr.RGN_ID=o.RGN_ID  
 
LEFT OUTER JOIN (SELECT bb.ORDR_ID, CONVERT(varchar, DecryptByKey(bb.CUST_NME)) [CUST_NME], CIS_LVL_TYPE  
 FROM [COWS].[dbo].[FSA_ORDR_CUST] bb   
 JOIN [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
 WHERE CIS_LVL_TYPE = 'H5') cst on cst.ORDR_ID = fo.ORDR_ID    
LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.SOI_CD, bb.CUST_NME, CONVERT(varchar, DecryptByKey(csd.CUST_NME)) AS CUST_NME2  
     FROM [COWS].[dbo].[FSA_ORDR_CUST] bb   
     LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
     LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
  WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
   FROM (SELECT SOI_CD, ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] ) soi  
   WHERE soi.ORDR_ID = bb.ORDR_ID)) si on si.ORDR_ID = fo.ORDR_ID   
     
WHERE o.PLTFRM_CD <> 'SF' AND oa.ADR_TYPE_ID = 3 AND foc.CIS_LVL_TYPE = 'H1'  
AND nsi.ORDR_STUS_DES LIKE '%DISC%' --Operational, Pending Operational  
 AND fo.CREAT_DT >= @startDate AND fo.CREAT_DT < @endDate  
   
CLOSE SYMMETRIC KEY FS@K3y   
 RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_IntlRptCPInventoryDisconnectReport '  + CAST(@QueryNumber AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  
  
  
  