USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_MNSRptCPT]    Script Date: 06/19/2018 17:59:51 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--============================================================================================
-- Project:		   pj017930 - COWS Reporting 
-- Author:			 	
-- Date:		   10/26/2016
-- Description:	   CPT orders last 120 days.	
-- Notes:
-- Updated by:     vn370313
-- Updated For:    Correct Column MANAGER   and  include MSS ENGN  Column as per user request
-- Updated Date:   01202017
-- Updated by:     vn370313
-- Updated For:    Corrected PM/NTE  PM=22 and 23=NTE
-- Updated Date:   01202017
-- Updated by:     vn370313
-- Updated For:    Added Condition RecStatus_ID of resource must be 1
-- Updated Date:   05032017
-- Updated by:     vn370313
-- Updated For:    cpt Company nme is no longer Encrypted
-- Updated Date:   06242018
-- [dbo].[sp_MNSRptCPT] 0
-- [dbo].[sp_MNSRptCPT] 1
--============================================================================================

ALTER Procedure [dbo].[sp_MNSRptCPT]
    @Secured			   INT=0
AS
BEGIN TRY

	SET NOCOUNT ON;
		
		
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
    
    
	
	SELECT DISTINCT 
		CPT.CPT_CUST_NBR AS [CPT Number],
		---CONVERT(varchar(max), DecryptByKey(cpt.COMPNY_NME)) AS [Customer Name],
		/*
		CASE @Secured 
							 WHEN '1' THEN 'Private Customer' 
							 --ELSE CONVERT(varchar(max), DecryptByKey(cpt.COMPNY_NME))
							 ELSE cpt.COMPNY_NME
						 END	AS	[Customer Name],
		*/ 
		CASE WHEN ((@Secured=1) AND (cpt.CSG_LVL_ID>0)) THEN ISNULL(dbo.decryptBinaryData(csd.CUST_NME),'')
		     WHEN ((@Secured=0) AND (cpt.CSG_LVL_ID>0)) THEN 'Private Customer'
			 WHEN (cpt.CSG_LVL_ID=0) THEN ISNULL(cpt.COMPNY_NME,'') END AS [Customer Name],
		cct.CPT_CUST_TYPE AS Type,
		case 
			when cpt.CPT_STUS_ID = 309 THEN 'Completed'
			when cpt.CPT_STUS_ID = 311 THEN 'Cancelled'
		END			AS [Status],
		cpt.DEV_CNT AS [Device Qty],  
		u.FULL_NME AS Submitter,
		ISNULL(nteu.FULL_NME,'') AS [NTE],
		ISNULL(pmu.FULL_NME,'')  AS [PM],
		ISNULL(mgru.FULL_NME,'') AS  [Manager],
		ISNULL(crmu.FULL_NME,'') AS [CRM],
		ISNULL(mssu.FULL_NME,'') AS [MSS Eng],
		CASE
			WHEN cpt.REVWD_PM_CD = 0 THEN 'N'
			WHEN cpt.REVWD_PM_CD = 1 THEN 'Y'
		END      AS [PM ACK Y/N], 
		cpt.CREAT_DT AS [Submit Date]
	FROM COWS.dbo.CPT cpt WITH(NOLOCK)
	LEFT OUTER JOIN COWS.dbo.CPT_RLTD_INFO cri  WITH(NOLOCK) ON cpt.CPT_ID = cri.CPT_ID
	LEFT OUTER JOIN COWS.dbo.LK_CPT_CUST_TYPE cct  WITH(NOLOCK)  ON cri.CPT_CUST_TYPE_ID = cct.CPT_CUST_TYPE_ID
	LEFT OUTER JOIN COWS.dbo.LK_STUS s   WITH(NOLOCK) ON cpt.CPT_STUS_ID = s.STUS_ID
	LEFT OUTER JOIN COWS.dbo.LK_USER u   WITH(NOLOCK) ON cpt.SUBMTR_USER_ID = u.USER_ID
	LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT nte ON cpt.CPT_ID = nte.CPT_ID AND nte.ROLE_ID = 23 AND nte.REC_STUS_ID=1  --- 22 before
	LEFT OUTER JOIN COWS.dbo.LK_USER nteu   WITH(NOLOCK) ON nte.CPT_USER_ID = nteu.USER_ID
	LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT pm   WITH(NOLOCK) ON cpt.CPT_ID = pm.CPT_ID AND pm.ROLE_ID = 22  AND pm.REC_STUS_ID=1 --- 23 Before
	LEFT OUTER JOIN COWS.dbo.LK_USER pmu   WITH(NOLOCK) ON pm.CPT_USER_ID = pmu.USER_ID
	LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT mss   WITH(NOLOCK) ON cpt.CPT_ID = mss.CPT_ID AND mss.ROLE_ID = 82 AND mss.REC_STUS_ID=1
	LEFT OUTER JOIN COWS.dbo.LK_USER mssu   WITH(NOLOCK) ON mss.CPT_USER_ID = mssu.USER_ID
	LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT mgr   WITH(NOLOCK) ON cpt.CPT_ID = mgr.CPT_ID AND mgr.ROLE_ID = 151 AND mgr.REC_STUS_ID=1
	LEFT OUTER JOIN COWS.dbo.LK_USER mgru   WITH(NOLOCK) ON mgr.CPT_USER_ID = mgru.USER_ID
	LEFT OUTER JOIN COWS.dbo.CPT_USER_ASMT crm   WITH(NOLOCK) ON cpt.CPT_ID = crm.CPT_ID AND crm.ROLE_ID = 153 AND crm.REC_STUS_ID=1
	LEFT OUTER JOIN COWS.dbo.LK_USER crmu   WITH(NOLOCK) ON crm.CPT_USER_ID = crmu.USER_ID
	LEFT JOIN COWS.dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=cpt.CPT_ID AND csd.SCRD_OBJ_TYPE_ID=2
	
	WHERE cpt.CPT_STUS_ID IN (309, 311) --Completed, Cancelled
			AND cpt.MODFD_DT > DATEADD(day, -90, GETDATE())


CLOSE SYMMETRIC KEY FS@K3y;
		
										
	RETURN 0;
  
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_CPTDailyReport '  	
	SET @Desc=@Desc
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH








GO


