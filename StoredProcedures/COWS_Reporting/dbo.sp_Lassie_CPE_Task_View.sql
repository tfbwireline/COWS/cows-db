USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_Lassie_CPE_Task_View]    Script Date: 9/26/2016 4:03:04 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: dlp0278
-- Create date: 1/11/2016 
-- Description:  SP used for Lassie CPE Task View
-- =============================================
ALTER PROCEDURE [dbo].[sp_Lassie_CPE_Task_View] 

AS
BEGIN TRY

SET NOCOUNT ON;

TRUNCATE TABLE [COWS_Reporting].[dbo].[LassieCpeTaskData]

INSERT INTO [COWS_Reporting].[dbo].[LassieCpeTaskData]
           ([Order_ID],[ORDR_ID],[SITE_ID],[DEVICE_ID],[TASK_ID],[TASK_NME],[STUS_ID],[CREAT_DT])

SELECT DISTINCT fsa.FTN as Order_ID
		   ,fsa.ORDR_ID
		   ,foc.SITE_ID
		   ,itm.DEVICE_ID
		   ,act.TASK_ID
		   ,task.TASK_NME
		   ,act.STUS_ID
		   ,act.CREAT_DT
	FROM [COWS].[dbo].[FSA_ORDR] fsa WITH (NOLOCK)
	INNER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc WITH (NOLOCK) ON foc.ORDR_ID = fsa.ORDR_ID
	INNER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] itm WITH (NOLOCK) 
					ON itm.ORDR_ID = fsa.ORDR_ID
					AND ISNULL(itm.DEVICE_ID,'')<> ''
	INNER JOIN [COWS].[dbo].[ORDR] ord WITH (NOLOCK) 
					ON ord.ORDR_ID = fsa.ORDR_ID
	INNER JOIN [COWS].[dbo].[ACT_TASK] act WITH (NOLOCK) 
					ON act.ORDR_ID = fsa.ORDR_ID 
	INNER JOIN [COWS].[dbo].[LK_TASK] task WITH (NOLOCK)
					ON act.TASK_ID = task.TASK_ID

	WHERE ord.ORDR_CAT_ID = 6 AND act.TASK_ID IN (600,601,602,603,1000,1001) 

END TRY

BEGIN CATCH

DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_Lassie_CPE_Task_View ' + ': '
	SET @Desc=@Desc 
	EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
	
END CATCH