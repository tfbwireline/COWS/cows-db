USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptESEventsOD]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--=========================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			11/15/2011
-- Description:		
--					Extract ES calendar for the following appointments: 
--
--					Appt Type:	17 = Preparation
--							  	18 = Lunch
--							   	19 = Personal
--								20 = Vacation
--								21 = Comp
--								22 = Meeting
--								23 = Training
--								24 = Project
--								25 = Schedule Block 
--								26 = Conf. Call
--								27 = Implementation
--
--					QueryNumber: 1 = Daily
--								 2 = Weekly
--								 3 = Monthly
--								 0 = Specify start date & end date: 'mm/dd/yyyy'
--								-1 = Called from On Demand report with date range specified
--									 in @startDate and @endDate
-- Modifications:
--
-- 03/29/2012		sxv0766: IM952822-Removed resulting string expression from Event Date
--					and End Date columns
--
--=========================================================================================

ALTER Procedure [dbo].[sp_MNSRptESEventsOD] --0, 'N', '2015/01/01', '2015/04/30'
	@SecuredUser		INT=0,	
	@getSensData		CHAR(1)='N',
	@startDate			DateTime=NULL,
	@endDate			DateTime=NULL	
AS

BEGIN TRY

	SET NOCOUNT ON;

	DECLARE @stDt		Datetime
	DECLARE @endDt		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)	
	DECLARE @dayOfWeek		INT
	
	DECLARE	@cnt			INT				--Iterator
	DECLARE	@tCnt			INT				--# of times to iterate
	DECLARE @apptID			INT
	DECLARE @users			VARCHAR(MAX)	--ASN_TO_USER_ID_LIST_TXT	
	
	DECLARE	@Appt	TABLE
	(
		ApptID			INT,
		UserID			INT
	)	
	DECLARE	@ApptUsers	TABLE
	(
		ApptID			INT,
		UserIDs			VARCHAR(MAX),
		Flag			BIT
	)	
	DECLARE @Appointments TABLE 
	(
		Appt_ID			INT,
		StartTime		DATETIME,
		EndTime			DATETIME,
		Appt_Type		INT
	)
	
	--=====================
	-- Determine Date Range
	--=====================
	
		
			SET @stDt=@startDate
			SET @endDt=DATEADD(day, 1, @endDate)
			
		
	

	--=======================================================
	-- Get Assigned User List and filter it out for MDS group
	--=======================================================	
	INSERT INTO	@ApptUsers	(ApptID, UserIDs, Flag)
		SELECT	DISTINCT APPT_ID, dbo.parseAssignedUsersFromAppt(ASN_TO_USER_ID_LIST_TXT), 0
		FROM	COWS.dbo.APPT with (nolock) 
		WHERE APPT_TYPE_ID	>= 17 and APPT_TYPE_ID <= 26 AND ASN_TO_USER_ID_LIST_TXT is not NULL	
		
	SET	@cnt =	0
	
	--Get total # of records
	SELECT @tCnt = COUNT(1) FROM @ApptUsers
	
	WHILE @cnt	< @tCnt
	BEGIN
	
		SET	@apptID	=	0			
		SET	@users	=	''	
					
		SELECT TOP 1 @apptID = ApptID, @users = UserIDs
		FROM @ApptUsers
		WHERE Flag = 0
					
		INSERT INTO	@Appt(ApptID, UserID)
			SELECT DISTINCT	@apptID, StringID
			FROM COWS_Reporting.dbo.parseStringWithDelimiter(@Users, '|') ulst
			JOIN COWS.dbo.USER_GRP_ROLE ugrl ON ulst.StringID = ugrl.USER_ID and ugrl.GRP_ID = 2 and ugrl.REC_STUS_ID = 1
			WHERE StringID	!=	''
						
		UPDATE @ApptUsers SET Flag=1 WHERE ApptID = @apptID
		SET @cnt += 1			
	END		
	
	--==============================================	
	--Get non-recurring appointments into temp table
	--==============================================	
	INSERT INTO @Appointments
		--SELECT STRT_TMST, END_TMST, APPT_TYPE_ID, ASN_TO_USER_ID_LIST_TXT
		--SELECT APPT_ID, STRT_TMST, END_TMST, SUBJ_TXT, DES, APPT_TYPE_ID
		SELECT APPT_ID, STRT_TMST, END_TMST, APPT_TYPE_ID
		FROM COWS.dbo.APPT with (nolock)
		WHERE END_TMST >= @stDt and STRT_TMST < @endDt 
			AND RCURNC_CD = 0
			AND APPT_TYPE_ID in (17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27)
			
	--==============================================		
	--Get all recurring appointments into temp table			
	--==============================================		
	INSERT INTO @Appointments 
		SELECT DISTINCT	ap.APPT_ID,
				apd.STRT_TMST, 
				apd.END_TMST, 
				ap.APPT_TYPE_ID
		FROM	COWS.dbo.APPT_RCURNC_DATA apd with (nolock) INNER JOIN
				COWS.dbo.APPT ap with (nolock) ON ap.APPT_ID = apd.APPT_ID
		WHERE	ap.RCURNC_CD = 1 AND ap.APPT_TYPE_ID in (17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27)
			AND ap.APPT_ID NOT in (222261,221993,222267,223274)
					

	
	SELECT	atyp.APPT_TYPE_DES 'Event Type', 
			CASE
				WHEN luser.DSPL_NME is NULL THEN ''
				ELSE luser.DSPL_NME END 'Assigned Activators',				
			appt.SUBJ_TXT 'Title',		
			apmt.StartTime 'Event Date',
			--REPLACE(SUBSTRING(CONVERT(varchar, apmt.StartTime, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, apmt.StartTime, 109), 13, 8)
			--+ ' ' + SUBSTRING(CONVERT(varchar, apmt.StartTime, 109), 25, 2) 'Event Date',			
			apmt.EndTime 'End Date',
			--REPLACE(SUBSTRING(CONVERT(varchar, apmt.EndTime, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, apmt.EndTime, 109), 13, 8)
			--+ ' ' + SUBSTRING(CONVERT(varchar, apmt.EndTime, 109), 25, 2) 'End Date',										
			CASE 
				WHEN appt.DES is NULL THEN ''
				ELSE appt.DES END 'Description'
			
	FROM	@Appointments apmt
		LEFT JOIN COWS.dbo.APPT appt with (nolock) ON apmt.Appt_ID = appt.APPT_ID 	
		JOIN @Appt aptu ON appt.APPT_ID = aptu.ApptID
		LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON aptu.UserID = luser.USER_ID
		LEFT JOIN COWS.dbo.LK_APPT_TYPE atyp with (nolock) ON appt.APPT_TYPE_ID = atyp.APPT_TYPE_ID
				
		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
	
	WHERE apmt.EndTime >= @stDt and apmt.StartTime < @endDt 
	ORDER BY 'Event Date'
	
		
  	RETURN 0;
  	  	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(100)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptESEventsOD '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @stDt, 101) + ', ' + CONVERT(VARCHAR(10), @endDt, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH


GO
