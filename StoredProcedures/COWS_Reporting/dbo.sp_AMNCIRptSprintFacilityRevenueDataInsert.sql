-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>  
-- Updated By:   Md M Monir  
-- Updated Date: 03/14/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME    
-- =============================================    
ALTER PROCEDURE [dbo].[sp_AMNCIRptSprintFacilityRevenueDataInsert]     
    
AS    
BEGIN    
    
 SET NOCOUNT ON;    
OPEN SYMMETRIC KEY FS@K3y     
  DECRYPTION BY CERTIFICATE S3cFS@CustInf0;    
    
INSERT into COWS_Reporting.dbo.AMNCISprintFacilityRevenueData    
SELECT DISTINCT     
ISNULL(fsa.FTN, CONVERT(varchar(20),ord.ORDR_ID)) [Tracking Number],    
c.ORDR_STUS_DES [Status Update] ,    
NULL [Group Customer ID],    
CASE     
  WHEN ord.CSG_LVL_ID = 0 AND h5.CUST_NME<>''  THEN coalesce(h5.CUST_NME,'PRIVATE CUSTOMER')  
    WHEN ord.CSG_LVL_ID = 0 AND h5.CUST_NME =''  THEN 'PRIVATE CUSTOMER'    
    WHEN ord.CSG_LVL_ID = 0 AND h5.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'   
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),h5.CUST_NME,'')   
 END [Customer Name] ,    
NULL [# of Ccts],     
fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES as [Order Bandwidth],     
ipl.CKT_SPD_QTY [speed],    
trpt.CKT_LGTH_IN_KMS_QTY [Length of CK],    
g.PLN_NME [PL#],     
NULL [Product Type 2],     
lkpt.PROD_TYPE_DES [Product Type],    
fsa.FTN [Order Ref #],     
NULL [OE #],     
NULL [CMS ID],     
ord.ORDR_ID [CDM ID #],     
NULL [A-end street],      
NULL [B-end street],     
NULL [B-end City],     
NULL  [B-end Postcode],     
trpt.A_END_REGION [County / Province or Region],     
NULL [B-end Country] ,    
lkv.VNDR_NME [Local Access Provider],     
lkvt.VNDR_TYPE_DES [Vendor Type],     
lkot.FSA_ORDR_TYPE_DES [Order Type],     
nrmv.VNDR_CKT_ID [Vendor circuit ID],    
fsa.TSUP_RFQ_NBR [RFQ No#],     
lkos.ORDR_STUS_DES [Status],    
NULL [Customer Term (months)],     
DATEDIFF(month, ord.CUST_CMMT_DT,ckt.VNDR_CNTRC_TERM_END_DT) [Vendor Term (months)],     
ISNULL(cktms.ACCS_ACPTC_DT, ordms.ORDR_BILL_CLEAR_INSTL_DT) [Local Access Installed Date],    
ISNULL(ordms.CUST_ACPTC_TURNUP_DT, ordms.ORDR_BILL_CLEAR_INSTL_DT)[Actual Handover Date],    
MONTH(cktm.TRGT_DLVRY_DT_RECV_DT) [Installed Month],    
YEAR(cktm.TRGT_DLVRY_DT_RECV_DT)[Installed Year],    
--ISNULL(p.DSCNCT_DT, '') [Disco Date],    
ordms.ORDR_DSCNCT_DT [Disco Date],    
lksc.SALS_CHNL_DES [Sales Channel],    
trpt.CUST_BILL_CUR_ID [Currency],    
NULL [Invoice Note],     
q.VNDR_MRC_IN_USD_AMT [Vendor Cost MRC - - excl Markup$$$],    
q.VNDR_NRC_IN_USD_AMT [Vendor Cost NRC$$$],    
q.VNDR_MRC_IN_USD_AMT * 12 [Vendor Cost Annual Rental - - excl Markup],    
trpt.CMNT_TXT [Comments],    
NULL [Revenue Currency],    
NULL [Access Revenue MRC Local Currency$$$],    
NULL [Installation Revenue Local Currency$$$],    
q.ACCS_CUST_MRC_IN_USD_AMT [Access Revenue USD per Month],    
q.ACCS_CUST_NRC_AMT [Installation Revenue],    
/*h5.CUST_CTY_NME [Origination City],*/  
--CASE     
--  WHEN ord.CSG_LVL_ID = 0 AND h5.CUST_CTY_NME<>''  THEN coalesce(h5.CUST_NME,'PRIVATE CITY')  
--    WHEN ord.CSG_LVL_ID = 0 AND h5.CUST_CTY_NME =''  THEN 'PRIVATE CITY'    
--    WHEN ord.CSG_LVL_ID = 0 AND h5.CUST_CTY_NME IS NULL  THEN 'PRIVATE CITY'   
--    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd.CTY_NME)),h5.CUST_CTY_NME,'')   
-- END [Origination City] ,
CASE WHEN (h5.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE h5.CUST_CTY_NME END AS  [Origination City], /*Monir 06062018*/     
--lkc.CTRY_NME [Origination country], 
CASE WHEN (h5.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE lkc.CTRY_NME END [Origination Country], /*Monir 06062018*/   
q.ASR_NRC_AMT [ASR Vendor Installation Cost US$],    
NULL [Revised MRC],    
NULL [Effective Date],    
NULL [Revised MRC1],    
NULL [Effective Date1],    
NULL [Revised MRC2],    
NULL [Effective Date2],    
NULL [Revised MRC3],    
NULL [Effective Date3],    
NULL [ASR Annual Rental Cost US$],    
NULL [Unit Cost/Kms]    
--'' [Service request type]    
FROM    
[COWS].[dbo].[ORDR] ord   WITH (NOLOCK) LEFT OUTER JOIN    
[COWS].[dbo].[FSA_ORDR] fsa  WITH (NOLOCK) on ord.ORDR_ID = fsa.ORDR_ID   
LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = fsa.ORDR_ID  
LEFT OUTER JOIN    
--[FSA_ORDR_GOM_XNCI] b on ord.ORDR_ID = b.ORDR_ID  LEFT OUTER JOIN    
[COWS].[dbo].[LK_ORDR_STUS] c  WITH (NOLOCK) on ord.ORDR_STUS_ID = c.ORDR_STUS_ID LEFT OUTER JOIN    
[COWS].[dbo].[IPL_ORDR] ipl  WITH (NOLOCK) on ord.ORDR_ID = ipl.ORDR_ID LEFT OUTER JOIN    
[COWS].[dbo].[TRPT_ORDR] trpt  WITH (NOLOCK) on ord.ORDR_ID = trpt.ORDR_ID LEFT OUTER JOIN    
[COWS].[dbo].[LK_SALS_CHNL] lksc  WITH (NOLOCK) on trpt.SALS_CHNL_ID = lksc.SALS_CHNL_ID LEFT OUTER JOIN    
--[NRM_CKT] g on fsa.FTN = g.FTN LEFT OUTER JOIN    
(SELECT bb.PLN_NME, bb.PLN_SEQ_NBR, bb.FTN from [COWS].[dbo].[NRM_CKT]bb  WITH (NOLOCK)  JOIN [COWS].[dbo].[FSA_ORDR] cc   WITH (NOLOCK) on bb.FTN = cc.FTN     
WHERE bb.PLN_SEQ_NBR = (SELECT MAX(PLN_SEQ_NBR) FROM (SELECT FTN, PLN_SEQ_NBR FROM [COWS].[dbo].[NRM_CKT]  WITH (NOLOCK) ) nckt where cc.FTN = nckt.FTN)) g on fsa.FTN = g.FTN LEFT OUTER JOIN    
[COWS].[dbo].[LK_PROD_TYPE] lkpt WITH (NOLOCK)  on fsa.PROD_TYPE_CD = lkpt.FSA_PROD_TYPE_CD LEFT OUTER JOIN    
[COWS].[dbo].[ORDR_ADR] oadr WITH (NOLOCK)  on ord.ORDR_ID = oadr.ORDR_ID LEFT OUTER JOIN    
[COWS].[dbo].[LK_VNDR_TYPE] lkvt  WITH (NOLOCK) on trpt.VNDR_TYPE_ID = lkvt.VNDR_TYPE_ID LEFT OUTER JOIN    
[COWS].[dbo].[LK_FSA_ORDR_TYPE] lkot  WITH (NOLOCK)  on fsa.ORDR_TYPE_CD = lkot.FSA_ORDR_TYPE_CD LEFT OUTER JOIN    
[COWS].[dbo].[NRM_VNDR] nrmv  WITH (NOLOCK) on fsa.FTN = nrmv.FTN LEFT OUTER JOIN    
[COWS].[dbo].[LK_ORDR_STUS] lkos  WITH (NOLOCK) on ord.ORDR_STUS_ID = lkos.ORDR_STUS_ID LEFT OUTER JOIN    
[COWS].[dbo].[CKT] ckt  WITH (NOLOCK) on ord.ORDR_ID = ckt.ORDR_ID LEFT OUTER JOIN    
[COWS].[dbo].[CKT_MS] cktm  WITH (NOLOCK) on ckt.CKT_ID = cktm.CKT_ID LEFT OUTER JOIN    
[COWS].[dbo].[NRM_SRVC_INSTC] p  WITH (NOLOCK) on fsa.FTN = p.FTN LEFT OUTER JOIN    
--[CKT_COST] q on ckt.CKT_ID = q.CKT_ID  LEFT OUTER JOIN    
(SELECT bb.CKT_ID, SUM(ISNULL(bb.VNDR_MRC_IN_USD_AMT,0))VNDR_MRC_IN_USD_AMT, bb.VNDR_CUR_ID,     
 bb.ASR_CUR_ID, bb.ACCS_CUST_CUR_ID, SUM(ISNULL(bb.ACCS_CUST_MRC_IN_USD_AMT,0)) ACCS_CUST_MRC_IN_USD_AMT,     
 SUM(ISNULL(bb.ACCS_CUST_NRC_AMT,0))ACCS_CUST_NRC_AMT, SUM(ISNULL(bb.ASR_NRC_AMT,0)) ASR_NRC_AMT,     
 (bb.VNDR_NRC_AMT)VNDR_NRC_AMT, SUM(ISNULL(bb.VNDR_NRC_IN_USD_AMT,0))VNDR_NRC_IN_USD_AMT,    
 SUM(ISNULL(bb.ASR_MRC_IN_USD_AMT,0)) ASR_MRC_IN_USD_AMT    
  FROM [COWS].[dbo].[CKT_COST] bb  WITH (NOLOCK)  LEFT OUTER JOIN [COWS].[dbo].[CKT] cc  WITH (NOLOCK)  on bb.CKT_ID = cc.CKT_ID     
 WHERE bb.VER_ID = (SELECT MAX(VER_ID) FROM (SELECT CKT_ID, VER_ID FROM [COWS].[dbo].[CKT_COST]  WITH (NOLOCK)  ) ver    
               WHERE bb.CKT_ID = ver.CKT_ID)     
     GROUP BY bb.CKT_ID, VNDR_CUR_ID, ASR_CUR_ID, ACCS_CUST_CUR_ID, VNDR_NRC_AMT )q on  ckt.CKT_ID = q.CKT_ID LEFT OUTER JOIN    
(SELECT bb.* FROM [COWS].[dbo].[CKT_MS] bb  WITH (NOLOCK)  LEFT OUTER JOIN [COWS].[dbo].[CKT] cc  WITH (NOLOCK)  on bb.CKT_ID = cc.CKT_ID    
 WHERE bb.VER_ID = (SELECT MAX(VER_ID) FROM (SELECT CKT_ID, VER_ID from [COWS].[dbo].[CKT_MS]) ver    
      WHERE bb.CKT_ID = VER_ID)) cktms on ckt.CKT_ID = cktms.CKT_ID LEFT OUTER JOIN    
[COWS].[dbo].[LK_CUR] lkcur WITH (NOLOCK) on q.VNDR_CUR_ID = lkcur.CUR_ID    LEFT OUTER JOIN    
[COWS].[dbo].[LK_CUR] lkcur2 WITH (NOLOCK)  on q.ASR_CUR_ID  = lkcur2.CUR_ID LEFT OUTER JOIN    
[COWS].[dbo].[LK_CUR] lkcur3 WITH (NOLOCK)  on q.ACCS_CUST_CUR_ID = lkcur3.CUR_ID  LEFT OUTER JOIN    
[COWS].[dbo].[LK_XNCI_RGN] lkxr WITH (NOLOCK)  on ord.RGN_ID = lkxr.RGN_ID LEFT OUTER JOIN    
[COWS].[dbo].[H5_FOLDR] h5 WITH (NOLOCK)  on ord.H5_FOLDR_ID = h5.H5_FOLDR_ID LEFT OUTER JOIN   
[COWS].[dbo].[CUST_SCRD_DATA] csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=h5.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6 LEFT OUTER JOIN   
[COWS].[dbo].[LK_CTRY] lkc WITH (NOLOCK)  on h5.ctry_cd = lkc.ctry_cd LEFT OUTER JOIN
[COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD    LEFT OUTER JOIN 
[COWS].[dbo].[FSA_ORDR_CUST] fsaoc WITH (NOLOCK)  on fsa.ORDR_ID = fsaoc.ORDR_ID LEFT OUTER JOIN    
[COWS].[dbo].[VNDR_ORDR] vo WITH (NOLOCK)  on ord.ORDR_ID = vo.ORDR_ID LEFT OUTER JOIN     
[COWS].[dbo].[VNDR_FOLDR] vf WITH (NOLOCK)  on vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID LEFT OUTER JOIN    
[COWS].[dbo].[LK_VNDR] lkv WITH (NOLOCK)  on vf.VNDR_CD = lkv.VNDR_CD LEFT OUTER JOIN    
[COWS].[dbo].[ORDR_MS] ordms WITH (NOLOCK)  on ord.ORDR_ID = ordms.ORDR_ID    
--Where lkxr.RGN_DES = 'AMNCI' and ord.ORDR_STUS_ID in (2,5)    
Where ord.DMSTC_CD = 1 AND ord.RGN_ID = 1     
 AND (fsa.PROD_TYPE_CD NOT IN ('MN') OR fsa.PROD_TYPE_CD IS NULL )    
 AND(ord.ORDR_STUS_ID in (2,5) )    
 AND ord.H5_FOLDR_ID IS NOT NULL    
 AND ord.ORDR_ID not in (Select [CDM ID #] from [COWS_Reporting].[dbo].[AMNCISprintFacilityRevenueData])    
END 