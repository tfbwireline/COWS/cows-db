USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_CANDEventTableLoad]    Script Date: 07/13/2018 11:51:03 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*

--==================================================================================
-- Project:			Customer Request - COWS Reporting 
-- Author:			David Phillips	
-- Date:			08/26/2016
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--==================================================================================
*/
ALTER PROCEDURE [dbo].[sp_CANDEventTableLoad]

AS
BEGIN TRY

	SET NOCOUNT ON;
	
	OPEN SYMMETRIC KEY FS@K3y 
    DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
    
    Truncate Table  [dbo].[CANDEventData]

	INSERT INTO [dbo].[CANDEventData]
           ([Event_ID],[Event_Status],[Item_Title],[Event_Type],[Activity_Type],[Start_Time]
           ,[End_Time],[Event_Duration],[Extra_Duration],[Customer_Name],[Action],[Modified_Date]
           ,[Modified_By],[Success_Activities],[Failed_Activities],[VPN_Platform_Type],[VAS_Type],[FMS_Circuit_Info],
			[Requestor],[Requestor_Email],[Assigned_User],[Engineer],[Engineer_Email])
				
    
	
	SELECT DISTINCT splk.EVENT_ID 'Event_ID', 
			UPPER(evst.EVENT_STUS_DES) 'Event_Status', 							
		
			CASE WHEN splk.EVENT_TITLE_TXT is NULL THEN '' 
				 ELSE splk.EVENT_TITLE_TXT 
		    END 'Item_Title',	
			
			lkst.SPLK_EVENT_TYPE_DES 'Event_Type',	
			lksa.SPLK_ACTY_TYPE_DES 'Activity_Type',				
			REPLACE(SUBSTRING(CONVERT(varchar, splk.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, splk.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, splk.STRT_TMST, 109), 25, 2) 'Start_Time',			
			REPLACE(SUBSTRING(CONVERT(varchar, splk.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, splk.END_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, splk.END_TMST, 109), 25, 2) 'End_Time',		
			splk.EVENT_DRTN_IN_MIN_QTY 'Event_Duration',
			splk.EXTRA_DRTN_TME_AMT 'Extra_Duration',
			CASE	WHEN evnt.CSG_LVL_ID > 0  THEN 'Private Customer'
					WHEN splk.CUST_NME is NULL THEN '' 
					ELSE splk.CUST_NME
				 END  'Customer_Name',
			CASE
				WHEN lkac.ACTN_DES = 'Completed Email Notification' THEN 'Completed' 
				ELSE lkac.ACTN_DES END 'Action',				
			REPLACE(SUBSTRING(CONVERT(varchar, sphi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, sphi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, sphi.CREAT_DT, 109), 25, 2) 'Modified_Date',			
			CASE
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END  'Modified_By',	
			CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '' 
					ELSE scev.SUCSS_ACTY_DES END 'Success_Activities',		
			CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '' ELSE flev.FAIL_ACTY_DES END  'Failed_Activities',
			'' 'VPN_Platform_Type',
			'' 'VAS_Type',
			'' 'FMS_Circuit_Info',
			 
			cuser.DSPL_NME 'Requestor',
			cuser.EMAIL_ADR 'Requestor_Email',
			COWS.dbo.GetSemiColonSepAssignUsers(splk.EVENT_ID) as  'Assigned_User',
			suser.DSPL_NME 'Engineer',
			suser.EMAIL_ADR 'Engineer_Email'	
		FROM COWS.dbo.SPLK_EVENT splk with (nolock) 
		    left join COWS.dbo.EVENT evnt with(nolock) on evnt.EVENT_ID = splk.EVENT_ID 
		    /* LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=splk.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=19 */		
			JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON splk.EVENT_STUS_ID = evst.EVENT_STUS_ID
			JOIN COWS.dbo.LK_SPLK_EVENT_TYPE lkst with (nolock) ON splk.SPLK_EVENT_TYPE_ID = lkst.SPLK_EVENT_TYPE_ID
			JOIN COWS.dbo.LK_SPLK_ACTY_TYPE lksa with (nolock) ON splk.SPLK_ACTY_TYPE_ID = lksa.SPLK_ACTY_TYPE_ID
			JOIN
			(
				SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT
					FROM COWS.dbo.EVENT_HIST eh with (nolock)					
					JOIN COWS.dbo.SPLK_EVENT sple with (nolock) ON eh.EVENT_ID = sple.EVENT_ID
					WHERE eh.ACTN_ID IN (1,3,5,9,10,11,12,15,16,19,44) AND sple.EVENT_STUS_ID not in (6,8) 
				UNION
				SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT
					FROM COWS.dbo.EVENT_HIST ehis with (nolock)
					JOIN
					(
						SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) 'maxEvHis'
							FROM COWS.dbo.EVENT_HIST eh with (nolock)
							JOIN COWS.dbo.SPLK_EVENT sple with (nolock) ON eh.EVENT_ID = sple.EVENT_ID
							WHERE eh.ACTN_ID in (18) AND sple.EVENT_STUS_ID = 6 
						GROUP BY eh.EVENT_ID
					)ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis
			)sphi ON splk.EVENT_ID = sphi.EVENT_ID						
			LEFT JOIN
			(
				SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lksu.SUCSS_ACTY_DES  + ', ' AS [text()] 
	 						FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)
	 						JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID 
	 						WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID 
	 						FOR xml PATH ('')
							) as SUCSS_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)	
					)evSucAct
		  		)evSucAct2
			)scev ON sphi.EVENT_HIST_ID = scev.EVENT_HIST_ID
			LEFT JOIN
			(
				SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lkfl.FAIL_ACTY_DES  + ', ' AS [text()] 
	 						FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)
	 						JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID 
	 						WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID 
	 						FOR xml PATH ('')
							) as FAIL_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)	
					)evFailAct
		  		)evFailAct2
			)flev ON sphi.EVENT_HIST_ID = flev.EVENT_HIST_ID			
			JOIN COWS.dbo.LK_USER luser1 with (nolock) ON sphi.CREAT_BY_USER_ID = luser1.USER_ID 
			JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON sphi.ACTN_ID = lkac.ACTN_ID
			JOIN COWS.dbo.LK_USER cuser with (nolock) ON splk.CREAT_BY_USER_ID = cuser.USER_ID -- 7/5/17
			JOIN COWS.dbo.LK_USER suser with (nolock) ON suser.USER_ID = splk.SALS_USER_ID -- 7/5/17
			
		 WHERE splk.STRT_TMST > DATEADD(dd,-365,GETDATE())

		UNION 

		SELECT DISTINCT  mpls.EVENT_ID 'Event_ID', 
				UPPER(evst.EVENT_STUS_DES) 'Event_Status', 							
				CASE WHEN mpls.EVENT_TITLE_TXT is NULL THEN '' 
				ELSE mpls.EVENT_TITLE_TXT 
				END 'Item_Title',
				lkmt.MPLS_EVENT_TYPE_DES 'Event_Type',				
				mpact.MPLS_ACTY_TYPE 'Activity_Type',
				
				REPLACE(SUBSTRING(CONVERT(varchar, mpls.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mpls.STRT_TMST, 109), 13, 8) 
					+ ' ' + SUBSTRING(CONVERT(varchar, mpls.STRT_TMST, 109), 25, 2) 'Start_Time',
				REPLACE(SUBSTRING(CONVERT(varchar, mpls.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mpls.END_TMST, 109), 13, 8) 
					+ ' ' + SUBSTRING(CONVERT(varchar, mpls.END_TMST, 109), 25, 2) 'End_Time',					
				mpls.EVENT_DRTN_IN_MIN_QTY 'Event_Duration',
				mpls.EXTRA_DRTN_TME_AMT 'Extra_Duration',														
				CASE	WHEN evnt.CSG_LVL_ID > 0  THEN 'Private Customer' 
						WHEN mpls.CUST_NME is NULL THEN '' 
						ELSE mpls.CUST_NME 
				END 'Customer_Name',
				CASE
					WHEN lkac.ACTN_DES = 'Completed Email Notification' THEN 'Completed' 
					ELSE lkac.ACTN_DES END 'Action',			
				REPLACE(SUBSTRING(CONVERT(varchar, mpev.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mpev.CREAT_DT, 109), 13, 8) 
					+ ' ' + SUBSTRING(CONVERT(varchar, mpev.CREAT_DT, 109), 25, 2) 'Modified_Date',			
				CASE
					WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''
					WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
					ELSE luser1.DSPL_NME END 'Modified_By',
				CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '' ELSE scev.SUCSS_ACTY_DES END 'Success_Activities',		
				CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '' ELSE flev.FAIL_ACTY_DES END 'Failed_Activities',
				
				lkvp.VPN_PLTFRM_TYPE_DES 'VPN_Platform_Type',
				CASE WHEN lkvt.VAS_TYPE_DES is NULL THEN '' ELSE lkvt.VAS_TYPE_DES END 'VAS_Type',
				'' 'FMS_Circuit_Info',
				 
				cuser.DSPL_NME 'Requestor',
				cuser.EMAIL_ADR 'Requestor_Email',
				COWS.dbo.GetSemiColonSepAssignUsers(mpls.EVENT_ID) as 'Assigned_User',
				suser.DSPL_NME 'Engineer',
				suser.EMAIL_ADR 'Engineer_Email'	
			FROM COWS.dbo.MPLS_EVENT mpls with (nolock) 
			   left join COWS.dbo.EVENT evnt with(nolock) on evnt.EVENT_ID = mpls.EVENT_ID 		
				JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mpls.EVENT_STUS_ID = evst.EVENT_STUS_ID
				JOIN COWS.dbo.LK_MPLS_EVENT_TYPE lkmt with (nolock) ON mpls.MPLS_EVENT_TYPE_ID = lkmt.MPLS_EVENT_TYPE_ID
				JOIN COWS.dbo.LK_VPN_PLTFRM_TYPE lkvp with (nolock) ON mpls.VPN_PLTFRM_TYPE_ID = lkvp.VPN_PLTFRM_TYPE_ID
				LEFT JOIN COWS.dbo.MPLS_EVENT_VAS_TYPE mvst with (nolock) ON mpls.EVENT_ID = mvst.EVENT_ID
				LEFT JOIN COWS.dbo.LK_VAS_TYPE lkvt with (nolock) ON mvst.VAS_TYPE_ID = lkvt.VAS_TYPE_ID	
				JOIN
				(
					SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT
						FROM COWS.dbo.EVENT_HIST eh with (nolock)
						JOIN COWS.dbo.MPLS_EVENT mple with (nolock) ON eh.EVENT_ID = mple.EVENT_ID
						WHERE eh.ACTN_ID IN (1,3,5,9,10,11,12,15,16,19,44) AND mple.EVENT_STUS_ID not in (6,8) 
					UNION
					SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT
						FROM COWS.dbo.EVENT_HIST ehis with (nolock)
						JOIN
						(
							SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) 'maxEvHis'
								FROM COWS.dbo.EVENT_HIST eh with (nolock)
								JOIN COWS.dbo.MPLS_EVENT mple with (nolock) ON eh.EVENT_ID = mple.EVENT_ID
								WHERE eh.ACTN_ID in (18) AND mple.EVENT_STUS_ID = 6 
							GROUP BY eh.EVENT_ID
						)ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis
				)mpev ON mpls.EVENT_ID = mpev.EVENT_ID						
				LEFT JOIN
				(
					SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID
		  			FROM
		  			(
						SELECT (SELECT lksu.SUCSS_ACTY_DES  + ', ' AS [text()] 
	 							FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)
	 							JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID 
	 							WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID 
	 							FOR xml PATH ('')
								) as SUCSS_ACTY_DES, EVENT_HIST_ID 
						FROM
						(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)	
						)evSucAct
		  			)evSucAct2
				)scev ON mpev.EVENT_HIST_ID = scev.EVENT_HIST_ID
				LEFT JOIN
				(
					SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID
		  			FROM
		  			(
						SELECT (SELECT lkfl.FAIL_ACTY_DES  + ', ' AS [text()] 
	 							FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)
	 							JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID 
	 							WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID 
	 							FOR xml PATH ('')
								) as FAIL_ACTY_DES, EVENT_HIST_ID 
						FROM
						(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)	
						)evFailAct
		  			)evFailAct2
				)flev ON mpev.EVENT_HIST_ID = flev.EVENT_HIST_ID			
				LEFT JOIN
				(
					SELECT distinct LEFT([MPLS_ACTY_TYPE],len([MPLS_ACTY_TYPE]) -1) as MPLS_ACTY_TYPE, EVENT_ID
					FROM
					(
						SELECT (SELECT lkma.MPLS_ACTY_TYPE_DES + ', ' AS [text()] 
								FROM COWS.dbo.MPLS_EVENT_ACTY_TYPE meac with (nolock)
								JOIN COWS.dbo.LK_MPLS_ACTY_TYPE lkma with (nolock) ON meac.MPLS_ACTY_TYPE_ID = lkma.MPLS_ACTY_TYPE_ID
								WHERE meac.EVENT_ID = mple.EVENT_ID 
								FOR xml PATH ('')
							   ) as MPLS_ACTY_TYPE, EVENT_ID 
						FROM 
						( SELECT EVENT_ID FROM COWS.dbo.MPLS_EVENT with (nolock)	
							WHERE EVENT_STUS_ID in (3, 6) 
						)
						mple
					)mpaty 
				)mpact ON mpls.EVENT_ID = mpact.EVENT_ID			
				JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mpev.CREAT_BY_USER_ID = luser1.USER_ID 
				JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON mpev.ACTN_ID = lkac.ACTN_ID
				
				JOIN COWS.dbo.LK_USER cuser with (nolock) ON mpls.CREAT_BY_USER_ID = cuser.USER_ID -- 7/5/17
				JOIN COWS.dbo.LK_USER suser with (nolock) ON suser.USER_ID = mpls.SALS_USER_ID -- 7/5/17
				
				
				WHERE mpls.STRT_TMST > DATEADD(dd,-365,GETDATE())
		UNION		
		
		SELECT DISTINCT adev.EVENT_ID 'Event_ID', 
			UPPER(evst.EVENT_STUS_DES) 'Event_Status', 		
									
			CASE WHEN adev.EVENT_TITLE_TXT is NULL THEN '' 
				 ELSE adev.EVENT_TITLE_TXT
				 END 'Item Title',	
			lkes.ENHNC_SRVC_NME 'Event_Type',							
			NULL 'Activity_Type',			
			REPLACE(SUBSTRING(CONVERT(varchar, adev.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, adev.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, adev.STRT_TMST, 109), 25, 2) 'Start_Time',
			REPLACE(SUBSTRING(CONVERT(varchar, adev.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, adev.END_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, adev.END_TMST, 109), 25, 2) 'End_Time',	
			adev.EVENT_DRTN_IN_MIN_QTY 'Event_Duration',
			adev.EXTRA_DRTN_TME_AMT 'Extra_Duration',		
			CASE	WHEN evnt.CSG_LVL_ID > 0  THEN 'Private Customer'
					WHEN adev.CUST_NME is NULL THEN '' 
					ELSE adev.CUST_NME END 'Customer_Name',	
			CASE
				WHEN lkac.ACTN_DES = 'Completed Email Notification' THEN 'Completed' 
				ELSE lkac.ACTN_DES END 'Action',			
			REPLACE(SUBSTRING(CONVERT(varchar, adhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, adhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, adhi.CREAT_DT, 109), 25, 2) 'Modified_Date',			
			CASE
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END 'Modified_By',				
			CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '' ELSE scev.SUCSS_ACTY_DES END 'Success_Activities',		
			CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '' ELSE flev.FAIL_ACTY_DES END 'Failed_Activities',
			'' 'VPN_Platform_Type',
			'' 'VAS_Type',
			fmsc.FMS_CKT_LIST 'FMS_Circuit_Info',
			
			cuser.DSPL_NME 'Requestor',
			cuser.EMAIL_ADR 'Requestor_Email',
			COWS.dbo.GetSemiColonSepAssignUsers(adev.EVENT_ID) as 'Assigned_User',
			suser.DSPL_NME 'Engineer',
			suser.EMAIL_ADR 'Engineer_Email'	
		FROM COWS.dbo.AD_EVENT adev with (nolock) 	
		    left join COWS.dbo.EVENT evnt with (nolock)  on evnt.EVENT_ID = adev.EVENT_ID 	
			JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON adev.EVENT_STUS_ID = evst.EVENT_STUS_ID
			JOIN COWS.dbo.LK_ENHNC_SRVC lkes with (nolock) ON adev.ENHNC_SRVC_ID = lkes.ENHNC_SRVC_ID
			JOIN
			(
				SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT
					FROM COWS.dbo.EVENT_HIST eh with (nolock)					
					JOIN COWS.dbo.AD_EVENT acde with (nolock) ON eh.EVENT_ID = acde.EVENT_ID
					WHERE eh.ACTN_ID IN (1,3,5,9,10,11,12,15,16,19,44) AND acde.EVENT_STUS_ID not in (6,8) 
				UNION
				SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT
					FROM COWS.dbo.EVENT_HIST ehis with (nolock)
					JOIN
					(
						SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) 'maxEvHis'
							FROM COWS.dbo.EVENT_HIST eh with (nolock)
							JOIN COWS.dbo.AD_EVENT acde with (nolock) ON eh.EVENT_ID = acde.EVENT_ID
							WHERE eh.ACTN_ID in (18) AND acde.EVENT_STUS_ID = 6 
						GROUP BY eh.EVENT_ID
					)ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis
			)adhi ON adev.EVENT_ID = adhi.EVENT_ID						
			LEFT JOIN
			(
				SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lksu.SUCSS_ACTY_DES  + ', ' AS [text()] 
	 						FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)
	 						JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID 
	 						WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID 
	 						FOR xml PATH ('')
							) as SUCSS_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)	
					)evSucAct
		  		)evSucAct2
			)scev ON adhi.EVENT_HIST_ID = scev.EVENT_HIST_ID
			LEFT JOIN
			(
				SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lkfl.FAIL_ACTY_DES  + ', ' AS [text()] 
	 						FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)
	 						JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID 
	 						WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID 
	 						FOR xml PATH ('')
							) as FAIL_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)	
					)evFailAct
		  		)evFailAct2
			)flev ON adhi.EVENT_HIST_ID = flev.EVENT_HIST_ID			
			LEFT JOIN
			(
				SELECT distinct LEFT([FMS_CKT_LIST],len([FMS_CKT_LIST]) -1) as FMS_CKT_LIST, EVENT_ID
				FROM
				(
					SELECT (SELECT adat.CKT_ID + ', ' AS [text()] 
							FROM COWS.dbo.AD_EVENT_ACCS_TAG adat with (nolock)							
							WHERE adat.EVENT_ID = acde.EVENT_ID 
							FOR xml PATH ('')
						   ) as FMS_CKT_LIST, EVENT_ID 
					FROM 
					( SELECT EVENT_ID FROM COWS.dbo.AD_EVENT with (nolock)
						WHERE EVENT_STUS_ID in (3, 6) 
					)
					acde
				)fmsci
			)fmsc ON adev.EVENT_ID = fmsc.EVENT_ID			
			JOIN COWS.dbo.LK_USER luser1 with (nolock) ON adhi.CREAT_BY_USER_ID = luser1.USER_ID 
			JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON adhi.ACTN_ID = lkac.ACTN_ID	
			
			JOIN COWS.dbo.LK_USER cuser with (nolock) ON adev.CREAT_BY_USER_ID = cuser.USER_ID -- 7/5/17
			JOIN COWS.dbo.LK_USER suser with (nolock) ON suser.USER_ID = adev.SALS_USER_ID -- 7/5/17
			
			WHERE adev.STRT_TMST > DATEADD(dd,-365,GETDATE())
	

	TRUNCATE TABLE COWS_Reporting.dbo.CANDEventAccessData
	
	INSERT INTO COWS_Reporting.dbo.CANDEventAccessData(Event_ID,CKT_ID,OLD_CKT_ID,LOC_CTY_NME,LOC_STT_NME,PL_DAL_CKT_NBR,TRNSPRT_OE_FTN_NBR,
		VAS_SOL_NBR,TRS_NET_449_ADR,MPLS_ACCS_BDWD_DES,OLD_PL_NBR,OLD_PORT_SPEED_DES,NEW_PL_NBR,
		NEW_PORT_SPEED_DES)
	(SELECT DISTINCT e.EVENT_ID, ad.CKT_ID, ad.OLD_CKT_ID, mpls.LOC_CTY_NME, mpls.LOC_STT_NME, mpls.PL_DAL_CKT_NBR, mpls.TRNSPRT_OE_FTN_NBR,
		  mpls.VAS_SOL_NBR, mpls.TRS_NET_449_ADR,ab.MPLS_ACCS_BDWD_DES,splk.OLD_PL_NBR,splk.OLD_PORT_SPEED_DES,splk.NEW_PL_NBR,
		  splk.NEW_PORT_SPEED_DES

		FROM COWS_Reporting.dbo.CANDEventData e WITH (NOLOCK) 
		LEFT OUTER JOIN COWS.dbo.AD_EVENT_ACCS_TAG ad WITH (NOLOCK) ON e.EVENT_ID = ad.EVENT_ID
		LEFT OUTER JOIN COWS.dbo.MPLS_EVENT_ACCS_TAG mpls WITH (NOLOCK)ON e.EVENT_ID = mpls.EVENT_ID
		LEFT OUTER JOIN COWS.dbo.LK_MPLS_ACCS_BDWD ab WITH (NOLOCK) ON ab.MPLS_ACCS_BDWD_ID = mpls.MPLS_ACCS_BDWD_ID 
		LEFT OUTER JOIN COWS.dbo.SPLK_EVENT_ACCS splk WITH (NOLOCK) ON e.EVENT_ID = splk.EVENT_ID)


	RETURN 0;
  
  	CLOSE SYMMETRIC KEY FS@K3y;
  
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_CANDEventTableLoad '  	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	--RETURN 1;
END CATCH

