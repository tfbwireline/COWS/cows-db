USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_TimeStudyData]    Script Date: 11/21/2019 1:25:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
----USE [COWS]
--GO
--/****** Object:  StoredProcedure [AD\xw557671].[RedesignMonthlyReport]    Script Date: 09/16/2015 07:30:59 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- [dbo].[sp_TimeStudyData] 0
---- =============================================
---- Author: Md M monir
---- Updated date: 01/23/2017
---- Description:	added Error Trap
-- =============================================
ALTER PROCEDURE [dbo].[sp_TimeStudyData]
 	@Secured			   INT=0
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY

DECLARE @startDate	Datetime=DATEADD(mm,-1,CONVERT(VARCHAR(25),DATEADD(DD,-(DAY(GETDATE())-1),GETDATE()),101))
DECLARE	@endDate	Datetime=cast((cast(SUBSTRING(CONVERT(VARCHAR(10), getdate(), 101),1,2) + '/01/' + CAST(YEAR(getdate()) AS VARCHAR(4)) as datetime) -1) AS datetime)  

OPEN SYMMETRIC KEY FS@K3y
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
SELECT    DISTINCT     CASE         
    --WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''  
    --WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'  
    --ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
    WHEN @secured = 0 and COWS.dbo.EVENT.CSG_LVL_ID =0 THEN COWS.dbo.MDS_EVENT_NEW.CUST_NME
	WHEN @secured = 0 and COWS.dbo.EVENT.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @secured = 0 and COWS.dbo.EVENT.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)), COWS.dbo.MDS_EVENT_NEW.CUST_NME,'')
    END 'Customer Name', 
					  COWS.dbo.MDS_EVENT_NEW.EVENT_ID, 
                      COWS.dbo.EVENT_HIST.CREAT_DT, COWS.dbo.LK_ACTN.ACTN_DES, ISNULL(COWS.dbo.LK_MDS_MAC_ACTY.MDS_MAC_ACTY_NME, '') AS 'MAC Activity',
                                    COWS.dbo.EVENT_HIST.CREAT_BY_USER_ID, 
                      COWS.dbo.LK_MDS_ACTY_TYPE.MDS_ACTY_TYPE_DES, COWS.dbo.MDS_FAST_TRK_TYPE.MDS_FAST_TRK_TYPE_DES, 
                      COWS.dbo.LK_APPT_TYPE.APPT_TYPE_DES, COWS.dbo.MDS_EVENT_NEW.EXTRA_DRTN_TME_AMT, 
                      COWS.dbo.MDS_EVENT_NEW.EVENT_DRTN_IN_MIN_QTY, COWS.dbo.MDS_EVENT_NEW.MNS_PM_ID, LK_USER_2.DSPL_NME AS SE, 
                      COWS.dbo.MDS_EVENT_NEW.MDS_FAST_TRK_CD, 
					'MDS Only'			AS [Network Activity Type],
					''           AS [S/C Flag]
FROM         COWS.dbo.LK_EVENT_TYPE WITH(NOLOCK) INNER JOIN
                      COWS.dbo.LK_ACTN WITH(NOLOCK) INNER JOIN
                      COWS.dbo.EVENT_HIST WITH(NOLOCK) ON COWS.dbo.LK_ACTN.ACTN_ID = COWS.dbo.EVENT_HIST.ACTN_ID INNER JOIN
                      COWS.dbo.LK_USER WITH(NOLOCK) ON COWS.dbo.LK_ACTN.MODFD_BY_USER_ID = COWS.dbo.LK_USER.USER_ID INNER JOIN
                      COWS.dbo.MDS_EVENT_NEW WITH(NOLOCK) ON COWS.dbo.EVENT_HIST.EVENT_ID = COWS.dbo.MDS_EVENT_NEW.EVENT_ID INNER JOIN
					  COWS.dbo.EVENT WITH(NOLOCK) ON COWS.dbo.EVENT.EVENT_ID = COWS.dbo.MDS_EVENT_NEW.EVENT_ID INNER JOIN
                      COWS.dbo.LK_MDS_ACTY_TYPE WITH(NOLOCK) ON 
                      COWS.dbo.MDS_EVENT_NEW.MDS_ACTY_TYPE_ID = COWS.dbo.LK_MDS_ACTY_TYPE.MDS_ACTY_TYPE_ID ON 
                      COWS.dbo.LK_EVENT_TYPE.EVENT_TYPE_ID = COWS.dbo.MDS_EVENT_NEW.EVENT_TYPE_ID LEFT OUTER JOIN
                      COWS.dbo.LK_USER AS LK_USER_2 WITH(NOLOCK) ON COWS.dbo.EVENT_HIST.CREAT_BY_USER_ID = LK_USER_2.USER_ID LEFT OUTER JOIN
                      COWS.dbo.MDS_FAST_TRK_TYPE WITH(NOLOCK) ON 
                      COWS.dbo.MDS_EVENT_NEW.MDS_FAST_TRK_TYPE_ID = COWS.dbo.MDS_FAST_TRK_TYPE.MDS_FAST_TRK_TYPE_ID LEFT OUTER JOIN
                      COWS.dbo.LK_APPT_TYPE WITH(NOLOCK) INNER JOIN
                      COWS.dbo.APPT WITH(NOLOCK) ON COWS.dbo.LK_APPT_TYPE.APPT_TYPE_ID = COWS.dbo.APPT.APPT_TYPE_ID ON 
                      COWS.dbo.MDS_EVENT_NEW.EVENT_ID = COWS.dbo.APPT.EVENT_ID    LEFT OUTER JOIN
                                    COWS.dbo.MDS_EVENT_MAC_ACTY ON 
                                    COWS.dbo.MDS_EVENT_MAC_ACTY.EVENT_ID = COWS.dbo.MDS_EVENT_NEW.EVENT_ID LEFT OUTER JOIN
                                    COWS.dbo.LK_MDS_MAC_ACTY ON COWS.dbo.LK_MDS_MAC_ACTY.MDS_MAC_ACTY_ID = COWS.dbo.MDS_EVENT_MAC_ACTY.MDS_MAC_ACTY_ID
									LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=COWS.dbo.MDS_EVENT_NEW.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
WHERE     (COWS.dbo.LK_ACTN.ACTN_DES <> 'Event Data Updated') AND (COWS.dbo.EVENT_HIST.CREAT_DT >= DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) 
                      - 1, 0)) AND (COWS.dbo.EVENT_HIST.CREAT_DT < DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0))

UNION

SELECT		DISTINCT	CASE         
						WHEN @secured = 0 and  COWS.dbo.EVENT.CSG_LVL_ID =0 THEN COWS.dbo.MDS_EVENT.CUST_NME
						WHEN @secured = 0 and COWS.dbo.EVENT.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
						WHEN @secured = 0 and COWS.dbo.EVENT.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
						ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),COWS.dbo.MDS_EVENT.CUST_NME,'')
						END 'Customer Name', 
						COWS.dbo.MDS_EVENT.EVENT_ID, 
                      COWS.dbo.EVENT_HIST.CREAT_DT, COWS.dbo.LK_ACTN.ACTN_DES, ISNULL(COWS.dbo.LK_MDS_MAC_ACTY.MDS_MAC_ACTY_NME, '') AS 'MAC Activity',
                                    COWS.dbo.EVENT_HIST.CREAT_BY_USER_ID, 
                      COWS.dbo.LK_MDS_ACTY_TYPE.MDS_ACTY_TYPE_DES, COWS.dbo.MDS_FAST_TRK_TYPE.MDS_FAST_TRK_TYPE_DES, 
                      COWS.dbo.LK_APPT_TYPE.APPT_TYPE_DES, COWS.dbo.MDS_EVENT.EXTRA_DRTN_TME_AMT, 
                      COWS.dbo.MDS_EVENT.EVENT_DRTN_IN_MIN_QTY, COWS.dbo.MDS_EVENT.MNS_PM_ID, LK_USER_2.DSPL_NME AS SE, 
                      CASE WHEN ISNULL(COWS.dbo.MDS_EVENT.MDS_FAST_TRK_TYPE_ID,'')='' THEN 0 ELSE 1 END AS MDS_FAST_TRK_CD,
					  lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type],
			CASE	
						WHEN eod.SC_CD = 'C'        THEN 'Complex'
						WHEN eod.SC_CD = 'S'        THEN 'Simple'
					    ELSE ''
						END                                     AS [S/C Flag]
FROM         COWS.dbo.LK_EVENT_TYPE WITH(NOLOCK) INNER JOIN
                      COWS.dbo.LK_ACTN WITH(NOLOCK) INNER JOIN
                      COWS.dbo.EVENT_HIST WITH(NOLOCK)  ON COWS.dbo.LK_ACTN.ACTN_ID = COWS.dbo.EVENT_HIST.ACTN_ID INNER JOIN
                      COWS.dbo.LK_USER WITH(NOLOCK) ON COWS.dbo.LK_ACTN.MODFD_BY_USER_ID = COWS.dbo.LK_USER.USER_ID INNER JOIN
                      COWS.dbo.MDS_EVENT WITH (NOLOCK) ON COWS.dbo.EVENT_HIST.EVENT_ID = COWS.dbo.MDS_EVENT.EVENT_ID INNER JOIN
					  COWS.dbo.EVENT WITH(NOLOCK) ON COWS.dbo.EVENT.EVENT_ID = COWS.dbo.MDS_EVENT.EVENT_ID INNER JOIN
                      COWS.dbo.LK_MDS_ACTY_TYPE WITH(NOLOCK) ON COWS.dbo.MDS_EVENT.MDS_ACTY_TYPE_ID = COWS.dbo.LK_MDS_ACTY_TYPE.MDS_ACTY_TYPE_ID ON 
                      COWS.dbo.LK_EVENT_TYPE.EVENT_TYPE_ID = COWS.dbo.EVENT.EVENT_TYPE_ID LEFT OUTER JOIN
                      COWS.dbo.LK_USER AS LK_USER_2 WITH(NOLOCK)  ON COWS.dbo.EVENT_HIST.CREAT_BY_USER_ID = LK_USER_2.USER_ID LEFT OUTER JOIN
                      COWS.dbo.MDS_FAST_TRK_TYPE WITH(NOLOCK) ON 
                      COWS.dbo.MDS_EVENT.MDS_FAST_TRK_TYPE_ID = COWS.dbo.MDS_FAST_TRK_TYPE.MDS_FAST_TRK_TYPE_ID LEFT OUTER JOIN
                      COWS.dbo.LK_APPT_TYPE WITH(NOLOCK) INNER JOIN
                      COWS.dbo.APPT WITH(NOLOCK) ON COWS.dbo.LK_APPT_TYPE.APPT_TYPE_ID = COWS.dbo.APPT.APPT_TYPE_ID ON 
                      COWS.dbo.MDS_EVENT.EVENT_ID = COWS.dbo.APPT.EVENT_ID    LEFT OUTER JOIN
                                    COWS.dbo.MDS_EVENT_MAC_ACTY ON 
                                    COWS.dbo.MDS_EVENT_MAC_ACTY.EVENT_ID = COWS.dbo.MDS_EVENT.EVENT_ID LEFT OUTER JOIN
                                    COWS.dbo.LK_MDS_MAC_ACTY ON COWS.dbo.LK_MDS_MAC_ACTY.MDS_MAC_ACTY_ID = COWS.dbo.MDS_EVENT_MAC_ACTY.MDS_MAC_ACTY_ID
									INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = COWS.dbo.MDS_EVENT.NTWK_ACTY_TYPE_ID
									LEFT OUTER JOIN [COWS].[dbo].[MDS_EVENT_ODIE_DEV] eod WITH (NOLOCK) ON eod.EVENT_ID = COWS.dbo.MDS_EVENT.EVENT_ID
									LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=COWS.dbo.MDS_EVENT.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
WHERE     (COWS.dbo.LK_ACTN.ACTN_DES <> 'Event Data Updated') AND (COWS.dbo.EVENT_HIST.CREAT_DT >= DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) 
                      - 1, 0)) AND (COWS.dbo.EVENT_HIST.CREAT_DT < DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0))
         
		 --print  DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) - 1, 0)
		 --Print  DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0)
		--WHERE MONTH ( r.CRETD_DT ) = Month (getDate()) AND YEAR (r.CRETD_DT)= YEAR(getDate())
		---r.CRETD_DT >=@startDate   AND r.CRETD_DT <=@endDate

UNION

SELECT	DISTINCT			 
					  '' as	[Customer Name], -- left blank per request
					 td.EVENT_ID, 
                     eh.CREAT_DT, 
                     es.EVENT_STUS_DES as ACTN_DES, 
                      'Fedline' AS 'MAC Activity',
                      1 as CREAT_BY_USER_ID, 
                      PRNT_ORDR_TYPE_DES as MDS_ACTY_TYPE_DES, 
                      'FEDLINE' as MDS_FAST_TRK_TYPE_DES, 
                      '' as APPT_TYPE_DES, 
                      0 as EXTRA_DRTN_TME_AMT, 
                      120 as EVENT_DRTN_IN_MIN_QTY, 
                      '' as MNS_PM_ID, 
                      lu.DSPL_NME AS SE, 
                      ''  AS MDS_FAST_TRK_CD,
					  ''			AS [Network Activity Type],
					''           AS [S/C Flag] 
FROM         
					  COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA td WITH (NOLOCK)
					  INNER JOIN COWS.dbo.FEDLINE_EVENT_USER_DATA ud WITH (NOLOCK) on ud.EVENT_ID = td.EVENT_ID
					  INNER JOIN COWS.dbo.LK_EVENT_STUS es WITH (NOLOCK) ON ud.EVENT_STUS_ID = es.EVENT_STUS_ID
					  INNER JOIN COWS.dbo.LK_FEDLINE_ORDR_TYPE ot WITH (NOLOCK) ON ot.ORDR_TYPE_CD = td.ORDR_TYPE_CD
					  LEFT OUTER JOIN COWS.dbo.LK_USER lu WITH (NOLOCK) on ud.ACTV_USER_ID = lu.USER_ID
					  INNER JOIN COWS.dbo.EVENT_HIST eh with (NOLOCK) on eh.EVENT_ID = td.EVENT_ID
						AND eh.ACTN_ID in (25,19)
WHERE      (eh.CREAT_DT >= DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) 
                      - 1, 0) AND eh.ACTN_ID = 25) AND (eh.CREAT_DT < DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0)AND eh.ACTN_ID = 25)
            OR
             
            (td.ORDR_TYPE_CD = 'DCS' and (eh.CREAT_DT >= DATEADD(mm, DATEDIFF(mm, 0, GETDATE()) 
                      - 1, 0) AND eh.ACTN_ID = 19) AND (eh.CREAT_DT < DATEADD(mm, DATEDIFF(mm, 0, GETDATE()), 0)AND eh.ACTN_ID = 19))          


CLOSE SYMMETRIC KEY FS@K3y


	
	END TRY
	BEGIN CATCH
		DECLARE @Desc VARCHAR(200)
		SET @Desc='EXEC COWS_Reporting.dbo.sp_TimeStudyData '+', ' 
		SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '
		EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;   --- Open later during deployment
	END CATCH
END
