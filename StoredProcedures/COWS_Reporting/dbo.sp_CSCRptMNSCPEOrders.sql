USE [COWS_Reporting]
GO
/*
--======================================================================================  
-- Project:   PJ004672 - COWS Reporting   
-- Author:   Sudarat Vongchumpit   
-- Date:   01/04/2012  
-- Description:    
--     Extract MNS CPE orders for CSC group.  
--  
--     QueryNumber: 1 = Disconnect Orders  
--         2 = Install Orders  
--  
--     FrequencyId: 1 = Daily  
--         2 = Weekly  
--         3 = Monthly  
--         0 = Specify start date & end date: 'mm/dd/yyyy'  
--  
-- Note:  
--  
--    ORDR.DMSTC_CD: 1 = International, 0 = Domestic    
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
--====================================================================================== 
*/ 
ALTER Procedure [dbo].[sp_CSCRptMNSCPEOrders]     
 @QueryNumber  INT,  
 @FrequencyId  INT=0,   
 @Secured   INT=0,  
 @inStartDtStr  VARCHAR(10)='',  
 @inEndDtStr   VARCHAR(10)=''   
  
AS  
BEGIN TRY  
   
 SET NOCOUNT ON;  
   
 DECLARE @startDate  Datetime  
 DECLARE @endDate  Datetime  
 DECLARE @today   Datetime  
 DECLARE @startDtStr  VARCHAR(10)   
 DECLARE @dayOfWeek  INT  
   
 IF @FrequencyId=0  
  BEGIN  
  
   SET @startDate=CONVERT(datetime, @inStartDtStr, 101)  
   SET @endDate=CONVERT(datetime, @inEndDtStr, 101)      
  
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @endDate=DATEADD(day, 1, @endDate)   
              
  END   
 ELSE IF @FrequencyId=1  
     BEGIN       
      -- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000  
    
   SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)  
   SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)     
     
  END  
 ELSE IF @FrequencyId=2  
  BEGIN  
   -- e.g. Run date is '2011-06-21' 3rd day of the week  
   -- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000  
     
   SET @today=GETDATE()  
   SET @dayOfWeek=DATEPART(dw, @today)  
  
   SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)  
   SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)  
      
  END  
 ELSE IF @FrequencyId=3  
  BEGIN      
     SET @today=DATEADD(MONTH,-1,GETDATE())  
       
      -- e.g. Run date is the 1st day of each month   
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000  
        
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));       
     
   SET @startDate=cast(@startDtStr AS datetime)     
   SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)  
  END  
    
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;  
   
 IF @QueryNumber=1  
 BEGIN  
   
  SELECT REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CREAT_DT,101),1), '0', '') +  
       SUBSTRING(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 2, 2) +  
       REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 4, 1), '0', '') +  
       RIGHT(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 6) 'Order Submit Date',  
    lkos.ORDR_STUS_DES 'Order Status',  
    CASE   
     WHEN ordr.DMSTC_CD = 1 THEN 'INTL'  
     ELSE 'DOM' END 'INTL/DOM',  
    fcust1.CUST_ID 'H1 ID',  
    ford.FTN 'FTN',  
     REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101),1), '0', '') +  
     SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 2, 2) +  
     REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 4, 1), '0', '') +  
     RIGHT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 6) 'Customer Commit Date',       
    CASE  
     WHEN CAST(ford.RELTD_FTN AS VARCHAR(10)) is NULL THEN ''   
     ELSE CAST(ford.RELTD_FTN AS VARCHAR(10)) END 'Related FTN',  
    ford.ORDR_SUB_TYPE_CD 'Order SubType Code',  
    ordr.H5_H6_CUST_ID 'H5/H6 Cust ID',  
    CASE  
     --WHEN ordr.DMSTC_CD = 1 THEN CASE  
     -- WHEN CONVERT(varchar(100), DecryptByKey(fcust2.CUST_NME)) is NULL THEN ''  
     -- WHEN @Secured = 0 AND ordr.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(fcust2.CUST_NME)) is NOT NULL THEN 'Private Customer'  
     -- ELSE CONVERT(varchar(100), DecryptByKey(fcust2.CUST_NME)) END  
     --WHEN ordr.DMSTC_CD = 0 THEN CASE  
     -- WHEN CONVERT(varchar(100), DecryptByKey(fcust3.CUST_NME)) is NULL THEN ''  
     -- WHEN @Secured = 0 AND ordr.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(fcust3.CUST_NME)) is NOT NULL THEN 'Private Customer'  
     -- ELSE CONVERT(varchar(100), DecryptByKey(fcust3.CUST_NME)) END 
     WHEN ordr.DMSTC_CD = 1 THEN CASE  
      WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd1.CUST_NME)),ISNULL(fcust2.CUST_NME,''))  END
     WHEN ordr.DMSTC_CD = 0 THEN CASE  
      WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd2.CUST_NME)),ISNULL(fcust3.CUST_NME,'')) END
     END 'Customer Name',
 
    CASE  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.STREET_ADR_1)) is NULL THEN ''   
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.STREET_ADR_1)) is NOT NULL THEN   
     -- CONVERT(varchar(200), DecryptByKey(oadi.STREET_ADR_1))  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(200), DecryptByKey(oadd.STREET_ADR_1)) is NULL THEN ''   
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(200), DecryptByKey(oadd.STREET_ADR_1)) is NOT NULL THEN   
     -- CONVERT(varchar(200), DecryptByKey(oadd.STREET_ADR_1)) END 'Address Line 1',  
     WHEN  ordr.DMSTC_CD = 1  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	   ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd3.STREET_ADR_1)),ISNULL(oadi.STREET_ADR_1,'')) END
      WHEN  ordr.DMSTC_CD = 0  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd4.STREET_ADR_1)),ISNULL(oadd.STREET_ADR_1,'')) END
      END 'Address Line 1',
    CASE  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.CTY_NME)) is NULL THEN ''  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.CTY_NME)) is NOT NULL THEN   
     -- CONVERT(varchar(200), DecryptByKey(oadi.CTY_NME))  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(200), DecryptByKey(oadd.CTY_NME)) is NULL THEN ''  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(200), DecryptByKey(oadd.CTY_NME)) is NOT NULL THEN  
     -- CONVERT(varchar(200), DecryptByKey(oadd.CTY_NME)) END 'City',  
      WHEN  ordr.DMSTC_CD = 1  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	   ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd3.CTY_NME)),ISNULL(oadi.CTY_NME,'')) END
      WHEN  ordr.DMSTC_CD = 0  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd4.CTY_NME)),ISNULL(oadd.CTY_NME,'')) END
	  END 'City',
    CASE   
     --WHEN ordr.DMSTC_CD = 1 AND ( CONVERT(varchar(200), DecryptByKey(oadi.PRVN_NME)) is NULL   
     -- OR CONVERT(varchar(200), DecryptByKey(oadi.PRVN_NME))  = '') THEN ''  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.PRVN_NME)) is NOT NULL THEN  
     -- CONVERT(varchar(200), DecryptByKey(oadi.PRVN_NME))  
     --WHEN ordr.DMSTC_CD = 0 AND ( CONVERT(varchar(3), DecryptByKey(oadd.STT_CD)) is NULL   
     -- OR CONVERT(varchar(3), DecryptByKey(oadd.STT_CD)) = '') THEN ''  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(3), DecryptByKey(oadd.STT_CD)) is NOT NULL THEN  
     -- CONVERT(varchar(3), DecryptByKey(oadd.STT_CD)) END 'Province Municipality/State Code', 
      
      WHEN  ordr.DMSTC_CD = 1  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	   ELSE COALESCE(CONVERT(varchar(3), DecryptByKey(csd3.STT_PRVN_NME)),ISNULL(oadi.PRVN_NME,'')) END
      WHEN  ordr.DMSTC_CD = 0  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(3), DecryptByKey(csd4.STT_PRVN_NME)),ISNULL(oadd.PRVN_NME,'')) END
	  END 'Province Municipality/State Code', 
    CASE  
     --WHEN ordr.DMSTC_CD = 1 AND ( CONVERT(varchar(12), DecryptByKey(oadi.ZIP_PSTL_CD)) is NULL   
     -- OR CONVERT(varchar(12), DecryptByKey(oadi.ZIP_PSTL_CD)) = '') THEN ''  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(12), DecryptByKey(oadi.ZIP_PSTL_CD)) is NOT NULL THEN  
     -- CONVERT(varchar(12), DecryptByKey(oadi.ZIP_PSTL_CD))  
     --WHEN ordr.DMSTC_CD = 0 AND ( CONVERT(varchar(12), DecryptByKey(oadd.ZIP_PSTL_CD)) is NULL   
     -- OR CONVERT(varchar(12), DecryptByKey(oadd.ZIP_PSTL_CD)) = '') THEN ''  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(12), DecryptByKey(oadd.ZIP_PSTL_CD)) is NOT NULL THEN  
     -- CONVERT(varchar(12), DecryptByKey(oadd.ZIP_PSTL_CD)) END 'Zip Code/Postal Code',
      
      WHEN  ordr.DMSTC_CD = 1  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	   ELSE COALESCE(CONVERT(varchar(12), DecryptByKey(csd3.ZIP_PSTL_CD)),ISNULL(oadi.ZIP_PSTL_CD,'')) END
      WHEN  ordr.DMSTC_CD = 0  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(12), DecryptByKey(csd4.ZIP_PSTL_CD)),ISNULL(oadd.ZIP_PSTL_CD,'')) END
	  END 'Zip Code/Postal Code',       
    CASE  
     WHEN ordr.DMSTC_CD = 1 AND oadi.CTRY_CD is NULL THEN ''  
     WHEN ordr.DMSTC_CD = 1 AND oadi.CTRY_CD is NOT NULL THEN oadi.CTRY_CD  
     WHEN ordr.DMSTC_CD = 0 AND oadd.CTRY_CD is NULL THEN ''  
     WHEN ordr.DMSTC_CD = 0 AND oadd.CTRY_CD is NOT NULL THEN oadd.CTRY_CD      
     END 'Country Code',  
    CASE  
     WHEN ordr.DMSTC_CD = 1 THEN ''  
     WHEN ordr.DMSTC_CD = 0 AND fcust3.CLLI_CD is NULL THEN ''   
     WHEN ordr.DMSTC_CD = 0 AND fcust3.CLLI_CD is not NULL THEN fcust3.CLLI_CD  
     END 'CLLID Code',  
    CASE  
     WHEN fdis.ODIE_DEV_NME is NULL THEN ''  
     ELSE fdis.ODIE_DEV_NME END 'Device Name',     
    CASE  
     WHEN fdis.MFR_NME = 'CISCO' THEN 'Cisco'  
     ELSE '' END 'MANUFACTURER',  
    CASE  
     WHEN ford.DISC_REAS_CD is NULL THEN ''  
     ELSE ford.DISC_REAS_CD END 'Disconnect Reason Code'  
  
  FROM COWS.dbo.FSA_ORDR ford with (nolock)      
   JOIN COWS.dbo.ORDR ordr with (nolock) ON ford.ORDR_ID = ordr.ORDR_ID   
   JOIN COWS.dbo.FSA_ORDR ford2 with (nolock) ON ordr.PRNT_ORDR_ID = ford2.ORDR_ID  
   JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON ordr.ORDR_STUS_ID = lkos.ORDR_STUS_ID   
   JOIN COWS.dbo.LK_FSA_ORDR_TYPE lkot with (nolock) ON ford.ORDR_TYPE_CD = lkot.FSA_ORDR_TYPE_CD  
   JOIN COWS.dbo.FSA_ORDR_CUST fcust1 with (nolock) ON ford.ORDR_ID = fcust1.ORDR_ID AND fcust1.CIS_LVL_TYPE = 'H1'     
   LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust2 with (nolock) ON ford.ORDR_ID = fcust2.ORDR_ID AND fcust2.CIS_LVL_TYPE = 'H5' 
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd1 WITH (NOLOCK) ON csd1.SCRD_OBJ_ID=fcust2.FSA_ORDR_CUST_ID  AND csd1.SCRD_OBJ_TYPE_ID=5   
   LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust3 with (nolock) ON ford.ORDR_ID = fcust3.ORDR_ID AND fcust3.CIS_LVL_TYPE = 'H6'  
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=fcust3.FSA_ORDR_CUST_ID  AND csd2.SCRD_OBJ_TYPE_ID=5
   LEFT JOIN COWS.dbo.ORDR_ADR oadi with (nolock) ON ordr.ORDR_ID = oadi.ORDR_ID AND oadi.CIS_LVL_TYPE = 'H5'
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd3 WITH (NOLOCK) ON csd3.SCRD_OBJ_ID=oadi.ORDR_ADR_ID  AND csd3.SCRD_OBJ_TYPE_ID=14  
   LEFT JOIN COWS.dbo.ORDR_ADR oadd with (nolock) ON ordr.ORDR_ID = oadd.ORDR_ID AND oadd.CIS_LVL_TYPE = 'H6'     
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd4 WITH (NOLOCK) ON csd4.SCRD_OBJ_ID=oadd.ORDR_ADR_ID  AND csd4.SCRD_OBJ_TYPE_ID=14
   LEFT JOIN (SELECT H5_H6_CUST_ID, ODIE_DEV_NME, CASE WHEN mdis.VNDR_NME is NULL THEN '' ELSE UPPER(mdis.VNDR_NME) END 'MFR_NME'   
         FROM COWS.dbo.MDS_EVENT_DISCO mdis with (nolock)  
         )as fdis ON CONVERT(VARCHAR,fcust2.CUST_ID) = fdis.H5_H6_CUST_ID OR CONVERT(VARCHAR,fcust3.CUST_ID) = fdis.H5_H6_CUST_ID
  WHERE ford.ORDR_TYPE_CD = 'DC' AND ford.PROD_TYPE_CD = 'CP' AND  
   ford.CREAT_DT >= @startDate AND ford.CREAT_DT < @endDate   
  ORDER BY ford.FTN  
     
 END   
 ELSE IF @QueryNumber=2  
 BEGIN  
   
  SELECT REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CREAT_DT,101),1), '0', '') +  
       SUBSTRING(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 2, 2) +  
       REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 4, 1), '0', '') +  
       RIGHT(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 6) 'Order Submit Date',  
    lkos.ORDR_STUS_DES 'Order Status',  
    CASE   
     WHEN ordr.DMSTC_CD = 1 THEN 'INTL'  
     ELSE 'DOM' END 'INTL/DOM',  
    fcust1.CUST_ID 'H1 ID',  
    ford.FTN 'FTN',  
     REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101),1), '0', '') +  
     SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 2, 2) +  
     REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 4, 1), '0', '') +  
     RIGHT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 6) 'Customer Commit Date',       
    CASE  
     WHEN CAST(ford.RELTD_FTN AS VARCHAR(10)) is NULL THEN ''   
     ELSE CAST(ford.RELTD_FTN AS VARCHAR(10)) END 'Related FTN',  
    ford.ORDR_SUB_TYPE_CD 'Order SubType Code',  
    ordr.H5_H6_CUST_ID 'H5/H6 Cust ID',  
    CASE  
     --WHEN ordr.DMSTC_CD = 1 THEN CASE  
     -- WHEN CONVERT(varchar(100), DecryptByKey(fcust2.CUST_NME)) is NULL THEN ''  
     -- WHEN @Secured = 0 AND ordr.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(fcust2.CUST_NME)) is NOT NULL THEN 'Private Customer'  
     -- ELSE CONVERT(varchar(100), DecryptByKey(fcust2.CUST_NME)) END  
     --WHEN ordr.DMSTC_CD = 0 THEN CASE  
     -- WHEN CONVERT(varchar(100), DecryptByKey(fcust3.CUST_NME)) is NULL THEN ''  
     -- WHEN @Secured = 0 AND ordr.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(fcust3.CUST_NME)) is NOT NULL THEN 'Private Customer'  
     -- ELSE CONVERT(varchar(100), DecryptByKey(fcust3.CUST_NME)) END 
     WHEN ordr.DMSTC_CD = 1 THEN CASE  
      WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd1.CUST_NME)),ISNULL(fcust2.CUST_NME,''))  END
     WHEN ordr.DMSTC_CD = 0 THEN CASE  
      WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd2.CUST_NME)),ISNULL(fcust3.CUST_NME,'')) END
     END 'Customer Name',  
    CASE  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.STREET_ADR_1)) is NULL THEN ''   
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.STREET_ADR_1)) is NOT NULL THEN   
     -- CONVERT(varchar(200), DecryptByKey(oadi.STREET_ADR_1))  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(200), DecryptByKey(oadd.STREET_ADR_1)) is NULL THEN ''   
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(200), DecryptByKey(oadd.STREET_ADR_1)) is NOT NULL THEN   
     -- CONVERT(varchar(200), DecryptByKey(oadd.STREET_ADR_1)) 
      WHEN  ordr.DMSTC_CD = 1  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	   ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd3.STREET_ADR_1)),ISNULL(oadi.STREET_ADR_1,'')) END
      WHEN  ordr.DMSTC_CD = 0  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd4.STREET_ADR_1)),ISNULL(oadd.STREET_ADR_1,'')) END
      END 'Address Line 1',  
    CASE  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.CTY_NME)) is NULL THEN ''  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.CTY_NME)) is NOT NULL THEN   
     -- CONVERT(varchar(200), DecryptByKey(oadi.CTY_NME))  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(200), DecryptByKey(oadd.CTY_NME)) is NULL THEN ''  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(200), DecryptByKey(oadd.CTY_NME)) is NOT NULL THEN  
     -- CONVERT(varchar(200), DecryptByKey(oadd.CTY_NME)) END 'City',  
      
      WHEN  ordr.DMSTC_CD = 1  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	   ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd3.CTY_NME)),ISNULL(oadi.CTY_NME,'')) END
      WHEN  ordr.DMSTC_CD = 0  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd4.CTY_NME)),ISNULL(oadd.CTY_NME,'')) END
	  END 'City',
    CASE   
     --WHEN ordr.DMSTC_CD = 1 AND ( CONVERT(varchar(200), DecryptByKey(oadi.PRVN_NME)) is NULL   
     -- OR CONVERT(varchar(200), DecryptByKey(oadi.PRVN_NME))  = '') THEN ''  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(200), DecryptByKey(oadi.PRVN_NME)) is NOT NULL THEN  
     -- CONVERT(varchar(200), DecryptByKey(oadi.PRVN_NME))  
     --WHEN ordr.DMSTC_CD = 0 AND ( CONVERT(varchar(3), DecryptByKey(oadd.STT_CD)) is NULL   
     -- OR CONVERT(varchar(3), DecryptByKey(oadd.STT_CD)) = '') THEN ''  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(3), DecryptByKey(oadd.STT_CD)) is NOT NULL THEN  
     -- CONVERT(varchar(3), DecryptByKey(oadd.STT_CD)) END 'Province Municipality/State Code',
     WHEN  ordr.DMSTC_CD = 1  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	   ELSE COALESCE(CONVERT(varchar(3), DecryptByKey(csd3.STT_PRVN_NME)),ISNULL(oadi.PRVN_NME,'')) END
      WHEN  ordr.DMSTC_CD = 0  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(3), DecryptByKey(csd4.STT_CD)),ISNULL(oadd.STT_CD,'')) END
	  END 'Province Municipality/State Code',  
    CASE  
     --WHEN ordr.DMSTC_CD = 1 AND ( CONVERT(varchar(12), DecryptByKey(oadi.ZIP_PSTL_CD)) is NULL   
     -- OR CONVERT(varchar(12), DecryptByKey(oadi.ZIP_PSTL_CD)) = '') THEN ''  
     --WHEN ordr.DMSTC_CD = 1 AND CONVERT(varchar(12), DecryptByKey(oadi.ZIP_PSTL_CD)) is NOT NULL THEN  
     -- CONVERT(varchar(12), DecryptByKey(oadi.ZIP_PSTL_CD))  
     --WHEN ordr.DMSTC_CD = 0 AND ( CONVERT(varchar(12), DecryptByKey(oadd.ZIP_PSTL_CD)) is NULL   
     -- OR CONVERT(varchar(12), DecryptByKey(oadd.ZIP_PSTL_CD)) = '') THEN ''  
     --WHEN ordr.DMSTC_CD = 0 AND CONVERT(varchar(12), DecryptByKey(oadd.ZIP_PSTL_CD)) is NOT NULL THEN  
     -- CONVERT(varchar(12), DecryptByKey(oadd.ZIP_PSTL_CD)) END 'Zip Code/Postal Code',
      
      WHEN  ordr.DMSTC_CD = 1  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	   ELSE COALESCE(CONVERT(varchar(12), DecryptByKey(csd3.ZIP_PSTL_CD)),ISNULL(oadi.ZIP_PSTL_CD,'')) END
      WHEN  ordr.DMSTC_CD = 0  THEN CASE
       WHEN @Secured = 0 AND ordr.CSG_LVL_ID >0 THEN 'Private Customer' 
	  ELSE COALESCE(CONVERT(varchar(12), DecryptByKey(csd4.ZIP_PSTL_CD)),ISNULL(oadd.ZIP_PSTL_CD,'')) END
	  END 'Zip Code/Postal Code',      
    CASE  
     WHEN ordr.DMSTC_CD = 1 AND oadi.CTRY_CD is NULL THEN ''  
     WHEN ordr.DMSTC_CD = 1 AND oadi.CTRY_CD is NOT NULL THEN oadi.CTRY_CD  
     WHEN ordr.DMSTC_CD = 0 AND oadd.CTRY_CD is NULL THEN ''  
     WHEN ordr.DMSTC_CD = 0 AND oadd.CTRY_CD is NOT NULL THEN oadd.CTRY_CD      
     END 'Country Code',  
    CASE  
     WHEN ordr.DMSTC_CD = 1 THEN ''  
     WHEN ordr.DMSTC_CD = 0 AND fcust3.CLLI_CD is NULL THEN ''   
     WHEN ordr.DMSTC_CD = 0 AND fcust3.CLLI_CD is not NULL THEN fcust3.CLLI_CD  
     END 'CLLID Code',  
    CASE  
     WHEN odie.ODIE_DEV_NME is NULL THEN ''  
     ELSE odie.ODIE_DEV_NME END 'Device Name',     
    CASE  
     WHEN fcpe.MFR_NME is NULL THEN ''  
     ELSE 'Cisco' END 'MANUFACTURER',  
    CASE  
     WHEN csct.CSC_Status is NULL THEN ''  
     ELSE csct.CSC_Status END 'CSC Maint Status'  
  
  FROM COWS.dbo.FSA_ORDR ford with (nolock)      
   JOIN COWS.dbo.ORDR ordr with (nolock) ON ford.ORDR_ID = ordr.ORDR_ID   
   JOIN COWS.dbo.FSA_ORDR ford2 with (nolock) ON ordr.PRNT_ORDR_ID = ford2.ORDR_ID  
   JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON ordr.ORDR_STUS_ID = lkos.ORDR_STUS_ID   
   JOIN COWS.dbo.LK_FSA_ORDR_TYPE lkot with (nolock) ON ford.ORDR_TYPE_CD = lkot.FSA_ORDR_TYPE_CD  
   JOIN COWS.dbo.FSA_ORDR_CUST fcust1 with (nolock) ON ford.ORDR_ID = fcust1.ORDR_ID AND fcust1.CIS_LVL_TYPE = 'H1'     
   LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust2 with (nolock) ON ford.ORDR_ID = fcust2.ORDR_ID AND fcust2.CIS_LVL_TYPE = 'H5'
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd1 WITH (NOLOCK) ON csd1.SCRD_OBJ_ID=fcust2.FSA_ORDR_CUST_ID  AND csd1.SCRD_OBJ_TYPE_ID=5       
   LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust3 with (nolock) ON ford.ORDR_ID = fcust3.ORDR_ID AND fcust3.CIS_LVL_TYPE = 'H6'
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=fcust3.FSA_ORDR_CUST_ID  AND csd2.SCRD_OBJ_TYPE_ID=5  
   LEFT JOIN COWS.dbo.ORDR_ADR oadi with (nolock) ON ordr.ORDR_ID = oadi.ORDR_ID AND oadi.CIS_LVL_TYPE = 'H5' 
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd3 WITH (NOLOCK) ON csd3.SCRD_OBJ_ID=oadi.ORDR_ADR_ID  AND csd3.SCRD_OBJ_TYPE_ID=14 
   LEFT JOIN COWS.dbo.ORDR_ADR oadd with (nolock) ON ordr.ORDR_ID = oadd.ORDR_ID AND oadd.CIS_LVL_TYPE = 'H6'
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd4 WITH (NOLOCK) ON csd4.SCRD_OBJ_ID=oadd.ORDR_ADR_ID  AND csd4.SCRD_OBJ_TYPE_ID=14     
   LEFT JOIN	COWS.dbo.FSA_MDS_EVENT_NEW fmds WITH (NOLOCK) ON	fmds.H5_H6_CUST_ID = CONVERT(VARCHAR,fcust2.CUST_ID) 
																OR	fmds.H5_H6_CUST_ID = CONVERT(VARCHAR,fcust3.CUST_ID)
   LEFT JOIN	COWS.dbo.MDS_EVENT_ODIE_DEV_NME odie WITH (NOLOCK) ON	odie.FSA_MDS_EVENT_ID	=	fmds.FSA_MDS_EVENT_ID
																	AND	odie.TAB_SEQ_NBR		=	fmds.TAB_SEQ_NBR
   LEFT JOIN (SELECT distinct ORDR_ID, MFR_NME   
       FROM COWS.dbo.FSA_ORDR_CPE_LINE_ITEM fcpe with (nolock)   
       WHERE UPPER(MFR_NME) = 'CISCO SYSTEMS'  
         )as fcpe ON ford.ORDR_ID = fcpe.ORDR_ID   
   LEFT JOIN (SELECT ORDR_ID, (lkts.TASK_NME + ' - ' + lkst.ORDR_STUS_DES ) 'CSC_Status'   
      FROM COWS.dbo.ACT_TASK atsk with (nolock)   
      JOIN COWS.dbo.LK_TASK lkts with (nolock) ON atsk.TASK_ID = lkts.TASK_ID  
      JOIN COWS.dbo.LK_ORDR_STUS lkst with (nolock) ON atsk.STUS_ID = lkst.ORDR_STUS_ID  
      WHERE atsk.TASK_ID = 400) as csct ON ford.ORDR_ID = csct.ORDR_ID  
       
  WHERE ford.ORDR_TYPE_CD = 'IN' AND ford.PROD_TYPE_CD = 'CP' AND ford.CPE_CPE_ORDR_TYPE_CD = 'MNS' AND  
   ford.CREAT_DT >= @startDate AND ford.CREAT_DT < @endDate   
  ORDER BY ford.FTN  
    
 END  
  
   
 CLOSE SYMMETRIC KEY FS@K3y;  
   
 RETURN 0;  
   
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_CSCRptMNSCPEOrders '  + CAST(@QueryNumber AS VARCHAR(2))   
 SET @Desc=@Desc + ', ' + CAST(@FrequencyId AS VARCHAR(2)) + ', ' + CAST(@Secured AS VARCHAR(2)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  

