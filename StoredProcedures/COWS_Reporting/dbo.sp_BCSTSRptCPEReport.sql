/*  
  
--==================================================================================  
-- Project:   PJ004672 - COWS Reporting   
-- Author:   Brad Settle  
-- Date:   03/16/2012  
-- Description:    
--     BCSTS CPE Tracking Report.  
--     Group Type:  GOM  
--     QueryNumber: 1 = Daily  
--         2 = Weekly  
--         3 = Monthly  
--         0 = Specify start date & end date: 'mm/dd/yyyy'  
--  
-- TEST: EXEC COWS_Reporting.dbo.sp_BCSTSRptCPEReport 1, 3  
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME  
--==================================================================================  
*/  
ALTER Procedure [dbo].[sp_BCSTSRptCPEReport]   
 @secured   BIT=0,  
 @QueryNumber  INT,  
 @inStartDtStr  VARCHAR(10)='',  
 @inEndDtStr   VARCHAR(10)=''  
AS  
BEGIN TRY  
  
 SET NOCOUNT ON;  
   
 DECLARE @startDate  DATETIME  
 DECLARE @endDate  DATETIME  
 DECLARE @today   DATETIME  
 DECLARE @startDtStr  VARCHAR(10)   
 DECLARE @dayOfWeek  INT  
   
 IF @QueryNumber=0  
  BEGIN  
   SET @startDate=CONVERT(DATETIME, @inStartDtStr, 101)  
   SET @endDate=CONVERT(DATETIME, @inEndDtStr, 101)      
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @endDate=DATEADD(day, 1, @endDate)   
  END   
 ELSE IF @QueryNumber=1  
     BEGIN       
      -- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000  
   SET @startDate=cast(CONVERT(VARCHAR(8), GETDATE(), 112) AS DATETIME)  
   SET @endDate=cast(CONVERT(VARCHAR(8), DATEADD(day, 1, GETDATE()), 112) AS DATETIME)     
  END  
 ELSE IF @QueryNumber=2  
  BEGIN  
   -- e.g. Run date is '2011-06-21' 3rd day of the week  
   -- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000  
   SET @today=GETDATE()  
   SET @dayOfWeek=DATEPART(dw, @today)  
   SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+6), @today), 112) As DATETIME)  
   SET @endDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS DATETIME)  
   --SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -7, GETDATE()), 112) As DateTime)  
   --SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS Datetime)    
  END  
 ELSE IF @QueryNumber=3  
  BEGIN      
     SET @today=DATEADD(MONTH,0,GETDATE())  
      -- e.g. Run date is the 1st day of each month   
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000  
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));       
   SET @startDate=cast(@startDtStr AS DATETIME)  
   SET @endDate=cast(CONVERT(VARCHAR(8), GETDATE()-15, 112) AS DATETIME)  
  END  
  
 --DECLARE @secured bit = 0 --Test purposes only  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
      
 SELECT DISTINCT  
  fo.FTN as [Field Tracking Number]  
  ,CASE  
   WHEN @secured = 0 and o.CSG_LVL_ID = 0 THEN si.CUST_NME   
   WHEN @secured = 0 and o.CSG_LVL_ID > 0 AND si.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
   WHEN @secured = 0 and o.CSG_LVL_ID > 0 AND si.CUST_NME2 IS NULL THEN NULL   
   ELSE COALESCE(si.CUST_NME2,ISNUll(si.CUST_NME,''))   
   END [Customer Name] --Secured  
  --,ISNULL(lkc5.CTRY_NME,'') as [Country]  
  ,CASE WHEN (h5f.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE lkc5.CTRY_NME END [Country] /*Monir 06062018*/
  --,ISNULL(h5f.CUST_CTY_NME,'') as [City] 
  ,CASE WHEN (h5f.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE h5f.CUST_CTY_NME END AS  [City] /*Monir 06062018*/ 
  ,CONVERT(VARCHAR(10),fo.CUST_WANT_DT,101) as [Order CWD]  
  ,ISNULL(fo.FSA_EXP_TYPE_CD,'') AS [EXPEDITE]  
  ,ISNULL(CPE_MSCP_CD,'') AS [Managed Services Channel Program (MSCP)]  
  ,CONVERT(VARCHAR(10),CUST_WANT_DT,101) AS [CUSTOMER WANT DATE (CWD)]  
  ,zz.CNTRC_TYPE_ID AS [PURCHASE or RENTAL] --Contract Type  
  ,zz.MRC  
  ,zz.NRC  
  ,[Date Assigned to GOM Queue]  
  ,CONVERT(VARCHAR(10),ISNULL(fogx.CREAT_DT,fogx.MODFD_DT),101) AS [Date Order Completed in GOM Queue]  
  ,ISNULL(ATLAS_WRK_ORDR_NBR,'') AS [Atlas Work Order Number]  
  ,ISNULL(focli.PLSFT_RQSTN_NBR,'') AS [People Soft Requisition Number]  
  ,zz.RQSTN_AMT AS [Requisition Amount]  
  ,CONVERT(VARCHAR(10),focli.RQSTN_DT,101) AS [Requisition Date]  
  ,ISNULL(CPE_VNDR_NME,'') AS [CPE Vendor Name]  
  ,ISNULL(CPE_EQPT_TYPE_TXT,'') AS [CPE Equipment Type]  
  ,ISNULL(focli.PRCH_ORDR_NBR,'') AS [Purchase Order Number]  
  ,CONVERT(VARCHAR(10),PRCH_ORDR_BACK_ORDR_SHIP_DT,101) AS [Purchase Order Back-Order Ship Date]  
  ,CONVERT(VARCHAR(10),VNDR_RECV_FROM_DT,101) AS [Vendor Receive-From Date]  
  ,ISNULL(VNDR_COURIER_TRK_NBR,'') AS [Tracking Number to NC Whse]  
  ,CONVERT(VARCHAR(10),PLSFT_RELS_DT,101) AS [People Soft Release Date]  
  ,ISNULL(PLSFT_RELS_ID,'') AS [People Soft Release ID]  
  ,ISNULL(PLSFT_RCPT_ID,'') AS [People Soft Reciept ID]  
  ,ISNULL(COURIER_TRK_NBR,'') AS [Courier/Tracking Number]  
  ,CONVERT(VARCHAR(10),DRTBN_SHIP_DT,101) AS [Date Shipped from Distribution Center]  
  ,ISNULL(SHPMT_TRK_NBR,'') AS [Shipping Tracking Number]  
  ,CONVERT(VARCHAR(10),CUST_DLVRY_DT,101) AS [Customer Delivery Date]  
  ,ISNULL(SHPMT_SIGN_BY_NME,'') AS [Signed By]  
 FROM COWS.dbo.ORDR o WITH (NOLOCK)  
  JOIN COWS.dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID = o.ORDR_ID  
  JOIN COWS.dbo.FSA_ORDR_GOM_XNCI fogx WITH (NOLOCK) ON fogx.ORDR_ID = o.ORDR_ID  
  LEFT OUTER JOIN COWS.dbo.FSA_ORDR_CPE_LINE_ITEM focli WITH (NOLOCK) ON focli.ORDR_ID = o.ORDR_ID    
  LEFT OUTER JOIN COWS.dbo.ORDR_ADR oa WITH (NOLOCK) ON o.ORDR_ID = oa.ORDR_ID   
  LEFT OUTER JOIN COWS.dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON fo.ORDR_ID = foc.ORDR_ID   
  LEFT OUTER JOIN COWS.dbo.LK_CTRY lkc WITH (NOLOCK) ON lkc.CTRY_CD = oa.CTRY_CD         
  LEFT OUTER JOIN COWS.dbo.FSA_ORDR_BILL_LINE_ITEM fobli WITH (NOLOCK) ON fobli.ORDR_ID = fo.ORDR_ID   
  LEFT OUTER JOIN COWS.dbo.H5_FOLDR h5f WITH (NOLOCK) ON h5f.H5_FOLDR_ID = o.H5_FOLDR_ID  
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=h5f.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6      
  LEFT OUTER JOIN COWS.dbo.LK_CTRY lkc5 WITH (NOLOCK) ON lkc5.CTRY_CD = h5f.CTRY_CD 
  LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD        
  LEFT OUTER JOIN (SELECT ORDR_ID, CONVERT(VARCHAR(10),MIN(at.CREAT_DT),101) AS [Date Assigned to GOM Queue]  
   FROM COWS.dbo.ACT_TASK at WITH (NOLOCK)  
   JOIN COWS.dbo.MAP_GRP_TASK mgt WITH (NOLOCK) ON mgt.TASK_ID=at.TASK_ID  
   WHERE mgt.GRP_ID=1 AND at.TASK_ID NOT IN (101,102)  
   GROUP BY ORDR_ID) gq ON gq.ORDR_ID = o.ORDR_ID  
  LEFT OUTER JOIN (SELECT bb.CUST_ID, bb.ORDR_ID, bb.SOI_CD, bb.CUST_NME, bb.CURR_BILL_CYC_CD    
   FROM [COWS].[dbo].[FSA_ORDR_CUST] bb WITH (NOLOCK)  
   JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.CIS_LVL_TYPE = 'H5') h5 ON h5.ORDR_ID = fo.ORDR_ID   
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, CONVERT(VARCHAR, DecryptByKey(csd.CUST_NME)) [CUST_NME], CIS_LVL_TYPE  
   FROM [COWS].[dbo].[FSA_ORDR_CUST] bb WITH (NOLOCK)  
   JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
   WHERE CIS_LVL_TYPE = 'H5') cst ON cst.ORDR_ID = fo.ORDR_ID    
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.SOI_CD, bb.CUST_NME, CONVERT(VARCHAR, DecryptByKey(csd.CUST_NME)) [CUST_NME2]  
    FROM [COWS].[dbo].[FSA_ORDR_CUST] bb WITH (NOLOCK)  
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
   WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
    FROM (  
    SELECT SOI_CD, ORDR_ID   
    FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi   
    WHERE soi.ORDR_ID = bb.ORDR_ID)) si ON si.ORDR_ID = fo.ORDR_ID   
  LEFT OUTER JOIN (SELECT ORDR_ID,CNTRC_TYPE_ID,MRC,NRC,CASE WHEN Sequence = 1 THEN RQSTN_AMT ELSE 0 END RQSTN_AMT   
   FROM (SELECT a.ORDR_ID,CNTRC_TYPE_ID,SUM(y.MRC)MRC,SUM(y.NRC)NRC,ISNULL(CAST(RQSTN_AMT as MONEY),0)RQSTN_AMT,  
     ROW_NUMBER() OVER (PARTITION BY RQSTN_AMT ORDER BY a.ORDR_ID) AS Sequence  
    FROM COWS.dbo.FSA_ORDR_GOM_XNCI a WITH (NOLOCK)   
    JOIN (SELECT ORDR_ID,CNTRC_TYPE_ID,SUM(MRC)MRC,SUM(NRC)NRC  
     FROM (SELECT a.ORDR_ID,CNTRC_TYPE_ID,ISNULL(CAST(MRC_CHG_AMT as MONEY),0) MRC,ISNULL(CAST(REPLACE(NRC_CHG_AMT,'null','') as MONEY),0) NRC  
      FROM COWS.dbo.FSA_ORDR_CPE_LINE_ITEM a WITH (NOLOCK)   
      JOIN COWS.dbo.FSA_ORDR_BILL_LINE_ITEM b WITH (NOLOCK) ON a.FSA_CPE_LINE_ITEM_ID = b.FSA_CPE_LINE_ITEM_ID  
      WHERE EQPT_TYPE_ID IN ('CLEI','SPKG','SRVC','MATRL') AND CNTRC_TYPE_ID <> ''  
     ) x GROUP BY ORDR_ID,CNTRC_TYPE_ID  
    ) y ON y.ORDR_ID = a.ORDR_ID  
    WHERE a.GRP_ID = 1  
    GROUP BY a.ORDR_ID,CNTRC_TYPE_ID,RQSTN_AMT) z   
   )zz ON zz.ORDR_ID = fo.ORDR_ID  
 WHERE fogx.GRP_ID = 1   
  AND fo.ORDR_ACTN_ID <> 3  
  AND fo.PROD_TYPE_CD = 'CP'  
  AND (fogx.CUST_DLVRY_DT >= @endDate OR fogx.CUST_DLVRY_DT IS NULL)  
    
 CLOSE SYMMETRIC KEY FS@K3y   
  
 RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_GOMRptCPEReport '  + CAST(@QueryNumber AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  