USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_LASSIE_NTE_Event_Info]    Script Date: 06/03/2019 9:08:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
--==================================================================================
-- Project:			PJ007370 - Enhance Lassie Feed
-- Author:			Arti Mashalkar
-- Date:			08/30/2012
-- Description:		
--					1.Extract Order Notes History for given OrderID
--					2.Extract MDS Events Records for given OrderID
--
-- Parameters:
--                  1. @ORDR_ID - Order ID
--                  2. @NTE_HIST_CD  = 'Y' Pull Notes History for order
--                  2. @MDS_EVENT_CD  = 'Y' Pull MDS Events for order
--
--    PJ017930 Changed to show all event info instead of just MDS events.  Requested by Nichole Hodges.
-- Updated By:		MD M MOnir
-- Date:			06/03/2019
-- Description:		
--					1.Event Title Was Decrypt eventhough wasnt Encrypted So put a is null Condition
--					2.Event Type Field Corrected
--==================================================================================
--11143383
--exec sp_Lassie_nte_Event_Info 'IN3G8R005927' ,'N','Y'
*/
ALTER procedure [dbo].[sp_LASSIE_NTE_Event_Info]
---Declare
@ORDR_ID  varchar(50)=11235884,
@NTE_HIST_CD  Char(1) = 'N' ,
@MDS_EVENT_CD  char(1) = 'Y'
AS
BEGIN TRY

SET NOCOUNT ON;

OPEN SYMMETRIC KEY FS@K3y 
    DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
    
    DECLARE @tOrdr_IDs TABLE (ORDR_ID int)
    
    --@ORDR_ID passed to Stored Proc is FTN for FSA_ORDR and ORDR_ID for IPL_ORDR
    -- Pull ORDR_ID for FSA_ORDR table using the FTN
    DECLARE @ORDER_ID int
    
      If exists ( select 1 from COWS.dbo.FSA_ORDR where FTN = @ORDR_ID )
     BEGIN
		 INSERT @tOrdr_IDs (ORDR_ID)
		(select  ORDR_ID from COWS.DBO.FSA_ORDR where FTN =  @ORDR_ID)
     END
     ELSE BEGIN
          IF ISNUMERIC(@ORDR_ID)<> 0
			BEGIN
				  INSERT @tOrdr_IDs (ORDR_ID)
					values (@ORDR_ID)
		    END
     END 
    
    
    -- Both Notes History and MDS Event Data cannot be pulled in same SP call
    IF @NTE_HIST_CD = 'Y' and @MDS_EVENT_CD = 'Y'
        BEGIN
          --return 0
          print 'here'
        END
    
    -- Notes History for the give @ORDR_ID    
    ELSE IF  @NTE_HIST_CD = 'Y' and @MDS_EVENT_CD = 'N'
        BEGIN
                        
		   select ntyp.NTE_TYPE_DES 'Note Type'
			, nte.NTE_TXT 'Note Text',usr.DSPL_NME 'Created By' 
			,nte.CREAT_DT 'Created On'
			,nte.ORDR_ID  'ORDR_ID'   
		   
		   from COWS.dbo.ORDR_NTE nte with (nolock)
			join COWS.dbo.LK_NTE_TYPE ntyp with (nolock) on ntyp.NTE_TYPE_ID = nte.NTE_TYPE_ID
			join COWS.dbo.LK_USER usr with(nolock) on usr.USER_ID = nte.CREAT_BY_USER_ID    
			where ordr_id in (SELECT ORDR_ID from  @tOrdr_IDs) order by nte.CREAT_DT desc 
   
        
        END
    
    -- MDS Events for the give @ORDR_ID    
    ELSE IF  @NTE_HIST_CD = 'N' and @MDS_EVENT_CD = 'Y'
        BEGIN
        
   -- test Ordr_Id = 1791
   -- Note:Change COWS.dbo.MDS_EVENT_NEW to MDS_EVENT in production
   
	select  mdEvnt.EVENT_ID 'Event ID','MDS' as 'Event Type', ISNULL(CONVERT(varchar, DecryptByKey(mdEvnt.EVENT_TITLE_TXT)),mdEvnt.EVENT_TITLE_TXT)  'Event Title' ,
	evStus.EVENT_STUS_DES 'Event Status',wfStus.WRKFLW_STUS_DES 'Work Flow Status',usr.DSPL_NME 'Assigned To' ,
	CreatUser.DSPL_NME 'Created By',mdEvnt.STRT_TMST 'Start Date'   from COWS.dbo.MDS_EVENT_NEW  mdEvnt with(nolock) 
	left outer  join COWS.dbo.LK_EVENT_STUS evStus with(nolock) on mdEvnt.EVENT_STUS_ID  = evStus.EVENT_STUS_ID 
	left outer join COWS.dbo.LK_WRKFLW_STUS wfStus with(nolock) on mdEvnt.WRKFLW_STUS_ID  = wfStus.WRKFLW_STUS_ID
		
	left outer join (select usrasm1.* from  COWS.dbo.EVENT_ASN_TO_USER usrasm1 
	 left outer join COWS.dbo.MDS_EVENT_NEW Evnt with(nolock) on Evnt.EVENT_ID = usrasm1.EVENT_ID  
	 where usrasm1.CREAT_DT = (select max(CREAT_DT) from (select  CREAT_DT,EVENT_ID from COWS.dbo.EVENT_ASN_TO_USER ) evDt where evDt.EVENT_ID = usrasm1.EVENT_ID ) 
	) usrasm on usrasm.EVENT_ID = mdEvnt.EVENT_ID 
		
	left outer join COWS.dbo.LK_USER usr with(nolock) on usr.USER_ID = usrasm.ASN_TO_USER_ID  
	left outer join COWS.dbo.LK_USER CreatUser with(nolock) on CreatUser.USER_ID = mdEvnt.CREAT_BY_USER_ID 
	where   mdEvnt.EVENT_ID in (select EVENT_ID from COWS.dbo.FSA_MDS_EVENT_ORDR FsaevOrdr where FsaevOrdr.ordr_id in (SELECT ORDR_ID from  @tOrdr_IDs))
	and mdEvnt.EVENT_STUS_ID in (1,2,3,4,6)
	UNION
	 select  mdEvnt.EVENT_ID 'Event ID','MDS' as 'Event Type',ISNULL(CONVERT(varchar, DecryptByKey(mdEvnt.EVENT_TITLE_TXT)),mdEvnt.EVENT_TITLE_TXT)  'Event Title' ,
	 evStus.EVENT_STUS_DES 'Event Status',wfStus.WRKFLW_STUS_DES 'Work Flow Status',usr.DSPL_NME 'Assigned To' ,
	 CreatUser.DSPL_NME 'Created By',mdEvnt.STRT_TMST 'Start Date'   from COWS.dbo.MDS_EVENT  mdEvnt with(nolock) 
	left outer  join COWS.dbo.LK_EVENT_STUS evStus with(nolock) on mdEvnt.EVENT_STUS_ID  = evStus.EVENT_STUS_ID 
	left outer join COWS.dbo.LK_WRKFLW_STUS wfStus with(nolock) on mdEvnt.WRKFLW_STUS_ID  = wfStus.WRKFLW_STUS_ID
		
	left outer join (select usrasm1.* from  COWS.dbo.EVENT_ASN_TO_USER usrasm1 
	 left outer join COWS.dbo.MDS_EVENT Evnt with(nolock) on Evnt.EVENT_ID = usrasm1.EVENT_ID  
	 where usrasm1.CREAT_DT = (select max(CREAT_DT) from (select  CREAT_DT,EVENT_ID from COWS.dbo.EVENT_ASN_TO_USER ) evDt where evDt.EVENT_ID = usrasm1.EVENT_ID ) 
	) usrasm on usrasm.EVENT_ID = mdEvnt.EVENT_ID 
		
	left outer join COWS.dbo.LK_USER usr with(nolock) on usr.USER_ID = usrasm.ASN_TO_USER_ID  
	left outer join COWS.dbo.LK_USER CreatUser with(nolock) on CreatUser.USER_ID = mdEvnt.CREAT_BY_USER_ID 
	where   mdEvnt.EVENT_ID in (select EVENT_ID from COWS.dbo.EVENT_CPE_DEV ecd where ecd.CPE_ORDR_NBR = @ORDR_ID
								UNION
								select EVENT_ID from COWS.dbo.EVENT_DEV_SRVC_MGMT eds where eds.MNS_ORDR_NBR = @ORDR_ID)
	and mdEvnt.EVENT_STUS_ID in (1,2,3,4,6)
	UNION
	 select  mdEvnt.EVENT_ID 'Event ID','UCaaS' as 'Event Type',ISNULL(CONVERT(varchar, DecryptByKey(mdEvnt.EVENT_TITLE_TXT)),mdEvnt.EVENT_TITLE_TXT)  'Event Title' ,
	 evStus.EVENT_STUS_DES 'Event Status',wfStus.WRKFLW_STUS_DES 'Work Flow Status',usr.DSPL_NME 'Assigned To' ,
	 CreatUser.DSPL_NME 'Created By',mdEvnt.STRT_TMST 'Start Date'   from COWS.dbo.UCAAS_EVENT  mdEvnt with(nolock) 
	left outer  join COWS.dbo.LK_EVENT_STUS evStus with(nolock) on mdEvnt.EVENT_STUS_ID  = evStus.EVENT_STUS_ID 
	left outer join COWS.dbo.LK_WRKFLW_STUS wfStus with(nolock) on mdEvnt.WRKFLW_STUS_ID  = wfStus.WRKFLW_STUS_ID
		
	left outer join (select usrasm1.* from  COWS.dbo.EVENT_ASN_TO_USER usrasm1 
	 left outer join COWS.dbo.MDS_EVENT Evnt with(nolock) on Evnt.EVENT_ID = usrasm1.EVENT_ID  
	 where usrasm1.CREAT_DT = (select max(CREAT_DT) from (select  CREAT_DT,EVENT_ID from COWS.dbo.EVENT_ASN_TO_USER ) evDt where evDt.EVENT_ID = usrasm1.EVENT_ID ) 
	) usrasm on usrasm.EVENT_ID = mdEvnt.EVENT_ID 
		
	left outer join COWS.dbo.LK_USER usr with(nolock) on usr.USER_ID = usrasm.ASN_TO_USER_ID  
	left outer join COWS.dbo.LK_USER CreatUser with(nolock) on CreatUser.USER_ID = mdEvnt.CREAT_BY_USER_ID 
	where   mdEvnt.EVENT_ID in (select EVENT_ID from COWS.dbo.EVENT_CPE_DEV ecd where ecd.CPE_ORDR_NBR = @ORDR_ID
								UNION
								select EVENT_ID from COWS.dbo.EVENT_DEV_SRVC_MGMT eds where eds.MNS_ORDR_NBR = @ORDR_ID)
	and mdEvnt.EVENT_STUS_ID in (1,2,3,4,6)
	
	UNION -- SIPT
	  select  mdEvnt.EVENT_ID 'Event ID','SIPT' as 'Event Type',ISNULL(CONVERT(varchar, DecryptByKey(mdEvnt.EVENT_TITLE_TXT)),mdEvnt.EVENT_TITLE_TXT)  'Event Title' ,
	 evStus.EVENT_STUS_DES 'Event Status',wfStus.WRKFLW_STUS_DES 'Work Flow Status',usr.DSPL_NME 'Assigned To' ,
	 CreatUser.DSPL_NME 'Created By',mdEvnt.CUST_REQ_ST_DT 'Start Date'   from COWS.dbo.SIPT_EVENT  mdEvnt with(nolock) 
	left outer  join COWS.dbo.LK_EVENT_STUS evStus with(nolock) on mdEvnt.EVENT_STUS_ID  = evStus.EVENT_STUS_ID 
	left outer join COWS.dbo.LK_WRKFLW_STUS wfStus with(nolock) on mdEvnt.WRKFLW_STUS_ID  = wfStus.WRKFLW_STUS_ID
		
	left outer join (select usrasm1.* from  COWS.dbo.EVENT_ASN_TO_USER usrasm1 
	 left outer join COWS.dbo.MDS_EVENT Evnt with(nolock) on Evnt.EVENT_ID = usrasm1.EVENT_ID  
	 where usrasm1.CREAT_DT = (select max(CREAT_DT) from (select  CREAT_DT,EVENT_ID from COWS.dbo.EVENT_ASN_TO_USER ) evDt where evDt.EVENT_ID = usrasm1.EVENT_ID ) 
	) usrasm on usrasm.EVENT_ID = mdEvnt.EVENT_ID 
		
	left outer join COWS.dbo.LK_USER usr with(nolock) on usr.USER_ID = usrasm.ASN_TO_USER_ID  
	left outer join COWS.dbo.LK_USER CreatUser with(nolock) on CreatUser.USER_ID = mdEvnt.CREAT_BY_USER_ID 
	where   mdEvnt.M5_ORDR_NBR  = @ORDR_ID
	and mdEvnt.EVENT_STUS_ID in (1,2,3,4,6)
	
	UNION -- Access Delivery
	  select  mdEvnt.EVENT_ID 'Event ID','AD' as 'Event Type',ISNULL(CONVERT(varchar, DecryptByKey(mdEvnt.EVENT_TITLE_TXT)),mdEvnt.EVENT_TITLE_TXT)  'Event Title' ,
	 evStus.EVENT_STUS_DES 'Event Status',wfStus.WRKFLW_STUS_DES 'Work Flow Status',usr.DSPL_NME 'Assigned To' ,
	 CreatUser.DSPL_NME 'Created By',mdEvnt.STRT_TMST 'Start Date'   from COWS.dbo.AD_EVENT  mdEvnt with(nolock) 
	left outer  join COWS.dbo.LK_EVENT_STUS evStus with(nolock) on mdEvnt.EVENT_STUS_ID  = evStus.EVENT_STUS_ID 
	left outer join COWS.dbo.LK_WRKFLW_STUS wfStus with(nolock) on mdEvnt.WRKFLW_STUS_ID  = wfStus.WRKFLW_STUS_ID
		
	left outer join (select usrasm1.* from  COWS.dbo.EVENT_ASN_TO_USER usrasm1 
	 left outer join COWS.dbo.MDS_EVENT Evnt with(nolock) on Evnt.EVENT_ID = usrasm1.EVENT_ID  
	 where usrasm1.CREAT_DT = (select max(CREAT_DT) from (select  CREAT_DT,EVENT_ID from COWS.dbo.EVENT_ASN_TO_USER ) evDt where evDt.EVENT_ID = usrasm1.EVENT_ID ) 
	) usrasm on usrasm.EVENT_ID = mdEvnt.EVENT_ID 
		
	left outer join COWS.dbo.LK_USER usr with(nolock) on usr.USER_ID = usrasm.ASN_TO_USER_ID  
	left outer join COWS.dbo.LK_USER CreatUser with(nolock) on CreatUser.USER_ID = mdEvnt.CREAT_BY_USER_ID 
	where   mdEvnt.FTN  = @ORDR_ID
	and mdEvnt.EVENT_STUS_ID in (1,2,3,4,6)
	
	UNION -- MPLS
	  select  mdEvnt.EVENT_ID 'Event ID','MPLS' as 'Event Type',ISNULL(CONVERT(varchar, DecryptByKey(mdEvnt.EVENT_TITLE_TXT)),mdEvnt.EVENT_TITLE_TXT)  'Event Title' ,
	 evStus.EVENT_STUS_DES 'Event Status',wfStus.WRKFLW_STUS_DES 'Work Flow Status',usr.DSPL_NME 'Assigned To' ,
	 CreatUser.DSPL_NME 'Created By',mdEvnt.STRT_TMST 'Start Date'   from COWS.dbo.MPLS_EVENT  mdEvnt with(nolock) 
	left outer  join COWS.dbo.LK_EVENT_STUS evStus with(nolock) on mdEvnt.EVENT_STUS_ID  = evStus.EVENT_STUS_ID 
	left outer join COWS.dbo.LK_WRKFLW_STUS wfStus with(nolock) on mdEvnt.WRKFLW_STUS_ID  = wfStus.WRKFLW_STUS_ID
		
	left outer join (select usrasm1.* from  COWS.dbo.EVENT_ASN_TO_USER usrasm1 
	 left outer join COWS.dbo.MDS_EVENT Evnt with(nolock) on Evnt.EVENT_ID = usrasm1.EVENT_ID  
	 where usrasm1.CREAT_DT = (select max(CREAT_DT) from (select  CREAT_DT,EVENT_ID from COWS.dbo.EVENT_ASN_TO_USER ) evDt where evDt.EVENT_ID = usrasm1.EVENT_ID ) 
	) usrasm on usrasm.EVENT_ID = mdEvnt.EVENT_ID 
		
	left outer join COWS.dbo.LK_USER usr with(nolock) on usr.USER_ID = usrasm.ASN_TO_USER_ID  
	left outer join COWS.dbo.LK_USER CreatUser with(nolock) on CreatUser.USER_ID = mdEvnt.CREAT_BY_USER_ID 
	where   mdEvnt.FTN  = @ORDR_ID
	and mdEvnt.EVENT_STUS_ID in (1,2,3,4,6)
	
	UNION -- NGVN
	  select  mdEvnt.EVENT_ID 'Event ID','NGVN' as 'Event Type',ISNULL(CONVERT(varchar, DecryptByKey(mdEvnt.EVENT_TITLE_TXT)),mdEvnt.EVENT_TITLE_TXT)  'Event Title' ,
	 evStus.EVENT_STUS_DES 'Event Status',wfStus.WRKFLW_STUS_DES 'Work Flow Status',usr.DSPL_NME 'Assigned To' ,
	 CreatUser.DSPL_NME 'Created By',mdEvnt.STRT_TMST 'Start Date'   from COWS.dbo.NGVN_EVENT  mdEvnt with(nolock) 
	left outer  join COWS.dbo.LK_EVENT_STUS evStus with(nolock) on mdEvnt.EVENT_STUS_ID  = evStus.EVENT_STUS_ID 
	left outer join COWS.dbo.LK_WRKFLW_STUS wfStus with(nolock) on mdEvnt.WRKFLW_STUS_ID  = wfStus.WRKFLW_STUS_ID
		
	left outer join (select usrasm1.* from  COWS.dbo.EVENT_ASN_TO_USER usrasm1 
	 left outer join COWS.dbo.MDS_EVENT Evnt with(nolock) on Evnt.EVENT_ID = usrasm1.EVENT_ID  
	 where usrasm1.CREAT_DT = (select max(CREAT_DT) from (select  CREAT_DT,EVENT_ID from COWS.dbo.EVENT_ASN_TO_USER ) evDt where evDt.EVENT_ID = usrasm1.EVENT_ID ) 
	) usrasm on usrasm.EVENT_ID = mdEvnt.EVENT_ID 
		
	left outer join COWS.dbo.LK_USER usr with(nolock) on usr.USER_ID = usrasm.ASN_TO_USER_ID  
	left outer join COWS.dbo.LK_USER CreatUser with(nolock) on CreatUser.USER_ID = mdEvnt.CREAT_BY_USER_ID 
	where   mdEvnt.FTN  = @ORDR_ID
	and mdEvnt.EVENT_STUS_ID in (1,2,3,4,6)
	
	UNION -- SPLK
	  select  mdEvnt.EVENT_ID 'Event ID','SPLK' as 'Event Type',ISNULL(CONVERT(varchar, DecryptByKey(mdEvnt.EVENT_TITLE_TXT)),mdEvnt.EVENT_TITLE_TXT)  'Event Title' ,
	 evStus.EVENT_STUS_DES 'Event Status',wfStus.WRKFLW_STUS_DES 'Work Flow Status',usr.DSPL_NME 'Assigned To' ,
	 CreatUser.DSPL_NME 'Created By',mdEvnt.STRT_TMST 'Start Date'   from COWS.dbo.SPLK_EVENT  mdEvnt with(nolock) 
	left outer  join COWS.dbo.LK_EVENT_STUS evStus with(nolock) on mdEvnt.EVENT_STUS_ID  = evStus.EVENT_STUS_ID 
	left outer join COWS.dbo.LK_WRKFLW_STUS wfStus with(nolock) on mdEvnt.WRKFLW_STUS_ID  = wfStus.WRKFLW_STUS_ID
		
	left outer join (select usrasm1.* from  COWS.dbo.EVENT_ASN_TO_USER usrasm1 
	 left outer join COWS.dbo.MDS_EVENT Evnt with(nolock) on Evnt.EVENT_ID = usrasm1.EVENT_ID  
	 where usrasm1.CREAT_DT = (select max(CREAT_DT) from (select  CREAT_DT,EVENT_ID from COWS.dbo.EVENT_ASN_TO_USER ) evDt where evDt.EVENT_ID = usrasm1.EVENT_ID ) 
	) usrasm on usrasm.EVENT_ID = mdEvnt.EVENT_ID 
		
	left outer join COWS.dbo.LK_USER usr with(nolock) on usr.USER_ID = usrasm.ASN_TO_USER_ID  
	left outer join COWS.dbo.LK_USER CreatUser with(nolock) on CreatUser.USER_ID = mdEvnt.CREAT_BY_USER_ID 
	where   mdEvnt.FTN  = @ORDR_ID
	and mdEvnt.EVENT_STUS_ID in (1,2,3,4,6)
		
     Order by mdEvnt.STRT_TMST desc   

        --select 'result'
        END
        	
		
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(300)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_LASSIE_NTE_Event_Info ' + CAST(@ORDR_ID AS VARCHAR(50))
	SET @Desc=@Desc + ', ' + CAST(@NTE_HIST_CD AS VARCHAR(4)) + ', ' + CONVERT(VARCHAR(10), @MDS_EVENT_CD, 101) 
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH


