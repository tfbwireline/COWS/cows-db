USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptMsMgrOrderTrackingReportOD]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_MNSRptMsMgrOrderTrackingReportOD')
EXEC ('CREATE PROCEDURE [dbo].[sp_MNSRptMsMgrOrderTrackingReportOD] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			06/21/2011
-- Description:		
--					Extract orders that need to be reviewed by the MNS PM.
--
-- QueryNumber: 	 
--				 	1 = Data: Priority #1 orders (A2 - E2) & Priority #2 orders (F2 - J2), 
--						including TOTALS line and All Orders TOTAL column		  								  
--				 	2 = Unused
--					3 = Data: Priority #1 Summary 
--					4 = Data: Priority #2 Summary 
--					5 = Text: "Priority Level Summary"
--					6 = Text: "Total Orders 31+ days Old"
--					7 = Text: "Total Orders 5-30 days Old"
--
-- FrequencyId: 	1 = Daily
--					2 = Weekly
--					3 = Monthly
--					4 = Specify start date & end date: 'mm/dd/yyyy'
--
--==================================================================================
ALTER Procedure [dbo].[sp_MNSRptMsMgrOrderTrackingReportOD]
	@QueryNumber		INT,
	@FrequencyId		INT=0,
	@SecuredUser  INT=0,   
	@getSensData  CHAR(1)='N',		
	@startDate		DATETIME=NULL,
	@endDate			DATETIME=NULL
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @stDt		Datetime
	DECLARE @endDt		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)	
	DECLARE @dayOfWeek		INT
	
	DECLARE @mnsPMName		VARCHAR(100)
	DECLARE @SQL			VARCHAR(500)
	
	
	IF @FrequencyId=4
    	BEGIN    	
    		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
		
			SET @stDt=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			SET @endDt=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)   
			
		END
	ELSE IF @FrequencyId=2
		BEGIN
			-- e.g. Run date is '2011-06-21' 3rd day of the week
			-- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000
			
			SET @today=GETDATE()
			SET @dayOfWeek=DATEPART(dw, @today)
			
			-- Start Date is Run Date
			--SET @stDt=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)
			SET @stDt=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)
			SET @endDt=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)
						
		END
	ELSE IF @FrequencyId=3
		BEGIN			 
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month 
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
  			
			-- Start Date is Run Date
			--SET @stDt=cast(@startDtStr AS datetime)
			SET @stDt=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			SET @endDt=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
		END
	ELSE IF @FrequencyId=1
		BEGIN

			SET @stDt=@startDate
			SET @endDt=@endDate			
			
			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			SET @endDt=DATEADD(day, 1, @endDt)
			
		END				
		
	IF @QueryNumber=1 
	  BEGIN
	  	 		 		 	
	  	TRUNCATE TABLE COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking
	  	
	  	--Populate Data table
	  	INSERT INTO COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking	  	
	  	SELECT 	FULL_NME 'MNSPM', Pending, Approved, RTSPending 'ReturnToSales',  
	  			CCDPriority, (Pending+Approved+RTSPending) 'TotalOrder'
	  	FROM
	  	(
				SELECT FULL_NME, [300] 'Pending', [1000] 'Approved', [500] 'RTSPending', CCDPriority
				FROM
				( 
					SELECT ordr.ORDR_ID, actt.TASK_ID, actt.STUS_ID,
						CASE
							WHEN luser.FULL_NME is NULL THEN 'Default Queue (No MNS PM)' 
							ELSE luser.FULL_NME END 'FULL_NME' , 
						CASE
							WHEN ordr.CUST_CMMT_DT is not NULL THEN
								CASE 
									WHEN DATEDIFF(day, ordr.CUST_CMMT_DT, @stDt) >= 31 THEN 1  
					 				WHEN (DATEDIFF(day, ordr.CUST_CMMT_DT, @stDt) >= 5 
					 				AND DATEDIFF(day, ordr.CUST_CMMT_DT, @stDt) < 31) THEN 2 END						
							WHEN ordr.CUST_CMMT_DT is NULL THEN
								CASE 
									WHEN DATEDIFF(day, ford.CUST_CMMT_DT, @stDt) >= 31 THEN 1  
					 				WHEN (DATEDIFF(day, ford.CUST_CMMT_DT, @stDt) >= 5 
					 				AND DATEDIFF(day, ford.CUST_CMMT_DT, @stDt) < 31) THEN 2 END
 						END 'CCDPriority'

					FROM  COWS.dbo.ORDR ordr with (nolock)
						JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON ordr.ORDR_ID = ford.ORDR_ID 						
						JOIN COWS.dbo.ACT_TASK actt with (nolock) ON ford.ORDR_ID = actt.ORDR_ID 							
						LEFT JOIN (SELECT distinct ORDR_ID, ASN_USER_ID 
									FROM COWS.dbo.USER_WFM_ASMT with (nolock)
									WHERE GRP_ID = 2 ) as uwasm ON ford.ORDR_ID = uwasm.ORDR_ID
						LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON uwasm.ASN_USER_ID = luser.USER_ID  
					WHERE actt.TASK_ID in (300, 500, 1000) AND actt.STUS_ID = 0 
						AND (DATEDIFF(day, ordr.CUST_CMMT_DT, @stDt ) >= 5 OR DATEDIFF(day, ford.CUST_CMMT_DT, @stDt ) >= 5)
					
				)mds1
				PIVOT
				(
					COUNT(ORDR_ID)
					FOR TASK_ID in ([300], [1000], [500])
		
				)AS pvt1
	  	
	  	)ordT
	  			
		-- Populate the entire report From A2 To J2
		
		SELECT DISTINCT msmgr.MNSPM 'Assigned Acct MNS PM', 
			CASE 
				WHEN t1.Pending is NULL THEN 0
				ELSE t1.Pending END 'New Order Review Pending', 
			CASE
				WHEN t1.Approved is NULL THEN 0
				ELSE t1.Approved END 'Bill Activation - Pending', 
			CASE
				WHEN t1.ReturnToSales is NULL THEN 0
				ELSE t1.ReturnToSales END 'Return To Sales',
			CASE
				WHEN t1.TotalOrder is NULL THEN 0
				ELSE t1.TotalOrder END 'TOTAL',
			CASE 
				WHEN t2.Pending is NULL THEN 0
				ELSE t2.Pending END 'New Order Review Pending', 
			CASE
				WHEN t2.Approved is NULL THEN 0
				ELSE t2.Approved END 'Bill Activation - Pending', 
			CASE
				WHEN t2.ReturnToSales is NULL THEN 0
				ELSE t2.ReturnToSales END 'Return To Sales',
			CASE 
				WHEN t2.TotalOrder is NULL THEN 0 
				ELSE t2.TotalOrder END 'TOTAL',
			CASE
				WHEN t1.TotalOrder is NULL AND t2.TotalOrder is NULL THEN 0
				WHEN t1.TotalOrder is NULL AND t2.TotalOrder is NOT NULL THEN t2.TotalOrder 
				WHEN t2.TotalOrder is NULL AND t1.TotalOrder is NOT NULL THEN t1.TotalOrder
				ELSE (t1.TotalOrder+t2.TotalOrder) END 'All Orders TOTAL'	 
		FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking msmgr with (nolock)
		LEFT JOIN
		(
			SELECT MNSPM, Pending, Approved, ReturnToSales, TotalOrder 
			FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock)
			WHERE CCDPriority = 1
		)t1 ON msmgr.MNSPM = t1.MNSPM 
		LEFT JOIN
		(
			SELECT MNSPM, Pending, Approved, ReturnToSales, TotalOrder 
			FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock)
			WHERE CCDPriority = 2
		)t2 ON msmgr.MNSPM = t2.MNSPM 

		UNION
   		SELECT 'TOTALS' 'Assigned Acct MNS PM', 
		  'New Order Review Pending'=(SELECT SUM(Pending) FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock) WHERE CCDPriority = 1),
		  'Bill Activation - Pending'=(SELECT SUM(Approved) FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock) WHERE CCDPriority = 1),
		  'Return To Sales'=(SELECT SUM(ReturnToSales) FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock) WHERE CCDPriority = 1),
		  'TOTAL'=(SELECT SUM(TotalOrder) FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock) WHERE CCDPriority = 1),
		  'New Order Review Pending'=(SELECT SUM(Pending) FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock) WHERE CCDPriority = 2),
		  'Bill Activation - Pending'=(SELECT SUM(Approved) FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock) WHERE CCDPriority = 2),
		  'Return To Sales'=(SELECT SUM(ReturnToSales) FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock) WHERE CCDPriority = 2),
		  'TOTAL'=(SELECT SUM(TotalOrder) FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock) WHERE CCDPriority = 2),
		  'All Orders TOTAL' = (SELECT SUM(TotalOrder) FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking with (nolock))
				
	  END
	--ELSE IF @QueryNumber=2	  
	--  BEGIN
	    		
		-- Populate Report: Priority #2 orders (F2 - I2) and "All Orders Total" column
		
		--SELECT Pending 'New Order Review Pending', Approved 'Bill Activation - Pending', ReturnToSales 'Return To Sales', TOTAL, AllOrdersTOTAL 'All Orders TOTAL' 
		--FROM
		--(
		--	SELECT TOP 100 m.MNSPM 'MNS PM', m.Pending, m.Approved, m.ReturnToSales, m.TotalOrder 'TOTAL',
		--		'AllOrdersTOTAL' = (SELECT ord.TOTAL FROM
		--		(SELECT MNSPM 'PMName', SUM(TotalOrder) 'TOTAL' FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking 
		-- 		GROUP BY MNSPM) ord WHERE ord.PMName = m.MNSPM)
		--	FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking m			
		--	WHERE m.CCDPriority = 2
		--	UNION
		--	SELECT TOP 100 'ZZ' 'MNS PM', SUM(Pending) 'Pending', SUM(Approved) 'Approved', SUM(ReturnToSales) 'ReturnToSales', 
		--		SUM(TotalOrder) 'TOTAL',
		--		 (SELECT ord.TOTAL FROM
		--		(SELECT SUM(TotalOrder) 'TOTAL' FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking ) ord)
		--	FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking
  		--	WHERE CCDPriority = 2
		--	ORDER BY 'MNS PM'
			
		--)tbl
		
	  --END
	ELSE IF @QueryNumber = 3
		BEGIN
		
			-- Populate 1st TOTAL Cell
		
			SELECT SUM(TotalOrder)
  				FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking
  				WHERE CCDPriority = 1
		
		END
	ELSE IF @QueryNumber = 4
		BEGIN
		
			-- Populate 2nd TOTAL Cell
		
			SELECT SUM(TotalOrder)
  				FROM COWS_Reporting.dbo.Data_MNS_MsMgrOrderTracking
  				WHERE CCDPriority = 2
		
		END		
	ELSE IF	@QueryNumber IN (5, 6, 7, 8,9)
		BEGIN
		
			SET @SQL=CASE @QueryNumber
        		WHEN 5 THEN 'SELECT ''Priority # 1 (Orders 31+ days Old)'''
        		WHEN 6 THEN 'SELECT ''Priority # 2 (Orders 5-30 days Old)'''
        		WHEN 7 THEN 'SELECT ''Priority Level Summary'''
        		WHEN 8 THEN 'SELECT ''Total Orders 31+ days Old'''
        		WHEN 9 THEN 'SELECT ''Total Orders 31+ days Old'''
        		END
      		EXEC (@SQL)		
		END
		
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(300)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptMsMgrOrderTrackingReportOD ' + CAST(@QueryNumber AS VARCHAR(4))
	SET @Desc=@Desc + ', ' + CAST(@FrequencyId AS VARCHAR(4)) + ', ' + CONVERT(VARCHAR(10), @stDt, 101) 
	SET @Desc=@Desc + ', ' + CONVERT(VARCHAR(10), @endDt, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH





GO
