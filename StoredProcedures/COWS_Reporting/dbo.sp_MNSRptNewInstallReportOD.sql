USE [COWS_Reporting]
GO
/*
--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			07/20/2011
-- Description:		
--					Extract device and vendor model of newly installed devices that
--					have been completed via Fasttrack or MDS scheduled events for 
--					specified parameter(s); 
--					Customer Name, H5, H6, Odie Device Name, Date Range.
--
-- 09/25/2011		sxv0766: Added @h5_h6_ID, @getSensData
-- 11/14/2011		sxv0766: Added @SecuredUser
-- 03/29/2012		sxv0766: Increased field sizes: Odie_Dev_Name to VARCHAR(100), 
--					Vendor_Model to VARCHAR(120)
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--==================================================================================
*/

ALTER Procedure [dbo].[sp_MNSRptNewInstallReportOD]
	@SecuredUser		INT=0,
	@getSensData		CHAR(1)='N',
	@startDate			Datetime=NULL,
	@endDate			Datetime=NULL
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDateStr	VARCHAR(20)
	DECLARE @endDateStr		VARCHAR(20)	
	
	SET @startDate=CONVERT(DATETIME, @startDate, 101)  
    SET @endDate=CONVERT(DATETIME, @endDate, 101)      
    -- Add 1 day to End Date in order to cover the End Date 24-hr period  
    SET @endDate=DATEADD(DAY, 1, @endDate)
    
    
	DECLARE  @tmp_mns_newInst TABLE
	(					
		Event_Status			VARCHAR(50),
		Event_Id				INT,
		Cust_Name				VARCHAR(100),
		Cust_Name2				VARCHAR(100),
		Scurd_Cd				Bit NULL,
		Chars_Id				VARCHAR(20),
		Start_Time				VARCHAR(25),
		End_Time				VARCHAR(25),
		Odie_Dev_Name			VARCHAR(100),
		Assigned_To				VARCHAR(100),
		Team_Email				VARCHAR(200),
		Vendor_Model			VARCHAR(120),
		MDS_Acty_Type			VARCHAR(25),
		MDS_Inst_Acty			VARCHAR(100),
		MDS_Mac_Acty			VARCHAR(100),
		Assigned_MNS_PM			VARCHAR(100)
	)
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	-- MDS Activity: 1 = Initial, 2 = MAC : Event in 'Completed' Status 
	
	INSERT INTO @tmp_mns_newInst(Event_Status, Event_Id, Cust_Name, Chars_Id, Start_Time, End_Time, Odie_Dev_Name, 
				Assigned_To, Team_Email, Vendor_Model, MDS_Acty_Type, MDS_Inst_Acty, MDS_Mac_Acty, Assigned_MNS_PM)
			SELECT	UPPER(evst.EVENT_STUS_DES)'Event Status', 
			mds.EVENT_ID 'Event Id', 
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME))
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				 
				END 'Customer Name',			
			mds.CHARS_ID 'Chars Id',
			mds.STRT_TMST 'Start Time',
			--REPLACE(SUBSTRING(CONVERT(varchar, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Start Time',			
			mds.END_TMST 'End Time',
			--REPLACE(SUBSTRING(CONVERT(varchar, mds.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 25, 2) 'End Time',			
			odie.ODIE_DEV_NME AS  'ODIE Device Name',
			CASE				
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END 'Assigned To',		
			CASE
				--WHEN CONVERT(varchar(200), DecryptByKey(mds.CUST_EMAIL_ADR)) is NULL THEN ''
				--ELSE CONVERT(varchar(200), DecryptByKey(mds.CUST_EMAIL_ADR)) 
				--END 'Team Email',
			
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_EMAIL_ADR
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)),mds.CUST_EMAIL_ADR,'')
				END 'Team Email',	
			( RTRIM(LTRIM(lman.MANF_NME)) + ' ' + RTRIM(LTRIM(lmdl.DEV_MODEL_NME))) 'Vendor Model',
			
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
			 CASE  
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'   
				ELSE '' END 'MDS Install Activity',
			CASE 
				WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
				ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 'Assigned Acct MNS PM'
								
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
		JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN (SELECT EVENT_ID, CREAT_DT, CREAT_BY_USER_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE ACTN_ID = 17 ) as evhi ON mds.EVENT_ID = evhi.EVENT_ID
		--LEFT JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
		--			FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
		--			WHERE REC_STUS_ID = 1
		--			GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID
		--LEFT JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
		--	AND lasg.maxRecTmst = easg.CREAT_DT				
		--LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON easg.ASN_TO_USER_ID = luser1.USER_ID		
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON evhi.CREAT_BY_USER_ID = luser1.USER_ID
		--LEFT JOIN COWS.dbo.MDS_EVENT_MNGD_DEV mdev with (nolock) ON fmds.FSA_MDS_EVENT_ID = mdev.FSA_MDS_EVENT_ID 
		LEFT JOIN COWS.dbo.MDS_EVENT_ODIE_DEV_NME odie WITH (NOLOCK) ON odie.FSA_MDS_EVENT_ID = fmds.FSA_MDS_EVENT_ID
		LEFT JOIN COWS.dbo.LK_DEV_MANF lman with (nolock) ON odie.MANF_ID = lman.MANF_ID
		LEFT JOIN COWS.dbo.LK_DEV_MODEL lmdl with (nolock) ON odie.DEV_MODEL_ID = lmdl.DEV_MODEL_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID			 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID				
						
	-- MDS Activity: 1 = Initial, 2 = MAC : Event in 'Completed' Status  	
	WHERE	mds.MDS_ACTY_TYPE_ID in (1, 2) AND mds.EVENT_STUS_ID in (6) 
		AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT)  AND evhi.CREAT_DT <COALESCE(@endDate,evhi.CREAT_DT)
		
		
		UNION ALL
		
		SELECT	UPPER(evst.EVENT_STUS_DES)'Event Status', 
			mds.EVENT_ID 'Event Id', 
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name',			
			mds.CHARS_ID 'Chars Id',
			mds.STRT_TMST 'Start Time',
			--REPLACE(SUBSTRING(CONVERT(varchar, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Start Time',			
			mds.END_TMST 'End Time',
			--REPLACE(SUBSTRING(CONVERT(varchar, mds.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 25, 2) 'End Time',			
			odie.ODIE_DEV_NME AS  'ODIE Device Name',
			CASE				
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END 'Assigned To',		
			CASE
				--WHEN CONVERT(varchar(200), DecryptByKey(mds.SHIP_CUST_EMAIL_ADR)) is NULL THEN ''    /* SHIP_CUST_EMAIL_ADR  Verify */
				--ELSE CONVERT(varchar(200), DecryptByKey(mds.SHIP_CUST_EMAIL_ADR))
				
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID =0 THEN mds.SHIP_CUST_EMAIL_ADR
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)),mds.SHIP_CUST_EMAIL_ADR,'') 
				END 'Team Email',  /* SHIP_CUST_EMAIL_ADR  Verify */
			( RTRIM(LTRIM(lman.MANF_NME)) + ' ' + RTRIM(LTRIM(lmdl.DEV_MODEL_NME))) 'Vendor Model', 
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
			 CASE  
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'   
				ELSE '' END 'MDS Install Activity',
			CASE 
				WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
				ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 'Assigned Acct MNS PM'
								
	FROM	COWS.dbo.MDS_EVENT mds with (nolock) 
		--JOIN COWS.dbo.FSA_MDS_EVENT fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
		LEFT JOIN (SELECT EVENT_ID, CREAT_DT, CREAT_BY_USER_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE ACTN_ID = 17 ) as evhi ON mds.EVENT_ID = evhi.EVENT_ID
		--LEFT JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
		--			FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
		--			WHERE REC_STUS_ID = 1
		--			GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID
		--LEFT JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
		--	AND lasg.maxRecTmst = easg.CREAT_DT				
		--LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON easg.ASN_TO_USER_ID = luser1.USER_ID		
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON evhi.CREAT_BY_USER_ID = luser1.USER_ID
		--LEFT JOIN COWS.dbo.MDS_EVENT_MNGD_DEV mdev with (nolock) ON fmds.FSA_MDS_EVENT_ID = mdev.FSA_MDS_EVENT_ID 
		--LEFT JOIN COWS.dbo.MDS_EVENT_ODIE_DEV_NME odie WITH (NOLOCK) ON odie.FSA_MDS_EVENT_ID = fmds.FSA_MDS_EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_ODIE_DEV odie WITH (NOLOCK) ON odie.EVENT_ID = mds.EVENT_ID
		LEFT JOIN COWS.dbo.LK_DEV_MANF lman with (nolock) ON odie.MANF_ID = lman.MANF_ID
		LEFT JOIN COWS.dbo.LK_DEV_MODEL lmdl with (nolock) ON odie.DEV_MODEL_ID = lmdl.DEV_MODEL_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID			 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID				
						
	-- MDS Activity: 1 = Initial, 2 = MAC : Event in 'Completed' Status  	
	WHERE	mds.MDS_ACTY_TYPE_ID in (1, 2) AND mds.EVENT_STUS_ID in (6)
		AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT)  AND evhi.CREAT_DT <COALESCE(@endDate,evhi.CREAT_DT)
		
	
			
	SELECT 	Event_Status 'Event Status',
			Event_Id 'Event Id',
			CASE 
				----WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				----WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				----ELSE Cust_Name 
				WHEN @SecuredUser = 0 and Scurd_Cd =0 THEN CUST_NAME
				WHEN @SecuredUser = 0 and Scurd_Cd >0 AND  CUST_NAME2 <>'' THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd >0 AND CUST_NAME2 <> '' THEN 'Private Customer'
				ELSE COALESCE(CUST_NAME2,CUST_NAME,'')
				END 'Customer Name',
			Chars_Id 'Chars Id',
			Start_Time 'Start Time',
			End_Time 'End Time',
			Odie_Dev_Name 'ODIE Device Name',
			Assigned_To	'Assigned To',
			Team_Email 'Team Email',
			Vendor_Model 'Vendor Model',
			MDS_Acty_Type 'MDS Activity Type',
			MDS_Inst_Acty 'MDS Install Activity',
			MDS_Mac_Acty  'MDS MAC Activity',
			Assigned_MNS_PM	'Assigned Acct MNS PM'
	FROM @tmp_mns_newInst
		
	
	
	CLOSE SYMMETRIC KEY FS@K3y; 
	
	RETURN 0;
  
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptNewInstallReportOD, '  
	SET @Desc=@Desc +  CAST(@SecuredUser AS VARCHAR) + ',''' + @getSensData + ''', '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH