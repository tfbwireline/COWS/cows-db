/*  
--=======================================================================================  
-- Project:   PJ004672 - COWS Reporting   
-- Author:   Sudarat Vongchumpit   
-- Date:   04/30/2012  
-- Description:    
--     Extract ENCI monthly report categorized by order activity types and   
--     order statuses.  
--       
-- QueryNumber:    
--     0 = Populate staging table  
--       = Query: Summary field C3; Number of orders installed  
--      1 = Dataset:  SELECT Installed orders              
--      2 = Dataset:  SELECT New orders (currently NOT in use )  
--     3 = Dataset: SELECT Pending Install orders (Snapshot)  
--     4 = Dataset: SELECT Pending Disconnect orders (Snapshot)  
--     5 = Dataset: SELECT Disconnected orders  
--     6 = Query:  Summary field C3 (for secured report ONLY)  
--     7, 8, 9, 10, 11 = Query: Summary fields for "Reported Month" column (C5, C7, C10, C12, C14)    
--     12, 13, 14, 15, 16, 17 = Query: Summary fields for "YTD" column   
--     19 = Query: Summary field C16; Reported Month - Pending installs   
--     20 = Query: Summary field C18; Reported Month - Pending disconnects       
--  
-- Notes:     
--     RGN_ID: 2 = ENCI  
--       
--     ORDR_TYPE_CD: BC = Billing Change, DC = Disconnect  
--  
-- Modifications:  
-- 09/19/2012  bts4947: added FSA CCD per client request   
--  
-- 05/17/2012  sxv0766: Initial deployment  
--  
-- 03/37/2013 ci554013: changed fields that displayed date and time to display only date.  
--  
-- 12/10/2013 ci554013: Added 3 new columns to end of report.(Standard Interval Date,Standard Interval Reason,STDI Comments)  
--      and added joins in queries(1,3,4,5) for new columns.  
--   
-- 06/25/2014 ci554013: Added column to display "Last updated By" in pending installs   
--      and pending disconnects..  
-- 07/15/2014   dlp0278:  Changed Assigned users to show all ENCI users assigned by calling     
--        function COWS.dbo.getCommaSeparatedUsers.  
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD  
-- [dbo].[sp_ENCIRptMonthlyReport]1, 1 
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME

-- Update date: 02/06/2019
-- Updated by: Md M Monir  
-- Description: Include Target Delivery Date   Some bug Fixed Count(*)   chnaged to Count(DISTINCT FTN) 
--=======================================================================================  
*/  
ALTER PROCEDURE [dbo].[sp_ENCIRptMonthlyReport]   
  @QueryNumber  INT,  
   @Secured      INT=0,  
   @inStartDtStr  VARCHAR(10)='',  
   @inEndDtStr   VARCHAR(10)=''  
AS  
  
BEGIN TRY  
  
 SET NOCOUNT ON;  
  
 DECLARE @startDate  Datetime  
 DECLARE @endDate     Datetime  
 DECLARE @today   Datetime  
 DECLARE @startDtStr  VARCHAR(10)   
   DECLARE @SQL      VARCHAR(1000)  
     
   DECLARE @updSQL   VARCHAR(500)  
  DECLARE @updSQL1     VARCHAR(100)  
     
   DECLARE @installed_cnt INT  
   DECLARE @met_ccd_pct    INT     
   DECLARE @total_mrc_inst INT  
   DECLARE @disconn_cnt    INT  
   DECLARE @total_mrc_disc INT  
   --DECLARE @new_ord_rcvd INT  
   DECLARE @disconn_rcvd INT  
   --DECLARE @miss_ccd_cnt INT  
   --DECLARE @avg_days_late INT  
     
   DECLARE @im    INT  
   DECLARE @rpt_mon  INT  
     
   DECLARE @a_mon   VARCHAR(3)  
   DECLARE @rpt_mon_nme VARCHAR(3)  
   DECLARE @rpt_year  VARCHAR(4)  
   DECLARE @mons_ytd  VARCHAR(100)   
     
   IF @inStartDtStr <> '' AND @inEndDtStr <> ''  
    BEGIN  
  
   SET @startDate=CONVERT(datetime, @inStartDtStr, 101)  
   SET @endDate=CONVERT(datetime, @inEndDtStr, 101)     
     
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @endDate=DATEADD(day, 1, @endDate)      
      
    END  
   ELSE  
    BEGIN  
     SET @today=DATEADD(MONTH,-1,GETDATE())  
       
      -- e.g. Run date is the 1st day of each month   
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000  
        
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));       
   SET @startDate=cast(@startDtStr AS datetime)  
   SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)  
  END  
    
 SET @rpt_mon_nme = SUBSTRING(CONVERT(VARCHAR, @startDate, 107), 1, 3)  
 SET @rpt_mon = MONTH(@startDate)    
 SET @rpt_year = YEAR(@startDate)  
   
 PRINT @startDate  
 PRINT @endDate  
 --================================  
 -- Prepare Year To Date month list  
 --================================  
 -- e.g.@mons_ytd value is: Jan+Feb+Mar+Apr  
    
 IF @QueryNumber IN (12, 13, 14, 15, 16, 17)  
 BEGIN  
  
  SET @im=1  
  SET @mons_ytd=''  
  WHILE ( @im <= @rpt_mon)  
  BEGIN  
   SET @a_mon = CASE @im  
    WHEN 1 THEN 'Jan'  
    WHEN 2 THEN 'Feb'  
    WHEN 3 THEN 'Mar'  
    WHEN 4 THEN 'Apr'  
    WHEN 5 THEN 'May'  
    WHEN 6 THEN 'Jun'  
    WHEN 7 THEN 'Jul'  
    WHEN 8 THEN 'Aug'  
    WHEN 9 THEN 'Sep'  
    WHEN 10 THEN 'Oct'  
    WHEN 11 THEN 'Nov'  
    WHEN 12 THEN 'Dec'  
   END   
   
   IF @im=1   
    SET @mons_ytd = @a_mon  
   ELSE  
    SET @mons_ytd += '+' + @a_mon  
    
   SET @im += 1  
  END   
   
 END  
    
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;  
   
 IF @QueryNumber=0  
  BEGIN  
      
   --==================  
   -- Orders: Installed  
   --==================  
     
   TRUNCATE TABLE COWS_Reporting.dbo.Data_ENCI_Monthly  
     
   INSERT INTO COWS_Reporting.dbo.Data_ENCI_Monthly ( FTN, NUA, Private_Line_No, Cust_Name, Scurd_Cd, Product, H5_City,    
     H5_Country, Speed_Service, Ordr_Type_CD, Ordr_Status_ID, MRC, NRC, Contract_Signed_Dt, Submitted_Dt, Ordr_CWD,   
     Cust_Commit_Dt, FSA_CCD, Instl_Dscnct_Dt, Ordr_Note, Vendor_Name, Assigned_User, Svc_Req_Type, GOM_Received_Ordr)  
   SELECT distinct  
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN Convert(varchar(50), ordr.ORDR_ID)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN ford.FTN  
    WHEN ordr.ORDR_CAT_ID = 2 THEN ford.FTN   
    WHEN ordr.ORDR_CAT_ID = 4 THEN Convert(varchar(50), ordr.ORDR_ID)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN Convert(varchar(50), ordr.ORDR_ID)  
    END 'FTN',  
   CASE  
    WHEN nrm.NUA_ADR is NULL THEN ''  
    ELSE nrm.NUA_ADR END 'NUA',    
   CASE    
    WHEN ordr.ORDR_CAT_ID = 1 THEN   
     CASE WHEN ipod.PRE_XST_PL_NBR is NULL THEN '' ELSE ipod.PRE_XST_PL_NBR END  
    WHEN ordr.ORDR_CAT_ID = 4 THEN   
     CASE WHEN ipod.PRE_XST_PL_NBR is NULL THEN '' ELSE ipod.PRE_XST_PL_NBR END  
    WHEN ordr.ORDR_CAT_ID = 5 THEN   
     CASE WHEN ipod.PRE_XST_PL_NBR is NULL THEN '' ELSE ipod.PRE_XST_PL_NBR END  
    WHEN ordr.ORDR_CAT_ID = 6 THEN   
     CASE WHEN nrm.PLN_NME is NULL THEN '' ELSE nrm.PLN_NME END  
    WHEN ordr.ORDR_CAT_ID = 2 THEN   
     CASE WHEN nrm.PLN_NME is NULL THEN '' ELSE nrm.PLN_NME END  
    END 'Private_Line_No',  
   --CASE   
   -- WHEN ordr.CSG_LVL_ID =0 THEN h5fd.CUST_NME  
   -- WHEN ordr.CSG_LVL_ID >0 THEN 'Private Customer'  
   -- END AS CUST_NME,  
   CASE  
    WHEN @secured = 0 and ordr.CSG_LVL_ID =0 THEN h5fd.CUST_NME  
    WHEN @secured = 0 and ordr.CSG_LVL_ID >0 AND h5fd.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
    WHEN @secured = 0 and ordr.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL   
    ELSE coalesce(h5fd.CUST_NME,CONVERT(varchar, DecryptByKey(csd.CUST_NME)),'') END CUST_NME, --Secured  
  
  
   CASE WHEN ordr.CSG_LVL_ID>0 THEN 1 ELSE 0 END Scurd_Cd,  
   CASE  
    WHEN ordr.ORDR_CAT_ID = 1 THEN lpty2.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 4 THEN lpty2.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 5 THEN lpty2.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 6 THEN lpty1.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 2 THEN lpty1.PROD_TYPE_DES END 'Product',   
   --CASE  
   -- WHEN h5fd.CUST_CTY_NME is NULL THEN ''  
   -- ELSE h5fd.CUST_CTY_NME END 'H5_City',
   CASE WHEN (h5fd.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE h5fd.CUST_CTY_NME END AS  [H5_City], /*Monir 06062018*/  
   --CASE  
   -- WHEN lctry.CTRY_NME is NULL THEN ''  
   -- ELSE lctry.CTRY_NME END 'H5_Country', 
   CASE WHEN (h5fd.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE lctry.CTRY_NME END [H5_Country], /*Monir 06062018*/    
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN   
     CASE WHEN ipod.CKT_SPD_QTY is NULL THEN '' ELSE CONVERT(varchar(20), ipod.CKT_SPD_QTY) END  
    WHEN ordr.ORDR_CAT_ID = 4 THEN   
     CASE WHEN ipod.CKT_SPD_QTY is NULL THEN '' ELSE CONVERT(varchar(20), ipod.CKT_SPD_QTY) END  
    WHEN ordr.ORDR_CAT_ID = 5 THEN   
     CASE WHEN ipod.CKT_SPD_QTY is NULL THEN '' ELSE CONVERT(varchar(20), ipod.CKT_SPD_QTY) END   
    WHEN ordr.ORDR_CAT_ID = 6 THEN   
     CASE WHEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES is NULL THEN '' ELSE fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES END  
    WHEN ordr.ORDR_CAT_ID = 2 THEN   
     CASE WHEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES is NULL THEN '' ELSE fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES END        
    END 'Speed_Service',  
   ford.ORDR_TYPE_CD 'Ordr_Type_CD',  
   ordr.ORDR_STUS_ID 'Ordr_Status_ID',  
   CASE  
    WHEN ordr.ORDR_CAT_ID = 2 THEN  
     ISNULL(CAST(blit.MRC_CHG_AMT AS MONEY), 0)   
    WHEN ordr.ORDR_CAT_ID = 6 THEN  
     ISNULL(CAST(ford.TPORT_CALC_RT_MRC_USD_AMT AS MONEY), 0)  
   END 'MRC',  
   CASE  
    WHEN ordr.ORDR_CAT_ID = 2 THEN   
     ISNULL(CAST(blit.NRC_CHG_AMT AS MONEY), 0)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN  
     ISNULL(CAST(ford.TPORT_CALC_RT_NRC_USD_AMT AS MONEY), 0)    
   END 'NRC',     
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipod.CUST_CNTRC_SIGN_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ipod.CUST_CNTRC_SIGN_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ipod.CUST_CNTRC_SIGN_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ford.CUST_SIGNED_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ford.CUST_SIGNED_DT, 101) END 'Contract_Signed_Dt' ,  
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN ipod.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 4 THEN ipod.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 5 THEN ipod.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 6 THEN ford.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 2 THEN ford.CREAT_DT END 'Submitted_Dt',  
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101)  
    --WHEN ordr.ORDR_CAT_ID = 1 THEN ISNULL( CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101), 'N/A')  
     --CASE WHEN ipod.CUST_ORDR_SBMT_DT is NULL THEN '' ELSE ipod.CUST_ORDR_SBMT_DT END  
    WHEN ordr.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ford.CUST_WANT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ford.CUST_WANT_DT, 101)  
    --WHEN ordr.ORDR_CAT_ID = 2 THEN ISNULL( CONVERT(VARCHAR(10), ford.CUST_WANT_DT, 101), 'N/A')  
     --CASE WHEN ford.CUST_WANT_DT is NULL THEN '' ELSE ford.CUST_WANT_DT END   
    END 'Ordr_CWD',     
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipod.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ipod.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ipod.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ford.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ford.CUST_CMMT_DT, 101)  END 'Cust_Commit_Dt',     
   CASE   
    WHEN chst.NEW_CCD_DT is NULL THEN NULL  
    WHEN chst.NEW_CCD_DT is NOT NULL THEN CONVERT(VARCHAR(10), chst.NEW_CCD_DT, 101) END 'FSA_CCD',     
   CASE   
    WHEN ltsk.CREAT_DT is NULL THEN ''   
    ELSE ltsk.CREAT_DT END 'Instl_Dscnct_Dt',  
   'N/A' 'Ordr_Note',      
   CASE  
    WHEN lvdr.VNDR_NME is NULL THEN ''  
    ELSE lvdr.VNDR_NME END 'Vendor_Name',    
   CASE  
    WHEN lusr.FULL_NME is NULL THEN ''  
    ELSE [COWS].[dbo].getCommaSeparatedUsers(ordr.ORDR_ID,7,0) END 'Assigned_User',      
    -- 7/15/14: ELSE lusr.FULL_NME END 'Assigned_User',     
   CASE  
    WHEN ordr.ORDR_CAT_ID = 2 THEN loty1.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 6 THEN loty1.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 4 THEN loty2.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 5 THEN loty2.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 1 THEN loty2.ORDR_TYPE_DES END 'Svc_Req_Type',  
   CASE   
    WHEN agtk.gomRcvDt is NULL THEN ''   
    ELSE agtk.gomRcvDt END 'GOM_Received_Ordr'  
    
  FROM COWS.dbo.ORDR ordr with (nolock)  
   LEFT JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON ordr.ORDR_ID = ford.ORDR_ID  
   LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = ford.ORDR_ID  
   LEFT JOIN COWS.dbo.IPL_ORDR ipod with (nolock) ON ordr.ORDR_ID = ipod.ORDR_ID  
   LEFT JOIN (SELECT distinct FTN, FMS_CKT_ID, NUA_ADR, PLN_NME  
        FROM COWS.dbo.NRM_CKT with (nolock)) nrm ON ford.FTN = nrm.FTN   
     LEFT JOIN COWS.dbo.LK_PROD_TYPE lpty1 with (nolock) ON ford.PROD_TYPE_CD = lpty1.FSA_PROD_TYPE_CD  
     LEFT JOIN COWS.dbo.LK_PROD_TYPE lpty2 with (nolock) ON ipod.PROD_TYPE_ID = lpty2.PROD_TYPE_ID  
     LEFT JOIN COWS.dbo.H5_FOLDR h5fd with (nolock) ON ordr.H5_FOLDR_ID = h5fd.H5_FOLDR_ID  
     LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=h5fd.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6  
     LEFT JOIN COWS.dbo.LK_CTRY lctry with (nolock) ON h5fd.CTRY_CD = lctry.CTRY_CD 
     LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD
        
    LEFT JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON ordr.ORDR_STUS_ID = lkos.ORDR_STUS_ID     
   LEFT JOIN(SELECT lnit.ORDR_ID, SUM(ISNULL(CAST(REPLACE(lnit.MRC_CHG_AMT,'null','') AS MONEY),0)) MRC_CHG_AMT, SUM(ISNULL(CAST(REPLACE(lnit.NRC_CHG_AMT,'null','') AS MONEY),0)) NRC_CHG_AMT    
           FROM COWS.dbo.FSA_ORDR_BILL_LINE_ITEM lnit with (nolock)  
           JOIN COWS.dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = lnit.ORDR_ID   
           GROUP by lnit.ORDR_ID) blit ON ordr.ORDR_ID = blit.ORDR_ID              
    LEFT JOIN COWS.dbo.ORDR_MS ordms with (nolock) ON ordr.ORDR_ID = ordms.ORDR_ID    
   LEFT JOIN (SELECT ccdh.ORDR_ID, ccdh.NEW_CCD_DT, ccdh.CCD_HIST_ID   
      FROM COWS.dbo.CCD_HIST ccdh with(nolock)   
      JOIN COWS.dbo.ORDR ordr with (nolock) ON ccdh.ORDR_ID = ordr.ORDR_ID    
            WHERE ccdh.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)   
                   FROM (SELECT ORDR_ID, CCD_HIST_ID   
                     FROM COWS.dbo.CCD_HIST with (nolock) ) chis   
                   WHERE chis.ORDR_ID = ccdh.ORDR_ID AND ordr.ORDR_STUS_ID = 2)  
               )chst on ordr.ORDR_ID = chst.ORDR_ID      
      
   JOIN (SELECT acts.ACT_TASK_ID, acts.TASK_ID, acts.CREAT_DT, acts.ORDR_ID  
     FROM COWS.dbo.ACT_TASK acts with (nolock)       
     LEFT JOIN COWS.dbo.ORDR ordr with (nolock) ON acts.ORDR_ID = ordr.ORDR_ID  
     WHERE acts.TASK_ID = 1001 AND acts.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)   
            FROM (SELECT ACT_TASK_ID, ORDR_ID  
              FROM COWS.dbo.ACT_TASK with (nolock) )mxts  
             WHERE mxts.ORDR_ID = acts.ORDR_ID AND ordr.ORDR_STUS_ID = 2)        
     )ltsk ON ordr.ORDR_ID = ltsk.ORDR_ID     
   LEFT JOIN(SELECT distinct vndo.TRMTG_CD, vndo.ORDR_ID, vndo.VNDR_FOLDR_ID   
      FROM COWS.dbo.VNDR_ORDR vndo with (nolock)  
      JOIN COWS.dbo.ORDR oder with (nolock) ON vndo.ORDR_ID = oder.ORDR_ID   
      WHERE vndo.TRMTG_CD = 1  
       )vndr ON ordr.ORDR_ID = vndr.ORDR_ID   
   LEFT JOIN COWS.dbo.VNDR_FOLDR vfdr ON vndr.VNDR_FOLDR_ID = vfdr.VNDR_FOLDR_ID  
   LEFT JOIN COWS.dbo.LK_VNDR lvdr ON vfdr.VNDR_CD = lvdr.VNDR_CD     
   LEFT JOIN (SELECT distinct uwas.ASMT_DT, uwas.ORDR_ID, uwas.ASN_USER_ID, uwas.GRP_ID  
        FROM COWS.dbo.USER_WFM_ASMT uwas with (nolock)  
        LEFT JOIN COWS.dbo.FSA_ORDR fsao with (nolock) ON uwas.ORDR_ID = fsao.ORDR_ID   
        WHERE uwas.ASMT_DT = (SELECT MAX(ASMT_DT)  
            FROM (SELECT distinct ASMT_DT, ORDR_ID   
              FROM COWS.dbo.USER_WFM_ASMT with (nolock))uswf    
               WHERE uwas.ORDR_ID = uswf.ORDR_ID )  
      )uwfa ON ordr.ORDR_ID =  uwfa.ORDR_ID  
    LEFT JOIN COWS.dbo.LK_USER lusr on uwfa.ASN_USER_ID = lusr.USER_ID  
      
    LEFT JOIN COWS.dbo.LK_ORDR_TYPE loty1 with (nolock) ON ford.ORDR_TYPE_CD = loty1.FSA_ORDR_TYPE_CD  
    LEFT JOIN COWS.dbo.LK_ORDR_TYPE loty2 with (nolock) ON ipod.ORDR_TYPE_ID = loty2.ORDR_TYPE_ID  
     
   --LEFT JOIN (SELECT ORDR_ID, CONVERT(VARCHAR(10), MIN(atsk.CREAT_DT), 101) as gomRcvDt   
   LEFT JOIN (SELECT ORDR_ID, MIN(atsk.CREAT_DT) as gomRcvDt  
      FROM COWS.dbo.ACT_TASK atsk with (nolock)        
      JOIN COWS.dbo.MAP_GRP_TASK mgtk with (nolock) ON mgtk.TASK_ID = atsk.TASK_ID        
      WHERE mgtk.GRP_ID = 1 AND atsk.TASK_ID NOT IN (101, 102)        
        GROUP BY ORDR_ID  
            )agtk ON ford.ORDR_ID = agtk.ORDR_ID  
             
  -- RGN_ID: 2 = ENCI Region, ORDR_STUS_ID: 2 = Complete  
   WHERE ltsk.CREAT_DT >= @startdate and  ltsk.CREAT_DT < @enddate AND ordr.RGN_ID = 2 AND      
    (ford.ORDR_TYPE_CD <> 'DC' OR ipod.ORDR_TYPE_ID <> 7) AND COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),h5fd.CUST_NME,'')<>''  
    
  UNION    
   --=====================  
   -- Orders: Disconnected  
   --=====================     
  
   SELECT distinct  
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN Convert(varchar(50), ordr.ORDR_ID)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN ford.FTN  
    WHEN ordr.ORDR_CAT_ID = 2 THEN ford.FTN   
    WHEN ordr.ORDR_CAT_ID = 4 THEN Convert(varchar(50), ordr.ORDR_ID)  
    END 'FTN',  
   CASE  
    WHEN nrm.NUA_ADR is NULL THEN ''  
    ELSE nrm.NUA_ADR END 'NUA',    
   CASE    
    WHEN ordr.ORDR_CAT_ID = 1 THEN   
     CASE WHEN ipod.PRE_XST_PL_NBR is NULL THEN '' ELSE ipod.PRE_XST_PL_NBR END  
    WHEN ordr.ORDR_CAT_ID = 4 THEN   
     CASE WHEN ipod.PRE_XST_PL_NBR is NULL THEN '' ELSE ipod.PRE_XST_PL_NBR END  
    WHEN ordr.ORDR_CAT_ID = 5 THEN   
     CASE WHEN ipod.PRE_XST_PL_NBR is NULL THEN '' ELSE ipod.PRE_XST_PL_NBR END  
    WHEN ordr.ORDR_CAT_ID = 6 THEN   
     CASE WHEN nrm.PLN_NME is NULL THEN '' ELSE nrm.PLN_NME END  
    WHEN ordr.ORDR_CAT_ID = 2 THEN   
     CASE WHEN nrm.PLN_NME is NULL THEN '' ELSE nrm.PLN_NME END  
    END 'Private_Line_No',  
   --CASE   
   -- WHEN ordr.CSG_LVL_ID =0 THEN h5fd.CUST_NME  
   -- WHEN ordr.CSG_LVL_ID >0 THEN 'Private Customer'  
   -- END AS CUST_NME,  
   CASE  
    WHEN @secured = 0 and ordr.CSG_LVL_ID =0 THEN h5fd.CUST_NME  
    WHEN @secured = 0 and ordr.CSG_LVL_ID >0 AND h5fd.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
    WHEN @secured = 0 and ordr.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL   
    ELSE coalesce(h5fd.CUST_NME,CONVERT(varchar, DecryptByKey(csd.CUST_NME)),'') END CUST_NME, --Secured  
   CASE WHEN ordr.CSG_LVL_ID>0 THEN 1 ELSE 0 END Scurd_Cd,    
   CASE  
    WHEN ordr.ORDR_CAT_ID = 1 THEN lpty2.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 4 THEN lpty2.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 5 THEN lpty2.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 6 THEN lpty1.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 2 THEN lpty1.PROD_TYPE_DES END 'Product',   
   --CASE  
   -- WHEN h5fd.CUST_CTY_NME is NULL THEN ''  
   -- ELSE h5fd.CUST_CTY_NME END 'H5_City', 
   CASE WHEN (h5fd.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE h5fd.CUST_CTY_NME END AS  [H5_City], /*Monir 06062018*/   
   --CASE  
   -- WHEN lctry.CTRY_NME is NULL THEN ''  
   -- ELSE lctry.CTRY_NME END 'H5_Country',
    CASE WHEN (h5fd.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE lctry.CTRY_NME END [H5_Country], /*Monir 06062018*/      
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN   
     CASE WHEN ipod.CKT_SPD_QTY is NULL THEN '' ELSE CONVERT(varchar(20), ipod.CKT_SPD_QTY) END  
    WHEN ordr.ORDR_CAT_ID = 4 THEN   
     CASE WHEN ipod.CKT_SPD_QTY is NULL THEN '' ELSE CONVERT(varchar(20), ipod.CKT_SPD_QTY) END  
    WHEN ordr.ORDR_CAT_ID = 5 THEN   
     CASE WHEN ipod.CKT_SPD_QTY is NULL THEN '' ELSE CONVERT(varchar(20), ipod.CKT_SPD_QTY) END    
    WHEN ordr.ORDR_CAT_ID = 6 THEN   
     CASE WHEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES is NULL THEN '' ELSE fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES END  
    WHEN ordr.ORDR_CAT_ID = 2 THEN   
     CASE WHEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES is NULL THEN '' ELSE fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES END        
    END 'Speed_Service',  
   ford.ORDR_TYPE_CD 'Ordr_Type_CD',  
   ordr.ORDR_STUS_ID 'Ordr_Status_ID',  
   CASE  
    WHEN ordr.ORDR_CAT_ID = 2 THEN  
     ISNULL(CAST(blit.MRC_CHG_AMT AS MONEY), 0)   
    WHEN ordr.ORDR_CAT_ID = 6 THEN  
     ISNULL(CAST(ford.TPORT_CALC_RT_MRC_USD_AMT AS MONEY), 0)  
   END 'MRC',  
   CASE  
    WHEN ordr.ORDR_CAT_ID = 2 THEN   
     ISNULL(CAST(blit.NRC_CHG_AMT AS MONEY), 0)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN  
     ISNULL(CAST(ford.TPORT_CALC_RT_NRC_USD_AMT AS MONEY), 0)    
   END 'NRC',     
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipod.CUST_CNTRC_SIGN_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ipod.CUST_CNTRC_SIGN_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ipod.CUST_CNTRC_SIGN_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ford.CUST_SIGNED_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ford.CUST_SIGNED_DT, 101) END 'Contract_Signed_Dt' ,  
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN ipod.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 4 THEN ipod.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 5 THEN ipod.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 6 THEN ford.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 2 THEN ford.CREAT_DT END 'Submitted_Dt',  
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101)  
    --WHEN ordr.ORDR_CAT_ID = 1 THEN ISNULL( CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101), 'N/A')  
     --CASE WHEN ipod.CUST_ORDR_SBMT_DT is NULL THEN '' ELSE ipod.CUST_ORDR_SBMT_DT END  
    WHEN ordr.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ford.CUST_WANT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ford.CUST_WANT_DT, 101)  
    --WHEN ordr.ORDR_CAT_ID = 2 THEN ISNULL( CONVERT(VARCHAR(10), ford.CUST_WANT_DT, 101), 'N/A')  
     --CASE WHEN ford.CUST_WANT_DT is NULL THEN '' ELSE ford.CUST_WANT_DT END   
    END 'Ordr_CWD',     
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipod.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ipod.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ipod.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ford.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ford.CUST_CMMT_DT, 101)  END 'Cust_Commit_Dt',     
   CASE   
    WHEN chst.NEW_CCD_DT is NULL THEN NULL  
    WHEN chst.NEW_CCD_DT is NOT NULL THEN CONVERT(VARCHAR(10), chst.NEW_CCD_DT, 101) END 'FSA_CCD',     
   CASE  
    WHEN ordms.ORDR_DSCNCT_DT is NULL  THEN ''  
    ELSE ordms.ORDR_DSCNCT_DT END 'Instl_Dscnct_Dt',  
   'N/A' 'Ordr_Note',  
   CASE  
    WHEN lvdr.VNDR_NME is NULL THEN ''  
    ELSE lvdr.VNDR_NME END 'Vendor_Name',    
   CASE  
    WHEN lusr.FULL_NME is NULL THEN ''  
    ELSE [COWS].[dbo].getCommaSeparatedUsers(ordr.ORDR_ID,7,0) END 'Assigned_User',  
    -- 7/15/14: ELSE lusr.FULL_NME END 'Assigned_User',     
   CASE  
    WHEN ordr.ORDR_CAT_ID = 2 THEN loty1.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 6 THEN loty1.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 1 THEN loty2.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 4 THEN loty2.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 5 THEN loty2.ORDR_TYPE_DES END 'Svc_Req_Type',  
   CASE   
    WHEN agtk.gomRcvDt is NULL THEN ''   
    ELSE agtk.gomRcvDt END 'GOM_Received_Ordr'  
    
  FROM COWS.dbo.ORDR ordr with (nolock)  
   LEFT JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON ordr.ORDR_ID = ford.ORDR_ID  
   LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = ford.ORDR_ID  
   LEFT JOIN COWS.dbo.IPL_ORDR ipod with (nolock) ON ordr.ORDR_ID = ipod.ORDR_ID  
   LEFT JOIN (SELECT distinct FTN, FMS_CKT_ID, NUA_ADR, PLN_NME  
        FROM COWS.dbo.NRM_CKT with (nolock)) nrm ON ford.FTN = nrm.FTN   
     LEFT JOIN COWS.dbo.LK_PROD_TYPE lpty1 with (nolock) ON ford.PROD_TYPE_CD = lpty1.FSA_PROD_TYPE_CD  
     LEFT JOIN COWS.dbo.LK_PROD_TYPE lpty2 with (nolock) ON ipod.PROD_TYPE_ID = lpty2.PROD_TYPE_ID  
     LEFT JOIN COWS.dbo.H5_FOLDR h5fd with (nolock) ON ordr.H5_FOLDR_ID = h5fd.H5_FOLDR_ID  
     LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=h5fd.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6  
     LEFT JOIN COWS.dbo.LK_CTRY lctry with (nolock) ON h5fd.CTRY_CD = lctry.CTRY_CD 
     LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD   
    LEFT JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON ordr.ORDR_STUS_ID = lkos.ORDR_STUS_ID     
   LEFT JOIN(SELECT lnit.ORDR_ID, SUM(ISNULL(CAST(REPLACE(lnit.MRC_CHG_AMT,'null','') AS MONEY),0)) MRC_CHG_AMT, SUM(ISNULL(CAST(REPLACE(lnit.NRC_CHG_AMT,'null','') AS MONEY),0)) NRC_CHG_AMT   
           FROM COWS.dbo.FSA_ORDR_BILL_LINE_ITEM lnit with (nolock)  
           JOIN COWS.dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = lnit.ORDR_ID   
           GROUP by lnit.ORDR_ID) blit ON ordr.ORDR_ID = blit.ORDR_ID              
    LEFT JOIN COWS.dbo.ORDR_MS ordms with (nolock) ON ordr.ORDR_ID = ordms.ORDR_ID     
   LEFT JOIN (SELECT ccdh.ORDR_ID, ccdh.NEW_CCD_DT, ccdh.CCD_HIST_ID   
      FROM COWS.dbo.CCD_HIST ccdh with(nolock)   
      JOIN COWS.dbo.ORDR ordr with (nolock) ON ccdh.ORDR_ID = ordr.ORDR_ID    
            WHERE ccdh.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)   
                   FROM (SELECT ORDR_ID, CCD_HIST_ID   
                     FROM COWS.dbo.CCD_HIST with (nolock) ) chis   
                   WHERE chis.ORDR_ID = ccdh.ORDR_ID AND ordr.ORDR_STUS_ID = 5)  
               )chst on ordr.ORDR_ID = chst.ORDR_ID      
   JOIN (SELECT acts.ACT_TASK_ID, acts.TASK_ID, acts.CREAT_DT, acts.ORDR_ID  
     FROM COWS.dbo.ACT_TASK acts with (nolock)       
     LEFT JOIN COWS.dbo.ORDR ordr with (nolock) ON acts.ORDR_ID = ordr.ORDR_ID  
     WHERE acts.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)   
            FROM (SELECT ACT_TASK_ID, ORDR_ID  
              FROM COWS.dbo.ACT_TASK with (nolock) )mxts  
             WHERE mxts.ORDR_ID = acts.ORDR_ID AND ordr.ORDR_STUS_ID = 5)        
     )ltsk ON ordr.ORDR_ID = ltsk.ORDR_ID     
   LEFT JOIN ( SELECT distinct vndo.TRMTG_CD, vndo.ORDR_ID, vndo.VNDR_FOLDR_ID   
      FROM COWS.dbo.VNDR_ORDR vndo with (nolock)  
      JOIN COWS.dbo.ORDR oder with (nolock) ON vndo.ORDR_ID = oder.ORDR_ID   
      WHERE vndo.TRMTG_CD = 1  
       )vndr ON ordr.ORDR_ID = vndr.ORDR_ID   
   LEFT JOIN COWS.dbo.VNDR_FOLDR vfdr ON vndr.VNDR_FOLDR_ID = vfdr.VNDR_FOLDR_ID  
   LEFT JOIN COWS.dbo.LK_VNDR lvdr ON vfdr.VNDR_CD = lvdr.VNDR_CD     
   LEFT JOIN (SELECT distinct uwas.ASMT_DT, uwas.ORDR_ID, uwas.ASN_USER_ID, uwas.GRP_ID  
        FROM COWS.dbo.USER_WFM_ASMT uwas with (nolock)  
        LEFT JOIN COWS.dbo.FSA_ORDR fsao with (nolock) ON uwas.ORDR_ID = fsao.ORDR_ID   
        WHERE uwas.ASMT_DT = (SELECT MAX(ASMT_DT)  
            FROM (SELECT distinct ASMT_DT, ORDR_ID   
              FROM COWS.dbo.USER_WFM_ASMT with (nolock))uswf    
               WHERE uwas.ORDR_ID = uswf.ORDR_ID )  
      )uwfa ON ordr.ORDR_ID =  uwfa.ORDR_ID  
    LEFT JOIN COWS.dbo.LK_USER lusr on uwfa.ASN_USER_ID = lusr.USER_ID  
      
    LEFT JOIN COWS.dbo.LK_ORDR_TYPE loty1 with (nolock) ON ford.ORDR_TYPE_CD = loty1.FSA_ORDR_TYPE_CD  
    LEFT JOIN COWS.dbo.LK_ORDR_TYPE loty2 with (nolock) ON ipod.ORDR_TYPE_ID = loty2.ORDR_TYPE_ID  
     
   --LEFT JOIN (SELECT ORDR_ID, CONVERT(VARCHAR(10), MIN(atsk.CREAT_DT), 101) as gomRcvDt  
   LEFT JOIN (SELECT ORDR_ID, MIN(atsk.CREAT_DT) as gomRcvDt  
      FROM COWS.dbo.ACT_TASK atsk with (nolock)        
      JOIN COWS.dbo.MAP_GRP_TASK mgtk with (nolock) ON mgtk.TASK_ID = atsk.TASK_ID        
      WHERE mgtk.GRP_ID = 1 AND atsk.TASK_ID NOT IN (101, 102)        
           GROUP BY ORDR_ID  
            )agtk ON ford.ORDR_ID = agtk.ORDR_ID  
             
  -- RGN_ID: 2 = ENCI Region, ORDR_STUS_ID: 5 = Disconnected  
   WHERE ordms.ORDR_DSCNCT_DT >= @startdate and ordms.ORDR_DSCNCT_DT < @enddate AND  
    ordr.RGN_ID = 2 AND ford.ORDR_TYPE_CD = 'DC' AND COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),h5fd.CUST_NME,'')<>''  
       
  UNION  
    
   --================  
   -- Orders: Pending  
   --================  
  SELECT distinct  
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN Convert(varchar(50), ordr.ORDR_ID)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN ford.FTN   
    WHEN ordr.ORDR_CAT_ID = 2 THEN ford.FTN  
    WHEN ordr.ORDR_CAT_ID = 4 THEN Convert(varchar(50), ordr.ORDR_ID)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN Convert(varchar(50), ordr.ORDR_ID)  
    END 'FTN',  
   CASE  
    WHEN nrm.NUA_ADR is NULL THEN ''  
    ELSE nrm.NUA_ADR END 'NUA',    
   CASE    
    WHEN ordr.ORDR_CAT_ID = 1 THEN   
     CASE WHEN ipod.PRE_XST_PL_NBR is NULL THEN '' ELSE ipod.PRE_XST_PL_NBR END  
    WHEN ordr.ORDR_CAT_ID = 4 THEN   
     CASE WHEN ipod.PRE_XST_PL_NBR is NULL THEN '' ELSE ipod.PRE_XST_PL_NBR END  
    WHEN ordr.ORDR_CAT_ID = 5 THEN   
     CASE WHEN ipod.PRE_XST_PL_NBR is NULL THEN '' ELSE ipod.PRE_XST_PL_NBR END  
    WHEN ordr.ORDR_CAT_ID = 6 THEN   
     CASE WHEN nrm.PLN_NME is NULL THEN '' ELSE nrm.PLN_NME END  
    WHEN ordr.ORDR_CAT_ID = 2 THEN   
     CASE WHEN nrm.PLN_NME is NULL THEN '' ELSE nrm.PLN_NME END  
    END 'Private_Line_No',  
   --CASE   
   -- WHEN ordr.CSG_LVL_ID =0 THEN h5fd.CUST_NME  
   -- WHEN ordr.CSG_LVL_ID >0 THEN 'Private Customer'  
   -- END AS CUST_NME,  
   CASE  
    WHEN @secured = 0 and ordr.CSG_LVL_ID =0 THEN h5fd.CUST_NME  
    WHEN @secured = 0 and ordr.CSG_LVL_ID >0 AND h5fd.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
    WHEN @secured = 0 and ordr.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL   
    ELSE coalesce(h5fd.CUST_NME,CONVERT(varchar, DecryptByKey(csd.CUST_NME)),'') END CUST_NME, --Secured  
   CASE WHEN ordr.CSG_LVL_ID>0 THEN 1 ELSE 0 END Scurd_Cd,  
   CASE  
    WHEN ordr.ORDR_CAT_ID = 1 THEN lpty2.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 4 THEN lpty2.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 5 THEN lpty2.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 6 THEN lpty1.PROD_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 2 THEN lpty1.PROD_TYPE_DES END 'Product',   
   --CASE  
   -- WHEN h5fd.CUST_CTY_NME is NULL THEN ''  
   -- ELSE h5fd.CUST_CTY_NME END 'H5_City',
   CASE WHEN (h5fd.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE h5fd.CUST_CTY_NME END AS  [H5_City], /*Monir 06062018*/    
   --CASE  
   -- WHEN lctry.CTRY_NME is NULL THEN ''  
   -- ELSE lctry.CTRY_NME END 'H5_Country',
   CASE WHEN (h5fd.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE lctry.CTRY_NME END [H5_Country], /*Monir 06062018*/      
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN   
     CASE WHEN ipod.CKT_SPD_QTY is NULL THEN '' ELSE CONVERT(varchar(20), ipod.CKT_SPD_QTY) END  
    WHEN ordr.ORDR_CAT_ID = 4 THEN   
     CASE WHEN ipod.CKT_SPD_QTY is NULL THEN '' ELSE CONVERT(varchar(20), ipod.CKT_SPD_QTY) END  
    WHEN ordr.ORDR_CAT_ID = 5 THEN   
     CASE WHEN ipod.CKT_SPD_QTY is NULL THEN '' ELSE CONVERT(varchar(20), ipod.CKT_SPD_QTY) END    
    WHEN ordr.ORDR_CAT_ID = 6 THEN   
     CASE WHEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES is NULL THEN '' ELSE fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES END  
    WHEN ordr.ORDR_CAT_ID = 2 THEN   
     CASE WHEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES is NULL THEN '' ELSE fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES END        
    END 'Speed_Service',  
   ford.ORDR_TYPE_CD 'Ordr_Type_CD',  
   ordr.ORDR_STUS_ID 'Ordr_Status_ID',  
   CASE  
    WHEN ordr.ORDR_CAT_ID = 2 THEN  
     ISNULL(CAST(blit.MRC_CHG_AMT AS MONEY), 0)   
    WHEN ordr.ORDR_CAT_ID = 6 THEN  
     ISNULL(CAST(ford.TPORT_CALC_RT_MRC_USD_AMT AS MONEY), 0)  
   END 'MRC',  
   CASE  
    WHEN ordr.ORDR_CAT_ID = 2 THEN   
     ISNULL(CAST(blit.NRC_CHG_AMT AS MONEY), 0)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN  
     ISNULL(CAST(ford.TPORT_CALC_RT_NRC_USD_AMT AS MONEY), 0)    
   END 'NRC',     
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipod.CUST_CNTRC_SIGN_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ipod.CUST_CNTRC_SIGN_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ipod.CUST_CNTRC_SIGN_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ford.CUST_SIGNED_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ford.CUST_SIGNED_DT, 101) END 'Contract_Signed_Dt' ,  
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN ipod.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 4 THEN ipod.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 5 THEN ipod.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 6 THEN ford.CREAT_DT  
    WHEN ordr.ORDR_CAT_ID = 2 THEN ford.CREAT_DT END 'Submitted_Dt',  
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101)  
    --WHEN ordr.ORDR_CAT_ID = 1 THEN ISNULL( CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101), 'N/A')  
     --CASE WHEN ipod.CUST_ORDR_SBMT_DT is NULL THEN '' ELSE ipod.CUST_ORDR_SBMT_DT END  
    WHEN ordr.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ipod.CUST_ORDR_SBMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ford.CUST_WANT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ford.CUST_WANT_DT, 101)  
    --WHEN ordr.ORDR_CAT_ID = 2 THEN ISNULL( CONVERT(VARCHAR(10), ford.CUST_WANT_DT, 101), 'N/A')  
     --CASE WHEN ford.CUST_WANT_DT is NULL THEN '' ELSE ford.CUST_WANT_DT END   
    END 'Ordr_CWD',     
   CASE   
    WHEN ordr.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipod.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ipod.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ipod.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ford.CUST_CMMT_DT, 101)  
    WHEN ordr.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ford.CUST_CMMT_DT, 101)  END 'Cust_Commit_Dt',     
   CASE   
    WHEN chst.NEW_CCD_DT is NULL THEN NULL  
    WHEN chst.NEW_CCD_DT is NOT NULL THEN CONVERT(VARCHAR(10), chst.NEW_CCD_DT, 101) END 'FSA_CCD',     
   CASE   
    WHEN ltsk.CREAT_DT is NULL THEN ''   
    ELSE ltsk.CREAT_DT END 'Instl_Dscnct_Dt',  
   odnt.NTE_TXT 'Ordr_Note',  
   CASE  
    WHEN lvdr.VNDR_NME is NULL THEN ''  
    ELSE lvdr.VNDR_NME END 'Vendor_Name',    
   CASE  
    WHEN lusr.FULL_NME is NULL THEN ''  
    ELSE [COWS].[dbo].getCommaSeparatedUsers(ordr.ORDR_ID,7,0) END 'Assigned_User',  
    -- 7/15/14: ELSE lusr.FULL_NME END 'Assigned_User',     
   CASE  
    WHEN ordr.ORDR_CAT_ID = 6 THEN loty1.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 2 THEN loty1.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 1 THEN loty2.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 4 THEN loty2.ORDR_TYPE_DES  
    WHEN ordr.ORDR_CAT_ID = 5 THEN loty2.ORDR_TYPE_DES END 'Svc_Req_Type',  
   CASE   
    WHEN agtk.gomRcvDt is NULL THEN ''   
    ELSE agtk.gomRcvDt END 'GOM_Received_Ordr'  
    
  FROM COWS.dbo.ORDR ordr with (nolock)  
   LEFT JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON ordr.ORDR_ID = ford.ORDR_ID  
   LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = ford.ORDR_ID  
   LEFT JOIN COWS.dbo.IPL_ORDR ipod with (nolock) ON ordr.ORDR_ID = ipod.ORDR_ID  
   LEFT JOIN (SELECT distinct FTN, FMS_CKT_ID, NUA_ADR, PLN_NME  
        FROM COWS.dbo.NRM_CKT with (nolock)) nrm ON ford.FTN = nrm.FTN   
     LEFT JOIN COWS.dbo.LK_PROD_TYPE lpty1 with (nolock) ON ford.PROD_TYPE_CD = lpty1.FSA_PROD_TYPE_CD  
     LEFT JOIN COWS.dbo.LK_PROD_TYPE lpty2 with (nolock) ON ipod.PROD_TYPE_ID = lpty2.PROD_TYPE_ID  
     LEFT JOIN COWS.dbo.H5_FOLDR h5fd with (nolock) ON ordr.H5_FOLDR_ID = h5fd.H5_FOLDR_ID  
     LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=h5fd.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6  
     LEFT JOIN COWS.dbo.LK_CTRY lctry with (nolock) ON h5fd.CTRY_CD = lctry.CTRY_CD   
     LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD 
    LEFT JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON ordr.ORDR_STUS_ID = lkos.ORDR_STUS_ID     
   LEFT JOIN (SELECT lnit.ORDR_ID, SUM(ISNULL(CAST(REPLACE(lnit.MRC_CHG_AMT,'null','') AS MONEY),0)) MRC_CHG_AMT, SUM(ISNULL(CAST(REPLACE(lnit.NRC_CHG_AMT,'null','') AS MONEY),0)) NRC_CHG_AMT    
           FROM COWS.dbo.FSA_ORDR_BILL_LINE_ITEM lnit with (nolock)  
           JOIN COWS.dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID = lnit.ORDR_ID   
           GROUP by lnit.ORDR_ID) blit ON ordr.ORDR_ID = blit.ORDR_ID              
    LEFT JOIN COWS.dbo.ORDR_MS ordms with (nolock) ON ordr.ORDR_ID = ordms.ORDR_ID    
   LEFT JOIN (SELECT ccdh.ORDR_ID, ccdh.NEW_CCD_DT, ccdh.CCD_HIST_ID   
      FROM COWS.dbo.CCD_HIST ccdh with(nolock)   
      JOIN COWS.dbo.ORDR ordr with (nolock) ON ccdh.ORDR_ID = ordr.ORDR_ID    
            WHERE ccdh.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)   
                   FROM (SELECT ORDR_ID, CCD_HIST_ID   
                     FROM COWS.dbo.CCD_HIST with (nolock) ) chis   
                   WHERE chis.ORDR_ID = ccdh.ORDR_ID AND ordr.ORDR_STUS_ID = 1)  
               )chst on ordr.ORDR_ID = chst.ORDR_ID      
   JOIN (SELECT acts.ACT_TASK_ID, acts.TASK_ID, acts.CREAT_DT, acts.ORDR_ID  
     FROM COWS.dbo.ACT_TASK acts with (nolock)       
     LEFT JOIN COWS.dbo.ORDR ordr with (nolock) ON acts.ORDR_ID = ordr.ORDR_ID  
     WHERE acts.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)   
            FROM (SELECT ACT_TASK_ID, ORDR_ID  
              FROM COWS.dbo.ACT_TASK with (nolock) )mxts  
             WHERE mxts.ORDR_ID = acts.ORDR_ID AND ordr.ORDR_STUS_ID = 1)        
     )ltsk ON ordr.ORDR_ID = ltsk.ORDR_ID  
   LEFT JOIN (SELECT onte.ORDR_ID, onte.NTE_TXT  
      FROM COWS.dbo.ORDR_NTE onte with (nolock)  
      LEFT JOIN COWS.dbo.ORDR ordr with (nolock) ON onte.ORDR_ID = ordr.ORDR_ID  
      WHERE onte.NTE_ID = (SELECT MAX(NTE_ID)  
            FROM (SELECT NTE_ID, ORDR_ID  
              FROM COWS.dbo.ORDR_NTE with (nolock) )mxnt  
            WHERE mxnt.ORDR_ID = onte.ORDR_ID and ordr.ORDR_STUS_ID = 1)  
       )odnt ON ltsk.ORDR_ID = odnt.ORDR_ID       
   LEFT JOIN (SELECT distinct vndo.TRMTG_CD, vndo.ORDR_ID, vndo.VNDR_FOLDR_ID   
      FROM COWS.dbo.VNDR_ORDR vndo with (nolock)  
      JOIN COWS.dbo.ORDR oder with (nolock) ON vndo.ORDR_ID = oder.ORDR_ID   
      WHERE vndo.TRMTG_CD = 1  
       )vndr ON ordr.ORDR_ID = vndr.ORDR_ID   
   LEFT JOIN COWS.dbo.VNDR_FOLDR vfdr ON vndr.VNDR_FOLDR_ID = vfdr.VNDR_FOLDR_ID  
   LEFT JOIN COWS.dbo.LK_VNDR lvdr ON vfdr.VNDR_CD = lvdr.VNDR_CD     
   LEFT JOIN (SELECT distinct uwas.ASMT_DT, uwas.ORDR_ID, uwas.ASN_USER_ID, uwas.GRP_ID  
        FROM COWS.dbo.USER_WFM_ASMT uwas with (nolock)  
        LEFT JOIN COWS.dbo.FSA_ORDR fsao with (nolock) ON uwas.ORDR_ID = fsao.ORDR_ID   
        WHERE uwas.ASMT_DT = (SELECT MAX(ASMT_DT)  
            FROM (SELECT distinct ASMT_DT, ORDR_ID   
              FROM COWS.dbo.USER_WFM_ASMT with (nolock))uswf    
               WHERE uwas.ORDR_ID = uswf.ORDR_ID )  
      )uwfa ON ordr.ORDR_ID =  uwfa.ORDR_ID  
    LEFT JOIN COWS.dbo.LK_USER lusr on uwfa.ASN_USER_ID = lusr.USER_ID  
      
    LEFT JOIN COWS.dbo.LK_ORDR_TYPE loty1 with (nolock) ON ford.ORDR_TYPE_CD = loty1.FSA_ORDR_TYPE_CD  
    LEFT JOIN COWS.dbo.LK_ORDR_TYPE loty2 with (nolock) ON ipod.ORDR_TYPE_ID = loty2.ORDR_TYPE_ID  
     
   --LEFT JOIN (SELECT ORDR_ID, CONVERT(VARCHAR(10), MIN(atsk.CREAT_DT), 101) as gomRcvDt   
   LEFT JOIN (SELECT ORDR_ID, MIN(atsk.CREAT_DT) as gomRcvDt  
      FROM COWS.dbo.ACT_TASK atsk with (nolock)        
      JOIN COWS.dbo.MAP_GRP_TASK mgtk with (nolock) ON mgtk.TASK_ID = atsk.TASK_ID        
      WHERE mgtk.GRP_ID = 1 AND atsk.TASK_ID NOT IN (101, 102)        
           GROUP BY ORDR_ID  
            )agtk ON ford.ORDR_ID = agtk.ORDR_ID  
             
  -- RGN_ID: 2 = ENCI Region, ORDR_STUS_ID: 1 = Pending  
   WHERE ordr.RGN_ID = 2 AND COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),h5fd.CUST_NME,'')<>''  
        
   --===============================================  
   -- Insert summary field records for reported year  
   --===============================================  
   -- Field # 1:  Number of orders installed  
   --    # 2:  CCD met %  
   --   # 3:  Total MRC installed  
   --   # 4:  Orders disconnected current month  
   --   # 5: Total MRC disconnected  
   --   # 6: Disconnects received  
   --================================================  
     
   IF NOT EXISTS(select 1 FROM COWS_Reporting.dbo.Data_ENCI_Monthly_Summary WHERE Year = CAST(@rpt_year AS INT))  
    BEGIN  
             
     INSERT INTO COWS_Reporting.dbo.Data_ENCI_Monthly_Summary (Year, FieldID, FieldName, FieldCD, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)   
      SELECT @rpt_year, 1, 'Number of orders installed', 'I', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  
     INSERT INTO COWS_Reporting.dbo.Data_ENCI_Monthly_Summary (Year, FieldID, FieldName, FieldCD, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)         
      SELECT @rpt_year, 2, 'CCD met %', 'I', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  
     INSERT INTO COWS_Reporting.dbo.Data_ENCI_Monthly_Summary (Year, FieldID, FieldName, FieldCD, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)         
      SELECT @rpt_year, 3, 'Total MRC installed', 'I', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0       
     INSERT INTO COWS_Reporting.dbo.Data_ENCI_Monthly_Summary (Year, FieldID, FieldName, FieldCD, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)         
      SELECT @rpt_year, 4, 'Orders disconnected current month', 'D', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  
     INSERT INTO COWS_Reporting.dbo.Data_ENCI_Monthly_Summary (Year, FieldID, FieldName, FieldCD, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)         
      SELECT @rpt_year, 5, 'Total MRC disconnected', 'D', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0       
     INSERT INTO COWS_Reporting.dbo.Data_ENCI_Monthly_Summary (Year, FieldID, FieldName, FieldCD, Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec)         
      SELECT @rpt_year, 6, 'Disconnects received', 'D', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0  
           
    END  
  
   SET @installed_cnt = 0  
   SET @met_ccd_pct  = 0          
   SET @total_mrc_inst = 0  
   SET @disconn_cnt = 0  
   SET @total_mrc_disc = 0  
   --SET @new_ord_rcvd = 0  
   SET @disconn_rcvd = 0  
     
   SET @installed_cnt = (SELECT COUNT(DISTINCT FTN) FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 2 )  
   SET @met_ccd_pct  = ((SELECT COUNT(DISTINCT FTN) FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 2  
         AND Instl_Dscnct_Dt < DATEADD(day, 1, Cust_Commit_Dt) )* 100/@installed_cnt)  
            --AND Instl_Dscnct_Dt <= Cust_Commit_Dt )* 100/@installed_cnt)    
   --SET @total_mrc_inst =(SELECT SUM(CAST(MRC AS INT)) FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 2 )
   SET @total_mrc_inst =(SELECT SUM(CAST(MRC AS INT)) FROM (
						 SELECT DISTINCT FTN,MRC
						 FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 2) A)  
   SET @disconn_cnt =(SELECT COUNT(DISTINCT FTN) FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 5 )  
   --SET @total_mrc_disc =(SELECT SUM(CAST(MRC AS INT)) FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 5 )  
   SET @total_mrc_disc =(SELECT SUM(CAST(MRC AS INT)) FROM (
						 SELECT DISTINCT FTN,MRC
						 FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 5) A )   
   --SET @new_ord_rcvd =(SELECT COUNT(DISTINCT FTN) FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 0 AND Ordr_Type_CD NOT IN ('BC', 'DC'))  
     
   SET @disconn_rcvd =(SELECT COUNT(DISTINCT FTN) FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Type_CD IN ('DC') AND  
         (GOM_Received_Ordr >= @startDate AND GOM_Received_Ordr < @endDate )  
         )   
   --========================================  
   -- Update Summary values of reported month  
   --========================================  
   SET @updSQL1 = CASE @rpt_mon  
    WHEN 1 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Jan='  
    WHEN 2 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Feb='  
    WHEN 3 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Mar='  
    WHEN 4 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Apr='  
    WHEN 5 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET May='  
    WHEN 6 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Jun='  
    WHEN 7 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Jul='  
    WHEN 8 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Aug='  
    WHEN 9 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Sep='  
    WHEN 10 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Oct='  
    WHEN 11 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Nov='  
    WHEN 12 THEN 'UPDATE COWS_Reporting.dbo.Data_ENCI_Monthly_Summary SET Dec='       
    END  
      
   SET @updSQL=@updSQL1 + CAST(@installed_cnt AS VARCHAR(5)) + ' WHERE Year=' + @rpt_year + ' AND FieldID = 1'  
   EXEC (@updSQL)  
   SET @updSQL=@updSQL1 + CAST(@met_ccd_pct AS VARCHAR(5)) + ' WHERE Year=' + @rpt_year + ' AND FieldID = 2'   
   EXEC (@updSQL)  
   SET @updSQL=@updSQL1 + CAST(@total_mrc_inst AS VARCHAR(9)) + ' WHERE Year=' + @rpt_year + ' AND FieldID = 3'  
   EXEC (@updSQL)     
   SET @updSQL=@updSQL1 + CAST(@disconn_cnt AS VARCHAR(5)) + ' WHERE Year=' + @rpt_year + ' AND FieldID = 4'   
   EXEC (@updSQL)  
   SET @updSQL=@updSQL1 + CAST(@total_mrc_disc AS VARCHAR(9)) + ' WHERE Year=' + @rpt_year + ' AND FieldID = 5'  
   EXEC (@updSQL)  
   --SET @updSQL=@updSQL1 + CAST(@new_ord_rcvd AS VARCHAR(5)) + ' WHERE Year=' + @rpt_year + ' AND FieldID = 13'   
   --EXEC (@updSQL)  
   SET @updSQL=@updSQL1 + CAST(@disconn_rcvd AS VARCHAR(5)) + ' WHERE Year=' + @rpt_year + ' AND FieldID = 6'  
   EXEC (@updSQL)     
     
   --========================  
   -- Select Installs Summary   
   --========================  
   --SET @SQL = 'SELECT FieldName ''Installs'', CASE WHEN CAST(' + @rpt_mon_nme + ' AS VARCHAR(8)) is NULL THEN '''' ELSE CAST(' + @rpt_mon_nme     
   --SET @SQL = @SQL + ' AS VARCHAR(8)) END ''Reported month'', CASE WHEN CAST((' + @mons_ytd + ') AS VARCHAR(8)) is NULL THEN '''' ELSE'  
   --SET @SQL = @SQL + ' CAST((' + @mons_ytd + ') AS VARCHAR(8)) END ''YTD'' '  
   --SET @SQL = @SQL + 'FROM COWS_Reporting.dbo.Data_ENCI_Monthly_Summary WHERE Year='   
   --SET @SQL = @SQL + @rpt_year + ' AND FieldCD = ''I'' AND FieldID <= 5 ORDER BY FieldID'  
   --=================================================  
   -- Select value of C3: Number of orders installed  
   --=================================================  
   SET @SQL = 'SELECT CASE WHEN CAST(' + @rpt_mon_nme + ' AS VARCHAR(8)) is NULL THEN '''' ELSE CAST(' + @rpt_mon_nme  
   SET @SQL = @SQL + ' AS VARCHAR(8)) END FROM COWS_Reporting.dbo.Data_ENCI_Monthly_Summary WHERE Year='  
   SET @SQL = @SQL + @rpt_year + ' AND FieldCD = ''I'' AND FieldID = 1'  
     
   EXEC(@SQL)  
        
  END  
  -- Installed  
 ELSE IF @QueryNumber=1  
  BEGIN  
              
   --========================  
   -- Select Installed orders  
   --========================  
   --DECLARE @Secured      INT=0
   SELECT 
     DISTINCT
     enci.FTN,  
     enci.NUA,  
     Private_Line_No 'Private Line Number',  
     CASE         
      WHEN Cust_Name is NULL THEN ''  
      WHEN @Secured = 0 AND Scurd_Cd = 1 AND Cust_Name is NOT NULL THEN 'Private Customer'  
      ELSE Cust_Name END 'Customer Name',       
     Product,  
     H5_City 'H5 City',  
     H5_Country 'H5 Country',  
     Speed_Service 'Speed of Service',   
     lkos.ORDR_STUS_DES 'Order Status',  
     MRC 'Monthly Recurring Charge (MRC)',  
     NRC 'Non-Recurring Charge (NRC)',  
     Contract_Signed_Dt 'Customer Signed Date',  
     CONVERT(varchar(10), Submitted_Dt, 101) 'Order Submit Date',  
     Ordr_CWD 'Customer Want Date',  
     Cust_Commit_Dt 'Customer Commit Date',  
     CONVERT(VARCHAR(10), FSA_CCD, 101) 'FSA CCD',  
     CONVERT(varchar(10), Instl_Dscnct_Dt, 101) 'Order Bill Clear/Install Date',  
     Vendor_Name 'Vendor Name',  
     --Assigned_User 'Assigned user in xNCI WFM',  
     Svc_Req_Type 'Service Request Type',  
     GOM_Received_Ordr 'GOM Receives Order Date',  
     osh.STDI_DT 'Standard Interval Date', 
     CONVERT(VARCHAR(10), cm.TRGT_DLVRY_DT, 101) [Target Delivery Date], 
     ISNULL(osh.STDI_REAS_DES,'') 'Standard Interval Reason',  
     ISNULL(osh.CMNT_TXT,'') 'STDI Comments'  
             
   FROM COWS_Reporting.dbo.Data_ENCI_Monthly enci WITH (NOLOCK)  
    JOIN COWS.dbo.LK_ORDR_STUS lkos WITH (NOLOCK) ON enci.Ordr_Status_ID = lkos.ORDR_STUS_ID   
    LEFT JOIN (SELECT FO.FTN,sh.ORDR_ID[ORDR_ID], sh.STDI_DT,sr.STDI_REAS_DES, CMNT_TXT,ROW_NUMBER() OVER (PARTITION BY sh.ORDR_ID ORDER BY sh.CREAT_DT DESC) AS rownum    
     FROM [COWS].[dbo].[ORDR_STDI_HIST] sh WITH (NOLOCK)  
     JOIN [COWS].[dbo].[FSA_ORDR] fo WITH (NOLOCK) ON FO.ORDR_ID = SH.ORDR_ID  
     JOIN [COWS].[dbo].[LK_STDI_REAS] sr WITH (NOLOCK) ON sh.STDI_REAS_ID = sr.STDI_REAS_ID) osh on enci.FTN = osh.FTN AND rownum = 1
     LEFT Outer JOIN [COWS].[dbo].[FSA_ORDR] fo WITH (NOLOCK) ON FO.FTN = enci.FTN  AND fo.ORDR_TYPE_CD <> 'DC'
    LEFT OUTER JOIN (SELECT  DISTINCT aa.ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT] aa   
		JOIN [COWS].[dbo].[ORDR] bb on  aa.ORDR_ID = bb.ORDR_ID where  
		CKT_ID = (SELECT  MAX(CKT_ID) FROM (SELECT DISTINCT ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT]) ck      
		WHERE bb.ORDR_ID = ck.ORDR_ID)) ckt ON ckt.ORDR_ID = FO.ORDR_ID  


		LEFT OUTER JOIN (
			SELECT DISTINCT aa.CKT_ID, aa.ACCS_DLVRY_DT, aa.ACCS_ACPTC_DT, aa.TRGT_DLVRY_DT   
			FROM [COWS].[dbo].[CKT_MS] aa WITH (NOLOCK)
			WHERE
				VER_ID = (
					SELECT TOP 1 ck.VER_ID
					FROM [COWS].[dbo].[CKT_MS] ck WITH (NOLOCK)
					WHERE ck.CKT_ID = aa.CKT_ID
					ORDER BY ck.CKT_ID, ck.VER_ID DESC
				)
		) cm on ckt.CKT_ID = cm.CKT_ID         
   WHERE enci.Ordr_Status_ID = 2 AND enci.Ordr_Type_CD NOT IN ('DC')    
   ORDER BY enci.FTN  
     
  END  
  -- New Orders  
 --ELSE IF @QueryNumber=2  
 -- BEGIN  
    
   --==================  
   -- Select New orders  
   --==================  
     
  
     
   --WHERE Ordr_Status_ID = 0 AND Ordr_Type_CD NOT IN ('BC', 'DC')     
   --ORDER BY FTN  
    
 -- END  
  -- Pending Installs  
 ELSE IF @QueryNumber=3  
  BEGIN  
    
   --========================  
   -- Select Pending Installs  
   --========================  
   --DECLARE @Secured      INT=0  
   SELECT DISTINCT enci.FTN,  
     enci.NUA,  
     Private_Line_No 'Private Line Number',  
     CASE         
      WHEN Cust_Name is NULL THEN ''  
      WHEN @Secured = 0 AND Scurd_Cd = 1 AND Cust_Name is NOT NULL THEN 'Private Customer'  
      ELSE Cust_Name END 'Customer Name',       
     Product,  
     H5_City 'H5 City',  
     H5_Country 'H5 Country',  
     Speed_Service 'Speed of Service',   
     lkos.ORDR_STUS_DES 'Order Status',  
     MRC 'Monthly Recurring Charge (MRC)',  
     NRC 'Non-Recurring Charge (NRC)',  
     Contract_Signed_Dt 'Customer Signed Date',  
     CONVERT(varchar(10), Submitted_Dt, 101) 'Order Submit Date',  
     Ordr_CWD 'Customer Want Date',  
     Cust_Commit_Dt 'Customer Commit Date',  
     CONVERT(VARCHAR(10), FSA_CCD, 101) 'FSA CCD',  
     CONVERT(varchar(10), Instl_Dscnct_Dt, 101) 'Last Update Date',  
     Ordr_Note 'Note',  
     Vendor_Name 'Vendor Name',  
     Assigned_User 'Assigned user in xNCI WFM',  
     Svc_Req_Type 'Service Request Type',  
     GOM_Received_Ordr 'GOM Receives Order Date',  
     osh.STDI_DT 'Standard Interval Date', 
     CONVERT(VARCHAR(10), cm.TRGT_DLVRY_DT, 101) [Target Delivery Date], 
     ISNULL(osh.STDI_REAS_DES,'') 'Standard Interval Reason',  
     ISNULL(osh.CMNT_TXT,'') 'STDI Comments',  
     luid.FULL_NME 'Last Updated By'  
       
   FROM COWS_Reporting.dbo.Data_ENCI_Monthly enci with (nolock)  
    LEFT JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON enci.FTN = ford.FTN  
    JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON enci.Ordr_Status_ID = lkos.ORDR_STUS_ID  
    LEFT JOIN (SELECT FO.FTN,sh.ORDR_ID[ORDR_ID], sh.STDI_DT,sr.STDI_REAS_DES, CMNT_TXT,ROW_NUMBER() OVER (PARTITION BY sh.ORDR_ID ORDER BY sh.CREAT_DT DESC) AS rownum    
     FROM [COWS].[dbo].[ORDR_STDI_HIST] sh WITH (NOLOCK)  
     JOIN [COWS].[dbo].[FSA_ORDR] fo WITH (NOLOCK) ON FO.ORDR_ID = SH.ORDR_ID  
     JOIN [COWS].[dbo].[LK_STDI_REAS] sr WITH (NOLOCK) ON sh.STDI_REAS_ID = sr.STDI_REAS_ID) osh on enci.FTN = osh.FTN AND rownum = 1  
         LEFT JOIN (SELECT ROW_NUMBER() OVER (PARTITION BY orn.ORDR_ID ORDER BY orn.NTE_ID DESC) AS rownum2,  
           orn.CREAT_BY_USER_ID, orn.ORDR_ID, lu.USER_ADID, lu.FULL_NME  
            FROM COWS.dbo.ORDR_NTE orn   
      JOIN cows.dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON orn.CREAT_BY_USER_ID = ugr.user_id AND grp_id = 7 AND user_id > 1  
      JOIN COWS.dbo.lk_user lu WITH (NOLOCK) ON ugr.user_id = lu.user_id  )   
       luid ON ford.ORDR_ID = luid.ORDR_ID AND rownum2 = 1  
      
   LEFT OUTER JOIN (SELECT  DISTINCT aa.ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT] aa   
		JOIN [COWS].[dbo].[ORDR] bb on  aa.ORDR_ID = bb.ORDR_ID where  
		CKT_ID = (SELECT  MAX(CKT_ID) FROM (SELECT DISTINCT ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT]) ck      
		WHERE bb.ORDR_ID = ck.ORDR_ID)) ckt ON ckt.ORDR_ID = ford.ORDR_ID  


		LEFT OUTER JOIN (
			SELECT DISTINCT aa.CKT_ID, aa.ACCS_DLVRY_DT, aa.ACCS_ACPTC_DT, aa.TRGT_DLVRY_DT   
			FROM [COWS].[dbo].[CKT_MS] aa WITH (NOLOCK)
			WHERE
				VER_ID = (
					SELECT TOP 1 ck.VER_ID
					FROM [COWS].[dbo].[CKT_MS] ck WITH (NOLOCK)
					WHERE ck.CKT_ID = aa.CKT_ID
					ORDER BY ck.CKT_ID, ck.VER_ID DESC
				)
		) cm on ckt.CKT_ID = cm.CKT_ID 
   WHERE enci.Ordr_Status_ID = 1 AND enci.Ordr_Type_CD NOT IN ('DC') AND  ford.ORDR_TYPE_CD <> 'DC'     
   ORDER BY enci.FTN  
     
  END  
  -- Pending Disconnects  
 ELSE IF @QueryNumber=4  
  BEGIN  
        
   --=================================  
   -- Select Pending Disconnect orders  
   --=================================  
   --DECLARE @Secured      INT=0       
   SELECT DISTINCT enci.FTN,  
     enci.NUA,  
     Private_Line_No 'Private Line Number',  
     CASE         
      WHEN Cust_Name is NULL THEN ''  
      WHEN @Secured = 0 AND Scurd_Cd = 1 AND Cust_Name is NOT NULL THEN 'Private Customer'  
      ELSE Cust_Name END 'Customer Name',       
     Product,  
     H5_City 'H5 City',  
     H5_Country 'H5 Country',  
     Speed_Service 'Speed of Service',   
     lkos.ORDR_STUS_DES 'Order Status',  
     MRC 'Monthly Recurring Charge (MRC)',  
     NRC 'Non-Recurring Charge (NRC)',  
     Contract_Signed_Dt 'Customer Signed Date',  
     CONVERT(varchar(10), Submitted_Dt, 101) 'Order Submit Date',  
     Ordr_CWD 'Customer Want Date',  
     Cust_Commit_Dt 'Customer Commit Date',  
     CONVERT(VARCHAR(10), FSA_CCD, 101) 'FSA CCD',  
     CONVERT(VARCHAR(10), Instl_Dscnct_Dt, 101) 'Last Update Date',  
     Ordr_Note 'Note',  
     Vendor_Name 'Vendor Name',  
     Assigned_User 'Assigned user in xNCI WFM',  
     Svc_Req_Type 'Service Request Type',  
     GOM_Received_Ordr 'GOM Receives Order Date',  
     osh.STDI_DT 'Standard Interval Date',  
     CONVERT(VARCHAR(10), cm.TRGT_DLVRY_DT, 101) [Target Delivery Date],
     ISNULL(osh.STDI_REAS_DES,'') 'Standard Interval Reason',  
     ISNULL(osh.CMNT_TXT,'') 'STDI Comments',  
     luid.FULL_NME 'Last Updated By'  
       
    FROM COWS_Reporting.dbo.Data_ENCI_Monthly enci with (nolock)  
    LEFT JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON enci.FTN = ford.FTN  
    JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON enci.Ordr_Status_ID = lkos.ORDR_STUS_ID  
    LEFT JOIN (SELECT FO.FTN,sh.ORDR_ID[ORDR_ID], sh.STDI_DT,sr.STDI_REAS_DES, CMNT_TXT,ROW_NUMBER() OVER (PARTITION BY sh.ORDR_ID ORDER BY sh.CREAT_DT DESC) AS rownum    
     FROM [COWS].[dbo].[ORDR_STDI_HIST] sh WITH (NOLOCK)  
     JOIN [COWS].[dbo].[FSA_ORDR] fo WITH (NOLOCK) ON FO.ORDR_ID = SH.ORDR_ID  
     JOIN [COWS].[dbo].[LK_STDI_REAS] sr WITH (NOLOCK) ON sh.STDI_REAS_ID = sr.STDI_REAS_ID) osh on enci.FTN = osh.FTN AND rownum = 1  
         LEFT JOIN (SELECT ROW_NUMBER() OVER (PARTITION BY orn.ORDR_ID ORDER BY orn.NTE_ID DESC) AS rownum2,  
           orn.CREAT_BY_USER_ID, orn.ORDR_ID, lu.USER_ADID, lu.FULL_NME  
            FROM COWS.dbo.ORDR_NTE orn   
      JOIN cows.dbo.USER_GRP_ROLE ugr WITH (NOLOCK) ON orn.CREAT_BY_USER_ID = ugr.user_id AND grp_id = 7 AND user_id > 1  
      JOIN COWS.dbo.lk_user lu WITH (NOLOCK) ON ugr.user_id = lu.user_id  )   
       luid ON ford.ORDR_ID = luid.ORDR_ID AND rownum2 = 1  
    LEFT OUTER JOIN (SELECT  DISTINCT aa.ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT] aa   
		JOIN [COWS].[dbo].[ORDR] bb on  aa.ORDR_ID = bb.ORDR_ID where  
		CKT_ID = (SELECT  MAX(CKT_ID) FROM (SELECT DISTINCT ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT]) ck      
		WHERE bb.ORDR_ID = ck.ORDR_ID)) ckt ON ckt.ORDR_ID = ford.ORDR_ID  


		LEFT OUTER JOIN (
			SELECT DISTINCT aa.CKT_ID, aa.ACCS_DLVRY_DT, aa.ACCS_ACPTC_DT, aa.TRGT_DLVRY_DT   
			FROM [COWS].[dbo].[CKT_MS] aa WITH (NOLOCK)
			WHERE
				VER_ID = (
					SELECT TOP 1 ck.VER_ID
					FROM [COWS].[dbo].[CKT_MS] ck WITH (NOLOCK)
					WHERE ck.CKT_ID = aa.CKT_ID
					ORDER BY ck.CKT_ID, ck.VER_ID DESC
				)
		) cm on ckt.CKT_ID = cm.CKT_ID    
    WHERE enci.Ordr_Status_ID = 1 AND enci.Ordr_Type_CD IN ('DC')   AND   ford.ORDR_TYPE_CD = 'DC'       
    ORDER BY enci.FTN  
      
  END    
  -- Disconnected this month  
 ELSE IF @QueryNumber=5  
  BEGIN  
      
   --===========================  
   -- Select Disconnected orders  
   --===========================  
   --DECLARE @Secured      INT=0
   SELECT 
     DISTINCT
     enci.FTN,  
     enci.NUA,  
     Private_Line_No 'Private Line Number',  
     CASE         
      WHEN Cust_Name is NULL THEN ''  
      WHEN @Secured = 0 AND Scurd_Cd = 1 AND Cust_Name is NOT NULL THEN 'Private Customer'  
      ELSE Cust_Name END 'Customer Name',       
     Product,  
     H5_City 'H5 City',  
     H5_Country 'H5 Country',  
     Speed_Service 'Speed of Service',   
     lkos.ORDR_STUS_DES 'Order Status',  
     MRC 'Monthly Recurring Charge (MRC)',  
     NRC 'Non-Recurring Charge (NRC)',  
     Contract_Signed_Dt 'Customer Signed Date',  
     CONVERT(VARCHAR(10), Submitted_Dt, 101) 'Order Submit Date',  
     Ordr_CWD 'Customer Want Date',  
     Cust_Commit_Dt 'Customer Commit Date',  
     CONVERT(VARCHAR(10), FSA_CCD, 101) 'FSA CCD',  
     CONVERT(VARCHAR(10), Instl_Dscnct_Dt, 101) 'Disconnect Date',  
     Vendor_Name 'Vendor Name',  
     --Assigned_User 'Assigned user in xNCI WFM',  
     Svc_Req_Type 'Service Request Type',  
     GOM_Received_Ordr 'GOM Receives Order Date', 
     CONVERT(VARCHAR(10), cm.TRGT_DLVRY_DT, 101) [Target Delivery Date], 
     osh.STDI_DT 'Standard Interval Date',  
     ISNULL(osh.STDI_REAS_DES,'') 'Standard Interval Reason',  
     ISNULL(osh.CMNT_TXT,'') 'STDI Comments'  
       
    FROM COWS_Reporting.dbo.Data_ENCI_Monthly enci with (nolock)  
     JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON enci.Ordr_Status_ID = lkos.ORDR_STUS_ID  
     LEFT JOIN (SELECT FO.FTN,sh.ORDR_ID[ORDR_ID], sh.STDI_DT,sr.STDI_REAS_DES, CMNT_TXT,ROW_NUMBER() OVER (PARTITION BY sh.ORDR_ID ORDER BY sh.CREAT_DT DESC) AS rownum    
     FROM [COWS].[dbo].[ORDR_STDI_HIST] sh WITH (NOLOCK)  
     JOIN [COWS].[dbo].[FSA_ORDR] fo WITH (NOLOCK) ON FO.ORDR_ID = SH.ORDR_ID  
     JOIN [COWS].[dbo].[LK_STDI_REAS] sr WITH (NOLOCK) ON sh.STDI_REAS_ID = sr.STDI_REAS_ID) osh on enci.FTN = osh.FTN AND rownum = 1      
     LEFT Outer JOIN [COWS].[dbo].[FSA_ORDR] fo WITH (NOLOCK) ON FO.FTN = enci.FTN  AND fo.ORDR_TYPE_CD = 'DC'
    LEFT OUTER JOIN (SELECT  DISTINCT aa.ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT] aa   
		JOIN [COWS].[dbo].[ORDR] bb on  aa.ORDR_ID = bb.ORDR_ID where  
		CKT_ID = (SELECT  MAX(CKT_ID) FROM (SELECT DISTINCT ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT]) ck      
		WHERE bb.ORDR_ID = ck.ORDR_ID)) ckt ON ckt.ORDR_ID = FO.ORDR_ID  


		LEFT OUTER JOIN (
			SELECT DISTINCT aa.CKT_ID, aa.ACCS_DLVRY_DT, aa.ACCS_ACPTC_DT, aa.TRGT_DLVRY_DT   
			FROM [COWS].[dbo].[CKT_MS] aa WITH (NOLOCK)
			WHERE
				VER_ID = (
					SELECT TOP 1 ck.VER_ID
					FROM [COWS].[dbo].[CKT_MS] ck WITH (NOLOCK)
					WHERE ck.CKT_ID = aa.CKT_ID
					ORDER BY ck.CKT_ID, ck.VER_ID DESC
				)
		) cm on ckt.CKT_ID = cm.CKT_ID   
    WHERE enci.Ordr_Status_ID = 5 AND enci.Ordr_Type_CD IN ('DC')  
    ORDER BY enci.FTN  
      
  END  
  -- Summary Fields: Pending installs, Pending disconnects  
 ELSE IF @QueryNumber IN (19, 20)  
  BEGIN  
    
   SET @SQL=CASE @QueryNumber  
          WHEN 19 THEN 'SELECT COUNT(DISTINCT FTN) FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 1 AND Ordr_Type_CD NOT IN (''DC'')'        
          WHEN 20 THEN 'SELECT COUNT(DISTINCT FTN) FROM COWS_Reporting.dbo.Data_ENCI_Monthly with (nolock) WHERE Ordr_Status_ID = 1 AND Ordr_Type_CD IN (''DC'')'  
          END  
        EXEC (@SQL)    
  
  END  
 ELSE IF @QueryNumber IN (6, 7, 8, 9, 10, 11)  
  BEGIN  
    
   SET @SQL = 'SELECT CASE WHEN CAST(' + @rpt_mon_nme + ' AS VARCHAR(8)) is NULL THEN '''' ELSE CAST(' + @rpt_mon_nme  
   SET @SQL = @SQL + ' AS VARCHAR(8)) END FROM COWS_Reporting.dbo.Data_ENCI_Monthly_Summary WHERE Year='  
   SET @SQL = @SQL + @rpt_year + ' AND FieldID ='  
     
   SET @SQL=CASE @QueryNumber      
    WHEN 6 THEN @SQL + '1'  
    WHEN 7 THEN @SQL + '2'  
    WHEN 8 THEN @SQL + '3'  
    WHEN 9 THEN @SQL + '4'  
    WHEN 10 THEN @SQL + '5'  
    WHEN 11 THEN @SQL + '6'  
    END  
      
   EXEC(@SQL)    
    
  END  
 ELSE IF @QueryNumber IN (12, 13, 14, 15, 16, 17)  
  BEGIN  
      
   SET @SQL = 'SELECT CASE WHEN CAST((' + @mons_ytd + ') AS VARCHAR(8)) is NULL THEN '''' ELSE '  
   SET @SQL = @SQL + 'CAST((' + @mons_ytd + ') AS VARCHAR(8)) END  FROM COWS_Reporting.dbo.Data_ENCI_Monthly_Summary '  
   SET @SQL = @SQL + 'WHERE Year=' + @rpt_year + ' AND FieldID ='  
      
   SET @SQL=CASE @QueryNumber  
    WHEN 12 THEN @SQL + '1'  
    WHEN 13 THEN @SQL + '2'  
    WHEN 14 THEN @SQL + '3'  
    WHEN 15 THEN @SQL + '4'  
    WHEN 16 THEN @SQL + '5'  
    WHEN 17 THEN @SQL + '6'      
    END  
      
   EXEC(@SQL)    
     
  END  
    
 CLOSE SYMMETRIC KEY FS@K3y;   
   
 RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_ENCIRptMonthlyReport ' + CAST(@QueryNumber AS VARCHAR(4)) + ', '   
 SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)   
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  
  
  