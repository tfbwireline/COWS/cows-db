USE [COWS_Reporting]
GO
/********************************************
* SP: sp_FedlineEventDuration
* Create Date: 8/1/2013
* Author: Frank Luna
*
* Description: Daily report for install failures.
* EXEC sp_FedlineEventDuration 
*		Query 1 - Daily report for 12 consecutive months.
*		Query 2 - Pivot table to show counts for shipped, disconnect and completed. Divided by month for 12 months
*		Query 3 - Query to shipped over 24 hours.						
*
* Modifications:
* 1/27/2014 - CI5554013: Changed event status time difference to be in minutes.
* -- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
******************************************/
ALTER PROCEDURE [dbo].[sp_FedlineEventDuration]
		@Secured		INT = 0,
		@query			INT,
		@endDate		DATE = '' 
AS
BEGIN TRY
	SET NOCOUNT ON;
--DECLARE @Secured	INT = 1	 /* Uncomment for testing */
--DECLARE @query		INT = 1	 /* Uncomment for testing */
DECLARE @startDate	DATE 
--DECLARE @endDate	DATE = '2/1/2014'

IF @endDate =''		/* If run on 1st of month gets prvious month's last day and 1st day of previous month */
	BEGIN 
	SET @endDate = DATEADD(DD,-1,GETDATE()) 		/* Comment for testing */
	SET @startDate=CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@endDate)-1),@endDate),101)  -- set to first day of month
	SET @endDate = DATEADD(DD,1,@endDate)		/* Resets date back to get the full 24hr day*/
	END
ELSE				/*Get get previous day of day entered so the 1st day can be entered with out having to 
					  know last day of month date and gets first day of that month */		
	BEGIN
	SET @endDate = DATEADD(DD,-1,@endDate) 
	SET @startDate=CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@endDate)-1),@endDate),101)  -- set to first day of month
	SET @endDate = DATEADD(DD,1,@endDate)		/* Resets date back to get the full 24hr day of last day of month*/
	END	

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
/*	
  Select Raw data report
*/
IF @query = 1
BEGIN
SET @startDate=DATEADD(MONTH,-12,@startDate)

SELECT DISTINCT
 CASE 
	WHEN @Secured = 0 AND ev.CSG_LVL_ID >0  AND CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)) is NOT NULL  THEN 'Private Customer'
	ELSE COALESCE(CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)),fd.CUST_NME,'') 
	END [Customer Name]
,fd.EVENT_ID [COWS Event ID]
,fd.FRB_REQ_ID [FRB Order ID]
,lot.PRNT_ORDR_TYPE_DES [Activity Type]
,fep.STRT_TME [PRCS Start Time]
,fep.END_TME [PRCS End Time]
,dbo.DateDiffWkendHolInMin(fep.STRT_TME,fep.END_TME)[Event Status in Min]
,lk.EVENT_STUS_DES [Start Status]
,lks.EVENT_STUS_DES [End Status]

FROM
[COWS].[dbo].[EVENT] ev
JOIN	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd  WITH (NOLOCK) ON fd.EVENT_ID = ev.EVENT_ID 
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=fd.FEDLINE_EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=23
JOIN	[COWS].[dbo].[FEDLINE_EVENT_USER_DATA] eud  WITH (NOLOCK) ON fd.EVENT_ID = eud.EVENT_ID 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_PRCS] fep  WITH (NOLOCK) ON fd.EVENT_ID = fep.EVENT_ID 
JOIN	[COWS].[dbo].[LK_EVENT_STUS] lk  WITH (NOLOCK) ON fep.EVENT_STUS_ID = Lk.EVENT_STUS_ID
LEFT JOIN	[COWS].[dbo].[LK_EVENT_STUS] lks  WITH (NOLOCK) ON fep.EVENT_END_STUS_ID = Lks.EVENT_STUS_ID
JOIN	[COWS].[dbo].[LK_FEDLINE_ORDR_TYPE] lot WITH (NOLOCK) ON fd.ORDR_TYPE_CD = lot.ORDR_TYPE_CD

WHERE fep.END_TME BETWEEN @startDate AND @endDate AND fd.FRB_REQ_ID > 0 ORDER BY fd.EVENT_ID DESC 

END

/*
  Pivot table for report to diplay current month and previous 11 months
*/

IF @query = 2
BEGIN

DECLARE @GetEvent TABLE
(
EVENT_ID		INT, 
FRB_REQ_ID		INT,
STRT_TME		DATETIME,
END_TME			DATETIME,
TIME_SEC		INT,
EVENT_STUS_ID	INT,
ORDR_TYPE_CD	INT,
SDate			DATE,
CDate			DATE NULL
)
DECLARE	@CountEvent TABLE
(
Cnt				INT,
ORDR_TYPE_CD	INT
)
DECLARE	@StatCanTbl TABLE
(
EVENT_ID		INT,
ORDR_TYPE_CD	INT,
STRT_TME		DATE,
SDate			DATE,
CDate			DATE
)
DECLARE	@StatCompTbl TABLE
(
EVENT_ID		INT,
ORDR_TYPE_CD	INT
)
CREATE TABLE #ConvTime
(
STATUS_NAME		VARCHAR(25),
Month_Name		VARCHAR(25),
Number			VARCHAR(50)
)

DECLARE @cntr		INT
DECLARE @MthCnt		INT
DECLARE @MthStart	DATE
DECLARE @MthEnd		DATE
DECLARE @SrtStrg	VARCHAR(3)
DECLARE @srtCntr	INT
DECLARE @MnthName	VARCHAR(10)
DECLARE @MnthYear	VARCHAR(4)
DECLARE @CycleDate	VARCHAR(15)	


SET @cntr = 1
SET @MthCnt = 0
SET @srtCntr = 1
WHILE @cntr <= 12
BEGIN

SET @MthStart=DATEADD(MONTH,@MthCnt,@startDate)
SET @MnthName = DATENAME(MONTH,@MthStart)
SET @MnthYear = DATENAME(YEAR,@MthStart)
SET @CycleDate = @MnthName + @MnthYear

INSERT INTO @GetEvent(EVENT_ID, FRB_REQ_ID, STRT_TME, END_TME, TIME_SEC, EVENT_STUS_ID,ORDR_TYPE_CD)
SELECT DISTINCT
fd.EVENT_ID [COWS Event ID]
,fd.FRB_REQ_ID [FRB Order ID]
,fep.STRT_TME [PRCS Start Time]
,fep.END_TME [PRCS End Time]
,dbo.DateTotalSec(fep.STRT_TME,fep.END_TME)[TIME IN SEC]
,fep.EVENT_STUS_ID [EVENT_STUS_ID]
,CASE 
	WHEN fep.EVENT_STUS_ID = 9 AND fep.EVENT_END_STUS_ID = 10 THEN 1
	WHEN fep.EVENT_STUS_ID = 5 AND fd.ORDR_TYPE_CD IN ('NCC') THEN 2
	WHEN fep.EVENT_STUS_ID = 5 AND fep.EVENT_END_STUS_ID = 6 THEN 3
	END [ORDR_TYPE_CD]
	
FROM
[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_USER_DATA] eud  WITH (NOLOCK) ON fd.EVENT_ID = eud.EVENT_ID 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_PRCS] fep  WITH (NOLOCK) ON eud.EVENT_ID = fep.EVENT_ID 
JOIN	[COWS].[dbo].[LK_EVENT_STUS] lks  WITH (NOLOCK) ON fep.EVENT_STUS_ID = Lks.EVENT_STUS_ID 

WHERE  DATENAME(MONTH,fep.END_TME) + DATENAME(YEAR,fep.END_TME)= @CycleDate AND fd.FRB_REQ_ID > 0 ORDER BY ORDR_TYPE_CD


UPDATE ge SET CDate = eh.CREAT_DT FROM @GetEvent ge INNER JOIN [COWS].[dbo].[EVENT_HIST] eh WITH (NOLOCK) ON eh.event_id = ge.EVENT_ID 
	WHERE ORDR_TYPE_CD = 2 AND eh.EVENT_HIST_ID = (SELECT MAX(EVENT_HIST_ID) 
		FROM [COWS].[dbo].[EVENT_HIST] WHERE  EVENT_ID = ge.event_id AND ACTN_ID = 26)
	
UPDATE ge SET SDate = fd.STRT_TME FROM @GetEvent ge INNER JOIN [COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd WITH (NOLOCK) ON fd.event_id = ge.EVENT_ID 
	WHERE ge.ORDR_TYPE_CD = 2 AND fd.FEDLINE_EVENT_ID = (SELECT MAX(FEDLINE_EVENT_ID) 
	FROM [COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] WHERE  EVENT_ID = ge.event_id)	

 SELECT @SrtStrg =  RIGHT(REPLICATE('0', 3) + CONVERT(NVARCHAR(3),@srtCntr),3)
  
IF (SELECT COUNT(*) FROM @GetEvent) > 0
	BEGIN 
	
	INSERT INTO @StatCanTbl(EVENT_ID,ORDR_TYPE_CD)
	SELECT 
	EVENT_Id
	,ORDR_TYPE_CD
	 FROM @GetEvent WHERE ORDR_TYPE_CD = 2 and (CDate = SDate) GROUP BY EVENT_Id,ORDR_TYPE_CD
	 	
	INSERT INTO @StatCompTbl(EVENT_ID,ORDR_TYPE_CD)
	SELECT 
	EVENT_Id
	,ORDR_TYPE_CD
	 FROM @GetEvent WHERE ORDR_TYPE_CD = 3 GROUP BY EVENT_Id,ORDR_TYPE_CD
	 
	INSERT INTO @CountEvent(Cnt,ORDR_TYPE_CD)
	SELECT 
	COUNT(EVENT_ID) 
	,ORDR_TYPE_CD
	FROM @StatCanTbl GROUP BY ORDR_TYPE_CD
	 
	INSERT INTO @CountEvent(Cnt,ORDR_TYPE_CD)
	SELECT 
	COUNT(EVENT_ID) 
	,ORDR_TYPE_CD
	FROM @StatCompTbl GROUP BY ORDR_TYPE_CD
 
	INSERT INTO @CountEvent(Cnt,ORDR_TYPE_CD)
	SELECT 
	COUNT(*) 
	,ORDR_TYPE_CD
	FROM @GetEvent WHERE ORDR_TYPE_CD = 1  GROUP BY ORDR_TYPE_CD
 	
	INSERT #ConvTime(STATUS_NAME,Month_Name,Number)
	SELECT 
	CASE 
		WHEN ORDR_TYPE_CD = 1 THEN 'Shipped'
		WHEN ORDR_TYPE_CD = 2 THEN 'Cancelled'
		WHEN ORDR_TYPE_CD = 3 THEN 'Completed'
		END
	,CASE 
		WHEN ORDR_TYPE_CD = 1 THEN 'Order' + @SrtStrg + LEFT(DATENAME(MM,@MthStart),3) + ' ' + DATENAME(YY,@MthStart) + ' Total #'
		WHEN ORDR_TYPE_CD = 2 THEN 'Order' + @SrtStrg + LEFT(DATENAME(MM,@MthStart),3) + ' ' + DATENAME(YY,@MthStart) + ' Total #'
		WHEN ORDR_TYPE_CD = 3 THEN 'Order' + @SrtStrg + LEFT(DATENAME(MM,@MthStart),3) + ' ' + DATENAME(YY,@MthStart) + ' Total #'
		END
	,Cnt
	FROM @CountEvent
	END

/* Inserts dummy rows when no value are found to prevent nulls from showing in report.*/
IF	(SELECT COUNT(*) FROM @CountEvent WHERE ORDR_TYPE_CD = 1) = 0
	BEGIN
	INSERT INTO #ConvTime(STATUS_NAME,Month_Name,Number)
	VALUES ('Shipped','Order' + @SrtStrg + LEFT(DATENAME(MM,@MthStart),3) + ' ' + DATENAME(YY,@MthStart) + ' Total #','0')
	END

IF	(SELECT COUNT(*) FROM @CountEvent WHERE ORDR_TYPE_CD = 2 ) = 0 
	BEGIN
	INSERT INTO #ConvTime(STATUS_NAME,Month_Name,Number)
	VALUES ('Cancelled','Order' + @SrtStrg + LEFT(DATENAME(MM,@MthStart),3) + ' ' + DATENAME(YY,@MthStart) + ' Total #','0')
	END

IF	(SELECT COUNT(*) FROM @CountEvent WHERE ORDR_TYPE_CD = 3 ) = 0
	BEGIN
	INSERT INTO #ConvTime(STATUS_NAME,Month_Name,Number)
	VALUES ('Completed','Order' + @SrtStrg + LEFT(DATENAME(MM,@MthStart),3) + ' ' + DATENAME(YY,@MthStart) + ' Total #','0')
	END

 SET @srtCntr = @srtCntr +1	
 SELECT @SrtStrg =  RIGHT(REPLICATE('0', 3) + CONVERT(NVARCHAR(3),@srtCntr),3)		
	
SET @cntr = @cntr + 1
SET @MthCnt = @MthCnt -	1
SET @srtCntr = @srtCntr +1	
 
DELETE FROM @GetEvent
DELETE FROM @StatCanTbl
DELETE FROM @StatCompTbl
DELETE FROM @CountEvent

END
 
DECLARE @cols NVARCHAR(2000) 
DECLARE @DynamicQuery NVARCHAR(4000) 

SELECT  @cols = STUFF(( SELECT DISTINCT ',' + QuoteName([Month_Name])
						FROM    #ConvTime
                        FOR XML PATH('') ), 1, 1,'')

/*
	Each Month Name has 'ORDERXX-' that was needed for sorting, remove that string now that sorting is complete.
*/
BEGIN
    WHILE @cols LIKE '%ORDER%'
    BEGIN
        SET @cols = REPLACE(@cols,SUBSTRING(@cols,PATINDEX('%ORDER%',@cols),8),'')
    END
END
--SELECT  @cols AS 'DEBUG -- After String manipulation'		-- for debugging

UPDATE #ConvTime SET Month_Name = REPLACE(Month_Name,SUBSTRING(Month_Name,PATINDEX('%ORDER%',Month_Name),8),'')
SET @DynamicQuery = 'SELECT STATUS_NAME[ ], '+ @cols +'  FROM 
(SELECT STATUS_NAME, Number, Month_Name FROM #ConvTime) base
 PIVOT (MAX(Number) FOR Month_Name IN ('+ @cols +')) AS pvt ORDER BY STATUS_NAME DESC' 


EXECUTE(@DynamicQuery)

DROP TABLE #ConvTime
	
END

/*
  To get Ship over 24 hours
*/

IF @query = 3
BEGIN

DECLARE @GetShip24 TABLE
(
EVENT_ID		INT, 
FRB_REQ_ID		INT,
STRT_TME		DATETIME,
END_TME			DATETIME,
TIME_SEC		INT,
EVENT_STUS_ID	INT,
EVENT_STUS_DES	VARCHAR(35)
)
DECLARE	@Over24S TABLE
(
EVENT_ID		INT, 
FRB_REQ_ID		INT,
Time_Over24		VARCHAR(30),
EVENT_STUS_DES	VARCHAR(35)
)

INSERT INTO @GetShip24(EVENT_ID,FRB_REQ_ID, STRT_TME, END_TME, TIME_SEC, EVENT_STUS_ID,EVENT_STUS_DES)
SELECT DISTINCT
fd.EVENT_ID [COWS Event ID]
,fd.FRB_REQ_ID [FRB Order ID]
,fep.STRT_TME [PRCS Start Time]
,fep.END_TME [PRCS End Time]
,dbo.DateTotalSec(fep.STRT_TME,fep.END_TME)[TIME IN SEC]
,eud.EVENT_STUS_ID [EVENT_STUS_ID]
,lk.EVENT_STUS_DES [End Status]

FROM
[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_USER_DATA] eud  WITH (NOLOCK) ON fd.EVENT_ID = eud.EVENT_ID 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_PRCS] fep  WITH (NOLOCK) ON eud.EVENT_ID = fep.EVENT_ID
JOIN	[COWS].[dbo].[LK_EVENT_STUS] lk  WITH (NOLOCK) ON eud.EVENT_STUS_ID = Lk.EVENT_STUS_ID

WHERE fep.END_TME BETWEEN @startDate AND @endDate
  AND fep.EVENT_STUS_ID = 9 AND fd.FRB_REQ_ID > 0

SELECT 
EVENT_ID [EVENT ID]
,FRB_REQ_ID [FRB ID]
,dbo.DateDayHrMin(TIME_SEC) [Time]
,EVENT_STUS_DES [Event Status]
FROM @GetShip24 WHERE TIME_SEC > 86400 

END

CLOSE SYMMETRIC KEY FS@K3y 
RETURN 0
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_FedlineEventDuration '  
	SET @Desc=@Desc + CONVERT(VARCHAR(1), @Secured) + ', '
		+ CONVERT(VARCHAR (1),@query)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH


