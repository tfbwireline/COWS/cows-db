USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptEventsPerCustomer]    Script Date: 11/21/2019 9:09:09 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
--=======================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			06/17/2011
-- Description:		
--					Extract one or many MDS event(s) excluding 'Disconnect' 
--					per customer. 
--					QueryNumber: 1 = Daily
--								 2 = Weekly
--								 3 = Monthly
--								 0 = Specify start date & end date: 'mm/dd/yyyy'
--
-- Note:			MDS Activity Type of Disconnect DOES NOT have FSA_MDS_EVENT record.
--
-- Modifications:
--
-- 03/29/2012		sxv0766: IM952822-Removed resulting string expression from YearMonth column
-- 01/27/2017		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--=======================================================================================
*/
ALTER PROCEDURE [dbo].[sp_MNSRptEventsPerCustomer]
	@QueryNumber		INT,	
	@Secured			INT=0,	
	@inStartDtStr		VARCHAR(10)='',
	@inEndDtStr			VARCHAR(10)=''
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDate		Datetime
	DECLARE @endDate		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)	
	DECLARE @dayOfWeek		INT
	
	IF @QueryNumber=0
		BEGIN

			SET @startDate=CONVERT(datetime, @inStartDtStr, 101)
			SET @endDate=CONVERT(datetime, @inEndDtStr, 101)				

			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			SET @endDate=DATEADD(day, 1, @endDate) 
												
		END		
	ELSE IF @QueryNumber=1
    	BEGIN    	
    		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
		
			SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)   
		END
	ELSE IF @QueryNumber=2
		BEGIN
			-- e.g. Run date is '2011-06-20' 2nd day of the week
			-- STRT_TMST >= 2011-06-13 00:00:00.000 and STRT_TMST < 2011-06-20 00:00:00.000
			
			SET @today=GETDATE()
			SET @dayOfWeek=DATEPART(dw, @today)
			--SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+6), @today), 112) As DateTime)
			--SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS Datetime)			
			SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)			
				
		END
	ELSE IF @QueryNumber=3
		BEGIN			 
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month 
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
			SET @startDate=cast(@startDtStr AS datetime)
			SET @endDate=cast((cast(SUBSTRING(CONVERT(VARCHAR(10), getdate(), 101),1,2) + '/01/' + CAST(YEAR(getdate()) AS VARCHAR(4)) as datetime) -1) AS datetime)  
		END
			
	--PRINT  @startDate
	--PRINT  @endDate
		
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SELECT	CASE mds.MDS_FAST_TRK_CD
			WHEN 1 THEN 'MDS Fast Track'
			WHEN 0 THEN 'MDS' END 'Event Type',
		mds.EVENT_ID 'Event ID',		
		CASE 						
			--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
			--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
			--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
			WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
			WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
			ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'')
			END 'Customer Name',		
		mds.STRT_TMST 'YearMonth', 
		--SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) 'YearMonth', 
		CASE
			WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
			ELSE '' END 'MDS Install Activity',	
		mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
		CASE 
			WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
			ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',			
		CASE
			WHEN moe.MDS_SRVC_TIER_ID is NULL THEN ''
			ELSE svti.MDS_SRVC_TIER_DES END 'Service Tier', 
		evst.EVENT_STUS_DES 'Event Status',	
		CASE
			WHEN luser1.DSPL_NME is NULL THEN ''
			ELSE luser1.DSPL_NME END 'Assigned Acct MNS PM',
		CASE
			WHEN mds.EVENT_STUS_ID = 2 THEN ''
			WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
			WHEN evhi.CREAT_BY_USER_ID is not NULL THEN luser2.DSPL_NME END 'Reviewed By',
		''                                     AS [S/C Flag], 
		'MDS Only'			AS [Network Activity Type]		
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)	
		JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID		
		LEFT JOIN ( SELECT mevt.EVENT_ID, max(ehis.EVENT_HIST_ID) 'maxHistId'
						FROM COWS.dbo.MDS_EVENT mevt with (nolock)
						JOIN COWS.dbo.EVENT_HIST ehis with (nolock) ON mevt.EVENT_ID = ehis.EVENT_ID 	
					WHERE mevt.EVENT_STUS_ID in (3, 4, 6) and ehis.ACTN_ID = 5
					GROUP BY mevt.EVENT_ID)evHist ON mds.EVENT_ID = evHist.EVENT_ID		
		LEFT JOIN COWS.dbo.EVENT_HIST evhi with (nolock) ON evHist.maxHistId = evhi.EVENT_HIST_ID			
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MNS moe with (nolock) ON fmds.FSA_MDS_EVENT_ID = moe.FSA_MDS_EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_SRVC_TIER svti with (nolock) ON moe.MDS_SRVC_TIER_ID = svti.MDS_SRVC_TIER_ID	
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.MNS_PM_ID = luser1.USER_ADID
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID
	
	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000		
	-- Only 'Initial Install' and 'MAC'
	
	-- EVENT_STUS_ID: 2 = Pending, 3 = Rework, 4 = Published, 6 = Completed
	WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) AND mds.STRT_TMST >= @startDate AND mds.STRT_TMST < @endDate
		
	--ORDER BY mds.EVENT_ID
	
	/*New MDS Events Code Start */
	UNION ALL

	SELECT	
		--CASE mds.MDS_FAST_TRK_CD      /*vn370313/ 01272017 CASE WHEN ISNULL(COWS.dbo.MDS_EVENT.MDS_FAST_TRK_TYPE_ID,'')='' THEN 0 ELSE 1 END AS MDS_FAST_TRK_CD*/
		--	WHEN 1 THEN 'MDS Fast Track'
		--	WHEN 0 THEN 'MDS' END 'Event Type',
		CASE WHEN ISNULL(mds.MDS_FAST_TRK_TYPE_ID,'')='' THEN 'MDS' ELSE 'MDS Fast Track' END AS 'Event Type' ,
		mds.EVENT_ID 'Event ID',		
		CASE 						
			--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
			--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
			--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
			WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
			WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
			ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'')
			END 'Customer Name',		
		mds.STRT_TMST 'YearMonth', 
		--SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) 'YearMonth', 
		CASE
			WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
			ELSE '' END 'MDS Install Activity',	
		mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
		CASE 
			WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
			ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',			
		CASE
			WHEN moe.MDS_SRVC_TIER_ID is NULL THEN ''
			ELSE svti.MDS_SRVC_TIER_DES END 'Service Tier', 
		evst.EVENT_STUS_DES 'Event Status',	
		CASE
			WHEN luser1.DSPL_NME is NULL THEN ''
			ELSE luser1.DSPL_NME END 'Assigned Acct MNS PM',
		CASE
			WHEN mds.EVENT_STUS_ID = 2 THEN ''
			WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
			WHEN evhi.CREAT_BY_USER_ID is not NULL THEN luser2.DSPL_NME END 'Reviewed By',
		CASE	
						WHEN eod.SC_CD = 'C'        THEN 'Complex'
						WHEN eod.SC_CD = 'S'        THEN 'Simple'
					    ELSE ''
						END                                     AS [S/C Flag],
		lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type]		
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)	
		JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = mds.NTWK_ACTY_TYPE_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID		
		LEFT JOIN ( SELECT mevt.EVENT_ID, max(ehis.EVENT_HIST_ID) 'maxHistId'
						FROM COWS.dbo.MDS_EVENT mevt with (nolock)
						JOIN COWS.dbo.EVENT_HIST ehis with (nolock) ON mevt.EVENT_ID = ehis.EVENT_ID 	
					WHERE mevt.EVENT_STUS_ID in (3, 4, 6) and ehis.ACTN_ID = 5
					GROUP BY mevt.EVENT_ID)evHist ON mds.EVENT_ID = evHist.EVENT_ID		
		LEFT JOIN COWS.dbo.EVENT_HIST evhi with (nolock) ON evHist.maxHistId = evhi.EVENT_HIST_ID			
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MNS moe with (nolock) ON fmds.FSA_MDS_EVENT_ID = moe.FSA_MDS_EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_SRVC_TIER svti with (nolock) ON moe.MDS_SRVC_TIER_ID = svti.MDS_SRVC_TIER_ID	
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.MNS_PM_ID = luser1.USER_ADID
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID
		LEFT OUTER JOIN [COWS].[dbo].[MDS_EVENT_ODIE_DEV] eod WITH (NOLOCK) ON eod.EVENT_ID = mds.EVENT_ID
	
	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000		
	-- Only 'Initial Install' and 'MAC'
	
	-- EVENT_STUS_ID: 2 = Pending, 3 = Rework, 4 = Published, 6 = Completed
	WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) AND mds.STRT_TMST >= @startDate AND mds.STRT_TMST < @endDate
		
	ORDER BY mds.EVENT_ID

	/*New MDS Events Code END */

	CLOSE SYMMETRIC KEY FS@K3y;
		
	RETURN 0;
			
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptEventsPerCustomer ' + CAST(@QueryNumber AS VARCHAR(4)) 
	SET @Desc=@Desc + ',' + CAST(@Secured AS VARCHAR(2)) + ': '	
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH




