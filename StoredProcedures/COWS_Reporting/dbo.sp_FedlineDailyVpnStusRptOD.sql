USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_FedlineDailyVpnStusRptOD]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_FedlineDailyVpnStusRptOD')
EXEC ('CREATE PROCEDURE [dbo].[sp_FedlineDailyVpnStusRptOD] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

--=======================================================================================
-- Project:			PJ006439 - COWS Reporting 
-- Author:			David Phillips	
-- Date:			09/20/2012
-- Description:		
--					Summary list of orders/events, regardless of activity type and status, 
--                  that details the customer specific information for the Device ID and 
--                  location being implemented/disconnected. Report is a canned report to be run 
--                  daily to keep track of on-going activity. Report will also be available to run 
--                  on an Ad-hoc/On-Demand basis where user can select a specific date or date range 
--                  for this data.
--					
--
-- Modifications:
-- dlp0278 - PJ8795 - 6/27/13 Added Production Tunnel Status field to report.
--
--=======================================================================================

ALTER Procedure [dbo].[sp_FedlineDailyVpnStusRptOD]
				 @startDate  datetime = NULL,
				 @endDate  datetime = NULL,
				 @getSensData  CHAR(1)='N',
				 @SecuredUser  INT = 0 
AS
BEGIN TRY

	DECLARE @aSQL			VARCHAR(MAX)
	
	CREATE TABLE #tmp_fedline_Day_VPN_Stus_rpt
	(					
		CustName				VARCHAR(100),
		State					VARCHAR(25),
		Ctry					VARCHAR(25),
		FRB_ID					VARCHAR(25),
		Event_ID				VARCHAR(25),
		Org_ID					VARCHAR(25),
		Expedite				VARCHAR(25),
		Event_Stus				VARCHAR(50),
		Tnnl_Cd                 VARCHAR(50),
		EventType				VARCHAR(50),
		ActType					VARCHAR(50),
		InstTyp					VARCHAR(50),
		OrdType					VARCHAR(25),
		Reason					VARCHAR(50),
		OrdSubDt				VARCHAR(25),
		ReqActDt				VARCHAR(100),
		DeviceId				VARCHAR(100),
		Engineer				VARCHAR(100),
		Activator				VARCHAR(100),
		ConfigType				VARCHAR(100),
		OrigSubDt               VARCHAR(50),
		ActTime                 VARCHAR(50)
	)

	SET NOCOUNT ON;
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SET @aSQL='INSERT INTO #tmp_fedline_Day_VPN_Stus_rpt(CustName, State, Ctry, FRB_ID, Event_ID,
						Org_ID, Expedite, Event_Stus, Tnnl_Cd, ActType, InstTyp, OrdType, Reason, OrdSubDt, 
						ReqActDt, DeviceId,	Engineer, Activator, ConfigType, OrigSubDt, ActTime)
	
	SELECT DISTINCT ISNULL(CONVERT(VARCHAR, DecryptByKey(fd.CUST_NME)),'''')
		   ,ISNULL(CONVERT(VARCHAR, DecryptByKey(fa.STT_CD)),'''')
		   ,ISNULL(c.CTRY_NME,'''') --Ctry	 
		   ,ISNULL(CONVERT(VARCHAR, fd.FRB_REQ_ID),'''')
		   ,ISNULL(CONVERT(VARCHAR, fd.EVENT_ID),'''')
		   ,ISNULL(CONVERT(VARCHAR, fd.ORG_ID),'''')
		   ,CASE
				WHEN IS_EXPEDITE_CD = 0 THEN ''No''
				WHEN IS_EXPEDITE_CD = 1 THEN ''Yes''
			END  
		   ,ISNULL(ls.EVENT_STUS_DES, '''') 
		   ,CASE 
				WHEN ud.TNNL_CD = 1 THEN ''1 tunnel up – CC1''
				WHEN ud.TNNL_CD = 2 THEN ''1 tunnel up – CC3''
				WHEN ud.TNNL_CD = 3 THEN ''2 tunnel up''
				ELSE ''''
			END 
		   ,ISNULL(ot.PRNT_ORDR_TYPE_DES, '''') 
		   ,ISNULL(fd.FRB_INSTL_TYPE_NME, '''')   
		   ,ISNULL(fd.ORDR_SUB_TYPE_NME, '''')  
		   ,ISNULL(fd.REPLMT_REAS_NME, '''')
		   ,ISNULL(CONVERT(VARCHAR (12), fd.REQ_RECV_DT, 107), '''')  
		   ,ISNULL(CONVERT(VARCHAR (12), fd.STRT_TME, 107), '''') 
		   ,ISNULL(fd.DEV_NME,'''') 
		   ,ISNULL(ue.FULL_NME, '''')  
		   ,ISNULL(ua.FULL_NME, '''')
		   ,ISNULL(fd.DSGN_TYPE_CD,'''')
		   ,ISNULL((SELECT REQ_RECV_DT FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA WITH (NOLOCK) WHERE EVENT_ID = fd.EVENT_ID AND ORDR_TYPE_CD IN (''NCI'',''DCS'')), '''') 
		   ,ISNULL((SUBSTRING(CONVERT(varchar, fd.STRT_TME, 109), 13, 8) +
			 '' - '' + SUBSTRING(CONVERT(varchar, fd.END_TME, 109), 13, 8)), '''')		
 	 FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA fd
 		INNER JOIN COWS.dbo.FEDLINE_EVENT_USER_DATA ud ON ud.EVENT_ID = fd.EVENT_ID 
		INNER JOIN COWS.dbo.LK_EVENT_STUS ls ON ud.EVENT_STUS_ID = ls.EVENT_STUS_ID AND ls.REC_STUS_ID = 1
		INNER JOIN COWS.dbo.LK_FEDLINE_ORDR_TYPE ot ON ot.ORDR_TYPE_CD = fd.ORDR_TYPE_CD
		LEFT OUTER JOIN COWS.dbo.LK_USER ua ON ua.USER_ID = ud.ACTV_USER_ID
		LEFT OUTER JOIN COWS.dbo.LK_USER ue ON ue.USER_ID = ud.ENGR_USER_ID
		LEFT OUTER JOIN COWS.dbo.FEDLINE_EVENT_ADR fa ON fa.FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID
									AND fa.ADR_TYPE_ID = 18
		LEFT OUTER JOIN COWS.dbo.LK_CTRY c ON c.CTRY_CD = fa.CTRY_CD
	
	 WHERE fd.REC_STUS_ID = 1'

	
	IF @startDate <> ''
		SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT >= ''' + convert(varchar(20),@startDate) + '''' 
		
	IF @endDate <> ''
		BEGIN
			SET @endDate = DATEADD(day, 1, @endDate)
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT <= ''' + convert(varchar(20),@endDate) + ''''	
		END
	
	SET  @aSQL=@aSQL +	'
		
		UNION ALL
		
		SELECT DISTINCT ISNULL(CONVERT(VARCHAR, DecryptByKey(fd.CUST_NME)),'''')
		   ,ISNULL(CONVERT(VARCHAR, DecryptByKey(fa.STT_CD)),'''')
		   ,ISNULL(c.CTRY_NME,'''') --Ctry	 
		   ,ISNULL(CONVERT(VARCHAR, fd.FRB_REQ_ID),'''')
		   ,ISNULL(CONVERT(VARCHAR, fd.EVENT_ID),'''')
		   ,ISNULL(CONVERT(VARCHAR, fd.ORG_ID),'''')
		   ,CASE
				WHEN IS_EXPEDITE_CD = 0 THEN ''No''
				WHEN IS_EXPEDITE_CD = 1 THEN ''Yes''
			END  
		   ,ISNULL(ls.EVENT_STUS_DES, '''') 
		   ,CASE 
				WHEN ud.TNNL_CD = 1 THEN ''1 tunnel up – CC1''
				WHEN ud.TNNL_CD = 2 THEN ''1 tunnel up – CC3''
				WHEN ud.TNNL_CD = 3 THEN ''2 tunnel up''
				ELSE ''''
			END 
		   ,ISNULL(ot.PRNT_ORDR_TYPE_DES, '''') 
		   ,ISNULL(fd.FRB_INSTL_TYPE_NME, '''')   
		   ,ISNULL(fd.ORDR_SUB_TYPE_NME, '''')  
		   ,ISNULL(fd.REPLMT_REAS_NME, '''')
		   ,ISNULL(CONVERT(VARCHAR (12), fd.REQ_RECV_DT, 107), '''')  
		   ,ISNULL(CONVERT(VARCHAR (12), fd.STRT_TME, 107), '''') 
		   ,ISNULL(fd.DEV_NME,'''') 
		   ,ISNULL(ue.FULL_NME, '''')  
		   ,ISNULL(ua.FULL_NME, '''')
		   ,ISNULL(fd.DSGN_TYPE_CD,'''')
		   ,ISNULL((SELECT REQ_RECV_DT FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA WITH (NOLOCK) WHERE EVENT_ID = fd.EVENT_ID AND ORDR_TYPE_CD IN (''NCI'',''DCS'')), '''') 
		   ,ISNULL((SUBSTRING(CONVERT(varchar, fd.STRT_TME, 109), 13, 8) +
			 '' - '' + SUBSTRING(CONVERT(varchar, fd.END_TME, 109), 13, 8)), '''')		
 	 FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA fd
 		INNER JOIN COWS.dbo.FEDLINE_EVENT_USER_DATA ud ON ud.EVENT_ID = fd.EVENT_ID 
		INNER JOIN COWS.dbo.LK_EVENT_STUS ls ON ud.EVENT_STUS_ID = ls.EVENT_STUS_ID AND ls.REC_STUS_ID = 1
		INNER JOIN COWS.dbo.LK_FEDLINE_ORDR_TYPE ot ON ot.ORDR_TYPE_CD = fd.ORDR_TYPE_CD
		LEFT OUTER JOIN COWS.dbo.LK_USER ua ON ua.USER_ID = ud.ACTV_USER_ID
		LEFT OUTER JOIN COWS.dbo.LK_USER ue ON ue.USER_ID = ud.ENGR_USER_ID
		LEFT OUTER JOIN COWS.dbo.FEDLINE_EVENT_ADR fa ON fa.FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID
									AND fa.ADR_TYPE_ID = 18
		LEFT OUTER JOIN COWS.dbo.LK_CTRY c ON c.CTRY_CD = fa.CTRY_CD
	
	 WHERE fd.REC_STUS_ID = 1 AND fd.EVENT_ID in (SELECT d.EVENT_ID from COWS.dbo.FEDLINE_EVENT_MSG m INNER JOIN COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA d on m.FEDLINE_EVENT_ID = d.FEDLINE_EVENT_ID AND d.ORDR_TYPE_CD = ''NCI''
							WHERE FEDLINE_EVENT_MSG_STUS_NME = ''Rework'') '

	
	IF @startDate <> ''
		SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT >= ''' + convert(varchar(20),@startDate) + '''' 
		
	IF @endDate <> ''
		BEGIN
			SET @endDate = DATEADD(day, 1, @endDate)
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT <= ''' + convert(varchar(20),@endDate) + ''''	
		END
	
		
	EXEC (@aSQL);

	SELECT 	
			CASE
				WHEN @SecuredUser = 0 AND CustName <> '' THEN 'Private Customer' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND CustName <> '' THEN 'Private Customer'
				ELSE CustName 
			 END			AS 'Customer Name'
			,Org_ID			AS 'ORG ID'
			,CASE
				WHEN @SecuredUser = 0 AND State <> '' THEN 'Private Addr' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND State <> '' THEN 'Private Addr'
				ELSE State 
			 END			AS 'State'
			,Ctry			AS 'Country'
			,FRB_ID			AS 'FRB ID'
			,OrigSubDt		AS 'Initial FRB Order Submission Date'
			,OrdSubDt		AS 'FRB Order Submission Date'
			,Expedite       AS 'Expedite'
			,Event_ID		AS 'COWS Event ID'
			,Event_Stus		AS 'COWS Event Status'
			,Tnnl_Cd        AS 'Production Tunnel Status'
			,ReqActDt		AS 'Requested Activity Date'
			,ActTime        AS 'Requested Activity Time'
			,DeviceId		AS 'Device ID/Asset ID'
			,ConfigType     AS 'Configuration Type'	
			,ActType		AS 'Activity Type'
			,InstTyp		AS 'Migration Type'
			,OrdType		AS 'Order Type'
			,Reason			AS 'Reason for Reorder/Replacement'
			,Engineer		AS 'Fedline Preconfig Engineer'
			,Activator		AS 'Fedline MDS Activator'
	FROM #tmp_fedline_Day_VPN_Stus_rpt
	WHERE FRB_ID > 0
	ORDER BY OrigSubDt
		
		
CLOSE SYMMETRIC KEY FS@K3y;


	IF OBJECT_ID('tempdb..#tmp_fedline_Day_VPN_Stus_rpt', 'U') IS NOT NULL	
		drop table #tmp_fedline_Day_VPN_Stus_rpt;	
		
	RETURN 0;

END TRY
BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_FedlineDailyVpnStusRptOD '  
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' 
	+ CONVERT(VARCHAR(10), @endDate, 101) + ', ' 
	+  CONVERT(VARCHAR(10),@SecuredUser) + ', ' + @getSensData		
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH



GO
