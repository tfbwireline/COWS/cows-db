USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_DomesticCPEOpsDailySummary]    Script Date: 05/15/2018 22:12:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---- =============================================
---- Author:		dlp0278
---- Create date: 08/03/2017
---- Description: Domestic CPE OPS Daily Report- Summary sheet
---- [dbo].[sp_DomesticCPEOpsDailySummary] 1
---- vn370313: Performance improvement
---- SELECT * FROM RAD_Reporting.rad.ReportQueue_new with (nolock) Where REPORTID in (165,0,4)
---- EXEC RAD_Reporting.rad.sp_ReportRefreshQueue_NEW 'N','165'
-- =============================================
ALTER PROCEDURE [dbo].[sp_DomesticCPEOpsDailySummary]
 @Secured			   INT=0
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY


OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

SELECT DISTINCT
	lu.DSPL_NME															AS [Assigned User] 
	,FTN																AS [Mach5 Order #]
	--,(SELECT TOP 1 REPLACE(LTRIM(REPLACE(DEVICE_ID, '0', ' ')), ' ', '0')  FROM COWS.dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
	--			WHERE fo.ORDR_ID = ORDR_ID )                            AS [Device]  /*vn370313: Performance improvement */
	,REPLACE(LTRIM(REPLACE(cpl.DEVICE_ID, '0', ' ')), ' ', '0')         AS [Device]
	,CASE
	   WHEN    fo.ORDR_SUB_TYPE_CD = 'PRCV' THEN 'PRCV'
	   WHEN    fo.ORDR_SUB_TYPE_CD = 'ROCP' THEN 'ROCP'
	   WHEN    fo.ORDR_SUB_TYPE_CD = 'ISMV' THEN 'ISMV'
       when    fo.ORDR_TYPE_CD = 'IN' then 'INST'
       when    fo.ORDR_TYPE_CD = 'DC' then 'DISC'
       WHEN    fo.ORDR_TYPE_CD = 'CN' then 'CAN'
     END																AS [Activity]
	--,ISNULL(CONVERT(VARCHAR, DecryptByKey(foc.CUST_NME)),'')			AS [CustomerName]
	--,CSG_LVL_ID
	,CASE
			WHEN @secured = 0 and o.CSG_LVL_ID =0 THEN foc.CUST_NME
			WHEN @secured = 0 and o.CSG_LVL_ID >0 AND csd.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @secured = 0 and o.CSG_LVL_ID >0 AND csd.CUST_NME IS NULL THEN NULL 
			ELSE COALESCE(foc.CUST_NME,CONVERT(varchar, DecryptByKey(csd.CUST_NME)),'') 
			END
			AS [CustomerName]
	,H5_H6_CUST_ID                                                      AS [H6 Id]
	,Convert(varchar (10),fo.CREAT_DT,101)								AS [Received Date]
	,Convert(varchar (10),ISNULL(o.CUST_CMMT_DT,fo.CUST_CMMT_DT),101)	AS [CCD]
	,CASE
		WHEN lt.TASK_NME = 'CPE Hold' THEN  'RTS-rejected'
		ELSE  lt.TASK_NME
	 END																AS [Status]
	 ,DATEDIFF(DD,at.CREAT_DT,GETDATE())								AS [Inbox Days]
	 ,ISNULL(prh.REQSTN_NBR,'')                                         AS [Req #]
	 , CASE
			WHEN prh.REQSTN_DT IS NULL  THEN ''
			ELSE Convert(varchar (10),prh.REQSTN_DT,101)
		END																AS [Req Date]
	 ,ISNULL(cli.PRCH_ORDR_NBR,'')										AS [PO #]
	 ,ISNULL(foc.SITE_ID,'')                                            AS [Site Id]
	 ,ISNULL(o.CPE_CLLI,'')                                             AS [CPE CLLI]
	 ,ISNULL(fo.CPE_VNDR,'')                                            AS [CPE Vendor]
	 ,ISNULL((SELECT TOP 1 ISNULL(lj.CPE_JPRDY_CD,'')
						FROM COWS.dbo.ORDR_JPRDY oj  WITH (NOLOCK) INNER JOIN
						     COWS.dbo.LK_CPE_JPRDY  lj       WITH (NOLOCK) ON lj.CPE_JPRDY_CD = oj.JPRDY_CD
						WHERE oj.ORDR_ID = fo.ORDR_ID
							ORDER BY oj.CREAT_DT DESC),'')				AS [Latest Jeopardy]
	
FROM COWS.dbo.FSA_ORDR fo WITH (NOLOCK)
INNER JOIN COWS.dbo.ORDR o WITH (NOLOCK) ON o.ORDR_ID = fo.ORDR_ID
INNER JOIN COWS.dbo.LK_ORDR_TYPE lot WITH (NOLOCK) ON lot.FSA_ORDR_TYPE_CD = fo.ORDR_TYPE_CD
LEFT OUTER JOIN COWS.dbo.FSA_ORDR_CPE_LINE_ITEM cpl WITH (NOLOCK) ON fo.ORDR_ID = cpl.ORDR_ID    /*vn370313: Performance improvement */
LEFT OUTER JOIN COWS.dbo.ACT_TASK at WITH (NOLOCK) ON at.ORDR_ID = fo.ORDR_ID
LEFT OUTER JOIN COWS.dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.ORDR_ID = fo.ORDR_ID
					AND foc.CIS_LVL_TYPE IN ('H5','H6')
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=foc.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
LEFT OUTER JOIN COWS.dbo.USER_WFM_ASMT uwa WITH (NOLOCK) ON uwa.ORDR_ID = fo.ORDR_ID
								AND GRP_ID = 13
LEFT OUTER JOIN COWS.dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = uwa.ASN_USER_ID
LEFT OUTER JOIN COWS.dbo.LK_TASK lt WITH (NOLOCK) ON lt.TASK_ID = at.TASK_ID
LEFT OUTER JOIN COWS.dbo.PS_REQ_HDR_QUEUE prh WITH (NOLOCK) ON prh.ORDR_ID = fo.ORDR_ID
LEFT OUTER JOIN COWS.dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON fo.ORDR_ID = cli.ORDR_ID		
												AND prh.REQSTN_NBR = cli.PLSFT_RQSTN_NBR 

WHERE fo.PROD_TYPE_CD = 'CP'
	AND (at.TASK_ID in (600,601,602,604,1000) and STUS_ID in (0,156))
	AND o.DMSTC_CD = 0
	AND o.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')  
	


Order by [Assigned User],[CCD],[STATUS]

CLOSE SYMMETRIC KEY FS@K3y;
		

	
	
	END TRY
	BEGIN CATCH
		DECLARE @Desc VARCHAR(200)
		SET @Desc='EXEC COWS_Reporting.dbo.sp_DomesticCPEOpsDaily '+', ' 
		SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '
		EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;   --- Open later during deployment
	END CATCH
END
GO


