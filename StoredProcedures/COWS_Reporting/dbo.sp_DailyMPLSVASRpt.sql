USE [COWS_Reporting]
GO
/*
-- =============================================
-- Author:		
-- Create date: 
-- Description: Extract MPLSVAS orders for the previous 30 days.
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- [dbo].[sp_DailyMPLSVASRpt] 1
-- =============================================
*/
ALTER PROCEDURE [dbo].[sp_DailyMPLSVASRpt]
	  @SecuredUser		bit = 0
AS
BEGIN TRY
-- Declare @SecuredUser		bit = 0
	SET NOCOUNT ON;
	
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 

	Declare @startDate Datetime = CONVERT(datetime, GETDATE(), 101)
	Declare @endDate Datetime = CONVERT(datetime, GETDATE(), 101)
	
	SET @startDate = DATEADD(DAY, -30, @startDate)				
	
	SELECT distinct
				 CASE
					WHEN @SecuredUser = 0 and ord.CSG_LVL_ID =0 THEN foc.CUST_NME	
					WHEN @SecuredUser = 0 and ord.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and ord.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
					ELSE coalesce(foc.CUST_NME, CONVERT(varchar, DecryptByKey(csd.CUST_NME)),'')	
				 END [Customer Name] --Secured
			    
				,ISNULL(fs.FTN, '') [FTN]
				,lc.CTRY_NME [Country]
				,ord.H5_H6_CUST_ID [H5 Account Number]
				,ISNULL(ord.CUST_CMMT_DT,fs.CUST_CMMT_DT)[CCD Date]
				,ls.ORDR_STUS_DES [Status]
				,ISNULL(nsi.NUA_ADR, nc.NUA_ADR) [NUA]
				,ot.ORDR_TYPE_DES [Order Type]
				,ORDR_SUB_TYPE_CD [Order Sub Type]
				
		
			FROM [COWS].[dbo].[FSA_ORDR] fs WITH (NOLOCK)
			INNER JOIN [COWS].[dbo].[ORDR] ord WITH (NOLOCK) ON fs.ORDR_ID = ord.ORDR_ID
			INNER JOIN [COWS].[dbo].[LK_ORDR_TYPE] ot WITH (NOLOCK) ON ot.FSA_ORDR_TYPE_CD = fs.ORDR_TYPE_CD
			LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] oa WITH (NOLOCK) 
					ON fs.ORDR_ID = oa.ORDR_ID AND oa.CIS_LVL_TYPE = 'H5'
			LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST]foc WITH (NOLOCK) ON ord.ORDR_ID = foc.ORDR_ID
								AND ord.H5_H6_CUST_ID = foc.CUST_ID
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=foc.FSA_ORDR_CUST_ID  
								AND csd.SCRD_OBJ_TYPE_ID=5
			LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] lc WITH (NOLOCK) ON oa.CTRY_CD = lc.CTRY_CD
			INNER JOIN [COWS].[dbo].[LK_ORDR_STUS] ls with (nolock) on ord.ORDR_STUS_ID = ls.ORDR_STUS_ID
			OUTER APPLY (SELECT TOP 1 NUA_ADR, FTN FROM[COWS].[dbo].[NRM_CKT] WITH (NOLOCK)
					WHERE FTN = fs.FTN
					ORDER by CREAT_DT DESC
								) nc 
			OUTER APPLY (SELECT TOP 1 NUA_ADR, FTN FROM [COWS].[dbo].[NRM_SRVC_INSTC] WITH (NOLOCK)
							WHERE FTN = fs.FTN
							ORDER by CREAT_DT DESC
								) nsi 

													 
			WHERE ORDR_SUB_TYPE_CD = 'MPLSVAS'
				AND fs.ORDR_SBMT_DT >= @startDate AND fs.ORDR_SBMT_DT < @endDate
			
			ORDER BY ISNULL(ord.CUST_CMMT_DT,fs.CUST_CMMT_DT) DESC
					 

CLOSE SYMMETRIC KEY FS@K3y 

	END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_DailyMPLSVASRpt'
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH	





