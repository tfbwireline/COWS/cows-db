USE [COWS_Reporting]
GO
/*
--======================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			06/21/2011
-- Description:		
--					Extract orders that need to be reviewed by the MNS PM.
--
--					QueryNumber: 1 = Daily
--								 2 = Weekly
--								 3 = Monthly
--								 0 = Specify start date & end date: 'mm/dd/yyyy'
--
-- 					"Pending"	= Task Id: 300, Stus Id: 0 (Pending)
-- 					"Approved"	= Task Id: 1000, Stus Id: 0 (Pending)
--
--
-- Modifications:
-- 
-- 11/30/2011 		sxv0766: Removed COWS Event ID,COWS Event Status columns from report
-- 11/05/2011 		sxv0766: Removed Order Submitter/Keyer per MNS Team request. This name
--					is not yet in COWS (from FSA ICD).
-- 03/29/2012		sxv0766: IM952822-Removed resulting string expression from CCD column
-- 01/27/2017		vn370313: No Change for NEW MDS EVENT since NO Table MDS_EVENT table  Used
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--======================================================================================
*/

ALTER Procedure [dbo].[sp_MNSRptOrderTrackingReport] 
	@QueryNumber		INT,
	@Secured			INT=0,
	@inStartDtStr		VARCHAR(10)='',
	@inEndDtStr			VARCHAR(10)=''
AS
BEGIN TRY
	
	SET NOCOUNT ON;
	
	DECLARE @startDate		Datetime
	DECLARE @endDate		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)	
	DECLARE @dayOfWeek		INT
	
	--DECLARE	@Order	TABLE
	--(	
	--	ORDR_ID		INT,
	--	TaskID		INT,
	--	GroupID		INT,
	--	IsCmplt		Bit DEFAULT 0
	--)
	
	IF @QueryNumber=0
		BEGIN

			SET @startDate=CONVERT(datetime, @inStartDtStr, 101)
			SET @endDate=CONVERT(datetime, @inEndDtStr, 101)				

			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			SET @endDate=DATEADD(day, 1, @endDate) 
												
		END	
	ELSE IF @QueryNumber=1
    	BEGIN    	
    		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
		
			SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)   
			
		END
	ELSE IF @QueryNumber=2
		BEGIN
			-- e.g. Run date is '2011-06-21' 3rd day of the week
			-- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000
			
			SET @today=GETDATE()
			SET @dayOfWeek=DATEPART(dw, @today)
			--SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+6), @today), 112) As DateTime)
			--SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS Datetime)
			SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)
				
		END
	ELSE IF @QueryNumber=3
		BEGIN			 
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month 
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
			--SET @startDate=cast(@startDtStr AS datetime)
			
			SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
		END
			
	--INSERT INTO @Order (ORDR_ID, TaskID)
	--	SELECT ord.ORDR_ID, atsk.TASK_ID
	--		FROM COWS.dbo.ORDR ord with (nolock)
	--		JOIN COWS.dbo.ACT_TASK atsk with (nolock) ON ord.ORDR_ID = atsk.ORDR_ID 
	--	WHERE atsk.TASK_ID in (300, 500, 1000) and atsk.STUS_ID = 0
							
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
 	SELECT	CASE
				--WHEN CONVERT(varchar(100), DecryptByKey(fcust.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND ordr.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(fcust.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(fcust.CUST_NME)) 
				WHEN @secured = 0 and ordr.CSG_LVL_ID =0 THEN fcust.CUST_NME
				WHEN @secured = 0 and ordr.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and ordr.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),fcust.CUST_NME,'') 
				END 'Customer Name',
 			ford.FTN 'Order FTN #', 	 			 			
 			CASE  				
 				WHEN atsk.TASK_ID = 1000 THEN 'Bill Activation - Pending'
 				WHEN atsk.TASK_ID = 500 THEN 'Return To Sales'
 				WHEN atsk.TASK_ID = 300 THEN 'New Order Review Pending' END 'COWS Order Status', 
			ordr.H5_H6_CUST_ID 'H5/H6 ID', 				
 			CASE
 				WHEN ordr.CUST_CMMT_DT is not NULL THEN ordr.CUST_CMMT_DT
 					--REPLACE(LEFT(CONVERT(VARCHAR(10),ordr.CUST_CMMT_DT,101),1), '0', '') +
 					--SUBSTRING(CONVERT(VARCHAR(10),ordr.CUST_CMMT_DT,101), 2, 2) +
 					--REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ordr.CUST_CMMT_DT,101), 4, 1), '0', '') +
 					--RIGHT(CONVERT(VARCHAR(10),ordr.CUST_CMMT_DT,101), 6)				
				ELSE ford.CUST_CMMT_DT END 'CCD',
					--REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101),1), '0', '') +
					--SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 2, 2) +
					--REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 4, 1), '0', '') +
					--RIGHT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 6) END 'CCD',				
 			CASE 
 				WHEN ordr.CUST_CMMT_DT is not NULL THEN
 					CASE
 						WHEN DATEDIFF(day, ordr.CUST_CMMT_DT, @startDate) >= 31 THEN 1  
 						WHEN (DATEDIFF(day, ordr.CUST_CMMT_DT, @startDate) >= 5 
 				  		AND DATEDIFF(day, ordr.CUST_CMMT_DT, @startDate ) < 31) THEN 2 END 				
 				WHEN ordr.CUST_CMMT_DT is NULL THEN
 					CASE
 						WHEN DATEDIFF(day, ford.CUST_CMMT_DT, @startDate) >= 31 THEN 1  
 						WHEN (DATEDIFF(day, ford.CUST_CMMT_DT, @startDate) >= 5 
 				  		AND DATEDIFF(day, ford.CUST_CMMT_DT, @startDate ) < 31) THEN 2 END
 				END 'CCD Priority',				
			CASE 
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 'Assigned Acct MNS PM' 						
				
	FROM	COWS.dbo.ORDR ordr with (nolock) 		
		JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON ordr.ORDR_ID = ford.ORDR_ID
		JOIN COWS.dbo.ACT_TASK atsk with (nolock) ON ordr.ORDR_ID = atsk.ORDR_ID
		LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust with (nolock) ON ford.ORDR_ID = fcust.ORDR_ID AND fcust.CIS_LVL_TYPE = 'H1'
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=fcust.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
		JOIN (SELECT distinct ORDR_ID, MNSPM_ID					
					FROM COWS.dbo.ODIE_REQ oreq with (nolock)
					JOIN COWS.dbo.ODIE_RSPN orsp with (nolock) ON oreq.REQ_ID =orsp.REQ_ID
					WHERE orsp.MNSPM_ID is not NULL AND orsp.MNSPM_ID <> '')AS otbl ON ford.ORDR_ID = otbl.ORDR_ID
		--LEFT JOIN COWS.dbo.ODIE_RSPN orsp with (nolock) ON oreq.maxOdieReqId = orsp.REQ_ID
		JOIN COWS.dbo.LK_USER luser2 with (nolock) ON otbl.MNSPM_ID = luser2.USER_ADID				
		--LEFT JOIN (SELECT distinct ORDR_ID, ASN_USER_ID 
		--			FROM COWS.dbo.USER_WFM_ASMT with (nolock)
		--			WHERE GRP_ID = 2 ) as uwasm ON ford.ORDR_ID = uwasm.ORDR_ID							
		--LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON uwasm.ASN_USER_ID = luser2.USER_ID		
		
	-- TASK_ID: 300 = New Order Review, 500 = SS RTS Pending, 1000 = Bill Activation Ready
	-- atsk.WG_PROF_ID: 300 = MDS 
	WHERE atsk.TASK_ID in (300, 500, 1000) AND atsk.STUS_ID = 0 AND 
		((DATEDIFF(day, ordr.CUST_CMMT_DT, @startDate) >= 5) OR (ordr.CUST_CMMT_DT is NULL AND DATEDIFF(day, ford.CUST_CMMT_DT, @startDate) >= 5))
	ORDER BY 'CCD Priority', 'Order FTN #'
			
	CLOSE SYMMETRIC KEY FS@K3y;
	
	RETURN 0;
	
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptOrderTrackingReport '  + CAST(@QueryNumber AS VARCHAR(4))
	SET @Desc=@Desc + ', ' + CAST(@Secured AS VARCHAR(2)) + ': '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH


