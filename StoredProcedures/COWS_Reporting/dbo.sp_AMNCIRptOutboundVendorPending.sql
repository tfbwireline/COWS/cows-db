USE [COWS_Reporting]
GO  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- Updated By:   Md M Monir
-- Updated Date: 03/14/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD  
-- EXEC COWS_Reporting.dbo.sp_AMNCIRptOutboundVendorPending  
-- [dbo].[sp_AMNCIRptOutboundVendorPending]1
-- =============================================  
ALTER PROCEDURE [dbo].[sp_AMNCIRptOutboundVendorPending]  
 @secured  BIT = 0,  
 @inStartDtStr   VARCHAR(10)= '',  
 @inEndDtStr  VARCHAR(10)= ''  
  
AS  
BEGIN TRY  
  
 SET NOCOUNT ON;  
 DECLARE @startDate  DATETIME  
 DECLARE @endDate  DATETIME  
 DECLARE @today   DATETIME  
 DECLARE @dayOfWeek  INT  
--DECLARE @secured  BIT = 0  
--DECLARE @inStartDtStr   VARCHAR(10)= '09/01/2012'  
--DECLARE @inEndDtStr  VARCHAR(10)= '01/30/2012'  
  
 SET @startDate=CONVERT(DATETIME, @inStartDtStr, 101)  
 SET @endDate=CONVERT(DATETIME, @inEndDtStr, 101)  
  
 SET @today=GETDATE()  
 SET @dayOfWeek=DATEPART(dw, @today)  
 SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+90), @today), 112) As DATETIME)  
 SET @endDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS DATETIME)  
   
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
--DECLARE @secured  bit=1 --for testing purposes only  
  
 SELECT DISTINCT  
  CASE  
	WHEN @secured = 0 AND c.CUST_NME<>''  THEN coalesce(c.CUST_NME,'PRIVATE CUSTOMER')
    WHEN @secured = 0 AND c.CUST_NME =''  THEN 'PRIVATE CUSTOMER'  
    WHEN @secured = 0 AND c.CUST_NME IS NULL THEN 'PRIVATE CUSTOMER'  
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),c.CUST_NME,'')
   END [CUSTOMER_NAME], --Secured  
  
  ISNULL(a.ORDR_ID, '') [OE_NUMBER], -- e.TRNSPRT_OE_FTN_NBR  
  ISNULL(x.VNDR_CKT_ID, 0) [CIRCUIT_ID],  
  'N/A'[SOL_NBR],  
  'N/A' [FE],  
  'N/A' [Order_type2],  
  ISNULL(g.ORDR_TYPE_DES, '') [Order_type],  
  ISNULL(z.CNTCT_PROD_TYPE_TXT, '') [PRODUCT_TYPE],  
  CONVERT(VARCHAR(10),a.CREAT_DT,101) [OE_CREATE_DATE],  
  CONVERT(VARCHAR(10),GETDATE(),101) [Current Date],  
  CASE  
   WHEN a.CUST_CMMT_DT IS NULL THEN NULL  
   ELSE  [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(), a.CUST_CMMT_DT) END  [Days Before/past CCD],  
  ISNULL(q.ORDR_STUS_DES, '') [Order Status],  
  CONVERT(VARCHAR(10),a.CUST_CMMT_DT,101) [CCD],  
  ISNULL(s.CTRY_NME, '') [DEST_COUNTRY],  
  '' [OPCD],  
  '' [CD_DTS],  
  ISNULL(k.VNDR_NME, '') [Vendor],  
  ISNULL( bb.FULL_NME, '') [ISS PM],  
  ISNULL( cc.FULL_NME, '') [Manager],  
  CASE  
   WHEN a.CUST_CMMT_DT IS NULL THEN NULL  
   WHEN [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(), a.CUST_CMMT_DT) < 0 THEN 'Past'   
   ELSE 'Due in 2 weeks' END [CCD Past Due Status],  
  '' [tracking #],  
  CASE  
   WHEN ct.ACCS_CUST_MRC_IN_USD_AMT > 0 THEN ct.ACCS_CUST_MRC_IN_USD_AMT END [MRC_1],  
  CASE  
   WHEN ct.ACCS_CUST_NRC_IN_USD_AMT > 0 THEN ct.ACCS_CUST_NRC_IN_USD_AMT END [NRC_1],  
  CASE  
   WHEN ct.ASR_MRC_IN_USD_AMT > 0 THEN ct.ASR_MRC_IN_USD_AMT END [MRC_2],  
  CASE  
   WHEN ct.ASR_NRC_IN_USD_AMT > 0 THEN ct.ASR_NRC_IN_USD_AMT END [NRC_2],  
  CASE  
   WHEN ct.VNDR_MRC_IN_USD_AMT > 0 THEN ct.VNDR_MRC_IN_USD_AMT END [MRC_3],  
  CASE  
   WHEN ct.VNDR_NRC_IN_USD_AMT > 0 THEN ct.VNDR_NRC_IN_USD_AMT END [NRC_3],  
  CASE  
   WHEN ct.ACCS_CUST_MRC_IN_USD_AMT + ct.ASR_MRC_IN_USD_AMT + ct.VNDR_MRC_IN_USD_AMT > 0 THEN   
    ct.ACCS_CUST_MRC_IN_USD_AMT + ct.ASR_MRC_IN_USD_AMT + ct.VNDR_MRC_IN_USD_AMT END [total MRC],  
  CASE  
   WHEN ct.ACCS_CUST_NRC_IN_USD_AMT + ct.ASR_NRC_IN_USD_AMT + ct.VNDR_NRC_IN_USD_AMT > 0 THEN  
    ct.ACCS_CUST_NRC_IN_USD_AMT + ct.ASR_NRC_IN_USD_AMT + ct.VNDR_NRC_IN_USD_AMT END [total NRC]  
    
 FROM [COWS].[dbo].[ORDR] a   
  LEFT OUTER JOIN [COWS].[dbo].[H5_FOLDR] c on c.H5_FOLDR_ID = a.H5_FOLDR_ID  
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=c.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6 
  --[COWS].[dbo].[FSA_ORDR_GOM_XNCI] l on a.ORDR_ID = l.ORDR_ID LEFT OUTER JOIN  
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR] m on a.ORDR_ID = m.VNDR_ORDR_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] g on m.VNDR_ORDR_TYPE_ID = g.ORDR_TYPE_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR_ORDR_TYPE] n on m.VNDR_ORDR_TYPE_ID = n.VNDR_ORDR_TYPE_ID   
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] y on m.VNDR_FOLDR_ID = y.VNDR_FOLDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] k on y.VNDR_CD = k.VNDR_CD   
  LEFT OUTER JOIN [COWS].[dbo].[EMAIL_REQ] o on o.ORDR_ID = a.ORDR_ID   
  --[COWS].[dbo].[LK_XNCI_EMAIL_TYPE_MAPNG] p on o.EMAIL_REQ_TYPE_ID = p.XNCI_EMAIL_TYPE_MAPNG_ID LEFT OUTER JOIN  
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_STUS] q on a.ORDR_STUS_ID = q.ORDR_STUS_ID   
  LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] r on a.ORDR_ID = r.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] s on s.CTRY_CD =  r.CTRY_CD   
  LEFT OUTER JOIN [COWS].[dbo].[CKT] x on m.ORDR_ID = x.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR_CNTCT] z on y.VNDR_FOLDR_ID = z.VNDR_FOLDR_ID  
  --JOIN (SELECT ORDR_ID, MAX(TASK_ID) as TASK_ID  
  -- FROM COWS.dbo.ACT_TASK WITH (NOLOCK)   
  -- WHERE TASK_ID <> 1001 ORDER BY ORDR_ID) at ON at.ORDR_ID = a.ORDR_ID  
  
  --(SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME)) [FRST_NME], CONVERT(varchar,   
  --  DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar, DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]  
  --FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
  --[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
  --WHERE bb.ROLE_ID = 64) ipm on ipm.ORDR_ID = a.ORDR_ID  LEFT OUTER JOIN  
    
  LEFT OUTER JOIN (SELECT bb.CKT_ID, bb.ACCS_CUST_MRC_IN_USD_AMT, bb.ASR_MRC_IN_USD_AMT, bb.VNDR_MRC_IN_USD_AMT,   
   bb.ACCS_CUST_NRC_IN_USD_AMT, bb.ASR_NRC_IN_USD_AMT, bb.VNDR_NRC_IN_USD_AMT, bb.VER_ID  
    FROM [COWS].[dbo].[CKT_COST] bb   
    LEFT OUTER JOIN [COWS].[dbo].[CKT_COST] cc on bb.CKT_ID = cc.CKT_ID   
   WHERE bb.VER_ID = (SELECT MAX(VER_ID)  
    FROM (SELECT VER_ID, CKT_ID FROM [COWS].[dbo].[CKT_COST] ) ckt  
    WHERE ckt.CKT_ID = bb.CKT_ID)) ct on ct.CKT_ID = x.CKT_ID   
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] aa on m.VNDR_FOLDR_ID = aa.VNDR_FOLDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] bb on aa.CREAT_BY_USER_ID = bb.USER_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] cc on cc.USER_ADID = bb.MGR_ADID  
 WHERE   
 a.RGN_ID = 1  -- AMNCI  
 AND a.ORDR_CAT_ID = 3  -- VENDOR  
 AND ct.ACCS_CUST_MRC_IN_USD_AMT + ct.ASR_MRC_IN_USD_AMT + ct.VNDR_MRC_IN_USD_AMT > 0 -- MRC > 0  
 AND ct.ACCS_CUST_NRC_IN_USD_AMT + ct.ASR_NRC_IN_USD_AMT + ct.VNDR_NRC_IN_USD_AMT > 0  -- NRC > 0  
   
 CLOSE SYMMETRIC KEY FS@K3y   
     
-- RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptOutboundVendorPending '  + CAST(@secured AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  
  
  