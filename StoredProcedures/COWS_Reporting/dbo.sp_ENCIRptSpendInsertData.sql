/*  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME 
-- =============================================  
*/  
ALTER PROCEDURE [dbo].[sp_ENCIRptSpendInsertData]  
AS  
BEGIN  
 SET NOCOUNT ON;  
  
OPEN SYMMETRIC KEY FS@K3y   
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
INSERT INTO ENCISpendReport  
SELECT DISTINCT  
ord.ORDR_ID [ENCISpendReport_ID],  
CASE  
WHEN c.RGN_DES = 'ENCI' THEN 'Implements in Europe' END [Region],   
fsa.FTN [FTN#],  
H5.H5_FOLDR_ID [H5 Customer Location],  
ISNULL(k.FULL_NME, k1.FULL_NME) [xNCI Assigned Manager],  
CAST(d.FMS_CKT_ID as integer) [FMS Circuit ID (per circuit)],  
CAST(d.PLN_NME as integer) [FMS PL number],  
CAST(d.PLN_SEQ_NBR as integer) [FMS PL Seq#  (Per Circuit)],  
e.VNDR_CKT_ID [Vendor Circuit ID (Per circuit)],  
fsa.TSUP_RFQ_NBR [RFQ#],  
ord.ORDR_ID [CDM Order OESOTS Number],  
NULL [CMS ID (recorded in CDM)],  
a.INTL_PL_CKT_TYPE_CD [Circuit Type],  
'' [Cost Type  (part of table for Access Cost)],  
h.ORDR_TYPE_DES [Order Type],  
'' [Finance budget: (Optional - cost budget for each circuit)],  
CASE   
 WHEN l.VNDR_MRC_AMT > 0 THEN 'Y' ELSE 'N' END [Do we pay vendor for Access Y/N],  
'' [Is this a new circuit or replacing an existing circuit Y/N],  
n.STUS_DES [Order Status (presented by system based on where the order is in the milestones)],  
j.SALS_CHNL_DES [Sales Channel (drop down menu)],  
    CASE  
  WHEN ord.CSG_LVL_ID =0 THEN si.CUST_NME   
  WHEN ord.CSG_LVL_ID >0 AND si.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ord.CSG_LVL_ID >0 AND si.CUST_NME2 IS NULL THEN NULL   
   END  [Sprint Customer_Name],  
'' [Customer Short Name],  
lkv.VNDR_NME [Local Access Provider (Vendor)],  
--'' [Local Access Provider Group (optional)],  
lkvt.VNDR_TYPE_DES [Vendor Type],  
lkpt.PROD_TYPE_DES [Access Product Type (Optional - requested by ENCI)],  
'' [Access Speed],  
--'' [Access Megabyte Value],  
'' [Length of Circuit (Kms) (Optional)],   
f.PROD_TYPE_DES [Product Type],  
'' [A-End Address],  
--h5.CUST_CTY_NME [A-End City],
CASE WHEN (h5.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE h5.CUST_CTY_NME END AS  [A-End City], /*Monir 06062018*/  
--lkc.CTRY_NME [A-End Country],
CASE WHEN (h5.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE lkc.CTRY_NME END [A-End Country], /*Monir 06062018*/    
'' [A-End International Postcode],  
NULL [Pop Number],  
'' [B-End Address],  
'' [B-End City],  
'' [B-End Country],  
'' [B-End International Postcode],  
--'' [B-end Country Region],  
DATEDIFF(MONTH,cktms.CNTRC_STRT_DT, cktms.CNTRC_END_DT) [Customer Access Contract Term],  
DATEDIFF(MONTH,cktms.CNTRC_STRT_DT, cktms.CNTRC_END_DT)  [Vendor Contract Term (Per Circuit)],  
----cktms.CNTRC_END_DT [Vendor Contract Term End Date (Per Circuit)],  
cktms.ACCS_DLVRY_DT [Access Delivery Date (per circuit)],  
ISNULL(ordms.CUST_ACPTC_TURNUP_DT, ordms.ORDR_BILL_CLEAR_INSTL_DT)[Order Install Date (Corresponds to Install date in FMS)],  
ISNULL(cktms.TRGT_DLVRY_DT_RECV_DT, ordms.ORDR_BILL_CLEAR_INSTL_DT) [Vendor Bill Start Date],  
--DATEPART(MONTH,ISNULL(cktms.ACCS_ACPTC_DT, ordms.ORDR_BILL_CLEAR_INSTL_DT)) [Local Access Installed Month],  
--DATEPART(YEAR,ISNULL(cktms.ACCS_ACPTC_DT, ordms.ORDR_BILL_CLEAR_INSTL_DT)) [Local Access Installed Year Vendor Bill],  
DATEPART(MONTH,cktms.VNDR_CNFRM_DSCNCT_DT) [Stop Date Vendor Bill Stop Month],  
--DATEPART(YEAR,cktms.VNDR_CNFRM_DSCNCT_DT) [Vendor Bill Stop Year],  
ISNULL(q.ACCS_CUST_NRC_IN_USD_AMT, 0) [Access NRC  Per Circuit],  
ISNULL(q.ACCS_CUST_MRC_IN_USD_AMT, 0) [Access MRC per circuit],  
'' [Currency Type  (part of table for Access Cost)],  
ordms.ORDR_BILL_CLEAR_INSTL_DT [Customer Billing Start date],  
--DATEPART(YEAR, ordms.ORDR_BILL_CLEAR_INSTL_DT) [Customer Billing Start Year],  
'' [Customer Billing Stop date],  
--'' [Customer Billing Stop Month],  
--'' [Customer Billing Stop Year],  
ISNULL(q.ACCS_CUST_NRC_IN_USD_AMT, 0) [Customer Access NRC],  
ISNULL(q.ACCS_CUST_MRC_IN_USD_AMT, 0) [Customer Access MRC],  
'' [Customer billing currency],  
SUBSTRING(p.ORDR_CMNT_TXT,0,255) [ASR Comments   (from NRM)],  
'' [IPT Availability Comments:],  
'' [Comments:  Ability to enter internal (tracking) and external not]  
--q.VNDR_NRC_IN_USD_AMT [Vendor Installation Cost US$],  
--q.VNDR_MRC_IN_USD_AMT [Monthly Vendor Rental Cost US$],  
--'' [Customer Installation Revenue US$],  
--'' [Customer Monthly Rental Revenue US$],  
--'' [Markup Ratio @ Install],  
--'' [Monthly Unit Cost US$]  
FROM [COWS].[dbo].[ORDR] ord LEFT OUTER JOIN  
[COWS].[dbo].[FSA_ORDR] a on ord.ORDR_ID = a.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_XNCI_RGN] c on ord.RGN_ID = c.RGN_ID  LEFT OUTER JOIN  
--[COWS].[dbo].[NRM_CKT] d on a.FTN = d.FTN  LEFT OUTER JOIN  
[COWS].[dbo].[NRM_VNDR] e on a.FTN = e.FTN LEFT OUTER JOIN  
[COWS].[dbo].[LK_PROD_TYPE] f on a.PROD_TYPE_CD = f.FSA_PROD_TYPE_CD  LEFT OUTER JOIN  
[COWS].[dbo].[CKT] g on a.ORDR_ID = g.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_ORDR_TYPE] h on a.ORDR_TYPE_CD = h.FSA_ORDR_TYPE_CD LEFT OUTER JOIN  
[COWS].[dbo].[TRPT_ORDR] i on a.ORDR_ID = i.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_SALS_CHNL] j on i.SALS_CHNL_ID = j.SALS_CHNL_ID LEFT OUTER JOIN  
[COWS].[dbo].[FSA_ORDR_GOM_XNCI] xnci on ord.ORDR_ID = xnci.ORDR_ID LEFT OUTER JOIN  
(SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID  
 FROM [COWS].[dbo].[USER_WFM_ASMT] bb LEFT OUTER JOIN   
 [COWS].[dbo].[FSA_ORDR_GOM_XNCI] cc on bb.ASN_USER_ID = cc.CREAT_BY_USER_ID  
 WHERE bb.ASMT_DT = (SELECT MAX(ISNULL(ASMT_DT,0))  
 FROM (SELECT DISTINCT ASMT_DT, ORDR_ID FROM [COWS].[dbo].[USER_WFM_ASMT] ) usr    
 WHERE usr.ORDR_ID  = bb.ORDR_ID )  
 AND bb.ASN_USER_ID  = (SELECT MAX(ISNULL(ASN_USER_ID,0))   
 FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID FROM [COWS].[dbo].[USER_WFM_ASMT] )uas  
 WHERE uas.ORDR_ID = bb.ORDR_ID)) wfm  
 on xnci.ORDR_ID = wfm.ORDR_ID LEFT OUTER JOIN  
(SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID   
 FROM [COWS].[dbo].[USER_WFM_ASMT] bb LEFT OUTER JOIN   
 [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
 WHERE bb.ASMT_DT = (SELECT MAX(ISNULL(ASMT_DT,0))  
 FROM (SELECT DISTINCT ASMT_DT, ORDR_ID FROM [COWS].[dbo].[USER_WFM_ASMT] ) usr    
 WHERE usr.ORDR_ID  = bb.ORDR_ID )  
 AND bb.ASN_USER_ID  = (SELECT MAX(ISNULL(ASN_USER_ID,0))   
 FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID FROM [COWS].[dbo].[USER_WFM_ASMT] )uas  
 WHERE uas.ORDR_ID = bb.ORDR_ID)) ur   
 on ur.ORDR_ID = a.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_USER] k1  on ur.ASN_USER_ID  =  k1.USER_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_USER] k  on wfm.ASN_USER_ID =  k.USER_ID LEFT OUTER JOIN  
  
[COWS].[dbo].[CKT_COST] l on g.CKT_ID = l.CKT_ID  LEFT OUTER JOIN  
        
    (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID   
     FROM [COWS].[dbo].[ACT_TASK] bb LEFT OUTER JOIN  
  [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ACT_TASK_ID = (SELECT MAX(ISNULL(ACT_TASK_ID,0))  
   FROM (SELECT ACT_TASK_ID, ORDR_ID FROM [COWS].[dbo].[ACT_TASK] ) tsk   
   WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk on tk.ORDR_ID = a.ORDR_ID LEFT OUTER JOIN  
     
      [COWS].[dbo].[LK_TASK] m  on tk.TASK_ID = m.TASK_ID LEFT OUTER JOIN  
      [COWS].[dbo].[LK_STUS]n  on m.ORDR_STUS_ID = n.STUS_ID  LEFT OUTER JOIN  
          (SELECT bb.ORDR_ID, bb.SOI_CD,   
           CONVERT(varchar, DecryptByKey(csd.CUST_NME))   
           [CUST_NME2],bb.CUST_NME AS [CUST_NME]  
     FROM [COWS].[dbo].[FSA_ORDR_CUST] bb LEFT OUTER JOIN  
  [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID  LEFT OUTER JOIN   
  [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
  WHERE bb.SOI_CD = (SELECT MAX(ISNULL(SOI_CD,0))  
   FROM (SELECT SOI_CD, ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] ) soi  
   WHERE soi.ORDR_ID = bb.ORDR_ID)) si on si.ORDR_ID = a.ORDR_ID  
LEFT OUTER JOIN [COWS].[dbo].[H5_FOLDR] H5 on ord.H5_FOLDR_ID = H5.H5_FOLDR_ID
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=h5.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6 
LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD  
LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] fsa on ord.ORDR_ID = fsa.ORDR_ID LEFT OUTER JOIN  
--[FSA_ORDR_GOM_XNCI] b on ord.ORDR_ID = b.ORDR_ID  LEFT OUTER JOIN  
--[LK_ORDR_STUS] c on ord.ORDR_STUS_ID = c.ORDR_STUS_ID LEFT OUTER JOIN  
[COWS].[dbo].[IPL_ORDR] ipl on ord.ORDR_ID = ipl.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[TRPT_ORDR] trpt on ord.ORDR_ID = trpt.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_SALS_CHNL] lksc on trpt.SALS_CHNL_ID = lksc.SALS_CHNL_ID LEFT OUTER JOIN  
--[NRM_CKT] g on fsa.FTN = g.FTN LEFT OUTER JOIN  
(SELECT bb.PLN_NME, bb.PLN_SEQ_NBR, bb.FTN, bb.FMS_CKT_ID from [COWS].[dbo].[NRM_CKT]bb JOIN [COWS].[dbo].[FSA_ORDR] cc on bb.FTN = cc.FTN   
WHERE bb.PLN_SEQ_NBR = (SELECT MAX(ISNULL(PLN_SEQ_NBR,0)) FROM (SELECT FTN, PLN_SEQ_NBR FROM [COWS].[dbo].[NRM_CKT]) nckt where cc.FTN = nckt.FTN)) d on fsa.FTN = d.FTN LEFT OUTER JOIN  
[COWS].[dbo].[LK_PROD_TYPE] lkpt on fsa.PROD_TYPE_CD = lkpt.FSA_PROD_TYPE_CD LEFT OUTER JOIN  
[COWS].[dbo].[ORDR_ADR] oadr on ord.ORDR_ID = oadr.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_VNDR_TYPE] lkvt on trpt.VNDR_TYPE_ID = lkvt.VNDR_TYPE_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_FSA_ORDR_TYPE]lkot on fsa.ORDR_TYPE_CD = lkot.FSA_ORDR_TYPE_CD LEFT OUTER JOIN  
[COWS].[dbo].[NRM_VNDR] nrmv on fsa.FTN = nrmv.FTN LEFT OUTER JOIN  
[COWS].[dbo].[LK_ORDR_STUS] lkos on ord.ORDR_STUS_ID = lkos.ORDR_STUS_ID LEFT OUTER JOIN  
[COWS].[dbo].[CKT] ckt on ord.ORDR_ID = ckt.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[CKT_MS] cktm on ckt.CKT_ID = cktm.CKT_ID LEFT OUTER JOIN  
[COWS].[dbo].[NRM_SRVC_INSTC] p on fsa.FTN = p.FTN LEFT OUTER JOIN  
--[CKT_COST] q on ckt.CKT_ID = q.CKT_ID  LEFT OUTER JOIN  
(SELECT bb.CKT_ID, SUM(bb.VNDR_MRC_IN_USD_AMT)VNDR_MRC_IN_USD_AMT, bb.VNDR_CUR_ID,   
 bb.ASR_CUR_ID, bb.ACCS_CUST_CUR_ID, SUM(bb.ACCS_CUST_MRC_IN_USD_AMT) ACCS_CUST_MRC_IN_USD_AMT,   
 SUM(bb.ACCS_CUST_NRC_AMT)ACCS_CUST_NRC_AMT, SUM(bb.ASR_NRC_AMT) ASR_NRC_AMT,   
 SUM(bb.VNDR_NRC_AMT)VNDR_NRC_AMT, SUM(bb.VNDR_NRC_IN_USD_AMT)VNDR_NRC_IN_USD_AMT,  
 SUM(bb.ASR_MRC_IN_USD_AMT) ASR_MRC_IN_USD_AMT, SUM(bb.ACCS_CUST_NRC_IN_USD_AMT) ACCS_CUST_NRC_IN_USD_AMT  
  FROM [COWS].[dbo].[CKT_COST] bb LEFT OUTER JOIN [COWS].[dbo].[CKT] cc on bb.CKT_ID = cc.CKT_ID   
 WHERE bb.VER_ID = (SELECT MAX(ISNULL(VER_ID,0)) FROM (SELECT CKT_ID, VER_ID FROM [COWS].[dbo].[CKT_COST] ) ver  
               WHERE bb.CKT_ID = ver.CKT_ID)   
     GROUP BY bb.CKT_ID, VNDR_CUR_ID, ASR_CUR_ID, ACCS_CUST_CUR_ID, VNDR_NRC_AMT )q on  ckt.CKT_ID = q.CKT_ID LEFT OUTER JOIN  
(SELECT bb.* FROM [COWS].[dbo].[CKT_MS] bb LEFT OUTER JOIN [COWS].[dbo].[CKT] cc on bb.CKT_ID = cc.CKT_ID  
 WHERE bb.VER_ID = (SELECT MAX(ISNULL(VER_ID,0)) FROM (SELECT CKT_ID, VER_ID from [COWS].[dbo].[CKT_MS]) ver  
      WHERE bb.CKT_ID = ver.CKT_ID)) cktms on ckt.CKT_ID = cktms.CKT_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_CUR] lkcur on q.VNDR_CUR_ID = lkcur.CUR_ID    LEFT OUTER JOIN  
[COWS].[dbo].[LK_CUR] lkcur2 on q.ASR_CUR_ID  = lkcur2.CUR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_CUR] lkcur3 on q.ACCS_CUST_CUR_ID = lkcur3.CUR_ID  LEFT OUTER JOIN  
[COWS].[dbo].[LK_XNCI_RGN] lkxr on ord.RGN_ID = lkxr.RGN_ID LEFT OUTER JOIN  
--[H5_FOLDR] h5 on ord.H5_FOLDR_ID = h5.H5_FOLDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_CTRY] lkc on h5.ctry_cd = lkc.ctry_cd LEFT OUTER JOIN  
[COWS].[dbo].[FSA_ORDR_CUST] fsaoc on fsa.ORDR_ID = fsaoc.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[VNDR_ORDR] vo on ord.ORDR_ID = vo.ORDR_ID LEFT OUTER JOIN   
[COWS].[dbo].[VNDR_FOLDR] vf on vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_VNDR] lkv on vf.VNDR_CD = lkv.VNDR_CD LEFT OUTER JOIN  
[COWS].[dbo].[ORDR_MS] ordms on ord.ORDR_ID = ordms.ORDR_ID  
  
WHERE c.RGN_DES = 'ENCI'  
AND(ord.ORDR_STUS_ID in (2,5) )  
 AND ord.H5_FOLDR_ID IS NOT NULL  
 AND ord.ORDR_ID not in (Select [CDM ID #] from [COWS_Reporting].[dbo].[AMNCISprintFacilityRevenueData])  
CLOSE SYMMETRIC KEY FS@K3y   
END  
  
  