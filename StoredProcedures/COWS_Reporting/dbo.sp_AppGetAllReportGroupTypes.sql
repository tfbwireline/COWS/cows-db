USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_AppGetAllReportGroupTypes]    Script Date: 8/20/2014 1:03:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_AppGetAllReportGroupTypes')
EXEC ('CREATE PROCEDURE [dbo].[sp_AppGetAllReportGroupTypes] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO


ALTER proc [dbo].[sp_AppGetAllReportGroupTypes]
As
BEGIN TRY

SET NOCOUNT ON;

IF EXISTS (select 'x' from master.dbo.sysprocesses where
			spid = @@SPID and hostname='PVMX0667')
BEGIN
	SELECT [RPT_GRP_TYPE_CD]
		   ,[RPT_TMPLT_FULL_PATH_NME]
		  ,[RPT_SAVE_AS_FULL_PATH_NME]
	  FROM [COWS_Reporting].[dbo].[LK_RPT_GRP_TYPE] order by RPT_GRP_TYPE_CD 
END
ELSE
BEGIN
	SELECT [RPT_GRP_TYPE_CD]
		   ,REPLACE([RPT_TMPLT_FULL_PATH_NME],'\\pvmx0668\prod_e$\qda','\\pvmx6160\PROD_F$\12S') as [RPT_TMPLT_FULL_PATH_NME]
		  ,REPLACE([RPT_SAVE_AS_FULL_PATH_NME],'\\pvmx0668\prod_e$\qda','\\pvmx6160\PROD_F$\12S') as [RPT_SAVE_AS_FULL_PATH_NME]
	  FROM [COWS_Reporting].[dbo].[LK_RPT_GRP_TYPE] order by RPT_GRP_TYPE_CD 
END
	
		
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC sp_AppGetAllReportGroupTypes '
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH








