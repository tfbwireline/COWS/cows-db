USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_FedlineWklyVPNCompletionRptOD]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_FedlineWklyVPNCompletionRptOD')
EXEC ('CREATE PROCEDURE [dbo].[sp_FedlineWklyVPNCompletionRptOD] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

--=======================================================================================
-- Project:			PJ006439 - COWS Reporting 
-- Author:			David Phillips	
-- Date:			09/20/2012
-- Description:		
--		Comprehensive list of install orders/events that details the customer specific information
--      for the Device ID and location being implemented. It also details per order/event if the 
--      activity met the agreed upon contract SLA and if applicable the responsible party for missing
--      the SLA. Report is a canned report to be run weekly to keep track of on-going activity, provide
--      an overall status report, and track order issues. Report will also be available to run on an 
--  
-- Modifications:
-- dlp0278 - PJ8795 - 6/27/13 Added Production Tunnel Status field to report.
-- dlp0278 - 6/27/13 Added a lookup to the FEDLINE_EVENT_MSG table to get the Rework reason.
--
--=======================================================================================

ALTER Procedure [dbo].[sp_FedlineWklyVPNCompletionRptOD]
     			@ReqActSrtDt  Datetime = NULL,
				@ReqActEndDt  Datetime = NULL,
				@OrdSubSrtDt  datetime = NULL,
				@OrdSubEndDt  datetime = NULL,
				@ORGID        VARCHAR (40) = '',
				@CustName     VARCHAR (100) = '',
				@Device       VARCHAR (15) = '',
				@getSensData  CHAR(1)='N',
				@SecuredUser  INT = 0,
				@startDate DATETIME = NULL,
				@endDate DATETIME = NULL


AS
BEGIN TRY

	DECLARE @aSQL			NVARCHAR(max)
	
	CREATE TABLE #tmp_fedline_VPN_Comp_rpt
	(					
		CustName				VARCHAR(100),
		State					VARCHAR(25),
		Ctry					VARCHAR(50),
		FRB_ID					VARCHAR(25),
		Event_ID				VARCHAR(25),
		DeviceId				VARCHAR(100),
		Event_Stus				VARCHAR(200),
		Tnnl_Cd                 VARCHAR(50),
		ActType					VARCHAR(50),
		OrdSubDt				VARCHAR(25),
		ReqActDt				VARCHAR(25),
		ActTime					VARCHAR(50),
		EvtComp					VARCHAR(50),
		SprintTme				VARCHAR(25),
		CustReqTme				VARCHAR(25),
		SLAmet					VARCHAR(5),
		Party					VARCHAR(100),
		OrigStrt				VARCHAR(100),
		Activator				VARCHAR(100),
		EventLastAct			VARCHAR(25),
		UpdLastAct				VARCHAR(50),
		Expedite				VARCHAR(1),
		ExpFee					VARCHAR(5),
		Config					VARCHAR(10),
		ORG_ID					VARCHAR(50),
		Trk_Nbr					VARCHAR(100),
		Config_Eng				VARCHAR(100),
		OrigSubDt               VARCHAR(50)
	)
	
	SET NOCOUNT ON;
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SET @aSQL='INSERT INTO #tmp_fedline_VPN_Comp_rpt(CustName, State, Ctry, FRB_ID, Event_ID, DeviceID, Event_Stus,Tnnl_Cd, ActType, OrdSubDt, ReqActDt, ActTime, EvtComp, SprintTme, CustReqTme, SLAmet, Party,OrigStrt, Activator, EventLastAct, UpdLastAct, Expedite, ExpFee, Config, ORG_ID, Trk_Nbr, Config_Eng,OrigSubDt)
	
	SELECT DISTINCT ISNULL(CONVERT(VARCHAR, DecryptByKey(fd.CUST_NME)),''''),ISNULL(CONVERT(VARCHAR, DecryptByKey(fa.STT_CD)),''''),ISNULL(c.CTRY_NME,''''),ISNULL(CONVERT(VARCHAR (25), fd.FRB_REQ_ID),''''),ISNULL(CONVERT(VARCHAR (25), fd.EVENT_ID),''''),ISNULL(fd.DEV_NME,''''),CASE ls.EVENT_STUS_DES
			WHEN ''Rework'' THEN ''Rework  -  '' + RSPN_DES	ELSE ls.EVENT_STUS_DES END ,CASE WHEN ud.TNNL_CD = 1 THEN ''1 tunnel up – CC1''	WHEN ud.TNNL_CD = 2 THEN ''1 tunnel up – CC3'' WHEN ud.TNNL_CD = 3 THEN ''2 tunnel up''	ELSE '''' END 
		   ,ISNULL(ot.PRNT_ORDR_TYPE_DES, ''''),ISNULL(CONVERT(VARCHAR (12), fd.REQ_RECV_DT, 107), ''''),ISNULL(CONVERT(VARCHAR (12), fd.STRT_TME, 107), ''''),ISNULL((SUBSTRING(CONVERT(varchar, fd.STRT_TME, 109), 13, 8) + '' - '' + SUBSTRING(CONVERT(varchar, fd.END_TME, 109), 13, 8)), ''''),ISNULL(CONVERT(VARCHAR (12),(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh WHERE eh.EVENT_ID = fd.EVENT_ID AND eh.ACTN_ID = ''17''),107), '''') 
		   ,ISNULL(DATEDIFF(day,fd.REQ_RECV_DT,(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh	WHERE eh.EVENT_ID = fd.EVENT_ID AND eh.ACTN_ID = ''17'')),''''),ISNULL(DATEDIFF(day,fd.REQ_RECV_DT,fd.STRT_TME),'''')
		   ,ISNULL(ud.SLA_CD,'''') 
		   ,ISNULL(ud.FAIL_CD_ID, ISNULL((SELECT TOP 1 FAIL_CD_ID from [COWS].[dbo].[FEDLINE_REQ_ACT_HIST]
										 WHERE fd.EVENT_ID = EVENT_ID ORDER BY CREAT_DT DESC),''''))
		   ,ISNULL((SELECT top 1 STRT_TME FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA ftd	WHERE ftd.CREAT_DT < (SELECT MAX(Creat_DT) FROM COWS.dbo.EVENT_HIST WHERE EVENT_ID = fd.EVENT_ID AND ACTN_ID = 15) AND ftd.EVENT_ID = fd.EVENT_ID ORDER BY ftd.CREAT_DT), ''''),ISNULL(ua.FULL_NME, ''''),ISNULL(CONVERT(VARCHAR (12),(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh WHERE eh.EVENT_ID = fd.EVENT_ID),107), ''''),(SELECT max(REQ_RECV_DT) from COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA 
			WHERE ORDR_TYPE_CD = ''NDM'' AND FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID),ISNULL(fd.IS_EXPEDITE_CD,''''),CASE WHEN fd.IS_EXPEDITE_CD = 1 AND REPLMT_REAS_NME = ''Defective Device - Sprint Mis-config'' THEN ''Yes'' ELSE ''N/A'' END,ISNULL(fd.DSGN_TYPE_CD,''''),ISNULL(CONVERT(VARCHAR, fd.ORG_ID),''''),ISNULL(ud.TRK_NBR, ''''),ISNULL(ue.FULL_NME, ''''),ISNULL((SELECT REQ_RECV_DT FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA WITH (NOLOCK) WHERE EVENT_ID = fd.EVENT_ID AND ORDR_TYPE_CD IN (''NCI'',''DCS'')),'''')
 	 FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA fd with (NOLOCK) INNER JOIN COWS.dbo.FEDLINE_EVENT_USER_DATA ud with (NOLOCK) ON ud.EVENT_ID = fd.EVENT_ID INNER JOIN COWS.dbo.LK_EVENT_STUS ls with (NOLOCK) ON ud.EVENT_STUS_ID = ls.EVENT_STUS_ID AND ls.REC_STUS_ID = 1
		INNER JOIN COWS.dbo.LK_FEDLINE_ORDR_TYPE ot with (NOLOCK) ON ot.ORDR_TYPE_CD = fd.ORDR_TYPE_CD LEFT OUTER JOIN COWS.dbo.LK_USER ua with(NOLOCK) ON ua.USER_ID = ud.ACTV_USER_ID	LEFT OUTER JOIN COWS.dbo.LK_USER ue with(NOLOCK) ON ue.USER_ID = ud.ENGR_USER_ID LEFT OUTER JOIN COWS.dbo.FEDLINE_EVENT_ADR fa with (NOLOCK) ON fa.FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID AND fa.ADR_TYPE_ID = 18
		LEFT OUTER JOIN COWS.dbo.LK_CTRY c WITH (NOLOCK) ON c.CTRY_CD = fa.CTRY_CD LEFT OUTER JOIN COWS.dbo.FEDLINE_EVENT_MSG fem WITH (NOLOCK)	ON fd.FEDLINE_EVENT_ID = fem.FEDLINE_EVENT_ID AND fem.FEDLINE_EVENT_MSG_STUS_NME = ''Rework''
	 WHERE fd.REC_STUS_ID = 1'  
		

		IF ((@startDate != NULL) AND (@endDate != NULL))
	BEGIN
		IF @ReqActSrtDt <> ''
			SET  @aSQL=@aSQL + ' AND fd.STRT_TME >= ''' + convert(varchar(20),@ReqActSrtDt) + '''' 
		
		IF @ReqActEndDt <> ''
			BEGIN
				SET @ReqActEndDt = DATEADD(day, 1, @ReqActEndDt)
				SET  @aSQL=@aSQL + ' AND fd.STRT_TME <= ''' + convert(varchar(20),@ReqActEndDt) + ''''	
			END

		IF @OrdSubSrtDt <> ''
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT >= ''' + convert(varchar(20),@OrdSubSrtDt) + '''' 
		
		IF @OrdSubEndDt <> ''
			BEGIN
				SET @OrdSubEndDt = DATEADD(day, 1, @OrdSubEndDt)
				SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT <= ''' + convert(varchar(20),@OrdSubEndDt) + ''''	
			END
	
		IF @ORGID <> ''
			SET  @aSQL=@aSQL + ' AND fd.ORG_ID = ''' + @ORGID + '''' 

		IF @Device <> ''
			SET  @aSQL=@aSQL + ' AND fd.DEV_NME like (''' + '%' + @Device + '%' + ''')' 
		
		IF @CustName  <> ''
			SET  @aSQL=@aSQL + ' AND DecryptByKey(fd.CUST_NME) like (''' + '%' +  @CustName  + '%' + ''')'  
	END
	ELSE
	BEGIN
		IF @startDate <> ''
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT >= ''' + convert(varchar(20),@startDate) + '''' 
		
		IF @endDate <> ''
			BEGIN
				SET @endDate = DATEADD(day, 1, @endDate)
				SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT <= ''' + convert(varchar(20),@endDate) + ''''	
			END
	END

		
	SET @aSQL= @aSQL + ' Union All
    
    	SELECT DISTINCT ISNULL(CONVERT(VARCHAR, DecryptByKey(fd.CUST_NME)),'''') 
		   ,ISNULL(CONVERT(VARCHAR, DecryptByKey(fa.STT_CD)),''''),ISNULL(c.CTRY_NME,''''),ISNULL(CONVERT(VARCHAR (25), fd.FRB_REQ_ID),''''),ISNULL(CONVERT(VARCHAR (25), fd.EVENT_ID),'''') 
		   ,ISNULL(fd.DEV_NME,''''),CASE ls.EVENT_STUS_DES WHEN ''Rework'' THEN ''Rework  -  '' + RSPN_DES ELSE ls.EVENT_STUS_DES END 
		   ,CASE WHEN ud.TNNL_CD = 1 THEN ''1 tunnel up – CC1''	WHEN ud.TNNL_CD = 2 THEN ''1 tunnel up – CC3'' WHEN ud.TNNL_CD = 3 THEN ''2 tunnel up'' ELSE '''' END,ISNULL(ot.PRNT_ORDR_TYPE_DES, '''') 
		   ,ISNULL(CONVERT(VARCHAR (12), fd.REQ_RECV_DT, 107), ''''),ISNULL(CONVERT(VARCHAR (12), fd.STRT_TME, 107), ''''),ISNULL((SUBSTRING(CONVERT(varchar, fd.STRT_TME, 109), 13, 8) + '' - '' + SUBSTRING(CONVERT(varchar, fd.END_TME, 109), 13, 8)), '''') 
		   ,ISNULL(CONVERT(VARCHAR (12),(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh WHERE eh.EVENT_ID = fd.EVENT_ID AND eh.ACTN_ID = ''17''),107), '''') 
		   ,ISNULL(DATEDIFF(day,fd.REQ_RECV_DT,(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh WHERE eh.EVENT_ID = fd.EVENT_ID AND eh.ACTN_ID = ''17'')),'''')   
		   ,ISNULL(DATEDIFF(day,fd.REQ_RECV_DT,fd.STRT_TME),'''')
		   ,ISNULL(ud.SLA_CD,'''') 
		   ,ISNULL(ud.FAIL_CD_ID, ISNULL((SELECT TOP 1 FAIL_CD_ID from [COWS].[dbo].[FEDLINE_REQ_ACT_HIST]
										 WHERE fd.EVENT_ID = EVENT_ID ORDER BY CREAT_DT DESC),''''))
		   ,ISNULL((SELECT top 1 STRT_TME FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA ftd	WHERE ftd.CREAT_DT < (SELECT MAX(Creat_DT) FROM COWS.dbo.EVENT_HIST WHERE EVENT_ID = fd.EVENT_ID AND ACTN_ID = 15)AND ftd.EVENT_ID = fd.EVENT_ID ORDER BY ftd.CREAT_DT), '''')   
            ,ISNULL(ua.FULL_NME, ''''),ISNULL(CONVERT(VARCHAR (12),(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh WHERE eh.EVENT_ID = fd.EVENT_ID),107), '''')   
		    ,(SELECT max(REQ_RECV_DT) from COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA WHERE ORDR_TYPE_CD = ''NDM'' AND FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID)  
			,ISNULL(fd.IS_EXPEDITE_CD,''''),CASE WHEN fd.IS_EXPEDITE_CD = 1 AND REPLMT_REAS_NME = ''Defective Device - Sprint Mis-config'' THEN ''Yes'' ELSE ''N/A'' END 
			,ISNULL(fd.DSGN_TYPE_CD,''''),ISNULL(CONVERT(VARCHAR, fd.ORG_ID),''''),ISNULL(ud.TRK_NBR, ''''),ISNULL(ue.FULL_NME, ''''),ISNULL((SELECT REQ_RECV_DT FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA WITH (NOLOCK) WHERE EVENT_ID = fd.EVENT_ID AND ORDR_TYPE_CD IN (''NCI'',''DCS'')),'''') 
		FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA fd	INNER JOIN COWS.dbo.FEDLINE_EVENT_USER_DATA ud ON ud.EVENT_ID = fd.EVENT_ID INNER JOIN COWS.dbo.LK_EVENT_STUS ls ON ud.EVENT_STUS_ID = ls.EVENT_STUS_ID AND ls.REC_STUS_ID = 1
		INNER JOIN COWS.dbo.LK_FEDLINE_ORDR_TYPE ot ON ot.ORDR_TYPE_CD = fd.ORDR_TYPE_CD LEFT OUTER JOIN COWS.dbo.LK_USER ua ON ua.USER_ID = ud.ACTV_USER_ID LEFT OUTER JOIN COWS.dbo.LK_USER ue with(NOLOCK) ON ue.USER_ID = ud.ENGR_USER_ID
		LEFT OUTER JOIN COWS.dbo.FEDLINE_EVENT_ADR fa ON fa.FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID AND fa.ADR_TYPE_ID = 18 LEFT OUTER JOIN COWS.dbo.LK_CTRY c ON c.CTRY_CD = fa.CTRY_CD	
		LEFT OUTER JOIN COWS.dbo.FEDLINE_EVENT_MSG fem WITH (NOLOCK)ON fd.FEDLINE_EVENT_ID = fem.FEDLINE_EVENT_ID AND fem.FEDLINE_EVENT_MSG_STUS_NME = ''Rework''
    	WHERE fd.REC_STUS_ID = 1 AND fd.EVENT_ID in (SELECT d.EVENT_ID from COWS.dbo.FEDLINE_EVENT_MSG m INNER JOIN COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA d on m.FEDLINE_EVENT_ID = d.FEDLINE_EVENT_ID AND d.ORDR_TYPE_CD = ''NCI''
							WHERE FEDLINE_EVENT_MSG_STUS_NME = ''Rework'') ' 
    	
   	IF ((@startDate != NULL) AND (@endDate != NULL))
	BEGIN
	   IF @ReqActSrtDt <> ''
			SET  @aSQL=@aSQL + ' AND fd.STRT_TME >= ''' + convert(varchar(20),@ReqActSrtDt) + '''' 
		
		IF @ReqActEndDt <> ''
			BEGIN
				SET @ReqActEndDt = DATEADD(day, 1, @ReqActEndDt)
				SET  @aSQL=@aSQL + ' AND fd.STRT_TME <= ''' + convert(varchar(20),@ReqActEndDt) + ''''	
			END

		IF @OrdSubSrtDt <> ''
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT >= ''' + convert(varchar(20),@OrdSubSrtDt) + '''' 
		
		IF @OrdSubEndDt <> ''
			BEGIN
				SET @OrdSubEndDt = DATEADD(day, 1, @OrdSubEndDt)
				SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT <= ''' + convert(varchar(20),@OrdSubEndDt) + ''''	
			END
	
		IF @ORGID <> ''
			SET  @aSQL=@aSQL + ' AND fd.ORG_ID = ''' + @ORGID + '''' 

		IF @Device <> ''
			SET  @aSQL=@aSQL + ' AND fd.DEV_NME like (''' + '%' + @Device + '%' + ''')' 
		
		IF @CustName  <> ''
			SET  @aSQL=@aSQL + ' AND DecryptByKey(fd.CUST_NME) like (''' + '%' +  @CustName  + '%' + ''')'  	
	END
	ELSE
	BEGIN
		IF @startDate <> ''
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT >= ''' + convert(varchar(20),@startDate) + '''' 
		
		IF @endDate <> ''
			BEGIN
				SET @endDate = DATEADD(day, 1, @endDate)
				SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT <= ''' + convert(varchar(20),@endDate) + ''''	
			END
	END
	
	
	EXEC sp_executesql @aSQL;
	
	--	PRINT @aSQL 
		
	SELECT	DISTINCT
		CASE
			WHEN @SecuredUser = 0 AND CustName <> '' THEN 'Private Customer' 
			WHEN @SecuredUser = 1 AND @getSensData='N' AND CustName <> '' THEN 'Private Customer'
			ELSE CustName 
		END					AS 'Customer Name'
		,ORG_ID             AS 'ORG ID'
		,CASE
			WHEN @SecuredUser = 0 AND State <> '' THEN 'Private Addr' 
			WHEN @SecuredUser = 1 AND @getSensData='N' AND State <> '' THEN 'Private Addr'
			ELSE State 
		END					AS 'State'
		,Ctry				AS 'Country'
		,FRB_ID				AS 'FRB ID'
		,OrigSubDt			AS 'Initial FRB Order Submission Date'
		,OrdSubDt			AS 'FRB Order Submission Date'
		,CASE	
			WHEN Expedite = 0	THEN 'No'
			WHEN Expedite = 1   THEN 'Yes'
		 END				AS 'Expedite'
		,ExpFee             AS 'Expedite Fee Waived'   
		,Event_ID			AS 'COWS Event ID'	
		,ReqActDt			AS 'Requested Activity Date'
		,ActTime			AS 'Requested Activity Time Block'
		,Event_Stus			AS 'COWS Event Status'
		,Tnnl_Cd            AS 'Production Tunnel Status'
		,DeviceId			AS 'Device ID/Asset ID'
		,Trk_Nbr            AS 'Shipping Tracking #'
		,Config             AS 'Configuration Type'
		,ActType			AS 'Activity Type'
		,EvtComp			AS 'Event Completion Date'
		,SprintTme			AS 'Sprint Time To Complete'
		,CustReqTme			AS 'Customer Requested Time To Complete'
		,CASE 
			WHEN ActType = 'Disconnect' THEN ''
			WHEN SLAmet = 'N' THEN 'No'
			ELSE 'Yes'
		 END				AS 'SLA Met?'
		,ISNULL(fc.RSPB_PARTY_NME, '')	AS 'Responsible Party for Missed SLA'
		,CASE
			WHEN datediff(hour,(SELECT MAX(Creat_DT) FROM COWS.dbo.EVENT_HIST with (NOLOCK)
						WHERE EVENT_ID = tfd.EVENT_ID AND ACTN_ID = 15),OrigStrt) = -2 THEN 'Yes'
			WHEN datediff(hour,(SELECT MAX(Creat_DT) FROM COWS.dbo.EVENT_HIST with (NOLOCK) 
						WHERE EVENT_ID = tfd.EVENT_ID AND ACTN_ID = 15),OrigStrt) = -1 THEN 'Yes'
			WHEN datediff(hour,(SELECT MAX(Creat_DT) FROM COWS.dbo.EVENT_HIST with (NOLOCK) 
						WHERE EVENT_ID = tfd.EVENT_ID AND ACTN_ID = 15),OrigStrt) = 0 THEN 'Yes'
			WHEN Event_Stus = 'Rework  -  Customer No Show' THEN 'Yes'
			ELSE 'N/A'
		 END AS 'No Show Fee Assessed?'
		,Config_Eng         AS 'Fedline Preconfig Engineer'
		,Activator			AS 'Fedline MDS Activator'
		,EventLastAct		AS 'Event Last Action Date'
		,ISNULL(CONVERT(VARCHAR (26), UpdLastAct, 107),'') AS 'Last Update To Requested Activity Date/Time Block'
		
 FROM #tmp_fedline_VPN_Comp_rpt tfd
 LEFT OUTER JOIN [COWS].[dbo].[LK_FEDLINE_EVENT_FAIL_CD] fc ON tfd.Party = fc.FAIL_CD_ID
	WHERE FRB_ID > 0
		ORDER BY DeviceId, Event_ID 
 
		
		
		
		
	CLOSE SYMMETRIC KEY FS@K3y;

	IF OBJECT_ID('tempdb..#tmp_fedline_VPN_Comp_rpt', 'U') IS NOT NULL	
		drop table #tmp_fedline_VPN_Comp_rpt;	
		
		
		
	RETURN 0;



END TRY
BEGIN CATCH

	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_FedlineWklyVPNCompletionRptOD '  
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @ReqActSrtDt, 101) 
		+ ', ' + CONVERT(VARCHAR(10), @ReqActEndDt, 101) 
		+ ', ' + @SecuredUser + ', ' + @getSensData
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;

END CATCH



GO
