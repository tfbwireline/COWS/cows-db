USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_FedlineShipStusRptOD]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_FedlineShipStusRptOD')
EXEC ('CREATE PROCEDURE [dbo].[sp_FedlineShipStusRptOD] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

--=======================================================================================
-- Project:			PJ006439 - COWS Reporting 
-- Author:			David Phillips	
-- Date:			09/20/2012
-- Description:		
--					Summary of install ONLY events that shows status of devices and 
--					locations being installed along with the applicable device 
--					shipping details. This is OnDemand Report.										
--					
-- Modifications:
-- dlp0278 - PJ8795 - 6/27/13 Added Production Tunnel Status field to report.
--
--=======================================================================================

ALTER Procedure [dbo].[sp_FedlineShipStusRptOD]
				@startDate  DATETIME = NULL,
				@endDate  DATETIME = NULL, 
				@getSensData  CHAR(1)='N',
				@SecuredUser  INT = 0

AS
BEGIN TRY

		DECLARE @aSQL			VARCHAR(5000)
	
	CREATE TABLE #tmp_fedline_ship_rpt
	(					
		CustName				VARCHAR(100),
		State					VARCHAR(50),
		Ctry					VARCHAR(50),
		FRB_ID					VARCHAR(25),
		Event_ID				VARCHAR(25),
		Org_ID					VARCHAR(25),
		Event_Stus				VARCHAR(50),
		Tnnl_Cd                 VARCHAR(50),
		ActType					VARCHAR(50),
		ReqActDt				VARCHAR(100),
		DeviceId				VARCHAR(100),
		ConfigEng				VARCHAR(100),
		ShipDt                  VARCHAR(100),
		Courier                 VARCHAR(100),
		TrkNbr					VARCHAR(100),
		Expedite				VARCHAR(50)
	)
	
	SET NOCOUNT ON;
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
		SET @aSQL='INSERT INTO #tmp_fedline_ship_rpt(CustName, State, Ctry, FRB_ID, Event_ID, Org_ID,
				Event_Stus, Tnnl_Cd, ActType, ReqActDt, DeviceId, ConfigEng, ShipDt, Courier, TrkNbr, Expedite)
	
	SELECT DISTINCT ISNULL(CONVERT(VARCHAR, DecryptByKey(fd.CUST_NME)),'''') -- Customer Name
		   ,ISNULL(CONVERT(VARCHAR, DecryptByKey(fa.STT_CD)),'''') -- State
		   ,ISNULL(c.CTRY_NME,'''') -- Country
		   ,ISNULL(CONVERT(VARCHAR, fd.FRB_REQ_ID),'''') -- FRB ID
		   ,ISNULL(CONVERT(VARCHAR, fd.EVENT_ID),'''') -- COWS Event ID
		   ,ISNULL(CONVERT(VARCHAR, fd.ORG_ID),'''') -- ORG ID
		   ,ISNULL(ls.EVENT_STUS_DES, '''') -- COWS Event Status
		   ,CASE 
				WHEN ud.TNNL_CD = 1 THEN ''1 tunnel up – CC1''
				WHEN ud.TNNL_CD = 2 THEN ''1 tunnel up – CC3''
				WHEN ud.TNNL_CD = 3 THEN ''2 tunnel up''
				ELSE ''''
			END -- Production Tunnel Status
		   ,ISNULL(ot.PRNT_ORDR_TYPE_DES, '''') -- Activity Type
		   ,ISNULL(CONVERT(VARCHAR (26), fd.STRT_TME, 109), '''') -- Requested Activity Date
		   ,ISNULL(fd.DEV_NME,'''') -- Device ID/Asset ID
		   ,ISNULL(lu.FULL_NME, '''') -- Fedline Preconfig Engineer
		   ,ISNULL(CONVERT(VARCHAR (12), ud.ARRIVAL_DT, 107), '''') -- Shipping Arrival Date
		   ,ISNULL(ud.SHIPPING_CXR_NME, '''') -- Shipping Courier
		   ,ISNULL(ud.TRK_NBR, '''') -- Shipping Tracking #
		   ,CASE 
				WHEN fd.IS_EXPEDITE_CD = 1 THEN ''Yes''
				WHEN fd.IS_EXPEDITE_CD = 0 THEN ''No''
				END -- Expedite
	 FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA fd
		INNER JOIN COWS.dbo.FEDLINE_EVENT_USER_DATA ud ON ud.EVENT_ID = fd.EVENT_ID 
		INNER JOIN COWS.dbo.LK_EVENT_STUS ls ON ud.EVENT_STUS_ID = ls.EVENT_STUS_ID AND ls.REC_STUS_ID = 1
		INNER JOIN COWS.dbo.LK_FEDLINE_ORDR_TYPE ot ON ot.ORDR_TYPE_CD = fd.ORDR_TYPE_CD
		INNER JOIN COWS.dbo.LK_USER lu ON lu.USER_ID = ud.ENGR_USER_ID
		INNER JOIN COWS.dbo.FEDLINE_EVENT_ADR fa ON fa.FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID
							AND fa.ADR_TYPE_ID = 18
		INNER JOIN COWS.dbo.LK_CTRY c ON c.CTRY_CD = fa.CTRY_CD
		
	WHERE fd.ORDR_TYPE_CD in (''NCI'',''HEI'',''NCM'',''NDM'',''NIM'') 
			AND fd.REC_STUS_ID = 1'
			
	IF @startDate <> ''
		BEGIN
			SET @startDate = DATEADD(day, -1, @startDate)
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT >= ''' + convert(varchar(20),@startDate) + '''' 
		END
	IF @endDate <> ''
		BEGIN
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT <= ''' + convert(varchar(20),@endDate) + ''''	
		END	
	
	EXEC (@aSQL);

	SELECT 	
			CASE
				WHEN @SecuredUser = 0 AND CustName <> '' THEN 'Private Customer' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND CustName <> '' THEN 'Private Customer'
				ELSE CustName 
			END				AS 'Customer Name'
			,Org_ID         AS 'ORG ID'
			,CASE
				WHEN @SecuredUser = 0 AND State <> '' THEN 'Private Addr' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND State <> '' THEN 'Private Addr'
				ELSE State 
			END				AS 'State'
			,Ctry			AS 'Country'			
			,FRB_ID			AS 'FRB ID'
			,Expedite       AS 'Expedite'
			,Event_ID		AS 'COWS Event ID'
			,Event_Stus		AS 'COWS Event Status'
			,Tnnl_Cd        AS 'Production Tunnel Status'
			,ReqActDt		AS 'Requested Activity Date/Time'
			,DeviceId		AS 'Device ID/Asset ID'
			,ActType		AS 'Activity Type'
			,ConfigEng		AS 'Fedline Preconfig Engineer'
			,ShipDt			AS 'Shipping Arrival Date'
			,Courier		AS 'Shipping Courier'
			,TrkNbr         AS 'Shipping Tracking #'

	FROM #tmp_fedline_ship_rpt
	WHERE FRB_ID > 0

	CLOSE SYMMETRIC KEY FS@K3y;

	IF OBJECT_ID('tempdb..#tmp_fedline_ship_rpt', 'U') IS NOT NULL	
		drop table #tmp_fedline_ship_rpt;	

	RETURN 0;			

		
CLOSE SYMMETRIC KEY FS@K3y;

	RETURN 0;

END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_FedlineShipStusRptOD ' + ', ' 
		+ CONVERT(VARCHAR(12), @startDate, 101) + ', '
		+ CONVERT(VARCHAR(12), @endDate, 101) + ', '
		+ CONVERT(VARCHAR (1),@SecuredUser) + ', ' + @getSensData   
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH
GO
