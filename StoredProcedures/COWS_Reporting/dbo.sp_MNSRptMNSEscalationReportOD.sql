USE [COWS_Reporting]
GO
/*
--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			07/20/2011
-- Description:		
--					Extract MDS events that have been submitted as escalations for
--					specified parameter(s);
--					Event Status, Customer Name, H5_H6_ID, and Date Range. 
--
--  09/23/2011		sxv0766: Added @h5_h6_ID
--	09/25/2011		sxv0766: Added @getSensData
--  11/02/2011		sxv0766: Updated Event Date format
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--==================================================================================
*/
ALTER PROCEDURE [dbo].[sp_MNSRptMNSEscalationReportOD] 
	@SecuredUser		INT=0,
	@getSensData		CHAR(1)='N',
	@startDate			Datetime=NULL,
	@endDate			Datetime=NULL
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDateStr	VARCHAR(20)
	DECLARE @endDateStr		VARCHAR(20)	

	SET @startDate=CONVERT(DATETIME, @startDate, 101)  
    SET @endDate=CONVERT(DATETIME, @endDate, 101)      
    -- Add 1 day to End Date in order to cover the End Date 24-hr period  
    SET @endDate=DATEADD(DAY, 1, @endDate)	
	
	DECLARE  @tmp_mns_escl TABLE
	(
		Cust_Name				VARCHAR(100),
		Cust_Name2			VARCHAR(100),
		Scurd_Cd				Bit,
		Event_ID				INT,
		Event_Submit_Dt			VARCHAR(10),
		Event_Schedule_Dt		VARCHAR(10),		
		Event_Status			VARCHAR(50),
		CPE_Req					VARCHAR(10),
		Event_Type				VARCHAR(25),
		IPM_Name				VARCHAR(100),
		Assigned_MNS_PM			VARCHAR(100)
	)
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	-- MDS Activity Type: Initial Install and MAC
	
	INSERT INTO @tmp_mns_escl(Cust_Name, Scurd_Cd, Event_ID, Event_Submit_Dt, Event_Schedule_Dt, 
				Event_Status, CPE_Req, Event_Type, IPM_Name, Assigned_MNS_PM)
			SELECT 
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,	
		    mds.EVENT_ID ,			
			REPLACE(LEFT(CONVERT(VARCHAR(10),mds.CREAT_DT,101),1), '0', '') +
				SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 4, 1), '0', '') +
				RIGHT(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 6) ,
			REPLACE(LEFT(CONVERT(VARCHAR(10),mds.STRT_TMST,101),1), '0', '') +
				SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 4, 1), '0', '') +
				RIGHT(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 6) ,
			UPPER(evst.EVENT_STUS_DES) ,
			CASE mds.SPRINT_CPE_NCR_ID
				WHEN NULL THEN 'NO'
				WHEN 3 THEN 'NO'
				ELSE 'YES'
				END ,
			mact.MDS_ACTY_TYPE_DES ,	
			CASE
				WHEN luser1.DSPL_NME is NULL THEN ''			
				ELSE luser1.DSPL_NME END ,			
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 		
		FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)	
			JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
			JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
			JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID 
			JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
			JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 		
			LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID				
		WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) AND mds.ESCL_CD = 1  
             AND mds.STRT_TMST >=COALESCE(@startDate,mds.STRT_TMST )  AND mds.STRT_TMST<COALESCE(@endDate,mds.STRT_TMST)   
	-- MDS Activity Type: Disconnect
	UNION
	SELECT 
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,	
		    mds.EVENT_ID ,			
			REPLACE(LEFT(CONVERT(VARCHAR(10),mds.CREAT_DT,101),1), '0', '') +
				SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 4, 1), '0', '') +
				RIGHT(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 6) ,
			REPLACE(LEFT(CONVERT(VARCHAR(10),mds.STRT_TMST,101),1), '0', '') +
				SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 4, 1), '0', '') +
				RIGHT(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 6) ,
			UPPER(evst.EVENT_STUS_DES) ,
			CASE mds.SPRINT_CPE_NCR_ID
				WHEN NULL THEN 'NO'
				WHEN 3 THEN 'NO'
				ELSE 'YES'
				END ,
			mact.MDS_ACTY_TYPE_DES ,	
			CASE
				WHEN luser1.DSPL_NME is NULL THEN ''			
				ELSE luser1.DSPL_NME END ,			
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 		
		FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)	
			JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
			LEFT JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
			JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID 
			JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
			JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 		
			LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID				
		WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) AND mds.ESCL_CD = 1 
				AND mds.STRT_TMST >=COALESCE(@startDate,mds.STRT_TMST )  AND mds.STRT_TMST<COALESCE(@endDate,mds.STRT_TMST)   
	
	UNION ALL
	
	SELECT 
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,	
		    mds.EVENT_ID ,			
			REPLACE(LEFT(CONVERT(VARCHAR(10),mds.CREAT_DT,101),1), '0', '') +
				SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 4, 1), '0', '') +
				RIGHT(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 6) ,
			REPLACE(LEFT(CONVERT(VARCHAR(10),mds.STRT_TMST,101),1), '0', '') +
				SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 4, 1), '0', '') +
				RIGHT(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 6) ,
			UPPER(evst.EVENT_STUS_DES) ,
			CASE mds.SPRINT_CPE_NCR_ID
				WHEN NULL THEN 'NO'
				WHEN 3 THEN 'NO'
				ELSE 'YES'
				END ,
			mact.MDS_ACTY_TYPE_DES ,	
			CASE
				WHEN luser1.DSPL_NME is NULL THEN ''			
				ELSE luser1.DSPL_NME END ,			
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 		
		FROM COWS.dbo.MDS_EVENT mds with (nolock)	
			JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
			JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
			JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID 
			JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
			JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 		
			LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID				
		WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) AND mds.ESCL_CD = 1  
             AND mds.STRT_TMST >=COALESCE(@startDate,mds.STRT_TMST )  AND mds.STRT_TMST<COALESCE(@endDate,mds.STRT_TMST)   
	-- MDS Activity Type: Disconnect
			UNION
	    SELECT 
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,	
		    mds.EVENT_ID ,			
			REPLACE(LEFT(CONVERT(VARCHAR(10),mds.CREAT_DT,101),1), '0', '') +
				SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 4, 1), '0', '') +
				RIGHT(CONVERT(VARCHAR(10),mds.CREAT_DT,101), 6) ,
			REPLACE(LEFT(CONVERT(VARCHAR(10),mds.STRT_TMST,101),1), '0', '') +
				SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 4, 1), '0', '') +
				RIGHT(CONVERT(VARCHAR(10),mds.STRT_TMST,101), 6) ,
			UPPER(evst.EVENT_STUS_DES) ,
			CASE mds.SPRINT_CPE_NCR_ID
				WHEN NULL THEN 'NO'
				WHEN 3 THEN 'NO'
				ELSE 'YES'
				END ,
			mact.MDS_ACTY_TYPE_DES ,	
			CASE
				WHEN luser1.DSPL_NME is NULL THEN ''			
				ELSE luser1.DSPL_NME END ,			
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 		
		FROM COWS.dbo.MDS_EVENT mds with (nolock)	
			JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
			LEFT JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
			JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID 
			JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
			JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 		
			LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID				
		WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) AND mds.ESCL_CD = 1 
				AND mds.STRT_TMST >=COALESCE(@startDate,mds.STRT_TMST )  AND mds.STRT_TMST<COALESCE(@endDate,mds.STRT_TMST)   
	
	
	
	--IF @startDate is NOT NULL AND @endDate is NOT NULL
	--	BEGIN
	--		-- Do the conversion before passing them to @aSQL
	--		SET @startDateStr = @startDate
	--		SET @endDateStr = @endDate
		
	--		SET @aSQL=@aSQL + 'AND mds.STRT_TMST >= ''' + @startDateStr + ''' AND mds.STRT_TMST < DATEADD(day, 1, ''' + @endDateStr + ''') ' 					
	--		SET @aSQLd=@aSQLd + 'AND mds.STRT_TMST >= ''' + @startDateStr + ''' AND mds.STRT_TMST < DATEADD(day, 1, ''' + @endDateStr + ''') '
	--	END
			
	--SET @aSQL=@aSQL + 'UNION ' + @aSQLd
	
	--EXEC (@aSQL)
	--=======================================	
	SELECT	CASE 
				--WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--ELSE Cust_Name 
				WHEN @SecuredUser = 0 and Scurd_Cd =0 THEN CUST_NAME
				WHEN @SecuredUser = 0 and Scurd_Cd >0 AND  CUST_NAME2 <>'' THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd >0 AND CUST_NAME2 <> '' THEN 'Private Customer'
				ELSE COALESCE(CUST_NAME2,CUST_NAME,'')	
			END 'Customer Name',	
			Event_ID 'COWS Event ID',
			Event_Submit_Dt 'Event Submit Date',
			Event_Schedule_Dt 'Event Schedule Date',
			Event_Status 'Event Status',
			CPE_Req 'CPE Requested',
			Event_Type 'Event Type',
			IPM_Name 'IPM Name', 
			Assigned_MNS_PM	'Assigned Acct MNS PM'
	
	FROM @tmp_mns_escl
	
	
	
	CLOSE SYMMETRIC KEY FS@K3y;
	
	RETURN 0;
			
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(300)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptMNSEscalationReportOD, '
	SET @Desc=@Desc +  CAST(@SecuredUser AS VARCHAR(2)) 
	SET @Desc=@Desc + ', ''' + @getSensData + ''', ' 
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH
