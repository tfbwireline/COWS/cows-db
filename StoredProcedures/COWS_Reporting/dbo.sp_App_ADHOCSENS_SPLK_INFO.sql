USE [COWS_Reporting]
GO
/*
--axm3320 7/23 Add Inprogress Action to the select query actn_id = 19
--axm3320: 12/4/2012 added action publish
--axm3320:3/19/2013 Replace action_id 15(reschedule) with 44 (reschedule-email notfn sent)
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
*/

ALTER proc [dbo].[sp_App_ADHOCSENS_SPLK_INFO]
 AS
 
 
 BEGIN TRY
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
    -- Insert statements for procedure here
    
    
Declare @SQLSELECT1 as varchar(max)
Declare @SQLSELECT2 as varchar(max)
Declare @SQLSELECT3 as varchar(max)
 
  set @SQLSELECT1 = 'With SPLK_Event_Data as (select distinct a.FTN,  
	splk.SPLK_Event_ID [SPLK_Event_ID],
    UPPER(splk.SPLK_Event_Status) [SPLK_Event_Status],   
	CASE WHEN ordr.CSG_LVL_ID = 0 THEN splk.SPLK_Item_Title 	
	WHEN ordr.CSG_LVL_ID > 0 AND splk.SPLK_Item_Title2 IS NOT NULL THEN ''PRIVATE CUSTOMER''
	WHEN ordr.CSG_LVL_ID > 0 AND splk.SPLK_Item_Title2 IS NULL THEN '''' END [SPLK_Item_Title],    		  
	CASE WHEN ordr.CSG_LVL_ID = 0 THEN splk.splk_Cust_Name 	
	WHEN ordr.CSG_LVL_ID > 0 AND splk.splk_Cust_Name2 IS NOT NULL THEN ''PRIVATE CUSTOMER''
	WHEN ordr.CSG_LVL_ID > 0 AND splk.splk_Cust_Name2 IS NULL THEN '''' END [SPLK_Cust_Name],    		   
	CASE WHEN splk.SPLK_DES_CMNT_TXT is NULL THEN '''' ELSE splk.SPLK_DES_CMNT_TXT END [SPLK_DES_CMNT_TXT],
	splk.SPLK_EVENT_TYPE_DES [SPLK_EVENT_TYPE_DES],
	splk.SPLK_ACTY_TYPE_DES  [SPLK_ACTY_TYPE_DES],
	splk.SPLK_IP_VER_ID [SPLK_IP_VER_ID],
    splk.SPLK_STRT_TMST [SPLK_STRT_TMST],	
    splk.splk_EVENT_DRTN_IN_MIN_QTY [SPLK_EVENT_DRTN_IN_MIN_QTY],
    splk.SPLK_EXTRA_DRTN_TME_AMT [SPLK_EXTRA_DRTN_TME_AMT],	
    splk.SPLK_END_TMST [SPLK_END_TMST],
    splk.SPLK_ACTN_DES [SPLK_ACTN_DES],
    splk.SPLK_MOD_DT 	[SPLK_MOD_DT],	
	splk.SPLK_MOD_BY [SPLK_MOD_BY],				
	CASE WHEN splk.SPLK_SUCSS_ACTY_DES is NULL THEN '''' ELSE splk.SPLK_SUCSS_ACTY_DES END [SPLK_SUCSS_ACTY_DES],		
	CASE WHEN splk.SPLK_FAIL_ACTY_DES is NULL THEN '''' ELSE splk.SPLK_FAIL_ACTY_DES END [SPLK_FAIL_ACTY_DES],	
	CASE WHEN splk.SPLK_CMNT_TXT is NULL THEN '''' ELSE splk.SPLK_CMNT_TXT END [SPLK_CMNT_TXT],
	case when splk.SPLK_ESCL_CD = 1 THEN ''Y'' else ''N'' END  [SPLK_ESCL_CD],
	case when splk.MDS_MNGD_CD = 1 THEN ''Y'' else ''N'' END  [SPLK_MDS_MNGD_CD],
	case when splk.RELTD_CMPS_NCR_CD = 1 THEN ''Y'' else ''N'' END  [SPLK_RELTD_CMPS_NCR_CD]
   from COWS.dbo.ORDR ordr with (nolock)
   left outer join (SELECT bb.FTN,bb.ORDR_ID  FROM [COWS].[dbo].[FSA_ORDR] bb with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
		FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
		WHERE fsa.FTN = bb.FTN)) a on a.ORDR_ID = ordr.ORDR_ID '


   set @SQLSELECT2 = ' full outer join ( select distinct splk.EVENT_ID [SPLK_Event_ID], splk.FTN,  
			UPPER(splkevst.EVENT_STUS_DES) [SPLK_Event_Status], 						
            CASE WHEN CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) is NULL THEN '''' ELSE CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) END [SPLK_Item_Title2],	
            CASE WHEN CONVERT(varchar, DecryptByKey(csd.CUST_NME)) is NULL THEN '''' ELSE CONVERT(varchar, DecryptByKey(csd.CUST_NME)) END [SPLK_Cust_Name2],
            splk.EVENT_TITLE_TXT AS [SPLK_Item_Title],	
            splk.CUST_NME AS [SPLK_Cust_Name],
			CASE WHEN splk.DSGN_CMNT_TXT is NULL THEN '''' ELSE splk.DSGN_CMNT_TXT END [SPLK_DES_CMNT_TXT],			
			lkmt.SPLK_EVENT_TYPE_DES  ,	
             lkma.SPLK_ACTY_TYPE_DES ,			
			CASE WHEN CAST(splk.IP_VER_ID AS VARCHAR(3)) is NULL THEN ''''
				ELSE CAST(splk.IP_VER_ID AS VARCHAR(3)) END [SPLK_IP_VER_ID],			
			splk.STRT_TMST  [SPLK_STRT_TMST],							
			splk.EVENT_DRTN_IN_MIN_QTY [SPLK_EVENT_DRTN_IN_MIN_QTY],
			splk.EXTRA_DRTN_TME_AMT [SPLK_EXTRA_DRTN_TME_AMT],				
			splk.END_TMST  [SPLK_END_TMST],	
			CASE
				WHEN lkac.ACTN_DES = ''Completed Email Notification'' THEN ''Completed'' 
				ELSE lkac.ACTN_DES END [SPLK_ACTN_DES],			
			sphi.CREAT_DT [SPLK_MOD_DT],							
			CASE WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END  [SPLK_MOD_BY],
			CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '''' ELSE scev.SUCSS_ACTY_DES END [SPLK_SUCSS_ACTY_DES],			
			CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '''' ELSE flev.FAIL_ACTY_DES END [SPLK_FAIL_ACTY_DES],				
			CASE WHEN sphi.CMNT_TXT is NULL THEN '''' ELSE sphi.CMNT_TXT END [SPLK_CMNT_TXT],
			splk.ESCL_CD [SPLK_ESCL_CD],
			splk.MDS_MNGD_CD [MDS_MNGD_CD],
			splk.RELTD_CMPS_NCR_CD [RELTD_CMPS_NCR_CD]
			from COWS.dbo.SPLK_EVENT splk with (nolock) 		
			left outer 	JOIN COWS.dbo.LK_EVENT_STUS splkevst with (nolock) ON splk.EVENT_STUS_ID = splkevst.EVENT_STUS_ID		
			left outer 	JOIN COWS.dbo.LK_SPLK_EVENT_TYPE lkmt with (nolock) ON splk.SPLK_EVENT_TYPE_ID  = lkmt.SPLK_EVENT_TYPE_ID
            left outer 	JOIN COWS.dbo.LK_SPLK_ACTY_TYPE lkma with (nolock) ON splk.SPLK_ACTY_TYPE_ID = lkma.SPLK_ACTY_TYPE_ID
			left outer 	JOIN COWS.dbo.AD_EVENT_ACCS_TAG splkaatg with (nolock) ON splk.EVENT_ID = splkaatg.EVENT_ID 
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=splk.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=19		  
		    JOIN
			(		SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT,eh.CMNT_TXT 
					FROM COWS.dbo.EVENT_HIST eh with (nolock)					
					JOIN COWS.dbo.SPLK_EVENT sple with (nolock) ON eh.EVENT_ID = sple.EVENT_ID
					WHERE eh.ACTN_ID in (5,44,19) AND sple.EVENT_STUS_ID in (3, 6) 
				--axm3320	WHERE eh.ACTN_ID in (5,10,12,15,19) and sple.EVENT_STUS_ID not in (8)
				UNION
				SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT,ehis.CMNT_TXT 
					FROM COWS.dbo.EVENT_HIST ehis with (nolock)
					JOIN
					(
						SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) ''maxEvHis''
							FROM COWS.dbo.EVENT_HIST eh with (nolock)
							JOIN COWS.dbo.SPLK_EVENT sple with (nolock) ON eh.EVENT_ID = sple.EVENT_ID
							WHERE eh.ACTN_ID in (18) AND sple.EVENT_STUS_ID = 6 
						GROUP BY eh.EVENT_ID
					)ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis
			)sphi ON splk.EVENT_ID = sphi.EVENT_ID						
			LEFT JOIN
			(
				SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lksu.SUCSS_ACTY_DES  + '', '' AS [text()] 
	 						FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)
	 						JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID 
	 						WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID --AND evsu.REC_STUS_ID = 1
	 						FOR xml PATH ('''')
							) as SUCSS_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)	
					)evSucAct
		  		)evSucAct2
			)scev ON sphi.EVENT_HIST_ID = scev.EVENT_HIST_ID
			LEFT JOIN
			(
				SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lkfl.FAIL_ACTY_DES  + '', '' AS [text()] 
	 						FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)
	 						JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID 
	 						WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID --AND evfl.REC_STUS_ID = 1
	 						FOR xml PATH ('''')
							) as FAIL_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)	
					)evFailAct
		  		)evFailAct2
			)flev ON sphi.EVENT_HIST_ID = flev.EVENT_HIST_ID			
			JOIN COWS.dbo.LK_USER luser1 with (nolock) ON sphi.CREAT_BY_USER_ID = luser1.USER_ID 
			JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON sphi.ACTN_ID = lkac.ACTN_ID ) as splk on splk.FTN = a.FTN ) '
            
   
set @SQLSELECT3 = ' select * from SPLK_Event_Data '

--print (@SQLSELECT1) 
--print (@SQLSELECT2) 
--print (@SQLSELECT3) 

exec ( @SQLSELECT1 + @SQLSELECT2 + @SQLSELECT3)

CLOSE SYMMETRIC KEY FS@K3y;


END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc= 'EXEC COWS_Reporting.dbo.sp_App_ADHOCSENS_SPLK_INFO'
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH

