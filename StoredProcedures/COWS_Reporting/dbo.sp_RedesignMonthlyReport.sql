USE [COWS_Reporting]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

----USE [COWS]
--GO
--/****** Object:  StoredProcedure [AD\xw557671].[RedesignMonthlyReport]    Script Date: 09/16/2015 07:30:59 ******/
--SET ANSI_NULLS ON
--GO
--SET QUOTED_IDENTIFIER ON
--GO
---- [dbo].[sp_RedesignMonthlyReport] 1
---- =============================================
---- Author:		AD\xw557671
---- Create date: 09/16/2015
---- Description:	<Description,,>
---- Updated By: Md M monir
---- Updated date: 01/23/2017
---- Description:	added Error Trap
-- Updated by:     vn370313
-- Updated For:    Added New Fields
-- Updated Date:   04282017
-- Updated by:     vn370313
-- Updated For:    Customer cant be seen
-- Updated Date:   07032018
-- =============================================
ALTER PROCEDURE [dbo].[sp_RedesignMonthlyReport]
	@Secured			   INT=0
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY



DECLARE @startDate	Datetime=DATEADD(mm,-1,CONVERT(VARCHAR(25),DATEADD(DD,-(DAY(GETDATE())-1),GETDATE()),101))
DECLARE	@endDate	Datetime= GETDATE()
	--DECLARE	@endDate	Datetime=DATEADD(dd,-19, GETDATE())
	--print  @startDate
	--Print  @endDate

		
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
    
    
	SELECT DISTINCT 	r.REDSGN_NBR						AS	'Redesign Number',
						r.SUBMIT_DT							AS  'Submitted Date' ,
						r.SLA_DT							AS  'SLA Due Date',
						r.EXPRTN_DT							AS  'Expiration Date',
						lrt.REDSGN_TYPE_NME					AS  'Redesign Type',
						r.H1_CD								AS  'H1',
						CASE WHEN (@Secured=1 AND r.CSG_LVL_ID>0) THEN ISNULL(dbo.decryptBinaryData(csd.CUST_NME), '')
						     WHEN (@Secured=0 AND r.CSG_LVL_ID>0) THEN 'Private Customer'  
							 ELSE ISNULL(r.CUST_NME, '') END AS	'Customer Name', 
						ls.STUS_DES							AS	'Status',
						lut.DSPL_NME						AS  'NTE Assigned',
						lup.DSPL_NME						AS  'PM Assigned',
						lus.DSPL_NME						AS  'MSS SE Assigned',
						lun.DSPL_NME						AS  'NE Assigned',
						ISNULL(r.NTE_LVL_Effort_AMT,0)		AS  'LOE',
						ISNULL(r.MSS_IMPL_EST_AMT,0)		AS  'MSS',
						r.COST_AMT							AS  'COST',
						CASE  r.STUS_ID WHEN '225' THEN  ( SELECT TOP 1 RN.CRETD_DT  FROM COWS.[dbo].[REDSGN_NOTES] RN with (nolock) Where r.REDSGN_ID = RN.REDSGN_ID  AND  (RN.Notes Like '%Status changed to Approved%' OR RN.Notes Like '%Status Update: Approved by NTE%') ORDER By RN.CRETD_DT DESC )
										WHEN '227' THEN  ( SELECT TOP 1 RN.CRETD_DT  FROM COWS.[dbo].[REDSGN_NOTES] RN with (nolock) Where r.REDSGN_ID = RN.REDSGN_ID  AND  (RN.Notes Like '%Status changed to Approved%' OR RN.Notes Like '%Status Update: Approved by NTE%') ORDER By RN.CRETD_DT DESC )	 
											       ELSE  NULL  END		AS  'Approval Date',
						CASE  r.STUS_ID WHEN '227' THEN (SELECT TOP 1 RT.CRETD_DT  FROM COWS.[dbo].[REDSGN_NOTES] RT with (nolock) WHERE r.REDSGN_ID = RT.REDSGN_ID  AND RT.Notes Like '%Status changed to Completed%'  ORDER By RT.CRETD_DT DESC )	
												   ELSE  NULL  END		AS  'Completion Date',
						CASE  r.STUS_ID WHEN '226' THEN (SELECT TOP 1 RT.CRETD_DT  FROM COWS.[dbo].[REDSGN_NOTES] RT with (nolock) WHERE r.REDSGN_ID = RT.REDSGN_ID  AND RT.Notes Like '%Cancelled%'  ORDER By RT.CRETD_DT DESC )	
												   ELSE  NULL  END		AS  'Cancel Date',
						( SELECT  DEV_NME + ',' 
								FROM COWS.dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK) 
								WHERE rdi.REC_STUS_ID = 1 
								AND rdi.REDSGN_ID = r.REDSGN_ID FOR XML PATH('') ) AS 'Device Name',
						CASE rd.DEV_CMPLTN_CD WHEN 1 THEN 'Y'  ELSE 'N' END  AS 'Completion Flag',
						CASE WHEN ISNULL(r.CISC_SMRT_LIC_CD,0)=0 THEN 'No' ELSE 'Yes' END	AS  'Cisco Smart License required',
						ISNULL(r.SMRT_ACCNT_DMN,'')		AS  'Smart Account Domain',
						ISNULL(r.VRTL_ACCNT,'')		AS  'Virtual Account',
						rdH.EVENT_ID AS 'EVENT ID',
						r.REDSGN_ID,
						r.STUS_ID,
						r.EXT_XPIRN_FLG_CD
		INTO #TempRedesign
		FROM	COWS.dbo.REDSGN r WITH (NOLOCK)
			Inner Join	COWS.dbo.LK_USER lu with (nolock) on lu.USER_ID = r.CRETD_BY_CD
			Inner Join	COWS.dbo.LK_STUS ls with (nolock) on ls.STUS_ID = r.STUS_ID
			Inner Join	COWS.dbo.LK_REDSGN_TYPE lrt with (nolock) on lrt.REDSGN_TYPE_ID = r.REDSGN_TYPE_ID 
			Left Join	COWS.dbo.REDSGN_DEVICES_INFO rd with (nolock) on r.REDSGN_ID = rd.REDSGN_ID AND rd.REC_STUS_ID = 1
			LEFT JOIN	COWS.dbo.REDSGN_DEV_EVENT_HIST rdH WITH (NOLOCK) ON RDH.REDSGN_DEV_ID = RD.REDSGN_DEV_ID
			Left Join	COWS.dbo.LK_USER lut with (nolock) on r.NTE_ASSIGNED = lut.USER_ADID 
			Left join	COWS.dbo.LK_USER lup with (nolock) on r.PM_ASSIGNED = lup.USER_ADID
			left join   COWS.dbo.lk_user lus with (nolock) on r.SDE_ASN_NME = lus.user_adid 
			left join   COWS.dbo.lk_user lun with (nolock) on r.NE_ASN_NME = lun.user_adid 
			left join   COWS.dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=r.REDSGN_ID AND csd.SCRD_OBJ_TYPE_ID=16
		WHERE MONTH ( r.CRETD_DT ) = Month (getDate()) AND YEAR (r.CRETD_DT)= YEAR(getDate())

CLOSE SYMMETRIC KEY FS@K3y;
		
declare @ntetbl table(REDSGN_ID int, nte varchar(max), devcnt int, devcmplt int, expcnt int)
	
	insert into @ntetbl
	select distinct rd.redsgn_id,
	case when ((select count(1) from COWS.dbo.REDSGN_NOTES rn with (nolock) where rd.redsgn_id=rn.redsgn_id and rn.notes like 'Status changed to Approved%')>0) then 1 else 0 end as nte,
	(select count(1) from COWS.dbo.redsgn_devices_info rdi with (nolock) where rdi.redsgn_id=rd.redsgn_id and rdi.rec_stus_id=1) as devcnt,
	(select count(1) from COWS.dbo.redsgn_devices_info rdi with (nolock) where rdi.redsgn_id=rd.redsgn_id and rdi.rec_stus_id=1 and isnull(rdi.DEV_CMPLTN_CD,0)=1) as devcmplt,
	(SELECT count(1) FROM COWS.dbo.REDSGN_NOTES rn WITH (NOLOCK) WHERE rd.REDSGN_ID = rn.REDSGN_ID AND rn.NOTES = 'Status Update: Cancelled by System after 180 days') as expcnt
	from #TempRedesign rd with (nolock)
	
SELECT t.[Redesign Number],
	   t.[Submitted Date],
	   t.[SLA Due Date],
	   t.[Expiration Date],
	   t.[Redesign Type],
	   t.H1,
	   t.[Customer Name],
	   t.Status,
	   t.[NTE Assigned],
	   t.[PM Assigned],
	   t.[MSS SE Assigned],
	   t.[NE Assigned],
	   t.LOE,
	   t.MSS,
	   t.COST,
	   t.[Approval Date],
	   t.[Completion Date],
	   t.[Cancel Date],
	   t.[Device Name],
	   t.[Cisco Smart License required],
	   t.[Smart Account Domain],
	   t.[Virtual Account],
	   t.[Completion Flag],
	   CASE WHEN ((t.STUS_ID=226) AND ((nt.nte=0) AND (nt.expcnt=0))) THEN 'Y' ELSE 'N' END AS 'Cancelled before Approval',
	   CASE WHEN ((t.STUS_ID=226) AND ((nt.nte=1) AND ((nt.devcmplt=0) or (nt.devcnt=nt.devcmplt)) AND (nt.expcnt=0))) THEN 'Y' ELSE 'N' END AS 'Cancelled after Approval',
	   CASE WHEN ((t.STUS_ID=226) AND ((nt.nte=1) AND (nt.devcmplt>0) AND (nt.devcnt!=nt.devcmplt) AND (nt.expcnt=0))) THEN 'Y' ELSE 'N' END AS 'Cancelled before all sites implemented',
	   CASE WHEN (ISNULL(t.EXT_XPIRN_FLG_CD,0)=0) THEN 'N' ELSE 'Y' END AS 'Expiration date extended',
	   CASE WHEN ((t.STUS_ID IN (226,229)) AND ((nt.nte=0) AND (nt.expcnt>0))) THEN 'Y' ELSE 'N' END AS 'Expired before Approval',
	   CASE WHEN ((t.STUS_ID IN (226,229)) AND ((nt.nte=1) AND ((nt.devcmplt=0) or (nt.devcnt=nt.devcmplt)) AND (nt.expcnt>0))) THEN 'Y' ELSE 'N' END AS 'Expired after Approval',
	   CASE WHEN ((t.STUS_ID IN (226,229)) AND ((nt.nte=1) AND (nt.devcmplt>0) AND (nt.devcnt!=nt.devcmplt) AND (nt.expcnt>0))) THEN 'Y' ELSE 'N' END AS 'Expired before all sites implemented',
	   t.[EVENT ID]
FROM #TempRedesign t WITH (NOLOCK) inner join
@ntetbl nt on nt.REDSGN_ID=t.REDSGN_ID
	
	
	END TRY
	BEGIN CATCH
		DECLARE @Desc VARCHAR(200)
		SET @Desc='EXEC COWS_Reporting.dbo.RedesignMonthlyReport '+', ' 
		SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '
		EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;   --- Open later during deployment
		--SELECT * FROM	RAD_Reporting.[rad].[ReportProcessLog] Order by LogDateTime DESC
	END CATCH
END
