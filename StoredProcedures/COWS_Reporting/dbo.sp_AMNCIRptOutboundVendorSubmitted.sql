USE [COWS_Reporting]
GO  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- Updated By:   Md M Monir
-- Updated Date: 03/14/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD  
-- EXEC COWS_Reporting.dbo.sp_AMNCIRptOutboundVendorSubmitted 
-- [dbo].[sp_AMNCIRptOutboundVendorSubmitted]1 
-- =============================================  
ALTER PROCEDURE [dbo].[sp_AMNCIRptOutboundVendorSubmitted]  
 @secured  BIT = 1,  
 @Interval  char(1) = 'M'  
  
AS  
BEGIN TRY  
  
 SET NOCOUNT ON;  
--DECLARE @secured  BIT = 0  
  
   
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
  
DELETE FROM [COWS_Reporting].[dbo].[AMNCIOutboundVendorSubmittedData]  WHERE  
[COWS_Reporting].[dbo].[AMNCIOutboundVendorSubmittedData].[OE] in  
(SELECT [OE] FROM [COWS_Reporting].[dbo].[AMNCIOutboundVendorSubmittedData] as ovs   
INNER JOIN [COWS].[dbo].[FSA_ORDR] as ov on ovs.[OE] = ov.FTN  
LEFT OUTER JOIN [COWS].[dbo].[ORDR] as odr  
on odr.ORDR_ID = ov.ORDR_ID)   
  
INSERT INTO [COWS_Reporting].[dbo].[AMNCIOutboundVendorSubmittedData]   
  
  
 SELECT DISTINCT  
  
--e.ORDR_ID,  
--a.ORDR_ID,  
    CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),e.ORDR_ID,101)  
   WHEN i.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),t.FTN,101) END  [OE],   
     
--c.NUA_ADR,  
--b5.BIC_CD,  
--b6.BIC_CD,  
--b5.Line_ITEM_DES,  
--b6.LINE_ITEM_DES,  
--c.[CKT_OBJ_ID],  
  
  CONVERT(VARCHAR(10),e.CREAT_DT,101) [OE_Create_date],  
     
  o.FULL_NME [ISS PM],  
  '' [FE],  
     
  CASE  
	WHEN @secured = 0 AND a.CSG_LVL_ID = 0 AND m.CUST_NME<>''  THEN coalesce(m.CUST_NME,'PRIVATE CUSTOMER')
    WHEN @secured = 0 AND a.CSG_LVL_ID > 0 AND m.CUST_NME =''  THEN 'PRIVATE CUSTOMER'   
    WHEN @secured = 0 AND a.CSG_LVL_ID > 0 AND m.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER' 
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),m.CUST_NME,'') 
   END [Customer_name], --Secured  
     
  t.CREAT_DT [Order_received_date] ,  
    CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),w.CUST_WANT_DT,101)  
   WHEN i.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) END [CWD],  
  g.ORDR_TYPE_DES  [order_status],  
      
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN z.PROD_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 2 THEN u.PROD_TYPE_DES END [product type],   
     
  p.FULL_NME [Manager],  
    
  SUBSTRING(CONVERT(VARCHAR(10), e.CREAT_DT, 101),1,2) + '/01/' + CAST(YEAR(e.CREAT_DT) AS VARCHAR(4)) [Month],  
     
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),e.ORDR_ID,101)  
   WHEN i.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),t.FTN,101) END  [OE_Number],   
     
  v.VNDR_NME [Vendor],   
  b5.MRC_CHG_AMT [MRC_1],   
  b5.NRC_CHG_AMT [NRC_1],   
  b6.MRC_CHG_AMT [MRC_2],   
  b6.NRC_CHG_AMT [NRC_2],  
  b1.MRC_CHG_AMT [MRC_3],  
  b1.NRC_CHG_AMT [NRC_3],   
  b2.MRC_CHG_AMT [MRC_4],   
  b2.NRC_CHG_AMT [NRC_4],   
  b3.MRC_CHG_AMT [MRC_5],   
  b3.NRC_CHG_AMT [NRC_5],   
  b4.MRC_CHG_AMT [MRC_6],   
  b4.NRC_CHG_AMT [NRC_6],   
  b7.MRC_CHG_AMT [MRC_7],   
  b7.NRC_CHG_AMT [NRC_7],   
  b8.MRC_CHG_AMT [MRC_8],   
  b8.NRC_CHG_AMT [NRC_8],   
  b9.MRC_CHG_AMT [MRC_9],   
  b9.NRC_CHG_AMT [NRC_9],   
    
  ISNULL(CAST(b5.MRC_CHG_AMT AS MONEY), 0) + ISNULL(CAST(b6.MRC_CHG_AMT AS MONEY), 0)    
  + ISNULL(CAST(b1.MRC_CHG_AMT AS MONEY), 0)+ ISNULL(CAST(b2.MRC_CHG_AMT AS MONEY), 0)   
  + ISNULL(CAST(b3.MRC_CHG_AMT AS MONEY), 0) + ISNULL(CAST(b4.MRC_CHG_AMT AS MONEY), 0)   
    + ISNULL(CAST(b7.MRC_CHG_AMT AS MONEY), 0)    [MRC_Total],   
      
   ISNULL(CAST(b5.NRC_CHG_AMT AS MONEY), 0) +  ISNULL(CAST(b6.NRC_CHG_AMT AS MONEY), 0)    
   +  ISNULL(CAST(b1.NRC_CHG_AMT AS MONEY), 0) +  ISNULL(CAST(b2.NRC_CHG_AMT AS MONEY), 0)   
   + ISNULL(CAST(b3.NRC_CHG_AMT AS MONEY), 0)   +  ISNULL(CAST(b4.NRC_CHG_AMT AS MONEY), 0)  
    +  ISNULL(CAST(b7.NRC_CHG_AMT AS MONEY), 0)  [NRC_Total],  
  CASE    
   WHEN DATEPART(DW, e.CREAT_DT) = 1 THEN CONVERT(NVARCHAR(10), (e.CREAT_DT + 3),101)  
   WHEN DATEPART(DW, e.CREAT_DT) = 2 THEN CONVERT(NVARCHAR(10), (e.CREAT_DT + 2),101)  
   WHEN DATEPART(DW, e.CREAT_DT) = 3 THEN CONVERT(NVARCHAR(10), (e.CREAT_DT + 1),101)  
   WHEN DATEPART(DW, e.CREAT_DT) = 5 THEN CONVERT(NVARCHAR(10), (e.CREAT_DT - 1),101)  
   WHEN DATEPART(DW, e.CREAT_DT) = 6 THEN CONVERT(NVARCHAR(10), (e.CREAT_DT - 2),101)  
   WHEN DATEPART(DW, e.CREAT_DT) = 7 THEN CONVERT(NVARCHAR(10), (e.CREAT_DT - 3),101)  
   ELSE CONVERT(NVARCHAR(10), GETDATE(), 101) END  [Week]  
      
 FROM (Select ORDR_ID, VNDR_ORDR_ID, VNDR_FOLDR_ID, CREAT_DT from [COWS].[dbo].[VNDR_ORDR] aa where VNDR_ORDR_ID =   
 (SELECT MAX(VNDR_ORDR_ID) from [COWS].[dbo].[VNDR_ORDR] bb where aa.ORDR_ID = bb.ORDR_ID)) e   
 LEFT OUTER JOIN [COWS].[dbo].[ORDR] a WITH (NOLOCK) ON e.VNDR_ORDR_ID = a.ORDR_ID  
 LEFT OUTER JOIN [COWS].[dbo].[ORDR] i WITH (NOLOCK) ON e.ORDR_ID = i.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[IPL_ORDR] w WITH (NOLOCK) ON e.ORDR_ID = w.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_CAT] b WITH (NOLOCK) ON a.ORDR_CAT_ID = b.ORDR_CAT_ID   
   
 LEFT OUTER JOIN (Select * from [COWS].[dbo].[FSA_ORDR]aa where aa.ORDR_ACTN_ID = 2) t ON e.ORDR_ID = t.ORDR_ID   
   
 LEFT OUTER JOIN (Select bb.NUA_ADR, bb.CREAT_DT, aa.[FTN], aa.[NRM_CKT_ID] ,aa.SRVC_INSTC_NME  
      ,aa.[CKT_OBJ_ID], aa.[CKT_NME], aa.[PLN_NME], aa.[PLN_SEQ_NBR]  
 FROM [COWS].[dbo].[NRM_CKT]bb, [COWS].[dbo].[NRM_CKT] aa where aa.CREAT_DT = (Select MAX(Creat_dt) from   
  [COWS].[dbo].[NRM_CKT] ) and aa.NUA_ADR = bb.NUA_ADR ) c  ON t.FTN = c.FTN   
   
   
 LEFT OUTER JOIN [COWS].[dbo].[LK_PPRT] d WITH (NOLOCK) ON a.PPRT_ID = d.PPRT_ID   
 --LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] f WITH (NOLOCK) ON e.ORDR_ID = f.ORDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] g WITH (NOLOCK) ON t.ORDR_TYPE_CD = g.FSA_ORDR_TYPE_CD   
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] x WITH (NOLOCK) ON w.ORDR_TYPE_ID = x.ORDR_TYPE_ID   
 LEFT OUTER JOIN [COWS].[dbo].[ORDR_MS] h WITH (NOLOCK) ON e.ORDR_ID = h.ORDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] j WITH (NOLOCK) ON  e.VNDR_FOLDR_ID = j.VNDR_FOLDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] v WITH (NOLOCK) ON j.VNDR_CD = v.VNDR_CD    
 LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] k WITH (NOLOCK) ON e.ORDR_ID =  k.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON k.CTRY_CD = l.CTRY_CD   
 LEFT OUTER JOIN [COWS].[dbo].[H5_FOLDR] m WITH (NOLOCK) ON a.H5_FOLDR_ID =  m.H5_FOLDR_ID   
 --LEFT OUTER JOIN [COWS].[dbo].[USER_WFM_ASMT] n WITH (NOLOCK) ON  e.ORDR_ID = n.ORDR_ID   
 LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=m.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6  
 LEFT OUTER JOIN (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID   
   FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
   LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
   FROM (SELECT DISTINCT ASMT_DT, ORDR_ID FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
   WHERE usr.ORDR_ID = bb.ORDR_ID)  
    AND bb.ASN_USER_ID = (SELECT MAX(ASN_USER_ID)   
    FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
    FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
    WHERE uas.ORDR_ID = bb.ORDR_ID)) ur   
    on  e.ORDR_ID = ur.ORDR_ID   
      
 LEFT OUTER JOIN [COWS].[dbo].[LK_USER] o WITH (NOLOCK) ON ur.ASN_USER_ID = o.USER_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_USER] p WITH (NOLOCK) ON o.MGR_ADID = p.USER_ADID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] u WITH (NOLOCK) ON t.PROD_TYPE_CD = u.FSA_PROD_TYPE_CD   
 LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] z WITH (NOLOCK) ON w.PROD_TYPE_ID = z.PROD_TYPE_ID  
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.SOI_CD, CONVERT(VARCHAR, DecryptByKey(bb.CUST_NME)) [CUST_NME]  
  FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  
    
 LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
   FROM (SELECT SOI_CD, ORDR_ID   
    FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi  
    WHERE soi.ORDR_ID = bb.ORDR_ID)) si ON e.ORDR_ID = si.ORDR_ID  
        
 LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID   
  FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
   FROM (SELECT ACT_TASK_ID, ORDR_ID   
    FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
    WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk on tk.ORDR_ID = e.ORDR_ID   
      
 LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID   
  FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
   FROM (SELECT ACT_TASK_ID, ORDR_ID   
    FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
    WHERE tsk.ORDR_ID = bb.ORDR_ID)) t2 on t2.ORDR_ID = i.ORDR_ID   
      
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME)) [FRST_NME],   
  CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]   
  FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ROLE_ID = 13) gom ON gom.ORDR_ID = e.ORDR_ID   
    
 LEFT OUTER JOIN (SELECT bb.BIC_CD, bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT,   
 bb.NRC_CHG_AMT FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
    JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
    WHERE bb.BILL_ITEM_TYPE_ID = 1) b1 on e.ORDR_ID = b1.ORDR_ID   
   
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 2) b2 on e.ORDR_ID = b2.ORDR_ID  
  
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 3) b3 on e.ORDR_ID = b3.ORDR_ID  
   
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 4) b4 on e.ORDR_ID = b4.ORDR_ID  
  
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.BIC_CD, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 5 AND bb.LINE_ITEM_DES LIKE '%Access%') b5 on e.ORDR_ID = b5.ORDR_ID  
  
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.BIC_CD, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 5 AND bb.LINE_ITEM_DES LIKE '%PORT%') b6 on e.ORDR_ID = b6.ORDR_ID  
   
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 7) b7 on e.ORDR_ID = b7.ORDR_ID  
   
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 8) b8 on e.ORDR_ID = b8.ORDR_ID  
  
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 9) b9 on e.ORDR_ID = b9.ORDR_ID     
  
 WHERE b.ORDR_CAT_ID = 3 -- vendor category  
  and k.CIS_LVL_TYPE = 'H5' and k.CTRY_CD IS NOT NULL  
        AND a.RGN_ID = 1  
   
 CLOSE SYMMETRIC KEY FS@K3y   
If @Interval = 'M'   
  SELECT OE, CONVERT(VARCHAR(10), OE_Create_date, 101) [OE_Create_date], [ISS PM], FE, Customer_name,   
  CONVERT(VARCHAR(10), Order_received_date, 101)Order_received_date, CONVERT(VARCHAR(10), CWD, 101) [CWD],   
  order_status, [product type], Manager, CONVERT(VARCHAR(10), [Month], 101)[Month],   
  OE_Number, Vendor, MRC_1, NRC_1, MRC_2, NRC_2, MRC_3, NRC_3, MRC_4, NRC_4,   
  MRC_5, NRC_5, MRC_6, NRC_6, MRC_7, NRC_7, MRC_8, NRC_8, MRC_9, NRC_9,   
  CAST(MRC_Total AS MONEY) MRC_Total, CAST(NRC_Total AS MONEY) NRC_Total  
   from  [COWS_Reporting].[dbo].[AMNCIOutboundVendorSubmittedData]  
  where OE IS NOT NULL   
  ORDER by OE  
Else  
  SELECT OE, CONVERT(VARCHAR(10), OE_Create_date, 101) [OE_Create_date], [ISS PM], FE, Customer_name,   
  CONVERT(VARCHAR(10), Order_received_date, 101)Order_received_date, CONVERT(VARCHAR(10), CWD, 101) [CWD], order_status,   
  [product type], Manager, CONVERT(VARCHAR(10), [Week], 101)[Week], OE_Number, Vendor,   
  MRC_1, NRC_1, MRC_2, NRC_2, MRC_3, NRC_3, MRC_4, NRC_4, MRC_5, NRC_5, MRC_6,   
  NRC_6, MRC_7, NRC_7, MRC_8, NRC_8, MRC_9, NRC_9, CAST(MRC_Total AS MONEY) MRC_Total, CAST(NRC_Total AS MONEY) NRC_Total  
   from  [COWS_Reporting].[dbo].[AMNCIOutboundVendorSubmittedData]  
  where OE IS NOT NULL   
  ORDER by OE  
  
     
 RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptOutboundVendorSubmitted '  + CAST(@secured AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc + GETDATE()  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  
  
  
  