USE [COWS_Reporting]
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- Updated By:   Md M Monir
-- Updated Date: 03/14/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- EXEC COWS_Reporting.dbo.sp_AMNCIRptOutboundVendorCCD 0, 1, '9/1/2017','12/16/2018', 1  
-- =============================================  
ALTER PROCEDURE [dbo].[sp_AMNCIRptOutboundVendorCCD]  
  @QueryNumber  INT,  
     @secured   BIT = 0,  
  @inStartDtStr    VARCHAR(10)= '',  
     @inEndDtStr   VARCHAR(10)= '',  
  @missed    BIT = 0  
  
AS  
BEGIN TRY  
 SET NOCOUNT ON;  
 DECLARE @startDate  DATETIME  
 DECLARE @endDate  DATETIME  
 DECLARE @today   DATETIME  
 DECLARE @startDtStr  VARCHAR(10)   
 DECLARE @dayOfWeek  INT  
   
 IF @QueryNumber=0  
  BEGIN  
   SET @startDate=CONVERT(DATETIME, @inStartDtStr, 101)  
   SET @endDate=CONVERT(DATETIME, @inEndDtStr, 101)      
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @endDate=DATEADD(DAY, 1, @endDate)  
  END  
 ELSE IF @QueryNumber=1  
     BEGIN       
      -- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000  
   SET @startDate=CAST(CONVERT(VARCHAR(8), GETDATE(), 112) AS DATETIME)  
   SET @endDate=CAST(CONVERT(VARCHAR(8), DATEADD(DAY, 1, GETDATE()), 112) AS DATETIME)     
  END  
 ELSE IF @QueryNumber=2  
  BEGIN  
   -- e.g. Run date is '2011-06-21' 3rd day of the week  
   -- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000  
   SET @today=GETDATE()  
   SET @dayOfWeek=DATEPART(dw, @today)  
   SET @startDate=CAST(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) AS DATETIME)  
   SET @endDate=CAST(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS DATETIME)  
  END  
 ELSE IF @QueryNumber=3  
  BEGIN      
     SET @today=DATEADD(MONTH,-1,GETDATE())  
      -- e.g. Run date is the 1st day of each month   
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000  
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));       
   SET @startDate=CAST(@startDtStr AS DATETIME)  
   SET @endDate=CAST(CONVERT(VARCHAR(8), GETDATE(), 112) AS DATETIME)  
  END  
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
 CREATE TABLE #tempROV ([Order_Class] varchar(20),[OE/CK#] int,[Create_Date] varchar(16),[CUSTOMER] varchar(100),[_CCD] varchar(24),  
 [_CWD] varchar(24),[ACT/OPCD] varchar(24),[CD] varchar(24),[CCD Met] varchar(10),[CCD Met #] int,[CCD Met All] int,[_ORDER_TYPE] varchar(24),  
 [Product] varchar(24),[Service] varchar(24),[Vendor] varchar(50),[DEST_COUNTRY] varchar(50),[PL#]  varchar(20),[ISS PM] varchar(50),  
 [Manager] varchar(50),[FE] varchar(20),[Complete_Month] varchar(24),[Complete_Week] varchar(24),[Missed Reason] varchar(255),[Access MRC] float,  
 [Access NRC] float, [Port MRC] float,[Port NRC] float,[MRC_1] float,[NRC_1] float,[MRC_2] float,[NRC_2] float,[MRC_3] float,[NRC_3] float,[MRC_4] float,  
 [NRC_4] float,[Total MRC] float,[Total NRC] float,[Missed Reason1] varchar(255),[Interval] int)  
  
 INSERT INTO #tempROV ([Order_Class],[OE/CK#],[Create_Date],[CUSTOMER],[_CCD],[_CWD],[ACT/OPCD],[CD],[CCD Met],[CCD Met #],[CCD Met All]  
 ,[_ORDER_TYPE],[Product],[Service],[Vendor],[DEST_COUNTRY],[PL#],[ISS PM],[Manager],[FE],[Complete_Month],[Complete_Week],[Missed Reason],[Access MRC],  
 [Access NRC],[Port MRC],[Port NRC],[MRC_1],[NRC_1],[MRC_2],[NRC_2],[MRC_3],[NRC_3],[MRC_4],[NRC_4],[Total MRC],[Total NRC],[Missed Reason1],[Interval])  
  
--DECLARE @secured BIT=0 -- for testing purposes only  
  
 SELECT DISTINCT  
  b.ORDR_CAT_DES [Order_Class],  
  a.ORDR_ID [OE/CK#],   
  CONVERT(VARCHAR(10),e.CREAT_DT,101) [Create_Date],   
  CASE  
	WHEN @secured = 0 AND m.CUST_NME<>''  THEN coalesce(m.CUST_NME,'PRIVATE CUSTOMER')
    WHEN @secured = 0 AND m.CUST_NME =''  THEN 'PRIVATE CUSTOMER'  
    WHEN @secured = 0 AND m.CUST_NME IS NULL THEN 'PRIVATE CUSTOMER' 
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),m.CUST_NME,'')
   END [CUSTOMER], --Secured  
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101)  
   WHEN i.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101)  
   WHEN i.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101)  
   WHEN i.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101)  
   WHEN i.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) END [_CCD],  
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),w.CUST_WANT_DT,101)  
   WHEN i.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10),w.CUST_WANT_DT,101)  
   WHEN i.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10),w.CUST_WANT_DT,101)  
   WHEN i.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10),t.CUST_WANT_DT,101)  
   WHEN i.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) END [_CWD],  
  CASE  
   WHEN i.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),t2.CREAT_DT,101)  
   WHEN i.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10),t2.CREAT_DT,101)  
   WHEN i.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10),t2.CREAT_DT,101)  
   WHEN i.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10),tk.CREAT_DT,101)  
   WHEN i.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),tk.CREAT_DT,101) END [ACT/OPCD],    
  CONVERT(VARCHAR(24),GETDATE(),120) [CD],   
  CASE  
   WHEN i.ORDR_CAT_ID = 1 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 'CCD Met' --IPL  
   WHEN i.ORDR_CAT_ID = 1 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 'Missed'    
   WHEN i.ORDR_CAT_ID = 4 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 'CCD Met' --NCCO  
   WHEN i.ORDR_CAT_ID = 4 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 'Missed'   
   WHEN i.ORDR_CAT_ID = 5 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 'CCD Met' --DPL  
   WHEN i.ORDR_CAT_ID = 5 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 'Missed'   
   WHEN i.ORDR_CAT_ID = 6 AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 'CCD Met'  --Mach5  
   WHEN i.ORDR_CAT_ID = 6 AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 'Missed'  
   WHEN i.ORDR_CAT_ID = 2 AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 'CCD Met'  --FSA  
   WHEN i.ORDR_CAT_ID = 2 AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 'Missed' END [CCD Met],   
  CASE  
   WHEN i.ORDR_CAT_ID = 1 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 1   
   WHEN i.ORDR_CAT_ID = 1 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 0   
   WHEN i.ORDR_CAT_ID = 4 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 1   
   WHEN i.ORDR_CAT_ID = 4 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 0  
   WHEN i.ORDR_CAT_ID = 5 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 1   
   WHEN i.ORDR_CAT_ID = 5 AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 0  
   WHEN i.ORDR_CAT_ID = 6 AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 1   
   WHEN i.ORDR_CAT_ID = 6 AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 0  
   WHEN i.ORDR_CAT_ID = 2 AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 1   
   WHEN i.ORDR_CAT_ID = 2 AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 0 END [CCD Met #],   
  1 [CCD Met All],   
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN x.ORDR_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 4 THEN x.ORDR_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 5 THEN x.ORDR_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 6 THEN g.ORDR_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 2 THEN g.ORDR_TYPE_DES END [_ORDER_TYPE],   
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN z.PROD_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 4 THEN z.PROD_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 5 THEN z.PROD_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 6 THEN u.PROD_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 2 THEN u.PROD_TYPE_DES END [Product],   
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN w.SRVC_TYPE_CD  
   WHEN i.ORDR_CAT_ID = 4 THEN w.SRVC_TYPE_CD  
   WHEN i.ORDR_CAT_ID = 5 THEN w.SRVC_TYPE_CD  
   WHEN i.ORDR_CAT_ID = 6 THEN c.SRVC_INSTC_NME  
   WHEN i.ORDR_CAT_ID = 2 THEN c.SRVC_INSTC_NME END [Service],   
  v.VNDR_NME [Vendor],   
  l.CTRY_NME [DEST_COUNTRY],   
  c.PLN_NME [PL#],   
  o.FULL_NME [ISS PM],   
  p.FULL_NME [Manager],   
  'N/A' [FE],  
  SUBSTRING(CONVERT(VARCHAR(10), tk.CREAT_DT, 101),1,2) + '/01/' + CAST(YEAR(tk.CREAT_DT) AS VARCHAR(4)) [Complete_Month],   
  CASE    
   WHEN DATEPART(DW, tk.CREAT_DT) = 1 THEN CONVERT(NVARCHAR(10), (tk.CREAT_DT + 3),101)  
   WHEN DATEPART(DW, tk.CREAT_DT) = 2 THEN CONVERT(NVARCHAR(10), (tk.CREAT_DT + 2),101)  
   WHEN DATEPART(DW, tk.CREAT_DT) = 3 THEN CONVERT(NVARCHAR(10), (tk.CREAT_DT + 1),101)  
   WHEN DATEPART(DW, tk.CREAT_DT) = 5 THEN CONVERT(NVARCHAR(10), (tk.CREAT_DT - 1),101)  
   WHEN DATEPART(DW, tk.CREAT_DT) = 6 THEN CONVERT(NVARCHAR(10), (tk.CREAT_DT - 2),101)  
   WHEN DATEPART(DW, tk.CREAT_DT) = 7 THEN CONVERT(NVARCHAR(10), (tk.CREAT_DT - 3),101)  
   ELSE CONVERT(NVARCHAR(10), GETDATE(), 101) END  [Complete_Week],  
  --q.CCD_MISSD_REAS_DES [Missed Reason],   
  '' [Missed Reason],   
  ca.MRC_CHG_AMT [Access MRC],   
  ca.NRC_CHG_AMT [Access NRC],   
  cp.MRC_CHG_AMT [Port MRC],   
  cp.NRC_CHG_AMT [Port NRC],   
  '' [MRC_1],   
  '' [NRC_1],   
  '' [MRC_2],   
  '' [NRC_2],   
  '' [MRC_3],   
  '' [NRC_3],   
  '' [MRC_4],   
  '' [NRC_4],   
  CAST(ca.MRC_CHG_AMT AS MONEY) + CAST(cp.MRC_CHG_AMT AS MONEY) [Total MRC],   
  CAST(ca.NRC_CHG_AMT AS MONEY) + CAST(cp.NRC_CHG_AMT AS MONEY) [Total NRC],  
  '' [Missed Reason1],  
  CASE   
   WHEN i.ORDR_CAT_ID = 1 AND x.ORDR_TYPE_DES='Install' AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN   
    COWS_Reporting.dbo.getDaysInfo(2,CONVERT(VARCHAR(10),t2.CREAT_DT,101),CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101))  
   WHEN i.ORDR_CAT_ID = 4 AND x.ORDR_TYPE_DES='Install' AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN   
    COWS_Reporting.dbo.getDaysInfo(2,CONVERT(VARCHAR(10),t2.CREAT_DT,101),CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101))  
   WHEN i.ORDR_CAT_ID = 5 AND x.ORDR_TYPE_DES='Install' AND CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN   
    COWS_Reporting.dbo.getDaysInfo(2,CONVERT(VARCHAR(10),t2.CREAT_DT,101),CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101))  
   WHEN i.ORDR_CAT_ID = 6 AND g.ORDR_TYPE_DES='Install' AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN       
    COWS_Reporting.dbo.getDaysInfo(2,CONVERT(VARCHAR(10),tk.CREAT_DT,101),CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101))  
   WHEN i.ORDR_CAT_ID = 2 AND g.ORDR_TYPE_DES='Install' AND CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN         
    COWS_Reporting.dbo.getDaysInfo(2,CONVERT(VARCHAR(10),tk.CREAT_DT,101),CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101))  
    ELSE 0 END [Interval]  
 FROM [COWS].[dbo].[VNDR_ORDR] e WITH (NOLOCK)  
 LEFT OUTER JOIN [COWS].[dbo].[ORDR] a WITH (NOLOCK) ON e.VNDR_ORDR_ID = a.ORDR_ID  
 LEFT OUTER JOIN [COWS].[dbo].[ORDR] i WITH (NOLOCK) ON e.ORDR_ID = i.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[IPL_ORDR] w WITH (NOLOCK) ON e.ORDR_ID = w.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_CAT] b WITH (NOLOCK) ON a.ORDR_CAT_ID = b.ORDR_CAT_ID   
 LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] t WITH (NOLOCK) ON e.ORDR_ID = t.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] c WITH (NOLOCK) ON t.FTN = c.FTN   
 LEFT OUTER JOIN [COWS].[dbo].[LK_PPRT] d WITH (NOLOCK) ON a.PPRT_ID = d.PPRT_ID      
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] g WITH (NOLOCK) ON t.ORDR_TYPE_CD = g.FSA_ORDR_TYPE_CD   
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] x WITH (NOLOCK) ON w.ORDR_TYPE_ID = x.ORDR_TYPE_ID   
 LEFT OUTER JOIN [COWS].[dbo].[ORDR_MS] h WITH (NOLOCK) ON e.ORDR_ID = h.ORDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] j WITH (NOLOCK) ON  e.VNDR_FOLDR_ID = j.VNDR_FOLDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] v WITH (NOLOCK) ON j.VNDR_CD = v.VNDR_CD    
 LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] k WITH (NOLOCK) ON e.ORDR_ID =  k.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON k.CTRY_CD = l.CTRY_CD   
 LEFT OUTER JOIN [COWS].[dbo].[H5_FOLDR] m WITH (NOLOCK) ON a.H5_FOLDR_ID =  m.H5_FOLDR_ID 
 LEFT OUTER JOIN [COWS].[dbo].[CUST_SCRD_DATA] csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=m.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6    
 LEFT OUTER JOIN (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID   
   FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
   LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
   FROM (SELECT DISTINCT ASMT_DT, ORDR_ID FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
   WHERE usr.ORDR_ID = bb.ORDR_ID)  
    AND bb.ASN_USER_ID = (SELECT MAX(ASN_USER_ID)   
    FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
    FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
    WHERE uas.ORDR_ID = bb.ORDR_ID)) ur   
    on  e.ORDR_ID = ur.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_USER] o WITH (NOLOCK) ON ur.ASN_USER_ID = o.USER_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_USER] p WITH (NOLOCK) ON o.MGR_ADID = p.USER_ADID     
 LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] u WITH (NOLOCK) ON t.PROD_TYPE_CD = u.FSA_PROD_TYPE_CD   
 LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] z WITH (NOLOCK) ON w.PROD_TYPE_ID = z.PROD_TYPE_ID  
 LEFT OUTER JOIN (SELECT 
		bb.ORDR_ID, 
		bb.SOI_CD, 
		bb.CUST_NME as [CUST_NME],
		CONVERT(VARCHAR, DecryptByKey(csd.CUST_NME)) [CUST_NME2]  
  FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID 
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
  WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
   FROM (SELECT SOI_CD, ORDR_ID   
    FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi  
    WHERE soi.ORDR_ID = bb.ORDR_ID)) si ON e.ORDR_ID = si.ORDR_ID    
 LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID   
  FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
   FROM (SELECT ACT_TASK_ID, ORDR_ID   
    FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
    WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk on tk.ORDR_ID = e.ORDR_ID   
 LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID   
  FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
   FROM (SELECT ACT_TASK_ID, ORDR_ID   
    FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
    WHERE tsk.ORDR_ID = bb.ORDR_ID)) t2 on t2.ORDR_ID = i.ORDR_ID   
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID, 
					CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) [FRST_NME2], 
					CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [LST_NME2], 
					CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) + ' '+ CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [NME2],
					CONVERT(varchar, DecryptByKey(csd2.CUST_EMAIL_ADR)) [EMAIL_ADR2], 
					bb.FRST_NME  as [FRST_NME], 
					bb.LST_NME   as [LST_NME], 
					bb.FRST_NME  + ' '+	bb.LST_NME   as [NME],
					bb.EMAIL_ADR as [EMAIL_ADR] 
  FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd2.SCRD_OBJ_TYPE_ID=15
  WHERE bb.ROLE_ID = 13) gom ON gom.ORDR_ID = e.ORDR_ID   
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 LEFT OUTER JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID WHERE bb.BILL_ITEM_TYPE_ID = 6) cp on e.ORDR_ID = cp.ORDR_ID   
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 LEFT OUTER JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID WHERE bb.BILL_ITEM_TYPE_ID = 5) ca on e.ORDR_ID = ca.ORDR_ID  
   
     
 WHERE b.ORDR_CAT_ID = 3 -- vendor category  
  AND (tk.TASK_ID = 1001 OR t2.TASK_ID = 1001)  --Completed FSA and IPL orders  
  AND k.ADR_TYPE_ID IN (6,12,18)  --Site Adress   
  AND (x.ORDR_TYPE_DES IN ('Disconnect','Install') OR g.ORDR_TYPE_DES IN ('Disconnect','Install'))  
  AND e.CREAT_DT >= @startDate AND e.CREAT_DT < @endDate  
  
--AND a.RGN_ID = 1  
  
 INSERT INTO [COWS_Reporting].[dbo].[AMNCIOutboundVendorCCD]  
 SELECT * FROM #tempROV t1   
 WHERE NOT EXISTS (SELECT * FROM [COWS_Reporting].[dbo].[AMNCIOutboundVendorCCD] t2  
  WHERE t1.[Order_Class]=t2.[Order_Class] AND t1.[OE/CK#]=t2.[OE/CK#] AND t1.[Create_Date]=t2.[Create_Date] AND t1.[CUSTOMER]=t2.[CUSTOMER]  
  AND t1.[_CCD]=t2.[_CCD] AND t1.[_CWD]=t2.[_CWD] AND t1.[ACT/OPCD]=t2.[ACT/OPCD] AND t1.[CD]=t2.[CD] AND t1.[CCD Met]=t2.[CCD Met]   
  AND t1.[CCD Met #]=t2.[CCD Met #] AND t1.[CCD Met All]=t2.[CCD Met All] AND t1.[_ORDER_TYPE]=t2.[_ORDER_TYPE] AND t1.[Product]=t2.[Product]   
  AND t1.[Service]=t2.[Service] AND t1.[Vendor]=t2.[Vendor] AND t1.[DEST_COUNTRY]=t2.[DEST_COUNTRY] AND t1.[PL#]=t2.[PL#] AND t1.[ISS PM]=t2.[ISS PM]   
  AND t1.[Manager]=t2.[Manager] AND t1.[FE]=t2.[FE] AND t1.[Complete_Month]=t2.[Complete_Month] AND t1.[Complete_Week]=t2.[Complete_Week]   
  AND t1.[Missed Reason]=t2.[Missed Reason] AND t1.[Access MRC]=t2.[Access MRC] AND t1.[Access NRC]=t2.[Access NRC] AND t1.[Port MRC]=t2.[Port MRC]  
  AND t1.[Port NRC]=t2.[Port NRC] AND t1.[MRC_1]=t2.[MRC_1] AND t1.[NRC_1]=t2.[NRC_1] AND t1.[MRC_2]=t2.[MRC_2] AND t1.[NRC_2]=t2.[NRC_2]   
  AND t1.[MRC_3]=t2.[MRC_3] AND t1.[NRC_3]=t2.[NRC_3] AND t1.[MRC_4]=t2.[MRC_4] AND t1.[NRC_4]=t2.[NRC_4] AND t1.[Total MRC]=t2.[Total MRC]   
  AND t1.[Total NRC]=t2.[Total NRC] AND t1.[Missed Reason1]=t2.[Missed Reason1] AND t1.[Interval]=t2.[Interval])  
  
 IF @missed <> 0  
  BEGIN   
   SELECT [Order_Class],[OE/CK#],[Create_Date],[CUSTOMER],[_CCD],[_CWD],[ACT/OPCD],[CD],[CCD Met],[CCD Met #],[CCD Met All],[_ORDER_TYPE],  
    [Product],[Service],[Vendor],[DEST_COUNTRY],[PL#],[ISS PM],[Manager],[FE],[Complete_Month],[Complete_Week],[Missed Reason],[Interval]   
   FROM #tempROV  
   WHERE [CCD Met] = 'Missed' AND [_ORDER_TYPE]='Install'  
  END  
 ELSE  
  BEGIN  
   SELECT [Order_Class],[OE/CK#],[Create_Date],[CUSTOMER],[_CCD],[_CWD],[ACT/OPCD],[CD],[CCD Met],[CCD Met #],[CCD Met All],[_ORDER_TYPE],  
    [Product],[Service],[Vendor],[DEST_COUNTRY],[PL#],[ISS PM],[Manager],[FE],[Complete_Month],[Complete_Week],[Missed Reason],[Access MRC],  
    [Access NRC],[Port MRC],[Port NRC],[MRC_1],[NRC_1],[MRC_2],[NRC_2],[MRC_3],[NRC_3],[MRC_4],[NRC_4],[Total MRC],[Total NRC],[Missed Reason1]   
   FROM #tempROV  
  END  
   
 DROP TABLE #tempROV  
        
 CLOSE SYMMETRIC KEY FS@K3y   
        
 RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptOutboundVendorCCD '  + CAST(@QueryNumber AS VARCHAR(4)) + ',' + CAST(@secured AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  
  