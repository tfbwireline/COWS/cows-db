USE [COWS_Reporting]
GO
/*
--============================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			01/26/2012
-- Description:		
--					Extract MPLS events that have been activated for specified parameter(s); 
--					Event Complete Date Range.
--
-- Notes:
-- 
-- 02/20/2012		sxv0766: Removed: Description and Comments, IP Version, Assigned To,
--							  		  Workflow Status, Activator Comments
--							 Added: Action
-- 7/25 2012         axm3320: Added Action id 19- 'In progress' orders
-- 8/24/2012         axm3320: EventItemTitle and Customer Name are encrypted information adding fix to decrypt the values				
--                            Add 2 new parameters   @SecuredUser ,   @getSensData               
--axm3320:3/19/2013 Replace action_id 15(reschedule) with 44 (reschedule-email notfn sent)
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- [dbo].[sp_CANDRptMplsOD] null, null, 1, 'N'
--============================================================================================
*/
ALTER PROCEDURE [dbo].[sp_CANDRptMplsOD]
	@startDate			Datetime=NULL,
	@endDate			Datetime=NULL,
	@SecuredUser		INT=0,	
	@getSensData		CHAR(1)='N'
AS
BEGIN TRY

	SET NOCOUNT ON;
	
			
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
	
	SELECT mpls.EVENT_ID 'Event ID', 
			UPPER(evst.EVENT_STUS_DES) 'Event Status', 
									
			CASE 
			    /*
				WHEN @SecuredUser = 0 AND evnt.SCURD_CD  = 1 AND CONVERT(varchar, DecryptByKey(mpls.EVENT_TITLE_TXT)) <> '' THEN 'Private Customer'
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(mpls.EVENT_TITLE_TXT)) <> '' THEN 'Private Customer'
				ELSE CONVERT(varchar, DecryptByKey(mpls.EVENT_TITLE_TXT)) */
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NOT NULL THEN 'Private Customer'
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NULL THEN mpls.EVENT_TITLE_TXT
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  = 0  THEN mpls.EVENT_TITLE_TXT
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NOT NULL THEN 'Private Customer'
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NULL THEN mpls.EVENT_TITLE_TXT
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  = 0 THEN mpls.EVENT_TITLE_TXT
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) ,mpls.EVENT_TITLE_TXT,'')
				END 'Item Title',		
														
			--CASE WHEN mpls.EVENT_TITLE_TXT is NULL THEN '' ELSE mpls.EVENT_TITLE_TXT END 'Item Title',	
			
			lkmt.MPLS_EVENT_TYPE_DES 'MPLS Event Type',				
			mpact.MPLS_ACTY_TYPE 'MPLS Activity Type',
			lkvp.VPN_PLTFRM_TYPE_DES 'VPN Platform Type',
			CASE WHEN lkvt.VAS_TYPE_DES is NULL THEN '' ELSE lkvt.VAS_TYPE_DES END 'VAS Type',
			REPLACE(SUBSTRING(CONVERT(varchar, mpls.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mpls.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mpls.STRT_TMST, 109), 25, 2) 'Start Time',
			REPLACE(SUBSTRING(CONVERT(varchar, mpls.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mpls.END_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mpls.END_TMST, 109), 25, 2) 'End Time',					
			mpls.EVENT_DRTN_IN_MIN_QTY 'Event Duration',
			mpls.EXTRA_DRTN_TME_AMT 'Extra Duration',										
			
				CASE  /*
				WHEN @SecuredUser = 0 AND evnt.SCURD_CD  = 1 AND CONVERT(varchar, DecryptByKey(mpls.CUST_NME)) <> '' THEN 'Private Customer'
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(mpls.CUST_NME)) <> '' THEN 'Private Customer'
				ELSE CONVERT(varchar, DecryptByKey(mpls.CUST_NME)) */
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NOT NULL THEN 'Private Customer'
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NULL THEN mpls.CUST_NME
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  = 0  THEN mpls.CUST_NME
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NOT NULL THEN 'Private Customer'
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NULL THEN mpls.CUST_NME
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  = 0 THEN mpls.CUST_NME
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)) ,mpls.CUST_NME,'')
				END 'Customer Name',		
								
			--CASE WHEN mpls.CUST_NME is NULL THEN '' ELSE mpls.CUST_NME END 'Customer Name',
			
			CASE
				WHEN lkac.ACTN_DES = 'Completed Email Notification' THEN 'Completed' 
				ELSE lkac.ACTN_DES END 'Action',			
			REPLACE(SUBSTRING(CONVERT(varchar, mpev.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mpev.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mpev.CREAT_DT, 109), 25, 2) 'Modified Date',			
			CASE
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END 'Modified By',
			CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '' ELSE scev.SUCSS_ACTY_DES END 'Success Activities',		
			CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '' ELSE flev.FAIL_ACTY_DES END 'Failed Activities'
		FROM COWS.dbo.MPLS_EVENT mpls with (nolock) 
		   left join COWS.dbo.EVENT evnt with(nolock) on evnt.EVENT_ID = mpls.EVENT_ID 	
		   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mpls.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=9	
			JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mpls.EVENT_STUS_ID = evst.EVENT_STUS_ID
			JOIN COWS.dbo.LK_MPLS_EVENT_TYPE lkmt with (nolock) ON mpls.MPLS_EVENT_TYPE_ID = lkmt.MPLS_EVENT_TYPE_ID
			JOIN COWS.dbo.LK_VPN_PLTFRM_TYPE lkvp with (nolock) ON mpls.VPN_PLTFRM_TYPE_ID = lkvp.VPN_PLTFRM_TYPE_ID
			LEFT JOIN COWS.dbo.MPLS_EVENT_VAS_TYPE mvst with (nolock) ON mpls.EVENT_ID = mvst.EVENT_ID
			LEFT JOIN COWS.dbo.LK_VAS_TYPE lkvt with (nolock) ON mvst.VAS_TYPE_ID = lkvt.VAS_TYPE_ID	
			JOIN
			(
				SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT
					FROM COWS.dbo.EVENT_HIST eh with (nolock)
					--JOIN COWS.dbo.MPLS_EVENT_ACTY_TYPE meat with (nolock) ON eh.EVENT_ID = meat.EVENT_ID
					JOIN COWS.dbo.MPLS_EVENT mple with (nolock) ON eh.EVENT_ID = mple.EVENT_ID
					WHERE eh.ACTN_ID in (44,19) AND mple.EVENT_STUS_ID in (3, 6) AND mple.STRT_TMST >= @startDate and mple.STRT_TMST < @endDate
				UNION
				SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT
					FROM COWS.dbo.EVENT_HIST ehis with (nolock)
					JOIN
					(
						SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) 'maxEvHis'
							FROM COWS.dbo.EVENT_HIST eh with (nolock)
							--JOIN COWS.dbo.MPLS_EVENT_ACTY_TYPE meat with (nolock) ON eh.EVENT_ID = meat.EVENT_ID
							JOIN COWS.dbo.MPLS_EVENT mple with (nolock) ON eh.EVENT_ID = mple.EVENT_ID
							WHERE eh.ACTN_ID in (18) AND mple.EVENT_STUS_ID = 6 AND mple.STRT_TMST >= @startDate and mple.STRT_TMST < @endDate
						GROUP BY eh.EVENT_ID
					)ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis
			)mpev ON mpls.EVENT_ID = mpev.EVENT_ID						
			LEFT JOIN
			(
				SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lksu.SUCSS_ACTY_DES  + ', ' AS [text()] 
	 						FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)
	 						JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID 
	 						WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID --AND evsu.REC_STUS_ID = 1
	 						FOR xml PATH ('')
							) as SUCSS_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)	
					)evSucAct
		  		)evSucAct2
			)scev ON mpev.EVENT_HIST_ID = scev.EVENT_HIST_ID
			LEFT JOIN
			(
				SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lkfl.FAIL_ACTY_DES  + ', ' AS [text()] 
	 						FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)
	 						JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID 
	 						WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID --AND evfl.REC_STUS_ID = 1
	 						FOR xml PATH ('')
							) as FAIL_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)	
					)evFailAct
		  		)evFailAct2
			)flev ON mpev.EVENT_HIST_ID = flev.EVENT_HIST_ID			
			LEFT JOIN
			(
				SELECT distinct LEFT([MPLS_ACTY_TYPE],len([MPLS_ACTY_TYPE]) -1) as MPLS_ACTY_TYPE, EVENT_ID
				FROM
				(
					SELECT (SELECT lkma.MPLS_ACTY_TYPE_DES + ', ' AS [text()] 
							FROM COWS.dbo.MPLS_EVENT_ACTY_TYPE meac with (nolock)
							JOIN COWS.dbo.LK_MPLS_ACTY_TYPE lkma with (nolock) ON meac.MPLS_ACTY_TYPE_ID = lkma.MPLS_ACTY_TYPE_ID
							WHERE meac.EVENT_ID = mple.EVENT_ID 
							FOR xml PATH ('')
						   ) as MPLS_ACTY_TYPE, EVENT_ID 
					FROM 
					( SELECT EVENT_ID FROM COWS.dbo.MPLS_EVENT with (nolock)	
						WHERE EVENT_STUS_ID in (3, 6) AND STRT_TMST >= @startDate AND STRT_TMST < @endDate
					)
					mple
				)mpaty 
			)mpact ON mpls.EVENT_ID = mpact.EVENT_ID			
			JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mpev.CREAT_BY_USER_ID = luser1.USER_ID 
			JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON mpev.ACTN_ID = lkac.ACTN_ID
								
		--WHERE mpls.EVENT_STUS_ID in (3, 6) AND mpls.STRT_TMST >= @startDate and mpls.STRT_TMST < @endDate
		
		ORDER BY mpls.EVENT_ID	
	
	CLOSE SYMMETRIC KEY FS@K3y;
	
	RETURN 0;
  
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_CANDRptMplsOD '  	
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH
