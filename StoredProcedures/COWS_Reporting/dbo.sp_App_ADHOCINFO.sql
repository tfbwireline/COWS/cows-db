/*
-- Notes: 
--05/25/2012 axm3320: Fix for IM1069656 get FTN from FSA_Ordr table for FSA orders
--                             get orderid as CTN from ordr table for IPL orders   
----                    Fix for IM1070232 : H1 Customer number pulled from FSA_ORDR_CUST.CUST_ID for FSA ORDERS instead of ODIE_REQ table

--axm3320: 5/31/2012 ACK_BY_VNDR_DT and SENT_TO_VNDR_DT moved from ORDR_MS to VNDR_ORDR_MS Table
--axm3320: 5/31/2012 GOM ASMT DT,GOM ASSIGN User pull from ORDR_NTE instead of USR_WFM table
--axm3320: 6/6/2012 ORDR_SBMT_DT (GOM) If product type is transport then pull from ordr_ms..SBMT_DT else FSA_ORDR..ORDR_SBMT_DT
--axm3320 : 6/11/2012 GOM ASSIGN User pulled from USER_WFM table as per changes implemented by CAPT
--axm3320: 7/18/2012 GOM Assign Date pulled from FSA_ORDR PRE_SBMT_CREAT_DT date
--axm3320: 7/18/2012 GOM Order Submit date pulled from ordr_nte table where nte_txt = 'Completing GOM Submit Task.'
--axm3320: 7/27/2012 Comment below code
--left outer join ( select ford.ORDR_ID,ford.ORDR_SBMT_DT ,fogom.CPE_VNDR_NME,usr.FULL_NME,ford.PRE_SBMT_CREAT_DT [ASMT_DT] from [COWS].dbo.FSA_ORDR ford with (nolock) 
--join (select * from [COWS].[dbo].FSA_ORDR_GOM_XNCI gx with (nolock) where gx.GRP_ID in (1))as fogom on fogom.ORDR_ID = ford.ORDR_ID 
--left join (select * from [COWS].[dbo].USER_WFM_ASMT uw with (nolock) where uw.GRP_ID in (1)) as uwa on uwa.ordr_ID = ford.ordr_id
--left join [COWS].[dbo].LK_USER usr with (nolock) on usr.USER_ID = uwa.ASN_USER_ID ) fsagom on fsagom.ORDR_ID = a.ORDR_ID 
--Updated code for GOM fields
--axm3320:8/16/2012 Updated code for pulling RTS information; commented below code
--left outer join (SELECT act.* FROM [COWS].[dbo].[ACT_TASK] act with (nolock) 
--		WHERE act.ACT_TASK_ID   = (SELECT MAX(ACT_TASK_ID)
--		FROM (SELECT ACT_TASK_ID,ORDR_ID FROM [COWS].[dbo].[ACT_TASK] with (nolock) where STUS_ID in (0,3)) act1 
--		WHERE act1.ORDR_ID = act.ORDR_ID)) actask on actask.ORDR_ID = ordr.ORDR_ID
--RTS Rules: Action Type:PreSubmit Orders:need to look at this note - GOM user initiated RTS on this order. For Submit Task id of 500 and status of 0
--axm3320: 10/3/2012 Alter sp for PJ006439 to pull non sensitive Fedline Event Data
--ci554013: 10/24/2012 Alter SP for pulling Presubmit Date from FSA_ORDR table instead of ORDR_MS
--dlp0278: 11/15/12 Changed H4 cust_id to pull from FSA_ORDR_CUST table
-- ci554013 11/28/2012 Added a new field Duration 
-- ci554013 2/27/2013 changed BusinessDayHourMinInterval to BusinessDayHourMinInterval66 for 66 hour weekend in calculation.
-- ci554013 3/28/2013 changed ACCS_IN_MB_VALU_QTY to ACCS_MB_DES.
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
*/

ALTER proc [dbo].[sp_App_ADHOCINFO]
@SQLSelect varchar(max) = '',
@SQLWhere varchar(max) = '',
--@COWS int = 1,
@nrm int = 0,
@adevent int = 0,
@mplsevent int = 0,
@splkevent int =0,
@fedlineevent int = 0 
 
as
begin
Declare @CTEADHOCTBL1 as varchar(max)
Declare @CTEADHOCTBL2 as varchar(max)
Declare @CTEADHOCTBL3 as varchar(max)
Declare @CTEADHOCTBL4 as varchar(max)

select @CTEADHOCTBL1 = ' With COWS_FSA_DATA as (select distinct ordr.ORDR_ID, CASE ORDR.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS SCURD_CD ,ordr.H5_H6_CUST_ID ,ordr.ORDR_CAT_ID, ordr.RGN_ID,
CASE 
   when ordr.DMSTC_CD = 0 THEN ''Y''
   WHEN ordr.DMSTC_CD = 1 THEN ''N'' END  [DMSTC_CD],
d.CKT_TYPE_CD,
h4.CUST_ID [H4_CUST_ID],
CASE 
	WHEN ordr.ORDR_CAT_ID=1 THEN d.CLCM_NME
	WHEN ordr.ORDR_CAT_ID=6 THEN
		CASE			
		   WHEN Sur.CLCM_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
		   WHEN Sur.CLCM_NME IS NULL THEN NULL END
	WHEN ordr.ORDR_CAT_ID=2 THEN
		CASE			
		   WHEN Sur.CLCM_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
		   WHEN Sur.CLCM_NME IS NULL THEN NULL END			
END [CLCM_NME], 
CASE 
	WHEN ordr.ORDR_CAT_ID=1 THEN d.SOI
	WHEN ordr.ORDR_CAT_ID=6 THEN i.SOI_CD
	WHEN ordr.ORDR_CAT_ID=2 THEN i.SOI_CD END [SOI],
CASE 
	WHEN ordr.ORDR_CAT_ID=1 THEN d.CUST_CNTRC_SIGN_DT
	WHEN ordr.ORDR_CAT_ID=6 THEN a.CUST_SIGNED_DT
	WHEN ordr.ORDR_CAT_ID=2 THEN a.CUST_SIGNED_DT END [CUST_CNTRC_SIGN_DT],
CASE 
	WHEN ordr.ORDR_CAT_ID = 1 THEN Convert(varchar(50),ordr.ORDR_ID)
	WHEN ordr.ORDR_CAT_ID = 6 THEN a.FTN
	WHEN ordr.ORDR_CAT_ID = 2 THEN a.FTN END [FTN],
a.RELTD_FTN,
a.TTRPT_JACK_NRFC_TYPE_CD,
CASE 
	WHEN ordr.ORDR_CAT_ID=1 THEN ordrtyp.ORDR_TYPE_DES
	WHEN ordr.ORDR_CAT_ID=6 THEN b.FSA_ORDR_TYPE_DES
	WHEN ordr.ORDR_CAT_ID=2 THEN b.FSA_ORDR_TYPE_DES END [FSA_ORDR_TYPE_DES],
osubTyp.ORDR_SUB_TYPE_DES, -- new field ORDR SUB TYP 
a.FSA_ORGNL_INSTL_ORDR_ID, -- new field OriginalInstallOrderID 
CASE 
	WHEN ordr.ORDR_CAT_ID=1 THEN ProdTyp.PROD_TYPE_DES
	WHEN ordr.ORDR_CAT_ID=6 THEN c.FSA_PROD_TYPE_DES
	WHEN ordr.ORDR_CAT_ID=2 THEN c.FSA_PROD_TYPE_DES END [FSA_PROD_TYPE_DES],
a.FSA_EXP_TYPE_CD,
a.TSUP_RFQ_NBR,
CASE 
	WHEN ordr.ORDR_CAT_ID=1 THEN d.CUST_WANT_DT
	WHEN ordr.ORDR_CAT_ID=6 THEN a.CUST_WANT_DT
	WHEN ordr.ORDR_CAT_ID=2 THEN a.CUST_WANT_DT END [CUST_WANT_DT],
CASE 
	WHEN ordr.ORDR_CAT_ID=1 THEN d.CUST_CMMT_DT
	WHEN ordr.ORDR_CAT_ID=6 THEN a.CUST_CMMT_DT
	WHEN ordr.ORDR_CAT_ID=2 THEN a.CUST_CMMT_DT END [CUST_CMMT_DT],
a.TTRPT_ACCS_CNTRC_TERM_CD,
a.TPORT_CNCTR_TYPE_ID,
a.TSUP_PRS_QOT_NBR,
a.CPE_ACCS_PRVDR_CD,
a.TTRPT_NCDE_DES , --Encoding
a.TTRPT_FRMNG_DES [TTRPT_FRMNG_DES], --Framing
a.TTRPT_ACCS_TYPE_DES, -- Access Type Description
bl.MRC_CHG_AMT,
bl.NRC_CHG_AMT,
(cast(bl.MRC_CHG_AMT as money) + cktCost.ACCS_MRC_USD) as TOTAL_CUST_BILL,
os.ORDR_STUS_DES,
i.SALS_PERSN_PRIM_CID,
CASE			
WHEN i.CUST_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN i.CUST_NME IS NULL THEN NULL END [CUST_NME] ,
a.TTRPT_SPD_OF_SRVC_BDWD_DES,
t.CMNT_TXT,
u.FULL_NME ,
m.CCD_MISSD_REAS_DES,
t.ORDR_ASN_VW_DT  ,
t.CUST_ACPTD_DT ,
o.ORDR_BILL_CLEAR_INSTL_DT,
t.CKT_LGTH_IN_KMS_QTY,
t.ACCS_MB_DES,
t.A_END_NEW_CKT_CD,
t.A_END_SPCL_CUST_ACCS_DETL_TXT,
xncirgn.RGN_DES,
t.B_END_NEW_CKT_CD,
t.B_END_SPCL_CUST_ACCS_DETL_TXT, 
t.IPT_AVLBLTY_ID,            
cu.CUR_NME,
t.VNDR_LOCAL_ACCS_PRVDR_GRP_NME,
vt.VNDR_TYPE_DES,
s.SALS_CHNL_DES,
--ORDR_MS Related Fields
a.PRE_SBMT_CREAT_DT as PRE_SBMT_DT,
voms.ACK_BY_VNDR_DT,
voms.SENT_TO_VNDR_DT,
o.VLDTD_DT,
--Commented as field missing
oh.HOLD_DT as ORDR_HOLD_DT,
--CKT_MS fields
cm.ACCS_ACPTC_DT,
cm.ACCS_DLVRY_DT,
cm.CNTRC_TERM_ID,
cm.TRGT_DLVRY_DT,
cm.TRGT_DLVRY_DT_RECV_DT,
cm.VNDR_CNFRM_DSCNCT_DT,
--ORDR_ADR fields
CASE			
	WHEN ad.BLDG_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
	WHEN ad.BLDG_NME IS NULL THEN NULL END [BLDG_NME] ,
CASE			
	WHEN ad.CTRY_CD IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
	WHEN ad.CTRY_CD IS NULL THEN NULL END [CTRY_CD] ,
CASE			
WHEN ad.CTY_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN ad.CTY_NME IS NULL THEN NULL END [CTY_NME] ,
CASE			
WHEN ad.FLR_ID IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN ad.FLR_ID IS NULL THEN NULL END [FLR_ID] ,

CASE			
WHEN ad.PRVN_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN ad.PRVN_NME IS NULL THEN NULL END [PRVN_NME] ,
CASE			
WHEN ad.RM_NBR IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN ad.RM_NBR IS NULL THEN NULL END [RM_NBR] ,
CASE			
WHEN ad.STREET_ADR_1 IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN ad.STREET_ADR_1 IS NULL THEN NULL END [STREET_ADR_1] ,
CASE			
WHEN ad.STT_CD IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN ad.STT_CD IS NULL THEN NULL END [STT_CD] ,
CASE			
WHEN ad.ZIP_PSTL_CD IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN ad.ZIP_PSTL_CD IS NULL THEN NULL END [ZIP_PSTL_CD] ,
CASE			
WHEN adh.H5_STT_CD  IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN adh.H5_STT_CD IS NULL THEN NULL END [H5_STT_CD] ,
CASE			
WHEN ad.CTRY_NME  IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN ad.CTRY_NME IS NULL THEN NULL END [H5_CTRY_CD] ,
CASE			
WHEN oc.PHN_NBR IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN oc.PHN_NBR IS NULL THEN NULL END [CNTCT_PHN_NBR] ,
CASE			
WHEN oc.FRST_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN oc.FRST_NME IS NULL THEN NULL END [CNTCT_FRST_NME] ,
CASE			
WHEN oc.LST_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN oc.LST_NME IS NULL THEN NULL END [CNTCT_LST_NME] ,
CASE			
WHEN oc.EMAIL_ADR IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN oc.EMAIL_ADR IS NULL THEN NULL END [CNTCT_EMAIL_ADR] ,

CASE			
WHEN oc.CNTCT_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN oc.CNTCT_NME IS NULL THEN NULL END [CNTCT_NME],
CASE			
WHEN osoc.PHN_NBR IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN osoc.PHN_NBR IS NULL THEN NULL END [OnSite_PHN_NBR],  
CASE			
WHEN osoc.EMAIL_ADR IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN osoc.EMAIL_ADR IS NULL THEN NULL END [OnSite_CNTCT_EMAIL_ADR] ,
CASE			
WHEN osoc.CNTCT_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN osoc.CNTCT_NME IS NULL THEN NULL END [OnSite_CNTCT_NME],
CASE			
WHEN aloc.PHN_NBR IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN aloc.PHN_NBR IS NULL THEN NULL END [AL_PHN_NBR],  
CASE			
WHEN aloc.EMAIL_ADR IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN aloc.EMAIL_ADR IS NULL THEN NULL END [AL_EMAIL_ADR] ,
CASE			
WHEN aloc.CNTCT_NME IS NOT NULL THEN ''PRIVATE CUSTOMER'' 
WHEN aloc.CNTCT_NME IS NULL THEN NULL END [AL_CNTCT_NME],'

select @CTEADHOCTBL2 =  ' f.VNDR_CD ,
e.VNDR_ORDR_ID ,
sp.NEW_PORT_SPEED_DES, 
cktCost.VNDR_MRC_USD,
cktCost.VNDR_NRC_USD,
cktCost.ACCS_MRC_USD,
cktCost.ACCS_NRC_USD,
cktCost.ASR_MRC_USD,
cktCost.ASR_NRC_USD,
(cktCost.VNDR_MRC_USD + cktCost.VNDR_NRC_USD + cktCost.ACCS_MRC_USD + cktCost.ACCS_NRC_USD + cktCost.ASR_MRC_USD + cktCost.ASR_NRC_USD) as COSTTYPE,
cktc.QOT_NBR,
cktc.VNDR_CUR_ID, 
ckt.VNDR_CNTRC_TERM_END_DT ,
ckt.VNDR_CKT_ID,
ckt.PL_SEQ_NBR, --New Field Private Line (populated by GOM) 
CASE 
	--WHEN ordr.ORDR_CAT_ID=1 THEN h.CUST_ID
	WHEN ordr.ORDR_CAT_ID=6 THEN h.CUST_ID
	WHEN ordr.ORDR_CAT_ID=2 THEN h.CUST_ID END [H1_CUST_ID],
vl.VLAN_ID,
vl.VLAN_PCT_QTY,
a.SPA_ACCS_INDCR_DES,
a.TPORT_BRSTBL_USAGE_TYPE_CD,
a.TPORT_ETHRNT_NRFC_INDCR,
a.TPORT_CUST_ROUTR_TAG_TXT,
a.TPORT_CUST_ROUTR_AUTO_NEGOT_CD,
acs.ACCS_CTY_NME_SITE_CD,
	--fsagom.ASMT_DT,
	CASE  when fsagom.uASMT_DT IS NOT NULL THEN fsagom.uASMT_DT
           WHEN fsagom.ASMT_DT IS NOT NULL THEN fsagom.ASMT_DT 
		   ELSE o.PRE_SBMT_DT END [ASMT_DT],
fsagom.CPE_VNDR_NME [VNDR_NME],
CASE   WHEN fsagomasmt.NOTE_DT IS NOT NULL THEN fsagomasmt.NOTE_DT
       WHEN a.PROD_TYPE_CD IN (''DN'',''DO'',''MP'',''MO'',''SN'',''SO'') AND o.SBMT_DT IS NOT NULL THEN o.SBMT_DT
       ELSE fsagom.ORDR_SBMT_DT END  [ORDR_SBMT_DT],
--fsagom.ORDR_SBMT_DT,
	--fsagom.ORDR_SBMT_DT,
	fsagom.FULL_NME [GOMASMTUSR],
	gomstus.GOM_ORDR_STUS [GOM_ORDR_STUS],
CASE 
 	WHEN a.PRE_SBMT_CREAT_DT IS NOT NULL AND fsagomasmt.NOTE_DT IS NOT NULL 
 		THEN Reporting.dbo.BusinessDayHourMinInterval66(CONVERT(VARCHAR(16),a.PRE_SBMT_CREAT_DT,121),CONVERT(VARCHAR(16),fsagomasmt.NOTE_DT,121))  
	WHEN a.PROD_TYPE_CD IN (''DN'',''DO'',''MP'',''MO'',''SN'',''SO'') AND o.PRE_SBMT_DT IS NOT NULL AND o.SBMT_DT IS NOT NULL 
		THEN Reporting.dbo.BusinessDayHourMinInterval66(CONVERT(VARCHAR(16),o.PRE_SBMT_DT,121),CONVERT(VARCHAR(16),o.SBMT_DT,121)) 
	WHEN a.PRE_SBMT_CREAT_DT IS NOT NULL AND a.ORDR_SBMT_DT IS NOT NULL 
		THEN Reporting.dbo.BusinessDayHourMinInterval66(CONVERT(VARCHAR(16),a.PRE_SBMT_CREAT_DT,121),CONVERT(VARCHAR(16),a.ORDR_SBMT_DT,121)) 
 	ELSE '''' END  [Duration],
CASE WHEN ORDR_ACTN_ID = 1 THEN CASE WHEN PS_RTS.ORDR_ID IS NULL THEN ''N'' ELSE ''Y'' END
     WHEN ORDR_ACTN_ID = 2 THEN CASE WHEN S_RTS.ORDR_ID IS NULL THEN ''N'' ELSE ''Y'' END END [RTS_Flag],
CASE WHEN ORDR_ACTN_ID = 1 THEN PS_RTS_Dt
     WHEN ORDR_ACTN_ID = 2 THEN S_RTS_DT END [RTS_Date],
rts.RTS_Notes [RTS_Notes],
CASE When ch.new_ccd_dt IS NULL THEN
CASE 
	 WHEN ordr.ORDR_CAT_ID = 1 THEN d.CUST_CMMT_DT
	 WHEN ordr.ORDR_CAT_ID = 6 THEN a.CUST_CMMT_DT
	 WHEN ordr.ORDR_CAT_ID = 2 THEN a.CUST_CMMT_DT END 
When ch.new_ccd_dt IS NOT NULL THEN ch.new_ccd_dt END [new_ccd_dt]
from COWS.dbo.ORDR ordr with (nolock) 
left outer join (SELECT bb.* , 
fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES,
fcpl.SPA_ACCS_INDCR_DES,
fcpl.TPORT_ETHRNT_NRFC_INDCR,
fcpl.TPORT_CUST_ROUTR_TAG_TXT,
fcpl.TPORT_CUST_ROUTR_AUTO_NEGOT_CD
        FROM [COWS].[dbo].[FSA_ORDR] bb with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = bb.ORDR_ID
		WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
		FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
		WHERE fsa.FTN = bb.FTN)) a on a.ORDR_ID = ordr.ORDR_ID
left outer join COWS.dbo.LK_VNDR lv with(nolock) on a.INSTL_VNDR_CD  = lv.VNDR_CD 
left outer join COWS.dbo.LK_XNCI_RGN xncirgn with(nolock) on ordr.RGN_ID = xncirgn.RGN_ID
left outer join COWS.dbo.LK_ORDR_STUS os with(nolock) on os.ORDR_STUS_ID = ordr.ORDR_STUS_ID
left outer join COWS.dbo.LK_FSA_ORDR_TYPE b with(nolock) on b.FSA_ORDR_TYPE_CD = a.ORDR_TYPE_CD
left outer join COWS.dbo.LK_ORDR_SUB_TYPE osubTyp with(nolock) on osubTyp.ORDR_SUB_TYPE_CD = a.ORDR_SUB_TYPE_CD 
left outer join COWS.dbo.LK_FSA_PROD_TYPE c with(nolock) on c.FSA_PROD_TYPE_CD = a.PROD_TYPE_CD
left outer join ( SELECT ORDR_ID,MRC [MRC_CHG_AMT] ,NRC [NRC_CHG_AMT] FROM (SELECT a.ORDR_ID,SUM(y.MRC)MRC,SUM(y.NRC)NRC
					FROM COWS.dbo.FSA_ORDR a WITH (NOLOCK) JOIN (SELECT ORDR_ID,SUM(MRC)MRC,SUM(NRC)NRC
					FROM (SELECT ORDR_ID,ISNULL(CAST(replace(MRC_CHG_AMT,''null'',0) AS MONEY),0) MRC,ISNULL(CAST(replace(NRC_CHG_AMT,''null'',0) AS MONEY),0) NRC
						FROM COWS.dbo.FSA_ORDR_BILL_LINE_ITEM WITH (NOLOCK) ) x GROUP BY ORDR_ID 
				) y ON y.ORDR_ID = a.ORDR_ID
				GROUP BY a.ORDR_ID) z ) bl on bl.ORDR_ID = ordr.ORDR_ID 
			left outer join (SELECT bb.ORDR_ID, bb.SOI_CD, CONVERT(varchar, DecryptByKey(csd.CUST_NME)) [CUST_NME], bb.BR_CD,bb.SALS_PERSN_PRIM_CID
 			   FROM COWS.dbo.FSA_ORDR_CUST bb WITH (NOLOCK)
 			   LEFT OUTER JOIN COWS.dbo.FSA_ORDR cc WITH (NOLOCK) ON  bb.ORDR_ID = cc.ORDR_ID 
 			   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCURD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCURD_OBJ_TYPE_ID=5
				WHERE (CIS_LVL_TYPE = ''H5'' OR CIS_LVL_TYPE = ''H6'') AND bb.SOI_CD = (SELECT MAX(SOI_CD)
					FROM (SELECT SOI_CD, ORDR_ID 
						FROM COWS.dbo.[FSA_ORDR_CUST] WITH (NOLOCK)) soi
				WHERE soi.ORDR_ID = bb.ORDR_ID AND (CIS_LVL_TYPE = ''H5'' OR CIS_LVL_TYPE = ''H6''))) i on i.ORDR_ID = ordr.ORDR_ID 
 left outer join (SELECT se.NEW_PORT_SPEED_DES,sl.FTN FROM [COWS].[dbo].[SPLK_EVENT_ACCS] se with (nolock)
        join [COWS].[dbo].[SPLK_EVENT] sl with (nolock) on sl.EVENT_ID = se.EVENT_ID where sl.EVENT_STUS_ID not in (8) ) sp 
		on sp.FTN = a.FTN '
		
		select @CTEADHOCTBL3 = 'left outer join COWS.dbo.IPL_ORDR d with (nolock) on d.ORDR_ID = ordr.ORDR_ID 
left outer join COWS.dbo.LK_ORDR_TYPE OrdrTyp with(nolock) on OrdrTyp.ORDR_TYPE_ID =  d.ORDR_TYPE_ID 
left outer join COWS.dbo.LK_PROD_TYPE ProdTyp with(nolock) on ProdTyp.PROD_TYPE_ID = d.PROD_TYPE_ID  
 left outer join COWS.dbo.TRPT_ORDR t with(nolock) on t.ORDR_ID = ordr.ORDR_ID 
 left outer join COWS.dbo.LK_USER u with (nolock) on u.USER_ID = t.XNCI_ASN_MGR_ID 
 left outer join COWS.dbo.LK_CCD_MISSD_REAS m with (nolock) on m.CCD_MISSD_REAS_ID  = t.CCD_MISSD_REAS_ID 
 left outer join COWS.dbo.LK_CUR cu with (nolock) on cu.CUR_ID = t.CUST_BILL_CUR_ID 
 left outer join COWS.dbo.LK_VNDR_TYPE vt with (nolock) on vt.VNDR_TYPE_ID  = t.VNDR_TYPE_ID 
 left outer join COWS.dbo.LK_SALS_CHNL s with (nolock) on s.SALS_CHNL_ID = t.SALS_CHNL_ID 
 left outer join COWS.dbo.LK_ACCS_CTY_SITE acs with (nolock) on acs.ACCS_CTY_SITE_ID  = t.ACCS_CTY_SITE_ID 
 left outer join (SELECT oo.* FROM [COWS].[dbo].[ORDR_MS] oo with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[ORDR] cc on oo.ORDR_ID = cc.ORDR_ID  
		WHERE oo.VER_ID = (SELECT MAX(VER_ID)
		FROM (SELECT VER_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_MS] with (nolock) ) oms 
		WHERE oms.ORDR_ID = oo.ORDR_ID)) o on o.ORDR_ID = ordr.ORDR_ID
	left outer join (SELECT ohm.* FROM [COWS].[dbo].[ORDR_HOLD_MS] ohm with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[ORDR] cc on ohm.ORDR_ID = cc.ORDR_ID  
		WHERE ohm.VER_ID = (SELECT MAX(VER_ID)
		FROM (SELECT VER_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_HOLD_MS] with (nolock) ) oms 
		WHERE oms.ORDR_ID = ohm.ORDR_ID)) oh on oh.ORDR_ID = ordr.ORDR_ID
	 left outer join (SELECT ck.* FROM [COWS].[dbo].[CKT] ck with (nolock) 
		WHERE ck.CKT_ID   = (SELECT MAX(CKT_ID)
		FROM (SELECT CKT_ID,ORDR_ID FROM [COWS].[dbo].[CKT] with (nolock) ) ock 
		WHERE ock.ORDR_ID = ck.ORDR_ID)) ckt on ckt.ORDR_ID = ordr.ORDR_ID
  left outer join (SELECT ckm.* FROM [COWS].[dbo].[CKT_MS] ckm with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[CKT] cc on ckm.CKT_ID = cc.CKT_ID  
		WHERE ckm.VER_ID  = (SELECT MAX(VER_ID)
		FROM (SELECT VER_ID,CKT_ID FROM [COWS].[dbo].[CKT_MS] with (nolock) ) ock 
		WHERE ock.CKT_ID = ckm.CKT_ID)) cm on cm.CKT_ID  = ckt.CKT_ID 
left outer join (SELECT ckto.QOT_NBR,ckto.CKT_ID,ckto.VNDR_CUR_ID  FROM [COWS].[dbo].[CKT_COST] ckto with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[CKT] cc on ckto.CKT_ID = cc.CKT_ID  
		WHERE ckto.VER_ID  = (SELECT MAX(VER_ID)
		FROM (SELECT VER_ID,CKT_ID FROM [COWS].[dbo].[CKT_COST] with (nolock) ) ock 
		WHERE ock.CKT_ID = ckto.CKT_ID)) cktc on cktc.CKT_ID  = ckt.CKT_ID 
LEFT OUTER JOIN (SELECT bb.[ORDR_ID], SUM(CAST( replace(VNDR_MRC_IN_USD_AMT,''null'',0) AS MONEY)) as VNDR_MRC_USD ,SUM(CAST (replace(VNDR_NRC_IN_USD_AMT,''null'',0) AS MONEY)) AS VNDR_NRC_USD, 
                  SUM(CAST( replace(VNDR_MRC_AMT,''null'',0) AS MONEY)) as VNDR_MRC, SUM(CAST( replace(VNDR_NRC_AMT,''null'',0) AS MONEY)) as VNDR_NRC,
                  SUM(CAST ( replace(aa.ACCS_CUST_MRC_IN_USD_AMT,''null'',0)  AS MONEY)) ACCS_MRC_USD, SUM(CAST ( replace(aa.ACCS_CUST_NRC_IN_USD_AMT,''null'',0) AS MONEY)) ACCS_NRC_USD,
                  SUM(CAST ( replace(aa.ACCS_CUST_MRC_AMT,''null'',0) AS MONEY)) ACCS_MRC, SUM(CAST (replace(aa.ACCS_CUST_NRC_AMT,''null'',0) AS MONEY)) ACCS_NRC,
                  SUM(CAST (replace(aa.ASR_MRC_IN_USD_AMT,''null'',0) AS MONEY))ASR_MRC_USD, SUM(CAST (replace(aa.ASR_NRC_IN_USD_AMT,''null'',0) AS MONEY))ASR_NRC_USD,
                  SUM(CAST (replace(aa.ASR_MRC_ATY,''null'',0) AS MONEY))ASR_MRC, SUM(CAST (replace(aa.ASR_NRC_AMT,''null'',0) AS MONEY))ASR_NRC
                  FROM [COWS].[dbo].[CKT_COST] aa JOIN COWS.dbo.CKT bb on aa.CKT_ID = bb.CKT_ID
                  JOIN [COWS].[dbo].[ORDR]cc  on bb.ORDR_ID = cc.ORDR_ID
                  GROUP by bb.ORDR_ID) cktCost on ordr.ORDR_ID = cktCost.ORDR_ID			
left outer join (SELECT adr.*,lc.CTRY_NME  FROM [COWS].[dbo].[ORDR_ADR] adr with (nolock)
      left outer join [COWS].[dbo].[LK_CTRY] lc with (nolock) on adr.CTRY_CD = lc.CTRY_CD 
 where adr.CIS_LVL_TYPE = ''H5'' and adr.REC_STUS_ID =1 )ad on ad.ORDR_ID = ordr.ORDR_ID
left outer join (SELECT adr1.ORDR_ID, adr1.STT_CD [H5_STT_CD] FROM [COWS].[dbo].[ORDR_ADR] adr1 with (nolock) where adr1.CIS_LVL_TYPE in (''H6'') and adr1.REC_STUS_ID =1 )adh on adh.ORDR_ID = ordr.ORDR_ID
left outer join (SELECT oct.CNTCT_NME,oct.PHN_NBR,oct.EMAIL_ADR,oct.FRST_NME,oct.LST_NME,oct.ORDR_ID  FROM [COWS].[dbo].[ORDR_CNTCT] oct with (nolock) JOIN
		[COWS].[dbo].[ORDR] cc on oct.ORDR_ID = cc.ORDR_ID  
		WHERE oct.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)
		FROM (SELECT ORDR_CNTCT_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_CNTCT] ct with (nolock) where ct.CNTCT_TYPE_ID = 1 and ct.CIS_LVL_TYPE = ''H5'' and ct.REC_STUS_ID = 1 ) omc 
		WHERE omc.ORDR_ID = oct.ORDR_ID)) oc on oc.ORDR_ID = ordr.ORDR_ID '
		
select @CTEADHOCTBL4 =  ' left outer join (SELECT oct.CNTCT_NME,oct.PHN_NBR,oct.EMAIL_ADR,oct.FRST_NME,oct.LST_NME,oct.ORDR_ID  FROM [COWS].[dbo].[ORDR_CNTCT] oct with (nolock) JOIN
		[COWS].[dbo].[ORDR] cc with (nolock) on oct.ORDR_ID = cc.ORDR_ID  
		WHERE oct.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)
		FROM (SELECT ORDR_CNTCT_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_CNTCT] ct with (nolock) where ct.CNTCT_TYPE_ID = 4 and ct.CIS_LVL_TYPE = ''H5'' and ct.REC_STUS_ID = 1 ) omc 
		WHERE omc.ORDR_ID = oct.ORDR_ID)) osoc on osoc.ORDR_ID = ordr.ORDR_ID 
left outer join (SELECT oct.CNTCT_NME,oct.PHN_NBR,oct.EMAIL_ADR,oct.FRST_NME,oct.LST_NME,oct.ORDR_ID  FROM [COWS].[dbo].[ORDR_CNTCT] oct with (nolock) JOIN
		[COWS].[dbo].[ORDR] cc with (nolock) on oct.ORDR_ID = cc.ORDR_ID  
		WHERE oct.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)
		FROM (SELECT ORDR_CNTCT_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_CNTCT] ct with (nolock) where ct.ROLE_ID = 7 and ct.REC_STUS_ID = 1) omc 
		WHERE omc.ORDR_ID = oct.ORDR_ID)) aloc on aloc.ORDR_ID = ordr.ORDR_ID 
left outer join (select vo.* from [COWS].[dbo].[VNDR_ORDR] vo with (nolock) join 
                 [COWS].[dbo].[ORDR] cc on vo.ORDR_ID = cc.ORDR_ID  
                 where vo.VNDR_ORDR_ID =(select MAX(VNDR_ORDR_ID) from (SELECT VNDR_ORDR_ID,ORDR_ID FROM [COWS].[dbo].[VNDR_ORDR] vo1 with (nolock) where vo1.TRMTG_CD =0 ) vo2 
		WHERE vo2.ORDR_ID = vo.ORDR_ID  )) e on e.ORDR_ID = ordr.ORDR_ID
left outer join COWS.dbo.VNDR_FOLDR f with(nolock) on f.VNDR_FOLDR_ID = e.VNDR_FOLDR_ID 
left outer join (select vom.* from [COWS].[dbo].[VNDR_ORDR_MS] vom with (nolock) join 
                 COWS.dbo.VNDR_ORDR v on vom.VNDR_ORDR_ID = v.VNDR_ORDR_ID   
                 where vom.VNDR_ORDR_MS_ID  =(select MAX(VNDR_ORDR_MS_ID) from (SELECT VNDR_ORDR_ID,VNDR_ORDR_MS_ID FROM [COWS].[dbo].[VNDR_ORDR_MS] vo1 with (nolock)) vo2 
		WHERE vo2.VNDR_ORDR_ID  = vom.VNDR_ORDR_ID )) voms on voms.VNDR_ORDR_ID = e.VNDR_ORDR_ID 
left outer join (select ORDR_ID,CUST_ID  from COWS.dbo.fsa_ordr_cust where CIS_LVL_TYPE = ''H1'') h on h.ORDR_ID = ordr.ORDR_ID
left outer join (select ORDR_ID,CUST_ID  from COWS.dbo.fsa_ordr_cust where CIS_LVL_TYPE = ''H4'') h4 on h4.ORDR_ID = ordr.ORDR_ID
left outer join (SELECT vln.* FROM [COWS].[dbo].[ORDR_VLAN] vln with (nolock) JOIN
		[COWS].[dbo].[ORDR] cc on vln.ORDR_ID = cc.ORDR_ID  
		WHERE vln.ORDR_VLAN_ID = (SELECT MAX(ORDR_VLAN_ID)
		FROM (SELECT ORDR_VLAN_ID,ORDR_ID,VLAN_SRC_NME FROM [COWS].[dbo].[ORDR_VLAN] with (nolock) ) vla 
		WHERE vla.ORDR_ID = vln.ORDR_ID and vla.VLAN_SRC_NME = ''FSA'')) vl on vl.ORDR_ID = ordr.ORDR_ID
left outer join ( select fsa.ordr_id,fsa.ORDR_SBMT_DT, case when lv.VNDR_NME  IS NULL OR lv.VNDR_NME = '''' then case when fogom.CPE_VNDR_NME IS NULL OR fogom.CPE_VNDR_NME = '''' then ''''
 else fogom.CPE_VNDR_NME end else lv.VNDR_NME end  [CPE_VNDR_NME],usr.FULL_NME,fsa.PRE_SBMT_CREAT_DT [ASMT_DT] ,uwa.ASMT_DT [uASMT_DT] from COWS.dbo.FSA_ORDR fsa with(nolock)
left outer join (select * from [COWS].[dbo].FSA_ORDR_GOM_XNCI gx with (nolock) where gx.GRP_ID in (1))fogom on fsa.ORDR_ID = fogom.ORDR_ID 
left join (select * from [COWS].[dbo].USER_WFM_ASMT uw with (nolock) where uw.GRP_ID in (1)) as uwa on uwa.ordr_ID = fsa.ordr_id
left join [COWS].[dbo].LK_USER usr with (nolock) on usr.USER_ID = uwa.ASN_USER_ID left join [COWS].[dbo].LK_VNDR lv with(nolock) on lv.VNDR_CD = fsa.INSTL_VNDR_CD 
 where fsa.ORDR_ID in (SELECT [ORDR_ID] FROM [COWS].[dbo].[ACT_TASK] with (nolock) where TASK_ID in (select TASK_ID from COWS.dbo.LK_TASK with(nolock) where TASK_NME like ''%GOM%''))) fsagom on fsagom.ORDR_ID = ordr.ORDR_ID 
left outer join (SELECT ORDR_ID, MIN(CREAT_DT) [NOTE_DT] FROM COWS.dbo.ORDR_NTE WITH (NOLOCK) WHERE NTE_TXT = ''Completing GOM Submit Task.'' GROUP BY ORDR_ID ) fsagomasmt on fsagomasmt.ORDR_ID = fsagom.ORDR_ID 
left outer join(select t.ACT_TASK_ID,t.TASK_ID,a.ORDR_ID,O.ORDR_STUS_ID,a.PROD_TYPE_CD,O.DMSTC_CD,os.ORDR_STUS_DES,
		  Case  when O.ORDR_STUS_ID =1 then --Pending
			CASE When a.PROD_TYPE_CD = ''CP'' and O.DMSTC_CD = 1 Then --CPE/International orders
				CASE when t.TASK_ID = 102 then ''GOM Milestones Pending''  
				else ts.TASK_NME + '' '' + os.ORDR_STUS_DES   END
			ELSE  ts.TASK_NME + '' '' + os.ORDR_STUS_DES   END  
		  else  os.ORDR_STUS_DES  --Completed 
		  End [GOM_ORDR_STUS] from [COWS].[dbo].ACT_TASK t  with (nolock)  JOIN [COWS].[dbo].FSA_ORDR a with (nolock) on a.ORDR_ID = t.ORDR_ID 
		  JOIN [COWS].[dbo].ORDR O with (nolock) on O.ORDR_ID = t.ORDR_ID join [COWS].[dbo].LK_TASK ts with(nolock) on ts.TASK_ID = t.TASK_ID 
		  join [COWS].[dbo].LK_ORDR_STUS os with (nolock) on os.ORDR_STUS_ID = O.ORDR_STUS_ID where t.ACT_TASK_ID = (select MAX(ACT_TASK_ID) from (select ACT_TASK_ID,ORDR_ID from [COWS].[dbo].ACT_TASK ac ) act1 where act1.ORDR_ID = t.ORDR_ID  ) 
   )gomstus on gomstus.ORDR_ID = fsagom.ORDR_ID
left outer join (SELECT oct.CNTCT_NME [CLCM_NME],oct.ORDR_ID  FROM [COWS].[dbo].[ORDR_CNTCT] oct with (nolock) JOIN
		[COWS].[dbo].[ORDR] cc with (nolock) on oct.ORDR_ID = cc.ORDR_ID  
		WHERE oct.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)
		FROM (SELECT ORDR_CNTCT_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_CNTCT] ct with (nolock) where
				 ct.CNTCT_TYPE_ID = 1 and ct.ROLE_ID = 13 and ct.REC_STUS_ID = 1 ) omc 
		WHERE omc.ORDR_ID = oct.ORDR_ID)) Sur on Sur.ORDR_ID = ordr.ORDR_ID
left outer join (SELECT ORDR_ID,MAX(CREAT_DT)[PS_RTS_Dt] FROM COWS.dbo.ORDR_NTE WHERE NTE_TXT = ''GOM user initiated RTS on this order.'' GROUP BY ORDR_ID) PS_RTS on PS_RTS.ORDR_ID = fsagom.ORDR_ID		
left outer join (SELECT act.ORDR_ID,act.CREAT_DT [S_RTS_DT] FROM [COWS].[dbo].[ACT_TASK] act with (nolock) 
		WHERE act.ACT_TASK_ID   = (SELECT MAX(ACT_TASK_ID)
		FROM (SELECT ACT_TASK_ID,ORDR_ID FROM [COWS].[dbo].[ACT_TASK] with (nolock) where TASK_ID = 500 and STUS_ID =0) act1 
		WHERE act1.ORDR_ID = act.ORDR_ID))S_RTS on S_RTS.ORDR_ID = fsagom.ORDR_ID		
 LEFT OUTER JOIN (SELECT NTE_ID,NTE_TYPE_ID,ORDR_ID,NTE_TXT [RTS_Notes] FROM [COWS].[dbo].[ORDR_NTE] nte where NTE_ID =
  (select MAX(NTE_ID) from (select NTE_ID,ORDR_ID from [COWS].[dbo].[ORDR_NTE] where NTE_TYPE_ID in (14,21)) nte1 where nte1.ORDR_ID  = nte.ORDR_ID) ) rts ON rts.ORDR_ID = fsagom.ORDR_ID 
 Left outer join (select ch.ordr_id,new_ccd_dt,ccd_hist_id from [COWS].[dbo].ccd_hist ch with(nolock) join 
  	[COWS].[dbo].[ORDR] cc with (nolock) on ch.ORDR_ID = cc.ORDR_ID  
  	where ch.CCD_HIST_ID = (select MAX(ccd_hist_id) from (select ORDR_ID,CCD_HIST_ID from [COWS].[dbo].ccd_hist with(nolock) ) ch1 where ch1.ordr_id = ch.ORDR_ID ))
  	ch on ch.ORDR_ID = ordr.ORDR_ID )'

Declare @SelectStmt as varchar(max)


IF @fedlineevent = 0
Begin

			select @SelectStmt =  '  from ' + ' COWS_FSA_DATA a '


			if @nrm  = 1
			begin
			select @SelectStmt = @SelectStmt + '  left outer join v_ADHOC_NRM_Data nrm with(nolock) on a.FTN = nrm.FTN '
			end

			if @adevent  = 1
			begin
			select @SelectStmt = @SelectStmt + '  full join v_ADHOC_ADEvent_Data adev with(nolock) on a.FTN = adev.FTN '
			end

			if @mplsevent  = 1
			begin
			select @SelectStmt = @SelectStmt + '  full join v_ADHOC_MPLSEvent_Data mpls with(nolock) on a.FTN = mpls.FTN '
			end

			if @splkevent  = 1
			begin
			select @SelectStmt = @SelectStmt + '  full join v_ADHOC_SPLKEvent_Data splk with(nolock) on a.FTN = splk.FTN '
			end

			set @SelectStmt = @SelectStmt + ' ' +  @SQLWhere

			--exec (@SQLSelect + @SelectStmt)

			exec (@CTEADHOCTBL1 + @CTEADHOCTBL2 + @CTEADHOCTBL3 + @CTEADHOCTBL4 + @SQLSelect + @SelectStmt)
END
  Else 
  BEGIN
     select @SelectStmt =  '  from ' + ' v_ADHOC_FedLineEvent_Data ' + ' ' +  @SQLWhere
     
     exec ( @SQLSelect + @SelectStmt)
  END

--print (@CTEADHOCTBL1)
--print (@CTEADHOCTBL2)
--print (@CTEADHOCTBL3)
--print (@CTEADHOCTBL4)
--print (@SQLSelect)
--print( @SelectStmt)


end
