USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptEventsPerCustomerOD]    Script Date: 11/21/2019 9:15:10 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
--======================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			07/19/2011
-- Description:		
--					Extract one or many MDS event(s) excluding 'Disconnect' per customer
--					for specified parameter(s); 
--					Event Status, Customer Name, H5, H6, Date Range.  
--
-- Notes:			Records with MDS_ACTY_TYPE of 'Initial Install' and 'MAC' ONLY 
--
-- 09/22/2011		sxv0766: Added @h5_h6_ID, @getSensData
--
-- 11/09/2011		sxv0766: Use value of FSA_MDS_EVENT.TAB_NME as H5 value
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--======================================================================================
*/
ALTER PROCEDURE [dbo].[sp_MNSRptEventsPerCustomerOD]
	@SecuredUser		INT=0,	
	@getSensData		CHAR(1)='N',
	@startDate			Datetime=NULL,
	@endDate			Datetime=NULL
AS
BEGIN TRY

	SET NOCOUNT ON;
		
	DECLARE @startDateStr	VARCHAR(20)
	DECLARE @endDateStr		VARCHAR(20)	
	
	SET @startDate=CONVERT(DATETIME, @startDate, 101)  
    SET @endDate=CONVERT(DATETIME, @endDate, 101)      
    -- Add 1 day to End Date in order to cover the End Date 24-hr period  
    SET @endDate=DATEADD(DAY, 1, @endDate)
	
	DECLARE  @tmp_mns_evtCust AS TABLE
	(
		Event					VARCHAR(20),
		Event_ID				INT,
		Cust_Name				VARCHAR(100),
		Cust_Name2			VARCHAR(100),
		Scurd_Cd				Bit,
		YearMonth				VARCHAR(20),
		MDS_Inst_Acty			VARCHAR(100),	
		MDS_Acty_Type			VARCHAR(25),
		MDS_Mac_Acty			VARCHAR(100),		
		Svc_Tier				VARCHAR(100),
		Event_Status			VARCHAR(50),
		Assigned_MNS_PM			VARCHAR(100),
		Reviewed_By				VARCHAR(100),
		SC_Flag					CHAR(10)
	)
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
		INSERT INTO @tmp_mns_evtCust(Event, Event_ID, Cust_Name, Scurd_Cd, YearMonth, MDS_Inst_Acty,
				MDS_Acty_Type, MDS_Mac_Acty, Svc_Tier, Event_Status, Assigned_MNS_PM, Reviewed_By, SC_Flag)
		SELECT CASE mds.MDS_FAST_TRK_CD
			WHEN 1 THEN 'MDS Fast Track'
			WHEN 0 THEN 'MDS' END ,
		mds.EVENT_ID ,				
		CASE 						
			WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
			ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
					
		CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,	
		SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) , 
		CASE
			WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
			ELSE '' END ,			
		mact.MDS_ACTY_TYPE_DES ,
		CASE 
			WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
			ELSE maac.MDS_MAC_ACTY_NME END ,		
		CASE
			WHEN moe.MDS_SRVC_TIER_ID is NULL THEN ''
			ELSE svti.MDS_SRVC_TIER_DES END , 
		evst.EVENT_STUS_DES ,
		CASE
			WHEN luser1.DSPL_NME is NULL THEN ''	
			ELSE luser1.DSPL_NME END ,
		CASE
			WHEN mds.EVENT_STUS_ID = 2 THEN ''
			WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
			WHEN evhi.CREAT_BY_USER_ID is not NULL THEN luser2.DSPL_NME END,
		''                                     AS [S/C Flag]
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)				
		JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID		
		LEFT JOIN ( SELECT mevt.EVENT_ID, max(ehis.EVENT_HIST_ID) 'maxHistId'
						FROM COWS.dbo.MDS_EVENT mevt with (nolock)
						JOIN COWS.dbo.EVENT_HIST ehis with (nolock) ON mevt.EVENT_ID = ehis.EVENT_ID 	
					WHERE mevt.EVENT_STUS_ID in (3, 4, 6) and ehis.ACTN_ID = 5
					GROUP BY mevt.EVENT_ID)evHist ON mds.EVENT_ID = evHist.EVENT_ID	   				
		LEFT JOIN COWS.dbo.EVENT_HIST evhi with (nolock) ON evHist.maxHistId = evhi.EVENT_HIST_ID			
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MNS moe with (nolock) ON fmds.FSA_MDS_EVENT_ID = moe.FSA_MDS_EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_SRVC_TIER svti with (nolock) ON moe.MDS_SRVC_TIER_ID = svti.MDS_SRVC_TIER_ID				
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.MNS_PM_ID = luser1.USER_ADID					
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID
	WHERE mds.MDS_ACTY_TYPE_ID <> 3  AND mds.EVENT_STUS_ID in (2, 3, 4, 6) 
	      AND mds.STRT_TMST >=COALESCE(@startDate,mds.STRT_TMST )  AND mds.STRT_TMST<COALESCE(@endDate,mds.STRT_TMST) 

		
	UNION ALL
	
	SELECT CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END ,
		mds.EVENT_ID ,				
		CASE 						
			WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
			ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
					
		CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,	
		SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) , 
		CASE
			WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
			ELSE '' END ,			
		mact.MDS_ACTY_TYPE_DES ,
		CASE 
			WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
			ELSE maac.MDS_MAC_ACTY_NME END ,		
		CASE
			WHEN moe.MDS_SRVC_TIER_ID is NULL THEN ''
			ELSE svti.MDS_SRVC_TIER_DES END , 
		evst.EVENT_STUS_DES ,
		CASE
			WHEN luser1.DSPL_NME is NULL THEN ''	
			ELSE luser1.DSPL_NME END ,
		CASE
			WHEN mds.EVENT_STUS_ID = 2 THEN ''
			WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
			WHEN evhi.CREAT_BY_USER_ID is not NULL THEN luser2.DSPL_NME END,
		CASE	
						WHEN eod.SC_CD = 'C'        THEN 'Complex'
						WHEN eod.SC_CD = 'S'        THEN 'Simple'
					    ELSE ''
						END                                     AS [S/C Flag]
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)				
		JOIN COWS.dbo.FSA_MDS_EVENT fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID		
		LEFT JOIN ( SELECT mevt.EVENT_ID, max(ehis.EVENT_HIST_ID) 'maxHistId'
						FROM COWS.dbo.MDS_EVENT mevt with (nolock)
						JOIN COWS.dbo.EVENT_HIST ehis with (nolock) ON mevt.EVENT_ID = ehis.EVENT_ID 	
					WHERE mevt.EVENT_STUS_ID in (3, 4, 6) and ehis.ACTN_ID = 5
					GROUP BY mevt.EVENT_ID)evHist ON mds.EVENT_ID = evHist.EVENT_ID	   				
		LEFT JOIN COWS.dbo.EVENT_HIST evhi with (nolock) ON evHist.maxHistId = evhi.EVENT_HIST_ID			
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MNS moe with (nolock) ON fmds.FSA_MDS_EVENT_ID = moe.FSA_MDS_EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_SRVC_TIER svti with (nolock) ON moe.MDS_SRVC_TIER_ID = svti.MDS_SRVC_TIER_ID				
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.MNS_PM_ID = luser1.USER_ADID					
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID
		LEFT OUTER JOIN [COWS].[dbo].[MDS_EVENT_ODIE_DEV] eod WITH (NOLOCK) ON eod.EVENT_ID = mds.EVENT_ID
	WHERE mds.MDS_ACTY_TYPE_ID <> 3  AND mds.EVENT_STUS_ID in (2, 3, 4, 6) 
	      AND mds.STRT_TMST >=COALESCE(@startDate,mds.STRT_TMST )  AND mds.STRT_TMST<COALESCE(@endDate,mds.STRT_TMST) 
	ORDER BY mds.EVENT_ID
	
	
	SELECT	Event 'Event Type',
			Event_ID 'Event ID',
			CASE 
				--WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--ELSE Cust_Name 
				WHEN @SecuredUser = 0 and Scurd_Cd =0 THEN CUST_NAME
				WHEN @SecuredUser = 0 and Scurd_Cd >0 AND  CUST_NAME2 <>'' THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd >0 AND CUST_NAME2 <> '' THEN 'Private Customer'
				ELSE COALESCE(CUST_NAME2,CUST_NAME,'')
				END 'Customer Name',		
			YearMonth,
			MDS_Inst_Acty 'MDS Install Activity',
			MDS_Acty_Type 'MDS Activity Type',
			MDS_Mac_Acty 'MDS MAC Activity', 
			Svc_Tier 'Service Tier',
			Event_Status 'Event Status',
			Assigned_MNS_PM 'Assigned Acct MNS PM',
			Reviewed_By 'Reviewed By',
			SC_Flag 'S/C Flag'
			
	FROM @tmp_mns_evtCust
		
	
	
	CLOSE SYMMETRIC KEY FS@K3y;
	
	RETURN 0;	
  
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(300)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptEventsPerCustomerOD, '
	SET @Desc=@Desc +  CAST(@SecuredUser AS VARCHAR(2)) 
	SET @Desc=@Desc + ', ''' + @getSensData + ''', '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)		
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH





