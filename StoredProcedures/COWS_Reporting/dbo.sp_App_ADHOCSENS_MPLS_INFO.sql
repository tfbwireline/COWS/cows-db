USE [COWS_Reporting]
GO
/*
--axm3320 7/23 Add Inprogress Action to the select query actn_id = 19
--axm3320 8/24 Event_title_text and CustomerName are sensitive information pulled for CSG 2 users
--axm3320: 12/4/2012 added action publish
--axm3320:3/19/2013 Replace action_id 15(reschedule) with 44 (reschedule-email notfn sent)
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
*/
ALTER Proc [dbo].[sp_App_ADHOCSENS_MPLS_INFO]
 AS

 BEGIN TRY
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
    -- Insert statements for procedure here
    
    
Declare @SQLSELECT1 as varchar(max)
Declare @SQLSELECT2 as varchar(max)
Declare @SQLSELECT3 as varchar(max)
--Declare @SQLSELECT4 as varchar(max) 
 
 set @SQLSELECT1 =  'With MPLS_Event_Data as (select distinct  a.FTN,
    mpls.EVENT_ID , 
	UPPER(mpls.EVENT_STUS_DES) [MPLS_EVSTUS], 		
	CASE WHEN ordr.CSG_LVL_ID = 0 THEN mpls.EVENT_TITLE_TXT  	
	WHEN ordr.CSG_LVL_ID > 0 AND mpls.EVENT_TITLE_TXT2 IS NOT NULL THEN ''PRIVATE CUSTOMER''
	WHEN ordr.CSG_LVL_ID > 0 AND mpls.EVENT_TITLE_TXT2 IS NULL THEN '''' END [MPLS_EVTTTLE],    					
	CASE WHEN ordr.CSG_LVL_ID = 0 THEN mpls.MCUST_NME  	
	WHEN ordr.CSG_LVL_ID > 0 AND mpls.MCUST_NME2 IS NOT NULL THEN ''PRIVATE CUSTOMER''
	WHEN ordr.CSG_LVL_ID > 0 AND mpls.MCUST_NME2 IS NULL THEN '''' END [MCUST_NME],  	
	CASE WHEN mpls.DES_CMNT_TXT is NULL THEN '''' ELSE mpls.DES_CMNT_TXT END [MPLS_DESCMNT],
	mpls.MPLS_EVENT_TYPE_DES ,	
	mpls.MPLS_ACTY_TYPE_DES ,	
	mpls.VPN_PLTFRM_TYPE_DES [MPLS_VPNPLT],
	mpls.VAS_TYPE_DES [MPLS_VASTYP] ,
	mpls.IP_VER_ID [MPLS_IPVER] ,
	mpls.STRT_TMST [MPLS_STRTTMST],
	mpls.EVENT_DRTN_IN_MIN_QTY [MPLS_EVNTDRTN] ,
	mpls.EXTRA_DRTN_TME_AMT [MPLS_EXTRADRTN] ,			
    mpls.End_Time 	[MPLS_EndTme],
	mpls.MPLS_ACTN_DES, 
	mpls.MPLS_MOD_DT [MPLS_MOD_DT],
	mpls.MPLS_MOD_BY [MPLS_MOD_BY],
	CASE WHEN mpls.SUCSS_ACTY_DES is NULL THEN '''' ELSE mpls.SUCSS_ACTY_DES END [MPLS_SUCSSACTY],
	CASE WHEN mpls.FAIL_ACTY_DES is NULL THEN '''' ELSE mpls.FAIL_ACTY_DES END [MPLS_FAILACTY],
	mpls.MPLS_CMNT_TXT [MPLS_CMNT_TXT],
	case when mpls.MPLS_ESCL_CD  = 1 THEN ''Y'' else ''N'' END  [MPLS_ESCL_CD],
	case when mpls.MDS_MNGD_CD  = 1 THEN ''Y'' else ''N'' END  [MPLS_MDS_MNGD_CD],
	case when mpls.RELTD_CMPS_NCR_CD  = 1 THEN ''Y'' else ''N'' END [MPLS_RELTD_CMPS_NCR_CD],
	case when  mpls.WHLSL_PTNR_CD  = 1 THEN ''Y'' else ''N'' END [MPLS_WHLSL_PTNR_CD],
	case when  mpls.CXR_PTNR_CD  = 1 THEN ''Y'' else ''N'' END [MPLS_CXR_PTNR_CD],
	case when  mpls.NDD_UPDTD_CD  = 1 THEN ''Y'' else ''N'' END [MPLS_NDD_UPDTD_CD]
   from COWS.dbo.ORDR ordr with (nolock) 
   left outer join (SELECT bb.FTN,bb.ORDR_ID  FROM [COWS].[dbo].[FSA_ORDR] bb with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
		FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
		WHERE fsa.FTN = bb.FTN)) a on a.ORDR_ID = ordr.ORDR_ID '
   
   set @SQLSELECT2 =  ' full outer join (select distinct mpls.EVENT_ID , mpls.FTN,
			UPPER(evst.EVENT_STUS_DES) [EVENT_STUS_DES], 	
     		CASE WHEN CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) is NULL THEN '''' ELSE CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) END [EVENT_TITLE_TXT2],	
			CASE WHEN CONVERT(varchar, DecryptByKey(csd.CUST_NME)) is NULL THEN '''' ELSE CONVERT(varchar, DecryptByKey(csd.CUST_NME)) END [MCUST_NME2],
			mpls.EVENT_TITLE_TXT AS[EVENT_TITLE_TXT],	
			mpls.CUST_NME AS [MCUST_NME],
			CASE WHEN mpls.DES_CMNT_TXT is NULL THEN '''' ELSE mpls.DES_CMNT_TXT END [DES_CMNT_TXT],
			lkmt.MPLS_EVENT_TYPE_DES ,	
			mpact.MPLS_ACTY_TYPE [MPLS_ACTY_TYPE_DES] ,	
			lkvp.VPN_PLTFRM_TYPE_DES ,
			lkvt.VAS_TYPE_DES ,
			mpls.IP_VER_ID ,
			mpls.STRT_TMST [STRT_TMST],
			mpls.EVENT_DRTN_IN_MIN_QTY ,
			mpls.EXTRA_DRTN_TME_AMT ,			
			mpls.END_TMST  [End_Time],	
			CASE
				WHEN lkac.ACTN_DES = ''Completed Email Notification'' THEN ''Completed'' 
				ELSE lkac.ACTN_DES END [MPLS_ACTN_DES],
			REPLACE(SUBSTRING(CONVERT(varchar, mpev.CREAT_DT,6), 1, 9), '' '', ''-'') + '' '' + SUBSTRING(CONVERT(varchar, mpev.CREAT_DT, 109), 13, 8) 
				+ '' '' + SUBSTRING(CONVERT(varchar, mpev.CREAT_DT, 109), 25, 2) [MPLS_MOD_DT],		
			CASE
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END [MPLS_MOD_BY],
							
			CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '''' ELSE scev.SUCSS_ACTY_DES END [SUCSS_ACTY_DES],		
			CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '''' ELSE flev.FAIL_ACTY_DES END [FAIL_ACTY_DES],
			CASE WHEN mpev.CMNT_TXT is NULL THEN '''' ELSE mpev.CMNT_TXT END [MPLS_CMNT_TXT],
			mpls.escl_cd [MPLS_ESCL_CD],
			mpls.MDS_MNGD_CD [MDS_MNGD_CD],
		    mpls.RELTD_CMPS_NCR_CD [RELTD_CMPS_NCR_CD],
		    mpls.WHLSL_PTNR_CD [WHLSL_PTNR_CD],
		    mpls.CXR_PTNR_CD [CXR_PTNR_CD],
		    mpls.NDD_UPDTD_CD	 [NDD_UPDTD_CD]										
			FROM COWS.dbo.MPLS_EVENT mpls with (nolock) 		
			left outer JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mpls.EVENT_STUS_ID = evst.EVENT_STUS_ID
			left outer join COWS.dbo.LK_MPLS_EVENT_TYPE lkmt with (nolock) ON mpls.MPLS_EVENT_TYPE_ID = lkmt.MPLS_EVENT_TYPE_ID
			left outer join COWS.dbo.LK_VPN_PLTFRM_TYPE lkvp with (nolock) ON mpls.VPN_PLTFRM_TYPE_ID = lkvp.VPN_PLTFRM_TYPE_ID
			left outer JOIN COWS.dbo.MPLS_EVENT_VAS_TYPE mvst with (nolock) ON mpls.EVENT_ID = mvst.EVENT_ID
			left outer JOIN COWS.dbo.LK_VAS_TYPE lkvt with (nolock) ON mvst.VAS_TYPE_ID = lkvt.VAS_TYPE_ID
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mpls.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=9	
			left outer JOIN
			( 	SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT,eh.CMNT_TXT 
					FROM COWS.dbo.EVENT_HIST eh with (nolock)
					--JOIN COWS.dbo.MPLS_EVENT_ACTY_TYPE meat with (nolock) ON eh.EVENT_ID = meat.EVENT_ID
					JOIN COWS.dbo.MPLS_EVENT mple with (nolock) ON eh.EVENT_ID = mple.EVENT_ID
       				WHERE eh.ACTN_ID in (5,44,19) AND mple.EVENT_STUS_ID in (3, 6)
					-- axm3320: 7/15 commenting WHERE eh.ACTN_ID in (5,10,12,15,19) AND mple.EVENT_STUS_ID not in (8)
				UNION
				SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT,ehis.CMNT_TXT 
					FROM COWS.dbo.EVENT_HIST ehis with (nolock)
					JOIN
					(
						SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) ''maxEvHis''
							FROM COWS.dbo.EVENT_HIST eh with (nolock)
							--JOIN COWS.dbo.MPLS_EVENT_ACTY_TYPE meat with (nolock) ON eh.EVENT_ID = meat.EVENT_ID
							JOIN COWS.dbo.MPLS_EVENT mple with (nolock) ON eh.EVENT_ID = mple.EVENT_ID
						--	axm3320 : 7/15 WHERE eh.ACTN_ID in (18) AND mple.EVENT_STUS_ID not in (8)
							WHERE eh.ACTN_ID in (18) AND mple.EVENT_STUS_ID in (6)
						GROUP BY eh.EVENT_ID
					)ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis
			)mpev ON mpls.EVENT_ID = mpev.EVENT_ID						
			LEFT outer  JOIN
			(
				SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lksu.SUCSS_ACTY_DES  + '', '' AS [text()] 
	 						FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)
	 						JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID 
	 						WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID --AND evsu.REC_STUS_ID = 1
	 						FOR xml PATH ('''')
							) as SUCSS_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)	
					)evSucAct
		  		)evSucAct2
			)scev ON mpev.EVENT_HIST_ID = scev.EVENT_HIST_ID
			LEFT outer JOIN
			(
				SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lkfl.FAIL_ACTY_DES  + '', '' AS [text()] 
	 						FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)
	 						JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID 
	 						WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID --AND evfl.REC_STUS_ID = 1
	 						FOR xml PATH ('''')
							) as FAIL_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)	
					)evFailAct
		  		)evFailAct2
			)flev ON mpev.EVENT_HIST_ID = flev.EVENT_HIST_ID			
			LEFT outer JOIN
			(
				SELECT distinct LEFT([MPLS_ACTY_TYPE],len([MPLS_ACTY_TYPE]) -1) as MPLS_ACTY_TYPE, EVENT_ID
				FROM
				(
					SELECT (SELECT lkma.MPLS_ACTY_TYPE_DES + '', '' AS [text()] 
							FROM COWS.dbo.MPLS_EVENT_ACTY_TYPE meac with (nolock)
							JOIN COWS.dbo.LK_MPLS_ACTY_TYPE lkma with (nolock) ON meac.MPLS_ACTY_TYPE_ID = lkma.MPLS_ACTY_TYPE_ID
							WHERE meac.EVENT_ID = mple.EVENT_ID 
							FOR xml PATH ('''')
						   ) as MPLS_ACTY_TYPE, EVENT_ID 
					FROM 
					( SELECT EVENT_ID FROM COWS.dbo.MPLS_EVENT with (nolock) where EVENT_STUS_ID in (3,6)	 
					 --SELECT EVENT_ID FROM COWS.dbo.MPLS_EVENT with (nolock) where EVENT_STUS_ID not in (8)					
											)
					mple
				)mpaty 
			)mpact ON mpls.EVENT_ID = mpact.EVENT_ID			
			left JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mpev.CREAT_BY_USER_ID = luser1.USER_ID 
			left JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON mpev.ACTN_ID = lkac.ACTN_ID ) as mpls on mpls.FTN = a.FTN ) '

set @SQLSELECT3 = ' select * from MPLS_Event_Data '

--print (@SQLSELECT1) 
--print (@SQLSELECT2) 
--print (@SQLSELECT3) 

exec ( @SQLSELECT1 + @SQLSELECT2 + @SQLSELECT3)

CLOSE SYMMETRIC KEY FS@K3y;


END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc= 'EXEC COWS_Reporting.dbo.sp_App_ADHOCSENS_MPLS_INFO'
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH
