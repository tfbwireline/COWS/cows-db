 --=============================================    
 --Author:  <Author,,Name>    
 --Create date: <Create Date,,>    
 --Description: <Description,,>    
    
 ----EXEC COWS_Reporting.dbo.sp_AMNCIRptPending 0    
 -- Modifications    
 -- 1- CI554013: 01/10/2013 changed ONVERT(VARCHAR(10),xxx,101) to CONVERT(DATE,xxxx). To correct date range issue.    
 -- 2- CI554013: 06/28/2013 Change join code for [USER_WFM_ASMT] and [CKT].    
     
 -- 3 - dlp0278 7/15/2013 Added City, STDI fields, "Due in 2 weeks" logic, removed 3 CCD columns, and modified COWS CCD to always     
 --        show the CCD date whether FSA or Modified. Also adding NCCO orders.    
 -- 4 - dlp0278 1/22/2014 Added BAR Pending On Hold records to the report.    
 --      
 --  CI554013: 3/28/2014 Added Parent FTN Column      
 --    
 -- dlp0278  7/24/2014 Added Equip Install Date to end of report.  
 -- Updated By:   Md M Monir  
 -- Updated Date: 03/14/2018  
 -- Updated Reason: SCURD_CD/CSG_LVL_CD  
 -- Update date: <06/06/2018>
 -- Updated by: Md M Monir  
 -- Description: <To get H5 Data>  CTY_NME                     
 --=============================================    
ALTER PROCEDURE [dbo].[sp_AMNCIRptPendingOD] --0, 'N', '07/01/2014', '07/04/2014'    
 @SecuredUser  INT=0,    
 @getSensData  CHAR(1)='N',    
 @startDate   Datetime=NULL,    
 @endDate   Datetime=NULL    
AS    
BEGIN TRY    
    
 OPEN SYMMETRIC KEY FS@K3y     
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;     
    
 IF ((@startDate IS NULL) AND (@endDate IS NULL))    
 BEGIN    
  SET @endDate = GETDATE()    
  SET @startDate = DATEADD(mm,-13,CONVERT(VARCHAR(25),DATEADD(DD,-(DAY(GETDATE())-1),GETDATE()),101))    
 END    
    
 SELECT DISTINCT     
 ISNULL(a.FTN, ISNULL(ipl.ORDR_ID, ncco.ORDR_ID)) [FTN],     
 i.RGN_DES [xNCI Region],    
 CASE    
  WHEN @SecuredUser = 0 AND si.CUST_NME<>''  THEN coalesce(si.CUST_NME,'PRIVATE CUSTOMER')  
  WHEN @SecuredUser = 0 AND si.CUST_NME =''  THEN 'PRIVATE CUSTOMER'   
  WHEN @SecuredUser = 0 AND si.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'   
  WHEN @SecuredUser = 1 AND @getSensData='N'AND si.CUST_NME<>''  THEN coalesce(si.CUST_NME,'PRIVATE CUSTOMER')  
  WHEN @SecuredUser = 1 AND @getSensData='N'AND si.CUST_NME =''  THEN 'PRIVATE CUSTOMER'    
  WHEN @SecuredUser = 1 AND @getSensData='N'AND si.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'    
  ELSE coalesce(si.CUST_NME2,si.CUST_NME,'')  
  END [Customer Name], --Secured  
 CASE     
  WHEN ord.ORDR_CAT_ID = 1 THEN lp.PROD_TYPE_DES    
  WHEN ord.ORDR_CAT_ID = 2 THEN d.PROD_TYPE_DES     
  WHEN ord.ORDR_CAT_ID = 6 THEN d.PROD_TYPE_DES     
  WHEN ord.ORDR_CAT_ID = 4 THEN nccop.PROD_TYPE_DES     
  END [Product],      
 CASE     
  WHEN ord.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ipl.CKT_SPD_QTY, 101)    
  WHEN ord.ORDR_CAT_ID = 6 THEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES    
  WHEN ord.ORDR_CAT_ID = 2 THEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES END [Order Bandwidth],    
 n.FULL_NME  [Implementation Contact],    
 CASE     
  WHEN ord.ORDR_CAT_ID = 1 THEN ipl.CLCM_NME    
  WHEN ord.ORDR_CAT_ID = 6 THEN cm.FRST_NME + ' ' + cm.LST_NME    
  WHEN ord.ORDR_CAT_ID = 2 THEN cm.FRST_NME + ' ' + cm.LST_NME END [CPM Contact],     
 CASE     
  WHEN ord.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),ipl.CUST_CMMT_DT,101)    
  WHEN ord.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10),a.CUST_CMMT_DT,101)    
  WHEN ord.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),a.CUST_CMMT_DT,101)     
  WHEN ord.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ncco.CCS_DT, 101) END [FSA CCD],     
 CASE     
  WHEN ord.ORDR_CAT_ID = 1 THEN ISNULL( CONVERT(DATETIME,ckm.TRGT_DLVRY_DT),  ipl.CUST_WANT_DT)    
  WHEN ord.ORDR_CAT_ID = 6 THEN ISNULL( CONVERT(DATETIME,ckm.TRGT_DLVRY_DT),  a.CUST_WANT_DT)    
  WHEN ord.ORDR_CAT_ID = 2 THEN ISNULL( CONVERT(DATETIME,ckm.TRGT_DLVRY_DT),  a.CUST_WANT_DT)  END [Sprint Target Delivery Date],    
 f.ORDR_STUS_DES [Order Status],      
 mg.FULL_NME [Manager],    
 CONVERT(VARCHAR(10),GetDate(),101) [Today],    
 -- 1- Beggin    
 /* CASE    
   WHEN a.CUST_WANT_DT IS NULL THEN 'No STDD'    
   WHEN CONVERT(DATE,a.CUST_WANT_DT) >= CONVERT(DATE,GetDate()) THEN 'Pending'     
   WHEN [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(), a.CUST_WANT_DT) < 0 THEN 'Past'     
   END [Past Due with STDD],    
  CASE    
   WHEN a.CUST_CMMT_DT IS NULL THEN 'No CCD'    
   WHEN CONVERT(DATE,a.CUST_CMMT_DT) >= CONVERT(DATE,GetDate()) THEN 'Pending'     
   WHEN [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(), a.CUST_CMMT_DT) < 0 THEN 'Past'     
   END [Past Due with CCD],    
 a.ORDR_TYPE_CD [Order Type],    
  CASE    
   WHEN a.CUST_CMMT_DT IS NULL THEN 'No CCD'    
   WHEN CONVERT(DATE,a.CUST_CMMT_DT) >= CONVERT(DATE,GetDate()) THEN 'Pending'     
   WHEN [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(), a.CUST_CMMT_DT) < 0 THEN 'Past'     
   ELSE 'Due in 2 weeks' END [CCD Past Due Status],    
 */    
  CASE    
   WHEN CONVERT(DATE,ISNULL(ord.CUST_CMMT_DT,ISNULL(a.CUST_CMMT_DT, ISNULL(ipl.CUST_CMMT_DT,ncco.CCS_DT)))) <= DATEADD(d,14,GETDATE())    
     AND [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(),ISNULL(ord.CUST_CMMT_DT,ISNULL(a.CUST_CMMT_DT, ISNULL(ipl.CUST_CMMT_DT,ncco.CCS_DT)))) > 0    
     THEN 'Due in 2 weeks'    
   WHEN CONVERT(DATE,ISNULL(ord.CUST_CMMT_DT,ISNULL(a.CUST_CMMT_DT, ISNULL(ipl.CUST_CMMT_DT,ncco.CCS_DT)))) > DATEADD(d,14,CONVERT(DATE,GetDate())) THEN 'Pending'    
   WHEN [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(), ISNULL(ord.CUST_CMMT_DT,ISNULL(a.CUST_CMMT_DT, ISNULL(ipl.CUST_CMMT_DT,ncco.CCS_DT)))) < 0 THEN 'Past'    
   END [CCD Past Due Status],    
-- 1- End    
 cast(b5.MRC_CHG_AMT as money) [MRC],    
 cast(b5.NRC_CHG_AMT as money) [NRC],    
 CASE    
  WHEN ord.ORDR_CAT_ID = 4 THEN ncco.SITE_CITY_NME    
  WHEN @SecuredUser = 0 AND oa.CTY_NME<>''  THEN coalesce(oa.CTY_NME,'PRIVATE CUSTOMER')  
  WHEN @SecuredUser = 0 AND oa.CTY_NME =''  THEN 'PRIVATE CUSTOMER'   
  WHEN @SecuredUser = 0 AND oa.CTY_NME Is NULL THEN 'PRIVATE CUSTOMER'  
  WHEN @SecuredUser = 1 AND @getSensData='N' AND oa.CTY_NME<>''  THEN coalesce(oa.CTY_NME,'PRIVATE CUSTOMER')  
  WHEN @SecuredUser = 1 AND @getSensData='N' AND oa.CTY_NME =''  THEN 'PRIVATE CUSTOMER'     
  WHEN @SecuredUser = 1 AND @getSensData='N' AND oa.CTY_NME IS NULL  THEN 'PRIVATE CUSTOMER'   
  ELSE coalesce(oa.CTY_NME2,oa.CTY_NME,'')  
  END [City], --Secured    
 k.CTRY_NME [H6 Country],    
 [GOM Receives Order],    
 ckm.ACCS_ACPTC_DT [Access Accepted Date],    
 ord.ORDR_ID [ORDER ID],    
 CASE     
  WHEN ord.ORDR_CAT_ID = 1 THEN lo.ORDR_TYPE_DES    
  WHEN ord.ORDR_CAT_ID = 6 THEN j.ORDR_TYPE_DES    
  WHEN ord.ORDR_CAT_ID = 2 THEN j.ORDR_TYPE_DES    
  WHEN ord.ORDR_CAT_ID = 4 THEN nccoo.ORDR_TYPE_DES    
 END [Order Type],    
 vnd.VNDR_ORDR_ID [Vendor Order ID],    
 vl.VNDR_NME [Vendor Name],    
 c.PRE_SBMT_DT [Pre-submit Date],    
 c.SBMT_DT [Submit Date],    
 vm.SENT_TO_VNDR_DT [Sent to Vendor Date],    
 ckm.ACCS_DLVRY_DT [Access Delivery Date],    
 ISNULL(ord.CUST_CMMT_DT,ISNULL(a.CUST_CMMT_DT, ISNULL(ipl.CUST_CMMT_DT,ncco.CCS_DT))) [COWS CCD],    
 (SELECT TOP 1 STDI_DT from [COWS].[dbo].[ORDR_STDI_HIST] WITH (NOLOCK) where ORDR_ID = ord.ORDR_ID Order by CREAT_DT DESC) [STDI DATE],    
 (SELECT STDI_REAS_DES from [COWS].[dbo].[LK_STDI_REAS] sr WITH (NOLOCK)    
  INNER JOIN (SELECT TOP 1 STDI_REAS_ID from [COWS].[dbo].[ORDR_STDI_HIST] WITH (NOLOCK) where ORDR_ID = ord.ORDR_ID Order by CREAT_DT DESC) sh     
  ON sh.STDI_REAS_ID = sr.STDI_REAS_ID) [STDI REASON ID],    
 (SELECT TOP 1 CMNT_TXT from [COWS].[dbo].[ORDR_STDI_HIST] WITH (NOLOCK) where ORDR_ID = ord.ORDR_ID Order by CREAT_DT DESC) [STDI COMMENTS],    
 a.PRNT_FTN [Parent FTN],    
 g.EQPT_INSTL_DT [Equip Install Date]    
    
 FROM [COWS].[dbo].[ORDR] ord WITH (NOLOCK)     
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] a WITH (NOLOCK) ON ord.ORDR_ID = a.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = a.ORDR_ID   
  LEFT OUTER JOIN  [COWS].[dbo].[ORDR_MS] c WITH (NOLOCK) ON ord.ORDR_ID = c.ORDR_ID     
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, CONVERT(VARCHAR, DecryptByKey(bb.FRST_NME)) [FRST_NME],     
     CONVERT(VARCHAR, DecryptByKey(bb.LST_NME)) [LST_NME]--, CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]     
    FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK)    
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc  WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID    
    WHERE bb.ROLE_ID = 13 AND bb.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)     
     FROM (SELECT ORDR_CNTCT_ID, ORDR_ID     
      FROM [COWS].[dbo].[ORDR_CNTCT] WITH (NOLOCK) WHERE ROLE_ID = 13) oc     
     WHERE oc.ORDR_ID = bb.ORDR_ID)) gom on ord.ORDR_ID = gom.ORDR_ID     
         
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, CONVERT(VARCHAR, DecryptByKey(bb.FRST_NME)) [FRST_NME],     
     CONVERT(VARCHAR, DecryptByKey(bb.LST_NME)) [LST_NME]--, CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]     
    FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK)     
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID     
    WHERE bb.ROLE_ID = 6 AND bb.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)     
     FROM (SELECT ORDR_CNTCT_ID, ORDR_ID     
      FROM [COWS].[dbo].[ORDR_CNTCT] WITH (NOLOCK) WHERE ROLE_ID = 6) oc     
     WHERE oc.ORDR_ID = bb.ORDR_ID)) cm on ord.ORDR_ID = cm.ORDR_ID    
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, CONVERT(VARCHAR, DecryptByKey(bb.FRST_NME)) [FRST_NME],     
     CONVERT(VARCHAR, DecryptByKey(bb.LST_NME)) [LST_NME]--, CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]     
    FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK)     
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID     
    WHERE bb.ROLE_ID = 35 AND bb.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)     
     FROM (SELECT ORDR_CNTCT_ID, ORDR_ID     
      FROM [COWS].[dbo].[ORDR_CNTCT] WITH (NOLOCK) WHERE ROLE_ID = 35) oc     
     WHERE oc.ORDR_ID = bb.ORDR_ID)    
     ) ipm on ord.ORDR_ID = ipm.ORDR_ID     
         
    
  LEFT OUTER JOIN   
  (SELECT cc.ORDR_ID,   
   bb.H5_FOLDR_ID,   
   CONVERT(VARCHAR, DecryptByKey(csd.CUST_NME)) AS CUST_NME2,   
   bb.CUST_NME AS CUST_NME,   
   --bb.CTRY_CD,   
   --bb.CUST_CTY_NME
   CASE WHEN (bb.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTRY_CD) ELSE bb.CTRY_CD END AS CTRY_CD, /*Monir 06062018*/ 
   CASE WHEN (bb.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.CTY_NME) ELSE bb.CUST_CTY_NME END AS  [CUST_CTY_NME] /*Monir 06062018*/    
    FROM [COWS].[dbo].[H5_FOLDR] bb WITH (NOLOCK)     
    LEFT OUTER JOIN [COWS].[dbo].[ORDR] cc WITH (NOLOCK) on bb.H5_FOLDR_ID = cc.H5_FOLDR_ID  
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6  
   ) si on ord.ORDR_ID = si.ORDR_ID     
    
         
  LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID, bb.STUS_ID    
    FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)    
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID     
    WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)    
    FROM (SELECT ACT_TASK_ID, ORDR_ID     
     FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk     
     WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk on ord.ORDR_ID = tk.ORDR_ID    
         
  LEFT OUTER JOIN (SELECT ORDR_ID, CONVERT(VARCHAR(10),MIN(at.CREAT_DT),101) as [GOM Receives Order]    
    FROM [COWS].dbo.ACT_TASK at WITH (NOLOCK)    
    JOIN [COWS].dbo.MAP_GRP_TASK mgt WITH (NOLOCK) on mgt.TASK_ID=at.TASK_ID    
    WHERE mgt.GRP_ID=1 AND at.TASK_ID NOT IN (101,102)    
    GROUP BY ORDR_ID) gq ON ord.ORDR_ID = gq.ORDR_ID    
        
--2  LEFT OUTER JOIN (SELECT  DISTINCT  bb.ASMT_DT,  bb.ORDR_ID,  bb.ASN_USER_ID,  bb.GRP_ID    
--   FROM [COWS].[dbo].[USER_WFM_ASMT] bb     
--   LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID     
--    WHERE  bb.GRP_ID = 5 AND bb.ASMT_DT = (SELECT MAX(ASMT_DT)    
--    FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, GRP_ID    
--     FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr      
--    WHERE bb.ORDR_ID = usr.ORDR_ID  )    
--    AND     
--    bb.ASN_USER_ID in (SELECT  ASN_USER_ID    
--     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID, GRP_ID    
--      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas    
--     WHERE bb.ORDR_ID = uas.ORDR_ID AND uas.GRP_ID =5)     
--   ) ur ON ord.ORDR_ID = ur.ORDR_ID     
        
  LEFT OUTER JOIN (SELECT  DISTINCT  bb.ASMT_DT,  bb.ORDR_ID,  bb.ASN_USER_ID,  bb.GRP_ID    
     FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)     
     LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID     
      WHERE  bb.ASN_USER_ID in (SELECT  top 1 ASN_USER_ID    
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID, GRP_ID    
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas    
     WHERE bb.ORDR_ID = uas.ORDR_ID AND uas.GRP_ID =5 ORDER BY ASMT_DT DESC)     
   ) ur ON ord.ORDR_ID = ur.ORDR_ID     
       
  LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] d WITH (NOLOCK) ON a.PROD_TYPE_CD = d.FSA_PROD_TYPE_CD    
  LEFT OUTER JOIN (SELECT TASK_ID, CREAT_DT FROM [COWS].[dbo].[LK_TASK] WITH (NOLOCK) WHERE TASK_ID = 206) e  ON tk.TASK_ID = e.TASK_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_STUS] f WITH (NOLOCK) ON ord.ORDR_STUS_ID = f.ORDR_STUS_ID    
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_GOM_XNCI] g WITH (NOLOCK) ON ord.ORDR_ID = g.ORDR_ID    
  LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] h WITH (NOLOCK) ON ord.ORDR_ID = h.ORDR_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_XNCI_RGN] i WITH (NOLOCK) ON ord.RGN_ID = i.RGN_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] j WITH (NOLOCK) ON  a.ORDR_TYPE_CD = j.FSA_ORDR_TYPE_CD    
  LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] r WITH (NOLOCK) ON a.FTN = r.FTN    
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] k WITH (NOLOCK) ON si.CTRY_CD = k.CTRY_CD    
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON r.TOC_CTRY_CD = l.CTRY_CD     
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] m WITH (NOLOCK) ON ord.ORDR_ID = m.ORDR_ID     
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] n WITH (NOLOCK) ON ur.ASN_USER_ID = n.USER_ID     
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] mg WITH (NOLOCK) on n.MGR_ADID = mg.USER_ADID    
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, SUM(CAST(bb.MRC_CHG_AMT AS MONEY)) MRC_CHG_AMT,     
  SUM(CAST (REPLACE(bb.NRC_CHG_AMT,'null','') AS MONEY)) NRC_CHG_AMT     
  FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
  JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
  GROUP by bb.ORDR_ID) b5 on ord.ORDR_ID = b5.ORDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR] vnd WITH (NOLOCK) ON ord.ORDR_ID = vnd.ORDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] vf WITH (NOLOCK) ON vnd.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] vl WITH (NOLOCK) ON vf.VNDR_CD = vl.VNDR_CD    
 LEFT OUTER JOIN (SELECT a.VNDR_ORDR_ID, SENT_TO_VNDR_DT FROM [COWS].[dbo].[VNDR_ORDR_MS] a WITH (NOLOCK)    
  WHERE VNDR_ORDR_MS_ID = (SELECT Top 1 VNDR_ORDR_MS_ID     
     from [COWS].[dbo].[VNDR_ORDR_MS] WITH (NOLOCK) WHERE VNDR_ORDR_ID = a.VNDR_ORDR_ID     
     ORDER BY VNDR_ORDR_MS_ID DESC)) vm ON vnd.VNDR_ORDR_ID = vm.VNDR_ORDR_ID    
    
--2  LEFT OUTER JOIN (SELECT  DISTINCT aa.ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT] aa     
--  JOIN [COWS].[dbo].[ORDR] bb on  aa.ORDR_ID = bb.ORDR_ID where    
--   CKT_ID = (SELECT  MAX(CKT_ID) FROM (SELECT DISTINCT ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT]) ck        
--   where bb.ORDR_ID = ck.ORDR_ID)) ckt on ord.ORDR_ID = ckt.ORDR_ID    
    
 LEFT OUTER JOIN (SELECT ck.ORDR_ID, ck.CKT_ID, VER_ID, ACCS_DLVRY_DT, ACCS_ACPTC_DT, TRGT_DLVRY_DT     
      FROM [COWS].[dbo].[CKT_MS] cm WITH (NOLOCK)      
      LEFT OUTER JOIN [COWS].[dbo].[CKT] ck WITH (NOLOCK) ON cm.CKT_ID = ck.CKT_ID    
      WHERE VER_ID = (SELECT MAX(VER_ID)     
          FROM [COWS].[dbo].[CKT_MS] WITH (NOLOCK)     
          WHERE CKT_ID = ck.CKT_ID))ckm ON ord.ORDR_ID = ckm.ORDR_ID    
         
        
 LEFT OUTER JOIN [COWS].[dbo].[IPL_ORDR] ipl WITH (NOLOCK) ON ord.ORDR_ID = ipl.ORDR_ID     
 LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] lp WITH (NOLOCK) on ipl.PROD_TYPE_ID = lp.PROD_TYPE_ID    
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] lo WITH (NOLOCK) on ipl.ORDR_TYPE_ID = lo.ORDR_TYPE_ID    
     
 LEFT OUTER JOIN (SELECT ORDR_ID, CONVERT(VARCHAR, DecryptByKey(csd.CTY_NME)) as [CTY_NME2] , O.CTY_NME as [CTY_NME]   
   FROM [COWS].[dbo].[ORDR_ADR] O WITH (NOLOCK)   
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=O.ORDR_ADR_ID  AND csd.SCRD_OBJ_TYPE_ID=14  
   WHERE CIS_LVL_TYPE = 'H5'  
   ) oa ON oa.ORDR_ID = ord.ORDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[NCCO_ORDR] ncco WITH (NOLOCK) on ord.ORDR_ID = ncco.ORDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] nccop WITH (NOLOCK) ON ncco.PROD_TYPE_ID = nccop.PROD_TYPE_ID    
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] nccoo WITH (NOLOCK) ON nccoo.ORDR_TYPE_ID = ncco.ORDR_TYPE_ID    
    
     
     
 WHERE     
 ord.DMSTC_CD = 1 AND ord.RGN_ID = 1 AND a.CREAT_DT BETWEEN @startDate AND @endDate    
 AND (a.PROD_TYPE_CD NOT IN ('MN') OR a.PROD_TYPE_CD IS NULL )    
 AND((ord.ORDR_STUS_ID not in (2,3,4,5,6) and tk.TASK_ID <> 1000 and NOT (tk.TASK_ID = 213 and tk.STUS_ID = 2) AND tk.TASK_ID <> 212 )    
  OR (tk.TASK_ID = 1000 and tk.STUS_ID = 3))    
 AND ord.H5_FOLDR_ID IS NOT NULL    
 AND (ord.ORDR_CAT_ID in (1,4) or (ord.ORDR_CAT_ID in (2,6) and a.ORDR_ACTN_ID = 2))    
     
 GROUP BY     
 ISNULL(a.FTN, ISNULL(ipl.ORDR_ID, ncco.ORDR_ID))     
 ,i.RGN_DES    
 ,oa.CTY_NME   
 ,oa.CTY_NME2     
 ,ncco.SITE_CITY_NME    
 ,k.CTRY_NME    
 /*,ord.SCURD_CD*/    
 ,ord.ORDR_CAT_ID    
 ,si.CUST_NME  
 ,si.CUST_NME2    
 ,lp.PROD_TYPE_DES    
 ,d.PROD_TYPE_DES     
 ,nccop.PROD_TYPE_DES    
 ,fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES     
 ,gom.FRST_NME    
 ,gom.LST_NME    
 ,cm.FRST_NME    
 ,cm.LST_NME    
 ,a.CUST_WANT_DT      
 ,a.CUST_CMMT_DT     
 ,r.PLN_NME    
 ,f.ORDR_STUS_DES     
 ,n.FULL_NME    
 ,mg.FULL_NME    
 ,[GOM Receives Order]    
 ,a.ORDR_TYPE_CD    
 ,b5.MRC_CHG_AMT    
 ,b5.NRC_CHG_AMT    
 ,a.INSTL_SOLU_SRVC_DES    
 ,h.A_END_REGION    
 ,e.CREAT_DT    
 ,ord.ORDR_ID    
 ,j.ORDR_TYPE_DES    
 ,nccoo.ORDR_TYPE_DES    
 ,lo.ORDR_TYPE_DES    
 ,vnd.VNDR_ORDR_ID    
 ,vl.VNDR_NME    
 ,c.PRE_SBMT_DT    
 ,c.SBMT_DT    
 ,vm.SENT_TO_VNDR_DT    
 ,ipl.CKT_SPD_QTY    
 ,ipl.SALS_PERSN_NME    
 ,ipl.CLCM_NME    
 ,ipl.CUST_CMMT_DT    
 ,ckm.TRGT_DLVRY_DT    
 ,ipl.CUST_WANT_DT    
 ,ckm.ACCS_ACPTC_DT    
 ,ckm.ACCS_DLVRY_DT    
 ,ord.CUST_CMMT_DT    
 ,ncco.CCS_DT    
 ,a.PRNT_FTN    
 ,g.EQPT_INSTL_DT    
 ORDER BY ISNULL(a.FTN, ISNULL(ipl.ORDR_ID, ncco.ORDR_ID))     
           
 CLOSE SYMMETRIC KEY FS@K3y     
         
 RETURN 0;    
     
END TRY    
    
BEGIN CATCH    
 DECLARE @Desc VARCHAR(200)    
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptPendingOD '  + ',' + CAST(@SecuredUser AS VARCHAR(4)) + ': '    
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @SecuredUser, 101)    
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;    
 RETURN 1;    
END CATCH    
    
    
    
    