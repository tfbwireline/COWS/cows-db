USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_IntlCostReport]    Script Date: 06/14/2018 04:03:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


--=============================================
--Author:		<Author,,Name>
--Create date: <Create Date,,>
--Description:	<Description,,>
-- 
--					QueryNumber: 0 = Manually specify date: 'mm/dd/yyyy'
--								 1 = Monthly
-- Enter date of month needed for report.
--EXEC RAD_Reporting.rad.sp_ReportRefreshQueue_NEW 'N','103'----NEW -- International Cost Report
--  EXEC COWS_Reporting.cows.dbo.sp_IntlCostReport,0,01/02/2013
-- ci554013 3/28/2013 changed ACCS_IN_MB_VALU_QTY to ACCS_MB_DES.
-- ci554013 2/17/2014 added 2 new columns
--		ACCS_CUST_MRC_IN_USD_AMT
--		ACCS_CUST_NRC_IN_USD_AMT
-- ci554013 - 6/19/2014 - added 4 columns at end of report: [NUA Number]
--                 [H4 Customer ID], [GSL#], [Private Line Number]
-- dlp0278 - 7/21/2014 - Added Zipcode.
-- vn370313 - 09142017  - Added H1 ID and Prequel Number removed  RFQ  NUmber and GSL Number
--[dbo].[sp_IntlCostReport] 1
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- Updated By:   Md M Monir
-- Updated Date: 06/13/2018
-- Updated Reason: For TTRPT_ACCS_Type_CD, use the record which has line_item_cd='ACS'
--                 For TTRPT_SPD_OFSRVC_BDWD_DES, use the record with line_item_cd='PRT'
--=============================================
ALTER PROCEDURE [dbo].[sp_IntlCostReport] 
		@QueryNumber	INT = 0,
		@dateStr  		VARCHAR(10)= ''

AS

BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @Year		INT 
	DECLARE @Month		INT 
	--DECLARE @dateStr  		VARCHAR(10)= '12/31/2016' --for testing only
	--DECLARE @QueryNumber	INT = 1   --for testing only

	IF @QueryNumber=0
		BEGIN

			SET @Year=DATEPART(YEAR,@dateStr)
			SET @Month=DATEPART(MONTH,@dateStr)
		END		
	ELSE IF @QueryNumber=1
    	BEGIN  
			SET @Year=DATEPART(YEAR, GETDATE())
			SET @Month=DATEPART(MONTH, GETDATE() - 1)  
		END

	--SET @Year=2018
	--SET @Month=05
    --PRINT @Year
	--PRINT @Month

OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

Select	odr.ORDR_ID                [ORDR ID],
		ISNULL(fsa.FTN,convert(varchar(50), odr.ORDR_ID))   [FTN], 
		ISNULL(lxr.RGN_DES,'')        [XNCI Region],
		ISNULL(ckt.VNDR_CKT_ID,'')    [Vendor Circuit Id], 
		/* ISNULL(fsa.TSUP_RFQ_NBR,'')   [RFQNo],*/  /*vn370313  09142017 Mayberry, Debbie L [CTO] */
		lot.ORDR_TYPE_DES	            [Order Type],
		lost.ORDR_SUB_TYPE_DES	      [Order Sub Type],
		lpt.PROD_TYPE_DES			      [Product Type],
		/*
		--foch4.CUST_ID				      [H4],
		--CONVERT(varchar, DecryptByKey(foch4.CUST_NME)) [H4 CUST NAME],*/
		h5.CUST_ID	                  [H5],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE h5.CUST_NME END AS [H5 CUST NAME],
		CASE odr.ORDR_CAT_ID 
			WHEN 1	THEN ISNULL(lv.CXR_CD,lvvo.VNDR_NME)
			WHEN 2 THEN CASE ISNULL(odr.PLTFRM_CD,'')
							WHEN 'SF' THEN ISNULL(sf.VNDR_NME,lvvo.VNDR_NME)
							WHEN 'CP' THEN ISNULL(cp.VNDR_NME,lvvo.VNDR_NME)
							WHEN 'RS' THEN ISNULL(rs.VNDR_NME,lvvo.VNDR_NME)
							ELSE lvvo.VNDR_NME
						END
			WHEN 4  THEN ISNULL(lvncco.VNDR_NME,lvvo.VNDR_NME)
			WHEN 6 THEN CASE ISNULL(odr.PLTFRM_CD,'')
							WHEN 'SF' THEN ISNULL(sf.VNDR_NME,lvvo.VNDR_NME)
							WHEN 'CP' THEN ISNULL(cp.VNDR_NME,lvvo.VNDR_NME)
							WHEN 'RS' THEN ISNULL(rs.VNDR_NME,lvvo.VNDR_NME)
							ELSE lvvo.VNDR_NME
						END
			ELSE lvvo.VNDR_NME
		END               [VNDR NME],
		/*--cows.web.getVendorName(odr.ordr_ID)	[VNDR NME],*/ 
		CASE odr.ORDR_CAT_ID 
			WHEN 1	THEN ISNULL(lv.CXR_CD,vf.VNDR_CD)
			WHEN 2 THEN CASE ISNULL(odr.PLTFRM_CD,'')
							WHEN 'SF' THEN ISNULL(sf.VNDR_CD,vf.VNDR_CD)
							WHEN 'cp' THEN ISNULL(cp.VNDR_CD,vf.VNDR_CD)
							WHEN 'rs' THEN ISNULL(rs.VNDR_CD,vf.VNDR_CD)
							ELSE vf.VNDR_CD
						END
			WHEN 4  THEN ISNULL(lvncco.VNDR_CD,vf.VNDR_CD)
			WHEN 6 THEN CASE ISNULL(odr.PLTFRM_CD,'')
							WHEN 'SF' THEN ISNULL(sf.VNDR_CD,vf.VNDR_CD)
							WHEN 'cp' THEN ISNULL(cp.VNDR_CD,vf.VNDR_CD)
							WHEN 'rs' THEN ISNULL(rs.VNDR_CD,vf.VNDR_CD)
							ELSE vf.VNDR_CD
						END
			ELSE vf.VNDR_CD
		END               [VNDR CD],
		ISNULL(fcpl2.TTRPT_SPD_OF_SRVC_BDWD_DES,ISNULL(ipl.CKT_SPD_QTY,'')) [Speed of Service],
		todr.ACCS_MB_DES	            [ACCESS MB Value], 
		lacs.ACCS_CTY_NME_SITE_CD	   [ACCESS CITY NAME SITE CD], 
		ISNULL(fcpl.TTRPT_ACCS_TYPE_CD,'') [ACCESS TYPE],
		--CONVERT(varchar, DecryptByKey(oadr.STREET_ADR_1)) [CUST ADDR LINE 1],
		--CONVERT(varchar, DecryptByKey(oadr.STREET_ADR_2)) [CUST ADDR LINE 2],
		--CONVERT(varchar, DecryptByKey(oadr.CTY_NME))		 [CUST CITY],
		--CONVERT(varchar, DecryptByKey(oadr.STT_CD))		 [CUST STATE],
		--CONVERT(varchar, DecryptByKey(oadr.ZIP_PSTL_CD))     [ZIPCODE],
		--CONVERT(varchar, DecryptByKey(oadr.PRVN_NME))	 [CUST PROVINCE],
		
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.STREET_ADR_1 END AS [CUST ADDR LINE 1],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.STREET_ADR_2 END AS [CUST ADDR LINE 2],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.CTY_NME END AS [CUST CITY],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.STT_CD END AS [CUST STATE],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.ZIP_PSTL_CD END AS [ZIPCODE],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.PRVN_NME END AS [CUST PROVINCE],
		
		lctry.CTRY_NME							[CUST COUNTRY],
		fsa.TTRPT_ACCS_CNTRC_TERM_CD		[CUST CONTRACT TERM], 
		cktms.CNTRC_TERM_ID				   [VENDOR CONTRACT TERM], 
		cktms.ACCS_ACPTC_DT					[ACCESS ACCEPTED DATE], 
		DATENAME(MONTH,cktms.ACCS_ACPTC_DT)	[INSTALL MONTH],
		ordMS.XNCI_CNFRM_RNL_DT				[CONFIRM RENEWAL DATE],
		cktms.VNDR_CNFRM_DSCNCT_DT			[VENDOR CONFIRM DISCONNECT DATE],
		DATENAME(MONTH,cktms.VNDR_CNFRM_DSCNCT_DT) [DISCONNECT MONTH],
		lccost.CUR_NME							[LOCAL CURRENCY],
		ccost.VNDR_MRC_AMT               [VENDOR COST MRC CURRENCY], 
		ccost.VNDR_NRC_AMT               [VENDOR COST NRC CURRENCY], 
		ccost.VNDR_MRC_IN_USD_AMT        [VNDR MRC IN USD AMT],
		ccost.VNDR_NRC_IN_USD_AMT        [VNDR NRC IN USD AMT],
		ccost.ACCS_CUST_MRC_IN_USD_AMT   [Access MRC IN USD AMT],
		ccost.ACCS_CUST_NRC_IN_USD_AMT   [Access NRC IN USD AMT],		
		NULL                       [Additonal Cost Description],
		NULL                       [Addtional Cost NRC Approved],
		NULL                       [Addtional Cost Tax],
		NULL                       [Additional Cost Currency],
		NULL                       [Additional Cost USD],
		ISNULL(STUFF((SELECT ',' + nck.NUA_ADR from cows.dbo.NRM_CKT nck
			WHERE fsa.FTN = nck.FTN 
			FOR XML PATH('')),1,1,''),nsi.NUA_ADR)		[NUA Number],
		foch4.CUST_ID								[H4 Customer ID],
		/*ISNULL(fsa.TSUP_PRS_QOT_NBR,'')				[GSL#], */		 /*vn370313  09142017 Mayberry, Debbie L [CTO] */
		ISNULL(foch1.CUST_ID,'') as [H1_ID],
		ISNULL(fsa.TPORT_PREQUAL_LINE_ITEM_ID,'') as [PreQual Number], 
		ISNULL(STUFF((SELECT ',' + nck.PLN_NME from cows.dbo.NRM_CKT nck
			WHERE fsa.FTN = nck.FTN 
			FOR XML PATH('')),1,1,''),'')			[Private Line Number]
		
	From	cows.dbo.ORDR odr with (nolock)
			left join	cows.dbo.FSA_ORDR fsa with (nolock) on odr.ordr_id = fsa.ordr_id
			LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock)  on fcpl.ORDR_ID = fsa.ORDR_ID AND fcpl.line_item_cd='ACS'
			LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl2 with (nolock) on fcpl2.ORDR_ID = fsa.ORDR_ID AND fcpl2.line_item_cd='PRT'
			left join	cows.dbo.ipl_ordr ipl with (nolock) on odr.ordr_id = ipl.ordr_id
			left join	cows.dbo.NCCO_ORDR ncco with (nolock) on ncco.ORDR_ID = odr.ORDR_ID
			left join	cows.dbo.LK_XNCI_RGN lxr with (nolock) on lxr.RGN_ID = odr.RGN_ID
			left join	cows.dbo.H5_FOLDR h5 with (nolock) on h5.H5_FOLDR_ID = odr.H5_FOLDR_ID
			inner join	cows.dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = odr.PPRT_ID
			inner join	cows.dbo.lk_prod_type lpt with (nolock) on lpt.PROD_TYPE_ID = lp.PROD_TYPE_ID
			inner join	cows.dbo.LK_ORDR_TYPE lot with (nolock) on lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
			left join	cows.dbo.LK_ORDR_SUB_TYPE lost with (nolock) on fsa.ORDR_SUB_TYPE_CD = lost.ORDR_SUB_TYPE_CD
			left join	cows.dbo.FSA_ORDR_CUST foch4 with (nolock) on		foch4.ORDR_ID = odr.ORDR_ID
																	and foch4.CIS_LVL_TYPE = 'H4'
			left join	cows.dbo.FSA_ORDR_CUST foch5es with (nolock) on		foch5es.ORDR_ID = odr.ORDR_ID
																	and foch5es.CIS_LVL_TYPE in ('H5','H6')
			left join	cows.dbo.FSA_ORDR_CUST foch1 with (nolock) on		foch1.ORDR_ID = odr.ORDR_ID
																	and foch1.CIS_LVL_TYPE = 'H1'
			LEFT JOIN	cows.dbo.IPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	ipl.ORDR_ID		=	la.ORDR_ID
																		AND	la.ACCS_TYPE_ID	=	'Terminating'
			LEFT JOIN	cows.dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD														
			LEFT JOIN	cows.dbo.LK_VNDR		sf	WITH (NOLOCK)			ON	sf.VNDR_CD		=	fcpl.CXR_ACCS_CD
			LEFT JOIN	cows.dbo.LK_VNDR		cp	WITH (NOLOCK)			ON	cp.VNDR_CD		=	fsa.VNDR_VPN_CD
			LEFT JOIN	cows.dbo.LK_VNDR		rs	WITH (NOLOCK)			ON	rs.VNDR_CD		=	fsa.INSTL_VNDR_CD
			LEFT JOIN	cows.dbo.LK_VNDR		lvncco	WITH (NOLOCK)		ON	lvncco.VNDR_CD	=	ncco.VNDR_CD
			LEFT JOIN	cows.dbo.CKT			ckt WITH (NOLOCK)			ON	ckt.ORDR_ID		=	odr.ORDR_ID
			LEFT JOIN	cows.dbo.TRPT_ORDR	todr WITH (NOLOCK)			ON	todr.ORDR_ID	=	odr.ORDR_ID
			LEFT JOIN	cows.dbo.ORDR_ADR	oadr WITH (NOLOCK)			ON	oadr.ORDR_ID	=	odr.ORDR_ID
																	AND	oadr.CIS_LVL_TYPE	in ('H5', 'H6')
			LEFT JOIN	(SELECT CKT_ID,MAX(VER_ID) AS VER_ID FROM cows.dbo.CKT_MS WITH (NOLOCK) GROUP BY CKT_ID) cmsmax ON cmsmax.CKT_ID = ckt.CKT_ID														
			LEFT JOIN	cows.dbo.CKT_MS		cktms WITH (NOLOCK)			ON	cktms.CKT_ID	=	cmsmax.CKT_ID AND cktms.VER_ID = cmsmax.VER_ID
			LEFT JOIN	cows.dbo.ORDR_MS		ordMS WITH (NOLOCK)			ON ordMS.ORDR_ID	=	odr.ORDR_ID
			LEFT JOIN	(SELECT CKT_ID,MAX(VER_ID) AS VER_ID FROM cows.dbo.CKT_COST WITH (NOLOCK) GROUP BY CKT_ID) ccmax ON ccmax.CKT_ID = ckt.CKT_ID														
			LEFT JOIN	cows.dbo.CKT_COST	ccost WITH (NOLOCK)			ON ccost.CKT_ID		=	ccmax.CKT_ID  AND ccmax.VER_ID = ccost.VER_ID
			LEFT JOIN	cows.dbo.LK_CUR	lccost WITH (NOLOCK)			ON lccost.CUR_ID	=	ccost.VNDR_CUR_ID
			LEFT JOIN	cows.dbo.LK_CTRY lctry WITH (NOLOCK)				ON lctry.CTRY_CD	=	oadr.CTRY_CD
			INNER JOIN	cows.dbo.ACT_TASK at WITH (NOLOCK)				ON at.ORDR_ID		=	odr.ORDR_ID
																	AND	at.TASK_ID		=	1001
																	AND	at.STUS_ID		=	0
			LEFT JOIN	cows.dbo.VNDR_ORDR vo WITH (NOLOCK)				ON vo.ORDR_ID		=	odr.ORDR_ID
																	AND vo.TRMTG_CD		=	ckt.TRMTG_CD
			LEFT JOIN	cows.dbo.VNDR_FOLDR vf WITH (NOLOCK)				ON vo.VNDR_FOLDR_ID	=	vf.VNDR_FOLDR_ID
			LEFT JOIN	cows.dbo.LK_VNDR	lvvo WITH (NOLOCK)				ON lvvo.VNDR_CD		=	vf.VNDR_CD
			LEFT JOIN	cows.dbo.LK_ACCS_CTY_SITE lacs WITH (NOLOCK)		ON lacs.ACCS_CTY_SITE_ID = todr.ACCS_CTY_SITE_ID
			LEFT JOIN	(SELECT nn.* FROM cows.dbo.NRM_SRVC_INSTC nn WITH (NOLOCK)
					WHERE nn.NRM_SRVC_INSTC_ID  = (SELECT MAX(NRM_SRVC_INSTC_ID)
					FROM (SELECT NRM_SRVC_INSTC_ID, FTN FROM cows.dbo.NRM_SRVC_INSTC WITH (NOLOCK) ) nrm 
					WHERE nrm.FTN = nn.FTN))  nsi  ON fsa.ftn = nsi.ftn


	WHERE DATEPART(YEAR,at.CREAT_DT) = @Year
		AND DATEPART(MONTH,at.CREAT_DT) = @Month	
		AND lot.ORDR_TYPE_ID 	<>	8					
		AND lpt.TRPT_CD			=	'1'
		AND odr.DMSTC_CD		=	'1'

UNION 

Select	odr.ORDR_ID                [ORDR ID],
		ISNULL(fsa.FTN,odr.ORDR_ID)   [FTN], 
		ISNULL(lxr.RGN_DES,'')        [XNCI Region],
		ISNULL(ckt.VNDR_CKT_ID,'')    [Vendor Circuit Id], 
		/*ISNULL(fsa.TSUP_RFQ_NBR,'')   [RFQNo],*/ /*vn370313  09142017 Mayberry, Debbie L [CTO] */
		lot.ORDR_TYPE_DES	            [Order Type],
		lost.ORDR_SUB_TYPE_DES	      [Order Sub Type],
		lpt.PROD_TYPE_DES			      [Product Type],
		/*
		foch4.CUST_ID				      [H4],
		CONVERT(varchar, DecryptByKey(foch4.CUST_NME)) [H4 CUST NAME],
		*/
		h5.CUST_ID	                  [H5],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE h5.CUST_NME END AS [H5 CUST NAME],
		CASE odr.ORDR_CAT_ID 
			WHEN 1	THEN ISNULL(lv.CXR_CD,lvvo.VNDR_NME)
			WHEN 2 THEN CASE ISNULL(odr.PLTFRM_CD,'')
							WHEN 'SF' THEN ISNULL(sf.VNDR_NME,lvvo.VNDR_NME)
							WHEN 'CP' THEN ISNULL(cp.VNDR_NME,lvvo.VNDR_NME)
							WHEN 'RS' THEN ISNULL(rs.VNDR_NME,lvvo.VNDR_NME)
							ELSE lvvo.VNDR_NME
						END
			WHEN 4  THEN ISNULL(lvncco.VNDR_NME,lvvo.VNDR_NME)
			WHEN 6 THEN CASE ISNULL(odr.PLTFRM_CD,'')
							WHEN 'SF' THEN ISNULL(sf.VNDR_NME,lvvo.VNDR_NME)
							WHEN 'CP' THEN ISNULL(cp.VNDR_NME,lvvo.VNDR_NME)
							WHEN 'RS' THEN ISNULL(rs.VNDR_NME,lvvo.VNDR_NME)
							ELSE lvvo.VNDR_NME
						END
			ELSE lvvo.VNDR_NME
		END               [VNDR NME],
		/*--cows.web.getVendorName(odr.ordr_ID)	[VNDR NME], */
		CASE odr.ORDR_CAT_ID 
			WHEN 1	THEN ISNULL(lv.CXR_CD,vf.VNDR_CD)
			WHEN 2 THEN CASE ISNULL(odr.PLTFRM_CD,'')
							WHEN 'SF' THEN ISNULL(sf.VNDR_CD,vf.VNDR_CD)
							WHEN 'cp' THEN ISNULL(cp.VNDR_CD,vf.VNDR_CD)
							WHEN 'rs' THEN ISNULL(rs.VNDR_CD,vf.VNDR_CD)
							ELSE vf.VNDR_CD
						END
			WHEN 4  THEN ISNULL(lvncco.VNDR_CD,vf.VNDR_CD)
			WHEN 6 THEN CASE ISNULL(odr.PLTFRM_CD,'')
							WHEN 'SF' THEN ISNULL(sf.VNDR_CD,vf.VNDR_CD)
							WHEN 'cp' THEN ISNULL(cp.VNDR_CD,vf.VNDR_CD)
							WHEN 'rs' THEN ISNULL(rs.VNDR_CD,vf.VNDR_CD)
							ELSE vf.VNDR_CD
						END
			ELSE vf.VNDR_CD
		END               [VNDR CD],
		ISNULL(fcpl2.TTRPT_SPD_OF_SRVC_BDWD_DES,ISNULL(ipl.CKT_SPD_QTY,'')) [Speed of Service],
		todr.ACCS_MB_DES	            [ACCESS MB Value], 
		lacs.ACCS_CTY_NME_SITE_CD	   [ACCESS CITY NAME SITE CD], 
		ISNULL(fcpl.TTRPT_ACCS_TYPE_CD,'') [ACCESS TYPE],
		
		--CONVERT(varchar, DecryptByKey(oadr.STREET_ADR_1)) [CUST ADDR LINE 1],
		--CONVERT(varchar, DecryptByKey(oadr.STREET_ADR_2)) [CUST ADDR LINE 2],
		--CONVERT(varchar, DecryptByKey(oadr.CTY_NME))		 [CUST CITY],
		--CONVERT(varchar, DecryptByKey(oadr.STT_CD))		 [CUST STATE],
		--CONVERT(varchar, DecryptByKey(oadr.ZIP_PSTL_CD))     [ZIPCODE],
		--CONVERT(varchar, DecryptByKey(oadr.PRVN_NME))	 [CUST PROVINCE],
		
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.STREET_ADR_1 END AS [CUST ADDR LINE 1],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.STREET_ADR_2 END AS [CUST ADDR LINE 2],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.CTY_NME END AS [CUST CITY],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.STT_CD END AS [CUST STATE],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.ZIP_PSTL_CD END AS [ZIPCODE],
		CASE WHEN odr.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' ELSE oadr.PRVN_NME END AS [CUST PROVINCE],
		
		lctry.CTRY_NME							[CUST COUNTRY],
		fsa.TTRPT_ACCS_CNTRC_TERM_CD		[CUST CONTRACT TERM], 
		cktms.CNTRC_TERM_ID				   [VENDOR CONTRACT TERM], 
		cktms.ACCS_ACPTC_DT					[ACCESS ACCEPTED DATE], 
		DATENAME(MONTH,cktms.ACCS_ACPTC_DT)	[INSTALL MONTH],
		ordMS.XNCI_CNFRM_RNL_DT				[CONFIRM RENEWAL DATE],
		cktms.VNDR_CNFRM_DSCNCT_DT			[VENDOR CONFIRM DISCONNECT DATE],
		DATENAME(MONTH,cktms.VNDR_CNFRM_DSCNCT_DT) [DISCONNECT MONTH],
		NULL 						   [LOCAL CURRENCY],
		NULL                    [VENDOR COST MRC CURRENCY], 
		NULL                    [VENDOR COST NRC CURRENCY],  
		NULL                    [VNDR MRC IN USD AMT],
		NULL                    [VNDR NRC IN USD AMT],
		NULL					[Access MRC IN USD AMT],
		NULL					[Access NRC IN USD AMT],
		lcct.CKT_CHG_TYPE_DES   [Additonal Cost Description],
		oaddl.CHG_NRC_AMT       [Addtional Cost NRC Approved],
		oaddl.TAX_RT_PCT_QTY    [Addtional Cost Tax],
		lcaddl.CUR_NME          [Additional Cost Currency],
		oaddl.CHG_NRC_IN_USD_AMT    [Additional Cost USD],
		ISNULL(STUFF((SELECT ',' + nck.NUA_ADR from cows.dbo.NRM_CKT nck
			WHERE fsa.FTN = nck.FTN 
			FOR XML PATH('')),1,1,''),nsi.NUA_ADR)		[NUA Number],
		foch4.CUST_ID								[H4 Customer ID],
		/*--ISNULL(fsa.TSUP_PRS_QOT_NBR,'')				[GSL#], */	 /*vn370313  09142017 Mayberry, Debbie L [CTO] */
		ISNULL(foch1.CUST_ID,'') as [H1_ID],
		ISNULL(fsa.TPORT_PREQUAL_LINE_ITEM_ID,'') as [PreQual Number],	
		ISNULL(STUFF((SELECT ',' + nck.PLN_NME from cows.dbo.NRM_CKT nck
			WHERE fsa.FTN = nck.FTN 
			FOR XML PATH('')),1,1,''),'')			[Private Line Number]
		
		
	From	cows.dbo.ORDR odr with (nolock)
			left join	cows.dbo.FSA_ORDR fsa with (nolock) on odr.ordr_id = fsa.ordr_id
			LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock)  on fcpl.ORDR_ID = fsa.ORDR_ID AND fcpl.line_item_cd='ACS'
			LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl2 with (nolock) on fcpl2.ORDR_ID = fsa.ORDR_ID AND fcpl2.line_item_cd='PRT'
			left join	cows.dbo.ipl_ordr ipl with (nolock) on odr.ordr_id = ipl.ordr_id
			left join	cows.dbo.NCCO_ORDR ncco with (nolock) on ncco.ORDR_ID = odr.ORDR_ID
			left join	cows.dbo.LK_XNCI_RGN lxr with (nolock) on lxr.RGN_ID = odr.RGN_ID
			left join	cows.dbo.H5_FOLDR h5 with (nolock) on h5.H5_FOLDR_ID = odr.H5_FOLDR_ID
			inner join	cows.dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = odr.PPRT_ID
			inner join	cows.dbo.lk_prod_type lpt with (nolock) on lpt.PROD_TYPE_ID = lp.PROD_TYPE_ID
			inner join	cows.dbo.LK_ORDR_TYPE lot with (nolock) on lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID
			left join	cows.dbo.LK_ORDR_SUB_TYPE lost with (nolock) on fsa.ORDR_SUB_TYPE_CD = lost.ORDR_SUB_TYPE_CD
			left join	cows.dbo.FSA_ORDR_CUST foch4 with (nolock) on		foch4.ORDR_ID = odr.ORDR_ID
																	and foch4.CIS_LVL_TYPE = 'H4'
			left join	cows.dbo.FSA_ORDR_CUST foch5es with (nolock) on		foch5es.ORDR_ID = odr.ORDR_ID
																	and foch5es.CIS_LVL_TYPE in ('H5','H6')
			left join	cows.dbo.FSA_ORDR_CUST foch1 with (nolock) on		foch1.ORDR_ID = odr.ORDR_ID
																	and foch1.CIS_LVL_TYPE = 'H1'
			LEFT JOIN	cows.dbo.IPL_ORDR_ACCS_INFO	la	WITH (NOLOCK)	ON	ipl.ORDR_ID		=	la.ORDR_ID
																		AND	la.ACCS_TYPE_ID	=	'Terminating'
			LEFT JOIN	cows.dbo.LK_FRGN_CXR			lv	WITH (NOLOCK)	ON	la.CXR_CD		=	lv.CXR_CD														
			LEFT JOIN	cows.dbo.LK_VNDR		sf	WITH (NOLOCK)			ON	sf.VNDR_CD		=	fcpl.CXR_ACCS_CD
			LEFT JOIN	cows.dbo.LK_VNDR		cp	WITH (NOLOCK)			ON	cp.VNDR_CD		=	fsa.VNDR_VPN_CD
			LEFT JOIN	cows.dbo.LK_VNDR		rs	WITH (NOLOCK)			ON	rs.VNDR_CD		=	fsa.INSTL_VNDR_CD
			LEFT JOIN	cows.dbo.LK_VNDR		lvncco	WITH (NOLOCK)		ON	lvncco.VNDR_CD	=	ncco.VNDR_CD
			LEFT JOIN	cows.dbo.CKT			ckt WITH (NOLOCK)			ON	ckt.ORDR_ID		=	odr.ORDR_ID
			LEFT JOIN	cows.dbo.TRPT_ORDR	todr WITH (NOLOCK)			ON	todr.ORDR_ID	=	odr.ORDR_ID
			LEFT JOIN	cows.dbo.ORDR_ADR	oadr WITH (NOLOCK)			ON	oadr.ORDR_ID	=	odr.ORDR_ID
																	AND	oadr.CIS_LVL_TYPE	in ('H5','H6')
			LEFT JOIN	(SELECT CKT_ID,MAX(VER_ID) AS VER_ID FROM cows.dbo.CKT_MS WITH (NOLOCK) GROUP BY CKT_ID) cmsmax ON cmsmax.CKT_ID = ckt.CKT_ID														
			LEFT JOIN	cows.dbo.CKT_MS		cktms WITH (NOLOCK)			ON	cktms.CKT_ID	=	cmsmax.CKT_ID AND cktms.VER_ID = cmsmax.VER_ID
			LEFT JOIN	cows.dbo.ORDR_MS		ordMS WITH (NOLOCK)			ON ordMS.ORDR_ID	=	odr.ORDR_ID
			LEFT JOIN	(SELECT ORDR_ID,CKT_CHG_TYPE_ID,MAX(VER_ID) AS VER_ID 
							FROM cows.dbo.ORDR_CKT_CHG occ WITH (NOLOCK) 
							GROUP BY ORDR_ID,CKT_CHG_TYPE_ID) occm	ON occm.ORDR_ID = odr.ORDR_ID 
			LEFT JOIN	cows.dbo.ORDR_CKT_CHG oaddl WITH (NOLOCK)		ON	oaddl.ORDR_ID	=	occm.ORDR_ID
																	AND	oaddl.CKT_CHG_TYPE_ID = occm.CKT_CHG_TYPE_ID
																	AND oaddl.VER_ID	=	occm.VER_ID	
			LEFT JOIN	cows.dbo.LK_CKT_CHG_TYPE  lcct WITH (NOLOCK)		ON lcct.CKT_CHG_TYPE_ID = oaddl.CKT_CHG_TYPE_ID
			LEFT JOIN	cows.dbo.LK_CUR	lcaddl WITH (NOLOCK)			ON lcaddl.CUR_ID	=	oaddl.CHG_CUR_ID
			LEFT JOIN	cows.dbo.LK_CTRY lctry WITH (NOLOCK)				ON lctry.CTRY_CD	=	oadr.CTRY_CD
			INNER JOIN	cows.dbo.ACT_TASK at WITH (NOLOCK)				ON at.ORDR_ID		=	odr.ORDR_ID
																	AND	at.TASK_ID		=	1001
																	AND	at.STUS_ID		=	0
			LEFT JOIN	cows.dbo.VNDR_ORDR vo WITH (NOLOCK)				ON vo.ORDR_ID		=	odr.ORDR_ID
																	AND vo.TRMTG_CD		=	ckt.TRMTG_CD
			LEFT JOIN	cows.dbo.VNDR_FOLDR vf WITH (NOLOCK)				ON vo.VNDR_FOLDR_ID	=	vf.VNDR_FOLDR_ID
			LEFT JOIN	cows.dbo.LK_VNDR	lvvo WITH (NOLOCK)				ON lvvo.VNDR_CD		=	vf.VNDR_CD
			LEFT JOIN	cows.dbo.LK_ACCS_CTY_SITE lacs WITH (NOLOCK)		ON lacs.ACCS_CTY_SITE_ID = todr.ACCS_CTY_SITE_ID
			LEFT JOIN	(SELECT nn.* FROM cows.dbo.NRM_SRVC_INSTC nn WITH (NOLOCK)
					WHERE nn.NRM_SRVC_INSTC_ID  = (SELECT MAX(NRM_SRVC_INSTC_ID)
					FROM (SELECT NRM_SRVC_INSTC_ID, FTN FROM cows.dbo.NRM_SRVC_INSTC WITH (NOLOCK) ) nrm 
					WHERE nrm.FTN = nn.FTN))  nsi  ON fsa.ftn = nsi.ftn


WHERE DATEPART(YEAR,at.CREAT_DT) = @Year
	AND DATEPART(MONTH,at.CREAT_DT) = @Month							
	AND lot.ORDR_TYPE_ID 	<>	8
	AND lpt.TRPT_CD		=	'1'
	AND odr.DMSTC_CD	=	'1'
	AND EXISTS
		(
			SELECT 'X'
				FROM	cows.dbo.ORDR_CKT_CHG occhg WITH (NOLOCK)
				WHERE	occhg.ORDR_ID = odr.ORDR_ID
					AND	occhg.SALS_STUS_ID = 30
		)

	CLOSE SYMMETRIC KEY FS@K3y 
RETURN 0; 
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_IntlCostReport ' + ',' + CONVERT(VARCHAR(10), @dateStr, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH	





GO


