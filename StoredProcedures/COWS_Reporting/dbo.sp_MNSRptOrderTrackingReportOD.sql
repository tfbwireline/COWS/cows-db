USE [COWS_Reporting]
GO
/*
--===============================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			07/21/2011
-- Description:		
--					Extract orders that need to be reviewed by the MNS PM for
--					specified parameter(s);
--					Customer Name, H5_H6_ID, SecuredUser, Masked Sensitive Data
--
--					TASK_ID: 
--						300 = New Order Review
--						500 = SS RTS Pending
--						1000 = Bill Activation Ready
-- Notes:
--					Is Date Range applicable ?
--
-- 11/30/2011 sxv0766: 	Removed @h1 param, COWS Event ID & COWS Event Status columns from report.
--
-- 11/05/2011 sxv0766:	Removed Order Submitter/Keyer per MNS Team request.
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--===============================================================================================
*/
ALTER Procedure [dbo].[sp_MNSRptOrderTrackingReportOD] 
	@SecuredUser  INT=0,   
	 @getSensData  CHAR(1)='N',   
	 @startDate   Datetime=NULL,  
	 @endDate   Datetime=NULL	
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDateStr	VARCHAR(20)
	DECLARE @endDateStr		VARCHAR(20)
	
	DECLARE @runDateStr		VARCHAR(20)

	DECLARE @aSQL			VARCHAR(5000)
	
	IF OBJECT_ID('tempdb..#tmp_ord_trk', 'U') IS NOT NULL	
	drop table #tmp_ord_trk;
	CREATE TABLE #tmp_ord_trk
	(
		Cust_Name				VARCHAR(100),
		Cust_Name2				VARCHAR(100),
		Scurd_Cd				Bit,
		Order_FTN				VARCHAR(50),
		Order_Status			VARCHAR(50),
		H5_H6_ID				INT,
		CCD						VARCHAR(10),
		CCD_Priority			CHAR(1),		
		Order_Submitter			VARCHAR(200),
		Assigned_MNS_PM			VARCHAR(100)
	)
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SET @aSQL='INSERT INTO #tmp_ord_trk(Cust_Name,Scurd_Cd,Order_FTN,Order_Status,H5_H6_ID,CCD,CCD_Priority,Assigned_MNS_PM)
			SELECT	
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE ordr.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,
			ford.FTN , 	 		
 			CASE  				
 				WHEN atsk.TASK_ID = 1000 THEN ''Bill Activation - Pending''
 				WHEN atsk.TASK_ID = 500 THEN ''Return To Sales''
 				WHEN atsk.TASK_ID = 300 THEN ''New Order Review Pending'' END ,			
			ordr.H5_H6_CUST_ID ,
			CASE
 				WHEN ordr.CUST_CMMT_DT is not NULL THEN
				REPLACE(LEFT(CONVERT(VARCHAR(10),ordr.CUST_CMMT_DT,101),1), ''0'', '''') +
				SUBSTRING(CONVERT(VARCHAR(10),ordr.CUST_CMMT_DT,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ordr.CUST_CMMT_DT,101), 4, 1), ''0'', '''') +
				RIGHT(CONVERT(VARCHAR(10), ordr.CUST_CMMT_DT, 101), 6) 
			ELSE REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101),1), ''0'', '''') +
				SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 2, 2) +
				REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 4, 1), ''0'', '''') +
				RIGHT(CONVERT(VARCHAR(10), ford.CUST_CMMT_DT, 101), 6) END ,			
			CASE 
				WHEN ordr.CUST_CMMT_DT is not NULL THEN
 					CASE 
 						WHEN DATEDIFF(day, ordr.CUST_CMMT_DT, GETDATE()) >= 31 THEN 1  
 						WHEN (DATEDIFF(day, ordr.CUST_CMMT_DT, GETDATE()) >= 5  
 						AND DATEDIFF(day, ordr.CUST_CMMT_DT, GETDATE() ) < 31) THEN 2 END			
			 	WHEN ordr.CUST_CMMT_DT is NULL THEN
 					CASE 
 						WHEN DATEDIFF(day, ford.CUST_CMMT_DT, GETDATE()) >= 31 THEN 1  
 						WHEN (DATEDIFF(day, ford.CUST_CMMT_DT, GETDATE()) >= 5  
 						AND DATEDIFF(day, ford.CUST_CMMT_DT, GETDATE() ) < 31) THEN 2 END END , 			
			CASE 
				WHEN luser2.DSPL_NME IS NULL THEN '''' 
				ELSE luser2.DSPL_NME END  
		FROM COWS.dbo.ORDR ordr with (nolock) 		
		JOIN COWS.dbo.FSA_ORDR ford with (nolock) ON ordr.ORDR_ID = ford.ORDR_ID
		JOIN COWS.dbo.ACT_TASK atsk with (nolock) ON ordr.ORDR_ID = atsk.ORDR_ID
		LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust with (nolock) ON ford.ORDR_ID =	fcust.ORDR_ID AND fcust.CIS_LVL_TYPE = ''H1''
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=fcust.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
		LEFT JOIN (SELECT distinct ORDR_ID, ASN_USER_ID 
					FROM COWS.dbo.USER_WFM_ASMT with (nolock)
					WHERE GRP_ID = 2 ) as uwasm ON ford.ORDR_ID = uwasm.ORDR_ID					
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON uwasm.ASN_USER_ID = luser2.USER_ID			
	WHERE atsk.TASK_ID in (300, 500, 1000) AND atsk.STUS_ID = 0 ' 

				
	IF @startDate is NOT NULL
	BEGIN
		-- Do the conversion before passing them to @aSQL
		SET @runDateStr = @startDate
		
		--SET @startDateStr = @startDate
		--SET @endDateStr = @endDate
			
		--SET @aSQL=@aSQL + 'AND mds.STRT_TMST >= ''' + @startDateStr + ''' AND mds.STRT_TMST <= DATEADD(day, 1, ''' + @endDateStr + ''')' 
		--SET @aSQL=@aSQL + 'AND ford.ORDR_SBMT_DT >= ''' + @startDateStr + ''' AND ford.ORDR_SBMT_DT < DATEADD(day, 1, ''' + @endDateStr + ''')' 
		
		SET @aSQL=@aSQL + 'AND (DATEDIFF(day, ordr.CUST_CMMT_DT,''' + @runDateStr + ''') >= 5 OR (ordr.CUST_CMMT_DT is NULL AND DATEDIFF(day, ford.CUST_CMMT_DT, ''' + @runDateStr + ''') >= 5)) '
	END

	IF @endDate is NOT NULL
	BEGIN
		SET @runDateStr = @endDate
		SET @aSQL=@aSQL + 'AND (DATEDIFF(day, ordr.CUST_CMMT_DT,''' + @runDateStr + ''') >= 5 OR (ordr.CUST_CMMT_DT is NULL AND DATEDIFF(day, ford.CUST_CMMT_DT, ''' + @runDateStr + ''') >= 5)) '
	END
	
	EXEC (@aSQL);
	
	--=====================
	
	SELECT  CASE 
				--WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--ELSE Cust_Name 
				WHEN @SecuredUser = 0 and Scurd_Cd =0 THEN CUST_NAME
				WHEN @SecuredUser = 0 and Scurd_Cd >0 AND  CUST_NAME2 <>'' THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd >0 AND CUST_NAME2 <> '' THEN 'Private Customer'
				ELSE COALESCE(CUST_NAME2,CUST_NAME,'')
				END 'Customer Name',
			Order_FTN 'Order FTN #',
			Order_Status 'COWS Order Status',
			H5_H6_ID 'H5/H6 ID',
			CCD,
			CCD_Priority 'CCD Priority',
			--Order_Submitter 'Order Submitter/Keyer',
			Assigned_MNS_PM 'Assigned Acct MNS PM'
	
	FROM #tmp_ord_trk
	
	ORDER BY 'CCD Priority', 'Order FTN #'
	
	---========
	-- Clean Up
	--=========		
	IF OBJECT_ID('tempdb..#tmp_ord_trk', 'U') IS NOT NULL	
		drop table #tmp_ord_trk;	
	
	CLOSE SYMMETRIC KEY FS@K3y;		
		
	RETURN 0;
	
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(300)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptOrderTrackingReportOD, '
	SET @Desc=@Desc + CAST(@SecuredUser AS VARCHAR(2)) + ', '''  + @getSensData + ''', ' 
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ''', '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH





