USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[getReportListByUserID]    Script Date: 05/29/2018 4:20:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
  
-- =========================================================  
-- Author:  jrg7298  
-- Create date: 06/05/2013  
-- Description: Gets the List of Reports using UserID and CSGLevel. 
-- Update By:  Vn370313  
-- Update adte date: 03/15/2018 
-- Description: GUse Rec_STUS=1 
-- =========================================================  
ALTER PROCEDURE [dbo].[getReportListByUserID] --'C', 40, 3  
 @ReportType CHAR(1)  
 ,@UserID  INT  
 ,@CSGLevel TINYINT  
AS  
BEGIN  
SET NOCOUNT ON;  
Begin Try  
  
DECLARE @UserGrp TABLE (GrpID SMALLINT)  
  
DECLARE @Temp TABLE   
(RPT_TYPE_ID INT,  
RPT_NME VARCHAR(500),  
RPT_DES VARCHAR(500),  
REPORTID INT,  
GroupID INT,  
IntlCD CHAR(1),  
ScheduleName VARCHAR(100),  
IntervalDesc VARCHAR(500),  
GRP_NME VARCHAR(20),  
OwnerName VARCHAR(100),  
Encrypted TINYINT,  
FolderTypeCD CHAR(1))  
  
  
INSERT INTO @UserGrp  
SELECT DISTINCT mrg.RPT_GRP_ID  
FROM COWS.dbo.USER_GRP_ROLE ugr WITH (NOLOCK) INNER JOIN  
  COWS.dbo.MAP_RPT_GRP mrg WITH (NOLOCK) ON mrg.GRP_ID = ugr.GRP_ID  
WHERE ugr.[USER_ID] = @UserID  
  AND ugr.[ROLE_ID] = 77  
  AND ugr.REC_STUS_ID=1 /*Added by Vn370313 03/18/2018*/
  
IF (@ReportType = 'C')  
BEGIN  
INSERT INTO @Temp  
SELECT DISTINCT RPTTyp.RPT_TYPE_ID ,RPTTyp.RPT_NME ,RPTTyp.RPT_DES,RPT.REPORTID, RPT.GroupID,  
    CASE WHEN (RSC.ScheduleName LIKE '%Daily%') THEN 'D'   
      WHEN (RSC.ScheduleName LIKE '%Weekly%') THEN 'W'  
      WHEN (RSC.ScheduleName LIKE '%Monthly%') THEN 'M'  
      WHEN (RSC.ScheduleName LIKE '%Quarterly%') THEN 'Q'  
      WHEN (RSC.ScheduleName LIKE '%Yearly%') THEN 'Y' ELSE 'D'  
      END AS IntlCD, RSC.ScheduleName, RSC.[Description] As IntervalDesc, lg.GRP_NME, lu.DSPL_NME as OwnerName, COALESCE(RFW.Encrypted,0) as Encrypted, COALESCE(RFW.FolderTypeCD, 'S') as FolderTypeCD  
   FROM  dbo.LK_RPT_TYPE RPTTyp with (nolock)   
   inner join RAD_Reporting.rad.Report RPT with (nolock) on RPT.ReportName = RPTTyp.RAD_REPORTNAME  
   inner join RAD_Reporting.rad.ReportTemplate RTP with (nolock) on RTP.TemplateGroupID = RPT.TemplateGroupID  
   inner join RAD_Reporting.rad.ReportSchedule RSC with (nolock) on RSC.ScheduleID = RTP.ScheduleID  
   inner join COWS.dbo.LK_GRP lg with (nolock) on lg.GRP_ID = RPT.GroupID  
   inner join COWS.dbo.LK_User lu with (nolock) on lu.USER_ADID = RPTTyp.CREAT_BY_AD_ID  
   left join RAD_Reporting.rad.REPORTFILEWEB_NEW RFW with (nolock) on RFW.REPORTID = RPT.REPORTID  
  where RPTTyp.RPT_TYPE_ID in (  
  select RGSCT.RPT_TYPE_ID from dbo.RPT_GRP_SCTY RGSCT with (nolock)  
  inner join @UserGrp URG  on URG.GrpID  = RGSCT.RPT_GRP_ID   
  inner join dbo.LK_RPT_GRP RG with (nolock) on RG.RPT_GRP_ID = URG.GrpID) and RPTTyp.RPT_GRP_TYPE_CD = 'C' and RPTTyp.ACT_CD = 1  
   AND (((RFW.FolderTypeCD IS NOT NULL) AND RFW.FolderTypeCD = CASE @CSGLevel WHEN 1 THEN 'X'  
          WHEN 2 THEN 'X'  
          ELSE 'S' END) OR (RFW.FolderTypeCD IS NULL))  
order by RPTTyp.RPT_NME  
  
IF (@CSGLevel IN (1,2))  
BEGIN  
INSERT INTO @Temp  
SELECT DISTINCT RPTTyp.RPT_TYPE_ID ,RPTTyp.RPT_NME ,RPTTyp.RPT_DES,RPT.REPORTID, RPT.GroupID,  
    CASE WHEN (RSC.ScheduleName LIKE '%Daily%') THEN 'D'   
      WHEN (RSC.ScheduleName LIKE '%Weekly%') THEN 'W'  
      WHEN (RSC.ScheduleName LIKE '%Monthly%') THEN 'M'  
      WHEN (RSC.ScheduleName LIKE '%Quarterly%') THEN 'Q'  
      WHEN (RSC.ScheduleName LIKE '%Yearly%') THEN 'Y' ELSE 'D'  
      END AS IntlCD, RSC.ScheduleName, RSC.[Description] As IntervalDesc, lg.GRP_NME, lu.DSPL_NME as OwnerName, COALESCE(RFW.Encrypted,0) as Encrypted, COALESCE(RFW.FolderTypeCD, 'S') as FolderTypeCD  
   FROM  dbo.LK_RPT_TYPE RPTTyp with (nolock)   
   inner join RAD_Reporting.rad.Report RPT with (nolock) on RPT.ReportName = RPTTyp.RAD_REPORTNAME  
   inner join RAD_Reporting.rad.ReportTemplate RTP with (nolock) on RTP.TemplateGroupID = RPT.TemplateGroupID  
   inner join RAD_Reporting.rad.ReportSchedule RSC with (nolock) on RSC.ScheduleID = RTP.ScheduleID  
   inner join COWS.dbo.LK_GRP lg with (nolock) on lg.GRP_ID = RPT.GroupID  
   inner join COWS.dbo.LK_User lu with (nolock) on lu.USER_ADID = RPTTyp.CREAT_BY_AD_ID  
   left join RAD_Reporting.rad.REPORTFILEWEB_NEW RFW with (nolock) on RFW.REPORTID = RPT.REPORTID  
 where RPTTyp.RPT_TYPE_ID in (  
  select RGSCT.RPT_TYPE_ID from dbo.RPT_GRP_SCTY RGSCT with (nolock)  
  inner join @UserGrp URG  on URG.GrpID  = RGSCT.RPT_GRP_ID   
  inner join dbo.LK_RPT_GRP RG with (nolock) on RG.RPT_GRP_ID = URG.GrpID) and RPTTyp.RPT_GRP_TYPE_CD = 'C' and RPTTyp.ACT_CD = 1  
   AND (((RFW.FolderTypeCD IS NOT NULL) AND (RFW.FolderTypeCD = 'S')) OR (RFW.FolderTypeCD IS NULL))  
   AND RPTTyp.RPT_TYPE_ID NOT IN (SELECT RPT_TYPE_ID FROM @Temp)  
order by RPTTyp.RPT_NME  
END  
  
SELECT * FROM @Temp WHERE ((@CSGLevel IN (1,2) AND ((RPT_NME LIKE '%Fedline%') OR (RPT_NME LIKE '%FRB%'))) OR ((RPT_NME NOT LIKE '%Fedline%') AND (RPT_NME NOT LIKE '%FRB%'))) ORDER BY RPT_NME
  
END  
ELSE IF (@ReportType = 'O')  
BEGIN  
SELECT DISTINCT RPTTyp.RPT_TYPE_ID ,RPTTyp.RPT_NME ,RPTTyp.RPT_DES, OD.STORED_PROC_NME, OD.FORM_NME, lu.DSPL_NME as OwnerName  
   FROM  dbo.LK_RPT_TYPE RPTTyp with (nolock)   
   inner join dbo.ON_DMND_RPT OD with (nolock) on RPTTyp.RPT_TYPE_ID = OD.RPT_TYPE_ID  
   inner join COWS.dbo.LK_User lu with (nolock) on lu.USER_ADID = RPTTyp.CREAT_BY_AD_ID  
  where RPTTyp.RPT_TYPE_ID in (  
  select RGSCT.RPT_TYPE_ID from dbo.RPT_GRP_SCTY RGSCT with (nolock)  
  inner join @UserGrp URG  on URG.GrpID  = RGSCT.RPT_GRP_ID   
  inner join dbo.LK_RPT_GRP RG with (nolock) on RG.RPT_GRP_ID = URG.GrpID) and RPTTyp.RPT_GRP_TYPE_CD = 'O' and RPTTyp.ACT_CD = 1  
  AND ((@CSGLevel IN (1,2) AND ((RPTTyp.RPT_NME LIKE '%Fedline%') OR (RPTTyp.RPT_NME LIKE '%FRB%'))) OR ((RPTTyp.RPT_NME NOT LIKE '%Fedline%') AND (RPTTyp.RPT_NME NOT LIKE '%FRB%')))
order by RPTTyp.RPT_NME  
END  
  
End Try  
  
Begin Catch  
 --EXEC [dbo].[insertErrorInfo]  
End Catch  
END  