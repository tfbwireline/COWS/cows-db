USE [COWS_Reporting]
GO
/*
-- =============================================
-- Author:		
-- Create date: 
-- Description: Extract Billing-Only orders for the previous 12 months
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- =============================================
*/


ALTER PROCEDURE [dbo].[sp_MonthlyBillOnlyOrdrs]
	  @SecuredUser		bit = 0
AS
BEGIN TRY

--	Declare  @SecuredUser		bit = 0

	SET NOCOUNT ON;
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
	
	Declare @startDate Datetime = CONVERT(datetime, GETDATE(), 101)
	Declare @endDate Datetime = CONVERT(datetime, GETDATE(), 101)
	
	SET @startDate = DATEADD(MONTH, -12, @startDate)				

    SELECT distinct
			CASE
					--WHEN @SecuredUser = 0 and ord.SCURD_CD = 0 THEN CONVERT(varchar, DecryptByKey(foc.CUST_NME))	
					--WHEN @SecuredUser = 0 and ord.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(foc.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					--WHEN @SecuredUser = 0 and ord.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(foc.CUST_NME)) IS NULL THEN NULL 
					--ELSE CONVERT(varchar, DecryptByKey(foc.CUST_NME))	
					WHEN @SecuredUser = 0 and ord.CSG_LVL_ID =0 THEN foc.CUST_NME
					WHEN @SecuredUser = 0 and ord.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and ord.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
					ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),foc.CUST_NME,'')
					END [Customer Name] --Secured
			,ISNULL(fs.FTN, '') [FTN]
			,lc.CTRY_NME [Country]
			,ord.H5_H6_CUST_ID [H5 Account Number]
			,fs.CUST_SIGNED_DT [Customer Sign Date]
			,ISNULL(ord.CUST_CMMT_DT,fs.CUST_CMMT_DT) [CCD Date]
			,om.XNCI_CNFRM_RNL_DT [xNCI Renewal Date]
				
			FROM [COWS].[dbo].[FSA_ORDR] fs WITH (NOLOCK)
			INNER JOIN [COWS].[dbo].[ORDR] ord WITH (NOLOCK) ON fs.ORDR_ID = ord.ORDR_ID
			LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] oa WITH (NOLOCK) 
					ON fs.ORDR_ID = oa.ORDR_ID AND oa.CIS_LVL_TYPE = 'H5'
			LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST]foc WITH (NOLOCK) ON ord.ORDR_ID = foc.ORDR_ID
								AND ord.H5_H6_CUST_ID = foc.CUST_ID
			LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] lc WITH (NOLOCK) ON oa.CTRY_CD = lc.CTRY_CD
			LEFT OUTER JOIN  [COWS].[dbo].[ORDR_MS] om WITH (NOLOCK)ON om.ORDR_ID = fs.ORDR_ID
	        LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=foc.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
											 
			WHERE ord.CREAT_DT >= @startDate AND ord.CREAT_DT < @endDate
					AND fs.ORDR_TYPE_CD = 'BC'
				
			ORDER By ISNULL(ord.CUST_CMMT_DT,fs.CUST_CMMT_DT) DESC		 
	
CLOSE SYMMETRIC KEY FS@K3y 

	END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MonthlyBillOnlyOrdrs '
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH
