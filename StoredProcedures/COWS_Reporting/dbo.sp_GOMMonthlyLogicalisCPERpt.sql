USE [COWS_Reporting]
GO
--=======================================================================================
-- Project:			CR 200 (COWS)
-- Author:			Ramesh Ragi	
-- Date:			06/27/2013
-- Description:		GOM Logicalis CPE Report
--=======================================================================================

ALTER PROCEDURE [dbo].[sp_GOMMonthlyLogicalisCPERpt]
			@Secured	INT=0
AS
BEGIN TRY
	SET NOCOUNT ON;
 
DECLARE @DiscDate date
	set @DiscDate = GETDATE()
    ----- Used for testing
	--set @DiscDate = '06/01/2013'
	--declare @Secured	INT=1
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	

	Select	CASE 
				WHEN @Secured = 0 AND odr.CSG_LVL_ID >0 AND CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NOT NULL 
					THEN 'Private Customer'
				ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)),focH1.CUST_NME,'') 
				END [End Customer Name]
			,ISNULL(focH5.CUST_ID,0) AS 'H5 Acct Number'
			,fsa.FTN	AS 'FTN'
			,ISNULL(foGOM.PRCH_ORDR_NBR,'') AS 'PO#'
			,ISNULL(CONVERT(VARCHAR (12), foGOM.CUST_DLVRY_DT, 107), '') AS 'Date Customer Delivery'
			,CASE foGOM.CUST_PRCH_CD
				WHEN 1 THEN 'True'
				ELSE 'False'
				END AS 'Customer Purchase'
			,CASE foGOM.CUST_RNTL_CD
				WHEN 1 THEN 'True'
				ELSE 'False'
				END AS 'Customer Rental'
			,ISNULL(foGOM.CUST_RNTL_TERM_NBR,'') AS 'Customer Rental Term'
			,ISNULL(foGOM.LOGICALIS_MNTH_LEASE_RT_AMT,'') AS 'Logicalis Monthly Lease Rate'
			,ISNULL(CONVERT(VARCHAR (12), foGOM.TERM_STRT_DT, 107), '') AS 'Term Start Date'
			,ISNULL(CONVERT(VARCHAR (12), foGOM.TERM_END_DT, 107), '') AS 'Term End Date'
			,ISNULL(CONVERT(VARCHAR (12), foGOM.TERM_60_DAY_NTFCTN_DT, 107), '') AS '60 Day Term Notification Date'
			,CASE foGOM.CUST_RNL_CD
				WHEN 1 THEN 'Yes'
				WHEN 2 THEN 'No'
				ELSE '' 
				END AS 'Customer Renewal'
			,ISNULL(foGOM.CUST_RNL_TERM_NBR,'') AS 'Customer Renewal Term'
			,ISNULL(foGOM.LOGICALIS_RNL_MNTH_LEASE_RT_AMT,'') AS 'Logicalis Renewal Monthly Lease Rate'
			,CASE foGOM.CUST_DSCNCT_CD
				WHEN 1 THEN 'Yes'
				WHEN 2 THEN 'No'
				ELSE '' 
				END AS 'Customer Disconnect'
			,ISNULL(CONVERT(VARCHAR (12), foGOM.CUST_DSCNCT_DT, 107), '') AS 'Customer Disconnect Date' 
			 
		FROM	COWS.dbo.FSA_ORDR	fsa WITH (NOLOCK)
	INNER JOIN	COWS.dbo.ORDR odr WITH (NOLOCK) ON odr.ORDR_ID = fsa.ORDR_ID	
	LEFT JOIN	COWS.dbo.FSA_ORDR_CUST	focH1	WITH (NOLOCK) ON	focH1.ORDR_ID			=	fsa.ORDR_ID
																AND	focH1.CIS_LVL_TYPE		=	'H1'
	LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=focH1.FSA_ORDR_CUST_ID  
																AND csd.SCRD_OBJ_TYPE_ID=5
	LEFT JOIN	COWS.dbo.FSA_ORDR_CUST	focH5	WITH (NOLOCK) ON	focH5.ORDR_ID			=	fsa.ORDR_ID
																AND	focH5.CIS_LVL_TYPE		=	'H5'
	LEFT JOIN	COWS.dbo.FSA_ORDR_GOM_XNCI foGOM WITH (NOLOCK) ON	foGOM.ORDR_ID			=	fsa.ORDR_ID
																AND	foGOM.GRP_ID			=	1
		WHERE	fsa.PROD_TYPE_CD = 'CP'	and foGOM.CUST_RNTL_CD = '1' 
				and (foGOM.CUST_DSCNCT_DT IS NULL OR foGOM.CUST_DSCNCT_DT > @DiscDate)
											
CLOSE SYMMETRIC KEY FS@K3y;		

	RETURN 0;

END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_LogicalisCPERpt' 
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH

