/*  
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
 -- Update date: <06/06/2018>
 -- Updated by: Md M Monir  
 -- Description: <To get H5 Data>  CTY_NME 
--exec sp_ENCIRptRegionalInstalledKPIOD 1,'Y','2012-01-01','2012-04-23'  
*/  
CREATE PROCEDURE [dbo].[sp_ENCIRptRegionalInstalledKPIOD]  
     @SecuredUser int = 0,  
     @getSensData  CHAR(1)='N',  
     @startDate Datetime=NULL,  
     @endDate Datetime=NULL  
AS  
BEGIN TRY  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
--DECLARE @SecuredUser    bit = 1 --testing  
  
    -- Insert statements for procedure here  
-- ENCI Regional Installed KPI Report  
DECLARE @aSQL   VARCHAR(max)  
DECLARE @aSQLwhere   VARCHAR(max)  
DECLARE @aSQLwhere1   VARCHAR(max)  
DECLARE @aSQLwhere2   VARCHAR(max)  
DECLARE @aSQLwhere3   VARCHAR(max)  
  
DECLARE @startDateStr VARCHAR(20)  
DECLARE @endDateStr  VARCHAR(20)  
DECLARE @SQLData  varchar(max)  
   
OPEN SYMMETRIC KEY FS@K3y   
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
IF OBJECT_ID('tempdb..#tmp_enci_Instl', 'U') IS NOT NULL   
  drop table #tmp_enci_Instl;  
CREATE TABLE #tmp_enci_Instl  
 ( FTN  varchar(50),  
NUA varchar(200),  
Private_Line_Number varchar(200),  
Customer_Name varchar(100),  
Customer_Name2 varchar(100),  
Product varchar(100),  
H5City varchar(100),  
H5Country varchar(100),  
Speed_Of_Service varchar(50),  
Order_Status varchar(25),  
Monthly_Recurring_Charge varchar(20),  
Non_Recurring_Charge varchar(20),  
Customer_Signed_Date varchar(25),  
Order_Submit_Date varchar(25),  
Complete_Order_was_recieved varchar(25),  
Customer_Want_Date varchar(25),  
Customer_Commit_Date varchar(25),  
Order_Bill_Clear_Install_date varchar(25),  
Vendor_Name varchar(50),  
Assigned_User_in_xNCI_WFM varchar(100),  
Service_Request_Type varchar(100),  
GOM_Recieves_Order_Date varchar(25),  
Scurd_Cd Bit  
 )  
  
SET @aSQL= N'Insert into #tmp_enci_Instl (FTN ,NUA ,Private_Line_Number ,Customer_Name,Customer_Name2 ,Product ,H5City ,H5Country ,Speed_Of_Service ,Order_Status ,Monthly_Recurring_Charge,Non_Recurring_Charge,Customer_Signed_Date ,Order_Submit_Date ,  
Complete_Order_was_recieved ,Customer_Want_Date ,Customer_Commit_Date ,Order_Bill_Clear_Install_date ,Vendor_Name ,Assigned_User_in_xNCI_WFM ,Service_Request_Type ,GOM_Recieves_Order_Date ,Scurd_Cd)  
SELECT DISTINCT CASE WHEN b.ORDR_CAT_ID = 1 THEN Convert(varchar(50),b.ORDR_ID)  
 WHEN b.ORDR_CAT_ID = 4 THEN Convert(varchar(50),b.ORDR_ID)  
 WHEN b.ORDR_CAT_ID = 5 THEN Convert(varchar(50),b.ORDR_ID)  
 WHEN b.ORDR_CAT_ID = 6 THEN c.FTN  
 WHEN b.ORDR_CAT_ID = 2 THEN c.FTN END [FTN],   
nc.NUA_ADR [NUA],CASE WHEN b.ORDR_CAT_ID = 1 THEN t.PRE_XST_PL_NBR  
   WHEN b.ORDR_CAT_ID = 4 THEN t.PRE_XST_PL_NBR  
   WHEN b.ORDR_CAT_ID = 5 THEN t.PRE_XST_PL_NBR   
   WHEN b.ORDR_CAT_ID = 6 THEN NC.PLN_NME  
   WHEN b.ORDR_CAT_ID = 2 THEN NC.PLN_NME END [Private_Line_Numbe],  
P.CUST_NME [Customer_Name],   
CONVERT(varchar, DecryptByKey(csd2.CUST_NME))  [Customer_Name2],  
CASE WHEN b.ORDR_CAT_ID = 1 THEN u.PROD_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 4 THEN u.PROD_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 5 THEN u.PROD_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 6 THEN d.PROD_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 2 THEN d.PROD_TYPE_DES END  [Product],   
--p.CUST_CTY_NME [H5City],
CASE WHEN (P.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd2.CTY_NME) ELSE P.CUST_CTY_NME END AS H5City, /*Monir 06062018*/
--lc.CTRY_NME  [H5Country],
CASE WHEN (p.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE lc.CTRY_NME END [H5Country], /*Monir 06062018*/    
CASE WHEN b.ORDR_CAT_ID = 1 THEN CONVERT(varchar,t.CKT_SPD_QTY)  
 WHEN b.ORDR_CAT_ID = 4 THEN CONVERT(varchar,t.CKT_SPD_QTY)  
 WHEN b.ORDR_CAT_ID = 5 THEN CONVERT(varchar,t.CKT_SPD_QTY)  
 WHEN b.ORDR_CAT_ID = 6 THEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES  
 WHEN b.ORDR_CAT_ID = 2 THEN fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES   
END  [Speed_Of_Service],f.ORDR_STUS_DES [Order_Status],  
ISNULL(CAST(b5.MRC_CHG_AMT AS MONEY), 0) [Monthly_Recurring_Charge],  
ISNULL(CAST(b5.NRC_CHG_AMT AS MONEY), 0) [Non_Recurring_Charge],  
CASE WHEN b.ORDR_CAT_ID = 1 THEN t.CUST_CNTRC_SIGN_DT  
 WHEN b.ORDR_CAT_ID = 4 THEN t.CUST_CNTRC_SIGN_DT  
 WHEN b.ORDR_CAT_ID = 5 THEN t.CUST_CNTRC_SIGN_DT  
 WHEN b.ORDR_CAT_ID = 6 THEN c.CUST_SIGNED_DT  
 WHEN b.ORDR_CAT_ID = 2 THEN c.CUST_SIGNED_DT   
 END [Customer_Signed_Date],   
CASE WHEN b.ORDR_CAT_ID = 1 THEN t.CREAT_DT  
 WHEN b.ORDR_CAT_ID = 4 THEN t.CREAT_DT  
 WHEN b.ORDR_CAT_ID = 5 THEN t.CREAT_DT  
 WHEN b.ORDR_CAT_ID = 6 THEN c.CREAT_DT  
 WHEN b.ORDR_CAT_ID = 2 THEN c.CREAT_DT   
 END [Order_Submit_Date],  
CASE WHEN b.ORDR_CAT_ID = 1 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 4 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 5 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 6 THEN c.ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 2 THEN c.ORDR_SBMT_DT  END [Complete_Order_was_recieved],  
CASE WHEN b.ORDR_CAT_ID = 1 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 4 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 5 THEN t.CUST_ORDR_SBMT_DT  
 WHEN b.ORDR_CAT_ID = 6 THEN c.CUST_WANT_DT  
 WHEN b.ORDR_CAT_ID = 2 THEN c.CUST_WANT_DT    
 END [Customer_Want_Date],  
CASE When ch.new_ccd_dt IS NULL THEN  
CASE WHEN b.ORDR_CAT_ID = 1 THEN t.CUST_CMMT_DT  
  WHEN b.ORDR_CAT_ID = 4 THEN t.CUST_CMMT_DT  
  WHEN b.ORDR_CAT_ID = 5 THEN t.CUST_CMMT_DT  
  WHEN b.ORDR_CAT_ID = 6 THEN c.CUST_CMMT_DT  
  WHEN b.ORDR_CAT_ID = 2 THEN c.CUST_CMMT_DT END   
When ch.new_ccd_dt IS NOT NULL THEN ch.new_ccd_dt END [Customer_Commit_Date],  
    
tk.CREAT_DT [Order_Bill_Clear_Install_date], '  
   
Set @aSQLwhere = N'CASE WHEN b.ORDR_CAT_ID = 1 THEN ab.VNDR_NME  
 WHEN b.ORDR_CAT_ID = 4 THEN ab.VNDR_NME  
 WHEN b.ORDR_CAT_ID = 5 THEN ab.VNDR_NME  
 WHEN b.ORDR_CAT_ID = 6 THEN ab.VNDR_NME  
 WHEN b.ORDR_CAT_ID = 2 THEN ab.VNDR_NME END   [Vendor_Name],  
lu.FULL_NME [Assigned_User_in_xNCI_WFM],   
CASE WHEN b.ORDR_CAT_ID = 2 THEN j.ORDR_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 6 THEN j.ORDR_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 4 THEN  m.ORDR_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 5 THEN  m.ORDR_TYPE_DES  
 WHEN b.ORDR_CAT_ID = 1 THEN  m.ORDR_TYPE_DES END [Service_Request_Type],  
gq.[GOM Receives Order] [GOM_Recieves_Order_Date],  
CASE b.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END [Scurd_Cd]  
FROM [COWS].[dbo].[ORDR] b LEFT OUTER JOIN  
[COWS].[dbo].[FSA_ORDR] c with (nolock) on b.ORDR_ID = c.ORDR_ID   
LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on c.ORDR_ID = c.ORDR_ID  
LEFT OUTER JOIN  
[COWS].[dbo].[IPL_ORDR] t with (nolock) on b.ORDR_ID = t.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_PROD_TYPE] d with (nolock) on c.PROD_TYPE_CD = d.FSA_PROD_TYPE_CD LEFT OUTER JOIN  
[COWS].[dbo].[LK_PROD_TYPE] u with (nolock) on  t.PROD_TYPE_ID = u.PROD_TYPE_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_ORDR_STUS] f with (nolock) on b.ORDR_STUS_ID = f.ORDR_STUS_ID LEFT OUTER JOIN  
[COWS].[dbo].[ORDR_MS] g with (nolock) on b.ORDR_ID = g.ORDR_ID JOIN  
(SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID, cc.ORDR_STUS_ID FROM [COWS].[dbo].[ACT_TASK] bb LEFT OUTER JOIN [COWS].[dbo].[ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
 WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID) FROM (SELECT ACT_TASK_ID, ORDR_ID, TASK_ID  FROM [COWS].[dbo].[ACT_TASK] ) tsk  WHERE tsk.ORDR_ID = bb.ORDR_ID and cc.ORDR_STUS_ID in (2))) tk on  b.ORDR_ID = tk.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_TASK] h with (nolock) on tk.TASK_ID = h.TASK_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_STUS]i with (nolock) on h.ORDR_STUS_ID = i.STUS_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_ORDR_TYPE] j with (nolock) on  c.ORDR_TYPE_CD = j.FSA_ORDR_TYPE_CD LEFT OUTER JOIN  
[COWS].[dbo].[LK_ORDR_TYPE] m with (nolock) on  t.ORDR_TYPE_ID = m.ORDR_TYPE_ID LEFT OUTER JOIN '  
   
 Set @aSQLwhere1 = N'  
[COWS].[dbo].[H5_FOLDR] cust on b.H5_FOLDR_ID = cust.H5_FOLDR_ID LEFT OUTER JOIN   
[COWS].[dbo].[LK_VNDR] l with (nolock) on c.INSTL_VNDR_CD = l.VNDR_CD   LEFT OUTER JOIN  
[COWS].[dbo].[TRPT_ORDR]  n with (nolock) on b.ORDR_ID = n.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_SALS_CHNL] o with (nolock) on n.SALS_CHNL_ID = o.SALS_CHNL_ID LEFT OUTER JOIN  
[COWS].[dbo].[CKT] q on b.ORDR_ID = q.ORDR_ID LEFT OUTER JOIN '  
  
Set @aSQLwhere2 = '(SELECT DISTINCT bb.CKT_ID, bb.VER_ID, bb.TRGT_DLVRY_DT, cc.PL_SEQ_NBR, cc.ORDR_ID from [COWS].[dbo].[CKT_MS] bb   
join [COWS].[dbo].[CKT] cc on cc.CKT_ID = bb.CKT_ID   
where bb.TRGT_DLVRY_DT = (SELECT MAX(TRGT_DLVRY_DT) from (SELECT TRGT_DLVRY_DT, CKT_ID FROM [COWS].[dbo].[CKT_COST]) ee where ee.CKT_ID = bb.CKT_ID ))td on b.ORDR_ID = td.ORDR_ID LEFT OUTER JOIN  
(SELECT DISTINCT bb.TRMTG_CD, bb.ORDR_ID, bb.VNDR_FOLDR_ID FROM [COWS].[dbo].[VNDR_ORDR] bb JOIN [COWS].[dbo].[ORDR] cc   
 on cc.ORDR_ID = bb.ORDR_ID WHERE TRMTG_CD = 0)  w on b.ORDR_ID = w.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[VNDR_FOLDR] x with (nolock) on w.VNDR_FOLDR_ID = x.VNDR_FOLDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_VNDR] y with (nolock) on x.VNDR_CD = y.VNDR_CD LEFT OUTER JOIN  
(SELECT DISTINCT bb.TRMTG_CD, bb.ORDR_ID, bb.VNDR_FOLDR_ID FROM [COWS].[dbo].[VNDR_ORDR] bb JOIN [COWS].[dbo].[ORDR] cc   
 on cc.ORDR_ID = bb.ORDR_ID WHERE TRMTG_CD = 1) z on b.ORDR_ID = z.ORDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[VNDR_FOLDR] aa on z.VNDR_FOLDR_ID = aa.VNDR_FOLDR_ID LEFT OUTER JOIN  
[COWS].[dbo].[LK_VNDR] ab on aa.VNDR_CD = ab.VNDR_CD LEFT OUTER JOIN  
(SELECT ad.ORDR_ID, ac.SRVC_CKT_DES FROM [COWS].[dbo].[IPL_PROD_SRVC_CKT] ac join [COWS].[dbo].[IPL_ORDR] ad on ad.PROD_TYPE_ID = ac.PROD_TYPE_ID WHERE ad.SRVC_TYPE_CD = ac.SRVC_TYPE_CD and ad.CKT_TYPE_CD = ac.CKT_TYPE_CD) ds on t.ORDR_ID = ds.ORDR_ID   
 LEFT OUTER JOIN  
(SELECT  DISTINCT bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK) LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT) FROM (SELECT DISTINCT ASMT_DT, ORDR_ID FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr WHERE bb.ORDR_ID = usr.ORDR_ID )  
AND bb.ASN_USER_ID = (SELECT  MAX(ASN_USER_ID) FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
 WHERE bb.ORDR_ID = uas.ORDR_ID) ) ufm ON b.ORDR_ID =  ufm.ORDR_ID LEFT OUTER JOIN '   
  
Set @aSQLwhere3 = N'[COWS].[dbo].[LK_USER] lu on ufm.ASN_USER_ID = lu.USER_ID LEFT OUTER JOIN  
(SELECT DISTINCT [FTN],[FMS_CKT_ID] ,[NUA_ADR] ,[PLN_NME]  
  FROM [COWS].[dbo].[NRM_CKT] ) nc on c.FTN = nc.FTN LEFT OUTER JOIN  
[COWS].[dbo].[H5_FOLDR] p on b.H5_FOLDR_ID = p.H5_FOLDR_ID LEFT OUTER JOIN  
[COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=p.H5_FOLDR_ID  AND csd2.SCRD_OBJ_TYPE_ID=6  LEFT OUTER JOIN  
[COWS].[dbo].[LK_CTRY] lc on p.CTRY_CD = lc.CTRY_CD LEFT OUTER JOIN 
[COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd2.CTRY_CD)) = lcs.CTRY_CD  LEFT OUTER JOIN 
(SELECT bb.ORDR_ID, SUM(CAST(bb.MRC_CHG_AMT AS MONEY)) MRC_CHG_AMT, SUM(CAST (bb.NRC_CHG_AMT AS MONEY)) NRC_CHG_AMT FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
      JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID GROUP by bb.ORDR_ID) b5 on b.ORDR_ID = b5.ORDR_ID LEFT OUTER JOIN  
(SELECT ORDR_ID, CONVERT(VARCHAR(10),MIN(at.CREAT_DT),101) as [GOM Receives Order] FROM [COWS].dbo.ACT_TASK at WITH (NOLOCK)  
      JOIN [COWS].dbo.MAP_GRP_TASK mgt WITH (NOLOCK) on mgt.TASK_ID=at.TASK_ID WHERE mgt.GRP_ID=1 AND at.TASK_ID NOT IN (101,102)  
      GROUP BY ORDR_ID) gq ON c.ORDR_ID = gq.ORDR_ID  
 Left outer join (select ch.ordr_id,new_ccd_dt,ccd_hist_id from [COWS].[dbo].ccd_hist ch with(nolock) join   
   [COWS].[dbo].[ORDR] cc with (nolock) on ch.ORDR_ID = cc.ORDR_ID    
   where ch.CCD_HIST_ID = (select MAX(ccd_hist_id) from (select ORDR_ID,CCD_HIST_ID from [COWS].[dbo].ccd_hist with(nolock) ) ch1 where ch1.ordr_id = ch.ORDR_ID ))  
   ch on ch.ORDR_ID = b.ORDR_ID WHERE b.RGN_ID = 2 AND (c.ORDR_TYPE_CD <> ''DC'' OR  t.ORDR_TYPE_ID <> 7) '  
  
IF @startDate is NOT NULL AND @endDate is NOT NULL  
 BEGIN  
  -- Do the conversion before passing them to @aSQL  
  --SET @startDateStr = convert(datetime,SUBSTRING(CONVERT(char, @startDate, 101), 1, 10))   
  --SET @endDateStr = convert(datetime,SUBSTRING(CONVERT(char, @endDate, 101), 1, 10))    
   
  --SET @aSQLwhere1=@aSQLwhere1 + 'AND (CASE WHEN b.ORDR_CAT_ID = 1 THEN SUBSTRING(CONVERT(char, t.CREAT_DT, 101), 1, 10) WHEN b.ORDR_CAT_ID = 2 THEN SUBSTRING(CONVERT(char, c.CREAT_DT, 101), 1, 10) END) >= ''' + @startDateStr + ''''   
  --SET @aSQLwhere1=@aSQLwhere1 +  ' AND (CASE WHEN b.ORDR_CAT_ID = 1 THEN SUBSTRING(CONVERT(char, t.CREAT_DT, 101), 1, 10) WHEN b.ORDR_CAT_ID = 2 THEN SUBSTRING(CONVERT(char, c.CREAT_DT, 101), 1, 10)  END) < DATEADD(day, 1, ''' + @endDateStr + ''') '  
    
     SET @startDateStr =  @startDate  
  SET @endDateStr = @endDate  
   
  SET @aSQLwhere3=@aSQLwhere3 + 'AND tk.CREAT_DT >= ''' + @startDateStr + ''''   
  SET @aSQLwhere3=@aSQLwhere3 +  ' AND tk.CREAT_DT < DATEADD(day, 1, ''' + @endDateStr + ''') '  
    
   
 END   
   
     set @aSQL = cast(@aSQL as varchar(max))  
     set  @aSQLwhere = cast(@aSQLwhere as varchar(max))  
     set  @aSQLwhere1 = cast(@aSQLwhere1 as varchar(max))  
     set  @aSQLwhere2 = cast(@aSQLwhere2 as varchar(max))  
     set  @aSQLwhere3 = cast(@aSQLwhere3 as varchar(max))  
   
 --   print (@aSQL + @aSQLwhere )  
 --print (@aSQLwhere1)  
 --   print (@aSQLwhere2 + @aSQLwhere3)  
       
    exec (@aSQL + @aSQLwhere + @aSQLwhere1 + @aSQLwhere2 + @aSQLwhere3)  
     
  
 --============  
 --Select Data  
 --============  
   
 select FTN 'FTN',  
NUA 'NUA',  
Private_Line_Number 'Private Line Number',  
dbo.CleanUpString(  
    CASE  WHEN @SecuredUser = 0 AND SCURD_CD = 1 AND Customer_Name2 <> '' THEN 'Private Customer'   
    WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Customer_Name2 <> '' THEN 'Private Customer'  
    WHEN (COALESCE(Customer_Name,Customer_Name2,'') = '') THEN ''  
    ELSE COALESCE(Customer_Name,Customer_Name2,'') END  
    ) as 'Customer Name',--Secured  
Product 'Product',      
H5City 'H5 City',  
H5Country 'H5 Country',  
Speed_Of_Service 'Speed Of Service' ,  
Order_Status 'Order Status' ,  
Monthly_Recurring_Charge 'Monthly Recurring Charge[MRC]' ,  
Non_Recurring_Charge 'Non Recurring Charge[NRC]' ,  
Customer_Signed_Date 'Customer Signed Date',  
Order_Submit_Date 'Order Submit Date' ,  
Complete_Order_was_recieved 'Complete Order was recieved' ,  
Customer_Want_Date 'Customer Want Date',  
Customer_Commit_Date 'Customer Commit Date' ,  
Order_Bill_Clear_Install_date 'Order Bill Clear Install date' ,  
Vendor_Name 'Vendor Name' ,  
CASE WHEN @SecuredUser = 0 AND SCURD_CD = 1 AND Assigned_User_in_xNCI_WFM <> '' THEN 'Private Customer'   
 WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Assigned_User_in_xNCI_WFM <> '' THEN 'Private Customer'  
 ELSE Assigned_User_in_xNCI_WFM END 'Assigned User in xNCI WFM',--Secured  
Service_Request_Type 'Service Request Type',  
GOM_Recieves_Order_Date 'GOM Recieves Order Date'  
 from #tmp_enci_Instl  
    
  
 IF OBJECT_ID('tempdb..#tmp_enci_Instl', 'U') IS NOT NULL   
  drop table #tmp_enci_Instl;  
    
  
CLOSE SYMMETRIC KEY FS@K3y   
  
RETURN 0;   
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(300)  
 --SET @Desc='EXEC COWS_Reporting.dbo.sp_ENCIRptRegionalInstalledKPIOD'   
 SET @Desc='EXEC COWS_Reporting.dbo.sp_ENCIRptRegionalInstalledKPIOD ' + CAST(@SecuredUser AS VARCHAR(2))  + ', '''  
 SET @Desc=@Desc + @getSensData + ','   
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)    
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  