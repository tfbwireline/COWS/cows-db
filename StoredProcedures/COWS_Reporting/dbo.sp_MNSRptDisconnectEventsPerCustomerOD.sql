USE [COWS_Reporting]
GO
/*
--========================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			07/18/2011
-- Description:		
--					Extract MDS Disconnect Events per customer for specified parameter(s);
--					Customer Name, H5, H6, Date Range.	
--					
-- Notes:			MDS_ACTY_TYPE_ID: 3 = Disconnect
--
-- Modifications:
--
-- 09/22/2011		sxv0766: Added h5_h6_ID, @getSensData
-- 01/25/2012		sxv0766: Specified VARCHAR size of 100 for Cust Name decryption
-- 02/21/2012		sxv0766: Added Reschedule Failed ACTN ID from 31 to 43
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--========================================================================================
*/
ALTER Procedure [dbo].[sp_MNSRptDisconnectEventsPerCustomerOD] 
	@SecuredUser		INT=0,	
	@getSensData		CHAR(1)='N',
	@startDate			Datetime=NULL,
	@endDate			Datetime=NULL
AS

BEGIN TRY

	SET NOCOUNT ON;

	DECLARE @startDateStr	VARCHAR(20)
	DECLARE @endDateStr		VARCHAR(20)		
	
	DECLARE @aSQL			VARCHAR(4000)
	
	SET @startDate=CONVERT(DATETIME, @startDate, 101)  
    SET @endDate=CONVERT(DATETIME, @endDate, 101)      
    -- Add 1 day to End Date in order to cover the End Date 24-hr period  
    SET @endDate=DATEADD(DAY, 1, @endDate)
   
	DECLARE @tmp_mns_disc AS TABLE
	(					
		Event_ID			INT,
		H1					VARCHAR(50),
		Cust_Name			VARCHAR(100),
		Cust_Name2			VARCHAR(100),
		Scurd_Cd			Bit,
		Start_Time			VARCHAR(25),
		End_Time			VARCHAR(25),
		Event_Stus			VARCHAR(50),
		MDS_Acty_Type		VARCHAR(25),	
		FastTrack_Type		VARCHAR(50),
		MNS_PM_Name			VARCHAR(100)
	)
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	

			INSERT INTO @tmp_mns_disc(Event_ID, H1, Cust_Name, Scurd_Cd, Start_Time, End_Time, Event_Stus, 
				MDS_Acty_Type, FastTrack_Type, MNS_PM_Name)
			SELECT 
			mds.EVENT_ID , 					
			mds.H1_ID , 		
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,	
			REPLACE(SUBSTRING(CONVERT(varchar, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) ,
			REPLACE(SUBSTRING(CONVERT(varchar, mds.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 25, 2) ,				
			UPPER(evst.EVENT_STUS_DES) , 
			mact.MDS_ACTY_TYPE_DES , 
			CASE
				WHEN mds.MDS_FAST_TRK_TYPE_ID is NULL THEN ''
				ELSE mftk.MDS_FAST_TRK_TYPE_DES END ,
			CASE
				WHEN luser.DSPL_NME is NULL THEN ''						
				ELSE luser.DSPL_NME END 
	
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_FAST_TRK_TYPE mftk with (nolock) ON mds.MDS_FAST_TRK_TYPE_ID = mftk.MDS_FAST_TRK_TYPE_ID
		LEFT JOIN ( SELECT EVENT_ID, CREAT_BY_USER_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (5, 9, 12, 15, 17) OR ACTN_ID between 31 and 43) ) evhi ON mds.EVENT_ID = evhi.EVENT_ID				
		LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON evhi.CREAT_BY_USER_ID = luser.USER_ID					
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) 
			AND mds.STRT_TMST >=COALESCE(@startDate,mds.STRT_TMST )  AND mds.STRT_TMST<COALESCE(@endDate,mds.STRT_TMST) 
													
	UNION ALL
	SELECT 
			mds.EVENT_ID , 					
			mds.H1 AS H1_ID, 		
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,	
			REPLACE(SUBSTRING(CONVERT(varchar, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) ,
			REPLACE(SUBSTRING(CONVERT(varchar, mds.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 25, 2) ,				
			UPPER(evst.EVENT_STUS_DES) , 
			mact.MDS_ACTY_TYPE_DES , 
			CASE
				WHEN mds.MDS_FAST_TRK_TYPE_ID is NULL THEN ''
				ELSE mftk.MDS_FAST_TRK_TYPE_DES END ,
			CASE
				WHEN luser.DSPL_NME is NULL THEN ''						
				ELSE luser.DSPL_NME END 
	
	FROM	COWS.dbo.MDS_EVENT mds with (nolock) 
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_FAST_TRK_TYPE mftk with (nolock) ON mds.MDS_FAST_TRK_TYPE_ID = mftk.MDS_FAST_TRK_TYPE_ID
		LEFT JOIN ( SELECT EVENT_ID, CREAT_BY_USER_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (5, 9, 12, 15, 17) OR ACTN_ID between 31 and 43) ) evhi ON mds.EVENT_ID = evhi.EVENT_ID				
		LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON evhi.CREAT_BY_USER_ID = luser.USER_ID					
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6) 
			AND mds.STRT_TMST >=COALESCE(@startDate,mds.STRT_TMST )  AND mds.STRT_TMST<COALESCE(@endDate,mds.STRT_TMST) 
	ORDER BY mds.EVENT_ID
	
	
	SELECT	Event_ID 'Event ID',
			H1,
			CASE 
				WHEN @SecuredUser = 0 and Scurd_Cd =0 THEN CUST_NAME
				WHEN @SecuredUser = 0 and Scurd_Cd >0 AND  CUST_NAME2 <>'' THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd >0 AND CUST_NAME2 <> '' THEN 'Private Customer'
				ELSE COALESCE(CUST_NAME2,CUST_NAME,'') 
	
				END 'Customer Name',
			
			Start_Time 'Start Time',
			End_Time 'End Time',
			Event_Stus 'Event Status',
			MDS_Acty_Type 'MDS Activity Type',
			FastTrack_Type 'Fast Track Type',
			MNS_PM_Name 'MNS PM Name'
	FROM @tmp_mns_disc
	
	
	CLOSE SYMMETRIC KEY FS@K3y;
	
	RETURN 0;

END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptDisconnectEventsPerCustomerOD, ' 
	SET @Desc=@Desc + CAST(@SecuredUser AS VARCHAR(2)) + ', ''' + @getSensData + ''', ' 
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH







