-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
  
-- EXEC COWS_Reporting.dbo.sp_AMNCIRptCCD 0, 0, '9/1/2010','12/16/2011', 1  
-- CI554013 change date: 7/28/2013  
--          Added the following columns: [PRS Quote Number],[RFQ Number], [QOT_NBR], [VNDR_ORDR_NBR]  
--   Added CKT_COST to joins.  
-- CI554013 change date: 12/10/2013  
--         Added New column[Last Updt DT] and added new join for new column.    
-- CI554013: 3/28/2014 Added Parent FTN Column    
-- vn370313: 11/17/2016 Added TTRPT_ACCS_TYPE_CD, TTRPT_ACCS_TYPE_DES, TTRPT_SPD_OF_SRVC_BDWD_DES   
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD  
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME 
-- [dbo].[sp_AMNCIRptCCD] 1  
-- =============================================  
ALTER PROCEDURE [dbo].[sp_AMNCIRptCCD]   
  @secured  BIT = 0  
  
  
AS  
BEGIN TRY  
--DECLARE @secured BIT=1 -- for testing purposes only  
  
  
    SET NOCOUNT ON;  
  
    DECLARE @startDate DATE  
	DECLARE @endDate DATE  

	SET @endDate = GETDATE()  
	SET @startDate = DATEADD(mm, -37, CONVERT(VARCHAR(25), DATEADD(DD, -(DAY(GETDATE()) - 1), GETDATE()), 101))  
    -----Last # Years Data Only As instructed by Pramod 06132018
	--SET @endDate = DATEADD(dd,-25,GETDATE())  
	--SET @startDate = DATEADD(mm, -37, CONVERT(VARCHAR(25), DATEADD(DD, -(DAY(GETDATE()) - 1), GETDATE()), 101))  

	--Print @endDate  
	--Print @startDate 
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
--DELETE FROM [COWS_Reporting].[dbo].[AMNCIRptCCDData]  WHERE  
--[COWS_Reporting].[dbo].[AMNCIRptCCDData].[Order Tracking No] in  
--(SELECT [Order Tracking No]  FROM [COWS_Reporting].[dbo].[AMNCIRptCCDData] as ovs   
--INNER JOIN [COWS].[dbo].[FSA_ORDR] as ov on ovs.[Order Tracking No] = CAST (ov.FTN AS VARCHAR(20))  
--LEFT OUTER JOIN [COWS].[dbo].[ORDR] as odr  
--on odr.ORDR_ID = ov.ORDR_ID)   
  
--INSERT INTO [COWS_Reporting].[dbo].[AMNCIRptCCDData]   
  
  
 SELECT DISTINCT  
 cast (ISNULL(i.FTN, ord.ORDR_ID) as varchar(20)) [FTN/Order ID],  
 --CONVERT(varchar, DecryptByKey(t.CUST_NME))  [Customer Name],  
   
 CASE  
 WHEN @secured = 0 and ord.CSG_LVL_ID =0 THEN t.CUST_NME  
 WHEN @secured = 0 and ord.CSG_LVL_ID >0 AND t.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
 WHEN @secured = 0 and ord.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd2.CUST_NME)) IS NULL THEN NULL   
 ELSE coalesce(t.CUST_NME,CONVERT(varchar, DecryptByKey(csd2.CUST_NME)),'') END [Customer Name], --Secured  
   
 CONVERT(VARCHAR(10), ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT), 101) [Implementation Date],  
 CASE   
  WHEN ord.ORDR_CAT_ID = 1 THEN w.SALS_PERSN_NME  
  WHEN ord.ORDR_CAT_ID = 4 THEN w.SALS_PERSN_NME  
  WHEN ord.ORDR_CAT_ID = 5 THEN w.SALS_PERSN_NME  
  WHEN ord.ORDR_CAT_ID = 6 THEN coalesce(tac.NME, tac.NME2,'')  
  WHEN ord.ORDR_CAT_ID = 2 THEN coalesce(tac.NME, tac.NME2,'') END [Implementation Contact],  
 coalesce(sis.NME, sis.NME2,'') [RAM],  
   
 CASE   
  WHEN ord.ORDR_CAT_ID = 1 THEN w.CLCM_NME  
  WHEN ord.ORDR_CAT_ID = 4 THEN w.CLCM_NME  
  WHEN ord.ORDR_CAT_ID = 5 THEN w.CLCM_NME  
  WHEN ord.ORDR_CAT_ID = 6 THEN g.FULL_NME  
  WHEN ord.ORDR_CAT_ID = 2 THEN g.FULL_NME END [CPM Contact],  
 CASE   
  WHEN ord.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), w.CUST_WANT_DT), 101)  
  WHEN ord.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), w.CUST_WANT_DT), 101)  
  WHEN ord.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), w.CUST_WANT_DT), 101)  
  WHEN ord.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), i.CUST_WANT_DT), 101)  
  WHEN ord.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), ISNULL( CONVERT(DATETIME,cm.TRGT_DLVRY_DT), i.CUST_WANT_DT), 101)  END [Sprint Target Delivery Date],  
 CASE   
  WHEN ord.ORDR_CAT_ID = 1 THEN lp.PROD_TYPE_DES  
  WHEN ord.ORDR_CAT_ID = 4 THEN lp.PROD_TYPE_DES  
  WHEN ord.ORDR_CAT_ID = 5 THEN lp.PROD_TYPE_DES  
  WHEN ord.ORDR_CAT_ID = 6 THEN h.PROD_TYPE_DES  
  WHEN ord.ORDR_CAT_ID = 2 THEN h.PROD_TYPE_DES END [Product],  
 CASE   
  WHEN ord.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, w.CUST_CMMT_DT), 101)  
  WHEN ord.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, w.CUST_CMMT_DT), 101)  
  WHEN ord.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, w.CUST_CMMT_DT), 101)  
  WHEN ord.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, i.CUST_CMMT_DT), 101)  
  WHEN ord.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), isnull(cd.NEW_CCD_DT, i.CUST_CMMT_DT), 101) END [Customer Commit Date],  
 xr.RGN_DES [xNCI Region],  
 oa.CTY_NME [H5 City],  
 --u.CTRY_NME [H5 Country], 
 CASE WHEN (t.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE u.CTRY_NME END [H5 Country], /*Monir 06062018*/  
 o.ORDR_STUS_DES [Order Status],  
 q.CCD_MISSD_REAS_DES [CCD Missed Reason],  
 mg.FULL_NME [Manager],  
 CASE  
  WHEN ord.ORDR_CAT_ID = 1 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) <= w.CUST_CMMT_DT THEN 1   
  WHEN ord.ORDR_CAT_ID = 1 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) > w.CUST_CMMT_DT  THEN 0   
  WHEN ord.ORDR_CAT_ID = 4 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) <= w.CUST_CMMT_DT THEN 1   
  WHEN ord.ORDR_CAT_ID = 4 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) > w.CUST_CMMT_DT  THEN 0  
  WHEN ord.ORDR_CAT_ID = 5 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) <= w.CUST_CMMT_DT THEN 1   
  WHEN ord.ORDR_CAT_ID = 5 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) > w.CUST_CMMT_DT  THEN 0  
  WHEN ord.ORDR_CAT_ID = 6 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) <= i.CUST_CMMT_DT THEN 1   
  WHEN ord.ORDR_CAT_ID = 6 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) > i.CUST_CMMT_DT  THEN 0  
  WHEN ord.ORDR_CAT_ID = 2 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) <= i.CUST_CMMT_DT THEN 1   
  WHEN ord.ORDR_CAT_ID = 2 AND ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) > i.CUST_CMMT_DT  THEN 0 END [CCD Met #],   
 1 [CCD Met All],  
  
 CASE    
  WHEN DATEPART(DW, ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT)) = 1 THEN CONVERT(VARCHAR(10), ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) + 3, 101)  
  WHEN DATEPART(DW, ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT)) = 2 THEN CONVERT(VARCHAR(10), ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) + 2, 101)  
  WHEN DATEPART(DW, ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT)) = 3 THEN CONVERT(VARCHAR(10), ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) + 1, 101)  
  WHEN DATEPART(DW, ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT)) = 5 THEN CONVERT(VARCHAR(10), ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) - 1, 101)  
  WHEN DATEPART(DW, ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT)) = 6 THEN CONVERT(VARCHAR(10), ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) - 2, 101)   
  WHEN DATEPART(DW, ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT)) = 7 THEN CONVERT(VARCHAR(10), ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT) - 3, 101)  
  ELSE CONVERT(VARCHAR(10), tk.CREAT_DT, 101)  END  [Week],  
 cast(DATEPART(month, ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT)) as varchar(2))   
  + '/01/' + cast(DATEPART(YEAR, ISNULL(cd.NEW_CCD_DT, tk.CREAT_DT))   as varchar(4)) [Month],  
  
 b5.MRC_CHG_AMT [MRC],  
 b5.NRC_CHG_AMT [NRC],  
 CASE   
 WHEN tk.TASK_ID = 1001 and ord.ORDR_STUS_ID <> 1 then CONVERT(VARCHAR(10), tk.CREAT_DT, 101) END [Bill Clear Date],  
  CASE   
  WHEN ord.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), w.CUST_WANT_DT, 101)   
  WHEN ord.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), w.CUST_WANT_DT, 101)  
  WHEN ord.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), w.CUST_WANT_DT, 101)  
  WHEN ord.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10), i.CUST_WANT_DT, 101)  
  WHEN ord.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10), i.CUST_WANT_DT, 101) END  [CWD],  
 CASE   
  WHEN ord.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10), w.CKT_SPD_QTY, 101)  
  WHEN ord.ORDR_CAT_ID = 4 THEN CONVERT(VARCHAR(10), w.CKT_SPD_QTY, 101)  
  WHEN ord.ORDR_CAT_ID = 5 THEN CONVERT(VARCHAR(10), w.CKT_SPD_QTY, 101)  
  WHEN ord.ORDR_CAT_ID = 6 THEN i.TTRPT_SPD_OF_SRVC_BDWD_DES  
  WHEN ord.ORDR_CAT_ID = 2 THEN i.TTRPT_SPD_OF_SRVC_BDWD_DES END [Access Speed],  
 CASE   
  WHEN ord.ORDR_CAT_ID = 1 THEN lo.ORDR_TYPE_DES  
  WHEN ord.ORDR_CAT_ID = 4 THEN lo.ORDR_TYPE_DES  
  WHEN ord.ORDR_CAT_ID = 5 THEN lo.ORDR_TYPE_DES  
  WHEN ord.ORDR_CAT_ID = 6 THEN n.ORDR_TYPE_DES  
  WHEN ord.ORDR_CAT_ID = 2 THEN n.ORDR_TYPE_DES END [Order Type],  
 s.ORDR_SUB_TYPE_DES [Order Sub Type],  
 --CONVERT(VARCHAR(10), vm.SENT_TO_VNDR_DT, 101) [Sent to Vendor Date],
 [Sent to Vendor Date]=ISNULL( (SELECT CONVERT(VARCHAR(10),MAX(voe.[SENT_DT]),101)  FROM [COWS].[dbo].[VNDR_ORDR_EMAIL] voe WITH (NOLOCK) WHERE vnd.VNDR_ORDR_ID = voe.VNDR_ORDR_ID) ,'')      ,      
 CONVERT(VARCHAR(10), om.PRE_SBMT_DT, 101) [Pre-submit Date],  
 CONVERT(VARCHAR(10), om.SBMT_DT, 101) [Submit Date],  
 vl.VNDR_NME [Vendor],  
 CONVERT(VARCHAR(10), cm.ACCS_ACPTC_DT, 101) [Access Accepted Date],  
 CONVERT(VARCHAR(10), cm.ACCS_DLVRY_DT, 101) [Access Delivery Date],  
 CONVERT(VARCHAR(10), cm.TRGT_DLVRY_DT, 101) [Target Delivery Date],  
 isnull(i.TSUP_PRS_QOT_NBR,'') [PRS Quote Number],  
 isnull(i.TSUP_RFQ_NBR,'') [RFQ Number],  
 isnull(ckc.QOT_NBR,'') [QOT_NBR],  
 isnull(ckc.VNDR_ORDR_NBR,'') [VNDR_ORDR_NBR],  
 osh.STDI_DT [STDI DATE],  
 osh.STDI_REAS_DES [STDI REASON ID],  
 osh.CMNT_TXT [STDI COMMENTS],  
 CONVERT(VARCHAR(10), orn.CREAT_DT, 101)+' '+RIGHT(CONVERT(VARCHAR(30), orn.CREAT_DT, 100),7) [Last Updt DT],  
 CONVERT(VARCHAR(10), i.CUST_SIGNED_DT, 101) [Customer Sign Date],  
 isnull(i.PRNT_FTN,'') [Parent FTN]  
 /* Added 11172016  by  vn370313 start */  
 ,isnull(fcpl.TTRPT_ACCS_TYPE_CD,'') as [Access Type Code]   
 ,isnull(fcpl.TTRPT_ACCS_TYPE_DES,'') as [Access Type Description]  
 ,isnull(fcpl2.TTRPT_SPD_OF_SRVC_BDWD_DES,'')  as [Speed of Service]  
 /* Added 11172016  by  vn370313 End */  

 FROM (SELECT * FROM [COWS].[dbo].[ORDR] WITH (NOLOCK)) ord   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] i WITH (NOLOCK) ON ord.ORDR_ID = i.ORDR_ID   
  --LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = i.ORDR_ID 
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock)  on fcpl.ORDR_ID = i.ORDR_ID AND fcpl.line_item_cd='ACS'
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl2 with (nolock) on fcpl2.ORDR_ID = i.ORDR_ID AND fcpl2.line_item_cd='PRT'
	 
  LEFT OUTER JOIN [COWS].[dbo].[IPL_ORDR] w WITH (NOLOCK) ON ord.ORDR_ID = w.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_GOM_XNCI] a WITH (NOLOCK) ON ord.ORDR_ID = a.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] b WITH (NOLOCK) ON ord.ORDR_ID = b.ORDR_ID   
  --LEFT OUTER JOIN [COWS].[dbo].[CKT] d WITH (NOLOCK) ON ord.ORDR_ID = d.ORDR_ID   
  --LEFT OUTER JOIN [COWS].[dbo].[USER_WFM_ASMT] e WITH (NOLOCK) ON ord.ORDR_ID = e.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] h WITH (NOLOCK) ON i.PROD_TYPE_CD = h.FSA_PROD_TYPE_CD   
   
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID,   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) [FRST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [LST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) + ' '+ CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [NME2],  
     CONVERT(varchar, DecryptByKey(csd2.CUST_EMAIL_ADR)) [EMAIL_ADR2],   
       
     bb.FRST_NME  as [FRST_NME],   
     bb.LST_NME   as [LST_NME],   
     bb.FRST_NME  + ' '+ bb.LST_NME   as [NME],  
     bb.EMAIL_ADR as [EMAIL_ADR],   
     bb.CNTCT_TYPE_ID   
  FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
  [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID  
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd2.SCRD_OBJ_TYPE_ID=15   
  WHERE bb.ROLE_ID = 13  
  ) tac on ord.ORDR_ID  = tac.ORDR_ID   
  
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID,   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) [FRST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [LST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) + ' '+ CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [NME2],  
     CONVERT(varchar, DecryptByKey(csd2.CUST_EMAIL_ADR)) [EMAIL_ADR2],   
       
     bb.FRST_NME  as [FRST_NME],   
     bb.LST_NME   as [LST_NME],   
     bb.FRST_NME  + ' '+ bb.LST_NME   as [NME],  
     bb.EMAIL_ADR as [EMAIL_ADR],    
     bb.CNTCT_TYPE_ID   
     
  FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK) LEFT OUTER JOIN   
  [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd2.SCRD_OBJ_TYPE_ID=15  
  WHERE bb.ROLE_ID = 11  
  ) sis on ord.ORDR_ID  = sis.ORDR_ID   
    
    
  LEFT OUTER JOIN (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID   
     FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
     LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.ASN_USER_ID  = (SELECT MAX(ASN_USER_ID)  
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
     WHERE uas.ORDR_ID = bb.ORDR_ID)  
     AND bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
    FROM (SELECT DISTINCT ASMT_DT, ORDR_ID   
     FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
    WHERE usr.ORDR_ID  = bb.ORDR_ID )) ur ON ur.ORDR_ID = i.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] g WITH (NOLOCK) ON ur.ASN_USER_ID = g.USER_ID   
     LEFT OUTER JOIN [COWS].[dbo].[LK_USER] mg WITH (NOLOCK) ON g.MGR_ADID = mg.USER_ADID   
  LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] k WITH (NOLOCK) ON ord.ORDR_ID = k.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_SALS_CHNL] j WITH (NOLOCK) ON j.SALS_CHNL_ID = k.SALS_CHNL_ID   
    
  LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.STUS_ID, bb.CREAT_DT, bb.ORDR_ID, cc.ORDR_STUS_ID  
   FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
    LEFT OUTER JOIN [COWS].[dbo].[ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
    FROM (SELECT ACT_TASK_ID, ORDR_ID   
     FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
    WHERE bb.ORDR_ID = tsk.ORDR_ID  )) tk ON ord.ORDR_ID = tk.ORDR_ID  
          
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_STUS] o WITH (NOLOCK) ON ord.ORDR_STUS_ID = o.ORDR_STUS_ID   
  LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] p WITH (NOLOCK) ON ord.ORDR_ID = p.ORDR_ID   
    
   LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.NEW_CCD_DT, bb.CCD_HIST_ID  
     FROM [COWS].[dbo].[CCD_HIST] bb LEFT OUTER JOIN  
  [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)  
   FROM (SELECT CCD_HIST_ID, ORDR_ID FROM [COWS].[dbo].[CCD_HIST] WITH (NOLOCK) ) ccd  
   WHERE ccd.ORDR_ID = bb.ORDR_ID)) cd on  ord.ORDR_ID = cd.ORDR_ID   
  
  LEFT OUTER JOIN [COWS].[dbo].[CCD_HIST_REAS] cch WITH (NOLOCK) on cd.CCD_HIST_ID = cch.CCD_HIST_ID  
  LEFT OUTER JOIN [COWS].[dbo].[LK_CCD_MISSD_REAS] q WITH (NOLOCK) ON cch.CCD_MISSD_REAS_ID = q.CCD_MISSD_REAS_ID   
  LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] r WITH (NOLOCK) ON r.FTN = i.FTN   
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON  r.TOC_CTRY_CD = l.CTRY_CD  
		  --LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.SOI_CD, CONVERT(varchar, DecryptByKey(bb.CUST_NME)) [CUST_NME]  
		  --  FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  WITH (NOLOCK)  
		  --  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
		  -- WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
		  --  FROM (SELECT SOI_CD, ORDR_ID   
		  --   FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi  
		  --  WHERE soi.ORDR_ID = bb.ORDR_ID)) si ON Z.ORDR_ID  = si.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] m WITH (NOLOCK) on i.ORDR_ID = m.ORDR_ID   
    
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] n WITH (NOLOCK) on i.ORDR_TYPE_CD = n.FSA_ORDR_TYPE_CD  
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_SUB_TYPE] s WITH (NOLOCK) on i.ORDR_SUB_TYPE_CD = s.ORDR_SUB_TYPE_CD  
   
    
    LEFT OUtER JOIN [COWS].[dbo].[H5_FOLDR] t WITH (NOLOCK) on ord.H5_FOLDR_ID = t.H5_FOLDR_ID  
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=t.H5_FOLDR_ID  AND csd2.SCRD_OBJ_TYPE_ID=6  
 LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] u  WITH (NOLOCK)on t.CTRY_CD = u.CTRY_CD  
 LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs WITH (NOLOCK) on CONVERT(varchar, DecryptByKey(csd2.CTRY_CD)) = lcs.CTRY_CD
 --LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] t on z.RGN_ID = t.RGN_ID  
   
  
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, SUM(CAST(bb.MRC_CHG_AMT AS MONEY)) MRC_CHG_AMT, SUM(CAST (bb.NRC_CHG_AMT AS MONEY)) NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 GROUP by bb.ORDR_ID) b5 on ord.ORDR_ID = b5.ORDR_ID  
 LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR] vnd WITH (NOLOCK) ON ord.ORDR_ID = vnd.ORDR_ID  
 LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] vf WITH (NOLOCK) ON vnd.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID  
 LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] vl WITH (NOLOCK) ON vf.VNDR_CD = vl.VNDR_CD  
		 --LEFT OUTER JOIN (SELECT VNDR_ORDR_ID, VER_ID, SENT_TO_VNDR_DT FROM [COWS].[dbo].[VNDR_ORDR_MS] a  
		 -- WHERE SENT_TO_VNDR_DT = (SELECT MAX(SENT_TO_VNDR_DT) from   
		 -- (SELECT DISTINCT VNDR_ORDR_ID, SENT_TO_VNDR_DT from [COWS].[dbo].[VNDR_ORDR_MS] WITH (NOLOCK)) v    
		 -- where a.VNDR_ORDR_ID = v.VNDR_ORDR_ID) AND VER_ID = (SELECT MAX(VER_ID)   
		 -- from (SELECT DISTINCT VNDR_ORDR_ID, VER_ID from [COWS].[dbo].[VNDR_ORDR_MS] WITH (NOLOCK)) vid   
		 -- where a.VNDR_ORDR_ID = vid.VNDR_ORDR_ID))  vm ON vnd.VNDR_ORDR_ID = vm.VNDR_ORDR_ID  
 --LEFT OUTER JOIN (SELECT MAX(SENT_TO_VNDR_DT) AS SENT_TO_VNDR_DT ,VNDR_ORDR_ID
 -- FROM [COWS].[dbo].[VNDR_ORDR_MS] a  WITH (NOLOCK) 
 -- WHERE  --VER_ID = (SELECT MAX(VER_ID)  from   [COWS].[dbo].[VNDR_ORDR_MS] b WITH (NOLOCK) Where a.VNDR_ORDR_ID=b.VNDR_ORDR_ID)
 --        VNDR_ORDR_EMAIL_ID = (SELECT MAX(VNDR_ORDR_EMAIL_ID)  from   [COWS].[dbo].[VNDR_ORDR_MS] b WITH (NOLOCK) Where a.VNDR_ORDR_ID=b.VNDR_ORDR_ID)
 -- Group by VNDR_ORDR_ID
 -- )
 -- vm ON vnd.VNDR_ORDR_ID = vm.VNDR_ORDR_ID 
   
 LEFT OUTER JOIN (SELECT ORDR_ID, MIN(at.CREAT_DT) as [GOM Receives Order]  
  FROM [COWS].dbo.ACT_TASK at WITH (NOLOCK)  
  JOIN [COWS].dbo.MAP_GRP_TASK mgt WITH (NOLOCK) on mgt.TASK_ID=at.TASK_ID  
  WHERE mgt.GRP_ID=1 AND at.TASK_ID NOT IN (101,102)  
  GROUP BY ORDR_ID) gq ON ord.ORDR_ID = gq.ORDR_ID  
 LEFT OUTER JOIN [COWS].[dbo].[LK_XNCI_RGN] xr WITH (NOLOCK) on ord.RGN_ID = xr.RGN_ID  
  
  
 LEFT OUTER JOIN COWS.dbo.ORDR_MS om  WITH (NOLOCK) on ord.ORDR_ID = om.ORDR_ID   
   
 LEFT OUTER JOIN (SELECT  DISTINCT aa.ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT] aa   
  JOIN [COWS].[dbo].[ORDR] bb on  aa.ORDR_ID = bb.ORDR_ID where  
   CKT_ID = (SELECT  MAX(CKT_ID) FROM (SELECT DISTINCT ORDR_ID, CKT_ID FROM [COWS].[dbo].[CKT]) ck      
   WHERE bb.ORDR_ID = ck.ORDR_ID)) ckt ON ord.ORDR_ID = ckt.ORDR_ID  
     
 LEFT OUTER JOIN (SELECT  DISTINCT aa.CKT_ID, aa.ACCS_DLVRY_DT, aa.ACCS_ACPTC_DT, aa.TRGT_DLVRY_DT   
  FROM [COWS].[dbo].[CKT_MS]  aa WITH (NOLOCK) JOIN [COWS].[dbo].[CKT] bb WITH (NOLOCK) on  aa.CKT_ID = bb.CKT_ID where  
   TRGT_DLVRY_DT = (SELECT  MAX(TRGT_DLVRY_DT) FROM (SELECT DISTINCT CKT_ID, TRGT_DLVRY_DT   
   FROM [COWS].[dbo].[CKT_MS] WITH (NOLOCK)) ck where bb.CKT_ID = ck.CKT_ID) AND ACCS_DLVRY_DT   
   = (SELECT MAX(ACCS_DLVRY_DT) from (SELECT DISTINCT CKT_ID, ACCS_DLVRY_DT   
   FROM [COWS].[dbo].[CKT_MS] WITH (NOLOCK)) ad where aa.CKT_ID = ad.CKT_ID ) AND ACCS_ACPTC_DT   
   = (SELECT MAX(ACCS_ACPTC_DT) from (SELECT DISTINCT CKT_ID, ACCS_ACPTC_DT   
   FROM [COWS].[dbo].[CKT_MS] WITH (NOLOCK)) aac where aa.CKT_ID = aac.CKT_ID )) cm on ckt.CKT_ID = cm.CKT_ID  
     
 --  Added join  
 LEFT OUTER JOIN (SELECT c.QOT_NBR,
						 c.VNDR_ORDR_NBR,
						 CKT_ID
				  FROM   [COWS].[dbo].[CKT_COST] c WITH (NOLOCK) 
				  WHERE  VER_ID= (SELECT MAX(VER_ID) FROM [COWS].[dbo].[CKT_COST] d WITH (NOLOCK) WHERE c.CKT_ID = d.CKT_ID)
				  ) ckc ON ckt.CKT_ID = ckc.CKT_ID 
 --LEFT OUTER JOIN [COWS].[dbo].[CKT_COST] ckc  WITH (NOLOCK)    ON ckt.CKT_ID = ckc.CKT_ID  
 LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] lp  WITH (NOLOCK) on w.PROD_TYPE_ID = lp.PROD_TYPE_ID  
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] lo  WITH (NOLOCK) on w.ORDR_TYPE_ID = lo.ORDR_TYPE_ID  
   
 LEFT OUTER JOIN (
	SELECT DISTINCT ORDR_ID, ISNULL(bb.CTY_NME, CONVERT(VARCHAR, DecryptByKey(csd.CTY_NME))) [CTY_NME]  
    FROM [COWS].[dbo].[ORDR_ADR] bb WITH (NOLOCK) 
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.ORDR_ADR_ID  AND csd.SCRD_OBJ_TYPE_ID=14
    WHERE CIS_LVL_TYPE = 'H5'
    ) oa ON oa.ORDR_ID = ord.ORDR_ID  
 -- Added left join for XNCI date.  
 LEFT OUTER JOIN (SELECT creat_dt[CREAT_DT],ordr_id[ORDR_ID],ROW_NUMBER() OVER (PARTITION BY ordr_id ORDER BY creat_dt DESC) AS dt_rownum   
  FROM (SELECT aa.CREAT_DT,aa.ORDR_ID,aa.CREAT_BY_USER_ID FROM [COWS].[dbo].[ORDR_NTE] aa   
  JOIN [COWS].[dbo].[USER_GRP_ROLE] bb WITH (NOLOCK) ON aa.CREAT_BY_USER_ID = bb.user_id AND GRP_ID = 5 and bb.USER_ID > 1) cc ) orn   
  ON ord.ordr_id = orn.ordr_id AND dt_rownum = 1  
 OUTER APPLY (SELECT TOP 1 ORDR_ID, sh.STDI_DT,sr.STDI_REAS_DES, CMNT_TXT FROM [COWS].[dbo].[ORDR_STDI_HIST] sh WITH (NOLOCK)  
      INNER JOIN [COWS].[dbo].[LK_STDI_REAS] sr WITH (NOLOCK) ON sh.STDI_REAS_ID = sr.STDI_REAS_ID  
      where ord.ORDR_ID = ORDR_ID   
      ORDER BY sh.CREAT_DT DESC) osh    
     
WHERE ord.DMSTC_CD = 1 AND ord.RGN_ID = 1   
 AND (i.PROD_TYPE_CD NOT IN ('MN') OR i.PROD_TYPE_CD IS NULL )  
 AND(ord.ORDR_STUS_ID in (2,5) or tk.TASK_ID = 1000 or (tk.TASK_ID = 213 and tk.STUS_ID = 2) or tk.TASK_ID = 212 )  
 AND ord.H5_FOLDR_ID IS NOT NULL  
 AND i.CREAT_DT BETWEEN @startDate AND @endDate 
 --AND  cast (ISNULL(i.FTN, ord.ORDR_ID) as varchar(20))='DC3O11175255'   
  
ORDER BY cast (ISNULL(i.FTN, ord.ORDR_ID) as varchar(20))
				 
      
 CLOSE SYMMETRIC KEY FS@K3y    
  
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptCCD ' + ',' + CAST(@secured AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc   
 EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  
  