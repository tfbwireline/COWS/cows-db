USE [COWS_Reporting]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author: dlp0278
-- Create date: 3/21/2016 
-- Description:  SP used for Lassie CPE Items View
-- =============================================
ALTER PROCEDURE [dbo].[sp_Lassie_CPE_Itm_View] 

AS
BEGIN TRY

SET NOCOUNT ON;

TRUNCATE TABLE [COWS_Reporting].[dbo].[LassieCpeItemData]

INSERT INTO [COWS_Reporting].[dbo].[LassieCpeItemData]
           ([Order_ID],[ORDR_ID],[SITE_ID],[DEVICE_ID],[CPE_REC_ONLY_CD],[CPE_EQPT_ONLY_CD],[MNTC_CD]
           ,[CNTRC_TYPE_ID],[EQPT_TYPE_ID],[PLSFT_RQSTN_NBR],[RQSTN_DT],[PRCH_ORDR_NBR]
           ,[EQPT_ITM_RCVD_DT],[ORDR_QTY],[EQPT_RCVD_BY_ADID],[CMPL_DT],[RCVD_QTY],[PID]
           ,[DROP_SHP],[SUPPLIER],[RAS_DT],[CPE_CLLI],[DMSTC_CD],[CMPNT_ID],[MDS_DES],[MANF_PART_CD]
           ,[CNTRC_TERM_ID],[CMPNT_FMLY]
           )

	SELECT distinct 
			fsa.FTN AS Order_ID
			,fsa.ORDR_ID 
		   ,foc.SITE_ID
		   ,itm.DEVICE_ID
		   ,fsa.CPE_REC_ONLY_CD
		   ,fsa.CPE_EQPT_ONLY_CD
		   ,itm.MNTC_CD
		   ,itm.CNTRC_TYPE_ID
		   ,itm.EQPT_TYPE_ID
		   ,itm.PLSFT_RQSTN_NBR
		   ,itm.RQSTN_DT
		   ,itm.PRCH_ORDR_NBR
		   ,itm.EQPT_ITM_RCVD_DT
		   ,itm.ORDR_QTY
		   ,itm.EQPT_RCVD_BY_ADID
		   ,itm.CMPL_DT
		   ,itm.RCVD_QTY
		   ,itm.PID
		   ,itm.DROP_SHP
		   ,itm.SUPPLIER
		   ,ord.RAS_DT
		   ,ord.CPE_CLLI
		   ,ord.DMSTC_CD
		   ,itm.CMPNT_ID 
		   ,itm.MDS_DES 
		   ,itm.MANF_PART_CD
		   ,itm.CNTRC_TERM_ID
		   ,itm.CMPNT_FMLY		   
	
	FROM [COWS].[dbo].[FSA_ORDR] fsa WITH (NOLOCK)
	INNER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc WITH (NOLOCK) ON foc.ORDR_ID = fsa.ORDR_ID
	INNER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] itm WITH (NOLOCK) 
					ON itm.ORDR_ID = fsa.ORDR_ID
					AND ISNULL(itm.DEVICE_ID,'')<> ''
	INNER JOIN [COWS].[dbo].[ORDR] ord WITH (NOLOCK) 
					ON ord.ORDR_ID = fsa.ORDR_ID
	
	WHERE ord.ORDR_CAT_ID = 6 

END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_LassieCpeItemData ' + ': '
	SET @Desc=@Desc 
	EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH

