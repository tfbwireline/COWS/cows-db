/*  
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD  
-- EXEC COWS_Reporting.dbo.sp_AMNCIRptSprintFacilityCCD 0, 0, '9/1/2010','12/16/2011', 1  
-- [dbo].[sp_AMNCIRptSprintFacilityCCD] 1  
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME 
-- =============================================  
*/  
ALTER PROCEDURE [dbo].[sp_AMNCIRptSprintFacilityCCD]   
 @secured  BIT = 1  
  
  
AS  
BEGIN TRY  
 SET NOCOUNT ON;  
--DECLARE @secured   BIT = 0  
  
  
  
  
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
--DECLARE @secured BIT=0 -- for testing purposes only  
  
DELETE FROM [COWS_Reporting].[dbo].[AMNCISprintFacilityCCDData]  WHERE  
[COWS_Reporting].[dbo].[AMNCISprintFacilityCCDData].[Order Tracking No] in  
(SELECT [Order Tracking No]  FROM [COWS_Reporting].[dbo].[AMNCISprintFacilityCCDData] as ovs   
INNER JOIN [COWS].[dbo].[FSA_ORDR] as ov on ovs.[Order Tracking No] = CAST (ov.FTN AS VARCHAR(20))  
LEFT OUTER JOIN [COWS].[dbo].[ORDR] as odr  
on odr.ORDR_ID = ov.ORDR_ID)   
  
INSERT INTO [COWS_Reporting].[dbo].[AMNCISprintFacilityCCDData]   
  
  
 SELECT DISTINCT  
 cast (i.FTN as varchar(20)) [Order Tracking No],  
 --si.CUST_NME  [Customer Name],  
 CASE  
 WHEN @secured = 0 and z.CSG_LVL_ID =0 THEN si.CUST_NME  
 WHEN @secured = 0 and z.CSG_LVL_ID >0 AND si.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER'   
 WHEN @secured = 0 and z.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(si.CUST_NME2)) IS NULL THEN NULL   
 ELSE coalesce(si.CUST_NME,si.CUST_NME2,'') END [Customer Name], --Secured  
   
 0 [Customer Reference Number],  
 CASE  
  WHEN cd.NEW_CCD_DT IS NOT NULL THEN cd.NEW_CCD_DT END [Implementation Date],  
 --tac.FRST_NME + ' ' + tac.LST_NME [Implementation Contact],  
 coalesce(tac.NME, tac.NME2,'') AS [Implementation Contact],  
 coalesce(sis.NME, sis.NME2,'') [RAM],  
 --sis.FRST_NME + ' '  + sis.LST_NME [RAM],  
 g.FULL_NME [CPM Contact],  
 cast(a.SHPMT_TRK_NBR as integer) [Order Tracking No1],  
 CASE   
  WHEN z.ORDR_CAT_ID = 1 THEN w.CUST_WANT_DT  
  WHEN z.ORDR_CAT_ID = 6 THEN i.CUST_WANT_DT  
  WHEN z.ORDR_CAT_ID = 2 THEN i.CUST_WANT_DT  END [Sprint Target Delivery Date],  
 h.PROD_TYPE_DES [Product],  
 i.CUST_CMMT_DT  [Customer Commit Date],  
 ISNULL(j.SALS_CHNL_DES, 'Americas/Emerging Markets') [Sales Region],  
 --l.CTRY_NME [Sales Sub Region],  
 '' [Sales Sub-Region],  
 --u.CTRY_NME [Origination Country],
 CASE WHEN (t.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE u.CTRY_NME END [Origination Country], /*Monir 06062018*/    
 ' ' [Termination Country],  
 o.ORDR_STUS_DES [Order Status],  
 q.CCD_MISSD_REAS_DES [CWD Missed Reason],  
 r.PLN_NME [PL #],  
 mg.FULL_NME [Manager],  
 mg.FULL_NME [Manager_2],  
 CASE  
  WHEN cd.NEW_CCD_DT <= i.CUST_CMMT_DT THEN 1   
  WHEN cd.NEW_CCD_DT >   i.CUST_CMMT_DT  THEN 0 END [CCD Met #],   
 1 [CCD Met All],  
 CASE    
  WHEN DATEPART(DW, i.CREAT_DT) = 1 THEN i.CREAT_DT + 3  
  WHEN DATEPART(DW, i.CREAT_DT) = 2 THEN i.CREAT_DT + 2  
  WHEN DATEPART(DW, i.CREAT_DT) = 3 THEN i.CREAT_DT + 1  
  WHEN DATEPART(DW, i.CREAT_DT) = 5 THEN i.CREAT_DT - 1  
  WHEN DATEPART(DW, i.CREAT_DT) = 6 THEN i.CREAT_DT - 2   
  WHEN DATEPART(DW, i.CREAT_DT) = 7 THEN i.CREAT_DT - 3  
  ELSE i.CREAT_DT  END  [Week],  
 cast(DATEPART(M, DATEADD(M, 0, i.CREAT_DT)) as varchar(2)) + '/01/' + cast(DATEPART(YEAR, DATEADD(M, 0, i.CREAT_DT))   as varchar(4)) [Month],  
  
 --cast(m.MRC_CHG_AMT as money) [MRC],  
   ISNULL(CAST(b5.MRC_CHG_AMT AS money), 0) + ISNULL(CAST(b6.MRC_CHG_AMT AS money), 0)    
  + ISNULL(CAST(b1.MRC_CHG_AMT AS money), 0)+ ISNULL(CAST(b2.MRC_CHG_AMT AS money), 0)   
  + ISNULL(CAST(b3.MRC_CHG_AMT AS money), 0) + ISNULL(CAST(b4.MRC_CHG_AMT AS float), 0)   
    + ISNULL(CAST(b7.MRC_CHG_AMT AS money), 0) [MRC],  
 --cast(m.NRC_CHG_AMT as money) [NRC],  
  
  ISNULL(CAST(b5.NRC_CHG_AMT AS money), 0) +  ISNULL(CAST(b6.NRC_CHG_AMT AS money), 0)    
   +  ISNULL(CAST(b1.NRC_CHG_AMT AS money), 0) +  ISNULL(CAST(b2.NRC_CHG_AMT AS money), 0)   
   + ISNULL(CAST(b3.NRC_CHG_AMT AS money), 0)   +  ISNULL(CAST(b4.NRC_CHG_AMT AS money), 0)  
    +  ISNULL(CAST(b7.NRC_CHG_AMT AS money), 0) [NRC],  
 '' [Asset Region],  
 i.INSTL_SOLU_SRVC_DES [Order Description],  
 CASE   
 WHEN tk.TASK_ID = 1001 and z.ORDR_STUS_ID = 2 then CONVERT(VARCHAR(10), tk.CREAT_DT, 101) END [Bill Clear Date],  
 CONVERT(VARCHAR(10), i.CUST_WANT_DT, 101)  [CWD],  
 ' '[End User],  
 fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES [Access Speed],  
 n.ORDR_TYPE_DES [Order Type],  
 s.ORDR_SUB_TYPE_DES [Order Sub Type],  
 i.CXR_SRVC_ID [F31]  
  
 FROM [COWS].[dbo].[ORDR] z WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] i WITH (NOLOCK) ON z.ORDR_ID = i.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = i.ORDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[IPL_ORDR] w WITH (NOLOCK) ON z.ORDR_ID = w.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_GOM_XNCI] a WITH (NOLOCK) ON z.ORDR_ID = a.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] b WITH (NOLOCK) ON z.ORDR_ID = b.ORDR_ID   
  --LEFT OUTER JOIN [COWS].[dbo].[CKT] d WITH (NOLOCK) ON a.ORDR_ID = d.ORDR_ID   
  --LEFT OUTER JOIN [COWS].[dbo].[USER_WFM_ASMT] e WITH (NOLOCK) ON a.ORDR_ID = e.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] h WITH (NOLOCK) ON i.PROD_TYPE_CD = h.FSA_PROD_TYPE_CD   
   
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID,   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) [FRST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [LST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) + ' '+ CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [NME2],  
     CONVERT(varchar, DecryptByKey(csd2.CUST_EMAIL_ADR)) [EMAIL_ADR2],   
       
     bb.FRST_NME  as [FRST_NME],   
     bb.LST_NME   as [LST_NME],   
     bb.FRST_NME  + ' '+ bb.LST_NME   as [NME],  
     bb.EMAIL_ADR as [EMAIL_ADR],   
  bb.CNTCT_TYPE_ID   
  FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
  [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID  
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd2.SCRD_OBJ_TYPE_ID=15    
  WHERE bb.ROLE_ID = 13) tac on z.ORDR_ID  = tac.ORDR_ID   
  
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID,   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) [FRST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [LST_NME2],   
     CONVERT(varchar, DecryptByKey(csd2.FRST_NME)) + ' '+ CONVERT(varchar, DecryptByKey(csd2.LST_NME)) [NME2],  
     CONVERT(varchar, DecryptByKey(csd2.CUST_EMAIL_ADR)) [EMAIL_ADR2],   
       
     bb.FRST_NME  as [FRST_NME],   
     bb.LST_NME   as [LST_NME],   
     bb.FRST_NME  + ' '+ bb.LST_NME   as [NME],  
     bb.EMAIL_ADR as [EMAIL_ADR],   
  bb.CNTCT_TYPE_ID   
  FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN   
  [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=bb.ORDR_CNTCT_ID  AND csd2.SCRD_OBJ_TYPE_ID=15   
  WHERE bb.ROLE_ID = 11) sis on z.ORDR_ID  = sis.ORDR_ID   
    
    
  LEFT OUTER JOIN (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID   
     FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
     LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
    FROM (SELECT DISTINCT ASMT_DT, ORDR_ID   
     FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
    WHERE usr.ORDR_ID  = bb.ORDR_ID )  
    AND bb.ASN_USER_ID  = (SELECT MAX(ASN_USER_ID)   
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
     WHERE uas.ORDR_ID = bb.ORDR_ID)) ur ON ur.ORDR_ID = i.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] g WITH (NOLOCK) ON ur.ASN_USER_ID = g.USER_ID   
     LEFT OUTER JOIN [COWS].[dbo].[LK_USER] mg WITH (NOLOCK) ON g.MGR_ADID = mg.USER_ADID   
  LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] k WITH (NOLOCK) ON z.ORDR_ID = k.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_SALS_CHNL] j WITH (NOLOCK) ON j.SALS_CHNL_ID = k.SALS_CHNL_ID   
  --LEFT OUTER JOIN [COWS].[dbo].[USER_WFM] m WITH (NOLOCK) ON m.PROD_TYPE_ID = h.PROD_TYPE_ID   
    
  LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.STUS_ID, bb.CREAT_DT, bb.ORDR_ID   
   FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
    FROM (SELECT ACT_TASK_ID, ORDR_ID   
     FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
    WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk ON tk.ORDR_ID = i.ORDR_ID   
      
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_STUS] o WITH (NOLOCK) ON z.ORDR_STUS_ID = o.ORDR_STUS_ID   
  LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] p WITH (NOLOCK) ON p.ORDR_ID = a.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[LK_CCD_MISSD_REAS] q WITH (NOLOCK) ON p.CCD_MISSD_REAS_ID = q.CCD_MISSD_REAS_ID   
  LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] r WITH (NOLOCK) ON r.FTN = i.FTN   
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON  r.TOC_CTRY_CD = l.CTRY_CD  
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.SOI_CD,   
    CONVERT(varchar, DecryptByKey(csd.CUST_NME)) [CUST_NME2],  
    bb.CUST_NME AS [CUST_NME]  
    FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  WITH (NOLOCK)  
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
   WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
    FROM (SELECT SOI_CD, ORDR_ID   
     FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi  
    WHERE soi.ORDR_ID = bb.ORDR_ID)) si ON Z.ORDR_ID  = si.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] m on i.ORDR_ID = m.ORDR_ID   
    
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] n on i.ORDR_TYPE_CD = n.FSA_ORDR_TYPE_CD  
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_SUB_TYPE] s on i.ORDR_SUB_TYPE_CD = s.ORDR_SUB_TYPE_CD  
   
    
 LEFT OUtER JOIN [COWS].[dbo].[H5_FOLDR] t on z.H5_FOLDR_ID = t.H5_FOLDR_ID  
 LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY]u on t.CTRY_CD = u.CTRY_CD 
 LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=t.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6  
LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD 
 --LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] t on z.RGN_ID = t.RGN_ID  
   
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.NEW_CCD_DT, bb.CCD_HIST_ID  
     FROM [COWS].[dbo].[CCD_HIST] bb LEFT OUTER JOIN  
  [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)  
   FROM (SELECT CCD_HIST_ID, ORDR_ID FROM [COWS].[dbo].[CCD_HIST] ) ccd  
   WHERE ccd.ORDR_ID = bb.ORDR_ID)) cd on  z.ORDR_ID = cd.ORDR_ID   
  
 LEFT OUTER JOIN (SELECT bb.BIC_CD, bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT,   
 bb.NRC_CHG_AMT FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
    JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
    WHERE bb.BILL_ITEM_TYPE_ID = 1) b1 on z.ORDR_ID = b1.ORDR_ID   
   
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 2) b2 on z.ORDR_ID = b2.ORDR_ID  
  
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 3) b3 on z.ORDR_ID = b3.ORDR_ID  
   
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 4) b4 on z.ORDR_ID = b4.ORDR_ID  
  
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.BIC_CD, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 5 AND bb.LINE_ITEM_DES LIKE '%Access%') b5 on z.ORDR_ID = b5.ORDR_ID  
  
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.BIC_CD, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 5 AND bb.LINE_ITEM_DES LIKE '%PORT%') b6 on z.ORDR_ID = b6.ORDR_ID  
   
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 7) b7 on z.ORDR_ID = b7.ORDR_ID  
   
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 8) b8 on z.ORDR_ID = b8.ORDR_ID  
  
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT   
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID   
 WHERE bb.BILL_ITEM_TYPE_ID = 9) b9 on z.ORDR_ID = b9.ORDR_ID  
  
 --LEFT OUTER JOIN COWS.dbo.ORDR_MS s on s.ORDR_ID = a.ORDR_ID   
 WHERE i.TSUP_RFQ_NBR IS NOT NULL  
  --AND j.SALS_CHNL_ID = 6 -- Americas/Emerging Markets  
  --AND b.CIS_LVL_TYPE = 'H5'  
  AND z.PLTFRM_CD = 'SF'  
  AND cd.NEW_CCD_DT IS NOT NULL  
  --AND tk.TASK_ID = 1001  
  AND z.RGN_ID = 1  
  
  
Select [Order Tracking No], [Customer Name], [Customer Reference Number],  CONVERT(VARCHAR(10), [Implementation Date], 101) [Implementation Date], [Implementation Contact], [RAM], [CPM contact], [Order Tracking No1],    
CONVERT(VARCHAR(10), [Sprint Target Delivery Date], 101) [Sprint Target Delivery Date], [Product],  CONVERT(VARCHAR(10), [Customer Commit Date], 101) [Customer Commit Date], [Sales Region], [Sales Sub-Region], [Origination Country], [Termination Country],
 [Order Status], [CWD Missed Reason],  
 [PL #], [Manager], [Manager_2], [CCD Met #],[CCD Met All], [Week], [Month], [MRC], [NRC], [Asset Region],  [Order Description],   CONVERT(VARCHAR(10), [Bill Clear Date], 101) [Bill Clear Date],  CONVERT(VARCHAR(10), [CWD], 101) [CWD], [End User],   
 [Access Speed], [Order Type], [Order Sub Type], [F31] from [COWS_Reporting].[dbo].[AMNCISprintFacilityCCDData]  
  
        
 CLOSE SYMMETRIC KEY FS@K3y   
        
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptSprintFacilityCCD ' + ',' + CAST(@secured AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc   
 EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  