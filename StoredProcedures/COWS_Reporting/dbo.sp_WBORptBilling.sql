USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_WBORptBilling]    Script Date: 06/19/2018 18:17:52 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/*
-- =============================================
-- Author:		
-- Create date: 
-- Description: Extract WBO Billing report data
--					QueryNumber: 1 = Daily
--								 0 = Specify start date & end date: 'mm/dd/yyyy'
-- 1- CI554013 change date: 01/16/2013
--	Added union to include NCCO to on demand report
-- 2- CI554013 
--	Added union to include NCCO to canned report
-- 3- CI554013 change date: 03/15/2013 Added new columns.
-- CI554013 change date: 5/8/2013
--          changed fs.CPE_REC_ONLY_CD to display 'CPE records only' instead of "Y"
-- CI554013 change date: 5/8/2013
--          edit NUA in select to also show DC with sub type of MPLSVAS
--  XXXXXXX Change date: 02/25/2015
--			Edited due to some FTN billing detail was omitting from Report 
--			So FSA_ORDR_BILL_LINE_ITEM_ID (unique) was included in inner query
-- dlp0278  4/3/2015  changed report to not run on the weekend but on Monday report for 3 days.
-- Md Monir 4/7/2015  changed report Optimize Query.
-- Md Monir 3/11/2016 changed report to exclude MACH5 Order.
-- Md Monir 6/20/2018 ctry_CODE was ambigious
[dbo].[sp_WBORptBilling] 0,null,null, null,1
-- =============================================
*/
ALTER PROCEDURE [dbo].[sp_WBORptBilling]
--	 Add the parameters for the stored procedure here
	    @QueryNumber		INT = 0,
		@startDate  	DateTime= null,
	    @endDate		DateTime= null,
	    @OrderType			VARCHAR(25)= NULL,
	    @SecuredUser		bit = 0
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @stDt		Datetime
	DECLARE @endDt		   Datetime

	
--DECLARE @QueryNumber		INT = 1
--DECLARE @inStartDtStr  	VARCHAR(10)= null     ---  These are here for testing
--DECLARE @inEndDtStr		VARCHAR(10)= null
--DECLARE @OrderType			VARCHAR(25)= NULL
--DECLARE @SecuredUser		bit = 0	

	
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
	
	IF @QueryNumber=0
		BEGIN

			SET @stDt=@startDate
			SET @endDt=@endDate
			SET @endDt = DATEADD(day, 1, @endDt)				

			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			--SET @endDt=DATEADD(day, 1, @endDt) 
												
		END	
	ELSE IF @QueryNumber=1
    	BEGIN    	

			SET @endDt=cast(CONVERT(varchar(8), CONVERT(date, GETDATE()), 112) AS datetime) 
			IF datename(dw,getdate()) = 'Monday'
				BEGIN
					SET @stDt=cast(CONVERT(varchar(8), CONVERT(date, DATEADD(day, -3, GETDATE())), 112) AS datetime)
				END
			ELSE
				BEGIN	
					SET @stDt=cast(CONVERT(varchar(8), CONVERT(date, DATEADD(day, -1, GETDATE())), 112) AS datetime)  
				END
		END

--print datename(dw,getdate())


		IF @OrderType IS NOT NULL
			BEGIN
Select 
	 [FTN],[Billing Cycle],[Order Type],[Order Sub Type],[H1] ,[H5],[Customer Name],[Site Address],[Site City],[Province] 
	,[Site Country],[Intl Postal Code],[Phone #],[Network Indicator],[IP Product Type],[Intl Carrier Code]
	,[Carrier Partner Name],[PL#],[PL Sequence],[CircuitType],[Circuit #],[Circuit Bandwidth],[AOI],[NUA],[Port type]
	,[Port Speed],[Port Activation Date],[Port Disco Date],[Usage Type],[Charge Code],[Charge Description],[Quantity]
	,[MRC],[NRC],[Bill Clear/Bill Stop Date],[Comments] ,[Half Tunnel],[Half Tunnel Quantity],[CPE Additional Info.],
	[SCA Number]
	from (
	SELECT  distinct 
				ISNULL(fs.FTN, '') [FTN],
				ISNULL (fs.CURR_BILL_CYC_CD, '') [Billing Cycle],
				ISNULL(b.FSA_ORDR_TYPE_DES, '') [Order Type],
				ISNULL(t.ORDR_SUB_TYPE_DES, '') [Order Sub Type],
				h1.CUST_ID [H1],
				fs.CUST_ID [H5],
				CASE
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(fs.CUST_NME))	
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(fs.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(fs.CUST_NME)) IS NULL THEN NULL 
					--ELSE CONVERT(varchar, DecryptByKey(fs.CUST_NME))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID =0 THEN fs.CUST_NME
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  fs.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  fs.CUST_NME2 IS NULL THEN NULL 
					ELSE COALESCE(fs.CUST_NME2,fs.CUST_NME,'')
					END [Customer Name], --Secured
				CASE
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(od.STREET_ADR_1))	
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.STREET_ADR_1)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.STREET_ADR_1)) IS NULL THEN NULL 
					--ELSE CONVERT(varchar, DecryptByKey(od.STREET_ADR_1))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID =0 THEN od.STREET_ADR_1
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  od.STREET_ADR_12 IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  od.STREET_ADR_12 IS NULL THEN NULL 
					ELSE COALESCE(od.STREET_ADR_12,od.STREET_ADR_1,'')
					END [Site Address], --Secured

				CASE
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(od.CTY_NME))	
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.CTY_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.CTY_NME)) IS NULL THEN NULL 
					--ELSE CONVERT(varchar, DecryptByKey(od.CTY_NME))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID =0 THEN od.CTY_NME
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  od.CTY_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  od.CTY_NME2 IS NULL THEN NULL 
					ELSE COALESCE(od.CTY_NME2,od.CTY_NME,'')
					END [Site City], --Secured
				
				CASE
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(od.PRVN_NME))	
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.PRVN_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.PRVN_NME)) IS NULL THEN NULL 
					--ELSE CONVERT(varchar, DecryptByKey(od.PRVN_NME))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID =0 THEN od.PRVN_NME
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  od.PRVN_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  od.PRVN_NME2 IS NULL THEN NULL 
					ELSE COALESCE(od.PRVN_NME2,od.PRVN_NME,'')
					END [Province], --Secured
				
				ch.CTRY_CD [Site Country],
				
				CASE
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(od.ZIP_PSTL_CD))	
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.ZIP_PSTL_CD)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					--WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.ZIP_PSTL_CD)) IS NULL THEN NULL 
					--ELSE CONVERT(varchar, DecryptByKey(od.ZIP_PSTL_CD))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID =0 THEN od.ZIP_PSTL_CD
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  od.ZIP_PSTL_CD2 IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID >0 AND  od.ZIP_PSTL_CD2 IS NULL THEN NULL 
					ELSE COALESCE(od.ZIP_PSTL_CD2,od.ZIP_PSTL_CD,'')
					END [Intl Postal Code], --Secured

				ISNULL(''''+ ph.PHN_NBR , '') [Phone #],
				ISNULL(fs.TPORT_ETHRNT_NRFC_INDCR, '') [Network Indicator], 
				ISNULL(v.PROD_TYPE_DES, '') [IP Product Type],
				ISNULL(fs.CXR_ACCS_CD, '') [Intl Carrier Code],
				ISNULL(r.PLTFRM_NME, '') [Carrier Partner Name],
				ISNULL(nc.PLN_NME, '') [PL#],
				ISNULL(nc.PLN_SEQ_NBR, '')[PL Sequence],
				ISNULL(nc.CKT_NME, '') [CircuitType],	
				ISNULL(nc.FMS_CKT_ID, '') [Circuit #],
				ISNULL(fs.TTRPT_SPD_OF_SRVC_BDWD_DES, '') [Circuit Bandwidth],
				'N/A' [AOI],
				CASE 
				   WHEN nc.NUA_ADR IS NOT NULL THEN nc.NUA_ADR
				   WHEN fs.ORDR_TYPE_CD = 'DC' and t.ORDR_SUB_TYPE_ID = 14 THEN ISNULL(fov.NW_ADR,'') 
				   ELSE '' END[NUA],
				ISNULL(fs.TPORT_PORT_TYPE_ID, '') [Port type],
				fs.TTRPT_SPD_OF_SRVC_BDWD_DES [Port Speed],
				ni.ACTV_DT [Port Activation Date],
				ni.DSCNCT_DT [Port Disco Date],
				ISNULL(fs.TPORT_BRSTBL_USAGE_TYPE_CD, '') [Usage Type],
				
				CASE
					WHEN (m.BIC_CD IS NULL or LEN(RTRIM(LTRIM(m.BIC_CD))) = 0) 
					AND (m.CHG_CD IS NULL or LEN(RTRIM(LTRIM(m.CHG_CD))) = 0) 
					AND (m.ITEM_CD IS NULL or LEN(RTRIM(LTRIM(m.ITEM_CD))) = 0) THEN cb.SRVC_LINE_ITEM_CD
					WHEN(m.BIC_CD IS NULL or LEN(RTRIM(LTRIM(m.BIC_CD))) = 0) 
					AND (m.CHG_CD IS NULL or LEN(RTRIM(LTRIM(m.CHG_CD))) = 0) THEN m.ITEM_CD
					ELSE ISNULL(BIC_CD, m.CHG_CD) END [Charge Code],

				CASE
					WHEN (m.ITEM_DES IS NULL OR LEN(RTRIM(LTRIM(m.ITEM_DES))) = 0) 
					AND (m.LINE_ITEM_DES IS NULL OR LEN(RTRIM(LTRIM(m.LINE_ITEM_DES))) = 0) THEN cb.MDS_DES					 
					WHEN (m.LINE_ITEM_DES IS NULL OR LEN(RTRIM(LTRIM(m.LINE_ITEM_DES))) = 0) THEN m.ITEM_DES
					ELSE m.LINE_ITEM_DES END [Charge Description],
					
				m.LINE_ITEM_QTY [Quantity],
				CAST (m.MRC_CHG_AMT AS MONEY) [MRC],
				CAST (m.NRC_CHG_AMT AS MONEY) [NRC],
				tk.CREAT_DT [Bill Clear/Bill Stop Date],
				ISNULL(o.CMNT_TXT, '') [Comments] 
				,case
					when fov.VAS_TYPE_CD = 'HLFTN' Then 'Yes'
					else 'No'
					end[Half Tunnel]
				,case
					when fov.VAS_TYPE_CD = 'HLFTN' Then fov.VAS_QTY
					else ' '
					end[Half Tunnel Quantity]
				,case
					when fs.CPE_REC_ONLY_CD = 'Y' Then 'CPE records only'
					else ISNULL(fs.CPE_REC_ONLY_CD, '')
					end[CPE Additional Info.]
				,m.FSA_ORDR_BILL_LINE_ITEM_ID	/*Added due to Missing BILL_LINE_ITEM in final report*/
				,RTRIM(fs.SCA_NBR) [SCA Number]
				FROM
				(SELECT bb.FTN, bb.ORDR_ID, bb.ORDR_ACTN_ID, fcpl.TPORT_ETHRNT_NRFC_INDCR, 
                                    bb.TTRPT_ALT_ACCS_PRVDR_CD, 
                                    bb.TTRPT_SPD_OF_SRVC_BDWD_DES, bb.TPORT_BRSTBL_USAGE_TYPE_CD,bb.ORDR_SUB_TYPE_CD, 
                                    bb.TPORT_PORT_TYPE_ID, bb.CXR_ACCS_CD,
                                    bb.ORDR_TYPE_CD, bb.PROD_TYPE_CD, bb.CPE_PHN_NBR,
                                    bb.CPE_REC_ONLY_CD , 
									dd.CUST_ID, dd.SOI_CD, dd.CUST_NME, dd.CURR_BILL_CYC_CD,dd.SRVC_SUB_TYPE_ID,
									bb.SCA_NBR,
									CONVERT(varchar, DecryptByKey(csd.CUST_NME)) AS  [CUST_NME2]
                                    FROM [COWS].[dbo].[FSA_ORDR] bb with (nolock) JOIN
                                    [COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID  Join 
                                    [COWS].[dbo].[FSA_ORDR_CUST] dd with (nolock) ON bb.ORDR_ID = dd.ORDR_ID LEFT OUTER JOIN 
                                    [COWS].[dbo].[CUST_SCRD_DATA] csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=dd.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
                                    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = bb.ORDR_ID
									/*
                                    WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
                                    FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
                                    WHERE  bb.FTN = fsa.FTN)  
									AND dd.CIS_LVL_TYPE = 'H5' AND dd.CUST_ID IS NOT NULL
									AND LEN(dd.CUST_ID) <> 0 AND dd.CURR_BILL_CYC_CD <> '43'
									*/
									/** MD MONIR  To exclude mach5 Order **/
									JOIN [COWS].[dbo].[LK_ORDR_CAT] LC with (nolock) ON  cc.ORDR_CAT_ID=LC.ORDR_CAT_ID
                                    WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
										FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
										WHERE  bb.FTN = fsa.FTN)  
									AND dd.CIS_LVL_TYPE = 'H5' AND dd.CUST_ID IS NOT NULL
									AND LEN(dd.CUST_ID) <> 0 AND dd.CURR_BILL_CYC_CD <> '43'
									----select * from [COWS].[dbo].[LK_ORDR_CAT]
									AND LC.ORDR_CAT_ID <> 6
									/** MD MONIR  To exclude mach5 Order **/
									
									) fs LEFT OUTER JOIN 
						
				[COWS].[dbo].[lk_FSA_ORDR_TYPE] b with (nolock) on fs.ORDR_TYPE_CD = b.FSA_ORDR_TYPE_CD LEFT OUTER JOIN
				[COWS].[dbo].[ORDR] p with (nolock) on fs.ORDR_ID = p.ORDR_ID LEFT OUTER JOIN
				[COWS].[dbo].[PLTFRM_MAPNG] d with (nolock) on p.PLTFRM_CD = d.PLTFRM_CD LEFT OUTER JOIN
				[COWS].[dbo].[LK_PLTFRM]r with (nolock) on d.PLTFRM_CD =  r.PLTFRM_CD LEFT OUTER JOIN
				--[COWS].[dbo].[ORDR_GRP] q with (nolock) on p.ORDR_ID = q.ORDR_ID LEFT OUTER JOIN
				--[COWS].[dbo].[PROD_ORDR_TYPE] e with (nolock) on q.PROD_TYPE_ID = e.PROD_TYPE_ID LEFT OUTER JOIN
				--[COWS].[dbo].[ORDR_ADR] i with (nolock) on fs.ORDR_ID = i.ORDR_ID LEFT OUTER JOIN   /* Removed due to FTN billing detail was removed */
				
				(SELECT bb.ORDR_ID, 
						bb.CTRY_CD, 
						bb.CTY_NME, 
						bb.STREET_ADR_1, 
						bb.STREET_ADR_2, 
						bb.STT_CD, 
						bb.ZIP_PSTL_CD, 
						bb.PRVN_NME, 
						CONVERT(varchar, DecryptByKey(csd.CTY_NME))  AS [CTY_NME2], 
						CONVERT(varchar, DecryptByKey(csd.STREET_ADR_1)) AS [STREET_ADR_12],
						CONVERT(varchar, DecryptByKey(csd.ZIP_PSTL_CD))   AS [ZIP_PSTL_CD2], 
						CONVERT(varchar, DecryptByKey(csd.STT_PRVN_NME))   AS [PRVN_NME2]
						FROM [COWS].[dbo].[ORDR_ADR] bb  with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID LEFT OUTER JOIN 
						[COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.ORDR_ADR_ID  AND csd.SCRD_OBJ_TYPE_ID=14 
						WHERE bb.CIS_LVL_TYPE = 'H5'  AND bb.CTRY_CD IS NOT NULL) od
						on fs.ORDR_ID = od.ORDR_ID  LEFT OUTER JOIN		
						
				[COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] m with (nolock) on m.ORDR_ID = fs.ORDR_ID LEFT OUTER JOIN
				--[COWS].[dbo].[ORDR_MS] n with (nolock) on fs.ORDR_ID = n.ORDR_ID LEFT OUTER JOIN
				[COWS].[dbo].[TRPT_ORDR] o with (nolock) on fs.ORDR_ID = o.ORDR_ID LEFT OUTER JOIN
				[COWS].[dbo].[LK_ORDR_SUB_TYPE] t with (nolock) on fs.ORDR_SUB_TYPE_CD = t.ORDR_SUB_TYPE_CD LEFT OUTER JOIN
				--[COWS].[dbo].[IPL_ORDR] u with (nolock) on p.ORDR_ID = u.ORDR_ID LEFT OUTER JOIN
				[COWS].[dbo].[LK_PROD_TYPE] s with (nolock) on fs.PROD_TYPE_CD = s.FSA_PROD_TYPE_CD LEFT OUTER JOIN
				[COWS].[dbo].[FSA_ORDR_VAS] fov with (nolock) on fs.ORDR_ID = fov.ORDR_ID LEFT OUTER JOIN

				
				(SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE bb.CIS_LVL_TYPE = 'H1') h1
						on fs.ORDR_ID = h1.ORDR_ID /*JOIN
						
				(SELECT bb.CUST_ID, bb.ORDR_ID, bb.SOI_CD, bb.CUST_NME, bb.CURR_BILL_CYC_CD, 
						bb.SRVC_SUB_TYPE_ID  FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  with (nolock) JOIN 
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE bb.CIS_LVL_TYPE = 'H5' AND bb.CUST_ID IS NOT NULL
						AND LEN(bb.CUST_ID) <> 0 AND bb.CURR_BILL_CYC_CD <> '43' ) h5
						on fs.ORDR_ID = h5.ORDR_ID */ LEFT OUTER JOIN
										
				(SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID 
 						FROM [COWS].[dbo].[ACT_TASK] bb  with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE bb.TASK_ID = 1001) tk on fs.ORDR_ID = tk.ORDR_ID LEFT OUTER JOIN
						
								
				(SELECT bb.NRM_CKT_ID, bb.FTN, bb.PLN_NME, bb.PLN_SEQ_NBR, bb.CKT_NME, bb.FMS_CKT_ID, bb.NUA_ADR 
 						FROM [COWS].[dbo].[NRM_CKT] bb  with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.FTN = cc.FTN 
						WHERE bb.NRM_CKT_ID = (SELECT MAX(NRM_CKT_ID)
						FROM (SELECT NRM_CKT_ID, FTN FROM [COWS].[dbo].[NRM_CKT]  with (nolock) ) nct 
						WHERE nct.FTN = bb.FTN)) nc on fs.FTN = nc.FTN LEFT OUTER JOIN	
				
				--(SELECT bb.ORDR_ID, bb.CTRY_CD, bb.ADR_TYPE_ID FROM [COWS].[dbo].[ORDR_ADR] bb JOIN
				--		[COWS].[dbo].[ORDR] cc on bb.ORDR_ID = cc.ORDR_ID
				--		WHERE bb.ADR_TYPE_ID = 6 or bb.ADR_TYPE_ID = 18 AND bb.CIS_LVL_TYPE = 'H5') cd 
				--		on fs.ORDR_ID = cd.ORDR_ID LEFT OUTER JOIN
							
				[COWS].[dbo].[LK_PROD_TYPE] v on fs.PROD_TYPE_CD = v.FSA_PROD_TYPE_CD LEFT OUTER JOIN

						
				(SELECT DISTINCT bb.FTN, bb.SRVC_INSTC_OBJ_ID, bb.ACTV_DT, bb.DSCNCT_DT
					FROM [COWS].[dbo].[NRM_SRVC_INSTC] bb  with (nolock) LEFT OUTER JOIN
					[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.FTN = cc.FTN 
					WHERE bb.SRVC_INSTC_OBJ_ID = (SELECT MAX(SRVC_INSTC_OBJ_ID)
					FROM (SELECT SRVC_INSTC_OBJ_ID, FTN FROM [COWS].[dbo].[NRM_SRVC_INSTC] with (nolock)  )nsi
					WHERE nsi.FTN = bb.FTN )AND bb.ACTV_DT = (SELECT MAX(ACTV_DT) FROM 
					(SELECT ACTV_DT, SRVC_INSTC_OBJ_ID 
					FROM [COWS].[dbo].[NRM_SRVC_INSTC]  with (nolock) ) act 
					WHERE act.SRVC_INSTC_OBJ_ID = bb.SRVC_INSTC_OBJ_ID)) ni on fs.FTN = ni.FTN LEFT OUTER JOIN 
					
				(SELECT distinct cc.ORDR_ID,cc.SRVC_LINE_ITEM_CD, cc.FSA_CPE_LINE_ITEM_ID, cc.MDS_DES, cc.TTRPT_ACCS_TYPE_DES, cc.TTRPT_SPD_OF_SRVC_BDWD_DES
					FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb  with (nolock) 
					JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] cc  
					 with (nolock) on cc.ORDR_ID = bb.ORDR_ID 
					) cb on  m.FSA_CPE_LINE_ITEM_ID = cb.FSA_CPE_LINE_ITEM_ID	LEFT OUTER JOIN
					
				[COWS].[dbo].[H5_FOLDR] ch on p.H5_H6_CUST_ID = ch.CUST_ID LEFT OUTER JOIN
					
				(SELECT bb.ORDR_ID, bb.PHN_NBR, bb.CIS_LVL_TYPE, bb.CNTCT_TYPE_ID 
					FROM [COWS].[dbo].[ORDR_CNTCT] bb with (nolock) LEFT OUTER JOIN
					[COWS].[dbo].[FSA_ORDR] cc with (nolock) on cc.ORDR_ID = bb.ORDR_ID 
					WHERE bb.CIS_LVL_TYPE = 'H5' and bb.CNTCT_TYPE_ID = 1) ph on
					fs.ORDR_ID = ph.ORDR_ID				

													 
				WHERE tk.CREAT_DT >= @stDt AND tk.CREAT_DT < @endDt
					 AND b.FSA_ORDR_TYPE_DES = @orderType 
					 AND (p.ORDR_STUS_ID = 2 or (fs.ORDR_TYPE_CD = 'DC' and p.ORDR_STUS_ID = 5))
-- 1-beggin
		UNION 
			SELECT distinct
				ncco.ORDR_ID [FTN] 
				,ncco.BILL_CYC_NBR [Billing Cycle] 
				,o.ordr_type_des [Order Type] 
				,'' [Order Sub Type] 
				,null [H1] 
				,NCCO.H5_ACCT_NBR [H5] 
				,ncco.cust_nme[Customer Name] 
				,'' [Site Address]
				,ncco.site_city_nme [Site City] 
				,'' [Province] 
				,NCCO.CTRY_CD [Site Country]
				,'' [Intl Postal Code] 
				,'' [Phone #]
				,'' [Network Indicator] 
				,ISNULL(v.PROD_TYPE_DES, '') [IP Product Type]
				,'' [Intl Carrier Code]
				,'' [Carrier Partner Name]
				,'' [PL#]
				, ''[PL Sequence]
				,'' [CircuitType]
				,'' [Circuit #]
				,'' [Circuit Bandwidth]
				,'N/A' [AOI]
				,'' [NUA]
				,'' [Port type]
				,'' [Port Speed]
				,NULL [Port Activation Date]
				,NULL [Port Disco Date]
				,'' [Usage Type]
				,'' [Charge Code]
				,'' [Charge Description]
				,'' [Quantity]
				,NULL [MRC]
				,NULL [NRC]
				,at.CREAT_DT [Bill Clear/Bill Stop Date]
				,ncco.CMNT_TXT [Comments] 
				,''[Half Tunnel]
				, ''[Half Tunnel Quantity]
				,''[CPE Additional Info.]
				,''[FSA_ORDR_BILL_LINE_ITEM_ID]   /*Added due to Missing BILL_LINE_ITEM in final report*/
				,'' [SCA Number]
FROM [COWS].dbo.NCCO_Ordr ncco
			inner join [COWS].[dbo].LK_ORDR_TYPE o WITH (NOLOCK) on ncco.ORDR_TYPE_ID = o.ORDR_TYPE_ID
			left join [COWS].[dbo].ACT_TASK at with (nolock) on ncco.ordr_id = at.ordr_id
			left join [COWS].[dbo].LK_PROD_TYPE v on ncco.PROD_TYPE_ID = v.PROD_TYPE_ID
			
			WHERE at.CREAT_DT >= @stDt AND at.CREAT_DT < @endDt and at.TASK_ID = 1001
-- 1-end
) billing ORDER BY FTN			
END
	ELSE 
		BEGIN
Select 
	 [FTN],[Billing Cycle],[Order Type],[Order Sub Type],[H1] ,[H5],[Customer Name],[Site Address],[Site City],[Province] 
	,[Site Country],[Intl Postal Code],[Phone #],[Network Indicator],[IP Product Type],[Intl Carrier Code]
	,[Carrier Partner Name],[PL#],[PL Sequence],[CircuitType],[Circuit #],[Circuit Bandwidth],[AOI],[NUA],[Port type]
	,[Port Speed],[Port Activation Date],[Port Disco Date],[Usage Type],[Charge Code],[Charge Description],[Quantity]
	,[MRC],[NRC],[Bill Clear/Bill Stop Date],[Comments] ,[Half Tunnel],[Half Tunnel Quantity],[CPE Additional Info.],
	[SCA Number]
	from (
	SELECT distinct	
				ISNULL(fs.FTN, '') [FTN],
				ISNULL (fs.CURR_BILL_CYC_CD, '') [Billing Cycle],
				ISNULL(b.FSA_ORDR_TYPE_DES, '') [Order Type],
				ISNULL(t.ORDR_SUB_TYPE_DES, '') [Order Sub Type],
				h1.CUST_ID [H1],
				fs.CUST_ID [H5],
				CASE
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(fs.CUST_NME))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(fs.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(fs.CUST_NME)) IS NULL THEN NULL 
					ELSE CONVERT(varchar, DecryptByKey(fs.CUST_NME))	END [Customer Name], --Secured
				CASE
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(od.STREET_ADR_1))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.STREET_ADR_1)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.STREET_ADR_1)) IS NULL THEN NULL 
					ELSE CONVERT(varchar, DecryptByKey(od.STREET_ADR_1))	END [Site Address], --Secured

				CASE
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(od.CTY_NME))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.CTY_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.CTY_NME)) IS NULL THEN NULL 
					ELSE CONVERT(varchar, DecryptByKey(od.CTY_NME))	END [Site City], --Secured
				
				CASE
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(od.PRVN_NME))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.PRVN_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.PRVN_NME)) IS NULL THEN NULL 
					ELSE CONVERT(varchar, DecryptByKey(od.PRVN_NME))	END [Province], --Secured
				
				ch.CTRY_CD [Site Country],
				
				CASE
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID = 0 THEN CONVERT(varchar, DecryptByKey(od.ZIP_PSTL_CD))	
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.ZIP_PSTL_CD)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
					WHEN @SecuredUser = 0 and p.CSG_LVL_ID > 0 AND CONVERT(varchar, DecryptByKey(od.ZIP_PSTL_CD)) IS NULL THEN NULL 
					ELSE CONVERT(varchar, DecryptByKey(od.ZIP_PSTL_CD))	END [Intl Postal Code], --Secured

				ISNULL(''''+ ph.PHN_NBR , '') [Phone #],
				ISNULL(fs.TPORT_ETHRNT_NRFC_INDCR, '') [Network Indicator], 
				ISNULL(v.PROD_TYPE_DES, '') [IP Product Type],
				ISNULL(fs.CXR_ACCS_CD, '') [Intl Carrier Code],
				ISNULL(r.PLTFRM_NME, '') [Carrier Partner Name],
				ISNULL(nc.PLN_NME, '') [PL#],
				ISNULL(nc.PLN_SEQ_NBR, '')[PL Sequence],
				ISNULL(nc.CKT_NME, '') [CircuitType],	
				ISNULL(nc.FMS_CKT_ID, '') [Circuit #],
				ISNULL(fs.TTRPT_SPD_OF_SRVC_BDWD_DES, '') [Circuit Bandwidth],
				'N/A' [AOI],
				CASE 
				WHEN nc.NUA_ADR IS NOT NULL THEN nc.NUA_ADR
				WHEN fs.ORDR_TYPE_CD = 'DC' and t.ORDR_SUB_TYPE_ID = 14 THEN ISNULL(fov.NW_ADR,'') 
				ELSE '' END[NUA],
				ISNULL(fs.TPORT_PORT_TYPE_ID, '') [Port type],
				fs.TTRPT_SPD_OF_SRVC_BDWD_DES [Port Speed],
				ni.ACTV_DT [Port Activation Date],
				ni.DSCNCT_DT [Port Disco Date],
				ISNULL(fs.TPORT_BRSTBL_USAGE_TYPE_CD, '') [Usage Type],
				
				--CASE
				--	WHEN m.BIC_CD IS NULL AND m.CHG_CD IS NULL THEN cb.SRVC_LINE_ITEM_CD 
				--	ELSE ISNULL(m.BIC_CD, m.CHG_CD) END [Charge Code],
				
				CASE
					WHEN (m.BIC_CD IS NULL or LEN(RTRIM(LTRIM(m.BIC_CD))) = 0) 
					AND (m.CHG_CD IS NULL or LEN(RTRIM(LTRIM(m.CHG_CD))) = 0) 
					AND (m.ITEM_CD IS NULL or LEN(RTRIM(LTRIM(m.ITEM_CD))) = 0) THEN cb.SRVC_LINE_ITEM_CD
					WHEN(m.BIC_CD IS NULL or LEN(RTRIM(LTRIM(m.BIC_CD))) = 0) 
					AND (m.CHG_CD IS NULL or LEN(RTRIM(LTRIM(m.CHG_CD))) = 0) THEN m.ITEM_CD
					ELSE ISNULL(BIC_CD, m.CHG_CD) END [Charge Code],

				CASE
					WHEN (m.ITEM_DES IS NULL OR LEN(RTRIM(LTRIM(m.ITEM_DES))) = 0) 
					AND (m.LINE_ITEM_DES IS NULL OR LEN(RTRIM(LTRIM(m.LINE_ITEM_DES))) = 0) THEN cb.MDS_DES					 
					WHEN (m.LINE_ITEM_DES IS NULL OR LEN(RTRIM(LTRIM(m.LINE_ITEM_DES))) = 0) THEN m.ITEM_DES
					ELSE m.LINE_ITEM_DES END [Charge Description],
					
				m.LINE_ITEM_QTY [Quantity],
				CAST(m.MRC_CHG_AMT AS MONEY) [MRC],
				CAST(m.NRC_CHG_AMT AS MONEY) [NRC],
				tk.CREAT_DT [Bill Clear/Bill Stop Date],
				ISNULL(o.CMNT_TXT, '') [Comments]
				,case
					when fov.VAS_TYPE_CD = 'HLFTN' Then 'Yes'
					else 'No'
					end[Half Tunnel]
				,case
					when fov.VAS_TYPE_CD = 'HLFTN' Then fov.VAS_QTY
					else ' '
					end[Half Tunnel Quantity]
				,case
					when fs.CPE_REC_ONLY_CD = 'Y' Then 'CPE records only'
					else ISNULL(fs.CPE_REC_ONLY_CD, '')
					end[CPE Additional Info.]
				,m.FSA_ORDR_BILL_LINE_ITEM_ID	/*Added due to Missing BILL_LINE_ITEM in final report*/
				,RTRIM(fs.SCA_NBR) [SCA Number]
				FROM
				(SELECT bb.FTN, bb.ORDR_ID, bb.ORDR_ACTN_ID, fcpl.TPORT_ETHRNT_NRFC_INDCR, 
                                    bb.TTRPT_ALT_ACCS_PRVDR_CD, 
                                    fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES, bb.TPORT_BRSTBL_USAGE_TYPE_CD,bb.ORDR_SUB_TYPE_CD, 
                                    bb.TPORT_PORT_TYPE_ID, bb.CXR_ACCS_CD,
                                    bb.ORDR_TYPE_CD, bb.PROD_TYPE_CD, bb.CPE_PHN_NBR,
                                    bb.CPE_REC_ONLY_CD , 
									dd.CUST_ID, dd.SOI_CD, dd.CUST_NME, dd.CURR_BILL_CYC_CD,dd.SRVC_SUB_TYPE_ID,
									bb.SCA_NBR
                                    FROM [COWS].[dbo].[FSA_ORDR] bb with (nolock) JOIN
                                    [COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID  Join [COWS].[dbo].[FSA_ORDR_CUST] dd with (nolock) ON
									bb.ORDR_ID = dd.ORDR_ID
									LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = bb.ORDR_ID
									/*
                                    WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
                                    FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
                                    WHERE  bb.FTN = fsa.FTN)  
									AND dd.CIS_LVL_TYPE = 'H5' AND dd.CUST_ID IS NOT NULL
									AND LEN(dd.CUST_ID) <> 0 AND dd.CURR_BILL_CYC_CD <> '43'
									*/
									/** MD MONIR To exclude mach5 Order **/
									JOIN [COWS].[dbo].[LK_ORDR_CAT] LC with (nolock) ON  cc.ORDR_CAT_ID=LC.ORDR_CAT_ID
                                    WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
										FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
										WHERE  bb.FTN = fsa.FTN)  
									AND dd.CIS_LVL_TYPE = 'H5' AND dd.CUST_ID IS NOT NULL
									AND LEN(dd.CUST_ID) <> 0 AND dd.CURR_BILL_CYC_CD <> '43'
									----select * from [COWS].[dbo].[LK_ORDR_CAT]
									AND LC.ORDR_CAT_ID <> 6
									/** MD MONIR To exclude mach5 Order **/
									
									) fs LEFT OUTER JOIN 
						
				[COWS].[dbo].[lk_FSA_ORDR_TYPE] b with (nolock) on fs.ORDR_TYPE_CD = b.FSA_ORDR_TYPE_CD LEFT OUTER JOIN
				[COWS].[dbo].[ORDR] p with (nolock) on fs.ORDR_ID = p.ORDR_ID LEFT OUTER JOIN
				[COWS].[dbo].[PLTFRM_MAPNG] d with (nolock) on p.PLTFRM_CD = d.PLTFRM_CD LEFT OUTER JOIN
				[COWS].[dbo].[LK_PLTFRM]r with (nolock) on d.PLTFRM_CD =  r.PLTFRM_CD LEFT OUTER JOIN
				--[COWS].[dbo].[ORDR_GRP] q with (nolock) on p.ORDR_ID = q.ORDR_ID LEFT OUTER JOIN
				--[COWS].[dbo].[PROD_ORDR_TYPE] e with (nolock) on q.PROD_TYPE_ID = e.PROD_TYPE_ID LEFT OUTER JOIN
				--[COWS].[dbo].[ORDR_ADR] i with (nolock) on fs.ORDR_ID = i.ORDR_ID LEFT OUTER JOIN /* Removed due to FTN billing detail was removed */
				
				(SELECT bb.ORDR_ID, bb.CTRY_CD, bb.CTY_NME, bb.STREET_ADR_1, bb.STREET_ADR_2, 
						bb.STT_CD, bb.ZIP_PSTL_CD, bb.PRVN_NME FROM [COWS].[dbo].[ORDR_ADR] bb with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE bb.CIS_LVL_TYPE = 'H5'  AND bb.CTRY_CD IS NOT NULL) od
						on fs.ORDR_ID = od.ORDR_ID  LEFT OUTER JOIN		
						
				[COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] m with (nolock) on m.ORDR_ID = fs.ORDR_ID LEFT OUTER JOIN
				--[COWS].[dbo].[ORDR_MS] n with (nolock) on fs.ORDR_ID = n.ORDR_ID LEFT OUTER JOIN
				[COWS].[dbo].[TRPT_ORDR] o with (nolock) on fs.ORDR_ID = o.ORDR_ID LEFT OUTER JOIN
				[COWS].[dbo].[LK_ORDR_SUB_TYPE] t with (nolock) on fs.ORDR_SUB_TYPE_CD = t.ORDR_SUB_TYPE_CD LEFT OUTER JOIN
				--[COWS].[dbo].[IPL_ORDR] u with (nolock) on p.ORDR_ID = u.ORDR_ID LEFT OUTER JOIN
				[COWS].[dbo].[LK_PROD_TYPE] s with (nolock) on fs.PROD_TYPE_CD = s.FSA_PROD_TYPE_CD LEFT OUTER JOIN
				[COWS].[dbo].[FSA_ORDR_VAS] fov with (nolock) on fs.ORDR_ID = fov.ORDR_ID LEFT OUTER JOIN

				
				(SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE bb.CIS_LVL_TYPE = 'H1') h1
						on fs.ORDR_ID = h1.ORDR_ID /*JOIN
						
				(SELECT bb.CUST_ID, bb.ORDR_ID, bb.SOI_CD, bb.CUST_NME, bb.CURR_BILL_CYC_CD, 
						bb.SRVC_SUB_TYPE_ID  FROM [COWS].[dbo].[FSA_ORDR_CUST] bb with (nolock) JOIN 
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE bb.CIS_LVL_TYPE = 'H5' AND bb.CUST_ID IS NOT NULL
						AND LEN(bb.CUST_ID) <> 0 AND bb.CURR_BILL_CYC_CD <> '43' ) h5
						on fs.ORDR_ID = h5.ORDR_ID */ LEFT OUTER JOIN
										
				(SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID 
 						FROM [COWS].[dbo].[ACT_TASK] bb with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
						WHERE bb.TASK_ID = 1001) tk on fs.ORDR_ID = tk.ORDR_ID LEFT OUTER JOIN
						
								
				(SELECT bb.NRM_CKT_ID, bb.FTN, bb.PLN_NME, bb.PLN_SEQ_NBR, bb.CKT_NME, bb.FMS_CKT_ID, bb.NUA_ADR 
 						FROM [COWS].[dbo].[NRM_CKT] bb with (nolock) JOIN
						[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.FTN = cc.FTN 
						WHERE bb.NRM_CKT_ID = (SELECT MAX(NRM_CKT_ID)
						FROM (SELECT NRM_CKT_ID, FTN FROM [COWS].[dbo].[NRM_CKT] with (nolock) ) nct 
						WHERE nct.FTN = bb.FTN)) nc on fs.FTN = nc.FTN LEFT OUTER JOIN	
				
				--(SELECT bb.ORDR_ID, bb.CTRY_CD, bb.ADR_TYPE_ID FROM [COWS].[dbo].[ORDR_ADR] bb JOIN
				--		[COWS].[dbo].[ORDR] cc on bb.ORDR_ID = cc.ORDR_ID
				--		WHERE bb.ADR_TYPE_ID = 6 or bb.ADR_TYPE_ID = 18 AND bb.CIS_LVL_TYPE = 'H5') cd 
				--		on fs.ORDR_ID = cd.ORDR_ID LEFT OUTER JOIN
							
				[COWS].[dbo].[LK_PROD_TYPE] v with (nolock) on fs.PROD_TYPE_CD = v.FSA_PROD_TYPE_CD LEFT OUTER JOIN

						
				(SELECT DISTINCT bb.FTN, bb.SRVC_INSTC_OBJ_ID, bb.ACTV_DT, bb.DSCNCT_DT
					FROM [COWS].[dbo].[NRM_SRVC_INSTC] bb with (nolock) LEFT OUTER JOIN
					[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.FTN = cc.FTN 
					WHERE bb.SRVC_INSTC_OBJ_ID = (SELECT MAX(SRVC_INSTC_OBJ_ID)
					FROM (SELECT SRVC_INSTC_OBJ_ID, FTN FROM [COWS].[dbo].[NRM_SRVC_INSTC] with (nolock) )nsi
					WHERE nsi.FTN = bb.FTN )AND bb.ACTV_DT = (SELECT MAX(ACTV_DT) FROM 
					(SELECT ACTV_DT, SRVC_INSTC_OBJ_ID 
					FROM [COWS].[dbo].[NRM_SRVC_INSTC] with (nolock) ) act 
					WHERE act.SRVC_INSTC_OBJ_ID = bb.SRVC_INSTC_OBJ_ID)) ni on fs.FTN = ni.FTN LEFT OUTER JOIN 
					
				(SELECT distinct cc.ORDR_ID,cc.SRVC_LINE_ITEM_CD, cc.FSA_CPE_LINE_ITEM_ID, cc.MDS_DES, cc.TTRPT_ACCS_TYPE_DES, cc.TTRPT_SPD_OF_SRVC_BDWD_DES
					FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb with (nolock)
					JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] cc  
					 with (nolock) on cc.ORDR_ID = bb.ORDR_ID 
					) cb on  m.FSA_CPE_LINE_ITEM_ID = cb.FSA_CPE_LINE_ITEM_ID	LEFT OUTER JOIN
					
				[COWS].[dbo].[H5_FOLDR] ch on p.H5_H6_CUST_ID = ch.CUST_ID LEFT OUTER JOIN
					
				(SELECT bb.ORDR_ID, bb.PHN_NBR, bb.CIS_LVL_TYPE, bb.CNTCT_TYPE_ID 
					FROM [COWS].[dbo].[ORDR_CNTCT] bb with (nolock) LEFT OUTER JOIN
					[COWS].[dbo].[FSA_ORDR] cc with (nolock) on cc.ORDR_ID = bb.ORDR_ID 
					WHERE bb.CIS_LVL_TYPE = 'H5' and bb.CNTCT_TYPE_ID = 1) ph on
					fs.ORDR_ID = ph.ORDR_ID	
				

									 
				WHERE tk.CREAT_DT >= @stDt AND tk.CREAT_DT < @endDt
				AND (p.ORDR_STUS_ID = 2 or (fs.ORDR_TYPE_CD = 'DC' and p.ORDR_STUS_iD = 5))
-- 2-beggin
UNION 
			SELECT distinct
				ncco.ORDR_ID [FTN] 
				,ncco.BILL_CYC_NBR [Billing Cycle] 
				,o.ordr_type_des [Order Type] 
				,'' [Order Sub Type] 
				,NULL [H1] 
				,NCCO.H5_ACCT_NBR [H5] 
				,ncco.cust_nme[Customer Name] 
				,'' [Site Address]
				,ncco.site_city_nme [Site City] 
				,'' [Province] 
				,NCCO.CTRY_CD [Site Country]
				,'' [Intl Postal Code] 
				,'' [Phone #]
				,'' [Network Indicator] 
				,ISNULL(v.PROD_TYPE_DES, '') [IP Product Type]
				,'' [Intl Carrier Code]
				,'' [Carrier Partner Name]
				,'' [PL#]
				, ''[PL Sequence]
				,'' [CircuitType]
				,'' [Circuit #]
				,'' [Circuit Bandwidth]
				,'N/A' [AOI]
				,'' [NUA]
				,'' [Port type]
				,'' [Port Speed]
				,NULL [Port Activation Date]
				,NULL [Port Disco Date]
				,'' [Usage Type]
				,'' [Charge Code]
				,'' [Charge Description]
				,'' [Quantity]
				,NULL [MRC]
				,NULL [NRC]
				,at.CREAT_DT [Bill Clear/Bill Stop Date]
				,ncco.CMNT_TXT [Comments]
				,''[Half Tunnel]
				,''[Half Tunnel Quantity]
				,''[CPE Additional Info.] 
				,''[FSA_ORDR_BILL_LINE_ITEM_ID]   /*Added due to Missing BILL_LINE_ITEM in final report*/
				,''[SCA Number]
FROM [COWS].dbo.NCCO_Ordr ncco
			inner join [COWS].[dbo].LK_ORDR_TYPE o WITH (NOLOCK) on ncco.ORDR_TYPE_ID = o.ORDR_TYPE_ID
			left join [COWS].[dbo].ACT_TASK at with (nolock) on ncco.ordr_id = at.ordr_id
			left join [COWS].[dbo].LK_PROD_TYPE v on ncco.PROD_TYPE_ID = v.PROD_TYPE_ID
			
			WHERE at.CREAT_DT >= @stDt AND at.CREAT_DT < @endDt and at.TASK_ID = 1001
-- 2-end
) billing ORDER BY FTN

		END
CLOSE SYMMETRIC KEY FS@K3y 

	END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_WBORptBilling_Test '  + CAST(@QueryNumber AS VARCHAR(4)) + ', '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @stDt, 101) + ', ' + CONVERT(VARCHAR(10), @endDt, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH

GO


