USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_FedlineSiteDelAuditRptOD]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_FedlineSiteDelAuditRptOD')
EXEC ('CREATE PROCEDURE [dbo].[sp_FedlineSiteDelAuditRptOD] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

--=======================================================================================
-- Project:			PJ006439 - COWS Reporting 
-- Author:			David Phillips	
-- Date:			09/20/2012
-- Description:		
--					Summary of disconnect/site delete orders/events, regardless of order/event 
--                  status that details if Sprint completed the disconnect request within the 
--                  agreed upon SLA. Sprint's best effort is to complete disconnect within 
--                  7 calendar days from the FRB Order Submission Date. Applicable event status 
--                  for these events are: Pending, In Progress, Complete, Completed - 
--                  Device Inaccessible, or Cancelled status. 								
--					
--
-- Modifications:
--
--=======================================================================================

ALTER Procedure [dbo].[sp_FedlineSiteDelAuditRptOD]
			@startDate  datetime = NULL,
			@endDate  datetime = NULL,
			@getSensData  CHAR(1)='N',
			@SecuredUser  INT = 0

				
AS 
BEGIN TRY		


	DECLARE @aSQL			VARCHAR(5000)
	
	CREATE TABLE #tmp_fedline_del_audit_rpt
	(					
		CustName				VARCHAR(100),
		FRB_ID					VARCHAR(25),
		Event_ID				VARCHAR(25),
		Org_ID					VARCHAR(25),
		Event_Stus				VARCHAR(50),
		EventType				VARCHAR(50),
		ActType					VARCHAR(50),
		DeviceId				VARCHAR(100),
		OrdSubDt				VARCHAR(25),
		ReqActDt				VARCHAR(100),
		EventComp				VARCHAR(100),
		SLA						VARCHAR(10),
		EventLastAct			VARCHAR(28),
		OrigSubDt               VARCHAR(50)
	)

	SET NOCOUNT ON;

	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
		
	SET @aSQL='INSERT INTO #tmp_fedline_del_audit_rpt(CustName, FRB_ID, Event_ID, Org_ID, Event_Stus,  
				 ActType, DeviceId, OrdSubDt, ReqActDt, EventComp, SLA, EventLastAct, OrigSubDt)
	
	SELECT DISTINCT ISNULL(CONVERT(VARCHAR, DecryptByKey(fd.CUST_NME)),'''')--CustName
		   ,ISNULL(CONVERT(VARCHAR, fd.FRB_REQ_ID),'''') --FRB_ID 
		   ,ISNULL(CONVERT(VARCHAR, fd.EVENT_ID),'''') -- Event ID
		   ,ISNULL(CONVERT(VARCHAR, fd.ORG_ID),'''') -- ORG ID
		   ,ISNULL(ls.EVENT_STUS_DES, '''')  -- Event Stus
		   ,ISNULL(ot.PRNT_ORDR_TYPE_DES, '''') --ActType
		   ,ISNULL(fd.DEV_NME,'''')  -- Device ID
		   ,ISNULL(CONVERT(VARCHAR (12), fd.REQ_RECV_DT,107), '''') -- OrdSubDt
		   ,ISNULL(CONVERT(VARCHAR (26), fd.STRT_TME, 109), '''') -- ReqActDT
		   ,ISNULL(CONVERT(VARCHAR (12),(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh
					WHERE eh.EVENT_ID = fd.EVENT_ID AND eh.ACTN_ID = ''17''),107), '''') -- EventComp
					
		   ,CASE
				WHEN DATEDIFF(day,fd.REQ_RECV_DT,(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh
					WHERE eh.EVENT_ID = fd.EVENT_ID AND eh.ACTN_ID = ''17'')) > 7 THEN ''No''
				WHEN ISNULL((SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh
					WHERE eh.EVENT_ID = fd.EVENT_ID AND eh.ACTN_ID = ''17''),'''') = '''' THEN '''' 
				ELSE ''Yes''
			END   -- SLA
		   ,ISNULL(CONVERT(VARCHAR (12),(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh
					WHERE eh.EVENT_ID = fd.EVENT_ID),107), '''') -- EventLastAct
		   ,CONVERT(VARCHAR (12),(SELECT min(REQ_RECV_DT) from COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA 
					WHERE EVENT_ID = fd.EVENT_ID), 107) --OrigSubDt
 	 FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA fd
		INNER JOIN COWS.dbo.FEDLINE_EVENT_USER_DATA ud ON ud.EVENT_ID = fd.EVENT_ID 
		INNER JOIN COWS.dbo.LK_EVENT_STUS ls ON ud.EVENT_STUS_ID = ls.EVENT_STUS_ID AND ls.REC_STUS_ID = 1
		INNER JOIN COWS.dbo.LK_FEDLINE_ORDR_TYPE ot ON ot.ORDR_TYPE_CD = fd.ORDR_TYPE_CD
		LEFT OUTER JOIN COWS.dbo.FEDLINE_EVENT_ADR fa ON fa.FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID
											AND fa.ADR_TYPE_ID = 18
		LEFT OUTER JOIN COWS.dbo.LK_CTRY c ON c.CTRY_CD = fa.CTRY_CD

	WHERE ot.ORDR_TYPE_CD IN (''DCC'',''DCS'',''HED'') 
			AND fd.REC_STUS_ID = 1'
			
		
	IF @startDate <> ''
		SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT >= ''' + convert(varchar(20),@startDate) + '''' 
		
	IF @endDate <> ''
		BEGIN
			SET @endDate = DATEADD(day, 1, @endDate)
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT <= ''' + convert(varchar(20),@endDate) + ''''	
		END


	EXEC (@aSQL);

	SELECT 
			CASE
				WHEN @SecuredUser = 0 AND CustName <> '' THEN 'Private Customer' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND CustName <> '' THEN 'Private Customer'
				ELSE CustName 
			END				AS 'Customer Name'
			,Org_ID         AS 'ORG ID'
			,FRB_ID			AS 'FRB ID'
			,OrigSubDt		AS 'Initial FRB Order Submission Date'
			,OrdSubDt		AS 'FRB Order Submission Date'
			,Event_ID		AS 'COWS Event ID'
			,Event_Stus		AS 'COWS Event Status'
			,ReqActDt		AS 'Requested Activity Date'
			,DeviceId		AS 'Device ID/Asset ID'
			,ActType		AS 'Activity Type'
			,EventComp      AS 'Event Completion Date'
			,SLA			AS 'Disconnect SLA MET?'
			,EventLastAct	AS 'Event Last Action Date'

	FROM #tmp_fedline_del_audit_rpt
	WHERE FRB_ID > 0

	CLOSE SYMMETRIC KEY FS@K3y;

	IF OBJECT_ID('tempdb..#tmp_fedline_del_audit_rpt', 'U') IS NOT NULL	
		drop table #tmp_fedline_del_audit_rpt;	


	RETURN 0;
	

END TRY
BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_FedlineSiteDelAuditRptOD '  
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', '
		+ CONVERT(VARCHAR(10), @endDate, 101) + ', '
		+ @SecuredUser + ', ' + @getSensData	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH


GO
