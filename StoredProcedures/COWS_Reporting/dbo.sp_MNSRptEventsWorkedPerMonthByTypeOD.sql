USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptEventsWorkedPerMonthByTypeOD]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_MNSRptEventsWorkedPerMonthByTypeOD')
EXEC ('CREATE PROCEDURE [dbo].[sp_MNSRptEventsWorkedPerMonthByTypeOD] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			11/29/2011
-- Description:		
--					Summary count of all events worked for the month/month-to-date.
--
-- Notes:
--
-- 02/21/2012		sxv0766: Added Reschedule Failed ACTN ID from 31 to 43
--
--==================================================================================
ALTER Procedure [dbo].[sp_MNSRptEventsWorkedPerMonthByTypeOD]	
	@SecuredUser  INT=0,   
	@getSensData  CHAR(1)='N',   	
	@startDate		Datetime=NULL,
	@endDate		Datetime=NULL
AS

BEGIN TRY

	SET NOCOUNT ON;
		
	SELECT	YearMonth 'Year Month', 
			(M_Install+M_MAC+F_Install+F_MAC) 'Total Events No Disco', 
			M_Install 'M Installs', 
			M_MAC 'M MAC', 
			F_Install 'F Installs', 
			F_MAC 'F MAC', 
			Disco,			
			CAST(((F_Install+F_MAC)*.01 * 10000)/(M_Install+M_MAC+F_Install+F_MAC) AS DECIMAL(10, 2)) '% Fast Track'
	FROM
	(
		SELECT	mins.YearMonth, 
				CASE 
					WHEN mins.M_Install is NULL THEN 0 
					ELSE mins.M_Install END 'M_Install', 
				CASE 
					WHEN mmac.M_MAC is NULL THEN 0 
					ELSE mmac.M_MAC END 'M_MAC', 
				CASE 
					WHEN fins.F_Install is NULL THEN 0 
					ELSE fins.F_Install END 'F_Install', 
				CASE 
					WHEN fmac.F_MAC is NULL THEN 0 
					ELSE fmac.F_MAC END 'F_MAC',
				CASE 
					WHEN disc.Disco is NULL THEN 0 
					ELSE disc.Disco END 'Disco'
		FROM
		(
 			SELECT SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7) 'YearMonth', COUNT(mds.EVENT_ID) 'M_Install' 
 			FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
 			JOIN COWS.dbo.EVENT_HIST evhi with (nolock) ON mds.EVENT_ID = evhi.EVENT_ID
 			WHERE mds.MDS_ACTY_TYPE_ID = 1 AND mds.MDS_FAST_TRK_CD = 0 AND (evhi.ACTN_ID in (15, 17) OR evhi.ACTN_ID between 31 and 43) 
				AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < DATEADD(day, 1, @endDate)
 			GROUP BY SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7)
		) mins
		LEFT JOIN
		(
 			SELECT SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7) 'YearMonth', COUNT(mds.EVENT_ID) 'M_MAC' 
 			FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
 			JOIN COWS.dbo.EVENT_HIST evhi with (nolock) ON mds.EVENT_ID = evhi.EVENT_ID
 			LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID AND mds.MDS_ACTY_TYPE_ID = 2
 			WHERE mds.MDS_ACTY_TYPE_ID = 2 AND mds.MDS_FAST_TRK_CD = 0 AND (evhi.ACTN_ID in (15, 17) OR evhi.ACTN_ID between 31 and 43) 
 				AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < DATEADD(day, 1, @endDate)
 			GROUP BY SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7)
		) mmac ON mins.YearMonth = mmac.YearMonth
		LEFT JOIN
		(
 			SELECT SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7) 'YearMonth', COUNT(mds.EVENT_ID) 'F_Install' 
 			FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
 			JOIN COWS.dbo.EVENT_HIST evhi with (nolock) ON mds.EVENT_ID = evhi.EVENT_ID
 			WHERE mds.MDS_ACTY_TYPE_ID = 1 AND mds.MDS_FAST_TRK_CD = 1 AND (evhi.ACTN_ID in (15, 17) OR evhi.ACTN_ID between 31 and 43) 
				AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < DATEADD(day, 1, @endDate)
 			GROUP BY SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7)
		) fins ON mmac.YearMonth = fins.YearMonth
		LEFT JOIN
		(
 			SELECT SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7) 'YearMonth', COUNT(mds.EVENT_ID) 'F_MAC' 
 			FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
 			JOIN COWS.dbo.EVENT_HIST evhi with (nolock) ON mds.EVENT_ID = evhi.EVENT_ID
 			LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID AND mds.MDS_ACTY_TYPE_ID = 2
 			WHERE mds.MDS_ACTY_TYPE_ID = 2 AND mds.MDS_FAST_TRK_CD = 1 AND (evhi.ACTN_ID in (15, 17) OR evhi.ACTN_ID between 31 and 43) 
 				AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < DATEADD(day, 1, @endDate)
 			GROUP BY SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7)
		) fmac ON fins.YearMonth = fmac.YearMonth
		LEFT JOIN
		(
 			SELECT SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7) 'YearMonth', COUNT(mds.EVENT_ID) 'Disco' 
 			FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
 			JOIN COWS.dbo.EVENT_HIST evhi with (nolock) ON mds.EVENT_ID = evhi.EVENT_ID
 			WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.MDS_FAST_TRK_CD in (0, 1) AND (evhi.ACTN_ID in (15, 17) OR evhi.ACTN_ID between 31 and 43)
				AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < DATEADD(day, 1, @endDate)
 			GROUP BY SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7)
		) disc ON fmac.YearMonth = disc.YearMonth
	)tbl	
	
	
	RETURN 0;
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptEventsWorkedPerMonthByTypeOD ' 	
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH






GO
