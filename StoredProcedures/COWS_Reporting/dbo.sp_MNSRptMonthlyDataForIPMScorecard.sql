USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptMonthlyDataForIPMScorecard]    Script Date: 11/21/2019 9:35:23 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
--=======================================================================================  
-- Project:   PJ004672 - COWS Reporting   
-- Author:   Sudarat Vongchumpit   
-- Date:   06/20/2011  
-- Description:    
--     Extract MDS events per month by IPM.   
-- Usage:  
--     exec sp_MNSRptMonthlyDataForIPMScorecard 0  
--     OR  
--     exec sp_MNSRptMonthlyDataForIPMScorecard 1  
--     OR  
--     exec sp_MNSRptMonthlyDataForIPMScorecard 0, '11/01/2011', '11/30/2011'  
-- Modifications:  
--  
-- 02/21/2012  sxv0766: Added Reschedule Failed ACTN ID from 31 to 43  
-- 03/20/2012  sxv0766: Removed String Conversion from 'Date And Time' column  
-- 03/27/2012  sxv0766: IM952822-Remove the resulting string expression from YearMonth   
--     column;IM952920-MAC Activity Concatenation; IM953116-duplicate events   
-- 01/27/2017		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked 
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--========================================================================================  
*/
  
ALTER PROCEDURE [dbo].[sp_MNSRptMonthlyDataForIPMScorecard]  
 @Secured   INT=0,  
 @inStartDtStr  VARCHAR(10)='',  
 @inEndDtStr   VARCHAR(10)=''  
AS  
  
BEGIN TRY  
  
 SET NOCOUNT ON;  
  
 DECLARE @startDate  Datetime  
 DECLARE @endDate  Datetime  
 DECLARE @today   Datetime  
 DECLARE @startDtStr  VARCHAR(10)   
     
     
 IF @inStartDtStr <> '' AND @inEndDtStr <> ''  
    BEGIN  
  
   SET @startDate=CONVERT(datetime, @inStartDtStr, 101)  
   SET @endDate=CONVERT(datetime, @inEndDtStr, 101)     
     
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @endDate=DATEADD(day, 1, @endDate)      
      
    END  
   ELSE  
    BEGIN     
      
     SET @today=DATEADD(MONTH,-1,GETDATE())  
       
      -- e.g. Run date is the 1st day of each month   
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000  
        
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));       
   SET @startDate=cast(@startDtStr AS datetime)  
   SET @endDate=cast((cast(SUBSTRING(CONVERT(VARCHAR(10), getdate(), 101),1,2) + '/01/' + CAST(YEAR(getdate()) AS VARCHAR(4)) as datetime) -1) AS datetime)  
   
  END  
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;    
    
 SELECT mds.EVENT_ID 'Event ID',  
   CASE mds.MDS_FAST_TRK_CD  
    WHEN 1 THEN 'MDS Fast Track'  
    WHEN 0 THEN 'MDS' END 'Event Type',   
   CASE         
    --WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''  
    --WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'  
    --ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME))
    WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
	WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'')  
    END 'Customer Name',      
   luser1.DSPL_NME 'Author',     
   evhi.CREAT_DT 'YearMonth',  
   --SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7) 'YearMonth',  
   CASE   
    WHEN evhi.ACTN_ID is NULL THEN ''  
    ELSE lact.ACTN_DES END 'Action',  
   CASE  
    WHEN evhi.FAIL_REAS_ID is NULL THEN ''  
    ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',    
    evhi.CREAT_DT 'Date And Time',    
   --REPLACE(SUBSTRING(CONVERT(varchar,  evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8)   
   -- + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',  
   CASE  
    WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'   
    ELSE '' END 'MDS Install Activity',   
   mact.MDS_ACTY_TYPE_DES AS 'MDS Activity Type',  
   --CASE   
   -- WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''  
   -- ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',      
   CASE  
    WHEN mcev.MAC_ACTY_DES is NULL THEN ''  
    ELSE mcev.MAC_ACTY_DES END 'MDS MAC Activity',    
   CASE  
    WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END 'Performed By',     
   CASE  
    WHEN evhi.CMNT_TXT is NULL THEN ''  
    ELSE evhi.CMNT_TXT END 'Comment',
	''                                   AS [S/C Flag], 
   'MDS Only'			AS [Network Activity Type]
 FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)    
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID    
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
  LEFT JOIN ( SELECT distinct EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID   
     FROM COWS.dbo.EVENT_HIST with (nolock)  
     WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID    
  LEFT JOIN  
  (  
   SELECT distinct LEFT([MAC_ACTY_DES],len([MAC_ACTY_DES]) -1) as MAC_ACTY_DES, evMc2.EVENT_ID  
   FROM  
   (  
    SELECT (SELECT lkmm.MDS_MAC_ACTY_NME  + ', ' AS [text()]   
       FROM COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock)  
       JOIN COWS.dbo.LK_MDS_MAC_ACTY lkmm with (nolock) ON mmac.MDS_MAC_ACTY_ID = lkmm.MDS_MAC_ACTY_ID   
       WHERE mmac.EVENT_ID = evMc.EVENT_ID   
       FOR xml PATH ('')  
      ) as MAC_ACTY_DES, EVENT_ID   
    FROM  
    (SELECT distinct EVENT_ID FROM COWS.dbo.MDS_EVENT_MAC_ACTY with (nolock)     
    )evMc  
   )evMc2  
  )mcev ON evhi.EVENT_ID = mcev.EVENT_ID       
 JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID

  LEFT JOIN COWS.dbo.LK_ACTN lact with (nolock) ON evhi.ACTN_ID = lact.ACTN_ID   
  LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID  
  --LEFT JOIN COWS.dbo.LK_MDS_INSTL_ACTY_TYPE mins with (nolock) ON mds.MDS_INSTL_ACTY_TYPE_ID = mins.MDS_INSTL_ACTY_TYPE_ID  
  --LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID AND mds.MDS_ACTY_TYPE_ID = 2  
  --LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID    
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID  
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID  
    
  -- ACTN_ID: 15 = Reschedule, 17 = Completed  
 --WHERE cast(SUBSTRING(CONVERT(varchar(8), mds.STRT_TMST, 112), 5, 2) AS int) = cast(SUBSTRING(CONVERT(varchar(8), DATEADD(month, -1, GETDATE()), 112) , 5, 2) AS int)     
 WHERE evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate   
 AND (((COALESCE(lfrs.FAIL_REAS_DES, '') <> '') AND (COALESCE(evhi.CMNT_TXT,'') <> '')) OR (COALESCE(lfrs.FAIL_REAS_DES, '') = ''))

 /*New MDS Events Code Start */
	UNION ALL
 SELECT mds.EVENT_ID 'Event ID',  
   --CASE mds.MDS_FAST_TRK_CD WHEN 1 THEN 'MDS Fast Track' WHEN 0 THEN 'MDS' END 'Event Type',
   CASE mds.MDS_FAST_TRK_TYPE_ID  WHEN 'A' THEN 'MDS Fast Track' WHEN 'S' THEN 'MDS Fast Track' ELSE  'MDS' END 'Event Type',   
   CASE         
    --WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''  
    --WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'  
    --ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME))
    
    WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
	WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
    END 'Customer Name',      
   luser1.DSPL_NME 'Author',     
   evhi.CREAT_DT 'YearMonth',  
   --SUBSTRING(CONVERT(char, evhi.CREAT_DT, 120), 1, 7) 'YearMonth',  
   CASE   
    WHEN evhi.ACTN_ID is NULL THEN ''  
    ELSE lact.ACTN_DES END 'Action',  
   CASE  
    WHEN evhi.FAIL_REAS_ID is NULL THEN ''  
    ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',    
    evhi.CREAT_DT 'Date And Time',    
   --REPLACE(SUBSTRING(CONVERT(varchar,  evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8)   
   -- + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',  
   CASE  
    WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'   
    ELSE '' END 'MDS Install Activity',   
   mact.MDS_ACTY_TYPE_DES AS 'MDS Activity Type',  
   --CASE   
   -- WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''  
   -- ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',      
   CASE  
    WHEN mcev.MAC_ACTY_DES is NULL THEN ''  
    ELSE mcev.MAC_ACTY_DES END 'MDS MAC Activity',    
   CASE  
    WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END 'Performed By',     
   CASE  
    WHEN evhi.CMNT_TXT is NULL THEN ''  
    ELSE evhi.CMNT_TXT END 'Comment',
			CASE	
						WHEN eod.SC_CD = 'C'        THEN 'Complex'
						WHEN eod.SC_CD = 'S'        THEN 'Simple'
					    ELSE ''
						END                                     AS [S/C Flag],
   lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type]  
 FROM COWS.dbo.MDS_EVENT mds with (nolock)    /* NEW  MDS EVENT */
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID    
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
  LEFT JOIN ( SELECT distinct EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID   
     FROM COWS.dbo.EVENT_HIST with (nolock)  
     WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID    
  LEFT JOIN  
  (  
   SELECT distinct LEFT([MAC_ACTY_DES],len([MAC_ACTY_DES]) -1) as MAC_ACTY_DES, evMc2.EVENT_ID  
   FROM  
   (  
    SELECT (SELECT lkmm.MDS_MAC_ACTY_NME  + ', ' AS [text()]   
       FROM COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock)  
       JOIN COWS.dbo.LK_MDS_MAC_ACTY lkmm with (nolock) ON mmac.MDS_MAC_ACTY_ID = lkmm.MDS_MAC_ACTY_ID   
       WHERE mmac.EVENT_ID = evMc.EVENT_ID   
       FOR xml PATH ('')  
      ) as MAC_ACTY_DES, EVENT_ID   
    FROM  
    (SELECT distinct EVENT_ID FROM COWS.dbo.MDS_EVENT_MAC_ACTY with (nolock)     
    )evMc  
   )evMc2  
  )mcev ON evhi.EVENT_ID = mcev.EVENT_ID       
 JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID

  LEFT JOIN COWS.dbo.LK_ACTN lact with (nolock) ON evhi.ACTN_ID = lact.ACTN_ID   
  LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID  
  --LEFT JOIN COWS.dbo.LK_MDS_INSTL_ACTY_TYPE mins with (nolock) ON mds.MDS_INSTL_ACTY_TYPE_ID = mins.MDS_INSTL_ACTY_TYPE_ID  
  --LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID AND mds.MDS_ACTY_TYPE_ID = 2  
  --LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID    
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID  
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID  
  LEFT OUTER JOIN [COWS].[dbo].[MDS_EVENT_ODIE_DEV] eod WITH (NOLOCK) ON eod.EVENT_ID = mds.EVENT_ID
  INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = mds.NTWK_ACTY_TYPE_ID
  -- ACTN_ID: 15 = Reschedule, 17 = Completed  
 --WHERE cast(SUBSTRING(CONVERT(varchar(8), mds.STRT_TMST, 112), 5, 2) AS int) = cast(SUBSTRING(CONVERT(varchar(8), DATEADD(month, -1, GETDATE()), 112) , 5, 2) AS int)     
 WHERE evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate   
 AND (((COALESCE(lfrs.FAIL_REAS_DES, '') <> '') AND (COALESCE(evhi.CMNT_TXT,'') <> '')) OR (COALESCE(lfrs.FAIL_REAS_DES, '') = ''))
/*New MDS Events Code OLD */
 ORDER BY mds.EVENT_ID  
    
 CLOSE SYMMETRIC KEY FS@K3y;   
   
 RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(100)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptMonthlyDataForIPMScorecard '   
 SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
