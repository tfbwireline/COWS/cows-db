/*  
--=============================================  
--Author:  Ragi, Ramesh  
--Create date: 9/2/2014  
--Description:   
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
-- Update date: <06/06/2018>
-- Updated by: Md M Monir   
-- [dbo].[sp_ENCIDailyPendingTask] 1  
--=============================================  
*/  
ALTER PROCEDURE [dbo].[sp_ENCIDailyPendingTask]   
   @secured INT=0  
AS  
BEGIN TRY  
  
OPEN SYMMETRIC KEY FS@K3y       
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
Select  
 CASE  
    WHEN @secured = 0 and o.CSG_LVL_ID =0 THEN h5.CUST_NME  
 WHEN @secured = 0 and o.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER'   
 WHEN @secured = 0 and o.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL   
 --ELSE CONVERT(varchar, DecryptByKey(csd.CUST_NME))  
 ELSE coalesce(h5.CUST_NME,CONVERT(varchar, DecryptByKey(csd.CUST_NME)),'')   
 END [Customer Name] --Secured  
,ISNULL(f.ftn, o.ordr_id) as 'FTN'  
,ont.CREAT_DT as 'Order submit date'  
,isnull(oENte.NTE_TXT,'') AS 'Last xNCI update'  
,isnull(lot.ORDR_TYPE_DES,'') AS 'Order type'  
,isnull(lu.DSPL_NME,'') as 'Assigned user'  
--,ISNULL(lc.CTRY_NME,'') as 'Country' 
,ISNULL(CASE WHEN (h5.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE lc.CTRY_NME END,'') [Country] /*Monir 06062018*/   
,ISNULL(f.CUST_WANT_DT,ISNULL(ipl.CUST_WANT_DT,n.CWD_DT)) as 'Customer want date'  
,isnull(los.ORDR_STUS_DES,'') AS 'Order Status'  
,ISNULL(lt.TASK_NME,'') AS 'Task Description'  
 From  COWS.dbo.ORDR o with (nolock)  
inner join  COWS.dbo.ACT_TASK at with (nolock) on o.ORDR_ID = at.ORDR_ID  
left join COWS.dbo.FSA_ORDR f with (nolock) on f.ORDR_ID = o.ORDR_ID  
inner join (select MIN(NTe_ID) AS nte_id, ordr_id as ordr_id from COWS.dbo.ORDR_NTE ot with (nolock) group by ORDR_ID) ordrSbmtDate on ordrSbmtDate.ordr_id = o.ORDR_ID  
inner join COWS.dbo.ORDR_NTE ont with (nolock) on ont.NTE_ID = ordrSbmtDate.nte_id  
left join (select MAX(nte_id) as nte_id, ot.ordr_id as ordr_id  
                        from   COWS.dbo.ORDR_NTE ot with (nolock)  
                  inner join COWS.dbo.LK_USER lu with (nolock) on lu.USER_ID = ot.CREAT_BY_USER_ID  
                  inner join COWS.dbo.USER_GRP_ROLE ugr with (nolock) on lu.USER_ID = ugr.USER_ID   
                        where lu.REC_STUS_ID = 1  
                              and ugr.GRP_ID = 7  
                              and ugr.REC_STUS_ID = 1  
                  group by ot.ORDR_ID  
              )   otEnciNte on otEnciNte.ordr_id = o.ORDR_ID  
left join COWS.dbo.ORDR_NTE oENte with (nolock) on oENte.NTE_ID = otEnciNte.nte_id       
left join COWS.dbo.LK_PPRT lp with (nolock) on lp.PPRT_ID = o.PPRT_ID  
left join COWS.dbo.LK_ORDR_TYPE lot with (nolock) on lot.ORDR_TYPE_ID = lp.ORDR_TYPE_ID  
left join COWS.dbo.USER_WFM_ASMT uwa with (nolock) on uwa.ORDR_ID = o.ORDR_ID and uwa.GRP_ID in (5,6,7)  
left join COWS.dbo.LK_USER lu with (nolock) on lu.USER_ID = uwa.ASN_USER_ID  
left join COWS.dbo.H5_FOLDR h5 with (nolock) on h5.H5_FOLDR_ID = o.H5_FOLDR_ID  
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=h5.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6  
left join COWS.dbo.LK_CTRY lc with (nolock) on lc.CTRY_CD = h5.CTRY_CD 
LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD 
left join COWS.dbo.NCCO_ORDR n with (nolock) on n.ORDR_ID = o.ORDR_ID  
left join COWS.dbo.IPL_ORDR ipl with (nolock) on ipl.ORDR_ID = o.ORDR_ID  
left join COWS.dbo.LK_ORDR_STUS los with (nolock) on los.ORDR_STUS_ID = o.ORDR_STUS_ID  
left join COWS.dbo.LK_TASK lt with (nolock) on at.TASK_ID = lt.TASK_ID  
      Where at.TASK_ID in (200,201,500)  
            and at.STUS_ID = 0  
            and isnull(o.H5_FOLDR_ID,0) != 0  
            and isnull(o.RGN_ID,0) = 2  
  
CLOSE SYMMETRIC KEY FS@K3y   
END TRY  
  
BEGIN CATCH  
  DECLARE @Desc VARCHAR(MAX)  
  SET @Desc='EXEC COWS_Reporting.dbo.sp_ENCIPendingTask';  
  EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
END CATCH  
  