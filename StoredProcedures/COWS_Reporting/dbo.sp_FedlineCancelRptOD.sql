USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_FedlineCancelRptOD]    Script Date: 5/13/2014 12:07:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.routines WHERE routine_schema='dbo' and routine_name='sp_FedlineCancelRptOD')
EXEC ('CREATE PROCEDURE [dbo].[sp_FedlineCancelRptOD] AS BEGIN SELECT ''WARNING: Default Definition''	END')
GO

--=======================================================================================
-- Project:			PJ006439 - COWS Reporting 
-- Author:			David Phillips	
-- Date:			09/20/2012
-- Description:		
--					Summary of orders/events, for installs and disconnects, 
--              	that are shown in a Cancelled status. This is the OnDemand.										
--					
-- Modifications:
--
--=======================================================================================

ALTER Procedure [dbo].[sp_FedlineCancelRptOD]
				@startDate  datetime = NULL,
				@endDate  datetime = NULL,
				@getSensData  CHAR(1)='N',
				@SecuredUser  INT = 0
				

AS
BEGIN TRY

	DECLARE @aSQL			VARCHAR(5000)
	
	CREATE TABLE #tmp_fedline_cancel_rpt
	(					
		CustName				VARCHAR(100),
		State					VARCHAR(50),
		Ctry					VARCHAR(50),
		FRB_ID					VARCHAR(25),
		Event_ID				VARCHAR(25),
		Org_ID					VARCHAR(40),
		Event_Stus				VARCHAR(50),
		CanType					VARCHAR(50),
		ActType					VARCHAR(50),
		OrdSubDt				VARCHAR(100),
		ReqActDt				VARCHAR(100),
		DeviceId				VARCHAR(100),
		EventLastAct			VARCHAR(25),
		UpdLastAct				VARCHAR(50),
		OrigSubDt               VARCHAR(50)
	)
	
	SET NOCOUNT ON;
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SET @aSQL='INSERT INTO #tmp_fedline_cancel_rpt(CustName, State, Ctry, FRB_ID, Event_ID, Org_ID,
				Event_Stus, CanType, ActType, OrdSubDt, ReqActDt, DeviceId, EventLastAct, UpdLastAct,
				OrigSubDt)
	SELECT DISTINCT ISNULL(CONVERT(VARCHAR, DecryptByKey(fd.CUST_NME)),'''') --CustName
		   ,ISNULL(CONVERT(VARCHAR, DecryptByKey(fa.STT_CD)),'''') --State
		   ,ISNULL(c.CTRY_NME,'''')  -- Ctry
		   ,ISNULL(CONVERT(VARCHAR, fd.FRB_REQ_ID),'''') -- FRB_ID 
		   ,ISNULL(CONVERT(VARCHAR, fd.EVENT_ID),'''')  --Event_ID
		   ,ISNULL(CONVERT(VARCHAR, fd.ORG_ID),'''') --Org_ID
		   ,ISNULL(ls.EVENT_STUS_DES, '''') -- Event_Stus,
		   ,CASE
				WHEN fd.ORDR_TYPE_CD IN (''NIC'',''NIM'') THEN ''Internal-Tadpole''
				ELSE ''External FRB'' 
			END 	-- CanType	
		   ,ISNULL(ot.PRNT_ORDR_TYPE_DES, '''')  -- ActType
		   ,ISNULL(CONVERT(VARCHAR (12), fd.REQ_RECV_DT, 107), '''') --OrdSubDt
		   ,ISNULL(CONVERT(VARCHAR (26), fd.STRT_TME, 109), '''')  --ReqActDt
		   ,ISNULL(fd.DEV_NME,'''') -- Device ID
		   ,ISNULL(CONVERT(VARCHAR (12),(SELECT max(eh.CREAT_DT) from COWS.dbo.EVENT_HIST eh
					WHERE eh.EVENT_ID = fd.EVENT_ID), 107), '''') -- EventLastAct
		   ,ISNULL((SELECT max(REQ_RECV_DT) from COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA 
					WHERE FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID),
					 fd.CREAT_DT) --UpdLastAct
			,CONVERT(VARCHAR (12),(SELECT min(REQ_RECV_DT) from COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA 
					WHERE EVENT_ID = fd.EVENT_ID), 107) --OrigSubDt
 	 FROM COWS.dbo.FEDLINE_EVENT_TADPOLE_DATA fd
		INNER JOIN COWS.dbo.FEDLINE_EVENT_USER_DATA ud ON ud.EVENT_ID = fd.EVENT_ID 
		INNER JOIN COWS.dbo.LK_EVENT_STUS ls ON ud.EVENT_STUS_ID = ls.EVENT_STUS_ID AND ls.REC_STUS_ID = 1
		INNER JOIN COWS.dbo.LK_FEDLINE_ORDR_TYPE ot ON ot.ORDR_TYPE_CD = fd.ORDR_TYPE_CD
		LEFT OUTER JOIN COWS.dbo.LK_USER lu ON lu.USER_ID = ud.ENGR_USER_ID
		INNER JOIN COWS.dbo.FEDLINE_EVENT_ADR fa ON fa.FEDLINE_EVENT_ID = fd.FEDLINE_EVENT_ID
											AND fa.ADR_TYPE_ID = 18
		INNER JOIN COWS.dbo.LK_CTRY c ON c.CTRY_CD = fa.CTRY_CD
 	
	WHERE fd.ORDR_TYPE_CD in (''DCC'',''NCC'', ''NIC'') 
			AND fd.REC_STUS_ID = 1'

	IF @startDate <> ''
		SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT >= ''' + convert(varchar(20),@startDate) + '''' 
		
	IF @endDate <> ''
		BEGIN
			SET @endDate = DATEADD(day, 1, @endDate)
			SET  @aSQL=@aSQL + ' AND fd.REQ_RECV_DT <= ''' + convert(varchar(20),@endDate) + ''''	
		END
	

	exec (@aSQL);

	SELECT 	
			CASE
				WHEN @SecuredUser = 0 AND CustName <> '' THEN 'Private Customer' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND CustName <> '' THEN 'Private Customer'
				ELSE CustName 
			END				AS 'Customer Name'
			,Org_ID         AS 'ORG ID'
			,CASE
				WHEN @SecuredUser = 0 AND State <> '' THEN 'Private Addr' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND State <> '' THEN 'Private Addr'
				ELSE State 
			END				AS 'State'
			,Ctry			AS 'Country'
			,FRB_ID			AS 'FRB ID'
			,OrigSubDt		AS 'Initial FRB Order Submission Date'
			,OrdSubDt		AS 'FRB Order Submission Date'
			,Event_ID		AS 'COWS Event ID'

			,Event_Stus		AS 'COWS Event Status'
			,CanType		AS 'Cancel Type'
			,ActType		AS 'Activity Type'
			,ReqActDt		AS 'Requested Activity Date/Time'
			,DeviceId		AS 'Device ID/Asset ID'
			,EventLastAct	AS 'Event Last Action Date'
			,UpdLastAct		AS 'Last Update To Requested Activity Date and Time'
	FROM #tmp_fedline_cancel_rpt

	CLOSE SYMMETRIC KEY FS@K3y;
	
	

	IF OBJECT_ID('tempdb..#tmp_fedline_cancel_rpt', 'U') IS NOT NULL	
		drop table #tmp_fedline_cancel_rpt;	


	RETURN 0;
	
END TRY


BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_FedlineCancelRptOD '  
	SET @Desc=@Desc + CONVERT(VARCHAR(12), @startDate, 101) + ', '
		+ CONVERT(VARCHAR(12), @endDate, 101) + ', '
		+ CONVERT(VARCHAR(1),@SecuredUser) + ', ' + @getSensData 
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH




GO
