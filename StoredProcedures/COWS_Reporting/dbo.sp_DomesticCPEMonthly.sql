USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_DomesticCPEMonthly]    Script Date: 08/22/2019 12:55:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
---- [dbo].[sp_RedesignMonthlyReport] 1
---- =============================================
---- Author:         vn370313
---- Create date: 06/06/2017
---- Description: Domestic CPE Monthly Report
---- Updatedby:            vn370313
---- Create date: 09/05/2017
---- Description: Change AND l.DROP_SHP != 'Y'     should be     ISNULL(l.DROP_SHP,'N') != 'Y' 
----              Removed Some Column to avoid multiple Line Items.
---- [dbo].[sp_DomesticCPEMonthly] 0
-- =============================================
ALTER PROCEDURE [dbo].[sp_DomesticCPEMonthly]
  @Secured                    INT=0
AS
BEGIN
       SET NOCOUNT ON;
       
       BEGIN TRY



              DECLARE @startDate   Datetime=DATEADD(mm,-1,CONVERT(VARCHAR(25),DATEADD(DD,-(DAY(GETDATE())-1),GETDATE()),101))
              DECLARE       @endDate      Datetime= GETDATE()
              --SET  @endDate      =DATEADD(dd,-5, CONVERT(VARCHAR(25),GETDATE(),101))
              --print  @startDate
              --Print  @endDate

              
              OPEN SYMMETRIC KEY FS@K3y 
              DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
              --CPE CLLI, SITE ID, ORDER#, DEVICE ID, CUSTOMER NAME, ORDER ACTIVITY (INST OR DISCO), DATE ORDER WAS COMPLETED/ACTIVATED/TERMINATED and if the order is a RECORDS ONLY 


              SELECT DISTINCT f.CPE_VNDR                    AS [Vendor],
                         o.CPE_CLLI           AS [CPE Clli], 
                           c.SITE_ID                    AS [Site Id], 
                           f.FTN, 
                           l.DEVICE_ID                  AS [Device],
						   CASE WHEN ISNULL(l.MNTC_CD,'')='' THEN 'N' ELSE l.MNTC_CD END AS 'MNS', --Added by jrg7298 for Peggy on 8/23/19
                           --ISNULL(CONVERT(VARCHAR, DecryptByKey(c.CUST_NME)),'') AS [CustomerName],
                           --[Customer Name]=CASE @Secured 
                           --                   WHEN '1' THEN 'Private Customer' 
                           --                   ELSE CONVERT(varchar(max), DecryptByKey(c.CUST_NME))
                           --            END,  
                           CASE
                                  WHEN @secured = 0 and o.CSG_LVL_ID =0 THEN c.CUST_NME
                                  WHEN @secured = 0 and o.CSG_LVL_ID >0 AND csd.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER' 
                                  WHEN @secured = 0 and o.CSG_LVL_ID >0 AND csd.CUST_NME IS NULL THEN NULL 
                                  ELSE COALESCE(c.CUST_NME,CONVERT(varchar, DecryptByKey(csd.CUST_NME)),'') 
                                  END
                                  AS [CustomerName],         
                           CASE   
                                  when ORDR_TYPE_CD = 'IN' THEN 'Install'
                                  WHEN ORDR_TYPE_CD = 'DC' THEN 'Disconnect'
                                  ELSE ORDR_TYPE_CD
                           END  AS                           [Order Type],
                           /*
                           MDS_DES                       AS [Equip Description],
                           CNTRC_TYPE_ID                 AS [Contract Type],
                           EQPT_TYPE_ID                  AS [Equipment Type], */ /*vn370313 09052017*/
                           a.CREAT_DT AS [Completion Date],
                           ISNULL(f.CPE_REC_ONLY_CD,'N') AS [Records Only],
                           ISNULL([dbo].[GetCommaSepJprdyCds] (f.ORDR_ID),'') AS [Jeopardy Code]
                           --jpdy.JPRDY_CD AS [Jeopardy Code]
                           
              FROM COWS.dbo.FSA_ORDR f WITH (NOLOCK)
                     INNER JOIN COWS.dbo.ORDR o WITH (NOLOCK) ON f.ORDR_ID = o.ORDR_ID
                     INNER JOIN COWS.dbo.ACT_TASK a WITH (NOLOCK) ON a.ORDR_ID = f.ORDR_ID
                     INNER JOIN COWS.dbo.FSA_ORDR_CPE_LINE_ITEM l WITH (NOLOCK) 
                           ON l.ORDR_ID = f.ORDR_ID AND MATL_CD NOT IN (044747,044746,044745)
                     INNER JOIN COWS.dbo.FSA_ORDR_CUST c WITH (NOLOCK) ON c.ORDR_ID = f.ORDR_ID
                     LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) 
                                                ON csd.SCRD_OBJ_ID=c.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
                     --LEFT OUTER JOIN COWS.dbo.ORDR_JPRDY jpdy WITH (NOLOCK) ON o.ORDR_ID = jpdy.ORDR_ID
              WHERE o.DMSTC_CD = 0
                     AND (a.TASK_ID = 1001 and a.STUS_ID = 0)
                     AND f.PROD_TYPE_CD = 'CP'
                     AND o.ORDR_CAT_ID = 6
                     AND ORDR_TYPE_CD IN ('IN','DC')
                     AND a.CREAT_DT >=@startDate   AND a.CREAT_DT <=@endDate
                     AND (l.EQPT_TYPE_ID NOT IN ('SVC', 'SPK','MSS') AND l.ITM_STUS = 401) 
                     /*AND l.DROP_SHP != 'Y'*/
                     AND ISNULL(l.DROP_SHP,'N') != 'Y'
                     AND o.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')
                     AND ISNULL(c.SITE_ID, '') <> ''
                     AND c.CIS_LVL_TYPE in ('H6')
                     
              
              ORDER BY [CPE Clli], FTN

CLOSE SYMMETRIC KEY FS@K3y;
              

       
       
       END TRY
       BEGIN CATCH
              DECLARE @Desc VARCHAR(200)
              SET @Desc='EXEC COWS_Reporting.dbo.sp_DomesticCPEMonthly '+', ' 
              SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '
              EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;   --- Open later during deployment
       END CATCH
END
