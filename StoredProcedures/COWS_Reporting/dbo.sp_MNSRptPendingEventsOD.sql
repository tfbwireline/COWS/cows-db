USE [COWS_Reporting]
GO
/*
--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			07/12/2011
-- Description:		
--					Extract MDS events in "Pending" status for specified parameter(s);
--					Customer Name and/or Date Range.
--					Extract Completed & Returned To Work Fast Track and MDS event(s) for
--					specified parameter(s); Customer Name, H5_H6_CUST_ID and/or Date Range.
--
-- Notes:			EVENT_STUS_ID: 2 = Pending
--
-- 09/26/2011		sxv0766: Added @h5_h6_ID, @getSensData
--
-- 11/17/2011		sxv0766: Added @SecuredUser
-- 01/25/2012		sxv0766: Specified VARCHAR size of 100 for Cust Name decryption
-- 03/29/2012		sxv0766: Changed Comments field size to VARCHAR(MAX)
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--==================================================================================
*/
ALTER PROCEDURE [dbo].[sp_MNSRptPendingEventsOD] 
 @SecuredUser  INT=0,   
 @getSensData  CHAR(1)='N',   
 @startDate   Datetime=NULL,  
 @endDate   Datetime=NULL  
AS  
BEGIN TRY  
  
 SET NOCOUNT ON;  
   
 DECLARE @startDateStr VARCHAR(20)  
 DECLARE @endDateStr  VARCHAR(20)   
 	SET @startDate=CONVERT(DATETIME, @startDate, 101)  
    SET @endDate=CONVERT(DATETIME, @endDate, 101)      
    -- Add 1 day to End Date in order to cover the End Date 24-hr period  
    SET @endDate=DATEADD(DAY, 1, @endDate)
 --DECLARE @aSQL   VARCHAR(6000)  
 --DECLARE @aSQLd   VARCHAR(3000)  
   
 --CREATE TABLE #tmp_mns_pend  
 --(  
 -- Event_Type    VARCHAR(20),  
 -- Event_ID    INT,  
 -- Event_Status   VARCHAR(50),  
 -- Modified    VARCHAR(25),  
 -- Event_Date    VARCHAR(25),  
 -- Event_Submitted   VARCHAR(25),    
 -- Cust_Name    VARCHAR(100),
 -- Cust_Name2				VARCHAR(100),  
 -- Scurd_Cd    Bit,  
 -- MDS_Acty_Type   VARCHAR(25),    
 -- MDS_Inst_Acty   VARCHAR(100),   
 -- MDS_Mac_Acty   VARCHAR(100),    
 -- Escalations    VARCHAR(5),    
 -- Comments    VARCHAR(MAX),    
 -- Author     VARCHAR(100),  
 -- Editor     VARCHAR(100),    
 -- Assigned_MNS_PM   VARCHAR(100)   
 --)  
   
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;  
 
 
   
 -- MDS Activity Type: Initial Install and MAC   
  SELECT CASE mds.MDS_FAST_TRK_CD  
    WHEN 1 THEN 'MDS Fast Track'  
    WHEN 0 THEN 'MDS Event' END 'Event Type',  
   mds.EVENT_ID 'Event ID',  
   UPPER(evst.EVENT_STUS_DES) 'Event Status',   
   CASE  
    WHEN mds.MODFD_DT is NULL THEN ''  
    ELSE mds.MODFD_DT END 'Modified',  
    --REPLACE(SUBSTRING(CONVERT(varchar,  mds.MODFD_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 13, 8)   
    --+ ' ' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 25, 2) END 'Modified',  
   mds.STRT_TMST 'Event Date',  
   --REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8)  
   -- + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Event Date',  
   mds.CREAT_DT 'Event Submitted On',  
   --REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8)   
   -- + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) 'Event Submitted On',     
   CASE         
    --WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''  
    --WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'  
    --ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
    WHEN @SecuredUser = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
	WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'')
    END 'Customer Name',      
   mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',  
   CASE  
    WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'   
    ELSE '' END 'MDS Install Activity',   
   CASE   
    WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''  
    ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',     
   CASE mds.ESCL_CD  
    WHEN 0 THEN 'No'  
    WHEN 1 THEN 'Yes' END 'Escalations',  
   mds.SHRT_DES 'Reviewer Comments',  
   luser1.DSPL_NME 'Author',  
   CASE  
    WHEN mds.MODFD_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END 'Editor',  
   CASE  
    WHEN luser3.DSPL_NME is NULL THEN ''  
    ELSE luser3.DSPL_NME END 'Assigned Acct MNS PM'  
     
 FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)   
  JOIN COWS.dbo.FSA_MDS_EVENT fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8   
  JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID   
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID  
  LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID    
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MODFD_BY_USER_ID = luser2.USER_ID    
  --LEFT JOIN(SELECT EVENT_ID, MAX(REQ_ID) as maxReqID   
  --  FROM COWS.dbo.ODIE_RSPN_INFO with (nolock)      
  --  GROUP BY EVENT_ID) as oinf ON mds.EVENT_ID = oinf.EVENT_ID  
  --LEFT JOIN COWS.dbo.ODIE_RSPN orsp with (nolock) ON oinf.maxReqID = orsp.REQ_ID   
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID  
    
  -- EVENT_STUS_ID: 2 = Pending   
    
 WHERE mds.EVENT_STUS_ID = 2 AND mds.MDS_ACTY_TYPE_ID <> 3   
  --mds.MODFD_DT >= @startDate AND mds.MODFD_DT < @endDate 
   AND mds.MODFD_DT >=COALESCE(@startDate,mds.MODFD_DT)  AND mds.MODFD_DT <COALESCE(@endDate,mds.MODFD_DT)   
   
 UNION   
  -- MDS Activity Type: Disconnect  
  SELECT CASE mds.MDS_FAST_TRK_CD  
    WHEN 1 THEN 'MDS Fast Track'  
    WHEN 0 THEN 'MDS Event' END 'Event Type',  
   mds.EVENT_ID 'Event ID',  
   UPPER(evst.EVENT_STUS_DES) 'Event Status',   
   CASE  
    WHEN mds.MODFD_DT is NULL THEN ''  
    ELSE mds.MODFD_DT END 'Modified',  
    --REPLACE(SUBSTRING(CONVERT(varchar,  mds.MODFD_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 13, 8)   
    --+ ' ' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 25, 2) END 'Modified',  
   mds.STRT_TMST 'Event Date',  
   --REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8)  
   -- + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Event Date',  
   mds.CREAT_DT 'Event Submitted On',   
   --REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8)   
   -- + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) 'Event Submitted On',     
   CASE         
    --WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''  
    --WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'  
    --ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
    WHEN @SecuredUser = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
	WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'')
    END 'Customer Name',      
   mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',  
   CASE  
    WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'
    ELSE '' END 'MDS Install Activity',   
   CASE   
    WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''  
    ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',      
   CASE mds.ESCL_CD  
    WHEN 0 THEN 'No'  
    WHEN 1 THEN 'Yes' END 'Escalations',     
   mds.SHRT_DES 'Reviewer Comments',  
   luser1.DSPL_NME 'Author',  
   CASE  
    WHEN mds.MODFD_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END 'Editor',  
   CASE  
    WHEN luser3.DSPL_NME is NULL THEN ''  
    ELSE luser3.DSPL_NME END 'Assigned Acct MNS PM'  
     
 FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)     
  JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID  
  LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID    
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MODFD_BY_USER_ID = luser2.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID  
    
  -- EVENT_STUS_ID: 2 = Pending   
    
 WHERE mds.EVENT_STUS_ID = 2 AND mds.MDS_ACTY_TYPE_ID = 3 
  AND mds.MODFD_DT >=COALESCE(@startDate,mds.MODFD_DT)  AND mds.MODFD_DT <COALESCE(@endDate,mds.MODFD_DT)   
  --mds.MODFD_DT >= @startDate AND mds.MODFD_DT < @endDate   
 
 
 /*New MDS Events Code Start */
	UNION ALL
-- MDS Activity Type: Initial Install and MAC   
  SELECT 
   --CASE mds.MDS_FAST_TRK_CD  
   -- WHEN 1 THEN 'MDS Fast Track'  
   -- WHEN 0 THEN 'MDS Event' END 'Event Type', 
   CASE WHEN ISNULL(mds.MDS_FAST_TRK_TYPE_ID,'')='' THEN 'MDS Event' ELSE 'MDS Fast Track' END AS 'Event Type' , 
   mds.EVENT_ID 'Event ID',  
   UPPER(evst.EVENT_STUS_DES) 'Event Status',   
   CASE  
    WHEN mds.MODFD_DT is NULL THEN ''  
    ELSE mds.MODFD_DT END 'Modified',  
    --REPLACE(SUBSTRING(CONVERT(varchar,  mds.MODFD_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 13, 8)   
    --+ ' ' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 25, 2) END 'Modified',  
   mds.STRT_TMST 'Event Date',  
   --REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8)  
   -- + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Event Date',  
   mds.CREAT_DT 'Event Submitted On',  
   --REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8)   
   -- + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) 'Event Submitted On',     
   CASE         
    --WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''  
    --WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'  
    --ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
    WHEN @SecuredUser = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
	WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'')
    END 'Customer Name',      
   mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',  
   CASE  
    WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'   
    ELSE '' END 'MDS Install Activity',   
   CASE   
    WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''  
    ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',     
   CASE mds.ESCL_CD  
    WHEN 0 THEN 'No'  
    WHEN 1 THEN 'Yes' END 'Escalations',  
   mds.SHRT_DES 'Reviewer Comments',  
   luser1.DSPL_NME 'Author',  
   CASE  
    WHEN mds.MODFD_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END 'Editor',  
   CASE  
    WHEN luser3.DSPL_NME is NULL THEN ''  
    ELSE luser3.DSPL_NME END 'Assigned Acct MNS PM'  
     
 FROM COWS.dbo.MDS_EVENT mds with (nolock)   
  JOIN COWS.dbo.FSA_MDS_EVENT fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID   
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
  JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID   
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID  
  LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID    
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MODFD_BY_USER_ID = luser2.USER_ID    
  --LEFT JOIN(SELECT EVENT_ID, MAX(REQ_ID) as maxReqID   
  --  FROM COWS.dbo.ODIE_RSPN_INFO with (nolock)      
  --  GROUP BY EVENT_ID) as oinf ON mds.EVENT_ID = oinf.EVENT_ID  
  --LEFT JOIN COWS.dbo.ODIE_RSPN orsp with (nolock) ON oinf.maxReqID = orsp.REQ_ID   
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID  
    
  -- EVENT_STUS_ID: 2 = Pending   
    
 WHERE mds.EVENT_STUS_ID = 2 AND mds.MDS_ACTY_TYPE_ID <> 3
  AND mds.MODFD_DT >=COALESCE(@startDate,mds.MODFD_DT)  AND mds.MODFD_DT <COALESCE(@endDate,mds.MODFD_DT)    
  --mds.MODFD_DT >= @startDate AND mds.MODFD_DT < @endDate   
   
 UNION   
  -- MDS Activity Type: Disconnect  
  SELECT 
    --CASE mds.MDS_FAST_TRK_CD  
    --WHEN 1 THEN 'MDS Fast Track'  
    --WHEN 0 THEN 'MDS Event' END 'Event Type', 
   CASE WHEN ISNULL(mds.MDS_FAST_TRK_TYPE_ID,'')='' THEN 'MDS Event' ELSE 'MDS Fast Track' END AS 'Event Type' ,
   mds.EVENT_ID 'Event ID',  
   UPPER(evst.EVENT_STUS_DES) 'Event Status',   
   CASE  
    WHEN mds.MODFD_DT is NULL THEN ''  
    ELSE mds.MODFD_DT END 'Modified',  
    --REPLACE(SUBSTRING(CONVERT(varchar,  mds.MODFD_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 13, 8)   
    --+ ' ' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 25, 2) END 'Modified',  
   mds.STRT_TMST 'Event Date',  
   --REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8)  
   -- + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Event Date',  
   mds.CREAT_DT 'Event Submitted On',   
   --REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8)   
   -- + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) 'Event Submitted On',     
   CASE         
    --WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''  
    --WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'  
    --ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME))
    WHEN @SecuredUser = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
	WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @SecuredUser = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
    END 'Customer Name',      
   mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',  
   CASE  
    WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'
    ELSE '' END 'MDS Install Activity',   
   CASE   
    WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''  
    ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',      
   CASE mds.ESCL_CD  
    WHEN 0 THEN 'No'  
    WHEN 1 THEN 'Yes' END 'Escalations',     
   mds.SHRT_DES 'Reviewer Comments',  
   luser1.DSPL_NME 'Author',  
   CASE  
    WHEN mds.MODFD_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END 'Editor',  
   CASE  
    WHEN luser3.DSPL_NME is NULL THEN ''  
    ELSE luser3.DSPL_NME END 'Assigned Acct MNS PM'  
     
 FROM COWS.dbo.MDS_EVENT mds with (nolock)     
  JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID   
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID  
  LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID    
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MODFD_BY_USER_ID = luser2.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID  
    
  -- EVENT_STUS_ID: 2 = Pending   
    
 WHERE mds.EVENT_STUS_ID = 2 AND mds.MDS_ACTY_TYPE_ID = 3 
 AND mds.MODFD_DT >=COALESCE(@startDate,mds.MODFD_DT)  AND mds.MODFD_DT <COALESCE(@endDate,mds.MODFD_DT)  
  --mds.MODFD_DT >= @startDate AND mds.MODFD_DT < @endDate   
/*New MDS Events Code OLD */  
 ORDER BY 'Event Date' DESC  
 
 
 /*  
 -- MDS Activity Type: Initial Install and MAC   
   
  SET @aSQL='INSERT INTO #tmp_mns_pend(Event_Type, Event_ID, Event_Status, Modified, Event_Date, Event_Submitted,  
     Cust_Name, Scurd_Cd, MDS_Acty_Type, MDS_Inst_Acty, MDS_Mac_Acty, Escalations, Comments, Author, Editor, Assigned_MNS_PM)  
    SELECT CASE mds.MDS_FAST_TRK_CD  
    WHEN 1 THEN ''MDS Fast Track''  
    WHEN 0 THEN ''MDS Event'' END ,  
   mds.EVENT_ID ,  
   UPPER(evst.EVENT_STUS_DES) ,     
   CASE  
    WHEN mds.MODFD_DT is NULL THEN ''''  
    ELSE REPLACE(SUBSTRING(CONVERT(varchar,  mds.MODFD_DT,6), 1, 9), '' '', ''-'') + '' '' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 13, 8)   
    + '' '' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 25, 2) END ,  
   REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), '' '', ''-'') + '' '' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8)  
    + '' '' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) ''Event_Date'',  
   REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), '' '', ''-'') + '' '' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8)   
    + '' '' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) ,     
   CASE 						
		WHEN mds.CUST_NME is NULL THEN ''''
		ELSE mds.CUST_NME END ,
	CASE 						
		WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''''
		ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
						
	CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,      
   mact.MDS_ACTY_TYPE_DES ,  
   CASE  
    WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN ''First MDS Implementation''      
    ELSE '''' END ,   
   CASE   
    WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''''  
    ELSE maac.MDS_MAC_ACTY_NME END ,      
   CASE mds.ESCL_CD  
    WHEN 1 THEN ''Yes''  
    ELSE ''No'' END ,  
   mds.SHRT_DES  ,  
   luser1.DSPL_NME ,  
   CASE  
    WHEN mds.MODFD_BY_USER_ID is NULL THEN ''''  
    ELSE luser2.DSPL_NME END ,  
   CASE  
    WHEN luser3.DSPL_NME is NULL THEN ''''  
    ELSE luser3.DSPL_NME END      
 FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)     
  JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID 
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8 
  JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID  
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID  
  LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID    
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MODFD_BY_USER_ID = luser2.USER_ID  
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID    
 WHERE mds.EVENT_STUS_ID = 2 AND mds.MDS_ACTY_TYPE_ID <> 3 '    
   
 -- MDS Activity Type: Disconnect  
   
  SET @aSQLd='SELECT CASE mds.MDS_FAST_TRK_CD  
    WHEN 1 THEN ''MDS Fast Track''  
    WHEN 0 THEN ''MDS Event'' END ,  
   mds.EVENT_ID ,  
   UPPER(evst.EVENT_STUS_DES) ,  
   CASE  
    WHEN mds.MODFD_DT is NULL THEN ''''  
    ELSE REPLACE(SUBSTRING(CONVERT(varchar,  mds.MODFD_DT,6), 1, 9), '' '', ''-'') + '' '' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 13, 8)   
    + '' '' + SUBSTRING(CONVERT(varchar, mds.MODFD_DT, 109), 25, 2) END ,  
   REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), '' '', ''-'') + '' '' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8)  
    + '' '' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) ''Event_Date'',  
   REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), '' '', ''-'') + '' '' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8)   
    + '' '' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) ,     
   CASE 						
		WHEN mds.CUST_NME is NULL THEN ''''
		ELSE mds.CUST_NME END ,
	CASE 						
		WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''''
		ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
						
	CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,      
   mact.MDS_ACTY_TYPE_DES ,  
   CASE  
    WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN ''First MDS Implementation''   
    ELSE '''' END ,    
   CASE   
    WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''''  
    ELSE maac.MDS_MAC_ACTY_NME END ,      
   CASE mds.ESCL_CD  
    WHEN 1 THEN ''Yes''  
    ELSE ''No'' END ,  
   mds.SHRT_DES  ,  
   luser1.DSPL_NME ,  
   CASE  
    WHEN mds.MODFD_BY_USER_ID is NULL THEN ''''  
    ELSE luser2.DSPL_NME END ,  
   CASE  
    WHEN luser3.DSPL_NME is NULL THEN ''''  
    ELSE luser3.DSPL_NME END      
 FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)   
  JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID 
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8 
  JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID  
  LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID    
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID     
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MODFD_BY_USER_ID = luser2.USER_ID  
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID  
 WHERE mds.EVENT_STUS_ID = 2 AND mds.MDS_ACTY_TYPE_ID = 3 '  
     
    
 IF @startDate is NOT NULL AND @endDate is NOT NULL  
  BEGIN  
   -- Do the conversion before passing them to @aSQL  
   SET @startDateStr = @startDate  
   SET @endDateStr = @endDate  
     
   --SET @aSQL=@aSQL + 'AND mds.STRT_TMST >= ''' + @startDateStr + ''' AND mds.STRT_TMST < DATEADD(day, 1, ''' + @endDateStr + ''') '    
   --SET @aSQLd=@aSQLd + 'AND mds.STRT_TMST >= ''' + @startDateStr + ''' AND mds.STRT_TMST < DATEADD(day, 1, ''' + @endDateStr + ''') '  
   SET @aSQL=@aSQL + 'AND mds.MODFD_DT >= ''' + @startDateStr + ''' AND mds.MODFD_DT < DATEADD(day, 1, ''' + @endDateStr + ''') '    
   SET @aSQLd=@aSQLd + 'AND mds.MODFD_DT >= ''' + @startDateStr + ''' AND mds.MODFD_DT < DATEADD(day, 1, ''' + @endDateStr + ''') '  
  END  
       
 SET @aSQL=@aSQL + 'UNION ' + @aSQLd + 'ORDER BY ''Event_Date'' DESC'  
   
 EXEC(@aSQL)  
 --========================  
   
 SELECT Event_Type 'Event Type',    
   Event_ID 'Event ID',   
   Event_Status 'Event Status',   
   Modified,    
   Event_Date 'Event Date',    
   Event_Submitted 'Event Submitted On',  
   CASE   
    --WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'  
    --WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'  
    --ELSE Cust_Name 
    WHEN @SecuredUser = 0 and Scurd_Cd =0 THEN CUST_NAME
	WHEN @SecuredUser = 0 and Scurd_Cd >0 AND  CUST_NAME2 <>'' THEN 'PRIVATE CUSTOMER' 
	WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd >0 AND CUST_NAME2 <> '' THEN 'Private Customer'
	ELSE COALESCE(CUST_NAME2,CUST_NAME,'')
    END 'Customer Name',          
   MDS_Acty_Type 'MDS Activity Type',   
   MDS_Inst_Acty 'MDS Install Activity',   
   MDS_Mac_Acty 'MDS MAC Activity',  
   Escalations,   
   Comments 'Reviewer Comments',   
   Author,     
   Editor,     
   Assigned_MNS_PM 'Assigned Acct MNS PM'  
     
 FROM #tmp_mns_pend  
    
 ---========  
 -- Clean Up  
 --=========    
 IF OBJECT_ID('tempdb..#tmp_mns_pend', 'U') IS NOT NULL   
  drop table #tmp_mns_pend;  
  
  
  */ 
   
 CLOSE SYMMETRIC KEY FS@K3y;    
    
 RETURN 0;  
    
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptPendingEventsOD, '   
 SET @Desc=@Desc  + CAST(@SecuredUser AS VARCHAR(2)) + ', '''  + @getSensData + ''', '   
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  


