USE [COWS_Reporting]
GO
/*
--============================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			01/27/2012
-- Description:		
--					Extract Access Delivery events for specified parameter(s); 
--					Event Complete Date Range.
--
-- Notes:
--
-- 02/22/2012		sxv0766: Removed: Description and Comments, IP Version, Assigned To, 
--							 		  Workflow Status, Activator Comments
--							 Added: Action
-- 8/28/2012        axm3320: Alter the stored procedure to pull Event Title and Customer name sensitive information
--axm3320:3/19/2013 Replace action_id 15(reschedule) with 44 (reschedule-email notfn sent)
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- [dbo].[sp_CANDRptAdOD] null, null, 1, 'N'
--============================================================================================
*/
ALTER PROCEDURE [dbo].[sp_CANDRptAdOD]
	@startDate			Datetime=NULL,
	@endDate			Datetime=NULL,
	@SecuredUser		INT=0,	
	@getSensData		CHAR(1)='N'
AS
BEGIN TRY

	SET NOCOUNT ON;
		
		
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
    
    
	SELECT adev.EVENT_ID 'Event ID', 
			UPPER(evst.EVENT_STUS_DES) 'Event Status', 	
			
			CASE 
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NOT NULL THEN 'Private Customer'
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NULL THEN adev.EVENT_TITLE_TXT
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  = 0  THEN adev.EVENT_TITLE_TXT
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NOT NULL THEN 'Private Customer'
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  > 0 AND csd.EVENT_TITLE_TXT IS NULL THEN adev.EVENT_TITLE_TXT
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  = 0 THEN adev.EVENT_TITLE_TXT
				ELSE CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) 
				END 'Item Title',		
									

			lkes.ENHNC_SRVC_NME 'AD Type',							
			fmsc.FMS_CKT_LIST 'FMS Circuit Info',			
			REPLACE(SUBSTRING(CONVERT(varchar, adev.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, adev.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, adev.STRT_TMST, 109), 25, 2) 'Start Time',
			REPLACE(SUBSTRING(CONVERT(varchar, adev.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, adev.END_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, adev.END_TMST, 109), 25, 2) 'End Time',	
			adev.EVENT_DRTN_IN_MIN_QTY 'Event Duration',
			adev.EXTRA_DRTN_TME_AMT 'Extra Duration',		
			CASE 
			    /*
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  > 0 AND CONVERT(varchar, DecryptByKey(adev.CUST_NME)) <> '' THEN 'Private Customer'
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  > 0 AND CONVERT(varchar, DecryptByKey(adev.CUST_NME)) <> '' THEN 'Private Customer'
				ELSE CONVERT(varchar, DecryptByKey(adev.CUST_NME)) 
				*/
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NOT NULL THEN 'Private Customer'
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NULL THEN adev.CUST_NME
				WHEN @SecuredUser = 0 AND evnt.CSG_LVL_ID  = 0  THEN adev.CUST_NME
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NOT NULL THEN 'Private Customer'
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  > 0 AND csd.CUST_NME IS NULL THEN adev.CUST_NME
				WHEN @SecuredUser = 1 AND @getSensData='N' AND evnt.CSG_LVL_ID  = 0 THEN adev.CUST_NME
				ELSE CONVERT(varchar, DecryptByKey(csd.CUST_NME)) 
				END 'Customer Name',		
	
			
			CASE
				WHEN lkac.ACTN_DES = 'Completed Email Notification' THEN 'Completed' 
				ELSE lkac.ACTN_DES END 'Action',			
			REPLACE(SUBSTRING(CONVERT(varchar, adhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, adhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, adhi.CREAT_DT, 109), 25, 2) 'Modified Date',			
			CASE
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END 'Modified By',				
			CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '' ELSE scev.SUCSS_ACTY_DES END 'Success Activities',		
			CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '' ELSE flev.FAIL_ACTY_DES END 'Failed Activities'
		FROM COWS.dbo.AD_EVENT adev with (nolock) 	
		    left join COWS.dbo.EVENT evnt with (nolock)  on evnt.EVENT_ID = adev.EVENT_ID 
		    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=adev.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=1	
			JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON adev.EVENT_STUS_ID = evst.EVENT_STUS_ID
			JOIN COWS.dbo.LK_ENHNC_SRVC lkes with (nolock) ON adev.ENHNC_SRVC_ID = lkes.ENHNC_SRVC_ID
			JOIN
			(
				SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT
					FROM COWS.dbo.EVENT_HIST eh with (nolock)					
					JOIN COWS.dbo.AD_EVENT acde with (nolock) ON eh.EVENT_ID = acde.EVENT_ID
					WHERE eh.ACTN_ID in (44,19) AND acde.EVENT_STUS_ID in (3, 6) AND acde.STRT_TMST >= @startDate and acde.STRT_TMST < @endDate
				UNION
				SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT
					FROM COWS.dbo.EVENT_HIST ehis with (nolock)
					JOIN
					(
						SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) 'maxEvHis'
							FROM COWS.dbo.EVENT_HIST eh with (nolock)
							JOIN COWS.dbo.AD_EVENT acde with (nolock) ON eh.EVENT_ID = acde.EVENT_ID
							WHERE eh.ACTN_ID in (18) AND acde.EVENT_STUS_ID = 6 AND acde.STRT_TMST >= @startDate and acde.STRT_TMST < @endDate
						GROUP BY eh.EVENT_ID
					)ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis
			)adhi ON adev.EVENT_ID = adhi.EVENT_ID						
			LEFT JOIN
			(
				SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lksu.SUCSS_ACTY_DES  + ', ' AS [text()] 
	 						FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)
	 						JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID 
	 						WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID --AND evsu.REC_STUS_ID = 1
	 						FOR xml PATH ('')
							) as SUCSS_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)	
					)evSucAct
		  		)evSucAct2
			)scev ON adhi.EVENT_HIST_ID = scev.EVENT_HIST_ID
			LEFT JOIN
			(
				SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID
		  		FROM
		  		(
					SELECT (SELECT lkfl.FAIL_ACTY_DES  + ', ' AS [text()] 
	 						FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)
	 						JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID 
	 						WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID --AND evfl.REC_STUS_ID = 1
	 						FOR xml PATH ('')
							) as FAIL_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)	
					)evFailAct
		  		)evFailAct2
			)flev ON adhi.EVENT_HIST_ID = flev.EVENT_HIST_ID			
			LEFT JOIN
			(
				SELECT distinct LEFT([FMS_CKT_LIST],len([FMS_CKT_LIST]) -1) as FMS_CKT_LIST, EVENT_ID
				FROM
				(
					SELECT (SELECT adat.CKT_ID + ', ' AS [text()] 
							FROM COWS.dbo.AD_EVENT_ACCS_TAG adat with (nolock)							
							WHERE adat.EVENT_ID = acde.EVENT_ID 
							FOR xml PATH ('')
						   ) as FMS_CKT_LIST, EVENT_ID 
					FROM 
					( SELECT EVENT_ID FROM COWS.dbo.AD_EVENT with (nolock)
						WHERE EVENT_STUS_ID in (3, 6) AND STRT_TMST >= @startDate AND STRT_TMST < @endDate
					)
					acde
				)fmsci
			)fmsc ON adev.EVENT_ID = fmsc.EVENT_ID			
			JOIN COWS.dbo.LK_USER luser1 with (nolock) ON adhi.CREAT_BY_USER_ID = luser1.USER_ID 
			JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON adhi.ACTN_ID = lkac.ACTN_ID		
			
		--WHERE adev.EVENT_STUS_ID in (6) 					
		
		ORDER BY adev.EVENT_ID

CLOSE SYMMETRIC KEY FS@K3y;
		
										
	RETURN 0;
  
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_CANDRptADOD '  	
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH
