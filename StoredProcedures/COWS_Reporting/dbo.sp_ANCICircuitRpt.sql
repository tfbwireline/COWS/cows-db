USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_ANCICircuitRpt]    Script Date: 09/06/2018 22:59:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--======================================================================================     
-- Author:   David Phillilps   
/*
-- Date:   01/22/2015  
-- Description:    
--          ANCI Circuit Report  
--  
--  
--     FrequencyId:   
--         1 = Weekly  
--         2 = Monthly  
--         0 = Specify start date & end date: 'mm/dd/yyyy'  
--   EXEC COWS_Reporting.dbo.sp_ANCICircuitRpt 2,1
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
-- Updated By:   Md M Monir
-- Updated Date: 09/06/2018
-- Updated Reason: added assigned_user 
--EXEC RAD_Reporting.rad.sp_ReportRefreshQueue_NEW 'N','129'
SELECT * FROM RAD_Reporting.rad.ReportQueue_new with (nolock) where REPORTID in (0,129)
*/
--======================================================================================  
ALTER Procedure [dbo].[sp_ANCICircuitRpt]      
 @FrequencyId  INT=1,   
 @Secured   INT=0,  
 @inStartDtStr  VARCHAR(10)='',  
 @inEndDtStr   VARCHAR(10)=''   
  
AS  
BEGIN TRY  
   
 SET NOCOUNT ON;  
   
 DECLARE @startDate  Datetime  
 DECLARE @endDate  Datetime  
 DECLARE @today   Datetime  
 DECLARE @startDtStr  VARCHAR(10)  
 -----Comment Out---Run Time 
 --DECLARE @FrequencyId int=2,
	--	 @inStartDtStr  VARCHAR(10)='',  
	--	 @inEndDtStr   VARCHAR(10)='' 
   
 IF @FrequencyId=0  
  BEGIN  
  
   SET @startDate=CONVERT(datetime, @inStartDtStr, 101)  
   SET @endDate=CONVERT(datetime, @inEndDtStr, 101)      
  
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @endDate=DATEADD(day, 1, @endDate)   
              
  END   
 ELSE IF @FrequencyId=1  
  BEGIN  
 
   SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(day, -7, GETDATE()), 112) As DateTime)  
   SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)  
      
  END  
 ELSE IF @FrequencyId=2  
  BEGIN      
     SET @today=DATEADD(MONTH,-1,GETDATE())  
       
      -- e.g. Run date is the 1st day of each month   
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000  
        
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));       
     
   SET @startDate=cast(@startDtStr AS datetime)     
   SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)  
   
  END 
   -----Comment Out---Run Time 
   --Print @startDate
   --SET @endDate=DATEADD(day, -10, @endDate) 
   --PRINT @endDate 
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;  
   
  SELECT DISTINCT
		ford.FTN [FTN]
		,CASE
		    /*
			WHEN @Secured = 0 and ordr.SCURD_CD = 0 THEN CONVERT(varchar, DecryptByKey(fcust1.CUST_NME))	
			WHEN @Secured = 0 and ordr.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(fcust1.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @Secured = 0 and ordr.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(fcust1.CUST_NME)) IS NULL THEN NULL 
			ELSE CONVERT(varchar,DecryptByKey(fcust1.CUST_NME))	
			*/
		    WHEN @Secured = 0 AND fcust1.CUST_NME<>''  THEN coalesce(fcust1.CUST_NME,'PRIVATE CUSTOMER')
			WHEN @Secured = 0 AND fcust1.CUST_NME =''  THEN 'PRIVATE CUSTOMER'
			WHEN @Secured = 0 AND fcust1.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'    
			ELSE coalesce(CONVERT(varchar, DecryptByKey(csd2.CUST_NME)),fcust1.CUST_NME,'')	
		 END [H1 Customer Name]
		,fcust1.CUST_ID [H1 Customer ID]
		,fcust2.CUST_ID [H4 Customer ID]
		,ordr.H5_H6_CUST_ID [H5/H6/ES Customer ID]
		,CASE
		    /*
			WHEN @Secured = 0 and ordr.SCURD_CD = 0 THEN CONVERT(varchar, DecryptByKey(fcust3.CUST_NME))	
			WHEN @Secured = 0 and ordr.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(fcust3.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @Secured = 0 and ordr.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(fcust3.CUST_NME)) IS NULL THEN NULL 
			ELSE CONVERT(varchar,DecryptByKey(fcust3.CUST_NME))	
		    */
		    WHEN @Secured = 0 AND fcust3.CUST_NME<>''  THEN coalesce(fcust3.CUST_NME,'PRIVATE CUSTOMER')
			WHEN @Secured = 0 AND fcust3.CUST_NME =''  THEN 'PRIVATE CUSTOMER'
			WHEN @Secured = 0 AND fcust3.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'    
			ELSE coalesce(CONVERT(varchar, DecryptByKey(csd4.CUST_NME)),fcust3.CUST_NME,'')
		END [H5/H6/ES Customer Name]
		,lfpt.FSA_PROD_TYPE_DES [Product Type]
		,lkot.FSA_ORDR_TYPE_DES [Order Type]
		,CONVERT(VARCHAR(12),ISNULL(ordr.CUST_CMMT_DT,ISNULL(ford.CUST_CMMT_DT,''))) [CCD]
		,ISNULL(CONVERT(VARCHAR(12),ford.CUST_WANT_DT),'') [CWD]
		,ISNULL(CONVERT(VARCHAR(12),ford.CREAT_DT),'') [Submit Date]
		,lkos.ORDR_STUS_DES [Order Status]
		,ISNULL(nc.PLN_NME,'') [Private Line]
		,ISNULL(nc.NUA_ADR,'') [NUA Address]
		,ISNULL(fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES,'') [Port Speed]
		,ISNULL(ford.TSUP_RFQ_NBR,'') [RFQ Number]
		,ISNULL(lv.VNDR_NME,'') [Vendor Name]
		
		,ISNULL(ford.TTRPT_ACCS_TYPE_DES,'') [Access Type Description]
		,ISNULL(fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES,'') [Speed of Service]
		,ISNULL(ckt.VNDR_CKT_ID,'') [Partner/Vendor Circuit ID]
		,ISNULL(ckt.LEC_ID,'') [LECID]
		,ISNULL(CONVERT(VARCHAR(12),cktms.ACCS_ACPTC_DT),'') [Access Acceptance Date]
		,ISNULL(CONVERT(VARCHAR(12),oms.ORDR_BILL_CLEAR_INSTL_DT),'') [Order Bill Clear/Install Date]
		,ISNULL(CONVERT(VARCHAR(12),oms.CUST_ACPTC_TURNUP_DT),'') [Close Date]
		,ISNULL(CONVERT(VARCHAR(12),cktms.VNDR_CNFRM_DSCNCT_DT),'') [Vendor Confirmed Disconnect Date]
		,ISNULL(CONVERT(VARCHAR(12),oms.ORDR_DSCNCT_DT),'') [Disconnect Completed Date]
		,CASE
			/*
			WHEN @Secured = 0 and ordr.SCURD_CD = 0 THEN CONVERT(varchar, DecryptByKey(oa.CTY_NME))	
			WHEN @Secured = 0 and ordr.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(oa.CTY_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @Secured = 0 and ordr.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(oa.CTY_NME)) IS NULL THEN NULL 
			ELSE CONVERT(varchar,DecryptByKey(oa.CTY_NME))	
			*/
		    WHEN @Secured = 0 AND oa.CTY_NME<>''  THEN coalesce(oa.CTY_NME,'PRIVATE CITY')
			WHEN @Secured = 0 AND oa.CTY_NME =''  THEN 'PRIVATE CITY'
			WHEN @Secured = 0 AND oa.CTY_NME IS NULL  THEN 'PRIVATE CITY'    
			ELSE coalesce(CONVERT(varchar, DecryptByKey(csd1.CTY_NME)),oa.CTY_NME,'')
			END [City]
		,lc.CTRY_NME [Country]
		,CASE
			/*
			WHEN @Secured = 0 and ordr.SCURD_CD = 0 THEN CONVERT(varchar, DecryptByKey(oa.STREET_ADR_1))	
			WHEN @Secured = 0 and ordr.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(oa.STREET_ADR_1)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @Secured = 0 and ordr.SCURD_CD = 1 AND CONVERT(varchar, DecryptByKey(oa.STREET_ADR_1)) IS NULL THEN NULL 
			ELSE CONVERT(varchar,DecryptByKey(oa.STREET_ADR_1))	
			*/
		    WHEN @Secured = 0 AND oa.STREET_ADR_1<>''  THEN coalesce(oa.STREET_ADR_1,'PRIVATE ADDRESS')
			WHEN @Secured = 0 AND oa.STREET_ADR_1 =''  THEN 'PRIVATE ADDRESS'
			WHEN @Secured = 0 AND oa.STREET_ADR_1 IS NULL  THEN 'PRIVATE ADDRESS'    
			ELSE coalesce(CONVERT(varchar, DecryptByKey(csd1.STREET_ADR_1)),oa.STREET_ADR_1,'')	
		 END [Address]
		,ISNULL(fobl.LINE_ITEM_DES,fobl.ITEM_DES) [Description]
		,fobl.LINE_ITEM_QTY [Quantity]
		,fobl.MRC_CHG_AMT [MRC Amount]
		,fobl.NRC_CHG_AMT [NRC Amount]		
		,CASE  
			WHEN lusr.FULL_NME is NULL THEN ''  
			ELSE [COWS].[dbo].getCommaSeparatedUsers(ordr.ORDR_ID,6,0) END 'Assigned_User'
     
  
  FROM COWS.dbo.FSA_ORDR ford with (nolock)  
   LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = ford.ORDR_ID    
   JOIN COWS.dbo.ORDR ordr with (nolock) ON ford.ORDR_ID = ordr.ORDR_ID
   LEFT JOIN COWS.dbo.ORDR_MS oms with (nolock) ON ford.ORDR_ID = oms.ORDR_ID
   LEFT JOIN COWS.dbo.ORDR_ADR oa with (nolock) ON oa.ORDR_ID = ford.ORDR_ID AND oa.CIS_LVL_TYPE in ('H5','H6','ES')
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd1 WITH (NOLOCK) ON csd1.SCRD_OBJ_ID=oa.ORDR_ADR_ID  AND csd1.SCRD_OBJ_TYPE_ID=14			
   LEFT JOIN COWS.dbo.LK_CTRY lc with (nolock) ON oa.CTRY_CD = lc.CTRY_CD												    
   JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON ordr.ORDR_STUS_ID = lkos.ORDR_STUS_ID   
   JOIN COWS.dbo.LK_FSA_ORDR_TYPE lkot with (nolock) ON ford.ORDR_TYPE_CD = lkot.FSA_ORDR_TYPE_CD  
   JOIN COWS.dbo.FSA_ORDR_CUST fcust1 with (nolock) ON ford.ORDR_ID = fcust1.ORDR_ID AND fcust1.CIS_LVL_TYPE = 'H1' 
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=fcust1.FSA_ORDR_CUST_ID  AND csd2.SCRD_OBJ_TYPE_ID=5    
   LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust2 with (nolock) ON ford.ORDR_ID = fcust2.ORDR_ID AND fcust2.CIS_LVL_TYPE = 'H4' 
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd3 WITH (NOLOCK) ON csd3.SCRD_OBJ_ID=fcust2.FSA_ORDR_CUST_ID  AND csd3.SCRD_OBJ_TYPE_ID=5   
   LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust3 with (nolock) ON ford.ORDR_ID = fcust3.ORDR_ID AND fcust3.CIS_LVL_TYPE in ('H5','H6','ES')
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd4 WITH (NOLOCK) ON csd4.SCRD_OBJ_ID=fcust3.FSA_ORDR_CUST_ID  AND csd4.SCRD_OBJ_TYPE_ID=5   			 
   LEFT JOIN COWS.dbo.LK_FSA_PROD_TYPE lfpt with (nolock) ON ford.PROD_TYPE_CD = lfpt.FSA_PROD_TYPE_CD 
   LEFT JOIN COWS.dbo.NRM_CKT nc with (nolock) ON ford.FTN = nc.FTN AND nc.REC_STUS_ID = 1
   LEFT JOIN COWS.dbo.NRM_SRVC_INSTC nsi with (nolock) ON nc.SRVC_INSTC_OBJ_ID = nsi.SRVC_INSTC_OBJ_ID
												AND nsi.SRVC_INSTC_NME = nc.SRVC_INSTC_NME
												AND nsi.REC_STUS_ID = 1
   LEFT JOIN COWS.dbo.VNDR_ORDR vo with (nolock) ON ford.ORDR_ID = vo.ORDR_ID AND vo.REC_STUS_ID = 1
   LEFT JOIN COWS.dbo.VNDR_FOLDR vf with (nolock) ON vo.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID AND vf.REC_STUS_ID = 1
   LEFT JOIN COWS.dbo.LK_VNDR lv with (nolock) ON vf.VNDR_CD = lv.VNDR_CD AND lv.REC_STUS_ID = 1
   LEFT JOIN COWS.dbo.FSA_ORDR_BILL_LINE_ITEM fobl with (nolock) ON ford.ORDR_ID = fobl.ORDR_ID
   LEFT JOIN COWS.dbo.CKT ckt with (nolock) ON ordr.ORDR_ID = ckt.ORDR_ID
   LEFT JOIN COWS.dbo.CKT_MS cktms with (nolock) ON cktms.CKT_ID = ckt.CKT_ID										
   LEFT JOIN (SELECT distinct uwas.ASMT_DT, uwas.ORDR_ID, uwas.ASN_USER_ID, uwas.GRP_ID  
        FROM COWS.dbo.USER_WFM_ASMT uwas with (nolock)  
        LEFT JOIN COWS.dbo.FSA_ORDR fsao with (nolock) ON uwas.ORDR_ID = fsao.ORDR_ID   
        WHERE uwas.ASMT_DT = (SELECT MAX(ASMT_DT)  
            FROM (SELECT distinct ASMT_DT, ORDR_ID   
              FROM COWS.dbo.USER_WFM_ASMT with (nolock))uswf    
               WHERE uwas.ORDR_ID = uswf.ORDR_ID )  
      )uwfa ON ordr.ORDR_ID =  uwfa.ORDR_ID  
    LEFT JOIN COWS.dbo.LK_USER lusr on uwfa.ASN_USER_ID = lusr.USER_ID 
  WHERE oms.CUST_ACPTC_TURNUP_DT >= @startDate AND oms.CUST_ACPTC_TURNUP_DT < @endDate 
	AND ordr.RGN_ID = 3  
  
  ORDER BY ford.FTN  
     
 CLOSE SYMMETRIC KEY FS@K3y;  
   
 RETURN 0;  
   
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_ANCICircuitRpt '    
 SET @Desc=@Desc + ', ' + CAST(@FrequencyId AS VARCHAR(2)) + ', ' + CAST(@Secured AS VARCHAR(2)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  




GO


