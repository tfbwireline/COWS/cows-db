USE COWS_Reporting
GO
/*
--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			06/14/2011
-- Description:		
--					Extract MDS Disconnect Events per customer.
--					QueryNumber: 1 = Daily
--								 2 = Weekly
--								 3 = Monthly
--								 0 = Specify start date & end date: 'mm/dd/yyyy'
--
-- Note:			MDS Activity Type of Disconnect DOES NOT have FSA_MDS_EVENT record.
--
-- Modifications:
-- [dbo].[sp_MNSRptDisconnectEventsPerCustomer] 3,0
-- 01/25/2012		sxv0766: Specified VARCHAR size of 100 for Cust Name decryption
-- 02/21/2012		sxv0766: Added Reschedule Failed ACTN ID from 31 to 43
-- 03/29/2012		sxv0766: IM952822-Removed resulting string expression from Event Start Date
--					and Event End Date columns
-- 01/27/2017		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--=================================================================================
*/

ALTER Procedure [dbo].[sp_MNSRptDisconnectEventsPerCustomer] 
	@QueryNumber		INT,
	@Secured			INT=0,
	@inStartDtStr		VARCHAR(10)='',
	@inEndDtStr			VARCHAR(10)=''	
AS

BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDate		Datetime
	DECLARE @endDate		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)	
	DECLARE @dayOfWeek		INT

	IF @QueryNumber=0
		BEGIN

			SET @startDate=CONVERT(datetime, @inStartDtStr, 101)
			SET @endDate=CONVERT(datetime, @inEndDtStr, 101)				
			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			SET @endDate=DATEADD(day, 1, @endDate)
			
		END
	ELSE IF @QueryNumber=1
    	BEGIN    	
    		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
		
			SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)   
		END
	ELSE IF @QueryNumber=2
		BEGIN
			-- e.g. Run date is '2011-06-21' 3rd day of the week
			-- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000
			
			SET @today=GETDATE()
			SET @dayOfWeek=DATEPART(dw, @today)
			SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)
							
		END
	ELSE IF @QueryNumber=3
		BEGIN			 
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month 
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
			SET @startDate=cast(@startDtStr AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
		END
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SELECT	DISTINCT
			mds.EVENT_ID 'Event ID',
			mds.H1_ID 'H1',
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
			END 'Customer Name',			
			mds.STRT_TMST 'Start Time',
			mds.END_TMST 'End Time', 
			UPPER(evst.EVENT_STUS_DES) 'Event Status', 
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type', 
			CASE
				WHEN mds.MDS_FAST_TRK_TYPE_ID IS NULL THEN ''
				ELSE mftk.MDS_FAST_TRK_TYPE_DES END 'Fast Track Type',
			CASE
				WHEN luser.DSPL_NME is NULL THEN ''	
				ELSE luser.DSPL_NME END 'MNS PM Name',
			b.ODIE_DEV_NME AS 'Device Name',
			c.MANF_NME AS 'Vendor',
			d.DEV_MODEL_NME AS 'Model',
			b.SERIAL_NBR AS 'Serial Number',
			b.H5_H6 AS 'H6',
			b.THRD_PARTY_CTRCT AS '3rd Party Contract #'
	
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_FAST_TRK_TYPE mftk with (nolock) ON mds.MDS_FAST_TRK_TYPE_ID = mftk.MDS_FAST_TRK_TYPE_ID
		LEFT JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID = luser.USER_ADID
		LEFT JOIN COWS.dbo.EVENT_DISCO_DEV b with (nolock) ON b.EVENT_ID = mds.EVENT_ID
		LEFT JOIN COWS.dbo.LK_DEV_MANF c with (nolock) ON c.MANF_ID = b.MANF_ID
		LEFT JOIN COWS.dbo.LK_DEV_MODEL d with (nolock) ON d.DEV_MODEL_ID = b.DEV_MODEL_ID
		
		--MDS_ACTY_TYPE_ID: 3 = Disconnect
		--Event Status: 2 = Pending, 3 = Rework, 4 = Published, 6 = Completed	
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6)
		AND mds.STRT_TMST >= @startDate AND mds.STRT_TMST < @endDate
	
	--ORDER BY mds.EVENT_ID
	
	/*New MDS Events Code Start */
	UNION ALL

	SELECT	DISTINCT
			mds.EVENT_ID 'Event ID', 
			mds.H1 'H1',  /* COWS.[dbo].[MDS_EVENT_NEW].H1_ID   =   COWS.[dbo].[MDS_EVENT].H1   vn370313 -  01272017*/
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME))
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'')  
			END 'Customer Name',			
			mds.STRT_TMST 'Start Time',
			mds.END_TMST 'End Time', 
			UPPER(evst.EVENT_STUS_DES) 'Event Status', 
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type', 
			CASE
				WHEN mds.MDS_FAST_TRK_TYPE_ID IS NULL THEN ''
				ELSE mftk.MDS_FAST_TRK_TYPE_DES END 'Fast Track Type',
			CASE
				WHEN luser.DSPL_NME is NULL THEN ''	
				ELSE luser.DSPL_NME END 'MNS PM Name',
			b.ODIE_DEV_NME AS 'Device Name',
			c.MANF_NME AS 'Vendor',
			d.DEV_MODEL_NME AS 'Model',
			b.SERIAL_NBR AS 'Serial Number',
			b.H5_H6 AS 'H6',
			b.THRD_PARTY_CTRCT AS '3rd Party Contract #'
	FROM	COWS.dbo.MDS_EVENT mds with (nolock) 
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_FAST_TRK_TYPE mftk with (nolock) ON mds.MDS_FAST_TRK_TYPE_ID = mftk.MDS_FAST_TRK_TYPE_ID
		LEFT JOIN COWS.dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
		LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID  = luser.USER_ADID
		LEFT JOIN COWS.dbo.EVENT_DISCO_DEV b with (nolock) ON b.EVENT_ID = mds.EVENT_ID
		LEFT JOIN COWS.dbo.LK_DEV_MANF c with (nolock) ON c.MANF_ID = b.MANF_ID
		LEFT JOIN COWS.dbo.LK_DEV_MODEL d with (nolock) ON d.DEV_MODEL_ID = b.DEV_MODEL_ID
		--MDS_ACTY_TYPE_ID: 3 = Disconnect
		--Event Status: 2 = Pending, 3 = Rework, 4 = Published, 6 = Completed	
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (2, 3, 4, 6)
		AND mds.STRT_TMST >= @startDate AND mds.STRT_TMST < @endDate
	
	ORDER BY mds.EVENT_ID


	/*New MDS Events Code END */


	CLOSE SYMMETRIC KEY FS@K3y; 
	
	RETURN 0;		
	

END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptDisconnectEventsPerCustomer ' + CAST(@QueryNumber AS VARCHAR(4)) 
	SET @Desc=@Desc + ',' + CAST(@Secured AS VARCHAR(2)) + ': '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH


