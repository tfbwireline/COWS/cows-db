/*

---- ===========================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- =============================================
*/
ALTER PROCEDURE [dbo].[sp_Orders_for_Metrics] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
     --Insert statements for procedure here
(SELECT * FROM         
(Select distinct
  CASE 
		WHEN c.ORDR_CAT_ID = 1 THEN convert(varchar(50),z.ORDR_ID)
		WHEN c.ORDR_CAT_ID = 2 THEN fs.FTN
		WHEN c.ORDR_CAT_ID = 6 THEN fs.FTN END [Order_ID],
		
   h1.CUST_ID [H1_ID], 
   h4.CUST_ID [H4_ID], 
   fs.PRNT_FTN [Parent_FTN],
    
   CASE 
		WHEN c.ORDR_CAT_ID = 1 THEN z.H5_CUST_ACCT_NBR
		WHEN c.ORDR_CAT_ID = 2 THEN h5.CUST_ID
		WHEN c.ORDR_CAT_ID = 6 THEN h5.CUST_ID END [H5_ID],
		
   h6.CUST_ID [H6_ID],
   x.FULL_NME [COWS_IPM],	 	 	 
   x.USER_ADID [COWS_IPM_AD_ID],	
   tac.FRST_NME + tac.LST_NME [FSA_IPM],	
   tac.EMAIL_ADR [FSA_IPM_Email_Address],	
   CASE 
		WHEN tk.CREAT_DT > nt.CREAT_DT THEN tk.CREAT_DT
    ELSE nt.CREAT_DT END [Last_Modified_Date_time],
    
    nm.NUA_ADR [NUA],
    nm.PLN_NME [PL#],
    
	CASE 
		WHEN c.ORDR_CAT_ID = 1 AND 	cd.NEW_CCD_DT IS NULL THEN z.CUST_CMMT_DT
		WHEN c.ORDR_CAT_ID = 2 AND 	cd.NEW_CCD_DT IS NULL THEN fs.CUST_CMMT_DT
		WHEN c.ORDR_CAT_ID = 6 AND 	cd.NEW_CCD_DT IS NULL THEN fs.CUST_CMMT_DT 
		WHEN cd.NEW_CCD_DT IS NOT NULL THEN cd.NEW_CCD_DT END [CCD],
		
      CASE
		  WHEN tk.TASK_ID = 1001 THEN tk.CREAT_DT ELSE NULL END [Bill_Clear_Date],
		  
 	  CASE
		--WHEN c.SCURD_CD = 0 AND c.ORDR_CAT_ID = 1 THEN CONVERT(varchar, DecryptByKey(w.CUST_NME))	
		--WHEN c.SCURD_CD = 1 AND c.ORDR_CAT_ID = 1 AND CONVERT(varchar, DecryptByKey(w.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
		--WHEN c.SCURD_CD = 1 AND c.ORDR_CAT_ID = 1 AND CONVERT(varchar, DecryptByKey(w.CUST_NME)) IS NULL THEN NULL 
		--WHEN c.SCURD_CD = 0 AND c.ORDR_CAT_ID = 2 THEN si.CUST_NME	
		--WHEN c.SCURD_CD = 1 AND c.ORDR_CAT_ID = 2 AND si.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER' 
		--WHEN c.SCURD_CD = 1 AND c.ORDR_CAT_ID = 2 AND si.CUST_NME IS NULL THEN NULL
		--WHEN c.SCURD_CD = 0 AND c.ORDR_CAT_ID = 6 THEN si.CUST_NME	
		--WHEN c.SCURD_CD = 1 AND c.ORDR_CAT_ID = 6 AND si.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER' 
		--WHEN c.SCURD_CD = 1 AND c.ORDR_CAT_ID = 6 AND si.CUST_NME IS NULL THEN NULL 
		c.ORDR_CAT_ID
		WHEN 6 THEN
			 CASE  
				WHEN c.CSG_LVL_ID =0 THEN  si.CUST_NME
				WHEN c.CSG_LVL_ID >0 AND   si.CUST_NME2 IS NOT NULL THEN  'PRIVATE CUSTOMER'
				WHEN c.CSG_LVL_ID >0 AND   si.CUST_NME2 IS NULL THEN  ''
				ELSE  COALESCE(si.CUST_NME,'') END
		WHEN 2 THEN
			 CASE  
				WHEN c.CSG_LVL_ID =0 THEN  si.CUST_NME
				WHEN c.CSG_LVL_ID >0 AND  si.CUST_NME2 IS NOT NULL THEN  'PRIVATE CUSTOMER'
				WHEN c.CSG_LVL_ID >0 AND  si.CUST_NME2 IS NULL THEN  ''
				ELSE COALESCE(si.CUST_NME,'') END
		WHEN 1 THEN
			 CASE  
				WHEN c.CSG_LVL_ID =0 THEN  w.CUST_NME
				WHEN c.CSG_LVL_ID >0 AND CONVERT(VARCHAR, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN  'PRIVATE CUSTOMER'
				WHEN c.CSG_LVL_ID >0 AND CONVERT(VARCHAR, DecryptByKey(csd.CUST_NME)) IS NULL THEN  ''
				ELSE COALESCE(w.CUST_NME,'') END			
		ELSE COALESCE(si.CUST_NME,w.CUST_NME,'' )
		END  [Customer_Name],
		
	CASE 
		WHEN c.ORDR_CAT_ID = 1 AND h.ESCALAT_ORDR_CD = 0 THEN 'N'
		WHEN c.ORDR_CAT_ID = 1 AND h.ESCALAT_ORDR_CD = 1 THEN 'Y'
		WHEN c.ORDR_CAT_ID = 6 THEN fs.INSTL_ESCL_CD
		WHEN c.ORDR_CAT_ID = 2 THEN fs.INSTL_ESCL_CD END [Escalation_Flag],
		
	u.EXP_TYPE_DES [Expedite],
	
	CASE 
		WHEN c.ORDR_CAT_ID = 1 THEN m.ORDR_TYPE_DES
		WHEN c.ORDR_CAT_ID = 6 THEN s.FSA_ORDR_TYPE_DES
		WHEN c.ORDR_CAT_ID = 2 THEN s.FSA_ORDR_TYPE_DES END [Order_Type],
		
    fs.ORDR_SUB_TYPE_CD [Order_Sub_Type],
    
 	CASE 
		WHEN c.ORDR_CAT_ID = 1 THEN t.PROD_TYPE_DES
		WHEN c.ORDR_CAT_ID = 6 THEN  i.PROD_TYPE_DES
		WHEN c.ORDR_CAT_ID = 2 THEN  i.PROD_TYPE_DES END [Product_Type],
		
 	CASE 
		WHEN c.ORDR_CAT_ID = 1 THEN  z.SOI
		WHEN c.ORDR_CAT_ID = 6 THEN  si.SOI_CD
		WHEN c.ORDR_CAT_ID = 2 THEN  si.SOI_CD END [SOI],
       
    j.PLTFRM_NME [Platform_Name],
    o.STUS_DES [Order_Status],
    q.JPRDY_CD [Jeopardy_Code] ,
    r.JPRDY_DES [Jeopardy_Description],
    nt.NTE_TXT  [COWS_Notes]
      
from 
  	  [COWS].[dbo].[ORDR] c  LEFT OUTER JOIN
     (SELECT bb.FTN, bb.ORDR_ID, bb.ORDR_ACTN_ID, bb.PROD_TYPE_CD, bb.ORDR_TYPE_CD, 
		bb.CUST_CMMT_DT, bb.INSTL_ESCL_CD, bb.ORDR_SUB_TYPE_CD, bb.PRNT_FTN, cc.DMSTC_CD
		FROM [COWS].[dbo].[FSA_ORDR] bb LEFT OUTER JOIN
		[COWS].[dbo].[ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
		FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] ) fsa 
		WHERE fsa.FTN = bb.FTN AND bb.ORDR_ACTN_ID <> 1  AND (cc.DMSTC_CD = 1) ) 
) fs on c.ORDR_ID = fs.ORDR_ID LEFT OUTER JOIN
		 	  
  	 (SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME)) [FRST_NME], CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar, DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR], bb.CNTCT_TYPE_ID 
		FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.ROLE_ID = 13) tac on c.ORDR_ID  = tac.ORDR_ID LEFT OUTER JOIN
		
      [COWS].[dbo].[LK_CNTCT_TYPE] e with (nolock) on  tac.CNTCT_TYPE_ID = e.CNTCT_TYPE_ID LEFT OUTER JOIN
      --[COWS].[dbo].[NRM_CKT] f with (nolock) on fs.FTN = f.FTN LEFT OUTER JOIN
      [COWS].[dbo].[TRPT_ORDR] g with (nolock) on c.ORDR_ID = g.ORDR_ID LEFT OUTER JOIN
      [COWS].[dbo].[ORDR_EXP] h with (nolock) on c.ORDR_ID = h.ORDR_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_EXP_TYPE] l with (nolock) on h.EXP_TYPE_ID = l.EXP_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_PROD_TYPE] i with (nolock) on fs.PROD_TYPE_CD = i.FSA_PROD_TYPE_CD LEFT OUTER JOIN
   	  [COWS].[dbo].[lk_FSA_ORDR_TYPE] s with (nolock) on fs.ORDR_TYPE_CD = s.FSA_ORDR_TYPE_CD LEFT OUTER JOIN
      [COWS].[dbo].[PLTFRM_MAPNG] k with (nolock) on c.[PLTFRM_CD] = k.PLTFRM_CD LEFT OUTER JOIN
      [COWS].[dbo].[LK_PLTFRM] j with (nolock) on k.PLTFRM_CD = j.PLTFRM_CD LEFT OUTER JOIN
      
 	  (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID 
 	   FROM [COWS].[dbo].[ACT_TASK] bb LEFT OUTER JOIN
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)
			FROM (SELECT ACT_TASK_ID, ORDR_ID FROM [COWS].[dbo].[ACT_TASK] ) tsk 
			WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk on c.ORDR_ID = tk.ORDR_ID LEFT OUTER JOIN
			
      [COWS].[dbo].[LK_TASK] n with (nolock) on tk.TASK_ID = n.TASK_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_STUS]o with (nolock) on n.ORDR_STUS_ID = o.STUS_ID LEFT OUTER JOIN
      [COWS].[dbo].[ORDR_NTE] p with (nolock) on c.ORDR_ID = p.ORDR_ID LEFT OUTER JOIN
      [COWS].[dbo].[ORDR_JPRDY] q with (nolock) on c.ORDR_ID = q.ORDR_ID LEFT OUTER JOIN 
      [COWS].[dbo].[LK_JPRDY] r with (nolock) on q.JPRDY_CD = r.JPRDY_CD LEFT OUTER JOIN
      
  	  (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID 
  		FROM [COWS].[dbo].[USER_WFM_ASMT] bb LEFT OUTER JOIN 
	    [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
	    WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT)
		FROM (SELECT DISTINCT ASMT_DT, ORDR_ID FROM [COWS].[dbo].[USER_WFM_ASMT] ) usr 	
		WHERE usr.ORDR_ID  = bb.ORDR_ID )
			AND bb.ASN_USER_ID  = (SELECT MAX(ASN_USER_ID) 
			FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID FROM [COWS].[dbo].[USER_WFM_ASMT] )uas
			WHERE uas.ORDR_ID = bb.ORDR_ID)) ur 
			on  c.ORDR_ID = ur.ORDR_ID LEFT OUTER JOIN
			
      [COWS].[dbo].[LK_USER] x with (nolock) on ur.ASN_USER_ID =  x.USER_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_ROLE]y with (nolock) on tac.ROLE_ID = y.ROLE_ID   LEFT OUTER JOIN
      [COWS].[dbo].[IPL_ORDR] z with (nolock) on c.ORDR_ID = z.ORDR_ID LEFT OUTER JOIN
      
      (SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE = 'H1') h1
		on c.ORDR_ID = h1.ORDR_ID LEFT OUTER JOIN
		
	  (SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE = 'H4') h4
		on  c.ORDR_ID = h4.ORDR_ID LEFT OUTER JOIN
		
	  (SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE = 'H5') h5
		on  c.ORDR_ID = h5.ORDR_ID LEFT OUTER JOIN	
			
		(SELECT bb.CUST_ID, bb.ORDR_ID, bb.SOI_CD FROM [COWS].[dbo].[FSA_ORDR_CUST] bb JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE = 'H6') h6
		on  c.ORDR_ID = h6.ORDR_ID LEFT OUTER JOIN
			
	-- axm3320 10/13/2011 view throwing error for CUST_NME field as field has been removed from ORDR table
	-- adding CUST_NME in select below		 	
 	  (SELECT bb.ORDR_ID, bb.SOI_CD, CONVERT(varchar, DecryptByKey(csd.CUST_NME)) [CUST_NME2],bb.CUST_NME [CUST_NME]
 	   FROM [COWS].[dbo].[FSA_ORDR_CUST] bb LEFT OUTER JOIN
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5 
		WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)
			FROM (SELECT SOI_CD, ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] ) soi
			WHERE soi.ORDR_ID = bb.ORDR_ID)) si on  c.ORDR_ID = si.ORDR_ID LEFT OUTER JOIN
										
	  (SELECT bb.NTE_ID, bb.ORDR_ID, bb.NTE_TXT, bb.CREAT_DT FROM [COWS].[dbo].[ORDR_NTE] bb LEFT OUTER JOIN 
		[COWS].[dbo].[FSA_ORDR] cc        on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.NTE_ID = (SELECT MAX(NTE_ID)
			FROM (SELECT NTE_ID, ORDR_ID FROM [COWS].[dbo].[ORDR_NTE] ) nte WHERE
		nte.ORDR_ID = bb.ORDR_ID)) nt on  c.ORDR_ID = nt.ORDR_ID LEFT OUTER JOIN
		
	  (SELECT bb.NRM_CKT_ID, bb.FTN, bb.NUA_ADR, bb.PLN_NME FROM [COWS].[dbo].[NRM_CKT] bb LEFT OUTER JOIN 
		[COWS].[dbo].[FSA_ORDR] cc        on cc.FTN = bb.FTN 
		WHERE bb.NRM_CKT_ID = (SELECT MAX(NRM_CKT_ID)
			FROM (SELECT NRM_CKT_ID, FTN FROM [COWS].[dbo].[NRM_CKT] ) nrm WHERE
		nrm.FTN = bb.FTN)) nm on  fs.FTN = nm.FTN LEFT OUTER JOIN

      [COWS].[dbo].[LK_ORDR_TYPE] m with (nolock) on z.ORDR_TYPE_ID = m.ORDR_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_PROD_TYPE] t with (nolock) on z.PROD_TYPE_ID = t.PROD_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_EXP_TYPE] u with (nolock) on l.EXP_TYPE_ID = u.EXP_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[H5_FOLDR] w with (nolock) on c.H5_FOLDR_ID = w.H5_FOLDR_ID  LEFT OUTER JOIN 
      [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=w.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6
      LEFT OUTER JOIN
      (SELECT bb.ORDR_ID, bb.NEW_CCD_DT, bb.CCD_HIST_ID
 	   FROM [COWS].[dbo].[CCD_HIST] bb LEFT OUTER JOIN
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)
			FROM (SELECT CCD_HIST_ID, ORDR_ID FROM [COWS].[dbo].[CCD_HIST] ) ccd
			WHERE ccd.ORDR_ID = bb.ORDR_ID)) cd on  c.ORDR_ID = cd.ORDR_ID 

      --[COWS].[dbo].[CCD_HIST] v on c.ORDR_ID = v.ORDR_ID
Where c.ORDR_CAT_ID != 3


) lv
WHERE lv.[Order_ID] IS NOT NULL)





CLOSE SYMMETRIC KEY FS@K3y 


END
