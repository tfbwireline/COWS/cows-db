-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>    
-- Updated By:   Md M Monir  
-- Updated Date: 03/14/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_    
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME 
-- EXEC COWS_Reporting.dbo.sp_AMNCISprintFacilityPending 4, 0    
-- =============================================    
ALTER PROCEDURE [dbo].[sp_AMNCIRptSprintFacilityPending]    
     @secured   BIT = 0,    
  @inStartDtStr    VARCHAR(10)= '',    
     @inEndDtStr   VARCHAR(10)= ''    
    
AS    
BEGIN TRY    
 SET NOCOUNT ON;    
 DECLARE @startDate  DATETIME    
 DECLARE @endDate  DATETIME    
 DECLARE @today   DATETIME    
 DECLARE @dayOfWeek  INT    
     
--DECLARE @secured  BIT = 0    
--DECLARE @inStartDtStr   VARCHAR(10)= '09/01/2012'    
--DECLARE @inEndDtStr  VARCHAR(10)= '01/30/2012'    
    
 SET @startDate=CONVERT(DATETIME, @inStartDtStr, 101)    
 SET @endDate=CONVERT(DATETIME, @inEndDtStr, 101)    
    
 SET @today=GETDATE()    
 SET @dayOfWeek=DATEPART(dw, @today)    
 SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+90), @today), 112) As DATETIME)    
 SET @endDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS DATETIME)    
     
 OPEN SYMMETRIC KEY FS@K3y     
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;     
    
    
 OPEN SYMMETRIC KEY FS@K3y     
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;     
    
    
 SELECT DISTINCT     
  a.FTN [Order Tracking No],     
 i.SALS_CHNL_DES [Sales Region],    
 l.CTRY_NME [Sales Sub Region], 
 CASE WHEN (b.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE l.CTRY_NME END [Sales Sub Region], /*Monir 06062018*/    
  CASE    
   WHEN @secured = 0 AND si.CUST_NME<>''  THEN coalesce(si.CUST_NME,'PRIVATE CUSTOMER')  
   WHEN @secured = 0 AND si.CUST_NME =''  THEN 'PRIVATE CUSTOMER'  
   WHEN @secured = 0 AND si.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'      
   ELSE coalesce(si.CUST_NME2,si.CUST_NME,'')  
   END [Customer Name], --Secured    
  d.PROD_TYPE_DES [Product],      
' ' [Order Bandwidth],    
  gom.FRST_NME + ' ' + gom.LST_NME [Implementation Contact],    
  cm.FRST_NME + ' ' + cm.LST_NME [CPM Contact],     
  CONVERT(VARCHAR(10),a.CUST_CMMT_DT,101) [Customer Commit Date],      
  CONVERT(VARCHAR(10),a.CUST_WANT_DT,101) [Sprint Target Delivery Date],      
 r.PLN_NME [Private Line Number],    
' ' [Circuit Access Number],    
' ' [Network Address],    
  f.ORDR_STUS_DES [Order Status],      
    
' ' [Complete order was received],    
' ' [Incomplete order was received],    
 n.FULL_NME [Manager],    
 mg.FULL_NME [Manager_2],    
 CONVERT(VARCHAR(10),GetDate(),101) [Today],    
  CASE    
   WHEN a.CUST_WANT_DT IS NULL THEN 'No STDD'    
   WHEN CONVERT(VARCHAR(10),a.CUST_WANT_DT,101) >= CONVERT(VARCHAR(10),GetDate(),101) THEN 'Pending'     
   WHEN [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(), a.CUST_WANT_DT) < 0 THEN 'Past'     
   END [Past Due with STDD],    
  CASE    
   WHEN a.CUST_CMMT_DT IS NULL THEN 'No CCD'    
   WHEN CONVERT(VARCHAR(10),a.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),GetDate(),101) THEN 'Pending'     
   WHEN [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(), a.CUST_CMMT_DT) < 0 THEN 'Past'     
   END [Past Due with CCD],    
 a.ORDR_TYPE_CD [Order Type],    
  CASE    
   WHEN a.CUST_CMMT_DT IS NULL THEN 'No CCD'    
   WHEN CONVERT(VARCHAR(10),a.CUST_CMMT_DT,101) >= CONVERT(VARCHAR(10),GetDate(),101) THEN 'Pending'     
   WHEN [Reporting].[dbo].BusinessDayDayZeroIntervalLag(GETDATE(), a.CUST_CMMT_DT) < 0 THEN 'Past'     
   ELSE 'Due in 2 weeks' END [CCD Past Due Status],    
' ' [Equipment Order],    
 --m.MRC_CHG_AMT [MRC],    
 --m.NRC_CHG_AMT [NRC],    
    
 --cast(m.MRC_CHG_AMT as money) [MRC],    
   ISNULL(CAST(b5.MRC_CHG_AMT AS money), 0) + ISNULL(CAST(b6.MRC_CHG_AMT AS money), 0)      
  + ISNULL(CAST(b1.MRC_CHG_AMT AS money), 0)+ ISNULL(CAST(b2.MRC_CHG_AMT AS money), 0)     
  + ISNULL(CAST(b3.MRC_CHG_AMT AS money), 0) + ISNULL(CAST(b4.MRC_CHG_AMT AS money), 0)     
    + ISNULL(CAST(b7.MRC_CHG_AMT AS money), 0) [MRC],    
 --cast(m.NRC_CHG_AMT as money) [NRC],    
    
  ISNULL(CAST(b5.NRC_CHG_AMT AS money), 0) +  ISNULL(CAST(b6.NRC_CHG_AMT AS money), 0)      
   +  ISNULL(CAST(b1.NRC_CHG_AMT AS money), 0) +  ISNULL(CAST(b2.NRC_CHG_AMT AS money), 0)     
   + ISNULL(CAST(b3.NRC_CHG_AMT AS money), 0)   +  ISNULL(CAST(b4.NRC_CHG_AMT AS money), 0)    
    +  ISNULL(CAST(b7.NRC_CHG_AMT AS money), 0) [NRC],    
    
    
'n/a' [OESOTS #],    
 a.INSTL_SOLU_SRVC_DES [Order Desc],    
  h.A_END_REGION [Asset Region],    
' ' [Origination Country],    
' ' [Termination Country],    
' ' [End user],    
' ' [Regional manager],    
[GOM Receives Order],    
' ' [Service Manager]    
    
 FROM [COWS].[dbo].[FSA_ORDR] a WITH (NOLOCK)    
  LEFT OUTER JOIN [COWS].[dbo].[ORDR] b WITH (NOLOCK) ON a.ORDR_ID = b.ORDR_ID    
  LEFT OUTER JOIN [COWS].[dbo].[ORDR_MS] c WITH (NOLOCK) ON a.ORDR_ID = c.ORDR_ID     
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, CONVERT(VARCHAR, DecryptByKey(bb.FRST_NME)) [FRST_NME],     
     CONVERT(VARCHAR, DecryptByKey(bb.LST_NME)) [LST_NME]--, CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]     
    FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK)    
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc  WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID     
    WHERE bb.ROLE_ID = 13 AND bb.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)     
     FROM (SELECT ORDR_CNTCT_ID, ORDR_ID     
      FROM [COWS].[dbo].[ORDR_CNTCT] WITH (NOLOCK) WHERE ROLE_ID = 13) oc     
     WHERE oc.ORDR_ID = bb.ORDR_ID)) gom on gom.ORDR_ID = a.ORDR_ID     
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, CONVERT(VARCHAR, DecryptByKey(bb.FRST_NME)) [FRST_NME],     
     CONVERT(VARCHAR, DecryptByKey(bb.LST_NME)) [LST_NME]--, CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]     
    FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK)     
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID     
    WHERE bb.ROLE_ID = 6 AND bb.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)     
     FROM (SELECT ORDR_CNTCT_ID, ORDR_ID     
      FROM [COWS].[dbo].[ORDR_CNTCT] WITH (NOLOCK) WHERE ROLE_ID = 6) oc     
     WHERE oc.ORDR_ID = bb.ORDR_ID)) cm on cm.ORDR_ID = a.ORDR_ID    
  LEFT OUTER JOIN (SELECT bb.ORDR_ID, CONVERT(VARCHAR, DecryptByKey(bb.FRST_NME)) [FRST_NME],     
     CONVERT(VARCHAR, DecryptByKey(bb.LST_NME)) [LST_NME]--, CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]     
    FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK)     
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID     
    WHERE bb.ROLE_ID = 35 AND bb.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)     
     FROM (SELECT ORDR_CNTCT_ID, ORDR_ID     
      FROM [COWS].[dbo].[ORDR_CNTCT] WITH (NOLOCK) WHERE ROLE_ID = 35) oc     
     WHERE oc.ORDR_ID = bb.ORDR_ID)    
     ) ipm on ipm.ORDR_ID = a.ORDR_ID     
  LEFT OUTER JOIN   
  ( SELECT   
  bb.ORDR_ID,   
  bb.SOI_CD,   
  bb.CIS_LVL_TYPE,   
  CONVERT(VARCHAR, DecryptByKey(csd.CUST_NME)) as [CUST_NME2],  
  bb.CUST_NME as [CUST_NME]    
    FROM [COWS].[dbo].[FSA_ORDR_CUST] bb WITH (NOLOCK)     
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID   
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5    
    WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)    
     FROM (SELECT SOI_CD, ORDR_ID     
      FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi    
     WHERE soi.ORDR_ID = bb.ORDR_ID)  
     ) si on si.ORDR_ID = a.ORDR_ID     
  LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID     
    FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)    
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) on bb.ORDR_ID = cc.ORDR_ID     
    WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)    
    FROM (SELECT ACT_TASK_ID, ORDR_ID     
     FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk     
     WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk on tk.ORDR_ID = a.ORDR_ID    
  LEFT OUTER JOIN (SELECT ORDR_ID, CONVERT(VARCHAR(10),MIN(at.CREAT_DT),101) as [GOM Receives Order]    
    FROM [COWS].dbo.ACT_TASK at WITH (NOLOCK)    
    JOIN [COWS].dbo.MAP_GRP_TASK mgt WITH (NOLOCK) on mgt.TASK_ID=at.TASK_ID    
    WHERE mgt.GRP_ID=1 AND at.TASK_ID NOT IN (101,102)    
    GROUP BY ORDR_ID) gq ON gq.ORDR_ID = a.ORDR_ID    
  LEFT OUTER JOIN (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID    
     FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)    
     LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID     
   WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT)    
    FROM (SELECT DISTINCT ASMT_DT, ORDR_ID     
     FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr      
    WHERE usr.ORDR_ID  = bb.ORDR_ID )    
    AND bb.ASN_USER_ID  = (SELECT MAX(ASN_USER_ID)     
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID     
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas    
     WHERE uas.ORDR_ID = bb.ORDR_ID)) ur ON ur.ORDR_ID = a.ORDR_ID     
  LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] d WITH (NOLOCK) ON a.PROD_TYPE_CD = d.FSA_PROD_TYPE_CD    
  LEFT OUTER JOIN [COWS].[dbo].[LK_TASK] e  WITH (NOLOCK) ON tk.TASK_ID = e.TASK_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_STUS] f WITH (NOLOCK) ON e.ORDR_STUS_ID = f.ORDR_STUS_ID    
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_GOM_XNCI] g WITH (NOLOCK) ON a.ORDR_ID = g.ORDR_ID    
  LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] h WITH (NOLOCK) ON a.ORDR_ID = h.ORDR_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_SALS_CHNL] i WITH (NOLOCK) ON h.SALS_CHNL_ID = i.SALS_CHNL_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] j WITH (NOLOCK) ON j.FSA_ORDR_TYPE_CD = a.ORDR_TYPE_CD     
  LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] r WITH (NOLOCK) ON r.FTN = a.FTN     
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] k WITH (NOLOCK) ON b.RGN_ID = k.RGN_ID     
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON l.CTRY_CD = r.TOC_CTRY_CD 
  LEFT OUTER JOIN COWS.dbo.[CUST_SCRD_DATA] csd WITH (NOLOCK) ON csd.[SCRD_OBJ_ID]=r.[NRM_CKT_ID] and csd.[SCRD_OBJ_TYPE_ID]=29
  LEFT JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd.CTRY_CD)) = lcs.CTRY_CD    
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] m WITH (NOLOCK) ON a.ORDR_ID = m.ORDR_ID     
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] n WITH (NOLOCK) ON ur.ASN_USER_ID = n.USER_ID     
     LEFT OUTER JOIN [COWS].[dbo].[LK_USER] mg WITH (NOLOCK) ON n.MGR_ADID = mg.USER_ADID     
    
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT,     
 bb.NRC_CHG_AMT FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
    JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
    WHERE bb.BILL_ITEM_TYPE_ID = 1) b1 on b.ORDR_ID = b1.ORDR_ID     
     
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
 WHERE bb.BILL_ITEM_TYPE_ID = 2) b2 on b.ORDR_ID = b2.ORDR_ID    
    
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
 WHERE bb.BILL_ITEM_TYPE_ID = 3) b3 on b.ORDR_ID = b3.ORDR_ID    
     
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
 WHERE bb.BILL_ITEM_TYPE_ID = 4) b4 on b.ORDR_ID = b4.ORDR_ID    
    
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
 WHERE bb.BILL_ITEM_TYPE_ID = 5 AND bb.LINE_ITEM_DES LIKE '%Access%') b5 on b.ORDR_ID = b5.ORDR_ID    
    
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
 WHERE bb.BILL_ITEM_TYPE_ID = 5 AND bb.LINE_ITEM_DES LIKE '%PORT%') b6 on b.ORDR_ID = b6.ORDR_ID    
     
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
 WHERE bb.BILL_ITEM_TYPE_ID = 7) b7 on b.ORDR_ID = b7.ORDR_ID    
     
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
 WHERE bb.BILL_ITEM_TYPE_ID = 8) b8 on b.ORDR_ID = b8.ORDR_ID    
    
 LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
 FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
 JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
 WHERE bb.BILL_ITEM_TYPE_ID = 9) b9 on b.ORDR_ID = b9.ORDR_ID    
    
 WHERE     
  b.RGN_ID = 1 --AMNCI    
  AND (b.PLTFRM_CD = 'SF' )    
  --AND si.CIS_LVL_TYPE = 'H5'    
 GROUP BY      
   a.FTN     
  ,i.SALS_CHNL_DES    
  ,l.CTRY_NME 
  ,lcs.CTRY_NME  
  /*,b.SCURD_CD */   
  ,si.CUST_NME   
  ,si.CUST_NME2   
  ,d.PROD_TYPE_DES      
  ,gom.FRST_NME    
  ,gom.LST_NME    
  ,cm.FRST_NME    
  ,cm.LST_NME    
  ,a.CUST_WANT_DT      
  ,a.CUST_CMMT_DT     
 ,r.PLN_NME    
  ,f.ORDR_STUS_DES     
 ,n.FULL_NME    
 ,mg.FULL_NME    
 ,[GOM Receives Order]    
  ,a.ORDR_TYPE_CD    
  ,b1.MRC_CHG_AMT    
  ,b2.MRC_CHG_AMT     
  ,b3.MRC_CHG_AMT    
  ,b4.MRC_CHG_AMT    
  ,b5.MRC_CHG_AMT    
  ,b6.MRC_CHG_AMT    
  ,b7.MRC_CHG_AMT    
  ,b1.NRC_CHG_AMT    
  ,b2.NRC_CHG_AMT    
  ,b3.NRC_CHG_AMT    
  ,b4.NRC_CHG_AMT    
  ,b5.NRC_CHG_AMT    
  ,b6.NRC_CHG_AMT     
  ,b7.NRC_CHG_AMT    
  ,a.INSTL_SOLU_SRVC_DES    
  ,h.A_END_REGION    
 ORDER BY a.FTN     
           
 CLOSE SYMMETRIC KEY FS@K3y     
          
 RETURN 0;    
     
END TRY    
    
BEGIN CATCH    
 DECLARE @Desc VARCHAR(200)    
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptSprintFacilityPending '  + ',' + CAST(@secured AS VARCHAR(4)) + ': '    
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)    
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;    
 RETURN 1;    
END CATCH    
    