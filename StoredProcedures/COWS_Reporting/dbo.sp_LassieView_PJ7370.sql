USE [COWS_Reporting]
GO
/*
---- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- Notes:
-- 9/4/2012 : Got latest version from Prod and Creating new SP for PJ007370
-- =============================================
-- axm3320: 10/29/2012 added H5- Address Fields,H5 PH_NBR
-- axm3320: 10/30/2012 fix added for dupliacte records for VNDR_CNFRM_DSCNCT_DT
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
*/
ALTER PROCEDURE [dbo].[sp_LassieView_PJ7370] 
	 --Add the parameters for the stored procedure here
AS
BEGIN
	 --SET NOCOUNT ON added to prevent extra result sets from
	 --interfering with SELECT statements.
	SET NOCOUNT ON;
	
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
     --Insert statements for procedure here
--(SELECT * FROM         
With Lassie_View_Data as (Select distinct
 
  CASE 
		WHEN ordr.ORDR_CAT_ID = 1 THEN z.ORDR_ID
		WHEN ordr.ORDR_CAT_ID = 2 THEN fs.FTN END [Order_ID],
   ordr.ORDR_ID,		
   h1.CUST_ID [H1_ID], 
   h4.CUST_ID [H4_ID], 
   fs.PRNT_FTN [Parent_FTN],
    
   CASE 
		WHEN ordr.ORDR_CAT_ID = 1 THEN z.H5_CUST_ACCT_NBR
		WHEN ordr.ORDR_CAT_ID = 2 THEN h5.CUST_ID END [H5_ID],
		
   h6.CUST_ID [H6_ID],
   x.FULL_NME [COWS_IPM],	 	 	 
   x.USER_ADID [COWS_IPM_AD_ID],	
   tac.FRST_NME + tac.LST_NME [FSA_IPM],	
   tac.EMAIL_ADR [FSA_IPM_Email_Address],	
   CASE 
		WHEN tk.CREAT_DT > nt.CREAT_DT THEN tk.CREAT_DT
    ELSE nt.CREAT_DT END [Last_Modified_Date_time],
    
    nm.NUA_ADR [NUA],
    nm.PLN_NME [PL#],
    
	CASE 
		WHEN ordr.ORDR_CAT_ID = 1 AND 	cd.NEW_CCD_DT IS NULL THEN z.CUST_CMMT_DT
		WHEN ordr.ORDR_CAT_ID = 2 AND 	cd.NEW_CCD_DT IS NULL THEN fs.CUST_CMMT_DT 
		WHEN cd.NEW_CCD_DT IS NOT NULL THEN cd.NEW_CCD_DT END [CCD],
		
      CASE
		  WHEN tk.TASK_ID = 1001 THEN tk.CREAT_DT ELSE NULL END [Bill_Clear_Date],
		  
 	    --CASE
		--WHEN ordr.SCURD_CD = 0 AND ordr.ORDR_CAT_ID = 1 THEN ISNULL( CONVERT(varchar, DecryptByKey(aa.CUST_NME)), '')
		--WHEN ordr.SCURD_CD = 0 AND ordr.ORDR_CAT_ID = 2 THEN si.CUST_NME	
		--WHEN ordr.SCURD_CD = 1 AND ordr.ORDR_CAT_ID = 2 AND si.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER' 
		--WHEN ordr.SCURD_CD = 1 AND ordr.ORDR_CAT_ID = 2 AND si.CUST_NME IS NULL THEN NULL END  [Customer_Name],
	CASE ordr.ORDR_CAT_ID 
		WHEN 1 THEN CASE 
				WHEN ordr.CSG_LVL_ID > 0 AND  CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN ordr.CSG_LVL_ID > 0 AND  CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN '' 
				ELSE COALESCE ( aa.CUST_NME,'') END
		WHEN 2 THEN  CASE
		        WHEN ordr.CSG_LVL_ID > 0 AND  si.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN ordr.CSG_LVL_ID > 0 AND  si.CUST_NME2 IS NULL THEN '' 
				ELSE COALESCE (si.CUST_NME,'') END
		END  [Customer_Name],
		
	CASE 
		WHEN ordr.ORDR_CAT_ID = 1 AND h.ESCALAT_ORDR_CD = 0 THEN 'N'
		WHEN ordr.ORDR_CAT_ID = 1 AND h.ESCALAT_ORDR_CD = 1 THEN 'Y'
		WHEN ordr.ORDR_CAT_ID = 2 THEN fs.INSTL_ESCL_CD END [Escalation_Flag],
		
	u.EXP_TYPE_DES [Expedite],
	
	CASE 
		WHEN ordr.ORDR_CAT_ID = 1 THEN m.ORDR_TYPE_DES
		WHEN ordr.ORDR_CAT_ID = 2 THEN s.FSA_ORDR_TYPE_DES END [Order_Type],
		
    fs.ORDR_SUB_TYPE_CD [Order_Sub_Type],
    
 	CASE 
		WHEN ordr.ORDR_CAT_ID = 1 THEN t.PROD_TYPE_DES
		WHEN ordr.ORDR_CAT_ID = 2 THEN  i.PROD_TYPE_DES END [Product_Type],
		
 	CASE 
		WHEN ordr.ORDR_CAT_ID = 1 THEN  z.SOI
		WHEN ordr.ORDR_CAT_ID = 2 THEN  si.SOI_CD END [SOI],
       
    j.PLTFRM_NME [Platform_Name],
    --o.STUS_DES [Order_Status],
    o.ORDR_STUS_DES [Order_Status],
    q.JPRDY_CD [Jeopardy_Code] ,
    r.JPRDY_DES [Jeopardy_Description],
    SUBSTRING(nt.NTE_TXT,1, 500)  [COWS_Notes]
    
    --Begin PJ7370 Fields
    ,CASE WHEN ordr.ORDR_STUS_ID  = 1 then -- Pending
           CASE when fs.PROD_TYPE_CD = 'CP' and ordr.DMSTC_CD = 1 Then
             Case when tk.TASK_ID = 102 then 'GOM Milestones Pending'
             else 
               Case when systemStus.DetlStatus IS NULL or systemStus.DetlStatus = '' then nonSysStus.NonSysDetlStatus 
                else  systemStus.DetlStatus
               END  
                --DetlStus.DetlStatus   
             END
           Else 
           --DetlStus.DetlStatus   
            Case when systemStus.DetlStatus IS NULL or systemStus.DetlStatus = '' then nonSysStus.NonSysDetlStatus 
               else  systemStus.DetlStatus
             END  
           END
     Else o.ORDR_STUS_DES -- Completed      
     END [System_Order_Status],  
     systemStus.StatusDt [StatusDt],
     ordr.DMSTC_CD [DMSTC_CD],
     ordr.CHARS_ID [CHARS_ID],
     ordractn.ORDR_ACTN_DES  [ORDR_ACTN_DES],
     fs.TTRPT_SPD_OF_SRVC_BDWD_DES,
     fs.TTRPT_ACCS_ARNGT_CD,
     fs.TTRPT_PRLEL_PRCS_CKT_ID,
     fs.TTRPT_MNGD_DATA_SRVC_CD,
     fs.TPORT_CUST_IP_ADR,
     CASE When ordr.ORDR_CAT_ID = 1 then z.CUST_ORDR_SBMT_DT 
     WHEN ordr.ORDR_CAT_ID = 2 Then fs.CUST_ORDR_SBMT_DT END [ CUST_ORDR_SBMT_DT],
     CASE When ordr.ORDR_CAT_ID = 1 then z.SCA_NBR  
     WHEN ordr.ORDR_CAT_ID = 2 Then fs.SCA_NBR END [SCA_NBR],
     adh6.H6_CTRY_CD,
     adh6.H6_CTY_NME,
     adh6.H6_PRVN_NME ,
     adh6.H6_STREET_ADR_1,
     adh6.H6_STREET_ADR_2,
     adh6.H6_STT_CD,
     adh5.H5_CTRY_CD,
     adh5.H5_CTY_NME,
     adh5.H5_PRVN_NME ,
     adh5.H5_STREET_ADR_1,
     adh5.H5_STREET_ADR_2,
     adh5.H5_STT_CD,
     CntctH6.H6_EMAIL_ADR,
     CntctH6.PHN_NBR [H6_PHN_NBR],
     CntctH5.H5_PH_NBR, 
     h6.H6_CUST_NME,
     CASE 
		WHEN ordr.ORDR_CAT_ID = 1 THEN  z.SOI
		WHEN ordr.ORDR_CAT_ID = 2 THEN  h6.SOI_CD END [H6_SOI],
		fsagom.CUST_DLVRY_DT,
		fsagom.HDWR_DLVRD_DT,
		fsagom.VNDR_SHIP_DT,
		
		CASE When ccdhist.new_ccd_dt IS NULL THEN
				  CASE WHEN ordr.ORDR_CAT_ID = 1 THEN z.CUST_CMMT_DT
					   WHEN ordr.ORDR_CAT_ID = 2 THEN fs.CUST_CMMT_DT END 
             When ccdhist.new_ccd_dt IS NOT NULL THEN ccdhist.new_ccd_dt END [new_ccd_dt],
             
             cm.ACCS_ACPTC_DT,
             cm.ACCS_DLVRY_DT,
             cm.TRGT_DLVRY_DT,
             cmd.VNDR_CNFRM_DSCNCT_DT, 
             VNDR_SENT_DT_I.SENT_TO_VNDR_DT_I,             
             VNDR_SENT_DT_D.SENT_TO_VNDR_DT_D,
             oms.ORDR_DSCNCT_DT,
             nrmsrvc.ORDR_STUS_DES [NRM_ORDR_STUS_DES],
             ISNULL(fob.MRC_CHG_AMT,0) AS [MRC_CHG_AMT],                
             ISNULL(fob.NRC_CHG_AMT,0) AS [NRC_CHG_AMT]
             
			--ISNULL(fob.TOTAL_MRC_NRC_AMT,0) AS [TOTAL_MRC_NRC_AMT]                
           
     
    --End PJ7370 Fields
      
from 
  	  [COWS].[dbo].[ORDR] ordr LEFT OUTER JOIN
     (SELECT bb.FTN, bb.ORDR_ID, bb.ORDR_ACTN_ID, bb.PROD_TYPE_CD, bb.ORDR_TYPE_CD, 
		bb.CUST_CMMT_DT, bb.INSTL_ESCL_CD, bb.ORDR_SUB_TYPE_CD, bb.PRNT_FTN, cc.DMSTC_CD, bb.CPE_CPE_ORDR_TYPE_CD
		,cpl.TTRPT_SPD_OF_SRVC_BDWD_DES,cpl.TTRPT_ACCS_ARNGT_CD,bb.TTRPT_PRLEL_PRCS_CKT_ID,bb.TTRPT_MNGD_DATA_SRVC_CD,bb.TPORT_CUST_IP_ADR,bb.CUST_ORDR_SBMT_DT,bb.SCA_NBR       
		FROM [COWS].[dbo].[FSA_ORDR] bb with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] cpl with (nolock) on cpl.ORDR_ID = bb.ORDR_ID
		WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
		FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR]  with (nolock) ) fsa 
		WHERE fsa.FTN = bb.FTN AND bb.ORDR_ACTN_ID = 2  AND (cc.DMSTC_CD = 1) ) 
UNION				
SELECT bb.FTN, bb.ORDR_ID, bb.ORDR_ACTN_ID, bb.PROD_TYPE_CD, bb.ORDR_TYPE_CD, 
		bb.CUST_CMMT_DT, bb.INSTL_ESCL_CD, bb.ORDR_SUB_TYPE_CD, bb.PRNT_FTN, cc.DMSTC_CD, bb.CPE_CPE_ORDR_TYPE_CD 
		,cpl.TTRPT_SPD_OF_SRVC_BDWD_DES,cpl.TTRPT_ACCS_ARNGT_CD,bb.TTRPT_PRLEL_PRCS_CKT_ID,bb.TTRPT_MNGD_DATA_SRVC_CD,bb.TPORT_CUST_IP_ADR ,bb.CUST_ORDR_SBMT_DT ,bb.SCA_NBR 
		FROM [COWS].[dbo].[FSA_ORDR] bb  with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] cpl with (nolock) on cpl.ORDR_ID = bb.ORDR_ID
		WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
		FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock)) fs1 
		WHERE fs1.FTN = bb.FTN AND bb.ORDR_ACTN_ID = 2  AND  (cc.DMSTC_CD = 0
			AND (bb.PROD_TYPE_CD = 'MN' 
			OR bb.PROD_TYPE_CD = 'SE' OR bb.PROD_TYPE_CD ='CP' or ((bb.PROD_TYPE_CD = 'IP' 
			AND (bb.CPE_CPE_ORDR_TYPE_CD = 'MNS' OR ISNULL(bb.TTRPT_MNGD_DATA_SRVC_CD,'') = 'Y')) 
			--OR (bb.PROD_TYPE_CD = 'IP' AND bb.ORDR_TYPE_CD = 'DC') 
			)))) 
) fs on ordr.ORDR_ID = fs.ORDR_ID LEFT OUTER JOIN
		 	  
  	 (SELECT bb.ORDR_ID, bb.ROLE_ID, bb.FRST_NME [FRST_NME], bb.LST_NME [LST_NME], bb.EMAIL_ADR [EMAIL_ADR], bb.CNTCT_TYPE_ID 
		FROM [COWS].[dbo].[ORDR_CNTCT] bb with (nolock) LEFT OUTER JOIN 
		[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.ROLE_ID = 13) tac on ordr.ORDR_ID  = tac.ORDR_ID LEFT OUTER JOIN
		
      [COWS].[dbo].[LK_CNTCT_TYPE] e with (nolock) on  tac.CNTCT_TYPE_ID = e.CNTCT_TYPE_ID LEFT OUTER JOIN
      --[COWS].[dbo].[NRM_CKT] f with (nolock) on fs.FTN = f.FTN LEFT OUTER JOIN
      [COWS].[dbo].[TRPT_ORDR] g with (nolock) on ordr.ORDR_ID = g.ORDR_ID LEFT OUTER JOIN
      [COWS].[dbo].[ORDR_EXP] h with (nolock) on ordr.ORDR_ID = h.ORDR_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_EXP_TYPE] l with (nolock) on h.EXP_TYPE_ID = l.EXP_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_PROD_TYPE] i with (nolock) on fs.PROD_TYPE_CD = i.FSA_PROD_TYPE_CD LEFT OUTER JOIN
   	  [COWS].[dbo].[lk_FSA_ORDR_TYPE] s with (nolock) on fs.ORDR_TYPE_CD = s.FSA_ORDR_TYPE_CD LEFT OUTER JOIN
      [COWS].[dbo].[PLTFRM_MAPNG] k with (nolock) on ordr.[PLTFRM_CD] = k.PLTFRM_CD LEFT OUTER JOIN
      [COWS].[dbo].[LK_PLTFRM] j with (nolock) on k.PLTFRM_CD = j.PLTFRM_CD LEFT OUTER JOIN
      
 	  (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID 
 	   FROM [COWS].[dbo].[ACT_TASK] bb LEFT OUTER JOIN
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)
			FROM (SELECT ACT_TASK_ID, ORDR_ID FROM [COWS].[dbo].[ACT_TASK] ) tsk 
			WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk on ordr.ORDR_ID = tk.ORDR_ID LEFT OUTER JOIN
			
      [COWS].[dbo].[LK_TASK] n with (nolock) on tk.TASK_ID = n.TASK_ID LEFT OUTER JOIN
      --[COWS].[dbo].[LK_STUS]o with (nolock) on n.ORDR_STUS_ID = o.STUS_ID LEFT OUTER JOIN
       [COWS].[dbo].[LK_ORDR_STUS]o with (nolock) on ordr.ORDR_STUS_ID = o.ORDR_STUS_ID LEFT OUTER JOIN
     [COWS].[dbo].[ORDR_NTE] p with (nolock) on ordr.ORDR_ID = p.ORDR_ID LEFT OUTER JOIN
      [COWS].[dbo].[ORDR_JPRDY] q with (nolock) on ordr.ORDR_ID = q.ORDR_ID LEFT OUTER JOIN 
      [COWS].[dbo].[LK_JPRDY] r with (nolock) on q.JPRDY_CD = r.JPRDY_CD LEFT OUTER JOIN
      
  	  (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID 
  		FROM [COWS].[dbo].[USER_WFM_ASMT] bb LEFT OUTER JOIN 
	    [COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
	    WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT)
		FROM (SELECT DISTINCT ASMT_DT, ORDR_ID FROM [COWS].[dbo].[USER_WFM_ASMT] ) usr 	
		WHERE usr.ORDR_ID  = bb.ORDR_ID )
			AND bb.ASN_USER_ID  = (SELECT MAX(ASN_USER_ID) 
			FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID FROM [COWS].[dbo].[USER_WFM_ASMT] )uas
			WHERE uas.ORDR_ID = bb.ORDR_ID)) ur 
			on  ordr.ORDR_ID = ur.ORDR_ID LEFT OUTER JOIN
			
      [COWS].[dbo].[LK_USER] x with (nolock) on ur.ASN_USER_ID =  x.USER_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_ROLE]y with (nolock) on tac.ROLE_ID = y.ROLE_ID   LEFT OUTER JOIN
      [COWS].[dbo].[IPL_ORDR] z with (nolock) on ordr.ORDR_ID = z.ORDR_ID LEFT OUTER JOIN
      
      (SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE = 'H1') h1
		on ordr.ORDR_ID = h1.ORDR_ID LEFT OUTER JOIN
		
	  (SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE = 'H4') h4
		on  ordr.ORDR_ID = h4.ORDR_ID LEFT OUTER JOIN
		
	  (SELECT bb.CUST_ID, bb.ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] bb JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE = 'H5') h5
		on  ordr.ORDR_ID = h5.ORDR_ID LEFT OUTER JOIN	
			
		(SELECT bb.CUST_ID, bb.ORDR_ID, bb.SOI_CD,bb.CUST_NME [H6_CUST_NME] FROM [COWS].[dbo].[FSA_ORDR_CUST] bb JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE = 'H6') h6
		on  ordr.ORDR_ID = h6.ORDR_ID LEFT OUTER JOIN
			
	-- axm3320 10/13/2011 view throwing error for CUST_NME field as field has been removed from ORDR table
	-- adding CUST_NME in select below		 	
 	  (SELECT bb.ORDR_ID, bb.SOI_CD, CONVERT(varchar, DecryptByKey(csd.CUST_NME)) [CUST_NME2],bb.CUST_NME [CUST_NME]
 	   FROM [COWS].[dbo].[FSA_ORDR_CUST] bb LEFT OUTER JOIN
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
		WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)
			FROM (SELECT SOI_CD, ORDR_ID FROM [COWS].[dbo].[FSA_ORDR_CUST] ) soi
			WHERE soi.ORDR_ID = bb.ORDR_ID)) si on  ordr.ORDR_ID = si.ORDR_ID LEFT OUTER JOIN
										
	  (SELECT bb.NTE_ID, bb.ORDR_ID, bb.NTE_TXT, bb.CREAT_DT FROM [COWS].[dbo].[ORDR_NTE] bb LEFT OUTER JOIN 
		[COWS].[dbo].[FSA_ORDR] cc        on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.NTE_ID = (SELECT MAX(NTE_ID)
			FROM (SELECT NTE_ID, ORDR_ID FROM [COWS].[dbo].[ORDR_NTE] ) nte WHERE
		nte.ORDR_ID = bb.ORDR_ID)) nt on  ordr.ORDR_ID = nt.ORDR_ID LEFT OUTER JOIN
		
	  (SELECT bb.NRM_CKT_ID, bb.FTN, bb.NUA_ADR, bb.PLN_NME FROM [COWS].[dbo].[NRM_CKT] bb LEFT OUTER JOIN 
		[COWS].[dbo].[FSA_ORDR] cc        on cc.FTN = bb.FTN 
		WHERE bb.NRM_CKT_ID = (SELECT MAX(NRM_CKT_ID)
			FROM (SELECT NRM_CKT_ID, FTN FROM [COWS].[dbo].[NRM_CKT] ) nrm WHERE
		nrm.FTN = bb.FTN)) nm on  fs.FTN = nm.FTN LEFT OUTER JOIN

      [COWS].[dbo].[LK_ORDR_TYPE] m with (nolock) on z.ORDR_TYPE_ID = m.ORDR_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_PROD_TYPE] t with (nolock) on z.PROD_TYPE_ID = t.PROD_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[LK_EXP_TYPE] u with (nolock) on l.EXP_TYPE_ID = u.EXP_TYPE_ID LEFT OUTER JOIN
      [COWS].[dbo].[H5_FOLDR] w with (nolock) on z.H5_CUST_ACCT_NBR = w.CUST_ID LEFT OUTER JOIN
      [COWS].[dbo].[H5_FOLDR] aa with (nolock) on ordr.H5_FOLDR_ID  = aa.H5_FOLDR_ID 
      LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=aa.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6
      LEFT OUTER JOIN
      
            (SELECT bb.ORDR_ID, bb.NEW_CCD_DT, bb.CCD_HIST_ID
 	   FROM [COWS].[dbo].[CCD_HIST] bb LEFT OUTER JOIN
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CCD_HIST_ID = (SELECT MAX(CCD_HIST_ID)
			FROM (SELECT CCD_HIST_ID, ORDR_ID FROM [COWS].[dbo].[CCD_HIST] ) ccd
			WHERE ccd.ORDR_ID = bb.ORDR_ID)) cd on  ordr.ORDR_ID = cd.ORDR_ID 

    --PJ007370 Changes Begin
    
  --  left outer join (select lt.TASK_NME + '-' + s.STUS_DES as 'DetlStatus' , o.ORDR_ID  from COWS.Dbo.ACT_TASK acttsk with(nolock)
		--left outer join COWS.dbo.MAP_GRP_TASK mgt with(nolock) on mgt.TASK_ID = acttsk.TASK_ID 
		--left outer join COWS.dbo.ORDR o with(nolock) on o.ORDR_ID = acttsk.ORDR_ID 
		--join COWS.dbo.LK_TASK lt with(nolock) on lt.TASK_ID = acttsk.TASK_ID 
		--join COWS.dbo.LK_STUS s with(nolock) on s.STUS_ID = acttsk.STUS_ID 
		--where GRP_ID in (4,11)
		--and mgt.MAP_GRP_TASK_ID = (select max(MAP_GRP_TASK_ID) from ( select MAP_GRP_TASK_ID,TASK_ID from COWS.dbo.MAP_GRP_TASK ) mgt1 where mgt1.TASK_ID = mgt.TASK_ID )
  --) DetlStus on DetlStus.ORDR_ID = ordr.ORDR_ID 
  
    left outer join  (select CASE WHEN lt.TASK_ID  = 1001 THEN 'Completed' ELSE lt.TASK_NME + '-' + s.STUS_DES END as 'DetlStatus' , o.ORDR_ID,acttsk.CREAT_DT as 'StatusDt'  from COWS.Dbo.ACT_TASK acttsk with(nolock)
		left outer join COWS.dbo.MAP_GRP_TASK mgt with(nolock) on mgt.TASK_ID = acttsk.TASK_ID 
		left outer join COWS.dbo.ORDR o with(nolock) on o.ORDR_ID = acttsk.ORDR_ID 
		join COWS.dbo.LK_TASK lt with(nolock) on lt.TASK_ID = acttsk.TASK_ID 
		join COWS.dbo.LK_STUS s with(nolock) on s.STUS_ID = acttsk.STUS_ID 
		join COWS.dbo.LK_GRP grp with(nolock) on grp.GRP_ID = mgt.GRP_ID 
		where mgt.GRP_ID in (4,11) and
		acttsk.STUS_ID not in (1,2)
       and acttsk.ACT_TASK_ID  = (select min(ACT_TASK_ID) from ( select ACT_TASK_ID,ordr_id,STUS_ID,Task_ID from COWS.Dbo.ACT_TASK ) act1 where act1.ORDR_ID = acttsk.ORDR_ID  and act1.STUS_ID not in (1,2) and act1.TASK_ID = mgt.TASK_ID  ) 
		and mgt.MAP_GRP_TASK_ID = (select min(MAP_GRP_TASK_ID) from ( select MAP_GRP_TASK_ID,TASK_ID from COWS.dbo.MAP_GRP_TASK ) mgt1 where mgt1.TASK_ID = mgt.TASK_ID ) ) systemStus on systemStus.ORDR_ID =  ordr.ORDR_ID 
		
		left outer join (
		select CASE WHEN lt.TASK_ID  = 1001 THEN 'Completed' ELSE lt.TASK_NME + '-' + s.STUS_DES END as 'NonSysDetlStatus' , o.ORDR_ID    from COWS.Dbo.ACT_TASK acttsk with(nolock)
		left outer join COWS.dbo.MAP_GRP_TASK mgt with(nolock) on mgt.TASK_ID = acttsk.TASK_ID 
		left outer join COWS.dbo.ORDR o with(nolock) on o.ORDR_ID = acttsk.ORDR_ID 
		join COWS.dbo.LK_TASK lt with(nolock) on lt.TASK_ID = acttsk.TASK_ID 
		join COWS.dbo.LK_STUS s with(nolock) on s.STUS_ID = acttsk.STUS_ID 
		join COWS.dbo.LK_GRP grp with(nolock) on grp.GRP_ID = mgt.GRP_ID 
		where 
		mgt.GRP_ID not in (4,11) and
		acttsk.STUS_ID not in (1,2)
        and acttsk.ACT_TASK_ID  = (select min(ACT_TASK_ID) from ( select ACT_TASK_ID,ordr_id,STUS_ID from COWS.Dbo.ACT_TASK ) act1 where act1.ORDR_ID  = acttsk.ORDR_ID and act1.STUS_ID not in (1,2) ) 
		and mgt.MAP_GRP_TASK_ID = (select min(MAP_GRP_TASK_ID) from ( select MAP_GRP_TASK_ID,TASK_ID from COWS.dbo.MAP_GRP_TASK ) mgt1 where mgt1.TASK_ID = mgt.TASK_ID ) ) nonSysStus on nonSysStus.ORDR_ID = ordr.ORDR_ID 
    
    left outer join COWS.dbo.LK_ORDR_ACTN ordractn with(nolock) on ordractn.ORDR_ACTN_ID = fs.ORDR_ACTN_ID 
     
    left outer join (SELECT adr1.ORDR_ID, adr1.STT_CD [H6_STT_CD]
                                        , adr1.STREET_ADR_1 [H6_STREET_ADR_1] 
                                        , adr1.STREET_ADR_2 [H6_STREET_ADR_2]
                                        , adr1.CTY_NME [H6_CTY_NME]
                                        , adr1.CTRY_CD [H6_CTRY_CD]
                                        , adr1.PRVN_NME  [H6_PRVN_NME]                                                                             
     FROM [COWS].[dbo].[ORDR_ADR] adr1 with (nolock) where adr1.CIS_LVL_TYPE in ('H6') and adr1.REC_STUS_ID =1 )adh6 on adh6.ORDR_ID = ordr.ORDR_ID 
     
     
     left outer join (SELECT adr1.ORDR_ID, adr1.STT_CD [H5_STT_CD]
                                        , adr1.STREET_ADR_1 [H5_STREET_ADR_1] 
                                        , adr1.STREET_ADR_2 [H5_STREET_ADR_2]
                                        , adr1.CTY_NME [H5_CTY_NME]
                                        , adr1.CTRY_CD [H5_CTRY_CD]
                                        , adr1.PRVN_NME  [H5_PRVN_NME]                                                                             
     FROM [COWS].[dbo].[ORDR_ADR] adr1 with (nolock) where adr1.CIS_LVL_TYPE in ('H5') and adr1.REC_STUS_ID =1 )adh5 on adh5.ORDR_ID = ordr.ORDR_ID 
     
     left outer join (SELECT bb.ORDR_ID, bb.PHN_NBR, bb.EMAIL_ADR [H6_EMAIL_ADR]
		FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE in ('H6') and  CNTCT_TYPE_ID  = 1) CntctH6 on ordr.ORDR_ID  = CntctH6.ORDR_ID      
	
	 left outer join (SELECT bb.ORDR_ID, bb.PHN_NBR [H5_PH_NBR]
		FROM [COWS].[dbo].[ORDR_CNTCT] bb LEFT OUTER JOIN 
		[COWS].[dbo].[FSA_ORDR] cc on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.CIS_LVL_TYPE in ('H5') and  CNTCT_TYPE_ID  = 1) CntctH5 on ordr.ORDR_ID  = CntctH5.ORDR_ID  
	
	left outer join (select gom.ORDR_id,gom.grp_id,gom.VNDR_SHIP_DT,gom.CUST_DLVRY_DT,gom.HDWR_DLVRD_DT from [COWS].[dbo].FSA_ORDR_GOM_XNCI gom with (nolock) 
		left join [COWS].[dbo].fsa_ordr fsa with(nolock) on fsa.ordr_id = gom.ordr_id 
		left join [COWS].[dbo].ordr o with(nolock) on o.ORDR_ID = gom.ordr_ID
		where  o.DMSTC_CD = 1 and fsa.ORDR_TYPE_CD in( 'IN','CP') and FSA.ORDR_ACTN_ID <>3 and gom.GRP_ID = 1 ) fsagom on fsagom.ORDR_ID = ordr.ORDR_ID 

   Left outer join (select ch.ordr_id,new_ccd_dt,ccd_hist_id from [COWS].[dbo].ccd_hist ch with(nolock) join 
  	[COWS].[dbo].[ORDR] cc with (nolock) on ch.ORDR_ID = cc.ORDR_ID  
  	where ch.CCD_HIST_ID = (select MAX(ccd_hist_id) from (select ORDR_ID,CCD_HIST_ID from [COWS].[dbo].ccd_hist with(nolock) ) ch1 where ch1.ordr_id = ch.ORDR_ID ))
  	ccdhist on ccdhist.ORDR_ID = ordr.ORDR_ID
  
  left outer join (SELECT ck.CKT_ID,ck.ORDR_ID FROM [COWS].[dbo].[CKT] ck with (nolock) 
               join ( select fsa.ordr_id from COWS.dbo.ORDR ordr with(nolock) join COWS.dbo.FSA_ORDR fsa with(nolock) on fsa.ORDR_ID = ordr.ORDR_ID 
                 where ordr.DMSTC_CD = 1 and fsa.ORDR_TYPE_CD = 'IN' and fsa.ORDR_ACTN_ID <>3  ) o on o.ORDR_ID = ck.ORDR_ID 
					WHERE ck.CKT_ID   = (SELECT MAX(CKT_ID)
					FROM (SELECT CKT_ID,ORDR_ID FROM [COWS].[dbo].[CKT] with (nolock) ) ock 
					WHERE ock.ORDR_ID = ck.ORDR_ID) ) ckt on ckt.ORDR_ID = ordr.ORDR_ID
		
  left outer join (SELECT ckm.TRGT_DLVRY_DT, ckm. ACCS_DLVRY_DT, ckm.ACCS_ACPTC_DT,ckm.CKT_ID FROM [COWS].[dbo].[CKT_MS] ckm with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[CKT] cc on ckm.CKT_ID = cc.CKT_ID  
		WHERE ckm.VER_ID  = (SELECT MAX(VER_ID)
		FROM (SELECT VER_ID,CKT_ID FROM [COWS].[dbo].[CKT_MS] with (nolock) ) ock 
		WHERE ock.CKT_ID = ckm.CKT_ID)) cm on cm.CKT_ID  = ckt.CKT_ID 
		
  --left outer join (SELECT ck.CKT_ID,ck.ORDR_ID FROM [COWS].[dbo].[CKT] ck with (nolock) 
  --             join ( select fsa.ordr_id from COWS.dbo.ORDR ordr with(nolock) join COWS.dbo.FSA_ORDR fsa with(nolock) on fsa.ORDR_ID = ordr.ORDR_ID 
  --               where ordr.DMSTC_CD = 1 and fsa.ORDR_TYPE_CD = 'DC' and fsa.ORDR_ACTN_ID <>3  ) o on o.ORDR_ID = ck.ORDR_ID 
		--			WHERE ck.CKT_ID   = (SELECT MAX(CKT_ID)
		--			FROM (SELECT CKT_ID,ORDR_ID FROM [COWS].[dbo].[CKT] with (nolock) ) ock 
		--			WHERE ock.ORDR_ID = ck.ORDR_ID) ) cktd on ckt.ORDR_ID = ordr.ORDR_ID
		
  left outer join (SELECT ckm.VNDR_CNFRM_DSCNCT_DT,ckm.CKT_ID,o.ordr_id  FROM [COWS].[dbo].[CKT_MS] ckm with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[CKT] cc on ckm.CKT_ID = cc.CKT_ID  
		 join (select fsa.ordr_id from COWS.dbo.ORDR ordr with(nolock) join COWS.dbo.FSA_ORDR fsa with(nolock) on fsa.ORDR_ID = ordr.ORDR_ID 
                 where ordr.DMSTC_CD = 1 and fsa.ORDR_TYPE_CD = 'DC' and fsa.ORDR_ACTN_ID <>3  ) o on o.ORDR_ID = cc.ORDR_ID 
		WHERE ckm.VER_ID  = (SELECT MAX(VER_ID)
		FROM (SELECT VER_ID,CKT_ID FROM [COWS].[dbo].[CKT_MS] with (nolock) ) ock 
		WHERE ock.CKT_ID = ckm.CKT_ID)
		and ckm.CKT_ID = (SELECT MAX(CKT_ID) FROM (SELECT CKT_ID,ORDR_ID FROM [COWS].[dbo].[CKT] with (nolock) ) ock 
					WHERE ock.ORDR_ID = o.ORDR_ID)
		) cmd on cmd.ORDR_ID = ordr.ORDR_ID 
		
 left outer join (select vom.SENT_TO_VNDR_DT [SENT_TO_VNDR_DT_I],v.ORDR_ID from [COWS].[dbo].[VNDR_ORDR_MS] vom with (nolock) join 
                 COWS.dbo.VNDR_ORDR v on vom.VNDR_ORDR_ID = v.VNDR_ORDR_ID   
                 join ( select fsa.ordr_id from COWS.dbo.ORDR ordr with(nolock) join COWS.dbo.FSA_ORDR fsa with(nolock) on fsa.ORDR_ID = ordr.ORDR_ID 
                 where ordr.DMSTC_CD = 1 and fsa.ORDR_TYPE_CD = 'IN' and fsa.ORDR_ACTN_ID <>3  ) o on o.ORDR_ID = v.ORDR_ID 
                 where vom.VNDR_ORDR_MS_ID  =(select MAX(VNDR_ORDR_MS_ID) from (SELECT VNDR_ORDR_ID,VNDR_ORDR_MS_ID FROM [COWS].[dbo].[VNDR_ORDR_MS] vo1 with (nolock)) vo2 
		WHERE vo2.VNDR_ORDR_ID  = vom.VNDR_ORDR_ID )) VNDR_SENT_DT_I on VNDR_SENT_DT_I.ORDR_ID = ordr.ORDR_ID
		
  left outer join (select vom.SENT_TO_VNDR_DT [SENT_TO_VNDR_DT_D],v.ORDR_ID from [COWS].[dbo].[VNDR_ORDR_MS] vom with (nolock) join 
                 COWS.dbo.VNDR_ORDR v on vom.VNDR_ORDR_ID = v.VNDR_ORDR_ID   
                 join ( select fsa.ordr_id from COWS.dbo.ORDR ordr with(nolock) join COWS.dbo.FSA_ORDR fsa with(nolock) on fsa.ORDR_ID = ordr.ORDR_ID 
                 where ordr.DMSTC_CD = 1 and fsa.ORDR_TYPE_CD = 'DC' and fsa.ORDR_ACTN_ID <>3  ) o on o.ORDR_ID = v.ORDR_ID 
           where vom.VNDR_ORDR_MS_ID  =(select MAX(VNDR_ORDR_MS_ID) from (SELECT VNDR_ORDR_ID,VNDR_ORDR_MS_ID FROM [COWS].[dbo].[VNDR_ORDR_MS] vo1 with (nolock)) vo2 
		WHERE vo2.VNDR_ORDR_ID  = vom.VNDR_ORDR_ID )) VNDR_SENT_DT_D on VNDR_SENT_DT_D.ORDR_ID = ordr.ORDR_ID
		
		
 left outer join (SELECT oo.ordr_dscnct_dt,oo.ORDR_ID  FROM [COWS].[dbo].[ORDR_MS] oo with (nolock) 
  join ( select fsa.ordr_id from COWS.dbo.ORDR ordr with(nolock) join COWS.dbo.FSA_ORDR fsa with(nolock) on fsa.ORDR_ID = ordr.ORDR_ID 
                 where ordr.DMSTC_CD = 1 and fsa.ORDR_TYPE_CD = 'DC' and fsa.ORDR_ACTN_ID <>3  ) fo on fo.ORDR_ID = oo.ORDR_ID 
		WHERE oo.VER_ID = (SELECT MAX(VER_ID)
		FROM (SELECT VER_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_MS] with (nolock) ) oms 
		WHERE oms.ORDR_ID = oo.ORDR_ID)) oms on oms.ORDR_ID = ordr.ORDR_ID		
  
  
  left outer join (select ORDR_STUS_DES,nrms.FTN from COWS.dbo.NRM_SRVC_INSTC nrms with(nolock)
 -- Check below condition if its Valid??? 
  join ( select fsa.FTN from COWS.dbo.ORDR ordr with(nolock) join COWS.dbo.FSA_ORDR fsa with(nolock) on fsa.ORDR_ID = ordr.ORDR_ID 
                 where ordr.DMSTC_CD = 1 and fsa.ORDR_TYPE_CD in ('DC','IN') and fsa.ORDR_ACTN_ID <>3  ) o on o.FTN  = nrms.FTN 
 
 where MODFD_DT  = (select MAX(MODFD_DT) from ( select MODFD_DT,FTN from COWS.dbo.NRM_SRVC_INSTC  with(nolock)) nrm1 where nrm1.FTN = nrms.FTN ))nrmsrvc on nrmsrvc.FTN = fs.FTN  
			
    --PJ007370 Changes End
 Left Join (SELECT SUM( [COWS_Reporting].dbo.ConvertInputToFloat(MRC_CHG_AMT)) AS MRC_CHG_AMT,SUM([COWS_Reporting].dbo.ConvertInputToFloat(NRC_CHG_AMT)) AS NRC_CHG_AMT, ORDR_ID
				FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] WITH (NOLOCK)
				GROUP BY ORDR_ID) fob ON fob.ORDR_ID = ordr.ORDR_ID   

 --Left Join (SELECT SUM( dbo.ConvertInputToFloat(NRC_CHG_AMT)  ) AS NRC_CHG_AMT,ORDR_ID
	--			FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] WITH (NOLOCK)
	--			GROUP BY ORDR_ID) fobN ON fobN.ORDR_ID = ordr.ORDR_ID   

			
	
) 
Select * from Lassie_View_Data lv WHERE lv.[Order_ID] IS NOT NULL 





CLOSE SYMMETRIC KEY FS@K3y 


END

