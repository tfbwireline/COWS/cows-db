USE [COWS_Reporting]
GO
/*
 --=============================================
 --Author:		dlp0278
 --Create date: 8/26/2013
 --Description:	Report to show all the non-event related appointments for Non-MDS and 
 --             Non-Fedline users.
 --
 --   EXEC COWS_Reporting.dbo.sp_ApptWklyRpt 0
 --
 -- Modifications
 -- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD                       
 --=============================================
 */
ALTER PROCEDURE [dbo].[sp_ApptWklyRpt]
	    @secured			BIT = 0

AS
BEGIN TRY
	
--DECLARE @secured		BIT = 0

SET NOCOUNT ON;

		
CREATE TABLE #temp_appt_types 
		(app_Type_id	int)

	INSERT INTO #temp_appt_types (app_Type_id)
		SELECT DISTINCT APPT_TYPE_ID
			FROM	COWS.dbo.LK_APPT_TYPE
				WHERE	(APPT_TYPE_ID	>	16	AND	APPT_TYPE_ID	<	28)	



	CREATE TABLE #tmp_appt_wkly_rpt  
		(Iden			Int	IDENTITY(1, 1),
		EVENT_ID		Int
		,StartTime		DateTime
		,EndTime		DateTime
		,SCURD_CD		BIT
		,Subject		Varchar(1000)
		,Description	Varchar(1000)
		,Location		Varchar(500)
		,AppointmentType	Varchar(50)
		,CreatedByUID		Int
		,GroupName			Varchar(200)
		,CreatedBy			Varchar(50)
		,ModifiedBy			varchar(50)
		,ModifiedDate		SmallDateTime
		,CreateDate			SmallDateTime
		,AssignedUsers		Varchar(Max)
		,Flag				Bit
		)
		
		--SET IDENTITY_INSERT #tmp_appt_wkly_rpt ON


	INSERT INTO #tmp_appt_wkly_rpt	(EVENT_ID, StartTime, EndTime, SCURD_CD, Subject, Description, Location, AppointmentType
									,CreatedByUID, GroupName, CreatedBy, ModifiedBy, ModifiedDate, CreateDate, AssignedUsers, Flag)		
					SELECT			a.EVENT_ID
									,STRT_TMST					AS	StartTime
									,END_TMST					AS	EndTime
									,CASE ev.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END				AS  SCURD_CD
									,SUBJ_TXT					AS	Subject
									,[DES]						AS	Description
									,APPT_LOC_TXT				AS	Location
									,lt.APPT_TYPE_DES			AS	AppointmentType
									,a.CREAT_BY_USER_ID			AS	CreatedByUID
									,''							AS	GroupName
									,lc.FULL_NME				AS	CreatedBy
									,ISNULL(lm.FULL_NME, '')	AS	ModifiedBy
									,a.MODFD_DT					AS	ModifiedDate
									,a.CREAT_DT					AS	CreateDate
									,ASN_TO_USER_ID_LIST_TXT	AS	AssignedUsers	
									,0							AS	Flag
						FROM		COWS.dbo.APPT			a	
						INNER JOIN	COWS.dbo.LK_APPT_TYPE	lt		ON	a.APPT_TYPE_ID		=	lt.APPT_TYPE_ID	
						INNER JOIN	#temp_appt_types			vt					ON	a.APPT_TYPE_ID		=	vt.app_Type_id
						INNER JOIN	COWS.dbo.LK_USER			lc		ON	a.CREAT_BY_USER_ID	=	lc.USER_ID
						LEFT JOIN	COWS.dbo.LK_USER			lm		ON	a.MODFD_BY_USER_ID	=	lm.USER_ID
						LEFT JOIN	COWS.dbo.EVENT			ev		ON	ev.EVENT_ID			=	a.EVENT_ID
						WHERE		((STRT_TMST	> DATEADD(day, -1, GETDATE())) AND (END_TMST < DATEADD(day,360,GETDATE())))
							AND ((a.REC_STUS_ID IS NULL) OR a.REC_STUS_ID IN (0,1))
							AND (a.RCURNC_CD = 0) AND (ev.EVENT_ID IS NULL)
	
						

		INSERT INTO #tmp_appt_wkly_rpt	(EVENT_ID		,StartTime	,EndTime	,SCURD_CD	,Subject ,Description	,Location	,AppointmentType
										,CreatedByUID	,GroupName	,CreatedBy	,ModifiedBy	,ModifiedDate	,CreateDate	,AssignedUsers,	Flag	)		
					SELECT			a.EVENT_ID
									,ar.STRT_TMST					AS	StartTime
									,ar.END_TMST					AS	EndTime
									,CASE ev.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END				AS  SCURD_CD
									,SUBJ_TXT					AS	Subject
									,[DES]						AS	Description
									,APPT_LOC_TXT				AS	Location
									,lt.APPT_TYPE_DES			AS	AppointmentType
									,a.CREAT_BY_USER_ID			AS	CreatedByUID
									,''							AS	GroupName
									,lc.FULL_NME				AS	CreatedBy
									,ISNULL(lm.FULL_NME, '')	AS	ModifiedBy
									,a.MODFD_DT					AS	ModifiedDate
									,a.CREAT_DT					AS	CreateDate
									,ASN_TO_USER_ID_LIST_TXT	AS	AssignedUsers	
									,0							AS	Flag
						FROM		[COWS].[dbo].[APPT]			a	WITH (NOLOCK)
						INNER JOIN  [COWS].[dbo].[APPT_RCURNC_DATA] ar WITH (NOLOCK)	ON	a.APPT_ID = ar.APPT_ID
						INNER JOIN	[COWS].[dbo].[LK_APPT_TYPE]	lt	WITH (NOLOCK)	ON	a.APPT_TYPE_ID		=	lt.APPT_TYPE_ID	
						INNER JOIN	#temp_appt_types			vt					ON	a.APPT_TYPE_ID		=	vt.app_Type_id
						INNER JOIN	[COWS].[dbo].[LK_USER]			lc	WITH (NOLOCK)	ON	a.CREAT_BY_USER_ID	=	lc.USER_ID
						LEFT JOIN	[COWS].[dbo].[LK_USER]			lm	WITH (NOLOCK)	ON	a.MODFD_BY_USER_ID	=	lm.USER_ID
						LEFT JOIN	[COWS].[dbo].[EVENT]			ev	WITH (NOLOCK)	ON	ev.EVENT_ID			=	a.EVENT_ID
						WHERE	((a.STRT_TMST	> DATEADD(day, -1, GETDATE())) AND (a.END_TMST < DATEADD(day,360,GETDATE())))
							AND ((a.REC_STUS_ID IS NULL) OR a.REC_STUS_ID IN (0,1))
							AND (a.RCURNC_CD = 1) AND (ev.EVENT_ID IS NULL)

		CREATE TABLE #tmp_a_wrk_tbl 
			(A	Varchar(50))
		CREATE TABLE #tmp_group_wrk_tbl 
			(GroupID	Varchar(50))									
		DECLARE @Cnt	Int
		DECLARE @TCnt	Int
	
		SET	@Cnt	=	0
		SET	@TCnt	=	0
		
		SELECT @Cnt = COUNT(1) FROM #tmp_appt_wkly_rpt
	
		WHILE @TCnt < @Cnt	
			BEGIN
				DELETE FROM #tmp_a_wrk_tbl
				DELETE FROM #tmp_group_wrk_tbl
				DECLARE @StrName	Varchar(Max)
				DECLARE	@User		Varchar(Max)
				DECLARE	@GroupName	Varchar(Max)
				DECLARE	@UID		Int
				DECLARE	@ID			Int
				SET @StrName	=	''
				SET @User		=	''
				SET	@GroupName	=	''
				SET	@UID		=	0
				SET	@ID			=	0
			
				SELECT Top 1	@StrName	= [dbo].[parseAssignedUsersFromAppt](AssignedUsers)
							,@UID		=	CreatedByUID
							,@ID		=	Iden
					FROM	#tmp_appt_wkly_rpt	
					WHERE	Flag		=	0
				
			------------------------------------------------------------------------
			--	Get the list of Assigned Users for a given Appt.
			------------------------------------------------------------------------	
				INSERT INTO #tmp_a_wrk_tbl (A)
					SELECT		REPLACE(StringID, ' ', '') 
						FROM	[dbo].[ParseStringWithDelimiter](@StrName, '|') 
									
				SELECT @User = COALESCE(@User + ', ' + FULL_NME, FULL_NME) 
					FROM 
						(SELECT FULL_NME FROM	[COWS].[dbo].[LK_USER] WITH (NOLOCK)
							WHERE USER_ID IN (SELECT A FROM #tmp_a_wrk_tbl))T

				
			------------------------------------------------------------------------
			--	Get the list of Group Names	for the User who created the Appt.
			------------------------------------------------------------------------	
				INSERT INTO	#tmp_group_wrk_tbl	(GroupID)
					SELECT DISTINCT	GRP_ID	
						FROM		[COWS].[dbo].[USER_GRP_ROLE] WITH (NOLOCK)
						WHERE		[USER_ID]	=	@UID
						 AND		REC_STUS_ID	=	1
	
				SELECT @GroupName = COALESCE(@GroupName + ', ' + GRP_NME, GRP_NME) 
						FROM 
						(SELECT GRP_NME FROM	[COWS].[dbo].[LK_GRP] WITH (NOLOCK)
							WHERE GRP_ID IN (SELECT GroupID FROM #tmp_group_wrk_tbl)
						)T						

			------------------------------------------------------------------------
			--	Update the temp table with the GroupName and Assigned User string.
			------------------------------------------------------------------------	
				UPDATE		#tmp_appt_wkly_rpt
					SET		AssignedUsers	=	Substring(@User, 3, Len(@USER)) 
							,GroupName		=	Substring(@GroupName, 3, Len(@GroupName)) 
							,Flag			=	1
					WHERE	Iden			=	@ID
			
				SET @TCnt = @TCnt+ 1
		END 

		
		SELECT DISTINCT	
					vt.AssignedUsers [Assigned]
					,vt.AppointmentType [Appointment Type]
					,vt.Subject [Subject]
					--,vt.Description [Description]
					,vt.GroupName [Group Name]
					,vt.StartTime	[Start Time]
					,vt.EndTime		[End Time]
					,vt.CreatedBy [Created By]	
					,vt.ModifiedBy [Modified By]	
					,vt.ModifiedDate [Modified Date]
					,vt.CreateDate	[Create Date]	
					
		FROM		#tmp_appt_wkly_rpt	vt
		WHERE		(AssignedUsers	!=	'') AND (GroupName != 'MDS')
		Order by AssignedUsers


	IF OBJECT_ID('tempdb..#tmp_appt_wkly_rpt', 'U') IS NOT NULL	
		drop table #tmp_appt_wkly_rpt;
		
	IF OBJECT_ID('tempdb..#temp_appt_types', 'U') IS NOT NULL	
		drop table #temp_appt_types;
		
	IF OBJECT_ID('tempdb..#tmp_a_wrk_tbl', 'U') IS NOT NULL	
		drop table #tmp_a_wrk_tbl;
		
	IF OBJECT_ID('tempdb..#tmp_group_wrk_tbl', 'U') IS NOT NULL	
		drop table #tmp_group_wrk_tbl;
		
	
	RETURN 0;
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_ApptWklyRpt '  + ',' + CAST(@secured AS VARCHAR(4)) + ': '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @secured, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH






