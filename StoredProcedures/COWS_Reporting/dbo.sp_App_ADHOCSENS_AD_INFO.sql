USE [COWS_Reporting]
GO
/*
-- axm3320: 8/17/2012 Create new Stored proc to get sesitive information for AD EVENTs
--axm3320: 12/4/2012 added action publish
--axm3320:3/19/2013 Replace action_id 15(reschedule) with 44 (reschedule-email notfn sent)
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
*/

ALTER PROC [dbo].[sp_App_ADHOCSENS_AD_INFO]
 AS
 BEGIN TRY
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
    -- Insert statements for procedure here
    
    
Declare @SQLSELECT1 as varchar(max)
Declare @SQLSELECT2 as varchar(max)
Declare @SQLSELECT3 as varchar(max)
--Declare @SQLSELECT4 as varchar(max) 
    
 select @SQLSELECT1 = 'With AD_Event_Data as (select distinct a.FTN, 
	adev.AD_Event_ID  [AD_Event_ID], 
	UPPER(adev.AD_Event_Status) [AD_Event_Status], 	
	CASE WHEN ordr.CSG_LVL_ID = 0 THEN adev.AD_Item_Title  	
	WHEN ordr.CSG_LVL_ID > 0 AND adev.AD_Item_Title2 IS NOT NULL THEN ''PRIVATE CUSTOMER''
	WHEN ordr.CSG_LVL_ID > 0 AND adev.AD_Item_Title2 IS NULL THEN '''' END [AD_Item_Title],  						
	CASE WHEN ordr.CSG_LVL_ID = 0 THEN adev.AD_Cust_Name  	
    WHEN ordr.CSG_LVL_ID > 0 AND adev.AD_Cust_Name2 IS NOT NULL THEN ''PRIVATE CUSTOMER''
    WHEN ordr.CSG_LVL_ID > 0 AND adev.AD_Cust_Name2 IS NULL THEN '''' END [AD_Cust_Name],  						
	CASE WHEN adev.AD_DES_CMNT_TXT is NULL THEN '''' ELSE adev.AD_DES_CMNT_TXT END [AD_DES_CMNT_TXT],
	adev.AD_Type [AD_Type],	
	adev.AD_CKT_ID [AD_CKT_ID],
	adev.AD_IP_VER_ID  [AD_IP_VER_ID],
	adev.AD_STRT_TMST [AD_STRT_TMST],
	adev.AD_EVENT_DRTN_IN_MIN_QTY [AD_EVENT_DRTN_IN_MIN_QTY],
	adev.AD_EXTRA_DRTN_TME_AMT [AD_EXTRA_DRTN_TME_AMT],		
	adev.AD_END_TMST [AD_END_TMST],
	adev.AD_ACTN_DES [AD_ACTN_DES] ,
	adev.AD_MOD_DT 	[AD_MOD_DT],	
	adev.AD_MOD_BY [AD_MOD_BY],				
	CASE WHEN adev.AD_SUCSS_ACTY_DES is NULL THEN '''' ELSE adev.AD_SUCSS_ACTY_DES END [AD_SUCSS_ACTY_DES],		
	CASE WHEN adev.AD_FAIL_ACTY_DES is NULL THEN '''' ELSE adev.AD_FAIL_ACTY_DES END [AD_FAIL_ACTY_DES],	
	CASE WHEN adev.AD_CMNT_TXT is NULL THEN '''' ELSE adev.AD_CMNT_TXT END [AD_CMNT_TXT],
	case when adev.ESCL_CD = 1 THEN ''Y'' else ''N'' END  [AD_ESCL_CD]
	from COWS.dbo.ORDR ordr with (nolock) 
    left outer join (SELECT bb.FTN,bb.ORDR_ID  FROM [COWS].[dbo].[FSA_ORDR] bb with (nolock) LEFT OUTER JOIN
		[COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID 
		WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)
		FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa 
		WHERE fsa.FTN = bb.FTN)) a on a.ORDR_ID = ordr.ORDR_ID '
        
        select @SQLSELECT2 =    ' full outer join (select distinct adev.EVENT_ID [AD_Event_ID], adev.FTN,  
			UPPER(adevst.EVENT_STUS_DES) [AD_Event_Status], 						
			CASE WHEN CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) is NULL THEN '''' ELSE CONVERT(varchar, DecryptByKey(csd.EVENT_TITLE_TXT)) END [AD_Item_Title2],	
			CASE WHEN CONVERT(varchar, DecryptByKey(csd.CUST_NME)) is NULL THEN '''' ELSE CONVERT(varchar, DecryptByKey(csd.CUST_NME)) END [AD_Cust_Name2],
			adev.EVENT_TITLE_TXT as [AD_Item_Title],	
			adev.CUST_NME as [AD_Cust_Name],
			CASE WHEN adev.DES_CMNT_TXT is NULL THEN '''' ELSE adev.DES_CMNT_TXT END [AD_DES_CMNT_TXT],
			adlkes.ENHNC_SRVC_NME [AD_Type],	
			fmsc.FMS_CKT_LIST [AD_CKT_ID],
			CASE
				WHEN CAST(adev.IP_VER_ID AS VARCHAR(3)) is NULL THEN ''''
				ELSE CAST(adev.IP_VER_ID AS VARCHAR(3)) END [AD_IP_VER_ID],
			adev.STRT_TMST  [AD_STRT_TMST],	
			adev.EVENT_DRTN_IN_MIN_QTY [AD_EVENT_DRTN_IN_MIN_QTY],
			adev.EXTRA_DRTN_TME_AMT [AD_EXTRA_DRTN_TME_AMT],			
			adev.END_TMST  [AD_END_TMST],								
			CASE
				WHEN lkac.ACTN_DES = ''Completed Email Notification'' THEN ''Completed'' 
				ELSE lkac.ACTN_DES END  [AD_ACTN_DES] , 				
			adhi.CREAT_DT [AD_MOD_DT],							
			CASE
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END [AD_MOD_BY],		
			CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '''' ELSE scev.SUCSS_ACTY_DES END [AD_SUCSS_ACTY_DES],		
			CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '''' ELSE flev.FAIL_ACTY_DES END  [AD_FAIL_ACTY_DES],
			CASE WHEN adhi.CMNT_TXT is NULL THEN '''' ELSE adhi.CMNT_TXT END [AD_CMNT_TXT],
			adev.ESCL_CD  [ESCL_CD]					
			from COWS.dbo.AD_EVENT adev with (nolock) 
			left outer 	JOIN COWS.dbo.LK_EVENT_STUS adevst with (nolock) ON adev.EVENT_STUS_ID = adevst.EVENT_STUS_ID
			left outer 	JOIN COWS.dbo.LK_ENHNC_SRVC adlkes with (nolock) ON adev.ENHNC_SRVC_ID = adlkes.ENHNC_SRVC_ID
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=adev.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=1
			left outer JOIN
			(   	SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT,eh.CMNT_TXT
					FROM COWS.dbo.EVENT_HIST eh with (nolock)					
					JOIN COWS.dbo.AD_EVENT acde with (nolock) ON eh.EVENT_ID = acde.EVENT_ID
					WHERE eh.ACTN_ID in (5,44,19) AND acde.EVENT_STUS_ID in (3, 6) 
					--axm3320: WHERE eh.ACTN_ID in (5,10,12,15) AND acde.EVENT_STUS_ID not in (8)
				UNION
				SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT,ehis.CMNT_TXT
					FROM COWS.dbo.EVENT_HIST ehis with (nolock)
					JOIN
					(	SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) ''maxEvHis''
							FROM COWS.dbo.EVENT_HIST eh with (nolock)
							JOIN COWS.dbo.AD_EVENT acde with (nolock) ON eh.EVENT_ID = acde.EVENT_ID
							WHERE eh.ACTN_ID in (18) AND acde.EVENT_STUS_ID = 6 
					--axm3320:	WHERE eh.ACTN_ID in (18) AND acde.EVENT_STUS_ID not in (8)
						GROUP BY eh.EVENT_ID
					)ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis
			)adhi ON adev.EVENT_ID = adhi.EVENT_ID						
			LEFT outer JOIN
			( 	SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID
		  		FROM
		  		( 	SELECT (SELECT lksu.SUCSS_ACTY_DES  + '', '' AS [text()] 
	 				FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)
	 				JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID 
	 				WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID --AND evsu.REC_STUS_ID = 1
	 				FOR xml PATH ('''')
				) as SUCSS_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)	
					)evSucAct
		  		)evSucAct2
			)scev ON adhi.EVENT_HIST_ID = scev.EVENT_HIST_ID
			LEFT outer JOIN
			( SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID
		  		FROM ( SELECT (SELECT lkfl.FAIL_ACTY_DES  + '', '' AS [text()] 
	 						FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)
	 						JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID 
	 						WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID --AND evfl.REC_STUS_ID = 1
	 						FOR xml PATH ('''')
							) as FAIL_ACTY_DES, EVENT_HIST_ID 
					FROM
					(SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)	
					)evFailAct
		  		)evFailAct2
			)flev ON adhi.EVENT_HIST_ID = flev.EVENT_HIST_ID			
			LEFT outer JOIN
			( SELECT distinct LEFT([FMS_CKT_LIST],len([FMS_CKT_LIST]) -1) as FMS_CKT_LIST, EVENT_ID
				FROM ( SELECT (SELECT adat.CKT_ID + '', '' AS [text()] 
							FROM COWS.dbo.AD_EVENT_ACCS_TAG adat with (nolock)							
							WHERE adat.EVENT_ID = acde.EVENT_ID 
							FOR xml PATH ('''')
						   ) as FMS_CKT_LIST, EVENT_ID 
					FROM 
					( SELECT EVENT_ID FROM COWS.dbo.AD_EVENT with (nolock) where EVENT_STUS_ID in (3,6)
					-- SELECT EVENT_ID FROM COWS.dbo.AD_EVENT with (nolock) where EVENT_STUS_ID not in (8)
					) acde
				)fmsci
			)fmsc ON adev.EVENT_ID = fmsc.EVENT_ID			
			left JOIN COWS.dbo.LK_USER luser1 with (nolock) ON adhi.CREAT_BY_USER_ID = luser1.USER_ID 
			left JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON adhi.ACTN_ID = lkac.ACTN_ID ) as adev on adev.FTN = a.FTN )'
            
            set @SQLSELECT3 = ' select * from AD_Event_Data '
            
--print (@SQLSELECT1) 
--print (@SQLSELECT2) 
--print (@SQLSELECT3) 

exec ( @SQLSELECT1 + @SQLSELECT2 + @SQLSELECT3)

CLOSE SYMMETRIC KEY FS@K3y;


END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_App_ADHOCSENS_AD_INFO' 
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH

