USE [COWS_Reporting]
GO
/*
--=========================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			11/21/2011
-- Description:		
--					Extract Fast Track and MDS event(s) in "Completed" & "Returned To Work" 
--					statuses for specified parameter(s); Customer Name and/or Date Range.
--
-- Notes:			
--
-- 02/21/2012		sxv0766: Added Reschedule Failed ACTN ID from 31 to 43, 
--							 Increased Comments field size to 4000 chars
-- 03/29/2012		sxv0766: Increased field sizes:Comments to VARCHAR(MAX), Action to 250
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--=========================================================================================
*/
ALTER Procedure [dbo].[sp_MNSRptEventsWorkedOD]
	@SecuredUser		INT=0,	
	@getSensData		CHAR(1)='N',	
	@startDate			Datetime=NULL,
	@endDate			Datetime=NULL
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDateStr	VARCHAR(20)
	DECLARE @endDateStr		VARCHAR(20)	
	
	SET @startDate=CONVERT(DATETIME, @startDate, 101)  
    SET @endDate=CONVERT(DATETIME, @endDate, 101)      
    -- Add 1 day to End Date in order to cover the End Date 24-hr period  
    SET @endDate=DATEADD(DAY, 1, @endDate)
	
	DECLARE  @mp_mns_evtWrked AS TABLE
	(
		Event					VARCHAR(20),
		Event_ID				INT,		
		Cust_Name				VARCHAR(100),
		Cust_Name2			VARCHAR(100),
		Scurd_Cd				Bit,
		Author					VARCHAR(100),
		YearMonth				VARCHAR(10),
		Event_Action			VARCHAR(250),
		Fail_Codes				VARCHAR(100),
		Date_And_Time			VARCHAR(25),				
		MDS_Inst_Acty			VARCHAR(100),	
		Performed_By			VARCHAR(100),
		Comment					VARCHAR(MAX),		
		MDS_Acty_Type			VARCHAR(25)
	)
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	-- MDS Activity Type: Initial Install and MAC 
	-- 12/01/2011: sxv0766 - Comment out JOIN COWS.dbo.FSA_MDS_EVENT
	

			INSERT INTO @mp_mns_evtWrked(Event, Event_ID, Cust_Name, Scurd_Cd, Author, YearMonth, Event_Action,  
 					Fail_Codes, Date_And_Time, MDS_Inst_Acty, Performed_By, Comment, MDS_Acty_Type)
 					
			SELECT	CASE mds.MDS_FAST_TRK_CD
				WHEN 1 THEN 'MDS Fast Track'
				WHEN 0 THEN 'MDS' END ,
			mds.EVENT_ID ,
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,
			luser1.DSPL_NME , 
			SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) , 
			CASE
				WHEN actn.ACTN_DES is NULL THEN ''		
				ELSE actn.ACTN_DES END ,
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END ,
 			REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) ,								
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END ,				
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END ,	
 			CASE
 				WHEN evhi.CMNT_TXT is NULL THEN ''
 				ELSE evhi.CMNT_TXT END ,				
			mact.MDS_ACTY_TYPE_DES 							
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)		
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID AND mds.MDS_ACTY_TYPE_ID = 2
		LEFT JOIN ( SELECT EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID			
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID 	
	WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND mds.EVENT_STUS_ID in (3, 6) 
	      AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT)  AND evhi.CREAT_DT <COALESCE(@endDate,evhi.CREAT_DT) 
	        
	UNION
	-- MDS Activity Type: Disconnect
	
	SELECT	CASE mds.MDS_FAST_TRK_CD
				WHEN 1 THEN 'MDS Fast Track'
				WHEN 0 THEN 'MDS' END ,
			mds.EVENT_ID ,
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,
			luser1.DSPL_NME , 
			SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) , 
			CASE
				WHEN actn.ACTN_DES is NULL THEN ''		
				ELSE actn.ACTN_DES END ,
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END ,
 			REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) ,								
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END ,							
			CASE
				WHEN luser2.DSPL_NME is NULL THEN '' 
				ELSE luser2.DSPL_NME END ,	
 			CASE
 				WHEN evhi.CMNT_TXT is NULL THEN ''
 				ELSE evhi.CMNT_TXT END ,				
			mact.MDS_ACTY_TYPE_DES 							
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID		
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN ( SELECT EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID					
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID			
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID 	
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (3, 6) 	
	      AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT )  AND evhi.CREAT_DT<COALESCE(@endDate,evhi.CREAT_DT)   
   
	UNION ALL
	SELECT	CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END ,
			mds.EVENT_ID ,
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,
			luser1.DSPL_NME , 
			SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) , 
			CASE
				WHEN actn.ACTN_DES is NULL THEN ''		
				ELSE actn.ACTN_DES END ,
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END ,
 			REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) ,								
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END ,				
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END ,	
 			CASE
 				WHEN evhi.CMNT_TXT is NULL THEN ''
 				ELSE evhi.CMNT_TXT END ,				
			mact.MDS_ACTY_TYPE_DES 							
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)		
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID AND mds.MDS_ACTY_TYPE_ID = 2
		LEFT JOIN ( SELECT EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID			
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID 	
	WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND mds.EVENT_STUS_ID in (3, 6) 
	      AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT)  AND evhi.CREAT_DT <COALESCE(@endDate,evhi.CREAT_DT) 
	        
	UNION
	-- MDS Activity Type: Disconnect
	
	SELECT	CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END ,
			mds.EVENT_ID ,
			CASE 						
				WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
				ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,
			luser1.DSPL_NME , 
			SUBSTRING(CONVERT(char, mds.STRT_TMST, 120), 1, 7) , 
			CASE
				WHEN actn.ACTN_DES is NULL THEN ''		
				ELSE actn.ACTN_DES END ,
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END ,
 			REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) ,								
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END ,							
			CASE
				WHEN luser2.DSPL_NME is NULL THEN '' 
				ELSE luser2.DSPL_NME END ,	
 			CASE
 				WHEN evhi.CMNT_TXT is NULL THEN ''
 				ELSE evhi.CMNT_TXT END ,				
			mact.MDS_ACTY_TYPE_DES 							
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID		
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN ( SELECT EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID in (15, 17) OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID					
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID			
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID 	
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (3, 6) 	
	      AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT )  AND evhi.CREAT_DT<COALESCE(@endDate,evhi.CREAT_DT)  
	      
	      	
	
	SELECT	Event,
			Event_ID 'Event ID', 
			CASE 
				--WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--ELSE Cust_Name 
				WHEN @SecuredUser = 0 and Scurd_Cd =0 THEN CUST_NAME
				WHEN @SecuredUser = 0 and Scurd_Cd >0 AND  CUST_NAME2 <>'' THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd >0 AND CUST_NAME2 <> '' THEN 'Private Customer'
				ELSE COALESCE(CUST_NAME2,CUST_NAME,'') 
				END 'Customer Name',			
			Author,	
			YearMonth,
			Event_Action 'Event Action',
			Fail_Codes 'Fail Codes',
			Date_And_Time 'Date And Time',	
			MDS_Inst_Acty 'MDS Install Activity',	
			Performed_By 'Performed By',	
			Comment,
			MDS_Acty_Type 'MDS Activity Type'
			
	FROM @mp_mns_evtWrked
	ORDER BY 'Event ID'
		
	
	CLOSE SYMMETRIC KEY FS@K3y;
	
	RETURN 0;
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(300)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptEventsWorkedOD, ' 	
	SET @Desc=@Desc +  CAST(@SecuredUser AS VARCHAR(2)) + ', ''' + @getSensData + ''', '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH



