USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptMonthlyCPE]    Script Date: 11/21/2019 9:34:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
--=======================================================================================
-- Author:			David Phillips	
-- Date:			10/8/2014
-- Description:		
-- Modifications:
--  [dbo].[sp_MNSRptMonthlyCPE] 0
-- 01/27/2017		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked
--  [dbo].[sp_MNSRptMonthlyCPE] 1
--=======================================================================================
*/

ALTER PROCEDURE [dbo].[sp_MNSRptMonthlyCPE]
	@QueryNumber		INT,	
	@Secured			INT=0	
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDate		Datetime
	DECLARE @endDate		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)	
	DECLARE @dayOfWeek		INT
	
	SET @today=DATEADD(MONTH,-1,GETDATE())
	SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
	SET @startDate=cast(@startDtStr AS datetime)
	SET @endDate=cast((cast(SUBSTRING(CONVERT(VARCHAR(10), getdate(), 101),1,2) + '/01/' + CAST(YEAR(getdate()) AS VARCHAR(4)) as datetime) -1) AS datetime)  
	--PRINT @startDate
	--PRINT @endDate			
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SELECT	distinct
		mds.END_TMST 'Event End Date',
		mds.EVENT_ID 'Event ID',
		CASE 						
			--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
			--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
			--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME))
			
			WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
			WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
			ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
			END 'Customer Name',
		emd.ODIE_DEV_NME 'Device Name',
		CASE mds.MDS_FAST_TRK_CD
			WHEN 1 THEN 'MDS Fast Track'
			WHEN 0 THEN 'MDS' END 'Event Type',
		mat.MDS_ACTY_TYPE_DES 'Activity Type',
		lmma.MDS_MAC_ACTY_NME 'MAC Activity',
		evst.EVENT_STUS_DES 'Event Status'	,
		sass.SRVC_ASSRN_SITE_SUPP_DES 'Site Support Contact',
		SPRINT_CPE_NCR_DES 'CPE Delivery Option',
		CONVERT(varchar (100), DecryptByKey(fmds.CTRY_RGN_NME)) 'Country',
		''                                    AS [S/C Flag], 
		'MDS Only'			AS [Network Activity Type]
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)	
		JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_MNGD_ACT_NEW mman with (nolock) on mds.EVENT_ID = mman.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mema with (nolock) on mema.EVENT_ID = mds.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY lmma with (nolock) on lmma.MDS_MAC_ACTY_ID = mema.MDS_MAC_ACTY_ID
		LEFT JOIN COWS.dbo.LK_MDS_ACTY_TYPE mat with (nolock) on mat.MDS_ACTY_TYPE_ID = mds.MDS_ACTY_TYPE_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_ODIE_DEV_NME emd with (nolock) on emd.FSA_MDS_EVENT_ID = fmds.FSA_MDS_EVENT_ID
		LEFT JOIN COWS.dbo.lk_SPRINT_CPE_NCR lsc WITH (NOLOCK) ON lsc.SPRINT_CPE_NCR_ID = mds.SPRINT_CPE_NCR_ID
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		left JOIN COWS.dbo.MDS_MNGD_ACT_NEW mma with (nolock) on emd.ODIE_DEV_NME = mma.ODIE_DEV_NME
							AND mma.EVENT_ID = fmds.EVENT_ID
		left JOIN COWS.dbo.LK_SRVC_ASSRN_SITE_SUPP sass with (nolock) on mma.SRVC_ASSRN_SITE_SUPP_ID = sass.SRVC_ASSRN_SITE_SUPP_ID
		
	WHERE  mds.END_TMST >= @startDate AND mds.END_TMST < @endDate
		AND CONVERT(varchar (100), DecryptByKey(fmds.US_INTL_ID)) = 'I'

	/*New MDS Events Code Start */
	UNION ALL

	SELECT	distinct
		mds.END_TMST 'Event End Date',
		mds.EVENT_ID 'Event ID',
		CASE 						
			--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
			--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
			--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
			
			WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
			WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
			ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'')
			END 'Customer Name',
		emd.ODIE_DEV_NME 'Device Name',     ---------- From Corrected New Joins
		--CASE mds.MDS_FAST_TRK_CD WHEN 1 THEN 'MDS Fast Track'	WHEN 0 THEN 'MDS' END 'Event Type',
		CASE mds.MDS_FAST_TRK_TYPE_ID 	WHEN 'A' THEN 'MDS Fast Track' WHEN 'S' THEN 'MDS Fast Track' ELSE  'MDS' END 'Event Type',
		mat.MDS_ACTY_TYPE_DES 'Activity Type',
		lmma.MDS_MAC_ACTY_NME 'MAC Activity',
		evst.EVENT_STUS_DES 'Event Status'	,
		sass.SRVC_ASSRN_SITE_SUPP_DES 'Site Support Contact',  ---------- From Corrected New Joins
		SPRINT_CPE_NCR_DES 'CPE Delivery Option',
		CONVERT(varchar (100), DecryptByKey(mds.CTRY_RGN_NME)) 'Country',
			CASE	
						WHEN emd.SC_CD = 'C'        THEN 'Complex'
						WHEN emd.SC_CD = 'S'        THEN 'Simple'
					    ELSE ''
						END                                     AS [S/C Flag],
		lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type]		
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		---Obsolete Table	
		--LEFT JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		/* we dont use those both tables( FSA_MDS_EVENt   and FSA_MDS_EVENT_NEW ) in new mds event i.e mds_event  */
		LEFT JOIN COWS.dbo.MDS_MNGD_ACT_NEW mman with (nolock) on mds.EVENT_ID = mman.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mema with (nolock) on mema.EVENT_ID = mds.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY lmma with (nolock) on lmma.MDS_MAC_ACTY_ID = mema.MDS_MAC_ACTY_ID
		LEFT JOIN COWS.dbo.LK_MDS_ACTY_TYPE mat with (nolock) on mat.MDS_ACTY_TYPE_ID = mds.MDS_ACTY_TYPE_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_ODIE_DEV emd with (nolock) on emd.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.lk_SPRINT_CPE_NCR lsc WITH (NOLOCK) ON lsc.SPRINT_CPE_NCR_ID = mds.SPRINT_CPE_NCR_ID
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		---Obsolete Table
		--left JOIN COWS.dbo.MDS_MNGD_ACT_NEW mma with (nolock) on mma.EVENT_ID = fmds.EVENT_ID  --AND emd.ODIE_DEV_NME = mma.ODIE_DEV_NME
		--left JOIN COWS.dbo.LK_SRVC_ASSRN_SITE_SUPP sass with (nolock) on mma.SRVC_ASSRN_SITE_SUPP_ID = sass.SRVC_ASSRN_SITE_SUPP_ID
		left JOIN COWS.dbo.LK_SRVC_ASSRN_SITE_SUPP sass with (nolock) on emd.SRVC_ASSRN_SITE_SUPP_ID = sass.SRVC_ASSRN_SITE_SUPP_ID
		INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = mds.NTWK_ACTY_TYPE_ID
	WHERE  mds.END_TMST >= @startDate AND mds.END_TMST < @endDate
		AND CONVERT(varchar (100), DecryptByKey(mds.US_INTL_CD)) = 'I'

	/*New MDS Events Code OLD */	
	ORDER BY mds.END_TMST
	
	CLOSE SYMMETRIC KEY FS@K3y;
		
	RETURN 0;
			
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptMonthlyCPE ' 
	SET @Desc=@Desc + ',' + CAST(@Secured AS VARCHAR(2)) + ': '	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH

