USE [COWS_Reporting]
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
---- [dbo].[sp_CPEFIFOMnthlyPerfomance] 1
---- =============================================
---- Author:		vn370313
---- Create date: 08/07/2017
---- Description: CPE Pending Mnthly Orders
---- Chnagedby:		vn370313
---- Create date: 09/07/2017
---- Description: Added  add the date when the Provisioner puts the jeopardy code 
				  [Date Entered Provisioning]  some time NULL so FIFO date calculation need Precautions
---- on an order  AND there a way for you to count the business days and not weekends when showing the provisioning Interval


---- [dbo].[sp_CPEFIFOMnthlyPerfomance] 0

CPE Provisioning FIFO Performance Report � this is used by the managers to grade the Provisioners on how long it is 
taking them to process orders and  if they are meeting our SLA to sales.     Orders are counted, BPM CPE Prov inbox = 0 
(Date Entered Provisioning),   FIFO Due Date = 3 business days from date order entered Provisioning,  Provisioning 
Completion Date = date Provisioners  moves order from �Eqpt Review� to next status OR Returns the order to sales.
The math then provides the Provisioning Interval.  Sample � order enters our inbox in BPM 7-11, is worked to eqpt receipts 
on 7-13, internal = 2   the report
Needs to account for weekends.  I manually count when there is a holiday.
it needs to capture all orders that came through the CPE Domestic Provisioning team

EXEC RAD_Reporting.rad.sp_ReportRefreshQueue_NEW 'N','167'
Select 	* from RAD_Reporting.[rad].[REPORTQUEUE_NEW] with (nolock)
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- [dbo].[sp_CPEFIFOMnthlyPerfomance] 1,1
-- =============================================
*/
ALTER PROCEDURE [dbo].[sp_CPEFIFOMnthlyPerfomance]
	 @Secured			   INT = 0,
	 @RptID                INT = 1
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY


        DECLARE @startDate	Datetime
		DECLARE	@endDate	Datetime
        IF  @RptID   = 0  -- Weekly Report
			BEGIN
				SET @startDate	= DATEADD(dd,-8,CONVERT(VARCHAR(25),GETDATE(),101))
				SET	@endDate = GETDATE()
			END
		ELSE -- Monthly
			BEGIN
				SET @startDate	= DATEADD(mm,-1,CONVERT(VARCHAR(25),DATEADD(DD,-(DAY(GETDATE())-1),GETDATE()),101))
				SET	@endDate = GETDATE()
			END
		
		--SET	@endDate	=DATEADD(dd,-8, CONVERT(VARCHAR(25),GETDATE(),101))
		--print  @startDate
		--Print  @endDate
		
		OPEN SYMMETRIC KEY FS@K3y 
		DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 
		
		SELECT  [Provisioner Name]	,
				[M5 Order],	
				[Sequence Id],	
				[Device Id],	
				[Date Entered Provisioning],	
				--[FIFO Due Date],	
				[FIFO Due Date]=
					CASE WHEN [Date Entered Provisioning] IS NOT NULL  THEN				
					CONVERT(VARCHAR(10),COWS_Reporting.dbo.[CalculateEndBusinessDate]([Date Entered Provisioning],4), 101)
					ELSE '' END,	
				[Provisioning Completion Date],	
				[Provisioning Interval],	
				[Jeopardy Code],	
				[Jeopardy Code Date],	
				[Customer Name],	
				[MNS],	
				[Sales Contact (IPM)],	
				[Request Type],	
				[Expedite],	
				MAX([Move Type]) AS [Move Type],	
				[Records Only]

		FROM(
		
		SELECT DISTINCT 
				ISNULL(lu.DSPL_NME,'')											    as [Provisioner Name],
				f.FTN																as [M5 Order], 
				f.ORDR_ID															as [Sequence Id],
				l.Device_ID															as [Device Id],
				(SELECT MIN(CONVERT(VARCHAR(10), at1.CREAT_DT, 101) ) 
						FROM COWS.dbo.ACT_TASK at1 WITH (NOLOCK)
						WHERE at1.TASK_ID IN (600)
						  AND at1.ORDR_ID = f.ORDR_ID)  
																					as [Date Entered Provisioning],
				/*
				(SELECT CONVERT(VARCHAR(10),COWS_Reporting.dbo.[CalculateEndBusinessDate](MIN(at1.CREAT_DT),4), 101) 
						FROM COWS.dbo.ACT_TASK at1 WITH (NOLOCK)
						WHERE at1.TASK_ID IN (600)
						   AND at1.ORDR_ID = f.ORDR_ID)
																					as [FIFO Due Date],
				*/
				'' AS [FIFO Due Date],
				ISNULL((SELECT MIN(CONVERT(VARCHAR(10), at1.CREAT_DT, 101) ) 
						FROM COWS.dbo.ACT_TASK at1 WITH (NOLOCK)
						WHERE at1.TASK_ID NOT IN (600)
						  AND at1.ORDR_ID = f.ORDR_ID),'') 
																					as [Provisioning Completion Date],

		        /*ISNULL((Select DATEDIFF(dd,MIN(t2.CREAT_DT),MIN(t1.CREAT_DT))*/
				ISNULL((Select COWS_REPORTING.dbo.[CalculateBusinessDays_CPE](MIN(t2.CREAT_DT),MIN(t1.CREAT_DT))
					FROM (
								SELECT (at1.CREAT_DT),at1.ORDR_ID FROM COWS.dbo.ACT_TASK at1 WITH (NOLOCK)  ----Equip Review---INBOX
									WHERE at1.TASK_ID NOT IN (600)	AND at1.ORDR_ID= f.ORDR_ID )  t1
					INNER JOIN (
								SELECT (at1.CREAT_DT),at1.ORDR_ID FROM COWS.dbo.ACT_TASK at1 WITH (NOLOCK)
									WHERE at1.TASK_ID NOT IN (602)	AND at1.ORDR_ID= f.ORDR_ID )  t2   ----Equip Receipt
						ON t1.ORDR_ID=t2.ORDR_ID ),'')
																					as [Provisioning Interval],
				ISNULL((Select TOP 1 JPRDY_CD from COWS.dbo.ORDR_JPRDY jpdy with (nolock) 
							WHERE ORDR_ID=o.ORDR_ID order by CREAT_DT DESC),'') 
																					AS [Jeopardy Code],
				ISNULL((Select TOP 1 CONVERT(VARCHAR(10),CREAT_DT, 101) from COWS.dbo.ORDR_JPRDY jpdy with (nolock) 
							WHERE ORDR_ID=o.ORDR_ID order by CREAT_DT DESC),'') 
																					AS [Jeopardy Code Date],
				CASE  
						WHEN @Secured = 0 AND o.CSG_LVL_ID >0 THEN 'Private Customer' 
						ELSE COALESCE(CONVERT(varchar(max), DecryptByKey(c.CUST_NME)),ISNULL(c.CUST_NME,''))
				END																	AS [Customer Name],
				
				CASE ISNULL(o.PROD_ID,'') WHEN 'MNS' THEN 'Y' ELSE '' END			as [MNS],
				
				CASE  
					WHEN @Secured = 0 AND o.CSG_LVL_ID >0  THEN 'Private Sales Contact' 
					ELSE (SELECT Top 1 COALESCE(CONVERT(varchar(max), DecryptByKey(csd.CUST_CNTCT_NME)), ISNULL(oc.CNTCT_NME,''))
									FROM  COWS.[dbo].[ORDR_CNTCT] oc WITH (NOLOCK) 
									      LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15
									WHERE ORDR_ID = f.ORDR_ID AND ROLE_ID = 99)
				END																	as [Sales Contact (IPM)] ,
				lot.ORDR_TYPE_DES													as [Request Type] ,
				CASE WHEN ISNULL(f.INSTL_ESCL_CD,'N') = 'N' THEN '' ELSE 'Y'  END	AS [Expedite],
				CASE 
					WHEN f.ORDR_SUB_TYPE_CD = 'ME' AND ISNULL(l.CPE_REUSE_CD,0) = 0	AND f.ORDR_TYPE_CD != 'DC' 
						AND NOT EXISTS(SELECT 1 FROM  COWS.dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
							           WHERE ISNULL(DROP_SHP,'N') <> 'Y'  AND MATL_CD NOT IN (044747,044746,044745) 
									   AND ISNULL(CPE_REUSE_CD,0) = 1  
									   AND ORDR_ID = f.ORDR_ID)  
									   THEN 'H'  
					WHEN f.ORDR_SUB_TYPE_CD = 'ME' AND ISNULL(l.CPE_REUSE_CD,0) = 1	THEN 'R'
					ELSE ''
					END																AS [Move Type],
				
				CASE 
					WHEN ISNULL(f.CPE_REC_ONLY_CD,'N') = 'N'  THEN ''
					ELSE 'Y'
			    END                                                               AS [Records Only]


		FROM COWS.dbo.FSA_ORDR f WITH (NOLOCK) 
			INNER JOIN COWS.dbo.ORDR o WITH (NOLOCK) ON f.ORDR_ID = o.ORDR_ID
			INNER JOIN COWS.dbo.ACT_TASK a WITH (NOLOCK) ON a.ORDR_ID = f.ORDR_ID
			INNER JOIN (SELECT DISTINCT ORDR_ID, DEVICE_ID, CPE_REUSE_CD
							 FROM COWS.dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
							 WHERE ISNULL(DROP_SHP,'N') <> 'Y'  AND MATL_CD NOT IN (044747,044746,044745)
							)l ON l.ORDR_ID = f.ORDR_ID
			INNER JOIN COWS.dbo.FSA_ORDR_CUST c WITH (NOLOCK) ON c.ORDR_ID = f.ORDR_ID AND c.CIS_LVL_TYPE in ('H5','H6')
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=c.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
			INNER JOIN COWS.dbo.LK_TASK lt	WITH (NOLOCK) ON a.TASK_ID = lt.TASK_ID

			LEFT JOIN COWS.[dbo].[LK_ORDR_TYPE] lot WITH (NOLOCK) ON f.ORDR_TYPE_CD = lot.FSA_ORDR_TYPE_CD
			LEFT JOIN COWS.[dbo].[LK_PROD_TYPE] lpt WITH (NOLOCK) ON f.PROD_TYPE_CD = lpt.FSA_PROD_TYPE_CD
			LEFT OUTER JOIN COWS.dbo.USER_WFM_ASMT uwa	WITH (NOLOCK) ON uwa.ORDR_ID = f.ORDR_ID
												AND uwa.GRP_ID in (13)  
	        LEFT OUTER JOIN COWS.dbo.LK_USER lu			WITH (NOLOCK) ON lu.USER_ID = uwa.ASN_USER_ID

		WHERE o.DMSTC_CD = 0 
			AND f.PROD_TYPE_CD = 'CP'
			AND o.ORDR_CAT_ID = 6  		
			AND ISNULL(o.PROD_ID,'') not in ('UCCH','UCSV','MIPT','UCSM')  
			AND f.CREAT_DT > @startDate  and f.CREAT_DT < @endDate
			AND ISNULL(lu.DSPL_NME,'') != ''
					
		--ORDER BY [Provisioner Name],[M5 Order]
		) A
		GROUP BY [Provisioner Name]	,
				[M5 Order],	
				[Sequence Id],	
				[Device Id],	
				[Date Entered Provisioning],	
				[FIFO Due Date],	
				[Provisioning Completion Date],	
				[Provisioning Interval],	
				[Jeopardy Code],	
				[Jeopardy Code Date],	
				[Customer Name],	
				[MNS],	
				[Sales Contact (IPM)],	
				[Request Type],	
				[Expedite],	
				[Records Only]
		 ORDER BY [Provisioner Name],[M5 Order]

CLOSE SYMMETRIC KEY FS@K3y;
		

	END TRY
	BEGIN CATCH
		DECLARE @Desc VARCHAR(200)
		SET @Desc='EXEC COWS_Reporting.dbo.sp_CPEFIFOMnthlyPerfomance '+', ' 
		SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '
		EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;   --- Open later during deployment
	END CATCH
END

