USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_DomesticCPEOpsDailyCancels]    Script Date: 05/15/2018 22:18:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


---- =============================================
---- Author:		dlp0278
---- Create date: 08/03/2017
---- Description: Domestic CPE OPS Daily Report- Cancel sheet
---- [dbo].[sp_DomesticCPEOpsDailyCancels] 1
---- vn370313  : Some performance improvement done  With Comment
-- =============================================
ALTER PROCEDURE [dbo].[sp_DomesticCPEOpsDailyCancels]
 @Secured			   INT=0
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY

OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

SELECT DISTINCT
	ISNULL(ISNULL(lu.DSPL_NME,plu.DSPL_NME),'')							AS [Assigned User] 
	,fo.FTN																AS [Mach5 Order #]
	--,(SELECT TOP 1 REPLACE(LTRIM(REPLACE(DEVICE_ID, '0', ' ')), ' ', '0')  FROM COWS.dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
	--			WHERE fo.ORDR_ID = ORDR_ID )                            AS [Device]   /* Performance vn370313*/
	,REPLACE(LTRIM(REPLACE((cpl.DEVICE_ID), '0', ' ')), ' ', '0')       AS [Device]   /* Performance vn370313*/
	,fo.PRNT_FTN                                                        AS [Parent Order #]
	--,ISNULL(CONVERT(VARCHAR, DecryptByKey(foc.CUST_NME)),'')			AS [CustomerName]
	--,CSG_LVL_ID
	,CASE
			WHEN @secured = 0 and o.CSG_LVL_ID =0 THEN foc.CUST_NME
			WHEN @secured = 0 and o.CSG_LVL_ID >0 AND csd.CUST_NME IS NOT NULL THEN 'PRIVATE CUSTOMER' 
			WHEN @secured = 0 and o.CSG_LVL_ID >0 AND csd.CUST_NME IS NULL THEN NULL 
			ELSE COALESCE(foc.CUST_NME,CONVERT(varchar, DecryptByKey(csd.CUST_NME)),'') 
			END
			AS [CustomerName]
	,H5_H6_CUST_ID                                                      AS [H6 Id]
	,Convert(varchar (10),fo.CREAT_DT,101)								AS [Received Date]

	 ,ISNULL(prh.REQSTN_NBR,'')                                         AS [Req #]
	 , CASE
			WHEN prh.REQSTN_DT IS NULL  THEN ''
			ELSE Convert(varchar (10),prh.REQSTN_DT,101)
		END																AS [Req Date]
	 ,ISNULL(cli.PRCH_ORDR_NBR,'')										AS [PO #]
	 ,ISNULL(foc.SITE_ID,'')                                            AS [Site Id]
	 ,ISNULL(pfo.CPE_VNDR,'')                                           AS [CPE Vendor]
	
FROM  COWS.dbo.FSA_ORDR fo WITH (NOLOCK)
LEFT OUTER JOIN  COWS.dbo.FSA_ORDR pfo WITH (NOLOCK) ON pfo.FTN = fo.PRNT_FTN
LEFT OUTER JOIN COWS.dbo.FSA_ORDR_CPE_LINE_ITEM cpl WITH (NOLOCK) ON fo.ORDR_ID = cpl.ORDR_ID  /* Performance vn370313*/
INNER JOIN  COWS.dbo.ORDR o WITH (NOLOCK) ON o.ORDR_ID = pfo.ORDR_ID
INNER JOIN COWS.dbo.LK_ORDR_TYPE lot WITH (NOLOCK) ON lot.FSA_ORDR_TYPE_CD = pfo.ORDR_TYPE_CD
INNER JOIN COWS.dbo.ACT_TASK at WITH (NOLOCK) ON at.ORDR_ID = fo.ORDR_ID 
								AND at.TASK_ID = 1001 and at.STUS_ID = 0
LEFT OUTER JOIN  COWS.dbo.FSA_ORDR_CUST foc WITH (NOLOCK) ON foc.ORDR_ID = fo.ORDR_ID
					AND foc.CIS_LVL_TYPE IN ('H5','H6')
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=foc.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
LEFT OUTER JOIN  COWS.dbo.USER_WFM_ASMT puwa WITH (NOLOCK) ON puwa.ORDR_ID = pfo.ORDR_ID
LEFT OUTER JOIN  COWS.dbo.USER_WFM_ASMT uwa WITH (NOLOCK) ON uwa.ORDR_ID = fo.ORDR_ID	
LEFT OUTER JOIN  COWS.dbo.LK_USER plu WITH (NOLOCK) ON plu.USER_ID = puwa.ASN_USER_ID							
LEFT OUTER JOIN  COWS.dbo.LK_USER lu WITH (NOLOCK) ON lu.USER_ID = uwa.ASN_USER_ID
LEFT OUTER JOIN  COWS.dbo.PS_REQ_HDR_QUEUE prh WITH (NOLOCK) ON prh.ORDR_ID = pfo.ORDR_ID
LEFT OUTER JOIN  COWS.dbo.FSA_ORDR_CPE_LINE_ITEM cli WITH (NOLOCK) ON pfo.ORDR_ID = cli.ORDR_ID		
												AND prh.REQSTN_NBR = cli.PLSFT_RQSTN_NBR 

WHERE fo.PROD_TYPE_CD = 'CP'
	--AND fo.ORDR_ID in (SELECT ORDR_ID from  COWS.dbo.ACT_TASK with (NOLOCK)
	--					WHERE TASK_ID in (1001) and STUS_ID = 0)   /* Performance No need its already filtered in Join vn370313*/
	AND o.DMSTC_CD = 0
	AND o.PROD_ID not in ('UCCH','UCSV','MIPT','UCSM')   


Order by [Assigned User], [Received Date] desc
CLOSE SYMMETRIC KEY FS@K3y;

	END TRY
	BEGIN CATCH
		DECLARE @Desc VARCHAR(200)
		SET @Desc='EXEC COWS_Reporting.dbo.sp_DomesticCPEOpsDailyCancels '+', ' 
		SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '
		EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;   --- Open later during deployment
	END CATCH
END
GO


