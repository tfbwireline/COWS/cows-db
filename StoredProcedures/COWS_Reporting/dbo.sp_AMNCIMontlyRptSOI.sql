USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_AMNCIMonthlyRptSOI]    Script Date: 06/04/2018 21:00:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Author:  David Phillips  
-- Create date: 2/12/2015  
-- Description: <Description,,>  
-- Updated By:  Md M Monir
-- Updated Date:03/14/2018   
-- EXEC COWS_Reporting.dbo.sp_AMNCIMonthlyRptSOI 0, 2  
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
-- Updated Date: 06/04/2018
-- Updated Reason: Performance Improvement   
-- =============================================  
ALTER PROCEDURE [dbo].[sp_AMNCIMonthlyRptSOI]   
  @SecuredUser  INT=0,  
  @Query    INT = 1  
  
AS  
BEGIN TRY  
  
 SET NOCOUNT ON;  
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
   
 IF @Query in (1,3)  
 BEGIN  
  
  DECLARE @startDate Datetime=DATEADD(mm,-1,CONVERT(VARCHAR(25),DATEADD(DD,-(DAY(GETDATE())-1),GETDATE()),101))  
  DECLARE @endDate Datetime=GETDATE()  
    
 END  
  
 IF @Query = 1  
 BEGIN  
  SELECT DISTINCT  
   fsa.FTN            [FTN],  
   CASE  
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID = 0 AND foc.CUST_NME<>''  THEN coalesce(foc.CUST_NME,'PRIVATE CUSTOMER')
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID > 0 AND foc.CUST_NME =''  THEN 'PRIVATE CUSTOMER'
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID > 0 AND foc.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'    
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),foc.CUST_NME,'')  
   END             [Customer Name],  
   CASE   
    WHEN ISNULL(oms.ORDR_DSCNCT_DT,oms.CUST_ACPTC_TURNUP_DT) IS NULL THEN ''         
    ELSE CONVERT(VARCHAR(10),ISNULL(oms.ORDR_DSCNCT_DT,ISNULL(oms.CUST_ACPTC_TURNUP_DT,'')),101)  
   END             [Implementation Date],  
   --tac.FRST_NME + ' ' + tac.LST_NME     [Implementation Contact],  
   --ram.FRST_NME + ' '  + ram.LST_NME     [RAM], 
    CASE  
	WHEN @SecuredUser = 0 AND LEN(tac.NAME)>1  THEN coalesce(tac.NAME,'')
    WHEN @SecuredUser = 0 AND LEN(tac.NAME)<=2  THEN '' 
    WHEN @SecuredUser = 0 AND tac.NAME IS NULL THEN ''   
    ELSE coalesce(tac.NAME,tac.NAME2,'')    
   END     [Implementation Contact],      
   CASE  
	WHEN @SecuredUser = 0 AND LEN(ram.NAME)>1  THEN coalesce(ram.NAME,'')
    WHEN @SecuredUser = 0 AND LEN(ram.NAME)<=2  THEN '' 
    WHEN @SecuredUser = 0 AND ram.NAME IS NULL THEN ''   
    ELSE coalesce(ram.NAME,ram.NAME2,'')    
   END  [RAM], 
   ISNULL(g.FULL_NME,'')        [CPM Contact],     
   lp.PROD_TYPE_DES         [Product],  
   CONVERT(VARCHAR(10),ISNULL(ord.CUST_CMMT_DT,fsa.CUST_CMMT_DT),101)  
                [Customer Commit Date],  
   rgn.RGN_DES           [xNCI Region],  
   /*CONVERT(varchar, DecryptByKey(oa.CTY_NME))   [H5 City], */
   CASE  
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID = 0 AND oa.CTY_NME<>''  THEN coalesce(oa.CTY_NME,'PRIVATE CITY')
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID > 0 AND oa.CTY_NME =''  THEN 'PRIVATE CITY'
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID > 0 AND oa.CTY_NME IS NULL  THEN 'PRIVATE CITY'    
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd2.CTY_NME)),oa.CTY_NME,'')  
   END             [H5 City], 
    
   os.ORDR_STUS_DES         [Order Status],  
   CONVERT(VARCHAR(10),ord.CREAT_DT,101)    [FSA Submit to COWS Date],  
   CASE  
    WHEN obli.MRC_CHG_AMT IS Null THEN ''  
    ELSE CAST (obli.MRC_CHG_AMT AS MONEY)  
   END             [MRC],  
   CASE  
    WHEN obli.NRC_CHG_AMT IS Null THEN ''  
    ELSE CAST (obli.NRC_CHG_AMT AS MONEY)  
   END             [NRC],  
   CASE   
    WHEN oms.ORDR_BILL_CLEAR_INSTL_DT IS NULL THEN ''         
    ELSE CONVERT(VARCHAR(10),oms.ORDR_BILL_CLEAR_INSTL_DT,101)  
   END             [Bill Clear Date],  
   ISNULL(fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES,'')   [Access Speed],  
   lo.ORDR_TYPE_DES         [Order Type],  
   ISNULL(los.ORDR_SUB_TYPE_DES,'')     [Order Sub Type],  
   ISNULL(vl.VNDR_NME,'')        [Vendor],  
   ISNULL(fsa.TSUP_PRS_QOT_NBR,'')      [PRS Quote Number],  
   (SELECT MAX(CREAT_DT) FROM [COWS].[dbo].[ORDR_NTE]  
    where ORDR_ID = ord.ORDR_ID)      [Last Update Date],  
    CASE   
    WHEN fsa.CUST_SIGNED_DT IS NULL THEN ''         
    ELSE CONVERT(VARCHAR(10),fsa.CUST_SIGNED_DT,101)  
   END             [Customer Sign Date],  
   fsa.PRNT_FTN          [Parent FTN],  
   ISNULL(si.SOI_CD,'')        [SOI Code],   
   ISNULL(lcc.CTRY_NME,'')       [Country]  
   
    
  FROM   [COWS].[dbo].[ORDR] ord WITH (NOLOCK)  
  INNER JOIN  [COWS].[dbo].[FSA_ORDR] fsa WITH (NOLOCK) ON ord.ORDR_ID = fsa.ORDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = fsa.ORDR_ID
  INNER JOIN  [COWS].[dbo].[LK_PROD_TYPE] lp WITH (NOLOCK) ON fsa.PROD_TYPE_CD = lp.FSA_PROD_TYPE_CD  
  INNER JOIN  [COWS].[dbo].[LK_ORDR_TYPE] lo WITH (NOLOCK) ON fsa.ORDR_TYPE_CD = lo.FSA_ORDR_TYPE_CD  
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_SUB_TYPE] los WITH (NOLOCK) ON los.ORDR_SUB_TYPE_CD = fsa.ORDR_SUB_TYPE_CD   
  INNER JOIN  [COWS].[dbo].[LK_ORDR_STUS] os WITH (NOLOCK) ON os.ORDR_STUS_ID = ord.ORDR_STUS_ID  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc WITH (NOLOCK)   
       ON foc.ORDR_ID = ord.ORDR_ID AND foc.CIS_LVL_TYPE IN ('H5','H6','ES') 
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=foc.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5
  LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] oa WITH (NOLOCK)
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=oa.ORDR_ADR_ID  AND csd2.SCRD_OBJ_TYPE_ID=14    
       ON oa.ORDR_ID = ord.ORDR_ID AND oa.CIS_LVL_TYPE IN ('H5','H6','ES')  
  LEFT OUTER JOIN [COWS].[dbo].[LK_XNCI_RGN] rgn WITH (NOLOCK) ON rgn.RGN_ID = ord.RGN_ID  
    
  LEFT OUTER JOIN (SELECT ORDR_ID, VER_ID, CUST_ACPTC_TURNUP_DT,ORDR_DSCNCT_DT,ORDR_BILL_CLEAR_INSTL_DT  
        FROM [COWS].[dbo].[ORDR_MS]om WITH (NOLOCK)  
       WHERE VER_ID = (SELECT MAX(VER_ID) FROM [COWS].[dbo].[ORDR_MS] om1   
           WHERE om1.ORDR_ID = om.ORDR_ID)  
      )oms ON oms.ORDR_ID = ord.ORDR_ID  
    
	LEFT OUTER JOIN (   SELECT	ORDR_ID, oc.ROLE_ID, 
							LTRIM((oc.FRST_NME+ ' ' + oc.LST_NME)) AS NAME,
							LTRIM((CONVERT(varchar, DecryptByKey(csd.FRST_NME)) + ' '+
							CONVERT(varchar, DecryptByKey(csd.LST_NME)))) AS [NAME2]
					FROM [COWS].[dbo].[ORDR_CNTCT] oc WITH (NOLOCK)  
					LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15 
					WHERE ROLE_ID = 13 
				) tac on ord.ORDR_ID  = tac.ORDR_ID 

	LEFT OUTER JOIN ( SELECT	ORDR_ID, oc.ROLE_ID, 
							LTRIM((oc.FRST_NME+ ' ' + oc.LST_NME)) AS NAME,
							LTRIM((CONVERT(varchar, DecryptByKey(csd.FRST_NME)) + ' '+
							CONVERT(varchar, DecryptByKey(csd.LST_NME)))) AS [NAME2]
					FROM [COWS].[dbo].[ORDR_CNTCT] oc WITH (NOLOCK)  
					LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15 
					WHERE ROLE_ID = 11) ram on ord.ORDR_ID  = ram.ORDR_ID   
         
  LEFT OUTER JOIN (SELECT  ASMT_DT, ORDR_ID, ASN_USER_ID   
         FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
          WHERE ASN_USER_ID  = (SELECT MAX(ASN_USER_ID)  
      FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
       FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
      WHERE uas.ORDR_ID = bb.ORDR_ID)  
      AND bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID   
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
     WHERE usr.ORDR_ID  = bb.ORDR_ID )) ur ON ur.ORDR_ID = fsa.ORDR_ID   
    
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] g WITH (NOLOCK) ON ur.ASN_USER_ID = g.USER_ID  
    
  LEFT OUTER JOIN (SELECT ORDR_ID, SOI_CD, CIS_LVL_TYPE  
    FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  WITH (NOLOCK)  
    WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
     FROM (SELECT SOI_CD, ORDR_ID   
      FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi  
     WHERE soi.ORDR_ID = bb.ORDR_ID)   
    )si ON ord.ORDR_ID  = si.ORDR_ID   
   
  /*  
  LEFT OUTER JOIN (SELECT oac.ORDR_ID, oac.CTRY_CD, oac.CIS_LVL_TYPE, oac.REC_STUS_ID, lcc.CTRY_NME   
    FROM [COWS].[dbo].[ORDR_ADR] oac WITH (NOLOCK)  
    INNER JOIN [COWS].[dbo].[LK_CTRY] lcc on lcc.CTRY_CD = oac.CTRY_CD  
     and lcc.REC_STUS_ID = 1)cntry  
   ON cntry.ORDR_ID = si.ORDR_ID and si.CIS_LVL_TYPE = cntry.CIS_LVL_TYPE  
    and cntry.REC_STUS_ID = 1  */
  
  LEFT JOIN   [COWS].[dbo].[LK_CTRY] lcc with (Nolock) ON oa.CTRY_CD=lcc.CTRY_CD AND  lcc.REC_STUS_ID = 1 AND si.CIS_LVL_TYPE = oa.CIS_LVL_TYPE  
     
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] obli WITH (NOLOCK)  
    ON ord.ORDR_ID = obli.ORDR_ID  
    
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR] vnd WITH (NOLOCK) ON ord.ORDR_ID = vnd.ORDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] vf WITH (NOLOCK) ON vnd.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] vl WITH (NOLOCK) ON vf.VNDR_CD = vl.VNDR_CD  
    
  WHERE ord.CREAT_DT BETWEEN @startDate AND @endDate  
   AND ord.ORDR_STUS_ID <> 4  
   --AND si.SOI_CD = '805' 
  ORDER BY fsa.FTN 
 END  
  
  
 IF @Query = 2  
 BEGIN  
  SELECT DISTINCT  
   fsa.FTN            [FTN],  
   CASE  
	WHEN @SecuredUser = 0 AND foc.CUST_NME<>''  THEN coalesce(foc.CUST_NME,'PRIVATE CUSTOMER')
    WHEN @SecuredUser = 0 AND foc.CUST_NME =''  THEN 'PRIVATE CUSTOMER' 
    WHEN @SecuredUser = 0 AND foc.CUST_NME IS NULL THEN 'PRIVATE CUSTOMER'   
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),foc.CUST_NME,'')    
   END             [Customer Name],  
   CASE   
    WHEN ISNULL(oms.ORDR_DSCNCT_DT,oms.CUST_ACPTC_TURNUP_DT) IS NULL THEN ''         
    ELSE CONVERT(VARCHAR(10),ISNULL(oms.ORDR_DSCNCT_DT,ISNULL(oms.CUST_ACPTC_TURNUP_DT,'')),101)  
   END             [Implementation Date],  
   --tac.FRST_NME + ' ' + tac.LST_NME     [Implementation Contact],  
   --ram.FRST_NME + ' '  + ram.LST_NME     [RAM],  
    CASE  
	WHEN @SecuredUser = 0 AND LEN(tac.NAME)>1  THEN coalesce(tac.NAME,'')
    WHEN @SecuredUser = 0 AND LEN(tac.NAME)<=2  THEN '' 
    WHEN @SecuredUser = 0 AND tac.NAME IS NULL THEN ''   
    ELSE coalesce(tac.NAME,tac.NAME2,'')    
   END     [Implementation Contact],      
   CASE  
	WHEN @SecuredUser = 0 AND LEN(ram.NAME)>1  THEN coalesce(ram.NAME,'')
    WHEN @SecuredUser = 0 AND LEN(ram.NAME)<=2  THEN '' 
    WHEN @SecuredUser = 0 AND ram.NAME IS NULL THEN ''   
    ELSE coalesce(ram.NAME,ram.NAME2,'')    
   END  [RAM],
   ISNULL(g.FULL_NME,'')        [CPM Contact],     
   lp.PROD_TYPE_DES         [Product],  
   CONVERT(VARCHAR(10),ISNULL(ord.CUST_CMMT_DT,fsa.CUST_CMMT_DT),101)  
                [Customer Commit Date],  
   rgn.RGN_DES           [xNCI Region],  
   --CONVERT(varchar, DecryptByKey(oa.CTY_NME))   [H5 City], 
   CASE  
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID = 0 AND oa.CTY_NME<>''  THEN coalesce(oa.CTY_NME,'PRIVATE CITY')
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID > 0 AND oa.CTY_NME =''  THEN 'PRIVATE CITY'
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID > 0 AND oa.CTY_NME IS NULL  THEN 'PRIVATE CITY'    
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd2.CTY_NME)),oa.CTY_NME,'')  
   END             [H5 City], 
   os.ORDR_STUS_DES         [Order Status],  
   CONVERT(VARCHAR(10),ord.CREAT_DT,101)    [FSA Submit to COWS Date],  
   CASE  
    WHEN obli.MRC_CHG_AMT IS Null THEN ''  
    ELSE CAST (obli.MRC_CHG_AMT AS MONEY)  
   END             [MRC],  
   CASE  
    WHEN obli.NRC_CHG_AMT IS Null THEN ''  
    ELSE CAST (obli.NRC_CHG_AMT AS MONEY)  
   END             [NRC],  
   CASE   
    WHEN oms.ORDR_BILL_CLEAR_INSTL_DT IS NULL THEN ''         
    ELSE CONVERT(VARCHAR(10),oms.ORDR_BILL_CLEAR_INSTL_DT,101)  
   END             [Bill Clear Date],  
   ISNULL(fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES,'')   [Access Speed],  
   lo.ORDR_TYPE_DES         [Order Type],  
   ISNULL(los.ORDR_SUB_TYPE_DES,'')     [Order Sub Type],  
   ISNULL(vl.VNDR_NME,'')        [Vendor],  
   ISNULL(fsa.TSUP_PRS_QOT_NBR,'')      [PRS Quote Number],  
   (SELECT MAX(CREAT_DT) FROM [COWS].[dbo].[ORDR_NTE]  
    where ORDR_ID = ord.ORDR_ID)      [Last Update Date],  
    CASE   
    WHEN fsa.CUST_SIGNED_DT IS NULL THEN ''         
    ELSE CONVERT(VARCHAR(10),fsa.CUST_SIGNED_DT,101)  
   END             [Customer Sign Date],  
   fsa.PRNT_FTN          [Parent FTN],  
   ISNULL(si.SOI_CD,'')        [SOI Code],   
   ISNULL(lcc.CTRY_NME,'')       [Country]  
     
    
  FROM   [COWS].[dbo].[ORDR] ord WITH (NOLOCK)  
  INNER JOIN  [COWS].[dbo].[FSA_ORDR] fsa WITH (NOLOCK) ON ord.ORDR_ID = fsa.ORDR_ID
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = fsa.ORDR_ID  
  INNER JOIN  [COWS].[dbo].[LK_PROD_TYPE] lp WITH (NOLOCK) ON fsa.PROD_TYPE_CD = lp.FSA_PROD_TYPE_CD  
  INNER JOIN  [COWS].[dbo].[LK_ORDR_TYPE] lo WITH (NOLOCK) ON fsa.ORDR_TYPE_CD = lo.FSA_ORDR_TYPE_CD  
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_SUB_TYPE] los WITH (NOLOCK) ON los.ORDR_SUB_TYPE_CD = fsa.ORDR_SUB_TYPE_CD   
  INNER JOIN  [COWS].[dbo].[LK_ORDR_STUS] os WITH (NOLOCK) ON os.ORDR_STUS_ID = ord.ORDR_STUS_ID  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc WITH (NOLOCK)   
       ON foc.ORDR_ID = ord.ORDR_ID AND foc.CIS_LVL_TYPE IN ('H5','H6','ES') 
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=foc.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5 
  LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] oa WITH (NOLOCK)   
       ON oa.ORDR_ID = ord.ORDR_ID AND oa.CIS_LVL_TYPE IN ('H5','H6','ES')
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=oa.ORDR_ADR_ID  AND csd2.SCRD_OBJ_TYPE_ID=14   
  LEFT OUTER JOIN [COWS].[dbo].[LK_XNCI_RGN] rgn WITH (NOLOCK) ON rgn.RGN_ID = ord.RGN_ID  
    
  LEFT OUTER JOIN (SELECT ORDR_ID, VER_ID, CUST_ACPTC_TURNUP_DT,ORDR_DSCNCT_DT,ORDR_BILL_CLEAR_INSTL_DT  
        FROM [COWS].[dbo].[ORDR_MS]om WITH (NOLOCK)  
       WHERE VER_ID = (SELECT MAX(VER_ID) FROM [COWS].[dbo].[ORDR_MS] om1   
           WHERE om1.ORDR_ID = om.ORDR_ID)  
      )oms ON oms.ORDR_ID = ord.ORDR_ID  
    
   LEFT OUTER JOIN (   SELECT	ORDR_ID, oc.ROLE_ID, 
									LTRIM((oc.FRST_NME+ ' ' + oc.LST_NME)) AS NAME,
									LTRIM((CONVERT(varchar, DecryptByKey(csd.FRST_NME)) + ' '+
									CONVERT(varchar, DecryptByKey(csd.LST_NME)))) AS [NAME2]
							FROM [COWS].[dbo].[ORDR_CNTCT] oc WITH (NOLOCK)  
							LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15 
						    WHERE ROLE_ID = 13 
						) tac on ord.ORDR_ID  = tac.ORDR_ID 
		
	LEFT OUTER JOIN ( SELECT	ORDR_ID, oc.ROLE_ID, 
							LTRIM((oc.FRST_NME+ ' ' + oc.LST_NME)) AS NAME,
							LTRIM((CONVERT(varchar, DecryptByKey(csd.FRST_NME)) + ' '+
							CONVERT(varchar, DecryptByKey(csd.LST_NME)))) AS [NAME2]
					FROM [COWS].[dbo].[ORDR_CNTCT] oc WITH (NOLOCK)  
					LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15 
					WHERE ROLE_ID = 11) ram on ord.ORDR_ID  = ram.ORDR_ID 
         
  LEFT OUTER JOIN (SELECT  ASMT_DT, ORDR_ID, ASN_USER_ID   
         FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
          WHERE ASN_USER_ID  = (SELECT MAX(ASN_USER_ID)  
      FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
       FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
      WHERE uas.ORDR_ID = bb.ORDR_ID)  
      AND bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID   
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
     WHERE usr.ORDR_ID  = bb.ORDR_ID )) ur ON ur.ORDR_ID = fsa.ORDR_ID   
    
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] g WITH (NOLOCK) ON ur.ASN_USER_ID = g.USER_ID  
    
  LEFT OUTER JOIN (SELECT ORDR_ID, SOI_CD, CIS_LVL_TYPE  
    FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  WITH (NOLOCK)  
    WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
     FROM (SELECT SOI_CD, ORDR_ID   
      FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi  
     WHERE soi.ORDR_ID = bb.ORDR_ID)   
    )si ON ord.ORDR_ID  = si.ORDR_ID   
    
  /*LEFT OUTER JOIN (SELECT oac.ORDR_ID, oac.CTRY_CD, oac.CIS_LVL_TYPE, oac.REC_STUS_ID, lcc.CTRY_NME   
    FROM [COWS].[dbo].[ORDR_ADR] oac WITH (NOLOCK)  
    INNER JOIN [COWS].[dbo].[LK_CTRY] lcc on lcc.CTRY_CD = oac.CTRY_CD  
     and lcc.REC_STUS_ID = 1)cntry  
   ON cntry.ORDR_ID = si.ORDR_ID and si.CIS_LVL_TYPE = cntry.CIS_LVL_TYPE  
    and cntry.REC_STUS_ID = 1 */ 
  LEFT JOIN   [COWS].[dbo].[LK_CTRY] lcc with (Nolock) ON oa.CTRY_CD=lcc.CTRY_CD AND  lcc.REC_STUS_ID = 1 AND si.CIS_LVL_TYPE = oa.CIS_LVL_TYPE  
       
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] obli WITH (NOLOCK)  
    ON ord.ORDR_ID = obli.ORDR_ID  
    
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR] vnd WITH (NOLOCK) ON ord.ORDR_ID = vnd.ORDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] vf WITH (NOLOCK) ON vnd.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] vl WITH (NOLOCK) ON vf.VNDR_CD = vl.VNDR_CD  
    
  WHERE si.SOI_CD = '805'   
   AND ord.ORDR_STUS_ID in (0,1)
 ORDER BY fsa.FTN  
 END  
  
 IF @Query = 3  
 BEGIN  
  print @startDate   
  print @endDate  
    
  SELECT DISTINCT  
   fsa.FTN            [FTN],  
   CASE  
	WHEN @SecuredUser = 0 AND foc.CUST_NME<>''  THEN coalesce(foc.CUST_NME,'PRIVATE CUSTOMER')
    WHEN @SecuredUser = 0 AND foc.CUST_NME =''  THEN 'PRIVATE CUSTOMER' 
    WHEN @SecuredUser = 0 AND foc.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'  
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),foc.CUST_NME,'')   
   END             [Customer Name],  
   CASE   
    WHEN ISNULL(oms.ORDR_DSCNCT_DT,oms.CUST_ACPTC_TURNUP_DT) IS NULL THEN ''         
    ELSE CONVERT(VARCHAR(10),ISNULL(oms.ORDR_DSCNCT_DT,ISNULL(oms.CUST_ACPTC_TURNUP_DT,'')),101)  
   END             [Implementation Date],  
   --tac.FRST_NME + ' ' + tac.LST_NME     [Implementation Contact],  
   --ram.FRST_NME + ' '  + ram.LST_NME     [RAM], 
   CASE  
	WHEN @SecuredUser = 0 AND LEN(tac.NAME)>1  THEN coalesce(tac.NAME,'')
    WHEN @SecuredUser = 0 AND LEN(tac.NAME)<=2  THEN '' 
    WHEN @SecuredUser = 0 AND tac.NAME IS NULL THEN ''   
    ELSE coalesce(tac.NAME,tac.NAME2,'')    
   END     [Implementation Contact],      
   CASE  
	WHEN @SecuredUser = 0 AND LEN(ram.NAME)>1  THEN coalesce(ram.NAME,'')
    WHEN @SecuredUser = 0 AND LEN(ram.NAME)<=2  THEN '' 
    WHEN @SecuredUser = 0 AND ram.NAME IS NULL THEN ''   
    ELSE coalesce(ram.NAME,ram.NAME2,'')    
   END  [RAM], 
   ISNULL(g.FULL_NME,'')        [CPM Contact],     
   lp.PROD_TYPE_DES         [Product],  
   CONVERT(VARCHAR(10),ISNULL(ord.CUST_CMMT_DT,fsa.CUST_CMMT_DT),101)  
                [Customer Commit Date],  
   rgn.RGN_DES           [xNCI Region],  
   --CONVERT(varchar, DecryptByKey(oa.CTY_NME))   [H5 City], 
   CASE  
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID = 0 AND oa.CTY_NME<>''  THEN coalesce(oa.CTY_NME,'PRIVATE CITY')
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID > 0 AND oa.CTY_NME =''  THEN 'PRIVATE CITY'
    WHEN @SecuredUser = 0 AND ord.CSG_LVL_ID > 0 AND oa.CTY_NME IS NULL  THEN 'PRIVATE CITY'    
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd2.CTY_NME)),oa.CTY_NME,'')  
   END             [H5 City], 
   os.ORDR_STUS_DES         [Order Status],  
   CONVERT(VARCHAR(10),ord.CREAT_DT,101)    [FSA Submit to COWS Date],  
   CASE  
    WHEN obli.MRC_CHG_AMT IS Null THEN ''  
    ELSE CAST (obli.MRC_CHG_AMT AS MONEY)  
   END             [MRC],  
   CASE  
    WHEN obli.NRC_CHG_AMT IS Null THEN ''  
    ELSE CAST (obli.NRC_CHG_AMT AS MONEY)  
   END             [NRC],  
   CASE   
    WHEN oms.ORDR_BILL_CLEAR_INSTL_DT IS NULL THEN ''         
    ELSE CONVERT(VARCHAR(10),oms.ORDR_BILL_CLEAR_INSTL_DT,101)  
   END             [Bill Clear Date],  
   ISNULL(fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES,'')   [Access Speed],  
   lo.ORDR_TYPE_DES         [Order Type],  
   ISNULL(los.ORDR_SUB_TYPE_DES,'')     [Order Sub Type],  
   ISNULL(vl.VNDR_NME,'')        [Vendor],  
   ISNULL(fsa.TSUP_PRS_QOT_NBR,'')      [PRS Quote Number],  
   (SELECT MAX(CREAT_DT) FROM [COWS].[dbo].[ORDR_NTE]  
    where ORDR_ID = ord.ORDR_ID)      [Last Update Date],  
    CASE   
    WHEN fsa.CUST_SIGNED_DT IS NULL THEN ''         
    ELSE CONVERT(VARCHAR(10),fsa.CUST_SIGNED_DT,101)  
   END             [Customer Sign Date],  
   fsa.PRNT_FTN          [Parent FTN],  
   ISNULL(si.SOI_CD,'')        [SOI Code],   
   ISNULL(lcc.CTRY_NME,'')       [Country]  
        
    
  FROM   [COWS].[dbo].[ORDR] ord WITH (NOLOCK)  
  INNER JOIN  [COWS].[dbo].[FSA_ORDR] fsa WITH (NOLOCK) ON ord.ORDR_ID = fsa.ORDR_ID 
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = fsa.ORDR_ID 
  INNER JOIN  [COWS].[dbo].[LK_PROD_TYPE] lp WITH (NOLOCK) ON fsa.PROD_TYPE_CD = lp.FSA_PROD_TYPE_CD  
  INNER JOIN  [COWS].[dbo].[LK_ORDR_TYPE] lo WITH (NOLOCK) ON fsa.ORDR_TYPE_CD = lo.FSA_ORDR_TYPE_CD  
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_SUB_TYPE] los WITH (NOLOCK) ON los.ORDR_SUB_TYPE_CD = fsa.ORDR_SUB_TYPE_CD   
  INNER JOIN  [COWS].[dbo].[LK_ORDR_STUS] os WITH (NOLOCK) ON os.ORDR_STUS_ID = ord.ORDR_STUS_ID  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc WITH (NOLOCK)   
       ON foc.ORDR_ID = ord.ORDR_ID AND foc.CIS_LVL_TYPE IN ('H5','H6','ES')
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=foc.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
  LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] oa WITH (NOLOCK)   
       ON oa.ORDR_ID = ord.ORDR_ID AND oa.CIS_LVL_TYPE IN ('H5','H6','ES')
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=oa.ORDR_ADR_ID  AND csd2.SCRD_OBJ_TYPE_ID=14  
  LEFT OUTER JOIN [COWS].[dbo].[LK_XNCI_RGN] rgn WITH (NOLOCK) ON rgn.RGN_ID = ord.RGN_ID  
    
  LEFT OUTER JOIN (SELECT ORDR_ID, VER_ID, CUST_ACPTC_TURNUP_DT,ORDR_DSCNCT_DT,ORDR_BILL_CLEAR_INSTL_DT  
        FROM [COWS].[dbo].[ORDR_MS]om WITH (NOLOCK)  
       WHERE VER_ID = (SELECT MAX(VER_ID) FROM [COWS].[dbo].[ORDR_MS] om1   
           WHERE om1.ORDR_ID = om.ORDR_ID)  
      )oms ON oms.ORDR_ID = ord.ORDR_ID  
    
   LEFT OUTER JOIN (   SELECT	ORDR_ID, oc.ROLE_ID, 
									LTRIM((oc.FRST_NME+ ' ' + oc.LST_NME)) AS NAME,
									LTRIM((CONVERT(varchar, DecryptByKey(csd.FRST_NME)) + ' '+
									CONVERT(varchar, DecryptByKey(csd.LST_NME)))) AS [NAME2]
							FROM [COWS].[dbo].[ORDR_CNTCT] oc WITH (NOLOCK)  
							LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15 
						    WHERE ROLE_ID = 13 
						) tac on ord.ORDR_ID  = tac.ORDR_ID 
		
	LEFT OUTER JOIN ( SELECT	ORDR_ID, oc.ROLE_ID, 
								LTRIM((oc.FRST_NME+ ' ' + oc.LST_NME)) AS NAME,
								LTRIM((CONVERT(varchar, DecryptByKey(csd.FRST_NME)) + ' '+
								CONVERT(varchar, DecryptByKey(csd.LST_NME)))) AS [NAME2]
						FROM [COWS].[dbo].[ORDR_CNTCT] oc WITH (NOLOCK)  
						LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oc.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15 
						WHERE ROLE_ID = 11) ram on ord.ORDR_ID  = ram.ORDR_ID  
         
  LEFT OUTER JOIN (SELECT  ASMT_DT, ORDR_ID, ASN_USER_ID   
         FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
          WHERE ASN_USER_ID  = (SELECT MAX(ASN_USER_ID)  
      FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
       FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
      WHERE uas.ORDR_ID = bb.ORDR_ID)  
      AND bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID   
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
     WHERE usr.ORDR_ID  = bb.ORDR_ID )) ur ON ur.ORDR_ID = fsa.ORDR_ID   
    
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] g WITH (NOLOCK) ON ur.ASN_USER_ID = g.USER_ID  
    
  LEFT OUTER JOIN (SELECT ORDR_ID, SOI_CD, CIS_LVL_TYPE  
    FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  WITH (NOLOCK)  
    WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
     FROM (SELECT SOI_CD, ORDR_ID   
      FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi  
     WHERE soi.ORDR_ID = bb.ORDR_ID)   
    )si ON ord.ORDR_ID  = si.ORDR_ID   
    
  /*LEFT OUTER JOIN (SELECT oac.ORDR_ID, oac.CTRY_CD, oac.CIS_LVL_TYPE, oac.REC_STUS_ID, lcc.CTRY_NME   
    FROM [COWS].[dbo].[ORDR_ADR] oac WITH (NOLOCK)  
    INNER JOIN [COWS].[dbo].[LK_CTRY] lcc on lcc.CTRY_CD = oac.CTRY_CD  
     and lcc.REC_STUS_ID = 1)cntry  
   ON cntry.ORDR_ID = si.ORDR_ID and si.CIS_LVL_TYPE = cntry.CIS_LVL_TYPE  
    and cntry.REC_STUS_ID = 1  */
  LEFT JOIN   [COWS].[dbo].[LK_CTRY] lcc with (Nolock) ON oa.CTRY_CD=lcc.CTRY_CD AND  lcc.REC_STUS_ID = 1 AND si.CIS_LVL_TYPE = oa.CIS_LVL_TYPE  
      
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] obli WITH (NOLOCK)  
    ON ord.ORDR_ID = obli.ORDR_ID  
    
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_ORDR] vnd WITH (NOLOCK) ON ord.ORDR_ID = vnd.ORDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] vf WITH (NOLOCK) ON vnd.VNDR_FOLDR_ID = vf.VNDR_FOLDR_ID  
  LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] vl WITH (NOLOCK) ON vf.VNDR_CD = vl.VNDR_CD  
    
  WHERE si.SOI_CD = '805'   
   AND ((ord.ORDR_STUS_ID in (2,5)  
   AND(SELECT MAX(CREAT_DT) FROM [COWS].[dbo].[ACT_TASK]  
    WHERE ORDR_ID = ord.ORDR_ID AND TASK_ID = 1001)   
     BETWEEN @startDate AND @endDate)  
   OR (ord.ORDR_STUS_ID = 4 and ord.CREAT_DT BETWEEN @startDate AND @endDate)) 
 ORDER BY fsa.FTN 
 END  
 CLOSE SYMMETRIC KEY FS@K3y   
  
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIMonthlyRptSOI ' + ',' + CAST(@SecuredUser AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc   
 EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  
GO


