USE [COWS_Reporting]
GO
/***************************************************
*  SP: SP_FedMnthInstallFail
*  Create Date: 8/1/2013
*  Author: Frank Luna
* 
*  Description: Daily report for install failures.
*		Query 1 - Monthly report
*		Query 2 - Responsible party Summary by month for year
*		Query 3 - 12 month report
*
*  EXEC SP_FedMnthInstallFail 1,1
* 
*  Modifications:
* -- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
******************************************************/
ALTER PROCEDURE [dbo].[sp_FedlineInstallFail]
		@Secured		INT = 0,
		@query			INT
AS
BEGIN TRY
	SET NOCOUNT ON;
/*		Remove comments for testing	*/
--DECLARE @Secured		INT = 1
--DECLARE @query			INT = 3
DECLARE @startDate		DATE
DECLARE @endDate		DATE

	SET @endDate = GETDATE()	
	SET @startDate = CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@endDate)-1),@endDate),101)

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
/*	 
   Current month
*/

IF @query = 1
BEGIN
SELECT DISTINCT

 CASE 
	WHEN @Secured = 0 AND ev.CSG_LVL_ID >0  AND CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)) is NOT NULL  THEN 'Private Customer'
	ELSE COALESCE(CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)),fd.CUST_NME,'') 
	END [Customer Name]
,fd.EVENT_ID [COWS Event ID]
,fd.FRB_REQ_ID [FRB Order ID]
,fd.DEV_NME [Device ID]
,eh.CREAT_DT [Rework Date]
,ISNULL(CONVERT(VARCHAR(20),frh.NEW_DT,120),'') [New Request Activity Date]
,ISNULL(fd.FRB_INSTL_TYPE_NME,'') [Migration Type]
,lot.PRNT_ORDR_TYPE_DES [Activity Type]
,fef.FAIL_CD_DES [Failure Reason]
,ISNULL(eh.CMNT_TXT,'') [Cause]
,fef.RSPB_PARTY_NME [Responsible Party]

From
[COWS].[dbo].[EVENT] ev 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd WITH (NOLOCK) ON fd.EVENT_ID = ev.EVENT_ID
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=fd.FEDLINE_EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=23 
JOIN	[COWS].[dbo].[FEDLINE_REQ_ACT_HIST] frh WITH (NOLOCK) ON fd.EVENT_ID = frh.EVENT_ID 
JOIN	[COWS].[dbo].[LK_FEDLINE_EVENT_FAIL_CD] fef WITH (NOLOCK) ON frh.FAIL_CD_ID = fef.FAIL_CD_ID 
JOIN	[COWS].[dbo].[EVENT_HIST] eh WITH (NOLOCK) ON frh.EVENT_HIST_ID = eh.EVENT_HIST_ID 
JOIN	[COWS].[dbo].LK_FEDLINE_ORDR_TYPE lot WITH (NOLOCK) ON fd.ORDR_TYPE_CD = lot.ORDR_TYPE_CD
WHERE eh.CREAT_DT BETWEEN @startDate and @endDate 

UNION 
SELECT DISTINCT
  CASE 
	WHEN @Secured = 0 AND ev.CSG_LVL_ID >0  AND CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)) is NOT NULL  THEN 'Private Customer'
	ELSE COALESCE(CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)),fd.CUST_NME,'') 
	END [Customer Name]
,fd.EVENT_ID [COWS Event ID]
,fd.FRB_REQ_ID [FRB Order ID]
,fd.DEV_NME [Device ID]
,ehf.CREAT_DT [Rework Date]
,''[New Request Activity Date]
,ISNULL(fd.FRB_INSTL_TYPE_NME,'') [Migration Type]
,lot.PRNT_ORDR_TYPE_DES [Activity Type]
,fef.FAIL_CD_DES [Failure Reason]
,ISNULL(ehf.CMNT_TXT,'') [Cause]
,fef.RSPB_PARTY_NME [Responsible Party]

FROM
[COWS].[dbo].[EVENT] ev 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd WITH (NOLOCK) ON fd.EVENT_ID = ev.EVENT_ID 
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=fd.FEDLINE_EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=23
JOIN	[COWS].[dbo].[FEDLINE_EVENT_USER_DATA] eud WITH (NOLOCK) ON fd.EVENT_ID = eud.EVENT_ID 
JOIN	[COWS].[dbo].[LK_FEDLINE_EVENT_FAIL_CD]  fef WITH (NOLOCK) ON eud.FAIL_CD_ID = fef.FAIL_CD_ID 
JOIN	[COWS].[dbo].LK_FEDLINE_ORDR_TYPE lot WITH (NOLOCK) ON fd.ORDR_TYPE_CD = lot.ORDR_TYPE_CD
JOIN	(SELECT  eh.EVENT_ID, eh.CMNT_TXT, eh.CREAT_DT 
			FROM [COWS].[dbo].[EVENT_HIST] eh JOIN 
            (SELECT  MAX (EVENT_HIST_ID) AS EVENT_HIST_ID, EVENT_ID AS EVENT_ID
                 FROM [COWS].[dbo].[EVENT_HIST] WHERE ACTN_ID = 45 GROUP BY EVENT_ID) eh1
      ON eh1.event_hist_id = eh.EVENT_HIST_ID and eh1.event_id = eh.EVENT_ID) ehf
ON ehf.EVENT_ID = eud.EVENT_ID                  
WHERE ehf.CREAT_DT BETWEEN @startDate and @endDate ORDER BY fd.EVENT_ID DESC

END
/*
    Responsible party by month for year
*/

IF @query = 2
BEGIN

DECLARE @KeyParty TABLE
(
EVENT_ID	VARCHAR(15),
CREAT_DT	DATE,
KeyParty	VARCHAR(10)
)
CREATE TABLE #TempTBL
(
KeyParty		VARCHAR(10),
MnthName		VARCHAR(25),
Cnt				INT
)

DECLARE @cntr		INT
DECLARE @MthCnt		INT
DECLARE @MthStart	DATE
DECLARE @MthEnd		DATE
DECLARE @SrtStrg	VARCHAR(3)
DECLARE @srtCntr	INT
DECLARE @MnthName	VARCHAR(10)
DECLARE @MnthYear	VARCHAR(4)
DECLARE @CycleDate	VARCHAR(15)	

SET @cntr = 1
SET @MthCnt = 0
SET @srtCntr = 1
WHILE @cntr <= 12
BEGIN
 SELECT @SrtStrg =  RIGHT(REPLICATE('0', 3) + CONVERT(NVARCHAR(3),@srtCntr),3)
  

SET @MthStart=DATEADD(MONTH,@MthCnt,@startDate)
SET @MnthName = DATENAME(MONTH,@MthStart)
SET @MnthYear = DATENAME(YEAR,@MthStart)
SET @CycleDate = @MnthName + @MnthYear


INSERT INTO @KeyParty(EVENT_ID,CREAT_DT,KeyParty)
SELECT DISTINCT
fd.EVENT_ID
,eh.CREAT_DT
,fef.RSPB_PARTY_NME
FROM
[COWS].[dbo].[EVENT] ev 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd WITH (NOLOCK) ON fd.EVENT_ID = ev.EVENT_ID 
JOIN	[COWS].[dbo].[FEDLINE_REQ_ACT_HIST] frh WITH (NOLOCK) ON fd.EVENT_ID = frh.EVENT_ID 
JOIN	[COWS].[dbo].[LK_FEDLINE_EVENT_FAIL_CD] fef WITH (NOLOCK) ON frh.FAIL_CD_ID = fef.FAIL_CD_ID 
JOIN	[COWS].[dbo].[EVENT_HIST] eh WITH (NOLOCK) ON frh.EVENT_HIST_ID = eh.EVENT_HIST_ID 

WHERE DATENAME(MONTH,eh.CREAT_DT)+DATENAME(YEAR,eh.CREAT_DT)= @CycleDate GROUP BY fd.EVENT_ID,EH.CREAT_DT,fef.RSPB_PARTY_NME
UNION 
SELECT DISTINCT
fd.EVENT_ID
,ehf.CREAT_DT 
,fef.RSPB_PARTY_NME
FROM
[COWS].[dbo].[EVENT] ev 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd WITH (NOLOCK) ON fd.EVENT_ID = ev.EVENT_ID 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_USER_DATA] eud WITH (NOLOCK) ON fd.EVENT_ID = eud.EVENT_ID 
JOIN	[COWS].[dbo].[LK_FEDLINE_EVENT_FAIL_CD]  fef WITH (NOLOCK) ON eud.FAIL_CD_ID = fef.FAIL_CD_ID 
JOIN	(SELECT  eh.EVENT_ID, eh.CMNT_TXT, eh.CREAT_DT 
			FROM [COWS].[dbo].[EVENT_HIST] eh JOIN 
            (SELECT  MAX (EVENT_HIST_ID) AS EVENT_HIST_ID, EVENT_ID AS EVENT_ID
                 FROM [COWS].[dbo].[EVENT_HIST] WHERE ACTN_ID = 45 GROUP BY EVENT_ID) eh1
      ON eh1.event_hist_id = eh.EVENT_HIST_ID and eh1.event_id = eh.EVENT_ID) ehf
ON ehf.EVENT_ID = eud.EVENT_ID                  
WHERE DATENAME(MONTH,ehf.CREAT_DT)+DATENAME(YEAR,ehf.CREAT_DT)= @CycleDate GROUP BY fd.EVENT_ID,EHF.CREAT_DT,fef.RSPB_PARTY_NME
 
SELECT @SrtStrg =  RIGHT(REPLICATE('0', 3) + CONVERT(NVARCHAR(3),@srtCntr),3)	
IF (SELECT COUNT(*) FROM @KeyParty) > 0
BEGIN
 
INSERT INTO #TempTBL(KeyParty,MnthName,Cnt)
SELECT DISTINCT
CASE	
	WHEN KeyParty = 'Fedline' THEN 'Fedline' 
	WHEN KeyParty = 'Sprint' THEN 'Sprint'
END 
,CASE 
	WHEN KeyParty = 'Fedline' THEN 'Order' + @SrtStrg + LEFT(@MnthName,3) + ' ' + DATENAME(YY,@MthStart) 
	WHEN KeyParty = 'Sprint' THEN 'Order' + @SrtStrg + LEFT(@MnthName,3) + ' ' + DATENAME(YY,@MthStart)
END 
,CASE 
	WHEN KeyParty = 'Fedline' THEN COUNT(KeyParty)
	WHEN KeyParty = 'Sprint' THEN COUNT(KeyParty)
	END 
	FROM @KeyParty GROUP BY KeyParty
END

IF (SELECT COUNT(*) FROM @KeyParty WHERE KeyParty = 'Fedline' ) = 0 
BEGIN
	INSERT INTO #TempTBL(KeyParty,MnthName,Cnt)
	VALUES ('Fedline','Order' + @SrtStrg + LEFT(@MnthName,3) + ' ' + DATENAME(YY,@MthStart),0)	
END
IF (SELECT COUNT(*) FROM @KeyParty WHERE KeyParty = 'Sprint' ) = 0 
BEGIN
	INSERT INTO #TempTBL(KeyParty,MnthName,Cnt)
	VALUES ('Sprint','Order' + @SrtStrg + LEFT(@MnthName,3) + ' ' + DATENAME(YY,@MthStart),0)	
END

DELETE FROM @KeyParty
SET @cntr = @cntr + 1
SET @MthCnt = @MthCnt -	1
SET @srtCntr = @srtCntr + 1	

END
DECLARE @cols NVARCHAR(2000) 
DECLARE @DynamicQuery NVARCHAR(4000) 

SELECT  @cols = STUFF(( SELECT DISTINCT ',' + QuoteName([MnthName])
						FROM    #TempTBL
                        FOR XML PATH('') ), 1, 1,'')

/*
	Each Month Name has 'ORDERXX-' that was needed for sorting, remove that string now that sorting is complete.
*/
BEGIN
    WHILE @cols LIKE '%ORDER%'
    BEGIN
        SET @cols = REPLACE(@cols,SUBSTRING(@cols,PATINDEX('%ORDER%',@cols),8),'')
    END
END

UPDATE #TempTBL SET MnthName = REPLACE(MnthName,SUBSTRING(MnthName,PATINDEX('%ORDER%',MnthName),8),'')
SET @DynamicQuery = 'SELECT KeyParty[Responsible Party], '+ @cols +'  FROM 
(SELECT KeyParty, Cnt, MnthName FROM #TempTBL) base
 PIVOT (MAX(Cnt) FOR MnthName IN ('+ @cols +')) AS pvt ORDER BY KeyParty' 


EXECUTE(@DynamicQuery)

DROP TABLE #TempTBL
 
END

/*
	 12 months
*/
IF @query = 3
BEGIN

SET @startDate=DATEADD(MONTH,-12,@startDate)

SELECT DISTINCT
 CASE 
	WHEN @Secured = 0 AND ev.CSG_LVL_ID >0  AND CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)) is NOT NULL  THEN 'Private Customer'
	ELSE COALESCE(CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)),fd.CUST_NME,'') 
	END [Customer Name]
,fd.EVENT_ID [COWS Event ID]
,fd.FRB_REQ_ID [FRB Order ID]
,fd.DEV_NME [Device ID]
,eh.CREAT_DT [Rework Date]
,ISNULL(CONVERT(VARCHAR(20),frh.NEW_DT,120),'') [New Request Activity Date]
,ISNULL(fd.FRB_INSTL_TYPE_NME,'') [Migration Type]
,lot.PRNT_ORDR_TYPE_DES [Activity TYPE]
,fef.FAIL_CD_DES [Failure Reason]
,ISNULL(eh.CMNT_TXT,'') [Cause]
,fef.RSPB_PARTY_NME [Responsible Party]

From
[COWS].[dbo].[EVENT] ev 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd WITH (NOLOCK) ON fd.EVENT_ID = ev.EVENT_ID 
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=fd.FEDLINE_EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=23
JOIN	[COWS].[dbo].[FEDLINE_REQ_ACT_HIST] frh WITH (NOLOCK) ON fd.EVENT_ID = frh.EVENT_ID 
JOIN	[COWS].[dbo].[LK_FEDLINE_EVENT_FAIL_CD] fef WITH (NOLOCK) ON frh.FAIL_CD_ID = fef.FAIL_CD_ID 
JOIN	[COWS].[dbo].[EVENT_HIST] eh WITH (NOLOCK) ON frh.EVENT_HIST_ID = eh.EVENT_HIST_ID 
JOIN	[COWS].[dbo].LK_FEDLINE_ORDR_TYPE lot WITH (NOLOCK) ON fd.ORDR_TYPE_CD = lot.ORDR_TYPE_CD
WHERE eh.CREAT_DT > @startDate

UNION 
SELECT DISTINCT
 CASE 
	WHEN @Secured = 0 AND ev.CSG_LVL_ID >0  AND CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)) is NOT NULL  THEN 'Private Customer'
	ELSE COALESCE(CONVERT(VARCHAR(100), DecryptByKey(csd.CUST_NME)),fd.CUST_NME,'') 
	END [Customer Name]
,fd.EVENT_ID [COWS Event ID]
,fd.FRB_REQ_ID [FRB Order ID]
,fd.DEV_NME [Device ID]
,ehf.CREAT_DT [Rework Date]
,''[New Request Activity Date]
,ISNULL(fd.FRB_INSTL_TYPE_NME,'') [Migration Type]
,lot.PRNT_ORDR_TYPE_DES [Activity Type]
,fef.FAIL_CD_DES [Failure Reason]
,ISNULL(ehf.CMNT_TXT,'') [Cause]
,fef.RSPB_PARTY_NME [Responsible Party]

FROM
[COWS].[dbo].[EVENT] ev 
JOIN	[COWS].[dbo].[FEDLINE_EVENT_TADPOLE_DATA] fd WITH (NOLOCK) ON fd.EVENT_ID = ev.EVENT_ID 
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=fd.FEDLINE_EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=23
JOIN	[COWS].[dbo].[FEDLINE_EVENT_USER_DATA] eud WITH (NOLOCK) ON fd.EVENT_ID = eud.EVENT_ID 
JOIN	[COWS].[dbo].[LK_FEDLINE_EVENT_FAIL_CD]  fef WITH (NOLOCK) ON eud.FAIL_CD_ID = fef.FAIL_CD_ID 
JOIN	[COWS].[dbo].LK_FEDLINE_ORDR_TYPE lot WITH (NOLOCK) ON fd.ORDR_TYPE_CD = lot.ORDR_TYPE_CD
JOIN	(SELECT  eh.EVENT_ID, eh.CMNT_TXT, eh.CREAT_DT 
			FROM [COWS].[dbo].[EVENT_HIST] eh JOIN 
            (SELECT  MAX (EVENT_HIST_ID) AS EVENT_HIST_ID, EVENT_ID AS EVENT_ID
                 FROM [COWS].[dbo].[EVENT_HIST] WHERE ACTN_ID = 45 GROUP BY EVENT_ID) eh1
      ON eh1.event_hist_id = eh.EVENT_HIST_ID and eh1.event_id = eh.EVENT_ID) ehf
ON ehf.EVENT_ID = eud.EVENT_ID                  
WHERE ehf.CREAT_DT > @startDate ORDER BY fd.EVENT_ID DESC
 
END
 
CLOSE SYMMETRIC KEY FS@K3y 	
		
	RETURN 0;
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.SP_FedlineInstallFail ' 
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null
END CATCH


