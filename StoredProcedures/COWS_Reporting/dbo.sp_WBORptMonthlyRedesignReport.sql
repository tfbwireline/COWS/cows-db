USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_WBORptMonthlyRedesignReport]    Script Date: 10/31/2018 20:23:32 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_WBORptMonthlyRedesignReport]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_WBORptMonthlyRedesignReport]
GO

USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_WBORptMonthlyRedesignReport]    Script Date: 10/31/2018 20:23:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/*
--=======================================================================================
-- Project:			PJ017930  
-- Author:			Md Mahmudur Rahman Monir
-- Date:			07/27/2016
-- Description:		
--					Extract WBO monthly Redesign Report Device that are closed for the Earleir Month 
-- Updated          08302016
-- Description:     [DEV_CMPLTN_CD]=1   /* Added based on 08302016 Meeting*/
-- Updated:			Md Mahmudur Rahman Monir
-- Date:			07/27/2016
-- Description:	    CASE WHEN p.H1_ID is NULL Then 'MMRNT2'	ELSE  '100035547'   END  AS 'BEC'   /* Vn370313   as per pramods instruction */
-- Description:     [DEV_CMPLTN_CD]=1   /* Added based on 08302016 Meeting*/
-- Updated:			Md Mahmudur Rahman Monir
-- Date:			03/14/2017
-- Description:	    Lot of Changes
-- Updated:			Md Mahmudur Rahman Monir
-- Date:			05/02/2017
-- Description:	    for DATA  [BILL_OVRRDN_CD] if 1 cost will be [BILL_OVRRDN_AMT] irrespective of all other rule
-- Updated:			Md Mahmudur Rahman Monir
-- Date:			07/05/2017
-- Description:	    H1   that exist in  dbo.REDSGN_H1_MRC_VALUES    table need to show rest wont be shown
-- Date:            03062018
-- Description:     Please update MRC BEC to 100035527 --- Digit in red changed from a 4 to a 2.
-- [dbo].[sp_WBORptMonthlyRedesignReport] 1
-- Updated:			Md Mahmudur Rahman Monir
-- Date:			04/30/2018
-- Description:     New Requirement from Pramod
-- Updated:			Md Mahmudur Rahman Monir
-- Date:			10/31/2018
-- Description:     PM_VOICE_OT_CHARGE_RATE
--=======================================================================================
*/
CREATE PROCEDURE [dbo].[sp_WBORptMonthlyRedesignReport]
  	@Secured			INT=0,
  	@inStartDtStr		VARCHAR(10)='',
  	@inEndDtStr			VARCHAR(10)=''
AS

BEGIN TRY
BEGIN
	SET NOCOUNT ON;

	--[dbo].[sp_WBORptMonthlyRedesignReport] 1
    --DECLARE @Secured    INT=0,
  	--@inStartDtStr		VARCHAR(10)='',
  	--@inEndDtStr			VARCHAR(10)=''

    DECLARE @startDate		Datetime
	DECLARE @endDate		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)	
	
	IF @inStartDtStr <> '' AND @inEndDtStr <> ''
  		BEGIN

			SET @startDate=CONVERT(datetime, @inStartDtStr, 101)
			SET @endDate=CONVERT(datetime, @inEndDtStr, 101)			
			
			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			SET @endDate=DATEADD(day, 1, @endDate)  		
  		
  		END
  	ELSE
  		BEGIN
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month   Check ENCIMonthlyReport
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
			SET @startDate=cast(@startDtStr AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			
			--set @endDate = DATEADD(MONTH,1,@endDate)  -- Remove Later
			--Print @startDate   -- Remove Later
			--print @endDate  -- Remove Later
		END

OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
/*
----SELECT 
----		'Y'								[Pass Through?]	
----		,'O'							[H1 Account Number]	
----		,'Conditional Optional'			[H4 Account Number]	
----		,'Conditional Optional'			[H6 Account Number]	
----		,'R'			[Qty]	
----		,'R'			[Install Date]	
----		,'R'			[Activation Date]	
----		,'Opt for MRC only'			[Disconnect Date]	
----		,'O'			[Reference Key]	
----		,'R'			[Charge Amount]	
----		,'R'			[BEC]	
----		,'Optional - Informational only to WBO; not uploaded into the system'			[BEC Name]	
----		,'If blank, use CPC or PC verbiage'			[Invoice Text Line 1]	
----		,'If blank, use CPC'			[Invoice Text Line 2]	
----		,'If blank, use CPC'			[Invoice Text Line 3]	
----		,'If not blank, use user entered text'			[Invoice Text Line 4]	
----		,'If not blank, use user entered text'			[Invoice Text Line 5]	
----		,'O'			[Charge Start Date]
----		,'O'			[Charge Stop Date]	
----		,'O - If Blank, then create a new Billing Item.  If Not blank, then update the existing billing item.'			[Customer Billing Item ID]
----UNION ALL
*/
SELECT 
		'Y'			[Pass Through?]
		,H1+''		[H1 Account Number]
		,''			[H4 Account Number]
		,H6+''		[H6 Account Number]
		,'1'		[Qty]
		,CloseDate	[Install Date]
		,CloseDate	[Activation Date]
		,''			[Disconnect Date]
		,RedesignID+'' [Reference Key]
		/*
		,CASE	WHEN K.MRC_RATE_VS_NRC IS NULL THEN CONVERT ( VARCHAR (18), CONVERT(DECIMAL(16,2),COST))
				WHEN K.MRC_RATE_VS_NRC IS NOT NULL THEN 
				CASE  WHEN A.BILL_OVRRDN_CD=1 THEN CONVERT ( VARCHAR (18),CONVERT(DECIMAL(16,2),COST) ) 
					  ELSE CONVERT ( VARCHAR (18),CONVERT(DECIMAL(16,2),COST * K.MRC_RATE_VS_NRC))  END
		        END	[Charge Amount]
		 */
		,CONVERT ( VARCHAR (18), CONVERT(DECIMAL(16,2),COST))  AS [Charge Amount]
		,BEC+''		[BEC]   
		,''			[BEC Name]
		,OdieSite	[Invoice Text Line 1]
		,''			[Invoice Text Line 2]	
		,''			[Invoice Text Line 3]		
		,''			[Invoice Text Line 4]		
		,''			[Invoice Text Line 5]	
		,''			[Charge Start Date]	
		,''			[Charge Stop Date]	
		,''			[Customer Billing Item ID]

		
FROM
(
Select	DISTINCT
		 r.REDSGN_NBR AS 'RedesignID' 
		,lrt.REDSGN_TYPE_NME 'RedesignType'
		,isnull(CONVERT(VARCHAR(10),rdi.DEV_BILL_DT, 101),'') AS 'CloseDate'
		,CASE WHEN p.H1_ID is NULL Then '' ELSE isnull(CONVERT(VARCHAR(10),DATEADD(month,35,rdi.DEV_BILL_DT), 101),'')  END AS 'CancelDate'   /* Redesign MRC : REDSGN_H1_MRC_VALUES  */
		/*
		,CASE WHEN lrt.REDSGN_CAT_ID=3 /*SECURITY*/   THEN ISNULL(CONVERT(VARCHAR,r.COST_AMT),'') 
		      WHEN lrt.REDSGN_CAT_ID=1 /*VOICE*/   THEN CASE WHEN rdi.NTE_CHRG_CD >= 0 THEN 
						(CONVERT(DECIMAL(16,2),(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='NTE_VOICE_CHARGE_RATE'))/60) *ISNULL(r.[NTE_LVL_Effort_AMT],0) 
						+(CONVERT(DECIMAL(16,2),(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='PM_VOICE_CHARGE_RATE'))/60) *ISNULL(r.[PM_LVL_EFFORT_AMT],0) 
						+(CONVERT(DECIMAL(16,2),(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='NE_VOICE_CHARGE_RATE'))/60) *ISNULL(r.[NE_LVL_EFFORT_AMT],0) 
						+(CONVERT(DECIMAL(16,2),(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='NTE_VOICE_OT_CHARGE_RATE'))/60) *ISNULL(r.[NTE_OT_LVL_EFFORT_AMT],0) 
						+(CONVERT(DECIMAL(16,2),(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='PM_VOICE_OT_CHARGE_RATE'))/60) *ISNULL(r.[PM_OT_LVL_EFFORT_AMT],0) 
						+(CONVERT(DECIMAL(16,2),(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='NE_VOICE_OT_CHARGE_RATE'))/60) *ISNULL(r.[NE_OT_LVL_EFFORT_AMT],0) 
						+(CONVERT(DECIMAL(16,2),(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='SPS_VOICE_CHARGE_RATE'))/60) *ISNULL(r.[SPS_LVL_EFFORT_AMT],0) 
						+(CONVERT(DECIMAL(16,2),(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='SPS_VOICE_OT_CHARGE_RATE'))/60) *ISNULL(r.[SPS_OT_LVL_EFFORT_AMT],0)
				
				END

			  WHEN lrt.REDSGN_CAT_ID=2 /*DATA*/ THEN 
				   CASE  WHEN 	r.BILL_OVRRDN_CD	=1  THEN  
						 CASE WHEN rdi.NTE_CHRG_CD = 1  THEN  [BILL_OVRRDN_AMT]  
						      WHEN rdi.NTE_CHRG_CD = 0  THEN  0
				              ELSE    0 END
				   ELSE
						CASE  WHEN rdi.NTE_CHRG_CD = 1 THEN  
						(CONVERT(DECIMAL(16,2),(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='NTE_CHARGE_RATE' ))/60 
						*ISNULL(r.NTE_LVL_Effort_AMT,0))  + 
						(SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='SDE_CHARGE_RATE' )
						ELSE (SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='SDE_CHARGE_RATE' )
						END
			  END
			  Else 0 END 
			  AS 'COST'*/
		,r.COST_AMT AS 'COST'
		,rdi.NTE_CHRG_CD
		,lrt.REDSGN_CAT_ID
		,CASE WHEN p.H1_ID is NULL Then 'MMRNT2'	ELSE  '100035527'   END  AS 'BEC'   /* Vn370313   as per pramods instruction */
		,rdi.DEV_NME AS 'OdieSite'	
		,ISNULL(r.H1_CD,'') AS 'H1'
		,REPLACE(ISNULL(rdi.H6_CUST_ID,''),N',','  ') AS 'H6'
		,'QDA'  AS 'System'  
		,r.BILL_OVRRDN_CD
 From COWS.dbo.REDSGN r with (nolock)
	    INNER JOIN COWS.dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK)  on rdi.REDSGN_ID = r.REDSGN_ID
		INNER  JOIN COWS.dbo.REDSGN_H1_MRC_VALUES P  with (nolock)  ON r.H1_CD=p.H1_ID AND P.REDSGN_CAT_ID=R.REDSGN_CAT_ID  AND P.REC_STUS_ID = 1  /* 07052017 Change H1 must exist in REDSGN_H1_MRC_VALUES*/
		LEFT  JOIN COWS.dbo.LK_REDSGN_TYPE lrt      with (nolock)  on r.REDSGN_TYPE_ID = lrt.REDSGN_TYPE_ID
		LEFT  JOIN COWS.dbo.REDSGN_CUST_BYPASS RCB  with (nolock)  on r.H1_CD=RCB.H1_CD
		/*LEFT  JOIN COWS.dbo.REDSGN_H1_MRC_VALUES P  with (nolock)  ON r.H1_CD=p.H1_ID AND P.REDSGN_CAT_ID=R.REDSGN_CAT_ID  AND P.REC_STUS_ID = 1*/  /* New Vn370313 */ 
				
 WHERE rdi.DEV_BILL_DT  IS NOT NULL
       AND [DEV_CMPLTN_CD]=1   /* Added based on 08302016 Meeting*/
       AND lrt.IS_BLBL_CD =1
	   AND lrt.RecStus_ID =1
	   AND RCB.H1_CD IS NULL
	   AND rdi.DEV_BILL_DT >= @startdate and rdi.DEV_BILL_DT < @enddate 
)  A   LEFT JOIN    COWS.dbo.REDSGN_H1_MRC_VALUES  K with (nolock) ON 
   A.H1=H1_ID AND K.REDSGN_CAT_ID=A.REDSGN_CAT_ID  AND REC_STUS_ID = 1
 --ORDER BY r.REDSGN_NBR ASC
WHERE A.COST > 0
				
CLOSE SYMMETRIC KEY FS@K3y; 
RETURN 0;
END
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_WBO_Monthly_Redsgn_Report '  + ', ' 
	SET @Desc=@Desc + CAST(@Secured AS VARCHAR(2)) + ': '
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;   --- Open later during deployment
	RETURN 1;
END CATCH				



/*
Select * From dbo.REDSGN  where REDSGN_NBR='R00050542'
Select * from dbo.REDSGN_DEVICES_INFO where RedSGN_ID=965
Select * from dbo.LK_REDSGN_TYPE  where REDSGN_TYPE_ID=15
Select * from REDSGN_CUST_BYPASS where H1_cd='921427171'

Select	DISTINCT
		r.REDSGN_NBR AS 'Redesign ID' 
		--,CASE  WHEN r.SCURD_CD= 1 and @Secured = 1  THEN 'Private Customer' ELSE dbo.decryptBinaryData(r.CUST_NME) END AS 'Customer'
		,lrt.REDSGN_TYPE_NME 'Redesign Type'
		,isnull(CONVERT(VARCHAR(10),rdi.DEV_BILL_DT, 101),'') AS 'Close Date'
		,CASE WHEN p.H1_ID is NULL Then '' ELSE isnull(CONVERT(VARCHAR(10),DATEADD(month,35,rdi.DEV_BILL_DT), 101),'')  END AS 'Cancel Date'   /* Redesign MRC : REDSGN_H1_MRC_VALUES  */
		,CASE WHEN lrt.REDSGN_CAT_ID=1 /*VOICE*/   THEN ISNULL(CONVERT(VARCHAR,r.COST_AMT),'') 
		      WHEN lrt.REDSGN_CAT_ID=2 /*DATA*/    THEN CASE WHEN rdi.NTE_CHRG_CD = 1 THEN (SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='NTE_CHARGE_RATE' )*ISNULL(r.NTE_LVL_Effort_AMT,0) + (SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='PM_CHARGE_RATE' )
												   ELSE (SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='PM_CHARGE_RATE' ) END
			  WHEN lrt.REDSGN_CAT_ID=3 /*SECUITY*/ THEN CASE WHEN rdi.NTE_CHRG_CD = 1 THEN (SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='NTE_CHARGE_RATE' )*ISNULL(r.NTE_LVL_Effort_AMT,0) + (SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='PM_CHARGE_RATE' )
												   ELSE (SELECT ISNULL(PRMTR_VALU_TXT,0) FROM COWS.dbo.Lk_SYS_CFG with (nolock) WHERE PRMTR_NME='PM_CHARGE_RATE' ) END
			  Else 0 END 
			  AS 'COST'
		--,lrt.REDSGN_CAT_ID	  --- 1=voice 2=Data, 3=Security
		,CASE WHEN p.H1_ID is NULL Then 'MMRNT2'	ELSE  '100035527'   END  AS 'BEC'   /* Vn370313   as per pramods instruction */
		---,'MMRNT2'	 AS 'BEC'
		,rdi.DEV_NME AS 'Odie Site'	
		,ISNULL(r.H1_CD,'') AS 'H1'
		,REPLACE(ISNULL(rdi.H6_CUST_ID,''),N',','  ') AS 'H6'
		,'QDA'  AS 'System'  
		
	From COWS.dbo.REDSGN r with (nolock)
	    INNER JOIN COWS.dbo.REDSGN_DEVICES_INFO rdi WITH (NOLOCK)  on rdi.REDSGN_ID = r.REDSGN_ID
		LEFT  JOIN COWS.dbo.LK_REDSGN_TYPE lrt      with (nolock)  on r.REDSGN_TYPE_ID = lrt.REDSGN_TYPE_ID
		LEFT  JOIN COWS.dbo.REDSGN_H1_MRC_VALUES P  with (nolock)  ON r.H1_CD=p.H1_ID
		LEFT  JOIN COWS.dbo.REDSGN_CUST_BYPASS RCB  with (nolock)  on r.H1_CD=RCB.H1_CD
				
 WHERE rdi.DEV_BILL_DT  IS NOT NULL
       AND [DEV_CMPLTN_CD]=1   /* Added based on 08302016 Meeting*/
       AND lrt.IS_BLBL_CD =1
	   AND lrt.RecStus_ID =1
	   AND RCB.H1_CD IS NULL
	   AND rdi.DEV_BILL_DT >= @startdate and rdi.DEV_BILL_DT < @enddate 


ORDER BY r.REDSGN_NBR ASC
*/



GO


