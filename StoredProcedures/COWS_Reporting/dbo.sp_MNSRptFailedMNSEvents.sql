USE [COWS_Reporting]
GO
/****** Object:  StoredProcedure [dbo].[sp_MNSRptFailedMNSEvents]    Script Date: 11/21/2019 9:29:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
--=======================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			06/21/2011
-- Description:		
--					Extract MDS & MDS Fasttrack events that have been rescheduled.
--					QueryNumber: 1 = Daily
--								 2 = Weekly
--								 3 = Monthly
--								 0 = Specify start date & end date: 'mm/dd/yyyy'			
--
-- Note:			MDS Activity Type of Disconnect DOES NOT have FSA_MDS_EVENT record.
--
-- Modifications:
--
-- 01/24/2012		sxv0766: Specified VARCHAR size of 100 for Cust Name decryption,
--							 Use Yesterday date for the daily report.
-- 02/16/2012		sxv0766: Added ACTN ID from 31 to 43
-- 03/29/2012		sxv0766: IM952822-Removed resulting string expression from Date And Time column
-- 10/07/2014		ci554013: Added "and cmnt_txt is not null" to subqueries on line 136 and 193.
--								and changed case in select to "evhi.CMNT_TXT 'Comment'"
-- 11/23/2016		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked
----EXEC COWS_Reporting.dbo.sp_MNSRptFailedMNSEvents 0,0,'01/01/2014','11/11/2016'
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD					
--=======================================================================================
*/
ALTER PROCEDURE [dbo].[sp_MNSRptFailedMNSEvents] 
	@QueryNumber		INT,	
	@Secured			INT=0,
	@inStartDtStr		VARCHAR(10)='',
	@inEndDtStr			VARCHAR(10)=''	
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDate		Datetime
	DECLARE @endDate		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)
	DECLARE @dayOfWeek		INT

	IF @QueryNumber=0
		BEGIN

			SET @startDate=CONVERT(datetime, @inStartDtStr, 101)
			SET @endDate=CONVERT(datetime, @inEndDtStr, 101)			
			
			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			SET @endDate=DATEADD(day, 1, @endDate)
			
		END
	ELSE IF @QueryNumber=1
    	BEGIN    	
    		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
		
			--SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			--SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)   

			-- Today's report shows a list of events failed yesterday 
			SET @startDate=cast(CONVERT(varchar(8), DATEADD(day, -1, GETDATE()), 112) AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)			
			
		END
	ELSE IF @QueryNumber=2
		BEGIN
			-- e.g. Run date is '2011-06-21' 3rd day of the week
			-- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000
			
			SET @today=GETDATE()
			SET @dayOfWeek=DATEPART(dw, @today)
			--SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+6), @today), 112) As DateTime)
			--SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-1), @today), 112) AS Datetime)			
			SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)			
							
		END
	ELSE IF @QueryNumber=3
		BEGIN			 
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month 
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
			SET @startDate=cast(@startDtStr AS datetime)
			SET @endDate=cast((cast(SUBSTRING(CONVERT(VARCHAR(10), getdate(), 101),1,2) + '/01/' + CAST(YEAR(getdate()) AS VARCHAR(4)) as datetime) -1) AS datetime)  
		END
		
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SELECT	mds.EVENT_ID 'Event ID',
			actn.ACTN_DES 'Action',
			CASE mds.MDS_FAST_TRK_CD
				WHEN 1 THEN 'MDS Fast Track'
				WHEN 0 THEN 'MDS' END 'Event',
			evhi.CREAT_DT 'Date And Time',	
			--REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',
			cntEvnt.totCnt 'Count Of Event ID',
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name',						
			CASE
				WHEN luser.DSPL_NME is NULL THEN ''
				ELSE luser.DSPL_NME END 'Assigned Acct MNS PM',
			luser1.DSPL_NME 'Author',
			CASE
				WHEN evhi.MODFD_BY_USER_ID is NULL THEN ''
				ELSE luser2.DSPL_NME END 'Editor',
			CASE
				WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
				ELSE luser3.DSPL_NME END 'Performed By',			
			evhi.CMNT_TXT 'Comment',
			CASE
				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
				ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',
			CASE
				WHEN lfac.FAIL_ACTY_ID is NULL THEN ''
				ELSE lfac.FAIL_ACTY_DES END 'Failed Activities',
			''                                   AS [S/C Flag], 
			'MDS Only'			AS [Network Activity Type]						
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN ( SELECT EVENT_ID, COUNT(EVENT_ID) 'totCnt'
					FROM COWS.dbo.EVENT_HIST with (nolock)
			   		WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43 AND cmnt_txt IS NOT NULL)  -- Reschedule action
			   		GROUP BY EVENT_ID)as cntEvnt ON mds.EVENT_ID = cntEvnt.EVENT_ID	
		LEFT JOIN ( SELECT EVENT_HIST_ID, EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43) AND cmnt_txt IS NOT NULL) as evhi ON mds.EVENT_ID = evhi.EVENT_ID			   	
		LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID = luser.USER_ADID			
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.MODFD_BY_USER_ID = luser2.USER_ID											
		-- Performed By
		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID		
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID	
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID
		LEFT JOIN COWS.dbo.EVENT_FAIL_ACTY evfa with (nolock) ON evhi.EVENT_HIST_ID = evfa.EVENT_HIST_ID
		LEFT JOIN COWS.dbo.LK_FAIL_ACTY lfac with (nolock) ON evfa.FAIL_ACTY_ID = lfac.FAIL_ACTY_ID 
	
	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000	
		
	WHERE mds.MDS_ACTY_TYPE_ID in (1, 2) AND mds.EVENT_STUS_ID in (3) -- AND evhi.FAIL_REAS_ID is NOT NULL
			AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate 
	
	UNION ALL
	
 	SELECT	mds.EVENT_ID 'Event ID',
 			actn.ACTN_DES 'Action',
 			CASE mds.MDS_FAST_TRK_CD
 				WHEN 1 THEN 'MDS Fast Track'
 				WHEN 0 THEN 'MDS' END 'Event',
			evhi.CREAT_DT 'Date And Time', 				
 			--REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',
 			cntEvnt.totCnt 'Count Of Event ID',
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name', 			
 			CASE
 				WHEN luser.DSPL_NME is NULL THEN ''
 				ELSE luser.DSPL_NME END 'Assigned Acct MNS PM',
 			luser1.DSPL_NME 'author',
 			CASE
 				WHEN evhi.MODFD_BY_USER_ID is NULL THEN ''
 				ELSE luser2.DSPL_NME END 'Editor',
 			CASE
 				WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
 				ELSE luser3.DSPL_NME END 'Performed By', 			
 			evhi.CMNT_TXT 'Comment',
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',
 			CASE
 				WHEN lfac.FAIL_ACTY_ID is NULL THEN ''
 				ELSE lfac.FAIL_ACTY_DES END 'Failed Activities',
			''                                   AS [S/C Flag], 
			'MDS Only'			AS [Network Activity Type]		 			
 	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)
 		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID 					
		LEFT JOIN ( SELECT EVENT_ID, COUNT(EVENT_ID) 'totCnt'
					FROM COWS.dbo.EVENT_HIST with (nolock)
			   		WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43 AND cmnt_txt IS NOT NULL)  -- Reschedule action
			   		GROUP BY EVENT_ID)as cntEvnt ON mds.EVENT_ID = cntEvnt.EVENT_ID 
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8			   	
		LEFT JOIN ( SELECT EVENT_HIST_ID, EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43) AND cmnt_txt IS NOT NULL)as evhi ON mds.EVENT_ID = evhi.EVENT_ID
 		LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID = luser.USER_ADID			
 		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
 		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.MODFD_BY_USER_ID = luser2.USER_ID 		
 		-- Performed By 			
 		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID 		
 		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID	
 		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID
 		LEFT JOIN COWS.dbo.EVENT_FAIL_ACTY evfa with (nolock) ON evhi.EVENT_HIST_ID = evfa.EVENT_HIST_ID
 		LEFT JOIN COWS.dbo.LK_FAIL_ACTY lfac with (nolock) ON evfa.FAIL_ACTY_ID = lfac.FAIL_ACTY_ID 
 	
 	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000	
 	-- EVENT_STUS_ID: 3 = Rework	
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (3) --AND evhi.FAIL_REAS_ID is NOT NULL
		AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate

	/*New MDS EVENT Start*/
	UNION ALL

	SELECT	mds.EVENT_ID 'Event ID',
			actn.ACTN_DES 'Action',
			CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END 'Event',
			evhi.CREAT_DT 'Date And Time',	
			--REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',
			cntEvnt.totCnt 'Count Of Event ID',
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name',						
			CASE
				WHEN luser.DSPL_NME is NULL THEN ''
				ELSE luser.DSPL_NME END 'Assigned Acct MNS PM',
			luser1.DSPL_NME 'Author',
			CASE
				WHEN evhi.MODFD_BY_USER_ID is NULL THEN ''
				ELSE luser2.DSPL_NME END 'Editor',
			CASE
				WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
				ELSE luser3.DSPL_NME END 'Performed By',			
			evhi.CMNT_TXT 'Comment',
			CASE
				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
				ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',
			CASE
				WHEN lfac.FAIL_ACTY_ID is NULL THEN ''
				ELSE lfac.FAIL_ACTY_DES END 'Failed Activities',
			CASE	
						WHEN eod.SC_CD = 'C'        THEN 'Complex'
						WHEN eod.SC_CD = 'S'        THEN 'Simple'
					    ELSE ''
						END                                     AS [S/C Flag],
			lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type]					
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = mds.NTWK_ACTY_TYPE_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
		LEFT JOIN ( SELECT EVENT_ID, COUNT(EVENT_ID) 'totCnt'
					FROM COWS.dbo.EVENT_HIST with (nolock)
			   		WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43 AND cmnt_txt IS NOT NULL)  -- Reschedule action
			   		GROUP BY EVENT_ID)as cntEvnt ON mds.EVENT_ID = cntEvnt.EVENT_ID	
		LEFT JOIN ( SELECT EVENT_HIST_ID, EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43) AND cmnt_txt IS NOT NULL) as evhi ON mds.EVENT_ID = evhi.EVENT_ID			   	
		LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID = luser.USER_ADID			
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.MODFD_BY_USER_ID = luser2.USER_ID											
		-- Performed By
		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID		
		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID	
		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID
		LEFT JOIN COWS.dbo.EVENT_FAIL_ACTY evfa with (nolock) ON evhi.EVENT_HIST_ID = evfa.EVENT_HIST_ID
		LEFT JOIN COWS.dbo.LK_FAIL_ACTY lfac with (nolock) ON evfa.FAIL_ACTY_ID = lfac.FAIL_ACTY_ID 
		LEFT OUTER JOIN [COWS].[dbo].[MDS_EVENT_ODIE_DEV] eod WITH (NOLOCK) ON eod.EVENT_ID = mds.EVENT_ID

	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000	
		
	WHERE mds.MDS_ACTY_TYPE_ID <> 3 AND ((mds.EVENT_STUS_ID in (3, 6)) OR ((mds.EVENT_STUS_ID = 4) AND (mds.WRKFLW_STUS_ID = 4)))  AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate
	
	UNION ALL
	
 	SELECT	mds.EVENT_ID 'Event ID',
 			actn.ACTN_DES 'Action',
 			CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END 'Event',
			evhi.CREAT_DT 'Date And Time', 				
 			--REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) 'Date And Time',
 			cntEvnt.totCnt 'Count Of Event ID',
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name', 			
 			CASE
 				WHEN luser.DSPL_NME is NULL THEN ''
 				ELSE luser.DSPL_NME END 'Assigned Acct MNS PM',
 			luser1.DSPL_NME 'author',
 			CASE
 				WHEN evhi.MODFD_BY_USER_ID is NULL THEN ''
 				ELSE luser2.DSPL_NME END 'Editor',
 			CASE
 				WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
 				ELSE luser3.DSPL_NME END 'Performed By', 			
 			evhi.CMNT_TXT 'Comment',
 			CASE
 				WHEN evhi.FAIL_REAS_ID is NULL THEN ''
 				ELSE lfrs.FAIL_REAS_DES END 'Fail Codes',
 			CASE
 				WHEN lfac.FAIL_ACTY_ID is NULL THEN ''
 				ELSE lfac.FAIL_ACTY_DES END 'Failed Activities',
			''                                     AS [S/C Flag],
			lmat.NTWK_ACTY_TYPE_DES			AS [Network Activity Type]		
 			
 	FROM	COWS.dbo.MDS_EVENT mds with (nolock)
 		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID 
		INNER JOIN [COWS].[dbo].[LK_MDS_NTWK_ACTY_TYPE] lmat WITH (NOLOCK) ON lmat.NTWK_ACTY_TYPE_ID = mds.NTWK_ACTY_TYPE_ID
 		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7					
		LEFT JOIN ( SELECT EVENT_ID, COUNT(EVENT_ID) 'totCnt'
					FROM COWS.dbo.EVENT_HIST with (nolock)
			   		WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43 AND cmnt_txt IS NOT NULL)  -- Reschedule action
			   		GROUP BY EVENT_ID)as cntEvnt ON mds.EVENT_ID = cntEvnt.EVENT_ID 			   	
		LEFT JOIN ( SELECT EVENT_HIST_ID, EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, FAIL_REAS_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43) AND cmnt_txt IS NOT NULL)as evhi ON mds.EVENT_ID = evhi.EVENT_ID
 		LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID = luser.USER_ADID			
 		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 
 		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.MODFD_BY_USER_ID = luser2.USER_ID 		
 		-- Performed By 			
 		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID 		
 		LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID	
 		LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID
 		LEFT JOIN COWS.dbo.EVENT_FAIL_ACTY evfa with (nolock) ON evhi.EVENT_HIST_ID = evfa.EVENT_HIST_ID
 		LEFT JOIN COWS.dbo.LK_FAIL_ACTY lfac with (nolock) ON evfa.FAIL_ACTY_ID = lfac.FAIL_ACTY_ID 
 	
 	-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-14 00:00:00.000 and STRT_TMST < 2011-06-15 00:00:00.000	
 	-- EVENT_STUS_ID: 3 = Rework	
	WHERE mds.MDS_ACTY_TYPE_ID = 3 AND ((mds.EVENT_STUS_ID in (3, 6)) OR ((mds.EVENT_STUS_ID = 4) AND (mds.WRKFLW_STUS_ID = 4)))  AND evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate	

	ORDER By mds.EVENT_ID
	/*New MDS EVENT Ends */

	CLOSE SYMMETRIC KEY FS@K3y; 
	
	RETURN 0;
			
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptFailedMNSEvents ' + CAST(@QueryNumber AS VARCHAR(4)) 
	SET @Desc=@Desc + ',' + CAST(@Secured AS VARCHAR(2)) + ': '	
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH