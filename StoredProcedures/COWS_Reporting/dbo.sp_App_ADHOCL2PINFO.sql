/*  
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD  
-- Update date: <06/06/2018>
-- Updated by: Md M Monir  
-- Description: <To get H5 Data>  CTY_NME 
*/  
ALTER Proc [dbo].[sp_App_ADHOCL2PINFO]  
as  
-- Note: orderID in all order tables is the same from ORDR table except CKT table where need to get CKTID from Orderid  
BEGIN  
-- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
   
OPEN SYMMETRIC KEY FS@K3y   
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
    -- Insert statements for procedure here  
      
select distinct ordr.ORDR_ID,  
ordr.ORDR_CAT_ID,   
ordr.RGN_ID,  
  
--IPL Order related Fields  
 d.CKT_TYPE_CD,  
d.H5_CUST_ACCT_NBR,  
d.CUST_CNTRC_SIGN_DT ,  
a.FTN,  
a.RELTD_FTN,  
a.TTRPT_JACK_NRFC_TYPE_CD,  
b.FSA_ORDR_TYPE_DES,  
osubTyp.ORDR_SUB_TYPE_DES, -- new field ORDR SUB TYP   
a.FSA_ORGNL_INSTL_ORDR_ID, -- new field OriginalInstallOrderID   
c.FSA_PROD_TYPE_DES,  
a.FSA_EXP_TYPE_CD,  
a.TSUP_RFQ_NBR,  
a.CUST_WANT_DT,  
a.CUST_CMMT_DT,  
a.TTRPT_ACCS_CNTRC_TERM_CD,  
a.TPORT_CNCTR_TYPE_ID,  
a.TSUP_PRS_QOT_NBR,  
a.CPE_ACCS_PRVDR_CD,  
a.TTRPT_NCDE_DES , --Encoding  
a.TTRPT_FRMNG_DES [TTRPT_FRMNG_DES], --Framing  
a.TTRPT_ACCS_TYPE_DES, -- Access Type Description  
bl.MRC_CHG_AMT,  
bl.NRC_CHG_AMT,  
(cast(bl.MRC_CHG_AMT as money) + cktc.ACCS_CUST_MRC_AMT) as TOTAL_CUST_BILL,  
--os.REC_STUS_DES,  
os.ORDR_STUS_DES,  
--FSA ORDR CUST  
i.SALS_PERSN_PRIM_CID,  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN i.CUST_NME   
  WHEN ordr.CSG_LVL_ID > 0 AND i.CUST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND i.CUST_NME2 IS NULL THEN NULL END [CUST_NME] ,  
  
  
a.TTRPT_SPD_OF_SRVC_BDWD_DES,  
  
---- NRM_CKT fields  
n.PLN_NME,  
n.PLN_SEQ_NBR,  
n.FMS_CKT_ID,  
n.NUA_ADR,  
n.TOC_ADR,  
  
--NRM_VNDR  
nv.LEC_ID,  
  
--TRPT_ORDR fields  
t.CMNT_TXT,  
u.FULL_NME ,  
m.CCD_MISSD_REAS_DES,  
t.ORDR_ASN_VW_DT  ,  
t.CUST_ACPTD_DT ,  
--axm3320: 1/30/2012 fix for Bill Clear Date;Data pulled from ORDR_MS..ORDR_BILL_CLEAR_INSTL_DT  
--t.BILL_CLEAR_DT,  
o.ORDR_BILL_CLEAR_INSTL_DT,  
t.CKT_LGTH_IN_KMS_QTY,  
t.ACCS_IN_MB_VALU_QTY,  
t.A_END_NEW_CKT_CD,  
t.A_END_SPCL_CUST_ACCS_DETL_TXT,  
xncirgn.RGN_DES as RGN_DES ,  
t.B_END_NEW_CKT_CD,  
t.B_END_SPCL_CUST_ACCS_DETL_TXT,   
t.IPT_AVLBLTY_ID,              
cu.CUR_NME,  
t.VNDR_LOCAL_ACCS_PRVDR_GRP_NME,  
vt.VNDR_TYPE_DES,  
s.SALS_CHNL_DES,  
  
--ORDR_MS Related Fields  
o.PRE_SBMT_DT,  
--o.ACK_BY_VNDR_DT,  
--o.SENT_TO_VNDR_DT,  
o.VLDTD_DT,  
--Commented as field missing  
oh.HOLD_DT as ORDR_HOLD_DT,  
  
--CKT_MS fields  
cm.ACCS_ACPTC_DT,  
cm.ACCS_DLVRY_DT,  
cm.CNTRC_TERM_ID,  
cm.TRGT_DLVRY_DT,  
cm.TRGT_DLVRY_DT_RECV_DT,  
cm.VNDR_CNFRM_DSCNCT_DT,  
  
--ORDR_ADR fields  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.BLDG_NME   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.BLDG_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.BLDG_NME2 IS NULL THEN NULL END [BLDG_NME] ,  
  
ad.CTRY_CD AS [CTRY_CD] ,  
  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.CTY_NME   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.CTY_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.CTY_NME2 IS NULL THEN NULL END [CTY_NME] ,  
  
-- ad.FLR_ID,  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.FLR_ID   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.FLR_ID2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.FLR_ID2 IS NULL THEN NULL END [FLR_ID] ,  
  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.PRVN_NME   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.PRVN_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.PRVN_NME2 IS NULL THEN NULL END [PRVN_NME] ,  
  
CASE  
  WHEN ordr.CSG_LVL_ID = 0 THEN ad.RM_NBR   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.RM_NBR2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
  WHEN ordr.CSG_LVL_ID > 0 AND ad.RM_NBR2 IS NULL THEN NULL END [RM_NBR] ,  
    
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN ad.STREET_ADR_1   
WHEN ordr.CSG_LVL_ID > 0 AND ad.STREET_ADR_12 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND ad.STREET_ADR_12 IS NULL THEN NULL END [STREET_ADR_1] ,  
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN ad.STT_CD   
WHEN ordr.CSG_LVL_ID > 0 AND ad.STT_CD2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND ad.STT_CD2 IS NULL THEN NULL END [STT_CD] ,  
    
    
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN ad.ZIP_PSTL_CD   
WHEN ordr.CSG_LVL_ID > 0 AND ad.ZIP_PSTL_CD2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND ad.ZIP_PSTL_CD2 IS NULL THEN NULL END [ZIP_PSTL_CD] ,  
  
--H5/H6 Cntry State code  
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN adh.STT_CD   
WHEN ordr.CSG_LVL_ID > 0 AND adh.STT_CD2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND adh.STT_CD2 IS NULL THEN NULL END [H5_STT_CD] ,  
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN ad.CTRY_NME   
WHEN ordr.CSG_LVL_ID > 0 AND ad.CTRY_NME  IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND ad.CTRY_NME  IS NULL THEN NULL END [H5_CTRY_CD] ,  
  
  
--H5/H6 Cntry State code  
  
  
-- ORDR_CNTCT  
oc.PHN_NBR [CNTCT_PHN_NBR],  
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN oc.FRST_NME   
WHEN ordr.CSG_LVL_ID > 0 AND oc.FRST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND oc.FRST_NME2 IS NULL THEN NULL END [CNTCT_FRST_NME],  
    
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN oc.LST_NME   
WHEN ordr.CSG_LVL_ID > 0 AND oc.LST_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND oc.LST_NME2 IS NULL THEN NULL END [CNTCT_LST_NME],  
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN oc.EMAIL_ADR   
WHEN ordr.CSG_LVL_ID > 0 AND oc.EMAIL_ADR2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND oc.EMAIL_ADR2 IS NULL THEN NULL END [CNTCT_EMAIL_ADR],  
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN oc.CNTCT_NME   
WHEN ordr.CSG_LVL_ID > 0 AND oc.CNTCT_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND oc.CNTCT_NME2 IS NULL THEN NULL END [CNTCT_NME],  
   
  
--OnSite ORDR_CNTC  
osoc.PHN_NBR [OnSite_PHN_NBR],   
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN osoc.OS_EMAIL_ADR    
WHEN ordr.CSG_LVL_ID > 0 AND osoc.OS_EMAIL_ADR2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND osoc.OS_EMAIL_ADR2 IS NULL THEN NULL END [OnSite_CNTCT_EMAIL_ADR],  
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN osoc.OS_CNTCT_NME    
WHEN ordr.CSG_LVL_ID > 0 AND osoc.OS_CNTCT_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND osoc.OS_CNTCT_NME2 IS NULL THEN NULL END [OnSite_CNTCT_NME],  
  
--Alternate ORDR_CNTCT  
aloc.PHN_NBR [AL_PHN_NBR],   
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN aloc.AL_EMAIL_ADR     
WHEN ordr.CSG_LVL_ID > 0 AND aloc.AL_EMAIL_ADR2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND aloc.AL_EMAIL_ADR2 IS NULL THEN NULL END [AL_EMAIL_ADR],  
  
CASE  
WHEN ordr.CSG_LVL_ID = 0 THEN aloc.AL_CNTCT_NME     
WHEN ordr.CSG_LVL_ID > 0 AND aloc.AL_CNTCT_NME2 IS NOT NULL THEN 'PRIVATE CUSTOMER'   
WHEN ordr.CSG_LVL_ID > 0 AND aloc.AL_CNTCT_NME2 IS NULL THEN NULL END [AL_CNTCT_NME],  
  
  
f.VNDR_CD ,  
  
e.VNDR_ORDR_ID ,  
  
  
  
sp.NEW_PORT_SPEED_DES,  
  
  
cktc.ACCS_CUST_MRC_AMT,  
  
cktc.VNDR_MRC_IN_USD_AMT,  
cktc.VNDR_NRC_IN_USD_AMT,  
cktc.ASR_MRC_IN_USD_AMT,  
cktc.ASR_NRC_IN_USD_AMT,  
cktc.ACCS_CUST_MRC_IN_USD_AMT,  
cktc.ACCS_CUST_NRC_IN_USD_AMT,  
cktc.ACCS_CUST_NRC_AMT,  
cktc.ASR_MRC_ATY,  
cktc.QOT_NBR,  
cktc.VNDR_CUR_ID,   
ckt.VNDR_CNTRC_TERM_END_DT ,  
ckt.VNDR_CKT_ID,  
ckt.PL_SEQ_NBR, --New Field Private Line (populated by GOM)  
(cktc.VNDR_MRC_IN_USD_AMT + cktc.VNDR_NRC_IN_USD_AMT + cktc.ASR_MRC_IN_USD_AMT + cktc.ASR_NRC_IN_USD_AMT + cktc.ACCS_CUST_MRC_IN_USD_AMT + cktc.ACCS_CUST_NRC_IN_USD_AMT) as COSTTYPE,  
  
h.H1_CUST_ID,  
--ORDR_VLAN   
  
vl.VLAN_ID,  
vl.VLAN_PCT_QTY,  
a.SPA_ACCS_INDCR_DES,  
a.TPORT_BRSTBL_USAGE_TYPE_CD,  
a.TPORT_ETHRNT_NRFC_INDCR,  
a.TPORT_CUST_ROUTR_TAG_TXT,  
a.TPORT_CUST_ROUTR_AUTO_NEGOT_CD,  
acs.ACCS_CTY_NME_SITE_CD,  
  
--BEGIN: CR 135 Fields  
  
--BEGIN: CR 135 Fields  
----BEGIN: MPLS Fields  
     mpls.EVENT_ID ,   
 UPPER(mpls.EVENT_STUS_DES) [EVENT_STUS_DES],         
 CASE WHEN mpls.EVENT_TITLE_TXT is NULL THEN '' ELSE mpls.EVENT_TITLE_TXT END [EVENT_TITLE_TXT],   
 --CASE WHEN mpls.CUST_NME is NULL THEN '' ELSE mpls.CUST_NME END  [MCUST_NME] ,  
 mpls.MCUST_NME [MCUST_NME],  
  
 CASE WHEN mpls.DES_CMNT_TXT is NULL THEN '' ELSE mpls.DES_CMNT_TXT END [DES_CMNT_TXT],  
   
 mpls.MPLS_EVENT_TYPE_DES ,   
 mpls.MPLS_ACTY_TYPE_DES ,   
 mpls.VPN_PLTFRM_TYPE_DES ,  
 mpls.VAS_TYPE_DES ,  
 mpls.IP_VER_ID ,  
    
 mpls.STRT_TMST [STRT_TMST],  
   
 mpls.EVENT_DRTN_IN_MIN_QTY ,  
   
 mpls.EXTRA_DRTN_TME_AMT ,     
     mpls.End_Time  [End_Time],  
     
 --mpls.DSPL_NME [DSPL_NME],  
   
 mpls.MPLS_ACTN_DES,   
           
 --mpls.WRKFLW_STUS_DES ,  
   
 mpls.MPLS_MOD_DT [MPLS_MOD_DT],  
      
 mpls.MPLS_MOD_BY [MPLS_MOD_BY],  
       
 CASE WHEN mpls.SUCSS_ACTY_DES is NULL THEN '' ELSE mpls.SUCSS_ACTY_DES END [SUCSS_ACTY_DES],  
     
 CASE WHEN mpls.FAIL_ACTY_DES is NULL THEN '' ELSE mpls.FAIL_ACTY_DES END [FAIL_ACTY_DES],  
   
 mpls.MPLS_CMNT_TXT [MPLS_CMNT_TXT],  
  --END MPLS Fields  
    
  ----BEGIN AD EVENT Fields  
    
 adev.AD_Event_ID  [AD_Event_ID],   
 UPPER(adev.AD_Event_Status) [AD_Event_Status],         
 CASE WHEN adev.AD_Item_Title is NULL THEN '' ELSE adev.AD_Item_Title END [AD_Item_Title],   
 CASE WHEN adev.AD_Cust_Name is NULL THEN '' ELSE adev.AD_Cust_Name END [AD_Cust_Name],  
 CASE WHEN adev.AD_DES_CMNT_TXT is NULL THEN '' ELSE adev.AD_DES_CMNT_TXT END [AD_DES_CMNT_TXT],  
 adev.AD_Type [AD_Type],   
 adev.AD_CKT_ID [AD_CKT_ID],  
 adev.AD_IP_VER_ID  [AD_IP_VER_ID],  
 adev.AD_STRT_TMST [AD_STRT_TMST],  
 adev.AD_EVENT_DRTN_IN_MIN_QTY [AD_EVENT_DRTN_IN_MIN_QTY],  
 adev.AD_EXTRA_DRTN_TME_AMT [AD_EXTRA_DRTN_TME_AMT],    
 adev.AD_END_TMST [AD_END_TMST],  
 --adev.AD_DSPL_NME [AD_DSPL_NME],  
 --adev.AD_WRKFLW_STUS_DES [AD_WRKFLW_STUS_DES],  
 adev.AD_ACTN_DES [AD_ACTN_DES] ,  
 adev.AD_MOD_DT  [AD_MOD_DT],   
 adev.AD_MOD_BY [AD_MOD_BY],      
 CASE WHEN adev.AD_SUCSS_ACTY_DES is NULL THEN '' ELSE adev.AD_SUCSS_ACTY_DES END [AD_SUCSS_ACTY_DES],    
 CASE WHEN adev.AD_FAIL_ACTY_DES is NULL THEN '' ELSE adev.AD_FAIL_ACTY_DES END [AD_FAIL_ACTY_DES],   
 CASE WHEN adev.AD_CMNT_TXT is NULL THEN '' ELSE adev.AD_CMNT_TXT END [AD_CMNT_TXT] ,  
  ----END AD EVENT FIelds  
  
 ----BEGIN SPLK EVENT Fields  
    splk.SPLK_Event_ID [SPLK_Event_ID],  
    UPPER(splk.SPLK_Event_Status) [SPLK_Event_Status],   
   CASE WHEN splk.SPLK_Item_Title is NULL THEN '' ELSE splk.SPLK_Item_Title END [SPLK_Item_Title],   
   CASE WHEN splk.splk_Cust_Name is NULL THEN '' ELSE splk.splk_Cust_Name END [SPLK_Cust_Name],     
   CASE WHEN splk.SPLK_DES_CMNT_TXT is NULL THEN '' ELSE splk.SPLK_DES_CMNT_TXT END [SPLK_DES_CMNT_TXT],  
 splk.SPLK_EVENT_TYPE_DES [SPLK_EVENT_TYPE_DES],  
 splk.SPLK_ACTY_TYPE_DES  [SPLK_ACTY_TYPE_DES],  
 splk.SPLK_IP_VER_ID [SPLK_IP_VER_ID],  
    splk.SPLK_STRT_TMST [SPLK_STRT_TMST],   
    splk.splk_EVENT_DRTN_IN_MIN_QTY [SPLK_EVENT_DRTN_IN_MIN_QTY],  
    splk.SPLK_EXTRA_DRTN_TME_AMT [SPLK_EXTRA_DRTN_TME_AMT],   
    splk.SPLK_END_TMST [SPLK_END_TMST],  
   -- splk.SPLK_DSPL_NME [SPLK_DSPL_NME],  
    --splk.SPLK_WRKFLW_STUS_DES [SPLK_WRKFLW_STUS_DES],  
    splk.SPLK_ACTN_DES [SPLK_ACTN_DES],  
    splk.SPLK_MOD_DT  [SPLK_MOD_DT],   
 splk.SPLK_MOD_BY [SPLK_MOD_BY],      
 CASE WHEN splk.SPLK_SUCSS_ACTY_DES is NULL THEN '' ELSE splk.SPLK_SUCSS_ACTY_DES END [SPLK_SUCSS_ACTY_DES],    
 CASE WHEN splk.SPLK_FAIL_ACTY_DES is NULL THEN '' ELSE splk.SPLK_FAIL_ACTY_DES END [SPLK_FAIL_ACTY_DES],   
 CASE WHEN splk.SPLK_CMNT_TXT is NULL THEN '' ELSE splk.SPLK_CMNT_TXT END [SPLK_CMNT_TXT]   
   
 ----END SPLK EVENT FIelds  
   
 --GOM fields  
 ,fsagom.ASMT_DT,  
 lv.VNDR_NME ,  
 fsagom.ORDR_SBMT_DT   
    
   --gom fields  
      
from COWS.dbo.ORDR ordr with (nolock)   
  
-- FSA Related Fields  
  
--left outer join COWS.dbo.FSA_ORDR a with(nolock) on a.ORDR_ID = ordr.ORDR_ID   
   
left outer join (SELECT   
  bb.FTN,  
  bb.RELTD_FTN,  
  bb.TTRPT_JACK_NRFC_TYPE_CD,  
  bb.FSA_ORGNL_INSTL_ORDR_ID, -- new field OriginalInstallOrderID   
  bb.FSA_EXP_TYPE_CD,  
  bb.TSUP_RFQ_NBR,  
  bb.CUST_WANT_DT,  
  bb.CUST_CMMT_DT,  
  bb.TTRPT_ACCS_CNTRC_TERM_CD,  
  bb.TPORT_CNCTR_TYPE_ID,  
  bb.TSUP_PRS_QOT_NBR,  
  bb.CPE_ACCS_PRVDR_CD,  
  bb.TTRPT_NCDE_DES , --Encoding  
  bb.TTRPT_FRMNG_DES [TTRPT_FRMNG_DES], --Framing  
  bb.TTRPT_ACCS_TYPE_DES, -- Access Type Description  
  bb.ORDR_ID,  
  bb.PROD_TYPE_CD,  
  bb.TPORT_BRSTBL_USAGE_TYPE_CD,  
  bb.ORDR_SUB_TYPE_CD,  
  bb.INSTL_VNDR_CD,  
  bb.ORDR_TYPE_CD,  
  fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES,  
  fcpl.SPA_ACCS_INDCR_DES,  
  fcpl.TPORT_ETHRNT_NRFC_INDCR,  
  fcpl.TPORT_CUST_ROUTR_TAG_TXT,  
  fcpl.TPORT_CUST_ROUTR_AUTO_NEGOT_CD  
  FROM [COWS].[dbo].[FSA_ORDR] bb with (nolock) LEFT OUTER JOIN  
  [COWS].[dbo].[ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = bb.ORDR_ID  
  WHERE bb.ORDR_ACTN_ID = (SELECT MAX(ORDR_ACTN_ID)  
  FROM (SELECT ORDR_ACTN_ID, FTN FROM [COWS].[dbo].[FSA_ORDR] with (nolock) ) fsa   
  WHERE fsa.FTN = bb.FTN)) a on a.ORDR_ID = ordr.ORDR_ID  
  
left outer join COWS.dbo.LK_VNDR lv with(nolock) on a.INSTL_VNDR_CD  = lv.VNDR_CD   
  
--left outer join COWS.dbo.LK_REC_STUS os with(nolock) on os.REC_STUS_ID = ordr.REC_STUS_ID  
  
  
--axm3320 1/23 fix for Region  
left outer join COWS.dbo.LK_XNCI_RGN xncirgn with(nolock) on ordr.RGN_ID = xncirgn.RGN_ID  
  
left outer join COWS.dbo.LK_ORDR_STUS os with(nolock) on os.ORDR_STUS_ID  = ordr.ORDR_STUS_ID   
  
left outer join COWS.dbo.LK_FSA_ORDR_TYPE b with(nolock) on b.FSA_ORDR_TYPE_CD = a.ORDR_TYPE_CD  
  
left outer join COWS.dbo.LK_ORDR_SUB_TYPE osubTyp with(nolock) on osubTyp.ORDR_SUB_TYPE_CD = a.ORDR_SUB_TYPE_CD   
  
left outer join COWS.dbo.LK_FSA_PROD_TYPE c with(nolock) on c.FSA_PROD_TYPE_CD = a.PROD_TYPE_CD  
  
  
left outer join (SELECT bli.* FROM COWS.dbo.FSA_ORDR_BILL_LINE_ITEM bli with (nolock) LEFT OUTER JOIN  
[COWS].[dbo].[ORDR] cc with (nolock) on bli.ORDR_ID = cc.ORDR_ID    
WHERE bli.FSA_ORDR_BILL_LINE_ITEM_ID  = (SELECT MAX(FSA_ORDR_BILL_LINE_ITEM_ID)  
FROM (SELECT FSA_ORDR_BILL_LINE_ITEM_ID,ORDR_ID FROM COWS.dbo.FSA_ORDR_BILL_LINE_ITEM il with (nolock) where il.MRC_CHG_AMT <> '' and il.NRC_CHG_AMT <> '') i   
WHERE i.ORDR_ID = bli.ORDR_ID)) bl on bl.ORDR_ID = ordr.ORDR_ID  
  
  
  
LEFT OUTER JOIN (SELECT bb.ORDR_ID,bb.SALS_PERSN_PRIM_CID, bb.SOI_CD,  
 CONVERT(varchar, DecryptByKey(csd.CUST_NME)) [CUST_NME2], bb.CUST_NME   
FROM [COWS].[dbo].[FSA_ORDR_CUST] bb with (nolock) LEFT OUTER JOIN  
[COWS].[dbo].[FSA_ORDR] cc with (nolock) on bb.ORDR_ID = cc.ORDR_ID   
LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5  
WHERE bb.CIS_LVL_TYPE = 'H5') i on i.ORDR_ID = a.ORDR_ID   
  
left outer join (
SELECT  DISTINCT 
nn.ftn,
nn.PLN_NME,  
nn.PLN_SEQ_NBR,  
nn.FMS_CKT_ID,  
nn.NUA_ADR,  
CASE WHEN (o.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd.[SITE_ADR]) ELSE nn.[TOC_ADR]  END AS [TOC_ADR]----nn.TOC_ADR

FROM [COWS].[dbo].[NRM_CKT] nn with (nolock)  
LEFT OUTER JOIN COWS.dbo.[CUST_SCRD_DATA] csd WITH (NOLOCK) ON csd.[SCRD_OBJ_ID]=nn.[NRM_CKT_ID] and csd.[SCRD_OBJ_TYPE_ID]=29
LEFT OUTER JOIN COWS.DBO.FSA_ORDR f WITH(Nolock) ON f.FTN=nn.ftn
LEFT JOIN COWS.dbo.ORDR O WITH (NOLOCK) ON O.ORDR_ID=f.ORDR_ID
WHERE nn.NRM_CKT_ID  = (SELECT MAX(NRM_CKT_ID)  
FROM (SELECT NRM_CKT_ID, FTN FROM [COWS].[dbo].[NRM_CKT] with (nolock) ) nrm   
WHERE nrm.FTN = nn.FTN)) n on n.FTN = a.FTN   
    
--SPLK_EVENT_ACCS  
 left outer join (SELECT se.NEW_PORT_SPEED_DES,sl.FTN FROM [COWS].[dbo].[SPLK_EVENT_ACCS] se with (nolock)  
        join [COWS].[dbo].[SPLK_EVENT] sl with (nolock) on sl.EVENT_ID = se.EVENT_ID where sl.EVENT_STUS_ID not in (8) ) sp   
  on sp.FTN = a.FTN   
    
-- NRM_VNDR  
  
left outer join (SELECT nvn.* FROM [COWS].[dbo].[NRM_VNDR] nvn with (nolock)  
WHERE nvn.NMS_VNDR_ID  = (SELECT MAX(NMS_VNDR_ID)  
FROM (SELECT NMS_VNDR_ID, FTN FROM [COWS].[dbo].[NRM_VNDR] with (nolock) ) nrm   
WHERE nrm.FTN = nvn.FTN)) nv on nv.FTN = a.FTN   
    
  
-- IPLOrdr related fields  
  
left outer join COWS.dbo.IPL_ORDR d with (nolock) on d.ORDR_ID = ordr.ORDR_ID   
-- 2 records returned for each orderid  
  
 --TRPT_ORDR fields   
 left outer join COWS.dbo.TRPT_ORDR t with(nolock) on t.ORDR_ID = ordr.ORDR_ID   
 left outer join COWS.dbo.LK_USER u with (nolock) on u.USER_ID = t.XNCI_ASN_MGR_ID   
 left outer join COWS.dbo.LK_CCD_MISSD_REAS m with (nolock) on m.CCD_MISSD_REAS_ID  = t.CCD_MISSD_REAS_ID   
 left outer join COWS.dbo.LK_CUR cu with (nolock) on cu.CUR_ID = t.CUST_BILL_CUR_ID   
 left outer join COWS.dbo.LK_VNDR_TYPE vt with (nolock) on vt.VNDR_TYPE_ID  = t.VNDR_TYPE_ID   
 left outer join COWS.dbo.LK_SALS_CHNL s with (nolock) on s.SALS_CHNL_ID = t.SALS_CHNL_ID   
  left outer join COWS.dbo.LK_ACCS_CTY_SITE acs with (nolock) on acs.ACCS_CTY_SITE_ID  = t.ACCS_CTY_SITE_ID   
  
 --ORDR_MS fields -- multiple rows returned**  
   
 left outer join (SELECT oo.* FROM [COWS].[dbo].[ORDR_MS] oo with (nolock) LEFT OUTER JOIN  
  [COWS].[dbo].[ORDR] cc with (nolock) on oo.ORDR_ID = cc.ORDR_ID    
  WHERE oo.VER_ID = (SELECT MAX(VER_ID)  
  FROM (SELECT VER_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_MS] with (nolock) ) oms   
  WHERE oms.ORDR_ID = oo.ORDR_ID)) o on o.ORDR_ID = ordr.ORDR_ID  
   
-- ORDR_HOLD_MS  
   
--left outer join COWS.dbo.ORDR_HOLD_MS oh with (nolock) on oh.ORDR_ID = ordr.ORDR_ID  
  
left outer join (SELECT ohm.* FROM [COWS].[dbo].[ORDR_HOLD_MS] ohm with (nolock) LEFT OUTER JOIN  
  [COWS].[dbo].[ORDR] cc with (nolock) on ohm.ORDR_ID = cc.ORDR_ID    
  WHERE ohm.VER_ID = (SELECT MAX(VER_ID)  
  FROM (SELECT VER_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_HOLD_MS] with (nolock) ) oms   
  WHERE oms.ORDR_ID = ohm.ORDR_ID)) oh on oh.ORDR_ID = ordr.ORDR_ID  
   
    
 --CKT, CKT_MS Fields   
 --axm3320 1/26 change   
   
 left outer join (SELECT ck.* FROM [COWS].[dbo].[CKT] ck with (nolock)   
  WHERE ck.CKT_ID  = (SELECT MAX(CKT_ID)  
  FROM (SELECT CKT_ID,ORDR_ID FROM [COWS].[dbo].[CKT] with (nolock) ) ock   
  WHERE ock.ORDR_ID = ck.ORDR_ID)) ckt on ckt.ORDR_ID = ordr.ORDR_ID  
    
 left outer join (SELECT ckm.* FROM [COWS].[dbo].[CKT_MS] ckm  with (nolock) LEFT OUTER JOIN  
  [COWS].[dbo].[CKT] cc with (nolock) on ckm.CKT_ID = cc.CKT_ID    
  WHERE ckm.VER_ID  = (SELECT MAX(VER_ID)  
  FROM (SELECT VER_ID,CKT_ID FROM [COWS].[dbo].[CKT_MS] with (nolock) ) ock   
  WHERE ock.CKT_ID = ckm.CKT_ID)) cm on cm.CKT_ID  = ckt.CKT_ID   
  
-- CKT_COST fields  
  
left outer join (SELECT ckto.* FROM [COWS].[dbo].[CKT_COST] ckto with (nolock) LEFT OUTER JOIN  
  [COWS].[dbo].[CKT] cc with (nolock) on ckto.CKT_ID = cc.CKT_ID    
  WHERE ckto.VER_ID  = (SELECT MAX(VER_ID)  
  FROM (SELECT VER_ID,CKT_ID FROM [COWS].[dbo].[CKT_COST] with (nolock) ) ock   
  WHERE ock.CKT_ID = ckto.CKT_ID)) cktc on cktc.CKT_ID  = ckt.CKT_ID   
  
    
--ORDR_ADR fields ORDR.orderid  
-- Get address of Type Primary  
  
left outer join (SELECT adr.ORDR_ID,  
      CONVERT(varchar, DecryptByKey(csd.CTY_NME)) [CTY_NME2],  
      CONVERT(varchar, DecryptByKey(csd.BLDG_NME)) [BLDG_NME2],  
      CONVERT(varchar, DecryptByKey(csd.FLR_ID)) [FLR_ID2],  
      CONVERT(varchar, DecryptByKey(csd.STT_PRVN_NME)) [PRVN_NME2],  
      CONVERT(varchar, DecryptByKey(csd.RM_NBR)) [RM_NBR2],  
      CONVERT(varchar, DecryptByKey(csd.STREET_ADR_1)) [STREET_ADR_12],  
      CONVERT(varchar, DecryptByKey(csd.ZIP_PSTL_CD)) [ZIP_PSTL_CD2],  
      CONVERT(varchar, DecryptByKey(csd.STT_CD)) [STT_CD2],  
        
      adr.CTY_NME [CTY_NME],  
      adr.BLDG_NME [BLDG_NME],  
      adr.CTRY_CD [CTRY_CD],  
      adr.FLR_ID [FLR_ID],  
      adr.PRVN_NME [PRVN_NME],  
      adr.RM_NBR [RM_NBR],  
      adr.STREET_ADR_1 [STREET_ADR_1],  
      adr.ZIP_PSTL_CD [ZIP_PSTL_CD],  
      adr.STT_CD [STT_CD],  
        
      lc.CTRY_NME    
    FROM [COWS].[dbo].[ORDR_ADR] adr with (nolock)  
     left outer join [COWS].[dbo].[LK_CTRY] lc with (nolock) on adr.CTRY_CD = lc.CTRY_CD   
     LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=adr.ORDR_ADR_ID  AND csd.SCRD_OBJ_TYPE_ID=14  
    where adr.CIS_LVL_TYPE = 'H5' and adr.REC_STUS_ID =1 )ad on ad.ORDR_ID = ordr.ORDR_ID  
   
--Get H5/H6 address  
  
--left outer join (SELECT adr1.ORDR_ID, adr1.STT_CD, adr1.CTRY_CD FROM [COWS].[dbo].[ORDR_ADR] adr1 with (nolock) where adr1.CIS_LVL_TYPE in ( 'H5','H6') and adr1.REC_STUS_ID =1 )adh on adh.ORDR_ID = ordr.ORDR_ID  
  
left outer join (SELECT adr1.ORDR_ID, adr1.STT_CD , csd.STT_CD AS STT_CD2   
    FROM [COWS].[dbo].[ORDR_ADR] adr1 with (nolock)   
    LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=adr1.ORDR_ADR_ID  AND csd.SCRD_OBJ_TYPE_ID=14   
    where adr1.CIS_LVL_TYPE in ('H6') and adr1.REC_STUS_ID =1 )adh on adh.ORDR_ID = ordr.ORDR_ID  
  
-- ORDR_CNTCT  
-- Get Contact of Type Primary  
left outer join (SELECT oct.PHN_NBR,  
      CONVERT(varchar, DecryptByKey(csd.CUST_CNTCT_NME)) [CNTCT_NME2],  
      CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) [EMAIL_ADR2],  
      CONVERT(varchar, DecryptByKey(csd.FRST_NME)) [FRST_NME2],  
      CONVERT(varchar, DecryptByKey(csd.LST_NME))[LST_NME2],  
      oct.CNTCT_NME,  
      oct.EMAIL_ADR,  
      oct.FRST_NME,  
      oct.LST_NME,  
        
      oct.ORDR_ID    
    FROM    [COWS].[dbo].[ORDR_CNTCT] oct with (nolock) JOIN  
      [COWS].[dbo].[ORDR] cc with (nolock) on oct.ORDR_ID = cc.ORDR_ID   
      LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oct.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15   
    WHERE oct.ORDR_CNTCT_ID = (  
    SELECT MAX(ORDR_CNTCT_ID)  
    FROM (SELECT ORDR_CNTCT_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_CNTCT] ct with (nolock) where ct.CNTCT_TYPE_ID = 1 and ct.CIS_LVL_TYPE = 'H5' and ct.REC_STUS_ID = 1 ) omc   
    WHERE omc.ORDR_ID = oct.ORDR_ID)) oc on oc.ORDR_ID = ordr.ORDR_ID   
  
-- OnSite ORDR_CNTCT Contact Type = 4 (Onsite Contact)  
left outer join (SELECT oct.PHN_NBR,  
      CONVERT(varchar, DecryptByKey(csd.CUST_CNTCT_NME)) [OS_CNTCT_NME2],  
      CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) [OS_EMAIL_ADR2],  
      CONVERT(varchar, DecryptByKey(csd.FRST_NME)) [OS_FRST_NME2],  
      CONVERT(varchar, DecryptByKey(csd.LST_NME))[OS_LST_NME2],  
      oct.CNTCT_NME AS [OS_CNTCT_NME],  
      oct.EMAIL_ADR AS [OS_EMAIL_ADR],  
      oct.FRST_NME AS [OS_FRST_NME],  
      oct.LST_NME AS  [OS_LST_NME],  
      oct.ORDR_ID    
    FROM [COWS].[dbo].[ORDR_CNTCT] oct with (nolock) JOIN  
      [COWS].[dbo].[ORDR] cc with (nolock) on oct.ORDR_ID = cc.ORDR_ID  
      LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oct.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15     
  WHERE oct.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)  
  FROM (SELECT ORDR_CNTCT_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_CNTCT] ct with (nolock) where ct.CNTCT_TYPE_ID = 4 and ct.CIS_LVL_TYPE = 'H5' and ct.REC_STUS_ID = 1 ) omc   
  WHERE omc.ORDR_ID = oct.ORDR_ID)) osoc on osoc.ORDR_ID = ordr.ORDR_ID   
  
--Alternate Contact RoleID = 7  
  
left outer join (SELECT oct.PHN_NBR,  
      CONVERT(varchar, DecryptByKey(csd.CUST_CNTCT_NME)) [AL_CNTCT_NME2],  
      CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) [AL_EMAIL_ADR2],  
      CONVERT(varchar, DecryptByKey(csd.FRST_NME)) [AL_FRST_NME2],  
      CONVERT(varchar, DecryptByKey(csd.LST_NME))[AL_LST_NME2],  
        
      oct.CNTCT_NME AS [AL_CNTCT_NME],  
      oct.EMAIL_ADR AS [AL_EMAIL_ADR],  
      oct.FRST_NME AS [AL_FRST_NME],  
      oct.LST_NME AS [AL_LST_NME],  
        
      oct.ORDR_ID    
    FROM [COWS].[dbo].[ORDR_CNTCT] oct with (nolock) JOIN  
      [COWS].[dbo].[ORDR] cc with (nolock) on oct.ORDR_ID = cc.ORDR_ID   
      LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=oct.ORDR_CNTCT_ID  AND csd.SCRD_OBJ_TYPE_ID=15      
  WHERE oct.ORDR_CNTCT_ID = (SELECT MAX(ORDR_CNTCT_ID)  
  FROM (SELECT ORDR_CNTCT_ID,ORDR_ID FROM [COWS].[dbo].[ORDR_CNTCT] ct with (nolock) where ct.ROLE_ID = 7 and ct.REC_STUS_ID = 1 ) omc   
  WHERE omc.ORDR_ID = oct.ORDR_ID)) aloc on aloc.ORDR_ID = ordr.ORDR_ID     
  
--VNDR_FOLDR  
--left outer join COWS.dbo.VNDR_ORDR e with (nolock) on e.ORDR_ID = ordr.ORDR_ID   
left outer join (select vo.* from [COWS].[dbo].[VNDR_ORDR] vo with (nolock) join   
                 [COWS].[dbo].[ORDR] cc with (nolock) on vo.ORDR_ID = cc.ORDR_ID    
                 where vo.VNDR_ORDR_ID =(select MAX(VNDR_ORDR_ID) from (SELECT VNDR_ORDR_ID,ORDR_ID FROM [COWS].[dbo].[VNDR_ORDR] vo1 with (nolock) where vo1.TRMTG_CD =0 ) vo2   
  WHERE vo2.ORDR_ID = vo.ORDR_ID  )) e on e.ORDR_ID = ordr.ORDR_ID  
    
left outer join COWS.dbo.VNDR_FOLDR f with(nolock) on f.VNDR_FOLDR_ID = e.VNDR_FOLDR_ID   
left outer join COWS.dbo.VNDR_FOLDR_CNTCT  fc with(nolock) on fc.VNDR_FOLDR_ID = e.VNDR_FOLDR_ID   
  
  
-- ODIE_REQ  
-- multiple records with same orderid  
  
left outer join (SELECT orq.* FROM [COWS].[dbo].[ODIE_REQ] orq with (nolock) JOIN  
  [COWS].[dbo].[ORDR] cc with (nolock) on orq.ORDR_ID = cc.ORDR_ID    
  WHERE orq.REQ_ID = (SELECT MAX(REQ_ID)  
  FROM (SELECT REQ_ID,ORDR_ID FROM [COWS].[dbo].[ODIE_REQ] with (nolock) ) oms   
  WHERE oms.ORDR_ID = orq.ORDR_ID)) h on h.ORDR_ID = ordr.ORDR_ID  
  
--ORDR_VLAN fields assuming there will be 1 ORDR_VLAN_ID per ORDR_ID  
  
  
left outer join (SELECT vln.* FROM [COWS].[dbo].[ORDR_VLAN] vln with (nolock) JOIN  
  [COWS].[dbo].[ORDR] cc with (nolock) on vln.ORDR_ID = cc.ORDR_ID    
  WHERE vln.ORDR_VLAN_ID = (SELECT MAX(ORDR_VLAN_ID)  
  FROM (SELECT ORDR_VLAN_ID,ORDR_ID,VLAN_SRC_NME FROM [COWS].[dbo].[ORDR_VLAN] with (nolock) ) vla   
  WHERE vla.ORDR_ID = vln.ORDR_ID and vla.VLAN_SRC_NME = 'FSA')) vl on vl.ORDR_ID = ordr.ORDR_ID  
  
  
--NEW - GOM Related Fields  
left outer join ( select ford.ORDR_ID,ford.ORDR_SBMT_DT,usrWast.ASMT_DT  from [COWS].dbo.FSA_ORDR ford with (nolock)   
join (select * from [COWS].[dbo].FSA_ORDR_GOM_XNCI gx with (nolock) where gx.GRP_ID in (1))as fogom on fogom.ORDR_ID = ford.ORDR_ID   
left join (select * from [COWS].[dbo].USER_WFM_ASMT uwa with (nolock) where uwa.GRP_ID in(1)) usrWast on usrWast.ORDR_ID = ford.ORDR_ID     
   
) fsagom on fsagom.ORDR_ID = a.ORDR_ID   
  
--MPLS EVENT FIELD CR 135  
  
full outer join (select distinct mpls.EVENT_ID , mpls.FTN,  
   UPPER(evst.EVENT_STUS_DES) [EVENT_STUS_DES],         
   CASE WHEN mpls.EVENT_TITLE_TXT is NULL THEN '' ELSE mpls.EVENT_TITLE_TXT END [EVENT_TITLE_TXT],   
   CASE WHEN mpls.CUST_NME is NULL THEN '' ELSE mpls.CUST_NME END  [MCUST_NME] ,  
   CASE WHEN mpls.DES_CMNT_TXT is NULL THEN '' ELSE mpls.DES_CMNT_TXT END [DES_CMNT_TXT],  
     
   lkmt.MPLS_EVENT_TYPE_DES ,   
   mpact.MPLS_ACTY_TYPE [MPLS_ACTY_TYPE_DES] ,   
   lkvp.VPN_PLTFRM_TYPE_DES ,  
   lkvt.VAS_TYPE_DES ,  
   mpls.IP_VER_ID ,  
     
   mpls.STRT_TMST [STRT_TMST],  
   --CONVERT(varchar(16), mpls.STRT_TMST,120)  [STRT_TMST],  
      
   mpls.EVENT_DRTN_IN_MIN_QTY ,  
     
   mpls.EXTRA_DRTN_TME_AMT ,     
          
   --CONVERT(varchar(16), mpls.END_TMST,120)  [End_Time],   
   mpls.END_TMST  [End_Time],   
         
   CASE  
    WHEN lkac.ACTN_DES = 'Completed Email Notification' THEN 'Completed'   
    ELSE lkac.ACTN_DES END [MPLS_ACTN_DES],  
         
   REPLACE(SUBSTRING(CONVERT(varchar, mpev.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mpev.CREAT_DT, 109), 13, 8)   
    + ' ' + SUBSTRING(CONVERT(varchar, mpev.CREAT_DT, 109), 25, 2) [MPLS_MOD_DT],    
      
   CASE  
    WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''  
    WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME    
    ELSE luser1.DSPL_NME END [MPLS_MOD_BY],  
         
   CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '' ELSE scev.SUCSS_ACTY_DES END [SUCSS_ACTY_DES],    
   CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '' ELSE flev.FAIL_ACTY_DES END [FAIL_ACTY_DES],  
       
      
   CASE WHEN mpev.CMNT_TXT is NULL THEN '' ELSE mpev.CMNT_TXT END [MPLS_CMNT_TXT]  
        
   FROM COWS.dbo.MPLS_EVENT mpls with (nolock)     
   left outer JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mpls.EVENT_STUS_ID = evst.EVENT_STUS_ID  
   left outer join COWS.dbo.LK_MPLS_EVENT_TYPE lkmt with (nolock) ON mpls.MPLS_EVENT_TYPE_ID = lkmt.MPLS_EVENT_TYPE_ID  
   left outer join COWS.dbo.LK_VPN_PLTFRM_TYPE lkvp with (nolock) ON mpls.VPN_PLTFRM_TYPE_ID = lkvp.VPN_PLTFRM_TYPE_ID  
   left outer JOIN COWS.dbo.MPLS_EVENT_VAS_TYPE mvst with (nolock) ON mpls.EVENT_ID = mvst.EVENT_ID  
   left outer JOIN COWS.dbo.LK_VAS_TYPE lkvt with (nolock) ON mvst.VAS_TYPE_ID = lkvt.VAS_TYPE_ID   
   left outer JOIN  
   (  
    SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT,eh.CMNT_TXT   
     FROM COWS.dbo.EVENT_HIST eh with (nolock)  
     --JOIN COWS.dbo.MPLS_EVENT_ACTY_TYPE meat with (nolock) ON eh.EVENT_ID = meat.EVENT_ID  
     JOIN COWS.dbo.MPLS_EVENT mple with (nolock) ON eh.EVENT_ID = mple.EVENT_ID  
     WHERE eh.ACTN_ID in (5,10,12,15,17) AND mple.EVENT_STUS_ID not in (8)  
    UNION  
    SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT,ehis.CMNT_TXT   
     FROM COWS.dbo.EVENT_HIST ehis with (nolock)  
     JOIN  
     (  
      SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) 'maxEvHis'  
       FROM COWS.dbo.EVENT_HIST eh with (nolock)  
       --JOIN COWS.dbo.MPLS_EVENT_ACTY_TYPE meat with (nolock) ON eh.EVENT_ID = meat.EVENT_ID  
       JOIN COWS.dbo.MPLS_EVENT mple with (nolock) ON eh.EVENT_ID = mple.EVENT_ID  
       WHERE eh.ACTN_ID in (18) AND mple.EVENT_STUS_ID not in (8)  
      GROUP BY eh.EVENT_ID  
     )ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis  
   )mpev ON mpls.EVENT_ID = mpev.EVENT_ID        
   LEFT outer  JOIN  
   (  
    SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID  
      FROM  
      (  
     SELECT (SELECT lksu.SUCSS_ACTY_DES  + ', ' AS [text()]   
        FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)  
        JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID   
        WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID --AND evsu.REC_STUS_ID = 1  
        FOR xml PATH ('')  
       ) as SUCSS_ACTY_DES, EVENT_HIST_ID   
     FROM  
     (SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)   
     )evSucAct  
      )evSucAct2  
   )scev ON mpev.EVENT_HIST_ID = scev.EVENT_HIST_ID  
   LEFT outer JOIN  
   (  
    SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID  
      FROM  
      (  
     SELECT (SELECT lkfl.FAIL_ACTY_DES  + ', ' AS [text()]   
        FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)  
        JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID   
        WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID --AND evfl.REC_STUS_ID = 1  
        FOR xml PATH ('')  
       ) as FAIL_ACTY_DES, EVENT_HIST_ID   
     FROM  
     (SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)   
     )evFailAct  
      )evFailAct2  
   )flev ON mpev.EVENT_HIST_ID = flev.EVENT_HIST_ID     
   LEFT outer JOIN  
   (  
    SELECT distinct LEFT([MPLS_ACTY_TYPE],len([MPLS_ACTY_TYPE]) -1) as MPLS_ACTY_TYPE, EVENT_ID  
    FROM  
    (  
     SELECT (SELECT lkma.MPLS_ACTY_TYPE_DES + ', ' AS [text()]   
       FROM COWS.dbo.MPLS_EVENT_ACTY_TYPE meac with (nolock)  
       JOIN COWS.dbo.LK_MPLS_ACTY_TYPE lkma with (nolock) ON meac.MPLS_ACTY_TYPE_ID = lkma.MPLS_ACTY_TYPE_ID  
       WHERE meac.EVENT_ID = mple.EVENT_ID   
       FOR xml PATH ('')  
         ) as MPLS_ACTY_TYPE, EVENT_ID   
     FROM   
     ( SELECT EVENT_ID FROM COWS.dbo.MPLS_EVENT with (nolock) where EVENT_STUS_ID not in (8)   
           )  
     mple  
    )mpaty   
   )mpact ON mpls.EVENT_ID = mpact.EVENT_ID     
   left JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mpev.CREAT_BY_USER_ID = luser1.USER_ID   
   left JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON mpev.ACTN_ID = lkac.ACTN_ID ) as mpls on mpls.FTN = a.FTN   
  
-- CR 135 AD EVENT FIELDS  
  
full outer join (select distinct adev.EVENT_ID [AD_Event_ID], adev.FTN,    
   UPPER(adevst.EVENT_STUS_DES) [AD_Event_Status],         
     
   CASE WHEN adev.EVENT_TITLE_TXT is NULL THEN '' ELSE adev.EVENT_TITLE_TXT END [AD_Item_Title],   
     
   CASE WHEN adev.CUST_NME is NULL THEN '' ELSE adev.CUST_NME END [AD_Cust_Name],  
     
   CASE WHEN adev.DES_CMNT_TXT is NULL THEN '' ELSE adev.DES_CMNT_TXT END [AD_DES_CMNT_TXT],  
     
   adlkes.ENHNC_SRVC_NME [AD_Type],   
     
   fmsc.FMS_CKT_LIST [AD_CKT_ID],  
     
   CASE  
    WHEN CAST(adev.IP_VER_ID AS VARCHAR(3)) is NULL THEN ''  
    ELSE CAST(adev.IP_VER_ID AS VARCHAR(3)) END [AD_IP_VER_ID],  
     
   adev.STRT_TMST  [AD_STRT_TMST],   
        
   adev.EVENT_DRTN_IN_MIN_QTY [AD_EVENT_DRTN_IN_MIN_QTY],  
   adev.EXTRA_DRTN_TME_AMT [AD_EXTRA_DRTN_TME_AMT],     
     
   adev.END_TMST  [AD_END_TMST],   
          
   CASE  
    WHEN lkac.ACTN_DES = 'Completed Email Notification' THEN 'Completed'   
    ELSE lkac.ACTN_DES END  [AD_ACTN_DES] ,   
      
   adhi.CREAT_DT [AD_MOD_DT],  
         
   CASE  
    WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''  
    WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME    
    ELSE luser1.DSPL_NME END [AD_MOD_BY],   
      
        
   CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '' ELSE scev.SUCSS_ACTY_DES END [AD_SUCSS_ACTY_DES],    
   CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '' ELSE flev.FAIL_ACTY_DES END  [AD_FAIL_ACTY_DES],  
     
   CASE WHEN adhi.CMNT_TXT is NULL THEN '' ELSE adhi.CMNT_TXT END [AD_CMNT_TXT]   
    
    from COWS.dbo.AD_EVENT adev with (nolock)   
    
   left outer  JOIN COWS.dbo.LK_EVENT_STUS adevst with (nolock) ON adev.EVENT_STUS_ID = adevst.EVENT_STUS_ID  
   left outer  JOIN COWS.dbo.LK_ENHNC_SRVC adlkes with (nolock) ON adev.ENHNC_SRVC_ID = adlkes.ENHNC_SRVC_ID  
       
     
   left outer JOIN  
   (  
    SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT,eh.CMNT_TXT  
     FROM COWS.dbo.EVENT_HIST eh with (nolock)       
     JOIN COWS.dbo.AD_EVENT acde with (nolock) ON eh.EVENT_ID = acde.EVENT_ID  
     WHERE eh.ACTN_ID in (5,10,12,15,17) AND acde.EVENT_STUS_ID not in (8)  
    UNION  
    SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT,ehis.CMNT_TXT  
     FROM COWS.dbo.EVENT_HIST ehis with (nolock)  
     JOIN  
     (  
      SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) 'maxEvHis'  
       FROM COWS.dbo.EVENT_HIST eh with (nolock)  
       JOIN COWS.dbo.AD_EVENT acde with (nolock) ON eh.EVENT_ID = acde.EVENT_ID  
       WHERE eh.ACTN_ID in (18) AND acde.EVENT_STUS_ID not in (8)  
      GROUP BY eh.EVENT_ID  
     )ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis  
   )adhi ON adev.EVENT_ID = adhi.EVENT_ID        
   LEFT outer JOIN  
   (  
    SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID  
      FROM  
      (  
     SELECT (SELECT lksu.SUCSS_ACTY_DES  + ', ' AS [text()]   
        FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)  
        JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID   
        WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID --AND evsu.REC_STUS_ID = 1  
        FOR xml PATH ('')  
       ) as SUCSS_ACTY_DES, EVENT_HIST_ID   
     FROM  
     (SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)   
     )evSucAct  
      )evSucAct2  
   )scev ON adhi.EVENT_HIST_ID = scev.EVENT_HIST_ID  
   LEFT outer JOIN  
   (  
    SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID  
      FROM  
      (  
     SELECT (SELECT lkfl.FAIL_ACTY_DES  + ', ' AS [text()]   
        FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)  
        JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID   
        WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID --AND evfl.REC_STUS_ID = 1  
        FOR xml PATH ('')  
       ) as FAIL_ACTY_DES, EVENT_HIST_ID   
     FROM  
     (SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)   
     )evFailAct  
      )evFailAct2  
   )flev ON adhi.EVENT_HIST_ID = flev.EVENT_HIST_ID     
   LEFT outer JOIN  
   (  
    SELECT distinct LEFT([FMS_CKT_LIST],len([FMS_CKT_LIST]) -1) as FMS_CKT_LIST, EVENT_ID  
    FROM  
    (  
     SELECT (SELECT adat.CKT_ID + ', ' AS [text()]   
       FROM COWS.dbo.AD_EVENT_ACCS_TAG adat with (nolock)         
       WHERE adat.EVENT_ID = acde.EVENT_ID   
       FOR xml PATH ('')  
         ) as FMS_CKT_LIST, EVENT_ID   
     FROM   
     ( SELECT EVENT_ID FROM COWS.dbo.AD_EVENT with (nolock) where EVENT_STUS_ID not in (8)  
        
     )  
     acde  
    )fmsci  
   )fmsc ON adev.EVENT_ID = fmsc.EVENT_ID     
   left JOIN COWS.dbo.LK_USER luser1 with (nolock) ON adhi.CREAT_BY_USER_ID = luser1.USER_ID   
   left JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON adhi.ACTN_ID = lkac.ACTN_ID ) as adev on adev.FTN = a.FTN   
              
             --End: CR 135 AD Events  
  
       --BEGIN: CR 135 SPLK (Sprint Link Events)  
     
        full outer join ( select distinct splk.EVENT_ID [SPLK_Event_ID], splk.FTN,    
   UPPER(splkevst.EVENT_STUS_DES) [SPLK_Event_Status],         
   CASE WHEN splk.EVENT_TITLE_TXT is NULL THEN '' ELSE splk.EVENT_TITLE_TXT END [SPLK_Item_Title],   
   CASE WHEN splk.CUST_NME is NULL THEN '' ELSE splk.CUST_NME END [SPLK_Cust_Name],  
   CASE WHEN splk.DSGN_CMNT_TXT is NULL THEN '' ELSE splk.DSGN_CMNT_TXT END [SPLK_DES_CMNT_TXT],  
     
   lkmt.SPLK_EVENT_TYPE_DES  ,   
             lkma.SPLK_ACTY_TYPE_DES ,  
     
   CASE  
    WHEN CAST(splk.IP_VER_ID AS VARCHAR(3)) is NULL THEN ''  
    ELSE CAST(splk.IP_VER_ID AS VARCHAR(3)) END [SPLK_IP_VER_ID],  
     
   splk.STRT_TMST  [SPLK_STRT_TMST],   
        
   splk.EVENT_DRTN_IN_MIN_QTY [SPLK_EVENT_DRTN_IN_MIN_QTY],  
   splk.EXTRA_DRTN_TME_AMT [SPLK_EXTRA_DRTN_TME_AMT],     
     
   splk.END_TMST  [SPLK_END_TMST],   
     
        
   CASE  
    WHEN lkac.ACTN_DES = 'Completed Email Notification' THEN 'Completed'   
    ELSE lkac.ACTN_DES END [SPLK_ACTN_DES],  
     
   sphi.CREAT_DT [SPLK_MOD_DT],  
         
   CASE  
    WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''  
    WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME    
    ELSE luser1.DSPL_NME END  [SPLK_MOD_BY],   
      
        
   CASE WHEN scev.SUCSS_ACTY_DES is NULL THEN '' ELSE scev.SUCSS_ACTY_DES END [SPLK_SUCSS_ACTY_DES],     
   CASE WHEN flev.FAIL_ACTY_DES is NULL THEN '' ELSE flev.FAIL_ACTY_DES END [SPLK_FAIL_ACTY_DES],   
     
   CASE WHEN sphi.CMNT_TXT is NULL THEN '' ELSE sphi.CMNT_TXT END [SPLK_CMNT_TXT]   
    
     
   from COWS.dbo.SPLK_EVENT splk with (nolock)   
    
   left outer  JOIN COWS.dbo.LK_EVENT_STUS splkevst with (nolock) ON splk.EVENT_STUS_ID = splkevst.EVENT_STUS_ID  
    
   left outer  JOIN COWS.dbo.LK_SPLK_EVENT_TYPE lkmt with (nolock) ON splk.SPLK_EVENT_TYPE_ID  = lkmt.SPLK_EVENT_TYPE_ID  
            left outer  JOIN COWS.dbo.LK_SPLK_ACTY_TYPE lkma with (nolock) ON splk.SPLK_ACTY_TYPE_ID = lkma.SPLK_ACTY_TYPE_ID  
  
   left outer  JOIN COWS.dbo.AD_EVENT_ACCS_TAG splkaatg with (nolock) ON splk.EVENT_ID = splkaatg.EVENT_ID     
    
    
     JOIN  
   (  SELECT eh.EVENT_ID, eh.EVENT_HIST_ID, eh.ACTN_ID, eh.CREAT_BY_USER_ID, eh.CREAT_DT,eh.CMNT_TXT   
     FROM COWS.dbo.EVENT_HIST eh with (nolock)       
     JOIN COWS.dbo.SPLK_EVENT sple with (nolock) ON eh.EVENT_ID = sple.EVENT_ID  
     WHERE eh.ACTN_ID in (5,10,12,15,17) and sple.EVENT_STUS_ID not in (8)  
    UNION  
    SELECT ehis.EVENT_ID, ehis.EVENT_HIST_ID, ehis.ACTN_ID, ehis.CREAT_BY_USER_ID, ehis.CREAT_DT,ehis.CMNT_TXT   
     FROM COWS.dbo.EVENT_HIST ehis with (nolock)  
     JOIN  
     (  
      SELECT eh.EVENT_ID, max(eh.EVENT_HIST_ID) 'maxEvHis'  
       FROM COWS.dbo.EVENT_HIST eh with (nolock)  
       JOIN COWS.dbo.SPLK_EVENT sple with (nolock) ON eh.EVENT_ID = sple.EVENT_ID  
       WHERE eh.ACTN_ID in (18) and sple.EVENT_STUS_ID not in (8)  
      GROUP BY eh.EVENT_ID  
     )ehst ON ehis.EVENT_HIST_ID = ehst.maxEvHis  
   )sphi ON splk.EVENT_ID = sphi.EVENT_ID        
   LEFT JOIN  
   (  
    SELECT distinct LEFT([SUCSS_ACTY_DES],len([SUCSS_ACTY_DES]) -1) as SUCSS_ACTY_DES, evSucAct2.EVENT_HIST_ID  
      FROM  
      (  
     SELECT (SELECT lksu.SUCSS_ACTY_DES  + ', ' AS [text()]   
        FROM COWS.dbo.EVENT_SUCSS_ACTY evsu with (nolock)  
        JOIN COWS.dbo.LK_SUCSS_ACTY lksu with (nolock) ON evsu.SUCSS_ACTY_ID = lksu.SUCSS_ACTY_ID   
        WHERE evsu.EVENT_HIST_ID = evSucAct.EVENT_HIST_ID --AND evsu.REC_STUS_ID = 1  
        FOR xml PATH ('')  
       ) as SUCSS_ACTY_DES, EVENT_HIST_ID   
     FROM  
     (SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_SUCSS_ACTY with (nolock)   
     )evSucAct  
      )evSucAct2  
   )scev ON sphi.EVENT_HIST_ID = scev.EVENT_HIST_ID  
   LEFT JOIN  
   (  
    SELECT distinct LEFT([FAIL_ACTY_DES],len([FAIL_ACTY_DES]) -1) as FAIL_ACTY_DES, evFailAct2.EVENT_HIST_ID  
      FROM  
      (  
     SELECT (SELECT lkfl.FAIL_ACTY_DES  + ', ' AS [text()]   
        FROM COWS.dbo.EVENT_FAIL_ACTY evfl with (nolock)  
        JOIN COWS.dbo.LK_FAIL_ACTY lkfl with (nolock) ON evfl.FAIL_ACTY_ID = lkfl.FAIL_ACTY_ID   
        WHERE evfl.EVENT_HIST_ID = evFailAct.EVENT_HIST_ID --AND evfl.REC_STUS_ID = 1  
        FOR xml PATH ('')  
       ) as FAIL_ACTY_DES, EVENT_HIST_ID   
     FROM  
     (SELECT EVENT_HIST_ID FROM COWS.dbo.EVENT_FAIL_ACTY with (nolock)   
     )evFailAct  
      )evFailAct2  
   )flev ON sphi.EVENT_HIST_ID = flev.EVENT_HIST_ID     
   JOIN COWS.dbo.LK_USER luser1 with (nolock) ON sphi.CREAT_BY_USER_ID = luser1.USER_ID   
   JOIN COWS.dbo.LK_ACTN lkac with (nolock) ON sphi.ACTN_ID = lkac.ACTN_ID ) as splk on splk.FTN = a.FTN  
              
   --         --End: CR 135 SPLK (Sprint Link Events)  
  
  
END  