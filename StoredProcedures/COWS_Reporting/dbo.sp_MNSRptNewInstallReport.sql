USE [COWS_Reporting]
GO
/*
--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			06/21/2011
-- Description:		
--					Extract device and vendor model of newly installed devices that
--					have been completed via Fasttrack or MDS scheduled events. 
--
--					QueryNumber: 1 = Daily - List of events completed yesterday
--								 2 = Weekly
--								 3 = Monthly
--								 0 = Specify start date & end date: 'mm/dd/yyyy'
--
-- Modifications:
--
-- 03/29/2012		sxv0766: IM952822-Removed resulting string expression from 
--					Start Time and End Time columns
--
-- 11/23/2016		vn370313: add new MDS events just by Union ALL  from MDS_EVENT table  at lower part so that you can just remove
--					MDS_EVENT_NEW   part  which is Old MDS event when asked
--					No Need FSA_MDS_EVENT_New/FSA_MDS_EVENT   Connect  MDS_EVENT  to MDS_EVENT_ODIE_DEV   (instead MDS_EVENT_ODIE_DEV_NME)
--                  with Event_ID     and use the  MANF_ID you need  
-- 03/29/2012		sxv0vn370313766: UNION ALL missing  added
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD					       
----EXEC COWS_Reporting.dbo.sp_MNSRptNewInstallReport 0,0,'01/01/2014','11/11/2016'	
----EXEC COWS_Reporting.dbo.sp_MNSRptNewInstallReport_Old 0,0,'01/01/2014','11/11/2016'					
--==================================================================================
*/

ALTER Procedure [dbo].[sp_MNSRptNewInstallReport]
	@QueryNumber		INT,
	@Secured			INT=0,
	@inStartDtStr		VARCHAR(10)='',
	@inEndDtStr			VARCHAR(10)=''
AS
BEGIN TRY

	SET NOCOUNT ON;

	DECLARE @startDate		Datetime
	DECLARE @endDate		Datetime
	DECLARE	@today			Datetime
	DECLARE @startDtStr		VARCHAR(10)	
	DECLARE @dayOfWeek		INT

	IF @QueryNumber=0
		BEGIN

			SET @startDate=CONVERT(datetime, @inStartDtStr, 101)
			SET @endDate=CONVERT(datetime, @inEndDtStr, 101)				

			-- Add 1 day to End Date in order to cover the End Date 24-hr period
			SET @endDate=DATEADD(day, 1, @endDate) 
												
		END	
	ELSE IF @QueryNumber=1
    	BEGIN    	
    		-- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000
		
			--SET @startDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			--SET @endDate=cast(CONVERT(varchar(8), DATEADD(day, 1, GETDATE()), 112) AS datetime)   
			
			-- Today's report shows a list of events completed yesterday 
			SET @startDate=cast(CONVERT(varchar(8), DATEADD(day, -1, GETDATE()), 112) AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
			
		END
	ELSE IF @QueryNumber=2
		BEGIN
			-- e.g. Run date is '2011-06-20' 2rd day of the week
			-- STRT_TMST >= 2011-06-13 00:00:00.000 and STRT_TMST < 2011-06-20 00:00:00.000
			
			SET @today=GETDATE()
			SET @dayOfWeek=DATEPART(dw, @today)
			SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) As DateTime)
			SET @endDate=cast(CONVERT(varchar(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS Datetime)
						
		END
	ELSE IF @QueryNumber=3
		BEGIN			 
  			SET @today=DATEADD(MONTH,-1,GETDATE())
    	
    		-- e.g. Run date is the 1st day of each month 
    		-- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000
    		
  			SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));  			
			SET @startDate=cast(@startDtStr AS datetime)
			SET @endDate=cast(CONVERT(varchar(8), GETDATE(), 112) AS datetime)
		END

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	SELECT	UPPER(evst.EVENT_STUS_DES)'Event Status', 
			mds.EVENT_ID 'Event Id', 
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME))
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				 
				END 'Customer Name',			
			mds.CHARS_ID 'Chars Id',
			mds.STRT_TMST 'Start Time',
			--REPLACE(SUBSTRING(CONVERT(varchar, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Start Time',			
			mds.END_TMST 'End Time',
			--REPLACE(SUBSTRING(CONVERT(varchar, mds.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 25, 2) 'End Time',			
			odie.ODIE_DEV_NME AS  'ODIE Device Name',
			CASE				
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END 'Assigned To',		
			CASE
				--WHEN CONVERT(varchar(200), DecryptByKey(mds.CUST_EMAIL_ADR)) is NULL THEN ''
				--ELSE CONVERT(varchar(200), DecryptByKey(mds.CUST_EMAIL_ADR)) 
				--END 'Team Email',
			
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_EMAIL_ADR
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)),mds.CUST_EMAIL_ADR,'')
				END 'Team Email',	
			( RTRIM(LTRIM(lman.MANF_NME)) + ' ' + RTRIM(LTRIM(lmdl.DEV_MODEL_NME))) 'Vendor Model',
			
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
			 CASE  
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'   
				ELSE '' END 'MDS Install Activity',
			CASE 
				WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
				ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 'Assigned Acct MNS PM'
								
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock) 
		JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		LEFT JOIN (SELECT EVENT_ID, CREAT_DT, CREAT_BY_USER_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE ACTN_ID = 17 ) as evhi ON mds.EVENT_ID = evhi.EVENT_ID
		--LEFT JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
		--			FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
		--			WHERE REC_STUS_ID = 1
		--			GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID
		--LEFT JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
		--	AND lasg.maxRecTmst = easg.CREAT_DT				
		--LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON easg.ASN_TO_USER_ID = luser1.USER_ID		
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON evhi.CREAT_BY_USER_ID = luser1.USER_ID
		--LEFT JOIN COWS.dbo.MDS_EVENT_MNGD_DEV mdev with (nolock) ON fmds.FSA_MDS_EVENT_ID = mdev.FSA_MDS_EVENT_ID 
		LEFT JOIN COWS.dbo.MDS_EVENT_ODIE_DEV_NME odie WITH (NOLOCK) ON odie.FSA_MDS_EVENT_ID = fmds.FSA_MDS_EVENT_ID
		LEFT JOIN COWS.dbo.LK_DEV_MANF lman with (nolock) ON odie.MANF_ID = lman.MANF_ID
		LEFT JOIN COWS.dbo.LK_DEV_MODEL lmdl with (nolock) ON odie.DEV_MODEL_ID = lmdl.DEV_MODEL_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID			 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID				
						
	-- MDS Activity: 1 = Initial, 2 = MAC : Event in 'Completed' Status  	
	WHERE	mds.MDS_ACTY_TYPE_ID in (1, 2) AND mds.EVENT_STUS_ID in (6) AND
			evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate
		
	UNION ALL
	/*New MDS EVENT Start*/
	
	SELECT	UPPER(evst.EVENT_STUS_DES)'Event Status', 
			mds.EVENT_ID 'Event Id', 
			CASE 						
				--WHEN CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NULL THEN ''
				--WHEN @Secured = 0 AND evt.SCURD_CD = 1 AND CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) is NOT NULL THEN 'Private Customer'
				--ELSE CONVERT(varchar(100), DecryptByKey(mds.CUST_NME)) 
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.CUST_NME
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),mds.CUST_NME,'') 
				END 'Customer Name',			
			mds.CHARS_ID 'Chars Id',
			mds.STRT_TMST 'Start Time',
			--REPLACE(SUBSTRING(CONVERT(varchar, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) 'Start Time',			
			mds.END_TMST 'End Time',
			--REPLACE(SUBSTRING(CONVERT(varchar, mds.END_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 13, 8) 
			--	+ ' ' + SUBSTRING(CONVERT(varchar, mds.END_TMST, 109), 25, 2) 'End Time',			
			odie.ODIE_DEV_NME AS  'ODIE Device Name',
			CASE				
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NULL THEN ''
				WHEN luser1.DSPL_NME is NULL AND luser1.FULL_NME is NOT NULL THEN luser1.FULL_NME 	
				ELSE luser1.DSPL_NME END 'Assigned To',		
			CASE
				--WHEN CONVERT(varchar(200), DecryptByKey(mds.SHIP_CUST_EMAIL_ADR)) is NULL THEN ''    /* SHIP_CUST_EMAIL_ADR  Verify */
				--ELSE CONVERT(varchar(200), DecryptByKey(mds.SHIP_CUST_EMAIL_ADR))
				
				WHEN @secured = 0 and evt.CSG_LVL_ID =0 THEN mds.SHIP_CUST_EMAIL_ADR
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
				WHEN @secured = 0 and evt.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)) IS NULL THEN NULL 
				ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_EMAIL_ADR)),mds.SHIP_CUST_EMAIL_ADR,'') 
				END 'Team Email',  /* SHIP_CUST_EMAIL_ADR  Verify */
			( RTRIM(LTRIM(lman.MANF_NME)) + ' ' + RTRIM(LTRIM(lmdl.DEV_MODEL_NME))) 'Vendor Model', 
			mact.MDS_ACTY_TYPE_DES 'MDS Activity Type',
			 CASE  
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'   
				ELSE '' END 'MDS Install Activity',
			CASE 
				WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
				ELSE maac.MDS_MAC_ACTY_NME END 'MDS MAC Activity',
			CASE
				WHEN luser2.DSPL_NME is NULL THEN ''
				ELSE luser2.DSPL_NME END 'Assigned Acct MNS PM'
								
	FROM	COWS.dbo.MDS_EVENT mds with (nolock) 
		--JOIN COWS.dbo.FSA_MDS_EVENT fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=7
		LEFT JOIN (SELECT EVENT_ID, CREAT_DT, CREAT_BY_USER_ID 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE ACTN_ID = 17 ) as evhi ON mds.EVENT_ID = evhi.EVENT_ID
		--LEFT JOIN(SELECT EVENT_ID, MAX(CREAT_DT) as maxRecTmst 
		--			FROM COWS.dbo.EVENT_ASN_TO_USER with (nolock)
		--			WHERE REC_STUS_ID = 1
		--			GROUP BY EVENT_ID) as lasg ON mds.EVENT_ID = lasg.EVENT_ID
		--LEFT JOIN COWS.dbo.EVENT_ASN_TO_USER easg with (nolock) ON lasg.EVENT_ID = easg.EVENT_ID 
		--	AND lasg.maxRecTmst = easg.CREAT_DT				
		--LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON easg.ASN_TO_USER_ID = luser1.USER_ID		
		LEFT JOIN COWS.dbo.LK_USER luser1 with (nolock) ON evhi.CREAT_BY_USER_ID = luser1.USER_ID
		--LEFT JOIN COWS.dbo.MDS_EVENT_MNGD_DEV mdev with (nolock) ON fmds.FSA_MDS_EVENT_ID = mdev.FSA_MDS_EVENT_ID 
		--LEFT JOIN COWS.dbo.MDS_EVENT_ODIE_DEV_NME odie WITH (NOLOCK) ON odie.FSA_MDS_EVENT_ID = fmds.FSA_MDS_EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_ODIE_DEV odie WITH (NOLOCK) ON odie.EVENT_ID = mds.EVENT_ID
		LEFT JOIN COWS.dbo.LK_DEV_MANF lman with (nolock) ON odie.MANF_ID = lman.MANF_ID
		LEFT JOIN COWS.dbo.LK_DEV_MODEL lmdl with (nolock) ON odie.DEV_MODEL_ID = lmdl.DEV_MODEL_ID		
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON mds.MNS_PM_ID = luser2.USER_ADID			 
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID				
						
	-- MDS Activity: 1 = Initial, 2 = MAC : Event in 'Completed' Status  	
	WHERE	mds.MDS_ACTY_TYPE_ID in (1, 2) AND mds.EVENT_STUS_ID in (6) AND
			evhi.CREAT_DT >= @startDate AND evhi.CREAT_DT < @endDate
	
	ORDER By mds.EVENT_ID
	/*New MDS EVENT Ends */

	CLOSE SYMMETRIC KEY FS@K3y; 
	
	RETURN 0;
		
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(100)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptNewInstallReport ' + CAST(@QueryNumber AS VARCHAR(4))
	SET @Desc=@Desc + ','  + CAST(@Secured AS VARCHAR(2)) + ': '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)	
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH


