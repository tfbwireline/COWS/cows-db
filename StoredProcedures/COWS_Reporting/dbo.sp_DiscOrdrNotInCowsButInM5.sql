USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_DiscOrdrNotinCowsButinM5]    Script Date: 10/08/2018 20:03:00 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_DiscOrdrNotInCowsButInM5]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_DiscOrdrNotInCowsButInM5]
GO

USE [COWS_Reporting]
GO

/****** Object:  StoredProcedure [dbo].[sp_DiscOrdrNotinCowsButinM5]    Script Date: 10/08/2018 20:03:00 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
-- =============================================  
-- Author:  MD M Monir 
-- Create date: 02132019
-- Description: INTL Monthly report showing Disc orders in M5 not in COWS.
--[dbo].[sp_DiscOrdrNotInCowsButInM5] 1
--[dbo].[sp_DiscOrdrNotInCowsButInM5] 2
--[dbo].[sp_DiscOrdrNotInCowsButInM5] 3   
*/ 
CREATE PROCEDURE [dbo].[sp_DiscOrdrNotInCowsButInM5] 
  @QueryNumber		   INT,  
  @secured  BIT = 0  
  
  
AS  
BEGIN TRY  
--DECLARE @secured BIT=1 -- for testing purposes only   
 SET NOCOUNT ON;  
   
  BEGIN  
  
  DECLARE @startDate Datetime=DATEADD(mm,-1,CONVERT(VARCHAR(25),DATEADD(DD,-(DAY(GETDATE())-1),GETDATE()),101))  
  DECLARE @endDate Datetime=GETDATE()  
    
  Print @endDate  
  Print @startDate 
  IF @QueryNumber=1
  BEGIN
		-- This query shows Disconnect orders in M5 not in COWS.  
		  Select ORDER_NBR AS [M5 Order Nbr],
				 CUST_NME AS [Cust Name],
				 CIS_CUST_ID AS [H6],
				 ctry.CTRY_NME AS [Country],
				 SITE_ID AS [Site],
				CASE 
					WHEN ORDR_SUBTYPE_CD = 'FD' THEN 'Full'
					WHEN ORDR_SUBTYPE_CD = 'PD' THEN 'Partial'
					ELSE ORDR_SUBTYPE_CD
				END AS [Disc Type],
				CUST_CMMT_DT   AS [CCD],
				m5.CREAT_DT   AS [M5 Create Date],
				PROD_NME AS [Product],
				CASE DOM_PROCESS_FLG_CD
					when 'N' Then 'INTL'
					when 'Y' THEN 'DMSTC'
					ELSE ''
				END AS [Intl/Dmstc] 
				  From openquery(m5,'select DISTINCT ord.ORDER_NBR,
											 ca.CUST_NME,
											 ca.CIS_CUST_ID,
											 ord.ORDR_SUBTYPE_CD,
											 ord.CUST_CMMT_DT,
											 ord.CREAT_DT,
											 ca.CTRY_CD,
											 ca.SITE_ID,
											 ord.PROD_NME,
											 ord.DOM_PROCESS_FLG_CD 
										from mach5.v_v5u_ordr ord 
									  inner join Mach5.V_V5U_CUST_ACCT ca on ca.CUST_ACCT_ID = ord.PRNT_ACCT_ID									
									  where ord.ORDR_TYPE_CD  like ''DC%''
										AND ord.DOM_PROCESS_FLG_CD  = ''N''
										and ca.CIS_HIER_LVL_CD = ''H6''
										')m5
					Inner join COWS.dbo.LK_CTRY ctry with (nolock) ON m5.CTRY_CD = ctry.CTRY_CD
					WHERE ORDER_NBR NOT IN  (SELECT DISTINCT FTN FROM COWS.dbo.FSA_ORDR with (nolock))
						AND m5.CREAT_DT > @startDate and m5.CREAT_DT < @endDate
	
		END
		IF @QueryNumber=2
		BEGIN
		--	-- Query below shows all Disconnect orders in M5 regardless in COWS or not
		Select ORDER_NBR AS [M5 Order Nbr],
				 CUST_NME AS [Cust Name],
				 CIS_CUST_ID AS [H6],
				 ctry.CTRY_NME AS [Country],
				 SITE_ID AS [Site],
				CASE 
					WHEN ORDR_SUBTYPE_CD = 'FD' THEN 'Full'
					WHEN ORDR_SUBTYPE_CD = 'PD' THEN 'Partial'
					ELSE ORDR_SUBTYPE_CD
				END AS [Disc Type],
				CUST_CMMT_DT   AS [CCD],
				m5.CREAT_DT   AS [M5 Create Date],
				PROD_NME AS [Product],
				CASE DOM_PROCESS_FLG_CD
					when 'N' Then 'INTL'
					when 'Y' THEN 'DMSTC'
					ELSE ''
				END AS [Intl/Dmstc] 
				  From openquery(m5,'select DISTINCT ord.ORDER_NBR,
											 ca.CUST_NME,
											 ca.CIS_CUST_ID,
											 ord.ORDR_SUBTYPE_CD,
											 ord.CUST_CMMT_DT,
											 ord.CREAT_DT,
											 ca.CTRY_CD,
											 ca.SITE_ID,
											 ord.PROD_NME,
											 ord.DOM_PROCESS_FLG_CD 
										from mach5.v_v5u_ordr ord 
									  inner join Mach5.V_V5U_CUST_ACCT ca on ca.CUST_ACCT_ID = ord.PRNT_ACCT_ID								
									  where ord.ORDR_TYPE_CD  like ''DC%''
										AND ord.ORDR_ID NOT IN (select DISTINCT ORDR_ID from Mach5.V_V5U_ORDR_DISC_CMPNT)
										and ca.CIS_HIER_LVL_CD = ''H6''
										')m5

					Inner join COWS.dbo.LK_CTRY ctry with (nolock) ON m5.CTRY_CD = ctry.CTRY_CD
					WHERE  m5.CREAT_DT > @startDate and m5.CREAT_DT < @endDate
			END
			IF @QueryNumber=3
			BEGIN
			--This query shows the Disconnect orders not in COWS with H6 Country <> US in M5
				 Select ORDER_NBR AS [M5 Order Nbr],
								 CUST_NME AS [Cust Name],
								 CIS_CUST_ID AS [H6],
								 ctry.CTRY_NME AS [Country],
								 SITE_ID AS [Site],
								CASE 
									WHEN ORDR_SUBTYPE_CD = 'FD' THEN 'Full'
									WHEN ORDR_SUBTYPE_CD = 'PD' THEN 'Partial'
									ELSE ORDR_SUBTYPE_CD
								END AS [Disc Type],
								CUST_CMMT_DT   AS [CCD],
								m5.CREAT_DT   AS [M5 Create Date],
								PROD_NME AS [Product],
								CASE DOM_PROCESS_FLG_CD
									when 'N' Then 'INTL'
									when 'Y' THEN 'DMSTC'
									ELSE ''
								END AS [Intl/Dmstc] 
								  From openquery(m5,'select DISTINCT ord.ORDER_NBR,
															 ca.CUST_NME,
															 ca.CIS_CUST_ID,
															 ord.ORDR_SUBTYPE_CD,
															 ord.CUST_CMMT_DT,
															 ord.CREAT_DT,
															 ca.CTRY_CD,
															 ca.SITE_ID,
															 ord.PROD_NME,
															 ord.DOM_PROCESS_FLG_CD 
														from mach5.v_v5u_ordr ord 
													  inner join Mach5.V_V5U_CUST_ACCT ca on ca.CUST_ACCT_ID = ord.PRNT_ACCT_ID									
													  where ord.ORDR_TYPE_CD  like ''DC%''
														AND ord.DOM_PROCESS_FLG_CD  = ''Y''
														and ca.CIS_HIER_LVL_CD = ''H6''
														')m5
									Inner join COWS.dbo.LK_CTRY ctry with (nolock) ON m5.CTRY_CD = ctry.CTRY_CD
									WHERE ORDER_NBR NOT IN  (SELECT DISTINCT FTN FROM COWS.dbo.FSA_ORDR with (nolock))
										AND m5.CREAT_DT > @startDate and m5.CREAT_DT < @endDate
										AND ctry.CTRY_CD <> 'US'
 
			END
 
 
 
 END   
  
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_DiscOrdrNotInCowsButInM5 ' + ',' + CAST(@secured AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc   
 ---EXEC Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
  
  

GO 