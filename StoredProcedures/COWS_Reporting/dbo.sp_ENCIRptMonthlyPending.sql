USE [COWS_Reporting]
GO
/*
 --=============================================
 --Author:		<Author,,Name>
 --Create date: <Create Date,,>
 --Description:	<Description,,>
 -- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
 ----EXEC COWS_Reporting.dbo.sp_AMNCIRptPending 4, 0
 -- 1- CI554013: 01/10/2013 changed ONVERT(VARCHAR(10),xxx,101) to CONVERT(DATE,xxxx). To correct date range issue.
 -- [dbo].[sp_ENCIRptMonthlyPending] 0
 --=============================================
 */
ALTER PROCEDURE [dbo].[sp_ENCIRptMonthlyPending]
	    @secured			BIT = 0,
		@inStartDtStr  		VARCHAR(10)= '',
	    @inEndDtStr			VARCHAR(10)= ''

AS
BEGIN TRY
	SET NOCOUNT ON;
	DECLARE @startDate		DATETIME
	DECLARE @endDate		DATETIME
	DECLARE	@today			DATETIME
	DECLARE @dayOfWeek		INT
	
--DECLARE @secured		BIT = 0

	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0; 


	SELECT DISTINCT 
	a.FTN [FTN],
	f.ORDR_STUS_DES [Order Status],
	om.ORDR_BILL_CLEAR_INSTL_DT [Bill Clear Date],
	ckt.PL_SEQ_NBR [FMS PL Number],
	r.NUA_ADR [NUA],	
	j.ORDR_TYPE_DES [Order Type],
	d.PROD_TYPE_DES  [Product],
	a.TSUP_RFQ_NBR [RFQ Number],
	foc1.CUST_ID [H1 Customer Number],
	foc4.CUST_ID [H4 Customer Account],
	ord.H5_H6_CUST_ID [H5/H6 ID],
	fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES [Speed of Service],
	h.ACCS_MB_DES [Access MB Value],
	CASE
		WHEN @secured = 0 and ord.CSG_LVL_ID =0 THEN foc5.CUST_NME
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' 
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 AND csd1.CUST_NME IS NULL THEN NULL 
		ELSE coalesce(foc5.CUST_NME,CONVERT(varchar, DecryptByKey(csd1.CUST_NME)),'') END [Sprint Customer Name], --Secured
	CASE
		WHEN @secured = 0 and ord.CSG_LVL_ID =0 THEN oa.STREET_ADR_1
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' 
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 AND csd2.STREET_ADR_1 IS NULL THEN NULL 
		ELSE coalesce(oa.STREET_ADR_1,CONVERT(varchar, DecryptByKey(csd2.STREET_ADR_1)),'') END [Address], --Secured
	CASE
		WHEN @secured = 0 and ord.CSG_LVL_ID =0 THEN oa.CTY_NME
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' 
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 AND csd2.CTY_NME IS NULL THEN NULL 
		ELSE coalesce(oa.CTY_NME,CONVERT(varchar, DecryptByKey(csd2.CTY_NME)),'') END [City], --Secured
	CASE
		WHEN @secured = 0 and ord.CSG_LVL_ID =0 THEN oa.ZIP_PSTL_CD
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 THEN 'PRIVATE CUSTOMER' 
		WHEN @secured = 0 and ord.CSG_LVL_ID >0 AND csd2.ZIP_PSTL_CD IS NULL THEN NULL 
		ELSE coalesce(oa.ZIP_PSTL_CD,CONVERT(varchar, DecryptByKey(csd2.ZIP_PSTL_CD)),'') END [Zip], --Secured
	lc.CTRY_NME [H6 Country]
	
	FROM [COWS].[dbo].[ORDR] ord 
		INNER JOIN      [COWS].[dbo].[ORDR_ADR] oa WITH (NOLOCK) ON ord.ORDR_ID = oa.ORDR_ID AND oa.CIS_LVL_TYPE = ('H5')
		INNER JOIN      [COWS].[dbo].[LK_XNCI_RGN] i WITH (NOLOCK) ON ord.RGN_ID = i.RGN_ID AND i.RGN_ID = 2
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=oa.ORDR_ADR_ID  AND csd2.SCRD_OBJ_TYPE_ID=14
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] a WITH (NOLOCK) ON ord.ORDR_ID = a.ORDR_ID
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = a.ORDR_ID
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc1 WITH (NOLOCK) ON ord.ORDR_ID = foc1.ORDR_ID AND foc1.CIS_LVL_TYPE = 'H1'				
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc4 WITH (NOLOCK) ON ord.ORDR_ID = foc4.ORDR_ID AND foc4.CIS_LVL_TYPE = 'H4'
		LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CUST] foc5 WITH (NOLOCK) ON ord.ORDR_ID = foc5.ORDR_ID AND foc5.CIS_LVL_TYPE = 'H5'
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd1 WITH (NOLOCK) ON csd1.SCRD_OBJ_ID=foc5.FSA_ORDR_CUST_ID  AND csd1.SCRD_OBJ_TYPE_ID=5
		LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] d WITH (NOLOCK) ON a.PROD_TYPE_CD = d.FSA_PROD_TYPE_CD
		LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_STUS] f WITH (NOLOCK) ON ord.ORDR_STUS_ID = f.ORDR_STUS_ID
		LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] h WITH (NOLOCK) ON ord.ORDR_ID = h.ORDR_ID		
		LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] j WITH (NOLOCK) ON  a.ORDR_TYPE_CD = j.FSA_ORDR_TYPE_CD
		LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] r WITH (NOLOCK) ON a.FTN = r.FTN
		LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON r.TOC_CTRY_CD = l.CTRY_CD 
		LEFT OUTER JOIN [COWS].[dbo].[CKT] ckt WITH (NOLOCK) on ckt.ORDR_ID = ord.ORDR_ID
		LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] lc WITH (NOLOCK) ON oa.CTRY_CD = lc.CTRY_CD 
		LEFT OUTER JOIN [COWS].[dbo].[ORDR_MS] om WITH (NOLOCK) ON ord.ORDR_ID = om.ORDR_ID
		
	WHERE ord.ORDR_STUS_ID = 1 
	
	ORDER BY a.FTN	
							
	CLOSE SYMMETRIC KEY FS@K3y 
						
	RETURN 0;
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_ENCIRptMonthlyPending '  + ',' + CAST(@secured AS VARCHAR(4)) + ': '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH





