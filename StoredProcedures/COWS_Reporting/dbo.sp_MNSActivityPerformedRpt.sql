USE [COWS_Reporting]
GO
/*
--=========================================================================================
-- Project:			Maintenance 
-- Author:			Jagan	
-- Date:			01/27/2014
-- Description:		
--	EXECUTE: sp_MNSActivityPerformedRpt 0,'M'   <--For current date. If 'M' or 'W' used, it will be month current date to 1st of current month 
--															or weekly from current date.
--	sp_MNSActivityPerformedRpt 0,D,00/00/0000,99/99/9999  <-- for date range
--								security code, report duration, start date, end date
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--=========================================================================================

*/

ALTER Procedure [dbo].[sp_MNSActivityPerformedRpt] 
	@secured		BIT = 0,
	@rptCd			VARCHAR = '',
	@inStartDtStr  	VARCHAR(10)= '',
	@inEndDtStr  	VARCHAR(10)= ''
AS

BEGIN TRY
--DECLARE @secured		BIT = 0
--DECLARE @rptCd			VARCHAR = 'M'
--DECLARE @inStartDtStr  	VARCHAR(10)= ''
--DECLARE @inEndDtStr  	VARCHAR(10)= ''

DECLARE @startDate		DATETIME
DECLARE @endDate		DATETIME
	

IF @inStartDtStr = '' and @inEndDtStr = ''
BEGIN	
	IF @rptCd = 'D'
		BEGIN
		SET @endDate=CONVERT(VARCHAR(25),GETDATE(),101)
		SET @startDate=CONVERT(VARCHAR(25),DATEADD(DD, -1, @endDate),101)
		END
	IF @rptCd = 'W'
		BEGIN
		SET @endDate=CONVERT(VARCHAR(25),GETDATE(),101)
		SET @startDate=CONVERT(VARCHAR(25),DATEADD(DD, -7, @endDate),101)
		END
	IF @rptCd = 'M'
		BEGIN
		/*January Month Issue*/
		/*
		SET @endDate=CONVERT(VARCHAR(25),DATEADD(DD, -1, GETDATE()),101) 
		SET @startDate=CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(@endDate)-1),@endDate),101)
		SET @endDate=CONVERT(VARCHAR(25),GETDATE(),101)		/* Resets end date to include last day of month in report*/
		*/
		SET @startDate=DATEADD(mm,-1,CONVERT(VARCHAR(25),DATEADD(DD,-(DAY(GETDATE())-1),GETDATE()),101))
		SET @endDate=CONVERT(VARCHAR(25),GETDATE(),101) 
		END
END
ELSE
BEGIN
	SET @startDate=CONVERT(VARCHAR(25),@inStartDtStr,101)
	SET @endDate=CONVERT(VARCHAR(25),DATEADD(DD, 1,@inEndDtStr),101) /* The datadd is to include end date in report*/
END
--PRINT   @startDate
--PRINT   @endDate

OPEN SYMMETRIC KEY FS@K3y             
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;

SELECT DISTINCT
me.EVENT_ID as 'Event ID',
les.EVENT_STUS_DES as 'Event Status',
lws.WRKFLW_STUS_DES as 'WorkFlow Status',
CASE 
	--WHEN @Secured = 0 AND ev.SCURD_CD = 1 AND CONVERT(VARCHAR(100), DecryptByKey(me.CUST_NME)) IS NOT NULL 
	--THEN 'Private Customer'
	--ELSE ISNULL(CONVERT(VARCHAR(100), DecryptByKey(me.CUST_NME)),'')   
	WHEN @secured = 0 and ev.CSG_LVL_ID =0 THEN me.CUST_NME
	WHEN @secured = 0 and ev.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NOT NULL THEN 'PRIVATE CUSTOMER' 
	WHEN @secured = 0 and ev.CSG_LVL_ID >0 AND CONVERT(varchar, DecryptByKey(csd.CUST_NME)) IS NULL THEN NULL 
	ELSE COALESCE(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),me.CUST_NME,'') 
END [Customer Name],
me.STRT_TMST as 'Event Start Date',
CASE me.MDS_ACTY_TYPE_ID WHEN 3 THEN md.ODIE_DEV_NME ELSE  mma.ODIE_DEV_NME END as 'ODIE Device Name',
la.ACTN_DES as 'Action',
lu.FULL_NME as 'Performed By',
ehst.CREAT_DT as 'Performed Date',
lmat.MDS_ACTY_TYPE_DES as 'MDS Activity Type',
CASE WHEN lmat.MDS_ACTY_TYPE_ID IN (2,5) THEN lmat.MDS_ACTY_TYPE_DES + ' - ' + (dbo.[GetCommaSepMDSMACActys] (me.EVENT_ID)) ELSE lmat.MDS_ACTY_TYPE_DES END as 'MDS MAC Activity',
COALESCE(mma.WOOB_CD,'') as 'WOOB'
FROM [COWS].[dbo].[EVENT] ev  WITH (NOLOCK) INNER JOIN
       COWS.dbo.MDS_EVENT_NEW me WITH (NOLOCK) ON me.EVENT_ID = ev.EVENT_ID INNER JOIN
       COWS.dbo.EVENT_HIST ehst WITH (NOLOCK) ON me.EVENT_ID = ehst.EVENT_ID INNER JOIN
	   COWS.dbo.LK_ACTN la WITH (NOLOCK) ON la.ACTN_ID = ehst.ACTN_ID INNER JOIN
       COWS.dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID] = ehst.CREAT_BY_USER_ID INNER JOIN
       COWS.dbo.LK_EVENT_STUS les WITH (NOLOCK) ON les.EVENT_STUS_ID = me.EVENT_STUS_ID INNER JOIN
       COWS.dbo.LK_WRKFLW_STUS lws WITH (NOLOCK) ON lws.WRKFLW_STUS_ID = me.WRKFLW_STUS_ID INNER JOIN
       COWS.dbo.LK_MDS_ACTY_TYPE lmat WITH (NOLOCK) ON me.MDS_ACTY_TYPE_ID = lmat.MDS_ACTY_TYPE_ID LEFT JOIN
       COWS.dbo.MDS_MNGD_ACT_NEW mma WITH (NOLOCK) ON mma.EVENT_ID = me.EVENT_ID LEFT JOIN
       COWS.dbo.MDS_EVENT_DISCO md WITH (NOLOCK) ON md.EVENT_ID = me.EVENT_ID  LEFT OUTER JOIN 
       [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=me.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
WHERE ehst.CREAT_DT >= @startDate
       and ehst.CREAT_DT <= @endDate
  and me.EVENT_STUS_ID in (3, 4, 5, 6, 7, 9, 10) and (me.wrkflw_stus_id <> 3)
  and la.ACTN_ID not in (2,4,6,7,8,14,20)
ORDER BY me.EVENT_ID DESC, ehst.CREAT_DT DESC
		
  	RETURN 0;
  	  	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(100)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptPostSubmit '
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH
