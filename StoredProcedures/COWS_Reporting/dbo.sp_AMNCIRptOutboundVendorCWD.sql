USE [COWS_Reporting]
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: AMNCI Outbound Vendor CWD Monthly Canned Report  
-- Updated By:   Md M Monir
-- Updated Date: 03/14/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
-- =============================================  
  
  
--sp_AMNCIRptOutboundVendorCWD 3,1,'','',0  
  
ALTER PROCEDURE [dbo].[sp_AMNCIRptOutboundVendorCWD]  
  @QueryNumber  INT,  
     @secured   BIT = 0,  
  @inStartDtStr    VARCHAR(10)= '',  
     @inEndDtStr   VARCHAR(10)= '',  
  @missed    BIT = 0  
  
AS  
BEGIN TRY  
 SET NOCOUNT ON;  
 DECLARE @startDate  Datetime  
 DECLARE @endDate  Datetime  
 DECLARE @today   Datetime  
 DECLARE @startDtStr  VARCHAR(10)   
 DECLARE @dayOfWeek  INT  
   
 IF @QueryNumber=0  
  BEGIN  
   SET @startDate=CONVERT(DATETIME, @inStartDtStr, 101)  
   SET @endDate=CONVERT(DATETIME, @inEndDtStr, 101)      
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
   SET @endDate=DATEADD(DAY, 1, @endDate)  
  END  
 ELSE IF @QueryNumber=1  
     BEGIN       
      -- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000  
   SET @startDate=CAST(CONVERT(varchar(8), GETDATE(), 112) AS DATETIME)  
   SET @endDate=CAST(CONVERT(varchar(8), DATEADD(DAY, 1, GETDATE()), 112) AS DATETIME)     
  END  
 ELSE IF @QueryNumber=2  
  BEGIN  
   -- e.g. Run date is '2011-06-21' 3rd day of the week  
   -- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000  
   SET @today=GETDATE()  
   SET @dayOfWeek=DATEPART(dw, @today)  
   SET @startDate=CAST(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) AS DATETIME)  
   SET @endDate=CAST(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS DATETIME)  
  END  
 ELSE IF @QueryNumber=3  
  BEGIN      
     SET @today=DATEADD(MONTH,-1,GETDATE())  
      -- e.g. Run date is the 1st day of each month   
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000  
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));       
   SET @startDate=CAST(@startDtStr AS DATETIME)  
   SET @endDate=CAST(CONVERT(varchar(8), GETDATE(), 112) AS DATETIME)  
  END  
  
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;   
  
 CREATE TABLE #tempROV ([Order_Class] varchar(20),[OE/CK#] int,[Create_Date] varchar(16),[CUSTOMER] varchar(100),[_CCD] varchar(24),  
 [_CWD] varchar(24),[ACT/OPCD] varchar(24),[CD] varchar(24),[CWD Met] varchar(10),[CWD Met #] int,[Met All] int,[_ORDER_TYPE] varchar(24),  
 [Product] varchar(24),[Service] varchar(24),[Vendor] varchar(50),[DEST_COUNTRY] varchar(50),[PL#]  varchar(20),[ISS PM] varchar(50),  
 [Manager] varchar(50),[FE] varchar(20),[Complete_Month] varchar(24),[Status] nvarchar(255),[OE#] int,[Missed Reason] varchar(255),[Interval] int )  
   
  
 INSERT INTO #tempROV ([Order_Class],[OE/CK#],[Create_Date],[CUSTOMER],[_CCD],[_CWD],[ACT/OPCD],[CD],[CWD Met],[CWD Met #],[Met All]  
 ,[_ORDER_TYPE],[Product],[Service],[Vendor],[DEST_COUNTRY],[PL#],[ISS PM],[Manager],[FE],[Complete_Month],[Status],[OE#],[Missed Reason],[Interval])  
   
  
--DECLARE @secured BIT=0 -- for testing purposes only  
  
 SELECT DISTINCT  
  b.ORDR_CAT_DES [Order_Class],  
  a.ORDR_ID [OE/CK#],   
  CONVERT(VARCHAR(10),e.CREAT_DT,101) [Create_Date],   
  CASE  
	WHEN @secured = 0 AND m.CUST_NME<>''  THEN coalesce(m.CUST_NME,'PRIVATE CUSTOMER')
    WHEN @secured = 0 AND m.CUST_NME =''  THEN 'PRIVATE CUSTOMER'  
    WHEN @secured = 0 AND m.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER' 
    ELSE coalesce(CONVERT(varchar, DecryptByKey(csd.CUST_NME)),m.CUST_NME,'') 
   END [CUSTOMER], --Secured  
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),w.CUST_CMMT_DT,101)  
   WHEN i.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101)  
   WHEN i.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),t.CUST_CMMT_DT,101) END [_CCD],  
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),w.CUST_WANT_DT,101)  
   WHEN i.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10),t.CUST_WANT_DT,101)  
   WHEN i.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) END [_CWD],  
  CASE  
   WHEN i.ORDR_CAT_ID = 1 THEN CONVERT(VARCHAR(10),t2.CREAT_DT,101)  
   WHEN i.ORDR_CAT_ID = 6 THEN CONVERT(VARCHAR(10),tk.CREAT_DT,101)  
   WHEN i.ORDR_CAT_ID = 2 THEN CONVERT(VARCHAR(10),tk.CREAT_DT,101) END [ACT/OPCD],    
  CONVERT(VARCHAR(24),GETDATE(),120) [CD],   
  CASE  
   WHEN i.ORDR_CAT_ID = 1 AND CONVERT(VARCHAR(10),w.CUST_WANT_DT,101) >= CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 'CWD Met' --IPL  
   WHEN i.ORDR_CAT_ID = 1 AND CONVERT(VARCHAR(10),w.CUST_WANT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 'Missed'    
   WHEN i.ORDR_CAT_ID = 6 AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) >= CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 'CWD Met'  --Mach5  
   WHEN i.ORDR_CAT_ID = 6 AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 'Missed'  
   WHEN i.ORDR_CAT_ID = 2 AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) >= CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 'CWD Met'  --FSA  
   WHEN i.ORDR_CAT_ID = 2 AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 'Missed' END [CWD Met],   
  CASE  
   WHEN i.ORDR_CAT_ID = 1 AND CONVERT(VARCHAR(10),w.CUST_WANT_DT,101) >= CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 1   
   WHEN i.ORDR_CAT_ID = 1 AND CONVERT(VARCHAR(10),w.CUST_WANT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN 0   
   WHEN i.ORDR_CAT_ID = 6 AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) >= CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 1   
   WHEN i.ORDR_CAT_ID = 6 AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 0  
   WHEN i.ORDR_CAT_ID = 2 AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) >= CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 1   
   WHEN i.ORDR_CAT_ID = 2 AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN 0 END [CWD Met #],   
  1 [Met All],   
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN x.ORDR_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 6 THEN g.ORDR_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 2 THEN g.ORDR_TYPE_DES END [_ORDER_TYPE],   
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN z.PROD_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 6 THEN u.PROD_TYPE_DES  
   WHEN i.ORDR_CAT_ID = 2 THEN u.PROD_TYPE_DES END [Product],   
  CASE   
   WHEN i.ORDR_CAT_ID = 1 THEN w.SRVC_TYPE_CD  
   WHEN i.ORDR_CAT_ID = 6 THEN c.SRVC_INSTC_NME  
   WHEN i.ORDR_CAT_ID = 2 THEN c.SRVC_INSTC_NME END [Service],   
  v.VNDR_NME [Vendor],   
  l.CTRY_NME [DEST_COUNTRY],   
  c.PLN_NME [PL#],   
  o.FULL_NME [ISS PM],   
  p.FULL_NME [Manager],   
  'N/A' [FE],  
  
  SUBSTRING(CONVERT(VARCHAR(10), tk.CREAT_DT, 101),1,2) + '/01/' + CAST(YEAR(tk.CREAT_DT) AS VARCHAR(4)) [Complete_Month],   
  --CAST(right(convert(varchar(20),tk.CREAT_DT,06), len(convert(varchar(20),tk.CREAT_DT,06)) -3) AS VARCHAR(5)) [Complete_Month],  
  ls.STUS_DES [Status],  
  a.ORDR_ID [OE#],   
  q.CCD_MISSD_REAS_DES [Missed Reason],   
  CASE WHEN i.ORDR_CAT_ID = 1 AND x.ORDR_TYPE_DES='Install' AND CONVERT(VARCHAR(10),w.CUST_WANT_DT,101) < CONVERT(VARCHAR(10),t2.CREAT_DT,101) THEN   
    -- (DATEDIFF(dd,CONVERT(VARCHAR(10),w.CUST_WANT_DT,101),CONVERT(VARCHAR(10),t2.CREAT_DT,101)))  
    ---(DATEDIFF(wk,CONVERT(VARCHAR(10),w.CUST_WANT_DT,101),CONVERT(VARCHAR(10),t2.CREAT_DT,101)) * 2)  
    ---(CASE WHEN DATENAME(dw,CONVERT(VARCHAR(10),w.CUST_WANT_DT,101)) = 'Sunday' THEN 1 ELSE 0 END)     
    ---(CASE WHEN DATENAME(dw,CONVERT(VARCHAR(10),t2.CREAT_DT,101)) = 'Saturday' THEN 1 ELSE 0 END)  
    COWS_Reporting.dbo.getDaysInfo(2,CONVERT(VARCHAR(10),t2.CREAT_DT,101),CONVERT(VARCHAR(10),w.CUST_WANT_DT,101))   
  
   WHEN i.ORDR_CAT_ID = 6 AND g.ORDR_TYPE_DES='Install' AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN      
    -- (DATEDIFF(dd,CONVERT(VARCHAR(10),t.CUST_WANT_DT,101),CONVERT(VARCHAR(10),tk.CREAT_DT,101)))  
    ---(DATEDIFF(wk,CONVERT(VARCHAR(10),t.CUST_WANT_DT,101),CONVERT(VARCHAR(10),tk.CREAT_DT,101)) * 2)  
    ---(CASE WHEN DATENAME(dw,CONVERT(VARCHAR(10),t.CUST_WANT_DT,101)) = 'Sunday' THEN 1 ELSE 0 END)     
    ---(CASE WHEN DATENAME(dw,CONVERT(VARCHAR(10),tk.CREAT_DT,101)) = 'Saturday' THEN 1 ELSE 0 END)   
    COWS_Reporting.dbo.getDaysInfo(2,CONVERT(VARCHAR(10),tk.CREAT_DT,101),CONVERT(VARCHAR(10),t.CUST_WANT_DT,101))   
      
   WHEN i.ORDR_CAT_ID = 2 AND g.ORDR_TYPE_DES='Install' AND CONVERT(VARCHAR(10),t.CUST_WANT_DT,101) < CONVERT(VARCHAR(10),tk.CREAT_DT,101) THEN      
    -- (DATEDIFF(dd,CONVERT(VARCHAR(10),t.CUST_WANT_DT,101),CONVERT(VARCHAR(10),tk.CREAT_DT,101)))  
    ---(DATEDIFF(wk,CONVERT(VARCHAR(10),t.CUST_WANT_DT,101),CONVERT(VARCHAR(10),tk.CREAT_DT,101)) * 2)  
    ---(CASE WHEN DATENAME(dw,CONVERT(VARCHAR(10),t.CUST_WANT_DT,101)) = 'Sunday' THEN 1 ELSE 0 END)     
    ---(CASE WHEN DATENAME(dw,CONVERT(VARCHAR(10),tk.CREAT_DT,101)) = 'Saturday' THEN 1 ELSE 0 END)   
    COWS_Reporting.dbo.getDaysInfo(2,CONVERT(VARCHAR(10),tk.CREAT_DT,101),CONVERT(VARCHAR(10),t.CUST_WANT_DT,101))   
    ELSE 0 END [Interval]  
 FROM [COWS].[dbo].[VNDR_ORDR] e  
 LEFT OUTER JOIN [COWS].[dbo].[ORDR] a WITH (NOLOCK) ON e.VNDR_ORDR_ID = a.ORDR_ID  
 LEFT OUTER JOIN [COWS].[dbo].[ORDR] i WITH (NOLOCK) ON e.ORDR_ID = i.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[IPL_ORDR] w WITH (NOLOCK) ON e.ORDR_ID = w.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_CAT] b WITH (NOLOCK) ON a.ORDR_CAT_ID = b.ORDR_CAT_ID   
 LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] t WITH (NOLOCK) ON e.ORDR_ID = t.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] c WITH (NOLOCK) ON t.FTN = c.FTN   
 LEFT OUTER JOIN [COWS].[dbo].[LK_PPRT] d WITH (NOLOCK) ON a.PPRT_ID = d.PPRT_ID   
 LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] f WITH (NOLOCK) ON e.ORDR_ID = f.ORDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] g WITH (NOLOCK) ON t.ORDR_TYPE_CD = g.FSA_ORDR_TYPE_CD   
 LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] x WITH (NOLOCK) ON w.ORDR_TYPE_ID = x.ORDR_TYPE_ID   
 LEFT OUTER JOIN [COWS].[dbo].[ORDR_MS] h WITH (NOLOCK) ON e.ORDR_ID = h.ORDR_ID    
 LEFT OUTER JOIN [COWS].[dbo].[VNDR_FOLDR] j WITH (NOLOCK) ON  e.VNDR_FOLDR_ID = j.VNDR_FOLDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_VNDR] v WITH (NOLOCK) ON j.VNDR_CD = v.VNDR_CD    
 LEFT OUTER JOIN [COWS].[dbo].[ORDR_ADR] k WITH (NOLOCK) ON e.ORDR_ID =  k.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] l WITH (NOLOCK) ON k.CTRY_CD = l.CTRY_CD   
 LEFT OUTER JOIN [COWS].[dbo].[H5_FOLDR] m WITH (NOLOCK) ON a.H5_FOLDR_ID =  m.H5_FOLDR_ID 
 LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=m.H5_FOLDR_ID  AND csd.SCRD_OBJ_TYPE_ID=6  
 --LEFT OUTER JOIN [COWS].[dbo].[USER_WFM_ASMT] n WITH (NOLOCK) ON  e.ORDR_ID = n.ORDR_ID   
 LEFT OUTER JOIN (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID   
   FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)  
   LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT)  
   FROM (SELECT DISTINCT ASMT_DT, ORDR_ID FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr    
   WHERE usr.ORDR_ID = bb.ORDR_ID)  
    AND bb.ASN_USER_ID = (SELECT MAX(ASN_USER_ID)   
    FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID   
    FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas  
    WHERE uas.ORDR_ID = bb.ORDR_ID)) ur   
    on  e.ORDR_ID = ur.ORDR_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_USER] o WITH (NOLOCK) ON ur.ASN_USER_ID = o.USER_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_USER] p WITH (NOLOCK) ON o.MGR_ADID = p.USER_ADID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_CCD_MISSD_REAS]q WITH (NOLOCK) ON q.CCD_MISSD_REAS_ID = f.CCD_MISSD_REAS_ID   
 --LEFT OUTER JOIN [COWS].[dbo].[CKT] s WITH (NOLOCK) ON e.ORDR_ID = s.ORDR_ID   
 --LEFT OUTER JOIN [COWS].[dbo].[CKT_COST] r WITH (NOLOCK) ON  s.CKT_ID = r.CKT_ID   
 --LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] t WITH (NOLOCK) ON d.PROD_TYPE_ID = t.PROD_TYPE_ID   
 LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] u WITH (NOLOCK) ON t.PROD_TYPE_CD = u.FSA_PROD_TYPE_CD   
 LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] z WITH (NOLOCK) ON w.PROD_TYPE_ID = z.PROD_TYPE_ID  
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.SOI_CD, CONVERT(VARCHAR, DecryptByKey(bb.CUST_NME)) [CUST_NME]  
  FROM [COWS].[dbo].[FSA_ORDR_CUST] bb  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)  
   FROM (SELECT SOI_CD, ORDR_ID   
    FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi  
    WHERE soi.ORDR_ID = bb.ORDR_ID)) si ON e.ORDR_ID = si.ORDR_ID    
 LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID,bb.STUS_ID    
  FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
   FROM (SELECT ACT_TASK_ID, ORDR_ID   
    FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
    WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk on tk.ORDR_ID = e.ORDR_ID   
  
 left outer join [COWS].[dbo].[LK_STUS] ls on ls.STUS_ID = tk.STUS_ID  
   
 LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID,bb.STUS_ID    
  FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)  
   FROM (SELECT ACT_TASK_ID, ORDR_ID   
    FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk   
    WHERE tsk.ORDR_ID = bb.ORDR_ID)) t2 on t2.ORDR_ID = i.ORDR_ID   
   
   
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.ROLE_ID, CONVERT(varchar, DecryptByKey(bb.FRST_NME)) [FRST_NME],   
  CONVERT(varchar, DecryptByKey(bb.LST_NME)) [LST_NME], CONVERT(varchar,DecryptByKey(bb.EMAIL_ADR)) [EMAIL_ADR]   
  FROM [COWS].[dbo].[ORDR_CNTCT] bb WITH (NOLOCK)  
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
  WHERE bb.ROLE_ID = 13) gom ON gom.ORDR_ID = e.ORDR_ID  
     
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 LEFT OUTER JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID WHERE bb.BILL_ITEM_TYPE_ID = 6) cp on e.ORDR_ID = cp.ORDR_ID   
 LEFT OUTER JOIN (SELECT bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)  
 LEFT OUTER JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID WHERE bb.BILL_ITEM_TYPE_ID = 5) ca on e.ORDR_ID = ca.ORDR_ID  
  
  --LEFT OUTER JOIN (SELECT bb.CKT_ID, bb.ACCS_CUST_MRC_IN_USD_AMT, bb.ASR_MRC_IN_USD_AMT, bb.VNDR_MRC_IN_USD_AMT, bb.ACCS_CUST_NRC_IN_USD_AMT, bb.ASR_NRC_IN_USD_AMT, bb.VNDR_NRC_IN_USD_AMT, bb.VER_ID  
  --   FROM [COWS].[dbo].[CKT_COST] bb LEFT OUTER JOIN  
   --[COWS].[dbo].[CKT_COST] cc on bb.CKT_ID = cc.CKT_ID   
   --WHERE bb.VER_ID = (SELECT MAX(VER_ID)  
   -- FROM (SELECT VER_ID, CKT_ID FROM [COWS].[dbo].[CKT_COST] ) ckt  
   -- WHERE ckt.CKT_ID = bb.CKT_ID)) ct on  s.CKT_ID = ct.CKT_ID  
     
 WHERE b.ORDR_CAT_ID = 3 -- vendor category  
  AND (tk.TASK_ID = 1001 OR t2.TASK_ID = 1001)  --Completed FSA and IPL orders  
  AND k.ADR_TYPE_ID IN (6,12,18)  --Site Adress   
  AND (x.ORDR_TYPE_DES IN ('Disconnect','Install') OR g.ORDR_TYPE_DES IN ('Disconnect','Install'))  
  AND e.CREAT_DT >= @startDate AND e.CREAT_DT < @endDate  
  
--AND a.RGN_ID = 1  
  
 INSERT INTO [COWS_Reporting].[dbo].[AMNCIOUTBOUNDVENDORCWDRPTDATA]  
           ([ORDER_CLASS]  
           ,[OE_CK#]  
           ,[CREATE_DATE]  
           ,[CUSTOMER]  
           ,[_CCD]  
           ,[_CWD]  
           ,[ACT_OPCD]  
           ,[CD]  
           ,[CWD_MET]  
           ,[CWD_MET_#]  
           ,[MET_ALL]  
           ,[_ORDER_TYPE2]  
           ,[PRODUCT]  
           ,[SERVICE]  
           ,[VENDOR]  
           ,[DEST_COUNTRY]  
           ,[PL#]  
           ,[ISS_PM]  
           ,[MANAGER]  
           ,[FE]  
           ,[COMPLETE_MONTH]  
           ,[STATUS]  
           ,[OE#]  
           ,[MISSED_REASON]  
           ,[INTERVAL])  
 SELECT [ORDER_CLASS]  
           ,[OE/CK#]   
           ,[CREATE_DATE]  
           ,[CUSTOMER]  
           ,[_CCD]  
           ,[_CWD]  
      ,[ACT/OPCD]  
           ,[CD]  
           ,[CWD Met]   
           ,[CWD Met #]              
           ,[Met All]   
           ,[_ORDER_TYPE]  
           ,[PRODUCT]  
           ,[SERVICE]  
           ,[VENDOR]  
           ,[DEST_COUNTRY]  
           ,[PL#]  
           ,[ISS PM]   
           ,[MANAGER]  
           ,[FE]  
           ,[COMPLETE_MONTH]  
           ,[STATUS]  
           ,[OE#]  
           ,[Missed Reason]  
           ,[INTERVAL] FROM #tempROV t1   
 WHERE NOT EXISTS (SELECT * FROM [COWS_Reporting].[dbo].[AMNCIOUTBOUNDVENDORCWDRPTDATA] t2  
  WHERE t1.[Order_Class]=t2.[Order_Class] AND t1.[OE/CK#]=t2.[OE_CK#] AND t1.[Create_Date]=t2.[Create_Date] AND t1.[CUSTOMER]=t2.[CUSTOMER]  
  AND t1.[_CCD]=t2.[_CCD] AND t1.[_CWD]=t2.[_CWD] AND t1.[ACT/OPCD]=t2.[ACT_OPCD] AND t1.[CD]=t2.[CD] AND t1.[CWD Met]=t2.[CWD_MET]   
  AND t1.[CWD Met #]=t2.[CWD_MET_#] AND t1.[Met All]=t2.[MET_ALL] AND t1.[_ORDER_TYPE]=t2.[_ORDER_TYPE2] AND t1.[Product]=t2.[Product]   
  AND t1.[Service]=t2.[Service] AND t1.[Vendor]=t2.[Vendor] AND t1.[DEST_COUNTRY]=t2.[DEST_COUNTRY] AND t1.[PL#]=t2.[PL#] AND t1.[ISS PM]=t2.[ISS_PM]  
  AND t1.[Manager]=t2.[Manager] AND t1.[FE]=t2.[FE] AND t1.[Complete_Month]=t2.[Complete_Month] AND t1.[OE#]=t2.[OE#] AND t1.[Missed Reason]=t2.[MISSED_REASON]  
  AND t1.[Status]= t2.[Status] AND t1.[Interval]=t2.[Interval])  
    
  
 IF @missed <> 0  
  BEGIN   
   SELECT [Order_Class],[OE/CK#],[Create_Date],[CUSTOMER],[_CCD],[_CWD],[ACT/OPCD],[CD],[CWD Met],[CWD Met #],[Met All],[_ORDER_TYPE],  
    [Product],[Service],[Vendor],[DEST_COUNTRY],[PL#],[ISS PM],[Manager],[FE],[Complete_Month],[Interval]   
   FROM #tempROV  
   WHERE [CWD Met] = 'Missed' AND [_ORDER_TYPE]='Install'  
  END  
 ELSE  
  BEGIN  
   SELECT [Order_Class],[OE/CK#],[Create_Date],[CUSTOMER],[_CCD],[_CWD],[ACT/OPCD],[CD],[CWD Met],[CWD Met #],[Met All],[_ORDER_TYPE],  
    [Product],[Service],[Vendor],[DEST_COUNTRY],[PL#],[ISS PM],[Manager],[FE],[Complete_Month],[Status],[OE#],[Missed Reason]  
   FROM #tempROV  
  END  
   
 DROP TABLE #tempROV  
     
 CLOSE SYMMETRIC KEY FS@K3y   
        
 RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptOutboundVendorCWD1 '  + CAST(@QueryNumber AS VARCHAR(4)) + ',' + CAST(@secured AS VARCHAR(4)) + ': '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  
 