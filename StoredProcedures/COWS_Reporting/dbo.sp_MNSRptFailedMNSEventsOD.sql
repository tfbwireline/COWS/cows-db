USE [COWS_Reporting]
GO
/*
--=======================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			07/22/2011
-- Description:		
--					Extract MDS & MDS Fasttrack events that failed with a count of  
--					how many times the event has been worked for a specified parameter(s);
--					Customer Name, h5_h6_id, Date Range 
--
-- Modifications:
--
-- 09/23/2011		sxv0766: Added @h5_h6_ID, @getSensData
-- 01/24/2012		sxv0766: Specified VARCHAR size of 100 for Cust Name decryption
-- 02/16/2012		sxv0766: Added Reschedule Failed ACTN ID from 31 to 43
-- 02/21/2012		sxv0766: Increased Comments field size to 4000 chars
-- 03/29/2012		sxv0766: Increased field sizes:Comments to VARCHAR(MAX), Action to 250
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--=======================================================================================
*/

ALTER Procedure [dbo].[sp_MNSRptFailedMNSEventsOD]   
 @SecuredUser  INT=0,  
 @getSensData  CHAR(1)='N',  
 @startDate   Datetime=NULL,  
 @endDate   Datetime=NULL    
AS  
BEGIN TRY  
  
 SET NOCOUNT ON;  
    
 DECLARE @startDateStr VARCHAR(20)  
 DECLARE @endDateStr  VARCHAR(20)    
   
	SET @startDate=CONVERT(DATETIME, @startDate, 101)  
    SET @endDate=CONVERT(DATETIME, @endDate, 101)      
    -- Add 1 day to End Date in order to cover the End Date 24-hr period  
    SET @endDate=DATEADD(DAY, 1, @endDate)  
   
 DECLARE  @tmp_mns_failed  TABLE 
 (       
  Event_Id    INT,  
  Action     VARCHAR(250),  
  Event     VARCHAR(25),  
  Date_And_Time   VARCHAR(25),  
  Event_Count    INT,    
  Cust_Name    VARCHAR(100), 
  Cust_Name2			VARCHAR(100), 
  Scurd_Cd    Bit,  
  Assigned_MNS_PM   VARCHAR(100),  
  Author     VARCHAR(100),  
  Editor     VARCHAR(100),  
  Performed_By   VARCHAR(100),  
  Comments    VARCHAR(MAX),  
  Fail_Code    VARCHAR(100),  
  Fail_Acty    VARCHAR(100)  
 )  
   
 OPEN SYMMETRIC KEY FS@K3y   
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;  
   
 -- MDS Activity Type: Initial Install and MAC  
       
   INSERT INTO @tmp_mns_failed(Event_Id, Action, Event, Date_And_Time, Event_Count, Cust_Name, Scurd_Cd,   
   Assigned_MNS_PM, Author, Editor, Performed_By, Comments, Fail_Code, Fail_Acty )  
   SELECT mds.EVENT_ID ,  
   actn.ACTN_DES ,  
   CASE mds.MDS_FAST_TRK_CD  
    WHEN 1 THEN 'MDS Fast Track'  
    WHEN 0 THEN 'MDS' END ,  
   REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8)   
    + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) ,      
   cntEvnt.totCnt ,  
	CASE 						
		WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
		ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
						
	CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd , 
   CASE  
    WHEN mds.MNS_PM_ID is NULL THEN ''     
    ELSE luser.DSPL_NME END ,  
   luser1.DSPL_NME ,  
   CASE  
    WHEN evhi.MODFD_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END ,  
   CASE  
    WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''  
    ELSE luser3.DSPL_NME END ,  
   CASE  
    WHEN evhi.CMNT_TXT is NULL THEN ''  
    ELSE evhi.CMNT_TXT END ,  
   CASE  
    WHEN evhi.FAIL_REAS_ID is NULL THEN ''      
    ELSE lfrs.FAIL_REAS_DES END ,  
   CASE  
    WHEN lfac.FAIL_ACTY_ID is NULL THEN ''  
    ELSE lfac.FAIL_ACTY_DES END       
 FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
  LEFT JOIN ( SELECT EVENT_ID, COUNT(EVENT_ID) as totCnt  
     FROM COWS.dbo.EVENT_HIST with (nolock)  
        WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43)  
        GROUP BY EVENT_ID)cntEvnt ON mds.EVENT_ID = cntEvnt.EVENT_ID    
  LEFT JOIN ( SELECT EVENT_HIST_ID, EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, FAIL_REAS_ID   
     FROM COWS.dbo.EVENT_HIST with (nolock)  
     WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID          
  LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID = luser.USER_ADID     
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.MODFD_BY_USER_ID = luser2.USER_ID    
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID   
     LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID  
  LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID  
  LEFT JOIN COWS.dbo.EVENT_FAIL_ACTY evfa with (nolock) ON evhi.EVENT_HIST_ID = evfa.EVENT_HIST_ID  
  LEFT JOIN COWS.dbo.LK_FAIL_ACTY lfac with (nolock) ON evfa.FAIL_ACTY_ID = lfac.FAIL_ACTY_ID    
 WHERE mds.MDS_ACTY_TYPE_ID in (1, 2) AND mds.EVENT_STUS_ID in (3)     
       AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT)  AND evhi.CREAT_DT <COALESCE(@endDate,evhi.CREAT_DT)   
 -- MDS Activity Type: Disconnect  
   UNION
 SELECT mds.EVENT_ID ,  
   actn.ACTN_DES ,  
   CASE mds.MDS_FAST_TRK_CD  
    WHEN 1 THEN 'MDS Fast Track'  
    WHEN 0 THEN 'MDS' END ,  
   REPLACE(SUBSTRING(CONVERT(char, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8)   
    + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) ,     
   cntEvnt.totCnt ,  
	CASE 						
		WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
		ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
						
	CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,   
   CASE  
    WHEN mds.MNS_PM_ID is NULL THEN ''     
    ELSE luser.DSPL_NME END ,  
   luser1.DSPL_NME ,  
   CASE  
    WHEN evhi.MODFD_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END ,  
   CASE  
    WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''  
    ELSE luser3.DSPL_NME END ,  
   CASE  
    WHEN evhi.CMNT_TXT is NULL THEN ''  
    ELSE evhi.CMNT_TXT END ,  
   CASE  
    WHEN evhi.FAIL_REAS_ID is NULL THEN ''      
    ELSE lfrs.FAIL_REAS_DES END ,  
   CASE  
    WHEN lfac.FAIL_ACTY_ID is NULL THEN ''  
    ELSE lfac.FAIL_ACTY_DES END        
 FROM COWS.dbo.MDS_EVENT_NEW mds with (nolock)   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
  LEFT JOIN ( SELECT EVENT_ID, COUNT(EVENT_ID) as totCnt  
     FROM COWS.dbo.EVENT_HIST with (nolock)  
        WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43)  
        GROUP BY EVENT_ID)cntEvnt ON mds.EVENT_ID = cntEvnt.EVENT_ID    
  LEFT JOIN ( SELECT EVENT_HIST_ID, EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, FAIL_REAS_ID   
     FROM COWS.dbo.EVENT_HIST with (nolock)  
     WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID    
  LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID = luser.USER_ADID     
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.MODFD_BY_USER_ID = luser2.USER_ID    
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID   
     LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID  
  LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID  
  LEFT JOIN COWS.dbo.EVENT_FAIL_ACTY evfa with (nolock) ON evhi.EVENT_HIST_ID = evfa.EVENT_HIST_ID  
  LEFT JOIN COWS.dbo.LK_FAIL_ACTY lfac with (nolock) ON evfa.FAIL_ACTY_ID = lfac.FAIL_ACTY_ID    
 WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (3)    
       AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT)  AND evhi.CREAT_DT <COALESCE(@endDate,evhi.CREAT_DT) 
  
  
 UNION ALL
 
 SELECT mds.EVENT_ID ,  
   actn.ACTN_DES ,  
   CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END ,  
   REPLACE(SUBSTRING(CONVERT(varchar, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8)   
    + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) ,      
   cntEvnt.totCnt ,  

	CASE 						
		WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
		ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
						
	CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd , 
   CASE  
    WHEN mds.MNS_PM_ID is NULL THEN ''     
    ELSE luser.DSPL_NME END ,  
   luser1.DSPL_NME ,  
   CASE  
    WHEN evhi.MODFD_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END ,  
   CASE  
    WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''  
    ELSE luser3.DSPL_NME END ,  
   CASE  
    WHEN evhi.CMNT_TXT is NULL THEN ''  
    ELSE evhi.CMNT_TXT END ,  
   CASE  
    WHEN evhi.FAIL_REAS_ID is NULL THEN ''      
    ELSE lfrs.FAIL_REAS_DES END ,  
   CASE  
    WHEN lfac.FAIL_ACTY_ID is NULL THEN ''  
    ELSE lfac.FAIL_ACTY_DES END       
 FROM COWS.dbo.MDS_EVENT mds with (nolock)   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
  LEFT JOIN ( SELECT EVENT_ID, COUNT(EVENT_ID) as totCnt  
     FROM COWS.dbo.EVENT_HIST with (nolock)  
        WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43)  
        GROUP BY EVENT_ID)cntEvnt ON mds.EVENT_ID = cntEvnt.EVENT_ID    
  LEFT JOIN ( SELECT EVENT_HIST_ID, EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, FAIL_REAS_ID   
     FROM COWS.dbo.EVENT_HIST with (nolock)  
     WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID          
  LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID = luser.USER_ADID     
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.MODFD_BY_USER_ID = luser2.USER_ID    
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID   
     LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID  
  LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID  
  LEFT JOIN COWS.dbo.EVENT_FAIL_ACTY evfa with (nolock) ON evhi.EVENT_HIST_ID = evfa.EVENT_HIST_ID  
  LEFT JOIN COWS.dbo.LK_FAIL_ACTY lfac with (nolock) ON evfa.FAIL_ACTY_ID = lfac.FAIL_ACTY_ID    
 WHERE mds.MDS_ACTY_TYPE_ID in (1, 2) AND mds.EVENT_STUS_ID in (3)     
       AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT)  AND evhi.CREAT_DT <COALESCE(@endDate,evhi.CREAT_DT)   
 -- MDS Activity Type: Disconnect  
  UNION 
 SELECT mds.EVENT_ID ,  
   actn.ACTN_DES ,  
   CASE mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS' END ,  
   REPLACE(SUBSTRING(CONVERT(char, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8)   
    + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) ,     
   cntEvnt.totCnt ,  

	CASE 						
		WHEN CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) is NULL THEN ''
		ELSE CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)) END ,
						
	CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,   
   CASE  
    WHEN mds.MNS_PM_ID is NULL THEN ''     
    ELSE luser.DSPL_NME END ,  
   luser1.DSPL_NME ,  
   CASE  
    WHEN evhi.MODFD_BY_USER_ID is NULL THEN ''  
    ELSE luser2.DSPL_NME END ,  
   CASE  
    WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''  
    ELSE luser3.DSPL_NME END ,  
   CASE  
    WHEN evhi.CMNT_TXT is NULL THEN ''  
    ELSE evhi.CMNT_TXT END ,  
   CASE  
    WHEN evhi.FAIL_REAS_ID is NULL THEN ''      
    ELSE lfrs.FAIL_REAS_DES END ,  
   CASE  
    WHEN lfac.FAIL_ACTY_ID is NULL THEN ''  
    ELSE lfac.FAIL_ACTY_DES END        
 FROM COWS.dbo.MDS_EVENT mds with (nolock)   
  JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
  LEFT JOIN ( SELECT EVENT_ID, COUNT(EVENT_ID) as totCnt  
     FROM COWS.dbo.EVENT_HIST with (nolock)  
        WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43)  
        GROUP BY EVENT_ID)cntEvnt ON mds.EVENT_ID = cntEvnt.EVENT_ID    
  LEFT JOIN ( SELECT EVENT_HIST_ID, EVENT_ID, ACTN_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT, FAIL_REAS_ID   
     FROM COWS.dbo.EVENT_HIST with (nolock)  
     WHERE (ACTN_ID = 15 OR ACTN_ID between 31 and 43) )as evhi ON mds.EVENT_ID = evhi.EVENT_ID    
  LEFT JOIN COWS.dbo.LK_USER luser with (nolock) ON mds.MNS_PM_ID = luser.USER_ADID     
  JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID   
  LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.MODFD_BY_USER_ID = luser2.USER_ID    
  LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON evhi.CREAT_BY_USER_ID = luser3.USER_ID   
     LEFT JOIN COWS.dbo.LK_ACTN actn with (nolock) ON evhi.ACTN_ID = actn.ACTN_ID  
  LEFT JOIN COWS.dbo.LK_FAIL_REAS lfrs with (nolock) ON evhi.FAIL_REAS_ID = lfrs.FAIL_REAS_ID  
  LEFT JOIN COWS.dbo.EVENT_FAIL_ACTY evfa with (nolock) ON evhi.EVENT_HIST_ID = evfa.EVENT_HIST_ID  
  LEFT JOIN COWS.dbo.LK_FAIL_ACTY lfac with (nolock) ON evfa.FAIL_ACTY_ID = lfac.FAIL_ACTY_ID    
 WHERE mds.MDS_ACTY_TYPE_ID = 3 AND mds.EVENT_STUS_ID in (3)    
       AND evhi.CREAT_DT >=COALESCE(@startDate,evhi.CREAT_DT)  AND evhi.CREAT_DT <COALESCE(@endDate,evhi.CREAT_DT)    
 
  
 SELECT Event_Id 'Event Id',  
   Action,  
   Event,  
   Date_And_Time 'Date And Time',  
   Event_Count 'Count Of Event Id',  
   CASE   
    --WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'  
    --WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'  
    --ELSE Cust_Name 
    WHEN @SecuredUser = 0 and Scurd_Cd =0 THEN CUST_NAME
	WHEN @SecuredUser = 0 and Scurd_Cd >0 AND  CUST_NAME2 <>'' THEN 'PRIVATE CUSTOMER' 
	WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd >0 AND CUST_NAME2 <> '' THEN 'Private Customer'
	ELSE COALESCE(CUST_NAME2,CUST_NAME,'')
    END 'Customer Name',     
   Assigned_MNS_PM 'Assigned Acct MNS PM',  
   Author,  
   Editor,  
   Performed_By 'Performed By',  
   Comments,  
   Fail_Code 'Fail Codes',  
   Fail_Acty 'Failed Activities'  
   
 FROM @tmp_mns_failed  
    
  
   
 CLOSE SYMMETRIC KEY FS@K3y;  
   
 RETURN 0;  
   
END TRY  
  
BEGIN CATCH  
 DECLARE @Desc VARCHAR(200)  
 SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptFailedMNSEventsOD, '    
 SET @Desc=@Desc + CAST(@SecuredUser AS VARCHAR(2)) + ', ''' + @getSensData + ''', '  
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)  
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;  
 RETURN 1;  
END CATCH  


