USE [COWS_Reporting]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[sp_MNSRptPMEventPublishCountOD]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[sp_MNSRptPMEventPublishCountOD]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*
--==================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			07/12/2011
-- Description:		
--					Extract MDS events published by PM for specified parameter(s);
--					Customer Name, H5_H6_ID, and Date Range.
--
-- Notes:			EVENT_STUS_ID: 4 = Published
--					ACTN_ID: 5 = Publish, 16 = Event Re-published
--
-- 09/26/2011		sxv0766: Added @h5_h6_ID, @getSensData
-- 11/09/2011		sxv0766: Use value of FSA_MDS_EVENT.TAB_NME as H5 value
-- 11/15/2011		sxv0766: Added @SecuredUser
-- 03/29/2012		sxv0766: Changed Comments field size to VARCHAR(MAX)
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD
--==================================================================================
*/
CREATE Procedure [dbo].[sp_MNSRptPMEventPublishCountOD]
	@SecuredUser		INT=0,
	@getSensData		CHAR(1)='N',	
	@startDate			Datetime=NULL,
	@endDate			Datetime=NULL
AS
BEGIN TRY

	SET NOCOUNT ON;
	
	DECLARE @startDateStr	VARCHAR(20)
	DECLARE @endDateStr		VARCHAR(20)	
	
    SET @startDate=CONVERT(datetime, @startDate, 101)  
    SET @endDate=CONVERT(datetime, @endDate, 101)      
  
   -- Add 1 day to End Date in order to cover the End Date 24-hr period  
    SET @endDate=DATEADD(day, 1, @endDate)
	
	CREATE TABLE #tmp_mns_pmPub
	(					
		Event_Type				VARCHAR(25),
		Event_Id				INT,
		Event_Stus				VARCHAR(50),
		Modified_Date			VARCHAR(25),
		Event_Date				VARCHAR(25),
		Event_Submitted_On		VARCHAR(25),
		Cust_Name				VARCHAR(100),
		Cust_Name2				VARCHAR(100),
		Scurd_Cd				Bit,
		MDS_Acty_Type			VARCHAR(25),
		MDS_Inst_Acty			VARCHAR(100),
		MDS_Mac_Acty			VARCHAR(100),
		Escalations				VARCHAR(5),
		Comments				VARCHAR(MAX),
		Author					VARCHAR(100),
		Editor					VARCHAR(100),
		Assigned_MNS_PM			VARCHAR(100)
	)
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
	
	-- MDS Activity Type: Initial Install and MAC 
	
 	INSERT INTO #tmp_mns_pmPub(Event_Type, Event_Id, Event_Stus, Modified_Date, Event_Date, Event_Submitted_On,
 				Cust_Name, Scurd_Cd, MDS_Acty_Type, MDS_Inst_Acty, MDS_Mac_Acty, Escalations, Comments, Author, Editor, Assigned_MNS_PM)
 			SELECT CASE mds.MDS_FAST_TRK_CD
				WHEN 1 THEN 'MDS Fast Track'
				WHEN 0 THEN 'MDS Event' END ,
			mds.EVENT_ID ,
			UPPER(evst.EVENT_STUS_DES) ,
			CASE
				WHEN evhi.CREAT_DT is NULL THEN ''
				ELSE REPLACE(SUBSTRING(CONVERT(char, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) END ,
			CASE
				WHEN mds.STRT_TMST is NULL THEN ''
				ELSE REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) END ,
			REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) ,
			COALESCE(CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)),mds.CUST_NME) ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,				
			mact.MDS_ACTY_TYPE_DES ,
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END ,	
			CASE 
				WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
				ELSE maac.MDS_MAC_ACTY_NME END ,				
			CASE mds.ESCL_CD
				WHEN 1 THEN 'Yes'
				ELSE 'No' END ,
			CASE
				WHEN evhi.CMNT_TXT is NULL THEN ''
				ELSE evhi.CMNT_TXT END ,
			luser1.DSPL_NME ,
			CASE
				WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
				ELSE luser2.DSPL_NME END ,
			CASE				
				WHEN luser3.DSPL_NME is NULL THEN ''
				ELSE luser3.DSPL_NME END			
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)			
		JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds with (nolock) ON mds.EVENT_ID = fmds.EVENT_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID				
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 		
		LEFT JOIN ( SELECT EVENT_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE ACTN_ID in ( 5, 16) ) evhi ON mds.EVENT_ID = evhi.EVENT_ID 
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID		
		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID		
	WHERE mds.MDS_ACTY_TYPE_ID <> 3 	 AND evhi.CREAT_DT >= COALESCE(@startDate,evhi.CREAT_DT) AND evhi.CREAT_DT < COALESCE(@endDate,evhi.CREAT_DT)			
	--WHERE mds.EVENT_STUS_ID = 4 AND mds.MDS_ACTY_TYPE_ID <> 3 '
	UNION
	-- MDS Activity Type: Disconnect
	
 	SELECT CASE mds.MDS_FAST_TRK_CD
				WHEN 1 THEN 'MDS Fast Track'
				WHEN 0 THEN 'MDS Event' END ,
			mds.EVENT_ID ,
			UPPER(evst.EVENT_STUS_DES) ,
			CASE
				WHEN evhi.CREAT_DT is NULL THEN ''
				ELSE REPLACE(SUBSTRING(CONVERT(char, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) END ,
			CASE
				WHEN mds.STRT_TMST is NULL THEN ''
				ELSE REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) END ,
			REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) ,
			COALESCE(CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)),mds.CUST_NME) ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,			
			mact.MDS_ACTY_TYPE_DES ,
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'
				ELSE '' END ,		
			CASE 
				WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
				ELSE maac.MDS_MAC_ACTY_NME END ,				
			CASE mds.ESCL_CD
				WHEN 1 THEN 'Yes'
				ELSE 'No' END ,			
			CASE
				WHEN evhi.CMNT_TXT is NULL THEN ''
				ELSE evhi.CMNT_TXT END ,
			luser1.DSPL_NME ,
			CASE
				WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
				ELSE luser2.DSPL_NME END ,
			CASE
				WHEN luser3.DSPL_NME is NULL THEN ''
				ELSE luser3.DSPL_NME END 			
	FROM	COWS.dbo.MDS_EVENT_NEW mds with (nolock)	
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID				
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 		
		LEFT JOIN ( SELECT EVENT_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT  
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE ACTN_ID in ( 5, 16) ) evhi ON mds.EVENT_ID = evhi.EVENT_ID 
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID
		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID
	WHERE mds.MDS_ACTY_TYPE_ID = 3  AND evhi.CREAT_DT >= COALESCE(@startDate,evhi.CREAT_DT) AND evhi.CREAT_DT < COALESCE(@endDate,evhi.CREAT_DT)		
	UNION ALL
	--WHERE mds.EVENT_STUS_ID = 4 AND mds.MDS_ACTY_TYPE_ID = 3 '
		
	SELECT CASE 
				mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS Event' END 'Event Type',
			mds.EVENT_ID ,
			UPPER(evst.EVENT_STUS_DES) ,
			CASE
				WHEN evhi.CREAT_DT is NULL THEN ''
				ELSE REPLACE(SUBSTRING(CONVERT(char, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) END ,
			CASE
				WHEN mds.STRT_TMST is NULL THEN ''
				ELSE REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) END ,
			REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) ,
			COALESCE(CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)),mds.CUST_NME) ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,				
			mact.MDS_ACTY_TYPE_DES ,
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation' 
				ELSE '' END ,	
			CASE 
				WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
				ELSE maac.MDS_MAC_ACTY_NME END ,				
			CASE mds.ESCL_CD
				WHEN 1 THEN 'Yes'
				ELSE 'No' END ,
			CASE
				WHEN evhi.CMNT_TXT is NULL THEN ''
				ELSE evhi.CMNT_TXT END ,
			luser1.DSPL_NME ,
			CASE
				WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
				ELSE luser2.DSPL_NME END ,
			CASE				
				WHEN luser3.DSPL_NME is NULL THEN ''
				ELSE luser3.DSPL_NME END			
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)			
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID				
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 		
		LEFT JOIN ( SELECT EVENT_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT 
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE ACTN_ID in ( 5, 16) ) evhi ON mds.EVENT_ID = evhi.EVENT_ID 
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID		
		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID		
	WHERE mds.MDS_ACTY_TYPE_ID <> 3  AND evhi.CREAT_DT >= COALESCE(@startDate,evhi.CREAT_DT) AND evhi.CREAT_DT < COALESCE(@endDate,evhi.CREAT_DT)		
	
	UNION
	SELECT CASE 
				mds.MDS_FAST_TRK_TYPE_ID
				WHEN 'A' THEN 'MDS Fast Track'
				WHEN 'S' THEN 'MDS Fast Track'
				ELSE  'MDS Event' END 'Event Type',
			mds.EVENT_ID ,
			UPPER(evst.EVENT_STUS_DES) ,
			CASE
				WHEN evhi.CREAT_DT is NULL THEN ''
				ELSE REPLACE(SUBSTRING(CONVERT(char, evhi.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, evhi.CREAT_DT, 109), 25, 2) END ,
			CASE
				WHEN mds.STRT_TMST is NULL THEN ''
				ELSE REPLACE(SUBSTRING(CONVERT(char, mds.STRT_TMST,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.STRT_TMST, 109), 25, 2) END ,
			REPLACE(SUBSTRING(CONVERT(char, mds.CREAT_DT,6), 1, 9), ' ', '-') + ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 13, 8) 
				+ ' ' + SUBSTRING(CONVERT(varchar, mds.CREAT_DT, 109), 25, 2) ,
			COALESCE(CONVERT(varchar(100), DecryptByKey(csd.CUST_NME)),mds.CUST_NME) ,
								
			CASE evt.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END AS Scurd_Cd ,			
			mact.MDS_ACTY_TYPE_DES ,
			CASE
				WHEN mds.MDS_ACTY_TYPE_ID = 1 THEN 'First MDS Implementation'
				ELSE '' END ,		
			CASE 
				WHEN mmac.MDS_MAC_ACTY_ID is NULL THEN ''
				ELSE maac.MDS_MAC_ACTY_NME END ,				
			CASE mds.ESCL_CD
				WHEN 1 THEN 'Yes'
				ELSE 'No' END ,			
			CASE
				WHEN evhi.CMNT_TXT is NULL THEN ''
				ELSE evhi.CMNT_TXT END ,
			luser1.DSPL_NME ,
			CASE
				WHEN evhi.CREAT_BY_USER_ID is NULL THEN ''
				ELSE luser2.DSPL_NME END ,
			CASE
				WHEN luser3.DSPL_NME is NULL THEN ''
				ELSE luser3.DSPL_NME END 			
	FROM	COWS.dbo.MDS_EVENT mds with (nolock)	
		JOIN COWS.dbo.LK_EVENT_STUS evst with (nolock) ON mds.EVENT_STUS_ID = evst.EVENT_STUS_ID
		LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=mds.EVENT_ID  AND csd.SCRD_OBJ_TYPE_ID=8
		JOIN COWS.dbo.LK_MDS_ACTY_TYPE mact with (nolock) ON mds.MDS_ACTY_TYPE_ID = mact.MDS_ACTY_TYPE_ID	
		JOIN COWS.dbo.EVENT evt with (nolock) on mds.EVENT_ID = evt.EVENT_ID
		LEFT JOIN COWS.dbo.MDS_EVENT_MAC_ACTY mmac with (nolock) ON mds.EVENT_ID = mmac.EVENT_ID
		LEFT JOIN COWS.dbo.LK_MDS_MAC_ACTY maac with (nolock) ON mmac.MDS_MAC_ACTY_ID = maac.MDS_MAC_ACTY_ID				
		JOIN COWS.dbo.LK_USER luser1 with (nolock) ON mds.CREAT_BY_USER_ID = luser1.USER_ID 		
		LEFT JOIN ( SELECT EVENT_ID, CMNT_TXT, CREAT_BY_USER_ID, MODFD_BY_USER_ID, CREAT_DT  
					FROM COWS.dbo.EVENT_HIST with (nolock)
					WHERE ACTN_ID in ( 5, 16) ) evhi ON mds.EVENT_ID = evhi.EVENT_ID 
		LEFT JOIN COWS.dbo.LK_USER luser2 with (nolock) ON evhi.CREAT_BY_USER_ID = luser2.USER_ID
		LEFT JOIN COWS.dbo.LK_USER luser3 with (nolock) ON mds.MNS_PM_ID = luser3.USER_ADID
	WHERE mds.MDS_ACTY_TYPE_ID = 3  AND evhi.CREAT_DT >= COALESCE(@startDate,evhi.CREAT_DT) AND evhi.CREAT_DT < COALESCE(@endDate,evhi.CREAT_DT)		
	
	
		
	SELECT
			Event_Type 'Event Type',
			Event_Id 'Event ID', 
			Event_Stus 'Event Status',
			Modified_Date 'Modified',
			Event_Date 'Event Date',
			Event_Submitted_On 'Event Submitted On',
			CASE 
				--WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
				--ELSE Cust_Name 
				WHEN @SecuredUser = 0 and Scurd_Cd =0 THEN CUST_NAME
				WHEN @SecuredUser = 0 and Scurd_Cd >0 AND  CUST_NAME2 <>'' THEN 'PRIVATE CUSTOMER' 
				WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd >0 AND CUST_NAME2 <> '' THEN 'Private Customer'
				ELSE COALESCE(CUST_NAME2,CUST_NAME,'')
				END 'Customer Name',			
			MDS_Acty_Type 'MDS Activity Type',
			MDS_Inst_Acty 'MDS Install Activity',
			MDS_Mac_Acty 'MDS MAC Activity',
			Escalations,
			Comments 'Reviewer Comments',
			Author,
			Editor,
			Assigned_MNS_PM 'Assigned Acct MNS PM'		
	FROM #tmp_mns_pmPub
	ORDER BY 'Event ID'
	
	---========
	-- Clean Up
	--=========		
	IF OBJECT_ID('tempdb..#tmp_mns_pmPub', 'U') IS NOT NULL	
		drop table #tmp_mns_pmPub;	
	
	CLOSE SYMMETRIC KEY FS@K3y;
	
	RETURN 0;
		
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_MNSRptPMEventPublishCountOD, '	
	SET @Desc=@Desc + CAST(@SecuredUser AS VARCHAR(2)) + ', ''' + @getSensData + ''', '
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH



