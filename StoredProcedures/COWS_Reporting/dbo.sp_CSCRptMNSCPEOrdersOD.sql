USE [COWS_Reporting]
GO
/*
--======================================================================================
-- Project:			PJ004672 - COWS Reporting 
-- Author:			Sudarat Vongchumpit	
-- Date:			01/04/2012
-- Description:		
--					Extract MNS CPE orders for CSC group for a specified parameter;
--					Mask Sensitive Data, Date Range.
--
--					QueryNumber: 1 = Disconnect Orders
--								 2 = Install Orders
-- Note:
-- Updated By:   Md M Monir
-- Updated Date: 03/19/2018
-- Updated Reason: SCURD_CD/CSG_LVL_CD			
--======================================================================================
*/
ALTER Procedure [dbo].[sp_CSCRptMNSCPEOrdersOD] --1,0,'N','07/01/2014', '07/23/2014'
	@QueryNumber		INT=2,
	@SecuredUser		INT=0,	
	@getSensData		CHAR(1)='N',
	@startDate			Datetime= null,
	@endDate			Datetime= null	

AS
BEGIN TRY
	
	SET NOCOUNT ON;

	DECLARE @startDateStr	VARCHAR(20)
	DECLARE @endDateStr		VARCHAR(20)	

	DECLARE @aSQL			NVARCHAR(max)
	
	CREATE TABLE #tmp_csc_cpe
	(
		Sbmt_Dt				VARCHAR(25),
		Ordr_Stus			VARCHAR(50),
		Dmstc_Cd			VARCHAR(6),
		H1_ID				INT,			
		FTN					VARCHAR(50),
		CCD					VARCHAR(25),
		Related_FTN			INT,
		Ordr_Type_Cd		VARCHAR(3),
		Ordr_SubType_Cd		VARCHAR(10),
		H5_H6_Cust_ID		INT,
		Cust_Name			VARCHAR(100),
		Scurd_Cd			Bit,
		Adr_Line1			VARCHAR(200),
		City				VARCHAR(100),
		Prov_State			VARCHAR(100),
		Zip_Cd				VARCHAR(20),
		Ctry_Cd				VARCHAR(3),
		CLLI_Cd				VARCHAR(50),						
		Dev_Name			VARCHAR(50),
		Mfr_Name			VARCHAR(200),
		CscMaint_Stus		VARCHAR(50),
		Disc_Reas_Cd		VARCHAR(200)	
	)
	
	OPEN SYMMETRIC KEY FS@K3y 
	DECRYPTION BY CERTIFICATE S3cFS@CustInf0;
			
	IF @QueryNumber=1
	BEGIN
	
		SET @aSQL='INSERT INTO #tmp_csc_cpe(Sbmt_Dt,Ordr_Stus,Dmstc_Cd,H1_ID,FTN,CCD,Related_FTN,Ordr_Type_Cd,Ordr_SubType_Cd,
					H5_H6_Cust_ID,Cust_Name,Scurd_Cd,Adr_Line1,City,Prov_State,Zip_Cd,Ctry_Cd,CLLI_Cd,Dev_Name,Mfr_Name,Disc_Reas_Cd)
				SELECT REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CREAT_DT,101),1), ''0'', '''') +
				   SUBSTRING(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 2, 2) +
				   REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 4, 1), ''0'', '''') +
				   RIGHT(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 6) ,
				lkos.ORDR_STUS_DES ,
				CASE 
					WHEN ordr.DMSTC_CD = 1 THEN ''INTL''
					ELSE ''DOM'' END ,
				fcust1.CUST_ID ,
				ford.FTN ,
 				REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101),1), ''0'', '''') +
					SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 2, 2) +
					REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 4, 1), ''0'', '''') +
					RIGHT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 6) ,					
				CASE
					WHEN CAST(ford.RELTD_FTN AS VARCHAR(10)) is NULL THEN ''''	
					ELSE CAST(ford.RELTD_FTN AS VARCHAR(10)) END ,
				ford.ORDR_TYPE_CD ,
				ford.ORDR_SUB_TYPE_CD ,
				ordr.H5_H6_CUST_ID ,
				CASE
				    WHEN  ordr.DMSTC_CD = 1  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				     ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd1.CUST_NME)),ISNULL(fcust2.CUST_NME,''''))  END
				    WHEN ordr.DMSTC_CD = 0 THEN CASE  
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				     ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd2.CUST_NME)),ISNULL(fcust3.CUST_NME,'''')) END
					
				END ,
				CASE ordr.CSG_LVL_ID WHEN 0 THEN 0 ELSE 1 END  AS SCURD_CD ,
				CASE
				
					WHEN  ordr.DMSTC_CD = 1  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd3.STREET_ADR_1)),ISNULL(oadi.STREET_ADR_1,'''')) END
				    WHEN  ordr.DMSTC_CD = 0  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd4.STREET_ADR_1)),ISNULL(oadd.STREET_ADR_1,'''')) END 
						
				END ,
				CASE
					
					WHEN  ordr.DMSTC_CD = 1  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd3.CTY_NME)),ISNULL(oadi.CTY_NME,'''')) END
				    WHEN  ordr.DMSTC_CD = 0  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd4.CTY_NME)),ISNULL(oadd.CTY_NME,'''')) END
				
				END ,
				CASE 
						
				    WHEN  ordr.DMSTC_CD = 1  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(3), DecryptByKey(csd3.STT_PRVN_NME)),ISNULL(oadi.PRVN_NME,'''')) END
				    WHEN  ordr.DMSTC_CD = 0  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(3), DecryptByKey(csd4.STT_CD)),ISNULL(oadd.STT_CD,'''')) END		
				END ,
				CASE
					
					WHEN  ordr.DMSTC_CD = 1  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(12), DecryptByKey(csd3.ZIP_PSTL_CD)),ISNULL(oadi.ZIP_PSTL_CD,'''')) END
				    WHEN  ordr.DMSTC_CD = 0  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(12), DecryptByKey(csd4.ZIP_PSTL_CD)),ISNULL(oadd.ZIP_PSTL_CD,'''')) END
	   
				END ,				
				CASE
					WHEN ordr.DMSTC_CD = 1 AND oadi.CTRY_CD is NULL THEN ''''
					WHEN ordr.DMSTC_CD = 1 AND oadi.CTRY_CD is NOT NULL THEN oadi.CTRY_CD
					WHEN ordr.DMSTC_CD = 0 AND oadd.CTRY_CD is NULL THEN ''''
					WHEN ordr.DMSTC_CD = 0 AND oadd.CTRY_CD is NOT NULL THEN oadd.CTRY_CD				
					END ,
				CASE
					WHEN ordr.DMSTC_CD = 1 THEN ''''
					WHEN ordr.DMSTC_CD = 0 AND fcust3.CLLI_CD is NULL THEN ''''	
					WHEN ordr.DMSTC_CD = 0 AND fcust3.CLLI_CD is not NULL THEN fcust3.CLLI_CD
					END ,
				CASE
					WHEN fdis.ODIE_DEV_NME is NULL THEN ''''
					ELSE fdis.ODIE_DEV_NME END ,			
				CASE
					WHEN fdis.MFR_NME = ''CISCO'' THEN ''Cisco''
					ELSE '''' END ,
				CASE
					WHEN ford.DISC_REAS_CD is NULL THEN ''''
					ELSE ford.DISC_REAS_CD END 				
			FROM	COWS.dbo.FSA_ORDR ford with (nolock)  		
			JOIN COWS.dbo.ORDR ordr with (nolock) ON ford.ORDR_ID = ordr.ORDR_ID 
			JOIN COWS.dbo.FSA_ORDR ford2 with (nolock) ON ordr.PRNT_ORDR_ID = ford2.ORDR_ID
			JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON ordr.ORDR_STUS_ID = lkos.ORDR_STUS_ID 
			JOIN COWS.dbo.LK_FSA_ORDR_TYPE lkot with (nolock) ON ford.ORDR_TYPE_CD = lkot.FSA_ORDR_TYPE_CD
			JOIN COWS.dbo.FSA_ORDR_CUST fcust1 with (nolock) ON ford.ORDR_ID = fcust1.ORDR_ID AND fcust1.CIS_LVL_TYPE = ''H1'' 		
			LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust2 with (nolock) ON ford.ORDR_ID = fcust2.ORDR_ID AND fcust2.CIS_LVL_TYPE = ''H5''
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd1 WITH (NOLOCK) ON csd1.SCRD_OBJ_ID=fcust2.FSA_ORDR_CUST_ID  AND csd1.SCRD_OBJ_TYPE_ID=5 		
			LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust3 with (nolock) ON ford.ORDR_ID = fcust3.ORDR_ID AND fcust3.CIS_LVL_TYPE = ''H6''
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=fcust3.FSA_ORDR_CUST_ID  AND csd2.SCRD_OBJ_TYPE_ID=5
			LEFT JOIN COWS.dbo.ORDR_ADR oadi with (nolock) ON ordr.ORDR_ID = oadi.ORDR_ID AND oadi.CIS_LVL_TYPE = ''H5''
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd3 WITH (NOLOCK) ON csd3.SCRD_OBJ_ID=oadi.ORDR_ADR_ID  AND csd3.SCRD_OBJ_TYPE_ID=14 
			LEFT JOIN COWS.dbo.ORDR_ADR oadd with (nolock) ON ordr.ORDR_ID = oadd.ORDR_ID AND oadd.CIS_LVL_TYPE = ''H6''
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd4 WITH (NOLOCK) ON csd4.SCRD_OBJ_ID=oadd.ORDR_ADR_ID  AND csd4.SCRD_OBJ_TYPE_ID=14			
			LEFT JOIN (SELECT H5_H6_CUST_ID, ODIE_DEV_NME, CASE WHEN mdis.VNDR_NME is NULL THEN '''' ELSE UPPER(mdis.VNDR_NME) END ''MFR_NME''
		 		  		FROM COWS.dbo.MDS_EVENT_DISCO mdis with (nolock)
		 		  		)as fdis ON CONVERT(VARCHAR,fcust2.CUST_ID) = fdis.H5_H6_CUST_ID OR CONVERT(VARCHAR,fcust3.CUST_ID) = fdis.H5_H6_CUST_ID			 
		 	WHERE ford.ORDR_TYPE_CD = ''DC'' AND ford.PROD_TYPE_CD = ''CP'' '
		
	END
	ELSE IF @QueryNumber=2
	BEGIN
	
		SET @aSQL='INSERT INTO #tmp_csc_cpe(Sbmt_Dt,Ordr_Stus,Dmstc_Cd,H1_ID,FTN,CCD,Related_FTN,Ordr_Type_Cd,Ordr_SubType_Cd,
					H5_H6_Cust_ID,Cust_Name,Scurd_Cd,Adr_Line1,City,Prov_State,Zip_Cd,Ctry_Cd,CLLI_Cd,Dev_Name,Mfr_Name,CscMaint_Stus)
				SELECT REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CREAT_DT,101),1), ''0'', '''') +
				   SUBSTRING(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 2, 2) +
				   REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 4, 1), ''0'', '''') +
				   RIGHT(CONVERT(VARCHAR(10),ford.CREAT_DT,101), 6) ,
				lkos.ORDR_STUS_DES ,
				CASE 
					WHEN ordr.DMSTC_CD = 1 THEN ''INTL''
					ELSE ''DOM'' END ,
				fcust1.CUST_ID ,
				ford.FTN ,
 				REPLACE(LEFT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101),1), ''0'', '''') +
					SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 2, 2) +
					REPLACE(SUBSTRING(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 4, 1), ''0'', '''') +
					RIGHT(CONVERT(VARCHAR(10),ford.CUST_CMMT_DT,101), 6) ,					
				CASE
					WHEN CAST(ford.RELTD_FTN AS VARCHAR(10)) is NULL THEN ''''	
					ELSE CAST(ford.RELTD_FTN AS VARCHAR(10)) END ,
				ford.ORDR_TYPE_CD ,	
				ford.ORDR_SUB_TYPE_CD ,
				ordr.H5_H6_CUST_ID ,
				CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				     ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd1.CUST_NME)),ISNULL(fcust2.CUST_NME,''''))  END
				    WHEN ordr.DMSTC_CD = 0 THEN CASE  
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				     ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd2.CUST_NME)),ISNULL(fcust3.CUST_NME,'''')) END
					
				END ,
				ordr.SCURD_CD ,
				CASE
				
					WHEN  ordr.DMSTC_CD = 1  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd3.STREET_ADR_1)),ISNULL(oadi.STREET_ADR_1,'''')) END
				    WHEN  ordr.DMSTC_CD = 0  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(200), DecryptByKey(csd4.STREET_ADR_1)),ISNULL(oadd.STREET_ADR_1,'''')) END 
						
				END ,
				CASE
					
					WHEN  ordr.DMSTC_CD = 1  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd3.CTY_NME)),ISNULL(oadi.CTY_NME,'''')) END
				    WHEN  ordr.DMSTC_CD = 0  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(100), DecryptByKey(csd4.CTY_NME)),ISNULL(oadd.CTY_NME,'''')) END
				
				END ,
				CASE 
						
				    WHEN  ordr.DMSTC_CD = 1  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(3), DecryptByKey(csd3.STT_PRVN_NME)),ISNULL(oadi.PRVN_NME,'''')) END
				    WHEN  ordr.DMSTC_CD = 0  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(3), DecryptByKey(csd4.STT_CD)),ISNULL(oadd.STT_CD,'''')) END		
				END ,
				CASE
					
					WHEN  ordr.DMSTC_CD = 1  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(12), DecryptByKey(csd3.ZIP_PSTL_CD)),ISNULL(oadi.ZIP_PSTL_CD,'''')) END
				    WHEN  ordr.DMSTC_CD = 0  THEN CASE
				    WHEN ordr.CSG_LVL_ID >0 THEN ''Private Customer'' 
				      ELSE COALESCE(CONVERT(varchar(12), DecryptByKey(csd4.ZIP_PSTL_CD)),ISNULL(oadd.ZIP_PSTL_CD,'''')) END
	   
				END ,				
				CASE
					WHEN ordr.DMSTC_CD = 1 AND oadi.CTRY_CD is NULL THEN ''''
					WHEN ordr.DMSTC_CD = 1 AND oadi.CTRY_CD is NOT NULL THEN oadi.CTRY_CD
					WHEN ordr.DMSTC_CD = 0 AND oadd.CTRY_CD is NULL THEN ''''
					WHEN ordr.DMSTC_CD = 0 AND oadd.CTRY_CD is NOT NULL THEN oadd.CTRY_CD				
					END ,
				CASE
					WHEN ordr.DMSTC_CD = 1 THEN ''''
					WHEN ordr.DMSTC_CD = 0 AND fcust3.CLLI_CD is NULL THEN ''''	
					WHEN ordr.DMSTC_CD = 0 AND fcust3.CLLI_CD is not NULL THEN fcust3.CLLI_CD
					END ,
				CASE
					WHEN odie.ODIE_DEV_NME is NULL THEN ''''
					ELSE odie.ODIE_DEV_NME END ,			
				CASE
					WHEN fcpe.MFR_NME is NULL THEN ''''
					ELSE ''Cisco'' END ,
				CASE
					WHEN csct.CSC_Status is NULL THEN ''''
					ELSE csct.CSC_Status END 
		FROM	COWS.dbo.FSA_ORDR ford with (nolock)  		
			JOIN COWS.dbo.ORDR ordr with (nolock) ON ford.ORDR_ID = ordr.ORDR_ID 
			JOIN COWS.dbo.FSA_ORDR ford2 with (nolock) ON ordr.PRNT_ORDR_ID = ford2.ORDR_ID
			JOIN COWS.dbo.LK_ORDR_STUS lkos with (nolock) ON ordr.ORDR_STUS_ID = lkos.ORDR_STUS_ID 
			JOIN COWS.dbo.LK_FSA_ORDR_TYPE lkot with (nolock) ON ford.ORDR_TYPE_CD = lkot.FSA_ORDR_TYPE_CD
			JOIN COWS.dbo.FSA_ORDR_CUST fcust1 with (nolock) ON ford.ORDR_ID = fcust1.ORDR_ID AND fcust1.CIS_LVL_TYPE = ''H1'' 		
			LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust2 with (nolock) ON ford.ORDR_ID = fcust2.ORDR_ID AND fcust2.CIS_LVL_TYPE = ''H5''
			 LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd1 WITH (NOLOCK) ON csd1.SCRD_OBJ_ID=fcust2.FSA_ORDR_CUST_ID  AND csd1.SCRD_OBJ_TYPE_ID=5 		
			LEFT JOIN COWS.dbo.FSA_ORDR_CUST fcust3 with (nolock) ON ford.ORDR_ID = fcust3.ORDR_ID AND fcust3.CIS_LVL_TYPE = ''H6''
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=fcust3.FSA_ORDR_CUST_ID  AND csd2.SCRD_OBJ_TYPE_ID=5 
			LEFT JOIN COWS.dbo.ORDR_ADR oadi with (nolock) ON ordr.ORDR_ID = oadi.ORDR_ID AND oadi.CIS_LVL_TYPE = ''H5''
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd3 WITH (NOLOCK) ON csd3.SCRD_OBJ_ID=oadi.ORDR_ADR_ID  AND csd3.SCRD_OBJ_TYPE_ID=14 
			LEFT JOIN COWS.dbo.ORDR_ADR oadd with (nolock) ON ordr.ORDR_ID = oadd.ORDR_ID AND oadd.CIS_LVL_TYPE = ''H6''
			LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd4 WITH (NOLOCK) ON csd4.SCRD_OBJ_ID=oadd.ORDR_ADR_ID  AND csd4.SCRD_OBJ_TYPE_ID=14			
			LEFT JOIN COWS.dbo.FSA_MDS_EVENT_NEW fmds WITH (NOLOCK) ON fmds.H5_H6_CUST_ID = CONVERT(VARCHAR,fcust2.CUST_ID) 
																	OR fmds.H5_H6_CUST_ID = CONVERT(VARCHAR,fcust3.CUST_ID)
			LEFT JOIN COWS.dbo.MDS_EVENT_ODIE_DEV_NME odie WITH (NOLOCK) ON	odie.FSA_MDS_EVENT_ID	=	fmds.FSA_MDS_EVENT_ID
																		AND	odie.TAB_SEQ_NBR		=	fmds.TAB_SEQ_NBR
			LEFT JOIN (SELECT distinct ORDR_ID, MFR_NME 
		 				FROM COWS.dbo.FSA_ORDR_CPE_LINE_ITEM fcpe with (nolock) 
		 				WHERE UPPER(MFR_NME) = ''CISCO SYSTEMS''
		 		  		)as fcpe ON ford.ORDR_ID = fcpe.ORDR_ID 
			LEFT JOIN (SELECT ORDR_ID, (lkts.TASK_NME + '' '' + lkst.ORDR_STUS_DES ) ''CSC_Status'' 
						FROM COWS.dbo.ACT_TASK atsk with (nolock) 
						JOIN COWS.dbo.LK_TASK lkts with (nolock) ON atsk.TASK_ID = lkts.TASK_ID
						JOIN COWS.dbo.LK_ORDR_STUS lkst with (nolock) ON atsk.STUS_ID = lkst.ORDR_STUS_ID
						WHERE atsk.TASK_ID = 400) as csct ON ford.ORDR_ID = csct.ORDR_ID					
		WHERE ford.ORDR_TYPE_CD = ''IN'' AND ford.PROD_TYPE_CD = ''CP'' AND ford.CPE_CPE_ORDR_TYPE_CD = ''MNS'' '
	
	END
	
	IF @startDate is NOT NULL AND @endDate is NOT NULL
		BEGIN
			
			-- Do the conversion before passing them to @aSQL
			SET @startDateStr = @startDate
			SET @endDateStr = @endDate
			
			SET @aSQL=@aSQL + 'AND ford.CREAT_DT >= ''' + @startDateStr + ''' AND ford.CREAT_DT < DATEADD(day, 1, ''' + @endDateStr + ''') '
			
		END	
	
	EXEC sp_executesql @aSQL
	
	--========================

	IF @QueryNumber=1
	BEGIN
	
		SELECT	Sbmt_Dt 'Order Submit Date',
				Ordr_Stus 'Order Status',
				Dmstc_Cd 'INTL/DOM',
				H1_ID 'H1 ID',
				FTN,
				CCD 'Customer Commit Date',
				CASE
					WHEN CAST(Related_FTN AS VARCHAR(10)) = '0' THEN ''
					ELSE CAST(Related_FTN AS VARCHAR(10)) END 'Related FTN',
				Ordr_SubType_Cd 'Order SubType Code',
				H5_H6_Cust_ID 'H5/H6 Cust ID',				
				CASE 
					WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
					WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
					ELSE Cust_Name END 'Customer Name',	
				Adr_Line1 'Address Line 1',
				City,
				Prov_State 'Province Municipality/State Code',
				Zip_Cd 'Zip Code/Postal Code',
				Ctry_Cd 'Country Code',
				CLLI_Cd 'CLLID Code',
				Dev_Name 'Device Name',
				Mfr_Name 'MANUFACTURER',
				Disc_Reas_Cd 'Disconnect Reason Code'	
					
		FROM #tmp_csc_cpe
		WHERE Ordr_Type_Cd = 'DC'
		ORDER BY FTN
	
	END
	ELSE IF @QueryNumber=2
	BEGIN
	
		SELECT	Sbmt_Dt 'Order Submit Date',
				Ordr_Stus 'Order Status',
				Dmstc_Cd 'INTL/DOM',
				H1_ID 'H1 ID',
				FTN,
				CCD 'Customer Commit Date',
				CASE
					WHEN CAST(Related_FTN AS VARCHAR(10)) = '0' THEN ''
					ELSE CAST(Related_FTN AS VARCHAR(10)) END 'Related FTN',					
				Ordr_SubType_Cd 'Order SubType Code',
				H5_H6_Cust_ID 'H5/H6 Cust ID',				
				CASE 
					WHEN @SecuredUser = 0 AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
					WHEN @SecuredUser = 1 AND @getSensData='N' AND Scurd_Cd = 1 AND Cust_Name <> '' THEN 'Private Customer'
					ELSE Cust_Name END 'Customer Name',	
				Adr_Line1 'Address Line 1',
				City,
				Prov_State 'Province Municipality/State Code',
				Zip_Cd 'Zip Code/Postal Code',
				Ctry_Cd 'Country Code',
				CLLI_Cd 'CLLID Code',
				Dev_Name 'Device Name',
				Mfr_Name 'MANUFACTURER',	
				CscMaint_Stus 'CSC Maint Status'
				
		FROM #tmp_csc_cpe
		WHERE Ordr_Type_Cd = 'IN'
		ORDER BY FTN
	
	END
	
	---========
	-- Clean Up
	--=========		
	IF OBJECT_ID('tempdb..#tmp_csc_cpe', 'U') IS NOT NULL	
		drop table #tmp_csc_cpe;
		
	CLOSE SYMMETRIC KEY FS@K3y;
	
	RETURN 0;
	
	
END TRY

BEGIN CATCH
	DECLARE @Desc VARCHAR(200)
	SET @Desc='EXEC COWS_Reporting.dbo.sp_CSCRptMNSCPEOrdersOD ' + CAST(@QueryNumber AS VARCHAR(2))
	SET @Desc=@Desc + ', ' + CAST(@SecuredUser AS VARCHAR(2)) + ', '''  + @getSensData + ''', ' 
	SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)
	EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;
	RETURN 1;
END CATCH





