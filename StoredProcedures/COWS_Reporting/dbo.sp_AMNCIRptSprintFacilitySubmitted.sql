-- =============================================    
-- Author:  <Author,,Name>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>    
-- Updated By:   Md M Monir  
-- Updated Date: 03/19/2018  
-- Updated Reason: SCURD_CD/CSG_LVL_CD    
-- Updated Reason: SCURD_CD/CSG_LVL_CD 
-- Update date: <06/06/2018>
-- Updated by: Md M Monir      
-- EXEC COWS_Reporting.dbo.sp_AMNCIRptSprintFacilitySubmitted 4, 0, '','', 0    
-- =============================================    
ALTER PROCEDURE [dbo].[sp_AMNCIRptSprintFacilitySubmitted]    
  @QueryNumber  INT,    
     @secured   BIT = 0,    
  @inStartDtStr    VARCHAR(10)= '',    
     @inEndDtStr   VARCHAR(10)= '',    
  @prior    BIT = 0    
    
AS    
BEGIN TRY    
 SET NOCOUNT ON;    
 DECLARE @startDate  DATETIME    
 DECLARE @endDate  DATETIME    
 DECLARE @today   DATETIME    
 DECLARE @startDtStr  VARCHAR(10)     
 DECLARE @dayOfWeek  INT    
     
 IF @QueryNumber=0    
  BEGIN    
   SET @startDate=CONVERT(DATETIME, @inStartDtStr, 101)    
   SET @endDate=CONVERT(DATETIME, @inEndDtStr, 101)        
   -- Add 1 day to End Date in order to cover the End Date 24-hr period    
   SET @endDate=DATEADD(DAY, 1, @endDate)    
  END    
 ELSE IF @QueryNumber=1    
     BEGIN         
      -- e.g. Run date is '2011-06-15' , STRT_TMST >= 2011-06-15 00:00:00.000 and STRT_TMST < 2011-06-16 00:00:00.000    
   SET @startDate=CAST(CONVERT(VARCHAR(8), GETDATE(), 112) AS DATETIME)    
   SET @endDate=CAST(CONVERT(VARCHAR(8), DATEADD(DAY, 1, GETDATE()), 112) AS DATETIME)       
  END    
 ELSE IF @QueryNumber=2    
  BEGIN    
   -- e.g. Run date is '2011-06-21' 3rd day of the week    
   -- STRT_TMST >= 2011-06-12 00:00:00.000 and STRT_TMST < 2011-06-19 00:00:00.000    
   SET @today=GETDATE()    
   SET @dayOfWeek=DATEPART(dw, @today)    
   SET @startDate=CAST(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek+5), @today), 112) AS DATETIME)    
   SET @endDate=CAST(CONVERT(VARCHAR(8), DATEADD(dd, -(@dayOfWeek-2), @today), 112) AS DATETIME)    
  END    
 ELSE IF @QueryNumber=3    
  BEGIN        
     SET @today=DATEADD(MONTH,-1,GETDATE())    
      -- e.g. Run date is the 1st day of each month     
      -- STRT_TMST >= 2011-05-01 00:00:00.000 and STRT_TMST < 2011-06-01 00:00:00.000    
     SET @startDtStr=SUBSTRING(CONVERT(VARCHAR(10), @today, 101),1,2) + '/01/' + CAST(YEAR(@today) AS VARCHAR(4));         
   SET @startDate=CAST(@startDtStr AS DATETIME)    
   SET @endDate=CAST(CONVERT(VARCHAR(8), GETDATE(), 112) AS DATETIME)    
  END    
 ELSE IF @QueryNumber=4    
  BEGIN        
      -- e.g. Run date is 1 full month to current date    
   SET @today=GETDATE()    
   SET @startDate=cast(CONVERT(VARCHAR(8), DATEADD(mm, -(5), @today), 112) As DATETIME)    
   SET @endDate=CAST(CONVERT(VARCHAR(8), GETDATE(), 112) AS DATETIME)    
  END    
    
 CREATE TABLE #tempSFS (    
  [Order Tracking No] int,    
  [Customer Name] nvarchar(255),    
  [Customer Reference Number] nvarchar(255),    
  [Installed Date] datetime,    
  [Implementation Contact] nvarchar(255),    
  [Sprint Target Delivery Date] datetime,    
  [Product] nvarchar(255),    
  [Customer Commit Date] datetime,    
  [Order Status] nvarchar(255),    
  [Received Service Request] datetime,    
  [Sales Region] nvarchar(255),    
  [Sales Sub-Region] nvarchar(255),    
  [Service Request Type changed] datetime,    
  [Service Request Type] nvarchar(255),    
  [Order Sub Type] nvarchar(255),    
  [Order Submit Date] datetime,    
  [Week] datetime,    
  [Port Speed] nvarchar(255),    
  [Origination Country] nvarchar(255),    
  [Termination Country] nvarchar(255),    
  [Order Description] nvarchar(255),    
  [MRC] nvarchar(255),    
  [NRC] nvarchar(255),    
  [Month] datetime    
  )    
      
--DECLARE @secured BIT=0 -- for testing purposes only    
 OPEN SYMMETRIC KEY FS@K3y     
 DECRYPTION BY CERTIFICATE S3cFS@CustInf0;     
    
 INSERT INTO #tempSFS ([Order Tracking No],[Customer Name],[Customer Reference Number],[Installed Date],[Implementation Contact],       
 [Sprint Target Delivery Date],[Product],[Customer Commit Date],[Order Status],[Received Service Request],[Sales Region],[Sales Sub-Region],    
 [Service Request Type changed],[Service Request Type],[Order Sub Type],[Order Submit Date],[Week],[Port Speed],[Origination Country],    
 [Termination Country],[Order Description],[MRC],[NRC],[Month])    
    
 SELECT DISTINCT     
  a.FTN [Order Tracking No],     
  CASE    
   WHEN @secured = 0 AND si.CUST_NME<>''  THEN coalesce(si.CUST_NME,'PRIVATE CUSTOMER')  
   WHEN @secured = 0 AND si.CUST_NME =''  THEN 'PRIVATE CUSTOMER'  
   WHEN @secured = 0 AND si.CUST_NME IS NULL  THEN 'PRIVATE CUSTOMER'      
   ELSE coalesce(si.CUST_NME2,si.CUST_NME,'')  
   END [Customer Name], --Secured    
  --ISNULL(si.CUST_NME,'') [Customer Name],      
  '0' [Customer Reference Number],      
  CASE    
   WHEN tk.TASK_ID = 1001 THEN CONVERT(NVARCHAR(10),tk.CREAT_DT,101) END [Installed Date],      
  ISNULL(g.FULL_NME,'') [Implementation Contact],    
  CONVERT(VARCHAR(10),a.CUST_WANT_DT,101) [Sprint Target Delivery Date],      
  d.PROD_TYPE_DES [Product],      
  CONVERT(VARCHAR(10),a.CUST_CMMT_DT,101) [Customer Commit Date],      
  f.STUS_DES [Order Status],      
  CONVERT(VARCHAR(10),n.RQSTN_DT,101) [Received Service Request],      
  ISNULL(i.SALS_CHNL_DES,'') [Sales Region],      
  'N/A' [Sales Sub-Region],      
  NULL [Service Request Type changed],     
  ISNULL(j.ORDR_TYPE_DES,'') [Service Request Type],    
  ISNULL(m.ORDR_SUB_TYPE_DES,'') [Order Sub Type],    
  CONVERT(VARCHAR(10),a.ORDR_SBMT_DT,101) [Order Submit Date],      
  CASE      
   WHEN DATEPART(DW, a.ORDR_SBMT_DT) = 1 THEN CONVERT(nvarchar(10), (a.ORDR_SBMT_DT + 3),101)    
   WHEN DATEPART(DW, a.ORDR_SBMT_DT) = 2 THEN CONVERT(nvarchar(10), (a.ORDR_SBMT_DT + 2),101)    
   WHEN DATEPART(DW, a.ORDR_SBMT_DT) = 3 THEN CONVERT(nvarchar(10), (a.ORDR_SBMT_DT + 1),101)    
   WHEN DATEPART(DW, a.ORDR_SBMT_DT) = 5 THEN CONVERT(nvarchar(10), (a.ORDR_SBMT_DT - 1),101)    
   WHEN DATEPART(DW, a.ORDR_SBMT_DT) = 6 THEN CONVERT(nvarchar(10), (a.ORDR_SBMT_DT - 2),101)    
   WHEN DATEPART(DW, a.ORDR_SBMT_DT) = 7 THEN CONVERT(nvarchar(10), (a.ORDR_SBMT_DT - 3),101)    
   ELSE CONVERT(nvarchar(10), a.ORDR_SBMT_DT, 101) END  [Week],    
  ISNULL(fcpl.TTRPT_SPD_OF_SRVC_BDWD_DES,'') [Port Speed],    
  --ISNULL(k.CTRY_NME,'') [Origination Country],  
  CASE WHEN (p.CSG_LVL_ID>0) THEN lcs.CTRY_NME ELSE k.CTRY_NME END [Origination Country], /*Monir 06062018*/   
  --ISNULL(o.TOC_CTRY_NME,'') [Termination Country],    
  CASE WHEN (b.CSG_LVL_ID>0) THEN dbo.decryptBinaryData(csd3.[CTRY_RGN_NME]) ELSE o.TOC_CTRY_NME END [Termination Country] ,
  ISNULL(b5.LINE_ITEM_DES, '') + ' ' + ISNULL(b6.LINE_ITEM_DES, '') [Order Description],    
  ISNULL(CAST(b5.MRC_CHG_AMT AS money), 0) + ISNULL(CAST(b6.MRC_CHG_AMT AS money), 0) [MRC],    
  ISNULL(CAST(b5.NRC_CHG_AMT AS money), 0) + ISNULL(CAST(b6.NRC_CHG_AMT AS money), 0) [NRC],    
  --CONVERT(VARCHAR(10),SUBSTRING(CONVERT(VARCHAR(10), a.ORDR_SBMT_DT, 101),1,2) + '/01/' + CAST(YEAR(a.ORDR_SBMT_DT) AS VARCHAR(4)),101) [Month]    
  CAST(DATEPART(M, DATEADD(M, 0, a.ORDR_SBMT_DT)) AS VARCHAR(2)) + '/01/' + CAST(DATEPART(YEAR, DATEADD(M, 0, a.ORDR_SBMT_DT)) AS VARCHAR(4)) [Month]    
 FROM [COWS].[dbo].[FSA_ORDR] a     
  LEFT OUTER JOIN [COWS].[dbo].[ORDR] b WITH (NOLOCK) ON a.ORDR_ID = b.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_CPE_LINE_ITEM] fcpl with (nolock) on fcpl.ORDR_ID = a.ORDR_ID   
  LEFT OUTER JOIN [COWS].[dbo].[ORDR_MS] c WITH (NOLOCK) ON a.ORDR_ID = c.ORDR_ID     
  LEFT OUTER JOIN (  
 SELECT bb.ORDR_ID,   
   bb.SOI_CD,   
   bb.CIS_LVL_TYPE,   
   CONVERT(VARCHAR, DecryptByKey(csd.CUST_NME)) [CUST_NME2],  
   bb.CUST_NME as  [CUST_NME]     
    FROM [COWS].[dbo].[FSA_ORDR_CUST] bb WITH (NOLOCK)    
   LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID   
   LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd WITH (NOLOCK) ON csd.SCRD_OBJ_ID=bb.FSA_ORDR_CUST_ID  AND csd.SCRD_OBJ_TYPE_ID=5      
    WHERE bb.SOI_CD = (SELECT MAX(SOI_CD)    
  FROM ( SELECT SOI_CD, ORDR_ID     
    FROM [COWS].[dbo].[FSA_ORDR_CUST] WITH (NOLOCK)) soi    
     WHERE soi.ORDR_ID = bb.ORDR_ID)) si   
     ON si.ORDR_ID = a.ORDR_ID     
  LEFT OUTER JOIN (SELECT bb.ACT_TASK_ID, bb.TASK_ID, bb.CREAT_DT, bb.ORDR_ID     
    FROM [COWS].[dbo].[ACT_TASK] bb WITH (NOLOCK)    
    LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID     
    WHERE bb.ACT_TASK_ID = (SELECT MAX(ACT_TASK_ID)    
    FROM (SELECT ACT_TASK_ID, ORDR_ID     
     FROM [COWS].[dbo].[ACT_TASK] WITH (NOLOCK)) tsk    
     WHERE tsk.ORDR_ID = bb.ORDR_ID)) tk ON tk.ORDR_ID = a.ORDR_ID    
  LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.BIC_CD, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
    FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
    JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
    WHERE bb.BILL_ITEM_TYPE_ID=5 AND bb.LINE_ITEM_DES LIKE '%Access%') b5 ON a.ORDR_ID = b5.ORDR_ID    
  LEFT OUTER JOIN (SELECT bb.LINE_ITEM_DES, bb.BIC_CD, bb.ORDR_ID, bb.MRC_CHG_AMT, bb.NRC_CHG_AMT     
    FROM [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] bb WITH (NOLOCK)    
    JOIN [COWS].dbo.[ORDR] cc WITH (NOLOCK) ON cc.ORDR_ID = bb.ORDR_ID     
    WHERE bb.BILL_ITEM_TYPE_ID=5 AND bb.LINE_ITEM_DES LIKE '%PORT%') b6 ON a.ORDR_ID = b6.ORDR_ID    
  LEFT OUTER JOIN (SELECT  bb.ASMT_DT, bb.ORDR_ID, bb.ASN_USER_ID    
      FROM [COWS].[dbo].[USER_WFM_ASMT] bb WITH (NOLOCK)    
      LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR] cc WITH (NOLOCK) ON bb.ORDR_ID = cc.ORDR_ID     
    WHERE bb.ASMT_DT = (SELECT MAX(ASMT_DT)    
     FROM (SELECT DISTINCT ASMT_DT, ORDR_ID     
      FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK)) usr      
     WHERE usr.ORDR_ID  = bb.ORDR_ID )    
     AND bb.ASN_USER_ID  = (SELECT MAX(ASN_USER_ID)     
      FROM (SELECT DISTINCT ASMT_DT, ORDR_ID, ASN_USER_ID     
       FROM [COWS].[dbo].[USER_WFM_ASMT] WITH (NOLOCK))uas    
    WHERE uas.ORDR_ID = bb.ORDR_ID)) ur ON ur.ORDR_ID = a.ORDR_ID     
  LEFT OUTER JOIN [COWS].[dbo].[LK_PROD_TYPE] d WITH (NOLOCK) ON a.PROD_TYPE_CD = d.FSA_PROD_TYPE_CD    
  LEFT OUTER JOIN [COWS].[dbo].[LK_TASK] e  WITH (NOLOCK) ON tk.TASK_ID = e.TASK_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_STUS] f WITH (NOLOCK) ON e.ORDR_STUS_ID = f.STUS_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_USER] g WITH (NOLOCK) ON ur.ASN_USER_ID = g.USER_ID     
     LEFT OUTER JOIN [COWS].[dbo].[LK_USER] mg WITH (NOLOCK) ON g.MGR_ADID = mg.USER_ADID     
  LEFT OUTER JOIN [COWS].[dbo].[TRPT_ORDR] h WITH (NOLOCK) ON a.ORDR_ID = h.ORDR_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_SALS_CHNL] i WITH (NOLOCK) ON h.SALS_CHNL_ID = i.SALS_CHNL_ID    
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_TYPE] j WITH (NOLOCK) ON j.FSA_ORDR_TYPE_CD = a.ORDR_TYPE_CD     
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_BILL_LINE_ITEM] l WITH (NOLOCK) ON a.ORDR_ID = l.ORDR_ID     
  LEFT OUTER JOIN [COWS].[dbo].[LK_ORDR_SUB_TYPE] m WITH (NOLOCK) ON m.ORDR_SUB_TYPE_CD = a.ORDR_SUB_TYPE_CD     
  LEFT OUTER JOIN [COWS].[dbo].[FSA_ORDR_GOM_XNCI] n WITH (NOLOCK) ON a.ORDR_ID = n.ORDR_ID    
  LEFT OUTER JOIN [COWS].[dbo].[NRM_CKT] o WITH (NOLOCK) ON a.FTN = o.FTN 
  LEFT OUTER JOIN COWS.dbo.[CUST_SCRD_DATA] csd3 WITH (NOLOCK) ON csd3.[SCRD_OBJ_ID]=o.[NRM_CKT_ID] and csd3.[SCRD_OBJ_TYPE_ID]=29
   
  LEFT OUTER JOIN [COWS].[dbo].[H5_FOLDR] p WITH (NOLOCK) ON b.H5_FOLDR_ID = p.H5_FOLDR_ID     
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] k WITH (NOLOCK) ON k.CTRY_CD = p.CTRY_CD    
  LEFT OUTER JOIN [COWS].dbo.CUST_SCRD_DATA csd2 WITH (NOLOCK) ON csd2.SCRD_OBJ_ID=p.H5_FOLDR_ID  AND csd2.SCRD_OBJ_TYPE_ID=6  
  LEFT OUTER JOIN [COWS].[dbo].[LK_CTRY] lcs on CONVERT(varchar, DecryptByKey(csd2.CTRY_CD)) = lcs.CTRY_CD 
 WHERE b.DMSTC_CD = 1    
  AND b.RGN_ID = 1 --AMNCI    
  AND b.PLTFRM_CD = 'SF'    
  AND si.CIS_LVL_TYPE = 'H5'    
  AND a.ORDR_SBMT_DT >= @startDate AND a.ORDR_SBMT_DT < @endDate    
  --AND a.ORDR_SBMT_DT >= '08-01-2011' AND a.ORDR_SBMT_DT < '02-01-2012'    
 UNION ALL     
 SELECT  [Order Tracking No],[Customer Name],[Customer Reference Number],  
   CONVERT(NVARCHAR(10),[Installed Date],101),[Implementation Contact],       
   CONVERT(NVARCHAR(10),[Sprint Target Delivery Date],101),[Product],  
   CONVERT(NVARCHAR(10),[Customer Commit Date],101),[Order Status],    
   CONVERT(NVARCHAR(10),[Received Service Request],101),[Sales Region],[Sales Sub-Region],    
   [Service Request Type changed],[Service Request Type],[Order Sub Type],  
   CONVERT(NVARCHAR(10),[Order Submit Date],101),     
   CONVERT(NVARCHAR(10),[Week],101),[Port Speed],[Origination Country],    
   [Termination Country],[Order Description],[MRC],[NRC],  
   CONVERT(NVARCHAR(10),[Month],101)     
 FROM [COWS_Reporting].[dbo].[AMNCISprintFacilitySubmitted]    
 WHERE [Order Submit Date] >= @startDate AND [Order Submit Date] < @endDate    
 --WHERE [Order Submit Date] >= '08-01-2011' AND [Order Submit Date] < '02-01-2012'    
 ORDER BY a.FTN     
     
 INSERT INTO [COWS_Reporting].[dbo].[AMNCISprintFacilitySubmitted]     
 ([Order Tracking No],[Customer Name],[Customer Reference Number],[Installed Date],[Implementation Contact],       
 [Sprint Target Delivery Date],[Product],[Customer Commit Date],[Order Status],[Received Service Request],[Sales Region],[Sales Sub-Region],    
 [Service Request Type changed],[Service Request Type],[Order Sub Type],[Order Submit Date],[Week],[Port Speed],[Origination Country],    
 [Termination Country],[Order Description],[MRC],[NRC],[Month])    
 SELECT DISTINCT * FROM #tempSFS t1    
 WHERE NOT EXISTS (SELECT DISTINCT * FROM [COWS_Reporting].[dbo].[AMNCISprintFacilitySubmitted] t2    
  WHERE t1.[Order Tracking No]=t2.[Order Tracking No]     
   AND t1.[Customer Name]=t2.[Customer Name]     
   AND t1.[Customer Reference Number]=t2.[Customer Reference Number]    
   AND t1.[Implementation Contact]=t2.[Implementation Contact]     
   AND t1.[Product]=t2.[Product]     
   AND t1.[Customer Commit Date]=t2.[Customer Commit Date]     
   AND t1.[Order Status]=t2.[Order Status]     
   AND t1.[Sales Region]=t2.[Sales Region]     
   AND t1.[Sales Sub-Region]=t2.[Sales Sub-Region]     
   AND t1.[Service Request Type]=t2.[Service Request Type]    
   AND t1.[Order Sub Type]=t2.[Order Sub Type]     
   AND t1.[Order Submit Date]=t2.[Order Submit Date]     
   AND t1.[Week]=t2.[Week]     
   AND t1.[Port Speed]=t2.[Port Speed]    
   AND t1.[Origination Country]=t2.[Origination Country]     
   AND t1.[Termination Country]=t2.[Termination Country]     
   AND t1.[Order Description]=t2.[Order Description]     
   AND t1.[MRC]=t2.[MRC]     
   AND t1.[NRC]=t2.[NRC]     
   AND t1.[Month]=t2.[Month])    
    
 IF @prior <> 0    
  BEGIN     
   SELECT DISTINCT * FROM #tempSFS    
   WHERE ([Installed Date] < [Order Submit Date]) OR ([Installed Date] IS NOT NULL AND [Order Submit Date] IS NULL)    
  END    
 ELSE    
  BEGIN    
   SELECT DISTINCT * FROM #tempSFS    
  END    
     
 DROP TABLE #tempSFS    
          
 CLOSE SYMMETRIC KEY FS@K3y     
          
 RETURN 0;    
     
END TRY    
    
BEGIN CATCH    
 DECLARE @Desc VARCHAR(200)    
 SET @Desc='EXEC COWS_Reporting.dbo.sp_AMNCIRptSprintFacilitySubmitted '  + CAST(@QueryNumber AS VARCHAR(4)) + ',' + CAST(@secured AS VARCHAR(4)) + ': '    
 SET @Desc=@Desc + CONVERT(VARCHAR(10), @startDate, 101) + ', ' + CONVERT(VARCHAR(10), @endDate, 101)    
 EXEC RAD_Reporting.rad.sp_ReportProcessLog null, @Desc, null, null, null;    
 RETURN 1;    
END CATCH    