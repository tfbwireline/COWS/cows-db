USE [COWS]
GO
_CreateObject 'SP'
	,'dbo'
	,'getQualificationsByUserID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================================================
-- Author:		qi931353  
-- Create date: 02/20/2019  
-- Description: Gets all Qualifications.
-- Update date: 08/21/2019
-- Description: Updated query to refer to profiles and not on role and group
-- Update date: 07/22/2020
-- Description: Updated query since Qualification is for MDS Admin/Activator only
-- Update date: 07/14/2021
-- Description: Added MPLS Activity Type
-- ===================================================================================
ALTER PROCEDURE [dbo].[getQualificationsByUserID]  --6148 (NonAdmin) --1383 (Admin)
  @USERID			INT
AS  
BEGIN  
SET NOCOUNT ON;  
BEGIN TRY
	
	--DECLARE @USERID INT = 6150 -- For Testing (1383)
	-- If an Admin, then show all MDS Activator that has qualification
	DECLARE @adminCount INT = 
		(SELECT DISTINCT COUNT (USER_ID) FROM dbo.MAP_USR_PRF WITH (NOLOCK)
			WHERE USER_ID = @USERID AND REC_STUS_ID = 1 AND USR_PRF_ID IN
				(SELECT USR_PRF_ID FROM dbo.LK_USR_PRF WITH (NOLOCK) WHERE USR_PRF_NME LIKE '%MDSAdmin%'))
	--PRINT @adminCount

	IF (@adminCount > 0 )
		BEGIN
			SELECT DISTINCT
					q.QLFCTN_ID AS 'QualificationID', q.REC_STUS_ID AS 'RecStatusID',
					q.ASN_TO_USER_ID AS 'AssignToUserID', au.DSPL_NME AS 'AssignToUser', 
					au.USER_ADID AS 'AssignToUserAdId', 
					q.CREAT_BY_USER_ID AS 'CreatedByUserID', cu.DSPL_NME AS 'CreatedByUser',
					cu.USER_ADID AS 'CreatedByAdId', q.CREAT_DT AS 'CreatedDate',
					q.MODFD_BY_USER_ID AS 'ModifiedByUserID', mu.DSPL_NME AS 'ModifiedByUser',
					mu.USER_ADID AS 'ModifiedByAdId', q.MODFD_DT AS 'ModifiedDate',
					-- Dedicated Customers
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.CUST_ID))
							FROM dbo.QLFCTN_DEDCTD_CUST itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.CUST_ID
							FOR XML PATH('')), 1, 1, '') CustomerIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), dc.CUST_NME))
							FROM dbo.QLFCTN_DEDCTD_CUST itm WITH (NOLOCK)
							JOIN dbo.LK_DEDCTD_CUST dc WITH (NOLOCK) ON itm.CUST_ID = dc.CUST_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY dc.CUST_NME
							FOR XML PATH('')), 1, 1, '') CustomerNames,

					-- Device
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.DEV_ID))
							FROM dbo.QLFCTN_DEV itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.DEV_ID
							FOR XML PATH('')), 1, 1, '') DeviceIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), d.DEV_NME))
							FROM dbo.QLFCTN_DEV itm WITH (NOLOCK)
							JOIN dbo.LK_DEV d WITH (NOLOCK) ON itm.DEV_ID = d.DEV_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY d.DEV_NME
							FOR XML PATH('')), 1, 1, '') DeviceNames,

					-- Enhance Services
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.ENHNC_SRVC_ID))
							FROM dbo.QLFCTN_ENHNC_SRVC itm WITH (NOLOCK)
							JOIN dbo.LK_ENHNC_SRVC es WITH (NOLOCK) ON itm.ENHNC_SRVC_ID = es.ENHNC_SRVC_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID AND es.REC_STUS_ID = 1
							ORDER BY itm.ENHNC_SRVC_ID
							FOR XML PATH('')), 1, 1, '') EnhanceServiceIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), es.ENHNC_SRVC_NME))
							FROM dbo.QLFCTN_ENHNC_SRVC itm WITH (NOLOCK)
							JOIN dbo.LK_ENHNC_SRVC es WITH (NOLOCK) ON itm.ENHNC_SRVC_ID = es.ENHNC_SRVC_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID AND es.REC_STUS_ID = 1
							ORDER BY es.ENHNC_SRVC_NME
							FOR XML PATH('')), 1, 1, '') EnhanceServiceNames,

					-- Event Types
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.EVENT_TYPE_ID))
							FROM dbo.QLFCTN_EVENT_TYPE itm WITH (NOLOCK)
							JOIN dbo.LK_EVENT_TYPE et WITH (NOLOCK) ON itm.EVENT_TYPE_ID = et.EVENT_TYPE_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID AND et.REC_STUS_ID = 1
							ORDER BY itm.EVENT_TYPE_ID
							FOR XML PATH('')), 1, 1, '') EventTypeIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), et.EVENT_TYPE_NME))
							FROM dbo.QLFCTN_EVENT_TYPE itm WITH (NOLOCK)
							JOIN dbo.LK_EVENT_TYPE et WITH (NOLOCK) ON itm.EVENT_TYPE_ID = et.EVENT_TYPE_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID AND et.REC_STUS_ID = 1
							ORDER BY et.EVENT_TYPE_NME
							FOR XML PATH('')), 1, 1, '') EventTypeNames,

					-- IP Version
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.IP_VER_ID))
							FROM dbo.QLFCTN_IP_VER itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.IP_VER_ID
							FOR XML PATH('')), 1, 1, '') IPVersionIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), ip.IP_VER_NME))
							FROM dbo.QLFCTN_IP_VER itm WITH (NOLOCK)
							JOIN dbo.LK_IP_VER ip WITH (NOLOCK) ON itm.IP_VER_ID = ip.IP_VER_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY ip.IP_VER_NME
							FOR XML PATH('')), 1, 1, '') IPVersionNames,

					-- MPLS Activity Type
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.MPLS_ACTY_TYPE_ID))
							FROM dbo.QLFCTN_MPLS_ACTY_TYPE itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.MPLS_ACTY_TYPE_ID
							FOR XML PATH('')), 1, 1, '') MplsActyTypeIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), nt.MPLS_ACTY_TYPE_DES))
							FROM dbo.QLFCTN_MPLS_ACTY_TYPE itm WITH (NOLOCK)
							JOIN dbo.LK_MPLS_ACTY_TYPE nt WITH (NOLOCK) ON itm.MPLS_ACTY_TYPE_ID = nt.MPLS_ACTY_TYPE_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY nt.MPLS_ACTY_TYPE_DES
							FOR XML PATH('')), 1, 1, '') MplsActyTypeNames,
					
					-- MDS Network Activity
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.NTWK_ACTY_TYPE_ID))
							FROM dbo.QLFCTN_NTWK_ACTY_TYPE itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.NTWK_ACTY_TYPE_ID
							FOR XML PATH('')), 1, 1, '') NetworkActivityIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), nt.NTWK_ACTY_TYPE_DES))
							FROM dbo.QLFCTN_NTWK_ACTY_TYPE itm WITH (NOLOCK)
							JOIN dbo.LK_MDS_NTWK_ACTY_TYPE nt WITH (NOLOCK) ON itm.NTWK_ACTY_TYPE_ID = nt.NTWK_ACTY_TYPE_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY nt.NTWK_ACTY_TYPE_DES
							FOR XML PATH('')), 1, 1, '') NetworkActivityNames,

					-- Special Projects
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.SPCL_PROJ_ID))
							FROM dbo.QLFCTN_SPCL_PROJ itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.SPCL_PROJ_ID
							FOR XML PATH('')), 1, 1, '') SpecialProjectIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), sp.SPCL_PROJ_NME))
							FROM dbo.QLFCTN_SPCL_PROJ itm WITH (NOLOCK)
							JOIN dbo.LK_SPCL_PROJ sp WITH (NOLOCK) ON itm.SPCL_PROJ_ID = sp.SPCL_PROJ_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY sp.SPCL_PROJ_NME
							FOR XML PATH('')), 1, 1, '') SpecialProjectNames,

					-- Vendor Models
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.DEV_MODEL_ID))
							FROM dbo.QLFCTN_VNDR_MODEL itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.DEV_MODEL_ID
							FOR XML PATH('')), 1, 1, '') VendorModelIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), dmf.MANF_NME + ' - ' + dm.DEV_MODEL_NME))
							FROM dbo.QLFCTN_VNDR_MODEL itm WITH (NOLOCK)
							JOIN dbo.LK_DEV_MODEL dm WITH (NOLOCK) ON itm.DEV_MODEL_ID = dm.DEV_MODEL_ID
							JOIN dbo.LK_DEV_MANF dmf WITH (NOLOCK) ON dm.MANF_ID = dmf.MANF_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY dmf.MANF_NME + ' - ' + dm.DEV_MODEL_NME
							FOR XML PATH('')), 1, 1, '') VendorModelNames

			FROM dbo.LK_QLFCTN q WITH (NOLOCK)
			JOIN dbo.MAP_USR_PRF assignee WITH (NOLOCK) ON q.ASN_TO_USER_ID = assignee.USER_ID
			JOIN dbo.LK_USER au WITH (NOLOCK) ON q.ASN_TO_USER_ID = au.USER_ID
			JOIN dbo.LK_USER cu WITH (NOLOCK) ON q.CREAT_BY_USER_ID = cu.USER_ID
			LEFT JOIN dbo.LK_USER mu WITH (NOLOCK) ON q.MODFD_BY_USER_ID = mu.USER_ID
			WHERE q.REC_STUS_ID = 1 AND q.ASN_TO_USER_ID != 1
			AND assignee.REC_STUS_ID = 1
			AND assignee.USR_PRF_ID = (SELECT USR_PRF_ID FROM dbo.LK_USR_PRF WITH (NOLOCK) WHERE USR_PRF_NME LIKE '%MDSEActivator%')
		END

	ELSE
		BEGIN
			SELECT DISTINCT
					q.QLFCTN_ID AS 'QualificationID', q.REC_STUS_ID AS 'RecStatusID',
					q.ASN_TO_USER_ID AS 'AssignToUserID', au.DSPL_NME AS 'AssignToUser', 
					au.USER_ADID AS 'AssignToUserAdId', 
					q.CREAT_BY_USER_ID AS 'CreatedByUserID', cu.DSPL_NME AS 'CreatedByUser',
					cu.USER_ADID AS 'CreatedByAdId', q.CREAT_DT AS 'CreatedDate',
					q.MODFD_BY_USER_ID AS 'ModifiedByUserID', mu.DSPL_NME AS 'ModifiedByUser',
					mu.USER_ADID AS 'ModifiedByAdId', q.MODFD_DT AS 'ModifiedDate',
					-- Dedicated Customers
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.CUST_ID))
							FROM dbo.QLFCTN_DEDCTD_CUST itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.CUST_ID
							FOR XML PATH('')), 1, 1, '') CustomerIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), dc.CUST_NME))
							FROM dbo.QLFCTN_DEDCTD_CUST itm WITH (NOLOCK)
							JOIN dbo.LK_DEDCTD_CUST dc WITH (NOLOCK) ON itm.CUST_ID = dc.CUST_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY dc.CUST_NME
							FOR XML PATH('')), 1, 1, '') CustomerNames,

					-- Device
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.DEV_ID))
							FROM dbo.QLFCTN_DEV itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.DEV_ID
							FOR XML PATH('')), 1, 1, '') DeviceIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), d.DEV_NME))
							FROM dbo.QLFCTN_DEV itm WITH (NOLOCK)
							JOIN dbo.LK_DEV d WITH (NOLOCK) ON itm.DEV_ID = d.DEV_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY d.DEV_NME
							FOR XML PATH('')), 1, 1, '') DeviceNames,

					-- Enhance Services
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.ENHNC_SRVC_ID))
							FROM dbo.QLFCTN_ENHNC_SRVC itm WITH (NOLOCK)
							JOIN dbo.LK_ENHNC_SRVC es WITH (NOLOCK) ON itm.ENHNC_SRVC_ID = es.ENHNC_SRVC_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID AND es.REC_STUS_ID = 1
							ORDER BY itm.ENHNC_SRVC_ID
							FOR XML PATH('')), 1, 1, '') EnhanceServiceIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), es.ENHNC_SRVC_NME))
							FROM dbo.QLFCTN_ENHNC_SRVC itm WITH (NOLOCK)
							JOIN dbo.LK_ENHNC_SRVC es WITH (NOLOCK) ON itm.ENHNC_SRVC_ID = es.ENHNC_SRVC_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID AND es.REC_STUS_ID = 1
							ORDER BY es.ENHNC_SRVC_NME
							FOR XML PATH('')), 1, 1, '') EnhanceServiceNames,

					-- Event Types
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.EVENT_TYPE_ID))
							FROM dbo.QLFCTN_EVENT_TYPE itm WITH (NOLOCK)
							JOIN dbo.LK_EVENT_TYPE et WITH (NOLOCK) ON itm.EVENT_TYPE_ID = et.EVENT_TYPE_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID AND et.REC_STUS_ID = 1
							ORDER BY itm.EVENT_TYPE_ID
							FOR XML PATH('')), 1, 1, '') EventTypeIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), et.EVENT_TYPE_NME))
							FROM dbo.QLFCTN_EVENT_TYPE itm WITH (NOLOCK)
							JOIN dbo.LK_EVENT_TYPE et WITH (NOLOCK) ON itm.EVENT_TYPE_ID = et.EVENT_TYPE_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID AND et.REC_STUS_ID = 1
							ORDER BY et.EVENT_TYPE_NME
							FOR XML PATH('')), 1, 1, '') EventTypeNames,

					-- IP Version
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.IP_VER_ID))
							FROM dbo.QLFCTN_IP_VER itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.IP_VER_ID
							FOR XML PATH('')), 1, 1, '') IPVersionIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), ip.IP_VER_NME))
							FROM dbo.QLFCTN_IP_VER itm WITH (NOLOCK)
							JOIN dbo.LK_IP_VER ip WITH (NOLOCK) ON itm.IP_VER_ID = ip.IP_VER_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY ip.IP_VER_NME
							FOR XML PATH('')), 1, 1, '') IPVersionNames,

					-- MPLS Activity Type
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.MPLS_ACTY_TYPE_ID))
							FROM dbo.QLFCTN_MPLS_ACTY_TYPE itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.MPLS_ACTY_TYPE_ID
							FOR XML PATH('')), 1, 1, '') MplsActyTypeIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), nt.MPLS_ACTY_TYPE_DES))
							FROM dbo.QLFCTN_MPLS_ACTY_TYPE itm WITH (NOLOCK)
							JOIN dbo.LK_MPLS_ACTY_TYPE nt WITH (NOLOCK) ON itm.MPLS_ACTY_TYPE_ID = nt.MPLS_ACTY_TYPE_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY nt.MPLS_ACTY_TYPE_DES
							FOR XML PATH('')), 1, 1, '') MplsActyTypeNames,
					
					-- MDS Network Activity
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.NTWK_ACTY_TYPE_ID))
							FROM dbo.QLFCTN_NTWK_ACTY_TYPE itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.NTWK_ACTY_TYPE_ID
							FOR XML PATH('')), 1, 1, '') NetworkActivityIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), nt.NTWK_ACTY_TYPE_DES))
							FROM dbo.QLFCTN_NTWK_ACTY_TYPE itm WITH (NOLOCK)
							JOIN dbo.LK_MDS_NTWK_ACTY_TYPE nt WITH (NOLOCK) ON itm.NTWK_ACTY_TYPE_ID = nt.NTWK_ACTY_TYPE_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY nt.NTWK_ACTY_TYPE_DES
							FOR XML PATH('')), 1, 1, '') NetworkActivityNames,

					-- Special Projects
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.SPCL_PROJ_ID))
							FROM dbo.QLFCTN_SPCL_PROJ itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.SPCL_PROJ_ID
							FOR XML PATH('')), 1, 1, '') SpecialProjectIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), sp.SPCL_PROJ_NME))
							FROM dbo.QLFCTN_SPCL_PROJ itm WITH (NOLOCK)
							JOIN dbo.LK_SPCL_PROJ sp WITH (NOLOCK) ON itm.SPCL_PROJ_ID = sp.SPCL_PROJ_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY sp.SPCL_PROJ_NME
							FOR XML PATH('')), 1, 1, '') SpecialProjectNames,

					-- Vendor Models
					STUFF((SELECT ',' + RTRIM(CONVERT(VARCHAR(MAX), itm.DEV_MODEL_ID))
							FROM dbo.QLFCTN_VNDR_MODEL itm WITH (NOLOCK)
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY itm.DEV_MODEL_ID
							FOR XML PATH('')), 1, 1, '') VendorModelIDs,
					STUFF((SELECT '; ' + RTRIM(CONVERT(VARCHAR(MAX), dmf.MANF_NME + ' - ' + dm.DEV_MODEL_NME))
							FROM dbo.QLFCTN_VNDR_MODEL itm WITH (NOLOCK)
							JOIN dbo.LK_DEV_MODEL dm WITH (NOLOCK) ON itm.DEV_MODEL_ID = dm.DEV_MODEL_ID
							JOIN dbo.LK_DEV_MANF dmf WITH (NOLOCK) ON dm.MANF_ID = dmf.MANF_ID
							WHERE itm.QLFCTN_ID = q.QLFCTN_ID
							ORDER BY dmf.MANF_NME + ' - ' + dm.DEV_MODEL_NME
							FOR XML PATH('')), 1, 1, '') VendorModelNames

			FROM dbo.LK_QLFCTN q WITH (NOLOCK)
			JOIN dbo.LK_USER au WITH (NOLOCK) ON q.ASN_TO_USER_ID = au.USER_ID
			JOIN dbo.LK_USER cu WITH (NOLOCK) ON q.CREAT_BY_USER_ID = cu.USER_ID
			LEFT JOIN dbo.LK_USER mu WITH (NOLOCK) ON q.MODFD_BY_USER_ID = mu.USER_ID
			WHERE q.REC_STUS_ID = 1 AND q.ASN_TO_USER_ID = @USERID
		END

END TRY 
  
BEGIN CATCH
 EXEC [dbo].[insertErrorInfo]  
END CATCH
END