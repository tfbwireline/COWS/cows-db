--PJ008006 - Change 01 - 03262015

USE [COWS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK01_LK_ASR_TYPE]') AND parent_object_id = OBJECT_ID(N'[dbo].[LK_ASR_TYPE]'))
ALTER TABLE [dbo].[LK_ASR_TYPE] DROP CONSTRAINT [FK01_LK_ASR_TYPE]
GO

USE [COWS]
GO

/****** Object:  Table [dbo].[LK_ASR_TYPE]    Script Date: 05/03/2015 12:19:47 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[LK_ASR_TYPE]') AND type in (N'U'))
DROP TABLE [dbo].[LK_ASR_TYPE]
GO

---------------------

-- Table LK_ASR_TYPE to store different types of ASR which would be used for XNCI group with COWS to communicate with internal and external vendors

CREATE TABLE [dbo].[LK_ASR_TYPE]
( 
	[ASR_TYPE_ID]             int  NOT NULL ,
	[ASR_TYPE_NME]       varchar(100)  NOT NULL ,
	[ASR_TYPE_DES]       varchar(200)  NULL ,
	[CREAT_DT]           smalldatetime  NOT NULL ,
	[CREAT_BY_USER_ID]        int  NULL ,
	CONSTRAINT [PK_LK_ASR_TYPE] PRIMARY KEY  CLUSTERED ([ASR_TYPE_ID] ASC),
	CONSTRAINT [FK01_LK_ASR_TYPE] FOREIGN KEY ([CREAT_BY_USER_ID]) REFERENCES [LK_USER]([USER_ID])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
go

--IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[arch].[FSA_ORDR]') AND name = N'UX01_FSA_ORDR_arch')
--ALTER TABLE [arch].[FSA_ORDR] DROP CONSTRAINT [UX01_FSA_ORDR_arch]
--GO

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[FSA_ORDR]') AND name = N'UX01_FSA_ORDR')
ALTER TABLE [dbo].[FSA_ORDR] DROP CONSTRAINT [UX01_FSA_ORDR]
GO

--IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[arch].[FSA_ORDR]') AND name = N'UX01_FSA_ORDR')
--ALTER TABLE [arch].[FSA_ORDR] DROP CONSTRAINT [UX01_FSA_ORDR]
--GO


--ALTER TABLE [arch].[FSA_ORDR] ALTER COLUMN [FTN] varchar(50)  NOT NULL
--go

ALTER TABLE [dbo].[FSA_ORDR] ALTER COLUMN [FTN] varchar(50)  NOT NULL
go

--ALTER TABLE [arch].[FSA_ORDR]
--	ADD CONSTRAINT [UX01_FSA_ORDR_arch] UNIQUE ([ORDR_ACTN_ID]  ASC,[FTN]  ASC,[PROD_TYPE_CD]  ASC)
--go

ALTER TABLE [dbo].[FSA_ORDR]
	ADD CONSTRAINT [UX01_FSA_ORDR] UNIQUE ([ORDR_ACTN_ID]  ASC,[FTN]  ASC,[PROD_TYPE_CD]  ASC)
go


ALTER TABLE [dbo].[LK_PPRT]
	ADD [ORDR_CAT_ID]  smallint  NULL
go


-- Table ASR to hold different ASR entries for MACH5 orders

CREATE TABLE [dbo].[ASR]
( 
	[ORDR_ID]            int  NOT NULL ,
	[ASR_TYPE_ID]        int  NOT NULL ,
	[IP_NODE_TXT]        varchar(100)  NULL ,
	[ACCS_BDWD_DES]      varchar(100)  NULL ,
	[ACCS_CTY_NME_SITE_CD] varchar(100)  NULL ,
	[TRNSPNT_CD]         bit  NULL ,
	[LEC_NNI_NBR]        varchar(50)  NULL ,
	[ENTRNC_ASMT_TXT]    varchar(100)  NULL ,
	[MSTR_FTN_CD]        int  NULL ,
	[DLCI_DES]           varchar(100)  NULL ,
	[H1_MATCH_MSTR_VAS_CD] bit  NULL ,
	[ASR_NOTES_TXT]      varchar(1000)  NULL ,
	[CREAT_DT]           datetime  NOT NULL ,
	[STUS_ID]            tinyint  NOT NULL ,
	[CREAT_BY_USER_ID]        int  NULL ,
	CONSTRAINT [FK01_ASR] FOREIGN KEY ([ORDR_ID]) REFERENCES [dbo].[ORDR]([ORDR_ID])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
CONSTRAINT [FK02_ASR] FOREIGN KEY ([ASR_TYPE_ID]) REFERENCES [dbo].[LK_ASR_TYPE]([ASR_TYPE_ID])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
CONSTRAINT [FK03_ASR] FOREIGN KEY ([CREAT_BY_USER_ID]) REFERENCES [dbo].[LK_USER]([USER_ID])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION,
CONSTRAINT [FK04_ASR] FOREIGN KEY ([STUS_ID]) REFERENCES [dbo].[LK_STUS]([STUS_ID])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
)
go

CREATE NONCLUSTERED INDEX [FK01_ASR] ON [dbo].[ASR]
( 
	[ORDR_ID]             ASC
)
go

CREATE NONCLUSTERED INDEX [FK02_ASR] ON [dbo].[ASR]
( 
	[ASR_TYPE_ID]         ASC
)
go

CREATE NONCLUSTERED INDEX [FK03_ASR] ON [dbo].[ASR]
( 
	[CREAT_BY_USER_ID]         ASC
)
go

CREATE NONCLUSTERED INDEX [FK04_ASR] ON [dbo].[ASR]
( 
	[STUS_ID]             ASC
)
go


-- Table M5_XML_MSG_CMCTN for inbound and outbound message communication from MACH5 to COWS or viceversa

CREATE TABLE [dbo].[M5_XML_MSG_CMCTN]
( 
	[MSG_XML_ID]         int  NOT NULL ,
	[MSG_XML_TXT]        varchar(max)  NOT NULL ,
	[ERROR_MSG_TXT]      varchar(max)  NULL ,
	[CREAT_DT]           smalldatetime  NOT NULL ,
	[IS_ERROR_CD]        bit  NOT NULL ,
	[IS_REJ_CD]          bit  NOT NULL ,
	[M5_ID]              varchar(50)  NULL ,
	CONSTRAINT [PK_M5_XML_MSG_CMCTN] PRIMARY KEY  CLUSTERED ([MSG_XML_ID] ASC)
)
go

ALTER TABLE [dbo].[M5_XML_MSG_CMCTN]
	ADD CONSTRAINT [DF_getdate_1080692515]
		 DEFAULT  getdate() FOR [CREAT_DT]
go

ALTER TABLE [dbo].[M5_XML_MSG_CMCTN]
	ADD CONSTRAINT [Default_Value_223_1018792366]
		 DEFAULT  (0) FOR [IS_REJ_CD]
go


--
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO