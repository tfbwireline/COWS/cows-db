USE [COWS]
GO

/****** Object:  Table [AD\ra214145].[M5_BPM_COWS_NFRC]    Script Date: 09/29/2015 17:41:02 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[M5_BPM_COWS_NRFC](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ORDR_ID] [int] NOT NULL,
	[TYPE_NME] [varchar](2) NULL,
	[SUB_TYPE_NME] [varchar](2) NULL,
	[RELTD_ORDR_ID] [int] NULL,
	[PROD_CD] [varchar](5) NULL,
	[H6_ID] [varchar](9) NULL,
	[H1_ID] [varchar](9) NULL,
	[XTRCT_CD] [varchar](1) NULL,
	[XTRCT_DT] [date] NULL,
	[FSA_ORDR_ID] [int] NULL,
	[FTN] [varchar](20) NULL,
	[STUS_ID] [smallint] NULL,
	[Error_msg] [varchar](1000) NULL,
 CONSTRAINT [PK_M5_BPM_COWS_NFRC] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


