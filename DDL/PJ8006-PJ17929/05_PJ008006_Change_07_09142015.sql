USE [COWS]
GO

ALTER TABLE dbo.AD_EVENT_ACCS_TAG
ALTER COLUMN OLD_CKT_ID VARCHAR(30) NULL
GO

ALTER TABLE dbo.AD_EVENT_ACCS_TAG
ALTER COLUMN CKT_ID VARCHAR(30) NOT NULL
GO

