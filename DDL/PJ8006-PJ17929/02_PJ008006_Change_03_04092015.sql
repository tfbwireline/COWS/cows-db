--PJ008006 - Change 03 - 04092015

USE [COWS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE DEFAULT [DF_getdate_1_20150402]
	AS getdate()
go

---------------------

-- Add new columns to dbo.ORDR_ADR
ALTER TABLE [dbo].[ORDR_ADR]
ADD [STREET_ADR_3] varbinary(max)  NULL
go

ALTER TABLE [dbo].[ORDR_ADR]
ADD [STE_TXT] varbinary(max)  NULL
go

-- Add new columns to arch.ORDR_ADR
--ALTER TABLE [arch].[ORDR_ADR]
--ADD [STREET_ADR_3] varbinary(max)  NULL
--go

--ALTER TABLE [arch].[ORDR_ADR]
--ADD [STE_TXT] varbinary(max)  NULL
--go

-- Add new columns to dbo.FSA_ORDR_CPE_LINE_ITEM
--ALTER TABLE [dbo].[FSA_ORDR_CPE_LINE_ITEM]
--ADD [CNTRC_NBR]  varchar(30)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR_CPE_LINE_ITEM]
--ADD [ACCS_BDWD_CD]  varchar(5)  NULL
--go

-- Add new columns to arch.FSA_ORDR_CPE_LINE_ITEM
--ALTER TABLE [arch].[FSA_ORDR_CPE_LINE_ITEM]
--ADD [CNTRC_NBR]  varchar(30)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR_CPE_LINE_ITEM]
--ADD [ACCS_BDWD_CD]  varchar(5)  NULL
--go

-- Add new columns to dbo.FSA_ORDR
ALTER TABLE [dbo].[FSA_ORDR]
ADD [TTRPT_ACCS_TERM_DES]  varchar(30)  NULL
go

ALTER TABLE [dbo].[FSA_ORDR]
ADD [TTRPT_QOT_XPIRN_DT]  date  NULL
go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_MRK_UP_AMT]  integer  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_TAX_AMT]  integer  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_BEAR_CHG_AMT]  integer  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_ADDON_CHG_1_AMT]  integer  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_ADDON_CHG_2_AMT]  integer  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_CUR_CD]  varchar(5)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TPORT_ACTL_HOST_SRVR_NBR]  integer  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [INSTLN_MODEM_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [INSTLN_PRIM_DNS_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [INSTLN_PRIM_DNS_RGSTR_WITH_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [INSTLN_SCNDY_DNS_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [INSTLN_SCNDY_DNS_SRVC_TXT]  varchar(50)  NULL
--go

ALTER TABLE [dbo].[FSA_ORDR]
ADD [TPORT_SPCL_INSTRTN_TXT]  varchar(100)  NULL
go

ALTER TABLE [dbo].[FSA_ORDR]
ADD [TPORT_DIA_CLASS_OF_SRVC_TXT]  varchar(50)  NULL
go

ALTER TABLE [dbo].[FSA_ORDR]
ADD [TPORT_DIA_NOC_TO_NOC_TXT]  varchar(50)  NULL
go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TPORT_DIA_INET_RGSTR_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TPORT_NEW_CNSTRCTN_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TPORT_UNMANNED_SITE_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TPORT_CAL_BEF_DSPCH_TXT]  varchar(50)  NULL
--go

ALTER TABLE [dbo].[FSA_ORDR]
ADD [TPORT_DUAL_STK_ADR_PRVDR_TXT]  varchar(50)  NULL
go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TPORT_PVC_NBR]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR]
--ADD [TPORT_DTE_DEV_TXT]  varchar(50)  NULL
--go

ALTER TABLE [dbo].[FSA_ORDR]
ADD [TPORT_LINK_MGT_PRCOL_TXT]  varchar(50)  NULL
go

ALTER TABLE [dbo].[FSA_ORDR]
ADD [TPORT_PROJ_DES]  varchar(50)  NULL
go

-- Add new columns to arch.FSA_ORDR
--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TTRPT_ACCS_TERM_DES]  varchar(30)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TTRPT_QOT_XPIRN_DT]  date  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_MRK_UP_AMT]  integer  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_TAX_AMT]  integer  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_BEAR_CHG_AMT]  integer  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_ADDON_CHG_1_AMT]  integer  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_ADDON_CHG_2_AMT]  integer  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TTRPT_ACCS_QOT_CUR_CD]  varchar(5)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_ACTL_HOST_SRVR_NBR]  integer  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [INSTLN_MODEM_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [INSTLN_PRIM_DNS_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [INSTLN_PRIM_DNS_RGSTR_WITH_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [INSTLN_SCNDY_DNS_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [INSTLN_SCNDY_DNS_SRVC_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_SPCL_INSTRTN_TXT]  varchar(100)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_DIA_CLASS_OF_SRVC_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_DIA_NOC_TO_NOC_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_DIA_INET_RGSTR_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_NEW_CNSTRCTN_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_UNMANNED_SITE_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_CAL_BEF_DSPCH_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_DUAL_STK_ADR_PRVDR_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_PVC_NBR]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_DTE_DEV_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_LINK_MGT_PRCOL_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR]
--ADD [TPORT_PROJ_DES]  varchar(50)  NULL
--go

-- Create new table dbo.ORDR_HOST_INET
--CREATE TABLE [dbo].[ORDR_HOST_INET]
--( 
--	[ORDR_ID]            int  NOT NULL ,
--	[IP_ADR]             varchar(50)  NOT NULL ,
--	[CREAT_DT]           date  NOT NULL 
--)
--go

-- Create new table arch.ORDR_HOST_INET
--CREATE TABLE [arch].[ORDR_HOST_INET]
--( 
--	[ORDR_ID]            int  NOT NULL ,
--	[IP_ADR]             varchar(50)  NOT NULL ,
--	[CREAT_DT]           date  NOT NULL 
--)
--go


---- Add new columns to table dbo.FSA_ORDR_BILL_LINE_ITEM
--ALTER TABLE [dbo].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [BNDL_TYPE_CD]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [BNDL_BIC_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PROMO_BIC_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PORT_BNDL_INDCR_CD]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PORT_MNU_BSD_INDCR_CD]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PORT_BIC_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [dbo].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PORT_PROMO_TXT]  varchar(50)  NULL
--go


---- Add new columns to table arch.FSA_ORDR_BILL_LINE_ITEM
--ALTER TABLE [arch].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [BNDL_TYPE_CD]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [BNDL_BIC_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PROMO_BIC_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PORT_BNDL_INDCR_CD]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PORT_MNU_BSD_INDCR_CD]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PORT_BIC_TXT]  varchar(50)  NULL
--go

--ALTER TABLE [arch].[FSA_ORDR_BILL_LINE_ITEM]
--ADD [PORT_PROMO_TXT]  varchar(50)  NULL
--go


-- Create new table arch.ASR to hold different ASR entries for MACH5 orders
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK01_ASR]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASR]'))
ALTER TABLE [dbo].[ASR] DROP CONSTRAINT [FK01_ASR]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK02_ASR]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASR]'))
ALTER TABLE [dbo].[ASR] DROP CONSTRAINT [FK02_ASR]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK03_ASR]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASR]'))
ALTER TABLE [dbo].[ASR] DROP CONSTRAINT [FK03_ASR]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK04_ASR]') AND parent_object_id = OBJECT_ID(N'[dbo].[ASR]'))
ALTER TABLE [dbo].[ASR] DROP CONSTRAINT [FK04_ASR]
GO



/****** Object:  Table [dbo].[ASR]    Script Date: 04/13/2015 12:06:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[ASR]') AND type in (N'U'))
DROP TABLE [dbo].[ASR]
GO

CREATE TABLE [dbo].[ASR]
( 
	[ASR_ID]             int  NOT NULL  IDENTITY ( 1,1 ) NOT FOR REPLICATION ,
	[ORDR_ID]            int  NOT NULL ,
	[ASR_TYPE_ID]        int  NOT NULL ,
	[IP_NODE_TXT]        varchar(100)  NULL ,
	[ACCS_BDWD_DES]      varchar(100)  NULL ,
	[ACCS_CTY_NME_SITE_CD] varchar(100)  NULL ,
	[TRNSPNT_CD]         bit  NULL ,
	[LEC_NNI_NBR]        int  NULL ,
	[ENTRNC_ASMT_TXT]    varchar(100)  NULL ,
	[MSTR_FTN_CD]        int  NULL ,
	[DLCI_DES]           varchar(100)  NULL ,
	[H1_MATCH_MSTR_VAS_CD] bit  NULL ,
	[ASR_NOTES_TXT]      varchar(1000)  NULL ,
	[CREAT_DT]           datetime  NOT NULL ,
	[STUS_ID]            tinyint  NOT NULL ,
	[CREAT_BY_USER_ID]   int  NOT NULL ,
	[PTNR_CXR_CD]        varchar(100)  NULL
)
go

ALTER TABLE [dbo].[ASR]
	ADD CONSTRAINT [PK_ASR] PRIMARY KEY  CLUSTERED ([ASR_ID] ASC)
go

ALTER TABLE [dbo].[ASR]
	ADD CONSTRAINT [FK01_MACH5_ASR] FOREIGN KEY ([ORDR_ID]) REFERENCES [dbo].[ORDR]([ORDR_ID])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [dbo].[ASR]
	ADD CONSTRAINT [FK02_MACH5_ASR] FOREIGN KEY ([ASR_TYPE_ID]) REFERENCES [dbo].[LK_ASR_TYPE]([ASR_TYPE_ID])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [dbo].[ASR]
	ADD CONSTRAINT [FK03_MACH5_ASR] FOREIGN KEY ([CREAT_BY_USER_ID]) REFERENCES [dbo].[LK_USER]([USER_ID])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

ALTER TABLE [dbo].[ASR]
	ADD CONSTRAINT [FK04_MACH5_ASR] FOREIGN KEY ([STUS_ID]) REFERENCES [dbo].[LK_STUS]([STUS_ID])
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go



--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[arch].[FK01_ASR]') AND parent_object_id = OBJECT_ID(N'[arch].[ASR]'))
--ALTER TABLE [arch].[ASR] DROP CONSTRAINT [FK01_ASR]
--GO

--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[arch].[FK02_ASR]') AND parent_object_id = OBJECT_ID(N'[arch].[ASR]'))
--ALTER TABLE [arch].[ASR] DROP CONSTRAINT [FK02_ASR]
--GO

--IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[arch].[FK03_ASR]') AND parent_object_id = OBJECT_ID(N'[arch].[ASR]'))
--ALTER TABLE [arch].[ASR] DROP CONSTRAINT [FK03_ASR]
--GO

--/****** Object:  Table [arch].[ASR]    Script Date: 04/13/2015 12:06:22 ******/
--IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[arch].[ASR]') AND type in (N'U'))
--DROP TABLE [arch].[ASR]
--GO

--CREATE TABLE [arch].[ASR]
--( 
--	[ASR_ID]             int  NOT NULL IDENTITY ( 1,1 ) NOT FOR REPLICATION,
--	[ORDR_ID]            int  NOT NULL ,
--	[ASR_TYPE_ID]        int  NOT NULL ,
--	[IP_NODE_TXT]        varchar(100)  NULL ,
--	[ACCS_BDWD_DES]      varchar(100)  NULL ,
--	[ACCS_CTY_NME_SITE_CD] varchar(100)  NULL ,
--	[TRNSPNT_CD]         bit  NULL ,
--	[LEC_NNI_NBR]        int  NULL ,
--	[ENTRNC_ASMT_TXT]    varchar(100)  NULL ,
--	[MSTR_FTN_CD]        int  NULL ,
--	[DLCI_DES]           varchar(100)  NULL ,
--	[H1_MATCH_MSTR_VAS_CD] bit  NULL ,
--	[ASR_NOTES_TXT]      varchar(1000)  NULL ,
--	[CREAT_DT]           datetime  NOT NULL ,
--	[STUS_ID]            tinyint  NOT NULL ,
--	[USER_ID]            int  NOT NULL ,
--	[PTNR_CXR_CD]        varchar(100)  NULL 
--)
--go

--ALTER TABLE [arch].[ASR]
--	ADD CONSTRAINT [PK_ARCH_ASR] PRIMARY KEY  CLUSTERED ([ASR_ID] ASC)
--go

--ALTER TABLE [arch].[ASR]
--	ADD CONSTRAINT [FK01_ARCH_ASR] FOREIGN KEY ([ORDR_ID]) REFERENCES [arch].[ORDR]([ORDR_ID])
--		ON DELETE NO ACTION
--		ON UPDATE NO ACTION
--go

--ALTER TABLE [arch].[ASR]
--	ADD CONSTRAINT [FK02_ARCH_ASR] FOREIGN KEY ([ASR_TYPE_ID]) REFERENCES [dbo].[LK_ASR_TYPE]([ASR_TYPE_ID])
--		ON DELETE NO ACTION
--		ON UPDATE NO ACTION
--go

--ALTER TABLE [arch].[ASR]
--	ADD CONSTRAINT [FK03_ARCH_ASR] FOREIGN KEY ([USER_ID]) REFERENCES [dbo].[LK_USER]([USER_ID])
--		ON DELETE NO ACTION
--		ON UPDATE NO ACTION
--go

--ALTER TABLE [arch].[ASR]
--	ADD CONSTRAINT [FK04_ARCH_ASR] FOREIGN KEY ([STUS_ID]) REFERENCES [dbo].[LK_STUS]([STUS_ID])
--		ON DELETE NO ACTION
--		ON UPDATE NO ACTION
--go


--
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO
