INSERT INTO [COWS].[dbo].[LK_MDS_3RDPARTY_SRVC_LVL]
           ([THRD_PARTY_SRVC_LVL_ID]
           ,[THRD_PARTY_SRVC_LVL_DES]
           ,[SRVC_TYPE_ID]
           ,[REC_STUS_ID]
           ,[CREAT_DT]
           ,[CREAT_BY_USER_ID]
           ,[MODFD_DT]
           ,[MODFD_BY_USER_ID])
     VALUES
           (7
           ,'Premium'
           ,8
           ,1
           ,GETDATE()
           ,1
           ,NULL
           ,NULL)
           ,(8
           ,'Enterprise'
           ,8
           ,1
           ,GETDATE()
           ,1
           ,NULL
           ,NULL)
GO


