USE [COWS]
GO
SET NOCOUNT ON
GO
IF NOT EXISTS
	(SELECT 'X' FROM [COWS].[dbo].[LK_SYS_CFG] WITH (NOLOCK) WHERE PRMTR_NME = 'Network Intl Only^MDSSlotDuration')
	BEGIN
		INSERT INTO [COWS].[dbo].[LK_SYS_CFG] (PRMTR_NME, PRMTR_VALU_TXT)
		SELECT 'Network Intl Only^MDSSlotDuration', '60,30'
	END
GO