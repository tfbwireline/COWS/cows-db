USE [COWS]
GO
/****** Object:  StoredProcedure [dbo].[resetIsLoggedToday]    Script Date: 05/16/2022 4:04:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jbolano15
-- Create date: 05/16/20222
-- Description:	Reset IS_LOGGED_TODAY value to 0
-- =============================================
CREATE PROCEDURE [dbo].[resetIsLoggedToday]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE LK_USER SET IS_LOGGED_TODAY = 0, MODFD_DT = GETDATE() WHERE IS_LOGGED_TODAY = 1
END
