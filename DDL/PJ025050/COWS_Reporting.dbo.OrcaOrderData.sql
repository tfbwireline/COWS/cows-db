Use COWS_Reporting

Alter table [dbo].[OrcaOrderData]
	ADD TPORT_CALC_RT_MRC_IN_CUR_AMT varchar (25),
		TPORT_CALC_RT_NRC_IN_CUR_AMT varchar (25),
		TPORT_CUR_NME  varchar (50),
		TPORT_CALC_RT_MRC_USD_AMT  varchar (25),
		TPORT_CALC_RT_NRC_USD_AMT  varchar (25)

/*
vendor raw MRC local currency quoted cost, 
vendor raw NRC local currency quoted cost
Currency Type
Vendor raw MRC USD quoted cost
Vendor raw NRC USD Quoted cost
*/