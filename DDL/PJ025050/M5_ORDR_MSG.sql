USE [COWS]
GO

/****** Object:  Table [dbo].[M5_ORDR_MSG]    Script Date: 02/18/2020 2:39:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[M5_BILL_CLEAR](
	[M5_BILL_CLEAR_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[ORDR_ID] [int] NOT NULL,
	[BILL_CLEAR_DT] [smalldatetime] NOT NULL,
	[CREAT_DT] [smalldatetime] NULL,
	[SENT_DT] [smalldatetime] NULL,
	[STUS_ID] [smallint] NOT NULL
	)
	
GO