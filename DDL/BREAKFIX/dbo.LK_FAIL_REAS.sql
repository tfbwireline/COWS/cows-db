use cows
go

ALTER TABLE dbo.LK_FAIL_REAS
ALTER COLUMN NTWK_ONLY_CD TINYINT NULL
GO

update dbo.lk_fail_reas set NTWK_ONLY_CD=2
where FAIL_REAS_DES in ('MDS Team Failure',
'Wireless',
'CPE Late/No Show',
'1mb Dial Access',
'Phase Complete, Sending back for next Phase',
'Return to Published')
