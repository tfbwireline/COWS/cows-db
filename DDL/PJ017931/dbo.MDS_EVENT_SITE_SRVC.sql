USE [COWS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO


ALTER TABLE dbo.MDS_EVENT_SITE_SRVC
ADD [M5_ORDR_NBR] VARCHAR(50) NULL
GO


SET ANSI_PADDING OFF
GO
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
