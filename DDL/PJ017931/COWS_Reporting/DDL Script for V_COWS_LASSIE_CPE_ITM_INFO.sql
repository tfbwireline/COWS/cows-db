USE [COWS_Reporting]
GO

/****** Object:  View [dbo].[V_COWS_LASSIE_CPE_ITM_INFO]    Script Date: 07/26/2017 11:18:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO









-----------------------------
-- 1. Create view V_COWS_LASSIE_CPE_ITM_INFO
ALTER VIEW [dbo].[V_COWS_LASSIE_CPE_ITM_INFO]

AS

SELECT Order_ID
	,ORDR_ID
	,SITE_ID
	,DEVICE_ID
	,CPE_CLLI AS [CPE Serving CLLI Code]
	,PLSFT_RQSTN_NBR AS [Material Req]
	,PID AS [Project Id]
	,RAS_DT
	,SUPPLIER AS [Supplier Name]
	,PRCH_ORDR_NBR
	,CMPNT_FMLY AS [Component Family]
	,RQSTN_DT AS [Requisition DT]
	,EQPT_ITM_RCVD_DT AS [Eqpt Receive DT]
	,EQPT_RCVD_BY_ADID AS [Equpt Receive By]
	from COWS_Reporting.dbo.LassieCpeItemData









GO


