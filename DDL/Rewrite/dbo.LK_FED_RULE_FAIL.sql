USE COWS
GO

CREATE TABLE [dbo].[LK_FED_RULE_FAIL](
	[FED_RULE_FAIL_ID] [tinyint] NOT NULL,
	[EVENT_RULE_ID] [int] NOT NULL,
	[FAIL_CD_ID] [smallint] NOT NULL,
	[REC_STUS_ID] [tinyint] NOT NULL,
	[MODFD_DT] [smalldatetime] NULL,
	[CREAT_DT] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_LK_FED_RULE_FAIL] PRIMARY KEY CLUSTERED 
(
	[FED_RULE_FAIL_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LK_FED_RULE_FAIL] ADD  CONSTRAINT [DF_LK_FED_RULE_FAIL_1]  DEFAULT ((1)) FOR [REC_STUS_ID]
GO

ALTER TABLE [dbo].[LK_FED_RULE_FAIL] ADD  CONSTRAINT [DF_LK_FED_RULE_FAIL_getdate]  DEFAULT (getdate()) FOR [CREAT_DT]
GO

ALTER TABLE [dbo].[LK_FED_RULE_FAIL]  ADD  CONSTRAINT [FK01_LK_FED_RULE_FAIL] FOREIGN KEY([REC_STUS_ID])
REFERENCES [dbo].[LK_REC_STUS] ([REC_STUS_ID])
GO
ALTER TABLE [dbo].[LK_FED_RULE_FAIL]  WITH CHECK ADD  CONSTRAINT [FK02_LK_FED_RULE_FAIL] FOREIGN KEY([EVENT_RULE_ID])
REFERENCES [dbo].[LK_EVENT_RULE] ([EVENT_RULE_ID])
go
ALTER TABLE [dbo].[LK_FED_RULE_FAIL]  WITH CHECK ADD  CONSTRAINT [FK03_LK_FED_RULE_FAIL] FOREIGN KEY([FAIL_CD_ID])
REFERENCES [dbo].[LK_FEDLINE_EVENT_FAIL_CD] ([FAIL_CD_ID])
go