USE [COWS]
GO

IF EXISTS (SELECT * FROM sys.symmetric_keys WHERE name = 'FS@K3y')
BEGIN
	if exists(select 1 
              from sys.openkeys 
              where key_name = 'FS@K3y' and database_name = db_name()
              )
    BEGIN
		CLOSE SYMMETRIC KEY FS@K3y;
	END
	DROP SYMMETRIC KEY FS@K3y;
END

IF EXISTS
	(SELECT * FROM sys.certificates WHERE name='S3cFS@CustInf0')
BEGIN
	DROP CERTIFICATE S3cFS@CustInf0
END


IF EXISTS 
    (SELECT * FROM sys.symmetric_keys WHERE name LIKE '%MasterKey##')
BEGIN
	DROP MASTER KEY 
END
GO

CREATE MASTER KEY ENCRYPTION BY 
	PASSWORD = 'S8r3s7R@m9s7J@g@nS8m@nN@1d8&*(&%^tu*B#######897By^&*b&*678IH()_)'
GO

CREATE CERTIFICATE S3cFS@CustInf0
   WITH SUBJECT = 'Secured FSA Customer Info';
GO

CREATE SYMMETRIC KEY FS@K3y
WITH KEY_SOURCE = 'S8r3s7R@m9s7J@g@nS8m@nN@1d8',
    IDENTITY_VALUE = 'S8r3s7R@m9s7J@g@nS8m@nN@1d8',
    ALGORITHM = AES_256
    ENCRYPTION BY CERTIFICATE S3cFS@CustInf0;
GO

GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\QDA-App-Admins]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\QDA-App-Admins]

GRANT CONTROL ON CERTIFICATE::S3cFS@CustInf0 TO [AD\QDA-App-Developers]
GRANT VIEW DEFINITION ON SYMMETRIC KEY::FS@K3y TO [AD\QDA-App-Developers]
