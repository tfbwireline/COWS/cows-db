USE COWS
GO

CREATE TABLE [dbo].[LK_USR_PRF_MENU](
	[USR_PRF_MENU_ID] [int] IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
	[USR_PRF_ID] [smallint] NOT NULL,
	[MENU_ID] [smallint] NOT NULL,
	[REC_STUS_ID] [tinyint] NOT NULL,
	[MODFD_DT] [smalldatetime] NULL,
	[CREAT_DT] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_LK_USR_PRF_MENU] PRIMARY KEY CLUSTERED 
(
	[USR_PRF_MENU_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UX01_LK_USR_PRF_MENU] UNIQUE NONCLUSTERED 
(
	[USR_PRF_ID] ASC, [MENU_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [IX01_LK_USR_PRF_MENU] ON [dbo].[LK_USR_PRF_MENU]
(
	[USR_PRF_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

ALTER TABLE [dbo].[LK_USR_PRF_MENU] ADD  CONSTRAINT [DF_LK_USR_PRF_MENU_1]  DEFAULT ((1)) FOR [REC_STUS_ID]
GO

ALTER TABLE [dbo].[LK_USR_PRF_MENU] ADD  CONSTRAINT [DF_LK_USR_PRF_MENU_getdate]  DEFAULT (getdate()) FOR [CREAT_DT]
GO

ALTER TABLE [dbo].[LK_USR_PRF_MENU]  ADD  CONSTRAINT [FK01_LK_USR_PRF_MENU] FOREIGN KEY([USR_PRF_ID])
REFERENCES [dbo].[LK_USR_PRF] ([USR_PRF_ID])
GO

ALTER TABLE [dbo].[LK_USR_PRF_MENU]  ADD  CONSTRAINT [FK02_LK_USR_PRF_MENU] FOREIGN KEY([MENU_ID])
REFERENCES [dbo].[LK_MENU] ([MENU_ID])
GO

ALTER TABLE [dbo].[LK_USR_PRF_MENU]  ADD  CONSTRAINT [FK03_LK_USR_PRF_MENU] FOREIGN KEY([REC_STUS_ID])
REFERENCES [dbo].[LK_REC_STUS] ([REC_STUS_ID])
GO