USE [COWS]
GO

-- Table LK_ACTN_TYPE to store the lookup data for action type in history tables in COWS DB
CREATE TABLE [dbo].[LK_ACTN_TYPE]
( 
	[ACTN_TYPE_ID]      smallint  		NOT NULL ,
	[ACTN_TYPE]         varchar(50)  	NOT NULL ,
	[REC_STUS_ID] 			tinyint 		NOT NULL CONSTRAINT [DF_RECSTUSID_LK_ACTN_TYPE_1] DEFAULT ((1)),
	[CREAT_DT]          smalldatetime  	NOT NULL CONSTRAINT [DF_getdate_LK_ACTN_TYPE_1] DEFAULT  getdate(),
	CONSTRAINT [PK_LK_ACTN_TYPE] PRIMARY KEY  CLUSTERED ([ACTN_TYPE_ID] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
	CONSTRAINT [UX01_LK_ACTN_TYPE] UNIQUE NONCLUSTERED ([ACTN_TYPE] ASC)
	WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

go

ALTER TABLE [dbo].[LK_ACTN_TYPE]  WITH NOCHECK ADD  CONSTRAINT [FK01_LK_ACTN_TYPE] FOREIGN KEY([REC_STUS_ID])
REFERENCES [dbo].[LK_REC_STUS] ([REC_STUS_ID])
GO

ALTER TABLE [dbo].[LK_ACTN_TYPE] CHECK CONSTRAINT [FK01_LK_ACTN_TYPE]
GO


ALTER TABLE dbo.LK_ACTN
ADD [ACTN_TYPE_ID] SMALLINT NULL

ALTER TABLE [dbo].[LK_ACTN]  WITH NOCHECK ADD  CONSTRAINT [FK04_LK_ACTN] FOREIGN KEY([ACTN_TYPE_ID])
REFERENCES [dbo].[LK_ACTN_TYPE] ([ACTN_TYPE_ID])
GO

ALTER TABLE [dbo].[LK_ACTN] CHECK CONSTRAINT [FK04_LK_ACTN]
GO