-- Project ID: PJ017930
-- Requested by: Ramesh Ragi
-- Date of Request: 09/09/2015
-- DA: Marwin Macalanda
-- DDL Version: PJ017930_Change_14_09092015
-- Change Summary:
-- 1. Add IS_BLBL_CD column to the dbo.LK_REDSGN_TYPE table


alter table DBO.LK_REDSGN_TYPE add IS_BLBL_CD bit default 0 not null;


