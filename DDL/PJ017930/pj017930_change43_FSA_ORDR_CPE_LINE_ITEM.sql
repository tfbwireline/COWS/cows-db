USE COWS
GO
ALTER TABLE dbo.FSA_ORDR_CPE_LINE_ITEM
	ADD ORDR_CMPNT_ID INT,
		SUPPLIER Varchar(50)

GO

ALTER TABLE dbo.FSA_ORDR_CPE_LINE_ITEM
	ADD ITM_STUS_NEW SmallInt NULL
GO


UPDATE dbo.FSA_ORDR_CPE_LINE_ITEM SET ITM_STUS_NEW = ITM_STUS
GO

ALTER TABLE dbo.FSA_ORDR_CPE_LINE_ITEM
	DROP COLUMN ITM_STUS 
GO

Exec sp_RENAME 'dbo.FSA_ORDR_CPE_LINE_ITEM.ITM_STUS_NEW', 'ITM_STUS' , 'COLUMN'
GO		


			
