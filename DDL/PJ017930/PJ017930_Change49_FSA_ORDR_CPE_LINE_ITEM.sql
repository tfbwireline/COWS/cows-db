USE [COWS]
GO

/****** Object:  Table [dbo].[FSA_ORDR_CPE_LINE_ITEM]    Script Date: 11/08/2016 08:59:42 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

ALTER TABLE dbo.FSA_ORDR_CPE_LINE_ITEM
  ADD
	[PLSFT_RQSTN_NBR] [varchar](10) NULL,
	[RQSTN_DT] [smalldatetime] NULL,
	[PRCH_ORDR_NBR] [varchar](15) NULL,
	[EQPT_ITM_RCVD_DT] [smalldatetime] NULL,
	[MATL_CD] [varchar](18) NULL,
	[UNIT_MSR] [varchar](3) NULL,
	[MANF_PART_CD] [varchar](20) NULL,
	[VNDR_CD] [varchar](10) NULL,
	[ORDR_QTY] [int] NULL,
	[UNIT_PRICE] [varchar](15) NULL,
	[MANF_DISCNT_CD] [varchar](500) NULL,
	[FMS_CKT_NBR] [int] NULL,
	[EQPT_RCVD_BY_ADID] [varchar](10) NULL,
	[CMPL_DT] [smalldatetime] NULL,
	[PO_LN_NBR] [int] NULL,
	[RCVD_QTY] [int] NULL,
	[PS_RCVD_STUS] [tinyint] NULL,
	[PID] [varchar](15) NULL,
	[DROP_SHP] [varchar](1) NULL,
	[DEVICE_ID] [varchar](25) NULL,
	[SUPPLIER] [varchar](50) NULL,
	[ORDR_CMPNT_ID] [int] NULL,
	[ITM_STUS] [smallint] NULL,
	[CMPNT_FMLY] [varchar](10) NULL,
	[RLTD_CMPNT_ID] [int] NULL,
	[STUS_CD] [varchar](2) NULL,
	[MFR_PS_ID] [varchar](30) NULL
 



