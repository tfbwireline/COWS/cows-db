USE COWS
GO

ALTER TABLE dbo.REDSGN
	ADD NE_LVL_EFFORT_AMT decimal(10,2),
		NTE_OT_LVL_EFFORT_AMT decimal(10,2),
		PM_OT_LVL_EFFORT_AMT decimal(10,2),
		NE_OT_LVL_EFFORT_AMT decimal(10,2),
		SPS_LVL_EFFORT_AMT decimal(10,2)
		
GO
	
Update dbo.REDSGN
	SET NE_LVL_EFFORT_AMT = SE_LVL_EFFORT_AMT

GO

ALTER TABLE dbo.REDSGN
	DROP COLUMN SE_LVL_EFFORT_AMT
GO



