USE COWS
GO

ALTER TABLE dbo.AD_EVENT
ALTER COLUMN [DES_CMNT_TXT] VARCHAR(MAX) NOT NULL
GO

ALTER TABLE dbo.MPLS_EVENT
ALTER COLUMN [DES_CMNT_TXT] VARCHAR(MAX) NOT NULL
GO

ALTER TABLE dbo.SPLK_EVENT
ALTER COLUMN [DSGN_CMNT_TXT] VARCHAR(MAX) NOT NULL
GO

ALTER TABLE dbo.NGVN_EVENT
ALTER COLUMN [DSGN_CMNT_TXT] VARCHAR(MAX) NOT NULL
GO
