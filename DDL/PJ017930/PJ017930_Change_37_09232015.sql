-- Project ID: PJ017930
-- Requested by: Ramesh Ragi
-- Date of Request: 09/23/2015
-- DA: Marwin Macalanda
-- DDL Version: PJ017930_Change_16_09232015
-- Change Summary:
-- 1. Add SDE_ASN_NME and VOI_ENGR_ASN_NME columns at the end of DBO.REDSGN table



alter table DBO.REDSGN add SDE_ASN_NME varchar(100);

--alter table DBO.REDSGN add  VOI_ENGR_ASN_NME varchar(100);


