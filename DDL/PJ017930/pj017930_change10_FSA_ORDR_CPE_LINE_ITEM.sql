USE [COWS]

GO

ALTER TABLE dbo.FSA_ORDR_CPE_LINE_ITEM
  ADD PLSFT_RQSTN_NBR Varchar(10),
       RQSTN_DT smalldatetime,
       PRCH_ORDR_NBR varchar(15),
       EQPT_ITM_RCVD_DT smalldatetime,
       MATL_CD varchar(18),
       UNIT_MSR varchar(3),
       MANF_PART_CD varchar(20),
       VNDR_CD varchar(10),
       ORDR_QTY int,
       UNIT_PRICE varchar(15),
       MANF_DISCNT_CD varchar(500),
       FMS_CKT_NBR int,
       EQPT_RCVD_BY_ADID varchar(10),
       ITM_STUS tinyint,
       CMPL_DT smalldatetime,
       PO_LN_NBR int,
       RCVD_QTY int,
       PS_RCVD_STUS tinyint,
       PID varchar(15),
       DROP_SHP varchar(1),
       DEVICE_ID varchar(25)