USE [COWS]
GO


ALTER TABLE dbo.ORDR 
	ADD
		[SMR_NMR] [varchar](1) NULL,
		[RAS_DT] [datetime] NULL,
		[DLVY_CLLI] [varchar](20) NULL,
		[PROD_ID] [varchar](5) NULL,
		[CPE_CLLI] [varchar](20) NULL