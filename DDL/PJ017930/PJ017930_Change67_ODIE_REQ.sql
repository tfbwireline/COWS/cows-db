USE COWS
GO

ALTER TABLE dbo.ODIE_REQ
DROP CONSTRAINT [DF_getdate_604674969]
GO

ALTER TABLE dbo.ODIE_REQ
ALTER COLUMN CREAT_DT DATETIME NOT NULL
GO

ALTER TABLE dbo.ODIE_REQ
ADD CONSTRAINT [DF_getdate_604674969] DEFAULT (getdate()) FOR [CREAT_DT]
GO