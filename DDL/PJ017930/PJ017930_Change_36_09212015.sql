-- Project ID: PJ017930
-- Requested by: Ramesh Ragi
-- Date of Request: 09/21/2015
-- DA: Marwin Macalanda
-- DDL Version: PJ017930_Change_15_09212015
-- Change Summary:
-- 1. Change the FAST_TRK_CD and DSPCH_RDY_CD columns of REDSGN_DEVICES_INFO to null


alter table DBO.REDSGN_DEVICES_INFO alter column FAST_TRK_CD bit null;

alter table DBO.REDSGN_DEVICES_INFO alter column DSPCH_RDY_CD bit null;

