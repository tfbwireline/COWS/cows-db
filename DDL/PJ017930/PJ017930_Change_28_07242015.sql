--PJ017930 - Change 06 - 07242015

USE [COWS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

-- 1. Add new columns to dbo.REDSGN
ALTER TABLE [dbo].[REDSGN]
ADD [EXPRTN_DT] smalldatetime NULL
go

--
SET ANSI_NULLS OFF
GO

SET QUOTED_IDENTIFIER OFF
GO
