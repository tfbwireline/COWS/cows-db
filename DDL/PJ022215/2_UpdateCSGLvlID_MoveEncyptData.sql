USE COWS
GO

SET DEADLOCK_PRIORITY 10;

OPEN SYMMETRIC KEY FS@K3y 
DECRYPTION BY CERTIFICATE S3cFS@CustInf0;


IF OBJECT_ID(N'tempdb..#H1Tbl',N'U') IS NOT NULL 
				DROP TABLE #H1Tbl

CREATE TABLE #H1Tbl (ID INT IDENTITY(1,1) NOT NULL, CPT_ID INT, EVENT_ID INT, REDSGN_ID INT, H1 CHAR(9), CSG_LVL_CD CHAR(5), Flag BIT)
DECLARE @H1 CHAR(9), @Ctr INT=1, @Cnt INT=0, @CsgLvl CHAR(5), @CsgLvlCD CHAR(5)

INSERT INTO #H1Tbl (CPT_ID, EVENT_ID, REDSGN_ID, H1, Flag)
SELECT DISTINCT CPT_ID, 0, 0, H1, 0
FROM dbo.CPT WITH (NOLOCK)
WHERE H1 IS NOT NULL
UNION
SELECT DISTINCT 0, EVENT_ID, 0, H1, 0
FROM dbo.AD_EVENT WITH (NOLOCK)
WHERE H1 IS NOT NULL
UNION
SELECT DISTINCT 0, EVENT_ID, 0,H1, 0
FROM dbo.MDS_EVENT WITH (NOLOCK)
WHERE H1 IS NOT NULL
UNION
SELECT DISTINCT 0, EVENT_ID, 0, H1_ID, 0
FROM dbo.MDS_EVENT_NEW WITH (NOLOCK)
WHERE H1_ID IS NOT NULL
UNION
SELECT DISTINCT 0, EVENT_ID, 0, H1, 0
FROM dbo.MPLS_EVENT WITH (NOLOCK)
WHERE H1 IS NOT NULL
UNION
SELECT DISTINCT 0, EVENT_ID, 0, H1, 0
FROM dbo.SPLK_EVENT WITH (NOLOCK)
WHERE H1 IS NOT NULL
UNION
SELECT DISTINCT 0, EVENT_ID, 0, H1, 0
FROM dbo.NGVN_EVENT WITH (NOLOCK)
WHERE H1 IS NOT NULL
UNION
SELECT DISTINCT 0, EVENT_ID, 0, H1, 0
FROM dbo.UCaaS_EVENT WITH (NOLOCK)
WHERE H1 IS NOT NULL
UNION
SELECT DISTINCT 0, EVENT_ID, 0, H1, 0
FROM dbo.SIPT_EVENT WITH (NOLOCK)
WHERE H1 IS NOT NULL
UNION
SELECT DISTINCT 0, 0, REDSGN_ID, H1_CD,0
FROM dbo.REDSGN WITH (NOLOCK)
WHERE H1_CD IS NOT NULL


SELECT @Cnt = COUNT(DISTINCT H1) FROM #H1Tbl
--PRINT @Cnt

WHILE (@Ctr<=@Cnt)
BEGIN
	SELECT TOP 1 @H1= H1
	FROM #H1Tbl
	WHERE Flag=0
	  AND ID=@Ctr

	SET @CsgLvlCD = ''
	SET @CsgLvl = ''

	DECLARE @x NVARCHAR(MAX) = 'SELECT @CsgLvlCD = CSG_LVL ' +
		'FROM OPENQUERY ' +
		'(M5, ' +
		'''select DISTINCT CSG_LVL ' +
		'from mach5.v_v5u_cust_acct S ' +
		'WHERE ' +
				'((S.CIS_HIER_LVL_CD=''''H1'''' AND S.TYPE_CD=DECODE(S.CIS_HIER_LVL_CD,''''H1'''',''''BILL'''',''''PHYS'''',''''PHYY'''')))' +
				--'((S.CIS_HIER_LVL_CD=''''H1''''))' +
				'AND S.CIS_CUST_ID = ''''' + CONVERT(VARCHAR, @H1) + ''''' AND S.CSG_LVL IS NOT NULL AND ROWNUM=1 '') '
		
		EXECUTE sp_executesql @x, N'@CsgLvlCD CHAR(5) OUTPUT', @CsgLvlCD=@CsgLvl OUTPUT
		IF ((@CsgLvl IS NOT NULL) AND (@CsgLvl <> ''))
			UPDATE #H1Tbl SET CSG_LVL_CD=@CsgLvl WHERE H1=@H1

		UPDATE #H1Tbl SET Flag=1 WHERE H1=@H1 AND Flag=0
		SET @Ctr=@Ctr+1
END

--SELECT * FROM #H1Tbl WHERE CSG_LVL_CD IS NOT NULL

--Update dbo.[Event] with CSG_LVL_CD
UPDATE eve
SET CSG_LVL_ID = CONVERT(TINYINT, ht.[CSG_LVL_CD])
FROM dbo.[EVENT] eve INNER JOIN 
#H1Tbl ht ON ht.[EVENT_ID]=eve.[EVENT_ID]
WHERE ht.[EVENT_ID] <> 0
AND ht.[CSG_LVL_CD] IS NOT NULL AND ht.[CSG_LVL_CD] <> ''

--Update dbo.[CPT] with CSG_LVL_CD
UPDATE cp
SET CSG_LVL_ID = CONVERT(TINYINT, ht.[CSG_LVL_CD])
FROM dbo.[CPT] cp INNER JOIN 
#H1Tbl ht ON ht.[CPT_ID]=cp.[CPT_ID]
WHERE ht.[CPT_ID] <> 0
AND ht.[CSG_LVL_CD] IS NOT NULL AND ht.[CSG_LVL_CD] <> ''

--Update dbo.[REDSGN] with CSG_LVL_CD
UPDATE rd
SET CSG_LVL_ID = CONVERT(TINYINT, ht.[CSG_LVL_CD])
FROM dbo.[REDSGN] rd INNER JOIN 
#H1Tbl ht ON ht.[REDSGN_ID]=rd.[REDSGN_ID]
WHERE ht.[REDSGN_ID] <> 0
AND ht.[CSG_LVL_CD] IS NOT NULL AND ht.[CSG_LVL_CD] <> ''

UPDATE orq
SET CSG_LVL_ID = CONVERT(TINYINT, ht.[CSG_LVL_CD])
FROM dbo.[ODIE_REQ] orq INNER JOIN 
#H1Tbl ht ON ht.[H1]=orq.[H1_CUST_ID]
WHERE orq.H1_CUST_ID IS NOT NULL AND ISNULL(orq.H1_CUST_ID,'')<>''
AND ht.[CSG_LVL_CD] IS NOT NULL AND ht.[CSG_LVL_CD] <> ''

UPDATE rcb
SET CSG_LVL_ID = CONVERT(TINYINT, ht.[CSG_LVL_CD])
FROM dbo.[REDSGN_CUST_BYPASS] rcb INNER JOIN 
#H1Tbl ht ON ht.[H1]=rcb.[H1_CD]
WHERE rcb.H1_CD IS NOT NULL AND ISNULL(rcb.H1_CD,'')<>''
AND ht.[CSG_LVL_CD] IS NOT NULL AND ht.[CSG_LVL_CD] <> ''

--Copy the data into new [CUST_SCRD_DATA]
INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [EVENT_TITLE_TXT], [EVENT_DES], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CUST_CNTCT_PGR_NBR],
[CUST_CNTCT_PGR_PIN_NBR], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME])
SELECT ad.[EVENT_ID], 1, [EVENT_TITLE_TXT], [EVENT_DES], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CUST_CNTCT_PGR_NBR],
[CUST_CNTCT_PGR_PIN_NBR], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME]
FROM dbo.AD_EVENT ad WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=ad.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [ZIP_PSTL_CD], [STT_PRVN_NME], [FLR_ID], [CTY_NME], [CTRY_RGN_NME],
[STREET_ADR_1], [CUST_NME])
SELECT [CPT_ID], 2, [CUST_ZIP_CD], [CUST_STT_PRVN_NME], [CUST_FLR_BLDG_NME],
[CUST_CTY_NME], [CUST_CTRY_RGN_NME], [CUST_ADR], [COMPNY_NME]
FROM dbo.[CPT] WITH (NOLOCK)
WHERE CSG_LVL_ID > 0


INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [STREET_ADR_1], [CTRY_RGN_NME], [CTY_NME], [FLR_ID], [CUST_CNTCT_NME],
[SRVC_ASSRN_POC_NME], [STT_PRVN_NME], [ZIP_PSTL_CD])
SELECT fme.[FSA_MDS_EVENT_ID], 3, [ADR], [CTRY_RGN_NME], [CTY_NME], [FLR_BLDG_NME], [INSTL_SITE_POC_NME],
[SRVC_ASSRN_POC_NME], [STT_PRVN_NME], [ZIP_CD]
FROM dbo.[FSA_MDS_EVENT] fme WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=fme.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [SITE_ADR], [CTRY_RGN_NME], [CTY_NME], [FLR_ID], [CUST_CNTCT_NME],
[SRVC_ASSRN_POC_NME], [STT_PRVN_NME], [ZIP_PSTL_CD])
SELECT fme.FSA_MDS_EVENT_ID, 4, [SITE_ADR], [CTRY_RGN_NME], [CTY_NME], [FLR_BLDG_NME], [INSTL_SITE_POC_NME],
[SRVC_ASSRN_POC_NME], [STT_PRVN_NME], [ZIP_CD]
FROM dbo.[FSA_MDS_EVENT_NEW] fme WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=fme.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME])
SELECT foc.[FSA_ORDR_CUST_ID], 5, [CUST_NME]
FROM dbo.[FSA_ORDR_CUST] foc WITH (NOLOCK) INNER JOIN
dbo.[ORDR] ord WITH (NOLOCK) ON ord.ORDR_ID=foc.ORDR_ID
WHERE (ord.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME])
SELECT hf.[H5_FOLDR_ID], 6, [CUST_NME]
FROM dbo.[H5_FOLDR] hf WITH (NOLOCK)
WHERE (hf.CSG_LVL_ID > 0)


INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [EVENT_TITLE_TXT], [CTRY_RGN_NME], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CTY_NME],
[FLR_ID], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME], [STREET_ADR_1], [SRVC_ASSRN_POC_CELL_PHN_NBR], [SRVC_ASSRN_POC_NME], [SRVC_ASSRN_POC_PHN_NBR],
[STT_PRVN_NME], [ZIP_PSTL_CD])
SELECT me.[EVENT_ID], 7, [EVENT_TITLE_TXT], [CTRY_RGN_NME], [INSTL_SITE_POC_CELL_PHN_NBR], [INSTL_SITE_POC_NME], [CTY_NME],
[FLR_BLDG_NME], [INSTL_SITE_POC_PHN_NBR], dbo.encryptstring(CUST_ACCT_TEAM_PDL_NME), [CUST_NME],
[SITE_ADR], [SRVC_ASSRN_POC_CELL_PHN_NBR], [SRVC_ASSRN_POC_NME], [SRVC_ASSRN_POC_PHN_NBR],
[STT_PRVN_NME], [ZIP_CD]
FROM dbo.MDS_EVENT me WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=me.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_EMAIL_ADR], [CUST_NME], [EVENT_TITLE_TXT])
SELECT me.[EVENT_ID], 8, [CUST_EMAIL_ADR], [CUST_NME], [EVENT_TITLE_TXT]
FROM dbo.MDS_EVENT_NEW me WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=me.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [EVENT_TITLE_TXT], [EVENT_DES], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CUST_CNTCT_PGR_NBR],
[CUST_CNTCT_PGR_PIN_NBR], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME])
SELECT mp.[EVENT_ID], 9, [EVENT_TITLE_TXT], [EVENT_DES], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CUST_CNTCT_PGR_NBR],
[CUST_CNTCT_PGR_PIN_NBR], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME]
FROM dbo.MPLS_EVENT mp WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=mp.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME])
SELECT nod.[ORDR_ID], 10, [CUST_NME]
FROM dbo.NCCO_ORDR nod WITH (NOLOCK) INNER JOIN
dbo.[ORDR] ord WITH (NOLOCK) ON ord.ORDR_ID=nod.ORDR_ID
WHERE (ord.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [EVENT_TITLE_TXT], [EVENT_DES], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CUST_CNTCT_PGR_NBR],
[CUST_CNTCT_PGR_PIN_NBR], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME])
SELECT ne.[EVENT_ID], 11, [EVENT_TITLE_TXT], [EVENT_DES], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CUST_CNTCT_PGR_NBR],
[CUST_CNTCT_PGR_PIN_NBR], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME]
FROM dbo.NGVN_EVENT ne WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=ne.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME])
SELECT [REQ_ID], 12, [CUST_NME]
FROM dbo.ODIE_REQ WITH (NOLOCK)
WHERE CSG_LVL_ID > 0

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME])
SELECT orp.[RSPN_ID], 13, orp.[CUST_NME]
FROM dbo.[ODIE_RSPN] orp WITH (NOLOCK) INNER JOIN
dbo.[ODIE_REQ] orq WITH (NOLOCK) ON orq.REQ_ID=orp.REQ_ID
WHERE orq.CSG_LVL_ID > 0


INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [BLDG_NME], [CTY_NME], [FLR_ID], [STT_PRVN_NME], [RM_NBR],
[STREET_ADR_1], [STREET_ADR_2], [STREET_ADR_3], [STT_CD], [ZIP_PSTL_CD])
SELECT oa.[ORDR_ADR_ID], 14, [BLDG_NME], [CTY_NME], [FLR_ID], [PRVN_NME], [RM_NBR],
[STREET_ADR_1], [STREET_ADR_2], [STREET_ADR_3], [STT_CD],
[ZIP_PSTL_CD]
FROM dbo.ORDR_ADR oa WITH (NOLOCK) INNER JOIN
dbo.[ORDR] ord WITH (NOLOCK) ON ord.ORDR_ID=oa.ORDR_ID
WHERE (ord.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_CNTCT_NME], [CUST_EMAIL_ADR], [FRST_NME], [LST_NME])
SELECT oc.[ORDR_CNTCT_ID], 15,  [CNTCT_NME], [EMAIL_ADR], [FRST_NME], [LST_NME]
FROM dbo.[ORDR_CNTCT] oc WITH (NOLOCK) INNER JOIN
dbo.[ORDR] ord WITH (NOLOCK) ON ord.ORDR_ID=oc.ORDR_ID
WHERE (ord.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_EMAIL_ADR], [CUST_NME])
SELECT rd.[REDSGN_ID], 16,  [CUST_EMAIL_ADR], [CUST_NME]
FROM dbo.[REDSGN] rd WITH (NOLOCK)
WHERE (rd.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME])
SELECT rd.[ID], 17, [CUST_NME]
FROM dbo.[REDSGN_CUST_BYPASS] rd WITH (NOLOCK)
WHERE (rd.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [EVENT_TITLE_TXT], [CTRY_RGN_NME], [CTY_NME], [CUST_NME], [FLR_ID],
[STREET_ADR_1], [CUST_EMAIL_ADR], [CUST_CNTCT_NME], [CUST_CNTCT_PHN_NBR], [STT_PRVN_NME], [ZIP_PSTL_CD])
SELECT se.[EVENT_ID], 18, [EVENT_TITLE_TXT], [CTRY_RGN_NME], [CTY_NME], [CUST_NME], [FLR_BLDG_NME],
[SITE_ADR], [SITE_CNTCT_EMAIL_ADR], [SITE_CNTCT_NME], [SITE_CNTCT_PHN_NBR], [STT_PRVN_NME], [ZIP_CD]
FROM dbo.[SIPT_EVENT] se WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=se.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [EVENT_TITLE_TXT], [EVENT_DES], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CUST_CNTCT_PGR_NBR],
[CUST_CNTCT_PGR_PIN_NBR], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME])
SELECT se.[EVENT_ID], 19, [EVENT_TITLE_TXT], [EVENT_DES], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CUST_CNTCT_PGR_NBR],
[CUST_CNTCT_PGR_PIN_NBR], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME]
FROM dbo.SPLK_EVENT se WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=se.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [EVENT_TITLE_TXT], [CTRY_RGN_NME], [CUST_CNTCT_CELL_PHN_NBR], [CUST_CNTCT_NME], [CTY_NME],
[FLR_ID], [CUST_CNTCT_PHN_NBR], [CUST_EMAIL_ADR], [CUST_NME], [STREET_ADR_1], [SRVC_ASSRN_POC_CELL_PHN_NBR], [SRVC_ASSRN_POC_NME], [SRVC_ASSRN_POC_PHN_NBR],
[STT_PRVN_NME], [ZIP_PSTL_CD])
SELECT uc.[EVENT_ID], 20, [EVENT_TITLE_TXT], [CTRY_RGN_NME], [INSTL_SITE_POC_CELL_PHN_NBR], [INSTL_SITE_POC_NME], [CTY_NME],
[FLR_BLDG_NME], [INSTL_SITE_POC_PHN_NBR], dbo.encryptstring(CUST_ACCT_TEAM_PDL_NME), [CUST_NME],
[SITE_ADR], [SRVC_ASSRN_POC_CELL_PHN_NBR], [SRVC_ASSRN_POC_NME], [SRVC_ASSRN_POC_PHN_NBR],
[STT_PRVN_NME], [ZIP_CD]
FROM dbo.UCaaS_EVENT uc WITH (NOLOCK) INNER JOIN
dbo.[EVENT] ev WITH (NOLOCK) ON ev.EVENT_ID=uc.EVENT_ID
WHERE (ev.CSG_LVL_ID > 0)

-- INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CTY_NME], [STT_PRVN_NME], [STREET_ADR_1], [STREET_ADR_2], [STT_CD], [ZIP_PSTL_CD])
-- SELECT [FEDLINE_EVENT_ADR_ID], 21, [CITY_NME], [PRVN_NME], [STREET_1_ADR], [STREET_2_ADR], [STT_CD], [ZIP_CD]
-- FROM dbo.FEDLINE_EVENT_ADR WITH (NOLOCK)

-- INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_CNTCT_NME], [CUST_EMAIL_ADR])
-- SELECT [FEDLINE_EVENT_CNTCT_ID], 22, [CNTCT_NME], [EMAIL_ADR]
-- FROM dbo.FEDLINE_EVENT_CNTCT WITH (NOLOCK)

-- INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME])
-- SELECT [FEDLINE_EVENT_ID], 23, [CUST_NME]
-- FROM dbo.FEDLINE_EVENT_TADPOLE_DATA WITH (NOLOCK)

-- INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [NTE_TXT])
-- SELECT [MSG_XML_ID], 24, [MSG_XML_TXT]
-- FROM dbo.FEDLINE_EVENT_TADPOLE_XML WITH (NOLOCK)

-- INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [EVENT_TITLE_TXT])
-- SELECT [EVENT_ID], 25, [EVENT_TITLE_TXT]
-- FROM dbo.FEDLINE_EVENT_USER_DATA WITH (NOLOCK)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [CUST_NME], [STREET_ADR_1], [STT_PRVN_NME], [CTY_NME], [CUST_CNTCT_NME], [CUST_EMAIL_ADR], [CUST_CNTCT_PHN_NBR])
SELECT [SDE_OPPTNTY_ID], 26, [CMPNY_NME], [ADDR], [STATE], [CITY], [CNTCT_NME], [CNTCT_EMAIL], [CNTCT_PHN]
FROM dbo.SDE_OPPTNTY WITH (NOLOCK)

INSERT INTO dbo.[CUST_SCRD_DATA] ([SCRD_OBJ_ID], [SCRD_OBJ_TYPE_ID], [NTE_TXT])
SELECT [SDE_OPPTNTY_NTE_ID], 27, [NTE_TXT]
FROM dbo.SDE_OPPTNTY_NTE WITH (NOLOCK)



--UPDATE dbo.FSA_ORDR_PORT_ACCS_DATA WITH data FROM dbo.FSA_ORDR
--Insert Access Components for Intl offnet/onnet
INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM ([ORDR_ID], CREAT_DT, [LINE_ITEM_CD],
[TTRPT_ACCS_TYPE_CD],
[TTRPT_ACCS_ARNGT_CD],
[TTRPT_ENCAP_CD],
[TTRPT_FBR_HAND_OFF_CD],
[CXR_ACCS_CD],
[CKT_ID],
[TTRPT_QOT_XPIRN_DT],
[TTRPT_ACCS_TERM_DES],
[TTRPT_ACCS_BDWD_TYPE],
[PL_NBR],
[TTRPT_ACCS_TYPE_DES])
SELECT DISTINCT f.[ORDR_ID], o.CREAT_DT, 'ACS',
f.[TTRPT_ACCS_TYPE_CD],
f.[TTRPT_ACCS_ARNGT_CD],
f.[TTRPT_ENCAP_CD],
f.[TTRPT_FBR_HAND_OFF_CD],
f.[CXR_ACCS_CD],
f.[CKT_ID],
f.[TTRPT_QOT_XPIRN_DT],
f.[TTRPT_ACCS_TERM_DES],
f.[TTRPT_ACCS_BDWD_TYPE],
f.[PL_NBR],
f.[TTRPT_ACCS_TYPE_DES]
FROM dbo.FSA_ORDR f WITH (NOLOCK) INNER JOIN
dbo.ORDR o WITH (NOLOCK) ON f.ORDR_ID=o.ORDR_ID
WHERE o.DMSTC_CD=1
AND f.PROD_TYPE_CD IN ('DN','DO','MP','MO','SN','SO','IN','IO')
GO

--Insert Port Components for Intl offnet/onnet
INSERT INTO dbo.FSA_ORDR_CPE_LINE_ITEM ([ORDR_ID], CREAT_DT, [LINE_ITEM_CD], [INSTL_DSGN_DOC_NBR],
[TTRPT_SPD_OF_SRVC_BDWD_DES],
[PORT_RT_TYPE_CD],
[TPORT_IP_VER_TYPE_CD],
[TPORT_IPV4_ADR_PRVDR_CD],
[TPORT_IPV4_ADR_QTY],
[TPORT_IPV6_ADR_PRVDR_CD],
[TPORT_IPV6_ADR_QTY],
[TPORT_VLAN_QTY],
[TPORT_ETHRNT_NRFC_INDCR],
[TPORT_CNCTR_TYPE_ID],
[TPORT_CUST_ROUTR_TAG_TXT],
[TPORT_CUST_ROUTR_AUTO_NEGOT_CD],
[SPA_ACCS_INDCR_DES],
[TPORT_DIA_NOC_TO_NOC_TXT],
[TPORT_PROJ_DES],
[TPORT_VNDR_QUOTE_ID],
[TPORT_ETH_ACCS_TYPE_CD])
SELECT DISTINCT f.[ORDR_ID], o.CREAT_DT, 'PRT', f.[INSTL_DSGN_DOC_NBR],
f.[TTRPT_SPD_OF_SRVC_BDWD_DES],
f.[PORT_RT_TYPE_CD],
f.[TPORT_IP_VER_TYPE_CD],
f.[TPORT_IPV4_ADR_PRVDR_CD],
f.[TPORT_IPV4_ADR_QTY],
f.[TPORT_IPV6_ADR_PRVDR_CD],
f.[TPORT_IPV6_ADR_QTY],
f.[TPORT_VLAN_QTY],
f.[TPORT_ETHRNT_NRFC_INDCR],
f.[TPORT_CNCTR_TYPE_ID],
f.[TPORT_CUST_ROUTR_TAG_TXT],
f.[TPORT_CUST_ROUTR_AUTO_NEGOT_CD],
f.[SPA_ACCS_INDCR_DES],
f.[TPORT_DIA_NOC_TO_NOC_TXT],
f.[TPORT_PROJ_DES],
f.[TPORT_VNDR_QUOTE_ID],
f.[TPORT_ETH_ACCS_TYPE_CD]
FROM dbo.FSA_ORDR f WITH (NOLOCK) INNER JOIN
dbo.ORDR o WITH (NOLOCK) ON f.ORDR_ID=o.ORDR_ID
WHERE o.DMSTC_CD=1
AND f.PROD_TYPE_CD IN ('DN','DO','MP','MO','SN','SO','IN','IO')
GO
