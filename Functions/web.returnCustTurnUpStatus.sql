USE [COWS]
GO
_CreateObject 'FS','web','returnCustTurnUpStatus'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <10/18/2011>
-- Description:	<To return Customer Turn Up Task 
--	when Order is completed and Customer Turn Up Field is Blank>
-- =============================================
ALTER FUNCTION  web.returnCustTurnUpStatus
(
	@ORDR_ID INT
)
RETURNS BIT
AS
BEGIN
DECLARE @Ret BIT
SET @Ret = 0
	IF EXISTS
		(
			SELECT		'X'
				FROM	dbo.ACT_TASK WITH (NOLOCK)
				WHERE	ORDR_ID =	@ORDR_ID
					AND	TASK_ID	=	213
					AND STUS_ID =	0
		)
		BEGIN
			SET @Ret = 1
		END
		
RETURN @Ret

END
GO

