USE [COWS]
GO
_CreateObject 'FS','dbo','getTimeSlotForEventID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sbg9814
-- Create date: 10/14/2011
-- Description:	Returns Yes or No for any given Bit field.
-- =============================================
ALTER FUNCTION [dbo].[getTimeSlotForEventID] 
(
	@EVENT_ID	Int
)
RETURNS Varchar(30)
AS 
BEGIN
	DECLARE	@RetStr	Varchar(30)
	SET		@RetStr	=	''
	SELECT	@RetStr	=	ISNULL((SubString(Convert(Varchar(20), ls.TME_SLOT_STRT_TME), 1, 5)	
					+	' - ' 
					+	SubString(Convert(Varchar(20), ls.TME_SLOT_END_TME), 1, 5)), '')
			FROM		dbo.MDS_EVENT_NEW				ev	WITH (NOLOCK)
			INNER JOIN	dbo.LK_EVENT_TYPE_TME_SLOT	ls	WITH (NOLOCK)	ON	ev.TME_SLOT_ID	=	ls.TME_SLOT_ID
																		AND	ev.EVENT_TYPE_ID=	ls.EVENT_TYPE_ID
			WHERE		ev.EVENT_ID	=	@EVENT_ID	
			
	SELECT	@RetStr	=	ISNULL((SubString(Convert(Varchar(20), ls.TME_SLOT_STRT_TME), 1, 5)	
					+	' - ' 
					+	SubString(Convert(Varchar(20), ls.TME_SLOT_END_TME), 1, 5)), '')
			FROM		dbo.MDS_EVENT				ev	WITH (NOLOCK)
			INNER JOIN	dbo.LK_EVENT_TYPE_TME_SLOT	ls	WITH (NOLOCK)	ON	ev.TME_SLOT_ID	=	ls.TME_SLOT_ID
			WHERE		ev.EVENT_ID	=	@EVENT_ID	
			  AND 		ls.EVENT_TYPE_ID = 5
			
			
	RETURN @RetStr
END

