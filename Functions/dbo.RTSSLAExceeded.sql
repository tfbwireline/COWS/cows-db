USE COWS
GO
_CreateObject 'FS','dbo','RTSSLAExceeded'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		kaw4664
-- Create date: 11/21/2011
-- Description:	Calculates days since submission of RTS task, 
--				determines if it is over 3 day business timer
--				Return 0 if SLA is ok, 1 if over
-- =============================================
ALTER FUNCTION dbo.RTSSLAExceeded
(
	@orderID	int
)
RETURNS BIT
AS
BEGIN
	DECLARE @TaskStart	DATETIME
	DECLARE @Now		DATETIME
	DECLARE @Interval	DATETIME
	DECLARE @return		BIT
	
	SET @return = 0
	SET @Now = GETDATE()
	SELECT @TaskStart = ISNULL(CREAT_DT,@Now) FROM dbo.ACT_TASK WITH (NOLOCK) WHERE ORDR_ID = @orderID AND STUS_ID = 0 AND TASK_ID = 500
	
	IF (ISNULL(@TaskStart,0) <> 0)
	BEGIN
		--Business Hours are from 8-4pm.  Set start time to be in this range.
		IF (DATEPART(HOUR,@TaskStart) < 8)
		BEGIN
			SET @TaskStart = CONVERT(DATETIME,CONVERT(DATETIME,@TaskStart)) + Convert(DATETIME,'08:00:00')
		END
		IF (CONVERT(TIME,@TaskStart) > CONVERT(TIME,'16:00:00'))
		BEGIN
			SET @TaskStart = CONVERT(DATETIME,CONVERT(DATETIME,DATEADD(DAY,1,@TaskStart))) + Convert(DATETIME,'08:00:00')
		END
		--select @TaskStart
		
		--Make sure start day is not a holiday/weekend day
		WHILE DATEPART(WEEKDAY,@TaskStart) IN (1,7) OR EXISTS (SELECT TOP 1 'X' FROM dbo.LK_SPRINT_HLDY WITH (NOLOCK) WHERE SPRINT_HLDY_DT = CONVERT(DATE,@TaskStart))
		BEGIN
			SET @TaskStart = DATEADD(DAY,1,@TaskStart)
		END
			
		--Get Baseline day interval.  Below this, calculate the number of weekend holidays to subtract
		SET @Interval = @Now - @TaskStart
		
		
		--Get # of weekday/holidays
		DECLARE @days INT
		SET @days = 0
		WHILE @TaskStart < @Now
		BEGIN
			IF DATEPART(WEEKDAY,@TaskStart) IN (1,7) OR EXISTS (SELECT TOP 1 'X' FROM dbo.LK_SPRINT_HLDY WITH (NOLOCK) WHERE SPRINT_HLDY_DT = CONVERT(DATE,@TaskStart))
			BEGIN
				SET @days = @days + 1
			END
			SET @TaskStart = DATEADD(DAY,1,@TaskStart)
		END
		
		--select @TaskStart, @Interval,@days
		
		IF (3 + @days) < @Interval
		BEGIN
			SET @return = 1
		END
	END
	RETURN @return
END
GO

