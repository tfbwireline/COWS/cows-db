USE [COWS]
GO
_CreateObject 'FS','dbo','getBooleanString'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sbg9814
-- Create date: 10/14/2011
-- Description:	Returns Yes or No for any given Bit field.
-- =============================================
ALTER FUNCTION [dbo].[getBooleanString] 
(
	@Input	Bit
)
RETURNS Varchar(3)
AS 
BEGIN	
	RETURN Case	@Input	When	0	Then	'No'	Else	'Yes'	End
END

