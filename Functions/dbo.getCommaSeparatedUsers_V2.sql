USE [COWS]
GO
/****** Object:  UserDefinedFunction [dbo].[getCommaSeparatedUsers_V2]    Script Date: 01/13/2021 11:27:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[getCommaSeparatedUsers_V2] 
(
	@ORDR_ID	Int
	,@PRF_ID		Int
	,@UserID	Int
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE	@Users	Varchar(Max)
	SET	@Users	=	''
	
	IF(@PRF_ID IN (86,44,98,56))
		BEGIN
			SELECT		@Users	=	COALESCE(@Users + ', ' + A.FULL_NME, A.FULL_NME)
			FROM	(SELECT lu.FULL_NME FROM dbo.LK_USER lu WITH (NOLOCK)	INNER JOIN
						(SELECT DISTINCT TOP 1 uw.ASN_USER_ID, uw.ASMT_DT	
							FROM	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	
							WHERE	uw.ORDR_ID		=	@ORDR_ID
								 AND uw.GRP_ID IN (13,15) ORDER BY uw.ASMT_DT DESC) AS X ON X.ASN_USER_ID=lu.USER_ID)A
		END
	ELSE IF(@PRF_ID IN (2,16,100,112,28,14))
		BEGIN
		SELECT		@Users	=	COALESCE(@Users + ', ' + A.FULL_NME, A.FULL_NME)
			FROM	(SELECT lu.FULL_NME FROM dbo.LK_USER lu WITH (NOLOCK)	INNER JOIN
						(SELECT DISTINCT  uw.ASN_USER_ID, uw.ASMT_DT	
							FROM	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	
							WHERE	uw.ORDR_ID		=	@ORDR_ID
								 AND uw.GRP_ID IN (5,6,7) 
								 AND	uw.ASN_USER_ID	=	Case	@UserID
																When	0	Then	uw.ASN_USER_ID
																Else	@UserID
															END) AS X ON X.ASN_USER_ID=lu.USER_ID)A
		END
	ELSE 
		BEGIN
		SELECT		@Users	=	COALESCE(@Users + ', ' + A.FULL_NME, A.FULL_NME)
			FROM	(SELECT lu.FULL_NME FROM dbo.LK_USER lu WITH (NOLOCK)	INNER JOIN
						(SELECT DISTINCT uw.ASN_USER_ID
							FROM	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	
							WHERE	uw.ORDR_ID		=	@ORDR_ID
								 AND uw.GRP_ID NOT IN (5,6,7,13,15)) AS X ON X.ASN_USER_ID=lu.USER_ID)A
		END												
	SELECT	@Users	=	Case	Len(@Users)
							When	0	Then	''
							Else	SubString(@Users, 3, Len(@Users))
						End												
			
	RETURN @Users
END
