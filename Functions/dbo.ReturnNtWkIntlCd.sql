USE [COWS]
GO
_CreateObject 'FS','dbo','ReturnNtWkIntlCd'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================
-- Author:		<Sarah Sandoval>
-- Create date: <12/01/2020>
-- Description:	<To return Network International on IPSD Event Grid>
-- =========================================================================
ALTER FUNCTION  [dbo].[ReturnNtWkIntlCd]
(
	@EVENT_ID INT
)
RETURNS CHAR(1)
AS
BEGIN
DECLARE @Ret CHAR(1)
SET @Ret = 'N'
	IF EXISTS
		(
			SELECT		'X'
				FROM	dbo.MDS_EVENT_NTWK_ACTY mena WITH (NOLOCK)
				WHERE	mena.EVENT_ID = @EVENT_ID
				AND	mena.NTWK_ACTY_TYPE_ID	=	4
		)
		BEGIN
			SET @Ret = 'Y'
		END
		
RETURN @Ret

END
