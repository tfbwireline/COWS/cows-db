USE [COWS]
GO
_CreateObject 'FS','web','checkWGExistsForAnOrder'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <10/11/2011>
-- Description:	<To Verify if Order existed in 
--	in a specific workgroup in it's order workflow>
-- =============================================
ALTER FUNCTION web.checkWGExistsForAnOrder
(
	@ORDR_ID	INT,
	@WG_ID		INT
)
RETURNS BIT
AS
BEGIN
DECLARE @Exists BIT
SET @Exists = 0
	
	--Added to include FSA Cancel FTN's (Note: Cancel on IPL orders would happen on same order/CTN) 
	IF EXISTS
		(
			SELECT		'X'
				FROM	dbo.ORDR		o	WITH (NOLOCK)
			LEFT JOIN	dbo.FSA_ORDR	f	WITH (NOLOCK) ON f.ORDR_ID = o.ORDR_ID
				WHERE	o.ORDR_ID		=	@ORDR_ID
					AND	f.ORDR_TYPE_CD			=	'CN' 
					AND ISNULL(f.PRNT_FTN,'')	<>	''
		)
		BEGIN
			SELECT		@ORDR_ID = ORDR_ID 
				FROM	dbo.FSA_ORDR WITH (NOLOCK)
				WHERE	FTN = (SELECT PRNT_FTN FROM dbo.FSA_ORDR WITH (NOLOCK) WHERE ORDR_ID = @ORDR_ID)
		END

	IF EXISTS
		(
			SELECT 'X'
			FROM	dbo.ORDR			o	WITH (NOLOCK)
		INNER JOIN	dbo.LK_PPRT			p	WITH (NOLOCK) ON o.PPRT_ID		=	p.PPRT_ID
		INNER JOIN	dbo.SM				s	WITH (NOLOCK) ON s.SM_ID		=	p.SM_ID
		INNER JOIN	dbo.WG_PTRN_SM		pt	WITH (NOLOCK) ON pt.WG_PTRN_ID	=	s.PRE_REQST_WG_PTRN_ID
		INNER JOIN	dbo.WG_PROF_SM		pf	WITH (NOLOCK) ON pf.WG_PROF_ID	=	pt.PRE_REQST_WG_PROF_ID
		INNER JOIN	dbo.MAP_GRP_TASK	mgt	WITH (NOLOCK) ON mgt.TASK_ID	=	pf.PRE_REQST_TASK_ID
			WHERE	o.ORDR_ID	=	@ORDR_ID
				AND mgt.TASK_ID NOT IN (100,101,109)
				AND	mgt.GRP_ID	=	@WG_ID
		)
		BEGIN
			SET @Exists = 1
		END
		
	IF	@WG_ID	=	1	
	AND	EXISTS
		(SELECT		'X'
			FROM	dbo.FSA_ORDR	WITH (NOLOCK)
			WHERE	ORDR_ID	=	@ORDR_ID
				AND ORDR_ACTN_ID	=	3
		)
		BEGIN
			SET @Exists = 1
		END	
RETURN @Exists

END
GO

