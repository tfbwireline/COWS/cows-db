USE [COWS]
GO
_CreateObject 'FS','dbo','GetFormattedFileSize'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[GetFormattedFileSize]
        (
         @FileLen BIGINT
        )
RETURNS VARCHAR(MAX)

AS
  BEGIN
		--Converts the FileSize inputted in bytes into Readable format
        DECLARE @FileSizeTxt VARCHAR(MAX)
		DECLARE @FileFormatLbls TABLE (ID INT IDENTITY(1,1), FormatType CHAR(2))
		INSERT INTO @FileFormatLbls (FormatType) VALUES ('B'), ('KB'), ('MB'), ('GB'), ('TB')

		 DECLARE @FileLenDec DECIMAL(20,2) = CONVERT(DECIMAL,@FileLen);
		 DECLARE @order TINYINT = 1
		 WHILE ((@FileLenDec>=1024) AND (@order < 4))
		 BEGIN
		   SET @order = @order+1
		   SET @FileLenDec = @FileLenDec/1024
		 END
		
		SET @FileSizeTxt = CONVERT(VARCHAR,@FileLenDec) + (SELECT FormatType FROM @FileFormatLbls WHERE ID=@order)

		RETURN @FileSizeTxt
    END