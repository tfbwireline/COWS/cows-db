USE [COWS]
GO
_CreateObject 'FS','dbo','getCommaSepRDSNs'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		sbg9814
-- Create date: 05/09/2012
-- Description:	This UDF returns Comma seperated Redesign Numbers.
-- =========================================================

ALTER FUNCTION [dbo].[getCommaSepRDSNs] 
(
	@iEventID INT,
	@EventType CHAR(10)
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @RDSN VARCHAR(Max)   
	
	IF (@EventType = 'MDS')
	BEGIN
		SELECT	@RDSN = CASE WHEN (CHARINDEX(ISNULL(RDSN_NBR,''),@RDSN)>0) THEN @RDSN ELSE COALESCE(@RDSN + ',', '')   + ISNULL(RDSN_NBR, '') END
			FROM		dbo.MDS_MNGD_ACT_NEW WITH (NOLOCK)    
			WHERE		EVENT_ID	=	@iEventID    
			ORDER BY	MDS_MNGD_ACT_ID		DESC  
			
		SELECT	@RDSN = CASE WHEN (CHARINDEX(ISNULL(RDSN_NBR,''),@RDSN)>0) THEN @RDSN ELSE COALESCE(@RDSN + ',', '')   + ISNULL(RDSN_NBR, '') END
			FROM		dbo.MDS_EVENT_ODIE_DEV WITH (NOLOCK)    
			WHERE		EVENT_ID	=	@iEventID    
			ORDER BY	MDS_EVENT_ODIE_DEV_ID		DESC  
	END
	ELSE IF (@EventType = 'UCaaS')
	BEGIN
		SELECT	@RDSN = CASE WHEN (CHARINDEX(ISNULL(RDSN_NBR,''),@RDSN)>0) THEN @RDSN ELSE COALESCE(@RDSN + ',', '')   + ISNULL(RDSN_NBR, '') END
			FROM		dbo.UCaaS_EVENT_ODIE_DEV WITH (NOLOCK)    
			WHERE		EVENT_ID	=	@iEventID    
			ORDER BY	UCaaS_EVENT_ODIE_DEV_ID		DESC  
	END
	
	RETURN @RDSN
END
GO
