USE [COWS]
GO
_CreateObject 'FS','dbo','getTimeSlotDesc'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jrg7298
-- Create date: 02/01/2015
-- Description:	Returns Time Slot Desc
-- =============================================
ALTER FUNCTION [dbo].[getTimeSlotDesc] 
(
	@TimeSlotID		TinyInt	
)
RETURNS Varchar(50)
AS 
BEGIN	
	DECLARE	@TimeSlotDesc	Varchar(100)
	SET	@TimeSlotDesc	=	''
	
	SELECT	Top	1	@TimeSlotDesc	=	CASE @TimeSlotID WHEN 1 THEN '06:00 AM CT-10:00 AM CT'
                    WHEN 2 THEN '10:00 AM CT-02:00 PM CT'
                    WHEN 3 THEN '02:00 PM CT-06:00 PM CT'
                    WHEN 4 THEN '06:00 PM CT-10:00 PM CT' ELSE '' END

			
	RETURN @TimeSlotDesc		
END

