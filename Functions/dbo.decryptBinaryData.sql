USE [COWS]
GO
_CreateObject 'FS','dbo','decryptBinaryData'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:		sbg9814
-- Create date: 08/17/2011
-- Description:	Decrypt an Input Varbinary data into string format.
-- ======================================================================

ALTER FUNCTION [dbo].[decryptBinaryData]
(
	@InputString Varbinary(Max)
)
RETURNS Varchar(Max)
AS
BEGIN
	RETURN CONVERT(varchar(max), DecryptByKey(@InputString))
END
GO


