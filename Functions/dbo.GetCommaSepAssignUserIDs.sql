USE [COWS]
GO
_CreateObject 'FS','dbo','GetCommaSepAssignUserIDs'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Jagannath Gangi
-- Create date: 2011-4-14
-- Description:	This SP is used to get Comma seperated UserIDs
-- =========================================================

ALTER FUNCTION [dbo].[GetCommaSepAssignUserIDs] 
(
	@EventID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @list VARCHAR(max)   
	SELECT @list = COALESCE(@list + ',', '') + CAST(FULL_NME AS VARCHAR(200)) 
	FROM
		(SELECT DISTINCT lu.FULL_NME, lu.DSPL_NME
		FROM dbo.EVENT_ASN_TO_USER ea WITH (NOLOCK) INNER JOIN
		     dbo.LK_USER lu WITH (NOLOCK) on lu.[USER_ID] = ea.ASN_TO_USER_ID
		WHERE ea.EVENT_ID = @EventID
		  AND ea.ASN_TO_USER_ID != 5001
		  AND ea.REC_STUS_ID = 1) x
	ORDER BY DSPL_NME
		
--All FastTrack should have COWS ES Activators  as Assigned User IM830464
		
	IF (((SELECT ISNULL(MDS_FAST_TRK_CD,0) FROM dbo.MDS_EVENT_NEW WITH (NOLOCK)
				where EVENT_ID = @EventID ) = 1)
		OR ((SELECT ISNULL(MDS_FAST_TRK_TYPE_ID,'') FROM dbo.MDS_EVENT WITH (NOLOCK)
				where EVENT_ID = @EventID ) != ''))
  		BEGIN 
    		SET @List = 'COWS ES Activators'
	    END

	RETURN @List
END