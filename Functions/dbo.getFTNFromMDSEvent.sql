USE [COWS]
GO
_CreateObject 'FS','dbo','getFTNFromMDSEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sbg9814
-- Create date: 10/14/2011
-- Description:	Returns Yes or No for any given Bit field.
-- =============================================
ALTER FUNCTION [dbo].[getFTNFromMDSEvent] 
(
	@EVENT_ID	Int
)
RETURNS Varchar(10)
AS 
BEGIN	
	DECLARE	@FTN	Varchar(20)
	SET	@FTN	=	''
	
	SELECT Top 1	@FTN	=	ISNULL(MNS_FTN_NBR, '')
		FROM		dbo.MDS_EVENT_MNS		oe	WITH (NOLOCK)
		INNER JOIN	dbo.FSA_MDS_EVENT_NEW	fm	WITH (NOLOCK)	ON	oe.FSA_MDS_EVENT_ID	=	fm.FSA_MDS_EVENT_ID
		WHERE		fm.EVENT_ID	=	@EVENT_ID
		
	IF	@FTN	=	''	OR	@FTN	=	'0'
		BEGIN
			SELECT Top 1	@FTN	=	ISNULL(ACCS_FTN_NBR, '')
				FROM		dbo.MDS_EVENT_ACCS	oe	WITH (NOLOCK)
				INNER JOIN	dbo.FSA_MDS_EVENT_NEW		fm	WITH (NOLOCK)	ON	oe.FSA_MDS_EVENT_ID	=	fm.FSA_MDS_EVENT_ID
				WHERE		fm.EVENT_ID	=	@EVENT_ID		
		END	
	
	IF	@FTN	=	''	OR	@FTN	=	'0'
		BEGIN
			SELECT Top 1	@FTN	=	ISNULL(mtd.CPE_FTN_NBR, '')
				FROM		dbo.MDS_EVENT_CPE mtd	WITH (NOLOCK) INNER JOIN
							dbo.FSA_MDS_EVENT_NEW fme WITH (NOLOCK) ON fme.FSA_MDS_EVENT_ID = mtd.FSA_MDS_EVENT_ID
				WHERE		fme.EVENT_ID	=	@EVENT_ID	
					AND		mtd.CPE_FTN_NBR IS NOT NULL
					AND		mtd.CPE_FTN_NBR <> '0'	
		END		
		
	IF	@FTN	=	'0'
		SET	@FTN	=	''	
	RETURN @FTN
END

