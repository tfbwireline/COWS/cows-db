USE [COWS]
GO
_CreateObject 'FS','dbo','GetCommaSepMNSEntitlements'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetCommaSepMNSEntitlements] 
(
	@ID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	declare @list varchar(max)
	select @list =  COALESCE(@list + ', ', '') + CAST(lms.MDS_SRVC_TIER_DES AS VARCHAR(100))  
	from dbo.LK_MDS_SRVC_TIER lms with (nolock)
	where MDS_SRVC_TIER_ID in 
	(select IntegerID from web.ParseCommaSeparatedIntegers((select MNS_ENTLMNT_ID from dbo.EVENT_DEV_SRVC_MGMT with (nolock) where EVENT_DEV_SRVC_MGMT_ID = @ID), ','))
	
	RETURN @List
END