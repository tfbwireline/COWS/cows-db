USE [COWS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetCommaSepEventFTNs]    Script Date: 02/26/2020 3:24:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Kyle Wichert
-- Create date: 2012-02-07
-- Description:	This SP is used to get Comma seperated FTNs
-- =========================================================

ALTER FUNCTION [dbo].[GetCommaSepEventFTNs] 
(
	@EventID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @list VARCHAR(max)
	DECLARE @FTN	TABLE (ftn	VARCHAR(20))
	
		INSERT INTO @FTN
		SELECT DISTINCT CPE_ORDR_NBR 
		FROM dbo.EVENT_CPE_DEV WITH (NOLOCK)
		WHERE EVENT_ID = @EventID AND REC_STUS_ID=1
	
	SELECT @list = COALESCE(@list + ',', '') + CAST(ftn AS VARCHAR(20)) 
	FROM	@FTN
	ORDER BY ftn
		
	RETURN @List
END