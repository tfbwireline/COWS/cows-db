USE [COWS]
GO
_CreateObject 'FS','dbo','parseAssignedUsersFromAppt'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[parseAssignedUsersFromAppt] 
(
	@AssignedUsers	Varchar(Max)
)
RETURNS VARCHAR(max)
AS
BEGIN
	SELECT @AssignedUsers = REPLACE(@AssignedUsers, '<ResourceIds />', '') 	
	SELECT @AssignedUsers = REPLACE(@AssignedUsers, '<ResourceIds>', '') 
	SELECT @AssignedUsers = REPLACE(@AssignedUsers, '<ResourceId Type="System.Int32" Value="', '') 
	SELECT @AssignedUsers = REPLACE(@AssignedUsers, '" />', '|') 
	SELECT @AssignedUsers = RTRIM(LTRIM(REPLACE(@AssignedUsers, '</ResourceIds>', ''))) 	
	SELECT @AssignedUsers = RTRIM(LTRIM(REPLACE(@AssignedUsers, ' ', ''))) 	
	SELECT @AssignedUsers = RTRIM(LTRIM(REPLACE(@AssignedUsers, CHAR(13) + Char(10), ''))) 	
	
			
	RETURN @AssignedUsers
END
GO