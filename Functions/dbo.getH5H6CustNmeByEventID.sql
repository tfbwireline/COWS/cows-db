USE [COWS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================================================================================================
-- Author:		qi931353
-- Create date: 10/22/2021
-- Description:	Returns the H6 CUST_NME for a given Event
-- Update date:	01/25/2022
-- Description:	Replaced FRCDFT_CD for Generic H1
-- jbolano15 - 02/01/2022 - remove condition for Generic H1; we will pull H6 Customer Name for both generic and non-generic
-- ssandov3 - 02/10/2022 - Update condition to get Network customer name first then customer name on FSA_ORDER_CUST table.
-- ssandov3 - 02/18/2022 - Update condition for Network Events to get H6 Customer Name on CUST_NME holder else on FSA_ORDER_CUST table.
-- ssandov3 - 03/15/2022 - Update condition for MDS + Network type to get NTWK_CUST_NME.
-- =========================================================================================================================================
ALTER FUNCTION [dbo].[getH5H6CustNmeByEventID]
(
	@EVENT_ID		INT
)
RETURNS NVARCHAR(MAX)
AS 
BEGIN	
	DECLARE	@HCustID	VARCHAR(MAX) = ''
	DECLARE	@HCustNme	NVARCHAR(MAX) = ''
	DECLARE @CNT_TYPE INT = 0
	SELECT @CNT_TYPE = COUNT(NTWK_ACTY_TYPE_ID) FROM dbo.MDS_EVENT_NTWK_ACTY WITH (NOLOCK) WHERE EVENT_ID = @EVENT_ID

	SELECT TOP 1 @HCustID = CASE WHEN ISNULL(H6, '') <> '' THEN H6
								ELSE NTWK_H6
							END,
				@HCustNme = CASE WHEN @CNT_TYPE = 1 
					AND (SELECT 'x' FROM dbo.MDS_EVENT_NTWK_ACTY WITH (NOLOCK) WHERE EVENT_ID = @EVENT_ID AND NTWK_ACTY_TYPE_ID != 1) <> '' 
						THEN CUST_NME ELSE NTWK_CUST_NME END
	FROM dbo.MDS_EVENT WHERE EVENT_ID = @EVENT_ID
	IF LEN(@HCustID) > 0 AND LEN(ISNULL(@HCustNme,'')) <= 0
	BEGIN
		SELECT TOP 1 @HCustNme = CUST_NME FROM dbo.FSA_ORDR_CUST WITH (NOLOCK) WHERE CUST_ID = @HCustID
	END

	RETURN @HCustNme
END

