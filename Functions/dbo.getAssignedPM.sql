﻿USE [COWS]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		dlp0278
-- Create date: 11/16/2011
-- Description:	Returns User Name of Assigned PM.
-- Update by: Md m Monir/ Vn370313
-- Updated for: New MDS Event
-- Updated Date: 12/15/2021
-- Updated By:	Sarah Sandoval
-- Description:	Added condition to look for OLD_USER_ADID for ONEID Migration
-- 01/11/2022 - jbolano15 - Add MNS-PM Role from CNTCT_DETL; concatenate with ";" for 2 or more persons
-- 01/11/2022 - jbolano15 - CNTCT_DETL take precedence over MDS_EVENT.MNS_PM_ID
-- 02/03/2022 - ssandov3 - Added REC_STUS_ID = 1 in Contact List condition
-- =============================================
ALTER FUNCTION [dbo].[getAssignedPM] 
(
	@EventID		Int	
	,@SiteContentID	Int
)
RETURNS Varchar(50)
AS 
BEGIN	
	DECLARE	@RevID	Varchar(50)
	SET	@RevID	=	''
	
	IF (@SiteContentID	=	5)
	BEGIN
		SELECT	Top	1	@RevID	=	ISNULL(lu.DSPL_NME, '')
			FROM		dbo.MDS_EVENT_NEW	me	WITH (NOLOCK)
			INNER JOIN	dbo.LK_USER				lu	WITH (NOLOCK)	ON	me.MNS_PM_ID	=	lu.USER_ADID	OR	me.MNS_PM_ID	=	lu.OLD_USER_ADID
			WHERE		me.EVENT_ID		=	@EventID
		IF (@RevID = '')
		BEGIN
			--SELECT @RevID = STUFF
			--(
			--	(
			--		SELECT DISTINCT '; ' + [NAME]
			--		FROM (
			--			SELECT ISNULL(lu.DSPL_NME, '') AS [NAME]
			--			FROM		dbo.MDS_EVENT	me	WITH (NOLOCK)
			--			INNER JOIN	dbo.LK_USER				lu	WITH (NOLOCK)	ON	me.MNS_PM_ID	=	lu.USER_ADID	OR	me.MNS_PM_ID	=	lu.OLD_USER_ADID
			--			WHERE		me.EVENT_ID		=	@EventID
			--			UNION
			--			SELECT ISNULL(lu.DSPL_NME, '') AS [NAME]
			--			FROM dbo.MDS_EVENT me WITH (NOLOCK)
			--				INNER JOIN dbo.CNTCT_DETL cd WITH (NOLOCK) ON cd.OBJ_ID = me.EVENT_ID AND cd.OBJ_TYP_CD = 'E' AND cd.ROLE_ID = 122
			--				INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON lu.EMAIL_ADR = cd.EMAIL_ADR
			--			WHERE		me.EVENT_ID		=	@EventID
			--			) AS x FOR XML PATH('')
			--	),
			--		1, 2, ''
			--)
			SELECT @RevID = ISNULL(lu.DSPL_NME, '')
			FROM dbo.MDS_EVENT me WITH (NOLOCK)
				INNER JOIN dbo.CNTCT_DETL cd WITH (NOLOCK) ON cd.OBJ_ID = me.EVENT_ID AND cd.OBJ_TYP_CD = 'E' AND cd.ROLE_ID = 122 AND cd.REC_STUS_ID = 1
				INNER JOIN dbo.LK_USER lu WITH (NOLOCK) ON lu.EMAIL_ADR = cd.EMAIL_ADR
			WHERE
				me.EVENT_ID = @EventID

			IF (@RevID = '')
			BEGIN
				SELECT @RevID = ISNULL(lu.DSPL_NME, '')
				FROM dbo.MDS_EVENT me WITH (NOLOCK)
					INNER JOIN dbo.LK_USER lu WITH (NOLOCK)	ON	me.MNS_PM_ID = lu.USER_ADID	OR me.MNS_PM_ID = lu.OLD_USER_ADID
				WHERE
					me.EVENT_ID = @EventID
			END
		END
		IF (@RevID = '')
		BEGIN
			SELECT TOP 1 @RevID	= ISNULL(lu.DSPL_NME, '')
			FROM dbo.MDS_EVENT_ODIE_DEV od WITH (NOLOCK) INNER JOIN
			dbo.REDSGN rd WITH (NOLOCK) ON rd.REDSGN_NBR = od.RDSN_NBR INNER JOIN	
			dbo.LK_USER	lu	WITH (NOLOCK)	ON	rd.PM_ASSIGNED	=	lu.USER_ADID	OR	rd.PM_ASSIGNED	=	lu.OLD_USER_ADID
			WHERE od.EVENT_ID = @EventID
			  AND rd.PM_ASSIGNED IS NOT NULL
		END
	END
	ELSE IF (@SiteContentID	=	19)
	BEGIN
		SELECT TOP 1 @RevID	= ISNULL(lu.DSPL_NME, '')
			FROM dbo.UCaaS_EVENT_ODIE_DEV od WITH (NOLOCK) INNER JOIN
			dbo.REDSGN rd WITH (NOLOCK) ON rd.REDSGN_NBR = od.RDSN_NBR INNER JOIN	
			dbo.LK_USER	lu	WITH (NOLOCK)	ON	rd.PM_ASSIGNED	=	lu.USER_ADID	OR	rd.PM_ASSIGNED	=	lu.OLD_USER_ADID
			WHERE od.EVENT_ID = @EventID
			  AND rd.PM_ASSIGNED IS NOT NULL
	END
	
	RETURN @RevID		
END

