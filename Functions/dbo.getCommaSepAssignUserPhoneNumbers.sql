USE [COWS]
GO
GO
_CreateObject 'FS','dbo','getCommaSepAssignUserPhoneNumbers'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Jagannath Gangi
-- Create date: 2011-4-14
-- Description:	This SP is used to get Comma seperated UserIDs
-- =========================================================

ALTER FUNCTION [dbo].[getCommaSepAssignUserPhoneNumbers] 
(
	@EventID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @list VARCHAR(max)   
	SELECT @list = COALESCE(@list + ',', '') + lu.PHN_NBR  
		FROM dbo.EVENT_ASN_TO_USER ea WITH (NOLOCK) INNER JOIN
		     dbo.LK_USER lu WITH (NOLOCK) on lu.[USER_ID] = ea.ASN_TO_USER_ID
		WHERE ea.EVENT_ID = @EventID
		  AND ea.REC_STUS_ID = 1
		ORDER BY lu.DSPL_NME
		
--All FastTrack should have COWS ES Activators  as Assigned User IM830464
		
	IF (SELECT ISNULL(MDS_FAST_TRK_CD,0) FROM dbo.MDS_EVENT_NEW WITH (NOLOCK)
				where EVENT_ID = @EventID) = 1
  		BEGIN 
    		SET @List = ''
	    END
		
	IF (SELECT ISNULL(MDS_FAST_TRK_TYPE_ID,'M') FROM dbo.MDS_EVENT WITH (NOLOCK)
				where EVENT_ID = @EventID) = 'M'
  		BEGIN 
    		SET @List = ''
	    END

	RETURN @List
END
GO
