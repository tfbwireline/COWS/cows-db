USE [COWS]
GO

/****** Object:  UserDefinedFunction [web].[getVendorNameForDisconnects]    Script Date: 09/14/2012 12:25:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[web].[getVendorNameForDisconnects]') AND type in (N'FN', N'IF', N'TF', N'FS', N'FT'))
DROP FUNCTION [web].[getVendorNameForDisconnects]
GO
