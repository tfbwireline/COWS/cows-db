USE [COWS]
GO
/****** Object:  UserDefinedFunction [dbo].[GetSemiColonSepAssignUsers]    Script Date: 01/30/2012 14:49:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Jagannath Gangi
-- Create date: 2011-4-14
-- Description:	This SP is used to get Semi Colon seperated Assign Users
-- =========================================================

ALTER FUNCTION [dbo].[GetSemiColonSepAssignUsers] 
(
	@EventID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @list VARCHAR(max)   
	SELECT @list = COALESCE(@list + ';', '') + CAST(lu.DSPL_NME AS VARCHAR(200)) 
		FROM dbo.EVENT_ASN_TO_USER ea WITH (NOLOCK) INNER JOIN
		     dbo.LK_USER lu WITH (NOLOCK) on lu.[USER_ID] = ea.ASN_TO_USER_ID
		WHERE ea.EVENT_ID = @EventID
		  AND ea.REC_STUS_ID = 1
		ORDER BY lu.DSPL_NME
		
--All FastTrack should have COWS ES Activators  as Assigned User IM830464
		
	IF (SELECT ISNULL(MDS_FAST_TRK_CD,0) FROM dbo.MDS_EVENT_NEW WITH (NOLOCK)
				where EVENT_ID = @EventID ) = 1
  		BEGIN 
    		SET @List = 'COWS ES Activators'
	    END
	ELSE IF (SELECT EVENT_TYPE_ID FROM dbo.EVENT WITH (NOLOCK) 
				WHERE EVENT_ID = @EventID) = 9
		BEGIN
			SET @List = 'COWS Fedline Activators'
		END
		
		
	RETURN @List
END
