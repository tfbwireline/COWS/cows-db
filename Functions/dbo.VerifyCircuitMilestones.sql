USE [COWS]
GO
_CreateObject 'FS','dbo','VerifyCircuitMilestones'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <04/10/2012>
-- Description:	<To verify if ckt milestones have been completed on an order.>
-- =============================================
ALTER FUNCTION dbo.VerifyCircuitMilestones
(
	@ORDR_ID INT
)
RETURNS Varchar(1)
AS
BEGIN
	
	DECLARE @return Varchar(1)
	DECLARE @Ordr_Type Varchar(2)
	DECLARE @Prod_Type_CD Varchar(2)
	SET @return = 'Y'
	
	SELECT	@Ordr_Type		=	ISNULL(ORDR_TYPE_CD,''),
			@Prod_Type_CD	=	ISNULL(PROD_TYPE_CD,'')
		FROM	dbo.FSA_ORDR WITH (NOLOCK) 
		WHERE	ORDR_ID = @ORDR_ID
	
	IF @Ordr_Type = 'IN' AND @Prod_Type_CD IN ('DN','DO','MP','SN','SO','MO')
		BEGIN
			IF EXISTS
				(
					SELECT 'X'
						FROM	dbo.CKT ckt WITH (NOLOCK)
					INNER JOIN	dbo.CKT_MS cktms WITH (NOLOCK) ON cktms.CKT_ID = ckt.CKT_ID
						WHERE	ckt.ORDR_ID		=	@ORDR_ID
							AND	cktms.VER_ID	=	(SELECT MAX(VER_ID)
														FROM	dbo.CKT_MS cktmss WITH (NOLOCK)
														WHERE	cktms.CKT_ID = cktmss.CKT_ID
													)
							AND	(
									(ISNULL(cktms.TRGT_DLVRY_DT,'') = '')	OR
									(ISNULL(cktms.TRGT_DLVRY_DT_RECV_DT,'') = '') OR 
									(ISNULL(cktms.ACCS_ACPTC_DT,'') = '') OR 
									(ISNULL(cktms.ACCS_DLVRY_DT,'') = '') 
								)	
				)
				BEGIN
					SET @return = 'N'
				END
				
			IF NOT EXISTS
				(
					SELECT 'X'
						FROM	dbo.CKT ckt WITH (NOLOCK)
					INNER JOIN	dbo.CKT_MS cktms WITH (NOLOCK) ON cktms.CKT_ID = ckt.CKT_ID
						WHERE	ckt.ORDR_ID		=	@ORDR_ID
				)
				BEGIN
					SET @return = 'N'
				END						
		END
	ELSE IF @Ordr_Type = 'CH'
		BEGIN
			IF EXISTS
				(
					SELECT 'X'
						FROM	dbo.ORDR o WITH (NOLOCK)
					INNER JOIN	dbo.LK_PPRT lp WITH (NOLOCK) ON lp.PPRT_ID = o.PPRT_ID 
						WHERE	o.ORDR_ID	=	@ORDR_ID
							AND	lp.SM_ID	IN	(30,31)
				)
				BEGIN
					IF EXISTS
						(
							SELECT 'X'
								FROM	dbo.CKT ckt WITH (NOLOCK)
							INNER JOIN	dbo.CKT_MS cktms WITH (NOLOCK) ON cktms.CKT_ID = ckt.CKT_ID
								WHERE	ckt.ORDR_ID		=	@ORDR_ID
									AND	cktms.VER_ID	=	(SELECT MAX(VER_ID)
																FROM	dbo.CKT_MS cktmss WITH (NOLOCK)
																WHERE	cktms.CKT_ID = cktmss.CKT_ID
															)
									AND	(
											(ISNULL(cktms.TRGT_DLVRY_DT,'') = '')	OR
											(ISNULL(cktms.TRGT_DLVRY_DT_RECV_DT,'') = '')
										)
						)
						BEGIN
							SET @return = 'N'
						END
						
					IF NOT EXISTS
					(
						SELECT 'X'
							FROM	dbo.CKT ckt WITH (NOLOCK)
						INNER JOIN	dbo.CKT_MS cktms WITH (NOLOCK) ON cktms.CKT_ID = ckt.CKT_ID
							WHERE	ckt.ORDR_ID		=	@ORDR_ID
					)
					BEGIN
						SET @return = 'N'
					END		
				END
		END
		
	
	
	
	RETURN @return

END
GO

