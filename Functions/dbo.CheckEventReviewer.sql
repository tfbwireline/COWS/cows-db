USE [COWS]
GO
_CreateObject 'FS','dbo','CheckEventReviewer'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jagannath Gangi
-- Create date: 06/30/2011
-- Description:	Checks if the User is a Reviewer for given eventid
-- sbg9814	09/29/2011	For MDS Events, we should NOT check Event History table.
-- =============================================
ALTER FUNCTION [dbo].[CheckEventReviewer]
(
	@EventID INT,
	@UserID INT
)
RETURNS BIT 
AS
BEGIN
	DECLARE @rslt		Bit 
	IF EXISTS (SELECT top 1 eh.EVENT_HIST_ID
			   FROM dbo.EVENT_HIST eh WITH (NOLOCK) INNER JOIN
					dbo.LK_USER lu WITH (NOLOCK) ON lu.[USER_ID] = eh.[MODFD_BY_USER_ID] INNER JOIN
					dbo.USER_GRP_ROLE ug WITH (NOLOCK) ON ug.[USER_ID] = lu.[USER_ID]
			   WHERE lu.[USER_ID] = @UserID
			     AND eh.EVENT_ID = @EventID
			     AND ug.ROLE_ID = 22
			     AND ug.REC_STUS_ID = 1
			   ORDER BY eh.EVENT_HIST_ID DESC)
		SET @rslt = 1
	ELSE
		SET @rslt = 0

	RETURN  @rslt	 
END
