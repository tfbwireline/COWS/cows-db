USE [COWS]
GO
_CreateObject 'FS','dbo','getH5H6ByEventID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sbg9814
-- Create date: 10/14/2011
-- Description:	Returns the CUST_ID for a given Event
-- =============================================
ALTER FUNCTION [dbo].[getH5H6ByEventID] 
(
	@EVENT_ID		Int
	,@TAB_SEQ_NBR	Int	
)
RETURNS VARCHAR(MAX)
AS 
BEGIN	
	DECLARE	@HCustID	varchar(max)
	DECLARE @H5H6CustID Varchar(9)
	DECLARE	@TAB_NME	Varchar(50)
	SET	@HCustID	=	''
	SET	@TAB_NME	=	''

		SELECT TOP 1 @HCustID = CASE WHEN mena.NTWK_ACTY_TYPE_ID=1 THEN me.H6
							   ELSE me.NTWK_H6
							   END
		FROM dbo.MDS_EVENT me WITH (NOLOCK) INNER JOIN
		dbo.MDS_EVENT_NTWK_ACTY mena WITH (NOLOCK) ON mena.EVENT_ID=me.EVENT_ID
		WHERE me.MDS_ACTY_TYPE_ID != 3
		  AND me.EVENT_ID=@EVENT_ID
		IF LEN(@HCustID)<=0
		BEGIN
			SELECT @HCustID = CASE WHEN @HCustID = '' THEN H5_H6 ELSE @HCustID + COALESCE(',' + H5_H6, '') END
			FROM 
			(SELECT DISTINCT H5_H6
			FROM dbo.EVENT_DISCO_DEV WITH (NOLOCK)
			WHERE EVENT_ID=@EVENT_ID
			  AND REC_STUS_ID=1) as x
		END

	RETURN @HCustID
END

