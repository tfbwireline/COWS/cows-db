USE [COWS]
GO
_CreateObject 'FS','dbo','getVendorOrderEmailSubject'
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[getVendorOrderEmailSubject]') AND type in (N'FN'))
DROP FUNCTION [dbo].[getVendorOrderEmailSubject]
GO

