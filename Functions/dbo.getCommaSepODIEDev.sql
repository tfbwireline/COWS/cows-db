USE [COWS]
GO
_CreateObject 'FS','dbo','getCommaSepODIEDev'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		jrg7298
-- Create date: 10/09/2015
-- Description:	This UDF returns Comma seperated ODIE Device #s.
-- =========================================================

ALTER FUNCTION [dbo].[getCommaSepODIEDev] 
(
	@iEventID INT,
	@EventType CHAR(10)
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @DevNme VARCHAR(Max)   
	
	IF (@EventType = 'MDS')
	BEGIN
		SELECT	@DevNme = COALESCE(@DevNme + ',', '')   + ISNULL(ODIE_DEV_NME, '')
			FROM		dbo.MDS_MNGD_ACT_NEW WITH (NOLOCK)    
			WHERE		EVENT_ID	=	@iEventID    
			ORDER BY	MDS_MNGD_ACT_ID		DESC  
	END
	ELSE IF (@EventType = 'UCaaS')
	BEGIN
		SELECT	@DevNme = COALESCE(@DevNme + ',', '')   + ISNULL(ODIE_DEV_NME, '')
			FROM		dbo.UCaaS_EVENT_ODIE_DEV WITH (NOLOCK)    
			WHERE		EVENT_ID	=	@iEventID    
			ORDER BY	UCaaS_EVENT_ODIE_DEV_ID		DESC  
	END
	
	RETURN @DevNme
END
GO
