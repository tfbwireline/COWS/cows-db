USE [COWS]
GO
_CreateObject 'FS','dbo','GetCommaSepAssignUserEmails'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetCommaSepAssignUserEmails] 
(
	@EventID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @list VARCHAR(max)   
	SELECT @list = COALESCE(@list + ',', '') + CAST(lu.EMAIL_ADR AS VARCHAR(100)) 
		FROM dbo.EVENT_ASN_TO_USER ea WITH (NOLOCK) INNER JOIN
		     dbo.LK_USER lu WITH (NOLOCK) on lu.[USER_ID] = ea.ASN_TO_USER_ID
		WHERE (ea.EVENT_ID = @EventID) AND (ea.REC_STUS_ID = 1) AND ea.ASN_TO_USER_ID != 5001
		ORDER BY lu.DSPL_NME
	RETURN @List
END