USE [COWS]
GO
_CreateObject 'FS','dbo','getReviewerForEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		sbg9814
-- Create date: 10/21/2011
-- Description:	Returns Yes or No for any given Bit field.
-- =============================================
ALTER FUNCTION [dbo].[getReviewerForEvent] 
(
	@EventID		Int	
	,@SiteContentID	Int
)
RETURNS Varchar(50)
AS 
BEGIN	
	DECLARE	@RevID	Varchar(50)
	SET	@RevID	=	''
	
	SELECT	Top	1	@RevID	=	ISNULL(lu.DSPL_NME, '')
		FROM		dbo.FSA_MDS_EVENT_ORDR	fm	WITH (NOLOCK)
		INNER JOIN	dbo.USER_WFM_ASMT		wf	WITH (NOLOCK)	ON	fm.ORDR_ID		=	wf.ORDR_ID	
		INNER JOIN	dbo.LK_USER				lu	WITH (NOLOCK)	ON	wf.ASN_USER_ID	=	lu.USER_ID
		WHERE		@SiteContentID	=	5
			AND		fm.EVENT_ID		=	@EventID
			AND		wf.GRP_ID		=	2		
			
	RETURN @RevID		
END

