USE [COWS]
GO
_CreateObject 'FS','dbo','GetPrevMDSFTUserByEventID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =========================================================
-- Author:		Jagannath Gangi
-- Create date: 2015-03-10
-- Description:	This Function is used to get Previous User who worked the FT event to different status
-- =========================================================

ALTER FUNCTION [dbo].[GetPrevMDSFTUserByEventID] 
(
	@EventID INT
)
RETURNS VARCHAR(200)
AS
BEGIN
	DECLARE @FullName VARCHAR(200)   
	SELECT TOP 1 @FullName = CAST(lu.FULL_NME AS VARCHAR(200))
		FROM dbo.EVENT_HIST eh WITH (NOLOCK) INNER JOIN
		     dbo.LK_USER lu WITH (NOLOCK) on lu.[USER_ID] = eh.CREAT_BY_USER_ID
		WHERE eh.EVENT_ID = @EventID
		  AND eh.ACTN_ID NOT IN (1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 18, 20, 21, 22, 23, 26, 27, 28, 29, 44, 45, 50, 60, 61)
		ORDER BY eh.EVENT_HIST_ID DESC
	IF (LEN(COALESCE(@FullName,'')) <= 0)
	SET @FullName = 'COWS ES Activators'
	
	RETURN @FullName
END
