USE [COWS]
GO
_CreateObject 'FS','dbo','getOrderStatusForEvent'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jrg7298
-- Create date: 03/26/2017
-- Description:	Returns Order Status/Task Status using ORDR_ID(Used by MDS Event).
-- =============================================
ALTER FUNCTION [dbo].[getOrderStatusForEvent] 
(
	@ORDR_ID	Int
)
RETURNS Varchar(100)
AS 
BEGIN	
	DECLARE	@OrdrStus	Varchar(100)


	SELECT TOP 1 @OrdrStus = Case	ISNULL(lt.TASK_NME, '')
							When	'Bill Activated'	Then	lo.ORDR_STUS_DES 
							WHEN	'xNCI Milestones Complete' THEN 'xNCI Milestones Complete'
							WHEN	'' THEN lo.ORDR_STUS_DES
							Else	lt.TASK_NME	+ '-' + ls.STUS_DES
						End
					FROM dbo.ORDR odr WITH (NOLOCK) inner join
					dbo.LK_ORDR_STUS lo WITH (NOLOCK) on lo.ORDR_STUS_ID = odr.ORDR_STUS_ID LEFT JOIN 
						 dbo.ACT_TASK		at	WITH (NOLOCK) ON	odr.ORDR_ID				=	at.ORDR_ID 
																							AND	at.STUS_ID				IN	(0, 3)
						LEFT JOIN	LK_TASK				lt WITH (NOLOCK) ON lt.TASK_ID				=	at.TASK_ID 
						LEFT JOIN	dbo.LK_STUS			ls WITH (NOLOCK) ON at.STUS_ID				=	ls.STUS_ID
					WHERE ((odr.ORDR_STUS_ID = lo.ORDR_STUS_ID)
					  AND ((lt.TASK_ID IS NULL) OR (lt.TASK_ID IN (600, 601, 602, 604, 1000, 1001) AND (at.STUS_ID IN (0,3))))
					  )
					  AND odr.ORDR_ID=@ORDR_ID
					ORDER BY at.CREAT_DT DESC

	IF (ISNULL(@OrdrStus,'')='')
	BEGIN
		SELECT TOP 1 @OrdrStus = Case	ISNULL(lt.TASK_NME, '')
							When	'Bill Activated'	Then	lo.ORDR_STUS_DES 
							WHEN	'xNCI Milestones Complete' THEN 'xNCI Milestones Complete'
							WHEN	'' THEN lo.ORDR_STUS_DES
							Else	lt.TASK_NME	+ '-' + ls.STUS_DES
						End
					FROM dbo.ORDR odr WITH (NOLOCK) inner join
					dbo.LK_ORDR_STUS lo WITH (NOLOCK) on lo.ORDR_STUS_ID = odr.ORDR_STUS_ID LEFT JOIN 
						 dbo.ACT_TASK		at	WITH (NOLOCK) ON	odr.ORDR_ID				=	at.ORDR_ID 
																							AND	at.STUS_ID				IN	(0, 3)
						LEFT JOIN	LK_TASK				lt WITH (NOLOCK) ON lt.TASK_ID				=	at.TASK_ID 
						LEFT JOIN	dbo.LK_STUS			ls WITH (NOLOCK) ON at.STUS_ID				=	ls.STUS_ID
					WHERE (odr.ORDR_STUS_ID = lo.ORDR_STUS_ID)					  
					  AND odr.ORDR_ID=@ORDR_ID
					ORDER BY at.CREAT_DT DESC
	END
	
	RETURN @OrdrStus
END
