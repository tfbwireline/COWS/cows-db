USE [COWS]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE FUNCTION [dbo].[GetOrderTasksByProfile] 
(
	@OrderID	Int,
	@PrntPrfID	SmallInt
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @list VARCHAR(max)
	If @PrntPrfID = 0
		SET @list = ''
	ELSE IF @PrntPrfID = 999
		BEGIN
			SELECT @list = COALESCE(@list + ',', '') + lt.TASK_NME + ' Pending'
			FROM			dbo.ACT_TASK		at WITH (NOLOCK)
				INNER JOIN	dbo.LK_TASK			lt WITH (NOLOCK) ON	at.TASK_ID	=	lt.TASK_ID
			WHERE	at.ORDR_ID	=	@OrderID
				AND	at.STUS_ID	=	0
				AND at.TASK_ID NOT IN (1000,1001)
		END
	ELSE 
		BEGIN
			SELECT @list = COALESCE(@list + ',', '') + lt.TASK_NME + ' Pending'
			FROM			dbo.ACT_TASK		at WITH (NOLOCK)
				INNER JOIN	dbo.LK_TASK			lt WITH (NOLOCK)	ON	at.TASK_ID	=	lt.TASK_ID
				INNER JOIN	dbo.MAP_PRF_TASK	mt WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
			WHERE	mt.PRNT_PRF_ID  = @PrntPrfID
				AND	at.ORDR_ID = @OrderID
				AND	at.STUS_ID = 0				
				
			
			SELECT @list = COALESCE(@list + ',', '') + lt.TASK_NME + ' HOLD'
			FROM			dbo.ACT_TASK		at WITH (NOLOCK)
				INNER JOIN	dbo.LK_TASK			lt WITH (NOLOCK)	ON	at.TASK_ID	=	lt.TASK_ID
				INNER JOIN	dbo.MAP_PRF_TASK	mt WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
			WHERE	mt.PRNT_PRF_ID  = @PrntPrfID
				AND	at.ORDR_ID = @OrderID
				AND	at.STUS_ID = 156
		END
	
	RETURN @List
END

GO


