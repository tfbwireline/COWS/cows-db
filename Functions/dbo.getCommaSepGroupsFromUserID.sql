USE [COWS]
GO
_CreateObject 'FS','dbo','getCommaSepGroupsFromUserID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[getCommaSepGroupsFromUserID] 
(
	@USERID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @list VARCHAR(max)   
	SELECT @list = COALESCE(@list + ',', '') + CAST(x.GRP_NME AS VARCHAR(100))
	FROM (SELECT DISTINCT lg.GRP_NME
		FROM dbo.USER_GRP_ROLE ugr WITH (NOLOCK) INNER JOIN
		     dbo.LK_GRP lg WITH (NOLOCK) on lg.GRP_ID = ugr.GRP_ID
		WHERE (ugr.[USER_ID] = @USERID) AND (ugr.REC_STUS_ID = 1) AND (lg.REC_STUS_ID = 1)
		UNION
		SELECT DISTINCT SUBSTRING(lup.USR_PRF_DES,1,(CHARINDEX(' ',lup.USR_PRF_DES)-1)) as GRP_NME
		FROM dbo.MAP_USR_PRF mup WITH (NOLOCK) INNER JOIN
		     dbo.LK_USR_PRF lup WITH (NOLOCK) on lup.USR_PRF_ID=mup.USR_PRF_ID
		WHERE (mup.[USER_ID] = @USERID) AND (mup.REC_STUS_ID = 1) AND (lup.REC_STUS_ID = 1) AND (CHARINDEX(' ',lup.USR_PRF_DES)>0)
		  AND lup.USR_PRF_DES NOT LIKE '%Read%'
		  AND lup.USR_PRF_DES NOT LIKE '%Report%') as x
		ORDER BY x.GRP_NME
	RETURN @List
END