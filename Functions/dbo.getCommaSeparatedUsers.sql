USE [COWS]
GO
_CreateObject 'FS','dbo','getCommaSeparatedUsers'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER FUNCTION [dbo].[getCommaSeparatedUsers] 
(
	@ORDR_ID	Int
	,@WG_ID		Int
	,@UserID	Int
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE	@Users	Varchar(Max)
	SET	@Users	=	''
	
	IF(@WG_ID = 13 OR @WG_ID = 15)
		BEGIN
			SELECT		@Users	=	COALESCE(@Users + ', ' + FULL_NME, FULL_NME)
			FROM	(SELECT FULL_NME FROM LK_USER WITH (NOLOCK)	WHERE USER_ID IN	
						(SELECT top 1 uw.ASN_USER_ID	
							FROM	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	
							WHERE	uw.ORDR_ID		=	@ORDR_ID
								ORDER BY uw.CREAT_DT DESC))A
		END
	ELSE
		BEGIN
		SELECT		@Users	=	COALESCE(@Users + ', ' + FULL_NME, FULL_NME)
			FROM	(SELECT FULL_NME FROM LK_USER WITH (NOLOCK)	WHERE USER_ID IN	
						(SELECT DISTINCT uw.ASN_USER_ID	
							FROM	dbo.USER_WFM_ASMT	uw	WITH (NOLOCK)	
							WHERE	uw.ORDR_ID		=	@ORDR_ID
								AND	uw.GRP_ID		=	@WG_ID
								AND	uw.ASN_USER_ID	=	Case	@UserID
															When	0	Then	uw.ASN_USER_ID
															Else	@UserID
														End))A
	END	
													
	SELECT	@Users	=	Case	Len(@Users)
							When	0	Then	''
							Else	SubString(@Users, 3, Len(@Users))
						End												
			
	RETURN @Users
END
GO