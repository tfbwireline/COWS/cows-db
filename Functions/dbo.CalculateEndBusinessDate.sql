USE COWS
GO
_CreateObject 'FS','dbo','CalculateEndBusinessDate'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <02/29/2016>
-- Description:	<To calculate end business date>
-- =============================================
--Select dbo.CalculateEndBusinessDate('03/05/16',5)
ALTER FUNCTION dbo.CalculateEndBusinessDate
(
	@StartDT DateTime,
	@Interval Int
)
RETURNS DateTime
AS
BEGIN
	DECLARE @Cnt Int = 0, @Date DateTime = @StartDT
	IF @Interval <= 0
		RETURN @StartDT
	ELSE
		BEGIN
			
			WHILE (@Cnt < @Interval)
				BEGIN
					IF DATEPART(WEEKDAY,@Date) NOT IN (1,7) 
						SET @Cnt = @Cnt + 1
					IF @Cnt < @Interval
						SET @Date = @Date + 1	
				END
		END
	RETURN @Date
END
GO

