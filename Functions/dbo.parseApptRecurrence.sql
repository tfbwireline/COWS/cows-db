USE [COWS]
GO
_CreateObject 'FT','dbo','parseApptRecurrence'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO  
-- =========================================================    
-- Author:		Kyle WIchert
-- Create date: 2011-08-30
-- Description: This procedure will take a record from dbo.Appt
--				and parse out any values it provides such as 
--				start date, end date, occurrence count, range etc  
--				Example input - 
-- <RecurrenceInfo Start="07/14/2011 12:00:00" End="07/15/2011 00:00:00" Id="37a94579-3826-4ba1-9984-bbb96d083c24" OccurrenceCount="2" Range="2" />
-- =========================================================    
    
ALTER FUNCTION [dbo].[parseApptRecurrence]   
(
	@InStr	VARCHAR(MAX)
)
RETURNS
@parsedValues TABLE
(
	header		VARCHAR(100),
	value		VARCHAR(100)
)
AS  
BEGIN

	DECLARE @str	VARCHAR(MAX)
	DECLARE @hdr	VARCHAR(100)
	DECLARE @val	VARCHAR(100)
	DECLARE @pos	INT
	SET @str = @InStr
	
	IF CHARINDEX('RecurrenceInfo', @str) > 0
	BEGIN
		SET @str = REPLACE(SUBSTRING(@str, CHARINDEX('RecurrenceInfo', @str) + 15, 10000), '/>','')
		SET @pos = CHARINDEX('=', @str)
		
		WHILE @pos > 0
		BEGIN
			SET @hdr = LEFT(@str, @pos-1)
			SET @val = SUBSTRING(@str, @pos + 2, CHARINDEX('"', @str, @pos + 2) - CHARINDEX('"', @str) - 1)
			
			INSERT INTO @parsedValues
			SELECT @hdr, @val
			
			SET @str = RIGHT(@str, LEN(@str) - CHARINDEX('"', @str, @pos + 2))
			SET @pos = CHARINDEX('=', @str)
		END
	END
	RETURN
END
GO
