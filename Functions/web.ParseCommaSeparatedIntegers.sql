USE [COWS]
GO
_CreateObject 'FTV','web','ParseCommaSeparatedIntegers'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- ======================================================================
-- Author:		csb9923
-- Create date: 4/16/11
-- Description:	The following is a general purpose UDF to split passed 
--				in delimiter separated Integer list into individual items.
--				If delimiter is not specified, 'comma' is taken as default
-- ======================================================================

ALTER FUNCTION [web].[ParseCommaSeparatedIntegers]
(
	@IntegerList varchar(max),
	@Delimiter char(1) 
)
RETURNS 
@ParsedList table
(
	IntegerID int
)
AS
BEGIN
	DECLARE @ID varchar(10), @Pos int

	IF @Delimiter = ''
	BEGIN
	SET @Delimiter = ','
	END

	SET @IntegerList = LTRIM(RTRIM(@IntegerList))+ @Delimiter
	SET @Pos = CHARINDEX(@Delimiter, @IntegerList, 1)

	IF REPLACE(@IntegerList, @Delimiter, '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @ID = LTRIM(RTRIM(LEFT(@IntegerList, @Pos - 1)))
			IF @ID <> ''
			BEGIN
				INSERT INTO @ParsedList (IntegerID) 
				VALUES (CAST(@ID AS int)) --Use Appropriate conversion
			END
			SET @IntegerList = RIGHT(@IntegerList, LEN(@IntegerList) - @Pos)
			SET @Pos = CHARINDEX(@Delimiter, @IntegerList, 1)
		END
	END	
	RETURN
END


