USE [COWS]
GO
_CreateObject 'FS','dbo','RemoveEmailSpecialCharacters'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
-- Updated By:   jrg7298
-- Updated Date: 07/14/2019
-- Updated Reason: Cleanup data for Emails
*/

ALTER FUNCTION [dbo].[RemoveEmailSpecialCharacters] 
 (@str VARCHAR(max))
RETURNS VARCHAR(max)
BEGIN
  DECLARE @ValidCharacters VARCHAR(max) = char(0) + char(1) + char(2) + char(3) + char(4) + char(5) + char(6) + char(7) + char(8) + char(11) + 
        char(12) + char(14) + char(15) + char(16) + char(17) + char(18) + char(19) + char(20) + char(21) + char(22) + 
        char(23) + char(24) + char(25) + char(26) + char(27) + char(28) + char(29) + char(30) + char(31);
  WHILE PATINDEX('%[' + @ValidCharacters + ']%',@str) > 0
   SET @str=REPLACE(@str, SUBSTRING(@str ,PATINDEX('%[' + @ValidCharacters +
']%',@str), 1) ,'')
  RETURN @str
  
END