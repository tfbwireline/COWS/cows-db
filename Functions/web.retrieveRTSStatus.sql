USE [COWS]
GO
_CreateObject 'FS','web','retrieveRTSStatus'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <03/22/2012>
-- Description:	<To retrieve RTS flag status>
-- =============================================
--Select web.retrieveRTSStatus(5389,2)
ALTER FUNCTION web.retrieveRTSStatus
(
	@ORDR_ID	INT,
	@IsStatus	TinyInt
)
RETURNS Varchar(500)
AS
BEGIN
	DECLARE @ORDR_ACTN_ID	SMALLINT
	DECLARE @ORDR_CAT_ID	SMALLINT
	DECLARE @RetValue		Varchar(500)
	SET @ORDR_ACTN_ID	=	0
	SET @ORDR_CAT_ID	=	0
	SET @RetValue		=	''
	
	IF @IsStatus = 2
		BEGIN
			SELECT TOP 1 @RetValue = ISNULL(NTE_TXT,'')
				FROM	dbo.ORDR_NTE WITH (NOLOCK)
				WHERE	ORDR_ID = @ORDR_ID
					AND	NTE_TYPE_ID = 21
				ORDER BY NTE_ID DESC
		END
	ELSE
		BEGIN
			SELECT		@ORDR_CAT_ID	=	o.ORDR_CAT_ID,
						@ORDR_ACTN_ID	=	ISNULL(fo.ORDR_ACTN_ID,0)
				FROM	dbo.ORDR		o	WITH (NOLOCK)
			LEFT JOIN	dbo.FSA_ORDR	fo	WITH (NOLOCK) ON fo.ORDR_ID = o.ORDR_ID
				WHERE	o.ORDR_ID = @ORDR_ID
	
			IF ((@ORDR_CAT_ID = 1) OR (@ORDR_CAT_ID	IN	(2,6) AND @ORDR_ACTN_ID = 2))
				BEGIN
					IF @IsStatus = 1 
						BEGIN
							IF EXISTS
								(
									SELECT 'X'
										FROM	dbo.ACT_TASK	WITH (NOLOCK)	
										WHERE	ORDR_ID		=	@ORDR_ID
											AND	TASK_ID		=	500
											AND	STUS_ID		=	0	
								)
								BEGIN
									SET @RetValue = 'Yes'
								END
						END
					ELSE
						BEGIN
							SELECT		@RetValue	=	CONVERT(Varchar(10),ISNULL(CREAT_DT,''))	
								FROM	dbo.ACT_TASK WITH (NOLOCK)
								WHERE	ORDR_ID =	@ORDR_ID
									AND	TASK_ID	=	500
									AND	STUS_ID	=	0
						
						END
				END
			ELSE IF @ORDR_ACTN_ID	=	1
				BEGIN
					IF @IsStatus = 1 
						BEGIN
							IF EXISTS
								(
									SELECT 'X'
										FROM	dbo.ORDR_NTE WITH (NOLOCK)
										WHERE	ORDR_ID		=	@ORDR_ID
											AND NTE_TXT		=	'GOM user initiated RTS on this order.'
								)
								BEGIN
									SET @RetValue = 'Yes'
								END
						END
					ELSE
						BEGIN
							SELECT		@RetValue	=	CONVERT(Varchar(10),ISNULL(CREAT_DT,''))	
								FROM	dbo.ORDR_NTE WITH (NOLOCK)
								WHERE	ORDR_ID =	@ORDR_ID
									AND NTE_TXT		=	'GOM user initiated RTS on this order.'
							
						END
				END
		
			IF @RetValue = '' AND @IsStatus = 1
				set @RetValue = 'No'
		END
	RETURN @RetValue

END
GO

