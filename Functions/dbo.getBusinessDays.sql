USE COWS
GO
_CreateObject 'FS','dbo','getBusinessDays'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		csb9923
-- Create date: 9/28/2011
-- Description:	returns the no of working days considering only weekends and sprint holidays.
-- =============================================
ALTER FUNCTION dbo.getBusinessDays
(
	-- Add the parameters for the function here
	@FromDate Datetime,
	@ToDate Datetime
)
RETURNS int
AS
BEGIN
	DECLARE @Diff INT
	DECLARE @Ctr INT
	DECLARE @WorkingDay INT
	SET @Diff = DATEDIFF(day,@FromDate,@ToDate)
	SET @Ctr = 0
	SET @WorkingDay = 0
		IF @Diff > 0
			BEGIN
				WHILE (@Ctr < @Diff)
					BEGIN
						IF @Ctr > 0
							SET @FromDate = DATEADD(day,1,@FromDate)
						
						--PRINT @FromDate
						--PRINT DATEPART(WEEKDAY,@FromDate)
						IF (
								DATEPART(WEEKDAY,@FromDate) NOT IN (1,7) 
								AND NOT EXISTS
								(
									SELECT 'X' FROM dbo.LK_SPRINT_HLDY WITH (NOLOCK) WHERE SPRINT_HLDY_DT = @FromDate
								)	
							)
							BEGIN
								SET @WorkingDay = @WorkingDay + 1
								--PRINT @WorkingDay
							END
						SET @Ctr = @Ctr + 1
					END
			END

RETURN @WorkingDay


END
GO

