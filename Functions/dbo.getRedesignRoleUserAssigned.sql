USE [COWS]
GO
/****** Object:  UserDefinedFunction [dbo].[getRedesignRoleUserAssigned]    Script Date: 1/18/2022 1:58:37 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================
-- Author:		Sarah Sandoval
-- Create date: 06/22/2021
-- Description:	Returns Assigned User per given Role.
-- Jbolano15 - 11/23/2021 - consider "@sprint.com" on email address then replace it to "@t-mobile.com" for proper conditioning
-- ===============================================================
Create FUNCTION [dbo].[getRedesignRoleUserAssigned] 
(
	@RedsgnID	INT,
	@RoleID		INT
)
RETURNS VARCHAR(MAX)
AS 
BEGIN	
	DECLARE	@users	VARCHAR(MAX) = ''
	--DECLARE @RedsgnID INT = 11122, @RoleID INT = 81

	-- Get NTE/PM Assigned from Redesign Table
	IF NOT EXISTS (SELECT 1 FROM dbo.CNTCT_DETL WITH (NOLOCK) WHERE OBJ_TYP_CD = 'R' AND OBJ_ID = @RedsgnID AND REC_STUS_ID = 1 AND ROLE_ID = @RoleID)
		BEGIN
			IF (@RoleID = 81)
				BEGIN
					SELECT @users = u.DSPL_NME
					FROM dbo.REDSGN r WITH (NOLOCK)
					JOIN dbo.LK_USER u WITH (NOLOCK) ON r.NTE_ASSIGNED = u.USER_ADID
					WHERE r.REDSGN_ID = @RedsgnID 
				END
			ELSE IF (@RoleID = 122)
				BEGIN
					SELECT @users = u.DSPL_NME
					FROM dbo.REDSGN r WITH (NOLOCK)
					JOIN dbo.LK_USER u WITH (NOLOCK) ON r.PM_ASSIGNED = u.USER_ADID
					WHERE r.REDSGN_ID = @RedsgnID 
				END
		END
	-- Get NTE/PM Assigned from Contact Details Table
	ELSE
		BEGIN
			SET @users = (SELECT usr.DSPL_NME + ','
							FROM dbo.CNTCT_DETL cd WITH (NOLOCK)
							JOIN dbo.LK_USER usr WITH (NOLOCK) ON REPLACE(cd.EMAIL_ADR, '@SPRINT.COM', '@T-MOBILE.COM') = REPLACE(usr.EMAIL_ADR, '@SPRINT.COM', '@T-MOBILE.COM')
							WHERE cd.OBJ_TYP_CD = 'R' AND cd.OBJ_ID = @RedsgnID AND cd.REC_STUS_ID = 1 AND cd.ROLE_ID = @RoleID FOR XML PATH(''))
		END
	
	--SELECT @users
	RETURN @users
END

