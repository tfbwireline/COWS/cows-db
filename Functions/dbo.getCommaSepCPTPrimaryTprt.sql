USE [COWS]
GO
_CreateObject 'FS','dbo','getCommaSepCPTPrimaryTprt'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		jrg7298
-- Create date: 10/09/2015
-- Description:	This UDF returns Comma seperated CPT Primary Transports.
-- =========================================================

ALTER FUNCTION [dbo].[getCommaSepCPTPrimaryTprt] 
(
	@CPT_ID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @PrimaryTprt VARCHAR(Max)   
	
		SELECT	@PrimaryTprt = COALESCE(@PrimaryTprt + ',', '')   + ISNULL(lcpw.CPT_PRIM_SCNDY_TPRT, '')
			FROM		dbo.CPT_PRIM_WAN cpw WITH (NOLOCK) INNER JOIN
						dbo.LK_CPT_PRIM_SCNDY_TPRT lcpw WITH (NOLOCK) ON cpw.CPT_PRIM_SCNDY_TPRT_ID = lcpw.CPT_PRIM_SCNDY_TPRT_ID
			WHERE		cpw.CPT_ID	=	@CPT_ID
	
	RETURN @PrimaryTprt
END
GO
