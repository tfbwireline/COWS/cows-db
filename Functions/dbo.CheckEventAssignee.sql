USE [COWS]
GO
_CreateObject 'FS','dbo','CheckEventAssignee'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Jagannath Gangi
-- Create date: 06/30/2011
-- Description:	Checks if the User is a Assignee for given eventid
-- =============================================
ALTER FUNCTION [dbo].[CheckEventAssignee] 
(
	@EventID INT,
	@UserID INT
)
RETURNS BIT 
AS
BEGIN
	DECLARE @rslt			BIT  
	DECLARE	@EVENT_TYPE_ID	Int
	SET	@EVENT_TYPE_ID	=	0
	SET @rslt			=	0
	
	SELECT		@EVENT_TYPE_ID	=	EVENT_TYPE_ID
		FROM	dbo.EVENT	WITH (NOLOCK)
		WHERE	EVENT_ID	=	@EventID
		
	IF	@EVENT_TYPE_ID	=	9
		BEGIN
			IF	EXISTS
				(SELECT 'X' FROM	dbo.FEDLINE_EVENT_USER_DATA	WITH (NOLOCK)
							WHERE	EVENT_ID		=	@EventID
								AND	(ENGR_USER_ID	=	@UserID	
								OR	ACTV_USER_ID	=	@UserID)
				)
				BEGIN
					SET	@rslt	=	1
				END
		END	
	ELSE	
		BEGIN	
			 IF EXISTS
				(
					SELECT		'X'
						FROM	dbo.MDS_EVENT_NEW	WITH (NOLOCK)
						WHERE	EVENT_ID		=	@EventID
							AND MDS_FAST_TRK_CD	=	1
							AND	EVENT_STUS_ID	=	6
							
				)
				BEGIN
					IF EXISTS (SELECT 'X'
									FROM dbo.EVENT_HIST WITH (NOLOCK)
									WHERE	EVENT_ID			=	@EventID 
										AND CREAT_BY_USER_ID	=	@UserID
										AND ACTN_ID				=	17
								)
						SET @rslt = 1
					ELSE
						SET @rslt = 0
				END
			ELSE 
				BEGIN
					IF EXISTS (SELECT 'x'
							   FROM dbo.EVENT_ASN_TO_USER ea WITH (NOLOCK)
							   WHERE ea.ASN_TO_USER_ID = @UserID
								 AND ea.EVENT_ID = @EventID
								 AND ea.REC_STUS_ID = 1)
						SET @rslt = 1
					ELSE
						SET @rslt = 0
				END
		END
	RETURN  @rslt
END
GO