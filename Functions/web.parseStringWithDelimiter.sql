USE [COWS]
GO
_CreateObject 'FTV','web','parseStringWithDelimiter'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:		csb9923
-- Create date: 4/16/11
-- Description:	The following is a general purpose UDF to split passed 
--				in delimiter separated list into individual items.
--				If delimiter is not specified, 'comma' is taken as default
-- ======================================================================

ALTER FUNCTION [web].[parseStringWithDelimiter]
(
	@StringList varchar(max),
	@Delimiter char(1) 
)
RETURNS 
@ParsedList table
(
	StringID varchar(400)
)
AS
BEGIN
	DECLARE @ID varchar(400), @Pos int

	IF @Delimiter = ''
	BEGIN
	SET @Delimiter = ','
	END

	SET @StringList = LTRIM(RTRIM(@StringList))+ @Delimiter
	SET @Pos = CHARINDEX(@Delimiter, @StringList, 1)

	IF REPLACE(@StringList, @Delimiter, '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @ID = LTRIM(RTRIM(LEFT(@StringList, @Pos - 1)))
			DECLARE	@NewLine		Char(2)
			SET		@NewLine	=	Char(13) + Char(10)
			SET		@ID			=	REPLACE(@ID, @NewLine, '')
			INSERT INTO @ParsedList (StringID) 
			VALUES ( @ID ) --Use Appropriate conversion
			SET @StringList = RIGHT(@StringList, LEN(@StringList) - @Pos)
			SET @Pos = CHARINDEX(@Delimiter, @StringList, 1)

		END
	END	
	RETURN
END
GO


