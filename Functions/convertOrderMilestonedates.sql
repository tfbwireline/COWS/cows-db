USE [COWS]
GO
_CreateObject 'FS','dbo','convertOrderMilestonedates'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <08/07/2011>
-- Description:	<To convert milestone dates to MMM/Day/Year format ie: AUG/07/11>
-- =============================================
--Select dbo.convertOrderMilestonedates(null)
ALTER FUNCTION dbo.convertOrderMilestonedates
(
	@input smalldatetime 
)
RETURNS Varchar(20)
AS
BEGIN
	DECLARE @output varchar(20)
	IF @input is not null
		Select @output = LEFT(CONVERT(VARCHAR(12), @input, 107), 3) + '/' + RIGHT(CONVERT(VARCHAR(12), @input, 1), 5)
	RETURN @output
END
GO

