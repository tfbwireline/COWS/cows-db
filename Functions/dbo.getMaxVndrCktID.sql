USE [COWS]
GO
_CreateObject 'FS','dbo','getMaxVndrCktID'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		jrg7298
-- Create date: 02/01/2015
-- Description:	Returns Time Slot Desc
-- =============================================
ALTER FUNCTION [dbo].[getMaxVndrCktID] 
(
	@OrdrID		Int	
)
RETURNS Varchar(50)
AS 
BEGIN	
	DECLARE	@VndrCktID	Varchar(200), @CktID INT, @OrdID INT
	SET	@VndrCktID	=	''

	
	SELECT @CktID = MAX(c1.CKT_ID), @OrdID = c1.ORDR_ID, @VndrCktID =c1.VNDR_CKT_ID 
						FROM dbo.CKT c1 WITH (NOLOCK) INNER JOIN
						dbo.ORDR ord WITH (NOLOCK) ON ord.ORDR_ID=c1.ORDR_ID
						WHERE c1.VNDR_CKT_ID IS NOT NULL AND ord.ORDR_CAT_ID=6 AND ord.ORDR_ID=@OrdrID
						GROUP BY c1.ORDR_ID, c1.VNDR_CKT_ID

			
	RETURN @VndrCktID		
END
