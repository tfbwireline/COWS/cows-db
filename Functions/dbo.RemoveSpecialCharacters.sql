USE [COWS]
GO
_CreateObject 'FS','dbo','RemoveSpecialCharacters'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
-- Updated By:   Md M Monir
-- Updated Date: 03/24/2018
-- Updated Reason: Added   [ / ] / %  Special Character to remove
*/

ALTER FUNCTION [dbo].[RemoveSpecialCharacters] --SELECT dbo.[RemoveSpecialCharacters]('BYR = D. BROOKS. INTL CPE. MARK PACKAGE FTN 11580472. PLACE WITH LOGICALIS. REFERENCE QUOTE #SPRINT/088/Q141775/SC. MSCP - DART #PP-CMSP-151011-43751. FINAL SHIP TO:  STIELSTRASSE 11; WIESBADEN, 99999; Germany.') -- gives '2'
(
    @Text VARCHAR(max)
)
RETURNS VARCHAR(max)
AS BEGIN

	SET @Text = replace(replace(@Text,char(10),''),char(13),'')

	DECLARE @expres  VARCHAR(50) = '%[~,#,$,&,*,(,),!,=,\,<,>,;'']%'
      
      WHILE PATINDEX( @expres, @Text ) > 0
          SET @Text = Replace(REPLACE( @Text, SUBSTRING(@Text, PATINDEX( @expres, @Text ), 1 ),''),'-','')   
    SET @Text = LTRIM(RTRIM(@Text))
	
	SET @Text =  REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
@Text
,'®','')
,'©','')
,'ö','o')
,'ë','e')
,'ä','a')
,'ü','u')
,'ú','u')
,'í','i')
,'ï','i')
,'™','')
,'é','e')
,'²','2')
,'è','e')
,'—','-')
,'–','-')
,'ó','o')
,'•',' ')
,'…','.')
,'ô','o')
,'â','a')
,'á','a')
,'ê','e')
,'è','e')
,'’',' ')
,'·',' ')
,'à','a')
,'å','a')
,'ã','a')
,'â€™',' ')
,'a€s','as')
,'ø','o')
,'ñ','n')
,'î','i')
,'ç','c')
,'Ç','C')
,'Ã','A')
,'”','"')
,'“','"')
,'Á','A')
,'¢','c')
,'Ã','A')
,'Å','A')
,'¶','S')
,'×','x')
,'†','')
,'š','')
,'¤','')
,'µ','')
,'õ','')
,'€','')
,'‘','')
,'Õ','')
,'ð','')
,'Ò','')
,'¨','')
,'º','')
,'°','')
,'ì','')
,'ƒ','')
,'ÿ','')
,'ß','')
,'«','')
,'»','')
,'Æ','')
,'¬','')
,'Ù','')
,'ý','')
,'û','')
,'|','')
,'Â','') -- added 4/4/17 11:00 a.m.
,'Š','S') -- added 5/2/17 11:22 a.m.
,'%','') -- added vn370313  3/24/17 11:22 a.m.
,'[','') -- added vn370313  3/24/17 11:22 a.m.
,']','') -- added vn370313  3/24/17 11:22 a.m.
    RETURN      @Text
END


