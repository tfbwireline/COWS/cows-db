USE [COWS]
GO
/****** Object:  UserDefinedFunction [dbo].[getPropertyID]    Script Date: 05/21/2019 9:43:25 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <08/01/2011>
-- Description:	<To retrieve property ID>
-- =============================================
-- =============================================
-- Changed By:	Naidu Vattigunta
-- Changed date: 09/20/2011 
-- Description:	Get Property ID for Disconnects
-- =============================================
--Select dbo.getPropertyID(288,2)
ALTER FUNCTION [dbo].[getPropertyID] 
(
	@OrderID		Int,
	@CategoryID		TinyInt
)
RETURNS int
AS 
BEGIN
	DECLARE @PropertyID		Int
	DECLARE @ProductID		Int
	DECLARE @OrderTypeID	Int
	DECLARE @TrptFlg		Bit
	DECLARE @IsIntlFlg		bit
	DECLARE @MDSFlg			bit
	DECLARE @OrderActionID	TinyInt

	DECLARE @PRNT_FTN	varchar(20)
	DECLARE @PROD_TYPE_CD				Varchar(2)
	DECLARE @CPE_CPE_ORDR_TYPE_CD		Varchar(4)
	DECLARE @TTRPT_MNGD_DATA_SRVC_CD	Varchar(1)
	DECLARE @FSA_ORDR_TYPE_CD			Varchar(2)

	DECLARE @RelatedFTN					varchar(20)
	DECLARE @TTRPT_ACCS_TYPE_CD		VARCHAR(4)
	DECLARE @FSA_ORDR_SUB_TYPE		VARCHAR(7)
	--DECLARE @DiscCnclFlag	Bit = 0
	
	SET @MDSFlg = '0'

	
	SET @RelatedFTN = ''
	SET @PRNT_FTN = ''
	SET @TTRPT_ACCS_TYPE_CD = ''
	
	
	IF @CategoryID IN (2,6)
		BEGIN
		
			SELECT	@ProductID					=	lp.PROD_TYPE_ID
					,@OrderTypeID				=	lo.ORDR_TYPE_ID
					,@FSA_ORDR_TYPE_CD			=	f.ORDR_TYPE_CD
					,@TrptFlg					=	lp.TRPT_CD
					,@PROD_TYPE_CD				=	ISNULL(f.PROD_TYPE_CD, '')
					,@CPE_CPE_ORDR_TYPE_CD		=	ISNULL(f.CPE_CPE_ORDR_TYPE_CD,'')
					,@TTRPT_MNGD_DATA_SRVC_CD	=	ISNULL(f.TTRPT_MNGD_DATA_SRVC_CD, '')
					,@IsIntlFlg					=	o.DMSTC_CD
					,@OrderActionID				=	f.ORDR_ACTN_ID
					
					,@RelatedFTN				=	ISNULL(f.RELTD_FTN,'')
					,@PRNT_FTN					=	ISNULL(f.PRNT_FTN,'')
					,@TTRPT_ACCS_TYPE_CD		=	ISNULL(f.TTRPT_ACCS_TYPE_CD,'')
					,@FSA_ORDR_SUB_TYPE         =   f.ORDR_SUB_TYPE_CD
		
		From	dbo.FSA_ORDR		f	WITH (NOLOCK) 
	INNER JOIN	dbo.LK_PROD_TYPE	lp	WITH (NOLOCK)	ON	lp.FSA_PROD_TYPE_CD = f.PROD_TYPE_CD
	INNER JOIN	dbo.LK_ORDR_TYPE	lo  WITH (NOLOCK)	ON	lo.FSA_ORDR_TYPE_CD = f.ORDR_TYPE_CD
	INNER JOIN	dbo.ORDR			o	WITH (NOLOCK)	ON	o.ORDR_ID = f.ORDR_ID
		WHERE	f.ORDR_ID = @OrderID

		--MDS Flag
		IF	@PROD_TYPE_CD				IN	('MN','SE')	OR
			@CPE_CPE_ORDR_TYPE_CD		=	'MNS'		OR
			@TTRPT_MNGD_DATA_SRVC_CD	=	'Y'
			BEGIN
				SET @MDSFlg = '1'
			END	
		
		IF ((@PROD_TYPE_CD = 'CP') OR (@PROD_TYPE_CD = 'IP'))
			BEGIN
				SET @TrptFlg = 0
				IF @TTRPT_ACCS_TYPE_CD <> ''
					SET @TrptFlg = 1 
				
				IF @OrderTypeID = 7
					BEGIN
						IF EXISTS
							(
								SELECT		'X'
									FROM	dbo.FSA_ORDR_CPE_LINE_ITEM WITH (NOLOCK)
									WHERE	ORDR_ID		=	@OrderID
										AND	LTRIM(RTRIM(ISNULL(CNTRC_TYPE_ID,''))) = 'Purchase'
							)
							BEGIN
								SET @MDSFlg = 0
							END
					END
			END
		
		IF 	@OrderTypeID = 8
			BEGIN
				IF @PRNT_FTN = ''
					RETURN @PropertyID
				ELSE
					IF NOT EXISTS
						(
							SELECT		'X'
								FROM	dbo.FSA_ORDR WITH (NOLOCK)
								WHERE	FTN = @PRNT_FTN
						)
						BEGIN
							RETURN @PropertyID
						END
					ELSE
						BEGIN
							IF EXISTS
								(
									SELECT		'X'
										FROM	dbo.FSA_ORDR WITH (NOLOCK)
										WHERE	FTN				=	@PRNT_FTN
											AND ORDR_SUB_TYPE_CD = 'MPLSVAS'
								)
								BEGIN
									RETURN @PropertyID
								END
						END
			END
				
	 		
			IF @OrderActionID = 1
				BEGIN
					SELECT @PropertyID = PPRT_ID
						FROM	dbo.LK_PPRT	WITH (NOLOCK)
						WHERE	ORDR_TYPE_ID	IS NULL
							AND	ISNULL(PROD_TYPE_ID,'')	=  CASE @ProductID
														WHEN 9 THEN PROD_TYPE_ID
														ELSE ''
													END

				END
			ELSE IF @OrderActionID = 2 AND @OrderTypeID <> 7
				BEGIN
					IF @OrderTypeID = 9
						BEGIN
							SET @TrptFlg = 0
							SET @MDSFlg = 0
							SET @IsIntlFlg = 0
						END
				
					SELECT @PropertyID	= PPRT_ID
						FROM	dbo.LK_PPRT	WITH (NOLOCK)
						WHERE	ORDR_TYPE_ID	=	@OrderTypeID
							AND	PROD_TYPE_ID	=	@ProductID
							AND RELTD_FTN_CD    =	0
							AND ISNULL(ORDR_SUB_TYPE_CD,'') = CASE @FSA_ORDR_SUB_TYPE
													WHEN 'MPLSVAS' THEN 14
													ELSE ''
													END
							AND	TRPT_CD			=	CASE @PROD_TYPE_CD
														WHEN 'GP' THEN TRPT_CD
														ELSE CASE 	
																WHEN @FSA_ORDR_TYPE_CD IN ('CN','BC','AR','RO','AN') THEN TRPT_CD
																ELSE @TrptFlg
															END
														END
							AND	INTL_CD			=	CASE @PROD_TYPE_CD
														WHEN 'GP' THEN	INTL_CD	
														ELSE CASE 	
																WHEN @FSA_ORDR_TYPE_CD ='CN' AND 
																	@FSA_ORDR_SUB_TYPE = 'MPLSVAS'
																	THEN 0
																WHEN @FSA_ORDR_TYPE_CD = 'CN' AND @CategoryID = 2 THEN INTL_CD
																WHEN @FSA_ORDR_TYPE_CD IN ('BC','AR','RO','AN') THEN INTL_CD
																ELSE @IsIntlFlg
																END
														END
							AND	MDS_CD			=	CASE @PROD_TYPE_CD
														WHEN 'GP' THEN	MDS_CD	
														ELSE CASE 	
																WHEN @FSA_ORDR_TYPE_CD IN ('CN','BC','AR','RO','AN') THEN MDS_CD
																ELSE @MDSFlg
																END
													END
							AND ORDR_CAT_ID		=	@CategoryID
							--AND	(
							--		((@DiscCnclFlag = 1) AND (PPRT_NME IN ('IntlDISCONNETTrptCancel','IntlDISCOFFNETTrptCancel')))
							--		 OR
							--		((@DiscCnclFlag = 0 AND (PPRT_NME NOT IN ('IntlDISCONNETTrptCancel','IntlDISCOFFNETTrptCancel'))))
							--	)																			 
				 END
			ELSE IF @OrderActionID = 2 AND @OrderTypeID = 7
				BEGIN
					IF (ISNULL(@RelatedFTN, '') <> '')
						BEGIN
						  SELECT @PropertyID	= PPRT_ID
								FROM	dbo.LK_PPRT	WITH (NOLOCK)
								WHERE	ORDR_TYPE_ID	=	@OrderTypeID
									AND	PROD_TYPE_ID	=	@ProductID
									AND ISNULL(ORDR_SUB_TYPE_CD,'') = CASE @FSA_ORDR_SUB_TYPE
															WHEN 'MPLSVAS' THEN 14
															ELSE ''
															END 
									AND	TRPT_CD			=	CASE @PROD_TYPE_CD
																WHEN 'GP' THEN TRPT_CD
																ELSE 
																	CASE 	
																WHEN @FSA_ORDR_TYPE_CD IN ('CN','BC','AR','RO') THEN TRPT_CD
																ELSE @TrptFlg
																END	
															END
									AND	INTL_CD			=	CASE @PROD_TYPE_CD
																WHEN 'GP' THEN	INTL_CD
																ELSE 
																CASE 
																	WHEN @FSA_ORDR_TYPE_CD ='CN' AND 
																		@FSA_ORDR_SUB_TYPE = 'MPLSVAS'
																		THEN 0
																	WHEN @FSA_ORDR_TYPE_CD IN ('CN','BC','AR','RO') THEN INTL_CD
																	ELSE @IsIntlFlg
																END														
															END
			 						AND	MDS_CD			=	CASE @PROD_TYPE_CD
																WHEN 'GP' THEN	MDS_CD	
																ELSE 
																CASE 	
																	WHEN @FSA_ORDR_TYPE_CD IN  ('CN','BC','AR','RO') THEN MDS_CD
																	ELSE @MDSFlg
																END
															END
									AND RELTD_FTN_CD    =	CASE @CategoryID
																WHEN 6 THEN '0'
																ELSE  CASE @PROD_TYPE_CD
																			WHEN 'GP' THEN '0'
																			ELSE CASE @TrptFlg
																					WHEN	'1'	THEN '0'
																					ELSE	'1'
																					END
																		END
															END
									AND ORDR_CAT_ID		=	@CategoryID						
															
						END
					ELSE 
						BEGIN
							SELECT @PropertyID	= PPRT_ID
							FROM  dbo.LK_PPRT	 WITH (NOLOCK)
							WHERE ORDR_TYPE_ID		=	@OrderTypeID
								AND	PROD_TYPE_ID	=	@ProductID
								AND ISNULL(ORDR_SUB_TYPE_CD,'') = CASE @FSA_ORDR_SUB_TYPE
													WHEN 'MPLSVAS' THEN 14
													ELSE ''
													END
								AND	TRPT_CD			=	CASE @PROD_TYPE_CD
																WHEN 'GP' THEN TRPT_CD
																ELSE 
																	CASE 	
																WHEN @FSA_ORDR_TYPE_CD IN ('CN','BC','AR','RO') THEN TRPT_CD
																ELSE @TrptFlg
																END	
															END
								AND	INTL_CD			=	CASE @PROD_TYPE_CD
															WHEN 'GP' THEN	INTL_CD	
															ELSE CASE 
																	WHEN @FSA_ORDR_TYPE_CD ='CN' AND 
																		@FSA_ORDR_SUB_TYPE = 'MPLSVAS'
																		THEN 0
																	WHEN @FSA_ORDR_TYPE_CD IN ('CN','BC','AR','RO') THEN INTL_CD
																	ELSE @IsIntlFlg
																END	
														END
			 					AND	MDS_CD			=	CASE @PROD_TYPE_CD
															WHEN 'GP' THEN	MDS_CD	
															ELSE 
																CASE 	
																	WHEN @FSA_ORDR_TYPE_CD IN ('CN','BC','AR','RO') THEN MDS_CD
																	ELSE @MDSFlg
																END
														END
								AND RELTD_FTN_CD    =	0
								AND ORDR_CAT_ID		=	@CategoryID
						END
				END
		END	

	ELSE
		BEGIN
			IF @CategoryID = 4 
				BEGIN
					SELECT	@ProductID		=	PROD_TYPE_ID
							,@OrderTypeID	=	ORDR_TYPE_ID
					FROM	dbo.NCCO_ORDR WITH (NOLOCK)
					WHERE	ORDR_ID = @OrderID
				END
			ELSE 
				BEGIN
					SELECT	@ProductID		=	lp.PROD_TYPE_ID
							,@OrderTypeID	=	lo.ORDR_TYPE_ID
					From	dbo.IPL_ORDR		i	WITH (NOLOCK)
				INNER JOIN	dbo.LK_PROD_TYPE	lp	WITH (NOLOCK)	ON	lp.PROD_TYPE_ID = i.PROD_TYPE_ID
				INNER JOIN	dbo.LK_ORDR_TYPE	lo  WITH (NOLOCK)	ON	lo.ORDR_TYPE_ID = i.ORDR_TYPE_ID
					WHERE	i.ORDR_ID = @OrderID
				END
			
			IF (@ProductID = 25)
			BEGIN
				SELECT Top 1 @PropertyID	= PPRT_ID
					FROM	dbo.LK_PPRT	WITH (NOLOCK)
					WHERE	ORDR_TYPE_ID	=	@OrderTypeID
						AND	PROD_TYPE_ID	=	@ProductID
						AND (CHARINDEX('w/oxNCI', PPRT_NME) > 0 OR ORDR_TYPE_ID IN (8,9))
					ORDER BY PPRT_ID 
			END
			ELSE IF (@ProductID IN (27,28,29)) --NCCO CPE/IPL/MNS
			BEGIN
				SELECT Top 1 @PropertyID	= PPRT_ID
					FROM	dbo.LK_PPRT	WITH (NOLOCK)
					WHERE	ORDR_TYPE_ID	=	@OrderTypeID
						AND	PROD_TYPE_ID	=	@ProductID
					ORDER BY PPRT_ID 
			END
			ELSE
			BEGIN
				SELECT Top 1 @PropertyID	= PPRT_ID
					FROM	dbo.LK_PPRT	WITH (NOLOCK)
					WHERE	ORDR_TYPE_ID	=	@OrderTypeID
						AND	PROD_TYPE_ID	=	@ProductID
						AND (CHARINDEX('wxNCIincludeMS', PPRT_NME) > 0 OR ORDR_TYPE_ID IN (8,9))
					ORDER BY PPRT_ID 
			END
		END

					 	
	RETURN @PropertyID

END