USE [COWS]
GO
_CreateObject 'FS','web','getVendorName'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <11/18/2011>
-- Description:	<To Retrieve Vendor Name on a INTL Transport order>
-- =============================================
ALTER FUNCTION [web].[getVendorName]
(
	@ORDR_ID	INT,
	@ORDR_CAT_ID SMALLINT,
	@PLTFRM_CD CHAR(2),
	@FSA_ORDR_TYPE_CD CHAR(2)
)
RETURNS VARCHAR(100)
AS
BEGIN

DECLARE @FSA_ORGNL_INSTL_ORDR_ID Varchar(20)
DECLARE @VNDR_NME	VARCHAR(100)

IF @ORDR_CAT_ID	=	2
	BEGIN
		IF @PLTFRM_CD != ''
			BEGIN					
				IF @FSA_ORDR_TYPE_CD	=	'DC'
					BEGIN
						SELECT	TOP 1 @FSA_ORGNL_INSTL_ORDR_ID 	= 	fcpe.FSA_ORGNL_INSTL_ORDR_ID
							FROM	dbo.FSA_ORDR f WITH (NOLOCK) 
						LEFT JOIN	dbo.FSA_ORDR_CPE_LINE_ITEM fcpe WITH (NOLOCK) ON	f.ORDR_ID = fcpe.ORDR_ID
							WHERE	f.ORDR_ID = @ORDR_ID
							   AND  fcpe.FSA_ORGNL_INSTL_ORDR_ID IS NOT NULL
							
							IF ISNULL(@FSA_ORGNL_INSTL_ORDR_ID,'')	<> ''
								BEGIN
									IF @PLTFRM_CD	= 'SF'
										BEGIN
											SELECT	TOP 1	@VNDR_NME	=	sf.VNDR_NME	
											FROM	dbo.FSA_ORDR	f	WITH (NOLOCK)
										LEFT JOIN	dbo.LK_VNDR		sf	WITH (NOLOCK)	ON	sf.VNDR_CD		=	f.CXR_ACCS_CD
											WHERE	(
														(f.FTN	=	@FSA_ORGNL_INSTL_ORDR_ID)
													) AND f.CXR_ACCS_CD IS NOT NULL
										END
									ELSE IF @PLTFRM_CD	= 'CP'
										BEGIN
											SELECT TOP 1	@VNDR_NME		=	cp.VNDR_NME
											FROM	dbo.FSA_ORDR	f	WITH (NOLOCK)
										LEFT JOIN	dbo.LK_VNDR		cp	WITH (NOLOCK)	ON	cp.VNDR_CD		=	f.VNDR_VPN_CD
											WHERE	(
														(f.FTN	=	@FSA_ORGNL_INSTL_ORDR_ID)
													) AND f.VNDR_VPN_CD IS NOT NULL
										END
									ELSE IF @PLTFRM_CD	= 'RS'
										BEGIN
											SELECT TOP 1	@VNDR_NME		=	rs.VNDR_NME
											FROM	dbo.FSA_ORDR	f	WITH (NOLOCK)
										LEFT JOIN	dbo.LK_VNDR		rs	WITH (NOLOCK)	ON	rs.VNDR_CD		=	f.INSTL_VNDR_CD
											WHERE	(
														(f.FTN	=	@FSA_ORGNL_INSTL_ORDR_ID)
													) AND f.INSTL_VNDR_CD IS NOT NULL
										END
								END
					END	
			END
		
	END

IF @ORDR_CAT_ID	=	6
	BEGIN
		 SELECT TOP 1 @VNDR_NME =  v.VNDR_NME FROM dbo.FSA_ORDR_CPE_LINE_ITEM  fc WITH (NOLOCK)
				INNER JOIN dbo.LK_VNDR v WITH (NOLOCK) on v.VNDR_CD = fc.CXR_ACCS_CD
			WHERE fc.ORDR_ID = @ORDR_ID and ISNULL(fc.CXR_ACCS_CD,'') <> ''
	END
	

RETURN @VNDR_NME
END
	