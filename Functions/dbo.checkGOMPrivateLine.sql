USE [COWS]
GO
_CreateObject 'FS','dbo','checkGOMPrivateLine'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <09/12/2011>
-- Description:	<To verify and assign property based on NRM status>
-- =============================================
--Select dbo.checkGOMPrivateLine(375)
ALTER FUNCTION [dbo].[checkGOMPrivateLine]
(
	@OrderID INT
)
RETURNS INT
AS
BEGIN
DECLARE @ProductID		Int
DECLARE @OrderTypeID	Int
DECLARE @TrptFlg		Bit
DECLARE @IsIntlFlg		bit
DECLARE @MDSFlg			bit
DECLARE @CPResale		bit
DECLARE @PropertyID		Int
DECLARE @PL	BIT
SET @PropertyID = 0
SET @PL = '0'
	IF EXISTS
		(
			SELECT		'X'
				FROM	dbo.ORDR_NTE WITH (NOLOCK)
				WHERE	ORDR_ID  = @OrderID
					AND  NTE_TXT = 'NRM Status:Pending OE & PL.Systematically completing NRM: Pending OE & PL task.'
		)
		BEGIN
			SET @PL = '1'			
		END
		
	SELECT @PropertyID = ISNULL(PPRT_ID,0) FROM dbo.ORDR WITH (NOLOCK) WHERE ORDR_ID = @OrderID
	IF @PropertyID <> 0
		BEGIN
			SELECT	@ProductID		=	lp.PROD_TYPE_ID,
					@OrderTypeID	=	lp.ORDR_TYPE_ID,
					@TrptFlg		=	lp.TRPT_CD,
					@MDSFlg			=	lp.MDS_CD,
					@IsIntlFlg		=	lp.INTL_CD,
					@CPResale		=	CASE ISNULL(o.PLTFRM_CD,'')
											WHEN 'CP'	THEN '1'
											WHEN 'RS'	THEN '1'
											ELSE '0'
											END 
				FROM	dbo.LK_PPRT lp	WITH (NOLOCK)
			INNER JOIN	dbo.ORDR	o	WITH (NOLOCK)	ON	o.PPRT_ID	=	lp.PPRT_ID
				WHERE	lp.PPRT_ID	=	@PropertyID
					AND	o.ORDR_ID	=	@OrderID
			
			IF @PL = '1'
				BEGIN 
					SELECT @PropertyID	=	PPRT_ID
						FROM	dbo.LK_PPRT WITH (NOLOCK)
						WHERE	PROD_TYPE_ID = @ProductID		
							AND	ORDR_TYPE_ID = @OrderTypeID
							AND	TRPT_CD	= 	@TrptFlg
							AND	MDS_CD	=	@MDSFlg
							AND	INTL_CD	=	@IsIntlFlg
							AND	PPRT_NME LIKE CASE	@CPResale
													WHEN '0' THEN  '%wNRMOEPL%'	
													ELSE '%CCD%'
												END
				END
			ELSE
				BEGIN
					SELECT @PropertyID	=	PPRT_ID
						FROM	dbo.LK_PPRT WITH (NOLOCK)
						WHERE	PROD_TYPE_ID = @ProductID		
							AND	ORDR_TYPE_ID = @OrderTypeID
							AND	TRPT_CD	= 	@TrptFlg
							AND	MDS_CD	=	@MDSFlg
							AND	INTL_CD	=	@IsIntlFlg
							AND	PPRT_NME LIKE CASE	@CPResale
													WHEN '0' THEN  '%w/oNRMOEPL%'	
													ELSE '%CCD%'
												END
				END	
					
		END	
RETURN @PropertyID		

END
