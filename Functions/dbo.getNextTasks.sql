USE COWS
GO
_CreateObject 'FTV','dbo','getNextTasks'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================  
-- Created by : Joseph Nixon
-- Modified by:	Ramesh Ragi
-- Create date: 03/01/2010
-- Description: Returns Desired Tasks by OrderID, Current TaskID, and OptionID
-- This Function is copied from CAPT, removed code relating to preReqWrkGrpID and SMException
-- ============================================= 

ALTER FUNCTION [dbo].[getNextTasks]
(
	 @OrderID		INT
	,@TaskID		SMALLINT
	,@OptionID		TINYINT
	,@ProfileID		SMALLINT
)
RETURNS

@NextTasks TABLE
(
	 OrderID				INT
	,SMID					INT
	,PreReqWGPatternID		INT
	,PreReqWGProfileID		INT
	,PreReqTaskID			SMALLINT
	,DesiredWGPatternID		INT
	,DesiredWGProfileID		INT
	,DesiredTaskID			SMALLINT
	,HasDependentTasks		BIT
	,HasDependentProfiles	BIT
)
AS  
BEGIN

--DECLARE @OrderID		INT
--DECLARE @TaskID		SMALLINT
--DECLARE @OptionID		TINYINT
--SET @OrderID = '13476877, 13476878, 13475699'
--SET @TaskID = 303
--SET @OptionID = 91
--
--DECLARE @NextTasks TABLE
--(
--	 SprintTNID			INT
--	,SMID				INT
--	,PreReqWGPatternID	INT
--	,PreReqWGProfileID	INT
--	,PreReqTaskID		SMALLINT
--	,DesiredWGPatternID	INT
--	,DesiredWGProfileID	INT
--	,DesiredTaskID		SMALLINT
--	,HasDependentTasks	BIT
--	,HasDependentProfiles	BIT
--  ,
--)
	-- Get Next Tasks by SM tables
	INSERT INTO @NextTasks
	SELECT
		 o.ORDR_ID
		,sm.SM_ID
		,sm.PRE_REQST_WG_PTRN_ID
		,pt.PRE_REQST_WG_PROF_ID
		,pf.PRE_REQST_TASK_ID
		,sm.DESRD_WG_PTRN_ID
		,pt.DESRD_WG_PROF_ID
		,pf.DESRD_TASK_ID
		,0 AS HasDependentTasks
		,0 AS HasDependentProfiles
	FROM dbo.ORDR o WITH (NOLOCK)
	INNER JOIN	dbo.LK_PPRT pp WITH (NOLOCK) ON o.PPRT_ID	=	pp.PPRT_ID
	INNER JOIN	dbo.SM		sm WITH (NOLOCK) ON pp.SM_ID	=	sm.SM_ID
	INNER JOIN	dbo.WG_PTRN_SM pt WITH (NOLOCK) ON sm.PRE_REQST_WG_PTRN_ID = pt.WG_PTRN_ID
	INNER JOIN	dbo.WG_PROF_SM pf WITH (NOLOCK) ON pt.PRE_REQST_WG_PROF_ID = pf.WG_PROF_ID 
	WHERE pf.PRE_REQST_TASK_ID = @TaskID
	AND pf.OPT_ID = @OptionID
	AND o.Ordr_ID = @OrderID
	AND pf.WG_PROF_ID = @ProfileID

--SELECT 'NextTasks' AS TableName, * FROM @NextTasks

	-- Set Desired Profile to PreReq Profile if Desired Task is NOT the End
	UPDATE @NextTasks
	SET DesiredWGProfileID = PreReqWGProfileID
	WHERE DesiredTaskID <> 1

--SELECT 'NextTasks' AS TableName, * FROM @NextTasks

	-- Set Desired Pattern to PreReq Pattern if Desired Profile is NOT the End
	UPDATE @NextTasks
	SET DesiredWGPatternID = PreReqWGPatternID
	WHERE DesiredWGProfileID <> 1

--SELECT 'NextTasks' AS TableName, * FROM @NextTasks

	DECLARE @DependentTasks TABLE
	(
		 OrderID			INT
		,SMID				SMALLINT 
		,WGPatternID		INT
		,WGProfileID		INT
		,PreReqTaskID		SMALLINT
		,DesiredTaskID		SMALLINT
		,PreReqTaskGrpID	TINYINT
		,Completed			BIT			
	)

	--	Find Dependent Tasks
	INSERT INTO @DependentTasks
	SELECT
		 nt.OrderID
		,nt.SMID
		,sm.PRE_REQST_WG_PTRN_ID
		,pt.PRE_REQST_WG_PROF_ID
		,pf.PRE_REQST_TASK_ID
		,pf.DESRD_TASK_ID
		,pf.PRE_REQST_TASK_GRP_REQR_CD
		,CASE pf.PRE_REQST_TASK_ID	
			WHEN @TaskID THEN 1
			WHEN 1 THEN	1
			ELSE 0
		 END AS Completed
	FROM @NextTasks nt
	INNER JOIN dbo.SM sm WITH (NOLOCK) ON nt.SMID = sm.SM_ID
	INNER JOIN dbo.WG_PTRN_SM pt WITH (NOLOCK) ON sm.PRE_REQST_WG_PTRN_ID = pt.WG_PTRN_ID
	INNER JOIN dbo.WG_PROF_SM pf WITH (NOLOCK) ON pt.PRE_REQST_WG_PROF_ID = pf.WG_PROF_ID
	WHERE nt.PreReqWGPatternID = pt.WG_PTRN_ID
	AND nt.PreReqWGProfileID = pf.WG_PROF_ID
	AND nt.DesiredTaskID = pf.DESRD_TASK_ID

	--SELECT 'DependentTasks' AS TableName, * FROM @DependentTasks

	--	Complete tasks with the same DesiredTaskID as the current Task 
	--	and the Current Task has a PreReqTaskGrpID of 0 because this
	--	is the exception group id and is exempt from dependencies.
	UPDATE dt
	SET Completed = 1
	FROM @DependentTasks dt
	WHERE EXISTS
	(
		SELECT 'X'
		FROM @DependentTasks dt1
		WHERE dt1.PreReqTaskID = @TaskID
		AND dt1.OrderID = dt.OrderID
		AND dt1.WGPatternID = dt.WGPatternID
		AND dt1.WGProfileID = dt.WGProfileID
		AND dt1.DesiredTaskID = dt.DesiredTaskID
		AND dt1.PreReqTaskGrpID = 0
	)

	--SELECT 'DependentTasks' AS TableName, * FROM @DependentTasks

	--	Complete tasks with a PreReqTaskGrpID of 0
	--	because nothing should depend on these.
	UPDATE @DependentTasks
	SET Completed = 1
	WHERE PreReqTaskGrpID = 0

	--SELECT 'DependentTasks3' AS TableName, * FROM @DependentTasks


	-- Complete the tasks that have a record in CaptActiveTasks which is complete
	UPDATE	dt
		SET		dt.Completed = 1
		FROM	@DependentTasks dt	
			INNER JOIN dbo.ACT_TASK at WITH (NOLOCK)
			ON at.ORDR_ID = dt.OrderID
			AND at.WG_PROF_ID = dt.WGProfileID
			AND at.TASK_ID = dt.PreReqTaskID
	WHERE at.STUS_ID = 2
	AND dt.Completed = 0				

	--Select * From LK_STUS


--SELECT 'DependentTasks' AS TableName, * FROM @DependentTasks

	-- Complete the tasks that have the same PreReqTaskGrpID as another Task
	-- from Dependent Task Table which is already Completed
	--UPDATE dt
	--SET dt.Completed = 1
	--FROM @DependentTasks dt	
	--WHERE dt.Completed = 0
	--AND EXISTS
	--(
	--	SELECT 'X'
	--	FROM @DependentTasks dt1
	--	WHERE dt1.OrderID = dt.OrderID
	--	AND dt1.WGPatternID = dt.WGPatternID
	--	AND dt1.WGProfileID = dt.WGProfileID
	--	AND dt1.DesiredTaskID = dt.DesiredTaskID
	--	AND dt1.PreReqTaskGrpID = dt.PreReqTaskGrpID
	--	AND dt1.Completed = 1
	--)

--SELECT 'DependentTasks' AS TableName, * FROM @DependentTasks

	-- Set tasks that have a dependent task
	UPDATE	nt
	SET		HasDependentTasks = 1
	FROM	@NextTasks nt
	INNER JOIN @DependentTasks dt
		ON nt.OrderID = dt.OrderID
		AND nt.PreReqWGPatternID = dt.WGPatternID
		AND nt.PreReqWGProfileID = dt.WGProfileID
		AND nt.DesiredTaskID = dt.DesiredTaskID
	WHERE dt.Completed = 0

--SELECT 'NextTasks' AS TableName, * FROM @NextTasks

	DECLARE @DependentProfiles TABLE
	(
		 OrderID			INT
		,SMID				SMALLINT 
		,WGPatternID		INT
		,PreReqWGProfileID	INT
		,DesiredWGProfileID	INT
		,Completed			BIT			
	)

	--	Find Dependent Profiles
	INSERT INTO @DependentProfiles
	SELECT
		 nt.OrderID
		,nt.SMID
		,nt.PreReqWGPatternID
		,pt.PRE_REQST_WG_PROF_ID
		,nt.DesiredWGProfileID
		,CASE pt.PRE_REQST_WG_PROF_ID
			WHEN nt.PreReqWGProfileID THEN 1
			ELSE 0
		 END AS Completed
	FROM @NextTasks nt
	INNER JOIN dbo.SM sm WITH (NOLOCK) ON nt.SMID = sm.SM_ID
	INNER JOIN dbo.WG_PTRN_SM pt WITH (NOLOCK) ON sm.PRE_REQST_WG_PTRN_ID = pt.WG_PTRN_ID
	WHERE nt.PreReqWGPatternID = pt.WG_PTRN_ID
	AND nt.DesiredWGProfileID = pt.DESRD_WG_PROF_ID
	AND nt.DesiredWGProfileID <> nt.PreReqWGProfileID

--SELECT 'DependentProfiles' AS TableName, * FROM @DependentProfiles

	--	Find Completed Profiles
	UPDATE dp
	SET Completed = 1
	FROM @DependentProfiles dp
	INNER JOIN dbo.WG_PROF_STUS pfs WITH (NOLOCK)
		ON dp.WGPatternID = pfs.WG_PTRN_ID
		AND dp.PreReqWGProfileID = pfs.WG_PROF_ID
	WHERE pfs.ORDR_ID = dp.OrderID
	AND pfs.STUS_ID = 2

--SELECT 'DependentProfiles' AS TableName, * FROM @DependentProfiles

	-- Update tasks that have a dependent profile
	UPDATE nt
	SET HasDependentProfiles = 1
	FROM @NextTasks nt
	INNER JOIN @DependentProfiles dp
		ON nt.OrderID = dp.OrderID
		AND nt.PreReqWGPatternID = dp.WGPatternID
--		AND nt.PreReqWGProfileID = dp.PreReqWGProfileID
		AND nt.DesiredWGProfileID = dp.DesiredWGProfileID
	WHERE dp.Completed = 0

--SELECT 'NextTasks' AS TableName, * FROM @NextTasks

	-- If the Desired Task is the End of the Profile
	IF EXISTS
	(
		SELECT 'X' FROM @NextTasks
		WHERE DesiredTaskID = 1
		AND DesiredWGProfileID <> 1
	)
	BEGIN
		-- Get Desired Task from Desired Profile
		INSERT INTO @NextTasks
		SELECT
			 nt.OrderID
			,nt.SMID
			,nt.PreReqWGPatternID
			,nt.PreReqWGProfileID
			,nt.PreReqTaskID
			,nt.DesiredWGPatternID
			,nt.DesiredWGProfileID
			,pf.DESRD_TASK_ID
			,nt.HasDependentTasks
			,nt.HasDependentProfiles
		FROM @NextTasks nt
		INNER JOIN dbo.WG_PROF_SM pf WITH (NOLOCK) ON nt.DesiredWGProfileID = pf.WG_PROF_ID
		WHERE pf.PRE_REQST_TASK_ID = 0
		AND nt.DesiredTaskID = 1
		AND nt.DesiredWGProfileID <> 1

--SELECT 'NextTasks' AS TableName, * FROM @NextTasks

		DELETE @NextTasks
		WHERE DesiredTaskID = 1
		AND DesiredWGProfileID <> 1

--SELECT 'NextTasks' AS TableName, * FROM @NextTasks

	END

	-- If the Desired Profile is the End of the Pattern
	IF EXISTS
	(
		SELECT 'X' FROM @NextTasks
		WHERE DesiredTaskID = 1
		AND DesiredWGProfileID = 1
		AND DesiredWGPatternID <> 1
	)
	BEGIN
		-- Get Desired Task and Desired Profile from Desired Pattern
		INSERT INTO @NextTasks
		SELECT
			 nt.OrderID
			,nt.SMID
			,nt.PreReqWGPatternID
			,nt.PreReqWGProfileID
			,nt.PreReqTaskID
			,nt.DesiredWGPatternID
			,pt.DESRD_WG_PROF_ID
			,pf.DESRD_TASK_ID
			,nt.HasDependentTasks
			,nt.HasDependentProfiles
		FROM @NextTasks nt
		INNER JOIN dbo.WG_PTRN_SM pt WITH (NOLOCK) ON nt.DesiredWGPatternID= pt.WG_PTRN_ID
		INNER JOIN dbo.WG_PROF_SM pf WITH (NOLOCK) ON pt.DESRD_WG_PROF_ID= pf.WG_PROF_ID
		WHERE pt.PRE_REQST_WG_PROF_ID = 0
		AND pf.PRE_REQST_TASK_ID= 0
		AND nt.DesiredTaskID = 1
		AND nt.DesiredWGProfileID = 1
		AND nt.DesiredWGPatternID <> 1

--SELECT 'NextTasks' AS TableName, * FROM @NextTasks

		DELETE @NextTasks
		WHERE DesiredTaskID = 1
		AND DesiredWGProfileID = 1
		AND DesiredWGPatternID <> 1

--SELECT 'NextTasks' AS TableName, * FROM @NextTasks

	END
	
	RETURN 

END  
