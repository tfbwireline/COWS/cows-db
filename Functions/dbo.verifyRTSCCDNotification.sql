USE COWS
GO
_CreateObject 'FS','dbo','verifyRTSCCDNotification'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Ramesh Ragi>
-- Create date: <01/10/2014>
-- Description:	<Verify if order is in xNCI RTS Response Received/xNCI CCD Ready>
-- =============================================
ALTER FUNCTION dbo.verifyRTSCCDNotification
(
	@ORDR_ID INT
)
RETURNS BIT
AS
BEGIN
DECLARE @bReturn BIT = 0
	IF EXISTS
		(
			SELECT 'X'
				FROM	dbo.ACT_TASK WITH (NOLOCK)
				WHERE	ORDR_ID = @ORDR_ID
					AND TASK_ID IN (211,219)
					AND STUS_ID = 0
		)
		SET @bReturn = 1
	
	RETURN @bReturn



END
GO

