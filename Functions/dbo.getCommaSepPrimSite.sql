USE [COWS]
GO
_CreateObject 'FS','dbo','getCommaSepPrimSite'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		jrg7298
-- Create date: 10/09/2015
-- Description:	This UDF returns Comma seperated CPT Prim Sites.
-- =========================================================

ALTER FUNCTION [dbo].[getCommaSepPrimSite] 
(
	@CPT_ID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @PrimSite VARCHAR(Max)   
	
		SELECT	@PrimSite = COALESCE(@PrimSite + ',', '')   + ISNULL(lp.CPT_PRIM_SITE, '')
			FROM		dbo.CPT_PRIM_SITE cp WITH (NOLOCK)    INNER JOIN
						dbo.LK_CPT_PRIM_SITE lp WITH (NOLOCK) ON lp.CPT_PRIM_SITE_ID = cp.CPT_PRIM_SITE_ID
			WHERE		CPT_ID	=	@CPT_ID
	
	RETURN @PrimSite
END
GO
