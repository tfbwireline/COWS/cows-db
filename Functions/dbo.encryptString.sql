USE [COWS]
GO
_CreateObject 'FS','dbo','encryptString'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:		sbg9814
-- Create date: 08/06/2011
-- Description:	Encrypt an Input string into Varbinary format.
-- ======================================================================

ALTER FUNCTION [dbo].[encryptString]
(
	@InputString Varchar(Max)
)
RETURNS Varbinary(Max)
AS
BEGIN
	RETURN EncryptByKey(Key_GUID('FS@K3y'), @InputString)
END
GO


