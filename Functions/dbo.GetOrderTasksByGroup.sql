USE [COWS]
GO
_CreateObject 'FS','dbo','GetOrderTasksByGroup'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =========================================================
-- Author:		Kyle Wichert
-- Create date: 2012-02-07
-- Description:	This SP is used to get Comma seperated FTNs
-- =========================================================

ALTER FUNCTION [dbo].[GetOrderTasksByGroup] 
(
	@OrderID	Int,
	@GroupID	SmallInt
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @list VARCHAR(max)
	If @GroupID = 0
		SET @list = ''
	ELSE IF @GroupID = 999
		BEGIN
			SELECT @list = COALESCE(@list + ',', '') + lt.TASK_NME + ' Pending'
			FROM			dbo.ACT_TASK		at WITH (NOLOCK)
				INNER JOIN	dbo.LK_TASK			lt WITH (NOLOCK) ON	at.TASK_ID	=	lt.TASK_ID
			WHERE	at.ORDR_ID	=	@OrderID
				AND	at.STUS_ID	=	0
				AND at.TASK_ID NOT IN (1000,1001)
		END
	ELSE 
		BEGIN
			SELECT @list = COALESCE(@list + ',', '') + lt.TASK_NME + ' Pending'
			FROM			dbo.ACT_TASK		at WITH (NOLOCK)
				INNER JOIN	dbo.LK_TASK			lt WITH (NOLOCK)	ON	at.TASK_ID	=	lt.TASK_ID
				INNER JOIN	dbo.MAP_GRP_TASK	mt WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
			WHERE	mt.GRP_ID  = @GroupID
				AND	at.ORDR_ID = @OrderID
				AND	at.STUS_ID = 0
				
			SELECT @list = COALESCE(@list + ',', '') + lt.TASK_NME + ' HOLD'
			FROM			dbo.ACT_TASK		at WITH (NOLOCK)
				INNER JOIN	dbo.LK_TASK			lt WITH (NOLOCK)	ON	at.TASK_ID	=	lt.TASK_ID
				INNER JOIN	dbo.MAP_GRP_TASK	mt WITH (NOLOCK)	ON	at.TASK_ID	=	mt.TASK_ID
			WHERE	mt.GRP_ID  = @GroupID
				AND	at.ORDR_ID = @OrderID
				AND	at.STUS_ID = 156
		END
	
	RETURN @List
END
