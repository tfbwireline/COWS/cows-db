USE [COWS]
GO
_CreateObject 'FS','dbo','GetCommaSepMDSMACActys'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER FUNCTION [dbo].[GetCommaSepMDSMACActys] 
(
	@EventID INT
)
RETURNS VARCHAR(max)
AS
BEGIN
	DECLARE @list VARCHAR(max)   
	SELECT @list = COALESCE(@list + ',', '') + CAST(lmm.MDS_MAC_ACTY_NME AS VARCHAR(100)) 
		FROM dbo.MDS_EVENT_MAC_ACTY mma WITH (NOLOCK) INNER JOIN
			 dbo.LK_MDS_MAC_ACTY lmm WITH (NOLOCK) ON lmm.MDS_MAC_ACTY_ID = mma.MDS_MAC_ACTY_ID
		WHERE (mma.EVENT_ID = @EventID)
	RETURN @List
END