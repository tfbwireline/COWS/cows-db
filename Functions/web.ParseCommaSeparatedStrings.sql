USE [COWS]
GO
_CreateObject 'FTV','web','ParseCommaSeparatedStrings'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================================
-- Author:		Naidu Vattigunta
-- Create date: 7/27/11
--The following is a general purpose UDF to split comma separated lists into individual items.
--Consider an additional input parameter for the delimiter, so that you can use any delimiter you like.
 -- ======================================================================
ALTER FUNCTION [web].[ParseCommaSeparatedStrings]
(
	@StringList varchar(max)
)
RETURNS 
@ParsedList table
(
	StringID varchar(50)
)
AS
BEGIN
	DECLARE @ID varchar(50), @Pos int

	SET @StringList = LTRIM(RTRIM(@StringList))+ ','
	SET @Pos = CHARINDEX(',', @StringList, 1)

	IF REPLACE(@StringList, ',', '') <> ''
	BEGIN
		WHILE @Pos > 0
		BEGIN
			SET @ID = LTRIM(RTRIM(LEFT(@StringList, @Pos - 1)))
 
--******************************************************************************************
			DECLARE	@NewLine		Char(2)
			SET		@NewLine	=	Char(13) + Char(10)
			SET		@ID			=	REPLACE(@ID, @NewLine, '')
--******************************************************************************************
 
			IF @ID <> ''
			BEGIN
				INSERT INTO @ParsedList (StringID) 
				VALUES ( @ID ) --Use Appropriate conversion
			END
			SET @StringList = RIGHT(@StringList, LEN(@StringList) - @Pos)
			SET @Pos = CHARINDEX(',', @StringList, 1)

		END
	END	
	RETURN
END


