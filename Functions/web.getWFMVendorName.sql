USE [COWS]
GO
_CreateObject 'FS','web','getWFMVendorName'
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Jagannath Gangi>
-- Create date: <01/01/2016>
-- Description:	<To Retrieve Vendor Name For a OrderID from WFM process>
-- =============================================
ALTER FUNCTION web.getWFMVendorName
(
	@ORDR_ID	INT
)
RETURNS VARCHAR(100)
AS
BEGIN

DECLARE @PltfrmCd CHAR(2)
DECLARE @FSA_ORDR_TYPE_CD CHAR(2)
DECLARE @ORDR_CAT_ID SMALLINT
DECLARE @VndrNme VARCHAR(100)
DECLARE @CxrAccsCd VARCHAR(50)
DECLARE @VndrVpnCd VARCHAR(50)
DECLARE @InstVndrCd VARCHAR(50)
DECLARE @FTN VARCHAR(50)

SET @VndrNme = ''

SELECT @PltfrmCd = od.PLTFRM_CD,
       @CxrAccsCd = ISNULL(fo.CXR_ACCS_CD,''),
	   @VndrVpnCd = ISNULL(fo.VNDR_VPN_CD,''),
	   @InstVndrCd = ISNULL(fo.INSTL_VNDR_CD,''),
	   @FTN = fo.FTN,
	   @ORDR_CAT_ID = od.ORDR_CAT_ID,
	   @FSA_ORDR_TYPE_CD = lot.FSA_ORDR_TYPE_CD
FROM dbo.ORDR od WITH (NOLOCK) INNER JOIN
     dbo.LK_PPRT P WITH (NOLOCK) ON od.PPRT_ID = P.PPRT_ID INNER JOIN
	 dbo.FSA_ORDR fo WITH (NOLOCK) ON fo.ORDR_ID=od.ORDR_ID LEFT JOIN 	
	 dbo.LK_ORDR_TYPE lot  WITH (NOLOCK) ON lot.ORDR_TYPE_ID = P.ORDR_TYPE_ID
WHERE od.ORDR_ID = @ORDR_ID

IF (@PltfrmCd = 'SF')
BEGIN
	SELECT @VndrNme = ISNULL(sf.VNDR_NME, '')
	FROM dbo.LK_VNDR sf	WITH (NOLOCK)
	WHERE sf.VNDR_CD = @CxrAccsCd

	IF (LEN(@VndrNme)<=0)
	BEGIN
		SELECT @VndrNme = ISNULL(rsf.VNDR_NME,ISNULL(web.getVendorName(@ORDR_ID, @ORDR_CAT_ID, @PltfrmCd, @FSA_ORDR_TYPE_CD),NULL))
		FROM dbo.FSA_ORDR rfs WITH (NOLOCK) INNER JOIN
			 dbo.LK_VNDR rsf	WITH (NOLOCK) ON rsf.VNDR_CD		=	rfs.CXR_ACCS_CD
		WHERE rfs.RELTD_FTN = @FTN
	END
END
ELSE IF (@PltfrmCd = 'CP')
BEGIN
	SELECT @VndrNme = ISNULL(cp.VNDR_NME, '')
	FROM dbo.LK_VNDR cp	WITH (NOLOCK)
	WHERE cp.VNDR_CD = @VndrVpnCd

	IF (LEN(@VndrNme)<=0)
	BEGIN
		SELECT @VndrNme = ISNULL(rcp.VNDR_NME,ISNULL(web.getVendorName(@ORDR_ID, @ORDR_CAT_ID, @PltfrmCd, @FSA_ORDR_TYPE_CD),NULL))
		FROM dbo.FSA_ORDR rfs WITH (NOLOCK) INNER JOIN
			 dbo.LK_VNDR rcp	WITH (NOLOCK) ON rcp.VNDR_CD		=	rfs.VNDR_VPN_CD
		WHERE rfs.RELTD_FTN = @FTN
	END
END
ELSE IF (@PltfrmCd = 'RS')
BEGIN
	SELECT @VndrNme = ISNULL(rs.VNDR_NME, '')
	FROM dbo.LK_VNDR rs	WITH (NOLOCK)
	WHERE rs.VNDR_CD = @InstVndrCd

	IF (LEN(@VndrNme)<=0)
	BEGIN
		SELECT @VndrNme = ISNULL(rrs.VNDR_NME,ISNULL(web.getVendorName(@ORDR_ID, @ORDR_CAT_ID, @PltfrmCd, @FSA_ORDR_TYPE_CD),NULL))
		FROM dbo.FSA_ORDR rfs WITH (NOLOCK) INNER JOIN
			 dbo.LK_VNDR rrs	WITH (NOLOCK) ON rrs.VNDR_CD		=	rfs.INSTL_VNDR_CD
		WHERE rfs.RELTD_FTN = @FTN
	END
END

RETURN @VndrNme
END
	